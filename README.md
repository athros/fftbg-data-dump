# This is the data from the FFTBattleground Bot.
Contained herein is the data provided by TheKillerNacho from his FFTBattleground bot from his channel located at: https://www.twitch.tv/fftbattleground

Folders:

1. FormerChampions
2. archive
   - This contains tournaments in previous Seasons. It is unordered.
3. allegiance
   - This contains each users allegiance via an INT.
4. classbonus
   - This contains each users XP Bonus classes for hte `!bonus` command
5. exp
   - This contains each user's total XP
6. gil
   - Each user's gil totals
7. logs
   - These are bot logs.
8. portrait
   - What portrait a user has set
9. prestige
   - This contains those users that have prestiged and the skills they have. The number of lines is the amount of times they have prestieged.
10. skillbonus
    - This contains the skill output for the `!bonus` command. 
11. tournament_*
    - These folders encapsulate an entire tournament.
12. userskills
    - This contains the skills list for every user who has a skill

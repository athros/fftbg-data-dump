Final Bets: white - 11 bets for 5,136G (66.5%, x0.50); black - 6 bets for 2,588G (33.5%, x1.98)

white bets:
Error72: 1,001G (19.5%, 69,304G)
BirbBrainsBot: 1,000G (19.5%, 190,246G)
E_Ballard: 548G (10.7%, 5,874G)
krombobreaker: 500G (9.7%, 4,996G)
Kurtis_E_Flush_: 500G (9.7%, 3,206G)
snkey: 436G (8.5%, 17,296G)
CosmicTactician: 300G (5.8%, 30,803G)
getthemoneyz: 298G (5.8%, 1,903,625G)
ValensEXP_: 252G (4.9%, 4,221G)
datadrivenbot: 200G (3.9%, 57,993G)
gorgewall: 101G (2.0%, 4,662G)

black bets:
Thyrandaal: 1,174G (45.4%, 1,174G)
Raixelol: 584G (22.6%, 21,290G)
Neech: 407G (15.7%, 407G)
bigbongsmoker: 312G (12.1%, 498G)
AllInBot: 100G (3.9%, 100G)
BartTradingCompany: 11G (0.4%, 118G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Ayeayex3
Female
Taurus
69
54
Knight
Elemental
Meatbone Slash
Equip Bow
Jump+3

Long Bow
Round Shield
Grand Helmet
White Robe
Wizard Mantle

Head Break, Speed Break, Mind Break, Dark Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball



Laserman1000
Male
Aquarius
50
51
Monk
Battle Skill
Abandon
Dual Wield
Ignore Terrain



Leather Hat
Black Costume
Magic Ring

Purification
Shield Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword, Surging Sword



PopsLIVE
Monster
Leo
53
80
Great Malboro










Gilgamensche
Male
Virgo
74
45
Monk
Time Magic
Abandon
Maintenance
Move-MP Up



Twist Headband
Brigandine
Reflect Ring

Spin Fist, Earth Slash, Purification, Chakra
Haste, Slow, Stop, Immobilize, Demi 2, Stabilize Time

Final Bets: white - 10 bets for 10,907G (67.0%, x0.49); brown - 12 bets for 5,366G (33.0%, x2.03)

white bets:
OneHundredFists: 5,327G (48.8%, 10,446G)
Mesmaster: 1,307G (12.0%, 1,307G)
DeathTaxesAndAnime: 1,188G (10.9%, 1,188G)
ayeayex3: 705G (6.5%, 705G)
Raixelol: 600G (5.5%, 600G)
lowlf: 548G (5.0%, 27,649G)
Jabuamba: 532G (4.9%, 532G)
AllInBot: 300G (2.8%, 300G)
old_overholt_: 300G (2.8%, 16,444G)
gingerfoo69: 100G (0.9%, 5,275G)

brown bets:
NicoSavoy: 2,000G (37.3%, 109,639G)
wiznaibusthis: 700G (13.0%, 1,668G)
iBardic: 500G (9.3%, 8,180G)
resjudicata3: 486G (9.1%, 486G)
hotpocketsofficial: 308G (5.7%, 308G)
0v3rr8d: 300G (5.6%, 12,256G)
BirbBrainsBot: 258G (4.8%, 51,243G)
datadrivenbot: 200G (3.7%, 63,449G)
getthemoneyz: 178G (3.3%, 1,972,096G)
PopsLIVE: 174G (3.2%, 174G)
SnarkMage: 162G (3.0%, 162G)
MemoriesofFinal: 100G (1.9%, 30,316G)

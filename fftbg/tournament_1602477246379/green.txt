Player: !Green
Team: Green Team
Palettes: Green/White



Evewho
Female
Capricorn
46
52
Chemist
Time Magic
Auto Potion
Secret Hunt
Jump+2

Panther Bag

Holy Miter
Adaman Vest
Cherche

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down
Float, Stabilize Time



Silentkaster
Male
Sagittarius
47
41
Mime

Mana Shield
Dual Wield
Jump+3



Twist Headband
Wizard Outfit
Feather Mantle

Mimic




XUnhingedMoose
Male
Aries
58
48
Mime

Counter
Defense UP
Levitate



Red Hood
Wizard Outfit
Jade Armlet

Mimic




Waterwatereverywhere
Female
Aries
68
74
Oracle
Elemental
Abandon
Sicken
Fly

Battle Folio

Flash Hat
Silk Robe
Sprint Shoes

Dispel Magic, Paralyze
Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball

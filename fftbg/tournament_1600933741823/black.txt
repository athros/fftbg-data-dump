Player: !Black
Team: Black Team
Palettes: Black/Red



Zachara
Male
Aries
59
75
Lancer
Talk Skill
Catch
Dual Wield
Jump+1

Dragon Whisker
Obelisk
Bronze Helmet
White Robe
Cursed Ring

Level Jump8, Vertical Jump6
Invitation, Preach, Insult, Mimic Daravon, Refute



Lydian C
Female
Leo
65
73
Calculator
Yin Yang Magic
Counter
Equip Polearm
Swim

Holy Lance

Triangle Hat
Linen Robe
Genji Gauntlet

CT, Prime Number, 4, 3
Spell Absorb, Life Drain, Zombie, Foxbird, Dispel Magic, Paralyze



HASTERIOUS
Male
Leo
44
56
Lancer
White Magic
Earplug
Equip Axe
Teleport

Battle Axe
Escutcheon
Crystal Helmet
White Robe
Germinas Boots

Level Jump5, Vertical Jump8
Cure, Cure 3, Protect 2, Shell 2, Esuna



Rozella
Female
Leo
71
39
Squire
Steal
Arrow Guard
Equip Sword
Lava Walking

Excalibur

Holy Miter
Diamond Armor
Germinas Boots

Throw Stone, Heal, Tickle, Fury, Scream
Steal Heart, Steal Helmet, Steal Armor, Steal Shield

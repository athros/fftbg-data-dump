Final Bets: black - 10 bets for 17,181G (81.3%, x0.23); brown - 7 bets for 3,946G (18.7%, x4.35)

black bets:
mindblownnnn: 5,698G (33.2%, 5,698G)
LivingHitokiri: 5,000G (29.1%, 87,452G)
Zachara: 1,250G (7.3%, 142,250G)
Lydian_C: 1,200G (7.0%, 4,324G)
Mushufasa_: 1,000G (5.8%, 100,458G)
NovaKnight21: 872G (5.1%, 872G)
randgridr: 858G (5.0%, 1,717G)
Raixelol: 600G (3.5%, 15,724G)
gilgamensche: 479G (2.8%, 479G)
Genkidou: 224G (1.3%, 1,818G)

brown bets:
BirbBrainsBot: 1,000G (25.3%, 99,311G)
getthemoneyz: 944G (23.9%, 2,048,177G)
otakutaylor: 790G (20.0%, 790G)
CorpusCav: 500G (12.7%, 18,057G)
resjudicata3: 412G (10.4%, 412G)
MemoriesofFinal: 200G (5.1%, 41,291G)
peeronid: 100G (2.5%, 2,169G)

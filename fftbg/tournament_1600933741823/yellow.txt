Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



3ngag3
Male
Gemini
74
47
Ninja
White Magic
Sunken State
Equip Polearm
Ignore Height

Partisan
Cypress Rod
Holy Miter
Wizard Outfit
Battle Boots

Shuriken, Bomb
Protect 2, Wall, Esuna



Morsigil
Female
Leo
71
73
Thief
Dance
Speed Save
Attack UP
Ignore Height

Ice Brand

Twist Headband
Judo Outfit
Feather Mantle

Gil Taking, Steal Weapon, Steal Status, Leg Aim
Witch Hunt, Disillusion, Last Dance



Mesmaster
Male
Libra
77
45
Mime

Mana Shield
Dual Wield
Waterwalking



Flash Hat
Mystic Vest
Power Wrist

Mimic




Jennie
Female
Cancer
55
50
Calculator
Yin Yang Magic
MA Save
Short Charge
Lava Walking

Madlemgen

Flash Hat
Brigandine
Rubber Shoes

CT, Height, Prime Number, 4, 3
Spell Absorb, Life Drain, Pray Faith, Blind Rage, Foxbird, Confusion Song, Sleep, Dark Holy

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DarrenDinosaurs
Female
Taurus
61
59
Knight
Throw
Auto Potion
Sicken
Jump+1

Blood Sword
Platinum Shield
Mythril Helmet
Genji Armor
Magic Ring

Armor Break, Shield Break, Justice Sword, Dark Sword
Shuriken, Bomb, Staff



FuzzyTigers
Female
Leo
43
42
Time Mage
Throw
Regenerator
Sicken
Move+1

Gold Staff

Twist Headband
Wizard Robe
Feather Boots

Haste 2, Slow, Slow 2, Immobilize, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Shuriken, Axe



OneHundredFists
Male
Aquarius
58
71
Bard
Summon Magic
Meatbone Slash
Dual Wield
Waterbreathing

Ramia Harp
Ramia Harp
Green Beret
Secret Clothes
Cherche

Magic Song, Last Song, Sky Demon
Moogle, Titan, Carbunkle, Cyclops



Deathmaker06
Male
Leo
61
80
Archer
White Magic
MA Save
Martial Arts
Swim

Hunting Bow
Crystal Shield
Holy Miter
Mystic Vest
Feather Mantle

Charge+1, Charge+4, Charge+7
Raise, Protect, Esuna

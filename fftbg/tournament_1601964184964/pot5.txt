Final Bets: red - 9 bets for 8,927G (43.6%, x1.29); yellow - 10 bets for 11,542G (56.4%, x0.77)

red bets:
Ruvelia_BibeI: 2,300G (25.8%, 2,300G)
resjudicata3: 1,874G (21.0%, 3,749G)
CorpusCav: 1,422G (15.9%, 65,416G)
BirbBrainsBot: 1,000G (11.2%, 100,543G)
lowlf: 664G (7.4%, 3,764G)
Laserman1000: 601G (6.7%, 12,501G)
NicoSavoy: 564G (6.3%, 564G)
QuestMoreLand: 412G (4.6%, 412G)
getthemoneyz: 90G (1.0%, 2,177,073G)

yellow bets:
Mushufasa_: 5,000G (43.3%, 8,889G)
DeathTaxesAndAnime: 1,997G (17.3%, 3,917G)
Rune339: 1,600G (13.9%, 1,600G)
AllInBot: 1,177G (10.2%, 1,177G)
FuzzyTigers: 635G (5.5%, 635G)
Chuckolator: 432G (3.7%, 432G)
Evewho: 200G (1.7%, 15,501G)
ValensEXP_: 200G (1.7%, 5,969G)
datadrivenbot: 200G (1.7%, 88,282G)
gorgewall: 101G (0.9%, 6,964G)

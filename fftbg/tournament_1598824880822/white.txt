Player: !White
Team: White Team
Palettes: White/Blue



Thunderducker
Monster
Gemini
64
56
Tiamat










Chuckolator
Female
Pisces
45
72
Squire
Jump
Counter Magic
Long Status
Ignore Terrain

Coral Sword
Venetian Shield
Cachusha
Power Sleeve
Spike Shoes

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Fury, Wish
Level Jump4, Vertical Jump8



Holandrix
Female
Pisces
47
70
Priest
Steal
Brave Save
Magic Defense UP
Lava Walking

Healing Staff

Headgear
White Robe
Wizard Mantle

Cure 3, Raise, Raise 2, Protect 2, Shell, Shell 2, Esuna, Magic Barrier
Gil Taking, Steal Heart, Steal Armor, Steal Status, Arm Aim



Krombobreaker
Male
Libra
56
50
Chemist
Steal
Distribute
Martial Arts
Move+3



Ribbon
Wizard Outfit
Bracer

Maiden's Kiss, Soft, Phoenix Down
Steal Helmet, Steal Shield, Steal Accessory, Arm Aim

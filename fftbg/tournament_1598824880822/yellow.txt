Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lord Burrah
Monster
Capricorn
36
74
Coeurl










Jeeboheebo
Monster
Capricorn
41
47
Sekhret










Smashy
Male
Aquarius
47
45
Priest
Draw Out
PA Save
Equip Shield
Ignore Height

Rainbow Staff
Genji Shield
Black Hood
Mythril Vest
Bracer

Cure, Cure 2, Raise, Raise 2, Protect, Protect 2, Wall
Koutetsu, Murasame, Heaven's Cloud



Reinoe
Male
Gemini
75
65
Monk
Time Magic
Regenerator
Dual Wield
Lava Walking



Headgear
Power Sleeve
Magic Gauntlet

Secret Fist, Purification, Chakra, Revive
Haste, Slow 2, Stop, Reflect, Demi, Stabilize Time

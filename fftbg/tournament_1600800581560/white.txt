Player: !White
Team: White Team
Palettes: White/Blue



J2DaBibbles
Male
Libra
71
48
Calculator
Yin Yang Magic
Dragon Spirit
Equip Knife
Jump+2

Spell Edge

Golden Hairpin
Silk Robe
Vanish Mantle

CT, 5, 3
Blind, Pray Faith, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy



Rune339
Female
Gemini
51
81
Archer
Basic Skill
Faith Save
Equip Polearm
Waterwalking

Iron Fan
Ice Shield
Black Hood
Mystic Vest
Elf Mantle

Charge+1, Charge+4, Charge+5
Accumulate, Dash, Heal, Wish



BuffaloCrunch
Male
Leo
79
35
Priest
Item
Dragon Spirit
Dual Wield
Ignore Terrain

Rainbow Staff
Wizard Staff
Flash Hat
Clothes
108 Gems

Raise, Raise 2, Esuna
Potion, Hi-Ether, Antidote, Echo Grass, Phoenix Down



Ruleof5
Female
Sagittarius
65
66
Archer
Draw Out
Abandon
Secret Hunt
Move+3

Silver Bow

Green Beret
Adaman Vest
Defense Ring

Charge+1, Charge+3, Charge+4, Charge+10
Asura, Koutetsu

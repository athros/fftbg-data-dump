Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SephDarkheart
Female
Virgo
68
49
Time Mage
Dance
Speed Save
Equip Bow
Fly

Windslash Bow

Triangle Hat
White Robe
108 Gems

Haste, Haste 2, Slow, Slow 2, Stop, Reflect, Quick, Stabilize Time, Meteor
Witch Hunt, Last Dance, Nether Demon



Tithonus
Female
Scorpio
77
70
Monk
Charge
Counter Magic
Equip Axe
Fly

Rainbow Staff

Feather Hat
Chain Vest
Magic Ring

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Charge+2, Charge+5



Dogsandcatsand
Female
Pisces
53
74
Wizard
Item
Counter Flood
Magic Attack UP
Waterbreathing

Poison Rod

Leather Hat
Judo Outfit
Feather Mantle

Fire 4, Ice, Ice 4, Empower, Flare
Potion, X-Potion, Holy Water, Remedy



Mindblownnnn
Female
Virgo
74
58
Priest
Talk Skill
Mana Shield
Equip Armor
Jump+1

Morning Star

Mythril Helmet
Adaman Vest
Elf Mantle

Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna
Persuade, Death Sentence, Insult, Negotiate, Mimic Daravon

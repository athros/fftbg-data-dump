Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Male
Libra
70
51
Mime

Counter Flood
Dual Wield
Move+3



Holy Miter
Leather Outfit
Defense Ring

Mimic




ALY327
Female
Taurus
66
66
Mediator
Time Magic
Distribute
Dual Wield
Move-MP Up

Blast Gun
Glacier Gun
Thief Hat
Mystic Vest
Spike Shoes

Invitation, Threaten, Preach, Death Sentence, Insult, Negotiate, Refute
Haste, Haste 2, Slow 2, Immobilize, Float, Reflect



ZCKaiser
Male
Sagittarius
64
42
Archer
Talk Skill
Speed Save
Equip Polearm
Jump+2

Obelisk
Platinum Shield
Crystal Helmet
Brigandine
Cursed Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Persuade, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon



Ogema TheDream
Female
Aquarius
40
80
Time Mage
White Magic
PA Save
Beastmaster
Jump+3

Iron Fan

Holy Miter
Mystic Vest
Wizard Mantle

Haste, Slow, Stabilize Time
Cure, Raise, Reraise, Shell, Holy

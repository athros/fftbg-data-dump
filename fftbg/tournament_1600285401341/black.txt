Player: !Black
Team: Black Team
Palettes: Black/Red



Resjudicata3
Female
Virgo
57
60
Summoner
Punch Art
Absorb Used MP
Equip Axe
Levitate

Healing Staff

Red Hood
Mystic Vest
Feather Mantle

Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Salamander, Silf, Fairy, Cyclops
Wave Fist, Secret Fist, Purification, Chakra, Revive



Roofiepops
Female
Virgo
81
58
Calculator
Yin Yang Magic
Absorb Used MP
Equip Shield
Jump+3

Papyrus Codex
Escutcheon
Golden Hairpin
Leather Outfit
Magic Gauntlet

CT, Prime Number, 5, 3
Doubt Faith, Petrify



FriendSquirrel
Female
Gemini
65
72
Dancer
White Magic
PA Save
Maintenance
Lava Walking

Ryozan Silk

Red Hood
Silk Robe
Cursed Ring

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Void Storage
Cure, Raise, Raise 2, Regen, Shell, Wall, Esuna



Willjin
Male
Gemini
69
79
Bard
Draw Out
Speed Save
Equip Sword
Swim

Heaven's Cloud

Holy Miter
Brigandine
Cursed Ring

Angel Song, Cheer Song, Magic Song, Diamond Blade, Hydra Pit
Muramasa

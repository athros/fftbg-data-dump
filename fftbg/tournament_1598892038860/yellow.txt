Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



J2DaBibbles
Male
Aquarius
44
39
Archer
Black Magic
Counter Flood
Dual Wield
Levitate

Hunting Bow
Poison Bow
Flash Hat
Black Costume
Germinas Boots

Charge+1, Charge+2
Bolt, Empower



StealthModeLocke
Female
Sagittarius
64
76
Knight
Talk Skill
Caution
Defense UP
Levitate

Slasher
Escutcheon
Crystal Helmet
Gold Armor
108 Gems

Head Break, Armor Break, Shield Break, Speed Break, Justice Sword
Persuade, Solution, Insult, Refute



Athros13
Male
Pisces
61
62
Time Mage
Steal
Auto Potion
Dual Wield
Move-MP Up

Cypress Rod
Ivory Rod
Golden Hairpin
Black Robe
Spike Shoes

Haste, Slow 2, Quick, Meteor
Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Pplvee1
Female
Cancer
66
71
Wizard
Basic Skill
HP Restore
Sicken
Levitate

Orichalcum

Cachusha
White Robe
Magic Gauntlet

Fire 4, Ice 3, Empower, Death
Heal, Yell, Cheer Up

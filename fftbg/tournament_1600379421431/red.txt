Player: !Red
Team: Red Team
Palettes: Red/Brown



Krombobreaker
Female
Cancer
46
79
Ninja
Black Magic
Counter Magic
Defense UP
Jump+3

Scorpion Tail
Morning Star
Flash Hat
Mystic Vest
Magic Ring

Bomb, Dictionary
Fire 2, Ice 2, Ice 4



Lythe Caraker
Female
Leo
60
45
Chemist
Black Magic
Counter Flood
Equip Axe
Waterbreathing

Flame Whip

Cachusha
Mythril Vest
Jade Armlet

Potion, Hi-Ether, Holy Water, Phoenix Down
Fire 2, Ice, Ice 3, Empower, Flare



3ngag3
Male
Capricorn
77
61
Geomancer
Black Magic
Critical Quick
Sicken
Swim

Kiyomori
Flame Shield
Cachusha
Power Sleeve
108 Gems

Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind
Fire, Fire 3, Bolt 2, Bolt 4, Ice 3, Ice 4, Empower, Frog



Reinoe
Female
Scorpio
80
77
Mime

Brave Save
Maintenance
Waterwalking



Circlet
Judo Outfit
Power Wrist

Mimic


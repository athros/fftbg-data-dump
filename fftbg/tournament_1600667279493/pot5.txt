Final Bets: blue - 9 bets for 14,607G (69.2%, x0.45); green - 9 bets for 6,510G (30.8%, x2.24)

blue bets:
nifboy: 11,295G (77.3%, 22,591G)
getthemoneyz: 1,000G (6.8%, 2,025,190G)
gorgewall: 700G (4.8%, 700G)
Shadowkept: 500G (3.4%, 3,896G)
silentkaster: 412G (2.8%, 412G)
datadrivenbot: 200G (1.4%, 71,960G)
Rune339: 200G (1.4%, 6,500G)
adellraynes: 200G (1.4%, 829G)
kilrazin: 100G (0.7%, 100G)

green bets:
reinoe: 2,000G (30.7%, 20,329G)
BirbBrainsBot: 1,000G (15.4%, 185,235G)
ValensEXP_: 1,000G (15.4%, 3,899G)
Freezerman47: 800G (12.3%, 1,853G)
superdevon1: 694G (10.7%, 2,778G)
lowlf: 568G (8.7%, 17,922G)
AllInBot: 200G (3.1%, 200G)
GenZealot: 148G (2.3%, 148G)
MemoriesofFinal: 100G (1.5%, 30,810G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rune339
Male
Sagittarius
52
60
Thief
Draw Out
Arrow Guard
Magic Defense UP
Fly

Short Edge

Thief Hat
Earth Clothes
Elf Mantle

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Heaven's Cloud, Kiyomori



Reddwind
Male
Aries
65
66
Knight
Talk Skill
Parry
Equip Axe
Lava Walking

Flail
Crystal Shield
Cross Helmet
Mythril Armor
Angel Ring

Armor Break, Shield Break, Mind Break
Invitation, Persuade, Insult



Ruleof5
Female
Capricorn
52
50
Archer
Throw
PA Save
Short Status
Move+3

Ice Bow

Leather Hat
Secret Clothes
Red Shoes

Charge+1, Charge+4, Charge+7
Shuriken, Knife



Mindblownnnn
Female
Taurus
75
67
Archer
Elemental
Counter Flood
Secret Hunt
Ignore Terrain

Blaze Gun
Bronze Shield
Diamond Helmet
Brigandine
Reflect Ring

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

Final Bets: blue - 19 bets for 11,080G (51.1%, x0.96); champion - 20 bets for 10,584G (48.9%, x1.05)

blue bets:
E_Ballard: 2,892G (26.1%, 2,892G)
maakur_: 1,644G (14.8%, 1,644G)
Arcblazer23: 1,000G (9.0%, 3,108G)
Aaron2Nerdy: 510G (4.6%, 1,510G)
Demon_Lord_Josh: 500G (4.5%, 3,983G)
ExplanationMarkWow: 500G (4.5%, 23,560G)
krombobreaker: 500G (4.5%, 28,821G)
CorpusCav: 500G (4.5%, 32,652G)
Lythe_Caraker: 500G (4.5%, 6,178G)
ValensEXP_: 444G (4.0%, 9,559G)
Chuckolator: 400G (3.6%, 400G)
Forkmore: 356G (3.2%, 356G)
LuckyLuckLuc2: 333G (3.0%, 6,750G)
DirkC_Fenrir84: 300G (2.7%, 1,281G)
XerxesMindbreaker: 200G (1.8%, 1,509G)
datadrivenbot: 200G (1.8%, 80,674G)
gorgewall: 101G (0.9%, 7,902G)
Lyner87: 100G (0.9%, 2,659G)
DHaveWord: 100G (0.9%, 4,435G)

champion bets:
SeniorBunk: 1,060G (10.0%, 1,060G)
BirbBrainsBot: 1,000G (9.4%, 192,885G)
acid_flashback: 1,000G (9.4%, 7,571G)
old_overholt_: 1,000G (9.4%, 60,991G)
Sairentozon7: 1,000G (9.4%, 53,953G)
Nizaha: 656G (6.2%, 8,210G)
lowlf: 640G (6.0%, 640G)
Evewho: 500G (4.7%, 6,551G)
AltimaMantoid: 500G (4.7%, 4,000G)
EvilLego6: 500G (4.7%, 10,404G)
SephDarkheart: 464G (4.4%, 464G)
pplvee1: 456G (4.3%, 456G)
resjudicata3: 400G (3.8%, 2,397G)
TheBrett: 264G (2.5%, 1,037G)
2b_yorha_b: 252G (2.4%, 252G)
rkane398: 212G (2.0%, 212G)
AllInBot: 200G (1.9%, 200G)
KGenjy: 200G (1.9%, 3,030G)
getthemoneyz: 180G (1.7%, 2,132,564G)
NeoXianWu_: 100G (0.9%, 100G)

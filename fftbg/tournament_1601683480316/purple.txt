Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thunderducker
Male
Sagittarius
49
64
Ninja
Punch Art
MA Save
Equip Polearm
Move+1

Persia
Musk Rod
Leather Hat
Secret Clothes
Power Wrist

Shuriken, Bomb, Staff, Dictionary
Spin Fist, Secret Fist, Purification



LuckyLuckLuc2
Female
Leo
50
62
Lancer
Talk Skill
Parry
Equip Gun
Retreat

Bestiary
Round Shield
Cross Helmet
Bronze Armor
Vanish Mantle

Level Jump4, Vertical Jump8
Threaten, Negotiate, Mimic Daravon, Refute, Rehabilitate



TheBrett
Male
Aries
50
73
Time Mage
Battle Skill
Damage Split
Short Status
Lava Walking

Gold Staff

Leather Hat
Judo Outfit
Red Shoes

Demi 2, Stabilize Time
Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Dark Sword



Chuckolator
Female
Virgo
73
77
Calculator
Yin Yang Magic
Distribute
Secret Hunt
Teleport 2

Bestiary

Green Beret
Light Robe
Sprint Shoes

CT, Height, Prime Number, 5, 4
Poison, Pray Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify

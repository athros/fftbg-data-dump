Player: !White
Team: White Team
Palettes: White/Blue



Lowlf
Male
Pisces
51
49
Knight
Animal Skill
Counter Flood
Equip Polearm
Jump+3

Gungnir
Diamond Shield
Barbuta
Crystal Mail
N-Kai Armlet

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



NovaKnight21
Female
Gemini
87
91
Oracle
Lucavi Skill
Absorb Used MP
Concentrate
Lava Walking

Gokuu Rod

Leather Hat
Mythril Vest
Magic Gauntlet

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Fenaen
Female
Aquarius
85
77
Ninja
Bio
Dragon Spirit
Equip Shield
Move+3

Ninja Edge
Platinum Shield
Triangle Hat
Wizard Outfit
Feather Boots

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



ValensEXP
Female
Cancer
77
43
Archer
Mighty Sword
Earplug
Equip Knife
Teleport

Poison Rod
Venetian Shield
Black Hood
Mystic Vest
Spike Shoes

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite

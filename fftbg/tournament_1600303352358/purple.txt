Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Jabuamba
Male
Libra
52
37
Summoner
Battle Skill
Dragon Spirit
Sicken
Waterbreathing

Poison Rod

Leather Hat
Mystic Vest
Defense Ring

Shiva, Ramuh, Titan, Golem, Carbunkle
Head Break, Shield Break, Magic Break, Power Break, Mind Break, Night Sword



OneHundredFists
Female
Leo
68
65
Calculator
Black Magic
Mana Shield
Equip Bow
Levitate

Mythril Bow

Thief Hat
Judo Outfit
Battle Boots

CT, Height, Prime Number, 5, 4, 3
Fire 3, Fire 4, Bolt, Bolt 4, Ice, Ice 2



Zenlion
Male
Pisces
53
46
Bard
Time Magic
Counter Magic
Equip Armor
Waterbreathing

Long Bow

Platinum Helmet
Chain Mail
Reflect Ring

Angel Song, Cheer Song, Battle Song, Nameless Song, Diamond Blade, Sky Demon
Reflect, Quick, Demi, Demi 2



Zagorsek
Female
Capricorn
42
43
Monk
Elemental
Auto Potion
Halve MP
Move-MP Up



Black Hood
Earth Clothes
108 Gems

Spin Fist, Pummel, Earth Slash, Secret Fist, Chakra, Revive
Pitfall, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

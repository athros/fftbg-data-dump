Player: !White
Team: White Team
Palettes: White/Blue



Raixelol
Female
Cancer
62
38
Mime

Counter
Martial Arts
Move-HP Up



Triangle Hat
Judo Outfit
Feather Boots

Mimic




Willjin
Female
Sagittarius
59
72
Calculator
Time Magic
Counter Flood
Magic Defense UP
Jump+3

Bestiary

Red Hood
Leather Outfit
Feather Boots

CT, Prime Number, 4, 3
Haste, Haste 2, Slow 2, Stop, Stabilize Time, Meteor



Maakur
Male
Aries
57
80
Geomancer
Yin Yang Magic
Damage Split
Long Status
Move+2

Battle Axe
Aegis Shield
Barette
Brigandine
Genji Gauntlet

Water Ball, Hallowed Ground, Local Quake, Gusty Wind, Lava Ball
Zombie, Dispel Magic



Dogsandcatsand
Female
Pisces
46
80
Wizard
Dance
Damage Split
Halve MP
Teleport

Rod

Black Hood
Light Robe
Magic Gauntlet

Fire, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3, Empower
Disillusion, Nameless Dance

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SaintOmerville
Male
Aquarius
68
71
Squire
Elemental
Abandon
Equip Sword
Retreat

Koutetsu Knife
Escutcheon
Black Hood
Leather Armor
108 Gems

Heal, Tickle, Yell, Fury, Scream
Pitfall, Water Ball, Hell Ivy, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Lythe Caraker
Female
Pisces
56
55
Time Mage
Draw Out
Counter Magic
Equip Bow
Move-HP Up

Poison Bow

Headgear
Silk Robe
Feather Mantle

Haste, Slow, Reflect, Demi 2, Stabilize Time
Murasame, Muramasa, Kikuichimoji



0v3rr8d
Male
Aries
43
70
Thief
Item
MP Restore
Equip Bow
Move-HP Up

Windslash Bow

Twist Headband
Mythril Vest
Bracer

Steal Heart, Steal Weapon, Arm Aim, Leg Aim
Potion, Ether, Antidote, Soft, Phoenix Down



SeniorBunk
Female
Capricorn
63
61
Summoner
Dance
Speed Save
Attack UP
Lava Walking

Dragon Rod

Leather Hat
Silk Robe
Bracer

Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Salamander, Silf, Cyclops
Wiznaibus, Polka Polka, Disillusion, Dragon Pit

Player: !White
Team: White Team
Palettes: White/Blue



Leonidusx
Male
Virgo
41
72
Ninja
Draw Out
Catch
Equip Gun
Waterwalking

Bloody Strings
Battle Folio
Cachusha
Clothes
Feather Mantle

Shuriken, Knife, Dictionary
Kikuichimoji



CorpusCav
Male
Aquarius
74
64
Samurai
Talk Skill
Arrow Guard
Concentrate
Fly

Holy Lance

Genji Helmet
Silk Robe
Magic Ring

Murasame, Kiyomori, Kikuichimoji
Preach, Insult, Negotiate, Refute



Powergems
Male
Leo
45
71
Squire
Draw Out
Abandon
Beastmaster
Lava Walking

Platinum Sword
Mythril Shield
Holy Miter
Genji Armor
Elf Mantle

Yell
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



Reinoe
Male
Virgo
47
50
Thief
Draw Out
MA Save
Dual Wield
Waterwalking

Air Knife
Air Knife
Golden Hairpin
Wizard Outfit
Leather Mantle

Gil Taking, Steal Weapon, Leg Aim
Asura, Koutetsu, Murasame, Heaven's Cloud

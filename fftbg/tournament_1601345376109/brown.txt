Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Khelor
Male
Virgo
44
48
Mime

Counter
Equip Shield
Jump+2


Diamond Shield
Thief Hat
Earth Clothes
Angel Ring

Mimic




Roofiepops
Female
Sagittarius
46
66
Mime

Arrow Guard
Equip Armor
Move-HP Up



Genji Helmet
Earth Clothes
Feather Mantle

Mimic




Daveb
Male
Virgo
60
46
Bard
Draw Out
Counter
Equip Polearm
Move+1

Spear

Holy Miter
Linen Cuirass
Magic Gauntlet

Magic Song, Sky Demon
Asura, Bizen Boat, Murasame



SephDarkheart
Female
Capricorn
77
39
Summoner
Black Magic
Caution
Maintenance
Teleport

Healing Staff

Barette
Mystic Vest
Wizard Mantle

Ramuh, Titan, Bahamut, Leviathan, Salamander, Fairy
Fire 2, Bolt, Death, Flare

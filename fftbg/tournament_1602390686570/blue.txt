Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DLJuggernaut
Monster
Virgo
56
53
Behemoth










HaplessOne
Male
Aries
62
64
Monk
Black Magic
Dragon Spirit
Magic Defense UP
Move+1



Thief Hat
Black Costume
Bracer

Spin Fist, Pummel, Purification, Revive
Fire 2, Bolt, Bolt 2, Bolt 4, Ice, Ice 2



Seppu777
Male
Aries
70
51
Samurai
Battle Skill
Catch
Doublehand
Fly

Bizen Boat

Gold Helmet
Plate Mail
N-Kai Armlet

Koutetsu, Bizen Boat, Kikuichimoji
Shield Break, Power Break



Lowlf
Female
Taurus
75
49
Oracle
Battle Skill
Caution
Halve MP
Jump+2

Battle Folio

Thief Hat
Black Robe
Feather Boots

Blind, Zombie, Blind Rage, Foxbird, Confusion Song, Paralyze, Sleep, Petrify
Shield Break

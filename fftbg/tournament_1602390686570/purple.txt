Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Deathmaker06
Male
Gemini
49
78
Monk
Throw
MP Restore
Magic Attack UP
Retreat



Feather Hat
Black Costume
Dracula Mantle

Wave Fist, Secret Fist, Purification, Chakra, Revive
Shuriken, Bomb



Lyonslegacy
Male
Sagittarius
76
75
Bard
Time Magic
Damage Split
Equip Knife
Teleport

Flame Rod

Golden Hairpin
Plate Mail
Leather Mantle

Angel Song, Life Song, Nameless Song, Last Song, Diamond Blade, Space Storage, Hydra Pit
Haste, Slow, Immobilize, Quick, Demi 2



StealthModeLocke
Male
Capricorn
50
65
Oracle
Elemental
Abandon
Equip Bow
Move+3

Ice Bow

Twist Headband
Linen Robe
Rubber Shoes

Blind, Poison, Doubt Faith, Dark Holy
Water Ball, Hell Ivy, Gusty Wind



ChiChiRodriguez
Male
Pisces
60
46
Ninja
Summon Magic
HP Restore
Equip Gun
Move+3

Papyrus Codex
Fairy Harp
Headgear
Mystic Vest
Germinas Boots

Shuriken
Shiva, Ifrit, Golem, Odin, Salamander, Silf, Fairy, Lich

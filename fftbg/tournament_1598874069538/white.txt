Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Virgo
69
71
Knight
Time Magic
MP Restore
Short Charge
Levitate

Giant Axe
Platinum Shield
Crystal Helmet
Bronze Armor
Wizard Mantle

Shield Break, Weapon Break, Speed Break, Mind Break
Haste, Slow, Slow 2, Immobilize, Float, Reflect, Stabilize Time, Meteor



Lokenwow
Male
Scorpio
51
69
Lancer
Throw
Damage Split
Concentrate
Swim

Mythril Spear
Buckler
Bronze Helmet
Diamond Armor
Feather Boots

Level Jump2, Vertical Jump7
Shuriken, Knife



Rocl
Female
Cancer
71
64
Calculator
Bio
Arrow Guard
Maintenance
Move-HP Up

Madlemgen
Bronze Shield
Iron Helmet
Maximillian
Wizard Mantle

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



VolgraTheMoose
Male
Pisces
52
69
Knight
Item
Sunken State
Long Status
Lava Walking

Chaos Blade
Bronze Shield
Platinum Helmet
Gold Armor
Setiemson

Head Break, Weapon Break, Speed Break
Potion, Hi-Potion, Eye Drop, Holy Water

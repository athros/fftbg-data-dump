Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



3ngag3
Female
Scorpio
59
46
Geomancer
Time Magic
PA Save
Defense UP
Move-MP Up

Battle Axe
Gold Shield
Black Hood
Adaman Vest
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Haste, Stop, Stabilize Time



Mushufasa
Female
Sagittarius
79
70
Ninja
Steal
Counter
Equip Sword
Waterwalking

Ancient Sword
Flail
Red Hood
Clothes
Rubber Shoes

Shuriken, Bomb, Staff
Gil Taking, Steal Accessory, Steal Status, Leg Aim



Brokenknight201
Male
Capricorn
66
66
Monk
Talk Skill
Parry
Defend
Lava Walking



Twist Headband
Brigandine
Bracer

Spin Fist, Purification, Revive
Preach, Insult, Mimic Daravon



SeniorBunk
Female
Libra
67
61
Knight
Elemental
Auto Potion
Dual Wield
Jump+1

Long Sword
Ancient Sword
Mythril Helmet
Genji Armor
Feather Boots

Head Break, Speed Break, Mind Break, Justice Sword
Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm

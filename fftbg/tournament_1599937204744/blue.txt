Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Smashy
Female
Cancer
51
40
Time Mage
Punch Art
Damage Split
Short Charge
Retreat

Ivory Rod

Holy Miter
Chameleon Robe
Germinas Boots

Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi
Wave Fist, Earth Slash, Purification, Chakra



Brokenknight201
Male
Sagittarius
46
56
Ninja
Talk Skill
Faith Save
Equip Sword
Waterbreathing

Sleep Sword
Mythril Sword
Thief Hat
Earth Clothes
Battle Boots

Wand
Mimic Daravon, Refute



VolgraTheMoose
Male
Pisces
44
60
Mediator
Time Magic
Hamedo
Equip Bow
Jump+2

Hunting Bow

Feather Hat
Silk Robe
Sprint Shoes

Invitation, Preach, Negotiate
Haste, Slow 2, Stabilize Time, Meteor



Chuckolator
Female
Pisces
59
61
Calculator
Nether Skill
Caution
Defend
Move+1

Ice Rod
Round Shield
Red Hood
Earth Clothes
Genji Gauntlet

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper

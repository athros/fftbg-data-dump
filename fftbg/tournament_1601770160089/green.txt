Player: !Green
Team: Green Team
Palettes: Green/White



E Ballard
Female
Libra
59
71
Ninja
Black Magic
Caution
Halve MP
Jump+1

Sasuke Knife
Main Gauche
Feather Hat
Leather Outfit
Defense Ring

Shuriken
Bolt, Ice, Frog, Death



Phi Sig
Male
Libra
76
74
Squire
Elemental
MA Save
Short Charge
Retreat

Long Sword
Bronze Shield
Headgear
Chain Vest
Red Shoes

Heal, Tickle, Yell, Cheer Up
Pitfall, Hell Ivy, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball



Reddash9
Male
Aries
61
51
Lancer
Elemental
Dragon Spirit
Maintenance
Fly

Spear
Aegis Shield
Circlet
Chain Mail
Defense Ring

Level Jump3, Vertical Jump6
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



ANFz
Male
Taurus
77
72
Geomancer
Charge
Critical Quick
Magic Attack UP
Move-HP Up

Kiyomori
Genji Shield
Headgear
Wizard Robe
108 Gems

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Charge+4, Charge+5

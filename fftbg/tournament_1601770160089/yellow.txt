Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Deathmaker06
Female
Capricorn
74
42
Knight
Time Magic
Counter Magic
Sicken
Waterbreathing

Ragnarok
Platinum Shield
Crystal Helmet
Chameleon Robe
Feather Boots

Armor Break, Weapon Break, Stasis Sword, Night Sword, Surging Sword
Haste 2, Slow 2, Float, Demi 2, Stabilize Time, Meteor



DeathTaxesAndAnime
Female
Virgo
76
59
Thief
Item
Catch
Throw Item
Move-HP Up

Ninja Edge

Triangle Hat
Mythril Vest
Rubber Shoes

Gil Taking, Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Hi-Potion, X-Potion, Echo Grass, Phoenix Down



Lord Gwarth
Male
Aries
69
49
Ninja
Steal
Counter Tackle
Equip Gun
Waterwalking

Fairy Harp
Ramia Harp
Twist Headband
Clothes
Cursed Ring

Shuriken, Knife
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Arm Aim



VolgraTheMoose
Male
Scorpio
66
54
Ninja
Talk Skill
Meatbone Slash
Equip Gun
Move-HP Up

Romanda Gun
Mythril Gun
Thief Hat
Clothes
Vanish Mantle

Shuriken, Wand
Invitation, Praise, Threaten, Insult, Refute

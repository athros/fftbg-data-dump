Final Bets: blue - 9 bets for 5,870G (41.6%, x1.40); brown - 7 bets for 8,236G (58.4%, x0.71)

blue bets:
DavenIII: 3,000G (51.1%, 19,749G)
BirbBrainsBot: 1,000G (17.0%, 68,113G)
getthemoneyz: 760G (12.9%, 2,162,711G)
Zachara: 300G (5.1%, 117,600G)
Blaster_Bomb: 210G (3.6%, 1,731G)
AllInBot: 200G (3.4%, 200G)
datadrivenbot: 200G (3.4%, 79,987G)
maakur_: 100G (1.7%, 3,055G)
DouglasDragonThePoet: 100G (1.7%, 7,426G)

brown bets:
almost__sane: 4,000G (48.6%, 24,087G)
Ruvelia_BibeI: 1,000G (12.1%, 11,707G)
Aaron2Nerdy: 927G (11.3%, 927G)
Sharosa: 857G (10.4%, 857G)
Mushufasa_: 500G (6.1%, 8,472G)
ColetteMSLP: 496G (6.0%, 13,729G)
DustBirdEX: 456G (5.5%, 3,888G)

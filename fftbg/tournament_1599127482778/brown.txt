Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HentaiWriter
Male
Libra
52
57
Monk
Elemental
Hamedo
Equip Knife
Move+3

Mythril Knife

Thief Hat
Leather Outfit
Magic Ring

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



VolgraTheMoose
Male
Sagittarius
56
70
Mediator
Summon Magic
MA Save
Dual Wield
Lava Walking

Madlemgen
Papyrus Codex
Cachusha
Clothes
Magic Ring

Threaten, Solution, Death Sentence, Insult, Negotiate, Refute
Ifrit, Golem, Fairy



Lijarkh
Male
Capricorn
61
64
Monk
Sing
Regenerator
Attack UP
Waterwalking

Hydra Bag

Black Hood
Brigandine
108 Gems

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Magic Song, Hydra Pit



Vultuous
Male
Aries
78
71
Lancer
Yin Yang Magic
Speed Save
Long Status
Fly

Dragon Whisker
Platinum Shield
Bronze Helmet
Bronze Armor
Battle Boots

Level Jump5, Vertical Jump8
Blind, Spell Absorb, Blind Rage, Dispel Magic, Dark Holy

Player: !Green
Team: Green Team
Palettes: Green/White



Rocl
Male
Aries
58
78
Priest
Jump
Abandon
Short Status
Move+2

Gold Staff

Golden Hairpin
Wizard Outfit
N-Kai Armlet

Cure 2, Raise, Regen, Protect, Protect 2, Esuna
Level Jump5, Vertical Jump5



Zenlion
Male
Scorpio
53
42
Time Mage
Throw
Parry
Magic Defense UP
Ignore Height

Gold Staff

Flash Hat
White Robe
Elf Mantle

Slow 2, Immobilize, Galaxy Stop
Knife, Dictionary



Laserman1000
Male
Taurus
60
69
Geomancer
Throw
Parry
Dual Wield
Move+3

Battle Axe
Giant Axe
Red Hood
Chameleon Robe
108 Gems

Pitfall, Static Shock, Will-O-Wisp, Blizzard
Shuriken, Bomb, Knife, Staff, Ninja Sword, Dictionary



Morsigil
Male
Pisces
71
47
Time Mage
Battle Skill
Counter
Doublehand
Move+2

Musk Rod

Holy Miter
Judo Outfit
Battle Boots

Haste, Haste 2, Slow, Slow 2, Float, Reflect, Demi, Stabilize Time, Meteor
Armor Break, Weapon Break, Magic Break, Night Sword

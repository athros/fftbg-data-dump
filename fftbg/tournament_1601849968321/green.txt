Player: !Green
Team: Green Team
Palettes: Green/White



ArchKnightX
Male
Libra
61
49
Mime

HP Restore
Martial Arts
Teleport



Feather Hat
Clothes
108 Gems

Mimic




HorusTaurus
Male
Aries
49
52
Mime

Counter
Defense UP
Lava Walking



Holy Miter
Judo Outfit
Power Wrist

Mimic




Powergems
Male
Sagittarius
53
49
Mediator
Steal
Counter
Equip Gun
Move-HP Up

Fairy Harp

Holy Miter
Silk Robe
Wizard Mantle

Praise, Mimic Daravon
Steal Helmet, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



TheBrett
Female
Leo
67
38
Mediator
Punch Art
HP Restore
Equip Sword
Fly

Save the Queen

Feather Hat
White Robe
Small Mantle

Praise, Insult, Mimic Daravon, Refute
Pummel, Purification, Chakra, Revive

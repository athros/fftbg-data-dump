Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dinin991
Female
Cancer
73
67
Archer
Battle Skill
Critical Quick
Sicken
Move-HP Up

Romanda Gun
Gold Shield
Cachusha
Wizard Outfit
Bracer

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Head Break, Mind Break, Stasis Sword, Explosion Sword



Lwtest
Male
Virgo
80
47
Bard
Steal
Mana Shield
Magic Defense UP
Jump+2

Bloody Strings

Green Beret
Bronze Armor
Spike Shoes

Cheer Song, Battle Song, Last Song, Sky Demon
Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim, Leg Aim



Leonidusx
Male
Aries
74
52
Knight
Elemental
Dragon Spirit
Doublehand
Move+3

Giant Axe

Bronze Helmet
Chameleon Robe
Cursed Ring

Head Break, Shield Break, Weapon Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind



ForagerCats
Male
Gemini
44
51
Priest
Basic Skill
Meatbone Slash
Secret Hunt
Ignore Height

Gold Staff

Black Hood
Robe of Lords
Small Mantle

Cure 3, Cure 4, Raise, Regen, Esuna, Holy
Accumulate, Throw Stone, Heal, Tickle, Yell

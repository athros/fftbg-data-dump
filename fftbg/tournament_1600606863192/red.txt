Player: !Red
Team: Red Team
Palettes: Red/Brown



Brokenknight201
Male
Leo
62
44
Ninja
Basic Skill
Counter
Equip Sword
Move+1

Orichalcum
Iron Sword
Holy Miter
Black Costume
Wizard Mantle

Shuriken, Ninja Sword
Accumulate, Heal, Yell, Fury



Nhammen
Female
Aquarius
64
55
Mediator
Yin Yang Magic
Parry
Dual Wield
Jump+3

Papyrus Codex
Papyrus Codex
Golden Hairpin
Wizard Robe
108 Gems

Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute
Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze



VolgraTheMoose
Male
Cancer
77
57
Lancer
Black Magic
Hamedo
Dual Wield
Lava Walking

Iron Fan
Ivory Rod
Gold Helmet
Carabini Mail
Sprint Shoes

Level Jump3, Vertical Jump7
Fire, Bolt 3, Ice 2, Empower, Death



Dogsandcatsand
Female
Virgo
56
67
Wizard
Draw Out
Regenerator
Defense UP
Jump+1

Wizard Rod

Flash Hat
Brigandine
Germinas Boots

Bolt 3, Ice 2, Ice 4, Frog
Asura, Koutetsu

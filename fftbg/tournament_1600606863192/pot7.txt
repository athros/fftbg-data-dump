Final Bets: yellow - 9 bets for 2,732G (56.2%, x0.78); black - 8 bets for 2,126G (43.8%, x1.29)

yellow bets:
loveyouallfriends: 1,000G (36.6%, 35,917G)
Afro_Gundam: 500G (18.3%, 1,000G)
bigbongsmoker: 332G (12.2%, 1,859G)
VolgraTheMoose: 250G (9.2%, 11,462G)
BirbBrainsBot: 237G (8.7%, 161,072G)
datadrivenbot: 200G (7.3%, 72,779G)
LuckyLuckLuc2: 111G (4.1%, 1,953G)
MemoriesofFinal: 100G (3.7%, 31,458G)
Lord_Gwarth: 2G (0.1%, 1,768G)

black bets:
Eoika: 542G (25.5%, 5,427G)
upvla: 500G (23.5%, 3,425G)
randgridr: 336G (15.8%, 5,000G)
king_smashington: 224G (10.5%, 224G)
AllInBot: 200G (9.4%, 200G)
gongonono: 200G (9.4%, 1,975G)
maakur_: 100G (4.7%, 1,507G)
getthemoneyz: 24G (1.1%, 2,014,439G)

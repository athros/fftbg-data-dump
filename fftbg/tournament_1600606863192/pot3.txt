Final Bets: white - 7 bets for 3,598G (54.8%, x0.82); black - 9 bets for 2,966G (45.2%, x1.21)

white bets:
Sairentozon7: 1,000G (27.8%, 27,086G)
AllInBot: 754G (21.0%, 754G)
BirbBrainsBot: 708G (19.7%, 162,597G)
skipsandwiches: 428G (11.9%, 428G)
getthemoneyz: 340G (9.4%, 2,014,990G)
king_smashington: 264G (7.3%, 1,056G)
genkidou: 104G (2.9%, 856G)

black bets:
Leonidusx: 1,000G (33.7%, 134,016G)
J2DaBibbles: 600G (20.2%, 600G)
randgridr: 336G (11.3%, 336G)
Chihuahua_Charity: 272G (9.2%, 272G)
VolgraTheMoose: 250G (8.4%, 11,659G)
datadrivenbot: 200G (6.7%, 72,711G)
Afro_Gundam: 108G (3.6%, 1,053G)
MemoriesofFinal: 100G (3.4%, 30,815G)
lwtest: 100G (3.4%, 4,080G)

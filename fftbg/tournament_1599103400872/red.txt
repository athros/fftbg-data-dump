Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Male
Scorpio
74
71
Priest
Punch Art
Damage Split
Equip Gun
Waterwalking

Bestiary

Flash Hat
Judo Outfit
Chantage

Cure, Raise, Regen, Protect 2, Shell, Wall, Magic Barrier
Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Phi Sig
Male
Aquarius
62
64
Calculator
Black Magic
Sunken State
Defend
Ignore Height

Bestiary

Feather Hat
Silk Robe
Rubber Shoes

CT, Height, 4, 3
Fire, Bolt 3



Lythe Caraker
Monster
Capricorn
62
56
Mindflayer










Eweaselbeth
Male
Aquarius
48
44
Squire
Charge
Counter Magic
Maintenance
Ignore Height

Snipe Bow
Platinum Shield
Bronze Helmet
Adaman Vest
Small Mantle

Throw Stone, Heal, Yell, Cheer Up, Wish
Charge+2, Charge+5, Charge+20

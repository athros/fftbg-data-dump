Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



ThanatosXRagnarok
Female
Libra
64
46
Chemist
Draw Out
Brave Save
Concentrate
Teleport

Romanda Gun

Headgear
Judo Outfit
Jade Armlet

Potion, X-Potion, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Asura, Heaven's Cloud



Forkmore
Male
Virgo
72
56
Samurai
Summon Magic
Counter
Attack UP
Ignore Height

Kiyomori

Bronze Helmet
Bronze Armor
Magic Gauntlet

Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Moogle, Ramuh, Leviathan, Silf, Fairy, Zodiac



Reddwind
Male
Sagittarius
53
38
Knight
Jump
Counter Tackle
Dual Wield
Fly

Ragnarok
Slasher
Mythril Helmet
Gold Armor
108 Gems

Armor Break, Shield Break, Magic Break, Justice Sword, Night Sword
Level Jump8, Vertical Jump2



RonaldoTheGypsy
Male
Cancer
56
51
Bard
Black Magic
Counter Magic
Sicken
Move+1

Ramia Harp

Red Hood
Chain Vest
Defense Ring

Battle Song, Magic Song
Fire 3, Bolt, Bolt 2, Empower, Death

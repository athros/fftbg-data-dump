Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Resjudicata3
Female
Aquarius
59
67
Priest
Throw
MA Save
Dual Wield
Move+2

Flail
Healing Staff
Green Beret
Mystic Vest
Power Wrist

Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Shell 2, Esuna
Bomb, Knife



0v3rr8d
Male
Capricorn
46
42
Archer
Item
Catch
Doublehand
Fly

Bow Gun

Holy Miter
Chain Vest
Angel Ring

Charge+7, Charge+20
Ether, Echo Grass, Maiden's Kiss, Phoenix Down



CorpusCav
Female
Aquarius
68
47
Knight
Elemental
Absorb Used MP
Long Status
Fly

Blood Sword
Platinum Shield
Genji Helmet
Leather Armor
Elf Mantle

Head Break, Shield Break, Stasis Sword, Surging Sword
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



DLJuggernaut
Male
Capricorn
75
75
Archer
Draw Out
Caution
Equip Gun
Move-HP Up

Madlemgen
Ice Shield
Leather Hat
Brigandine
Spike Shoes

Charge+1, Charge+3, Charge+4, Charge+7, Charge+10
Heaven's Cloud, Kikuichimoji, Masamune

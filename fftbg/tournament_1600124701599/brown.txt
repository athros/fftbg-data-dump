Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Saldarin
Male
Libra
75
58
Knight
Punch Art
Distribute
Short Charge
Teleport

Defender
Round Shield
Circlet
Leather Armor
Spike Shoes

Armor Break, Stasis Sword, Night Sword
Spin Fist, Pummel, Wave Fist, Purification, Revive



Go2sleepTV
Female
Virgo
77
77
Wizard
Steal
HP Restore
Equip Gun
Fly

Battle Folio

Twist Headband
Earth Clothes
Angel Ring

Fire 2, Fire 3, Bolt
Gil Taking, Steal Helmet, Steal Weapon, Steal Status, Arm Aim



Ayeayex3
Female
Virgo
48
66
Geomancer
Charge
Absorb Used MP
Martial Arts
Move-HP Up

Platinum Sword
Aegis Shield
Golden Hairpin
Chain Vest
Germinas Boots

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Charge+10



Old Overholt
Female
Scorpio
75
61
Priest
Draw Out
Parry
Short Charge
Waterbreathing

Rainbow Staff

Green Beret
Wizard Robe
Battle Boots

Cure 3, Cure 4, Raise, Raise 2, Protect, Shell 2, Esuna
Bizen Boat, Murasame, Muramasa

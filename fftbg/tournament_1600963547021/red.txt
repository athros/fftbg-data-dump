Player: !Red
Team: Red Team
Palettes: Red/Brown



Formerlydrdong
Female
Libra
62
40
Oracle
Steal
Counter Flood
Attack UP
Move-MP Up

Papyrus Codex

Flash Hat
Power Sleeve
Magic Ring

Blind, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Steal Status



KinniQuacks
Male
Leo
75
62
Chemist
Black Magic
Meatbone Slash
Concentrate
Retreat

Glacier Gun

Red Hood
Leather Outfit
Diamond Armlet

Potion, X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down
Fire 3, Bolt 2, Bolt 4, Ice 4, Flare



AbandonedHall
Male
Aries
55
61
Bard
Jump
Earplug
Maintenance
Jump+3

Bloody Strings

Red Hood
Brigandine
Elf Mantle

Life Song, Cheer Song, Battle Song, Nameless Song, Diamond Blade
Level Jump3, Vertical Jump4



Foamsoldier
Male
Taurus
58
52
Lancer
Punch Art
Dragon Spirit
Equip Polearm
Waterbreathing

Persia
Gold Shield
Iron Helmet
Light Robe
Wizard Mantle

Level Jump8, Vertical Jump7
Earth Slash, Secret Fist, Purification, Revive

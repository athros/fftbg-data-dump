Final Bets: blue - 10 bets for 19,714G (78.3%, x0.28); yellow - 10 bets for 5,449G (21.7%, x3.62)

blue bets:
Willjin: 10,000G (50.7%, 148,546G)
VolgraTheMoose: 2,001G (10.2%, 108,360G)
Shalloween: 2,000G (10.1%, 49,362G)
Zerguzen: 2,000G (10.1%, 2,477G)
reinoe: 2,000G (10.1%, 59,657G)
CorpusCav: 500G (2.5%, 4,822G)
Smashy: 500G (2.5%, 4,723G)
douchetron: 313G (1.6%, 313G)
krombobreaker: 200G (1.0%, 1,994G)
datadrivenbot: 200G (1.0%, 62,314G)

yellow bets:
xendriick: 2,000G (36.7%, 9,548G)
BirbBrainsBot: 1,000G (18.4%, 156,772G)
E_Ballard: 812G (14.9%, 93,893G)
highelectricaltemperature: 400G (7.3%, 2,190G)
Leonidusx: 300G (5.5%, 15,610G)
HS39: 267G (4.9%, 267G)
getthemoneyz: 242G (4.4%, 1,782,722G)
HentaiWriter: 220G (4.0%, 220G)
Sir_Jim_0f_Slim: 108G (2.0%, 108G)
BartTradingCompany: 100G (1.8%, 1,331G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Widewalk
Monster
Pisces
58
68
Dark Behemoth










Forkmore
Male
Gemini
67
58
Knight
Basic Skill
MA Save
Attack UP
Ignore Height

Coral Sword
Platinum Shield
Platinum Helmet
Light Robe
Small Mantle

Shield Break, Magic Break, Speed Break, Power Break, Justice Sword
Dash, Throw Stone, Cheer Up, Fury, Wish



Nhammen
Female
Libra
44
58
Samurai
Punch Art
Parry
Dual Wield
Levitate

Obelisk
Holy Lance
Gold Helmet
Genji Armor
Vanish Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji, Masamune
Pummel, Secret Fist, Purification, Chakra



Lowlf
Female
Aquarius
58
49
Oracle
Elemental
Damage Split
Halve MP
Jump+2

Gokuu Rod

Twist Headband
Chameleon Robe
Spike Shoes

Blind, Spell Absorb, Zombie, Blind Rage, Foxbird, Dispel Magic, Petrify
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lythe Caraker
Male
Capricorn
62
75
Chemist
Punch Art
Blade Grasp
Maintenance
Move+3

Hydra Bag

Red Hood
Earth Clothes
N-Kai Armlet

Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Spin Fist, Purification, Revive, Seal Evil



GenZealot
Female
Leo
76
79
Archer
Throw
Counter Tackle
Dual Wield
Move+2

Stone Gun
Glacier Gun
Bronze Helmet
Mythril Vest
Small Mantle

Charge+2, Charge+3, Charge+4
Sword, Ninja Sword



Firesheath
Male
Serpentarius
45
51
Bard
Item
Speed Save
Doublehand
Move+1

Long Bow

Black Hood
Mythril Armor
Sprint Shoes

Cheer Song, Space Storage, Hydra Pit
Potion, Hi-Potion, Holy Water, Phoenix Down



Withpaint
Male
Cancer
80
42
Archer
Basic Skill
Damage Split
Defend
Retreat

Long Bow

Barbuta
Judo Outfit
Power Wrist

Charge+2, Charge+3, Charge+20
Throw Stone, Heal, Wish

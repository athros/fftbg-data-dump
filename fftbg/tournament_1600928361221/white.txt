Player: !White
Team: White Team
Palettes: White/Blue



Firesheath
Female
Pisces
68
73
Wizard
Basic Skill
HP Restore
Equip Shield
Jump+3

Dagger
Genji Shield
Triangle Hat
Chain Vest
Genji Gauntlet

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower
Accumulate, Throw Stone, Heal, Yell, Cheer Up



Gorgewall
Male
Libra
51
54
Calculator
Curse Skill
Blade Grasp
Beastmaster
Lava Walking

Papyrus Codex

Circlet
Black Costume
Rubber Shoes

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Gobizku
Male
Scorpio
49
54
Mime

Critical Quick
Magic Defense UP
Teleport



Thief Hat
Wizard Outfit
Chantage

Mimic




Laserman1000
Male
Sagittarius
48
75
Archer
Punch Art
Caution
Attack UP
Move+1

Windslash Bow

Black Hood
Earth Clothes
Bracer

Charge+1, Charge+4, Charge+5, Charge+7, Charge+10
Pummel, Wave Fist, Earth Slash, Purification

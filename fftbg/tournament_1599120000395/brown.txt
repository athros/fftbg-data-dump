Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fattunaking
Male
Aries
77
48
Monk
Time Magic
MA Save
Attack UP
Ignore Height



Feather Hat
Secret Clothes
Rubber Shoes

Secret Fist, Purification, Chakra
Haste, Stop, Quick



Thunderducker
Male
Sagittarius
68
46
Time Mage
Item
Counter Tackle
Equip Armor
Ignore Terrain

Cypress Rod

Mythril Helmet
Genji Armor
Sprint Shoes

Haste, Reflect, Demi, Demi 2
Potion, X-Potion, Hi-Ether, Remedy, Phoenix Down



NovaKnight21
Female
Pisces
75
56
Squire
Battle Skill
Counter
Equip Axe
Waterbreathing

Flail
Ice Shield
Headgear
Crystal Mail
Setiemson

Accumulate, Heal, Wish
Power Break, Mind Break



Go2sleepTV
Female
Scorpio
59
66
Geomancer
Charge
Blade Grasp
Magic Attack UP
Fly

Giant Axe
Round Shield
Holy Miter
Mystic Vest
Sprint Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1

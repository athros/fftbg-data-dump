Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lowlf
Male
Sagittarius
73
65
Summoner
Battle Skill
Damage Split
Equip Axe
Ignore Terrain

White Staff

Feather Hat
Light Robe
Magic Gauntlet

Moogle, Shiva, Ramuh, Titan, Odin, Leviathan, Salamander, Silf, Cyclops
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword



Reinoe
Male
Pisces
41
69
Lancer
Battle Skill
MA Save
Halve MP
Move-HP Up

Iron Fan
Round Shield
Genji Helmet
Crystal Mail
Sprint Shoes

Level Jump2, Vertical Jump2
Power Break, Night Sword



Roofiepops
Female
Capricorn
77
82
Mime

Counter Flood
Dual Wield
Move+2



Thief Hat
Brigandine
Defense Armlet

Mimic




FriendlySkeleton
Male
Leo
57
68
Lancer
Time Magic
Counter Flood
Dual Wield
Fly

Cypress Rod
Partisan
Leather Helmet
Platinum Armor
Small Mantle

Level Jump3, Vertical Jump8
Float, Quick, Demi

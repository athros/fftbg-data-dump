Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Reddwind
Male
Aries
55
80
Knight
Throw
Counter Flood
Magic Attack UP
Waterwalking

Ragnarok
Genji Shield
Bronze Helmet
Linen Robe
Defense Ring

Shield Break, Weapon Break, Speed Break, Power Break, Dark Sword
Shuriken, Bomb, Knife, Sword



NicoSavoy
Female
Aquarius
49
59
Monk
Steal
Earplug
Secret Hunt
Ignore Height



Holy Miter
Judo Outfit
Small Mantle

Wave Fist, Purification, Revive
Steal Heart, Steal Shield



MADSUPERVILLAIN
Female
Cancer
72
68
Ninja
Elemental
Dragon Spirit
Equip Axe
Levitate

Giant Axe
Wizard Staff
Feather Hat
Black Costume
Rubber Shoes

Shuriken, Bomb, Knife, Hammer
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



OneHundredFists
Female
Scorpio
53
46
Mediator
Charge
Abandon
Dual Wield
Move-MP Up

Battle Folio
Papyrus Codex
Golden Hairpin
Linen Robe
Jade Armlet

Invitation, Persuade, Praise, Preach, Solution, Death Sentence, Mimic Daravon
Charge+1, Charge+4, Charge+5

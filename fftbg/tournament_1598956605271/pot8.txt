Final Bets: red - 9 bets for 9,913G (57.0%, x0.75); champion - 7 bets for 7,477G (43.0%, x1.33)

red bets:
NicoSavoy: 2,926G (29.5%, 2,926G)
DouglasDragonThePoet: 1,364G (13.8%, 2,728G)
gorgewall: 1,127G (11.4%, 1,127G)
VolgraTheMoose: 1,001G (10.1%, 143,655G)
rocl: 1,000G (10.1%, 70,007G)
BirbBrainsBot: 1,000G (10.1%, 38,261G)
SephDarkheart: 753G (7.6%, 753G)
Leonidusx: 500G (5.0%, 13,127G)
getthemoneyz: 242G (2.4%, 1,806,621G)

champion bets:
bruubarg: 3,756G (50.2%, 26,756G)
pepperbuster: 1,500G (20.1%, 1,500G)
Zachara: 1,301G (17.4%, 141,801G)
MemoriesofFinal: 304G (4.1%, 304G)
DracoknightZero: 216G (2.9%, 216G)
FriendlySkeleton: 200G (2.7%, 982G)
datadrivenbot: 200G (2.7%, 66,377G)

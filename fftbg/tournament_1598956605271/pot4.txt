Final Bets: purple - 6 bets for 4,894G (67.4%, x0.48); brown - 7 bets for 2,362G (32.6%, x2.07)

purple bets:
DavenIII: 2,000G (40.9%, 4,035G)
BirbBrainsBot: 1,000G (20.4%, 36,641G)
Leonidusx: 1,000G (20.4%, 12,281G)
Raixelol: 352G (7.2%, 1,373G)
SephDarkheart: 304G (6.2%, 304G)
Zachara: 238G (4.9%, 142,238G)

brown bets:
gorgewall: 704G (29.8%, 704G)
getthemoneyz: 588G (24.9%, 1,806,042G)
MemoriesofFinal: 470G (19.9%, 470G)
FriendlySkeleton: 200G (8.5%, 1,286G)
datadrivenbot: 200G (8.5%, 66,548G)
DouglasDragonThePoet: 100G (4.2%, 2,143G)
brenogarwin: 100G (4.2%, 17,195G)

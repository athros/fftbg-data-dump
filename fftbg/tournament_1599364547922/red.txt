Player: !Red
Team: Red Team
Palettes: Red/Brown



Powermhero
Female
Virgo
64
70
Priest
Elemental
Earplug
Halve MP
Jump+1

White Staff

Triangle Hat
Leather Outfit
Elf Mantle

Cure, Raise, Raise 2, Protect, Esuna
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Gusty Wind, Lava Ball



Reddwind
Male
Virgo
49
61
Knight
Elemental
Arrow Guard
Attack UP
Jump+1

Mythril Sword
Aegis Shield
Iron Helmet
Genji Armor
Defense Armlet

Armor Break, Weapon Break, Mind Break
Water Ball, Hallowed Ground, Local Quake, Quicksand, Blizzard, Lava Ball



Thyrandaal
Male
Aries
61
76
Monk
Battle Skill
Distribute
Equip Bow
Waterbreathing

Hunting Bow

Golden Hairpin
Adaman Vest
Chantage

Earth Slash, Purification, Chakra
Magic Break



Nhammen
Female
Gemini
77
44
Thief
Draw Out
Parry
Dual Wield
Move+1

Cultist Dagger
Diamond Sword
Red Hood
Clothes
Bracer

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Leg Aim
Heaven's Cloud, Kiyomori, Kikuichimoji

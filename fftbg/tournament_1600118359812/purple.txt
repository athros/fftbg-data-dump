Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Brokenknight201
Male
Aquarius
70
56
Archer
Summon Magic
Distribute
Doublehand
Move-MP Up

Stone Gun

Triangle Hat
Wizard Outfit
Sprint Shoes

Charge+1, Charge+5, Charge+7
Moogle, Ramuh, Leviathan, Lich



SkylerBunny
Female
Leo
74
53
Chemist
Time Magic
Caution
Equip Sword
Ignore Terrain

Kiyomori

Red Hood
Brigandine
Setiemson

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Haste, Haste 2, Immobilize, Quick, Demi 2, Stabilize Time, Meteor



Gelwain
Female
Sagittarius
58
63
Ninja
Jump
Arrow Guard
Concentrate
Jump+1

Orichalcum
Sasuke Knife
Green Beret
Leather Outfit
Leather Mantle

Staff, Dictionary
Level Jump4, Vertical Jump5



Killth3kid
Male
Sagittarius
77
77
Knight
Elemental
Caution
Sicken
Move-MP Up

Slasher
Round Shield
Barbuta
Light Robe
Spike Shoes

Weapon Break, Speed Break, Power Break, Mind Break
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

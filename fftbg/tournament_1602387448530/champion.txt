Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Murderclan
Monster
Capricorn
62
54
Squidraken










YowaneHaku
Male
Cancer
54
38
Samurai
Punch Art
Caution
Short Status
Ignore Terrain

Muramasa

Circlet
Mythril Armor
Elf Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Masamune
Pummel, Revive



Seppu777
Female
Aquarius
53
47
Summoner
Elemental
Distribute
Short Charge
Ignore Height

Papyrus Codex

Holy Miter
Wizard Robe
Leather Mantle

Moogle, Cyclops
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball



Tripaplex
Female
Taurus
54
40
Thief
Punch Art
Counter Flood
Attack UP
Ignore Height

Assassin Dagger

Flash Hat
Adaman Vest
Small Mantle

Gil Taking, Arm Aim, Leg Aim
Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive

Final Bets: white - 13 bets for 7,788G (58.0%, x0.73); purple - 11 bets for 5,647G (42.0%, x1.38)

white bets:
YowaneHaku: 1,500G (19.3%, 9,945G)
BirbBrainsBot: 1,000G (12.8%, 72,548G)
reinoe: 1,000G (12.8%, 36,081G)
ihavenoideawhatname: 786G (10.1%, 786G)
E_Ballard: 700G (9.0%, 700G)
ValensEXP_: 512G (6.6%, 21,503G)
Breakdown777: 500G (6.4%, 2,716G)
Lord_Gwarth: 500G (6.4%, 1,667G)
HorusTaurus: 440G (5.6%, 440G)
Twisted_Nutsatchel: 300G (3.9%, 17,664G)
Zachara: 250G (3.2%, 3,250G)
datadrivenbot: 200G (2.6%, 85,179G)
Therealelmdor: 100G (1.3%, 2,776G)

purple bets:
Zagorsek: 1,768G (31.3%, 1,768G)
TheMurkGnome: 1,100G (19.5%, 1,100G)
HaplessOne: 888G (15.7%, 4,280G)
maakur_: 712G (12.6%, 712G)
Mushufasa_: 400G (7.1%, 35,038G)
AllInBot: 200G (3.5%, 200G)
FuzzyTigers: 200G (3.5%, 841G)
getthemoneyz: 178G (3.2%, 2,214,196G)
DouglasDragonThePoet: 100G (1.8%, 3,996G)
SRG_Chugganomics: 100G (1.8%, 3,872G)
king_smashington: 1G (0.0%, 1,228G)

Player: !White
Team: White Team
Palettes: White/Blue



Galkife
Male
Capricorn
75
81
Oracle
Jump
Mana Shield
Long Status
Teleport

Papyrus Codex

Headgear
Silk Robe
Magic Ring

Blind, Poison, Pray Faith, Zombie, Blind Rage, Paralyze, Sleep
Level Jump3, Vertical Jump8



ValensEXP
Male
Virgo
58
81
Squire
Battle Skill
Hamedo
Equip Bow
Teleport

Bow Gun
Bronze Shield
Black Hood
Diamond Armor
Angel Ring

Dash, Heal
Head Break, Armor Break, Weapon Break, Speed Break, Surging Sword



Daveb
Female
Pisces
73
44
Ninja
Draw Out
Damage Split
Beastmaster
Move-HP Up

Assassin Dagger
Assassin Dagger
Leather Hat
Mystic Vest
Power Wrist

Knife
Asura, Koutetsu, Heaven's Cloud



Mesmaster
Female
Pisces
68
56
Summoner
Throw
Damage Split
Equip Armor
Move+1

Dragon Rod

Circlet
Chameleon Robe
Genji Gauntlet

Shiva, Titan, Bahamut
Shuriken

Player: !Red
Team: Red Team
Palettes: Red/Brown



Technominari
Female
Aquarius
79
54
Priest
Talk Skill
HP Restore
Short Charge
Jump+2

Gold Staff

Headgear
Wizard Robe
Red Shoes

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Shell, Wall, Esuna, Holy
Persuade, Threaten, Negotiate, Rehabilitate



MemoriesofFinal
Female
Pisces
41
47
Thief
Summon Magic
Caution
Sicken
Jump+1

Long Sword

Feather Hat
Clothes
Defense Armlet

Gil Taking, Steal Armor, Steal Weapon, Steal Accessory
Moogle, Shiva, Titan, Silf, Fairy, Lich, Cyclops



Dogsandcatsand
Male
Virgo
70
50
Knight
Sing
Sunken State
Defend
Teleport 2

Mythril Sword
Platinum Shield
Circlet
Diamond Armor
Sprint Shoes

Shield Break, Weapon Break, Mind Break, Justice Sword
Cheer Song, Space Storage, Hydra Pit



SephDarkheart
Male
Leo
68
52
Knight
Punch Art
PA Save
Short Status
Swim

Ragnarok
Mythril Shield
Mythril Helmet
Gold Armor
Defense Ring

Armor Break, Weapon Break, Speed Break
Wave Fist, Earth Slash, Secret Fist, Purification, Revive

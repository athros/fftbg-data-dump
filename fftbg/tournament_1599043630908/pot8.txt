Final Bets: white - 5 bets for 3,326G (24.9%, x3.01); champion - 12 bets for 10,016G (75.1%, x0.33)

white bets:
BirbBrainsBot: 1,000G (30.1%, 64,847G)
iBardic: 1,000G (30.1%, 39,675G)
getthemoneyz: 802G (24.1%, 1,809,397G)
ColetteMSLP: 424G (12.7%, 424G)
SephDarkheart: 100G (3.0%, 10,577G)

champion bets:
bruubarg: 3,033G (30.3%, 18,033G)
Willjin: 3,000G (30.0%, 91,180G)
MemoriesofFinal: 1,000G (10.0%, 8,320G)
pepperbuster: 1,000G (10.0%, 2,500G)
CosmicTactician: 500G (5.0%, 48,913G)
Raixelol: 356G (3.6%, 1,204G)
krombobreaker: 264G (2.6%, 7,725G)
AltimaMantoid: 250G (2.5%, 6,000G)
datadrivenbot: 200G (2.0%, 66,513G)
ar_tactic: 200G (2.0%, 97,397G)
FriendlySkeleton: 112G (1.1%, 112G)
gorgewall: 101G (1.0%, 4,222G)

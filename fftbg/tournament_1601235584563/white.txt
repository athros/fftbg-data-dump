Player: !White
Team: White Team
Palettes: White/Blue



DarrenDinosaurs
Male
Gemini
68
72
Knight
Jump
Damage Split
Dual Wield
Move+3

Blood Sword
Diamond Sword
Circlet
Black Robe
Feather Mantle

Shield Break, Power Break, Mind Break, Dark Sword
Level Jump5, Vertical Jump2



Superdevon1
Male
Cancer
65
65
Chemist
Throw
Distribute
Equip Sword
Fly

Diamond Sword

Cachusha
Power Sleeve
Battle Boots

Potion, Hi-Potion, X-Potion, Ether, Antidote, Soft, Phoenix Down
Bomb



Cloudycube
Female
Leo
44
71
Thief
Basic Skill
MP Restore
Equip Armor
Move+1

Spell Edge

Headgear
Wizard Robe
Reflect Ring

Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Heal, Tickle, Cheer Up, Fury, Wish



SephDarkheart
Female
Aries
52
49
Dancer
Talk Skill
Counter
Monster Talk
Waterbreathing

Cute Bag

Thief Hat
Wizard Robe
Jade Armlet

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Disillusion, Dragon Pit
Invitation, Persuade, Threaten, Preach, Solution, Death Sentence, Refute

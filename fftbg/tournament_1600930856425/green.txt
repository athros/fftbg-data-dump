Player: !Green
Team: Green Team
Palettes: Green/White



Grasnikk
Monster
Taurus
55
60
Tiamat










3ngag3
Male
Scorpio
70
57
Geomancer
Time Magic
Catch
Secret Hunt
Ignore Terrain

Slasher
Mythril Shield
Holy Miter
Brigandine
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Lava Ball
Haste, Haste 2, Stop, Quick



Jabuamba
Monster
Leo
53
72
Behemoth










Lowlf
Male
Aries
40
49
Lancer
White Magic
Counter Flood
Halve MP
Ignore Height

Javelin
Gold Shield
Bronze Helmet
Platinum Armor
Cursed Ring

Level Jump8, Vertical Jump8
Cure 3, Raise, Reraise, Esuna, Holy, Magic Barrier

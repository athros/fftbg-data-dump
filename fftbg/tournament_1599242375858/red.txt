Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Male
Taurus
62
60
Archer
Draw Out
Absorb Used MP
Magic Defense UP
Jump+2

Snipe Bow
Round Shield
Feather Hat
Mystic Vest
Small Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Asura, Bizen Boat, Kiyomori



Douchetron
Female
Libra
75
57
Ninja
Steal
Speed Save
Long Status
Teleport

Flail
Kunai
Black Hood
Mystic Vest
Reflect Ring

Sword, Spear
Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim



Phi Sig
Female
Libra
72
43
Wizard
Basic Skill
PA Save
Short Charge
Waterbreathing

Mythril Knife

Leather Hat
Wizard Outfit
Elf Mantle

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Death
Accumulate, Heal, Cheer Up, Wish



Mudrockk
Female
Taurus
71
75
Time Mage
Charge
Abandon
Equip Shield
Fly

Iron Fan
Aegis Shield
Green Beret
Earth Clothes
Dracula Mantle

Haste, Slow, Stop, Float, Quick, Stabilize Time
Charge+2, Charge+3

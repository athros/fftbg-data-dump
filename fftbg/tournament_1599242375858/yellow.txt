Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



FoeSquirrel
Female
Sagittarius
72
73
Squire
Draw Out
MA Save
Magic Defense UP
Jump+3

Snipe Bow
Crystal Shield
Ribbon
Clothes
Jade Armlet

Throw Stone, Heal, Tickle, Cheer Up, Fury
Murasame, Kiyomori, Muramasa



DeathTaxesAndAnime
Female
Sagittarius
43
70
Wizard
Math Skill
Auto Potion
Magic Defense UP
Jump+3

Poison Rod

Holy Miter
Rubber Costume
Battle Boots

Fire 3, Flare
CT, Prime Number, 4, 3



Holandrix
Female
Virgo
50
53
Thief
Draw Out
Critical Quick
Concentrate
Move+1

Spell Edge

Twist Headband
Mystic Vest
Reflect Ring

Arm Aim
Bizen Boat, Kikuichimoji



SQUiDSQUARKLIN
Male
Cancer
71
79
Samurai
Basic Skill
Abandon
Doublehand
Move-MP Up

Koutetsu Knife

Gold Helmet
Chain Mail
Wizard Mantle

Koutetsu, Murasame
Dash, Heal, Wish, Ultima

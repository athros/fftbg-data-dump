Player: !Black
Team: Black Team
Palettes: Black/Red



WhiteTigress
Male
Aries
79
80
Squire
Yin Yang Magic
Auto Potion
Dual Wield
Move+3

Iron Sword
Long Sword
Twist Headband
Mythril Armor
Jade Armlet

Accumulate, Heal, Tickle, Cheer Up, Fury
Doubt Faith, Paralyze



Technominari
Female
Leo
68
61
Priest
Punch Art
Distribute
Equip Gun
Waterwalking

Stone Gun

Leather Hat
Light Robe
Defense Armlet

Cure, Cure 2, Cure 4, Raise 2, Reraise, Protect, Esuna
Pummel, Purification, Chakra, Revive



Bigbongsmoker
Monster
Aries
49
55
Behemoth










Bruubarg
Female
Aries
64
75
Archer
Black Magic
Meatbone Slash
Martial Arts
Levitate


Buckler
Genji Helmet
Clothes
108 Gems

Charge+2, Charge+5
Fire, Fire 2, Fire 3, Ice, Ice 3, Empower, Frog

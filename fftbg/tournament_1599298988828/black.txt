Player: !Black
Team: Black Team
Palettes: Black/Red



LordTomS
Female
Cancer
78
74
Squire
Jump
MP Restore
Magic Defense UP
Move+3

Morning Star
Bronze Shield
Leather Helmet
Carabini Mail
Germinas Boots

Dash, Throw Stone, Heal, Scream
Level Jump8, Vertical Jump5



Smashy
Male
Aquarius
51
70
Lancer
Basic Skill
Meatbone Slash
Short Status
Swim

Holy Lance
Crystal Shield
Genji Helmet
White Robe
Sprint Shoes

Level Jump8, Vertical Jump5
Accumulate, Throw Stone, Heal



Jeeboheebo
Female
Aries
61
46
Geomancer
Throw
Caution
Doublehand
Jump+2

Broad Sword

Flash Hat
Wizard Outfit
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Lava Ball
Wand



Lawndough
Female
Sagittarius
58
76
Ninja
Punch Art
Auto Potion
Secret Hunt
Teleport

Hidden Knife
Flame Whip
Black Hood
Leather Outfit
Reflect Ring

Shuriken, Knife, Ninja Sword, Dictionary
Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil

Final Bets: green - 7 bets for 3,711G (46.9%, x1.13); black - 9 bets for 4,200G (53.1%, x0.88)

green bets:
Panushenko: 1,504G (40.5%, 1,504G)
lijarkh: 676G (18.2%, 676G)
brodeity: 622G (16.8%, 622G)
NicoSavoy: 360G (9.7%, 360G)
bigbongsmoker: 248G (6.7%, 5,345G)
datadrivenbot: 200G (5.4%, 56,540G)
gorgewall: 101G (2.7%, 6,068G)

black bets:
BirbBrainsBot: 1,000G (23.8%, 165,098G)
E_Ballard: 892G (21.2%, 130,265G)
BartTradingCompany: 656G (15.6%, 6,569G)
resjudicata3: 472G (11.2%, 472G)
getthemoneyz: 332G (7.9%, 1,830,258G)
randgridr: 328G (7.8%, 328G)
SeniorBunk: 300G (7.1%, 23,334G)
Absalom_20: 120G (2.9%, 4,339G)
Lawndough: 100G (2.4%, 1,749G)

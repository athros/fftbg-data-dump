Final Bets: black - 4 bets for 2,377G (42.5%, x1.35); purple - 11 bets for 3,217G (57.5%, x0.74)

black bets:
BirbBrainsBot: 1,000G (42.1%, 163,745G)
Lawndough: 743G (31.3%, 743G)
BartTradingCompany: 578G (24.3%, 5,787G)
getthemoneyz: 56G (2.4%, 1,830,182G)

purple bets:
E_Ballard: 892G (27.7%, 131,157G)
resjudicata3: 472G (14.7%, 472G)
NicoSavoy: 360G (11.2%, 360G)
randgridr: 328G (10.2%, 328G)
SeniorBunk: 300G (9.3%, 23,634G)
bigbongsmoker: 248G (7.7%, 5,593G)
datadrivenbot: 200G (6.2%, 56,740G)
Krinku: 116G (3.6%, 116G)
gorgewall: 101G (3.1%, 6,169G)
brodeity: 100G (3.1%, 722G)
DeathTaxesAndAnime: 100G (3.1%, 1,643G)

Final Bets: blue - 7 bets for 5,036G (32.4%, x2.08); purple - 11 bets for 10,484G (67.6%, x0.48)

blue bets:
LDSkinny: 1,000G (19.9%, 18,789G)
old_overholt_: 1,000G (19.9%, 31,796G)
BirbBrainsBot: 1,000G (19.9%, 33,533G)
gorgewall: 724G (14.4%, 724G)
CorpusCav: 500G (9.9%, 8,371G)
AllInBot: 444G (8.8%, 444G)
3ngag3: 368G (7.3%, 8,876G)

purple bets:
reinoe: 3,000G (28.6%, 31,821G)
JonnyCue: 1,704G (16.3%, 1,704G)
ColetteMSLP: 1,173G (11.2%, 1,173G)
resjudicata3: 1,166G (11.1%, 1,166G)
latebit: 1,111G (10.6%, 16,227G)
Nizaha: 876G (8.4%, 63,697G)
Error72: 408G (3.9%, 408G)
Wonser: 396G (3.8%, 3,236G)
SephDarkheart: 380G (3.6%, 37,955G)
getthemoneyz: 158G (1.5%, 2,035,190G)
scsuperstar: 112G (1.1%, 112G)

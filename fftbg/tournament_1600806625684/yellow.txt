Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Girfred
Male
Virgo
79
64
Mediator
Charge
Parry
Equip Knife
Move+1

Short Edge

Thief Hat
Light Robe
Feather Mantle

Threaten, Refute, Rehabilitate
Charge+1, Charge+2



Nizaha
Female
Capricorn
56
52
Thief
Throw
HP Restore
Equip Sword
Levitate

Defender

Cachusha
Secret Clothes
Defense Ring

Gil Taking, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Bomb



Latebit
Monster
Virgo
62
59
Mindflayer










Firesheath
Male
Aries
76
76
Oracle
Steal
MP Restore
Secret Hunt
Move+3

Madlemgen

Leather Hat
Mythril Vest
Defense Armlet

Spell Absorb, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze
Steal Shield, Steal Weapon, Steal Accessory, Leg Aim

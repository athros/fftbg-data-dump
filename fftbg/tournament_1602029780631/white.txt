Player: !White
Team: White Team
Palettes: White/Blue



Deathmaker06
Male
Gemini
57
44
Ninja
Time Magic
Faith Save
Doublehand
Retreat

Mage Masher

Red Hood
Earth Clothes
Magic Ring

Shuriken, Ninja Sword
Haste, Stop, Immobilize, Quick, Demi 2, Stabilize Time



MelloHeart
Female
Scorpio
47
49
Time Mage
Talk Skill
Parry
Monster Talk
Levitate

Rainbow Staff

Leather Hat
White Robe
Bracer

Haste, Slow, Slow 2, Float, Reflect, Quick, Demi, Stabilize Time
Praise, Death Sentence, Negotiate, Mimic Daravon, Refute



DarrenDinosaurs
Male
Pisces
57
51
Squire
Time Magic
Caution
Dual Wield
Retreat

Panther Bag
Slasher
Barbuta
Platinum Armor
Germinas Boots

Dash, Heal, Cheer Up, Fury, Wish, Scream
Slow, Slow 2, Stop, Reflect, Demi, Demi 2, Stabilize Time



Reinoe
Male
Aquarius
73
38
Monk
Jump
Speed Save
Dual Wield
Ignore Terrain



Triangle Hat
Judo Outfit
Dracula Mantle

Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Level Jump5, Vertical Jump3

Final Bets: black - 20 bets for 11,803G (35.2%, x1.84); champion - 27 bets for 21,759G (64.8%, x0.54)

black bets:
Runeseeker22: 1,252G (10.6%, 12,529G)
sinnyil2: 1,200G (10.2%, 108,134G)
CorpusCav: 1,200G (10.2%, 22,533G)
HaateXIII: 960G (8.1%, 960G)
resjudicata3: 750G (6.4%, 1,476G)
thunderducker: 700G (5.9%, 2,798G)
SkylerBunny: 696G (5.9%, 696G)
run_with_stone_GUNs: 696G (5.9%, 25,248G)
reinoe: 636G (5.4%, 636G)
Laserman1000: 626G (5.3%, 3,826G)
Dasutin23: 512G (4.3%, 11,247G)
killth3kid: 500G (4.2%, 25,448G)
Snowfats: 500G (4.2%, 5,759G)
grishhammer: 492G (4.2%, 985G)
AllInBot: 383G (3.2%, 383G)
mirapoix: 200G (1.7%, 5,123G)
old_overholt_: 200G (1.7%, 26,861G)
Thyrandaal: 100G (0.8%, 70,505G)
MemoriesofFinal: 100G (0.8%, 13,778G)
gingerfoo69: 100G (0.8%, 3,648G)

champion bets:
SephDarkheart: 10,000G (46.0%, 24,568G)
Panushenko: 1,162G (5.3%, 1,162G)
Zetchryn: 1,000G (4.6%, 3,778G)
BirbBrainsBot: 1,000G (4.6%, 151,400G)
Nizaha: 828G (3.8%, 73,660G)
DeathTaxesAndAnime: 800G (3.7%, 3,764G)
tacobouteverything: 782G (3.6%, 782G)
HASTERIOUS: 696G (3.2%, 696G)
dogsandcatsand: 624G (2.9%, 45,968G)
lowlf: 512G (2.4%, 3,654G)
Shalloween: 500G (2.3%, 6,224G)
ruleof5: 500G (2.3%, 1,778G)
getthemoneyz: 456G (2.1%, 1,947,377G)
roofiepops: 416G (1.9%, 416G)
CrazyLou64: 414G (1.9%, 414G)
ValensEXP_: 304G (1.4%, 2,945G)
tripaplex: 212G (1.0%, 212G)
Musashi45: 208G (1.0%, 208G)
Belkra: 200G (0.9%, 903G)
datadrivenbot: 200G (0.9%, 61,296G)
aceof86: 200G (0.9%, 4,459G)
Yangusburger: 200G (0.9%, 6,330G)
daveb_: 144G (0.7%, 6,138G)
gorgewall: 101G (0.5%, 12,896G)
BartTradingCompany: 100G (0.5%, 8,251G)
nhammen: 100G (0.5%, 425,484G)
douchetron: 100G (0.5%, 14,233G)

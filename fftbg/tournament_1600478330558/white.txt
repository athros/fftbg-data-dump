Player: !White
Team: White Team
Palettes: White/Blue



Prince Rogers Nelson
Female
Virgo
39
47
Summoner
Talk Skill
Counter Tackle
Short Charge
Lava Walking

Ice Rod

Black Hood
White Robe
Red Shoes

Moogle, Ramuh, Ifrit, Bahamut, Odin, Salamander, Silf, Lich
Invitation, Preach, Solution, Mimic Daravon, Refute



Nekojin
Male
Leo
66
50
Mediator
Basic Skill
Earplug
Short Charge
Move+2

Papyrus Codex

Ribbon
Clothes
Genji Gauntlet

Threaten, Preach, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Heal, Tickle, Yell, Wish, Scream



CorpusCav
Female
Gemini
71
73
Chemist
Summon Magic
Caution
Doublehand
Move+1

Star Bag

Green Beret
Brigandine
Elf Mantle

Potion, Ether, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Golem, Carbunkle, Salamander, Silf, Lich



3ngag3
Male
Pisces
77
46
Calculator
White Magic
Catch
Short Charge
Levitate

Iron Fan

Feather Hat
Clothes
Elf Mantle

CT, Height, Prime Number, 3
Cure 2, Raise, Raise 2, Protect, Wall, Esuna

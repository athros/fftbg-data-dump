Player: !Black
Team: Black Team
Palettes: Black/Red



Old Overholt
Male
Taurus
49
59
Bard
Time Magic
Catch
Halve MP
Retreat

Ramia Harp

Holy Miter
Carabini Mail
Genji Gauntlet

Angel Song, Life Song, Hydra Pit
Haste, Haste 2, Slow, Stop, Reflect, Quick, Demi, Demi 2



Acid Flashback
Female
Aries
79
76
Mime

Critical Quick
Equip Armor
Swim



Golden Hairpin
Gold Armor
Feather Mantle

Mimic




Error72
Male
Scorpio
57
55
Mime

Dragon Spirit
Equip Armor
Move-HP Up



Bronze Helmet
Carabini Mail
Sprint Shoes

Mimic




LDSkinny
Female
Taurus
43
57
Oracle
Summon Magic
PA Save
Short Charge
Teleport

Battle Folio

Red Hood
Mythril Vest
Battle Boots

Blind, Spell Absorb, Silence Song, Foxbird, Dispel Magic, Sleep
Moogle, Shiva, Ramuh, Carbunkle, Odin, Salamander, Zodiac

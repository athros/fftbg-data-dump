Player: !Black
Team: Black Team
Palettes: Black/Red



Kilrazin
Female
Capricorn
46
61
Wizard
Time Magic
Regenerator
Equip Axe
Ignore Height

Flame Whip

Thief Hat
Chain Vest
Sprint Shoes

Fire 3, Fire 4, Bolt, Bolt 4, Ice 3, Ice 4, Frog, Death
Haste, Haste 2, Slow, Float, Stabilize Time, Meteor



3ngag3
Male
Leo
75
71
Squire
Jump
MP Restore
Dual Wield
Lava Walking

Coral Sword
Blood Sword
Green Beret
Secret Clothes
Leather Mantle

Dash, Tickle
Level Jump4, Vertical Jump2



NicoSavoy
Male
Libra
65
54
Samurai
Yin Yang Magic
Faith Save
Dual Wield
Waterbreathing

Kiyomori
Kiyomori
Circlet
Leather Armor
Magic Gauntlet

Asura, Kikuichimoji
Blind, Zombie, Paralyze, Sleep, Petrify, Dark Holy



Arcblazer23
Female
Sagittarius
52
53
Oracle
Punch Art
MA Save
Equip Sword
Teleport

Koutetsu Knife

Feather Hat
Leather Outfit
Reflect Ring

Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Dark Holy
Purification, Revive, Seal Evil

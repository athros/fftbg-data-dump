Player: !Red
Team: Red Team
Palettes: Red/Brown



Go2sleepTV
Female
Taurus
45
57
Summoner
Item
Distribute
Short Charge
Move+3

Ice Rod

Black Hood
Mythril Vest
Spike Shoes

Shiva, Ifrit, Titan, Carbunkle
Ether, Antidote, Soft



NovaKnight21
Male
Virgo
75
73
Archer
Punch Art
Faith Save
Doublehand
Swim

Bow Gun

Twist Headband
Power Sleeve
Sprint Shoes

Charge+1, Charge+5, Charge+20
Pummel, Wave Fist, Earth Slash, Purification, Revive



Zachara
Female
Aries
71
64
Ninja
Item
PA Save
Long Status
Move+2

Flail
Flame Whip
Flash Hat
Earth Clothes
Jade Armlet

Knife, Staff
Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Remedy, Phoenix Down



Jeeboheebo
Female
Pisces
61
61
Priest
Yin Yang Magic
Auto Potion
Short Charge
Ignore Height

Wizard Staff

Leather Hat
Robe of Lords
Magic Ring

Cure 3, Cure 4, Reraise, Protect, Protect 2, Shell, Esuna, Holy, Magic Barrier
Blind, Poison, Pray Faith, Blind Rage, Petrify

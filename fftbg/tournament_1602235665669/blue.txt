Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DeathTaxesAndAnime
Female
Cancer
38
71
Thief
Draw Out
Caution
Dual Wield
Jump+3

Zorlin Shape
Air Knife
Headgear
Earth Clothes
Feather Mantle

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Heaven's Cloud



CassiePhoenix
Male
Aries
55
38
Lancer
Steal
MA Save
Doublehand
Waterwalking

Panther Bag

Cross Helmet
Gold Armor
Battle Boots

Level Jump8, Vertical Jump8
Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



IBardic
Male
Sagittarius
74
67
Squire
White Magic
Mana Shield
Long Status
Jump+1

Flame Whip
Mythril Shield
Holy Miter
Carabini Mail
Feather Mantle

Accumulate, Heal, Cheer Up, Wish, Scream
Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Shell 2, Wall, Esuna, Holy



HASTERIOUS
Male
Capricorn
69
51
Archer
Item
Counter Tackle
Doublehand
Fly

Mythril Gun

Flash Hat
Mythril Vest
Dracula Mantle

Charge+3, Charge+4, Charge+7
Potion, Eye Drop, Echo Grass, Phoenix Down

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



3ngag3
Female
Taurus
56
60
Mediator
Item
PA Save
Throw Item
Jump+3

Glacier Gun

Twist Headband
Leather Outfit
Germinas Boots

Invitation, Persuade, Solution, Death Sentence, Insult, Negotiate, Rehabilitate
Potion, Ether, Hi-Ether, Antidote, Soft, Holy Water, Remedy, Phoenix Down



Latebit
Male
Taurus
56
42
Ninja
Item
Mana Shield
Equip Bow
Move-HP Up

Bow Gun
Bow Gun
Twist Headband
Earth Clothes
Elf Mantle

Knife, Staff
Potion, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down



NicoSavoy
Male
Cancer
51
73
Mime

MA Save
Dual Wield
Jump+2



Thief Hat
Wizard Robe
Bracer

Mimic




Galkife
Male
Leo
61
45
Oracle
Elemental
Parry
Equip Armor
Levitate

Bestiary

Headgear
Bronze Armor
Angel Ring

Blind, Poison, Spell Absorb, Pray Faith, Paralyze, Sleep
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

Player: !Red
Team: Red Team
Palettes: Red/Brown



ArchKnightX
Male
Libra
67
70
Archer
Black Magic
Critical Quick
Equip Bow
Swim

Yoichi Bow

Twist Headband
Power Sleeve
Magic Ring

Charge+1, Charge+2
Fire, Fire 3, Bolt 2, Frog



DLJuggernaut
Male
Taurus
52
70
Thief
Item
Auto Potion
Throw Item
Teleport

Broad Sword

Red Hood
Earth Clothes
Defense Armlet

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Leg Aim
X-Potion, Eye Drop, Echo Grass, Remedy, Phoenix Down



Ruleof5
Monster
Aquarius
71
47
Cockatrice










Resjudicata3
Female
Leo
72
61
Monk
Elemental
Caution
Long Status
Move+3



Flash Hat
Mythril Vest
Spike Shoes

Earth Slash, Secret Fist, Seal Evil
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

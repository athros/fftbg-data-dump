Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Female
Scorpio
51
62
Archer
Summon Magic
Counter Flood
Maintenance
Jump+3

Ice Bow

Bronze Helmet
Power Sleeve
Elf Mantle

Charge+1, Charge+4, Charge+5, Charge+7
Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Salamander, Fairy



PopsLIVE
Male
Scorpio
65
44
Ninja
Draw Out
Counter Magic
Equip Bow
Ignore Height

Bow Gun
Snipe Bow
Flash Hat
Power Sleeve
108 Gems

Shuriken, Bomb
Kiyomori



DHaveWord
Female
Gemini
43
70
Samurai
Throw
MA Save
Short Status
Lava Walking

Koutetsu Knife

Crystal Helmet
Mythril Armor
N-Kai Armlet

Koutetsu, Kiyomori, Muramasa
Shuriken, Knife, Staff, Wand



Smashy
Male
Libra
63
68
Monk
Draw Out
MP Restore
Attack UP
Teleport 2



Leather Hat
Mystic Vest
Setiemson

Spin Fist, Wave Fist, Earth Slash, Purification
Asura, Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji

Player: !Green
Team: Green Team
Palettes: Green/White



Xsifthewolf
Male
Aquarius
64
58
Geomancer
Yin Yang Magic
Distribute
Equip Polearm
Waterwalking

Mythril Spear
Aegis Shield
Triangle Hat
Judo Outfit
Bracer

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Dispel Magic, Paralyze, Petrify



PuzzleSecretary
Male
Aries
58
63
Ninja
Yin Yang Magic
MA Save
Halve MP
Ignore Terrain

Kunai
Kunai
Leather Hat
Judo Outfit
Red Shoes

Shuriken, Bomb
Poison, Dispel Magic



DeathTaxesAndAnime
Female
Pisces
46
60
Wizard
Math Skill
Brave Save
Short Charge
Move+2

Orichalcum

Green Beret
Wizard Outfit
N-Kai Armlet

Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Empower
CT, 5, 3



Seppu777
Male
Pisces
48
48
Lancer
Punch Art
Speed Save
Equip Axe
Levitate

Scorpion Tail
Genji Shield
Mythril Helmet
Silk Robe
Bracer

Level Jump8, Vertical Jump8
Pummel, Purification, Chakra, Revive

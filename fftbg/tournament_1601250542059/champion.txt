Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Evewho
Female
Serpentarius
53
54
Chemist
Charge
MP Restore
Equip Shield
Move-HP Up

Panther Bag
Escutcheon
Barette
Leather Outfit
Rubber Shoes

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Charge+3, Charge+4, Charge+10



Ar Tactic
Female
Capricorn
58
57
Summoner
Draw Out
MA Save
Equip Knife
Teleport

Main Gauche

Green Beret
Linen Robe
Sprint Shoes

Moogle, Shiva, Titan, Golem, Carbunkle, Silf
Bizen Boat, Kikuichimoji



Chuckolator
Male
Capricorn
67
65
Samurai
Basic Skill
Catch
Equip Gun
Move+3

Ramia Harp

Barbuta
Mythril Armor
Salty Rage

Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Accumulate, Throw Stone, Heal, Yell



Rabbitlogik
Female
Aries
54
53
Thief
Elemental
PA Save
Doublehand
Retreat

Blood Sword

Green Beret
Clothes
N-Kai Armlet

Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Arm Aim
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball

Final Bets: blue - 11 bets for 7,470G (56.4%, x0.77); yellow - 7 bets for 5,775G (43.6%, x1.29)

blue bets:
Carledo: 2,802G (37.5%, 140,142G)
VolgraTheMoose: 1,080G (14.5%, 1,080G)
BirbBrainsBot: 1,000G (13.4%, 92,029G)
E_Ballard: 672G (9.0%, 672G)
ZPawZ: 500G (6.7%, 51,998G)
AllInBot: 416G (5.6%, 416G)
ColetteMSLP: 300G (4.0%, 1,600G)
J_52: 300G (4.0%, 8,876G)
datadrivenbot: 200G (2.7%, 63,399G)
BartTradingCompany: 100G (1.3%, 8,925G)
MemoriesofFinal: 100G (1.3%, 9,195G)

yellow bets:
Nwalme583: 2,262G (39.2%, 2,262G)
ValensEXP_: 1,353G (23.4%, 1,353G)
Zachara: 1,300G (22.5%, 143,300G)
bruubarg: 500G (8.7%, 9,614G)
Mushufasa_: 200G (3.5%, 4,799G)
BaronHaynes: 100G (1.7%, 20,385G)
getthemoneyz: 60G (1.0%, 1,930,439G)

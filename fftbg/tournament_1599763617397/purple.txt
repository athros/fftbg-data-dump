Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



StealthModeLocke
Male
Libra
79
67
Thief
White Magic
Counter Flood
Concentrate
Jump+2

Iron Sword

Holy Miter
Mystic Vest
Diamond Armlet

Steal Heart, Steal Helmet, Steal Status, Arm Aim, Leg Aim
Cure, Cure 2, Cure 3, Protect 2, Shell



Latebit
Monster
Sagittarius
74
73
Cockatrice










Actual JP
Female
Cancer
72
64
Geomancer
Jump
Counter
Defense UP
Levitate

Asura Knife
Bronze Shield
Holy Miter
Chain Vest
Spike Shoes

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Level Jump8, Vertical Jump3



Icy Blue20
Male
Aquarius
43
62
Geomancer
Jump
Regenerator
Magic Defense UP
Teleport

Slasher
Mythril Shield
Feather Hat
Silk Robe
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind
Level Jump5, Vertical Jump7

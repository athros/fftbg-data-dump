Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SephDarkheart
Male
Capricorn
73
53
Monk
Black Magic
PA Save
Doublehand
Lava Walking



Green Beret
Chain Vest
Battle Boots

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Fire 2, Fire 3, Ice 2, Ice 3



Krombobreaker
Female
Gemini
51
57
Mediator
Time Magic
Earplug
Maintenance
Swim

Blaze Gun

Green Beret
Mystic Vest
Angel Ring

Threaten, Solution, Insult, Negotiate, Refute
Stop, Immobilize, Demi 2



Loveyouallfriends
Male
Cancer
70
48
Mediator
Elemental
Absorb Used MP
Equip Gun
Move-MP Up

Papyrus Codex

Holy Miter
White Robe
Rubber Shoes

Preach, Solution, Insult, Mimic Daravon, Rehabilitate
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind



Resjudicata3
Male
Gemini
56
66
Wizard
Draw Out
Critical Quick
Equip Polearm
Waterwalking

Cypress Rod

Green Beret
Adaman Vest
Power Wrist

Fire 2, Fire 3, Bolt 2, Ice 2, Death
Heaven's Cloud, Kiyomori, Muramasa

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Smashy
Female
Aries
55
80
Lancer
Talk Skill
Counter Magic
Defend
Move+2

Mythril Spear
Genji Shield
Circlet
Diamond Armor
Cursed Ring

Level Jump2, Vertical Jump7
Invitation, Preach, Death Sentence, Mimic Daravon, Refute



Just Here2
Female
Capricorn
66
36
Mediator
Draw Out
Counter
Dual Wield
Move-MP Up

Stone Gun
Blast Gun
Holy Miter
Black Robe
Cursed Ring

Invitation, Threaten, Solution, Death Sentence, Rehabilitate
Koutetsu, Kiyomori



VolgraTheMoose
Male
Taurus
54
49
Lancer
Black Magic
Absorb Used MP
Doublehand
Move+3

Partisan

Gold Helmet
Genji Armor
Diamond Armlet

Level Jump8, Vertical Jump5
Fire 3, Bolt 2, Bolt 3, Ice, Ice 3, Death



Runeseeker22
Male
Capricorn
68
52
Archer
Throw
MP Restore
Short Charge
Swim

Ice Bow

Black Hood
Power Sleeve
108 Gems

Charge+20
Shuriken, Bomb

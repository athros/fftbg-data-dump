Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Greggernaut
Male
Taurus
54
53
Lancer
Time Magic
Auto Potion
Maintenance
Retreat

Gokuu Rod
Genji Shield
Cross Helmet
Plate Mail
Power Wrist

Level Jump8, Vertical Jump5
Float, Quick, Demi 2, Stabilize Time



NicoSavoy
Monster
Libra
47
45
Bull Demon










Douchetron
Male
Pisces
53
78
Wizard
Throw
Abandon
Equip Polearm
Waterwalking

Ryozan Silk

Flash Hat
Black Robe
Dracula Mantle

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Frog
Sword, Spear, Wand



Kylan
Male
Leo
73
78
Mediator
Throw
MA Save
Secret Hunt
Ignore Terrain

Mythril Gun

Black Hood
Earth Clothes
Dracula Mantle

Invitation, Praise, Negotiate, Mimic Daravon, Refute, Rehabilitate
Shuriken, Wand, Dictionary

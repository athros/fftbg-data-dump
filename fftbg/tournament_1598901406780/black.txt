Player: !Black
Team: Black Team
Palettes: Black/Red



Galkife
Female
Cancer
50
77
Oracle
Black Magic
MP Restore
Equip Sword
Move+3

Save the Queen

Green Beret
Mystic Vest
Sprint Shoes

Blind, Spell Absorb, Doubt Faith, Dark Holy
Fire, Bolt, Bolt 3, Ice 2, Ice 3, Death



Brenogarwin
Male
Aquarius
43
62
Thief
Battle Skill
MP Restore
Equip Shield
Waterwalking

Blind Knife
Escutcheon
Red Hood
Mythril Vest
Spike Shoes

Steal Weapon
Head Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Surging Sword



OneHundredFists
Female
Gemini
70
69
Knight
Talk Skill
Distribute
Short Status
Move-HP Up

Giant Axe
Gold Shield
Iron Helmet
Leather Armor
Diamond Armlet

Armor Break, Weapon Break, Magic Break, Night Sword
Threaten, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute



Snkey
Male
Capricorn
50
51
Wizard
Punch Art
Faith Save
Attack UP
Teleport

Rod

Golden Hairpin
Chameleon Robe
Bracer

Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Frog, Death, Flare
Wave Fist, Earth Slash, Secret Fist, Purification, Revive

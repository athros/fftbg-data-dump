Final Bets: purple - 19 bets for 6,174G (13.7%, x6.28); champion - 15 bets for 38,798G (86.3%, x0.16)

purple bets:
LDSkinny: 1,000G (16.2%, 32,295G)
BirbBrainsBot: 1,000G (16.2%, 3,315G)
lowlf: 504G (8.2%, 504G)
JonnyCue: 461G (7.5%, 461G)
Lord_Gwarth: 440G (7.1%, 440G)
Sairentozon7: 354G (5.7%, 3,818G)
Raixelol: 348G (5.6%, 5,914G)
SephDarkheart: 304G (4.9%, 304G)
getthemoneyz: 248G (4.0%, 1,793,801G)
Runeseeker22: 248G (4.0%, 4,239G)
krombobreaker: 244G (4.0%, 244G)
Leonidusx: 236G (3.8%, 236G)
killth3kid: 200G (3.2%, 6,963G)
RobotOcelot: 181G (2.9%, 3,620G)
macradarkstar: 100G (1.6%, 181G)
Firesheath: 100G (1.6%, 3,203G)
Thyrandaal: 100G (1.6%, 34,902G)
Mushufasa_: 100G (1.6%, 11,998G)
HASTERIOUS: 6G (0.1%, 644G)

champion bets:
JumbocactuarX27: 15,035G (38.8%, 15,035G)
DeathTaxesAndAnime: 6,855G (17.7%, 13,443G)
reinoe: 5,084G (13.1%, 10,168G)
VolgraTheMoose: 5,001G (12.9%, 266,088G)
DavenIII: 2,404G (6.2%, 4,808G)
Nizaha: 788G (2.0%, 90,183G)
Kronikle: 608G (1.6%, 47,568G)
fenaen: 560G (1.4%, 3,112G)
CorpusCav: 500G (1.3%, 7,000G)
WhiteTigress: 500G (1.3%, 7,917G)
holdenmagronik: 441G (1.1%, 441G)
arumz: 400G (1.0%, 3,902G)
thunderducker: 300G (0.8%, 1,248G)
datadrivenbot: 200G (0.5%, 64,983G)
daveb_: 122G (0.3%, 20,491G)

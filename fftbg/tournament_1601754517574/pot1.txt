Final Bets: red - 22 bets for 10,424G (66.3%, x0.51); blue - 6 bets for 5,306G (33.7%, x1.96)

red bets:
BirbBrainsBot: 1,000G (9.6%, 43,801G)
Lord_Gwarth: 1,000G (9.6%, 16,595G)
Sairentozon7: 1,000G (9.6%, 79,280G)
getthemoneyz: 956G (9.2%, 2,159,099G)
VolgraTheMoose: 936G (9.0%, 35,734G)
E_Ballard: 876G (8.4%, 876G)
Shalloween: 600G (5.8%, 12,195G)
ApplesNP: 500G (4.8%, 91,023G)
krombobreaker: 500G (4.8%, 33,419G)
AltimaMantoid: 500G (4.8%, 12,500G)
DHaveWord: 484G (4.6%, 484G)
Mushufasa_: 388G (3.7%, 388G)
helpimabug: 288G (2.8%, 288G)
TheBrett: 272G (2.6%, 7,456G)
AllInBot: 200G (1.9%, 200G)
datadrivenbot: 200G (1.9%, 82,452G)
2b_yorha_b: 200G (1.9%, 3,390G)
Lydian_C: 123G (1.2%, 6,672G)
gorgewall: 101G (1.0%, 7,147G)
DouglasDragonThePoet: 100G (1.0%, 6,067G)
cloudycube: 100G (1.0%, 501G)
Tithonus: 100G (1.0%, 3,845G)

blue bets:
almost__sane: 2,000G (37.7%, 37,090G)
ko2q: 1,384G (26.1%, 2,768G)
lowlf: 648G (12.2%, 4,065G)
upvla: 600G (11.3%, 600G)
roofiepops: 474G (8.9%, 2,609G)
CorpusCav: 200G (3.8%, 35,970G)

Final Bets: red - 10 bets for 3,084G (12.3%, x7.15); champion - 23 bets for 22,048G (87.7%, x0.14)

red bets:
Rytor: 556G (18.0%, 556G)
roofiepops: 474G (15.4%, 3,258G)
Mushufasa_: 388G (12.6%, 388G)
Lolthsmeat: 387G (12.5%, 387G)
theQueenfuta: 333G (10.8%, 708G)
Demon_Lord_Josh: 300G (9.7%, 4,318G)
CorpusCav: 222G (7.2%, 34,968G)
murderclan: 150G (4.9%, 2,095G)
regularguy428: 150G (4.9%, 158G)
getthemoneyz: 124G (4.0%, 2,160,210G)

champion bets:
TheBrett: 7,369G (33.4%, 7,369G)
Thyrandaal: 3,420G (15.5%, 6,707G)
HASTERIOUS: 2,903G (13.2%, 2,903G)
Snowfats: 1,000G (4.5%, 24,847G)
ko2q: 790G (3.6%, 1,580G)
AltimaMantoid: 700G (3.2%, 12,700G)
Laserman1000: 685G (3.1%, 12,785G)
resjudicata3: 560G (2.5%, 560G)
seppu777: 555G (2.5%, 2,986G)
krombobreaker: 500G (2.3%, 33,698G)
old_overholt_: 500G (2.3%, 57,503G)
DHaveWord: 484G (2.2%, 484G)
helpimabug: 473G (2.1%, 473G)
brokenknight201: 374G (1.7%, 374G)
BirbBrainsBot: 311G (1.4%, 45,931G)
CosmicTactician: 300G (1.4%, 2,287G)
AllInBot: 200G (0.9%, 200G)
Evewho: 200G (0.9%, 8,928G)
datadrivenbot: 200G (0.9%, 82,484G)
2b_yorha_b: 200G (0.9%, 4,155G)
Lydian_C: 123G (0.6%, 7,235G)
gorgewall: 101G (0.5%, 7,004G)
AbandonedHall: 100G (0.5%, 614G)

Final Bets: white - 8 bets for 6,006G (50.1%, x0.99); black - 11 bets for 5,972G (49.9%, x1.01)

white bets:
Thyrandaal: 2,407G (40.1%, 2,407G)
JumbocactuarX27: 1,057G (17.6%, 1,057G)
BirbBrainsBot: 1,000G (16.7%, 116,254G)
pplvee1: 584G (9.7%, 8,883G)
CosmicTactician: 500G (8.3%, 46,621G)
getthemoneyz: 230G (3.8%, 1,814,795G)
bigbongsmoker: 128G (2.1%, 4,496G)
formerlydrdong: 100G (1.7%, 2,786G)

black bets:
VolgraTheMoose: 1,211G (20.3%, 123,343G)
Zachara: 1,000G (16.7%, 140,500G)
DaveStrider55: 1,000G (16.7%, 2,319G)
Raixelol: 896G (15.0%, 896G)
E_Ballard: 876G (14.7%, 129,300G)
SephDarkheart: 336G (5.6%, 6,138G)
acrusade: 200G (3.3%, 4,491G)
datadrivenbot: 200G (3.3%, 61,854G)
daveb_: 111G (1.9%, 2,226G)
gorgewall: 101G (1.7%, 5,627G)
TheSabretoothe: 41G (0.7%, 1,141G)

Player: !White
Team: White Team
Palettes: White/Blue



MemoriesofFinal
Male
Leo
66
71
Monk
Battle Skill
Arrow Guard
Equip Gun
Move-HP Up

Papyrus Codex

Golden Hairpin
Adaman Vest
Sprint Shoes

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break



SephDarkheart
Monster
Capricorn
47
71
Dark Behemoth










CosmicTactician
Male
Pisces
45
43
Samurai
Summon Magic
Abandon
Dual Wield
Levitate

Muramasa
Asura Knife
Crystal Helmet
White Robe
Elf Mantle

Murasame, Kikuichimoji
Moogle, Shiva, Titan, Salamander, Fairy



DaveStrider55
Female
Serpentarius
66
53
Squire
Item
Brave Save
Attack UP
Teleport

Sleep Sword
Platinum Shield
Twist Headband
Chain Vest
Genji Gauntlet

Accumulate, Throw Stone, Heal, Fury, Wish
Potion, X-Potion, Hi-Ether, Elixir, Antidote, Soft, Phoenix Down

Final Bets: purple - 12 bets for 13,138G (58.1%, x0.72); brown - 6 bets for 9,488G (41.9%, x1.38)

purple bets:
DavenIII: 2,000G (15.2%, 7,730G)
Error72: 1,878G (14.3%, 1,878G)
latebit: 1,532G (11.7%, 1,532G)
Skyridge0: 1,500G (11.4%, 63,590G)
VolgraTheMoose: 1,329G (10.1%, 1,329G)
getthemoneyz: 1,000G (7.6%, 2,227,080G)
BirbBrainsBot: 1,000G (7.6%, 115,244G)
dogsandcatsand: 971G (7.4%, 971G)
ValensEXP_: 524G (4.0%, 23,385G)
khelor_: 500G (3.8%, 8,835G)
Lord_Gwarth: 500G (3.8%, 5,082G)
superdevon1: 404G (3.1%, 6,392G)

brown bets:
E_Ballard: 7,621G (80.3%, 7,621G)
Lolthsmeat: 1,000G (10.5%, 52,834G)
AllInBot: 263G (2.8%, 263G)
Smugzug: 240G (2.5%, 12,540G)
datadrivenbot: 200G (2.1%, 88,038G)
HeroponThrawn: 164G (1.7%, 164G)

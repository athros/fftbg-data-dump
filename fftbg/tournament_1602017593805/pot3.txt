Final Bets: white - 14 bets for 11,046G (61.6%, x0.62); black - 14 bets for 6,892G (38.4%, x1.60)

white bets:
Mesmaster: 3,000G (27.2%, 5,111G)
DeathTaxesAndAnime: 2,717G (24.6%, 5,328G)
Lolthsmeat: 1,580G (14.3%, 1,580G)
Sharosa: 677G (6.1%, 677G)
lowlf: 668G (6.0%, 1,983G)
Nizaha: 668G (6.0%, 14,944G)
old_overholt_: 500G (4.5%, 61,479G)
AllInBot: 327G (3.0%, 327G)
DHaveWord: 208G (1.9%, 208G)
Evewho: 200G (1.8%, 7,944G)
ValensEXP_: 200G (1.8%, 4,847G)
gorgewall: 101G (0.9%, 5,982G)
maakur_: 100G (0.9%, 1,817G)
tripaplex: 100G (0.9%, 10,416G)

black bets:
Thyrandaal: 2,723G (39.5%, 2,723G)
BirbBrainsBot: 1,000G (14.5%, 114,597G)
killth3kid: 832G (12.1%, 22,089G)
Finewax: 400G (5.8%, 3,899G)
2b_yorha_b: 400G (5.8%, 22,572G)
CapnChaos12: 300G (4.4%, 12,189G)
CosmicTactician: 300G (4.4%, 3,269G)
KGenjy: 250G (3.6%, 3,692G)
FuzzyTigers: 214G (3.1%, 214G)
datadrivenbot: 200G (2.9%, 87,357G)
Lydian_C: 124G (1.8%, 7,747G)
Widewalk: 112G (1.6%, 112G)
getthemoneyz: 32G (0.5%, 2,182,587G)
DrAntiSocial: 5G (0.1%, 1,727G)

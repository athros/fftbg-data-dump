Player: !White
Team: White Team
Palettes: White/Blue



StealthModeLocke
Female
Taurus
59
59
Mime

Regenerator
Equip Shield
Retreat


Bronze Shield
Golden Hairpin
Earth Clothes
Germinas Boots

Mimic




Nizaha
Female
Aries
57
49
Summoner
Steal
Brave Save
Short Status
Jump+3

Wizard Staff

Black Hood
Silk Robe
Rubber Shoes

Moogle, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Fairy, Lich
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Status



PoroTact
Male
Gemini
62
71
Mime

Absorb Used MP
Magic Defense UP
Move-MP Up


Escutcheon
Triangle Hat
Earth Clothes
Cursed Ring

Mimic




Heropong
Female
Taurus
68
60
Monk
Charge
Brave Save
Attack UP
Retreat



Headgear
Adaman Vest
Diamond Armlet

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil
Charge+1, Charge+10

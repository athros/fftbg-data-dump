Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Sagittarius
45
73
Thief
Basic Skill
Counter Flood
Concentrate
Fly

Ancient Sword

Flash Hat
Chain Vest
Angel Ring

Steal Shield, Steal Status
Heal, Yell, Cheer Up, Wish, Scream



Thyrandaal
Male
Taurus
61
75
Mime

Counter Magic
Martial Arts
Fly



Feather Hat
Leather Outfit
Leather Mantle

Mimic




Krombobreaker
Monster
Libra
80
65
Holy Dragon










Phi Sig
Female
Gemini
64
75
Thief
Yin Yang Magic
Auto Potion
Concentrate
Teleport

Hidden Knife

Barette
Brigandine
Defense Armlet

Steal Heart, Steal Armor, Steal Status, Leg Aim
Blind, Silence Song, Foxbird, Dispel Magic

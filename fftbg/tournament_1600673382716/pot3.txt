Final Bets: white - 11 bets for 6,824G (63.1%, x0.58); black - 8 bets for 3,988G (36.9%, x1.71)

white bets:
thunderducker: 1,646G (24.1%, 1,646G)
DeathTaxesAndAnime: 1,447G (21.2%, 1,447G)
Lydian_C: 1,200G (17.6%, 6,118G)
Mesmaster: 802G (11.8%, 802G)
Zachara: 386G (5.7%, 143,386G)
ar_tactic: 300G (4.4%, 94,056G)
otakutaylor: 272G (4.0%, 272G)
AllInBot: 200G (2.9%, 200G)
datadrivenbot: 200G (2.9%, 72,244G)
khelor_: 200G (2.9%, 7,082G)
BirbBrainsBot: 171G (2.5%, 188,973G)

black bets:
gilgamensche: 912G (22.9%, 2,280G)
nifboy: 800G (20.1%, 8,597G)
Aeriyah: 614G (15.4%, 614G)
Raixelol: 600G (15.0%, 12,001G)
getthemoneyz: 462G (11.6%, 2,024,290G)
CorpusCav: 250G (6.3%, 7,358G)
gingerfoo69: 250G (6.3%, 9,037G)
victoriolue: 100G (2.5%, 93,971G)

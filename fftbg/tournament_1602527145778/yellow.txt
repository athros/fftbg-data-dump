Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Killth3kid
Female
Taurus
66
72
Lancer
Talk Skill
Regenerator
Monster Talk
Lava Walking

Obelisk
Genji Shield
Leather Helmet
Leather Armor
Defense Armlet

Level Jump4, Vertical Jump7
Persuade, Praise, Insult, Negotiate, Rehabilitate



Just Here2
Female
Leo
70
70
Time Mage
Item
Damage Split
Throw Item
Ignore Height

Rainbow Staff

Barette
Light Robe
Rubber Shoes

Haste, Reflect, Demi, Stabilize Time
Potion, X-Potion, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



Mudrockk
Female
Cancer
59
56
Squire
Jump
Arrow Guard
Equip Bow
Move-MP Up

Night Killer
Crystal Shield
Red Hood
Diamond Armor
Cursed Ring

Accumulate, Heal, Tickle, Yell, Fury, Wish
Level Jump3, Vertical Jump4



Shalloween
Male
Pisces
78
57
Bard
Basic Skill
Regenerator
Doublehand
Levitate

Bloody Strings

Thief Hat
Mystic Vest
Bracer

Life Song, Space Storage, Sky Demon, Hydra Pit
Yell, Cheer Up, Wish, Scream

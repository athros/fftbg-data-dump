Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CaptainAdmiralSPATULA
Female
Taurus
80
69
Priest
Draw Out
Brave Save
Magic Attack UP
Jump+1

Scorpion Tail

Golden Hairpin
Mythril Vest
Jade Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell 2, Wall, Esuna
Asura, Heaven's Cloud, Kiyomori



HASTERIOUS
Female
Scorpio
41
41
Priest
Math Skill
Distribute
Magic Attack UP
Jump+3

Rainbow Staff

Green Beret
Rubber Costume
Defense Ring

Cure, Cure 2, Cure 4, Raise, Regen, Wall, Esuna, Magic Barrier
CT, 5, 4, 3



WhiteTigress
Male
Capricorn
65
46
Squire
Battle Skill
Caution
Equip Axe
Retreat

Mace of Zeus
Flame Shield
Gold Helmet
Mystic Vest
Magic Ring

Accumulate, Throw Stone, Heal, Tickle, Cheer Up, Wish, Scream
Shield Break, Weapon Break, Magic Break



Butterbelljedi
Female
Cancer
76
69
Calculator
Spirit Skill
Meatbone Slash
Maintenance
Jump+1

Hydra Bag

Platinum Helmet
Platinum Armor
Genji Gauntlet

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch, Bite, Self Destruct, Flame Attack, Small Bomb, Spark

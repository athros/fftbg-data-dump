Player: !Black
Team: Black Team
Palettes: Black/Red



Tripaplex
Male
Leo
69
52
Calculator
Foul Skill
HP Restore
Equip Axe
Swim

Morning Star

Thief Hat
Linen Cuirass
Defense Armlet

Blue Magic
Straight Dash, Oink, Toot, Snort, Bequeath Bacon, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



Twisted Nutsatchel
Male
Pisces
42
42
Wizard
Battle Skill
Earplug
Long Status
Waterwalking

Blind Knife

Holy Miter
Secret Clothes
Leather Mantle

Fire 4, Bolt 3, Ice, Empower, Death
Head Break, Shield Break, Magic Break



Firesheath
Male
Pisces
47
50
Monk
Throw
HP Restore
Dual Wield
Move-HP Up



Thief Hat
Adaman Vest
Spike Shoes

Secret Fist, Chakra, Revive
Shuriken, Bomb, Knife, Staff



IndecisiveNinja
Male
Sagittarius
48
56
Chemist
Charge
Counter Flood
Beastmaster
Swim

Mythril Gun

Holy Miter
Earth Clothes
Dracula Mantle

Potion, Hi-Potion, Antidote, Eye Drop, Soft, Phoenix Down
Charge+10

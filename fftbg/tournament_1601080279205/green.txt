Player: !Green
Team: Green Team
Palettes: Green/White



Dogsandcatsand
Female
Pisces
45
59
Wizard
Time Magic
Parry
Short Charge
Teleport

Thunder Rod

Green Beret
Adaman Vest
Spike Shoes

Fire 4, Bolt, Bolt 2, Ice, Empower, Flare
Haste, Demi 2, Meteor



Resjudicata3
Female
Cancer
68
63
Samurai
Battle Skill
Sunken State
Sicken
Waterwalking

Holy Lance

Cross Helmet
Plate Mail
Reflect Ring

Asura, Bizen Boat, Kiyomori, Muramasa
Speed Break, Mind Break, Dark Sword



ALY327
Male
Scorpio
65
67
Summoner
Throw
MA Save
Beastmaster
Jump+3

Healing Staff

Golden Hairpin
Black Robe
Defense Ring

Moogle, Ramuh, Titan, Golem, Lich
Shuriken, Bomb, Hammer, Wand



HaateXIII
Male
Capricorn
45
73
Squire
Black Magic
Mana Shield
Halve MP
Move+1

Long Sword
Genji Shield
Golden Hairpin
Power Sleeve
Cursed Ring

Throw Stone, Heal, Yell, Cheer Up
Fire 3, Fire 4, Bolt 3, Ice, Empower

Final Bets: white - 10 bets for 4,384G (77.9%, x0.28); black - 3 bets for 1,243G (22.1%, x3.53)

white bets:
BirbBrainsBot: 1,000G (22.8%, 80,439G)
MemoriesofFinal: 1,000G (22.8%, 17,953G)
helpimabug: 859G (19.6%, 859G)
heropong: 377G (8.6%, 377G)
TheBrett: 284G (6.5%, 19,036G)
2b_yorha_b: 250G (5.7%, 10,523G)
getthemoneyz: 214G (4.9%, 2,168,671G)
datadrivenbot: 200G (4.6%, 83,920G)
DeathTaxesAndAnime: 100G (2.3%, 10,015G)
CassiePhoenix: 100G (2.3%, 3,271G)

black bets:
AllInBot: 618G (49.7%, 618G)
FE34923: 450G (36.2%, 2,584G)
Genkidou: 175G (14.1%, 1,487G)

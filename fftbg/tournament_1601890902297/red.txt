Player: !Red
Team: Red Team
Palettes: Red/Brown



TheSabretoothe
Male
Taurus
77
72
Thief
Charge
Meatbone Slash
Doublehand
Move+1

Spell Edge

Twist Headband
Judo Outfit
Leather Mantle

Steal Shield
Charge+1, Charge+3, Charge+4



Mesmaster
Male
Capricorn
51
62
Knight
Sing
Critical Quick
Equip Sword
Teleport 2

Save the Queen
Buckler
Barbuta
Plate Mail
Setiemson

Head Break, Armor Break, Weapon Break, Magic Break, Dark Sword
Angel Song



WillardJBradley
Female
Aries
54
47
Mediator
Throw
Faith Save
Beastmaster
Ignore Height

Romanda Gun

Flash Hat
Black Costume
Genji Gauntlet

Invitation, Threaten, Solution, Insult, Rehabilitate
Bomb



YaBoy125
Female
Cancer
81
65
Samurai
Punch Art
Meatbone Slash
Concentrate
Move+3

Obelisk

Bronze Helmet
Light Robe
Battle Boots

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Pummel, Secret Fist, Purification, Revive

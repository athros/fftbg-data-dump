Final Bets: purple - 6 bets for 3,382G (23.2%, x3.31); brown - 11 bets for 11,181G (76.8%, x0.30)

purple bets:
DeathTaxesAndAnime: 1,311G (38.8%, 1,311G)
Leonidusx: 752G (22.2%, 752G)
ColetteMSLP: 651G (19.2%, 651G)
Raixelol: 352G (10.4%, 712G)
DracoknightZero: 216G (6.4%, 216G)
PersonaTaken: 100G (3.0%, 100G)

brown bets:
reinoe: 5,854G (52.4%, 5,854G)
Mesmaster: 2,000G (17.9%, 89,650G)
BirbBrainsBot: 1,000G (8.9%, 31,689G)
macradarkstar: 500G (4.5%, 2,183G)
MemoriesofFinal: 471G (4.2%, 471G)
HentaiWriter: 347G (3.1%, 347G)
pepperbuster: 275G (2.5%, 4,275G)
rocl: 232G (2.1%, 76,562G)
Vultuous: 200G (1.8%, 2,923G)
datadrivenbot: 200G (1.8%, 65,438G)
getthemoneyz: 102G (0.9%, 1,804,849G)

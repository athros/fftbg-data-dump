Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Shalloween
Male
Serpentarius
73
53
Knight
Jump
Counter Flood
Equip Polearm
Teleport 2

Partisan
Ice Shield
Circlet
Carabini Mail
Jade Armlet

Armor Break, Power Break, Mind Break
Level Jump8, Vertical Jump7



Vorap
Male
Gemini
75
66
Chemist
Summon Magic
PA Save
Equip Knife
Levitate

Cultist Dagger

Black Hood
Brigandine
Dracula Mantle

Hi-Potion, X-Potion, Hi-Ether, Elixir, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Moogle, Shiva, Ramuh, Ifrit, Golem, Leviathan, Salamander, Silf



Latebit
Male
Pisces
51
78
Time Mage
Punch Art
Counter Flood
Equip Armor
Move+2

White Staff

Black Hood
Gold Armor
Elf Mantle

Haste, Stop, Immobilize, Float, Quick
Spin Fist, Secret Fist, Purification



Sharknaldson
Female
Taurus
69
70
Samurai
Jump
Speed Save
Attack UP
Move+2

Murasame

Gold Helmet
Bronze Armor
Small Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa
Level Jump8, Vertical Jump7

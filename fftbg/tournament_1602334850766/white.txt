Player: !White
Team: White Team
Palettes: White/Blue



DavenIII
Male
Cancer
64
77
Thief
Time Magic
Mana Shield
Doublehand
Levitate

Hidden Knife

Holy Miter
Judo Outfit
Defense Armlet

Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory
Haste, Slow 2, Reflect, Demi 2, Stabilize Time



DrAntiSocial
Male
Sagittarius
65
76
Chemist
Talk Skill
Faith Save
Dual Wield
Ignore Terrain

Cute Bag
Dagger
Black Hood
Brigandine
Feather Boots

Hi-Ether, Maiden's Kiss, Soft, Phoenix Down
Threaten, Preach, Insult, Mimic Daravon



King Smashington
Female
Aries
54
50
Mime

Abandon
Equip Shield
Jump+3


Platinum Shield
Leather Hat
Judo Outfit
Diamond Armlet

Mimic




E Ballard
Male
Libra
57
38
Samurai
Basic Skill
Auto Potion
Equip Bow
Levitate

Hunting Bow

Iron Helmet
Reflect Mail
Angel Ring

Asura, Koutetsu, Muramasa
Accumulate, Dash, Throw Stone, Wish

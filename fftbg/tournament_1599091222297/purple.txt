Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ArchKnightX
Female
Pisces
80
57
Summoner
Black Magic
HP Restore
Short Status
Move-MP Up

Dragon Rod

Leather Hat
Power Sleeve
Small Mantle

Carbunkle, Bahamut, Silf, Cyclops
Fire, Bolt 4, Ice, Ice 4, Death



Dogsandcatsand
Female
Capricorn
72
74
Wizard
Draw Out
Mana Shield
Dual Wield
Waterbreathing

Assassin Dagger
Dragon Rod
Triangle Hat
Mystic Vest
108 Gems

Fire 4, Death
Murasame



MemoriesofFinal
Female
Cancer
67
41
Mediator
Steal
Counter
Magic Defense UP
Ignore Height

Stone Gun

Holy Miter
Adaman Vest
Dracula Mantle

Invitation, Threaten, Preach, Solution, Refute, Rehabilitate
Steal Heart, Steal Armor, Steal Shield, Leg Aim



Brokenknight201
Male
Pisces
51
60
Ninja
Elemental
Sunken State
Martial Arts
Fly



Green Beret
Clothes
Magic Gauntlet

Shuriken, Bomb, Knife, Wand
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

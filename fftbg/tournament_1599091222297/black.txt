Player: !Black
Team: Black Team
Palettes: Black/Red



Nhammen
Female
Taurus
45
60
Samurai
Jump
Auto Potion
Long Status
Move-MP Up

Bizen Boat

Crystal Helmet
Chain Mail
Spike Shoes

Asura, Kiyomori, Muramasa
Level Jump4, Vertical Jump4



Maakur
Male
Taurus
57
45
Lancer
Time Magic
Meatbone Slash
Equip Sword
Retreat

Kiyomori
Venetian Shield
Grand Helmet
Chain Mail
N-Kai Armlet

Level Jump4, Vertical Jump5
Haste 2, Stop, Float, Quick, Stabilize Time



SephDarkheart
Male
Sagittarius
39
78
Oracle
Sing
Counter
Equip Sword
Move-HP Up

Defender

Green Beret
Silk Robe
Dracula Mantle

Blind, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Petrify
Cheer Song, Magic Song, Space Storage



VolgraTheMoose
Female
Libra
56
40
Summoner
Elemental
Damage Split
Short Charge
Ignore Height

Wizard Rod

Feather Hat
Black Robe
Power Wrist

Moogle, Ramuh, Ifrit
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball

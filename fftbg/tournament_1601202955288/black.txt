Player: !Black
Team: Black Team
Palettes: Black/Red



Spuzzmocker
Male
Gemini
47
46
Summoner
Elemental
Distribute
Maintenance
Move+1

Rod

Flash Hat
Chameleon Robe
Reflect Ring

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Odin, Salamander, Fairy
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



VolgraTheMoose
Female
Leo
61
47
Geomancer
Steal
Counter
Dual Wield
Fly

Giant Axe
Slasher
Twist Headband
Silk Robe
Setiemson

Pitfall, Water Ball, Hallowed Ground, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim



DHaveWord
Male
Taurus
75
72
Bard
Elemental
Arrow Guard
Doublehand
Jump+1

Ultimus Bow

Green Beret
Carabini Mail
N-Kai Armlet

Battle Song, Diamond Blade, Space Storage
Water Ball, Hallowed Ground, Quicksand, Gusty Wind, Lava Ball



DeathTaxesAndAnime
Female
Leo
66
45
Thief
Basic Skill
MA Save
Equip Polearm
Move+3

Holy Lance

Black Hood
Leather Outfit
Bracer

Steal Armor, Steal Accessory, Arm Aim, Leg Aim
Wish

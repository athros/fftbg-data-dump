Final Bets: black - 6 bets for 3,994G (54.0%, x0.85); champion - 8 bets for 3,402G (46.0%, x1.17)

black bets:
gorgewall: 2,244G (56.2%, 2,244G)
7Cerulean7: 900G (22.5%, 5,266G)
ar_tactic: 350G (8.8%, 96,155G)
AllInBot: 200G (5.0%, 200G)
datadrivenbot: 200G (5.0%, 72,183G)
lwtest: 100G (2.5%, 4,319G)

champion bets:
BirbBrainsBot: 1,000G (29.4%, 157,349G)
ForagerCats: 500G (14.7%, 7,513G)
Afro_Gundam: 500G (14.7%, 1,000G)
getthemoneyz: 342G (10.1%, 2,013,295G)
xsifthewolf: 300G (8.8%, 8,931G)
genkidou: 300G (8.8%, 628G)
Zerguzen: 260G (7.6%, 260G)
MemoriesofFinal: 200G (5.9%, 29,663G)

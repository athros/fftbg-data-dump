Final Bets: white - 11 bets for 3,714G (15.6%, x5.42); black - 17 bets for 20,123G (84.4%, x0.18)

white bets:
IaibOaob: 678G (18.3%, 11,712G)
readdesert: 661G (17.8%, 661G)
lotharek: 500G (13.5%, 7,594G)
3ngag3: 408G (11.0%, 4,484G)
AllInBot: 338G (9.1%, 338G)
getthemoneyz: 262G (7.1%, 2,100,493G)
TheBrett: 216G (5.8%, 216G)
DuPlatypus: 200G (5.4%, 938G)
Evewho: 200G (5.4%, 1,881G)
argentbast: 150G (4.0%, 3,695G)
gorgewall: 101G (2.7%, 16,072G)

black bets:
OneHundredFists: 5,707G (28.4%, 11,191G)
tinytittyfiend: 5,000G (24.8%, 6,763G)
ColetteMSLP: 2,060G (10.2%, 2,060G)
LivingHitokiri: 1,000G (5.0%, 138,366G)
Laserman1000: 940G (4.7%, 9,940G)
BirbBrainsBot: 832G (4.1%, 64,496G)
E_Ballard: 796G (4.0%, 796G)
killth3kid: 772G (3.8%, 41,345G)
krombobreaker: 600G (3.0%, 15,063G)
khelor_: 500G (2.5%, 5,022G)
CorpusCav: 400G (2.0%, 35,065G)
AltimaMantoid: 316G (1.6%, 16,116G)
Einswolf: 300G (1.5%, 4,639G)
2b_yorha_b: 300G (1.5%, 5,690G)
ValensEXP_: 200G (1.0%, 3,783G)
datadrivenbot: 200G (1.0%, 80,080G)
ayeCoop: 200G (1.0%, 550G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Projekt Mike
Male
Libra
71
54
Knight
Charge
Faith Save
Doublehand
Ignore Terrain

Ice Brand

Cross Helmet
Chameleon Robe
Rubber Shoes

Head Break, Shield Break, Weapon Break, Speed Break, Power Break, Justice Sword, Dark Sword
Charge+1, Charge+3



Brokenknight201
Female
Aries
72
77
Geomancer
Time Magic
Absorb Used MP
Magic Attack UP
Move+3

Diamond Sword
Mythril Shield
Leather Hat
Power Sleeve
Magic Gauntlet

Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Haste, Stop, Float, Stabilize Time



FriendSquirrel
Female
Virgo
71
70
Dancer
Yin Yang Magic
Damage Split
Doublehand
Move+3

Cashmere

Feather Hat
Clothes
Power Wrist

Witch Hunt, Nameless Dance
Life Drain, Foxbird, Paralyze



Redmage4evah
Male
Libra
67
78
Ninja
Sing
Arrow Guard
Concentrate
Waterbreathing

Flail
Morning Star
Twist Headband
Judo Outfit
Defense Ring

Shuriken, Bomb
Battle Song

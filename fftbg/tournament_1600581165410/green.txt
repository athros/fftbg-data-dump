Player: !Green
Team: Green Team
Palettes: Green/White



WhiteTigress
Male
Taurus
40
80
Squire
Elemental
Critical Quick
Equip Bow
Ignore Height

Windslash Bow

Circlet
Leather Outfit
Spike Shoes

Accumulate, Heal, Tickle, Scream
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Willjin
Female
Taurus
44
72
Thief
Yin Yang Magic
Faith Save
Equip Gun
Retreat

Papyrus Codex

Twist Headband
Brigandine
Magic Gauntlet

Gil Taking, Steal Heart, Steal Armor, Steal Status, Arm Aim
Blind, Poison, Spell Absorb, Blind Rage, Dispel Magic, Paralyze, Dark Holy



RunicMagus
Male
Virgo
62
73
Mediator
Black Magic
Distribute
Dual Wield
Move-HP Up

Glacier Gun
Blast Gun
Leather Hat
Chain Vest
Sprint Shoes

Persuade, Praise, Threaten, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon
Fire 3, Bolt 2, Ice 2, Frog



Lube Squid
Monster
Leo
71
62
Squidraken








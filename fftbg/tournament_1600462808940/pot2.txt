Final Bets: green - 8 bets for 4,882G (19.0%, x4.27); yellow - 21 bets for 20,823G (81.0%, x0.23)

green bets:
LanseDM: 2,000G (41.0%, 14,070G)
PoroTact: 1,089G (22.3%, 1,089G)
Nizaha: 852G (17.5%, 70,884G)
BoneMiser: 364G (7.5%, 364G)
getthemoneyz: 226G (4.6%, 1,981,636G)
mirapoix: 200G (4.1%, 3,546G)
gorgewall: 101G (2.1%, 13,522G)
moonliquor: 50G (1.0%, 74,897G)

yellow bets:
NicoSavoy: 10,000G (48.0%, 128,330G)
old_overholt_: 3,000G (14.4%, 24,246G)
BirbBrainsBot: 1,000G (4.8%, 79,515G)
douchetron: 911G (4.4%, 911G)
Shalloween: 880G (4.2%, 880G)
evontno: 604G (2.9%, 604G)
krombobreaker: 500G (2.4%, 7,757G)
roofiepops: 436G (2.1%, 436G)
run_with_stone_GUNs: 424G (2.0%, 11,433G)
ericzubat: 420G (2.0%, 4,984G)
DHaveWord: 412G (2.0%, 11,325G)
RagequitSA: 344G (1.7%, 344G)
resjudicata3: 340G (1.6%, 340G)
ValensEXP_: 336G (1.6%, 5,012G)
Smugzug: 300G (1.4%, 300G)
AllInBot: 200G (1.0%, 200G)
ShadowNinja25: 200G (1.0%, 200G)
datadrivenbot: 200G (1.0%, 68,376G)
Dodecadeath: 116G (0.6%, 116G)
MemoriesofFinal: 100G (0.5%, 36,883G)
Firesheath: 100G (0.5%, 706G)

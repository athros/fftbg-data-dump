Player: !Black
Team: Black Team
Palettes: Black/Red



Thyrandaal
Male
Capricorn
42
57
Knight
Basic Skill
Absorb Used MP
Short Status
Waterbreathing

Iron Sword
Crystal Shield
Circlet
Chameleon Robe
Bracer

Head Break, Armor Break, Weapon Break, Speed Break, Mind Break, Surging Sword
Accumulate, Wish



NicoSavoy
Female
Aries
64
51
Summoner
Item
Parry
Throw Item
Move-MP Up

Thunder Rod

Cachusha
Brigandine
N-Kai Armlet

Shiva, Ramuh, Golem, Silf
Potion, Hi-Ether, Soft, Phoenix Down



Belkra
Male
Sagittarius
77
80
Oracle
Punch Art
Regenerator
Concentrate
Jump+3

Cypress Rod

Black Hood
Earth Clothes
Feather Boots

Spell Absorb, Pray Faith, Doubt Faith, Foxbird, Dispel Magic, Petrify
Spin Fist, Pummel, Chakra



RuneS 77
Female
Sagittarius
50
73
Geomancer
Draw Out
Auto Potion
Sicken
Move-MP Up

Kiyomori
Flame Shield
Headgear
Linen Robe
Defense Armlet

Pitfall, Water Ball, Static Shock, Quicksand, Sand Storm, Blizzard
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji

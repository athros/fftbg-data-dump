Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Peeronid
Female
Virgo
77
55
Mediator
Throw
Damage Split
Dual Wield
Jump+1

Romanda Gun
Mythril Gun
Black Hood
Judo Outfit
Genji Gauntlet

Praise, Threaten, Solution, Negotiate, Refute
Shuriken



Lowlf
Female
Gemini
47
66
Priest
Time Magic
Hamedo
Defense UP
Lava Walking

White Staff

Twist Headband
Light Robe
Small Mantle

Cure 2, Cure 3, Shell, Wall, Esuna
Haste, Slow 2, Immobilize, Float, Quick, Stabilize Time



Moonliquor
Female
Scorpio
48
44
Geomancer
White Magic
Caution
Equip Gun
Jump+1

Battle Folio
Gold Shield
Golden Hairpin
Wizard Robe
Red Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Cure 2, Cure 4, Raise, Raise 2, Protect, Esuna, Holy



Firesheath
Female
Sagittarius
58
79
Samurai
Punch Art
HP Restore
Short Charge
Teleport 2

Koutetsu Knife

Circlet
Wizard Robe
Wizard Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Pummel, Secret Fist, Purification, Chakra, Revive

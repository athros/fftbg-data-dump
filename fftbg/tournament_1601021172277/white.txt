Player: !White
Team: White Team
Palettes: White/Blue



IaibOaob
Female
Aquarius
68
61
Oracle
Punch Art
Counter Magic
Secret Hunt
Move-HP Up

Whale Whisker

Black Hood
Linen Robe
Feather Mantle

Foxbird, Dispel Magic, Sleep
Secret Fist, Purification, Chakra



Gorgewall
Female
Taurus
66
68
Dancer
Battle Skill
Auto Potion
Defense UP
Move+3

Ryozan Silk

Green Beret
Robe of Lords
Red Shoes

Wiznaibus, Polka Polka, Disillusion, Void Storage
Head Break, Weapon Break, Justice Sword



MemoriesofFinal
Male
Virgo
46
66
Summoner
White Magic
Auto Potion
Short Charge
Waterbreathing

Healing Staff

Red Hood
Wizard Outfit
Feather Mantle

Moogle, Ifrit, Golem, Carbunkle, Bahamut, Salamander
Raise 2, Reraise, Regen, Shell, Shell 2, Esuna, Holy



Mirapoix
Female
Capricorn
59
47
Calculator
Gore Skill
Counter Tackle
Equip Sword
Lava Walking

Heaven's Cloud

Twist Headband
Reflect Mail
Sprint Shoes

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul

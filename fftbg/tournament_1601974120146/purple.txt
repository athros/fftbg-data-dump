Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Scsuperstar
Male
Aries
67
78
Summoner
Sing
Hamedo
Defense UP
Move+3

Battle Folio

Thief Hat
Clothes
108 Gems

Moogle, Shiva, Ramuh, Ifrit, Golem, Odin, Fairy
Battle Song



FuzzyTigers
Male
Aquarius
76
44
Wizard
Charge
Absorb Used MP
Dual Wield
Ignore Terrain

Dragon Rod
Thunder Rod
Black Hood
Silk Robe
Sprint Shoes

Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice 2, Frog
Charge+1, Charge+2



Rhyser76
Male
Cancer
52
70
Priest
Draw Out
Dragon Spirit
Halve MP
Move-MP Up

White Staff

Headgear
White Robe
Diamond Armlet

Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect, Esuna
Asura, Koutetsu, Heaven's Cloud



Bay
Male
Scorpio
65
45
Knight
Throw
Speed Save
Equip Bow
Ignore Terrain

Night Killer
Platinum Shield
Gold Helmet
Bronze Armor
Angel Ring

Head Break, Shield Break, Weapon Break, Magic Break, Stasis Sword
Shuriken, Bomb, Ninja Sword

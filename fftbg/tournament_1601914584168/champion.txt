Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Dogsandcatsand
Female
Sagittarius
71
58
Wizard
Item
Counter Magic
Short Status
Move+3

Thunder Rod

Red Hood
Earth Clothes
Angel Ring

Fire, Bolt 2, Bolt 3, Ice 4
Potion, Ether, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Ioksee
Female
Pisces
72
68
Monk
Dance
Caution
Maintenance
Fly



Golden Hairpin
Power Sleeve
Cherche

Purification
Disillusion, Nether Demon



CapnChaos12
Male
Cancer
44
80
Squire
Yin Yang Magic
HP Restore
Dual Wield
Move-HP Up

Long Sword
Morning Star
Platinum Helmet
Leather Outfit
Small Mantle

Throw Stone, Heal, Cheer Up, Fury, Wish
Blind, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze



Actual JP
Male
Aquarius
64
62
Thief
Throw
Speed Save
Sicken
Ignore Terrain

Blood Sword

Green Beret
Adaman Vest
Angel Ring

Steal Helmet, Steal Shield
Shuriken

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sairentozon7
Monster
Capricorn
80
59
Archaic Demon










Bryon W
Male
Scorpio
64
46
Bard
Summon Magic
PA Save
Halve MP
Waterwalking

Long Bow

Flash Hat
Chain Vest
Feather Boots

Angel Song
Ifrit, Titan, Golem, Carbunkle, Silf, Fairy, Cyclops



JarlBrolaf
Female
Cancer
66
45
Samurai
Item
Meatbone Slash
Equip Gun
Move-HP Up

Fairy Harp

Leather Helmet
Black Robe
Cursed Ring

Asura, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Potion, Ether, Echo Grass, Soft, Holy Water, Remedy



Mirapoix
Female
Taurus
59
73
Thief
Battle Skill
Counter Magic
Equip Armor
Levitate

Blind Knife

Cross Helmet
Diamond Armor
Feather Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Head Break, Armor Break, Shield Break, Weapon Break, Mind Break, Dark Sword, Explosion Sword

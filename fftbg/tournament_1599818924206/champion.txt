Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Neech
Male
Pisces
55
64
Chemist
Elemental
Auto Potion
Defense UP
Ignore Height

Cute Bag

Green Beret
Adaman Vest
Sprint Shoes

Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Pitfall, Water Ball, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Kronikle
Male
Virgo
73
39
Monk
Time Magic
Brave Save
Defend
Ignore Height



Barette
Secret Clothes
Spike Shoes

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Haste, Float, Demi, Stabilize Time, Meteor



Go2sleepTV
Female
Virgo
71
40
Mediator
Time Magic
HP Restore
Dual Wield
Swim

Stone Gun
Blast Gun
Headgear
Linen Robe
Defense Armlet

Persuade, Threaten, Mimic Daravon
Haste, Slow, Stop, Immobilize, Demi, Demi 2, Stabilize Time



Sinnyil2
Male
Taurus
48
65
Mime

MA Save
Martial Arts
Lava Walking



Twist Headband
Brigandine
Small Mantle

Mimic


Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Krombobreaker
Female
Cancer
63
42
Dancer
Black Magic
HP Restore
Martial Arts
Levitate

Persia

Twist Headband
Linen Robe
Feather Mantle

Witch Hunt, Slow Dance, Nether Demon, Dragon Pit
Fire, Bolt, Bolt 2, Bolt 4, Ice 2, Empower



Aldrammech
Female
Leo
67
65
Monk
Talk Skill
Caution
Equip Shield
Move-MP Up


Escutcheon
Triangle Hat
Leather Outfit
Cursed Ring

Earth Slash, Purification
Invitation, Solution, Negotiate, Refute



Pplvee1
Monster
Sagittarius
64
45
Bull Demon










Itsonlyspencer
Female
Taurus
71
75
Oracle
Draw Out
Regenerator
Long Status
Waterbreathing

Battle Bamboo

Golden Hairpin
Brigandine
Feather Mantle

Spell Absorb, Pray Faith, Zombie, Confusion Song, Dispel Magic, Dark Holy
Bizen Boat, Murasame

Final Bets: white - 8 bets for 3,320G (27.1%, x2.69); brown - 9 bets for 8,923G (72.9%, x0.37)

white bets:
Zachara: 1,330G (40.1%, 140,330G)
Willjin: 600G (18.1%, 17,325G)
goth_Applebees_hostess: 489G (14.7%, 2,489G)
CorpusCav: 300G (9.0%, 30,167G)
AllInBot: 200G (6.0%, 200G)
grimthegameking: 200G (6.0%, 625G)
gorgewall: 101G (3.0%, 11,604G)
Heroebal: 100G (3.0%, 4,647G)

brown bets:
OneHundredFists: 5,069G (56.8%, 40,826G)
technominari: 1,000G (11.2%, 90,381G)
BirbBrainsBot: 1,000G (11.2%, 133,139G)
thunderducker: 600G (6.7%, 600G)
lijarkh: 316G (3.5%, 1,684G)
ValensEXP_: 300G (3.4%, 6,833G)
sireaulou: 224G (2.5%, 224G)
getthemoneyz: 214G (2.4%, 2,129,532G)
datadrivenbot: 200G (2.2%, 79,336G)

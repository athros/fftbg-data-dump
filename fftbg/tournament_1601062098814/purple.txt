Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Killabandaid
Female
Gemini
50
52
Time Mage
Black Magic
Brave Save
Martial Arts
Ignore Terrain

White Staff

Thief Hat
Chain Vest
Jade Armlet

Haste, Haste 2, Slow, Slow 2, Stop, Reflect, Stabilize Time, Meteor
Fire, Fire 2, Bolt 2, Bolt 4, Ice, Ice 3



Gorgewall
Female
Taurus
66
68
Dancer
Battle Skill
Auto Potion
Defense UP
Move+3

Ryozan Silk

Green Beret
Robe of Lords
Red Shoes

Wiznaibus, Polka Polka, Disillusion, Void Storage
Head Break, Weapon Break, Justice Sword



DarrenDinosaurs
Male
Pisces
60
50
Priest
Jump
Counter Tackle
Defense UP
Jump+1

Flail

Holy Miter
Brigandine
Red Shoes

Cure, Cure 2, Raise, Shell, Wall
Level Jump8, Vertical Jump2



CorpusCav
Female
Sagittarius
80
67
Chemist
Draw Out
Counter
Dual Wield
Retreat

Blind Knife
Star Bag
Feather Hat
Brigandine
Defense Ring

Potion, X-Potion, Antidote, Phoenix Down
Asura, Murasame, Heaven's Cloud, Kikuichimoji

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lyner87
Male
Libra
57
78
Bard
Time Magic
Earplug
Sicken
Move+3

Silver Bow

Flash Hat
Chain Mail
Feather Mantle

Angel Song, Magic Song, Space Storage, Hydra Pit
Immobilize, Reflect, Demi 2, Stabilize Time, Meteor



Wiznaibusthis
Female
Libra
67
60
Lancer
Black Magic
PA Save
Concentrate
Fly

Partisan
Crystal Shield
Platinum Helmet
Linen Cuirass
108 Gems

Level Jump5, Vertical Jump5
Fire, Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Death, Flare



HaateXIII
Monster
Aquarius
79
74
Dark Behemoth










Powergems
Female
Aries
54
51
Geomancer
Basic Skill
Sunken State
Sicken
Move+2

Kikuichimoji
Bronze Shield
Leather Hat
White Robe
Genji Gauntlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Accumulate, Wish, Scream

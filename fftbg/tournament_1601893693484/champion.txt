Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Helpimabug
Male
Leo
50
50
Monk
White Magic
Auto Potion
Magic Attack UP
Jump+3



Thief Hat
Wizard Outfit
108 Gems

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Cure, Cure 3, Raise, Reraise, Regen, Esuna



TheBrett
Female
Leo
77
68
Wizard
Time Magic
Meatbone Slash
Doublehand
Move+3

Assassin Dagger

Golden Hairpin
Wizard Robe
108 Gems

Bolt, Bolt 3, Bolt 4, Empower
Slow 2, Immobilize, Stabilize Time



CassiePhoenix
Female
Aquarius
56
75
Samurai
Item
Damage Split
Magic Attack UP
Retreat

Chirijiraden

Crystal Helmet
Black Robe
Vanish Mantle

Bizen Boat, Kiyomori, Muramasa
Potion, X-Potion, Soft, Remedy, Phoenix Down



Aloisia
Female
Taurus
65
76
Summoner
Item
Counter Tackle
Beastmaster
Waterbreathing

Gold Staff

Green Beret
Silk Robe
N-Kai Armlet

Ifrit, Golem, Carbunkle, Odin, Salamander
X-Potion, Antidote, Remedy, Phoenix Down

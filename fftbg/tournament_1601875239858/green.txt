Player: !Green
Team: Green Team
Palettes: Green/White



NicoSavoy
Male
Sagittarius
63
45
Mime

Distribute
Dual Wield
Levitate



Ribbon
Chameleon Robe
Red Shoes

Mimic




SRG Chugganomics
Monster
Scorpio
53
46
Ahriman










Evewho
Female
Libra
43
77
Geomancer
Draw Out
Brave Save
Secret Hunt
Waterwalking

Slasher
Gold Shield
Ribbon
Brigandine
Bracer

Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Koutetsu, Muramasa



IaibOaob
Female
Leo
58
70
Wizard
Summon Magic
Arrow Guard
Equip Knife
Jump+1

Main Gauche

Black Hood
Chameleon Robe
Reflect Ring

Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Flare
Moogle, Ramuh, Odin, Leviathan, Fairy

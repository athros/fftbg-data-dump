Player: !White
Team: White Team
Palettes: White/Blue



GenZealot
Female
Gemini
67
79
Samurai
Jump
Arrow Guard
Magic Attack UP
Jump+2

Koutetsu Knife

Crystal Helmet
Chameleon Robe
Feather Mantle

Murasame
Level Jump8, Vertical Jump8



Gorgewall
Female
Libra
76
71
Lancer
Steal
Parry
Magic Defense UP
Ignore Terrain

Iron Fan
Mythril Shield
Circlet
Chameleon Robe
Spike Shoes

Level Jump2, Vertical Jump5
Gil Taking, Steal Shield, Leg Aim



Aaron2Nerdy
Female
Cancer
81
40
Priest
Charge
Earplug
Short Status
Move+3

Oak Staff

Barette
Silk Robe
Germinas Boots

Cure, Cure 2, Cure 4, Raise 2, Protect 2, Wall, Esuna, Holy
Charge+1



WireLord
Female
Taurus
74
51
Chemist
Yin Yang Magic
MP Restore
Short Status
Ignore Height

Panther Bag

Golden Hairpin
Black Costume
Red Shoes

Potion, Elixir, Antidote, Eye Drop, Phoenix Down
Blind, Poison, Pray Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic

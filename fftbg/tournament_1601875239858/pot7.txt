Final Bets: yellow - 17 bets for 7,477G (69.4%, x0.44); purple - 6 bets for 3,293G (30.6%, x2.27)

yellow bets:
NicoSavoy: 1,058G (14.2%, 1,058G)
BirbBrainsBot: 1,000G (13.4%, 78,209G)
Laserman1000: 834G (11.2%, 17,034G)
reinoe: 728G (9.7%, 728G)
AllInBot: 568G (7.6%, 568G)
Rune339: 514G (6.9%, 514G)
CorpusCav: 500G (6.7%, 51,560G)
LeoNightFury: 500G (6.7%, 6,871G)
FE34923: 400G (5.3%, 4,402G)
getthemoneyz: 372G (5.0%, 2,167,330G)
2b_yorha_b: 250G (3.3%, 10,269G)
datadrivenbot: 200G (2.7%, 83,346G)
bdb_goldenleo: 153G (2.0%, 2,561G)
DeathTaxesAndAnime: 100G (1.3%, 17,999G)
MoonSugarFiend: 100G (1.3%, 34,001G)
FuzzyTigers: 100G (1.3%, 2,767G)
scsuperstar: 100G (1.3%, 10,016G)

purple bets:
lowlf: 1,111G (33.7%, 5,785G)
FatTunaKing: 1,000G (30.4%, 9,209G)
clancrushbone: 517G (15.7%, 517G)
Arcblazer23: 364G (11.1%, 364G)
Evewho: 200G (6.1%, 7,728G)
gorgewall: 101G (3.1%, 4,295G)

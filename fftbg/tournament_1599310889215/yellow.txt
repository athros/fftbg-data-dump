Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lokenwow
Female
Pisces
75
60
Mime

Abandon
Martial Arts
Ignore Terrain



Headgear
Adaman Vest
Sprint Shoes

Mimic




Blastinus
Female
Cancer
52
70
Mediator
Battle Skill
MP Restore
Equip Sword
Move+2

Koutetsu Knife

Black Hood
Rubber Costume
Cherche

Invitation, Persuade, Praise, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon
Head Break, Shield Break, Speed Break, Justice Sword, Explosion Sword



E Ballard
Male
Capricorn
64
61
Wizard
Yin Yang Magic
MP Restore
Magic Attack UP
Jump+1

Faith Rod

Feather Hat
Mystic Vest
Salty Rage

Fire, Fire 2, Bolt, Bolt 3, Bolt 4, Ice 4, Empower
Blind, Spell Absorb, Foxbird, Dispel Magic



CassiePhoenix
Male
Libra
69
53
Monk
Throw
MA Save
Dual Wield
Swim



Golden Hairpin
Judo Outfit
Jade Armlet

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Shuriken, Bomb

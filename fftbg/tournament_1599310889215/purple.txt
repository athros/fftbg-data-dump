Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Rocl
Female
Libra
69
79
Samurai
Yin Yang Magic
Counter Magic
Sicken
Move-HP Up

Mythril Spear

Iron Helmet
Platinum Armor
Red Shoes

Koutetsu, Muramasa
Blind, Poison, Pray Faith, Zombie, Blind Rage, Dispel Magic



Skillomono
Male
Libra
39
48
Knight
Black Magic
Regenerator
Short Status
Swim

Ice Brand
Crystal Shield
Circlet
Black Robe
Jade Armlet

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword
Fire 3, Bolt 2, Bolt 3, Ice 2, Ice 3



Ar Tactic
Male
Gemini
52
61
Samurai
White Magic
HP Restore
Secret Hunt
Move-MP Up

Javelin

Barbuta
Carabini Mail
Angel Ring

Koutetsu, Heaven's Cloud, Masamune
Cure, Cure 3, Cure 4, Reraise, Protect, Protect 2, Shell, Wall, Esuna, Holy



Thyrandaal
Male
Aries
67
68
Thief
Charge
Parry
Short Charge
Move+1

Dagger

Flash Hat
Mythril Vest
Magic Ring

Gil Taking, Steal Shield, Leg Aim
Charge+1, Charge+5, Charge+10

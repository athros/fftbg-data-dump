Player: !Black
Team: Black Team
Palettes: Black/Red



MemoriesofFinal
Male
Sagittarius
65
37
Lancer
Elemental
Auto Potion
Magic Attack UP
Ignore Terrain

Iron Fan
Round Shield
Grand Helmet
Light Robe
Diamond Armlet

Level Jump3, Vertical Jump7
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



SkylerBunny
Female
Sagittarius
70
45
Archer
Draw Out
HP Restore
Secret Hunt
Jump+3

Blaze Gun
Gold Shield
Green Beret
Secret Clothes
Jade Armlet

Charge+1, Charge+4, Charge+7, Charge+20
Asura, Koutetsu



DAC169
Male
Capricorn
74
69
Oracle
Item
Counter Tackle
Equip Armor
Move-MP Up

Papyrus Codex

Red Hood
Chain Mail
108 Gems

Poison, Spell Absorb, Life Drain, Silence Song, Blind Rage, Dispel Magic
Hi-Potion, Ether, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Old Overholt
Male
Virgo
67
43
Lancer
Steal
Dragon Spirit
Dual Wield
Move+3

Partisan
Spear
Circlet
White Robe
Genji Gauntlet

Level Jump4, Vertical Jump5
Steal Armor, Steal Weapon, Steal Status

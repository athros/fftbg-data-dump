Final Bets: white - 7 bets for 3,703G (39.6%, x1.53); black - 7 bets for 5,649G (60.4%, x0.66)

white bets:
OneHundredFists: 1,069G (28.9%, 32,416G)
BirbBrainsBot: 1,000G (27.0%, 79,935G)
nok____: 500G (13.5%, 21,609G)
getthemoneyz: 434G (11.7%, 2,171,147G)
2b_yorha_b: 250G (6.8%, 10,520G)
Zachara: 250G (6.8%, 112,250G)
MemoriesofFinal: 200G (5.4%, 18,655G)

black bets:
DeathTaxesAndAnime: 2,377G (42.1%, 4,662G)
scsuperstar: 1,072G (19.0%, 1,072G)
TheBrett: 1,000G (17.7%, 15,875G)
FE34923: 500G (8.9%, 3,180G)
ar_tactic: 300G (5.3%, 72,026G)
AllInBot: 200G (3.5%, 200G)
datadrivenbot: 200G (3.5%, 83,763G)

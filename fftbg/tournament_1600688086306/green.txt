Player: !Green
Team: Green Team
Palettes: Green/White



Smashy
Male
Taurus
77
66
Wizard
White Magic
MA Save
Secret Hunt
Levitate

Wizard Rod

Holy Miter
Black Costume
Leather Mantle

Fire, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Death, Flare
Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Wall



Skipsandwiches
Male
Scorpio
73
54
Ninja
Charge
Speed Save
Short Charge
Lava Walking

Cultist Dagger
Hidden Knife
Red Hood
Black Costume
N-Kai Armlet

Shuriken, Knife
Charge+5, Charge+7



OneHundredFists
Female
Sagittarius
77
81
Priest
Black Magic
Speed Save
Magic Attack UP
Move+1

Healing Staff

Triangle Hat
Linen Robe
Defense Ring

Cure, Raise, Shell 2, Holy, Magic Barrier
Bolt, Ice, Ice 2, Ice 4



Reddwind
Male
Aquarius
81
67
Knight
Throw
Catch
Magic Attack UP
Teleport 2

Long Sword
Escutcheon
Platinum Helmet
White Robe
Elf Mantle

Head Break, Armor Break, Weapon Break, Power Break
Bomb, Stick, Dictionary

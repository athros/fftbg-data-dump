Final Bets: white - 8 bets for 4,252G (44.5%, x1.24); black - 10 bets for 5,293G (55.5%, x0.80)

white bets:
Jeeboheebo: 1,211G (28.5%, 1,211G)
pepperbuster: 1,000G (23.5%, 32,072G)
Chihuahua_Charity: 946G (22.2%, 946G)
OneHundredFists: 528G (12.4%, 528G)
otakutaylor: 300G (7.1%, 1,897G)
VolgraTheMoose: 125G (2.9%, 12,218G)
MemoriesofFinal: 100G (2.4%, 30,641G)
getthemoneyz: 42G (1.0%, 2,019,049G)

black bets:
Error72: 1,001G (18.9%, 27,169G)
BirbBrainsBot: 1,000G (18.9%, 191,708G)
Zachara: 900G (17.0%, 142,400G)
krombobreaker: 500G (9.4%, 17,090G)
upvla: 500G (9.4%, 2,549G)
silentkaster: 412G (7.8%, 412G)
AllInBot: 344G (6.5%, 344G)
Smashy: 336G (6.3%, 336G)
datadrivenbot: 200G (3.8%, 71,655G)
skipsandwiches: 100G (1.9%, 20,975G)

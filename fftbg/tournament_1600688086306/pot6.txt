Final Bets: black - 9 bets for 6,221G (74.7%, x0.34); brown - 5 bets for 2,108G (25.3%, x2.95)

black bets:
Zachara: 1,270G (20.4%, 143,270G)
Error72: 1,001G (16.1%, 29,840G)
krombobreaker: 1,000G (16.1%, 17,326G)
BirbBrainsBot: 1,000G (16.1%, 192,011G)
JumbocactuarX27: 740G (11.9%, 740G)
getthemoneyz: 462G (7.4%, 2,018,463G)
Smashy: 336G (5.4%, 336G)
ShadowNinja25: 212G (3.4%, 212G)
datadrivenbot: 200G (3.2%, 72,188G)

brown bets:
otakutaylor: 1,000G (47.4%, 3,707G)
skipsandwiches: 436G (20.7%, 20,955G)
SephDarkheart: 372G (17.6%, 44,126G)
AllInBot: 200G (9.5%, 200G)
MemoriesofFinal: 100G (4.7%, 30,460G)

Player: !Green
Team: Green Team
Palettes: Green/White



E Ballard
Male
Aquarius
76
55
Bard
Yin Yang Magic
Counter
Magic Attack UP
Move+3

Bloody Strings

Holy Miter
Linen Cuirass
Germinas Boots

Cheer Song, Last Song, Diamond Blade
Blind, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Sleep, Petrify



HS39
Male
Capricorn
77
63
Calculator
Time Magic
Damage Split
Equip Knife
Ignore Terrain

Flame Rod

Flash Hat
Wizard Outfit
Small Mantle

CT, Prime Number, 5, 4, 3
Haste 2, Stop, Immobilize, Quick, Stabilize Time



BaronHaynes
Monster
Libra
44
62
Goblin










Lumberinjack
Female
Pisces
55
72
Dancer
Yin Yang Magic
Counter
Equip Bow
Jump+3

Night Killer

Headgear
Black Costume
Sprint Shoes

Wiznaibus, Slow Dance, Disillusion
Spell Absorb, Life Drain, Blind Rage

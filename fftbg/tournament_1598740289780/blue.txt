Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Soren Of Tyto
Female
Scorpio
61
72
Ninja
Summon Magic
Counter
Equip Gun
Retreat

Stone Gun
Blaze Gun
Black Hood
Power Sleeve
Genji Gauntlet

Bomb, Knife, Wand
Ramuh, Golem, Bahamut, Leviathan, Salamander, Lich, Cyclops



Kronikle
Female
Leo
66
51
Samurai
Elemental
Dragon Spirit
Magic Attack UP
Waterwalking

Heaven's Cloud

Iron Helmet
Gold Armor
Leather Mantle

Heaven's Cloud
Pitfall, Water Ball, Local Quake, Blizzard, Gusty Wind, Lava Ball



Laserman1000
Female
Capricorn
59
72
Geomancer
Dance
Parry
Equip Knife
Jump+1

Short Edge
Mythril Shield
Red Hood
Mythril Vest
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand
Slow Dance, Last Dance, Nether Demon, Dragon Pit



FriendSquirrel
Female
Cancer
75
48
Thief
Charge
Critical Quick
Attack UP
Teleport

Nagrarock

Twist Headband
Power Sleeve
108 Gems

Steal Shield, Steal Status, Leg Aim
Charge+10, Charge+20

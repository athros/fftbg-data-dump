Player: !Red
Team: Red Team
Palettes: Red/Brown



Zachara
Female
Aquarius
64
69
Samurai
Yin Yang Magic
Critical Quick
Equip Shield
Jump+1

Kikuichimoji
Platinum Shield
Genji Helmet
Diamond Armor
Genji Gauntlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Poison, Spell Absorb, Blind Rage, Dispel Magic, Paralyze



Ribbiks
Male
Scorpio
54
64
Calculator
Black Magic
Mana Shield
Halve MP
Jump+1

Battle Folio

Black Hood
Black Costume
Power Wrist

Height, Prime Number, 5, 3
Fire 2, Bolt, Bolt 2, Ice 3, Ice 4, Death, Flare



Leonidusx
Male
Capricorn
55
79
Archer
Elemental
Arrow Guard
Doublehand
Retreat

Stone Gun

Flash Hat
Earth Clothes
Bracer

Charge+1
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



CorpusCav
Female
Aries
75
72
Chemist
Elemental
Mana Shield
Halve MP
Move+1

Zorlin Shape

Twist Headband
Brigandine
Reflect Ring

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard

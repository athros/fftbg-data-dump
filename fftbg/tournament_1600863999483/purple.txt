Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Krombobreaker
Male
Scorpio
69
76
Ninja
Battle Skill
Parry
Equip Gun
Move+2

Battle Folio
Bestiary
Barette
Chain Vest
Feather Boots

Shuriken, Bomb, Knife
Head Break



Latebit
Female
Scorpio
44
69
Thief
Jump
Brave Save
Magic Defense UP
Jump+1

Spell Edge

Headgear
Mythril Vest
Small Mantle

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Level Jump4, Vertical Jump8



Dinin991
Female
Virgo
79
74
Ninja
Time Magic
Counter Flood
Equip Gun
Move-HP Up

Bestiary
Fairy Harp
Cachusha
Mythril Vest
Sprint Shoes

Axe
Haste, Haste 2, Slow, Stop, Immobilize, Float, Reflect, Meteor



Ar Tactic
Female
Leo
42
67
Summoner
Draw Out
Hamedo
Defense UP
Move-HP Up

Wizard Rod

Holy Miter
Linen Robe
Sprint Shoes

Moogle, Shiva, Ramuh, Golem
Asura

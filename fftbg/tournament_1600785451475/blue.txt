Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gongonono
Female
Aries
44
56
Dancer
Elemental
Catch
Concentrate
Move+3

Persia

Headgear
Wizard Outfit
Small Mantle

Slow Dance, Polka Polka, Nameless Dance
Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind



Thyrandaal
Female
Aquarius
58
53
Geomancer
Steal
MP Restore
Magic Defense UP
Jump+1

Rune Blade
Ice Shield
Barette
Black Robe
Feather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



Kithmar796
Male
Leo
73
76
Wizard
Yin Yang Magic
Catch
Maintenance
Jump+1

Poison Rod

Flash Hat
Earth Clothes
Defense Armlet

Fire 3, Bolt, Bolt 2, Ice 3, Death
Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Petrify



VolgraTheMoose
Male
Gemini
57
50
Archer
Talk Skill
Parry
Doublehand
Teleport

Romanda Gun

Crystal Helmet
Clothes
Magic Ring

Charge+5, Charge+7, Charge+10
Invitation, Praise, Threaten, Negotiate

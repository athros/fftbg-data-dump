Player: !White
Team: White Team
Palettes: White/Blue



Fenaen
Female
Sagittarius
63
79
Time Mage
Dance
Counter Flood
Defense UP
Fly

Sage Staff

Thief Hat
White Robe
Magic Ring

Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Reflect, Demi 2, Stabilize Time, Galaxy Stop
Disillusion, Nameless Dance, Last Dance, Nether Demon



Moocaotao
Female
Pisces
66
51
Chemist
Battle Skill
Counter
Magic Defense UP
Move+1

Main Gauche

Twist Headband
Wizard Outfit
Sprint Shoes

Ether, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Armor Break, Power Break, Mind Break



Run With Stone GUNs
Male
Aries
67
51
Mediator
Item
Absorb Used MP
Dual Wield
Jump+1

Madlemgen
Battle Folio
Twist Headband
Mythril Vest
Germinas Boots

Praise, Insult, Refute
X-Potion, Soft, Holy Water, Remedy, Phoenix Down



ZCKaiser
Male
Leo
60
56
Calculator
Black Magic
Caution
Equip Sword
Waterbreathing

Kiyomori

Triangle Hat
Wizard Robe
N-Kai Armlet

CT, Height, 5, 3
Fire 2, Bolt, Bolt 2, Bolt 4, Ice

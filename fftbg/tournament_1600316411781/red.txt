Player: !Red
Team: Red Team
Palettes: Red/Brown



Zbgs
Male
Aries
50
50
Archer
Jump
Earplug
Magic Defense UP
Fly

Windslash Bow

Twist Headband
Chain Vest
Defense Armlet

Charge+1, Charge+3, Charge+7
Level Jump5, Vertical Jump2



Skillomono
Male
Virgo
65
75
Chemist
Yin Yang Magic
Damage Split
Equip Axe
Ignore Height

Battle Axe

Feather Hat
Black Costume
Small Mantle

Potion, Hi-Potion, Soft, Phoenix Down
Spell Absorb, Life Drain, Pray Faith, Silence Song, Blind Rage, Foxbird, Paralyze, Sleep



Reinoe
Female
Capricorn
59
42
Geomancer
Throw
Dragon Spirit
Short Charge
Ignore Terrain

Slasher
Crystal Shield
Red Hood
White Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Hammer, Staff, Dictionary



NicoSavoy
Male
Scorpio
73
62
Monk
Sing
Arrow Guard
Beastmaster
Move-MP Up



Green Beret
Adaman Vest
Salty Rage

Wave Fist, Purification, Revive, Seal Evil
Life Song, Battle Song, Magic Song, Last Song, Diamond Blade

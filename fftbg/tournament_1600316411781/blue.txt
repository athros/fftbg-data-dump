Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nifboy
Female
Taurus
40
77
Oracle
Elemental
Distribute
Long Status
Jump+1

Cypress Rod

Black Hood
Mythril Vest
Battle Boots

Poison, Life Drain, Silence Song, Blind Rage
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Victoriolue
Female
Libra
65
52
Geomancer
Steal
Earplug
Magic Attack UP
Fly

Giant Axe
Mythril Shield
Triangle Hat
Mythril Vest
108 Gems

Pitfall, Water Ball, Local Quake, Static Shock, Sand Storm, Blizzard
Gil Taking, Steal Heart, Leg Aim



Krombobreaker
Male
Cancer
40
57
Knight
Black Magic
Auto Potion
Attack UP
Lava Walking

Rune Blade
Mythril Shield
Gold Helmet
Silk Robe
Bracer

Magic Break, Power Break, Mind Break, Justice Sword, Dark Sword
Fire, Fire 2, Bolt 3, Ice, Ice 2



Gorgewall
Female
Leo
70
69
Samurai
Talk Skill
Arrow Guard
Short Charge
Move+3

Asura Knife

Genji Helmet
Silk Robe
Red Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Negotiate, Refute

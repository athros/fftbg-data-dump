Player: !White
Team: White Team
Palettes: White/Blue



Fattunaking
Male
Pisces
70
47
Monk
Steal
Parry
Secret Hunt
Move+3



Triangle Hat
Power Sleeve
Red Shoes

Wave Fist, Secret Fist, Purification, Revive
Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim



QuestMoreLand
Female
Pisces
62
74
Wizard
Draw Out
Sunken State
Doublehand
Ignore Terrain

Wizard Rod

Triangle Hat
Chain Vest
Magic Gauntlet

Bolt, Bolt 3, Ice 2
Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji



Ribbiks
Male
Libra
40
76
Wizard
Punch Art
Earplug
Equip Sword
Ignore Height

Long Sword

Thief Hat
Adaman Vest
Diamond Armlet

Fire, Fire 3, Bolt 2, Ice 4, Empower
Revive, Seal Evil



Pathogen7
Male
Gemini
66
79
Monk
Black Magic
MA Save
Equip Knife
Move+2

Main Gauche

Thief Hat
Wizard Outfit
Red Shoes

Earth Slash, Purification, Revive
Fire 2, Bolt 4

Player: !Black
Team: Black Team
Palettes: Black/Red



ValensEXP
Male
Sagittarius
79
45
Squire
Steal
Regenerator
Equip Sword
Fly

Chaos Blade

Green Beret
Crystal Mail
Genji Gauntlet

Heal, Tickle, Fury
Steal Weapon, Steal Status



Mudrockk
Monster
Virgo
73
46
Juravis










Gorgewall
Male
Leo
54
52
Thief
Time Magic
Abandon
Defense UP
Fly

Mythril Knife

Red Hood
Chain Vest
Bracer

Steal Shield, Arm Aim, Leg Aim
Haste, Haste 2, Stop, Immobilize, Meteor



DLJuggernaut
Male
Aries
54
68
Archer
Elemental
Parry
Doublehand
Move+3

Gastrafitis

Green Beret
Mythril Vest
Salty Rage

Charge+1, Charge+3, Charge+20
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !Red
Team: Red Team
Palettes: Red/Brown



Latebit
Female
Gemini
41
57
Priest
Item
HP Restore
Throw Item
Jump+2

White Staff

Leather Hat
Wizard Robe
Setiemson

Cure 2, Cure 3, Raise, Raise 2, Protect, Shell, Wall, Esuna
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



EvilLego6
Female
Aquarius
59
69
Mediator
White Magic
PA Save
Long Status
Waterwalking

Blast Gun

Holy Miter
Wizard Robe
Angel Ring

Persuade, Preach, Solution, Refute
Cure 4, Raise, Regen, Protect, Shell 2, Esuna, Holy



Mirapoix
Monster
Capricorn
61
64
Apanda










CorpusCav
Female
Cancer
41
51
Chemist
Battle Skill
Damage Split
Doublehand
Move-MP Up

Assassin Dagger

Holy Miter
Chain Vest
Reflect Ring

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword

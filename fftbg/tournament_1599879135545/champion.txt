Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



CosmicTactician
Female
Gemini
44
65
Time Mage
Punch Art
Parry
Short Charge
Waterbreathing

Ivory Rod

Feather Hat
Clothes
Small Mantle

Haste, Haste 2, Stop, Stabilize Time
Wave Fist, Secret Fist, Revive



OneHundredFists
Female
Aquarius
60
66
Geomancer
Yin Yang Magic
Damage Split
Magic Attack UP
Move+3

Slasher
Platinum Shield
Green Beret
Wizard Robe
Small Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic



Galkife
Male
Sagittarius
50
64
Squire
Throw
Counter Tackle
Martial Arts
Move+3


Genji Shield
Gold Helmet
Clothes
Sprint Shoes

Accumulate, Throw Stone, Heal
Bomb



Zeroroute
Female
Taurus
55
79
Priest
Summon Magic
Meatbone Slash
Short Charge
Swim

Healing Staff

Golden Hairpin
Black Costume
108 Gems

Cure 2, Cure 3, Raise, Protect, Protect 2, Wall, Esuna
Moogle, Golem, Carbunkle, Silf, Fairy, Lich

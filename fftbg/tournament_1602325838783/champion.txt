Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Gorgewall
Male
Capricorn
64
68
Bard
Elemental
Mana Shield
Maintenance
Swim

Bloody Strings

Green Beret
Clothes
Small Mantle

Angel Song, Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade, Hydra Pit
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard



KasugaiRoastedPeas
Male
Pisces
61
73
Mime

Mana Shield
Martial Arts
Move+3


Aegis Shield
Red Hood
Judo Outfit
Spike Shoes

Mimic




Lijarkh
Male
Capricorn
82
62
Monk
Yin Yang Magic
Counter Tackle
Equip Bow
Move+2

Gastrafitis

Red Hood
Mythril Vest
Bracer

Spin Fist, Wave Fist, Earth Slash, Chakra, Revive
Blind, Life Drain, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Dark Holy



Dem0nj0ns
Male
Leo
79
79
Lancer
Punch Art
Earplug
Equip Bow
Levitate

Gastrafitis
Genji Shield
Genji Helmet
Carabini Mail
Sprint Shoes

Level Jump8, Vertical Jump8
Spin Fist, Pummel, Wave Fist, Purification, Revive

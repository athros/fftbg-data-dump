Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DrAntiSocial
Male
Virgo
65
76
Knight
Elemental
Counter Magic
Long Status
Jump+1

Battle Axe
Gold Shield
Diamond Helmet
Linen Robe
Feather Mantle

Head Break, Shield Break, Weapon Break, Magic Break, Mind Break, Stasis Sword
Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



JethroThrul
Male
Aquarius
70
64
Priest
Talk Skill
Absorb Used MP
Dual Wield
Move+2

Morning Star
Morning Star
Holy Miter
Adaman Vest
Elf Mantle

Cure, Cure 4, Raise, Shell 2, Esuna
Invitation, Persuade, Praise, Solution, Insult, Refute



Thunderducker
Female
Capricorn
67
55
Wizard
Steal
MA Save
Magic Attack UP
Levitate

Thunder Rod

Thief Hat
Mystic Vest
Rubber Shoes

Fire, Bolt 2, Ice, Ice 2, Flare
Gil Taking



NicoSavoy
Female
Pisces
75
71
Dancer
Talk Skill
PA Save
Equip Shield
Jump+2

Star Bag
Crystal Shield
Red Hood
Clothes
Diamond Armlet

Slow Dance, Polka Polka, Last Dance, Obsidian Blade, Nether Demon
Invitation, Solution, Mimic Daravon, Refute

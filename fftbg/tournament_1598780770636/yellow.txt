Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Go2sleepTV
Male
Gemini
63
54
Ninja
Black Magic
Critical Quick
Equip Sword
Swim

Ragnarok
Flame Whip
Black Hood
Power Sleeve
Defense Ring

Bomb, Spear
Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Empower, Death



Vorap
Male
Virgo
74
61
Mediator
Item
Mana Shield
Defend
Waterbreathing

Mythril Gun

Leather Hat
Black Costume
Small Mantle

Persuade
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



Gorgewall
Male
Taurus
58
77
Ninja
Punch Art
HP Restore
Beastmaster
Teleport 2

Kunai
Koga Knife
Twist Headband
Power Sleeve
Bracer

Shuriken, Knife, Ninja Sword, Spear
Secret Fist, Purification, Revive, Seal Evil



Donovan
Male
Libra
55
45
Summoner
Battle Skill
Earplug
Halve MP
Fly

Thunder Rod

Red Hood
Chain Vest
Magic Ring

Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Fairy, Cyclops
Head Break, Power Break, Mind Break, Justice Sword, Surging Sword

Final Bets: red - 7 bets for 4,222G (73.7%, x0.36); blue - 6 bets for 1,503G (26.3%, x2.81)

red bets:
Zachara: 1,200G (28.4%, 135,200G)
BirbBrainsBot: 1,000G (23.7%, 157,719G)
gorgewall: 788G (18.7%, 788G)
CT_5_Holy: 620G (14.7%, 620G)
MemoriesofFinal: 284G (6.7%, 284G)
datadrivenbot: 200G (4.7%, 58,158G)
Xignealx: 130G (3.1%, 1,395G)

blue bets:
TheMurkGnome: 472G (31.4%, 472G)
DaveStrider55: 399G (26.5%, 399G)
getthemoneyz: 232G (15.4%, 1,778,819G)
KasugaiRoastedPeas: 200G (13.3%, 476G)
AllInBot: 100G (6.7%, 100G)
Vultuous: 100G (6.7%, 1,088G)

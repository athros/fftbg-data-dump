Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CorpusCav
Female
Virgo
48
60
Dancer
Talk Skill
Damage Split
Equip Shield
Lava Walking

Ryozan Silk
Gold Shield
Headgear
Silk Robe
Bracer

Witch Hunt, Slow Dance, Disillusion, Nameless Dance
Invitation, Threaten, Preach, Insult, Refute



Thunderducker
Male
Capricorn
57
57
Chemist
Charge
Abandon
Martial Arts
Jump+1



Twist Headband
Secret Clothes
Magic Ring

Potion, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down
Charge+5, Charge+7



Yangusburger
Female
Capricorn
68
52
Wizard
Steal
Distribute
Equip Shield
Move-MP Up

Mythril Knife
Genji Shield
Green Beret
Light Robe
Wizard Mantle

Fire, Bolt 2, Ice 2, Ice 3, Ice 4
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory



Baconbacon1207
Male
Scorpio
67
46
Time Mage
Charge
Critical Quick
Equip Knife
Move+2

Ninja Edge

Red Hood
Clothes
Defense Ring

Haste, Slow 2, Stop, Reflect, Demi 2, Meteor
Charge+3, Charge+5, Charge+10

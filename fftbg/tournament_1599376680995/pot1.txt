Final Bets: red - 12 bets for 6,481G (56.5%, x0.77); blue - 10 bets for 4,985G (43.5%, x1.30)

red bets:
Thyrandaal: 2,000G (30.9%, 126,105G)
latebit: 1,000G (15.4%, 14,290G)
Laserman1000: 962G (14.8%, 9,162G)
Raixelol: 524G (8.1%, 2,875G)
lowlf: 476G (7.3%, 2,794G)
krombobreaker: 449G (6.9%, 449G)
killth3kid: 250G (3.9%, 13,681G)
BooshPlays: 200G (3.1%, 1,037G)
datadrivenbot: 200G (3.1%, 55,819G)
Sietaha: 200G (3.1%, 3,863G)
Absalom_20: 120G (1.9%, 4,445G)
ImmortalMage: 100G (1.5%, 747G)

blue bets:
Greggernaut: 2,000G (40.1%, 32,882G)
ZPawZ: 1,184G (23.8%, 1,184G)
Yangusburger: 500G (10.0%, 5,199G)
getthemoneyz: 380G (7.6%, 1,840,568G)
BirbBrainsBot: 200G (4.0%, 200G)
PuzzleSecretary: 196G (3.9%, 920G)
velvet_muffins: 185G (3.7%, 185G)
holdenmagronik: 171G (3.4%, 1,559G)
CorpusCav: 100G (2.0%, 16,654G)
BartTradingCompany: 69G (1.4%, 691G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Genkidou
Female
Cancer
38
46
Samurai
White Magic
Counter Tackle
Defend
Teleport

Javelin

Crystal Helmet
Bronze Armor
Jade Armlet

Kiyomori, Masamune
Cure 2, Raise, Reraise, Regen, Protect 2, Esuna



ALY327
Female
Capricorn
46
54
Archer
Dance
PA Save
Beastmaster
Waterwalking

Night Killer
Buckler
Green Beret
Chain Vest
Magic Gauntlet

Charge+1, Charge+7
Polka Polka, Last Dance, Nether Demon



Zachara
Female
Leo
58
63
Monk
Time Magic
Earplug
Equip Armor
Retreat



Flash Hat
Crystal Mail
Red Shoes

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive
Stop, Immobilize, Quick, Stabilize Time, Galaxy Stop



OneHundredFists
Female
Cancer
56
69
Oracle
Draw Out
Distribute
Dual Wield
Waterwalking

Musk Rod
Cypress Rod
Red Hood
White Robe
Sprint Shoes

Poison, Silence Song, Blind Rage, Confusion Song
Asura, Kiyomori

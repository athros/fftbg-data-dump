Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kohlingen
Female
Aquarius
42
58
Wizard
Basic Skill
Dragon Spirit
Beastmaster
Jump+3

Cute Bag

Headgear
Power Sleeve
Magic Ring

Fire, Fire 4, Ice 2, Empower
Accumulate, Dash, Heal, Cheer Up, Scream



Old Overholt
Male
Gemini
66
59
Knight
Summon Magic
Damage Split
Long Status
Move+1

Giant Axe
Buckler
Barbuta
Crystal Mail
Germinas Boots

Shield Break, Power Break, Mind Break
Moogle, Salamander, Lich



Lythe Caraker
Monster
Virgo
77
69
Red Panther










CorpusCav
Female
Aquarius
74
76
Wizard
White Magic
Counter Magic
Attack UP
Jump+1

Wizard Rod

Green Beret
Judo Outfit
Small Mantle

Fire, Fire 2, Fire 3, Bolt 3, Ice 3, Frog
Cure 2, Cure 3, Cure 4, Reraise, Regen, Protect, Shell 2, Wall, Esuna, Holy, Magic Barrier

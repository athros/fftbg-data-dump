Player: !White
Team: White Team
Palettes: White/Blue



Jeeboheebo
Female
Libra
43
62
Wizard
Summon Magic
Caution
Short Charge
Retreat

Flame Rod

Leather Hat
Judo Outfit
N-Kai Armlet

Fire 2, Bolt, Bolt 2, Bolt 4, Empower, Death, Flare
Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Salamander, Fairy



Error72
Female
Taurus
53
72
Oracle
Jump
Brave Save
Doublehand
Move+3

Madlemgen

Leather Hat
Chameleon Robe
Sprint Shoes

Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep
Level Jump5, Vertical Jump8



Dogsandcatsand
Female
Capricorn
65
66
Wizard
Basic Skill
Parry
Defense UP
Jump+2

Dragon Rod

Golden Hairpin
Wizard Robe
108 Gems

Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 4, Flare
Accumulate, Yell, Ultima



Fenaen
Female
Taurus
79
49
Squire
Yin Yang Magic
Absorb Used MP
Equip Gun
Lava Walking

Blaze Gun
Round Shield
Gold Helmet
Wizard Outfit
Wizard Mantle

Heal, Tickle, Yell, Cheer Up, Wish, Scream
Life Drain, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze

Final Bets: red - 14 bets for 6,360G (69.8%, x0.43); green - 4 bets for 2,752G (30.2%, x2.31)

red bets:
Thyrandaal: 2,000G (31.4%, 124,965G)
thunderducker: 1,200G (18.9%, 8,901G)
NicoSavoy: 1,000G (15.7%, 6,050G)
Raixelol: 524G (8.2%, 2,215G)
Sietaha: 300G (4.7%, 3,346G)
killth3kid: 250G (3.9%, 14,273G)
BirbBrainsBot: 225G (3.5%, 225G)
PuzzleSecretary: 221G (3.5%, 221G)
Yangusburger: 200G (3.1%, 4,419G)
datadrivenbot: 200G (3.1%, 55,767G)
CorpusCav: 100G (1.6%, 15,815G)
DeathTaxesAndAnime: 100G (1.6%, 9,985G)
getthemoneyz: 20G (0.3%, 1,840,593G)
alcibiadeez: 20G (0.3%, 663G)

green bets:
SkylerBunny: 2,000G (72.7%, 20,768G)
Treafa: 388G (14.1%, 1,131G)
Segomod: 300G (10.9%, 7,426G)
BartTradingCompany: 64G (2.3%, 647G)

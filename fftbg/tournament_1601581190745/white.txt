Player: !White
Team: White Team
Palettes: White/Blue



Exigency 44
Male
Scorpio
63
56
Monk
Item
Catch
Defend
Move-MP Up



Feather Hat
Earth Clothes
Jade Armlet

Spin Fist, Wave Fist, Chakra, Revive
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down



Dogsandcatsand
Female
Aries
66
37
Wizard
Summon Magic
Counter
Equip Gun
Move+2

Battle Folio

Black Hood
Linen Robe
Feather Mantle

Bolt, Bolt 3, Ice, Ice 4, Frog, Death
Moogle, Shiva, Carbunkle, Salamander, Lich



Lyonslegacy
Male
Libra
63
69
Ninja
Battle Skill
Counter Flood
Attack UP
Move+1

Kunai
Short Edge
Triangle Hat
Wizard Outfit
Dracula Mantle

Shuriken, Wand
Magic Break, Speed Break, Mind Break, Stasis Sword



Tripaplex
Female
Libra
44
65
Calculator
Lucavi Skill
Regenerator
Defend
Waterbreathing

Flame Rod
Buckler
Diamond Helmet
Chameleon Robe
N-Kai Armlet

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima

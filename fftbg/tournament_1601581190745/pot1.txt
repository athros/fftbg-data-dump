Final Bets: red - 16 bets for 35,739G (76.8%, x0.30); blue - 22 bets for 10,793G (23.2%, x3.31)

red bets:
E_Ballard: 29,383G (82.2%, 29,383G)
scsuperstar: 1,939G (5.4%, 1,939G)
Nizaha: 636G (1.8%, 5,401G)
lowlf: 628G (1.8%, 18,231G)
JonnyCue: 500G (1.4%, 15,474G)
escobro: 500G (1.4%, 8,716G)
DavenIII: 487G (1.4%, 4,872G)
tinytittyfiend: 336G (0.9%, 336G)
Smugzug: 336G (0.9%, 993G)
king_smashington: 272G (0.8%, 272G)
lotharek: 200G (0.6%, 2,552G)
daveb_: 122G (0.3%, 5,174G)
maakur_: 100G (0.3%, 4,862G)
heropong: 100G (0.3%, 2,577G)
GeNoFPaniC: 100G (0.3%, 1,337G)
Thyrandaal: 100G (0.3%, 168,690G)

blue bets:
latebit: 1,079G (10.0%, 107,980G)
CosmicTactician: 1,000G (9.3%, 3,230G)
BirbBrainsBot: 1,000G (9.3%, 155,428G)
Sairentozon7: 1,000G (9.3%, 44,611G)
dogsandcatsand: 1,000G (9.3%, 6,682G)
Lord_Gwarth: 770G (7.1%, 770G)
seppu777: 628G (5.8%, 628G)
helpimabug: 580G (5.4%, 580G)
Snowfats: 500G (4.6%, 13,624G)
krombobreaker: 500G (4.6%, 24,626G)
Cryptopsy70: 404G (3.7%, 2,345G)
thunderducker: 400G (3.7%, 3,462G)
getthemoneyz: 348G (3.2%, 2,130,952G)
Grasnikk: 280G (2.6%, 280G)
2b_yorha_b: 240G (2.2%, 240G)
AllInBot: 200G (1.9%, 200G)
datadrivenbot: 200G (1.9%, 80,224G)
HorusTaurus: 200G (1.9%, 5,420G)
AbandonedHall: 156G (1.4%, 156G)
IanTheWoodchuck: 108G (1.0%, 108G)
OneHundredFists: 100G (0.9%, 10,187G)
yagyuudarkness: 100G (0.9%, 1,292G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Seaweed B
Male
Virgo
71
37
Lancer
Battle Skill
Abandon
Attack UP
Waterbreathing

Obelisk
Crystal Shield
Circlet
Plate Mail
Sprint Shoes

Level Jump2, Vertical Jump4
Shield Break, Weapon Break, Mind Break



Thunderducker
Male
Capricorn
46
75
Lancer
Elemental
Mana Shield
Equip Sword
Move+2

Broad Sword
Crystal Shield
Barbuta
Gold Armor
Genji Gauntlet

Level Jump5, Vertical Jump8
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Vorackgriever
Female
Libra
66
57
Oracle
Steal
Critical Quick
Defend
Jump+2

Battle Bamboo

Feather Hat
Silk Robe
Diamond Armlet

Blind, Poison, Spell Absorb, Pray Faith, Blind Rage, Paralyze, Dark Holy
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



HASTERIOUS
Female
Aquarius
62
50
Oracle
Elemental
MP Restore
Equip Gun
Swim

Battle Folio

Flash Hat
Earth Clothes
Genji Gauntlet

Blind, Spell Absorb, Life Drain, Confusion Song, Dispel Magic
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

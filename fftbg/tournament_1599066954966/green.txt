Player: !Green
Team: Green Team
Palettes: Green/White



Kronikle
Male
Virgo
59
37
Knight
Talk Skill
Dragon Spirit
Long Status
Move-HP Up

Chaos Blade
Gold Shield
Genji Helmet
Bronze Armor
Small Mantle

Armor Break, Weapon Break, Magic Break, Mind Break
Persuade, Praise, Threaten, Solution



Run With Stone GUNs
Female
Sagittarius
80
78
Calculator
Black Magic
Mana Shield
Equip Axe
Ignore Height

Healing Staff

Black Hood
Black Costume
Vanish Mantle

CT, Height, Prime Number, 3
Fire 3, Fire 4, Bolt 4, Ice 4, Frog, Death



Yangusburger
Monster
Scorpio
80
50
Great Malboro










NovaKnight21
Female
Libra
57
52
Oracle
Basic Skill
Abandon
Magic Defense UP
Move-MP Up

Papyrus Codex

Twist Headband
Adaman Vest
Germinas Boots

Poison, Spell Absorb, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Dash, Yell, Cheer Up

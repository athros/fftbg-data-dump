Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zagorsek
Female
Gemini
60
57
Summoner
Dance
Damage Split
Doublehand
Ignore Height

Rainbow Staff

Green Beret
Black Robe
Cursed Ring

Moogle, Shiva, Carbunkle, Bahamut, Leviathan, Silf, Lich, Cyclops
Wiznaibus, Disillusion, Last Dance, Void Storage, Nether Demon



Thyrandaal
Male
Aquarius
51
51
Squire
Steal
Brave Save
Concentrate
Move-HP Up

Broad Sword
Genji Shield
Ribbon
Diamond Armor
Setiemson

Accumulate, Heal, Tickle, Wish
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



Mesmaster
Female
Pisces
55
45
Wizard
Draw Out
Auto Potion
Equip Sword
Jump+1

Long Sword

Twist Headband
Mystic Vest
Dracula Mantle

Fire, Fire 2, Fire 3, Bolt 4, Ice 2
Asura, Kiyomori, Kikuichimoji



Sinnyil2
Male
Cancer
61
43
Squire
Item
Counter
Throw Item
Move-HP Up

Nagrarock
Gold Shield
Thief Hat
Gold Armor
Dracula Mantle

Throw Stone, Heal, Cheer Up
Potion, Hi-Potion, Echo Grass, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



Jeeboheebo
Female
Pisces
72
55
Knight
Time Magic
HP Restore
Doublehand
Move+3

Battle Axe

Grand Helmet
Mythril Armor
Diamond Armlet

Magic Break, Power Break
Haste, Immobilize, Quick, Demi, Demi 2, Stabilize Time



Ayeayex3
Female
Capricorn
43
54
Geomancer
Black Magic
Regenerator
Equip Polearm
Move-HP Up

Holy Lance
Diamond Shield
Headgear
Chameleon Robe
N-Kai Armlet

Pitfall, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bolt 2, Ice, Ice 3



Kellios11
Male
Scorpio
77
41
Chemist
Draw Out
Dragon Spirit
Equip Knife
Move+2

Mythril Knife

Holy Miter
Mystic Vest
Leather Mantle

Potion, Ether, Hi-Ether, Maiden's Kiss, Soft
Koutetsu, Murasame, Heaven's Cloud



Laserman1000
Male
Scorpio
71
69
Mediator
Yin Yang Magic
Brave Save
Dual Wield
Swim

Papyrus Codex
Papyrus Codex
Cachusha
Power Sleeve
Red Shoes

Invitation, Persuade, Preach, Death Sentence, Negotiate, Refute
Blind, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Vorap
Male
Sagittarius
43
57
Bard
Item
Hamedo
Martial Arts
Move+3

Windslash Bow

Cachusha
Wizard Outfit
Dracula Mantle

Angel Song, Life Song, Nameless Song, Diamond Blade
Potion, Ether, Antidote, Eye Drop, Soft, Phoenix Down



Latebit
Female
Cancer
54
50
Thief
Draw Out
Regenerator
Doublehand
Retreat

Spell Edge

Green Beret
Chain Vest
N-Kai Armlet

Steal Heart, Steal Shield, Steal Weapon, Steal Status
Koutetsu, Heaven's Cloud, Muramasa



Phi Sig
Male
Aries
73
52
Archer
Elemental
Faith Save
Doublehand
Waterbreathing

Bow Gun

Feather Hat
Mystic Vest
Germinas Boots

Charge+1, Charge+4, Charge+5, Charge+10
Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



SQUiDSQUARKLIN
Female
Gemini
48
44
Calculator
White Magic
Parry
Equip Knife
Waterwalking

Wizard Rod

Thief Hat
Mystic Vest
Defense Armlet

CT, Height, 5, 3
Raise 2, Regen, Protect 2, Shell, Shell 2, Esuna

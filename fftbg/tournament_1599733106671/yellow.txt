Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Male
Cancer
66
57
Thief
Basic Skill
Distribute
Equip Gun
Teleport

Mythril Gun

Triangle Hat
Adaman Vest
Angel Ring

Steal Accessory, Steal Status, Leg Aim
Accumulate, Throw Stone, Yell, Fury, Wish, Scream, Ultima



Gorgewall
Male
Capricorn
78
77
Time Mage
Black Magic
Hamedo
Equip Gun
Ignore Terrain

Glacier Gun

Leather Hat
Mythril Vest
Rubber Shoes

Slow 2, Reflect, Stabilize Time
Fire, Fire 3, Ice, Empower, Frog



Jeeboheebo
Male
Aquarius
75
79
Mime

Counter
Equip Shield
Jump+1


Genji Shield
Cross Helmet
Adaman Vest
Diamond Armlet

Mimic




Nolana
Female
Capricorn
72
74
Calculator
White Magic
Counter Flood
Sicken
Levitate

Rod

Red Hood
Mystic Vest
Defense Ring

CT, Height, 3
Cure 4, Raise, Protect 2, Shell, Shell 2, Esuna

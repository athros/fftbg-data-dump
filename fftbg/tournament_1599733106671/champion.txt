Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



ValensEXP
Male
Virgo
40
71
Knight
Draw Out
Catch
Attack UP
Levitate

Ragnarok
Flame Shield
Diamond Helmet
Bronze Armor
Battle Boots

Armor Break, Shield Break, Weapon Break, Magic Break, Night Sword
Asura, Bizen Boat, Kiyomori, Kikuichimoji



Sairentozon7
Female
Cancer
59
80
Summoner
Punch Art
Parry
Magic Defense UP
Teleport

Rod

Headgear
White Robe
Cursed Ring

Moogle, Titan, Carbunkle, Bahamut, Odin, Cyclops
Spin Fist, Purification, Chakra, Revive



MemoriesofFinal
Male
Scorpio
74
59
Thief
Battle Skill
Counter
Equip Sword
Teleport

Save the Queen

Twist Headband
Earth Clothes
Feather Boots

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Leg Aim
Armor Break, Shield Break, Magic Break, Speed Break, Night Sword



Seaweed B
Male
Capricorn
77
69
Archer
Time Magic
Absorb Used MP
Doublehand
Move+3

Snipe Bow

Green Beret
Earth Clothes
Leather Mantle

Charge+1, Charge+3, Charge+4
Haste, Immobilize, Demi, Stabilize Time, Galaxy Stop

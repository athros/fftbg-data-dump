Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Resjudicata3
Female
Capricorn
78
77
Thief
Yin Yang Magic
Regenerator
Halve MP
Move-MP Up

Mythril Sword

Black Hood
Mythril Vest
Small Mantle

Steal Weapon, Arm Aim, Leg Aim
Blind, Doubt Faith, Zombie, Foxbird, Dispel Magic, Sleep



Lowlf
Female
Scorpio
81
44
Summoner
Dance
Regenerator
Equip Shield
Waterwalking

Ice Rod
Diamond Shield
Headgear
Power Sleeve
Feather Mantle

Moogle, Shiva, Ifrit, Titan, Bahamut, Odin, Leviathan, Fairy, Lich, Cyclops
Polka Polka, Void Storage, Dragon Pit



SuppleThink
Female
Aquarius
78
72
Priest
Item
Parry
Magic Attack UP
Move+3

Oak Staff

Golden Hairpin
White Robe
N-Kai Armlet

Cure, Cure 2, Cure 3, Protect, Shell 2, Esuna
X-Potion, Ether, Antidote, Echo Grass, Remedy, Phoenix Down



CorpusCav
Female
Pisces
43
51
Chemist
Jump
Auto Potion
Equip Shield
Levitate

Mythril Gun
Flame Shield
Black Hood
Earth Clothes
108 Gems

Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water
Level Jump4, Vertical Jump6

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ayeayex3
Male
Sagittarius
76
49
Knight
Charge
Counter Flood
Short Status
Jump+1

Defender
Platinum Shield
Gold Helmet
Leather Armor
Leather Mantle

Head Break, Armor Break, Weapon Break, Magic Break, Mind Break, Dark Sword, Surging Sword
Charge+1, Charge+3, Charge+5



Nifboy
Male
Capricorn
42
43
Mediator
Yin Yang Magic
Abandon
Doublehand
Ignore Height

Romanda Gun

Red Hood
Wizard Outfit
Spike Shoes

Persuade, Threaten, Solution, Insult, Refute
Blind, Foxbird, Dispel Magic, Paralyze, Sleep



Zachara
Male
Aries
49
49
Mediator
Yin Yang Magic
Hamedo
Dual Wield
Waterbreathing

Madlemgen
Bestiary
Flash Hat
Brigandine
Magic Gauntlet

Preach, Death Sentence, Refute
Poison, Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic



Khelor
Male
Sagittarius
51
60
Mime

Mana Shield
Short Status
Teleport



Green Beret
Mythril Vest
Power Wrist

Mimic


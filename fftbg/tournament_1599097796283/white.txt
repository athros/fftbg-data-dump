Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Aquarius
49
42
Knight
Black Magic
Distribute
Attack UP
Move+3

Hydra Bag
Diamond Shield
Genji Helmet
Linen Cuirass
Cursed Ring

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword
Bolt 4, Ice, Ice 2, Flare



Macradarkstar
Female
Sagittarius
71
66
Monk
Draw Out
Sunken State
Magic Attack UP
Ignore Terrain



Leather Hat
Secret Clothes
Diamond Armlet

Secret Fist, Purification, Revive
Bizen Boat, Heaven's Cloud, Kiyomori, Chirijiraden



Ruleof5
Female
Cancer
60
43
Archer
Item
Sunken State
Martial Arts
Move-HP Up


Ice Shield
Barette
Leather Outfit
Magic Ring

Charge+1, Charge+7, Charge+20
X-Potion, Hi-Ether, Holy Water, Phoenix Down



Old Overholt
Male
Aquarius
78
72
Thief
Sing
PA Save
Maintenance
Jump+2

Blind Knife

Leather Hat
Mystic Vest
Dracula Mantle

Gil Taking, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Cheer Song, Battle Song, Nameless Song, Space Storage

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Zepharoth89
Female
Sagittarius
58
57
Mediator
Steal
Critical Quick
Equip Armor
Fly

Mythril Gun

Cross Helmet
Leather Armor
Germinas Boots

Praise, Death Sentence, Mimic Daravon, Refute
Gil Taking, Steal Shield, Steal Weapon, Leg Aim



CapnChaos12
Female
Capricorn
74
62
Summoner
Dance
Hamedo
Short Charge
Fly

Thunder Rod

Black Hood
Linen Robe
Red Shoes

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Silf
Wiznaibus, Last Dance, Nether Demon



Estan AD
Female
Capricorn
68
47
Summoner
Item
Blade Grasp
Beastmaster
Jump+2

Wizard Rod

Twist Headband
Light Robe
Diamond Armlet

Ramuh, Ifrit, Carbunkle, Lich
Potion, Eye Drop, Maiden's Kiss, Phoenix Down



Nekojin
Male
Virgo
62
43
Ninja
Battle Skill
Dragon Spirit
Equip Knife
Fly

Short Edge
Poison Rod
Golden Hairpin
Mythril Vest
Bracer

Shuriken, Bomb, Dictionary
Shield Break, Magic Break, Speed Break, Power Break

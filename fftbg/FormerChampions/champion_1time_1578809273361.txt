Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Willjin
Male
Virgo
65
53
Knight
Basic Skill
Parry
Equip Bow
Move+3

Save the Queen

Diamond Helmet
White Robe
Feather Boots

Magic Break, Speed Break
Accumulate, Throw Stone, Heal



Beowulfrulez
Male
Libra
67
52
Archer
Elemental
Blade Grasp
Short Charge
Swim

Ice Bow

Golden Hairpin
Power Sleeve
Salty Rage

Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Gut922
Male
Virgo
41
51
Geomancer
Time Magic
Blade Grasp
Equip Axe
Jump+2

Slasher
Genji Shield
Flash Hat
White Robe
Battle Boots

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Float, Demi 2, Stabilize Time



Gavriel
Male
Sagittarius
65
38
Samurai
Black Magic
Damage Split
Attack UP
Lava Walking

Muramasa

Mythril Helmet
Leather Armor
Dracula Mantle

Murasame, Heaven's Cloud, Muramasa
Bolt 3, Bolt 4, Ice 4

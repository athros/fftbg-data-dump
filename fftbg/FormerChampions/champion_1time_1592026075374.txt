Player: !zChamp
Team: Champion Team
Palettes: Black/Red



CosmicTactician
Female
Capricorn
68
40
Archer
Dance
Catch
Equip Armor
Move-HP Up

Hunting Bow
Round Shield
Golden Hairpin
White Robe
Defense Ring

Charge+1, Charge+3
Wiznaibus, Disillusion, Last Dance, Nether Demon



Prince Rogers Nelson
Male
Libra
60
56
Knight
Talk Skill
Counter
Magic Defense UP
Move-HP Up

Ice Brand
Venetian Shield
Cross Helmet
Linen Cuirass
Elf Mantle

Head Break, Shield Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Invitation, Preach, Solution, Mimic Daravon, Rehabilitate



Skillomono
Male
Aquarius
77
65
Knight
Charge
Regenerator
Defend
Retreat

Defender
Platinum Shield
Circlet
Linen Cuirass
Vanish Mantle

Magic Break, Power Break, Justice Sword
Charge+1, Charge+3, Charge+4, Charge+5, Charge+10



Galkife
Monster
Aries
78
38
Serpentarius








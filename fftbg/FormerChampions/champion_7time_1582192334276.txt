Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



G Regulate
Male
Capricorn
63
75
Geomancer
Summon Magic
Abandon
Secret Hunt
Teleport 2

Ancient Sword
Buckler
Thief Hat
Silk Robe
Feather Boots

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Gusty Wind, Lava Ball
Moogle, Ifrit, Golem, Odin, Lich



Ominnous
Female
Pisces
57
68
Summoner
Jump
Catch
Short Charge
Jump+2

Oak Staff

Triangle Hat
Adaman Vest
Feather Boots

Moogle, Shiva, Ifrit, Golem, Leviathan
Level Jump2, Vertical Jump7



Laserman1000
Female
Gemini
44
73
Thief
White Magic
Sunken State
Equip Gun
Levitate

Glacier Gun

Black Hood
Judo Outfit
Setiemson

Steal Heart, Steal Weapon, Steal Accessory, Arm Aim
Cure, Raise, Reraise, Protect 2, Wall, Esuna



BobertLocke
Female
Cancer
57
59
Oracle
Summon Magic
Regenerator
Equip Axe
Move-MP Up

Wizard Staff

Triangle Hat
Brigandine
Reflect Ring

Blind, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Moogle, Carbunkle

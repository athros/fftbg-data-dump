Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Oobs56
Female
Virgo
81
63
Mediator
Summon Magic
PA Save
Long Status
Move+3

Stone Gun

Golden Hairpin
Leather Outfit
Magic Ring

Invitation, Persuade, Praise, Preach, Death Sentence, Insult, Negotiate, Refute
Moogle, Golem, Fairy



ZombiFrancis
Female
Sagittarius
70
73
Archer
Steal
Abandon
Defend
Lava Walking

Windslash Bow

Red Hood
Judo Outfit
108 Gems

Charge+5
Steal Armor, Steal Shield, Steal Weapon



NeoKevlar
Monster
Scorpio
64
52
Steel Giant










Mesmaster
Female
Virgo
70
44
Wizard
Math Skill
Mana Shield
Equip Sword
Jump+1

Koutetsu Knife

Red Hood
Clothes
Magic Gauntlet

Fire, Fire 2, Bolt, Bolt 2, Ice, Ice 3, Ice 4
CT, 4, 3

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Rozina
Female
Cancer
75
76
Oracle
Math Skill
Hamedo
Magic Defense UP
Jump+2

Wizard Staff

Green Beret
Judo Outfit
Defense Armlet

Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify
CT, Prime Number, 5, 4



Willjin
Monster
Pisces
59
79
Mindflayer










Carla
Female
Aries
60
50
Ninja
Draw Out
Absorb Used MP
Concentrate
Jump+1

Morning Star
Flail
Headgear
Mythril Vest
Magic Ring

Shuriken, Staff
Bizen Boat, Muramasa, Kikuichimoji



Doria
Female
Scorpio
52
53
Summoner
Yin Yang Magic
HP Restore
Equip Axe
Move+3

Giant Axe

Holy Miter
Wizard Outfit
Feather Boots

Moogle, Ramuh, Titan, Golem, Carbunkle, Odin, Silf, Cyclops
Blind, Poison, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Dark Holy



Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Hewf
Male
Aquarius
57
76
Archer
Jump
Counter Tackle
Dual Wield
Swim

Romanda Gun
Blast Gun
Leather Hat
Secret Clothes
Rubber Shoes

Charge+3, Charge+4, Charge+7
Level Jump2, Vertical Jump3



ShintaroNayaka
Monster
Gemini
66
48
Black Chocobo










Bryan792
Female
Taurus
54
43
Mediator
Time Magic
Abandon
Long Status
Move+3

Bestiary

Holy Miter
Brigandine
Angel Ring

Invitation, Preach, Solution, Negotiate, Mimic Daravon, Refute
Demi 2, Stabilize Time



Hzor
Female
Scorpio
67
60
Lancer
Item
Speed Save
Throw Item
Move+1

Holy Lance
Genji Shield
Genji Helmet
Black Robe
N-Kai Armlet

Level Jump8, Vertical Jump7
Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down

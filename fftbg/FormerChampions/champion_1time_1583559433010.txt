Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ANFz
Male
Pisces
74
44
Geomancer
Draw Out
Absorb Used MP
Concentrate
Waterbreathing

Giant Axe
Buckler
Leather Hat
Secret Clothes
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud



DeliciousSub
Male
Capricorn
45
72
Ninja
Time Magic
HP Restore
Equip Shield
Move+2

Spell Edge
Round Shield
Headgear
Brigandine
Bracer

Bomb
Haste 2, Stop, Immobilize, Float, Quick



Deathmaker06
Male
Gemini
39
46
Geomancer
Sing
Earplug
Equip Polearm
Retreat

Gungnir
Bronze Shield
Leather Hat
Wizard Outfit
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind
Angel Song, Battle Song, Diamond Blade



Genoguy
Female
Gemini
43
45
Mime

Damage Split
Secret Hunt
Move-MP Up



Iron Helmet
Earth Clothes
Magic Ring

Mimic


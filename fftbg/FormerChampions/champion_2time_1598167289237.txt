Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



StealthModeLocke
Male
Capricorn
66
67
Thief
Draw Out
Counter
Dual Wield
Ignore Terrain

Air Knife
Sleep Sword
Red Hood
Secret Clothes
Leather Mantle

Steal Shield, Steal Status
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



DeathTaxesAndAnime
Female
Gemini
55
71
Wizard
Math Skill
Distribute
Equip Armor
Ignore Terrain

Dragon Rod

Barbuta
Mythril Armor
Sprint Shoes

Fire, Fire 2, Fire 3, Bolt 3, Ice 3, Ice 4, Frog
CT, 5, 4



Roofiepops
Male
Capricorn
40
58
Oracle
Steal
Arrow Guard
Short Charge
Jump+3

Ivory Rod

Triangle Hat
Earth Clothes
Rubber Shoes

Blind, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dark Holy
Arm Aim, Leg Aim



Neerrm
Female
Scorpio
71
80
Geomancer
Dance
Abandon
Equip Sword
Teleport

Koutetsu Knife
Genji Shield
Twist Headband
Clothes
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Witch Hunt, Polka Polka, Nameless Dance, Dragon Pit

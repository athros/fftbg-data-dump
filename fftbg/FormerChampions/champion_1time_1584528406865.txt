Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Grininda
Male
Sagittarius
64
80
Wizard
Elemental
Speed Save
Secret Hunt
Teleport

Poison Rod

Green Beret
Mystic Vest
Power Wrist

Fire, Fire 2, Ice 2, Ice 4, Empower
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



MalakoFox
Male
Aries
48
53
Mime

Regenerator
Equip Armor
Jump+3



Mythril Helmet
Leather Armor
Genji Gauntlet

Mimic




Satystar
Male
Aries
42
63
Mediator
Basic Skill
Parry
Halve MP
Lava Walking

Blaze Gun

Triangle Hat
Light Robe
Small Mantle

Praise, Preach, Death Sentence, Negotiate, Refute, Rehabilitate
Heal, Yell, Wish



HaychDub
Male
Cancer
79
60
Lancer
Sing
PA Save
Doublehand
Move+3

Ivory Rod

Mythril Helmet
Maximillian
Sprint Shoes

Level Jump8, Vertical Jump4
Battle Song, Sky Demon

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Roofiepops
Female
Capricorn
59
74
Oracle
Jump
Counter
Short Status
Move+3

Battle Bamboo

Golden Hairpin
Wizard Robe
Defense Armlet

Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze
Level Jump5, Vertical Jump8



Dasutin23
Male
Leo
48
41
Calculator
Limit
HP Restore
Martial Arts
Move-MP Up

Battle Folio
Ice Shield
Circlet
Carabini Mail
Genji Gauntlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Killth3kid
Male
Cancer
46
51
Chemist
Black Magic
Auto Potion
Sicken
Move+1

Panther Bag

Green Beret
Earth Clothes
Diamond Armlet

Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Fire, Fire 2, Fire 3, Fire 4, Bolt 2, Ice 2, Ice 3, Empower



OpHendoslice
Male
Pisces
51
63
Ninja
Yin Yang Magic
Faith Save
Short Status
Move+2

Ninja Edge
Mythril Knife
Black Hood
Leather Outfit
108 Gems

Shuriken, Bomb
Blind, Spell Absorb, Pray Faith, Silence Song, Blind Rage, Dispel Magic

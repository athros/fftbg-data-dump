Player: !zChamp
Team: Champion Team
Palettes: Green/White



Biffcake01
Female
Virgo
73
75
Priest
Time Magic
Parry
Equip Gun
Jump+1

Blast Gun

Leather Hat
Silk Robe
Magic Ring

Raise, Reraise, Esuna
Haste, Haste 2, Slow, Stabilize Time



GatsbysGhost
Female
Gemini
51
61
Lancer
Punch Art
Counter
Sicken
Lava Walking

Obelisk
Diamond Shield
Bronze Helmet
White Robe
Genji Gauntlet

Level Jump8, Vertical Jump6
Secret Fist



ArchKnightX
Female
Sagittarius
72
41
Knight
Item
Brave Save
Defend
Retreat

Iron Sword
Crystal Shield
Crystal Helmet
Linen Robe
N-Kai Armlet

Head Break, Mind Break, Surging Sword
Hi-Potion, Echo Grass, Holy Water, Remedy, Phoenix Down



Moocaotao
Female
Taurus
70
44
Knight
Yin Yang Magic
Earplug
Sicken
Jump+2

Coral Sword
Venetian Shield
Circlet
Black Robe
Sprint Shoes

Magic Break, Speed Break, Mind Break, Dark Sword
Blind, Poison, Life Drain, Blind Rage, Foxbird, Confusion Song, Sleep, Petrify

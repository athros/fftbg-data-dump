Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Sunny
Female
Virgo
77
60
Mediator
Summon Magic
Parry
Equip Knife
Lava Walking

Air Knife

Red Hood
Leather Vest
Sprint Shoes

Praise, Mimic Daravon, Refute
Moogle, Golem, Bahamut, Odin, Leviathan, Silf, Fairy



Roberto
Male
Gemini
74
55
Lancer
Yin Yang Magic
Dragon Spirit
Equip Armor
Fly

Obelisk
Crystal Shield
Iron Helmet
Secret Clothes
Elf Mantle

Level Jump8, Vertical Jump2
Poison, Life Drain, Pray Faith, Silence Song, Blind Rage, Foxbird, Paralyze



Everett
Male
Sagittarius
74
57
Monk
Elemental
Faith Up
Doublehand
Lava Walking



Leather Hat
Mythril Vest
Power Wrist

Purification, Chakra, Revive
Pitfall, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Helga
Female
Cancer
78
62
Wizard
Steal
Parry
Magic Defense UP
Levitate

Flame Rod

Triangle Hat
Leather Outfit
Angel Ring

Fire 2, Fire 4, Bolt, Bolt 4, Flare
Steal Helmet, Steal Accessory



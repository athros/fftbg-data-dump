Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Roofiepops
Male
Taurus
78
80
Samurai
Time Magic
MA Save
Equip Armor
Lava Walking

Muramasa

Red Hood
Crystal Mail
Sprint Shoes

Koutetsu, Bizen Boat, Heaven's Cloud
Haste 2, Slow, Slow 2, Stop, Float, Demi 2, Stabilize Time



Pplvee1
Monster
Libra
54
72
Ghost










DavenIII
Male
Virgo
54
53
Thief
Throw
Parry
Equip Armor
Waterwalking

Mage Masher

Genji Helmet
Crystal Mail
Magic Gauntlet

Steal Shield, Steal Status, Leg Aim
Shuriken, Staff, Dictionary



SomthingMore
Male
Virgo
66
75
Mediator
White Magic
Counter Tackle
Dual Wield
Jump+3

Stone Gun
Blast Gun
Golden Hairpin
Clothes
Feather Boots

Praise, Threaten, Solution, Death Sentence, Mimic Daravon, Refute
Cure, Cure 3, Protect, Protect 2, Shell 2, Esuna, Holy

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Actual JP
Male
Sagittarius
69
43
Lancer
Sing
Critical Quick
Secret Hunt
Jump+2

Holy Lance
Bronze Shield
Circlet
Mythril Armor
Small Mantle

Level Jump5, Vertical Jump8
Magic Song, Nameless Song, Diamond Blade, Sky Demon



Sharosa
Male
Pisces
67
48
Wizard
Elemental
Counter Flood
Long Status
Jump+1

Thunder Rod

Holy Miter
Silk Robe
Power Wrist

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Empower, Flare
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



ALY327
Female
Libra
70
77
Geomancer
Draw Out
Distribute
Halve MP
Move+3

Rune Blade
Flame Shield
Twist Headband
Adaman Vest
Salty Rage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard
Bizen Boat, Kikuichimoji



Run With Stone GUNs
Male
Aries
70
57
Bard
Jump
Parry
Doublehand
Move+3

Bloody Strings

Black Hood
Rubber Costume
Sprint Shoes

Angel Song, Life Song, Cheer Song, Battle Song, Diamond Blade, Sky Demon
Level Jump2, Vertical Jump7

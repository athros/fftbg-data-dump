Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Denamda
Male
Aquarius
80
67
Chemist
Summon Magic
MP Restore
Halve MP
Levitate

Cute Bag

Holy Miter
Rubber Costume
Dracula Mantle

Potion, X-Potion, Ether, Hi-Ether, Soft, Remedy
Moogle, Shiva, Titan, Leviathan, Salamander, Fairy



Gelwain
Female
Leo
63
54
Time Mage
Elemental
Meatbone Slash
Attack UP
Teleport

Oak Staff

Green Beret
Wizard Robe
Angel Ring

Haste 2, Slow 2, Quick, Stabilize Time
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard



Rexamajinx
Female
Sagittarius
79
38
Knight
Time Magic
HP Restore
Sicken
Move+3

Defender

Bronze Helmet
White Robe
Angel Ring

Weapon Break, Speed Break, Mind Break
Haste, Slow 2, Immobilize, Float, Reflect



Lanryte
Female
Cancer
52
62
Thief
Time Magic
Counter Flood
Defense UP
Jump+1

Air Knife

Black Hood
Mythril Vest
Spike Shoes

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Haste, Haste 2, Stop, Float, Quick, Demi

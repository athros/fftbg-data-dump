Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lijarkh
Male
Taurus
49
57
Monk
Yin Yang Magic
Abandon
Dual Wield
Move+1



Twist Headband
Leather Outfit
Power Wrist

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra
Poison, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic



ShintaroNayaka
Male
Aries
69
50
Knight
Punch Art
Counter
Concentrate
Swim

Defender
Flame Shield
Circlet
Platinum Armor
Vanish Mantle

Magic Break, Mind Break, Surging Sword
Pummel, Wave Fist, Earth Slash, Secret Fist, Revive



Volgrathemoose
Male
Taurus
63
43
Squire
Charge
Critical Quick
Equip Gun
Jump+1

Bestiary
Buckler
Crystal Helmet
Brigandine
Leather Mantle

Accumulate, Dash, Throw Stone, Heal, Tickle, Fury
Charge+5, Charge+7, Charge+10



B0shii
Male
Cancer
47
59
Samurai
Elemental
Dragon Spirit
Equip Shield
Move+1

Asura Knife
Flame Shield
Genji Helmet
Linen Robe
Defense Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa
Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

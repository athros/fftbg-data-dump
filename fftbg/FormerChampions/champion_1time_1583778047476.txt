Player: !zChamp
Team: Champion Team
Palettes: Black/Red



SeedSC
Female
Aries
48
44
Chemist
White Magic
Distribute
Secret Hunt
Retreat

Blast Gun

Feather Hat
Adaman Vest
Leather Mantle

Potion, X-Potion, Ether, Soft, Phoenix Down
Cure 2, Raise, Raise 2, Regen, Shell 2, Wall, Esuna



KasugaiRoastedPeas
Monster
Scorpio
67
59
Dark Behemoth










Mizucrux
Male
Virgo
69
43
Archer
Draw Out
Speed Save
Doublehand
Jump+3

Cross Bow

Leather Hat
Chain Vest
Reflect Ring

Charge+3, Charge+4, Charge+10
Heaven's Cloud, Muramasa, Kikuichimoji



Lythe Caraker
Male
Aries
41
51
Squire
Throw
Brave Up
Short Status
Move-HP Up

Bow Gun

Twist Headband
Genji Armor
Magic Gauntlet

Accumulate, Heal, Scream
Shuriken, Stick

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Evewho
Female
Capricorn
39
56
Chemist
Dance
Abandon
Equip Armor
Move-HP Up

Blast Gun

Red Hood
Genji Armor
Sprint Shoes

Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Nameless Dance, Last Dance, Void Storage, Nether Demon, Dragon Pit



Miku Shikhu
Male
Gemini
63
45
Ninja
Charge
Sunken State
Equip Gun
Jump+2

Bloody Strings
Battle Folio
Flash Hat
Adaman Vest
Rubber Shoes

Shuriken
Charge+4



Vorackgriever
Female
Aquarius
42
68
Archer
Punch Art
Critical Quick
Doublehand
Fly

Gastrafitis

Black Hood
Earth Clothes
Feather Mantle

Charge+1, Charge+2, Charge+3, Charge+20
Earth Slash, Secret Fist, Purification, Chakra, Seal Evil



Electric Algus
Male
Gemini
39
52
Mime

Dragon Spirit
Dual Wield
Move+2



Feather Hat
Power Sleeve
Rubber Shoes

Mimic


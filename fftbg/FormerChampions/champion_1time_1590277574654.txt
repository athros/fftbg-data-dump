Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Nifboy
Female
Aquarius
36
47
Monk
Basic Skill
Counter Magic
Magic Defense UP
Lava Walking



Flash Hat
Clothes
Wizard Mantle

Spin Fist, Wave Fist, Earth Slash, Chakra, Revive
Dash, Scream



Maakur
Monster
Leo
69
68
Blue Dragon










ArchKnightX
Female
Gemini
59
58
Ninja
Draw Out
Absorb Used MP
Attack UP
Jump+1

Iga Knife
Ninja Edge
Feather Hat
Wizard Outfit
Jade Armlet

Knife
Asura, Koutetsu, Murasame, Heaven's Cloud



Krasny1944
Male
Gemini
69
52
Mediator
Elemental
Earplug
Maintenance
Swim

Mythril Gun

Twist Headband
Mystic Vest
Rubber Shoes

Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Mimic Daravon, Refute
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

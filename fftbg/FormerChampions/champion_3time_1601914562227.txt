Player: !zChamp
Team: Champion Team
Palettes: Green/White



Mindblownnnn
Female
Cancer
42
68
Priest
Yin Yang Magic
Auto Potion
Magic Attack UP
Levitate

Healing Staff

Holy Miter
Robe of Lords
Battle Boots

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Esuna, Holy
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



Gongonono
Male
Capricorn
67
56
Mediator
Sing
Parry
Secret Hunt
Jump+2

Glacier Gun

Feather Hat
Black Robe
Wizard Mantle

Persuade, Threaten, Refute, Rehabilitate
Cheer Song, Last Song, Diamond Blade, Hydra Pit



Grininda
Male
Pisces
77
82
Knight
Draw Out
Parry
Short Status
Move+3

Save the Queen
Round Shield
Circlet
Leather Armor
Sprint Shoes

Head Break, Power Break, Mind Break
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa



Cryptopsy70
Female
Cancer
73
69
Squire
Dance
Meatbone Slash
Equip Armor
Jump+3

Iron Sword
Escutcheon
Cachusha
Silk Robe
Sprint Shoes

Accumulate, Heal, Tickle, Cheer Up, Fury
Slow Dance, Nameless Dance, Last Dance, Obsidian Blade, Nether Demon

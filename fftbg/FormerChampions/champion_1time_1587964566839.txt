Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Pie108
Male
Virgo
62
63
Geomancer
Summon Magic
Abandon
Martial Arts
Move+3

Battle Axe
Aegis Shield
Green Beret
Clothes
Magic Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Lava Ball
Moogle, Shiva, Golem, Carbunkle, Odin, Leviathan, Salamander, Lich



XalSmellsBad
Male
Sagittarius
54
79
Summoner
Throw
Sunken State
Sicken
Jump+2

Rod

Golden Hairpin
Linen Robe
Sprint Shoes

Moogle, Ifrit, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Silf, Fairy, Lich, Cyclops
Stick



Grimmace45
Male
Capricorn
80
68
Wizard
Talk Skill
Regenerator
Attack UP
Waterbreathing

Air Knife

Thief Hat
Black Robe
Leather Mantle

Fire, Fire 4, Bolt 2, Bolt 4, Ice 3, Ice 4
Threaten, Mimic Daravon



ZephyrTempest
Monster
Scorpio
72
41
Behemoth








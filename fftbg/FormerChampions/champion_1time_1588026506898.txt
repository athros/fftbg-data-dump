Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Midori Ribbon
Female
Leo
42
52
Samurai
Charge
Regenerator
Martial Arts
Ignore Terrain



Crystal Helmet
Genji Armor
Genji Gauntlet

Koutetsu, Bizen Boat, Muramasa
Charge+1, Charge+5



Reductions
Male
Sagittarius
54
55
Samurai
Talk Skill
Auto Potion
Equip Bow
Ignore Terrain

Poison Bow

Genji Helmet
Carabini Mail
Diamond Armlet

Asura, Heaven's Cloud, Muramasa, Masamune
Mimic Daravon



Galkife
Male
Leo
78
62
Monk
Battle Skill
MA Save
Halve MP
Move+1



Flash Hat
Mystic Vest
Leather Mantle

Spin Fist, Pummel, Earth Slash, Chakra, Revive
Shield Break, Justice Sword



KasugaiRoastedPeas
Male
Sagittarius
64
70
Archer
Basic Skill
MA Save
Sicken
Waterwalking

Windslash Bow

Golden Hairpin
Power Sleeve
Magic Ring

Charge+2, Charge+3, Charge+5, Charge+7
Dash, Cheer Up, Fury, Ultima

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



MixReveal
Monster
Leo
79
57
Ultima Demon










Kalpho
Monster
Taurus
55
75
Behemoth










Mushin Z
Male
Aries
45
57
Geomancer
Draw Out
Regenerator
Defense UP
Move+1

Slasher
Mythril Shield
Flash Hat
Wizard Robe
Leather Mantle

Pitfall, Water Ball, Blizzard, Gusty Wind, Lava Ball
Murasame, Kiyomori



Wolflux
Male
Aquarius
49
69
Knight
Steal
Hamedo
Halve MP
Jump+1

Ancient Sword
Flame Shield
Mythril Helmet
Wizard Robe
Germinas Boots

Head Break, Armor Break, Shield Break, Weapon Break, Mind Break, Stasis Sword, Surging Sword
Steal Heart, Steal Helmet, Steal Weapon

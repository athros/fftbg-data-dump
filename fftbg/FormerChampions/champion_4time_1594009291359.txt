Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Pplvee1
Female
Aquarius
51
56
Geomancer
Time Magic
Critical Quick
Secret Hunt
Move+3

Mythril Sword
Platinum Shield
Thief Hat
Leather Outfit
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind
Haste, Slow 2, Float, Reflect, Demi, Demi 2, Meteor



Mathlexis
Female
Cancer
76
74
Geomancer
White Magic
Distribute
Defend
Levitate

Kiyomori
Ice Shield
Holy Miter
Linen Robe
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Cure 3, Raise, Reraise, Regen, Shell, Esuna



Lowlf
Male
Leo
42
41
Priest
Draw Out
Caution
Equip Knife
Jump+2

Dragon Rod

Golden Hairpin
White Robe
Small Mantle

Cure, Cure 2, Raise, Protect, Protect 2, Wall, Esuna
Murasame, Kiyomori, Kikuichimoji



Shalloween
Female
Capricorn
78
63
Dancer
Item
Damage Split
Short Charge
Swim

Ryozan Silk

Leather Hat
Earth Clothes
Small Mantle

Wiznaibus, Polka Polka, Nameless Dance, Last Dance, Nether Demon, Dragon Pit
Potion, X-Potion, Ether, Elixir, Antidote, Echo Grass, Holy Water

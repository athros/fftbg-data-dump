Player: !zChamp
Team: Champion Team
Palettes: Green/White



Ruebyy
Male
Gemini
70
60
Knight
Elemental
Earplug
Equip Bow
Levitate

Ultimus Bow
Bronze Shield
Genji Helmet
Plate Mail
Magic Gauntlet

Head Break, Armor Break, Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard



Firesheath
Female
Scorpio
77
74
Thief
Jump
Counter
Concentrate
Jump+3

Mage Masher

Holy Miter
Judo Outfit
Defense Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Level Jump4, Vertical Jump3



Sinnyil2
Female
Pisces
78
47
Calculator
Black Magic
Faith Save
Defend
Move+1

Papyrus Codex

Green Beret
Power Sleeve
Angel Ring

Height, 5, 4, 3
Fire, Fire 2, Fire 4, Bolt, Bolt 2, Ice, Ice 3



Sairentozon7
Female
Gemini
79
43
Wizard
Dance
MP Restore
Martial Arts
Move-MP Up



Twist Headband
Linen Robe
Feather Mantle

Ice 2, Ice 4, Frog
Witch Hunt, Wiznaibus, Disillusion, Obsidian Blade, Dragon Pit

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Nizaha
Male
Taurus
80
44
Oracle
Black Magic
Arrow Guard
Doublehand
Move+1

Papyrus Codex

Triangle Hat
Judo Outfit
Angel Ring

Blind, Spell Absorb, Life Drain, Silence Song, Sleep, Petrify
Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower



Kronikle
Male
Aries
49
48
Calculator
Animal Skill
Catch
Short Status
Move-HP Up

Bestiary
Escutcheon
Golden Hairpin
Robe of Lords
Defense Ring

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



SQUiDSQUARKLIN
Monster
Libra
68
50
Black Chocobo










Alrightbye
Female
Cancer
52
66
Priest
Time Magic
Speed Save
Equip Gun
Teleport

Mythril Gun

Holy Miter
Light Robe
Elf Mantle

Cure 3, Cure 4, Reraise, Protect, Esuna, Holy
Haste 2, Stop, Immobilize, Quick, Demi, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Fenaen
Female
Libra
52
56
Archer
Jump
MA Save
Doublehand
Jump+3

Night Killer

Feather Hat
Clothes
Feather Boots

Charge+1, Charge+3, Charge+4, Charge+7
Level Jump4, Vertical Jump5



Lokenwow
Female
Taurus
65
66
Time Mage
White Magic
Speed Save
Equip Axe
Waterwalking

Gold Staff

Thief Hat
Leather Outfit
Defense Ring

Haste, Slow, Slow 2, Stop, Immobilize, Demi, Stabilize Time
Cure 2, Reraise, Protect



Rinhander
Monster
Aries
49
70
Ahriman










Butterbelljedi
Female
Gemini
55
57
Geomancer
Draw Out
Critical Quick
Dual Wield
Jump+1

Ice Brand
Bizen Boat
Green Beret
Wizard Outfit
Cursed Ring

Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji

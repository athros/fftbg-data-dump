Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Sinnyil2
Female
Scorpio
72
49
Samurai
Steal
Auto Potion
Concentrate
Jump+2

Koutetsu Knife

Genji Helmet
Chameleon Robe
Magic Gauntlet

Heaven's Cloud, Kikuichimoji
Gil Taking, Steal Status, Arm Aim



Zenlion
Female
Cancer
69
66
Mediator
Charge
HP Restore
Maintenance
Move-HP Up

Mage Masher

Twist Headband
Mystic Vest
Leather Mantle

Persuade, Praise, Insult, Mimic Daravon, Refute, Rehabilitate
Charge+3, Charge+5, Charge+7



Aneyus
Female
Taurus
60
56
Wizard
Basic Skill
Earplug
Short Charge
Move+1

Dragon Rod

Headgear
Black Robe
Sprint Shoes

Fire 3, Bolt 2, Bolt 4, Ice 2, Ice 3
Dash, Yell, Cheer Up



Hamborn
Female
Pisces
56
68
Thief
Talk Skill
Dragon Spirit
Secret Hunt
Swim

Ninja Edge

Holy Miter
Judo Outfit
Leather Mantle

Gil Taking, Steal Heart, Steal Status
Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



StealthModeLocke
Male
Aquarius
54
68
Wizard
Time Magic
MP Restore
Equip Bow
Retreat

Hunting Bow

Leather Hat
Mystic Vest
108 Gems

Fire 2, Fire 3, Fire 4, Ice, Ice 2, Empower
Slow, Slow 2, Reflect, Stabilize Time



Firesheath
Female
Sagittarius
40
69
Archer
White Magic
Mana Shield
Equip Bow
Teleport

Poison Bow
Genji Shield
Headgear
Power Sleeve
108 Gems

Charge+1, Charge+3
Cure 2, Protect 2, Shell, Shell 2



TheMM42
Female
Gemini
68
69
Thief
Black Magic
Critical Quick
Dual Wield
Fly

Mythril Knife
Spell Edge
Triangle Hat
Judo Outfit
Reflect Ring

Gil Taking, Steal Weapon, Arm Aim
Bolt 4



Argentbast
Female
Serpentarius
70
40
Mediator
Elemental
Distribute
Concentrate
Waterwalking

Blast Gun

Thief Hat
Mystic Vest
Magic Ring

Invitation, Preach, Solution, Negotiate, Mimic Daravon, Refute
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ZeroHeat
Male
Serpentarius
46
68
Archer
Summon Magic
Parry
Dual Wield
Jump+1

Yoichi Bow

Flash Hat
Wizard Outfit
Genji Gauntlet

Charge+10
Moogle, Ramuh, Ifrit, Golem, Bahamut, Silf, Fairy



Phi Sig
Male
Leo
59
79
Oracle
Punch Art
Dragon Spirit
Equip Sword
Jump+1

Heaven's Cloud

Flash Hat
Secret Clothes
Magic Ring

Poison, Life Drain, Blind Rage, Dispel Magic, Paralyze
Pummel, Wave Fist, Purification, Revive



Toksu
Monster
Aquarius
71
54
Hydra










Elelor
Female
Virgo
44
52
Knight
Item
Counter Flood
Dual Wield
Jump+1

Sleep Sword
Blood Sword
Genji Helmet
Gold Armor
Spike Shoes

Armor Break, Shield Break, Magic Break
Ether, Holy Water, Phoenix Down

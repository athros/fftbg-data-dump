Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Reddwind
Male
Libra
62
57
Knight
Time Magic
Meatbone Slash
Long Status
Jump+1

Battle Axe
Diamond Shield
Mythril Helmet
Diamond Armor
N-Kai Armlet

Weapon Break, Magic Break, Speed Break
Haste, Demi, Demi 2



Magicandy
Female
Sagittarius
57
77
Knight
Steal
PA Save
Equip Sword
Ignore Height

Ragnarok
Platinum Shield
Leather Helmet
Genji Armor
Genji Gauntlet

Shield Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Surging Sword
Steal Weapon, Steal Status



Actual JP
Female
Aquarius
46
54
Oracle
Draw Out
Auto Potion
Martial Arts
Move+2

Papyrus Codex

Holy Miter
Silk Robe
108 Gems

Pray Faith, Doubt Faith, Silence Song, Paralyze
Asura, Koutetsu



YaBoy125
Male
Sagittarius
78
54
Knight
Item
Absorb Used MP
Short Charge
Jump+2

Defender
Genji Shield
Mythril Helmet
Carabini Mail
Rubber Shoes

Shield Break, Mind Break, Stasis Sword, Night Sword, Surging Sword
Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

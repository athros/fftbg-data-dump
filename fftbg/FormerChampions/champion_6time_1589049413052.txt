Player: !zChamp
Team: Champion Team
Palettes: White/Blue



EizanTayama
Monster
Aries
54
77
Wyvern










Hydroshade
Male
Pisces
63
76
Monk
White Magic
Auto Potion
Attack UP
Move+1



Flash Hat
Secret Clothes
Sprint Shoes

Earth Slash, Purification, Revive
Cure, Raise, Reraise, Protect, Shell, Esuna



ThePineappleSalesman
Female
Scorpio
75
68
Ninja
Yin Yang Magic
Mana Shield
Attack UP
Teleport

Mythril Knife
Hydra Bag
Green Beret
Judo Outfit
Angel Ring

Shuriken, Bomb, Knife, Sword, Dictionary
Poison, Pray Faith, Doubt Faith, Silence Song, Paralyze



Leakimiko
Male
Cancer
66
51
Lancer
White Magic
Counter Tackle
Dual Wield
Move-HP Up

Holy Lance
Dragon Whisker
Mythril Helmet
Chain Mail
Leather Mantle

Level Jump3, Vertical Jump2
Cure 3, Cure 4, Raise, Reraise

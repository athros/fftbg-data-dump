Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Thyrandaal
Monster
Aquarius
61
64
Red Dragon










Josephiroth 143
Male
Capricorn
61
70
Samurai
Time Magic
PA Save
Equip Sword
Move-HP Up

Ragnarok

Gold Helmet
Chain Mail
Angel Ring

Asura, Koutetsu, Muramasa
Slow, Stop, Immobilize, Float, Stabilize Time



Gorgewall
Male
Aries
79
51
Mime

HP Restore
Magic Defense UP
Jump+1


Buckler
Leather Hat
Wizard Outfit
Chantage

Mimic




Avin Chaos
Female
Libra
70
72
Summoner
Punch Art
Auto Potion
Short Charge
Teleport

Rainbow Staff

Flash Hat
Light Robe
Defense Armlet

Ramuh, Carbunkle, Bahamut, Lich, Cyclops
Spin Fist, Pummel, Secret Fist, Purification, Revive

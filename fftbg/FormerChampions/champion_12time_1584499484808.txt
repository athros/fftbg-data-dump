Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Lodrak
Male
Leo
50
65
Ninja
Yin Yang Magic
Blade Grasp
Equip Axe
Move+3

Flail
Slasher
Headgear
Brigandine
Cursed Ring

Shuriken, Bomb, Sword, Hammer
Silence Song, Blind Rage, Confusion Song



JumbocactuarX27
Monster
Gemini
54
39
Red Chocobo










LDSkinny
Female
Pisces
44
50
Summoner
Charge
Critical Quick
Short Charge
Waterwalking

Thunder Rod

Golden Hairpin
Linen Robe
Battle Boots

Moogle, Ifrit, Carbunkle, Leviathan, Salamander, Fairy, Lich, Zodiac
Charge+1, Charge+2, Charge+4, Charge+5



Daveb
Female
Gemini
79
47
Ninja
Item
Caution
Short Status
Jump+2

Short Edge
Sasuke Knife
Twist Headband
Wizard Outfit
Defense Ring

Shuriken, Bomb, Staff
Potion, Ether, Remedy, Phoenix Down

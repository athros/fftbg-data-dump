Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Chuckolator
Female
Leo
58
76
Geomancer
Draw Out
Counter Magic
Magic Attack UP
Waterwalking

Battle Axe
Bronze Shield
Leather Hat
Black Costume
Cursed Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Kiyomori



Lord Gwarth
Male
Virgo
63
57
Calculator
White Magic
Absorb Used MP
Equip Axe
Teleport

Slasher

Red Hood
Wizard Robe
Genji Gauntlet

CT, Height, Prime Number, 5, 4, 3
Cure, Cure 2, Raise, Reraise, Shell 2, Esuna



VolgraTheMoose
Male
Aquarius
54
67
Archer
Steal
Counter Tackle
Doublehand
Swim

Mythril Bow

Crystal Helmet
Earth Clothes
Rubber Shoes

Charge+4, Charge+5, Charge+7
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Leg Aim



Pplvee1
Male
Leo
58
77
Ninja
Charge
Brave Save
Equip Gun
Move-MP Up

Blaze Gun
Glacier Gun
Red Hood
Mystic Vest
Wizard Mantle

Bomb
Charge+1, Charge+3, Charge+10, Charge+20

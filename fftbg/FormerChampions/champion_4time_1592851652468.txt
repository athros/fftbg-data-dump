Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



WalkerNash
Male
Sagittarius
74
58
Geomancer
Steal
Counter Flood
Sicken
Waterwalking

Slasher
Diamond Shield
Golden Hairpin
Mystic Vest
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Helmet, Steal Accessory, Steal Status, Arm Aim



Magicandy
Female
Aquarius
38
78
Ninja
Draw Out
Mana Shield
Defend
Swim

Sasuke Knife
Spell Edge
Green Beret
Judo Outfit
Bracer

Shuriken, Bomb, Knife, Staff
Asura, Bizen Boat



Richardserious
Male
Sagittarius
65
45
Priest
Black Magic
MP Restore
Equip Sword
Move+2

Kikuichimoji

Holy Miter
Power Sleeve
Small Mantle

Raise, Raise 2, Shell, Esuna
Fire, Bolt, Bolt 2, Bolt 3, Bolt 4, Empower, Death



RunicMagus
Female
Gemini
65
57
Priest
Punch Art
Distribute
Sicken
Levitate

Rainbow Staff

Headgear
Robe of Lords
Elf Mantle

Cure, Raise, Raise 2, Reraise, Protect, Protect 2, Shell, Esuna, Holy
Wave Fist, Earth Slash, Chakra

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Silentperogy
Male
Cancer
74
48
Monk
Battle Skill
Counter Magic
Dual Wield
Teleport



Twist Headband
Clothes
N-Kai Armlet

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Magic Break, Power Break, Justice Sword



HASTERIOUS
Male
Leo
73
67
Bard
Talk Skill
Distribute
Doublehand
Retreat

Bloody Strings

Holy Miter
Wizard Outfit
Angel Ring

Life Song, Magic Song, Nameless Song, Hydra Pit
Threaten, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate



ApplesauceBoss
Female
Sagittarius
69
50
Geomancer
Draw Out
Meatbone Slash
Magic Attack UP
Jump+1

Iron Sword
Buckler
Flash Hat
Earth Clothes
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat



Joewcarson
Male
Aries
50
49
Chemist
Summon Magic
Earplug
Maintenance
Ignore Height

Stone Gun

Thief Hat
Wizard Outfit
Jade Armlet

Potion, Hi-Ether, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Ifrit, Titan, Golem, Carbunkle, Odin, Leviathan, Fairy, Cyclops

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ThePineappleSalesman
Female
Capricorn
80
80
Archer
Elemental
Sunken State
Doublehand
Jump+3

Ultimus Bow

Headgear
Rubber Costume
Reflect Ring

Charge+2, Charge+3, Charge+7, Charge+10
Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball



Lijarkh
Male
Pisces
71
79
Monk
Time Magic
Counter Tackle
Attack UP
Waterbreathing



Twist Headband
Wizard Outfit
Defense Armlet

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Haste, Haste 2, Reflect, Stabilize Time



Reinoe
Male
Aries
53
47
Time Mage
Draw Out
Sunken State
Doublehand
Levitate

Gold Staff

Leather Hat
Wizard Outfit
Dracula Mantle

Haste, Immobilize, Float, Reflect, Demi
Bizen Boat, Heaven's Cloud, Kikuichimoji



Sairentozon7
Male
Aquarius
71
79
Samurai
Throw
MA Save
Equip Shield
Teleport

Heaven's Cloud
Gold Shield
Iron Helmet
Linen Cuirass
Dracula Mantle

Asura, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Shuriken, Knife, Staff, Stick, Wand

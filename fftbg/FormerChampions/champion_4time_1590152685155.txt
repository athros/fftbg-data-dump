Player: !zChamp
Team: Champion Team
Palettes: Black/Red



CorpusCav
Female
Virgo
53
70
Dancer
Elemental
Counter Flood
Dual Wield
Levitate

Cashmere
Cashmere
Black Hood
Linen Robe
N-Kai Armlet

Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Last Dance
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Dowdlron
Female
Gemini
69
61
Mediator
Yin Yang Magic
Distribute
Beastmaster
Jump+1

Papyrus Codex

Red Hood
Silk Robe
Sprint Shoes

Persuade, Solution, Death Sentence, Negotiate, Refute
Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Confusion Song, Dark Holy



Luna XIV
Male
Capricorn
47
79
Mime

Absorb Used MP
Magic Defense UP
Move+1



Flash Hat
Judo Outfit
Magic Gauntlet

Mimic




Zachara
Male
Leo
65
67
Oracle
Draw Out
Regenerator
Equip Knife
Move-MP Up

Rod

Green Beret
Earth Clothes
Defense Armlet

Blind, Poison, Life Drain, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Petrify, Dark Holy
Heaven's Cloud, Muramasa

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Ayntlerz
Female
Aquarius
73
72
Squire
Time Magic
Counter Flood
Magic Attack UP
Move-MP Up

Panther Bag
Ice Shield
Red Hood
Judo Outfit
Cursed Ring

Heal, Yell, Scream
Haste, Slow, Slow 2, Immobilize, Float, Quick, Demi, Stabilize Time, Meteor



Carchan131
Female
Scorpio
43
42
Geomancer
Item
Speed Save
Equip Knife
Ignore Height

Air Knife
Buckler
Holy Miter
Silk Robe
Magic Ring

Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Hi-Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



Powergems
Male
Taurus
49
67
Samurai
Punch Art
Counter Flood
Doublehand
Lava Walking

Spear

Circlet
Diamond Armor
Elf Mantle

Bizen Boat, Murasame, Heaven's Cloud
Pummel, Earth Slash, Secret Fist, Purification, Revive



Xenomorith
Male
Gemini
66
78
Ninja
Time Magic
MA Save
Beastmaster
Lava Walking

Flame Whip
Sasuke Knife
Green Beret
Earth Clothes
Diamond Armlet

Shuriken, Knife
Haste, Float

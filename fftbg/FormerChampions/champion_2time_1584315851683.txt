Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



PotionDweller
Monster
Virgo
45
71
Ultima Demon










HaateXIII
Female
Libra
75
52
Samurai
Punch Art
Distribute
Beastmaster
Jump+1

Asura Knife

Crystal Helmet
Bronze Armor
Cherche

Koutetsu, Kiyomori
Wave Fist, Secret Fist, Purification, Chakra, Seal Evil



Fftbchamp
Male
Scorpio
53
64
Knight
Punch Art
Sunken State
Attack UP
Move-MP Up

Sleep Sword
Platinum Shield
Barbuta
Linen Cuirass
Defense Armlet

Shield Break, Weapon Break, Stasis Sword, Justice Sword
Earth Slash, Purification, Revive



Clippopo
Male
Taurus
58
49
Archer
Elemental
Mana Shield
Halve MP
Jump+1

Night Killer
Aegis Shield
Feather Hat
Mystic Vest
Germinas Boots

Charge+10
Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



ApplesauceBoss
Male
Sagittarius
72
76
Monk
Yin Yang Magic
Hamedo
Magic Defense UP
Move+3



Flash Hat
Judo Outfit
Reflect Ring

Spin Fist, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Blind, Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Dispel Magic, Sleep



OneHundredFists
Male
Aries
71
58
Squire
Jump
MP Restore
Equip Bow
Retreat

Poison Bow
Ice Shield
Twist Headband
Plate Mail
Magic Gauntlet

Accumulate, Dash, Heal
Level Jump5, Vertical Jump8



Zeroroute
Female
Capricorn
59
54
Samurai
Summon Magic
Abandon
Doublehand
Teleport

Spear

Cross Helmet
Gold Armor
Rubber Shoes

Kiyomori
Moogle, Shiva, Ramuh, Golem, Carbunkle, Fairy, Cyclops



Kronikle
Male
Aries
79
48
Ninja
Black Magic
Mana Shield
Beastmaster
Move-MP Up

Assassin Dagger
Mage Masher
Triangle Hat
Earth Clothes
Power Wrist

Shuriken, Knife
Fire 4, Bolt 3, Bolt 4, Ice 4

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Slickdang
Male
Aries
68
65
Time Mage
Throw
Arrow Guard
Equip Armor
Waterbreathing

Oak Staff

Leather Helmet
Bronze Armor
Small Mantle

Haste, Slow, Immobilize, Float, Reflect, Quick, Demi, Demi 2, Galaxy Stop
Shuriken, Knife, Ninja Sword, Wand



Mesmaster
Female
Sagittarius
78
56
Samurai
Dance
Critical Quick
Short Charge
Fly

Partisan

Leather Helmet
Genji Armor
Genji Gauntlet

Kiyomori, Muramasa, Masamune
Polka Polka



Rislyeu
Female
Sagittarius
78
53
Archer
Battle Skill
Auto Potion
Attack UP
Ignore Height

Mythril Gun
Mythril Shield
Golden Hairpin
Rubber Costume
Battle Boots

Charge+3, Charge+5
Head Break, Speed Break, Mind Break, Stasis Sword



Lastly
Female
Taurus
80
42
Ninja
Jump
MP Restore
Sicken
Jump+1

Kunai
Short Edge
Twist Headband
Power Sleeve
Red Shoes

Shuriken, Bomb, Spear
Level Jump5, Vertical Jump2

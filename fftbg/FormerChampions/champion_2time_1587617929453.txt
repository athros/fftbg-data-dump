Player: !zChamp
Team: Champion Team
Palettes: Green/White



ArchKnightX
Male
Aries
54
79
Samurai
Time Magic
Earplug
Sicken
Fly

Asura Knife

Circlet
Silk Robe
Elf Mantle

Kiyomori
Haste, Haste 2, Stop, Reflect, Quick, Stabilize Time, Meteor



DonCardenio
Monster
Cancer
80
74
Dark Behemoth










Loveyouallfriends
Female
Scorpio
49
42
Priest
Draw Out
Speed Save
Magic Defense UP
Move+1

Flail

Feather Hat
Black Costume
Angel Ring

Raise, Raise 2, Protect, Shell, Shell 2, Wall, Esuna, Holy
Bizen Boat



Arch8000
Female
Pisces
68
82
Geomancer
White Magic
Abandon
Defense UP
Move-MP Up

Slasher
Bronze Shield
Feather Hat
Earth Clothes
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Cure 3, Raise, Reraise, Protect 2, Shell 2, Wall, Esuna, Holy

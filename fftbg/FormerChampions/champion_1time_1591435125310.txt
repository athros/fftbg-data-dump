Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Einhanderz
Male
Taurus
80
79
Ninja
Battle Skill
Speed Save
Defense UP
Move+3

Ninja Edge
Koga Knife
Holy Miter
Power Sleeve
Magic Gauntlet

Shuriken, Staff
Shield Break, Weapon Break, Magic Break, Speed Break, Dark Sword



Byrdturbo
Male
Virgo
39
65
Mediator
Elemental
Parry
Short Status
Levitate

Battle Folio

Holy Miter
Judo Outfit
Vanish Mantle

Invitation, Persuade, Praise, Threaten, Solution, Negotiate, Refute
Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Rocl
Female
Aries
74
37
Oracle
Summon Magic
Counter Flood
Short Status
Waterbreathing

Musk Rod

Black Hood
Wizard Robe
Dracula Mantle

Poison, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze, Petrify
Moogle, Carbunkle, Bahamut, Lich



Deathmaker06
Male
Pisces
72
68
Thief
Item
Parry
Magic Defense UP
Retreat

Main Gauche

Flash Hat
Brigandine
Magic Gauntlet

Gil Taking, Steal Armor, Steal Weapon
Potion, Hi-Potion, Hi-Ether, Antidote, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Chocobo Coco
Female
Cancer
54
57
Wizard
Item
Counter
Defend
Move+3

Flame Rod

Green Beret
Mythril Vest
Elf Mantle

Fire 3, Fire 4, Bolt 4, Ice 4, Death, Flare
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Phi Sig
Female
Scorpio
51
43
Chemist
Talk Skill
Hamedo
Monster Talk
Waterbreathing

Mythril Gun

Leather Hat
Judo Outfit
Feather Mantle

Potion, Eye Drop, Soft, Phoenix Down
Invitation, Praise, Preach, Insult, Refute



Ultimainferno
Female
Scorpio
81
59
Archer
White Magic
Arrow Guard
Equip Bow
Fly

Yoichi Bow

Red Hood
Adaman Vest
108 Gems

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Cure 2, Raise, Raise 2, Reraise, Regen, Shell 2, Wall, Esuna



Spidergun0
Male
Leo
64
51
Monk
Summon Magic
Meatbone Slash
Dual Wield
Jump+3



Thief Hat
Chain Vest
Genji Gauntlet

Spin Fist, Pummel, Revive
Moogle, Ramuh, Fairy, Lich

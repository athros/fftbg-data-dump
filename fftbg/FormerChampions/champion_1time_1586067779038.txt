Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Rogueain
Female
Cancer
75
62
Lancer
Elemental
Counter
Martial Arts
Move-MP Up


Flame Shield
Crystal Helmet
Plate Mail
Reflect Ring

Level Jump5, Vertical Jump8
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Zepi Zza
Monster
Gemini
45
53
Tiamat










WireLord
Female
Scorpio
48
76
Ninja
Yin Yang Magic
Counter
Equip Polearm
Swim

Ivory Rod
Spear
Headgear
Earth Clothes
Feather Mantle

Stick, Wand
Blind Rage, Foxbird, Dispel Magic



Legitimized
Female
Pisces
46
48
Samurai
Black Magic
Caution
Short Status
Levitate

Murasame

Bronze Helmet
Light Robe
Angel Ring

Koutetsu, Murasame
Fire, Fire 3, Bolt 3, Bolt 4, Ice 2, Empower, Frog

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Lotte
Female
Sagittarius
56
55
Geomancer
Draw Out
Abandon
Equip Polearm
Swim

Mythril Spear
Bronze Shield
Twist Headband
Secret Clothes
Red Shoes

Pitfall, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind
Heaven's Cloud, Kiyomori, Muramasa



Angil
Female
Scorpio
79
62
Summoner
Dance
Faith Up
Maintenance
Retreat

Wizard Rod

Holy Miter
Linen Robe
Germinas Boots

Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nether Demon



Yelena
Female
Sagittarius
49
81
Lancer
Item
Abandon
Throw Item
Retreat

Octagon Rod
Diamond Shield
Grand Helmet
Genji Armor
Red Shoes

Level Jump8, Vertical Jump8
Potion, Antidote, Phoenix Down



Flossie
Female
Gemini
66
77
Lancer
Summon Magic
MP Restore
Dual Wield
Jump+1

Iron Fan
Spear
Genji Helmet
Platinum Armor
Magic Gauntlet

Level Jump8, Vertical Jump2
Moogle, Ifrit, Carbunkle, Bahamut, Leviathan, Silf, Fairy, Lich, Zodiac



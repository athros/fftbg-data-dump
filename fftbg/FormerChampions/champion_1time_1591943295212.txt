Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Powermhero
Female
Aries
47
57
Mediator
Item
Damage Split
Long Status
Retreat

Battle Folio

Leather Hat
White Robe
Sprint Shoes

Praise, Solution, Death Sentence, Mimic Daravon, Refute
Potion, Eye Drop, Holy Water, Phoenix Down



Mokura3rd
Female
Libra
67
46
Thief
Battle Skill
Catch
Defense UP
Levitate

Dagger

Twist Headband
Brigandine
Power Wrist

Gil Taking, Steal Heart, Steal Armor, Steal Accessory
Mind Break, Stasis Sword



Thyrandaal
Monster
Taurus
69
53
Explosive










Moocaotao
Male
Aquarius
41
50
Geomancer
Talk Skill
Arrow Guard
Short Charge
Levitate

Battle Axe
Aegis Shield
Red Hood
Wizard Robe
Diamond Armlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Solution, Death Sentence, Negotiate, Mimic Daravon, Refute

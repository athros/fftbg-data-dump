Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



MrFlabyo
Female
Aquarius
72
71
Oracle
Summon Magic
Regenerator
Equip Knife
Move+2

Orichalcum

Leather Hat
Wizard Robe
Dracula Mantle

Dispel Magic, Paralyze
Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Salamander, Fairy, Lich



ColetteMSLP
Female
Cancer
43
55
Oracle
Draw Out
Faith Up
Beastmaster
Jump+2

Iron Fan

Barette
Mystic Vest
Small Mantle

Spell Absorb, Zombie, Blind Rage, Foxbird, Dispel Magic
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



Aneyus
Male
Sagittarius
65
46
Archer
Summon Magic
Counter
Doublehand
Teleport

Perseus Bow

Golden Hairpin
Mystic Vest
Spike Shoes

Charge+2, Charge+4, Charge+10
Moogle, Ramuh, Titan, Leviathan, Fairy



YaBoy125
Male
Scorpio
56
54
Archer
Yin Yang Magic
Counter
Doublehand
Move-MP Up

Ice Bow

Red Hood
Clothes
Magic Ring

Charge+1, Charge+20
Blind, Pray Faith, Dispel Magic, Paralyze, Sleep

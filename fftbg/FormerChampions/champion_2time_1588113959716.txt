Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Baron Von Scrub
Male
Taurus
61
56
Monk
Draw Out
Critical Quick
Magic Defense UP
Move+2



Holy Miter
Adaman Vest
Cursed Ring

Pummel, Purification
Koutetsu, Murasame



Midori Ribbon
Female
Virgo
72
62
Thief
Throw
Sunken State
Maintenance
Waterwalking

Dagger

Triangle Hat
Adaman Vest
Small Mantle

Steal Heart, Steal Status, Arm Aim
Shuriken, Staff



PetitFoulard
Male
Aquarius
80
41
Geomancer
Battle Skill
Meatbone Slash
Long Status
Ignore Terrain

Slasher
Escutcheon
Thief Hat
Chameleon Robe
Leather Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Head Break, Armor Break, Magic Break, Power Break, Stasis Sword, Justice Sword, Surging Sword



ANFz
Male
Leo
75
79
Squire
Item
PA Save
Equip Polearm
Waterbreathing

Javelin
Hero Shield
Feather Hat
Linen Cuirass
Magic Ring

Accumulate, Throw Stone, Yell, Cheer Up, Wish
Potion, X-Potion, Ether, Echo Grass, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Hales Bopp It
Female
Pisces
39
79
Summoner
Draw Out
Abandon
Equip Polearm
Jump+3

Battle Bamboo

Thief Hat
Robe of Lords
Diamond Armlet

Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Fairy, Lich, Cyclops
Bizen Boat, Murasame, Kiyomori



Lythe Caraker
Male
Virgo
61
44
Ninja
White Magic
Mana Shield
Martial Arts
Waterbreathing



Feather Hat
Black Costume
Genji Gauntlet

Bomb
Cure, Raise, Raise 2, Protect 2, Shell, Esuna, Holy



SneakyFingerz
Male
Sagittarius
70
42
Geomancer
Item
Speed Save
Equip Sword
Ignore Terrain

Ice Brand
Aegis Shield
Feather Hat
Light Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Lava Ball
Potion, X-Potion



Go Pack
Female
Virgo
49
75
Summoner
Talk Skill
Counter
Short Charge
Waterbreathing

Wizard Staff

Leather Hat
Adaman Vest
Reflect Ring

Moogle, Titan, Golem, Odin, Salamander, Silf
Invitation, Persuade, Praise, Insult, Refute

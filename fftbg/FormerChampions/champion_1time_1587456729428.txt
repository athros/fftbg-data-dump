Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Byrdturbo
Male
Gemini
47
78
Calculator
Tentacle Skill
Damage Split
Doublehand
Waterwalking

Thunder Rod

Genji Helmet
Mythril Armor
Feather Mantle

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



R Raynos
Female
Gemini
40
48
Knight
Dance
Distribute
Equip Polearm
Jump+3

Gokuu Rod
Platinum Shield
Mythril Helmet
Carabini Mail
Diamond Armlet

Head Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword
Slow Dance, Obsidian Blade



ExecutedGiraffe
Female
Capricorn
55
62
Time Mage
Charge
Meatbone Slash
Equip Bow
Move-HP Up

Bow Gun

Flash Hat
Black Robe
Defense Ring

Haste, Haste 2, Slow 2, Immobilize, Reflect, Quick, Stabilize Time
Charge+1, Charge+3, Charge+4, Charge+5



CapnChaos12
Female
Aries
59
47
Geomancer
Draw Out
MA Save
Attack UP
Move-MP Up

Battle Axe
Mythril Shield
Triangle Hat
Wizard Robe
Vanish Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Asura, Murasame

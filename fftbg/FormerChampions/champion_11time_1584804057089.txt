Player: !zChamp
Team: Champion Team
Palettes: Black/Red



LoLDrones
Male
Cancer
48
55
Archer
Steal
Sunken State
Defend
Lava Walking

Mythril Bow

Flash Hat
Brigandine
Jade Armlet

Charge+2, Charge+7
Gil Taking, Steal Armor, Steal Shield, Steal Accessory



KitchTowel
Monster
Pisces
43
47
Steel Giant










Phi Sig
Male
Pisces
83
70
Bard
Time Magic
Counter
Doublehand
Jump+2

Long Bow

Twist Headband
Chain Mail
Elf Mantle

Sky Demon
Stop, Immobilize, Stabilize Time



PotionDweller
Male
Sagittarius
64
67
Archer
Elemental
Critical Quick
Equip Shield
Move+3

Yoichi Bow
Crystal Shield
Green Beret
Mythril Vest
Chantage

Charge+4
Water Ball, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball

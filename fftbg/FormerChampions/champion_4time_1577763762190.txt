Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Oralia
Female
Libra
44
49
Chemist
Elemental
Damage Split
Dual Wield
Retreat

Stone Gun
Glacier Gun
Triangle Hat
Power Sleeve
Germinas Boots

Potion, Hi-Potion, Antidote, Echo Grass, Remedy, Phoenix Down
Water Ball, Hell Ivy, Local Quake, Sand Storm, Blizzard, Gusty Wind



Ilona
Female
Libra
54
66
Monk
Yin Yang Magic
Hamedo
Equip Gun
Jump+1

Papyrus Codex

Flash Hat
Leather Outfit
Wizard Mantle

Pummel, Wave Fist, Chakra, Revive
Pray Faith, Zombie, Foxbird, Paralyze, Sleep



Avrom
Male
Scorpio
48
61
Geomancer
Charge
Sunken State
Magic Attack UP
Jump+2

Panther Bag
Ice Shield
Leather Hat
Linen Robe
Cursed Ring

Hell Ivy, Local Quake, Will-O-Wisp, Gusty Wind, Lava Ball
Charge+4, Charge+5, Charge+7, Charge+10



Nance
Female
Capricorn
52
65
Archer
Basic Skill
MA Save
Maintenance
Move+2

Mythril Gun
Diamond Shield
Leather Hat
Wizard Outfit
N-Kai Armlet

Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Heal, Tickle, Cheer Up



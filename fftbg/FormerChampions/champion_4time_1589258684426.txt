Player: !zChamp
Team: Champion Team
Palettes: Green/White



Ferroniss
Female
Aquarius
49
61
Dancer
Black Magic
MA Save
Magic Defense UP
Move+3

Ryozan Silk

Holy Miter
Wizard Outfit
Diamond Armlet

Witch Hunt, Polka Polka, Obsidian Blade, Nether Demon, Dragon Pit
Fire, Fire 2, Fire 4, Bolt 4, Ice, Flare



Zagorsek
Male
Capricorn
79
57
Time Mage
Yin Yang Magic
Damage Split
Equip Armor
Ignore Height

Octagon Rod

Diamond Helmet
Genji Armor
Elf Mantle

Haste 2, Slow, Reflect, Quick, Demi, Demi 2
Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Dispel Magic



CorpusCav
Female
Capricorn
67
57
Calculator
Yin Yang Magic
Damage Split
Doublehand
Move+3

Bestiary

Cachusha
Black Robe
Angel Ring

Height, Prime Number, 5, 4
Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Paralyze, Sleep, Petrify, Dark Holy



HASTERIOUS
Male
Cancer
67
51
Ninja
Item
Counter Flood
Attack UP
Jump+2

Flail
Hidden Knife
Green Beret
Leather Outfit
Angel Ring

Shuriken, Bomb
Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ThanatosXRagnarok
Monster
Sagittarius
49
77
Tiamat










AniZero
Male
Libra
46
59
Lancer
Punch Art
PA Save
Doublehand
Move-MP Up

Gokuu Rod

Platinum Helmet
Plate Mail
Sprint Shoes

Level Jump3, Vertical Jump7
Wave Fist, Earth Slash, Purification, Revive



MattMan119
Male
Virgo
63
45
Samurai
Throw
Abandon
Maintenance
Move+3

Murasame

Platinum Helmet
Carabini Mail
Magic Ring

Asura, Koutetsu, Murasame, Kikuichimoji
Shuriken, Hammer, Ninja Sword



BobertLocke
Male
Capricorn
54
69
Monk
Elemental
Regenerator
Equip Axe
Move+1

Giant Axe

Holy Miter
Leather Outfit
Jade Armlet

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil
Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ItotheCtotheE
Monster
Cancer
75
79
Wyvern










Brunhilda
Female
Leo
54
62
Ninja
Dance
Brave Up
Doublehand
Swim

Mage Masher

Golden Hairpin
Leather Outfit
Genji Gauntlet

Shuriken, Bomb, Dictionary
Witch Hunt, Polka Polka, Disillusion, Last Dance, Dragon Pit



Sanderson
Male
Aquarius
58
45
Knight
Item
Faith Up
Short Status
Move-HP Up

Ragnarok

Gold Helmet
Reflect Mail
Elf Mantle

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword
X-Potion, Antidote



Jane
Female
Scorpio
80
37
Geomancer
Basic Skill
Counter Tackle
Equip Knife
Move-HP Up

Iron Sword
Crystal Shield
Holy Miter
Chain Vest
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Accumulate, Heal, Yell, Wish

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



DeathOfMarat
Male
Gemini
44
60
Archer
Elemental
HP Restore
Defense UP
Jump+1

Silver Bow

Golden Hairpin
Brigandine
Bracer

Charge+2, Charge+5, Charge+7
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Lava Ball



Squallyuk
Female
Cancer
45
52
Dancer
Throw
Speed Save
Halve MP
Move-HP Up

Cute Bag

Holy Miter
Light Robe
Small Mantle

Wiznaibus, Obsidian Blade
Shuriken



VVitchFaye
Female
Aries
47
47
Summoner
Charge
MP Restore
Equip Shield
Move+2

Poison Rod
Kaiser Plate
Leather Hat
Wizard Robe
Power Wrist

Moogle, Ifrit, Titan, Salamander
Charge+2, Charge+5, Charge+7, Charge+10



Bahumat989
Monster
Sagittarius
57
69
Black Chocobo








Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Mesmaster
Male
Cancer
69
38
Knight
Jump
Counter Tackle
Short Status
Retreat

Ragnarok
Gold Shield
Barbuta
Gold Armor
Chantage

Head Break, Magic Break, Night Sword
Level Jump3, Vertical Jump5



HASTERIOUS
Female
Virgo
65
47
Summoner
Punch Art
Critical Quick
Short Charge
Move-HP Up

Rod

Green Beret
Black Robe
Elf Mantle

Moogle, Titan, Carbunkle, Bahamut, Lich
Spin Fist, Wave Fist, Purification, Revive, Seal Evil



Regios91
Male
Sagittarius
68
62
Time Mage
Sing
Abandon
Equip Shield
Move+3

Oak Staff
Gold Shield
Green Beret
Black Robe
Defense Ring

Slow, Stop, Immobilize, Float, Stabilize Time
Last Song, Sky Demon



Lord Gwarth
Male
Virgo
68
79
Lancer
Summon Magic
Dragon Spirit
Dual Wield
Move+2

Holy Lance
Obelisk
Circlet
Wizard Robe
Cherche

Level Jump8, Vertical Jump4
Moogle, Ifrit, Bahamut, Salamander

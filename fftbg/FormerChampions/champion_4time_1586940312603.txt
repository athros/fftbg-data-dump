Player: !zChamp
Team: Champion Team
Palettes: Black/Red



RubenFlonne
Monster
Cancer
43
70
Red Chocobo










DHaveWord
Male
Taurus
73
58
Knight
Talk Skill
Critical Quick
Attack UP
Move-MP Up

Chaos Blade
Kaiser Plate
Leather Helmet
Chain Mail
Power Wrist

Shield Break, Night Sword, Surging Sword
Negotiate, Mimic Daravon



Willwagnermusic
Female
Libra
70
60
Thief
Throw
Counter Flood
Equip Gun
Ignore Terrain

Mythril Gun

Holy Miter
Chain Vest
Defense Ring

Steal Accessory, Steal Status, Arm Aim, Leg Aim
Staff



Tithonus
Male
Leo
70
40
Lancer
Steal
Parry
Long Status
Jump+2

Dragon Whisker
Gold Shield
Platinum Helmet
Black Robe
Leather Mantle

Level Jump2, Vertical Jump2
Steal Armor, Steal Accessory, Arm Aim

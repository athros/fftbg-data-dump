Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Old Overholt
Female
Virgo
73
46
Priest
Summon Magic
Parry
Short Charge
Move+2

White Staff

Triangle Hat
Chameleon Robe
Small Mantle

Cure 3, Raise, Protect 2, Shell 2, Wall, Esuna, Magic Barrier
Moogle, Ramuh, Titan, Golem, Carbunkle, Fairy



Kronikle
Male
Aries
76
72
Knight
White Magic
Absorb Used MP
Short Charge
Move-HP Up

Defender
Round Shield
Platinum Helmet
Mythril Armor
Cherche

Weapon Break, Speed Break, Power Break, Justice Sword
Cure, Cure 2, Reraise, Protect



Maakur
Female
Capricorn
67
63
Chemist
White Magic
Faith Save
Dual Wield
Jump+2

Cute Bag
Hydra Bag
Headgear
Rubber Costume
Diamond Armlet

Ether, Hi-Ether, Echo Grass, Holy Water, Remedy, Phoenix Down
Cure, Raise, Raise 2, Wall, Esuna, Holy



Wonser
Male
Scorpio
76
74
Oracle
Black Magic
Speed Save
Martial Arts
Swim

Gokuu Rod

Cachusha
Power Sleeve
Defense Armlet

Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify
Fire, Flare

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Grasnikk
Female
Capricorn
63
74
Chemist
Basic Skill
Counter
Defend
Move-MP Up

Blaze Gun

Golden Hairpin
Power Sleeve
Cursed Ring

X-Potion, Eye Drop, Maiden's Kiss, Phoenix Down
Dash, Heal, Tickle, Fury



King Smashington
Male
Libra
46
62
Lancer
Throw
Auto Potion
Sicken
Waterwalking

Holy Lance
Hero Shield
Circlet
Gold Armor
Battle Boots

Level Jump8, Vertical Jump6
Shuriken, Knife



DarrenDinosaurs
Female
Gemini
78
79
Time Mage
White Magic
Distribute
Short Charge
Jump+1

Wizard Staff

Leather Hat
Earth Clothes
Wizard Mantle

Stop, Immobilize, Float, Reflect
Cure, Cure 3, Raise, Raise 2, Shell 2, Esuna



DeathTaxesAndAnime
Female
Aries
54
53
Thief
Throw
PA Save
Concentrate
Move+3

Sasuke Knife

Green Beret
Clothes
Defense Ring

Gil Taking, Steal Armor, Steal Status
Shuriken, Sword, Staff, Axe

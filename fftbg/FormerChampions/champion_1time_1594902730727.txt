Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Evewho
Female
Pisces
71
47
Chemist
Time Magic
HP Restore
Equip Shield
Ignore Terrain

Cute Bag
Platinum Shield
Golden Hairpin
Mythril Vest
Cursed Ring

Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Slow 2, Quick, Demi



Superdevon1
Female
Virgo
43
68
Oracle
Punch Art
Hamedo
Concentrate
Jump+1

Whale Whisker

Flash Hat
White Robe
Genji Gauntlet

Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Petrify
Spin Fist, Earth Slash, Purification, Revive



Choco Joe
Female
Aries
68
63
Calculator
Black Magic
PA Save
Sicken
Jump+3

Papyrus Codex

Red Hood
Earth Clothes
Salty Rage

CT, Height, 5, 4, 3
Fire 2, Fire 4, Bolt, Bolt 2, Ice 2



Smokegiant
Male
Capricorn
50
80
Summoner
Yin Yang Magic
Caution
Maintenance
Jump+2

Wizard Staff

Flash Hat
Linen Robe
Spike Shoes

Moogle, Ifrit, Titan, Salamander, Fairy
Pray Faith, Doubt Faith, Zombie, Dispel Magic

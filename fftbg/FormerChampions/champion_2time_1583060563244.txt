Player: !zChamp
Team: Champion Team
Palettes: Black/Red



R Raynos
Male
Scorpio
58
75
Ninja
Yin Yang Magic
Auto Potion
Concentrate
Lava Walking

Morning Star
Short Edge
Flash Hat
Earth Clothes
Elf Mantle

Shuriken, Bomb, Knife, Dictionary
Poison, Spell Absorb, Pray Faith, Foxbird, Confusion Song, Paralyze, Sleep



DaveStrider55
Male
Pisces
66
53
Oracle
Punch Art
Speed Save
Short Charge
Swim

Ivory Rod

Red Hood
Chameleon Robe
Leather Mantle

Blind, Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song
Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive



Fspll
Male
Virgo
75
57
Archer
Punch Art
Damage Split
Doublehand
Fly

Poison Bow

Twist Headband
Wizard Outfit
Sprint Shoes

Charge+2, Charge+4, Charge+5
Pummel, Secret Fist, Purification, Revive



Musclestache
Male
Aries
51
41
Lancer
Elemental
Auto Potion
Dual Wield
Ignore Height

Javelin
Holy Lance
Barbuta
Genji Armor
Germinas Boots

Level Jump2, Vertical Jump5
Hallowed Ground, Static Shock, Blizzard, Gusty Wind

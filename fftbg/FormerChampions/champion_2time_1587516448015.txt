Player: !zChamp
Team: Champion Team
Palettes: Green/White



DeathTaxesAndAnime
Female
Aquarius
65
60
Samurai
White Magic
Abandon
Defense UP
Move+3

Spear

Barbuta
Light Robe
Salty Rage

Asura
Cure 2, Cure 4, Raise, Raise 2, Regen, Wall, Esuna, Holy



Lordminsc
Male
Capricorn
51
56
Knight
Basic Skill
MP Restore
Dual Wield
Waterwalking

Giant Axe
Iron Sword
Gold Helmet
Linen Robe
Dracula Mantle

Armor Break, Shield Break, Power Break, Night Sword
Accumulate, Heal, Cheer Up



Elkydeluxe
Female
Aquarius
60
76
Knight
Jump
Brave Up
Equip Knife
Levitate

Mage Masher
Crystal Shield
Crystal Helmet
Mythril Armor
Genji Gauntlet

Shield Break, Weapon Break, Speed Break, Stasis Sword
Level Jump2, Vertical Jump6



Dictatorhowells
Male
Cancer
76
80
Priest
Draw Out
MA Save
Long Status
Levitate

Flame Whip

Holy Miter
Robe of Lords
Setiemson

Cure, Cure 2, Raise, Raise 2, Protect, Protect 2, Shell, Esuna
Heaven's Cloud

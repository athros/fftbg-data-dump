Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Artea
Male
Cancer
74
48
Ninja
Talk Skill
Caution
Equip Polearm
Waterwalking

Dragon Whisker
Ivory Rod
Twist Headband
Brigandine
Cursed Ring

Shuriken, Bomb
Persuade, Praise, Solution, Insult, Refute



LuxorDoesntFrame
Female
Libra
65
49
Summoner
Jump
Counter Flood
Defend
Teleport

Oak Staff

Flash Hat
Light Robe
Angel Ring

Moogle, Shiva, Leviathan, Salamander, Silf
Level Jump3, Vertical Jump7



Lunaeriss
Female
Scorpio
60
69
Wizard
Basic Skill
MP Restore
Equip Polearm
Swim

Ryozan Silk

Red Hood
Wizard Outfit
Power Wrist

Fire 3, Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice 2, Empower
Heal, Tickle, Cheer Up, Wish



ArcHeRHooD
Female
Virgo
45
78
Thief
Throw
Speed Save
Equip Axe
Jump+2

Assassin Dagger

Leather Hat
Brigandine
Magic Ring

Arm Aim
Shuriken, Bomb, Knife, Spear, Dictionary

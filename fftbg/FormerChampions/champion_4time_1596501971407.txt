Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



E Ballard
Male
Capricorn
63
56
Calculator
Black Magic
Caution
Equip Polearm
Swim

Ryozan Silk

Leather Hat
Secret Clothes
Spike Shoes

CT, Height, Prime Number, 4
Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Flare



StealthModeLocke
Male
Gemini
50
66
Mime

Arrow Guard
Defense UP
Retreat



Black Hood
Rubber Costume
Dracula Mantle

Mimic




DeathTaxesAndAnime
Female
Capricorn
58
53
Mime

Counter
Martial Arts
Move+3


Escutcheon
Leather Hat
Wizard Outfit
Feather Boots

Mimic




Blorpy
Male
Capricorn
76
63
Bard
Jump
Arrow Guard
Dual Wield
Teleport

Fairy Harp
Fairy Harp
Feather Hat
Mythril Armor
Reflect Ring

Cheer Song, Battle Song
Level Jump4, Vertical Jump3

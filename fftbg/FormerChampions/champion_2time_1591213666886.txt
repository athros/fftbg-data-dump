Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Nok
Female
Aquarius
72
67
Monk
Item
Mana Shield
Equip Gun
Teleport

Stone Gun

Black Hood
Leather Outfit
Diamond Armlet

Spin Fist, Pummel, Seal Evil
Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Lythe Caraker
Male
Scorpio
64
56
Priest
Punch Art
Regenerator
Long Status
Teleport

Flame Whip

Ribbon
Silk Robe
Wizard Mantle

Cure 3, Cure 4, Raise, Shell 2, Wall, Holy
Earth Slash, Purification, Chakra



Lastly
Monster
Cancer
78
81
Wyvern










Firesheath
Female
Aries
58
75
Geomancer
Punch Art
MP Restore
Equip Armor
Waterwalking

Giant Axe
Diamond Shield
Cross Helmet
Clothes
Spike Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive

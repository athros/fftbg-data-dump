Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



SephDarkheart
Male
Virgo
43
67
Chemist
Steal
Hamedo
Defend
Move-HP Up

Cute Bag

Flash Hat
Mystic Vest
Leather Mantle

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Phoenix Down
Gil Taking, Steal Accessory, Arm Aim



DeathTaxesAndAnime
Female
Aries
45
59
Mime

PA Save
Martial Arts
Teleport


Ice Shield
Leather Hat
Judo Outfit
Bracer

Mimic




Puls3evo
Female
Cancer
48
41
Mediator
Jump
Parry
Martial Arts
Lava Walking



Black Hood
Chain Vest
Reflect Ring

Persuade, Praise, Threaten, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Level Jump4, Vertical Jump4



DesertWooder
Female
Sagittarius
65
58
Calculator
Black Magic
Counter Flood
Equip Bow
Move+2

Long Bow

Twist Headband
Black Costume
Feather Mantle

CT, Height, Prime Number, 5, 4
Fire 2, Bolt 2, Ice, Ice 3, Empower, Frog

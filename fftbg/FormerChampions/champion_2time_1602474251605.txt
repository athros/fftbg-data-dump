Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Mirapoix
Male
Scorpio
53
60
Monk
Steal
Caution
Doublehand
Levitate



Holy Miter
Leather Outfit
Rubber Shoes

Earth Slash, Purification, Revive
Steal Heart, Steal Status



RunicMagus
Female
Cancer
68
53
Geomancer
Black Magic
Mana Shield
Long Status
Ignore Height

Battle Axe
Escutcheon
Triangle Hat
Wizard Robe
Rubber Shoes

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Fire 2, Fire 3, Bolt, Bolt 2, Ice 3, Frog



Jehudy7
Female
Cancer
73
66
Lancer
Time Magic
MP Restore
Short Status
Move+3

Javelin
Crystal Shield
Crystal Helmet
White Robe
Rubber Shoes

Level Jump8, Vertical Jump4
Haste, Immobilize, Demi, Demi 2, Stabilize Time



CorpusCav
Monster
Capricorn
53
68
Apanda








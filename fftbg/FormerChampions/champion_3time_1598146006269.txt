Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Error72
Female
Leo
57
43
Mime

Counter Flood
Defend
Retreat



Crystal Helmet
Brigandine
Leather Mantle

Mimic




DAC169
Female
Aries
56
43
Ninja
Time Magic
Meatbone Slash
Magic Defense UP
Move+1

Morning Star
Spell Edge
Triangle Hat
Black Costume
Red Shoes

Stick
Haste, Slow 2, Quick, Stabilize Time



Jjhull3
Male
Capricorn
45
61
Lancer
Draw Out
Mana Shield
Equip Armor
Move+2

Mythril Spear
Buckler
Holy Miter
Chain Vest
Germinas Boots

Level Jump4, Vertical Jump6
Asura, Koutetsu, Murasame, Kikuichimoji, Chirijiraden



LordTomS
Male
Gemini
60
67
Samurai
Time Magic
Mana Shield
Long Status
Ignore Terrain

Javelin

Genji Helmet
Leather Armor
108 Gems

Koutetsu, Muramasa, Kikuichimoji
Haste, Slow, Reflect

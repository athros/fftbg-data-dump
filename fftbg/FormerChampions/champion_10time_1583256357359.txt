Player: !zChamp
Team: Champion Team
Palettes: Green/White



Noopockets
Monster
Aquarius
45
67
Explosive










PKayge
Male
Capricorn
59
50
Monk
Draw Out
Counter Tackle
Attack UP
Lava Walking



Triangle Hat
Mythril Vest
Defense Ring

Spin Fist, Pummel, Secret Fist, Chakra, Revive, Seal Evil
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Masamune



Twelfthrootoftwo
Female
Serpentarius
59
81
Ninja
Basic Skill
HP Restore
Equip Gun
Move-MP Up

Blast Gun
Blaze Gun
Flash Hat
Wizard Outfit
Leather Mantle

Shuriken
Heal, Tickle, Scream



RongRongArts
Male
Leo
46
43
Geomancer
Battle Skill
Faith Up
Equip Gun
Move+3

Ramia Harp
Ice Shield
Thief Hat
Judo Outfit
Battle Boots

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Lava Ball
Head Break, Armor Break, Shield Break, Magic Break, Mind Break, Justice Sword

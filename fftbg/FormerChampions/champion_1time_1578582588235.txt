Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Zarialei
Female
Leo
64
68
Dancer
Steal
Critical Quick
Short Charge
Move+3

Cashmere

Green Beret
Brigandine
108 Gems

Witch Hunt, Slow Dance, Disillusion
Gil Taking, Steal Heart, Steal Helmet, Steal Armor



Tate
Female
Taurus
80
62
Archer
Item
Absorb Used MP
Equip Gun
Move+3

Stone Gun
Round Shield
Green Beret
Leather Vest
Leather Mantle

Charge+1, Charge+10
Potion, Hi-Ether, Antidote, Echo Grass, Soft, Holy Water



Ali
Female
Aries
67
52
Thief
Yin Yang Magic
Counter Flood
Equip Shield
Move+2

Orichalcum
Crystal Shield
Leather Hat
Leather Outfit
Magic Ring

Steal Weapon
Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Foxbird, Dispel Magic



Byron
Male
Cancer
68
71
Monk
White Magic
MA Save
Equip Sword
Retreat

Muramasa

Feather Hat
Earth Clothes
Germinas Boots

Wave Fist, Purification, Seal Evil
Raise, Raise 2, Wall

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Duckfist
Male
Cancer
70
48
Samurai
White Magic
Parry
Defend
Move-MP Up

Dragon Whisker

Genji Helmet
Linen Robe
Spike Shoes

Koutetsu, Murasame, Kiyomori, Kikuichimoji
Cure, Cure 2, Cure 3, Raise, Wall, Esuna, Holy



SeniorBunk
Monster
Aquarius
71
54
Serpentarius










Belife42
Female
Gemini
63
48
Mime

Faith Up
Secret Hunt
Retreat



Green Beret
Mystic Vest
Angel Ring

Mimic




Chompie
Female
Capricorn
44
59
Calculator
Yin Yang Magic
Counter Magic
Equip Bow
Jump+2

Mythril Bow

Barette
Earth Clothes
Germinas Boots

CT, Prime Number, 4, 3
Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify

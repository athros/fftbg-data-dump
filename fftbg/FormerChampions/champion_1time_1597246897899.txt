Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Douchetron
Male
Capricorn
63
47
Lancer
Throw
Counter
Short Charge
Move-MP Up

Partisan
Diamond Shield
Barbuta
Carabini Mail
Reflect Ring

Level Jump8, Vertical Jump7
Staff



Benticore
Female
Leo
79
57
Summoner
Throw
Counter
Short Charge
Move+3

Sage Staff

Holy Miter
Wizard Robe
Reflect Ring

Moogle, Shiva, Ramuh, Odin, Silf, Fairy, Lich
Shuriken



ThanatosXRagnarok
Monster
Cancer
63
67
Chocobo










Mrfripps
Female
Aquarius
79
66
Lancer
White Magic
Mana Shield
Halve MP
Jump+1

Mythril Spear
Genji Shield
Mythril Helmet
Crystal Mail
Germinas Boots

Level Jump2, Vertical Jump6
Cure, Cure 4, Reraise, Protect 2, Esuna

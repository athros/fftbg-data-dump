Player: !zChamp
Team: Champion Team
Palettes: Black/Red



RigidWolf
Male
Sagittarius
62
73
Ninja
Item
Catch
Attack UP
Move+3

Ninja Edge
Blind Knife
Twist Headband
Mythril Vest
Jade Armlet

Bomb, Knife, Stick
Hi-Potion, X-Potion, Antidote, Phoenix Down



Ruvelia BibeI
Female
Capricorn
46
48
Lancer
Item
Regenerator
Throw Item
Jump+1

Javelin
Crystal Shield
Bronze Helmet
White Robe
Germinas Boots

Level Jump5, Vertical Jump8
Potion, X-Potion, Hi-Ether, Elixir, Eye Drop, Soft, Remedy, Phoenix Down



Veekose
Male
Libra
44
59
Monk
Yin Yang Magic
Caution
Dual Wield
Move+1



Headgear
Clothes
Feather Boots

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Seal Evil
Blind, Poison, Life Drain, Dispel Magic, Sleep



Ruined15
Male
Taurus
73
47
Mediator
Battle Skill
Auto Potion
Magic Attack UP
Jump+1

Papyrus Codex

Golden Hairpin
Linen Robe
Leather Mantle

Praise, Threaten, Death Sentence, Negotiate, Mimic Daravon, Refute
Head Break, Magic Break

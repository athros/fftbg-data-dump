Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lodrak
Male
Cancer
60
74
Knight
Item
HP Restore
Throw Item
Move+3

Defender
Aegis Shield
Iron Helmet
Chameleon Robe
Battle Boots

Head Break, Shield Break, Power Break, Mind Break
Potion, Hi-Ether, Holy Water



LDSkinny
Male
Capricorn
50
59
Time Mage
Draw Out
Faith Up
Defense UP
Jump+1

White Staff

Leather Hat
Brigandine
Jade Armlet

Haste, Haste 2, Reflect, Quick, Demi 2, Stabilize Time
Koutetsu, Bizen Boat, Heaven's Cloud



Insonder
Male
Aquarius
50
53
Monk
Elemental
Dragon Spirit
Attack UP
Waterbreathing



Leather Hat
Black Costume
Jade Armlet

Spin Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



LordSDB
Male
Aries
70
81
Calculator
Robosnake Skill
Damage Split
Equip Armor
Ignore Height

Papyrus Codex
Buckler
Gold Helmet
Mythril Armor
Feather Mantle

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



TheGuesty
Male
Cancer
55
58
Monk
Time Magic
Dragon Spirit
Dual Wield
Fly



Thief Hat
Chain Vest
N-Kai Armlet

Earth Slash, Purification, Revive
Slow 2, Stop, Immobilize, Float, Stabilize Time, Meteor



DavenIII
Male
Pisces
67
73
Knight
Jump
Damage Split
Sicken
Waterwalking

Ragnarok
Hero Shield
Crystal Helmet
White Robe
Red Shoes

Head Break, Armor Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Level Jump8, Vertical Jump8



Gum44
Male
Gemini
71
48
Monk
Steal
MP Restore
Long Status
Ignore Terrain



Triangle Hat
Black Costume
Dracula Mantle

Purification
Gil Taking, Steal Weapon



Shalloween
Female
Scorpio
52
57
Mime

HP Restore
Dual Wield
Move-MP Up



Twist Headband
Mythril Vest
Battle Boots

Mimic


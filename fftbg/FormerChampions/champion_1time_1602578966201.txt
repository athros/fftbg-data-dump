Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



YowaneHaku
Male
Serpentarius
55
38
Knight
Yin Yang Magic
Parry
Dual Wield
Move+2

Giant Axe
Giant Axe
Grand Helmet
Plate Mail
Magic Ring

Head Break, Shield Break, Power Break, Justice Sword
Spell Absorb, Life Drain, Pray Faith, Blind Rage, Dark Holy



Deeps One
Male
Gemini
74
61
Bard
Summon Magic
Faith Save
Equip Gun
Lava Walking

Blast Gun

Feather Hat
Crystal Mail
Magic Gauntlet

Life Song, Hydra Pit
Carbunkle, Salamander, Fairy, Cyclops



Lolthsmeat
Male
Virgo
56
51
Bard
Summon Magic
Arrow Guard
Concentrate
Move+2

Lightning Bow

Triangle Hat
Carabini Mail
Small Mantle

Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Diamond Blade, Sky Demon
Moogle, Golem, Carbunkle, Bahamut, Odin



Genkidou
Male
Pisces
46
39
Ninja
Draw Out
Counter Magic
Defend
Move-MP Up

Kunai
Spell Edge
Triangle Hat
Chain Vest
Setiemson

Shuriken
Murasame, Kiyomori, Muramasa, Chirijiraden

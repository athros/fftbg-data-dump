Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Sarine
Female
Cancer
80
80
Mime

Absorb Used MP
Defense UP
Retreat



Leather Hat
Leather Outfit
Angel Ring

Mimic




Melodee
Monster
Cancer
52
46
Steel Giant










Morlee
Male
Leo
49
61
Priest
Jump
Brave Up
Martial Arts
Jump+3



Flash Hat
Chameleon Robe
Feather Mantle

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell
Level Jump5, Vertical Jump2



Loleta
Female
Aries
78
46
Mediator
Item
Damage Split
Secret Hunt
Ignore Terrain

Mage Masher

Holy Miter
White Robe
Jade Armlet

Praise, Insult, Refute, Rehabilitate
Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down

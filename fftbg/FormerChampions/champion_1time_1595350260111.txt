Player: !zChamp
Team: Champion Team
Palettes: Green/White



CapnChaos12
Male
Scorpio
52
65
Archer
Jump
Auto Potion
Doublehand
Ignore Terrain

Romanda Gun

Iron Helmet
Brigandine
Elf Mantle

Charge+1, Charge+4, Charge+5
Level Jump2, Vertical Jump2



Dexef
Male
Aries
80
80
Chemist
Draw Out
Speed Save
Equip Axe
Waterbreathing

Gold Staff

Green Beret
Clothes
Cursed Ring

Potion, Hi-Potion, Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Kiyomori, Kikuichimoji, Chirijiraden



TasisSai
Female
Taurus
74
56
Wizard
Steal
MA Save
Maintenance
Ignore Height

Dragon Rod

Green Beret
Silk Robe
Defense Armlet

Bolt, Bolt 3, Bolt 4, Ice 2, Ice 4, Empower
Steal Heart, Steal Weapon, Steal Status, Arm Aim, Leg Aim



Lord Burrah
Male
Taurus
62
41
Archer
Punch Art
Arrow Guard
Equip Shield
Fly

Poison Bow
Platinum Shield
Feather Hat
Judo Outfit
Magic Ring

Charge+1, Charge+3, Charge+5, Charge+20
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Revive

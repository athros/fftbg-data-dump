Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Reddwind
Male
Cancer
51
76
Knight
Black Magic
Abandon
Equip Polearm
Move-MP Up

Ryozan Silk
Gold Shield
Platinum Helmet
Mythril Armor
Sprint Shoes

Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Explosion Sword
Fire, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Death



Quadh0nk
Female
Taurus
79
48
Dancer
Throw
Counter
Concentrate
Retreat

Cashmere

Holy Miter
White Robe
Red Shoes

Witch Hunt, Wiznaibus, Last Dance, Void Storage
Shuriken, Ninja Sword



WitchHunterIX
Female
Cancer
45
46
Summoner
Battle Skill
Counter Tackle
Short Charge
Jump+2

Wizard Staff

Headgear
White Robe
Salty Rage

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Silf, Lich
Head Break, Weapon Break, Surging Sword



HaychDub
Female
Sagittarius
80
70
Summoner
Draw Out
Regenerator
Short Charge
Move-MP Up

Wizard Staff

Golden Hairpin
Secret Clothes
Dracula Mantle

Titan, Bahamut, Leviathan
Asura, Koutetsu

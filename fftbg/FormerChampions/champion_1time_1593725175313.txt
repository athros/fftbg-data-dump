Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



OtherBrand
Female
Pisces
64
80
Mediator
Charge
PA Save
Short Status
Jump+1

Romanda Gun

Green Beret
Black Robe
Small Mantle

Invitation, Persuade, Preach, Insult, Refute
Charge+7, Charge+20



Nifboy
Female
Scorpio
65
40
Priest
Math Skill
Critical Quick
Defend
Teleport

Gold Staff

Holy Miter
Chain Vest
Jade Armlet

Cure 4, Raise, Raise 2, Reraise, Protect 2, Esuna
CT, Height, 5, 4, 3



OneHundredFists
Male
Taurus
48
48
Mime

Speed Save
Dual Wield
Move-HP Up



Feather Hat
Chain Vest
Bracer

Mimic




Shalloween
Male
Aries
52
53
Monk
Charge
Meatbone Slash
Dual Wield
Jump+2



Holy Miter
Adaman Vest
Dracula Mantle

Pummel, Earth Slash, Purification, Chakra, Seal Evil
Charge+1, Charge+4

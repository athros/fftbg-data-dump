Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Maeveen
Female
Scorpio
70
64
Priest
Draw Out
Hamedo
Short Status
Ignore Terrain

Rainbow Staff

Golden Hairpin
Wizard Robe
Genji Gauntlet

Cure, Cure 3, Raise, Raise 2, Shell, Shell 2, Wall, Esuna
Asura, Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji



Coralreeferz
Male
Pisces
46
51
Geomancer
Battle Skill
Catch
Beastmaster
Ignore Terrain

Battle Axe
Ice Shield
Red Hood
Light Robe
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Shield Break, Speed Break, Power Break, Mind Break, Stasis Sword



Zagorsek
Male
Virgo
77
67
Knight
Punch Art
Earplug
Equip Armor
Move-MP Up

Ragnarok

Thief Hat
Chameleon Robe
Bracer

Head Break, Magic Break, Justice Sword
Wave Fist, Earth Slash, Purification, Revive



Arku E
Female
Leo
72
57
Dancer
Time Magic
Faith Up
Short Charge
Ignore Terrain

Ryozan Silk

Thief Hat
Silk Robe
Battle Boots

Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Obsidian Blade
Haste, Haste 2, Immobilize, Float, Reflect, Quick, Meteor

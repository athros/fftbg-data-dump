Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Brokenknight201
Male
Serpentarius
66
68
Knight
Throw
Damage Split
Dual Wield
Move+3

Platinum Sword
Ancient Sword
Crystal Helmet
Black Robe
Leather Mantle

Head Break, Weapon Break
Shuriken, Spear, Wand



Upvla
Male
Virgo
81
78
Monk
Yin Yang Magic
Distribute
Attack UP
Move+3



Headgear
Earth Clothes
Feather Mantle

Spin Fist, Pummel, Purification, Chakra, Seal Evil
Blind Rage, Foxbird, Dispel Magic, Paralyze, Dark Holy



Maakur
Male
Taurus
59
70
Knight
White Magic
PA Save
Attack UP
Move-MP Up

Star Bag
Genji Shield
Leather Helmet
Reflect Mail
Defense Armlet

Head Break, Shield Break, Weapon Break
Cure, Cure 2, Cure 4, Raise, Reraise, Shell, Wall



Victoriolue
Female
Taurus
54
44
Geomancer
Battle Skill
Auto Potion
Equip Axe
Waterwalking

Morning Star
Flame Shield
Golden Hairpin
Linen Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Shield Break, Mind Break

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Fluffywormhole
Male
Pisces
55
64
Wizard
Steal
Arrow Guard
Magic Defense UP
Waterwalking

Ice Rod

Flash Hat
Black Robe
Wizard Mantle

Fire 2, Bolt 3, Ice, Empower, Flare
Gil Taking, Steal Armor, Steal Weapon



Cryptopsy70
Male
Capricorn
45
47
Bard
Charge
Earplug
Equip Shield
Jump+3

Ramia Harp
Gold Shield
Red Hood
Carabini Mail
Wizard Mantle

Magic Song
Charge+1, Charge+2, Charge+20



ApplesauceBoss
Male
Aries
75
55
Bard
Basic Skill
Catch
Equip Sword
Ignore Height

Save the Queen

Holy Miter
Power Sleeve
Feather Boots

Angel Song, Cheer Song, Last Song
Accumulate, Heal, Tickle, Scream



LanseDM
Male
Capricorn
80
63
Mime

HP Restore
Martial Arts
Waterbreathing



Triangle Hat
Judo Outfit
Spike Shoes

Mimic


Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Easton
Male
Aries
53
44
Squire
Item
Regenerator
Secret Hunt
Move-HP Up

Giant Axe

Black Hood
Linen Robe
Feather Boots

Accumulate, Dash, Throw Stone, Heal, Tickle, Cheer Up
Potion, Ether, Hi-Ether, Antidote, Soft, Holy Water



Abdel
Male
Pisces
75
75
Mediator
Summon Magic
Catch
Dual Wield
Move-HP Up

Mythril Gun
Glacier Gun
Flash Hat
Leather Vest
Germinas Boots

Threaten, Preach, Negotiate, Mimic Daravon, Refute
Ramuh, Golem, Salamander, Fairy



Estelle
Female
Aquarius
80
51
Mediator
Draw Out
Blade Grasp
Magic Attack UP
Fly

Mage Masher

Cachusha
Light Robe
Feather Boots

Invitation, Mimic Daravon, Refute
Koutetsu, Heaven's Cloud, Muramasa



Loreen
Female
Capricorn
43
65
Calculator
Time Magic
Counter
Halve MP
Swim

Iron Fan

Golden Hairpin
Mythril Vest
Spike Shoes

CT, Prime Number, 5, 4
Haste, Slow 2, Stop, Demi, Stabilize Time



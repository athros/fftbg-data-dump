Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Phi Sig
Male
Capricorn
47
76
Chemist
White Magic
Auto Potion
Equip Polearm
Move-MP Up

Gokuu Rod

Feather Hat
Judo Outfit
Red Shoes

X-Potion, Eye Drop, Holy Water, Phoenix Down
Shell 2, Esuna



Evewho
Female
Cancer
73
74
Oracle
Math Skill
Brave Save
Doublehand
Retreat

Octagon Rod

Cachusha
Judo Outfit
Bracer

Poison, Pray Faith, Silence Song, Dispel Magic
CT, Prime Number, 4



Flacococo
Female
Pisces
62
76
Squire
Draw Out
Mana Shield
Equip Knife
Fly

Ice Rod
Platinum Shield
Black Hood
Crystal Mail
Reflect Ring

Tickle, Fury, Wish, Scream
Koutetsu, Heaven's Cloud, Muramasa



TheMurkGnome
Female
Leo
59
41
Summoner
Item
Blade Grasp
Magic Attack UP
Waterwalking

Papyrus Codex

Golden Hairpin
Brigandine
Magic Ring

Moogle, Ifrit, Lich, Cyclops
Potion, X-Potion, Antidote, Maiden's Kiss, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Green/White



DamnThatShark
Male
Taurus
57
45
Monk
Charge
Arrow Guard
Equip Gun
Waterwalking

Bestiary

Headgear
Adaman Vest
Genji Gauntlet

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Charge+3, Charge+10, Charge+20



Creggers
Male
Libra
52
65
Ninja
White Magic
Hamedo
Short Charge
Lava Walking

Sasuke Knife
Short Edge
Holy Miter
Chain Vest
Bracer

Shuriken
Cure 2, Raise, Raise 2, Reraise, Protect, Shell 2, Esuna



Randgridr
Male
Pisces
69
58
Monk
Time Magic
Auto Potion
Halve MP
Retreat



Black Hood
Clothes
Angel Ring

Spin Fist, Earth Slash, Purification, Chakra, Revive
Haste, Haste 2, Slow, Immobilize, Quick



Ohhinm
Male
Taurus
52
77
Archer
Time Magic
Parry
Equip Axe
Ignore Terrain

Flail
Gold Shield
Triangle Hat
Rubber Costume
Defense Armlet

Charge+1, Charge+2, Charge+7, Charge+20
Haste, Haste 2, Slow 2, Quick, Meteor

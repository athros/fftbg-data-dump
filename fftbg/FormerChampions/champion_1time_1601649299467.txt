Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Heropong
Male
Virgo
59
64
Priest
Time Magic
Counter Magic
Magic Defense UP
Move-HP Up

Flame Whip

Holy Miter
Earth Clothes
Spike Shoes

Cure, Cure 3, Raise, Regen, Shell, Esuna
Haste, Slow 2, Immobilize, Float, Reflect, Demi



SephDarkheart
Female
Leo
43
65
Wizard
Item
Counter Flood
Short Charge
Jump+3

Dragon Rod

Cachusha
Mythril Vest
Spike Shoes

Fire, Ice, Ice 4, Empower, Frog, Flare
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down



CassiePhoenix
Male
Libra
79
62
Ninja
Jump
Mana Shield
Concentrate
Retreat

Hidden Knife
Flame Whip
Headgear
Power Sleeve
Feather Boots

Bomb, Wand, Dictionary
Level Jump8, Vertical Jump2



ColetteMSLP
Male
Aries
44
43
Chemist
Draw Out
Faith Save
Maintenance
Ignore Terrain

Mythril Gun

Holy Miter
Rubber Costume
Battle Boots

Potion, Hi-Potion, Ether, Eye Drop
Asura, Koutetsu, Murasame

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Latebit
Male
Scorpio
46
78
Archer
Yin Yang Magic
Parry
Equip Axe
Retreat

Flail
Diamond Shield
Golden Hairpin
Leather Outfit
Dracula Mantle

Charge+1, Charge+7, Charge+20
Poison, Doubt Faith, Dispel Magic, Paralyze, Sleep



Just Here2
Female
Virgo
45
79
Mime

Catch
Doublehand
Jump+1



Circlet
Brigandine
Wizard Mantle

Mimic




Laserman1000
Male
Aquarius
66
62
Samurai
Jump
MP Restore
Magic Attack UP
Move+3

Koutetsu Knife

Genji Helmet
Mythril Armor
Sprint Shoes

Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji, Masamune, Chirijiraden
Level Jump8, Vertical Jump6



Nhammen
Male
Aquarius
76
65
Knight
Black Magic
Abandon
Attack UP
Jump+1

Iron Sword
Ice Shield
Crystal Helmet
Chain Mail
Small Mantle

Head Break, Armor Break, Weapon Break, Magic Break, Speed Break
Fire 2, Fire 3, Fire 4, Bolt, Bolt 4, Ice 2, Ice 3, Empower

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Zachara
Female
Virgo
65
42
Squire
Draw Out
Dragon Spirit
Beastmaster
Swim

Long Sword
Ice Shield
Leather Hat
Carabini Mail
Angel Ring

Throw Stone, Heal, Yell, Wish, Scream
Asura, Murasame



TheChainNerd
Female
Capricorn
45
75
Time Mage
Punch Art
Counter Magic
Equip Armor
Teleport

Battle Folio

Gold Helmet
Carabini Mail
Jade Armlet

Slow 2, Stop, Float, Reflect, Demi, Demi 2, Stabilize Time
Wave Fist, Secret Fist, Purification, Revive



FeDoran
Male
Pisces
47
49
Summoner
White Magic
Catch
Long Status
Fly

Gold Staff

Red Hood
Black Robe
Diamond Armlet

Ramuh, Golem, Carbunkle, Leviathan, Lich
Raise, Regen, Protect, Shell, Wall, Holy



CapnChaos12
Male
Libra
54
58
Lancer
Punch Art
Caution
Beastmaster
Move+2

Holy Lance
Mythril Shield
Gold Helmet
Mythril Armor
Rubber Shoes

Level Jump5, Vertical Jump5
Secret Fist

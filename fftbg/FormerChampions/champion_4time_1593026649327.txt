Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Zeroroute
Female
Taurus
73
76
Summoner
Charge
Damage Split
Magic Attack UP
Swim

Oak Staff

Triangle Hat
White Robe
Leather Mantle

Shiva, Ramuh, Carbunkle, Bahamut, Silf, Fairy, Lich
Charge+1, Charge+4



Lyner87
Male
Virgo
59
55
Knight
Black Magic
Distribute
Long Status
Lava Walking

Excalibur
Round Shield
Iron Helmet
Chain Mail
Wizard Mantle

Head Break, Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Dark Sword
Fire 3, Bolt, Bolt 3, Bolt 4, Ice 3, Frog, Flare



SomthingMore
Female
Aries
53
64
Summoner
Dance
Faith Save
Equip Knife
Move+3

Orichalcum

Flash Hat
Black Robe
Reflect Ring

Moogle, Shiva, Ramuh, Bahamut, Leviathan, Silf, Fairy, Lich, Cyclops
Disillusion, Nameless Dance, Void Storage



Evdoggity
Female
Pisces
75
62
Dancer
Throw
Counter Flood
Martial Arts
Retreat

Ryozan Silk

Flash Hat
Light Robe
Defense Ring

Witch Hunt, Slow Dance, Disillusion
Shuriken, Bomb

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



SQUiDSQUARKLIN
Female
Cancer
43
59
Priest
Draw Out
Damage Split
Equip Polearm
Waterbreathing

Octagon Rod

Triangle Hat
Wizard Robe
Red Shoes

Raise, Raise 2, Protect, Protect 2, Shell, Wall, Esuna
Koutetsu, Murasame, Heaven's Cloud



Aldrammech
Female
Scorpio
37
63
Oracle
Draw Out
Counter Tackle
Dual Wield
Jump+2

Cypress Rod
Whale Whisker
Red Hood
Brigandine
Dracula Mantle

Blind, Blind Rage
Asura, Kikuichimoji



Firesheath
Male
Capricorn
66
49
Bard
Battle Skill
Counter Tackle
Maintenance
Move-HP Up

Fairy Harp

Triangle Hat
Maximillian
Spike Shoes

Life Song, Magic Song, Diamond Blade, Space Storage
Head Break, Mind Break



SephDarkheart
Female
Capricorn
44
53
Lancer
Dance
MP Restore
Dual Wield
Jump+1

Mythril Spear
Holy Lance
Gold Helmet
Light Robe
Jade Armlet

Level Jump8, Vertical Jump8
Wiznaibus, Slow Dance

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Vultuous
Female
Pisces
44
42
Summoner
Black Magic
Meatbone Slash
Short Charge
Move+2

Poison Rod

Holy Miter
Wizard Robe
Spike Shoes

Golem, Leviathan
Fire, Fire 4, Ice, Ice 2, Ice 3, Empower



ALY327
Male
Scorpio
48
59
Lancer
Time Magic
Speed Save
Defend
Lava Walking

Holy Lance
Crystal Shield
Circlet
Crystal Mail
Jade Armlet

Level Jump3, Vertical Jump8
Haste, Haste 2, Slow, Reflect, Quick, Demi, Demi 2, Stabilize Time



Amiture
Male
Serpentarius
55
78
Summoner
White Magic
PA Save
Magic Attack UP
Jump+1

Dragon Rod

Green Beret
Silk Robe
108 Gems

Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Salamander
Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Protect 2, Shell



Chuckolator
Female
Scorpio
51
50
Calculator
Black Magic
Catch
Equip Shield
Lava Walking

Ice Rod
Bronze Shield
Flash Hat
Judo Outfit
Cursed Ring

CT, Height, Prime Number, 4
Fire, Bolt 2, Bolt 3, Ice, Ice 4, Frog, Flare

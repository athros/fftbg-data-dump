Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Buttkrakenn
Male
Gemini
58
80
Monk
Jump
Auto Potion
Short Charge
Move-MP Up



Flash Hat
Secret Clothes
Angel Ring

Pummel, Wave Fist, Purification, Chakra, Revive
Level Jump5, Vertical Jump2



Ekanselttar
Female
Serpentarius
81
43
Ninja
Charge
Earplug
Equip Sword
Ignore Terrain

Ancient Sword
Iron Sword
Red Hood
Mythril Vest
Leather Mantle

Shuriken
Charge+4



Virilikus
Male
Scorpio
53
61
Lancer
Elemental
Meatbone Slash
Maintenance
Move+2

Obelisk
Ice Shield
Diamond Helmet
Diamond Armor
Angel Ring

Level Jump3, Vertical Jump2
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Neohart
Female
Sagittarius
45
48
Thief
Black Magic
Regenerator
Equip Knife
Move+3

Air Knife

Barette
Leather Outfit
Wizard Mantle

Steal Heart, Steal Helmet, Steal Status, Leg Aim
Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Empower, Frog

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Marblescrew
Female
Taurus
71
48
Summoner
Time Magic
Counter Magic
Secret Hunt
Move-MP Up

Ice Rod

Triangle Hat
Black Robe
Leather Mantle

Moogle, Titan, Bahamut, Lich
Slow, Slow 2, Stop, Demi 2, Stabilize Time



Thunderducker
Male
Gemini
46
41
Ninja
Black Magic
Blade Grasp
Equip Sword
Swim

Long Sword
Sasuke Knife
Twist Headband
Secret Clothes
Small Mantle

Knife, Sword
Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 2



MattmanX311
Female
Scorpio
60
41
Squire
Talk Skill
Auto Potion
Sicken
Ignore Terrain

Hunting Bow

Black Hood
Wizard Outfit
Battle Boots

Accumulate, Heal, Tickle
Invitation, Preach, Solution, Mimic Daravon, Refute, Rehabilitate



BenYuPoker
Monster
Leo
55
61
Dark Behemoth








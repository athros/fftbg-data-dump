Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Reddash9
Male
Aquarius
50
51
Priest
Summon Magic
Arrow Guard
Equip Polearm
Move-MP Up

Mythril Spear

Green Beret
Adaman Vest
Cursed Ring

Raise 2, Protect 2, Shell, Shell 2, Esuna
Moogle, Titan, Odin, Leviathan, Salamander, Fairy



HaateXIII
Male
Gemini
76
64
Monk
Talk Skill
Damage Split
Concentrate
Lava Walking



Holy Miter
Secret Clothes
N-Kai Armlet

Earth Slash, Purification, Revive
Insult, Refute



WASD Buttons
Female
Aries
69
78
Samurai
Battle Skill
Parry
Equip Knife
Teleport

Dagger

Iron Helmet
Bronze Armor
Red Shoes

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Speed Break, Stasis Sword



Mesmaster
Female
Sagittarius
55
79
Samurai
Basic Skill
Speed Save
Defense UP
Ignore Height

Koutetsu Knife

Iron Helmet
White Robe
Magic Gauntlet

Koutetsu, Murasame, Heaven's Cloud, Masamune
Accumulate, Heal, Wish

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Outer Monologue
Female
Cancer
41
74
Dancer
Elemental
PA Save
Equip Gun
Waterbreathing

Bestiary

Holy Miter
Linen Robe
Sprint Shoes

Witch Hunt, Wiznaibus, Obsidian Blade
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Nelrith
Female
Scorpio
59
64
Dancer
White Magic
PA Save
Defense UP
Move+3

Hydra Bag

Headgear
White Robe
Magic Gauntlet

Wiznaibus, Polka Polka, Disillusion, Nether Demon
Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Wall, Esuna



Rislyeu
Male
Scorpio
76
78
Mime

Sunken State
Equip Armor
Ignore Height



Iron Helmet
Reflect Mail
Genji Gauntlet

Mimic




UmaiJam
Male
Sagittarius
77
57
Monk
Draw Out
Faith Save
Dual Wield
Waterbreathing



Thief Hat
Wizard Outfit
Magic Gauntlet

Spin Fist, Pummel, Secret Fist, Chakra, Revive
Heaven's Cloud, Muramasa, Kikuichimoji

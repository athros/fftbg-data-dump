Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



HaplessOne
Male
Capricorn
79
52
Ninja
Time Magic
Meatbone Slash
Concentrate
Move+3

Short Edge
Hidden Knife
Twist Headband
Mythril Vest
Bracer

Shuriken, Staff, Stick, Wand
Slow 2, Immobilize



NovaKnight21
Male
Scorpio
65
48
Archer
Jump
MA Save
Doublehand
Ignore Terrain

Silver Bow

Green Beret
Secret Clothes
Magic Gauntlet

Charge+2, Charge+4
Level Jump5, Vertical Jump4



Airking990
Monster
Aries
78
79
Ghost










Cloud92684
Female
Scorpio
63
48
Oracle
Item
Parry
Short Charge
Retreat

Bestiary

Golden Hairpin
Chameleon Robe
Bracer

Life Drain, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Potion, Hi-Potion, Ether, Holy Water, Remedy, Phoenix Down

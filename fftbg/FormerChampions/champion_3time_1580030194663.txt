Player: !zChamp
Team: Champion Team
Palettes: Green/White



Ironchefj
Female
Aries
54
56
Ninja
Jump
Hamedo
Sicken
Waterwalking

Flail
Flame Whip
Black Hood
Adaman Vest
N-Kai Armlet

Bomb
Level Jump8, Vertical Jump8



G4ryo4k
Female
Leo
74
70
Monk
Jump
MP Restore
Attack UP
Move-HP Up



Headgear
Chain Vest
Bracer

Pummel, Earth Slash, Revive, Seal Evil
Level Jump8, Vertical Jump2



HyorinMaster
Male
Capricorn
77
73
Monk
White Magic
Regenerator
Short Status
Move+2



Cachusha
Clothes
Battle Boots

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Raise, Shell, Wall, Esuna



Arachness
Female
Pisces
59
79
Oracle
Draw Out
Dragon Spirit
Doublehand
Fly

Battle Folio

Headgear
Silk Robe
Defense Ring

Blind, Poison, Spell Absorb, Pray Faith, Blind Rage, Foxbird, Confusion Song, Paralyze, Sleep
Muramasa, Masamune

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



TeaTime29
Male
Sagittarius
75
60
Mediator
Punch Art
Faith Save
Dual Wield
Ignore Height

Romanda Gun
Blaze Gun
Flash Hat
Mystic Vest
Small Mantle

Persuade, Praise, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate
Pummel, Secret Fist, Purification



BoneMiser
Female
Leo
78
79
Samurai
Elemental
PA Save
Maintenance
Levitate

Kiyomori

Genji Helmet
White Robe
Red Shoes

Asura, Murasame, Heaven's Cloud
Water Ball, Hallowed Ground, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



UmaiJam
Female
Serpentarius
46
57
Calculator
Black Magic
HP Restore
Defend
Retreat

Iron Fan

Flash Hat
Chameleon Robe
Power Wrist

CT, Height, Prime Number, 5, 3
Fire 2, Fire 4, Bolt 4



Cherri
Monster
Capricorn
55
58
Wyvern








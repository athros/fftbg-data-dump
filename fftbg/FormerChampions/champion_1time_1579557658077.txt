Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Remaru7 7
Female
Libra
75
57
Squire
Punch Art
Arrow Guard
Magic Attack UP
Lava Walking

Poison Bow

Twist Headband
Adaman Vest
Bracer

Accumulate, Dash, Yell, Cheer Up
Wave Fist, Secret Fist, Chakra, Revive



Narcius
Female
Libra
75
37
Knight
Jump
Sunken State
Short Status
Move+3

Defender

Crystal Helmet
Linen Robe
Magic Ring

Head Break, Shield Break, Speed Break, Power Break, Mind Break, Justice Sword
Level Jump8, Vertical Jump2



RaitoGG
Male
Cancer
75
64
Samurai
Charge
HP Restore
Equip Axe
Waterwalking

Heaven's Cloud

Cross Helmet
Plate Mail
N-Kai Armlet

Murasame, Muramasa
Charge+7, Charge+10



Solomongrundy85
Female
Aquarius
61
49
Ninja
Steal
Dragon Spirit
Concentrate
Fly

Flame Whip
Kunai
Thief Hat
Earth Clothes
Angel Ring

Shuriken
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory

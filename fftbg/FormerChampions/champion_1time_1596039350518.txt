Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Joewcarson
Male
Libra
48
71
Samurai
Charge
Auto Potion
Equip Knife
Jump+3

Short Edge

Leather Helmet
Linen Cuirass
Genji Gauntlet

Asura, Murasame, Muramasa, Kikuichimoji
Charge+3



Fenaen
Male
Virgo
73
44
Knight
Elemental
Caution
Long Status
Move+2

Save the Queen
Flame Shield
Bronze Helmet
Crystal Mail
Small Mantle

Weapon Break, Speed Break, Power Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball



Jeffersonsa
Monster
Aries
60
49
Tiamat










Daveb
Female
Taurus
74
58
Ninja
Steal
Damage Split
Beastmaster
Retreat

Short Edge
Flail
Red Hood
Earth Clothes
Wizard Mantle

Bomb
Gil Taking, Steal Heart, Steal Armor, Steal Status, Arm Aim

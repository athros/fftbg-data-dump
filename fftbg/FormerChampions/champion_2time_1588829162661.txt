Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



FNCardascia
Monster
Serpentarius
71
69
Blue Dragon










Smegma Sorcerer
Female
Capricorn
58
52
Mime

Auto Potion
Dual Wield
Waterwalking



Holy Miter
White Robe
Spike Shoes

Mimic




ArchKnightX
Female
Gemini
65
40
Samurai
Steal
Caution
Equip Shield
Lava Walking

Holy Lance
Buckler
Mythril Helmet
Black Robe
Small Mantle

Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Gil Taking, Steal Armor, Steal Weapon, Steal Status



B0shii
Male
Scorpio
69
73
Monk
Time Magic
Parry
Equip Bow
Ignore Height

Ice Bow

Feather Hat
Earth Clothes
Genji Gauntlet

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Haste 2, Slow 2, Stop, Immobilize, Reflect, Demi 2, Meteor

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Twelfthrootoftwo
Female
Sagittarius
53
55
Mediator
Item
Damage Split
Throw Item
Teleport

Romanda Gun

Leather Hat
Earth Clothes
Reflect Ring

Invitation, Persuade, Threaten, Preach, Negotiate, Refute
Potion, Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Coralreeferz
Male
Sagittarius
50
75
Ninja
Item
MA Save
Attack UP
Teleport

Flame Whip
Mythril Knife
Red Hood
Secret Clothes
Wizard Mantle

Bomb, Knife
Potion, Ether, Antidote, Phoenix Down



Firesheath
Monster
Virgo
49
76
Ultima Demon










Dogsandcatsand
Female
Leo
45
45
Wizard
Time Magic
Arrow Guard
Short Charge
Levitate

Dragon Rod

Golden Hairpin
Wizard Robe
Wizard Mantle

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Death, Flare
Haste, Float, Reflect

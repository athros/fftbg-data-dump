Player: !zChamp
Team: Champion Team
Palettes: Green/White



AniZero
Male
Leo
80
68
Ninja
Time Magic
MA Save
Equip Gun
Fly

Romanda Gun
Blast Gun
Thief Hat
Earth Clothes
Battle Boots

Shuriken
Haste, Slow, Slow 2, Reflect, Quick, Demi 2, Stabilize Time



JonnyCue
Monster
Virgo
79
77
Archaic Demon










Oogthecaveman
Female
Gemini
58
49
Mediator
Yin Yang Magic
Brave Up
Long Status
Teleport

Stone Gun

Feather Hat
Judo Outfit
Defense Armlet

Invitation, Praise, Threaten, Preach
Spell Absorb, Blind Rage, Confusion Song, Sleep, Petrify



Nomoment
Male
Aries
53
75
Ninja
Talk Skill
Counter
Equip Armor
Ignore Terrain

Sasuke Knife
Ninja Edge
Flash Hat
Diamond Armor
Red Shoes

Shuriken, Bomb
Persuade, Praise, Preach, Death Sentence, Insult, Mimic Daravon, Refute

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Reamplify
Male
Libra
44
72
Archer
Punch Art
Mana Shield
Doublehand
Teleport

Stone Gun

Flash Hat
Judo Outfit
Genji Gauntlet

Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Spin Fist, Pummel, Wave Fist, Purification, Seal Evil



Lowlf
Male
Aries
77
55
Mediator
Time Magic
Catch
Doublehand
Move+3

Glacier Gun

Red Hood
Power Sleeve
Dracula Mantle

Invitation, Praise, Preach, Mimic Daravon, Refute, Rehabilitate
Haste, Slow 2, Stabilize Time, Meteor



PlatinumPlume
Male
Pisces
49
71
Monk
Summon Magic
Counter Flood
Equip Knife
Ignore Height

Assassin Dagger

Thief Hat
Leather Outfit
Bracer

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Moogle, Ifrit, Titan, Leviathan, Silf, Lich, Cyclops



Almost  Sane
Male
Pisces
80
77
Summoner
Jump
PA Save
Concentrate
Move-HP Up

Poison Rod

Golden Hairpin
Black Costume
Power Wrist

Moogle, Shiva, Ifrit, Titan, Golem
Level Jump8, Vertical Jump3

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



KyleWonToLiveForever
Male
Leo
48
72
Mime

Meatbone Slash
Equip Shield
Ignore Terrain


Crystal Shield
Twist Headband
Adaman Vest
Angel Ring

Mimic




Maleblackfiora
Male
Capricorn
57
42
Calculator
Black Magic
Auto Potion
Equip Armor
Waterwalking

Bestiary

Gold Helmet
Gold Armor
Battle Boots

CT, Height, Prime Number, 5, 3
Fire 2, Fire 3, Bolt, Bolt 4, Ice 2, Ice 3, Ice 4



Laserman1000
Male
Scorpio
63
64
Ninja
Battle Skill
Sunken State
Equip Axe
Fly

Rainbow Staff
White Staff
Red Hood
Brigandine
Elf Mantle

Shuriken, Staff
Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Justice Sword, Dark Sword, Night Sword



Kaidykat
Male
Gemini
44
46
Bard
White Magic
Abandon
Equip Polearm
Move-MP Up

Javelin

Thief Hat
Mythril Vest
Diamond Armlet

Angel Song, Cheer Song, Battle Song, Nameless Song, Space Storage
Cure 2, Raise, Raise 2, Regen, Shell 2, Wall, Esuna

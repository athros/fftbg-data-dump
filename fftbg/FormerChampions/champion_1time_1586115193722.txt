Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



OmnibotGamma
Male
Libra
43
65
Archer
Yin Yang Magic
Caution
Doublehand
Retreat

Ultimus Bow

Thief Hat
Mystic Vest
Germinas Boots

Charge+1, Charge+4
Blind, Life Drain, Silence Song, Blind Rage, Dispel Magic



RughSontos
Male
Libra
77
54
Monk
Talk Skill
Dragon Spirit
Short Status
Ignore Terrain



Holy Miter
Judo Outfit
Elf Mantle

Spin Fist, Revive
Solution, Mimic Daravon, Refute, Rehabilitate



Lezprobe
Monster
Leo
72
72
Wild Boar










ZephyrTempest
Female
Gemini
59
53
Squire
Elemental
Parry
Equip Polearm
Move+3

Iron Fan
Escutcheon
Golden Hairpin
Mythril Vest
N-Kai Armlet

Accumulate, Throw Stone, Heal, Tickle
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

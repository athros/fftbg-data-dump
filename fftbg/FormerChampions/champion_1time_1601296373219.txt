Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Lydian C
Female
Taurus
51
73
Monk
Draw Out
Counter
Doublehand
Retreat

Hydra Bag

Black Hood
Mythril Vest
Wizard Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Koutetsu, Bizen Boat, Murasame, Muramasa



Genkidou
Female
Leo
70
59
Archer
Jump
Auto Potion
Dual Wield
Lava Walking

Ultimus Bow

Twist Headband
Clothes
Power Wrist

Charge+1, Charge+2
Level Jump3, Vertical Jump4



Touchingthefuzzy
Male
Capricorn
50
74
Monk
Basic Skill
Parry
Short Charge
Fly



Green Beret
Chain Vest
Genji Gauntlet

Pummel, Wave Fist, Purification
Heal, Yell, Wish



Edmundo
Male
Gemini
67
62
Ninja
Draw Out
HP Restore
Magic Defense UP
Waterwalking

Sasuke Knife
Iga Knife
Feather Hat
Black Costume
Defense Ring

Shuriken, Bomb, Knife, Staff, Axe, Wand
Bizen Boat, Heaven's Cloud

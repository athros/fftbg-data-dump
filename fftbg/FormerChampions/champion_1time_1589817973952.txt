Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Kellios11
Female
Virgo
68
67
Priest
Draw Out
Damage Split
Equip Shield
Levitate

Wizard Staff
Buckler
Red Hood
Black Costume
Chantage

Cure 3, Raise, Raise 2, Reraise, Protect, Wall, Esuna, Holy
Bizen Boat, Heaven's Cloud, Kiyomori



Wizblizz
Male
Virgo
57
43
Time Mage
Jump
Auto Potion
Doublehand
Ignore Terrain

White Staff

Feather Hat
Chameleon Robe
Feather Boots

Haste, Slow, Slow 2, Stop, Quick, Demi 2, Meteor
Level Jump5, Vertical Jump8



LDSkinny
Female
Aquarius
43
46
Oracle
Black Magic
Meatbone Slash
Equip Polearm
Jump+2

Obelisk

Feather Hat
Wizard Robe
Cursed Ring

Blind, Poison, Pray Faith, Dispel Magic, Dark Holy
Fire 3, Bolt 2, Bolt 4, Ice 2, Ice 3, Death



ShintaroNayaka
Male
Virgo
79
80
Ninja
Black Magic
Catch
Equip Sword
Levitate

Defender
Kunai
Feather Hat
Earth Clothes
Cursed Ring

Knife, Spear
Bolt 2, Ice 2, Ice 3, Empower, Flare

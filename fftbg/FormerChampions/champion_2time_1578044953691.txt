Player: !zChamp
Team: Champion Team
Palettes: Green/White



Mary
Male
Sagittarius
77
75
Geomancer
Yin Yang Magic
Abandon
Equip Shield
Retreat

Giant Axe
Aegis Shield
Holy Miter
Wizard Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Blind, Life Drain, Pray Faith, Doubt Faith, Dispel Magic, Paralyze, Sleep



Judd
Male
Aquarius
73
58
Knight
White Magic
HP Restore
Equip Polearm
Move+1

Ragnarok

Mythril Helmet
Leather Armor
Diamond Armlet

Weapon Break, Magic Break, Explosion Sword
Raise, Regen, Protect, Protect 2, Wall



Sula
Female
Aries
71
74
Thief
Punch Art
Parry
Dual Wield
Levitate

Ninja Edge
Air Knife
Triangle Hat
Mythril Vest
Sprint Shoes

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Earth Slash, Purification, Chakra



Louise
Female
Sagittarius
54
77
Dancer
Draw Out
Earplug
Beastmaster
Swim

Star Bag

Black Hood
Leather Outfit
Wizard Mantle

Wiznaibus, Slow Dance, Disillusion, Dragon Pit
Asura, Bizen Boat, Muramasa



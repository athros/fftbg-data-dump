Player: !zChamp
Team: Champion Team
Palettes: White/Blue



CT 5 Holy
Female
Virgo
76
69
Priest
Draw Out
Distribute
Magic Attack UP
Retreat

Healing Staff

Flash Hat
Black Costume
Germinas Boots

Raise, Raise 2, Protect 2, Shell, Shell 2, Esuna, Holy
Koutetsu, Bizen Boat, Murasame



NovaKnight21
Male
Cancer
57
45
Calculator
Imp Skill
Meatbone Slash
Doublehand
Levitate

Battle Bamboo

Thief Hat
Mythril Armor
Red Shoes

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam



ApplesauceBoss
Female
Pisces
69
73
Geomancer
Draw Out
HP Restore
Equip Gun
Waterbreathing

Romanda Gun
Venetian Shield
Red Hood
Chain Vest
Defense Armlet

Pitfall, Hallowed Ground, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bizen Boat, Kiyomori, Kikuichimoji, Masamune, Chirijiraden



Choco Joe
Male
Cancer
50
71
Ninja
Steal
Brave Save
Equip Gun
Jump+1

Mythril Gun
Blast Gun
Feather Hat
Brigandine
Bracer

Shuriken
Steal Armor, Steal Weapon, Arm Aim, Leg Aim

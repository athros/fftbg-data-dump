Player: !zChamp
Team: Champion Team
Palettes: Green/White



Buttkrakenn
Female
Taurus
80
66
Samurai
Talk Skill
Counter Magic
Monster Talk
Retreat

Koutetsu Knife

Leather Helmet
Linen Robe
Feather Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Refute



Nobodyhasone
Female
Leo
76
76
Archer
Elemental
Counter Tackle
Short Charge
Levitate

Cross Bow
Genji Shield
Triangle Hat
Earth Clothes
Diamond Armlet

Charge+1, Charge+4, Charge+7, Charge+10
Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Granais
Monster
Serpentarius
51
65
Steel Giant










Chronog
Male
Cancer
63
52
Lancer
Yin Yang Magic
Counter
Maintenance
Move+2

Javelin
Gold Shield
Leather Helmet
Mythril Armor
Genji Gauntlet

Level Jump8, Vertical Jump5
Blind, Life Drain, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Dark Holy

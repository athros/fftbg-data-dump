Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Kronikle
Male
Aquarius
52
39
Knight
Yin Yang Magic
Absorb Used MP
Equip Axe
Move+2

Morning Star
Mythril Shield
Crystal Helmet
Linen Cuirass
Cursed Ring

Head Break, Armor Break, Mind Break, Justice Sword, Surging Sword
Blind, Poison, Life Drain, Pray Faith, Zombie, Silence Song, Paralyze, Dark Holy



Firesheath
Female
Sagittarius
75
74
Mediator
Charge
Damage Split
Sicken
Move+3

Madlemgen

Flash Hat
Leather Outfit
Leather Mantle

Invitation, Insult, Refute
Charge+2, Charge+5, Charge+20



Rocl
Female
Scorpio
57
78
Thief
Dance
Caution
Attack UP
Move+1

Dagger

Feather Hat
Mythril Vest
Defense Ring

Gil Taking, Steal Heart, Steal Accessory, Steal Status, Leg Aim
Witch Hunt, Slow Dance, Polka Polka, Disillusion, Last Dance, Obsidian Blade



Laserman1000
Male
Virgo
80
40
Knight
Draw Out
Speed Save
Dual Wield
Teleport 2

Battle Axe
Sleep Sword
Crystal Helmet
Reflect Mail
108 Gems

Armor Break, Shield Break, Weapon Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



PoroTact
Female
Cancer
52
48
Priest
Yin Yang Magic
Counter Magic
Equip Armor
Jump+2

Flail

Gold Helmet
Mythril Vest
Leather Mantle

Cure 2, Raise, Regen, Shell, Wall, Esuna
Blind, Spell Absorb, Confusion Song



Ar Tactic
Female
Capricorn
54
52
Summoner
Dance
Meatbone Slash
Doublehand
Move-HP Up

Oak Staff

Thief Hat
Chameleon Robe
108 Gems

Moogle, Ifrit, Golem, Carbunkle, Leviathan, Fairy, Cyclops
Last Dance, Void Storage



Nhammen
Male
Scorpio
46
66
Archer
Summon Magic
Regenerator
Doublehand
Teleport

Yoichi Bow

Crystal Helmet
Clothes
Spike Shoes

Charge+1, Charge+3, Charge+5, Charge+7
Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Silf, Fairy



Old Overholt
Female
Leo
53
43
Priest
Draw Out
HP Restore
Equip Armor
Jump+2

Scorpion Tail

Genji Helmet
Mythril Armor
Sprint Shoes

Cure, Cure 3, Raise, Reraise, Regen, Protect 2, Shell, Wall, Esuna
Murasame, Heaven's Cloud

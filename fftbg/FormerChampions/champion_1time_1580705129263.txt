Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Mirapoix
Female
Scorpio
54
46
Summoner
Draw Out
Parry
Short Charge
Swim

Mace of Zeus

Golden Hairpin
Silk Robe
Defense Armlet

Ramuh, Silf, Fairy
Asura, Bizen Boat, Muramasa



RedGrinGrumbl3
Male
Cancer
64
60
Summoner
Battle Skill
Absorb Used MP
Short Charge
Levitate

White Staff

Green Beret
Silk Robe
Wizard Mantle

Moogle, Ramuh, Ifrit, Carbunkle, Silf, Fairy
Shield Break, Power Break, Mind Break, Stasis Sword, Night Sword, Surging Sword



Regular Obbi
Male
Cancer
79
43
Monk
Draw Out
Abandon
Equip Shield
Move+3


Bronze Shield
Ribbon
Leather Vest
Bracer

Pummel, Purification, Chakra, Revive
Muramasa



Hamborn
Male
Aquarius
56
38
Squire
Punch Art
Speed Save
Defense UP
Move+2

Sleep Sword

Feather Hat
Silk Robe
Small Mantle

Dash, Heal, Tickle, Yell, Wish
Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil

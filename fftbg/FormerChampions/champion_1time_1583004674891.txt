Player: !zChamp
Team: Champion Team
Palettes: Green/White



Oogthecaveman
Male
Cancer
62
57
Lancer
Time Magic
Brave Up
Equip Bow
Ignore Terrain

Hunting Bow
Mythril Shield
Mythril Helmet
Diamond Armor
Cursed Ring

Level Jump5, Vertical Jump8
Haste, Haste 2, Immobilize, Float, Stabilize Time



Mellichor
Female
Aquarius
72
46
Oracle
Basic Skill
Damage Split
Halve MP
Move+3

Papyrus Codex

Triangle Hat
Mythril Vest
Wizard Mantle

Blind, Life Drain, Silence Song, Blind Rage, Dispel Magic
Accumulate, Dash, Throw Stone, Heal, Yell, Cheer Up, Wish, Scream



MyFakeLife
Male
Leo
74
58
Knight
Draw Out
Dragon Spirit
Equip Shield
Jump+2

Ice Brand
Flame Shield
Genji Helmet
Linen Cuirass
Rubber Shoes

Armor Break, Magic Break, Stasis Sword
Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa



Meta Five
Male
Libra
65
43
Samurai
Time Magic
MP Restore
Sicken
Move+2

Bizen Boat

Cross Helmet
Mythril Armor
Germinas Boots

Asura, Heaven's Cloud, Muramasa
Haste, Slow, Slow 2, Stabilize Time, Meteor

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Kronikle
Female
Capricorn
77
54
Samurai
Item
Earplug
Magic Attack UP
Ignore Height

Kikuichimoji

Circlet
Light Robe
Feather Mantle

Koutetsu, Murasame, Heaven's Cloud, Muramasa
X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down



Ross From Cali
Monster
Serpentarius
68
56
Reaper










Sinnyil2
Female
Scorpio
74
64
Summoner
Talk Skill
PA Save
Short Charge
Levitate

Ice Rod

Black Hood
Silk Robe
108 Gems

Moogle, Shiva, Golem, Carbunkle, Leviathan, Silf
Invitation, Praise, Solution, Mimic Daravon



StealthModeLocke
Male
Capricorn
51
72
Chemist
Basic Skill
Distribute
Doublehand
Levitate

Cute Bag

Flash Hat
Mythril Vest
Wizard Mantle

Potion, Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Heal, Fury

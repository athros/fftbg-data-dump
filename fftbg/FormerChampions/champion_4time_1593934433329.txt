Player: !zChamp
Team: Champion Team
Palettes: Green/White



Safira125
Female
Scorpio
57
80
Lancer
Battle Skill
Regenerator
Dual Wield
Levitate

Dragon Whisker
Holy Lance
Leather Helmet
Plate Mail
Genji Gauntlet

Level Jump3, Vertical Jump5
Head Break, Armor Break, Power Break, Justice Sword



Enkikavlar
Female
Gemini
70
49
Wizard
Math Skill
Speed Save
Equip Knife
Retreat

Koga Knife

Holy Miter
Adaman Vest
Angel Ring

Fire, Bolt 2, Bolt 4, Ice 3, Death, Flare
CT, Prime Number, 4, 3



O Heyno
Male
Capricorn
61
49
Thief
Punch Art
Counter Flood
Long Status
Jump+2

Cultist Dagger

Holy Miter
Earth Clothes
Small Mantle

Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Spin Fist, Earth Slash, Secret Fist, Chakra, Revive



ColetteMSLP
Female
Capricorn
36
53
Ninja
Black Magic
Blade Grasp
Short Status
Teleport 2

Sasuke Knife
Flame Whip
Black Hood
Chain Vest
Rubber Shoes

Shuriken
Fire, Fire 2, Fire 4, Bolt 2

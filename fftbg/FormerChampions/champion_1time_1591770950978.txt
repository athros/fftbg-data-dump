Player: !zChamp
Team: Champion Team
Palettes: Green/White



Oobs56
Female
Taurus
66
45
Knight
Draw Out
Absorb Used MP
Equip Polearm
Ignore Terrain

Spear
Flame Shield
Diamond Helmet
Mythril Armor
Cursed Ring

Shield Break, Magic Break, Speed Break, Mind Break, Night Sword, Explosion Sword
Asura, Koutetsu, Heaven's Cloud, Muramasa



Cougboi
Male
Cancer
72
42
Mime

Counter
Martial Arts
Move+3



Twist Headband
Clothes
Feather Mantle

Mimic




HASTERIOUS
Female
Taurus
65
79
Geomancer
Battle Skill
Counter Tackle
Defense UP
Teleport

Slasher
Mythril Shield
Thief Hat
Adaman Vest
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shield Break, Magic Break, Power Break, Justice Sword, Surging Sword



Holyonline
Male
Scorpio
63
47
Bard
Charge
Absorb Used MP
Short Charge
Jump+2

Fairy Harp

Headgear
Earth Clothes
Wizard Mantle

Life Song, Battle Song, Magic Song
Charge+7, Charge+20

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Forkmore
Female
Cancer
67
43
Calculator
Gore Skill
Absorb Used MP
Defend
Waterbreathing

Papyrus Codex
Crystal Shield
Diamond Helmet
Diamond Armor
Salty Rage

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



O Heyno
Male
Capricorn
63
50
Monk
Basic Skill
Counter Magic
Concentrate
Lava Walking



Black Hood
Brigandine
Feather Mantle

Pummel, Wave Fist, Purification, Revive, Seal Evil
Accumulate, Heal, Yell, Cheer Up, Fury, Wish, Scream



Fluffskull
Male
Leo
67
44
Ninja
Basic Skill
Speed Save
Equip Shield
Waterbreathing

Spell Edge
Flame Shield
Black Hood
Secret Clothes
Angel Ring

Shuriken, Bomb, Knife, Ninja Sword
Accumulate, Heal, Cheer Up



Mesmaster
Female
Scorpio
57
70
Lancer
Time Magic
Auto Potion
Defend
Fly

Musk Rod
Genji Shield
Circlet
Wizard Robe
Sprint Shoes

Level Jump8, Vertical Jump7
Slow, Stop, Immobilize

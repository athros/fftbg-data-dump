Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



R Raynos
Male
Virgo
71
43
Samurai
Charge
Counter Flood
Magic Defense UP
Waterbreathing

Kikuichimoji

Barbuta
Chain Mail
Sprint Shoes

Bizen Boat, Murasame, Kiyomori, Muramasa
Charge+1



J2DaBibbles
Female
Aquarius
45
71
Time Mage
Summon Magic
Counter Tackle
Short Charge
Jump+3

Bestiary

Headgear
Secret Clothes
Dracula Mantle

Haste, Haste 2, Reflect, Stabilize Time
Shiva, Carbunkle, Salamander, Silf



Gorgewall
Male
Virgo
63
57
Knight
Jump
Caution
Sicken
Ignore Height

Rune Blade
Diamond Shield
Diamond Helmet
Leather Armor
Elf Mantle

Armor Break, Weapon Break, Magic Break, Night Sword
Level Jump4, Vertical Jump6



Hales Bopp It
Male
Pisces
77
66
Mime

PA Save
Martial Arts
Lava Walking



Thief Hat
Power Sleeve
Defense Armlet

Mimic


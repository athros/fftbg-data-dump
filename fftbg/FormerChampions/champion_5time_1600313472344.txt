Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ColetteMSLP
Male
Taurus
61
70
Wizard
Battle Skill
Counter Flood
Equip Sword
Waterwalking

Kiyomori

Thief Hat
Black Robe
Wizard Mantle

Fire 2, Ice, Ice 2, Ice 3, Death
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Night Sword



Thyrandaal
Male
Scorpio
41
69
Mime

Speed Save
Short Status
Swim



Thief Hat
Brigandine
Genji Gauntlet

Mimic




Firesheath
Male
Taurus
71
64
Archer
Item
HP Restore
Short Charge
Move-MP Up

Ice Bow

Green Beret
Earth Clothes
Elf Mantle

Charge+3, Charge+5
Potion, Ether, Soft



Zagorsek
Female
Cancer
74
56
Calculator
White Magic
Speed Save
Long Status
Levitate

Bestiary

Thief Hat
Light Robe
Defense Ring

CT, Height, Prime Number, 5, 4, 3
Cure 2, Cure 3, Raise, Reraise, Regen, Shell

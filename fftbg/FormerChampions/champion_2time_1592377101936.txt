Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



NeoKevlar
Male
Aquarius
76
40
Mime

Counter
Doublehand
Move+2



Red Hood
Wizard Outfit
Power Wrist

Mimic




ALY327
Male
Gemini
71
45
Oracle
Jump
Mana Shield
Dual Wield
Ignore Terrain

Ivory Rod
Octagon Rod
Flash Hat
Judo Outfit
Cursed Ring

Life Drain, Silence Song, Blind Rage, Paralyze, Sleep
Level Jump8, Vertical Jump8



Lawnboxer
Monster
Sagittarius
72
70
Red Chocobo










Evewho
Female
Aries
60
43
Chemist
Time Magic
MA Save
Dual Wield
Waterbreathing

Hydra Bag
Blind Knife
Green Beret
Earth Clothes
Magic Gauntlet

X-Potion, Antidote, Echo Grass, Holy Water, Phoenix Down
Haste, Haste 2, Stabilize Time, Meteor

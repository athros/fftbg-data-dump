Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Run With Stone GUNs
Male
Aquarius
60
41
Mediator
Elemental
Auto Potion
Dual Wield
Ignore Height

Blaze Gun
Glacier Gun
Green Beret
Light Robe
Vanish Mantle

Invitation, Praise, Solution, Insult, Refute
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Dogsandcatsand
Female
Aquarius
43
75
Mime

Arrow Guard
Equip Shield
Fly


Ice Shield
Twist Headband
Mythril Vest
Genji Gauntlet

Mimic




Forkmore
Female
Virgo
53
41
Wizard
Battle Skill
Dragon Spirit
Short Charge
Fly

Assassin Dagger

Black Hood
White Robe
Battle Boots

Fire, Fire 2, Fire 4, Bolt 2, Ice 3, Flare
Head Break, Magic Break, Speed Break, Mind Break, Stasis Sword



Killth3kid
Male
Sagittarius
59
66
Archer
Elemental
Counter
Equip Sword
Jump+1

Ice Brand
Hero Shield
Feather Hat
Black Costume
Red Shoes

Charge+1, Charge+3, Charge+7, Charge+20
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind

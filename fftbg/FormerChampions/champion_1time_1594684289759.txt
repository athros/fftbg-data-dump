Player: !zChamp
Team: Champion Team
Palettes: Green/White



Superdevon1
Monster
Sagittarius
50
55
Byblos










StealthModeLocke
Monster
Aries
47
65
Red Panther










Nsm013
Female
Scorpio
65
68
Oracle
Charge
Damage Split
Long Status
Jump+1

Panther Bag

Leather Hat
White Robe
Red Shoes

Poison, Life Drain, Blind Rage, Confusion Song, Paralyze, Sleep, Dark Holy
Charge+1, Charge+2, Charge+3, Charge+5, Charge+20



Shineeyo
Female
Capricorn
66
59
Thief
Time Magic
MP Restore
Equip Knife
Move+1

Kunai

Triangle Hat
Judo Outfit
Genji Gauntlet

Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory
Slow 2, Stop, Immobilize, Float, Reflect, Meteor

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Reddwind
Male
Taurus
78
66
Knight
Charge
Sunken State
Beastmaster
Fly

Defender
Flame Shield
Circlet
Plate Mail
Rubber Shoes

Shield Break, Magic Break, Speed Break, Stasis Sword, Surging Sword
Charge+4



VolgraTheMoose
Male
Virgo
77
42
Archer
Item
PA Save
Doublehand
Ignore Terrain

Mythril Bow

Thief Hat
Mystic Vest
Magic Gauntlet

Charge+1, Charge+2, Charge+4, Charge+7
Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



Lastly
Female
Sagittarius
43
56
Dancer
Summon Magic
Sunken State
Equip Polearm
Teleport

Ryozan Silk

Black Hood
Wizard Robe
N-Kai Armlet

Slow Dance, Polka Polka, Disillusion
Moogle, Ifrit, Carbunkle, Leviathan, Silf, Fairy, Lich



GrandmasterFrankerZ
Male
Scorpio
64
52
Bard
Yin Yang Magic
Hamedo
Dual Wield
Jump+1

Bloody Strings
Fairy Harp
Green Beret
Carabini Mail
Rubber Shoes

Angel Song, Magic Song, Last Song, Space Storage
Blind, Poison, Blind Rage, Confusion Song

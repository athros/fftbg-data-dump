Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Reddwind
Male
Sagittarius
74
45
Knight
Punch Art
Earplug
Halve MP
Move-HP Up

Slasher
Genji Shield
Barbuta
Genji Armor
Diamond Armlet

Head Break, Magic Break, Speed Break, Night Sword
Secret Fist, Chakra, Revive



SkylerBunny
Monster
Taurus
48
77
Cockatrice










Genericllama
Male
Libra
67
52
Thief
Time Magic
Meatbone Slash
Equip Bow
Lava Walking

Perseus Bow

Feather Hat
Mythril Vest
Elf Mantle

Steal Status
Float, Reflect, Quick, Stabilize Time



Ko2q
Female
Virgo
69
48
Thief
Jump
Mana Shield
Equip Gun
Ignore Terrain

Blaze Gun

Thief Hat
Earth Clothes
Leather Mantle

Steal Heart, Steal Weapon, Steal Accessory, Arm Aim
Level Jump4, Vertical Jump5

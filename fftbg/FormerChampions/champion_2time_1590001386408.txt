Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Skillomono
Female
Virgo
42
78
Oracle
Talk Skill
Catch
Short Status
Move+2

Iron Fan

Black Hood
Wizard Robe
Magic Gauntlet

Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Petrify, Dark Holy
Persuade, Threaten, Preach, Rehabilitate



DustBirdEX
Female
Pisces
57
73
Samurai
Elemental
Counter Magic
Magic Defense UP
Retreat

Spear

Cross Helmet
Plate Mail
Angel Ring

Bizen Boat, Kiyomori, Kikuichimoji
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Phytik
Male
Scorpio
38
44
Ninja
Time Magic
Mana Shield
Sicken
Ignore Height

Kunai
Kunai
Feather Hat
Leather Outfit
Feather Mantle

Bomb, Staff, Stick, Wand, Dictionary
Stop, Reflect, Demi, Stabilize Time



TheChainNerd
Male
Virgo
56
46
Squire
Talk Skill
Counter Flood
Monster Talk
Ignore Height

Coral Sword
Buckler
Headgear
Genji Armor
Power Wrist

Throw Stone, Heal, Yell, Cheer Up, Scream
Preach, Solution, Insult, Mimic Daravon, Rehabilitate

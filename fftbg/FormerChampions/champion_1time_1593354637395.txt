Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Gorgewall
Male
Sagittarius
76
67
Archer
Time Magic
Dragon Spirit
Defense UP
Waterwalking

Stone Gun
Venetian Shield
Twist Headband
Leather Outfit
Germinas Boots

Charge+1, Charge+2, Charge+7
Haste, Haste 2, Slow 2, Quick



Zmoses
Male
Serpentarius
42
79
Priest
Yin Yang Magic
Brave Save
Concentrate
Lava Walking

Flail

Black Hood
Chameleon Robe
Feather Mantle

Reraise, Regen, Protect, Esuna, Holy
Life Drain, Confusion Song, Dispel Magic, Paralyze



Twelfthrootoftwo
Male
Taurus
61
70
Geomancer
Basic Skill
Absorb Used MP
Short Status
Waterbreathing

Battle Axe
Aegis Shield
Headgear
Mythril Vest
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Gusty Wind
Accumulate, Heal, Yell, Cheer Up, Fury



Nok
Female
Aries
41
66
Monk
Yin Yang Magic
Critical Quick
Attack UP
Levitate



Red Hood
Mythril Vest
Sprint Shoes

Pummel, Wave Fist, Earth Slash, Purification
Spell Absorb, Pray Faith, Doubt Faith, Zombie, Confusion Song, Dark Holy

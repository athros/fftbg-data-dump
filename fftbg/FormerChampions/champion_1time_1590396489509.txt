Player: !zChamp
Team: Champion Team
Palettes: Green/White



Wooyuji
Male
Taurus
38
72
Bard
Yin Yang Magic
Absorb Used MP
Attack UP
Levitate

Fairy Harp

Leather Hat
Power Sleeve
Sprint Shoes

Angel Song, Sky Demon
Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Sleep



ApplesauceBoss
Male
Capricorn
50
74
Knight
Talk Skill
Earplug
Equip Gun
Fly

Glacier Gun
Crystal Shield
Grand Helmet
Diamond Armor
Red Shoes

Head Break, Speed Break, Night Sword
Invitation, Negotiate, Mimic Daravon, Rehabilitate



EnemyController
Monster
Pisces
41
45
Cockatrice










Firesheath
Female
Capricorn
57
70
Dancer
Draw Out
Parry
Dual Wield
Fly

Ryozan Silk
Cashmere
Twist Headband
Wizard Robe
Red Shoes

Witch Hunt, Wiznaibus, Nameless Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon
Koutetsu, Kikuichimoji

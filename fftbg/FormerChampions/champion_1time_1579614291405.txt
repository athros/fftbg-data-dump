Player: !zChamp
Team: Champion Team
Palettes: Green/White



ShintaroNayaka
Female
Aries
69
77
Dancer
Battle Skill
Caution
Magic Attack UP
Move+3

Cashmere

Triangle Hat
Leather Outfit
Wizard Mantle

Polka Polka, Nameless Dance, Obsidian Blade, Dragon Pit
Head Break, Power Break, Explosion Sword



Dalawing
Monster
Aquarius
66
41
Iron Hawk










KrzyforBacon
Male
Sagittarius
60
42
Geomancer
Draw Out
Damage Split
Long Status
Retreat

Hydra Bag
Mythril Shield
Flash Hat
Wizard Robe
Bracer

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball
Asura



Ayrara
Female
Libra
64
42
Archer
Item
MA Save
Throw Item
Lava Walking

Long Bow

Black Hood
Leather Vest
Angel Ring

Charge+1, Charge+4, Charge+10, Charge+20
X-Potion, Ether, Elixir, Antidote, Eye Drop, Echo Grass, Soft, Holy Water

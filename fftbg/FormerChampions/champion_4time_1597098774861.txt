Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Grininda
Female
Cancer
49
70
Archer
Summon Magic
Distribute
Defense UP
Move+2

Night Killer
Aegis Shield
Leather Hat
Judo Outfit
Dracula Mantle

Charge+1
Moogle, Shiva, Ifrit, Titan, Silf



Chuckolator
Male
Scorpio
60
51
Lancer
Charge
Counter Tackle
Dual Wield
Retreat

Holy Lance
Spear
Genji Helmet
Linen Robe
Elf Mantle

Level Jump8, Vertical Jump8
Charge+1, Charge+2, Charge+10



Muffin Money
Male
Leo
46
75
Knight
Steal
Arrow Guard
Attack UP
Jump+3

Mythril Sword
Aegis Shield
Barbuta
Linen Robe
Germinas Boots

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Fenaen
Male
Aquarius
41
41
Knight
Item
Absorb Used MP
Short Charge
Retreat

Chaos Blade
Round Shield
Barbuta
Carabini Mail
Jade Armlet

Shield Break, Magic Break, Mind Break, Justice Sword
Hi-Potion, Ether, Hi-Ether, Eye Drop, Phoenix Down

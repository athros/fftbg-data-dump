Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Ashley
Male
Sagittarius
82
60
Geomancer
Draw Out
MP Restore
Magic Defense UP
Move-HP Up

Long Sword
Genji Shield
Golden Hairpin
Chain Vest
Magic Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji



Jimmie
Male
Aries
59
47
Ninja
Steal
Hamedo
Doublehand
Ignore Height

Air Knife

Feather Hat
Earth Clothes
Cursed Ring

Shuriken, Stick, Dictionary
Steal Armor, Steal Shield, Arm Aim



Rickey
Male
Leo
79
69
Squire
Throw
Abandon
Dual Wield
Waterbreathing

Mage Masher
Scorpion Tail
Feather Hat
Earth Clothes
Feather Mantle

Heal, Cheer Up, Wish, Scream
Shuriken, Spear, Dictionary



Cati
Female
Leo
39
43
Geomancer
Charge
Counter Flood
Equip Armor
Levitate

Battle Axe
Platinum Shield
Leather Helmet
Silk Robe
Battle Boots

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+4, Charge+10, Charge+20



Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Maakur
Female
Libra
66
42
Geomancer
Draw Out
Abandon
Short Status
Move-HP Up

Asura Knife
Kaiser Plate
Leather Hat
Silk Robe
Salty Rage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Bizen Boat, Kikuichimoji



Deathazuma
Female
Gemini
50
59
Knight
Time Magic
MP Restore
Attack UP
Fly

Diamond Sword
Platinum Shield
Barbuta
Chain Mail
Small Mantle

Shield Break, Weapon Break, Magic Break, Stasis Sword, Dark Sword, Surging Sword
Haste 2, Stop, Immobilize, Stabilize Time



Iringahn
Male
Aquarius
74
57
Time Mage
Yin Yang Magic
Distribute
Equip Shield
Lava Walking

Wizard Staff
Mythril Shield
Holy Miter
Linen Robe
N-Kai Armlet

Slow, Stop, Demi 2, Stabilize Time, Galaxy Stop
Spell Absorb, Life Drain, Foxbird, Dark Holy



Ctharvey
Monster
Taurus
52
55
Dragon








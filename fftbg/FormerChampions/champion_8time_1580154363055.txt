Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



AStatue
Male
Libra
74
57
Archer
Throw
Caution
Doublehand
Jump+2

Glacier Gun

Green Beret
Secret Clothes
Diamond Armlet

Charge+5, Charge+7, Charge+20
Shuriken, Bomb, Dictionary



RongRongArts
Male
Aries
56
40
Chemist
Basic Skill
Parry
Doublehand
Waterbreathing

Stone Gun

Golden Hairpin
Mythril Vest
Red Shoes

X-Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Dash, Heal, Yell, Cheer Up



Mtmcl
Female
Aquarius
64
68
Mediator
Battle Skill
Speed Save
Concentrate
Jump+1

Bestiary

Green Beret
Wizard Robe
Magic Ring

Invitation, Persuade, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Shield Break, Weapon Break, Explosion Sword



TCSyd
Female
Virgo
53
75
Ninja
Time Magic
Catch
Defend
Move+2

Assassin Dagger
Mage Masher
Thief Hat
Brigandine
Elf Mantle

Shuriken, Bomb, Ninja Sword, Wand
Haste, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Skillomono
Male
Taurus
67
67
Geomancer
Yin Yang Magic
Abandon
Beastmaster
Move+3

Battle Axe
Buckler
Black Hood
Silk Robe
108 Gems

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Blind, Life Drain, Pray Faith, Blind Rage, Confusion Song, Dispel Magic



DeathTaxesAndAnime
Female
Leo
48
76
Lancer
Summon Magic
Blade Grasp
Halve MP
Move+3

Obelisk
Bronze Shield
Cross Helmet
Mythril Armor
Reflect Ring

Level Jump3, Vertical Jump8
Moogle, Shiva, Titan, Golem, Carbunkle



SQUiDSQUARKLIN
Male
Aquarius
76
46
Geomancer
Time Magic
Distribute
Attack UP
Move+2

Kikuichimoji
Bronze Shield
Twist Headband
Wizard Outfit
108 Gems

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Haste 2, Slow, Immobilize



Maakur
Male
Gemini
72
73
Priest
Battle Skill
Auto Potion
Equip Axe
Jump+2

Slasher

Flash Hat
Linen Robe
Red Shoes

Cure 2, Raise, Reraise, Shell 2, Esuna, Holy
Head Break, Shield Break, Magic Break, Mind Break, Justice Sword, Night Sword

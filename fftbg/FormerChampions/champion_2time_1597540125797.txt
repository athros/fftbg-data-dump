Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Bruubarg
Male
Taurus
57
44
Thief
Talk Skill
Counter
Dual Wield
Jump+2

Dagger
Dagger
Thief Hat
Rubber Costume
Power Wrist

Steal Shield, Arm Aim
Praise, Death Sentence, Mimic Daravon, Refute



CT 5 Holy
Female
Gemini
75
42
Thief
Time Magic
MA Save
Long Status
Jump+1

Short Edge

Thief Hat
Power Sleeve
N-Kai Armlet

Steal Weapon, Arm Aim, Leg Aim
Haste, Slow, Stop, Quick, Demi 2



SephDarkheart
Male
Pisces
54
67
Wizard
White Magic
Dragon Spirit
Equip Gun
Retreat

Bestiary

Holy Miter
White Robe
Defense Armlet

Fire 2, Fire 4, Bolt 4, Ice 2, Ice 3, Ice 4
Cure, Cure 4, Raise, Shell, Esuna



Lanshaft
Female
Capricorn
38
57
Mediator
Summon Magic
Dragon Spirit
Defend
Jump+1

Bestiary

Holy Miter
Mythril Vest
Bracer

Praise, Negotiate, Refute
Moogle, Ifrit, Golem, Carbunkle, Odin, Leviathan, Salamander

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



TitanMeteor
Female
Aries
64
59
Dancer
White Magic
HP Restore
Defend
Move+3

Cashmere

Golden Hairpin
Adaman Vest
Germinas Boots

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Nether Demon
Cure, Raise, Raise 2, Shell 2, Esuna



Play Puyopuyo
Male
Aquarius
63
59
Mime

Counter
Martial Arts
Waterwalking



Feather Hat
Leather Outfit
Reflect Ring

Mimic




Alaylle
Female
Taurus
59
71
Summoner
White Magic
Counter
Short Charge
Ignore Terrain

Wizard Staff

Holy Miter
Wizard Robe
Red Shoes

Ramuh, Ifrit, Salamander, Fairy, Cyclops
Cure 2, Cure 3, Cure 4, Raise, Shell



Itskabraxxis
Female
Leo
47
51
Samurai
Throw
Meatbone Slash
Dual Wield
Swim

Koutetsu Knife
Kiyomori
Gold Helmet
Linen Cuirass
Battle Boots

Koutetsu, Murasame, Muramasa
Shuriken, Dictionary

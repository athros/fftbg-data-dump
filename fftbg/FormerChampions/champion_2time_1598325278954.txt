Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



DavenIII
Male
Aquarius
78
75
Monk
Elemental
Mana Shield
Equip Armor
Ignore Terrain



Circlet
Silk Robe
Genji Gauntlet

Pummel, Purification, Revive
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Panushenko
Monster
Sagittarius
60
66
Steel Giant










Pplvee1
Female
Aries
53
47
Geomancer
Steal
Damage Split
Secret Hunt
Swim

Giant Axe
Flame Shield
Triangle Hat
Clothes
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Steal Heart, Steal Helmet, Steal Accessory



Laserman1000
Male
Gemini
46
69
Oracle
Summon Magic
Parry
Equip Bow
Retreat

Yoichi Bow

Green Beret
White Robe
Rubber Shoes

Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic
Moogle, Ramuh, Ifrit, Bahamut, Odin, Salamander, Fairy

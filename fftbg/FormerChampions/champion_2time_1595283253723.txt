Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Scurg
Male
Aries
77
42
Samurai
Steal
Abandon
Equip Polearm
Waterwalking

Battle Bamboo

Iron Helmet
Black Robe
Dracula Mantle

Koutetsu, Murasame, Heaven's Cloud, Muramasa
Gil Taking, Steal Heart, Steal Accessory, Leg Aim



Helpimabug
Male
Leo
67
41
Monk
Charge
Faith Save
Equip Knife
Retreat

Air Knife

Thief Hat
Mystic Vest
Angel Ring

Pummel, Earth Slash, Purification, Revive
Charge+1, Charge+2



Joewcarson
Male
Pisces
65
62
Ninja
Item
Parry
Long Status
Move+2

Sasuke Knife
Kunai
Flash Hat
Mythril Vest
Diamond Armlet

Shuriken, Knife
Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy



Miku Shikhu
Male
Sagittarius
45
57
Geomancer
Throw
Counter Tackle
Equip Armor
Waterbreathing

Mythril Sword
Genji Shield
Diamond Helmet
Silk Robe
N-Kai Armlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bomb, Knife

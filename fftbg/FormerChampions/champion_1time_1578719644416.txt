Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Heartofgoldfish
Male
Scorpio
38
57
Archer
Item
Caution
Secret Hunt
Jump+2

Hunting Bow
Diamond Shield
Triangle Hat
Chain Vest
Wizard Mantle

Charge+1, Charge+3, Charge+4, Charge+7
Potion, Hi-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Crookus
Monster
Cancer
56
54
Steel Giant










Nissie
Female
Gemini
46
68
Samurai
Steal
Dragon Spirit
Short Status
Ignore Terrain

Heaven's Cloud

Iron Helmet
Wizard Robe
Spike Shoes

Koutetsu, Bizen Boat, Murasame, Muramasa, Kikuichimoji
Gil Taking, Steal Helmet, Steal Weapon



Lorine
Female
Gemini
80
73
Mime

Caution
Monster Talk
Lava Walking



Red Hood
Secret Clothes
Angel Ring

Mimic


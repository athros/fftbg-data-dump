Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Sypheck
Male
Pisces
65
39
Thief
Talk Skill
MP Restore
Concentrate
Jump+2

Assassin Dagger

Green Beret
Wizard Outfit
Rubber Shoes

Steal Heart, Leg Aim
Invitation, Persuade, Preach, Rehabilitate



Escorian
Monster
Scorpio
69
53
Dark Behemoth










Jeffisdelen
Male
Gemini
72
43
Knight
Punch Art
Caution
Short Status
Move-MP Up

Ragnarok

Genji Helmet
Silk Robe
Sprint Shoes

Head Break, Weapon Break, Stasis Sword, Dark Sword
Spin Fist, Pummel, Earth Slash, Chakra, Revive, Seal Evil



MerlinCross
Male
Leo
75
57
Oracle
Item
MP Restore
Throw Item
Retreat

Bestiary

Headgear
Robe of Lords
Elf Mantle

Poison, Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Paralyze
Potion, Hi-Potion, X-Potion, Phoenix Down

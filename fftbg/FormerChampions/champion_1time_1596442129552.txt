Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Roofiepops
Female
Cancer
50
62
Samurai
White Magic
Parry
Equip Bow
Teleport 2

Hunting Bow

Diamond Helmet
Linen Robe
Rubber Shoes

Heaven's Cloud, Kikuichimoji
Cure 2, Raise, Raise 2, Regen, Protect, Protect 2, Shell, Wall, Esuna



DAC169
Male
Gemini
54
60
Geomancer
Battle Skill
MA Save
Maintenance
Move+1

Giant Axe
Crystal Shield
Green Beret
Black Robe
Sprint Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Magic Break, Justice Sword, Explosion Sword



Lydian C
Female
Virgo
53
71
Squire
Draw Out
Speed Save
Maintenance
Teleport 2

Flail
Crystal Shield
Leather Hat
Earth Clothes
Cherche

Accumulate, Heal, Tickle
Koutetsu, Murasame, Muramasa



Chuckolator
Monster
Leo
64
57
Taiju








Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Moocaotao
Female
Capricorn
51
73
Lancer
Item
Parry
Secret Hunt
Fly

Partisan
Diamond Shield
Genji Helmet
White Robe
Feather Mantle

Level Jump2, Vertical Jump6
Potion, Ether, Antidote, Echo Grass, Holy Water, Phoenix Down



Red Celt
Female
Leo
48
70
Ninja
Summon Magic
Regenerator
Concentrate
Ignore Terrain

Dagger
Sasuke Knife
Twist Headband
Mythril Vest
Battle Boots

Shuriken, Bomb, Knife
Shiva, Ramuh, Ifrit, Odin, Silf, Fairy, Lich



OpHendoslice
Male
Leo
64
64
Knight
Basic Skill
Faith Save
Equip Bow
Move+3

Cross Bow
Bronze Shield
Platinum Helmet
Diamond Armor
Leather Mantle

Speed Break, Stasis Sword, Justice Sword
Dash, Heal, Cheer Up, Fury, Wish



Gorgewall
Female
Cancer
40
62
Dancer
Item
Brave Save
Short Status
Teleport

Ryozan Silk

Green Beret
Chain Vest
Leather Mantle

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Obsidian Blade
Hi-Potion, Echo Grass, Soft, Holy Water, Phoenix Down

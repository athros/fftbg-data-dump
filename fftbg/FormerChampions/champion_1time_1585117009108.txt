Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



PotionDweller
Female
Cancer
77
73
Summoner
Jump
Damage Split
Short Charge
Teleport 2

Thunder Rod

Black Hood
Adaman Vest
Defense Armlet

Carbunkle, Odin, Leviathan, Silf
Level Jump4, Vertical Jump7



EunosXX
Male
Aquarius
49
55
Chemist
Draw Out
Counter Tackle
Maintenance
Teleport

Mage Masher

Headgear
Earth Clothes
Sprint Shoes

Hi-Ether, Antidote, Echo Grass, Remedy, Phoenix Down
Asura, Koutetsu, Murasame



Black Sheep 0213
Monster
Sagittarius
41
47
Ultima Demon










Jeardra
Female
Scorpio
61
43
Samurai
Battle Skill
Regenerator
Equip Axe
Retreat

Gold Staff

Iron Helmet
Bronze Armor
Battle Boots

Murasame, Heaven's Cloud, Muramasa
Shield Break, Magic Break, Power Break, Stasis Sword, Dark Sword, Night Sword, Surging Sword

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Kronikle
Male
Capricorn
60
55
Knight
Charge
Counter
Equip Armor
Jump+1

Materia Blade
Mythril Shield
Genji Helmet
Clothes
Battle Boots

Armor Break, Speed Break, Power Break, Justice Sword
Charge+1, Charge+2, Charge+4, Charge+20



Lijarkh
Male
Taurus
60
61
Monk
Elemental
PA Save
Maintenance
Waterbreathing



Flash Hat
Wizard Outfit
Defense Ring

Secret Fist, Purification, Chakra, Revive
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



Lyonslegacy
Male
Virgo
55
43
Mediator
Steal
Earplug
Maintenance
Waterbreathing

Battle Folio

Holy Miter
Linen Robe
Feather Boots

Invitation, Persuade, Praise, Preach, Negotiate, Mimic Daravon
Gil Taking, Steal Shield, Steal Accessory, Leg Aim



Go2sleepTV
Female
Scorpio
78
46
Oracle
White Magic
MA Save
Attack UP
Move+2

Iron Fan

Feather Hat
Light Robe
Red Shoes

Blind, Poison, Pray Faith, Foxbird, Dispel Magic, Paralyze
Raise, Protect 2, Shell, Wall, Esuna, Holy

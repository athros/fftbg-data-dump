Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Reddwind
Male
Scorpio
63
58
Knight
Sing
Abandon
Magic Defense UP
Waterbreathing

Battle Axe
Escutcheon
Cross Helmet
Plate Mail
Small Mantle

Head Break, Speed Break, Mind Break, Justice Sword
Life Song



OtherBrand
Monster
Capricorn
46
78
Iron Hawk










Fenixcrest
Female
Taurus
75
74
Priest
Elemental
Abandon
Equip Shield
Swim

Flail
Bronze Shield
Holy Miter
Chameleon Robe
Sprint Shoes

Cure, Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna, Holy
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Forkmore
Male
Capricorn
52
63
Ninja
Draw Out
Abandon
Equip Armor
Move-HP Up

Hidden Knife
Orichalcum
Circlet
White Robe
Bracer

Shuriken, Bomb, Knife
Asura, Bizen Boat, Kiyomori, Muramasa

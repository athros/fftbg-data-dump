Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



UmaiJam
Female
Cancer
63
60
Wizard
Elemental
Parry
Short Charge
Move+2

Mage Masher

Flash Hat
Wizard Robe
Defense Armlet

Fire 3, Fire 4, Bolt 3, Ice 4
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Lava Ball



Superdevon1
Monster
Aries
55
43
Coeurl










ShintaroNayaka
Female
Sagittarius
46
48
Mediator
Item
Parry
Long Status
Lava Walking

Blast Gun

Feather Hat
Black Robe
Red Shoes

Praise, Solution, Insult, Mimic Daravon, Refute
Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Sairentozon7
Female
Sagittarius
80
78
Ninja
Charge
Counter Magic
Short Charge
Waterwalking

Ninja Edge
Sasuke Knife
Green Beret
Mythril Vest
Battle Boots

Shuriken
Charge+2, Charge+3, Charge+4, Charge+7

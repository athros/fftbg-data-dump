Player: !zChamp
Team: Champion Team
Palettes: Green/White



L2 Sentinel
Male
Aries
54
38
Archer
Summon Magic
Sunken State
Sicken
Move+2

Romanda Gun
Genji Shield
Triangle Hat
Judo Outfit
Spike Shoes

Charge+1, Charge+2, Charge+5, Charge+10, Charge+20
Shiva, Ifrit, Carbunkle, Salamander, Silf, Fairy



Goust18
Female
Sagittarius
53
50
Archer
Item
MP Restore
Doublehand
Waterwalking

Silver Bow

Feather Hat
Mythril Vest
Dracula Mantle

Charge+1, Charge+5
Hi-Potion, Phoenix Down



Godsblackarm
Female
Cancer
79
74
Wizard
Draw Out
Counter Magic
Halve MP
Teleport

Thunder Rod

Golden Hairpin
Light Robe
Dracula Mantle

Bolt 4, Ice 2, Frog
Murasame, Kiyomori



Kohlingen
Female
Gemini
42
58
Summoner
Dance
Distribute
Long Status
Move+2

White Staff

Red Hood
Brigandine
Jade Armlet

Moogle, Ramuh, Carbunkle, Leviathan, Fairy
Witch Hunt, Slow Dance, Polka Polka, Disillusion, Obsidian Blade, Void Storage

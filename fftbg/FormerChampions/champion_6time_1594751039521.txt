Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Dogsandcatsand
Female
Leo
48
52
Squire
Elemental
MP Restore
Equip Gun
Jump+2

Mythril Gun
Aegis Shield
Black Hood
Black Costume
Germinas Boots

Throw Stone, Heal, Tickle
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



JumbocactuarX27
Female
Gemini
62
80
Thief
Yin Yang Magic
Speed Save
Dual Wield
Move+2

Dagger
Mage Masher
Feather Hat
Earth Clothes
108 Gems

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim
Spell Absorb, Pray Faith, Zombie, Silence Song, Dispel Magic, Sleep



Dtrain332
Male
Serpentarius
54
65
Knight
Jump
Earplug
Equip Knife
Jump+3

Air Knife
Ice Shield
Iron Helmet
Genji Armor
Battle Boots

Magic Break, Power Break, Justice Sword
Level Jump2, Vertical Jump3



Mysteriousdewd
Male
Taurus
49
48
Lancer
Charge
PA Save
Concentrate
Move+1

Holy Lance
Bronze Shield
Crystal Helmet
Light Robe
Sprint Shoes

Level Jump8, Vertical Jump6
Charge+7

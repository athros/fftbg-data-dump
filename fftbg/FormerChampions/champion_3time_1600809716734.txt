Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Vorap
Female
Gemini
63
74
Thief
Time Magic
Blade Grasp
Defend
Waterwalking

Blood Sword

Twist Headband
Earth Clothes
Power Wrist

Steal Heart, Steal Armor, Arm Aim
Haste, Haste 2, Slow, Immobilize, Float, Quick, Stabilize Time



Arumz
Female
Aquarius
79
44
Mediator
Battle Skill
Absorb Used MP
Martial Arts
Jump+3

Bestiary

Red Hood
Clothes
Leather Mantle

Persuade, Preach, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Armor Break, Night Sword



Forkmore
Male
Aries
41
72
Archer
Summon Magic
Faith Save
Doublehand
Move+2

Snipe Bow

Black Hood
Judo Outfit
Germinas Boots

Charge+1, Charge+2, Charge+3
Shiva, Ramuh, Ifrit, Golem, Bahamut, Odin



Lord Gwarth
Male
Leo
62
62
Ninja
Draw Out
Regenerator
Equip Gun
Levitate

Mythril Gun
Blast Gun
Feather Hat
Mystic Vest
Rubber Shoes

Shuriken, Sword, Wand
Bizen Boat, Kiyomori

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Friendsquirrel
Male
Cancer
79
79
Calculator
Byblos
Abandon
Dual Wield
Waterwalking

Battle Folio
Bestiary
Black Hood
Power Sleeve
Genji Gauntlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Hamborn
Female
Virgo
59
72
Archer
White Magic
Auto Potion
Equip Armor
Move+3

Long Bow

Genji Helmet
Wizard Outfit
Magic Gauntlet

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Cure 2, Cure 3, Raise, Regen, Shell 2, Esuna



Hzor
Male
Aquarius
46
76
Ninja
Summon Magic
Dragon Spirit
Concentrate
Retreat

Flail
Short Edge
Golden Hairpin
Black Costume
Magic Gauntlet

Knife, Sword, Hammer, Staff, Dictionary
Shiva, Carbunkle, Salamander, Fairy, Lich



B0shii
Female
Virgo
74
78
Summoner
Talk Skill
Sunken State
Beastmaster
Swim

Gold Staff

Leather Hat
Chameleon Robe
Battle Boots

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Fairy, Lich
Threaten, Solution, Insult, Negotiate, Refute, Rehabilitate

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Firesheath
Female
Virgo
68
80
Ninja
Punch Art
Sunken State
Defend
Teleport

Hidden Knife
Koga Knife
Leather Hat
Wizard Outfit
Power Wrist

Stick, Wand
Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive



Choco Joe
Female
Cancer
71
66
Samurai
Elemental
Auto Potion
Equip Bow
Ignore Height

Long Bow

Leather Helmet
Leather Armor
Diamond Armlet

Asura, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Pitfall, Water Ball, Local Quake, Sand Storm, Gusty Wind



Lucientes
Male
Cancer
72
62
Priest
Black Magic
Auto Potion
Beastmaster
Move+1

Gold Staff

Red Hood
Silk Robe
Wizard Mantle

Raise, Esuna
Bolt 3, Empower



Lydian C
Female
Taurus
79
43
Time Mage
Item
MA Save
Doublehand
Retreat

Gokuu Rod

Flash Hat
Mythril Vest
Genji Gauntlet

Haste, Haste 2, Slow 2, Demi
Potion, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down

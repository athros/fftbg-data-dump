Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Digitalsocrates
Female
Taurus
48
73
Samurai
Punch Art
Caution
Equip Bow
Teleport 2

Hunting Bow

Cross Helmet
Bronze Armor
Spike Shoes

Muramasa
Wave Fist, Secret Fist, Purification, Chakra, Revive



E Ballard
Male
Cancer
57
71
Calculator
Black Magic
HP Restore
Equip Knife
Lava Walking

Hidden Knife

Leather Hat
Rubber Costume
Reflect Ring

CT, Height, Prime Number, 4, 3
Fire 2, Fire 3, Bolt, Bolt 3, Ice, Ice 2, Flare



Cryptopsy70
Male
Virgo
59
44
Summoner
Item
PA Save
Defend
Move+2

Ice Rod

Headgear
Clothes
Diamond Armlet

Shiva
Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down



Baron Von Scrub
Female
Virgo
56
50
Squire
Battle Skill
Speed Save
Doublehand
Move+3

Rune Blade

Headgear
Mythril Vest
Defense Armlet

Accumulate, Dash, Wish
Weapon Break

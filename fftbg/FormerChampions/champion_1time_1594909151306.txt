Player: !zChamp
Team: Champion Team
Palettes: Green/White



Run With Stone GUNs
Male
Virgo
66
46
Mediator
Throw
Damage Split
Dual Wield
Move+3

Bestiary
Papyrus Codex
Red Hood
Secret Clothes
Spike Shoes

Persuade, Praise, Threaten, Death Sentence, Refute, Rehabilitate
Bomb, Sword, Stick



LDSkinny
Female
Pisces
48
69
Oracle
Item
Meatbone Slash
Equip Polearm
Jump+2

Partisan

Holy Miter
Black Costume
Diamond Armlet

Spell Absorb, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Petrify
Potion, Echo Grass, Maiden's Kiss, Phoenix Down



TeaTime29
Monster
Gemini
59
66
Grenade










PoroTact
Female
Pisces
53
80
Dancer
Elemental
MA Save
Defend
Move-MP Up

Panther Bag

Flash Hat
Black Robe
Defense Ring

Witch Hunt, Wiznaibus, Nameless Dance, Obsidian Blade, Dragon Pit
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lord Burrah
Female
Capricorn
77
60
Chemist
Steal
Faith Save
Dual Wield
Ignore Height

Main Gauche
Blind Knife
Barette
Mythril Vest
Magic Gauntlet

Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down
Gil Taking, Steal Armor, Steal Status, Arm Aim



Kronikle
Male
Libra
71
72
Knight
Summon Magic
Damage Split
Sicken
Move+3

Defender
Gold Shield
Iron Helmet
Gold Armor
Jade Armlet

Head Break, Weapon Break, Power Break, Night Sword
Moogle, Salamander, Silf, Fairy



Xoomwaffle
Male
Taurus
52
57
Mime

Speed Save
Magic Attack UP
Move-MP Up



Red Hood
Judo Outfit
Leather Mantle

Mimic




E Ballard
Female
Taurus
49
66
Squire
Charge
Meatbone Slash
Equip Polearm
Move+3

Partisan
Buckler
Flash Hat
Adaman Vest
Magic Ring

Accumulate, Cheer Up, Fury, Wish, Scream
Charge+1, Charge+5, Charge+7, Charge+10

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



CapnChaos12
Female
Leo
43
50
Knight
Yin Yang Magic
Earplug
Concentrate
Ignore Terrain

Defender
Crystal Shield
Mythril Helmet
Mythril Armor
Germinas Boots

Weapon Break, Night Sword, Surging Sword
Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify



Toka222
Male
Pisces
53
61
Ninja
Item
Parry
Sicken
Lava Walking

Orichalcum
Hidden Knife
Holy Miter
Earth Clothes
Vanish Mantle

Knife, Sword, Wand
Potion, Hi-Potion, Ether, Hi-Ether



Miyokari
Female
Cancer
74
43
Thief
Talk Skill
Counter Magic
Monster Talk
Move-MP Up

Hidden Knife

Twist Headband
Wizard Outfit
Rubber Shoes

Steal Helmet, Steal Armor, Steal Weapon
Persuade, Threaten, Refute



Nizaha
Male
Taurus
50
79
Priest
Talk Skill
Catch
Secret Hunt
Teleport

Scorpion Tail

Holy Miter
Light Robe
Salty Rage

Cure 4, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Esuna
Persuade, Praise, Threaten, Preach, Insult, Mimic Daravon

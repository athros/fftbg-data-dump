Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Creggers
Male
Aquarius
44
43
Ninja
White Magic
Hamedo
Short Status
Retreat

Mythril Knife
Spell Edge
Triangle Hat
Earth Clothes
Small Mantle

Shuriken, Dictionary
Cure 3, Raise, Raise 2, Regen, Protect 2, Wall, Esuna, Holy



Sans From Snowdin
Male
Cancer
52
61
Lancer
Summon Magic
Sunken State
Dual Wield
Move+1

Spear
Holy Lance
Bronze Helmet
Platinum Armor
Rubber Shoes

Level Jump5, Vertical Jump6
Moogle, Ramuh, Silf, Lich



Watzel
Female
Cancer
64
74
Mediator
Jump
Mana Shield
Doublehand
Move-HP Up

Blaze Gun

Holy Miter
White Robe
Defense Armlet

Invitation, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Level Jump8, Vertical Jump8



Just Here2
Female
Aquarius
76
56
Calculator
Yin Yang Magic
Faith Save
Short Status
Move+2

Papyrus Codex

Feather Hat
Earth Clothes
Battle Boots

Height, 4, 3
Poison, Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep

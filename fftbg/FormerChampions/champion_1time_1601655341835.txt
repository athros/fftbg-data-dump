Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Daveb
Male
Leo
62
70
Ninja
Steal
Auto Potion
Magic Defense UP
Waterwalking

Ninja Edge
Hidden Knife
Leather Hat
Chain Vest
Small Mantle

Shuriken, Bomb, Wand
Steal Heart, Steal Status, Arm Aim



Khelor
Monster
Cancer
49
67
Ahriman










Aaron2Nerdy
Male
Gemini
72
77
Wizard
Jump
Faith Save
Martial Arts
Move-HP Up



Twist Headband
Black Robe
Magic Gauntlet

Fire, Fire 4, Ice
Level Jump8, Vertical Jump3



DavenIII
Male
Sagittarius
55
42
Geomancer
Item
Distribute
Equip Armor
Move-MP Up

Heaven's Cloud
Flame Shield
Thief Hat
Platinum Armor
Reflect Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, Antidote, Echo Grass, Soft, Phoenix Down

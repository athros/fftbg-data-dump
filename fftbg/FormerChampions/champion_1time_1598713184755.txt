Player: !zChamp
Team: Champion Team
Palettes: Green/White



Ko2q
Male
Virgo
61
75
Wizard
Time Magic
Parry
Defense UP
Ignore Terrain

Assassin Dagger

Flash Hat
Mythril Vest
Feather Boots

Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Flare
Float, Stabilize Time



Lokenwow
Male
Scorpio
62
54
Oracle
Talk Skill
Dragon Spirit
Dual Wield
Swim

Octagon Rod
Gokuu Rod
Twist Headband
White Robe
Red Shoes

Spell Absorb, Pray Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze
Invitation, Persuade, Threaten, Refute, Rehabilitate



Sairentozon7
Female
Pisces
80
74
Thief
Throw
MA Save
Equip Polearm
Move+3

Gokuu Rod

Red Hood
Chain Vest
Dracula Mantle

Gil Taking, Steal Heart, Steal Status
Shuriken, Knife



Nizaha
Male
Leo
45
61
Thief
Black Magic
HP Restore
Long Status
Retreat

Main Gauche

Flash Hat
Wizard Outfit
Red Shoes

Gil Taking, Steal Weapon, Leg Aim
Fire 2, Bolt 3, Bolt 4, Ice 3

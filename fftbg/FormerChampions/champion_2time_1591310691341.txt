Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Thyrandaal
Monster
Capricorn
72
74
Vampire










Sinnyil2
Monster
Cancer
71
75
Red Chocobo










DustBirdEX
Female
Gemini
36
52
Samurai
Battle Skill
Blade Grasp
Maintenance
Retreat

Mythril Spear

Mythril Helmet
Chameleon Robe
Magic Ring

Asura, Koutetsu, Bizen Boat, Murasame
Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Explosion Sword



Wyonearth
Male
Cancer
74
79
Priest
Elemental
Damage Split
Halve MP
Move+3

Gold Staff

Triangle Hat
Power Sleeve
Magic Ring

Cure 3, Raise, Protect, Protect 2, Shell, Shell 2, Esuna, Holy
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

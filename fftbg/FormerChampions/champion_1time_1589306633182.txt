Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Gelwain
Male
Leo
63
63
Ninja
Charge
Caution
Equip Gun
Move+2

Glacier Gun
Blast Gun
Triangle Hat
Leather Outfit
Dracula Mantle

Shuriken, Bomb, Knife
Charge+1, Charge+5, Charge+7



Volgrathemoose
Female
Leo
67
48
Geomancer
Item
HP Restore
Attack UP
Swim

Coral Sword
Diamond Shield
Red Hood
Chain Vest
Dracula Mantle

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
X-Potion, Ether, Eye Drop, Phoenix Down



Miyokari
Monster
Leo
65
59
Chocobo










LAGBOT30000
Male
Capricorn
66
52
Oracle
Charge
Regenerator
Beastmaster
Jump+1

Cypress Rod

Golden Hairpin
Wizard Outfit
Germinas Boots

Blind, Pray Faith, Doubt Faith, Silence Song, Foxbird, Confusion Song, Paralyze, Dark Holy
Charge+1, Charge+2, Charge+4, Charge+10

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



SpencoJFrog
Male
Sagittarius
44
55
Oracle
Sing
Speed Save
Equip Bow
Fly

Ice Bow

Holy Miter
Black Robe
Dracula Mantle

Blind, Poison, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep
Battle Song



WhiteTigress
Male
Aries
64
59
Archer
Elemental
HP Restore
Doublehand
Ignore Height

Cross Bow

Golden Hairpin
Wizard Outfit
Angel Ring

Charge+3, Charge+5, Charge+7, Charge+10
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Blizzard, Lava Ball



DejaPoo21
Female
Virgo
76
52
Summoner
Battle Skill
Counter
Equip Gun
Move+1

Glacier Gun

Black Hood
Black Robe
Bracer

Moogle, Ifrit, Bahamut, Odin, Leviathan, Salamander, Fairy
Head Break, Armor Break, Shield Break, Speed Break



Jigglefluffenstuff
Female
Gemini
50
71
Dancer
Yin Yang Magic
Hamedo
Maintenance
Move+1

Persia

Twist Headband
Clothes
Cursed Ring

Slow Dance, Nameless Dance, Last Dance, Obsidian Blade
Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Klonoa331
Male
Virgo
42
70
Chemist
Yin Yang Magic
Counter
Halve MP
Jump+1

Cute Bag

Golden Hairpin
Judo Outfit
Feather Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down
Blind, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Fenaen
Male
Virgo
42
67
Oracle
Item
Meatbone Slash
Equip Bow
Jump+1

Bow Gun

Green Beret
Black Robe
Elf Mantle

Blind, Doubt Faith, Blind Rage, Confusion Song, Sleep
Potion, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down



DrAntiSocial
Male
Pisces
45
47
Lancer
Item
Arrow Guard
Doublehand
Ignore Terrain

Obelisk

Mythril Helmet
Gold Armor
108 Gems

Level Jump8, Vertical Jump7
Potion, Eye Drop, Holy Water, Remedy, Phoenix Down



EizanTayama
Male
Virgo
56
60
Wizard
Basic Skill
Brave Up
Defense UP
Lava Walking

Wizard Rod

Thief Hat
Silk Robe
Small Mantle

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 4, Empower, Flare
Heal, Fury, Scream

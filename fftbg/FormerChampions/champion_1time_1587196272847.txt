Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Lijarkh
Male
Taurus
71
59
Monk
Sing
HP Restore
Halve MP
Move+2



Twist Headband
Mythril Vest
Small Mantle

Spin Fist, Purification, Chakra, Seal Evil
Angel Song, Magic Song, Space Storage



L2 Sentinel
Male
Cancer
77
44
Monk
Yin Yang Magic
Critical Quick
Dual Wield
Levitate



Golden Hairpin
Judo Outfit
Wizard Mantle

Pummel, Earth Slash, Purification, Chakra, Revive, Seal Evil
Life Drain, Doubt Faith, Foxbird



Sinnyil2
Male
Leo
48
69
Oracle
Jump
Critical Quick
Secret Hunt
Ignore Terrain

Octagon Rod

Headgear
White Robe
Wizard Mantle

Blind, Life Drain, Pray Faith, Zombie, Silence Song
Level Jump2, Vertical Jump2



Humble Fabio
Monster
Leo
44
38
Red Dragon








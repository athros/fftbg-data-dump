Player: !zChamp
Team: Champion Team
Palettes: Green/White



Lordhewlett
Female
Leo
42
75
Geomancer
Battle Skill
Damage Split
Equip Sword
Move+1

Kikuichimoji
Mythril Shield
Holy Miter
Adaman Vest
Bracer

Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Armor Break, Weapon Break, Power Break, Mind Break, Dark Sword



DavenIII
Male
Capricorn
45
54
Samurai
Jump
Caution
Dual Wield
Move+3

Partisan
Javelin
Genji Helmet
Crystal Mail
Red Shoes

Asura, Koutetsu, Murasame
Level Jump8, Vertical Jump2



WoooBlaaa
Male
Gemini
55
63
Oracle
Battle Skill
Faith Up
Secret Hunt
Move+1

Ivory Rod

Black Hood
Black Robe
Cursed Ring

Blind, Spell Absorb, Doubt Faith, Blind Rage, Confusion Song
Head Break, Weapon Break, Magic Break, Power Break, Justice Sword, Dark Sword



Cataphract116
Monster
Gemini
49
58
Chocobo








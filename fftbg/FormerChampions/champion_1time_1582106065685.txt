Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Upvla
Male
Capricorn
58
44
Wizard
Steal
Meatbone Slash
Short Charge
Lava Walking

Thunder Rod

Holy Miter
White Robe
Rubber Shoes

Fire 2, Fire 4, Bolt, Bolt 4, Ice 3, Ice 4, Empower, Death
Steal Armor, Steal Shield, Steal Accessory



VRNocturne
Female
Capricorn
76
57
Archer
Steal
Catch
Dual Wield
Teleport

Mythril Bow

Golden Hairpin
Wizard Outfit
Small Mantle

Charge+1, Charge+2, Charge+3, Charge+5
Steal Heart, Steal Weapon, Arm Aim



Sypheck
Female
Cancer
71
77
Dancer
Talk Skill
Parry
Doublehand
Waterwalking

Ryozan Silk

Green Beret
Black Robe
Elf Mantle

Witch Hunt, Polka Polka, Disillusion, Last Dance
Praise, Negotiate, Refute



Turious
Female
Sagittarius
69
46
Chemist
Elemental
Caution
Equip Bow
Lava Walking

Long Bow

Green Beret
Mythril Vest
Rubber Shoes

Hi-Potion, Hi-Ether, Soft, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

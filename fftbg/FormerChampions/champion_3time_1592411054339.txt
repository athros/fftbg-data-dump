Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Zmoses
Monster
Gemini
78
62
Squidraken










Thyrandaal
Male
Gemini
39
57
Lancer
Throw
Mana Shield
Doublehand
Move-HP Up

Holy Lance

Cross Helmet
Crystal Mail
Magic Gauntlet

Level Jump8, Vertical Jump7
Shuriken, Knife



BlackFireUK
Female
Capricorn
61
58
Time Mage
White Magic
Speed Save
Short Charge
Jump+3

Gold Staff

Triangle Hat
Black Robe
Jade Armlet

Haste, Haste 2, Slow, Quick, Meteor
Cure, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna, Holy



D4rr1n
Female
Scorpio
69
71
Geomancer
Item
Regenerator
Throw Item
Move+3

Blood Sword
Escutcheon
Green Beret
Chameleon Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm
Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water

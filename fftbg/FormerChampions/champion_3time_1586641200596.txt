Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ThePineappleSalesman
Monster
Taurus
67
63
King Behemoth










CapnChaos12
Monster
Cancer
52
70
Dragon










MantisFinch
Female
Leo
50
40
Lancer
Talk Skill
Sunken State
Monster Talk
Retreat

Holy Lance
Platinum Shield
Cross Helmet
Chain Mail
Jade Armlet

Level Jump5, Vertical Jump2
Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Arch8000
Female
Gemini
53
51
Lancer
Punch Art
Caution
Equip Polearm
Retreat

Ryozan Silk
Escutcheon
Iron Helmet
Silk Robe
Diamond Armlet

Level Jump5, Vertical Jump8
Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Seal Evil

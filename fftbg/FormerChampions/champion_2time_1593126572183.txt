Player: !zChamp
Team: Champion Team
Palettes: Green/White



Brokenknight201
Female
Capricorn
49
53
Calculator
Black Magic
Regenerator
Equip Bow
Fly

Bow Gun

Flash Hat
Adaman Vest
Bracer

Height, Prime Number, 3
Ice 3, Death



Sparker9
Monster
Virgo
60
69
Archaic Demon










Zalerah
Female
Leo
46
39
Wizard
Draw Out
Counter Magic
Martial Arts
Teleport



Twist Headband
Mythril Vest
Feather Boots

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Frog
Murasame, Heaven's Cloud, Kikuichimoji, Masamune



Hasterious
Male
Aquarius
65
56
Squire
Draw Out
Abandon
Magic Attack UP
Jump+2

Dagger
Mythril Shield
Twist Headband
Leather Armor
Defense Ring

Accumulate, Heal, Yell, Wish
Muramasa, Kikuichimoji

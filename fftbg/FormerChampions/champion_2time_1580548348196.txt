Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Ninesilvers
Male
Cancer
45
46
Ninja
Jump
Abandon
Equip Gun
Retreat

Glacier Gun
Romanda Gun
Leather Hat
Rubber Costume
Wizard Mantle

Bomb
Level Jump4, Vertical Jump5



Cpt Qc
Male
Taurus
72
63
Monk
Charge
Abandon
Sicken
Move+1



Feather Hat
Leather Outfit
Wizard Mantle

Pummel, Purification, Chakra, Revive, Seal Evil
Charge+1, Charge+5, Charge+10



Squirespawn
Female
Libra
75
73
Ninja
White Magic
Counter Flood
Equip Axe
Move-MP Up

Hidden Knife
Healing Staff
Flash Hat
Leather Outfit
Feather Boots

Bomb, Knife
Raise, Raise 2, Regen, Protect, Shell 2, Wall, Esuna



KupoKel
Female
Libra
55
60
Monk
Basic Skill
Abandon
Sicken
Jump+2



Golden Hairpin
Earth Clothes
Wizard Mantle

Spin Fist, Secret Fist, Purification, Seal Evil
Heal, Wish, Scream

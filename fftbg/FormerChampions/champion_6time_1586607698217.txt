Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ALY327
Monster
Sagittarius
74
45
Dark Behemoth










Chuckolator
Male
Serpentarius
45
58
Thief
Talk Skill
HP Restore
Equip Gun
Waterbreathing

Bestiary

Cachusha
Leather Outfit
Battle Boots

Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Invitation, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Arch8000
Male
Leo
79
76
Priest
Black Magic
Counter Magic
Defense UP
Jump+3

Flail

Twist Headband
Silk Robe
Sprint Shoes

Cure, Cure 3, Raise, Regen, Esuna
Fire, Ice 2, Empower



V3rdeni
Male
Gemini
48
44
Chemist
Throw
HP Restore
Attack UP
Swim

Blaze Gun

Thief Hat
Black Costume
Feather Mantle

Hi-Ether, Antidote, Echo Grass, Phoenix Down
Shuriken, Sword

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Joewcarson
Male
Aries
70
68
Lancer
Item
Dragon Spirit
Secret Hunt
Jump+1

Holy Lance
Aegis Shield
Diamond Helmet
Diamond Armor
Genji Gauntlet

Level Jump8, Vertical Jump8
Potion, Antidote, Soft, Holy Water, Remedy, Phoenix Down



Moonliquor
Female
Pisces
59
61
Chemist
Battle Skill
Counter Flood
Beastmaster
Move+3

Cute Bag

Twist Headband
Mythril Vest
Defense Armlet

Potion, Ether, Eye Drop, Phoenix Down
Head Break, Shield Break, Magic Break, Speed Break, Power Break, Justice Sword, Night Sword, Surging Sword



Mtueni
Male
Aries
41
76
Samurai
Basic Skill
Parry
Secret Hunt
Jump+3

Javelin

Genji Helmet
Bronze Armor
Battle Boots

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji, Masamune, Chirijiraden
Cheer Up



Maakur
Male
Capricorn
71
67
Thief
Time Magic
Abandon
Doublehand
Move-HP Up

Orichalcum

Feather Hat
Mythril Vest
Magic Ring

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Haste, Slow, Quick, Stabilize Time

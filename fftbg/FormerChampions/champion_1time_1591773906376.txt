Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Silentkaster
Female
Virgo
39
64
Lancer
Talk Skill
Meatbone Slash
Equip Polearm
Retreat

Ivory Rod
Bronze Shield
Barbuta
Chain Mail
Bracer

Level Jump8, Vertical Jump6
Preach, Death Sentence, Negotiate, Refute



EnemyController
Male
Pisces
67
63
Knight
Basic Skill
Faith Save
Defense UP
Ignore Terrain

Defender
Aegis Shield
Gold Helmet
Leather Armor
Dracula Mantle

Shield Break, Weapon Break, Speed Break, Justice Sword
Accumulate, Dash, Throw Stone, Heal



Killth3kid
Female
Aquarius
46
71
Squire
Item
Meatbone Slash
Throw Item
Lava Walking

Poison Bow
Genji Shield
Headgear
Judo Outfit
Jade Armlet

Accumulate, Throw Stone, Heal, Yell
Potion, X-Potion



Phi Sig
Male
Serpentarius
58
42
Bard
Jump
Earplug
Long Status
Fly

Ramia Harp

Golden Hairpin
Leather Outfit
Small Mantle

Life Song, Space Storage, Sky Demon
Level Jump5, Vertical Jump8

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Mala
Female
Capricorn
46
52
Thief
Draw Out
Critical Quick
Magic Defense UP
Swim

Short Edge

Green Beret
Chain Vest
Magic Ring

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Status, Leg Aim
Koutetsu, Muramasa



Kendre
Monster
Aries
67
60
Blue Dragon










Alta
Female
Aquarius
75
44
Mediator
Battle Skill
HP Restore
Defend
Lava Walking

Romanda Gun

Golden Hairpin
Silk Robe
Germinas Boots

Persuade, Praise, Negotiate, Mimic Daravon, Refute
Shield Break, Weapon Break



Gilberte
Female
Taurus
43
79
Chemist
Charge
Brave Up
Attack UP
Move+1

Blaze Gun

Flash Hat
Adaman Vest
Wizard Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Remedy, Phoenix Down
Charge+4, Charge+7



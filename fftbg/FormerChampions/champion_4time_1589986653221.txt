Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ZZ Yoshi
Female
Gemini
71
50
Dancer
White Magic
Critical Quick
Equip Shield
Move+3

Ryozan Silk
Aegis Shield
Flash Hat
Black Robe
Cursed Ring

Wiznaibus, Last Dance, Nether Demon, Dragon Pit
Cure, Cure 2, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna



DaveStrider55
Female
Pisces
56
61
Thief
Throw
Auto Potion
Equip Gun
Waterwalking

Romanda Gun

Green Beret
Mythril Vest
Sprint Shoes

Steal Helmet
Shuriken, Bomb, Knife, Staff, Axe



Roqqqpsi
Male
Gemini
54
60
Calculator
Farm Skill
Sunken State
Short Status
Move+3

Ice Rod
Genji Shield
Flash Hat
Genji Armor
Spike Shoes

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



Twelfthrootoftwo
Male
Aries
44
69
Thief
Time Magic
Parry
Magic Defense UP
Ignore Terrain

Spell Edge

Thief Hat
Adaman Vest
Reflect Ring

Gil Taking, Steal Armor, Steal Shield
Haste, Haste 2, Slow, Stop, Immobilize, Float, Demi 2

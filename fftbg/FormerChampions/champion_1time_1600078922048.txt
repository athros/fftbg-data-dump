Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



CassiePhoenix
Female
Pisces
65
43
Samurai
Dance
Auto Potion
Magic Attack UP
Jump+3

Obelisk

Diamond Helmet
Platinum Armor
Dracula Mantle

Bizen Boat, Heaven's Cloud, Kiyomori
Slow Dance, Disillusion, Last Dance



Zachara
Male
Serpentarius
73
61
Lancer
Battle Skill
Faith Save
Doublehand
Jump+1

Gokuu Rod

Mythril Helmet
Crystal Mail
Defense Ring

Level Jump5, Vertical Jump7
Weapon Break, Magic Break, Justice Sword



Reddwind
Male
Cancer
57
43
Knight
White Magic
Hamedo
Magic Defense UP
Ignore Terrain

Defender
Ice Shield
Circlet
Diamond Armor
Rubber Shoes

Magic Break, Mind Break, Dark Sword
Raise 2, Reraise, Wall, Esuna



Grininda
Male
Aquarius
74
75
Archer
Punch Art
Auto Potion
Doublehand
Jump+1

Long Bow

Holy Miter
Judo Outfit
Defense Ring

Charge+2, Charge+3
Wave Fist, Earth Slash, Purification, Revive

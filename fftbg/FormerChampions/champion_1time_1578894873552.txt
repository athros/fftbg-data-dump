Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



TristalMTG
Male
Gemini
50
56
Archer
Yin Yang Magic
Brave Up
Magic Defense UP
Ignore Terrain

Perseus Bow

Ribbon
Mythril Vest
Defense Ring

Charge+3, Charge+5
Blind Rage, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Virilikus
Monster
Serpentarius
44
39
Explosive










ItotheCtotheE
Male
Virgo
72
42
Thief
Throw
Arrow Guard
Equip Sword
Move-HP Up

Rune Blade

Thief Hat
Clothes
Dracula Mantle

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Leg Aim
Shuriken, Bomb, Sword, Dictionary



Error72
Female
Cancer
70
52
Monk
Dance
Sunken State
Defend
Lava Walking



Holy Miter
Black Costume
N-Kai Armlet

Pummel, Wave Fist, Purification, Revive
Wiznaibus, Dragon Pit

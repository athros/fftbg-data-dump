Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Nifboy
Male
Cancer
75
42
Wizard
Math Skill
Sunken State
Sicken
Fly

Mage Masher

Feather Hat
Adaman Vest
Vanish Mantle

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Empower, Frog, Flare
Height, 5, 4



Gorgewall
Male
Leo
48
54
Lancer
Item
Speed Save
Dual Wield
Levitate

Dragon Whisker
Holy Lance
Diamond Helmet
Linen Robe
Dracula Mantle

Level Jump8, Vertical Jump8
Potion, X-Potion, Echo Grass, Soft, Phoenix Down



Vorap
Male
Aquarius
69
63
Mime

Counter Tackle
Martial Arts
Ignore Terrain



Red Hood
Mystic Vest
Wizard Mantle

Mimic




DLJuggernaut
Male
Aquarius
45
68
Ninja
White Magic
Speed Save
Maintenance
Move+2

Mythril Knife
Zorlin Shape
Green Beret
Chain Vest
Cursed Ring

Knife
Cure, Cure 2, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2

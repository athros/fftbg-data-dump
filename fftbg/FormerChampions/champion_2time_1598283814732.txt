Player: !zChamp
Team: Champion Team
Palettes: Green/White



NovaKnight21
Female
Leo
38
44
Samurai
Throw
Sunken State
Equip Sword
Levitate

Defender

Diamond Helmet
Linen Cuirass
Dracula Mantle

Bizen Boat, Heaven's Cloud, Kikuichimoji
Bomb, Wand



Maakur
Female
Taurus
68
52
Knight
Charge
Meatbone Slash
Equip Polearm
Move-MP Up

Obelisk
Platinum Shield
Iron Helmet
Linen Cuirass
Jade Armlet

Head Break, Shield Break, Mind Break, Stasis Sword
Charge+1, Charge+2, Charge+3, Charge+5



Lydian C
Female
Gemini
52
44
Samurai
Steal
Arrow Guard
Secret Hunt
Jump+3

Kiyomori

Genji Helmet
Mythril Armor
Magic Gauntlet

Koutetsu, Kiyomori, Kikuichimoji
Steal Heart, Steal Status, Leg Aim



Powergems
Monster
Leo
66
38
Dryad








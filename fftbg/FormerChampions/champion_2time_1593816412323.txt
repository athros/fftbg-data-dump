Player: !zChamp
Team: Champion Team
Palettes: Green/White



Forkmore
Female
Pisces
75
66
Summoner
Battle Skill
Parry
Short Charge
Move+1

Rod

Black Hood
Chameleon Robe
Magic Ring

Moogle, Ifrit, Titan, Bahamut, Silf
Armor Break, Speed Break, Power Break, Mind Break, Justice Sword



Laserman1000
Male
Libra
55
53
Calculator
White Magic
Dragon Spirit
Secret Hunt
Retreat

Papyrus Codex

Flash Hat
Linen Robe
Reflect Ring

CT, 5, 4, 3
Cure, Cure 2, Cure 3, Raise, Regen, Protect, Shell 2, Esuna



Galkife
Female
Pisces
69
59
Wizard
Math Skill
HP Restore
Equip Armor
Move+2

Ice Rod

Grand Helmet
Brigandine
Cursed Ring

Fire 3, Bolt, Bolt 4, Ice, Ice 2
CT, Height, Prime Number, 3



Lowlf
Male
Libra
61
47
Monk
Summon Magic
Hamedo
Dual Wield
Move-MP Up



Leather Hat
Power Sleeve
N-Kai Armlet

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil
Shiva, Ramuh, Carbunkle, Leviathan, Fairy

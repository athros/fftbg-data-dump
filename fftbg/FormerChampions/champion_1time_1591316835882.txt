Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Reinoe
Female
Pisces
64
77
Samurai
Basic Skill
Counter Magic
Equip Bow
Levitate

Ice Bow

Crystal Helmet
Genji Armor
Jade Armlet

Asura, Bizen Boat, Kiyomori, Kikuichimoji
Dash, Throw Stone, Heal, Tickle, Ultima



Nok
Female
Leo
76
60
Thief
Item
Arrow Guard
Throw Item
Ignore Terrain

Orichalcum

Triangle Hat
Leather Outfit
Genji Gauntlet

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Leg Aim
Potion, X-Potion, Ether, Hi-Ether, Remedy, Phoenix Down



Waterwatereverywhere
Female
Leo
55
77
Samurai
Item
Parry
Secret Hunt
Move+2

Kiyomori

Gold Helmet
Genji Armor
Genji Gauntlet

Bizen Boat
Potion, Hi-Potion, Antidote, Eye Drop, Remedy, Phoenix Down



ThePineappleSalesman
Male
Aquarius
60
55
Geomancer
Summon Magic
Counter
Dual Wield
Move+2

Giant Axe
Battle Axe
Red Hood
Black Robe
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Moogle, Titan, Leviathan, Lich

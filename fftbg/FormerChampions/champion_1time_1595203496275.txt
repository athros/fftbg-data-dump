Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Laserman1000
Male
Cancer
62
78
Chemist
Sing
Damage Split
Beastmaster
Jump+3

Hydra Bag

Flash Hat
Chain Vest
Rubber Shoes

Potion, Soft, Remedy, Phoenix Down
Nameless Song



Kronikle
Male
Scorpio
70
56
Ninja
Item
Counter
Equip Sword
Ignore Height

Diamond Sword
Ice Brand
Flash Hat
Clothes
Elf Mantle

Bomb, Ninja Sword
Potion, Hi-Ether, Echo Grass, Holy Water, Remedy, Phoenix Down



Killth3kid
Monster
Scorpio
69
63
Dark Behemoth










Heroebal
Male
Capricorn
46
54
Geomancer
Draw Out
Distribute
Maintenance
Move+2

Iron Sword
Crystal Shield
Feather Hat
Wizard Outfit
Defense Armlet

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Koutetsu, Heaven's Cloud, Kiyomori

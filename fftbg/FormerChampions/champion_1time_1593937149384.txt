Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Lowlf
Female
Cancer
68
75
Chemist
Dance
Counter Flood
Equip Shield
Move+1

Mythril Gun
Genji Shield
Holy Miter
Chain Vest
Cursed Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Witch Hunt



ThreeMileIsland
Female
Taurus
74
51
Wizard
Throw
Catch
Short Charge
Jump+3

Dragon Rod

Flash Hat
Wizard Robe
N-Kai Armlet

Fire 3, Bolt, Bolt 4, Ice, Ice 2, Ice 3, Empower, Flare
Dictionary



Roofiepops
Male
Capricorn
46
61
Ninja
Charge
Damage Split
Maintenance
Fly

Short Edge
Sasuke Knife
Feather Hat
Power Sleeve
Defense Ring

Shuriken, Bomb
Charge+7, Charge+20



Oobs56
Female
Aquarius
46
54
Monk
Battle Skill
MA Save
Beastmaster
Swim



Black Hood
Chain Vest
Cherche

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Seal Evil
Shield Break, Dark Sword

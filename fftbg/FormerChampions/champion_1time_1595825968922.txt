Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



MLWebz0r
Monster
Capricorn
53
53
Dragon










Kahjeethaswares
Female
Capricorn
67
63
Archer
Jump
PA Save
Equip Shield
Waterbreathing

Gastrafitis
Diamond Shield
Barette
Leather Outfit
Leather Mantle

Charge+1, Charge+4, Charge+7, Charge+10
Level Jump5, Vertical Jump8



Powergems
Male
Aries
72
80
Geomancer
Black Magic
Abandon
Martial Arts
Move+2


Buckler
Headgear
Wizard Outfit
Angel Ring

Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Fire 2, Ice 2



Treafa
Male
Aries
65
70
Priest
Item
Counter
Equip Sword
Levitate

Coral Sword

Red Hood
Silk Robe
Battle Boots

Cure 2, Reraise, Protect 2, Shell, Shell 2
Potion, Soft, Phoenix Down

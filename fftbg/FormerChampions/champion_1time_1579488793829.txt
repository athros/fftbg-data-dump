Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Alexknyde
Female
Pisces
62
73
Oracle
Time Magic
MA Save
Magic Attack UP
Lava Walking

Dragon Rod

Twist Headband
Light Robe
Small Mantle

Life Drain, Zombie, Confusion Song, Dispel Magic, Sleep, Dark Holy
Haste 2, Float, Demi, Stabilize Time



DunkasaurusRekt
Monster
Cancer
60
52
Byblos










LavyWays
Female
Libra
66
56
Lancer
Yin Yang Magic
Auto Potion
Maintenance
Waterwalking

Mythril Spear
Round Shield
Circlet
Crystal Mail
Leather Mantle

Level Jump5, Vertical Jump5
Blind, Life Drain, Confusion Song, Dispel Magic



Eco1327
Female
Capricorn
55
49
Dancer
Yin Yang Magic
Critical Quick
Short Charge
Fly

Ryozan Silk

Flash Hat
Earth Clothes
Spike Shoes

Nameless Dance, Dragon Pit
Poison, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify, Dark Holy

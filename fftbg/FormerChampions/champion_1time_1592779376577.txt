Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



ArchKnightX
Male
Gemini
80
43
Knight
Item
Damage Split
Throw Item
Levitate

Giant Axe
Escutcheon
Iron Helmet
Mythril Armor
Genji Gauntlet

Armor Break, Weapon Break, Speed Break, Power Break, Justice Sword, Dark Sword, Surging Sword
Potion, Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Roofiepops
Monster
Aries
67
42
Taiju










Extinctrational
Male
Aquarius
76
59
Lancer
White Magic
Counter Magic
Beastmaster
Retreat

Iron Fan
Bronze Shield
Crystal Helmet
Maximillian
Red Shoes

Level Jump8, Vertical Jump7
Cure, Cure 4, Raise, Raise 2, Regen, Shell 2, Esuna



Lythe Caraker
Female
Taurus
77
46
Time Mage
Summon Magic
Arrow Guard
Martial Arts
Swim



Twist Headband
Light Robe
Angel Ring

Slow, Quick, Demi, Demi 2, Stabilize Time
Moogle, Shiva, Ramuh, Odin, Leviathan, Silf, Fairy, Lich

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



EnemyController
Male
Aquarius
41
76
Summoner
Elemental
Parry
Equip Polearm
Jump+3

Cashmere

Triangle Hat
Black Robe
Cherche

Moogle, Shiva, Ifrit, Fairy
Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Maakur
Male
Leo
55
66
Knight
Draw Out
PA Save
Defense UP
Levitate

Defender
Gold Shield
Iron Helmet
Plate Mail
Sprint Shoes

Head Break, Armor Break, Weapon Break, Magic Break, Justice Sword, Dark Sword, Surging Sword
Koutetsu, Murasame, Kiyomori, Masamune



Fenaen
Male
Gemini
54
58
Knight
Jump
Caution
Dual Wield
Waterbreathing

Chaos Blade
Slasher
Mythril Helmet
Reflect Mail
Battle Boots

Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Surging Sword
Level Jump8, Vertical Jump7



Ferroniss
Female
Cancer
72
48
Time Mage
Yin Yang Magic
Parry
Short Status
Swim

Wizard Staff

Holy Miter
Adaman Vest
Wizard Mantle

Slow, Quick, Demi, Demi 2, Stabilize Time
Blind, Pray Faith, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic

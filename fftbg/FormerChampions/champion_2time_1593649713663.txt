Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Grandlanzer
Female
Capricorn
44
80
Thief
Charge
MA Save
Concentrate
Ignore Height

Zorlin Shape

Green Beret
Adaman Vest
Magic Ring

Steal Helmet, Arm Aim
Charge+3



Ring Wyrm
Male
Libra
52
64
Geomancer
Item
Meatbone Slash
Throw Item
Ignore Terrain

Diamond Sword
Escutcheon
Thief Hat
Mythril Vest
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Gusty Wind
Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



Just Here2
Male
Aries
58
41
Ninja
Battle Skill
Brave Save
Secret Hunt
Move-HP Up

Sasuke Knife
Hidden Knife
Flash Hat
Mystic Vest
Angel Ring

Sword, Stick
Magic Break, Speed Break, Power Break, Mind Break



Lemonjohns
Monster
Gemini
63
75
Blue Dragon








Player: !zChamp
Team: Champion Team
Palettes: Green/White



The Jar
Male
Libra
74
37
Knight
Time Magic
Brave Up
Doublehand
Ignore Height

Diamond Sword

Leather Helmet
Platinum Armor
Elf Mantle

Stasis Sword
Haste 2, Slow, Stabilize Time



Not Tiler Kiwi
Male
Sagittarius
54
72
Monk
Yin Yang Magic
Auto Potion
Defense UP
Move-MP Up



Headgear
Mythril Vest
Small Mantle

Pummel, Earth Slash, Chakra, Revive, Seal Evil
Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song



WeatherWolf4
Female
Pisces
46
77
Oracle
Black Magic
Arrow Guard
Equip Axe
Ignore Height

Slasher

Thief Hat
White Robe
Elf Mantle

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2



BapLogg
Male
Sagittarius
75
44
Mime

Mana Shield
Defend
Teleport



Flash Hat
Chain Vest
Battle Boots

Mimic


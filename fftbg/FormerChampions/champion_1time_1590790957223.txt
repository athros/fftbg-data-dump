Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



CT 5 Holy
Female
Serpentarius
60
70
Wizard
Draw Out
Sunken State
Beastmaster
Jump+2

Cute Bag

Red Hood
Mythril Vest
Power Wrist

Bolt 4, Ice 2, Frog, Flare
Kikuichimoji



Nizaha
Female
Pisces
65
58
Wizard
Draw Out
Critical Quick
Short Charge
Jump+2

Poison Rod

Feather Hat
Black Robe
Rubber Shoes

Fire, Fire 2, Fire 4, Bolt 2, Bolt 4, Frog
Koutetsu, Bizen Boat, Masamune



Old Overholt
Male
Virgo
41
52
Archer
Black Magic
Abandon
Doublehand
Jump+2

Bow Gun

Feather Hat
Clothes
Spike Shoes

Charge+1, Charge+2, Charge+4, Charge+7, Charge+20
Fire 2, Fire 3, Bolt, Ice 2, Ice 3, Empower



Zanderriv
Female
Pisces
69
62
Mediator
Punch Art
Caution
Equip Polearm
Fly

Partisan

Leather Hat
Wizard Outfit
Genji Gauntlet

Threaten, Preach, Insult, Negotiate, Refute
Pummel, Purification, Chakra, Revive

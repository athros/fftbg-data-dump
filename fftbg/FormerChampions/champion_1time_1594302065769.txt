Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Gooseyourself
Male
Scorpio
73
80
Calculator
White Magic
Distribute
Defense UP
Ignore Terrain

Bestiary

Golden Hairpin
Chameleon Robe
Sprint Shoes

CT, Height, 5, 4, 3
Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Esuna



Just Here2
Male
Cancer
76
56
Geomancer
Jump
Auto Potion
Defense UP
Lava Walking

Koutetsu Knife
Flame Shield
Black Hood
Silk Robe
Power Wrist

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Level Jump5, Vertical Jump8



Miku Shikhu
Male
Sagittarius
53
73
Wizard
Elemental
Meatbone Slash
Defense UP
Move+1

Air Knife

Green Beret
White Robe
108 Gems

Fire, Fire 4, Bolt, Bolt 3, Ice 2, Ice 4, Death, Flare
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Dogsandcatsand
Female
Sagittarius
70
47
Wizard
Time Magic
Regenerator
Short Charge
Waterwalking

Air Knife

Black Hood
White Robe
Jade Armlet

Bolt, Bolt 2, Bolt 4, Ice 2, Frog
Haste, Slow 2, Stop, Immobilize, Demi 2, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Firesheath
Monster
Cancer
64
48
Bomb










ShintaroNayaka
Male
Serpentarius
45
45
Bard
Elemental
Critical Quick
Maintenance
Move-MP Up

Fairy Harp

Flash Hat
Earth Clothes
Reflect Ring

Battle Song, Space Storage
Pitfall, Water Ball, Local Quake, Static Shock, Sand Storm, Gusty Wind



Lowlf
Male
Aries
76
67
Monk
Talk Skill
Absorb Used MP
Dual Wield
Ignore Height



Green Beret
Clothes
Bracer

Wave Fist, Purification, Chakra, Revive
Rehabilitate



Kronikle
Female
Pisces
42
52
Geomancer
Draw Out
Earplug
Short Charge
Fly

Platinum Sword
Gold Shield
Headgear
Wizard Outfit
Wizard Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Kiyomori, Kikuichimoji

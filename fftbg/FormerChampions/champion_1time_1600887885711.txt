Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Smashy
Male
Capricorn
61
76
Calculator
Marsh Skill
Auto Potion
Equip Bow
Jump+1

Night Killer

Grand Helmet
Silk Robe
Reflect Ring

Blue Magic
Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



Zagorsek
Male
Sagittarius
47
76
Calculator
Yin Yang Magic
Arrow Guard
Equip Sword
Swim

Iron Sword

Triangle Hat
Linen Robe
Diamond Armlet

CT, Height, Prime Number, 5, 3
Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Paralyze



Setsaku
Female
Virgo
67
80
Samurai
Black Magic
Arrow Guard
Equip Sword
Retreat

Save the Queen

Iron Helmet
Crystal Mail
Cursed Ring

Asura, Murasame, Kikuichimoji
Bolt 2, Ice 3, Frog, Flare



Nhammen
Male
Gemini
50
40
Samurai
Throw
Auto Potion
Defense UP
Retreat

Bizen Boat

Platinum Helmet
Carabini Mail
Spike Shoes

Koutetsu, Bizen Boat, Kiyomori
Shuriken, Ninja Sword

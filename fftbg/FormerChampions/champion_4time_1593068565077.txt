Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Galkife
Male
Sagittarius
72
52
Archer
Time Magic
Damage Split
Defense UP
Waterbreathing

Lightning Bow

Thief Hat
Secret Clothes
108 Gems

Charge+4, Charge+10
Slow 2, Immobilize, Reflect, Demi 2, Stabilize Time, Galaxy Stop



DLJuggernaut
Female
Sagittarius
79
69
Dancer
Charge
Earplug
Martial Arts
Move-HP Up



Feather Hat
Linen Robe
Red Shoes

Slow Dance, Disillusion, Nameless Dance, Nether Demon
Charge+7



RunicMagus
Male
Leo
74
80
Bard
Battle Skill
Brave Save
Doublehand
Retreat

Ramia Harp

Black Hood
Mystic Vest
N-Kai Armlet

Battle Song, Sky Demon
Armor Break, Weapon Break, Power Break, Mind Break



Livvexx
Female
Serpentarius
47
50
Thief
White Magic
MA Save
Doublehand
Move+3

Ninja Edge

Thief Hat
Black Costume
Power Wrist

Steal Helmet, Steal Shield, Steal Weapon, Arm Aim
Cure, Raise, Raise 2, Regen, Shell, Esuna

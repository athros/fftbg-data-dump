Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Wilbur
Male
Leo
58
37
Monk
Draw Out
Hamedo
Beastmaster
Move-MP Up



Triangle Hat
Earth Clothes
Jade Armlet

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive
Murasame



Adrian
Male
Libra
78
57
Summoner
Yin Yang Magic
Dragon Spirit
Sicken
Waterbreathing

Wizard Staff

Ribbon
Wizard Robe
Small Mantle

Moogle, Shiva, Ramuh, Ifrit, Bahamut, Odin, Leviathan, Fairy, Lich
Poison, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Brandise
Female
Virgo
78
47
Squire
Draw Out
Mana Shield
Short Charge
Move+3

Morning Star

Triangle Hat
Silk Robe
Reflect Ring

Throw Stone, Heal, Wish
Asura, Koutetsu, Bizen Boat, Muramasa, Masamune



Alfredo
Male
Sagittarius
51
57
Mime

PA Save
Attack UP
Move-MP Up



Thief Hat
Clothes
Power Wrist

Mimic




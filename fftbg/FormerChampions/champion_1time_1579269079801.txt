Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Dreygyron
Monster
Virgo
79
48
Tiamat










TheRedMelody
Male
Sagittarius
73
41
Wizard
Talk Skill
Abandon
Maintenance
Jump+2

Dagger

Triangle Hat
Linen Robe
108 Gems

Fire 2, Fire 4, Bolt 2, Ice, Ice 4, Death
Praise, Insult, Refute



Chewcombow
Female
Sagittarius
73
58
Monk
Basic Skill
Caution
Attack UP
Move-HP Up



Red Hood
Earth Clothes
Sprint Shoes

Wave Fist, Earth Slash, Revive
Accumulate, Dash, Throw Stone, Yell, Wish, Scream, Ultima



MohawkBigNOS
Female
Pisces
73
48
Chemist
Dance
Parry
Defense UP
Waterwalking

Mythril Gun

Ribbon
Judo Outfit
Germinas Boots

Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Disillusion

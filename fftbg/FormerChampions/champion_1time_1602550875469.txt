Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



DrAntiSocial
Monster
Scorpio
58
70
Tiamat










Reinoe
Monster
Aries
75
44
Red Chocobo










Vorap
Male
Capricorn
77
59
Mediator
Yin Yang Magic
MP Restore
Doublehand
Jump+2

Mythril Gun

Cachusha
Chameleon Robe
Cursed Ring

Praise, Threaten, Death Sentence, Insult, Negotiate, Refute
Pray Faith



Smashy
Male
Sagittarius
45
75
Monk
Elemental
Counter Magic
Halve MP
Jump+1



Green Beret
Power Sleeve
Bracer

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

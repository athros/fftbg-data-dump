Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Wyonearth
Female
Capricorn
69
45
Mime

Faith Save
Secret Hunt
Lava Walking



Green Beret
Adaman Vest
Genji Gauntlet

Mimic




Tougou
Monster
Leo
78
72
Juravis










Volgrathemoose
Monster
Libra
50
65
Red Chocobo










E Ballard
Male
Aries
55
78
Knight
Elemental
Parry
Secret Hunt
Lava Walking

Ice Brand
Platinum Shield
Gold Helmet
Bronze Armor
Defense Armlet

Armor Break, Magic Break, Night Sword
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind

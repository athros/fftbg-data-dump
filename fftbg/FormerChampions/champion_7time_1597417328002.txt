Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Lydian C
Female
Taurus
67
56
Thief
Throw
Earplug
Equip Gun
Retreat

Papyrus Codex

Holy Miter
Black Costume
Bracer

Gil Taking, Steal Heart, Steal Shield, Steal Accessory
Shuriken, Knife



Shadricklul
Female
Gemini
42
76
Thief
Item
Caution
Dual Wield
Fly

Blood Sword
Orichalcum
Triangle Hat
Leather Outfit
Salty Rage

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory
Potion, Elixir, Eye Drop, Echo Grass, Phoenix Down



Itsonlyspencer
Female
Scorpio
57
63
Wizard
Draw Out
Speed Save
Sicken
Waterwalking

Thunder Rod

Golden Hairpin
Wizard Robe
Spike Shoes

Fire, Fire 2, Bolt 2, Ice, Empower
Bizen Boat, Kiyomori



ArrenJevleth
Female
Cancer
80
53
Chemist
Time Magic
Catch
Attack UP
Move+1

Blaze Gun

Green Beret
Leather Outfit
Sprint Shoes

Potion, X-Potion, Hi-Ether, Eye Drop, Holy Water, Remedy, Phoenix Down
Slow, Demi 2, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



OmnibotGamma
Male
Virgo
76
63
Bard
Item
Hamedo
Throw Item
Swim

Fairy Harp

Ribbon
Clothes
Reflect Ring

Life Song, Magic Song, Sky Demon
Potion, Hi-Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down



Ququroon
Female
Pisces
74
75
Samurai
Elemental
Mana Shield
Dual Wield
Move+3

Mythril Spear
Holy Lance
Bronze Helmet
Leather Armor
Magic Gauntlet

Koutetsu, Murasame
Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Nickelbank
Male
Serpentarius
75
71
Wizard
Jump
Counter Magic
Magic Attack UP
Fly

Flame Rod

Ribbon
White Robe
108 Gems

Fire 4, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Death
Level Jump8, Vertical Jump3



TheOneRanger
Female
Taurus
77
78
Ninja
Talk Skill
Counter Magic
Monster Talk
Jump+3

Short Edge
Spell Edge
Cachusha
Power Sleeve
Rubber Shoes

Shuriken, Knife, Dictionary
Invitation, Praise, Preach, Solution, Death Sentence, Mimic Daravon, Refute

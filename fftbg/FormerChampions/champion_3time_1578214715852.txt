Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Tedra
Female
Capricorn
53
64
Mime

Mana Shield
Dual Wield
Jump+3



Black Hood
Clothes
Defense Armlet

Mimic




Reynaldo
Male
Scorpio
67
60
Time Mage
Throw
MP Restore
Concentrate
Ignore Height

Healing Staff

Thief Hat
Clothes
Genji Gauntlet

Haste, Haste 2, Immobilize, Reflect, Stabilize Time
Knife



Collete
Female
Aquarius
56
41
Calculator
White Magic
Damage Split
Magic Attack UP
Ignore Terrain

Ice Rod

Twist Headband
Linen Robe
Vanish Mantle

CT, Height, Prime Number, 4, 3
Cure 3, Raise, Shell, Esuna



Loren
Male
Leo
69
61
Geomancer
Steal
Counter
Short Charge
Jump+1

Mythril Sword
Buckler
Golden Hairpin
Linen Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Steal Accessory, Steal Status, Arm Aim, Leg Aim



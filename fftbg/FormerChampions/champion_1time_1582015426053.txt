Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Zaxxaramas
Female
Libra
44
73
Ninja
Elemental
Hamedo
Equip Gun
Retreat

Battle Folio
Bloody Strings
Golden Hairpin
Chain Vest
Spike Shoes

Shuriken, Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Alaylle
Female
Capricorn
69
73
Priest
Battle Skill
Catch
Short Status
Move+1

Healing Staff

Red Hood
Light Robe
Reflect Ring

Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Shell, Holy
Speed Break, Justice Sword



Alc Trinity
Monster
Cancer
52
41
Ghost










Neetneph
Male
Cancer
69
77
Ninja
Battle Skill
Damage Split
Equip Gun
Teleport

Papyrus Codex
Madlemgen
Headgear
Wizard Outfit
Spike Shoes

Bomb
Armor Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Surging Sword

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Gelwain
Monster
Taurus
60
60
Plague










Smashy
Male
Gemini
76
51
Samurai
Jump
Parry
Concentrate
Waterbreathing

Bizen Boat

Cross Helmet
Gold Armor
Feather Boots

Asura, Heaven's Cloud
Level Jump5, Vertical Jump6



Maakur
Male
Leo
64
70
Geomancer
Basic Skill
Damage Split
Equip Armor
Move-HP Up

Chirijiraden
Round Shield
Platinum Helmet
Earth Clothes
Cherche

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Accumulate, Throw Stone, Tickle, Yell



CorpusCav
Female
Scorpio
44
64
Chemist
Time Magic
MP Restore
Equip Sword
Move-HP Up

Diamond Sword

Red Hood
Mystic Vest
Chantage

Potion, Hi-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Immobilize, Reflect, Quick, Demi 2, Galaxy Stop

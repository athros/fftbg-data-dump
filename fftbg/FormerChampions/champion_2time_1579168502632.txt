Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Lesserorganisms
Male
Gemini
58
69
Time Mage
Punch Art
Counter Tackle
Defense UP
Levitate

Gold Staff

Thief Hat
Clothes
Defense Armlet

Haste, Stop, Immobilize, Reflect, Stabilize Time
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil



RedGrinGrumbl3
Male
Virgo
48
47
Mime

Abandon
Attack UP
Move+1



Green Beret
Wizard Outfit
Feather Mantle

Mimic




Wigraf
Female
Cancer
74
50
Knight
Punch Art
Auto Potion
Defend
Teleport

Save the Queen

Bronze Helmet
Crystal Mail
Magic Gauntlet

Head Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Wave Fist, Purification, Chakra, Revive, Seal Evil



MillionGhosts
Male
Aquarius
44
63
Bard
Item
Meatbone Slash
Throw Item
Waterwalking

Silver Bow

Cachusha
Earth Clothes
Red Shoes

Angel Song, Cheer Song, Magic Song, Diamond Blade, Space Storage
Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down

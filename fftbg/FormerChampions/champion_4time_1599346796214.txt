Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Maakur
Male
Scorpio
80
57
Lancer
Basic Skill
Auto Potion
Attack UP
Move+2

Holy Lance
Crystal Shield
Bronze Helmet
Genji Armor
Cursed Ring

Level Jump8, Vertical Jump7
Accumulate, Throw Stone, Heal



Baconbacon1207
Monster
Cancer
52
53
Red Chocobo










Technominari
Female
Aquarius
79
77
Priest
Basic Skill
Meatbone Slash
Short Charge
Levitate

Gold Staff

Holy Miter
Chameleon Robe
Rubber Shoes

Cure, Cure 3, Raise, Reraise, Regen, Protect 2, Shell 2, Wall, Esuna, Holy
Heal, Tickle



VolgraTheMoose
Male
Capricorn
71
80
Ninja
Draw Out
Sunken State
Attack UP
Jump+3

Kunai
Koga Knife
Holy Miter
Leather Outfit
Bracer

Bomb
Bizen Boat, Murasame, Muramasa

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Reinoe
Male
Virgo
63
71
Time Mage
Punch Art
Parry
Sicken
Teleport

Oak Staff

Holy Miter
White Robe
Wizard Mantle

Haste, Haste 2, Float, Quick, Demi, Stabilize Time
Purification, Chakra, Revive, Seal Evil



Bad1dea
Female
Scorpio
50
68
Mediator
Time Magic
Parry
Concentrate
Lava Walking

Blaze Gun

Black Hood
Clothes
Dracula Mantle

Invitation, Praise, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Slow 2, Immobilize, Demi, Demi 2



Maeveen
Male
Aquarius
54
55
Chemist
Basic Skill
Earplug
Beastmaster
Jump+1

Stone Gun

Green Beret
Wizard Outfit
Angel Ring

Potion, Hi-Potion, Ether, Hi-Ether, Holy Water, Remedy, Phoenix Down
Tickle, Wish, Scream



Basmal
Male
Libra
57
71
Archer
Elemental
HP Restore
Doublehand
Move-MP Up

Night Killer

Flash Hat
Chain Vest
Wizard Mantle

Charge+20
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

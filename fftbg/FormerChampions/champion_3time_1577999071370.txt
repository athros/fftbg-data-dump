Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Emmalynn
Female
Capricorn
65
78
Wizard
Talk Skill
Parry
Magic Defense UP
Move+2

Cultist Dagger

Thief Hat
Silk Robe
Elf Mantle

Fire 2, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4
Persuade, Praise, Death Sentence, Insult, Refute



Clayborne
Monster
Libra
65
43
Hydra










Jeramie
Male
Leo
64
77
Squire
Jump
Parry
Equip Armor
Ignore Terrain

Battle Axe

Platinum Helmet
Robe of Lords
Feather Mantle

Accumulate, Dash, Throw Stone, Heal
Level Jump3, Vertical Jump5



Bess
Female
Virgo
46
49
Time Mage
Item
Abandon
Throw Item
Move-HP Up

Battle Folio

Flash Hat
Linen Robe
N-Kai Armlet

Quick, Stabilize Time
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down



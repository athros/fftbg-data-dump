Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Wurstwaesserchen
Female
Leo
62
75
Summoner
Steal
Parry
Short Charge
Levitate

Healing Staff

Thief Hat
Silk Robe
Angel Ring

Moogle, Ifrit, Titan, Carbunkle, Silf, Fairy
Gil Taking, Steal Helmet



Clar3d
Monster
Virgo
72
51
Dragon










Pelerin
Male
Aries
66
71
Oracle
Sing
Distribute
Beastmaster
Ignore Terrain

Battle Folio

Thief Hat
Leather Outfit
Genji Gauntlet

Life Drain, Pray Faith, Foxbird, Dispel Magic
Life Song, Magic Song, Nameless Song



NovaKnight21
Male
Serpentarius
80
41
Samurai
Throw
Critical Quick
Martial Arts
Levitate



Leather Helmet
Chain Mail
Magic Gauntlet

Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Shuriken, Bomb, Ninja Sword

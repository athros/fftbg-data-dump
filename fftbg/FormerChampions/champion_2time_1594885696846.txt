Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Silentkaster
Female
Leo
63
53
Mime

PA Save
Attack UP
Move+2



Barbuta
Rubber Costume
Rubber Shoes

Mimic




Victoriolue
Female
Virgo
73
45
Dancer
Yin Yang Magic
Distribute
Defend
Levitate

Cashmere

Green Beret
Light Robe
Feather Mantle

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance
Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Sleep, Petrify



Thyrandaal
Monster
Taurus
82
60
Holy Dragon










Furrytomahawkk
Male
Scorpio
52
51
Thief
Throw
Counter Tackle
Short Charge
Ignore Height

Assassin Dagger

Triangle Hat
Power Sleeve
Defense Ring

Gil Taking, Steal Helmet, Steal Weapon, Arm Aim
Bomb, Spear, Wand

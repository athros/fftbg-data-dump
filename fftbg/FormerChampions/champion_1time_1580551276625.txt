Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Sinnyil2
Male
Libra
47
41
Squire
Throw
Arrow Guard
Concentrate
Ignore Height

Ancient Sword

Golden Hairpin
Mythril Vest
Bracer

Dash, Heal, Yell, Cheer Up, Wish
Sword



Rollopeo
Male
Capricorn
54
67
Summoner
Time Magic
Damage Split
Equip Armor
Waterbreathing

Dragon Rod

Headgear
Crystal Mail
Elf Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Lich
Haste 2, Slow, Float, Reflect, Demi 2, Meteor



Nilbusmaximus
Monster
Scorpio
47
61
Steel Giant










ItsTokey89
Male
Leo
38
47
Geomancer
Basic Skill
Brave Up
Sicken
Waterwalking

Giant Axe
Escutcheon
Thief Hat
Brigandine
Bracer

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Dash, Throw Stone, Scream

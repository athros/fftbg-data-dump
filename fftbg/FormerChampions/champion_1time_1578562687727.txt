Player: !zChamp
Team: Champion Team
Palettes: Green/White



Bigmacronin
Monster
Sagittarius
46
58
Bull Demon










XHereticRamza
Monster
Leo
65
48
Archaic Demon










Adrien
Male
Leo
74
56
Knight
Time Magic
Parry
Short Charge
Move+2

Hydra Bag
Platinum Shield
Iron Helmet
Gold Armor
Small Mantle

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break
Haste 2, Reflect, Demi, Stabilize Time, Meteor



Ole
Male
Cancer
73
46
Geomancer
Battle Skill
Faith Up
Magic Attack UP
Waterbreathing

Bizen Boat
Round Shield
Flash Hat
Judo Outfit
Leather Mantle

Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Head Break, Armor Break, Speed Break, Power Break, Night Sword

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



VolgraTheMoose
Female
Pisces
45
66
Summoner
Yin Yang Magic
MP Restore
Short Charge
Move+3

Bestiary

Holy Miter
Mythril Vest
Reflect Ring

Ramuh, Ifrit, Golem, Odin, Silf, Fairy, Lich
Poison, Foxbird, Dispel Magic, Paralyze



TheChainNerd
Male
Sagittarius
44
76
Squire
Sing
Absorb Used MP
Magic Defense UP
Teleport

Blind Knife
Flame Shield
Feather Hat
Crystal Mail
Feather Mantle

Throw Stone, Heal, Tickle, Cheer Up, Wish
Diamond Blade, Sky Demon



L2 Sentinel
Male
Libra
52
51
Summoner
Elemental
MA Save
Short Charge
Lava Walking

Dragon Rod

Flash Hat
Leather Outfit
Sprint Shoes

Ramuh, Ifrit, Bahamut, Odin, Salamander, Silf, Cyclops
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



PoroTact
Male
Cancer
71
61
Monk
Elemental
Dragon Spirit
Doublehand
Move-HP Up



Twist Headband
Judo Outfit
Feather Mantle

Wave Fist, Purification, Revive
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: Green/White



FoeSquirrel
Female
Serpentarius
41
70
Time Mage
Summon Magic
Speed Save
Sicken
Ignore Height

Healing Staff

Twist Headband
Earth Clothes
Wizard Mantle

Haste, Slow, Float, Reflect, Quick
Moogle, Carbunkle, Salamander, Fairy, Lich



Zagorsek
Female
Aquarius
54
46
Ninja
Talk Skill
Auto Potion
Equip Knife
Ignore Height

Ninja Edge
Dragon Rod
Twist Headband
Leather Outfit
Diamond Armlet

Shuriken, Knife
Invitation, Praise, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



Username123132
Female
Serpentarius
79
65
Squire
Battle Skill
Catch
Long Status
Jump+1

Platinum Sword
Genji Shield
Twist Headband
Secret Clothes
Cursed Ring

Accumulate, Heal, Tickle, Yell, Cheer Up, Fury
Shield Break, Weapon Break, Speed Break



Lawndough
Male
Capricorn
69
58
Monk
Jump
Damage Split
Equip Polearm
Move+3

Ivory Rod

Flash Hat
Wizard Outfit
Diamond Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification
Level Jump5, Vertical Jump8

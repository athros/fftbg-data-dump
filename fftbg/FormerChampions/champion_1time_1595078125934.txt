Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Nhammen
Male
Gemini
42
77
Squire
Steal
Brave Save
Defense UP
Ignore Terrain

Battle Axe
Platinum Shield
Grand Helmet
Gold Armor
Feather Boots

Heal, Tickle, Yell, Fury, Scream
Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Status



Boosted420
Female
Capricorn
76
39
Monk
White Magic
Counter
Magic Attack UP
Move+3



Leather Hat
Secret Clothes
Leather Mantle

Pummel, Purification, Revive
Raise, Regen, Protect 2, Shell 2, Esuna



Lemonjohns
Female
Aquarius
73
66
Calculator
Robosnake Skill
Counter Magic
Martial Arts
Move+3

Gokuu Rod

Green Beret
Judo Outfit
Spike Shoes

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



CT 5 Holy
Female
Aries
38
70
Summoner
White Magic
Critical Quick
Short Charge
Jump+1

Faith Rod

Twist Headband
Linen Robe
Wizard Mantle

Moogle, Shiva, Salamander, Fairy, Lich
Cure, Cure 3, Protect, Esuna, Holy, Magic Barrier

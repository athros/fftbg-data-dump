Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Lionhermit
Monster
Capricorn
57
70
Serpentarius










Jeeboheebo
Female
Libra
72
57
Chemist
Talk Skill
Auto Potion
Dual Wield
Move-HP Up

Stone Gun
Blast Gun
Holy Miter
Mystic Vest
Leather Mantle

Potion, Ether, Holy Water, Remedy, Phoenix Down
Invitation, Persuade, Preach, Solution, Death Sentence, Mimic Daravon



JollyGreenGiant82
Male
Leo
53
44
Oracle
Throw
Counter Tackle
Magic Defense UP
Teleport

Papyrus Codex

Flash Hat
Linen Robe
Battle Boots

Life Drain, Silence Song, Paralyze, Sleep, Dark Holy
Spear



Rolodex
Female
Cancer
48
49
Geomancer
Punch Art
Counter Tackle
Halve MP
Jump+1

Ancient Sword
Genji Shield
Black Hood
Silk Robe
Leather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind
Pummel, Secret Fist, Purification, Revive

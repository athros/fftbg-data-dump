Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Withpaint
Male
Sagittarius
54
63
Calculator
Black Magic
MP Restore
Halve MP
Levitate

Papyrus Codex

Barette
Wizard Robe
Spike Shoes

Height, 5, 3
Fire, Fire 2, Fire 3, Ice 4, Empower, Frog



LuckyLuckLuc2
Male
Scorpio
51
43
Monk
Summon Magic
Mana Shield
Equip Shield
Fly


Kaiser Plate
Red Hood
Earth Clothes
Diamond Armlet

Spin Fist, Pummel, Secret Fist, Purification
Moogle, Carbunkle, Odin, Salamander



0v3rr8d
Female
Scorpio
62
51
Geomancer
Summon Magic
Counter
Equip Sword
Levitate

Excalibur

Thief Hat
Secret Clothes
Genji Gauntlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shiva, Golem, Leviathan, Lich



Technominari
Female
Aquarius
65
52
Priest
Draw Out
Mana Shield
Equip Bow
Move-HP Up

Ultimus Bow

Thief Hat
Linen Robe
Red Shoes

Cure 4, Reraise, Regen, Protect, Shell, Esuna, Holy
Asura, Koutetsu, Murasame, Kiyomori, Muramasa

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Delcake
Monster
Gemini
45
79
Byblos










Ruebyy
Male
Capricorn
63
50
Knight
Basic Skill
Counter
Doublehand
Swim

Save the Queen

Bronze Helmet
Carabini Mail
Feather Mantle

Head Break, Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword, Night Sword
Throw Stone, Heal, Yell, Fury



CrownOfHorns
Male
Pisces
67
77
Time Mage
Black Magic
Dragon Spirit
Doublehand
Move-MP Up

Gokuu Rod

Flash Hat
Brigandine
N-Kai Armlet

Quick, Demi, Stabilize Time
Fire 4, Bolt 2, Frog



Braisethelard
Male
Pisces
78
72
Geomancer
Throw
Counter Tackle
Long Status
Ignore Height

Slasher
Crystal Shield
Ribbon
Black Robe
Genji Gauntlet

Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Shuriken, Bomb

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Uyjhm
Male
Sagittarius
44
54
Thief
Yin Yang Magic
Regenerator
Long Status
Fly

Mythril Knife

Triangle Hat
Rubber Costume
Small Mantle

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Spell Absorb, Life Drain, Foxbird, Dispel Magic, Paralyze



Clippopo
Male
Leo
70
68
Mediator
Battle Skill
Counter
Magic Attack UP
Move-MP Up

Main Gauche

Red Hood
Linen Robe
N-Kai Armlet

Persuade, Praise, Insult, Negotiate, Mimic Daravon, Refute
Head Break, Shield Break, Magic Break, Power Break, Stasis Sword



Zagorsek
Female
Taurus
45
63
Samurai
Elemental
Counter Flood
Magic Attack UP
Waterbreathing

Kikuichimoji

Grand Helmet
Chain Mail
Feather Boots

Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa, Chirijiraden
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Rolodex
Male
Leo
64
55
Mediator
White Magic
Sunken State
Long Status
Lava Walking

Stone Gun

Golden Hairpin
Brigandine
Genji Gauntlet

Invitation, Persuade, Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Cure 3, Cure 4, Raise, Protect 2, Wall, Holy

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



VolgraTheMoose
Male
Aries
69
41
Lancer
Sing
Counter Flood
Halve MP
Waterbreathing

Mythril Spear
Mythril Shield
Genji Helmet
Platinum Armor
Defense Ring

Level Jump8, Vertical Jump8
Space Storage, Hydra Pit



Galkife
Male
Gemini
71
58
Summoner
Sing
Abandon
Short Charge
Retreat

Wizard Rod

Black Hood
Power Sleeve
Sprint Shoes

Shiva, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Lich
Angel Song, Cheer Song



Fluffskull
Female
Sagittarius
55
43
Summoner
Charge
Counter Flood
Short Status
Levitate

Rod

Black Hood
Mythril Vest
Dracula Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Salamander, Fairy
Charge+3, Charge+5, Charge+10, Charge+20



Lythe Caraker
Female
Pisces
58
57
Dancer
Draw Out
Brave Save
Martial Arts
Move+2



Twist Headband
Light Robe
Wizard Mantle

Slow Dance, Polka Polka, Disillusion, Obsidian Blade, Nether Demon, Dragon Pit
Koutetsu, Muramasa, Kikuichimoji

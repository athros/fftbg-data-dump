Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Joewcarson
Male
Cancer
76
52
Chemist
Elemental
Mana Shield
Equip Polearm
Retreat

Holy Lance

Thief Hat
Judo Outfit
Chantage

Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Galkife
Male
Leo
68
70
Calculator
Time Magic
Speed Save
Sicken
Move+2

Madlemgen

Green Beret
Light Robe
Power Wrist

CT, Height, Prime Number, 5, 4, 3
Haste, Haste 2, Slow, Float, Demi 2



Latebit
Male
Taurus
60
52
Archer
Item
Arrow Guard
Long Status
Ignore Terrain

Bow Gun
Genji Shield
Thief Hat
Judo Outfit
Cursed Ring

Charge+1, Charge+3, Charge+7, Charge+20
Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Victoriolue
Male
Libra
72
58
Mime

Counter
Martial Arts
Ignore Height



Triangle Hat
Leather Outfit
Germinas Boots

Mimic


Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Lawnboxer
Monster
Cancer
74
69
Great Malboro










Victoriolue
Female
Aries
69
58
Summoner
Throw
Abandon
Short Charge
Jump+3

Ice Rod

Thief Hat
Chameleon Robe
Rubber Shoes

Moogle, Ramuh, Ifrit, Golem, Bahamut, Leviathan, Silf, Fairy
Shuriken, Bomb, Sword, Stick, Dictionary



Kalpho
Male
Taurus
63
69
Lancer
Steal
Counter Flood
Equip Polearm
Ignore Terrain

Cashmere
Bronze Shield
Cross Helmet
Gold Armor
Cursed Ring

Level Jump8, Vertical Jump7
Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



Kintaru Oe
Female
Pisces
67
55
Samurai
Charge
Caution
Maintenance
Waterwalking

Bizen Boat

Cross Helmet
Linen Robe
N-Kai Armlet

Heaven's Cloud, Kikuichimoji
Charge+1, Charge+2, Charge+5, Charge+10, Charge+20

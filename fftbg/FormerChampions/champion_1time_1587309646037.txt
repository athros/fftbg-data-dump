Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Gooseyourself
Female
Aquarius
45
80
Time Mage
Summon Magic
Damage Split
Equip Gun
Ignore Height

Stone Gun

Green Beret
White Robe
Rubber Shoes

Haste 2, Slow, Slow 2, Immobilize, Quick, Stabilize Time
Moogle, Shiva, Ramuh, Golem, Carbunkle, Odin



SaintOmerville
Male
Pisces
52
49
Ninja
Draw Out
Abandon
Equip Knife
Jump+1

Sasuke Knife
Iga Knife
Triangle Hat
Mystic Vest
Jade Armlet

Sword, Staff
Murasame, Heaven's Cloud, Kiyomori



Run With Stone Guns
Male
Taurus
49
70
Monk
Sing
MA Save
Sicken
Waterbreathing



Black Hood
Chain Vest
Wizard Mantle

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra
Life Song, Space Storage, Sky Demon



ColetteMSLP
Female
Libra
42
77
Samurai
Throw
Caution
Attack UP
Move-HP Up

Koutetsu Knife

Cross Helmet
Gold Armor
Jade Armlet

Kikuichimoji
Knife, Staff, Ninja Sword

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Miku Shikhu
Male
Gemini
55
49
Lancer
Battle Skill
Earplug
Attack UP
Move+3

Spear
Aegis Shield
Iron Helmet
Leather Armor
Small Mantle

Level Jump8, Vertical Jump8
Armor Break, Magic Break, Speed Break, Justice Sword, Explosion Sword



RobotOcelot
Male
Virgo
79
49
Samurai
Throw
Counter Magic
Magic Attack UP
Waterbreathing

Kikuichimoji

Cross Helmet
Black Robe
Angel Ring

Murasame, Kiyomori
Staff, Spear



NovaKnight21
Male
Cancer
56
45
Chemist
Steal
Sunken State
Equip Sword
Lava Walking

Asura Knife

Headgear
Wizard Outfit
Wizard Mantle

Hi-Potion, Ether, Holy Water, Remedy, Phoenix Down
Gil Taking, Steal Helmet, Steal Accessory



Firesheath
Male
Pisces
67
53
Time Mage
White Magic
Critical Quick
Dual Wield
Retreat

Whale Whisker
Whale Whisker
Thief Hat
White Robe
Feather Boots

Haste, Haste 2, Slow, Reflect, Quick, Demi 2, Stabilize Time
Cure, Cure 2, Raise, Raise 2, Reraise, Regen, Shell, Esuna

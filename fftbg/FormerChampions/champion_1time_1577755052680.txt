Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Jonathon
Male
Scorpio
77
80
Ninja
Time Magic
Catch
Attack UP
Retreat

Dagger
Mythril Knife
Red Hood
Judo Outfit
Feather Boots

Sword, Dictionary
Haste 2, Slow 2, Float, Reflect, Quick, Stabilize Time



Carlita
Female
Virgo
76
42
Geomancer
Item
Speed Save
Equip Knife
Fly

Kunai
Escutcheon
Headgear
White Robe
Feather Mantle

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Garrette
Male
Libra
56
60
Bard
Yin Yang Magic
Meatbone Slash
Doublehand
Lava Walking

Fairy Harp

Headgear
Secret Clothes
Defense Ring

Last Song
Spell Absorb, Zombie, Silence Song, Dispel Magic, Sleep, Petrify



Edmond
Male
Aquarius
42
58
Thief
White Magic
Earplug
Equip Polearm
Waterbreathing

Air Knife

Headgear
Leather Outfit
Angel Ring

Steal Heart, Steal Helmet, Steal Shield
Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Wall, Esuna



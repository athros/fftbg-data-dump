Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sairentozon7
Monster
Serpentarius
76
37
King Behemoth










JethroThrul
Male
Sagittarius
55
52
Chemist
Time Magic
Critical Quick
Equip Knife
Swim

Hidden Knife

Green Beret
Judo Outfit
Feather Boots

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Haste, Quick, Demi 2, Stabilize Time



Vorap
Male
Scorpio
57
54
Chemist
Throw
Distribute
Equip Armor
Lava Walking

Dagger

Red Hood
Chain Mail
Power Wrist

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Shuriken, Staff, Wand



Legionx13
Female
Libra
63
72
Samurai
White Magic
Damage Split
Equip Knife
Move-MP Up

Spell Edge

Cross Helmet
Silk Robe
Leather Mantle

Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji
Cure, Cure 4, Raise, Regen, Protect, Wall, Esuna

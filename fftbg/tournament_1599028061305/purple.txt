Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sinnyil2
Female
Virgo
64
51
Geomancer
Draw Out
Brave Save
Equip Knife
Waterbreathing

Flame Rod
Genji Shield
Golden Hairpin
Judo Outfit
Sprint Shoes

Pitfall, Water Ball, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball
Asura, Koutetsu, Muramasa



PantherIscariot
Male
Aquarius
66
68
Bard
Black Magic
Counter
Equip Armor
Ignore Terrain

Silver Bow

Circlet
Light Robe
108 Gems

Angel Song, Cheer Song, Battle Song, Magic Song, Hydra Pit
Fire, Fire 2, Fire 3, Ice 3, Ice 4



Lowlf
Monster
Virgo
69
71
Plague










FriendlySkeleton
Male
Gemini
75
40
Ninja
Punch Art
Earplug
Short Charge
Swim

Spell Edge
Dagger
Leather Hat
Wizard Outfit
Magic Ring

Shuriken, Ninja Sword
Spin Fist, Secret Fist, Revive

Player: !Black
Team: Black Team
Palettes: Black/Red



Chuckolator
Female
Aries
59
78
Oracle
Time Magic
HP Restore
Doublehand
Retreat

Cypress Rod

Red Hood
Silk Robe
Angel Ring

Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify
Haste, Slow 2, Reflect, Demi



Powermhero
Female
Virgo
81
57
Summoner
Throw
Absorb Used MP
Short Status
Retreat

Bestiary

Feather Hat
Linen Robe
Setiemson

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Lich, Cyclops
Bomb



DesertWooder
Female
Aries
81
69
Samurai
White Magic
Auto Potion
Doublehand
Levitate

Koutetsu Knife

Circlet
Linen Robe
Genji Gauntlet

Asura, Heaven's Cloud, Kiyomori
Cure 2, Cure 4, Raise, Regen, Protect 2, Shell, Esuna



Fattunaking
Male
Aquarius
47
49
Monk
Throw
Auto Potion
Equip Polearm
Jump+2

Spear

Headgear
Adaman Vest
Germinas Boots

Spin Fist, Chakra
Shuriken, Bomb, Sword, Dictionary

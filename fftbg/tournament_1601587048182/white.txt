Player: !White
Team: White Team
Palettes: White/Blue



CorpusCav
Female
Pisces
75
75
Chemist
Summon Magic
Auto Potion
Magic Defense UP
Jump+2

Blaze Gun

Triangle Hat
Power Sleeve
Vanish Mantle

Potion, Echo Grass, Holy Water
Moogle, Shiva, Titan, Salamander, Silf, Lich



EmperorBeef
Female
Aries
68
42
Ninja
Summon Magic
Brave Save
Equip Gun
Move-HP Up

Battle Folio
Papyrus Codex
Green Beret
Power Sleeve
Magic Ring

Bomb
Moogle, Ramuh, Golem, Carbunkle, Leviathan, Silf



Nizaha
Male
Capricorn
61
48
Priest
Time Magic
MP Restore
Beastmaster
Ignore Height

Scorpion Tail

Red Hood
Power Sleeve
Sprint Shoes

Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Shell, Wall, Esuna
Immobilize, Stabilize Time



Just Here2
Male
Sagittarius
56
38
Priest
Time Magic
Damage Split
Equip Armor
Jump+2

Flail

Iron Helmet
Leather Armor
Elf Mantle

Cure 2, Raise, Regen, Protect 2, Shell, Shell 2, Esuna
Haste, Haste 2, Slow, Reflect, Quick, Demi 2, Stabilize Time

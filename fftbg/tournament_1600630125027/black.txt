Player: !Black
Team: Black Team
Palettes: Black/Red



Rytor
Male
Sagittarius
75
51
Squire
Throw
Critical Quick
Attack UP
Waterwalking

Platinum Sword
Buckler
Iron Helmet
Plate Mail
Genji Gauntlet

Accumulate, Throw Stone, Heal, Tickle, Yell, Ultima
Bomb, Hammer, Staff



Maakur
Male
Leo
72
81
Monk
Yin Yang Magic
Regenerator
Dual Wield
Move+2



Red Hood
Wizard Outfit
Salty Rage

Spin Fist, Purification, Revive, Seal Evil
Blind, Poison, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify



Chuckolator
Female
Capricorn
69
46
Time Mage
Punch Art
Regenerator
Magic Attack UP
Jump+1

Gokuu Rod

Golden Hairpin
Mythril Vest
Feather Boots

Slow, Float
Spin Fist, Earth Slash, Secret Fist, Chakra, Revive, Seal Evil



Forkmore
Male
Serpentarius
73
39
Mediator
Charge
Critical Quick
Equip Axe
Teleport

Flame Whip

Holy Miter
Power Sleeve
Feather Mantle

Persuade, Preach, Negotiate, Mimic Daravon, Rehabilitate
Charge+7, Charge+20

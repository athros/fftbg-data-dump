Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Fattunaking
Male
Scorpio
65
76
Monk
Sing
Distribute
Equip Sword
Move+2

Kikuichimoji

Thief Hat
Brigandine
Bracer

Spin Fist, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Angel Song, Life Song, Battle Song, Last Song



DeathTaxesAndAnime
Monster
Aquarius
49
50
Holy Dragon










Dasutin23
Female
Capricorn
58
76
Wizard
Summon Magic
MA Save
Equip Bow
Move-HP Up

Night Killer

Leather Hat
Light Robe
Feather Boots

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Ice, Ice 4, Empower, Flare
Ifrit, Leviathan, Silf, Fairy



KasugaiRoastedPeas
Male
Aquarius
52
75
Mime

Counter
Martial Arts
Levitate



Twist Headband
Mythril Vest
Sprint Shoes

Mimic


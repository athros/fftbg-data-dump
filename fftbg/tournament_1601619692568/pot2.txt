Final Bets: green - 7 bets for 2,908G (25.5%, x2.92); yellow - 11 bets for 8,501G (74.5%, x0.34)

green bets:
BirbBrainsBot: 1,000G (34.4%, 166,038G)
getthemoneyz: 484G (16.6%, 2,132,562G)
ColetteMSLP: 480G (16.5%, 6,361G)
AllInBot: 352G (12.1%, 352G)
TheSabretoothe: 292G (10.0%, 292G)
Evewho: 200G (6.9%, 4,713G)
readdesert: 100G (3.4%, 1,313G)

yellow bets:
NicoSavoy: 3,841G (45.2%, 3,841G)
thunderducker: 1,124G (13.2%, 1,124G)
ruleof5: 1,000G (11.8%, 50,055G)
DirkC_Fenrir84: 500G (5.9%, 1,000G)
Lythe_Caraker: 495G (5.8%, 495G)
PantherIscariot: 444G (5.2%, 2,698G)
Treafa: 396G (4.7%, 8,168G)
Palladigm: 200G (2.4%, 10,695G)
KasugaiRoastedPeas: 200G (2.4%, 7,170G)
datadrivenbot: 200G (2.4%, 81,127G)
gorgewall: 101G (1.2%, 11,392G)

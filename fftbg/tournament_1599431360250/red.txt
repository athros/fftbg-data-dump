Player: !Red
Team: Red Team
Palettes: Red/Brown



KasugaiRoastedPeas
Female
Cancer
66
57
Knight
Yin Yang Magic
HP Restore
Long Status
Levitate

Defender
Aegis Shield
Mythril Helmet
Linen Robe
Defense Armlet

Head Break, Armor Break, Mind Break, Explosion Sword
Poison, Doubt Faith, Zombie, Confusion Song, Paralyze



DejaPoo21
Male
Aries
52
43
Mediator
Throw
Auto Potion
Dual Wield
Fly

Battle Folio
Papyrus Codex
Twist Headband
Chameleon Robe
Germinas Boots

Praise, Threaten, Preach, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Shuriken, Ninja Sword, Spear, Stick



Mesmaster
Male
Sagittarius
66
57
Knight
Summon Magic
Regenerator
Halve MP
Ignore Height

Iron Sword
Gold Shield
Grand Helmet
Robe of Lords
Diamond Armlet

Weapon Break, Power Break, Dark Sword, Night Sword
Moogle, Shiva, Titan, Golem, Odin, Salamander



Redmage4evah
Male
Pisces
53
56
Bard
Throw
MP Restore
Dual Wield
Lava Walking

Bloody Strings
Bloody Strings
Thief Hat
Earth Clothes
Angel Ring

Angel Song, Magic Song, Nameless Song, Last Song
Sword

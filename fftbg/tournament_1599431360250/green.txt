Player: !Green
Team: Green Team
Palettes: Green/White



Reddwind
Male
Gemini
68
64
Knight
Sing
Regenerator
Martial Arts
Move-MP Up


Crystal Shield
Diamond Helmet
Carabini Mail
Defense Ring

Armor Break, Weapon Break, Power Break, Stasis Sword
Angel Song, Battle Song, Magic Song, Sky Demon, Hydra Pit



PantherIscariot
Male
Sagittarius
68
67
Mime

Counter
Defense UP
Move-HP Up



Ribbon
Power Sleeve
Genji Gauntlet

Mimic




Error72
Male
Libra
60
48
Ninja
Talk Skill
HP Restore
Equip Armor
Move-MP Up

Kunai
Cultist Dagger
Ribbon
Linen Robe
N-Kai Armlet

Bomb
Death Sentence, Negotiate, Refute



Gorgewall
Female
Scorpio
46
76
Mediator
White Magic
Meatbone Slash
Equip Knife
Move-HP Up

Orichalcum

Twist Headband
Chameleon Robe
Cherche

Solution, Death Sentence, Insult, Refute, Rehabilitate
Raise, Raise 2, Protect, Shell 2, Wall, Esuna

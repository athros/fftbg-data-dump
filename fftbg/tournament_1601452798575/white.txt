Player: !White
Team: White Team
Palettes: White/Blue



Mesmaster
Male
Leo
58
72
Archer
Throw
Counter Tackle
Doublehand
Teleport

Yoichi Bow

Genji Helmet
Power Sleeve
N-Kai Armlet

Charge+1, Charge+4
Shuriken, Bomb, Ninja Sword



Firesheath
Female
Taurus
60
76
Time Mage
Summon Magic
Counter Flood
Equip Bow
Ignore Terrain

Ultimus Bow

Green Beret
Leather Outfit
N-Kai Armlet

Haste, Stop, Demi, Demi 2, Stabilize Time
Moogle, Shiva, Ramuh, Ifrit, Titan, Odin



MemoriesofFinal
Male
Libra
76
71
Archer
Sing
Catch
Doublehand
Teleport

Hunting Bow

Diamond Helmet
Black Costume
Germinas Boots

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7
Life Song, Battle Song, Last Song, Hydra Pit



IBardic
Male
Virgo
75
56
Bard
Jump
Damage Split
Concentrate
Move+1

Ramia Harp

Golden Hairpin
Black Costume
Defense Armlet

Angel Song, Last Song, Hydra Pit
Level Jump3, Vertical Jump8

Final Bets: green - 13 bets for 4,522G (45.6%, x1.19); yellow - 7 bets for 5,396G (54.4%, x0.84)

green bets:
DeathTaxesAndAnime: 1,120G (24.8%, 1,120G)
Raixelol: 600G (13.3%, 16,505G)
LivingHitokiri: 500G (11.1%, 2,358G)
ColetteMSLP: 350G (7.7%, 2,254G)
superdevon1: 328G (7.3%, 328G)
Neech: 312G (6.9%, 357G)
ValensEXP_: 296G (6.5%, 8,232G)
hotpocketsofficial: 264G (5.8%, 264G)
khelor_: 200G (4.4%, 3,756G)
datadrivenbot: 200G (4.4%, 60,141G)
AllInBot: 152G (3.4%, 152G)
BartTradingCompany: 100G (2.2%, 7,636G)
Astroblitz: 100G (2.2%, 120G)

yellow bets:
OneHundredFists: 2,000G (37.1%, 13,774G)
BirbBrainsBot: 1,000G (18.5%, 144,143G)
Leonidusx: 1,000G (18.5%, 35,549G)
ser_pyrro: 500G (9.3%, 1,851G)
BappleTrees: 420G (7.8%, 7,771G)
getthemoneyz: 376G (7.0%, 1,942,710G)
MemoriesofFinal: 100G (1.9%, 8,297G)

Final Bets: white - 9 bets for 4,653G (47.9%, x1.09); black - 8 bets for 5,051G (52.1%, x0.92)

white bets:
OneHundredFists: 1,000G (21.5%, 11,774G)
superdevon1: 719G (15.5%, 719G)
Raixelol: 600G (12.9%, 17,221G)
hotpocketsofficial: 579G (12.4%, 579G)
upvla: 500G (10.7%, 500G)
BappleTrees: 420G (9.0%, 7,351G)
AllInBot: 333G (7.2%, 333G)
getthemoneyz: 302G (6.5%, 1,942,334G)
Leonidusx: 200G (4.3%, 34,549G)

black bets:
LivingHitokiri: 2,955G (58.5%, 2,955G)
BirbBrainsBot: 1,000G (19.8%, 143,143G)
ValensEXP_: 296G (5.9%, 8,585G)
MemoriesofFinal: 200G (4.0%, 8,197G)
khelor_: 200G (4.0%, 3,995G)
datadrivenbot: 200G (4.0%, 60,380G)
BartTradingCompany: 100G (2.0%, 7,755G)
Astroblitz: 100G (2.0%, 239G)

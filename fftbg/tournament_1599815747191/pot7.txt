Final Bets: blue - 4 bets for 3,322G (23.3%, x3.30); white - 14 bets for 10,961G (76.7%, x0.30)

blue bets:
SkyridgeZero: 1,500G (45.2%, 40,828G)
BirbBrainsBot: 1,000G (30.1%, 47,668G)
Panushenko: 584G (17.6%, 584G)
getthemoneyz: 238G (7.2%, 1,915,462G)

white bets:
iBardic: 4,000G (36.5%, 20,016G)
DeathTaxesAndAnime: 2,408G (22.0%, 4,722G)
brenogarwin: 1,188G (10.8%, 1,188G)
NicoSavoy: 1,000G (9.1%, 48,848G)
Raixelol: 596G (5.4%, 16,848G)
DaveStrider55: 364G (3.3%, 364G)
Neech: 296G (2.7%, 4,577G)
hotpocketsofficial: 236G (2.2%, 236G)
FriendlySkeleton: 226G (2.1%, 226G)
datadrivenbot: 200G (1.8%, 57,795G)
AllInBot: 139G (1.3%, 139G)
Rune339: 108G (1.0%, 108G)
BartTradingCompany: 100G (0.9%, 3,853G)
Mushufasa_: 100G (0.9%, 1,208G)

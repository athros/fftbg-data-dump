Player: !Green
Team: Green Team
Palettes: Green/White



PolkaGrandma
Female
Serpentarius
76
55
Oracle
Talk Skill
Damage Split
Equip Knife
Jump+1

Dragon Rod

Golden Hairpin
Chain Vest
Leather Mantle

Doubt Faith, Blind Rage, Foxbird
Invitation, Praise, Threaten, Preach, Refute



DaveStrider55
Male
Capricorn
79
62
Thief
Draw Out
Hamedo
Equip Axe
Ignore Terrain

Slasher

Red Hood
Black Costume
Germinas Boots

Steal Heart, Steal Armor, Steal Shield, Steal Accessory
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Bruubarg
Monster
Cancer
54
72
Iron Hawk










MemoriesofFinal
Female
Pisces
73
52
Thief
Punch Art
Counter
Equip Bow
Ignore Terrain

Ice Bow

Headgear
Judo Outfit
Jade Armlet

Gil Taking, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Spin Fist, Wave Fist, Purification, Revive, Seal Evil

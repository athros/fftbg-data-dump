Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Gorgewall
Male
Pisces
70
39
Ninja
Jump
Counter
Equip Sword
Move+2

Short Edge
Muramasa
Black Hood
Brigandine
Power Wrist

Shuriken, Bomb
Level Jump3, Vertical Jump6



Mushufasa
Male
Aries
61
64
Archer
Time Magic
Counter Tackle
Dual Wield
Jump+2

Hunting Bow
Hunting Bow
Headgear
Power Sleeve
Battle Boots

Charge+1, Charge+10, Charge+20
Slow 2, Stop, Demi, Stabilize Time



Zagorsek
Male
Capricorn
55
46
Chemist
Time Magic
Counter Flood
Equip Gun
Move+3

Bloody Strings

Thief Hat
Power Sleeve
Bracer

Hi-Potion, X-Potion, Ether, Antidote, Phoenix Down
Haste, Haste 2, Slow, Stop, Reflect, Demi, Demi 2, Stabilize Time



Kronikle
Female
Aquarius
78
50
Monk
Yin Yang Magic
PA Save
Secret Hunt
Move+3



Twist Headband
Brigandine
N-Kai Armlet

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Poison, Pray Faith, Doubt Faith, Blind Rage, Confusion Song, Paralyze, Sleep

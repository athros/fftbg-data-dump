Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CosmicTactician
Female
Gemini
59
75
Summoner
Talk Skill
Caution
Short Charge
Waterbreathing

Poison Rod

Golden Hairpin
Light Robe
Elf Mantle

Shiva, Ramuh, Ifrit, Titan, Carbunkle, Silf, Lich
Invitation, Praise, Solution, Refute, Rehabilitate



Silentkaster
Male
Pisces
64
65
Ninja
Basic Skill
Regenerator
Attack UP
Waterbreathing

Spell Edge
Short Edge
Flash Hat
Brigandine
Power Wrist

Shuriken, Spear, Dictionary
Accumulate, Throw Stone, Heal, Cheer Up, Fury



ZCKaiser
Female
Capricorn
68
43
Knight
Item
Absorb Used MP
Beastmaster
Levitate

Ice Brand
Diamond Shield
Circlet
Genji Armor
108 Gems

Armor Break, Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword
Potion, X-Potion, Antidote, Eye Drop, Echo Grass



Reddwind
Male
Gemini
44
62
Knight
Throw
MP Restore
Equip Axe
Jump+3

Battle Axe
Kaiser Plate
Bronze Helmet
Carabini Mail
Leather Mantle

Speed Break, Dark Sword, Night Sword
Bomb, Staff

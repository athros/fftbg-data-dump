Final Bets: blue - 8 bets for 5,984G (66.5%, x0.50); yellow - 8 bets for 3,016G (33.5%, x1.98)

blue bets:
E_Ballard: 3,008G (50.3%, 3,008G)
snkey: 1,467G (24.5%, 13,200G)
CosmicTactician: 500G (8.4%, 29,524G)
bigbongsmoker: 316G (5.3%, 2,896G)
Neech: 292G (4.9%, 1,414G)
datadrivenbot: 200G (3.3%, 58,398G)
gorgewall: 101G (1.7%, 4,668G)
BartTradingCompany: 100G (1.7%, 370G)

yellow bets:
BirbBrainsBot: 1,000G (33.2%, 192,740G)
getthemoneyz: 668G (22.1%, 1,903,243G)
pplvee1: 312G (10.3%, 312G)
brodeity: 268G (8.9%, 2,163G)
ValensEXP_: 252G (8.4%, 3,134G)
mindblownnnn: 216G (7.2%, 216G)
projekt_mike: 200G (6.6%, 4,725G)
AllInBot: 100G (3.3%, 100G)

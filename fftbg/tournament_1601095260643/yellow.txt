Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Resjudicata3
Male
Virgo
71
72
Priest
Battle Skill
Sunken State
Attack UP
Ignore Height

Mace of Zeus

Headgear
Judo Outfit
Power Wrist

Cure 3, Cure 4, Raise, Raise 2, Regen, Esuna
Shield Break, Speed Break, Justice Sword, Surging Sword



Escobro
Female
Cancer
57
64
Chemist
Summon Magic
Earplug
Magic Defense UP
Lava Walking

Cute Bag

Feather Hat
Clothes
Leather Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Remedy, Phoenix Down
Shiva, Ramuh, Ifrit, Carbunkle, Silf, Lich, Cyclops, Zodiac



StealthModeLocke
Female
Leo
44
57
Priest
Draw Out
Parry
Equip Knife
Ignore Terrain

Orichalcum

Twist Headband
Silk Robe
Elf Mantle

Cure, Raise, Raise 2, Reraise, Protect, Protect 2, Shell, Shell 2, Esuna
Murasame, Kikuichimoji



Leonidusx
Male
Sagittarius
45
53
Ninja
Summon Magic
Counter Flood
Equip Gun
Jump+3

Blast Gun
Blaze Gun
Black Hood
Mystic Vest
Germinas Boots

Shuriken, Knife, Sword
Moogle, Shiva, Ramuh, Titan, Carbunkle, Leviathan

Player: !Green
Team: Green Team
Palettes: Green/White



NobodySomeone
Female
Aquarius
73
65
Oracle
Draw Out
Mana Shield
Secret Hunt
Teleport

Musk Rod

Green Beret
Wizard Robe
Feather Boots

Blind, Foxbird
Asura, Heaven's Cloud, Kikuichimoji



Gobizku
Male
Aries
69
67
Geomancer
Summon Magic
Counter
Equip Knife
Move-HP Up

Dagger
Gold Shield
Feather Hat
Clothes
Defense Armlet

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Golem, Carbunkle, Leviathan, Salamander



Holdenmagronik
Female
Libra
74
69
Chemist
Punch Art
Parry
Dual Wield
Fly

Blind Knife
Cute Bag
Flash Hat
Wizard Outfit
Genji Gauntlet

Hi-Potion, X-Potion, Ether, Hi-Ether, Phoenix Down
Purification, Revive



Rocl
Male
Leo
51
56
Lancer
Time Magic
Damage Split
Dual Wield
Jump+1

Javelin
Holy Lance
Crystal Helmet
White Robe
Leather Mantle

Level Jump8, Vertical Jump8
Haste, Float, Quick, Stabilize Time

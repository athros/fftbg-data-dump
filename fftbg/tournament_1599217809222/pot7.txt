Final Bets: red - 7 bets for 2,243G (75.7%, x0.32); brown - 3 bets for 720G (24.3%, x3.12)

red bets:
E_Ballard: 872G (38.9%, 130,041G)
acrusade: 400G (17.8%, 3,790G)
Raixelol: 396G (17.7%, 396G)
datadrivenbot: 200G (8.9%, 61,717G)
BirbBrainsBot: 146G (6.5%, 115,799G)
bigbongsmoker: 128G (5.7%, 4,927G)
gorgewall: 101G (4.5%, 5,649G)

brown bets:
Zachara: 576G (80.0%, 139,576G)
CosmicTactician: 100G (13.9%, 47,409G)
getthemoneyz: 44G (6.1%, 1,814,011G)

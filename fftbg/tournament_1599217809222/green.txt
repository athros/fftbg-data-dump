Player: !Green
Team: Green Team
Palettes: Green/White



Bigbongsmoker
Male
Virgo
61
74
Monk
Jump
Catch
Halve MP
Waterbreathing



Feather Hat
Black Costume
Power Wrist

Wave Fist, Secret Fist, Purification, Revive
Level Jump8, Vertical Jump8



Lijarkh
Female
Taurus
58
72
Monk
White Magic
Counter Tackle
Sicken
Jump+2



Red Hood
Earth Clothes
Genji Gauntlet

Spin Fist, Pummel, Purification, Revive, Seal Evil
Raise, Raise 2, Protect, Esuna



CassiePhoenix
Male
Leo
58
50
Monk
Talk Skill
Speed Save
Dual Wield
Jump+2



Leather Hat
Chain Vest
Magic Gauntlet

Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil
Threaten, Insult



VolgraTheMoose
Male
Sagittarius
72
74
Archer
Basic Skill
Regenerator
Doublehand
Waterbreathing

Windslash Bow

Thief Hat
Mystic Vest
Cursed Ring

Charge+2, Charge+20
Accumulate, Heal, Wish

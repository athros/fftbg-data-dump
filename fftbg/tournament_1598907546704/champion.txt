Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Maakur
Female
Sagittarius
54
61
Summoner
Punch Art
Earplug
Short Status
Teleport

Ice Rod

Ribbon
White Robe
108 Gems

Moogle, Ramuh, Golem, Odin, Leviathan, Silf, Cyclops
Spin Fist, Wave Fist, Secret Fist, Revive



HaateXIII
Male
Aries
70
50
Priest
Basic Skill
Mana Shield
Equip Bow
Ignore Height

Poison Bow

Holy Miter
Wizard Outfit
Feather Boots

Raise, Raise 2, Shell 2, Esuna, Holy
Accumulate, Dash, Scream



Kronikle
Female
Scorpio
76
53
Archer
Yin Yang Magic
MP Restore
Doublehand
Move-MP Up

Yoichi Bow

Red Hood
Clothes
Battle Boots

Charge+3, Charge+5, Charge+7, Charge+20
Poison, Spell Absorb, Pray Faith, Dispel Magic, Paralyze



KasugaiRoastedPeas
Female
Capricorn
47
80
Mediator
Summon Magic
Faith Save
Magic Attack UP
Ignore Height

Blaze Gun

Flash Hat
Linen Robe
Jade Armlet

Invitation, Persuade, Threaten, Solution, Death Sentence, Mimic Daravon, Rehabilitate
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Bahamut

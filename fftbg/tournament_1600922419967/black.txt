Player: !Black
Team: Black Team
Palettes: Black/Red



Lythe Caraker
Male
Libra
67
61
Lancer
Sing
Damage Split
Equip Polearm
Levitate

Cypress Rod
Diamond Shield
Bronze Helmet
Maximillian
Magic Ring

Level Jump8, Vertical Jump7
Life Song, Magic Song, Space Storage



CorpusCav
Female
Gemini
59
76
Chemist
Summon Magic
Catch
Short Charge
Move+1

Blaze Gun

Twist Headband
Judo Outfit
108 Gems

Potion, Hi-Potion, X-Potion, Ether, Antidote, Holy Water, Remedy, Phoenix Down
Moogle, Golem, Odin, Leviathan, Fairy, Lich



Lyner87
Female
Serpentarius
52
47
Priest
Item
Earplug
Magic Defense UP
Move-HP Up

Morning Star

Leather Hat
Black Robe
Genji Gauntlet

Regen, Protect, Protect 2, Shell, Shell 2, Esuna
Potion, Hi-Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Vorap
Monster
Taurus
79
41
Reaper








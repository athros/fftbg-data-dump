Player: !Red
Team: Red Team
Palettes: Red/Brown



Byrdturbo
Male
Cancer
70
45
Wizard
Elemental
Sunken State
Martial Arts
Ignore Height

Poison Rod

Leather Hat
Power Sleeve
Magic Ring

Fire 2, Fire 3, Fire 4, Bolt 4, Ice 2, Empower, Death
Pitfall, Water Ball, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



ValensEXP
Male
Taurus
59
46
Ninja
Basic Skill
Brave Save
Sicken
Jump+1

Flame Whip
Mythril Knife
Thief Hat
Leather Outfit
N-Kai Armlet

Shuriken, Bomb, Wand
Accumulate, Dash, Throw Stone, Tickle, Wish



Ninjaboy3234
Male
Libra
69
59
Bard
Summon Magic
Absorb Used MP
Sicken
Move+2

Ramia Harp

Black Hood
Mythril Armor
Power Wrist

Angel Song, Life Song, Magic Song, Hydra Pit
Moogle, Ifrit, Bahamut



Bruubarg
Male
Gemini
39
76
Ninja
Black Magic
Critical Quick
Beastmaster
Ignore Terrain

Air Knife
Kunai
Triangle Hat
Clothes
Diamond Armlet

Shuriken, Knife
Fire, Fire 2, Bolt 3, Bolt 4, Ice, Ice 2, Empower

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lijarkh
Male
Taurus
56
67
Monk
Sing
Sunken State
Equip Shield
Move-HP Up


Mythril Shield
Headgear
Mystic Vest
Leather Mantle

Secret Fist, Chakra, Revive
Cheer Song, Nameless Song, Sky Demon



Ar Tactic
Monster
Leo
46
42
Red Chocobo










Victoriolue
Male
Leo
74
76
Knight
Punch Art
Counter
Short Status
Move+1

Long Sword
Flame Shield
Diamond Helmet
Plate Mail
Bracer

Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword
Pummel, Purification, Chakra, Revive



Reddwind
Male
Gemini
53
57
Knight
Time Magic
Counter Magic
Attack UP
Move+2

Save the Queen
Aegis Shield
Bronze Helmet
Bronze Armor
Wizard Mantle

Shield Break, Magic Break, Speed Break, Justice Sword
Stop, Immobilize, Reflect, Demi, Stabilize Time

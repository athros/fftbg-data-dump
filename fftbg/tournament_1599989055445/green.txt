Player: !Green
Team: Green Team
Palettes: Green/White



Skipsandwiches
Female
Virgo
47
78
Calculator
White Magic
Brave Save
Secret Hunt
Move-MP Up

Battle Folio

Flash Hat
White Robe
108 Gems

Height, Prime Number, 5
Cure, Cure 2, Protect 2, Shell, Esuna



Lokenwow
Male
Aries
58
40
Monk
White Magic
Abandon
Equip Sword
Lava Walking

Kikuichimoji

Red Hood
Earth Clothes
Spike Shoes

Pummel, Secret Fist, Purification, Chakra, Revive
Cure 2, Cure 4, Protect 2, Shell 2, Esuna



CorpusCav
Monster
Leo
62
60
Ghoul










Electric Algus
Female
Serpentarius
65
52
Wizard
Dance
Faith Save
Maintenance
Teleport

Wizard Rod

Flash Hat
Wizard Robe
Wizard Mantle

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice 2, Empower, Frog, Death
Witch Hunt, Nameless Dance, Dragon Pit

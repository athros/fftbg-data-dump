Final Bets: red - 10 bets for 5,756G (56.2%, x0.78); blue - 8 bets for 4,491G (43.8%, x1.28)

red bets:
VolgraTheMoose: 1,001G (17.4%, 126,429G)
Lifebregin: 1,000G (17.4%, 17,422G)
BartTradingCompany: 879G (15.3%, 8,794G)
resjudicata3: 704G (12.2%, 704G)
Draconis345: 556G (9.7%, 556G)
Raixelol: 508G (8.8%, 1,219G)
rocl: 360G (6.3%, 4,204G)
ColetteMSLP: 300G (5.2%, 2,947G)
bigbongsmoker: 248G (4.3%, 5,368G)
datadrivenbot: 200G (3.5%, 56,036G)

blue bets:
DavenIII: 1,500G (33.4%, 7,032G)
BirbBrainsBot: 1,000G (22.3%, 170,542G)
getthemoneyz: 942G (21.0%, 1,832,052G)
SephDarkheart: 348G (7.7%, 348G)
SeniorBunk: 300G (6.7%, 17,207G)
lwtest: 200G (4.5%, 3,051G)
gorgewall: 101G (2.2%, 5,914G)
brodeity: 100G (2.2%, 1,089G)

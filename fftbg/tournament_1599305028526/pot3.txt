Final Bets: white - 9 bets for 7,899G (52.4%, x0.91); black - 10 bets for 7,164G (47.6%, x1.10)

white bets:
Raixelol: 2,048G (25.9%, 2,048G)
resjudicata3: 1,586G (20.1%, 1,586G)
Draconis345: 1,253G (15.9%, 1,253G)
Lifebregin: 1,000G (12.7%, 18,202G)
BartTradingCompany: 853G (10.8%, 8,532G)
brodeity: 500G (6.3%, 989G)
SephDarkheart: 440G (5.6%, 440G)
roqqqpsi: 119G (1.5%, 1,323G)
Vultuous: 100G (1.3%, 980G)

black bets:
DavenIII: 3,203G (44.7%, 6,282G)
VolgraTheMoose: 1,001G (14.0%, 127,210G)
BirbBrainsBot: 988G (13.8%, 169,511G)
rocl: 360G (5.0%, 4,125G)
upvla: 350G (4.9%, 1,849G)
lijarkh: 312G (4.4%, 312G)
getthemoneyz: 302G (4.2%, 1,830,716G)
bigbongsmoker: 248G (3.5%, 5,627G)
projekt_mike: 200G (2.8%, 903G)
datadrivenbot: 200G (2.8%, 56,245G)

Player: !Green
Team: Green Team
Palettes: Green/White



Bruubarg
Female
Capricorn
63
65
Summoner
Draw Out
Parry
Magic Defense UP
Jump+3

Battle Folio

Feather Hat
White Robe
Red Shoes

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Silf, Fairy, Cyclops
Asura, Bizen Boat, Heaven's Cloud, Kikuichimoji, Chirijiraden



CapnSauce420
Male
Taurus
74
56
Lancer
Steal
Counter Magic
Maintenance
Jump+1

Octagon Rod
Ice Shield
Iron Helmet
Chain Mail
Rubber Shoes

Level Jump8, Vertical Jump2
Steal Heart, Steal Shield, Steal Weapon, Leg Aim



ALY327
Male
Libra
74
80
Knight
Jump
Regenerator
Equip Gun
Retreat

Glacier Gun
Crystal Shield
Grand Helmet
Mythril Armor
Defense Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword
Level Jump2, Vertical Jump3



Upvla
Male
Leo
72
60
Monk
Elemental
Brave Save
Secret Hunt
Ignore Height



Ribbon
Adaman Vest
Bracer

Wave Fist, Earth Slash, Purification, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Gusty Wind, Lava Ball

Final Bets: purple - 8 bets for 4,977G (61.9%, x0.62); brown - 9 bets for 3,064G (38.1%, x1.62)

purple bets:
Mushufasa_: 1,800G (36.2%, 1,800G)
VolgraTheMoose: 1,001G (20.1%, 142,187G)
BirbBrainsBot: 873G (17.5%, 18,004G)
Raixelol: 528G (10.6%, 1,624G)
AltimaMantoid: 500G (10.0%, 3,700G)
pepperbuster: 155G (3.1%, 855G)
rottings0ul: 100G (2.0%, 1,592G)
TheSabretoothe: 20G (0.4%, 2,188G)

brown bets:
Rytor: 650G (21.2%, 32,548G)
E_Ballard: 608G (19.8%, 608G)
Smashy: 500G (16.3%, 8,685G)
SephDarkheart: 426G (13.9%, 426G)
gorgewall: 301G (9.8%, 3,674G)
Sietaha: 200G (6.5%, 5,140G)
DeathTaxesAndAnime: 200G (6.5%, 8,123G)
BartTradingCompany: 147G (4.8%, 1,474G)
getthemoneyz: 32G (1.0%, 1,850,229G)

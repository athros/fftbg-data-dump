Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Gemini
50
52
Thief
Punch Art
Arrow Guard
Dual Wield
Move-MP Up

Kunai
Mage Masher
Black Hood
Judo Outfit
Spike Shoes

Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Spin Fist, Pummel, Secret Fist, Purification



Lumberinjack
Female
Scorpio
47
56
Priest
Elemental
Dragon Spirit
Beastmaster
Move+3

Flail

Green Beret
Black Robe
Feather Boots

Cure, Raise, Reraise, Regen, Protect, Shell 2, Esuna
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Snowfats
Male
Pisces
61
79
Time Mage
Punch Art
Damage Split
Short Charge
Move-HP Up

Wizard Staff

Ribbon
Black Robe
Defense Armlet

Stop, Reflect, Stabilize Time
Pummel



CaptainAdmiralSPATULA
Female
Virgo
77
45
Archer
Battle Skill
Brave Save
Maintenance
Jump+1

Hydra Bag
Round Shield
Leather Hat
Chain Vest
N-Kai Armlet

Charge+1, Charge+3
Head Break, Armor Break, Magic Break, Speed Break, Stasis Sword, Justice Sword, Surging Sword

Player: !White
Team: White Team
Palettes: White/Blue



Virilikus
Male
Capricorn
64
45
Lancer
White Magic
Meatbone Slash
Equip Sword
Jump+2

Murasame
Round Shield
Genji Helmet
Reflect Mail
Spike Shoes

Level Jump2, Vertical Jump8
Cure, Cure 4, Raise, Raise 2, Regen, Shell 2, Wall



Meta Five
Male
Virgo
50
76
Chemist
Draw Out
Catch
Martial Arts
Move-HP Up

Mythril Knife

Black Hood
Black Costume
Cherche

Potion, Ether, Elixir, Phoenix Down
Asura, Heaven's Cloud, Kikuichimoji



Dasutin23
Male
Libra
52
37
Lancer
Time Magic
Parry
Doublehand
Jump+1

Dragon Whisker

Leather Helmet
Mythril Armor
Defense Armlet

Level Jump5, Vertical Jump4
Slow, Slow 2, Demi, Stabilize Time



TheDeeyo
Female
Pisces
66
68
Geomancer
Punch Art
Critical Quick
Maintenance
Jump+2

Battle Axe
Genji Shield
Golden Hairpin
Black Robe
Cursed Ring

Hell Ivy, Local Quake, Quicksand, Blizzard, Lava Ball
Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive, Seal Evil

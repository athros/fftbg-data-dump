Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kronikle
Male
Virgo
45
52
Knight
Time Magic
Critical Quick
Equip Armor
Move-MP Up

Excalibur
Venetian Shield
Barette
Power Sleeve
Rubber Shoes

Shield Break, Mind Break, Stasis Sword, Justice Sword, Surging Sword
Haste, Haste 2, Float, Quick, Stabilize Time



Muffin Money
Male
Scorpio
71
53
Samurai
Steal
Faith Save
Equip Bow
Ignore Terrain

Mythril Bow

Gold Helmet
Maximillian
Defense Armlet

Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Gil Taking, Steal Weapon, Steal Accessory



CorpusCav
Monster
Libra
64
70
Plague










CT 5 Holy
Male
Sagittarius
42
78
Archer
Talk Skill
Parry
Doublehand
Ignore Terrain

Long Bow

Twist Headband
Mystic Vest
Feather Mantle

Charge+7, Charge+20
Persuade, Preach, Death Sentence, Insult, Negotiate, Refute

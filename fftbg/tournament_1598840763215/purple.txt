Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



PantherIscariot
Female
Aquarius
57
63
Thief
Dance
Mana Shield
Defense UP
Move-MP Up

Dagger

Leather Hat
Mythril Vest
Dracula Mantle

Steal Helmet, Steal Weapon, Steal Accessory
Last Dance, Obsidian Blade



Shalloween
Male
Cancer
37
73
Bard
Battle Skill
Auto Potion
Magic Attack UP
Retreat

Mythril Bow

Headgear
Leather Outfit
Red Shoes

Angel Song, Battle Song, Magic Song, Space Storage
Head Break, Armor Break, Weapon Break, Speed Break, Mind Break



Victoriolue
Male
Scorpio
45
66
Chemist
Battle Skill
MP Restore
Equip Shield
Move-HP Up

Assassin Dagger
Venetian Shield
Headgear
Power Sleeve
Jade Armlet

Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Head Break, Power Break



Just Here2
Male
Sagittarius
65
78
Samurai
Charge
Absorb Used MP
Sicken
Ignore Terrain

Heaven's Cloud

Crystal Helmet
Chameleon Robe
Power Wrist

Asura, Bizen Boat, Kiyomori, Kikuichimoji
Charge+1, Charge+4, Charge+20

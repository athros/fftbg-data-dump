Player: !Black
Team: Black Team
Palettes: Black/Red



Chaosdesigned
Female
Gemini
66
58
Geomancer
Yin Yang Magic
Meatbone Slash
Sicken
Ignore Terrain

Giant Axe
Gold Shield
Cachusha
Power Sleeve
Bracer

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Blind, Poison, Zombie, Confusion Song, Dispel Magic, Petrify



Sireaulou
Female
Libra
72
68
Wizard
Draw Out
HP Restore
Attack UP
Jump+1

Wizard Rod

Barette
Brigandine
Defense Ring

Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Empower, Frog, Flare
Bizen Boat, Kiyomori, Kikuichimoji



Laserman1000
Male
Pisces
69
44
Calculator
Demon Skill
Dragon Spirit
Equip Gun
Jump+3

Fairy Harp

Golden Hairpin
Adaman Vest
Red Shoes

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



DrAntiSocial
Female
Libra
58
59
Dancer
Talk Skill
Speed Save
Equip Knife
Move+2

Rod

Golden Hairpin
Linen Robe
Sprint Shoes

Wiznaibus, Disillusion, Last Dance, Void Storage, Dragon Pit
Solution, Mimic Daravon, Refute

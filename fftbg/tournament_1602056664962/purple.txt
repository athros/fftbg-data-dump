Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Go2sleepTV
Male
Taurus
45
62
Monk
Battle Skill
Arrow Guard
Attack UP
Jump+1



Flash Hat
Wizard Outfit
Magic Gauntlet

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Shield Break, Magic Break, Power Break, Mind Break



Chuckolator
Female
Virgo
76
46
Lancer
Time Magic
Meatbone Slash
Magic Defense UP
Swim

Dragon Whisker
Flame Shield
Circlet
Silk Robe
Diamond Armlet

Level Jump5, Vertical Jump3
Demi, Meteor



Seppu777
Male
Taurus
46
73
Lancer
Elemental
Speed Save
Defense UP
Levitate

Ivory Rod
Escutcheon
Circlet
Light Robe
Rubber Shoes

Level Jump2, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



NovaKnight21
Male
Pisces
66
78
Samurai
Talk Skill
Critical Quick
Equip Axe
Jump+3

Morning Star

Cross Helmet
Light Robe
Feather Boots

Asura, Koutetsu
Invitation, Praise, Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate

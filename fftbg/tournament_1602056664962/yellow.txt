Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HASTERIOUS
Male
Libra
54
69
Geomancer
Battle Skill
Blade Grasp
Attack UP
Move-HP Up

Murasame
Bronze Shield
Golden Hairpin
Mythril Vest
Rubber Shoes

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Mind Break, Dark Sword



3ngag3
Male
Scorpio
78
77
Knight
Punch Art
Parry
Defense UP
Move+3

Broad Sword
Aegis Shield
Bronze Helmet
Diamond Armor
Elf Mantle

Armor Break, Speed Break, Stasis Sword
Wave Fist, Purification, Chakra



ValensEXP
Male
Virgo
50
46
Squire
White Magic
Counter Magic
Long Status
Move+2

Flame Whip
Venetian Shield
Holy Miter
Adaman Vest
Magic Gauntlet

Accumulate, Dash, Cheer Up, Scream
Cure, Cure 4, Raise 2



MemoriesofFinal
Female
Libra
42
74
Wizard
Draw Out
Brave Save
Halve MP
Jump+3

Thunder Rod

Black Hood
Chameleon Robe
Salty Rage

Fire, Fire 3, Bolt, Bolt 4, Ice 2, Ice 3, Ice 4, Empower
Murasame, Heaven's Cloud

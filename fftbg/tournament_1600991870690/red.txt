Player: !Red
Team: Red Team
Palettes: Red/Brown



Gelwain
Male
Taurus
99
98
Calculator
Nether Skill
Blade Grasp
Maintenance
Ignore Terrain

Stone Gun
Escutcheon
Headgear
Mythril Armor
Chantage

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Sharknaldson
Male
Serpentarius
84
89
Calculator
Limit
Auto Potion
Equip Shield
Waterbreathing

Blast Gun
Ice Shield
Crystal Helmet
Chain Vest
Diamond Armlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Run With Stone GUNs
Male
Aquarius
83
96
Calculator
Imp Skill
Meatbone Slash
Equip Bow
Jump+1

Ice Bow

Red Hood
Mythril Armor
Spike Shoes

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam



RunicMagus
Female
Aquarius
93
82
Calculator
Catfish Skill
Mana Shield
Magic Defense UP
Jump+1

Persia
Round Shield
Circlet
Judo Outfit
N-Kai Armlet

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast

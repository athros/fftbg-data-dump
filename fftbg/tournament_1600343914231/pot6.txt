Final Bets: black - 10 bets for 5,986G (55.3%, x0.81); purple - 8 bets for 4,834G (44.7%, x1.24)

black bets:
bruubarg: 3,274G (54.7%, 3,274G)
getthemoneyz: 572G (9.6%, 1,962,186G)
MemoriesofFinal: 500G (8.4%, 28,800G)
run_with_stone_GUNs: 412G (6.9%, 11,871G)
ValensEXP_: 328G (5.5%, 1,485G)
Foamsoldier: 300G (5.0%, 8,483G)
AllInBot: 200G (3.3%, 200G)
datadrivenbot: 200G (3.3%, 62,540G)
BartTradingCompany: 100G (1.7%, 10,045G)
peeronid: 100G (1.7%, 923G)

purple bets:
BirbBrainsBot: 1,000G (20.7%, 7,674G)
Mazairasama: 1,000G (20.7%, 4,026G)
pepperbuster: 854G (17.7%, 22,854G)
nifboy: 764G (15.8%, 6,012G)
Lord_Gwarth: 500G (10.3%, 3,601G)
skipsandwiches: 416G (8.6%, 16,502G)
farside00831: 200G (4.1%, 5,334G)
MilkaChoko: 100G (2.1%, 981G)

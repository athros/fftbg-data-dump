Final Bets: green - 10 bets for 3,769G (50.3%, x0.99); purple - 7 bets for 3,725G (49.7%, x1.01)

green bets:
BirbBrainsBot: 1,000G (26.5%, 8,912G)
nifboy: 764G (20.3%, 6,958G)
bruubarg: 600G (15.9%, 600G)
Lord_Gwarth: 500G (13.3%, 4,220G)
ValensEXP_: 328G (8.7%, 1,157G)
AllInBot: 200G (5.3%, 200G)
getthemoneyz: 176G (4.7%, 1,961,614G)
Thyrandaal: 100G (2.7%, 63,829G)
CosmicTactician: 100G (2.7%, 28,945G)
latebit: 1G (0.0%, 10,277G)

purple bets:
Mazairasama: 1,000G (26.8%, 5,264G)
pepperbuster: 912G (24.5%, 23,912G)
DavenIII: 813G (21.8%, 1,626G)
MemoriesofFinal: 500G (13.4%, 28,300G)
MilkaChoko: 200G (5.4%, 1,105G)
datadrivenbot: 200G (5.4%, 62,340G)
BartTradingCompany: 100G (2.7%, 9,945G)

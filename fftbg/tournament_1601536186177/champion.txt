Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Gelwain
Male
Virgo
80
65
Knight
Black Magic
Distribute
Defense UP
Levitate

Chaos Blade
Ice Shield
Crystal Helmet
Silk Robe
Sprint Shoes

Head Break, Magic Break, Speed Break, Justice Sword
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 2, Frog



StealthModeLocke
Male
Libra
58
79
Archer
Punch Art
Meatbone Slash
Equip Gun
Ignore Height

Papyrus Codex
Platinum Shield
Black Hood
Power Sleeve
Defense Ring

Charge+5, Charge+7
Earth Slash, Purification, Revive



GiggleStik
Male
Leo
72
58
Archer
Time Magic
Parry
Doublehand
Move-MP Up

Bow Gun

Green Beret
Rubber Costume
Diamond Armlet

Charge+2, Charge+3, Charge+4, Charge+10
Stop, Demi, Stabilize Time



CorpusCav
Monster
Scorpio
72
70
Apanda








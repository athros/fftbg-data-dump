Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Shalloween
Male
Scorpio
52
48
Samurai
Item
Parry
Throw Item
Lava Walking

Murasame

Iron Helmet
Reflect Mail
Defense Ring

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori
Potion, Hi-Potion, Antidote, Soft, Holy Water, Remedy, Phoenix Down



Reinoe
Male
Gemini
74
58
Monk
Charge
Sunken State
Dual Wield
Jump+3



Black Hood
Leather Outfit
Rubber Shoes

Wave Fist, Earth Slash, Purification, Revive
Charge+3, Charge+4, Charge+5



Ar Tactic
Female
Leo
80
63
Samurai
Black Magic
Speed Save
Martial Arts
Waterwalking



Leather Helmet
Chameleon Robe
Spike Shoes

Asura, Heaven's Cloud, Kiyomori
Ice, Ice 3, Flare



VolgraTheMoose
Male
Aquarius
49
67
Geomancer
Black Magic
Mana Shield
Equip Knife
Ignore Terrain

Thunder Rod
Mythril Shield
Triangle Hat
Rubber Costume
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Fire 3, Fire 4, Bolt 3, Ice, Ice 2, Ice 4, Empower, Flare

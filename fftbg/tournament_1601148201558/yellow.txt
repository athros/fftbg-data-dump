Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BuffaloCrunch
Female
Libra
51
78
Lancer
Item
HP Restore
Concentrate
Move+1

Javelin
Round Shield
Leather Helmet
Light Robe
Diamond Armlet

Level Jump5, Vertical Jump6
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down



Maakur
Male
Capricorn
56
63
Lancer
Sing
Damage Split
Sicken
Swim

Holy Lance
Ice Shield
Cross Helmet
Leather Armor
Defense Armlet

Level Jump8, Vertical Jump2
Life Song, Magic Song, Last Song, Diamond Blade, Sky Demon, Hydra Pit



Zagorsek
Male
Sagittarius
59
55
Archer
Throw
Abandon
Attack UP
Jump+2

Hunting Bow
Genji Shield
Headgear
Brigandine
N-Kai Armlet

Charge+3, Charge+10
Shuriken, Staff



Mirapoix
Monster
Leo
58
75
Iron Hawk








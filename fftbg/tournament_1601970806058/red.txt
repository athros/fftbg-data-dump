Player: !Red
Team: Red Team
Palettes: Red/Brown



Mesmaster
Male
Virgo
69
41
Archer
Summon Magic
Arrow Guard
Doublehand
Jump+2

Hunting Bow

Cross Helmet
Judo Outfit
Rubber Shoes

Charge+1, Charge+3, Charge+4, Charge+5, Charge+20
Moogle, Ifrit, Carbunkle, Silf



FuzzyTigers
Male
Aries
44
74
Chemist
Punch Art
Dragon Spirit
Maintenance
Teleport

Romanda Gun

Cachusha
Earth Clothes
Magic Gauntlet

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Soft, Remedy, Phoenix Down
Spin Fist, Purification, Chakra, Revive



ValensEXP
Male
Virgo
46
43
Squire
Time Magic
Abandon
Magic Attack UP
Waterwalking

Battle Axe
Crystal Shield
Leather Hat
Power Sleeve
Power Wrist

Accumulate, Heal, Yell, Cheer Up
Stop, Immobilize, Demi 2, Meteor



Smallboops
Female
Libra
73
49
Dancer
Steal
Earplug
Secret Hunt
Retreat

Main Gauche

Green Beret
Leather Outfit
Magic Ring

Witch Hunt, Slow Dance, Polka Polka, Nameless Dance, Last Dance
Gil Taking, Steal Heart, Steal Armor, Steal Shield

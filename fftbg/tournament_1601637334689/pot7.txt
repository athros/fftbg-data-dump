Final Bets: green - 8 bets for 10,992G (58.5%, x0.71); white - 7 bets for 7,808G (41.5%, x1.41)

green bets:
HorusTaurus: 4,718G (42.9%, 4,718G)
LivingHitokiri: 2,000G (18.2%, 153,296G)
E_Ballard: 1,850G (16.8%, 1,850G)
BirbBrainsBot: 1,000G (9.1%, 172,909G)
getthemoneyz: 678G (6.2%, 2,130,025G)
VynxYukida: 346G (3.1%, 346G)
datadrivenbot: 200G (1.8%, 82,480G)
Evewho: 200G (1.8%, 2,738G)

white bets:
AllInBot: 3,754G (48.1%, 3,754G)
SephDarkheart: 1,536G (19.7%, 3,073G)
Zachara: 1,208G (15.5%, 127,708G)
heropong: 864G (11.1%, 864G)
TheSabretoothe: 296G (3.8%, 296G)
CosmicTactician: 100G (1.3%, 4,295G)
king_smashington: 50G (0.6%, 1,422G)

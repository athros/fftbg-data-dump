Final Bets: white - 5 bets for 5,147G (58.5%, x0.71); purple - 8 bets for 3,644G (41.5%, x1.41)

white bets:
AllInBot: 2,198G (42.7%, 2,198G)
SephDarkheart: 1,799G (35.0%, 1,799G)
Zachara: 1,000G (19.4%, 127,000G)
CosmicTactician: 100G (1.9%, 4,224G)
king_smashington: 50G (1.0%, 1,387G)

purple bets:
BirbBrainsBot: 1,000G (27.4%, 173,909G)
LivingHitokiri: 1,000G (27.4%, 154,296G)
Rhyser76: 454G (12.5%, 454G)
VynxYukida: 400G (11.0%, 746G)
TheSabretoothe: 296G (8.1%, 296G)
datadrivenbot: 200G (5.5%, 82,680G)
getthemoneyz: 194G (5.3%, 2,130,219G)
heropong: 100G (2.7%, 964G)

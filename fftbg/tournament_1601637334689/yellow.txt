Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ZCKaiser
Male
Gemini
45
56
Thief
Time Magic
Counter Tackle
Short Charge
Jump+3

Cute Bag

Red Hood
Mystic Vest
Dracula Mantle

Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Haste, Haste 2, Slow, Slow 2, Float, Demi 2



Just Here2
Male
Aquarius
78
79
Time Mage
Battle Skill
Brave Save
Magic Attack UP
Jump+3

Sage Staff

Leather Hat
Linen Robe
Feather Boots

Haste, Haste 2, Slow 2, Reflect, Meteor
Armor Break, Magic Break



ALY327
Female
Taurus
55
68
Geomancer
Draw Out
Meatbone Slash
Defend
Lava Walking

Slasher
Bronze Shield
Golden Hairpin
White Robe
Angel Ring

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Kiyomori



DeathTaxesAndAnime
Female
Taurus
62
49
Thief
Battle Skill
Absorb Used MP
Dual Wield
Retreat

Platinum Sword
Mythril Knife
Flash Hat
Mythril Vest
Feather Boots

Steal Shield
Shield Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Dark Sword

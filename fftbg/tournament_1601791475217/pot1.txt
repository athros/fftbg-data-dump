Final Bets: red - 16 bets for 6,677G (34.7%, x1.88); blue - 16 bets for 12,582G (65.3%, x0.53)

red bets:
DLJuggernaut: 1,490G (22.3%, 2,922G)
CorpusCav: 900G (13.5%, 42,420G)
WireLord: 680G (10.2%, 680G)
technominari: 500G (7.5%, 83,980G)
GenZealot: 500G (7.5%, 24,456G)
Shalloween: 400G (6.0%, 10,262G)
seppu777: 333G (5.0%, 7,152G)
Lolthsmeat: 324G (4.9%, 1,537G)
helpimabug: 292G (4.4%, 292G)
goth_Applebees_hostess: 250G (3.7%, 1,219G)
Reddash9: 228G (3.4%, 228G)
AllInBot: 200G (3.0%, 200G)
VynxYukida: 200G (3.0%, 5,632G)
BirbBrainsBot: 179G (2.7%, 67,875G)
gorgewall: 101G (1.5%, 4,029G)
Firesheath: 100G (1.5%, 2,091G)

blue bets:
DeathTaxesAndAnime: 2,268G (18.0%, 4,448G)
rnark: 1,976G (15.7%, 3,953G)
latebit: 1,800G (14.3%, 22,124G)
Thyrandaal: 1,767G (14.0%, 3,465G)
RunicMagus: 1,020G (8.1%, 46,724G)
HASTERIOUS: 772G (6.1%, 772G)
Demon_Lord_Josh: 537G (4.3%, 537G)
krombobreaker: 500G (4.0%, 28,928G)
getthemoneyz: 442G (3.5%, 2,162,707G)
dem0nj0ns: 300G (2.4%, 12,139G)
ValensEXP_: 200G (1.6%, 6,675G)
Heroebal: 200G (1.6%, 3,047G)
Evewho: 200G (1.6%, 5,166G)
resjudicata3: 200G (1.6%, 4,119G)
datadrivenbot: 200G (1.6%, 80,283G)
MemoriesofFinal: 200G (1.6%, 4,904G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Seppu777
Male
Gemini
56
57
Samurai
Punch Art
Auto Potion
Dual Wield
Jump+1

Koutetsu Knife
Chirijiraden
Cross Helmet
Plate Mail
Elf Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Earth Slash, Purification, Revive, Seal Evil



DHaveWord
Female
Aquarius
67
59
Archer
Black Magic
MP Restore
Long Status
Move-HP Up

Bow Gun
Diamond Shield
Bronze Helmet
Mystic Vest
Defense Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Fire, Fire 2, Fire 4, Ice 2



Technominari
Female
Taurus
50
82
Priest
Draw Out
Auto Potion
Magic Attack UP
Waterwalking

Rainbow Staff

Golden Hairpin
Black Robe
Dracula Mantle

Cure, Cure 2, Cure 3, Raise, Shell, Wall, Esuna
Bizen Boat, Kiyomori



ANFz
Male
Virgo
48
55
Geomancer
Punch Art
Auto Potion
Magic Attack UP
Move-MP Up

Bizen Boat
Round Shield
Black Hood
Wizard Outfit
108 Gems

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive

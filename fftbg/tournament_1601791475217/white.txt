Player: !White
Team: White Team
Palettes: White/Blue



XerxesMindbreaker
Male
Leo
69
80
Mime

Meatbone Slash
Attack UP
Move+1



Triangle Hat
Power Sleeve
Leather Mantle

Mimic




Chuckolator
Male
Cancer
46
42
Squire
Throw
Faith Save
Magic Attack UP
Jump+2

Sleep Sword
Round Shield
Flash Hat
Earth Clothes
Feather Mantle

Dash, Tickle, Yell, Cheer Up
Shuriken, Bomb, Wand, Dictionary



Krombobreaker
Female
Virgo
45
43
Oracle
Draw Out
MA Save
Equip Armor
Teleport

Musk Rod

Leather Hat
Crystal Mail
Diamond Armlet

Blind, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Sleep
Heaven's Cloud



Evewho
Female
Scorpio
39
68
Chemist
Dance
Caution
Sicken
Retreat

Star Bag

Leather Hat
Black Costume
Dracula Mantle

X-Potion, Antidote, Echo Grass, Holy Water, Phoenix Down
Slow Dance, Polka Polka, Disillusion, Obsidian Blade, Void Storage, Dragon Pit

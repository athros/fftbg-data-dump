Player: !Green
Team: Green Team
Palettes: Green/White



Mhorzak
Male
Libra
73
66
Calculator
Tentacle Skill
Critical Quick
Equip Gun
Fly

Battle Folio

Barbuta
Carabini Mail
Reflect Ring

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



HuffFlex
Male
Aries
48
52
Samurai
Basic Skill
Caution
Equip Knife
Ignore Terrain

Orichalcum

Leather Helmet
Light Robe
Small Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Muramasa
Throw Stone, Heal, Fury



ValensEXP
Male
Taurus
47
79
Samurai
Throw
Counter
Dual Wield
Waterbreathing

Chirijiraden
Asura Knife
Crystal Helmet
Light Robe
N-Kai Armlet

Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji
Shuriken, Stick, Dictionary



Arcblazer23
Female
Leo
73
50
Mediator
Throw
Regenerator
Equip Shield
Retreat

Stone Gun
Aegis Shield
Green Beret
Clothes
Genji Gauntlet

Threaten, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Bomb, Ninja Sword

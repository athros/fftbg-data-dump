Player: !White
Team: White Team
Palettes: White/Blue



Kilrazin
Female
Pisces
58
79
Mediator
Basic Skill
Abandon
Long Status
Move-HP Up

Mythril Gun

Red Hood
Clothes
Magic Ring

Persuade, Death Sentence, Insult
Accumulate, Heal, Fury



Farside00831
Male
Taurus
39
72
Lancer
White Magic
MP Restore
Martial Arts
Jump+1


Aegis Shield
Leather Helmet
Genji Armor
Cursed Ring

Level Jump3, Vertical Jump3
Cure 2, Cure 4, Raise, Raise 2, Shell, Wall, Esuna



Galkife
Female
Aquarius
41
48
Wizard
White Magic
Faith Save
Attack UP
Move+3

Mage Masher

Thief Hat
Wizard Outfit
Rubber Shoes

Bolt 3, Ice, Ice 2
Cure, Cure 2, Cure 4, Raise 2, Regen, Esuna



Reddwind
Male
Pisces
38
61
Knight
Elemental
Absorb Used MP
Equip Axe
Waterwalking

Gold Staff
Hero Shield
Diamond Helmet
Gold Armor
Reflect Ring

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

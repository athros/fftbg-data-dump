Player: !Green
Team: Green Team
Palettes: Green/White



NicoSavoy
Female
Scorpio
61
77
Summoner
Steal
MP Restore
Short Charge
Jump+1

Panther Bag

Black Hood
Black Robe
Jade Armlet

Moogle, Golem, Silf
Steal Weapon, Steal Accessory, Leg Aim



Kronikle
Female
Scorpio
76
45
Ninja
Jump
MA Save
Equip Sword
Teleport

Defender
Ancient Sword
Headgear
Clothes
108 Gems

Shuriken, Wand
Level Jump5, Vertical Jump3



ValensEXP
Male
Aries
63
58
Knight
Throw
Abandon
Equip Armor
Ignore Terrain

Mythril Sword
Ice Shield
Iron Helmet
Chain Vest
Genji Gauntlet

Head Break, Magic Break, Speed Break, Justice Sword
Shuriken, Wand



Lowlf
Female
Capricorn
68
60
Summoner
Draw Out
Arrow Guard
Short Status
Move-HP Up

Dragon Rod

Flash Hat
Wizard Outfit
Sprint Shoes

Moogle, Ifrit, Odin, Leviathan
Bizen Boat

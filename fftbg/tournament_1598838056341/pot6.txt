Final Bets: white - 19 bets for 42,930G (79.6%, x0.26); purple - 19 bets for 10,988G (20.4%, x3.91)

white bets:
Zagorsek: 15,861G (36.9%, 15,861G)
poorest_hobo: 12,456G (29.0%, 44,671G)
VolgraTheMoose: 2,500G (5.8%, 201,874G)
dogsandcatsand: 2,311G (5.4%, 4,532G)
latebit: 2,000G (4.7%, 59,541G)
CT_5_Holy: 1,304G (3.0%, 13,045G)
carnivalnights: 1,000G (2.3%, 1,675G)
reinoe: 1,000G (2.3%, 34,158G)
E_Ballard: 816G (1.9%, 98,349G)
thunderducker: 777G (1.8%, 2,651G)
Lemonjohns: 713G (1.7%, 713G)
BartTradingCompany: 413G (1.0%, 4,137G)
PuzzleSecretary: 400G (0.9%, 754G)
Lord_Gwarth: 337G (0.8%, 5,260G)
josephiroth_143: 319G (0.7%, 319G)
resjudicata3: 300G (0.7%, 18,973G)
datadrivenbot: 200G (0.5%, 65,211G)
roofiepops: 123G (0.3%, 3,338G)
killth3kid: 100G (0.2%, 10,100G)

purple bets:
Thyrandaal: 3,582G (32.6%, 3,582G)
Wrath189: 1,486G (13.5%, 1,486G)
EnemyController: 1,111G (10.1%, 1,680,018G)
BirbBrainsBot: 1,000G (9.1%, 168,286G)
mixReveal: 500G (4.6%, 8,064G)
pepperbuster: 500G (4.6%, 1,011G)
Greggernaut: 400G (3.6%, 4,925G)
reddwind_: 356G (3.2%, 4,004G)
krombobreaker: 308G (2.8%, 308G)
MemoriesofFinal: 292G (2.7%, 292G)
CorpusCav: 250G (2.3%, 6,008G)
Leonidusx: 200G (1.8%, 6,073G)
BooshPlays: 200G (1.8%, 2,390G)
Genericllama: 200G (1.8%, 1,592G)
getthemoneyz: 192G (1.7%, 1,785,727G)
PantherIscariot: 111G (1.0%, 7,206G)
ANFz: 100G (0.9%, 3,113G)
arumz: 100G (0.9%, 2,498G)
ko2q: 100G (0.9%, 2,716G)

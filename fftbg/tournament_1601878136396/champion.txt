Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



RunicMagus
Male
Pisces
58
80
Bard
Charge
Caution
Short Status
Swim

Bloody Strings

Headgear
Crystal Mail
Feather Mantle

Life Song, Cheer Song, Space Storage, Sky Demon, Hydra Pit
Charge+2, Charge+4



Evewho
Female
Sagittarius
68
71
Knight
Elemental
Abandon
Defend
Move+2

Defender
Flame Shield
Gold Helmet
Platinum Armor
Feather Mantle

Weapon Break, Magic Break, Mind Break, Justice Sword, Surging Sword
Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



Ar Tactic
Male
Virgo
58
75
Knight
Basic Skill
Earplug
Equip Bow
Move-HP Up

Windslash Bow
Round Shield
Barbuta
Chameleon Robe
Magic Gauntlet

Head Break, Armor Break, Shield Break, Speed Break, Power Break, Justice Sword
Dash, Tickle, Cheer Up



DavenIII
Male
Sagittarius
46
80
Ninja
Talk Skill
Damage Split
Attack UP
Jump+2

Mage Masher
Dagger
Thief Hat
Black Costume
Feather Mantle

Shuriken, Wand
Invitation, Praise, Preach, Insult

Player: !Red
Team: Red Team
Palettes: Red/Brown



HorusTaurus
Female
Cancer
68
71
Calculator
Demon Skill
Critical Quick
Concentrate
Jump+1

Iron Fan
Genji Shield
Barbuta
Carabini Mail
Germinas Boots

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Just Here2
Female
Pisces
43
74
Summoner
Talk Skill
MP Restore
Magic Attack UP
Move-MP Up

Flame Rod

Black Hood
Wizard Robe
Vanish Mantle

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Fairy, Lich
Invitation, Persuade, Death Sentence, Negotiate, Refute, Rehabilitate



Maakur
Male
Aquarius
40
77
Samurai
Jump
Mana Shield
Defend
Fly

Asura Knife

Diamond Helmet
Leather Armor
Battle Boots

Asura, Murasame
Level Jump4, Vertical Jump6



SephDarkheart
Male
Virgo
56
66
Knight
Punch Art
Abandon
Doublehand
Move+2

Materia Blade

Leather Helmet
Bronze Armor
Elf Mantle

Armor Break, Shield Break, Weapon Break, Power Break, Mind Break, Stasis Sword
Pummel, Earth Slash, Purification, Chakra, Revive

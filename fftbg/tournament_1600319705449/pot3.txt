Final Bets: white - 11 bets for 5,326G (25.4%, x2.94); black - 16 bets for 15,638G (74.6%, x0.34)

white bets:
DeathTaxesAndAnime: 1,002G (18.8%, 17,098G)
biske13: 1,000G (18.8%, 36,134G)
prince_rogers_nelson_: 825G (15.5%, 825G)
Raixelol: 600G (11.3%, 11,896G)
Mushufasa_: 500G (9.4%, 19,077G)
Zerguzen: 500G (9.4%, 5,379G)
xenocalypse: 298G (5.6%, 298G)
datadrivenbot: 200G (3.8%, 64,341G)
Swish604: 200G (3.8%, 2,199G)
gorgewall: 101G (1.9%, 5,629G)
BartTradingCompany: 100G (1.9%, 10,262G)

black bets:
Mesmaster: 5,000G (32.0%, 207,744G)
OneHundredFists: 2,088G (13.4%, 4,096G)
CorpusCav: 1,165G (7.4%, 45,745G)
Musashi45: 1,000G (6.4%, 15,512G)
BirbBrainsBot: 1,000G (6.4%, 4,153G)
Lythe_Caraker: 1,000G (6.4%, 155,441G)
Laserman1000: 991G (6.3%, 9,591G)
DaveStrider55: 754G (4.8%, 754G)
Shalloween: 706G (4.5%, 1,412G)
CT_5_Holy: 600G (3.8%, 600G)
lowlf: 540G (3.5%, 12,797G)
AllInBot: 264G (1.7%, 264G)
silentkaster: 200G (1.3%, 1,566G)
getthemoneyz: 130G (0.8%, 1,960,707G)
Firesheath: 100G (0.6%, 4,291G)
MemoriesofFinal: 100G (0.6%, 22,173G)

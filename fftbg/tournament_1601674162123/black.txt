Player: !Black
Team: Black Team
Palettes: Black/Red



DirkC Fenrir84
Male
Taurus
64
70
Lancer
Talk Skill
Counter Flood
Maintenance
Retreat

Spear
Flame Shield
Gold Helmet
Crystal Mail
Sprint Shoes

Level Jump3, Vertical Jump7
Threaten, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute



J2DaBibbles
Female
Virgo
51
57
Chemist
Throw
Counter
Maintenance
Move+3

Glacier Gun

Twist Headband
Leather Outfit
Magic Gauntlet

Potion, Hi-Ether, Eye Drop
Shuriken



Killth3kid
Female
Virgo
44
80
Wizard
Charge
Counter Magic
Equip Knife
Jump+3

Ice Rod

Twist Headband
Linen Robe
Germinas Boots

Fire, Fire 3, Bolt, Bolt 4, Ice 4
Charge+3, Charge+5, Charge+7, Charge+10



Gorgewall
Female
Leo
39
48
Dancer
Item
Auto Potion
Equip Sword
Move+1

Kikuichimoji

Feather Hat
Linen Robe
Cherche

Witch Hunt, Wiznaibus, Last Dance, Void Storage, Dragon Pit
Hi-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



Zachara
Female
Cancer
38
56
Mediator
Battle Skill
Mana Shield
Equip Bow
Jump+2

Mythril Bow

Black Hood
Chain Vest
Angel Ring

Praise, Threaten, Preach, Insult, Mimic Daravon, Refute, Rehabilitate
Armor Break, Shield Break, Magic Break, Mind Break, Dark Sword



KyleWonToLiveForever
Male
Virgo
48
69
Ninja
Battle Skill
PA Save
Magic Defense UP
Jump+2

Dagger
Scorpion Tail
Feather Hat
Leather Outfit
Genji Gauntlet

Bomb
Head Break, Weapon Break, Speed Break, Dark Sword



CorpusCav
Female
Leo
68
53
Chemist
Dance
Damage Split
Long Status
Waterwalking

Sleep Sword

Headgear
Clothes
Power Wrist

Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down
Polka Polka, Disillusion



Sinnyil2
Female
Cancer
53
57
Geomancer
White Magic
Hamedo
Halve MP
Move+3

Slasher
Platinum Shield
Red Hood
Light Robe
Setiemson

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 3, Raise, Raise 2, Reraise, Regen, Shell 2, Esuna, Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Firesheath
Female
Libra
53
50
Dancer
Draw Out
HP Restore
Defend
Jump+1

Cashmere

Twist Headband
Leather Outfit
Feather Boots

Witch Hunt, Wiznaibus, Polka Polka, Obsidian Blade, Nether Demon
Asura, Koutetsu, Heaven's Cloud, Kikuichimoji, Chirijiraden



Skincrawler
Female
Leo
71
63
Ninja
Battle Skill
MA Save
Magic Attack UP
Move+3

Spell Edge
Kunai
Golden Hairpin
Earth Clothes
Germinas Boots

Shuriken, Bomb, Staff
Head Break, Weapon Break, Magic Break, Speed Break, Mind Break, Night Sword, Surging Sword



BappleTrees
Female
Cancer
61
47
Chemist
Jump
Parry
Short Charge
Move+1

Panther Bag

Green Beret
Brigandine
Defense Ring

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Level Jump4, Vertical Jump2



Snkey
Male
Taurus
78
58
Mime

Auto Potion
Sicken
Waterwalking



Flash Hat
Mystic Vest
108 Gems

Mimic


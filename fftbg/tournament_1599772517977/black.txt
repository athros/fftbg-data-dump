Player: !Black
Team: Black Team
Palettes: Black/Red



Mindblownnnn
Female
Serpentarius
39
80
Monk
Summon Magic
Counter Tackle
Sicken
Move-HP Up



Ribbon
Judo Outfit
Spike Shoes

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Moogle, Shiva, Bahamut, Fairy



E Ballard
Male
Pisces
64
69
Bard
White Magic
Faith Save
Equip Knife
Ignore Height

Hidden Knife

Golden Hairpin
Reflect Mail
Dracula Mantle

Life Song, Cheer Song, Magic Song, Diamond Blade
Cure, Raise, Raise 2, Reraise, Regen, Wall



Nhammen
Female
Aquarius
59
69
Thief
Summon Magic
Critical Quick
Dual Wield
Teleport

Mythril Sword
Ninja Edge
Flash Hat
Chain Vest
N-Kai Armlet

Steal Heart, Steal Armor, Arm Aim
Moogle, Ramuh, Bahamut, Silf



Sairentozon7
Male
Capricorn
64
54
Bard
Punch Art
Dragon Spirit
Maintenance
Move+1

Silver Bow

Feather Hat
Black Costume
Feather Boots

Angel Song, Life Song, Battle Song, Magic Song, Last Song, Diamond Blade, Space Storage
Purification, Revive, Seal Evil

Player: !White
Team: White Team
Palettes: White/Blue



Galkife
Female
Cancer
53
56
Monk
Charge
Distribute
Long Status
Move+3



Green Beret
Adaman Vest
Genji Gauntlet

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Charge+1, Charge+10



Gorgewall
Male
Taurus
46
54
Lancer
Steal
MA Save
Attack UP
Jump+3

Battle Bamboo
Flame Shield
Mythril Helmet
Leather Armor
Angel Ring

Level Jump2, Vertical Jump2
Steal Helmet, Steal Status



Lowlf
Male
Cancer
60
66
Archer
White Magic
Catch
Magic Attack UP
Move-HP Up

Snipe Bow
Flame Shield
Feather Hat
Brigandine
Dracula Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Cure, Cure 3, Reraise, Protect 2, Shell, Wall



SephDarkheart
Female
Aquarius
67
66
Lancer
Item
Counter Flood
Throw Item
Jump+2

Holy Lance
Hero Shield
Mythril Helmet
Gold Armor
Elf Mantle

Level Jump2, Vertical Jump6
X-Potion, Holy Water, Remedy, Phoenix Down

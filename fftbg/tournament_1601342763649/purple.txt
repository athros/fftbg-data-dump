Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nhammen
Male
Cancer
80
55
Geomancer
Time Magic
Regenerator
Defend
Move-MP Up

Slasher
Bronze Shield
Green Beret
Wizard Robe
Red Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind
Stop, Quick



Thunderducker
Female
Cancer
61
68
Calculator
Time Magic
Damage Split
Short Charge
Jump+2

Battle Folio

Ribbon
Linen Robe
Jade Armlet

CT, Height, Prime Number, 5, 4, 3
Slow, Reflect, Demi 2, Stabilize Time



TheBrett
Female
Scorpio
69
79
Lancer
Dance
Abandon
Defend
Lava Walking

Partisan
Diamond Shield
Circlet
Carabini Mail
Dracula Mantle

Level Jump5, Vertical Jump2
Disillusion



Daveb
Female
Gemini
60
69
Ninja
Jump
Auto Potion
Defend
Waterbreathing

Mythril Knife
Kunai
Black Hood
Clothes
Rubber Shoes

Shuriken, Knife
Level Jump3, Vertical Jump5

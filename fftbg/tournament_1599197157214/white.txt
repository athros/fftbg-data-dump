Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Sagittarius
74
64
Knight
Jump
Parry
Defend
Move-HP Up

Platinum Sword
Flame Shield
Bronze Helmet
Genji Armor
Cursed Ring

Magic Break, Speed Break, Power Break, Night Sword
Level Jump4, Vertical Jump7



RobotOcelot
Female
Pisces
63
63
Samurai
Charge
Counter Magic
Equip Sword
Swim

Long Sword

Crystal Helmet
Crystal Mail
Feather Mantle

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Charge+2



Thunderducker
Male
Scorpio
59
42
Archer
Battle Skill
Catch
Dual Wield
Ignore Height

Mythril Gun
Stone Gun
Holy Miter
Judo Outfit
Magic Gauntlet

Charge+1
Shield Break, Magic Break, Speed Break, Stasis Sword



CaptainAdmiralSPATULA
Female
Sagittarius
54
60
Summoner
Charge
Counter Tackle
Short Charge
Teleport

Flame Rod

Red Hood
Brigandine
108 Gems

Moogle, Ramuh, Ifrit, Leviathan, Silf
Charge+1, Charge+3, Charge+4, Charge+10

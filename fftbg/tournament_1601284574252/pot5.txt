Final Bets: red - 10 bets for 4,314G (73.3%, x0.37); green - 5 bets for 1,575G (26.7%, x2.74)

red bets:
BirbBrainsBot: 1,000G (23.2%, 23,840G)
MemoriesofFinal: 955G (22.1%, 955G)
Mesmaster: 603G (14.0%, 603G)
AllInBot: 469G (10.9%, 469G)
khelor_: 414G (9.6%, 414G)
getthemoneyz: 316G (7.3%, 2,094,189G)
datadrivenbot: 200G (4.6%, 81,040G)
Rhyser76: 134G (3.1%, 134G)
Lydian_C: 123G (2.9%, 5,209G)
Sharosa: 100G (2.3%, 2,574G)

green bets:
reddwind_: 476G (30.2%, 1,222G)
3ngag3: 408G (25.9%, 3,696G)
Lolthsmeat: 244G (15.5%, 562G)
xGlaive: 239G (15.2%, 239G)
Spuzzmocker: 208G (13.2%, 208G)

Final Bets: white - 11 bets for 14,987G (80.0%, x0.25); black - 7 bets for 3,752G (20.0%, x3.99)

white bets:
DLJuggernaut: 5,000G (33.4%, 16,677G)
MoonSugarFiend: 3,000G (20.0%, 22,432G)
Thyrandaal: 2,380G (15.9%, 4,667G)
Mushufasa_: 1,234G (8.2%, 16,993G)
Lord_Gwarth: 1,000G (6.7%, 8,445G)
HorusTaurus: 867G (5.8%, 867G)
Shalloween: 600G (4.0%, 48,702G)
holdenmagronik: 496G (3.3%, 496G)
AllInBot: 200G (1.3%, 200G)
datadrivenbot: 200G (1.3%, 89,036G)
nhammen: 10G (0.1%, 38,821G)

black bets:
BirbBrainsBot: 1,000G (26.7%, 113,555G)
killth3kid: 828G (22.1%, 31,408G)
getthemoneyz: 502G (13.4%, 2,183,118G)
MelloHeart: 500G (13.3%, 1,177G)
JonnyCue: 472G (12.6%, 10,978G)
2b_yorha_b: 400G (10.7%, 20,900G)
FuzzyTigers: 50G (1.3%, 534G)

Final Bets: green - 8 bets for 4,467G (45.4%, x1.20); yellow - 8 bets for 5,376G (54.6%, x0.83)

green bets:
HaplessOne: 2,511G (56.2%, 2,511G)
LivingHitokiri: 500G (11.2%, 156,180G)
MemoriesofFinal: 356G (8.0%, 356G)
ColetteMSLP: 300G (6.7%, 2,055G)
Genkidou: 200G (4.5%, 1,454G)
datadrivenbot: 200G (4.5%, 81,185G)
ADeatonic: 200G (4.5%, 7,388G)
ayeCoop: 200G (4.5%, 1,846G)

yellow bets:
iBardic: 2,000G (37.2%, 9,432G)
BirbBrainsBot: 1,000G (18.6%, 91,073G)
Tithonus: 856G (15.9%, 856G)
Lolthsmeat: 500G (9.3%, 2,682G)
AllInBot: 468G (8.7%, 468G)
getthemoneyz: 252G (4.7%, 2,110,455G)
CassiePhoenix: 200G (3.7%, 2,880G)
readdesert: 100G (1.9%, 1,057G)

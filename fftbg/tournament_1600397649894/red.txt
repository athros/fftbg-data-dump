Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Male
Aries
58
60
Time Mage
Yin Yang Magic
Abandon
Magic Defense UP
Jump+1

White Staff

Black Hood
Black Robe
Angel Ring

Haste, Slow, Slow 2, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time
Zombie, Dispel Magic, Paralyze



DustBirdEX
Female
Libra
61
66
Ninja
Summon Magic
PA Save
Long Status
Lava Walking

Spell Edge
Flame Whip
Leather Hat
Judo Outfit
Diamond Armlet

Shuriken, Axe
Moogle, Ramuh, Carbunkle, Fairy



Smashy
Male
Virgo
50
69
Knight
Black Magic
Arrow Guard
Beastmaster
Waterbreathing

Ancient Sword
Aegis Shield
Platinum Helmet
Plate Mail
Defense Armlet

Weapon Break, Magic Break, Speed Break, Justice Sword
Fire 3, Bolt 2, Bolt 3, Flare



Ericzubat
Male
Aries
52
70
Archer
Summon Magic
Mana Shield
Short Status
Move+1

Snipe Bow
Bronze Shield
Flash Hat
Mythril Vest
Genji Gauntlet

Charge+1, Charge+3, Charge+7
Moogle, Shiva, Ifrit, Bahamut, Odin, Silf, Fairy, Lich

Final Bets: white - 16 bets for 12,105G (52.9%, x0.89); purple - 17 bets for 10,787G (47.1%, x1.12)

white bets:
Thyrandaal: 2,000G (16.5%, 250,674G)
resjudicata3: 1,707G (14.1%, 1,707G)
AllInBot: 1,583G (13.1%, 1,583G)
RunicMagus: 1,020G (8.4%, 24,066G)
krombobreaker: 1,000G (8.3%, 22,141G)
Zeroroute: 932G (7.7%, 932G)
DrAntiSocial: 824G (6.8%, 824G)
Nizaha: 612G (5.1%, 12,686G)
2b_yorha_b: 554G (4.6%, 554G)
ExplanationMarkWow: 500G (4.1%, 15,990G)
silentkaster: 365G (3.0%, 23,465G)
Lyner87: 308G (2.5%, 308G)
Blakelmenakle: 200G (1.7%, 13,806G)
datadrivenbot: 200G (1.7%, 78,547G)
TheBrett: 200G (1.7%, 1,327G)
NicoSavoy: 100G (0.8%, 48,592G)

purple bets:
Shalloween: 2,993G (27.7%, 2,993G)
skipsandwiches: 1,001G (9.3%, 163,191G)
BirbBrainsBot: 1,000G (9.3%, 28,744G)
DeathTaxesAndAnime: 868G (8.0%, 868G)
DanielRKnealeArt: 790G (7.3%, 15,815G)
E_Ballard: 784G (7.3%, 784G)
MemoriesofFinal: 658G (6.1%, 658G)
nekojin: 500G (4.6%, 8,388G)
scsuperstar: 358G (3.3%, 358G)
Lord_Gwarth: 348G (3.2%, 348G)
seppu777: 333G (3.1%, 1,634G)
Zerguzen: 280G (2.6%, 280G)
ChiChiRodriguez: 236G (2.2%, 236G)
Liesol: 200G (1.9%, 1,043G)
farside00831: 200G (1.9%, 236G)
getthemoneyz: 138G (1.3%, 2,094,788G)
mirapoix: 100G (0.9%, 9,287G)

Final Bets: purple - 9 bets for 13,304G (78.3%, x0.28); brown - 8 bets for 3,693G (21.7%, x3.60)

purple bets:
OneHundredFists: 6,656G (50.0%, 13,051G)
CorpusCav: 3,200G (24.1%, 28,372G)
HaplessOne: 1,536G (11.5%, 10,240G)
lowlf: 624G (4.7%, 16,756G)
reddwind_: 488G (3.7%, 488G)
AllInBot: 200G (1.5%, 200G)
resjudicata3: 200G (1.5%, 5,434G)
datadrivenbot: 200G (1.5%, 78,391G)
Genkidou: 200G (1.5%, 3,408G)

brown bets:
BirbBrainsBot: 1,000G (27.1%, 132,254G)
Sairentozon7: 1,000G (27.1%, 35,058G)
chaosdesigned: 475G (12.9%, 475G)
getthemoneyz: 366G (9.9%, 2,130,384G)
BoneMiser: 300G (8.1%, 2,737G)
elimit404: 252G (6.8%, 252G)
ValensEXP_: 200G (5.4%, 9,451G)
grimthegameking: 100G (2.7%, 100G)

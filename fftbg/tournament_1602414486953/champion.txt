Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Gorgewall
Female
Virgo
64
46
Samurai
Steal
Arrow Guard
Doublehand
Jump+1

Heaven's Cloud

Circlet
Carabini Mail
Reflect Ring

Asura, Koutetsu, Murasame, Heaven's Cloud, Masamune
Steal Heart, Steal Accessory



Leonidusx
Male
Scorpio
53
76
Lancer
Black Magic
Meatbone Slash
Equip Sword
Move+3

Defender

Iron Helmet
Linen Cuirass
Angel Ring

Level Jump4, Vertical Jump6
Fire, Fire 2, Fire 3, Bolt 2, Frog, Death



TheMurkGnome
Male
Sagittarius
54
38
Calculator
White Magic
Regenerator
Equip Bow
Ignore Height

Perseus Bow

Green Beret
Power Sleeve
Jade Armlet

CT, Height, Prime Number, 4
Raise 2, Esuna, Holy



Olva
Female
Scorpio
46
69
Geomancer
Draw Out
PA Save
Equip Knife
Swim

Mythril Knife
Platinum Shield
Thief Hat
Earth Clothes
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Koutetsu, Bizen Boat, Kikuichimoji

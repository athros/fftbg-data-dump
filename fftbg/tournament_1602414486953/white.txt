Player: !White
Team: White Team
Palettes: White/Blue



Heropong
Female
Aquarius
64
41
Samurai
Jump
MA Save
Attack UP
Jump+1

Kikuichimoji

Cross Helmet
Gold Armor
N-Kai Armlet

Koutetsu, Murasame, Muramasa
Level Jump8, Vertical Jump6



SensualOnix
Monster
Sagittarius
64
67
King Behemoth










E Ballard
Male
Serpentarius
64
74
Ninja
Punch Art
Parry
Equip Gun
Ignore Height

Battle Folio
Papyrus Codex
Headgear
Adaman Vest
Setiemson

Ninja Sword
Pummel, Purification, Revive



VolgraTheMoose
Male
Libra
57
41
Archer
Yin Yang Magic
MP Restore
Dual Wield
Move+2

Bow Gun
Night Killer
Platinum Helmet
Brigandine
Feather Mantle

Charge+1, Charge+4, Charge+5, Charge+20
Poison, Spell Absorb, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Petrify

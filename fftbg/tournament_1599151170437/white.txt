Player: !White
Team: White Team
Palettes: White/Blue



Nifboy
Male
Sagittarius
62
55
Mime

Damage Split
Secret Hunt
Jump+1



Triangle Hat
Black Costume
Leather Mantle

Mimic




Old Overholt
Monster
Aries
65
59
Cockatrice










Killth3kid
Male
Libra
54
39
Wizard
Throw
Counter Flood
Halve MP
Move+2

Cultist Dagger

Red Hood
Black Robe
Diamond Armlet

Bolt 4
Dictionary



Mrfripps
Male
Sagittarius
79
41
Geomancer
Punch Art
Catch
Equip Armor
Ignore Height

Asura Knife
Round Shield
Iron Helmet
Adaman Vest
Red Shoes

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Spin Fist, Secret Fist, Purification, Revive

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Laserman1000
Male
Sagittarius
53
81
Calculator
Demon Skill
Meatbone Slash
Equip Shield
Jump+2

Rod
Flame Shield
Iron Helmet
Genji Armor
Battle Boots

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Chuckolator
Male
Libra
58
60
Lancer
Charge
Absorb Used MP
Concentrate
Jump+3

Dragon Whisker
Escutcheon
Crystal Helmet
Crystal Mail
Germinas Boots

Level Jump8, Vertical Jump8
Charge+1, Charge+2, Charge+4, Charge+5, Charge+20



BuffaloCrunch
Female
Leo
65
60
Dancer
Yin Yang Magic
Damage Split
Doublehand
Fly

Star Bag

Feather Hat
Chameleon Robe
Jade Armlet

Witch Hunt, Polka Polka, Disillusion, Void Storage, Dragon Pit
Doubt Faith, Foxbird, Confusion Song, Dispel Magic



Smashy
Female
Gemini
78
49
Monk
Summon Magic
Abandon
Equip Shield
Move+3

Hydra Bag
Diamond Shield
Flash Hat
Brigandine
Leather Mantle

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Leviathan, Silf, Fairy

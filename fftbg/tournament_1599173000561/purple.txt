Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Scorpio
75
43
Samurai
Elemental
Counter Magic
Martial Arts
Ignore Height

Koutetsu Knife

Barbuta
Linen Robe
Leather Mantle

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Gusty Wind, Lava Ball



RobotOcelot
Female
Sagittarius
46
77
Lancer
Dance
Critical Quick
Doublehand
Jump+2

Ivory Rod

Barbuta
Diamond Armor
Leather Mantle

Level Jump5, Vertical Jump5
Wiznaibus, Last Dance



FriendSquirrel
Female
Pisces
73
59
Summoner
Jump
Absorb Used MP
Beastmaster
Retreat

Flame Rod

Golden Hairpin
Earth Clothes
Dracula Mantle

Moogle, Ifrit, Odin, Salamander
Level Jump2, Vertical Jump7



Ruleof5
Female
Virgo
36
51
Archer
White Magic
Damage Split
Short Charge
Retreat

Long Bow

Gold Helmet
Black Costume
Wizard Mantle

Charge+1, Charge+4, Charge+5, Charge+7
Cure, Raise, Raise 2, Regen, Protect 2, Shell 2, Wall, Esuna

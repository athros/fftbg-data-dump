Player: !Red
Team: Red Team
Palettes: Red/Brown



Snowfats
Female
Scorpio
48
73
Archer
Punch Art
Parry
Short Status
Move-HP Up

Windslash Bow

Feather Hat
Leather Outfit
Dracula Mantle

Charge+1, Charge+3, Charge+5, Charge+7
Earth Slash, Secret Fist, Purification, Chakra, Revive



Error72
Male
Gemini
49
79
Knight
Sing
PA Save
Magic Defense UP
Ignore Height

Defender
Diamond Shield
Gold Helmet
White Robe
Spike Shoes

Head Break, Weapon Break, Speed Break
Life Song, Magic Song, Diamond Blade, Hydra Pit



BooshPlays
Female
Libra
78
71
Calculator
Mighty Sword
Abandon
Doublehand
Jump+3

Gokuu Rod

Cachusha
Platinum Armor
Chantage

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Redmage4evah
Female
Cancer
48
47
Mime

Counter
Equip Armor
Move+2



Genji Helmet
Diamond Armor
108 Gems

Mimic


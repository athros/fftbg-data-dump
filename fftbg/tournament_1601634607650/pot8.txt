Final Bets: purple - 5 bets for 3,962G (36.5%, x1.74); champion - 12 bets for 6,878G (63.5%, x0.58)

purple bets:
2b_yorha_b: 1,584G (40.0%, 1,584G)
Mushufasa_: 1,234G (31.1%, 147,534G)
LivingHitokiri: 500G (12.6%, 151,837G)
ColetteMSLP: 484G (12.2%, 13,699G)
Sharosa: 160G (4.0%, 160G)

champion bets:
AllInBot: 1,013G (14.7%, 1,013G)
BirbBrainsBot: 1,000G (14.5%, 169,723G)
MemoriesofFinal: 1,000G (14.5%, 2,529G)
HorusTaurus: 1,000G (14.5%, 6,342G)
Error72: 633G (9.2%, 633G)
heropong: 571G (8.3%, 571G)
seppu777: 555G (8.1%, 2,144G)
Zachara: 500G (7.3%, 124,500G)
TheSabretoothe: 200G (2.9%, 654G)
datadrivenbot: 200G (2.9%, 82,972G)
VynxYukida: 200G (2.9%, 1,552G)
getthemoneyz: 6G (0.1%, 2,129,125G)

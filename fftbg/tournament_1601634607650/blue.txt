Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HorusTaurus
Male
Aquarius
41
75
Archer
Battle Skill
Critical Quick
Defend
Move+1

Romanda Gun
Buckler
Crystal Helmet
Adaman Vest
Feather Boots

Charge+2, Charge+4
Head Break, Power Break, Justice Sword, Night Sword



GeNoFPaniC
Female
Sagittarius
58
79
Monk
Talk Skill
Arrow Guard
Equip Armor
Ignore Terrain



Crystal Helmet
Black Robe
Feather Mantle

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification
Invitation, Preach, Insult



AtmaDragoon
Female
Serpentarius
53
52
Chemist
Throw
Catch
Equip Gun
Retreat

Battle Folio

Cachusha
Power Sleeve
Jade Armlet

Potion, Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down
Bomb, Dictionary



Thyrandaal
Female
Aries
58
71
Oracle
Item
Faith Save
Equip Bow
Levitate

Night Killer

Headgear
White Robe
Magic Gauntlet

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic
Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



LeoNightFury
Female
Gemini
53
69
Mime

Hamedo
Magic Attack UP
Fly



Triangle Hat
Platinum Armor
Wizard Mantle

Mimic




Mushufasa
Female
Virgo
65
50
Thief
Item
Counter Flood
Attack UP
Waterwalking

Mythril Knife

Golden Hairpin
Black Costume
Magic Gauntlet

Steal Armor, Steal Accessory, Leg Aim
Potion, Hi-Potion, Remedy



ALY327
Female
Capricorn
71
74
Summoner
Steal
Faith Save
Short Charge
Move-HP Up

Wizard Staff

Green Beret
Mystic Vest
Wizard Mantle

Ifrit, Golem, Carbunkle, Leviathan, Fairy
Steal Heart, Steal Armor, Leg Aim



Genkidou
Male
Libra
61
52
Archer
Jump
Speed Save
Doublehand
Lava Walking

Windslash Bow

Cross Helmet
Adaman Vest
Defense Ring

Charge+3, Charge+5
Level Jump8, Vertical Jump2

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lyner87
Male
Pisces
47
45
Knight
Sing
Abandon
Halve MP
Jump+2

Mythril Sword
Round Shield
Leather Helmet
Genji Armor
Reflect Ring

Weapon Break, Surging Sword
Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Space Storage, Sky Demon



Roofiepops
Female
Aries
72
61
Thief
Black Magic
Speed Save
Sicken
Waterbreathing

Short Edge

Triangle Hat
Earth Clothes
Genji Gauntlet

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon
Fire 2, Fire 3, Flare



Shalloween
Male
Leo
70
57
Ninja
Item
Arrow Guard
Equip Gun
Jump+1

Bestiary
Battle Folio
Leather Hat
Clothes
Leather Mantle

Shuriken, Bomb, Hammer
Potion, Hi-Potion, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down



HASTERIOUS
Male
Scorpio
66
68
Lancer
Punch Art
Damage Split
Maintenance
Ignore Terrain

Obelisk
Gold Shield
Leather Helmet
Leather Armor
Feather Mantle

Level Jump8, Vertical Jump8
Spin Fist, Purification, Revive, Seal Evil

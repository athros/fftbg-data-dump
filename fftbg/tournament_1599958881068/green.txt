Player: !Green
Team: Green Team
Palettes: Green/White



Powergems
Female
Sagittarius
42
69
Monk
Steal
Earplug
Doublehand
Move+3



Golden Hairpin
Judo Outfit
Magic Ring

Spin Fist, Wave Fist, Purification, Chakra, Revive
Steal Shield, Arm Aim, Leg Aim



Ogema TheDream
Male
Aries
61
53
Mime

Absorb Used MP
Dual Wield
Jump+3



Leather Hat
Judo Outfit
Defense Armlet

Mimic




Nickyfive
Male
Scorpio
61
52
Monk
Yin Yang Magic
Auto Potion
Short Status
Jump+2

Cute Bag

Golden Hairpin
Wizard Outfit
Wizard Mantle

Spin Fist, Secret Fist, Purification, Revive
Poison, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Dark Holy



Gorgewall
Female
Virgo
44
62
Squire
Battle Skill
Speed Save
Short Charge
Ignore Terrain

Ancient Sword
Escutcheon
Green Beret
Leather Outfit
Magic Ring

Dash, Heal, Yell, Ultima
Weapon Break, Magic Break, Mind Break, Justice Sword

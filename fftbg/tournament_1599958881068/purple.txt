Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thunderducker
Female
Pisces
75
70
Oracle
Item
Parry
Throw Item
Jump+1

Iron Fan

Leather Hat
White Robe
Salty Rage

Poison, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep
Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



Victoriolue
Male
Serpentarius
42
60
Lancer
Draw Out
Counter Tackle
Attack UP
Jump+2

Partisan
Crystal Shield
Mythril Helmet
Crystal Mail
Angel Ring

Level Jump4, Vertical Jump5
Koutetsu, Kiyomori, Muramasa, Kikuichimoji



DAC169
Female
Cancer
69
64
Summoner
Elemental
Sunken State
Short Charge
Teleport 2

Oak Staff

Barette
Wizard Robe
Defense Ring

Moogle, Ifrit, Odin, Fairy, Lich, Cyclops
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



SkylerBunny
Female
Virgo
70
61
Lancer
Yin Yang Magic
Meatbone Slash
Long Status
Move+1

Partisan
Gold Shield
Genji Helmet
Leather Armor
N-Kai Armlet

Level Jump3, Vertical Jump8
Spell Absorb, Life Drain, Doubt Faith, Zombie, Dispel Magic

Player: !Red
Team: Red Team
Palettes: Red/Brown



Ayntlerz
Male
Gemini
74
51
Chemist
Summon Magic
Critical Quick
Equip Sword
Ignore Height

Chaos Blade

Cachusha
Clothes
Diamond Armlet

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Ramuh, Titan, Golem, Carbunkle, Odin, Silf, Fairy



ANFz
Male
Aquarius
80
73
Geomancer
Punch Art
Parry
Magic Attack UP
Fly

Panther Bag
Kaiser Plate
Golden Hairpin
Silk Robe
Defense Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Pummel, Earth Slash, Secret Fist, Purification



Mindblownnnn
Male
Aquarius
56
74
Calculator
Black Magic
MA Save
Sicken
Move+2

Bestiary

Holy Miter
Leather Outfit
Spike Shoes

CT, Height, Prime Number, 5, 4
Ice, Ice 2



NovaKnight21
Female
Taurus
76
77
Time Mage
Punch Art
Distribute
Short Charge
Waterbreathing

White Staff

Twist Headband
Light Robe
Red Shoes

Haste 2, Immobilize, Float, Quick, Stabilize Time
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive

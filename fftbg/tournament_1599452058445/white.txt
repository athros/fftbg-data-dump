Player: !White
Team: White Team
Palettes: White/Blue



ValensEXP
Female
Capricorn
50
45
Squire
Summon Magic
Counter Flood
Defense UP
Jump+3

Mythril Sword
Buckler
Platinum Helmet
Mystic Vest
Battle Boots

Heal, Tickle, Yell, Cheer Up, Wish
Titan, Carbunkle, Odin, Leviathan



Lord Gwarth
Male
Pisces
73
58
Lancer
Basic Skill
Dragon Spirit
Doublehand
Waterwalking

Iron Fan

Iron Helmet
Chameleon Robe
Red Shoes

Level Jump5, Vertical Jump6
Heal, Wish, Scream



CorpusCav
Female
Libra
45
46
Wizard
Throw
Parry
Long Status
Teleport

Dagger

Feather Hat
Wizard Robe
108 Gems

Fire, Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Death
Shuriken, Bomb, Axe



Acid Flashback
Male
Virgo
63
66
Monk
Time Magic
Damage Split
Short Status
Waterbreathing



Twist Headband
Black Costume
Wizard Mantle

Pummel, Earth Slash, Purification, Revive
Slow, Slow 2, Immobilize, Reflect, Quick, Demi 2, Stabilize Time, Meteor

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Leonidusx
Male
Aries
56
71
Ninja
Steal
MP Restore
Equip Gun
Move+1

Stone Gun
Glacier Gun
Red Hood
Mythril Vest
Reflect Ring

Shuriken, Knife, Dictionary
Steal Armor, Steal Weapon



Mesmaster
Female
Virgo
59
36
Wizard
Draw Out
Distribute
Long Status
Jump+1

Rod

Triangle Hat
Linen Robe
Genji Gauntlet

Fire 3, Fire 4, Bolt 2, Bolt 4, Empower, Frog, Death, Flare
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



ALY327
Female
Gemini
76
78
Priest
Punch Art
Auto Potion
Halve MP
Move+3

Wizard Staff

Barette
Leather Outfit
Bracer

Cure 3, Raise, Shell, Esuna
Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil



Upvla
Male
Capricorn
73
71
Monk
Summon Magic
Catch
Dual Wield
Ignore Terrain



Golden Hairpin
Secret Clothes
Defense Ring

Pummel, Secret Fist, Purification, Chakra
Moogle, Shiva, Ifrit, Carbunkle, Leviathan

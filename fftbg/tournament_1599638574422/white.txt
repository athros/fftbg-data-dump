Player: !White
Team: White Team
Palettes: White/Blue



Douchetron
Female
Pisces
77
64
Priest
Steal
Counter Tackle
Equip Gun
Waterwalking

Fairy Harp

Twist Headband
Black Robe
Small Mantle

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Shell, Shell 2, Wall
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Steal Status



TeaTime29
Female
Scorpio
64
46
Time Mage
Summon Magic
Regenerator
Short Charge
Ignore Height

Iron Fan

Feather Hat
Linen Robe
Magic Ring

Haste, Immobilize, Reflect, Quick, Meteor
Shiva, Titan, Carbunkle, Bahamut, Leviathan



Mindblownnnn
Male
Capricorn
48
45
Monk
Steal
Regenerator
Doublehand
Lava Walking



Ribbon
Secret Clothes
108 Gems

Spin Fist, Pummel, Earth Slash, Purification, Revive
Steal Heart, Steal Armor



Zenlion
Male
Aries
42
64
Archer
Battle Skill
Counter Tackle
Defense UP
Levitate

Snipe Bow
Round Shield
Green Beret
Mystic Vest
Magic Gauntlet

Charge+1, Charge+2, Charge+7
Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword

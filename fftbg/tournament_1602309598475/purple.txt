Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Technominari
Female
Aquarius
65
78
Priest
Basic Skill
Speed Save
Magic Defense UP
Move-HP Up

Wizard Staff

Thief Hat
Mythril Vest
Small Mantle

Cure 3, Raise, Raise 2, Reraise, Regen, Esuna, Holy
Wish, Scream



DustBirdEX
Male
Gemini
69
65
Samurai
Black Magic
Counter
Magic Attack UP
Swim

Murasame

Platinum Helmet
Diamond Armor
Feather Boots

Koutetsu, Murasame, Heaven's Cloud
Fire 3, Bolt 3, Bolt 4, Ice, Ice 2



Xenomorith
Female
Aquarius
50
68
Dancer
Elemental
Abandon
Short Status
Move+2

Cute Bag

Twist Headband
Power Sleeve
Feather Mantle

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Void Storage, Dragon Pit
Sand Storm, Blizzard, Gusty Wind, Lava Ball



HASTERIOUS
Female
Taurus
49
40
Priest
Math Skill
Auto Potion
Short Charge
Move-MP Up

Mace of Zeus

Triangle Hat
Leather Outfit
Reflect Ring

Cure, Cure 2, Raise 2, Regen, Shell 2, Esuna
CT, Height, Prime Number, 5

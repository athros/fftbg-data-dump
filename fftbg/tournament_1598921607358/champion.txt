Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Skillomono
Male
Scorpio
45
48
Geomancer
Throw
Abandon
Magic Attack UP
Move+2

Slasher
Flame Shield
Leather Hat
Silk Robe
Red Shoes

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Wand



Strifu
Monster
Leo
75
59
King Behemoth










Go2sleepTV
Female
Aquarius
68
68
Monk
Charge
Dragon Spirit
Sicken
Jump+1



Red Hood
Earth Clothes
Elf Mantle

Spin Fist, Pummel, Revive
Charge+10



Mesmaster
Male
Leo
50
57
Knight
Draw Out
Catch
Halve MP
Jump+3

Defender
Crystal Shield
Barbuta
Leather Armor
Leather Mantle

Armor Break, Weapon Break, Mind Break, Stasis Sword, Justice Sword
Asura, Bizen Boat, Heaven's Cloud

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Leonidusx
Male
Libra
53
56
Lancer
Black Magic
Speed Save
Sicken
Ignore Terrain

Cypress Rod
Aegis Shield
Barbuta
Plate Mail
Bracer

Level Jump3, Vertical Jump6
Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Empower, Frog, Death



Skinny666
Female
Virgo
74
76
Mediator
Throw
Meatbone Slash
Dual Wield
Move+3

Stone Gun
Blast Gun
Red Hood
Chain Vest
Magic Gauntlet

Invitation, Solution, Refute, Rehabilitate
Knife



JethroThrul
Male
Pisces
63
58
Oracle
Time Magic
Caution
Equip Armor
Move+3

Madlemgen

Golden Hairpin
Chain Mail
Defense Ring

Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic
Haste, Slow, Stop, Immobilize, Stabilize Time



Resjudicata3
Male
Scorpio
69
77
Thief
Sing
Counter Tackle
Equip Gun
Move+1

Blaze Gun

Leather Hat
Adaman Vest
Elf Mantle

Steal Shield, Steal Weapon, Arm Aim
Life Song, Last Song, Space Storage

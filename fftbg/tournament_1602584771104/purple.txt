Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



OneHundredFists
Male
Cancer
69
50
Monk
Yin Yang Magic
Counter Magic
Equip Armor
Move+1



Holy Miter
Leather Armor
Cherche

Earth Slash, Purification, Chakra, Revive
Blind, Life Drain, Sleep, Dark Holy



Khelor
Male
Cancer
45
69
Calculator
White Magic
Meatbone Slash
Equip Axe
Lava Walking

White Staff

Green Beret
Earth Clothes
Leather Mantle

Height, 5, 4
Cure, Raise, Shell, Shell 2, Esuna



Evewho
Female
Taurus
58
65
Chemist
Yin Yang Magic
Brave Save
Dual Wield
Move-MP Up

Dagger
Cultist Dagger
Holy Miter
Chain Vest
Bracer

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Blind, Poison, Dispel Magic, Paralyze, Dark Holy



CassiePhoenix
Female
Aquarius
72
40
Samurai
Basic Skill
Counter
Magic Attack UP
Waterwalking

Asura Knife

Genji Helmet
Wizard Robe
Genji Gauntlet

Asura, Koutetsu, Muramasa
Cheer Up

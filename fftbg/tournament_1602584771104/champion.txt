Player: !zChamp
Team: Champion Team
Palettes: Black/Red



JethroThrul
Male
Aquarius
54
48
Mediator
Sing
Meatbone Slash
Equip Sword
Levitate

Murasame

Holy Miter
Light Robe
Genji Gauntlet

Persuade, Threaten, Death Sentence, Negotiate, Rehabilitate
Angel Song, Cheer Song, Battle Song, Sky Demon



Reddwind
Male
Leo
80
66
Knight
Elemental
Arrow Guard
Concentrate
Move+1

Mythril Sword
Mythril Shield
Genji Helmet
Gold Armor
Reflect Ring

Head Break, Power Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



TheBrett
Female
Virgo
49
62
Summoner
Steal
Parry
Concentrate
Waterbreathing

Thunder Rod

Triangle Hat
Power Sleeve
Rubber Shoes

Moogle, Shiva, Ramuh, Leviathan, Silf, Fairy, Cyclops
Gil Taking, Steal Armor, Leg Aim



OneHundredFists
Female
Leo
80
61
Time Mage
White Magic
Regenerator
Equip Sword
Lava Walking

Rune Blade

Triangle Hat
Black Costume
108 Gems

Haste 2, Immobilize, Quick, Demi 2
Cure, Raise, Protect 2, Shell, Wall, Esuna

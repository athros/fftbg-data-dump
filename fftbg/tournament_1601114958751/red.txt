Player: !Red
Team: Red Team
Palettes: Red/Brown



Ruleof5
Female
Taurus
38
77
Archer
White Magic
Arrow Guard
Maintenance
Jump+1

Lightning Bow

Holy Miter
Power Sleeve
Jade Armlet

Charge+3, Charge+7
Cure, Raise, Raise 2, Shell, Shell 2



Evewho
Female
Gemini
45
42
Chemist
Battle Skill
Speed Save
Equip Polearm
Move-HP Up

Holy Lance

Leather Hat
Chain Vest
Angel Ring

Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Phoenix Down
Head Break, Weapon Break, Power Break, Stasis Sword, Justice Sword



DouglasDragonThePoet
Female
Libra
46
60
Archer
Steal
Counter Flood
Doublehand
Teleport

Gastrafitis

Barette
Brigandine
Magic Gauntlet

Charge+10
Steal Armor, Steal Shield, Arm Aim



PopsLIVE
Male
Capricorn
47
42
Chemist
Elemental
Counter Flood
Equip Armor
Swim

Hydra Bag

Crystal Helmet
Wizard Robe
Magic Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Gusty Wind

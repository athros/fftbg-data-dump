Final Bets: purple - 19 bets for 10,486G (34.0%, x1.95); brown - 15 bets for 20,396G (66.0%, x0.51)

purple bets:
Leonidusx: 2,000G (19.1%, 241,704G)
latebit: 1,913G (18.2%, 3,751G)
mindblownnnn: 1,501G (14.3%, 1,501G)
BirbBrainsBot: 1,000G (9.5%, 165,654G)
upvla: 600G (5.7%, 600G)
Meta_Five: 500G (4.8%, 3,085G)
nok____: 500G (4.8%, 3,897G)
silentkaster: 428G (4.1%, 428G)
MoonSugarFiend: 301G (2.9%, 301G)
TsukiNoHanaCh: 269G (2.6%, 269G)
Spartan_Paladin: 255G (2.4%, 4,655G)
killabandaid: 236G (2.3%, 236G)
Moshyhero: 200G (1.9%, 1,562G)
datadrivenbot: 200G (1.9%, 76,009G)
DocSTN: 200G (1.9%, 1,100G)
Rhyser76: 108G (1.0%, 108G)
Belkra: 100G (1.0%, 526G)
FlavaFraze: 100G (1.0%, 1,297G)
DeathTaxesAndAnime: 75G (0.7%, 2,237G)

brown bets:
ruleof5: 10,000G (49.0%, 42,550G)
NovaKnight21: 3,165G (15.5%, 6,331G)
resjudicata3: 2,367G (11.6%, 2,367G)
robespyah: 738G (3.6%, 738G)
Runeseeker22: 724G (3.5%, 14,481G)
getthemoneyz: 574G (2.8%, 2,083,147G)
khelor_: 527G (2.6%, 527G)
krombobreaker: 500G (2.5%, 23,058G)
AllInBot: 393G (1.9%, 393G)
MemoriesofFinal: 308G (1.5%, 308G)
CosmicTactician: 300G (1.5%, 29,327G)
AltimaMantoid: 300G (1.5%, 23,600G)
Evewho: 200G (1.0%, 4,041G)
iBluntDouble: 200G (1.0%, 1,078G)
RunicMagus: 100G (0.5%, 22,969G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Silentkaster
Male
Virgo
55
67
Thief
Time Magic
Mana Shield
Attack UP
Retreat

Kunai

Holy Miter
Chain Vest
Genji Gauntlet

Steal Helmet, Steal Shield, Leg Aim
Haste, Slow, Slow 2, Immobilize, Float, Quick



Spartan Paladin
Male
Aries
74
36
Knight
Yin Yang Magic
Arrow Guard
Dual Wield
Retreat

Ragnarok
Rune Blade
Barbuta
Light Robe
Feather Boots

Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword
Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze



Meta Five
Female
Cancer
72
56
Dancer
Black Magic
Distribute
Defend
Waterwalking

Cute Bag

Twist Headband
Robe of Lords
Magic Ring

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Dragon Pit
Fire 2, Bolt, Bolt 4, Ice, Death, Flare



ANFz
Male
Scorpio
76
79
Geomancer
Black Magic
Mana Shield
Attack UP
Move+1

Slasher
Gold Shield
Red Hood
Adaman Vest
Magic Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Fire 4, Bolt 2, Bolt 4, Ice 2, Ice 4

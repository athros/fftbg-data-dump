Player: !White
Team: White Team
Palettes: White/Blue



Gorgewall
Male
Gemini
78
79
Mime

Blade Grasp
Martial Arts
Swim



Triangle Hat
Power Sleeve
Sprint Shoes

Mimic



Murderclan
Female
Leo
50
58
Summoner
Item
Counter Flood
Throw Item
Ignore Terrain

Papyrus Codex

Barette
White Robe
Germinas Boots

Moogle, Golem, Carbunkle, Silf
Potion, X-Potion, Eye Drop, Echo Grass, Remedy, Phoenix Down



Lolthsmeat
Male
Capricorn
65
68
Archer
Black Magic
Catch
Short Charge
Jump+2

Windslash Bow

Twist Headband
Secret Clothes
Reflect Ring

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Fire, Ice 4



ASkyNightly
Male
Scorpio
45
39
Lancer
Elemental
Mana Shield
Dual Wield
Move-MP Up

Spear
Partisan
Mythril Helmet
Black Robe
Magic Ring

Level Jump8, Vertical Jump8
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

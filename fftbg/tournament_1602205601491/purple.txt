Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Chuckolator
Monster
Aquarius
57
78
Dryad










Dogsandcatsand
Female
Capricorn
61
43
Wizard
Time Magic
Arrow Guard
Attack UP
Ignore Terrain

Dragon Rod

Feather Hat
Light Robe
Elf Mantle

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 4, Flare
Slow, Slow 2, Immobilize



Demon Lord Josh
Male
Aquarius
70
42
Geomancer
Basic Skill
Parry
Beastmaster
Waterbreathing

Kiyomori
Ice Shield
Feather Hat
White Robe
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Accumulate, Dash, Cheer Up, Wish



ForagerCats
Female
Pisces
55
54
Chemist
Punch Art
Meatbone Slash
Dual Wield
Ignore Terrain

Blind Knife
Main Gauche
Green Beret
Mythril Vest
Battle Boots

Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Earth Slash, Secret Fist, Purification, Chakra

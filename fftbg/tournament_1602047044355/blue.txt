Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mhorzak
Female
Aries
68
73
Archer
Steal
MP Restore
Halve MP
Retreat

Glacier Gun
Ice Shield
Twist Headband
Black Costume
Red Shoes

Charge+1, Charge+2, Charge+4, Charge+5
Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Arm Aim



Mesmaster
Female
Gemini
68
44
Thief
Jump
Meatbone Slash
Equip Polearm
Waterbreathing

Ryozan Silk

Leather Hat
Mythril Vest
Elf Mantle

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim
Level Jump8, Vertical Jump5



Chuckolator
Male
Sagittarius
58
75
Time Mage
Throw
PA Save
Defend
Fly

Ivory Rod

Headgear
Chameleon Robe
Jade Armlet

Haste, Slow, Slow 2, Stabilize Time
Shuriken



Nekojin
Male
Gemini
54
50
Squire
Talk Skill
Distribute
Equip Sword
Jump+1

Kiyomori
Round Shield
Feather Hat
Secret Clothes
Defense Armlet

Accumulate, Dash, Tickle, Yell, Cheer Up, Fury
Praise, Preach, Death Sentence, Insult, Negotiate, Refute, Rehabilitate

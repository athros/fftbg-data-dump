Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Blastinus
Male
Sagittarius
39
54
Priest
Sing
Catch
Defense UP
Jump+3

Wizard Staff

Feather Hat
Black Robe
Dracula Mantle

Raise, Raise 2, Shell, Wall, Esuna
Angel Song, Nameless Song, Last Song, Hydra Pit



Victoriolue
Male
Aquarius
50
74
Mediator
Steal
Dragon Spirit
Beastmaster
Jump+2

Madlemgen

Feather Hat
Silk Robe
Power Wrist

Praise, Solution, Death Sentence, Refute
Steal Heart, Steal Armor, Steal Accessory, Arm Aim



AltimaMantoid
Female
Pisces
76
60
Calculator
Black Magic
Parry
Equip Gun
Move-HP Up

Battle Folio

Red Hood
Black Costume
Reflect Ring

CT, 5, 3
Fire, Fire 4, Bolt 3, Bolt 4, Ice 3, Ice 4



Chuckolator
Female
Sagittarius
79
46
Samurai
Yin Yang Magic
Counter
Doublehand
Ignore Terrain

Bizen Boat

Crystal Helmet
Light Robe
Defense Armlet

Asura, Bizen Boat, Murasame, Heaven's Cloud, Masamune
Blind, Spell Absorb, Pray Faith, Doubt Faith, Dispel Magic

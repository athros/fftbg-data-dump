Player: !White
Team: White Team
Palettes: White/Blue



Zenlion
Male
Taurus
52
63
Priest
Talk Skill
Earplug
Maintenance
Fly

Gold Staff

Twist Headband
Adaman Vest
Diamond Armlet

Cure 2, Raise, Reraise, Protect, Shell 2, Esuna
Death Sentence, Negotiate, Mimic Daravon



Wonser
Male
Sagittarius
60
46
Time Mage
Black Magic
Regenerator
Defense UP
Ignore Height

Mace of Zeus

Holy Miter
Wizard Robe
Diamond Armlet

Slow 2, Stop, Immobilize, Demi, Stabilize Time
Fire, Fire 4, Bolt, Bolt 4, Ice, Ice 3, Ice 4, Empower, Death



Akash7
Male
Capricorn
63
38
Chemist
Steal
Mana Shield
Equip Shield
Ignore Terrain

Mage Masher
Bronze Shield
Red Hood
Mystic Vest
Magic Gauntlet

Potion, Ether, Hi-Ether, Antidote, Eye Drop
Gil Taking, Steal Heart, Steal Armor, Steal Weapon



Mesmaster
Male
Gemini
49
44
Geomancer
Jump
Dragon Spirit
Attack UP
Swim

Battle Axe
Round Shield
Headgear
Judo Outfit
Genji Gauntlet

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Level Jump5, Vertical Jump8

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thunderducker
Male
Capricorn
50
77
Chemist
Steal
Faith Save
Maintenance
Retreat

Hydra Bag

Black Hood
Black Costume
Defense Armlet

Antidote, Holy Water, Phoenix Down
Steal Heart, Steal Shield, Steal Accessory, Steal Status, Arm Aim



CassiePhoenix
Female
Capricorn
50
69
Calculator
Time Magic
Counter
Equip Bow
Jump+3

Ice Bow

Twist Headband
Mystic Vest
Sprint Shoes

CT, Prime Number, 5, 4
Haste, Demi, Demi 2, Stabilize Time



ALY327
Male
Capricorn
61
41
Knight
Steal
PA Save
Magic Attack UP
Move+1

Battle Axe
Platinum Shield
Genji Helmet
Black Robe
Vanish Mantle

Head Break, Armor Break, Magic Break, Speed Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Steal Shield, Steal Weapon, Arm Aim



Fawkes
Male
Libra
73
53
Archer
Basic Skill
Parry
Equip Bow
Jump+2

Ice Bow

Barbuta
Leather Outfit
Magic Gauntlet

Charge+1, Charge+3, Charge+7, Charge+10
Accumulate, Dash, Heal, Yell, Cheer Up

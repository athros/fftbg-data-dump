Player: !Red
Team: Red Team
Palettes: Red/Brown



Krombobreaker
Female
Taurus
55
68
Dancer
White Magic
Faith Save
Defense UP
Swim

Cashmere

Leather Hat
Brigandine
Germinas Boots

Witch Hunt, Polka Polka, Last Dance, Void Storage
Cure 2, Cure 4, Raise, Raise 2, Reraise, Shell, Shell 2, Wall, Esuna



Resjudicata3
Female
Scorpio
62
74
Summoner
Elemental
Counter Tackle
Dual Wield
Ignore Terrain

Bestiary
Papyrus Codex
Feather Hat
Wizard Outfit
Small Mantle

Moogle, Shiva, Odin, Silf, Fairy
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Zagorsek
Male
Taurus
53
52
Bard
Draw Out
MA Save
Dual Wield
Move-MP Up

Fairy Harp
Bloody Strings
Red Hood
Gold Armor
N-Kai Armlet

Magic Song, Last Song, Space Storage
Koutetsu, Bizen Boat



Yangusburger
Male
Sagittarius
78
65
Mediator
Black Magic
Counter
Short Status
Fly

Bestiary

Flash Hat
Black Costume
N-Kai Armlet

Invitation, Persuade, Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon
Fire 4, Death

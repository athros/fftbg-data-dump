Final Bets: red - 21 bets for 24,436G (76.9%, x0.30); blue - 11 bets for 7,345G (23.1%, x3.33)

red bets:
toka222: 7,500G (30.7%, 400,881G)
Shalloween: 6,171G (25.3%, 12,343G)
acid_flashback: 2,008G (8.2%, 2,008G)
Raixelol: 2,000G (8.2%, 5,551G)
BirbBrainsBot: 1,000G (4.1%, 113,934G)
Lifebregin: 1,000G (4.1%, 23,246G)
Nizaha: 816G (3.3%, 78,405G)
CapnChaos12: 614G (2.5%, 614G)
letdowncity: 556G (2.3%, 45,158G)
Smugzug: 500G (2.0%, 1,123G)
getthemoneyz: 332G (1.4%, 1,812,111G)
TruthDefined: 318G (1.3%, 318G)
krombobreaker: 288G (1.2%, 8,685G)
killth3kid: 250G (1.0%, 15,194G)
lyans00: 200G (0.8%, 5,790G)
Yangusburger: 200G (0.8%, 16,321G)
datadrivenbot: 200G (0.8%, 62,826G)
acrusade: 150G (0.6%, 3,293G)
bigbongsmoker: 128G (0.5%, 1,543G)
Chuckolator: 105G (0.4%, 6,672G)
nhammen: 100G (0.4%, 419,270G)

blue bets:
rocl: 1,250G (17.0%, 1,250G)
BaronHaynes: 1,000G (13.6%, 61,546G)
E_Ballard: 884G (12.0%, 131,451G)
dogsandcatsand: 836G (11.4%, 836G)
Phi_Sig: 555G (7.6%, 44,817G)
KasugaiRoastedPeas: 504G (6.9%, 504G)
Mushufasa_: 500G (6.8%, 25,768G)
Smashy: 500G (6.8%, 9,551G)
Lord_Gwarth: 500G (6.8%, 3,134G)
Dasutin23: 432G (5.9%, 432G)
holdenmagronik: 384G (5.2%, 3,453G)

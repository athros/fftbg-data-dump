Player: !Red
Team: Red Team
Palettes: Red/Brown



Run With Stone GUNs
Male
Cancer
72
57
Mime

Mana Shield
Martial Arts
Move-MP Up



Black Hood
Chain Vest
Elf Mantle

Mimic




King Smashington
Female
Cancer
58
56
Ninja
Dance
PA Save
Equip Gun
Teleport

Bloody Strings
Ramia Harp
Feather Hat
Chain Vest
Germinas Boots

Shuriken, Knife
Last Dance, Obsidian Blade, Nether Demon



Bryon W
Male
Aquarius
45
65
Ninja
White Magic
Arrow Guard
Equip Gun
Jump+3

Bestiary
Bestiary
Red Hood
Leather Outfit
Cherche

Shuriken, Bomb, Staff, Axe, Stick, Wand
Cure, Cure 3, Raise, Raise 2, Protect 2, Shell, Wall, Esuna



Nhammen
Female
Capricorn
66
73
Geomancer
Steal
Parry
Concentrate
Move+3

Giant Axe
Round Shield
Leather Hat
Linen Robe
108 Gems

Pitfall, Hell Ivy, Local Quake, Quicksand, Blizzard, Lava Ball
Steal Accessory

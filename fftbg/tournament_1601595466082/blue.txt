Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kellios11
Male
Pisces
65
43
Mediator
Steal
Regenerator
Equip Bow
Teleport

Silver Bow

Twist Headband
Silk Robe
Small Mantle

Invitation, Preach, Negotiate, Mimic Daravon, Rehabilitate
Steal Helmet, Steal Shield, Leg Aim



Run With Stone GUNs
Male
Sagittarius
53
74
Samurai
Steal
Caution
Equip Knife
Lava Walking

Short Edge

Cross Helmet
Leather Armor
Germinas Boots

Bizen Boat, Kikuichimoji
Steal Helmet, Steal Status



Powergems
Female
Scorpio
70
66
Wizard
Math Skill
Arrow Guard
Dual Wield
Fly

Poison Rod
Thunder Rod
Flash Hat
Light Robe
Feather Boots

Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 4, Frog
CT, 5, 3



Arcblazer23
Female
Cancer
54
47
Summoner
Steal
Distribute
Equip Knife
Jump+3

Cultist Dagger

Leather Hat
Adaman Vest
Defense Armlet

Moogle, Shiva, Ifrit, Carbunkle, Silf
Gil Taking, Steal Helmet, Leg Aim

Player: !Red
Team: Red Team
Palettes: Red/Brown



MonkMagnus
Female
Virgo
77
51
Summoner
Throw
Counter
Sicken
Teleport

Flame Rod

Twist Headband
Rubber Costume
Jade Armlet

Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Salamander, Fairy
Bomb



Gorgewall
Female
Sagittarius
70
61
Archer
Black Magic
Auto Potion
Secret Hunt
Ignore Terrain

Snipe Bow
Gold Shield
Black Hood
Earth Clothes
Power Wrist

Charge+4, Charge+5, Charge+20
Ice 3, Frog



HASTERIOUS
Female
Capricorn
72
80
Wizard
Draw Out
Brave Save
Short Charge
Lava Walking

Ice Rod

Headgear
Chameleon Robe
Genji Gauntlet

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 3, Frog
Koutetsu, Bizen Boat



Chuckolator
Female
Aries
62
47
Archer
Steal
Counter Magic
Equip Polearm
Move+2

Cashmere
Round Shield
Grand Helmet
Power Sleeve
Power Wrist

Charge+2, Charge+3, Charge+5, Charge+20
Steal Shield, Steal Accessory

Final Bets: white - 7 bets for 3,907G (59.5%, x0.68); black - 5 bets for 2,658G (40.5%, x1.47)

white bets:
VolgraTheMoose: 2,001G (51.2%, 24,327G)
DHaveWord: 722G (18.5%, 1,444G)
SephDarkheart: 412G (10.5%, 3,944G)
bigbongsmoker: 352G (9.0%, 1,885G)
datadrivenbot: 200G (5.1%, 76,837G)
Lolthsmeat: 116G (3.0%, 116G)
JermZzZzZz: 104G (2.7%, 104G)

black bets:
BirbBrainsBot: 1,000G (37.6%, 173,829G)
Evewho: 500G (18.8%, 1,893G)
getthemoneyz: 486G (18.3%, 2,084,933G)
AllInBot: 472G (17.8%, 472G)
MemoriesofFinal: 200G (7.5%, 736G)

Final Bets: green - 8 bets for 5,862G (57.6%, x0.74); yellow - 7 bets for 4,310G (42.4%, x1.36)

green bets:
resjudicata3: 3,117G (53.2%, 6,112G)
Zachara: 1,361G (23.2%, 151,861G)
SephDarkheart: 412G (7.0%, 4,356G)
Lolthsmeat: 263G (4.5%, 263G)
ChrisWado: 209G (3.6%, 209G)
datadrivenbot: 200G (3.4%, 77,037G)
Evewho: 200G (3.4%, 2,093G)
leaferrm: 100G (1.7%, 2,703G)

yellow bets:
VolgraTheMoose: 1,001G (23.2%, 22,966G)
BirbBrainsBot: 1,000G (23.2%, 172,469G)
Rytor: 767G (17.8%, 1,535G)
DHaveWord: 612G (14.2%, 612G)
getthemoneyz: 418G (9.7%, 2,084,364G)
MemoriesofFinal: 312G (7.2%, 312G)
AllInBot: 200G (4.6%, 200G)

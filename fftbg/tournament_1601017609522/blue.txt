Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Squidestsquid
Female
Cancer
46
40
Lancer
Talk Skill
Arrow Guard
Maintenance
Ignore Height

Iron Fan
Bronze Shield
Barbuta
Plate Mail
Magic Gauntlet

Level Jump4, Vertical Jump5
Invitation, Praise, Death Sentence, Refute



Go2sleepTV
Male
Taurus
64
42
Knight
Punch Art
Auto Potion
Dual Wield
Jump+1

Blood Sword
Diamond Sword
Gold Helmet
Diamond Armor
Feather Mantle

Armor Break, Justice Sword, Dark Sword
Earth Slash, Purification, Revive



Mirapoix
Female
Sagittarius
42
61
Calculator
Dungeon Skill
Sunken State
Equip Armor
Move+2

Battle Folio

Bronze Helmet
Clothes
Defense Ring

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



Killabandaid
Male
Libra
52
48
Monk
Battle Skill
Meatbone Slash
Magic Defense UP
Move-HP Up



Green Beret
Wizard Outfit
Battle Boots

Spin Fist, Purification, Chakra, Revive
Magic Break, Speed Break

Player: !Green
Team: Green Team
Palettes: Green/White



XerxesMindbreaker
Male
Cancer
46
57
Mime

Abandon
Magic Attack UP
Lava Walking



Leather Hat
Power Sleeve
Red Shoes

Mimic




MemoriesofFinal
Male
Gemini
76
70
Mime

Faith Save
Equip Armor
Ignore Height



Crystal Helmet
Judo Outfit
Bracer

Mimic




TheSabretoothe
Female
Aries
59
71
Mime

Sunken State
Martial Arts
Move-MP Up


Buckler
Flash Hat
Judo Outfit
Rubber Shoes

Mimic




Ghazzzzzz
Female
Sagittarius
53
45
Ninja
Black Magic
Faith Save
Magic Defense UP
Move-HP Up

Koga Knife
Kunai
Feather Hat
Mythril Vest
Dracula Mantle

Bomb, Staff
Bolt, Ice 3, Ice 4

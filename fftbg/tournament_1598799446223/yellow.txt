Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Actual JP
Male
Cancer
46
67
Calculator
White Magic
Hamedo
Equip Armor
Jump+1

Bestiary

Crystal Helmet
Wizard Outfit
Angel Ring

CT, 5, 3
Raise, Protect 2, Shell, Shell 2, Wall, Esuna, Holy



MagikarpPKMN
Monster
Leo
64
42
Tiamat










Phi Sig
Male
Scorpio
45
74
Knight
White Magic
Arrow Guard
Martial Arts
Fly

Save the Queen
Platinum Shield
Genji Helmet
Bronze Armor
Wizard Mantle

Weapon Break, Magic Break, Mind Break, Stasis Sword
Cure, Cure 2, Cure 3, Raise 2, Esuna, Magic Barrier



LeoNightFury
Male
Capricorn
44
59
Chemist
Summon Magic
Speed Save
Secret Hunt
Levitate

Star Bag

Leather Hat
Power Sleeve
Sprint Shoes

Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Moogle, Shiva, Ramuh, Ifrit

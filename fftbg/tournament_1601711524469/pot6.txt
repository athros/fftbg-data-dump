Final Bets: black - 12 bets for 5,372G (55.4%, x0.80); purple - 7 bets for 4,321G (44.6%, x1.24)

black bets:
CorpusCav: 1,211G (22.5%, 36,631G)
BirbBrainsBot: 1,000G (18.6%, 2,578G)
WireLord: 922G (17.2%, 922G)
NovaKnight21: 900G (16.8%, 900G)
TheBrett: 268G (5.0%, 5,732G)
Aaron2Nerdy: 212G (3.9%, 212G)
AllInBot: 200G (3.7%, 200G)
datadrivenbot: 200G (3.7%, 82,210G)
Sharosa: 164G (3.1%, 164G)
elimit404: 124G (2.3%, 124G)
gorgewall: 101G (1.9%, 7,271G)
getthemoneyz: 70G (1.3%, 2,141,277G)

purple bets:
bigbongsmoker: 1,498G (34.7%, 2,996G)
Willjin: 600G (13.9%, 33,863G)
resjudicata3: 556G (12.9%, 556G)
Mudrockk: 500G (11.6%, 21,944G)
ColetteMSLP: 492G (11.4%, 13,687G)
Deeps_one: 400G (9.3%, 4,020G)
IaibOaob: 275G (6.4%, 22,059G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DuneMeta
Male
Virgo
60
75
Wizard
Elemental
HP Restore
Halve MP
Lava Walking

Dragon Rod

Triangle Hat
Linen Robe
Small Mantle

Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Empower
Water Ball, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind, Lava Ball



ASimpleSpaceBee
Female
Cancer
47
50
Mime

Arrow Guard
Sicken
Jump+1



Red Hood
Clothes
Red Shoes

Mimic




Victoriolue
Female
Pisces
45
47
Summoner
White Magic
Brave Save
Short Charge
Swim

Poison Rod

Holy Miter
Black Robe
Leather Mantle

Moogle, Titan, Carbunkle, Leviathan, Salamander, Silf, Cyclops
Cure, Raise, Shell 2, Esuna



Seaweed B
Female
Scorpio
69
58
Geomancer
Draw Out
Caution
Short Charge
Jump+2

Giant Axe
Mythril Shield
Flash Hat
Judo Outfit
Red Shoes

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Koutetsu

Final Bets: black - 15 bets for 96,406G (66.4%, x0.50); brown - 6 bets for 48,675G (33.6%, x1.98)

black bets:
blinkmachine: 26,756G (27.8%, 26,756G)
Forkmore: 22,222G (23.1%, 81,135G)
Leonidusx: 20,000G (20.7%, 135,571G)
NicoSavoy: 20,000G (20.7%, 107,032G)
OneHundredFists: 2,078G (2.2%, 2,078G)
MemoriesofFinal: 1,000G (1.0%, 29,835G)
BirbBrainsBot: 1,000G (1.0%, 185,917G)
DeathTaxesAndAnime: 820G (0.9%, 820G)
CorpusCav: 800G (0.8%, 7,200G)
lijarkh: 584G (0.6%, 584G)
DHaveWord: 416G (0.4%, 12,762G)
ayeayex3: 300G (0.3%, 1,114G)
datadrivenbot: 200G (0.2%, 70,735G)
getthemoneyz: 130G (0.1%, 2,021,435G)
gilgamensche: 100G (0.1%, 116G)

brown bets:
AllInBot: 46,992G (96.5%, 46,992G)
Raixelol: 600G (1.2%, 9,465G)
thunderducker: 455G (0.9%, 4,326G)
soren_of_tyto: 420G (0.9%, 420G)
genkidou: 108G (0.2%, 108G)
otakutaylor: 100G (0.2%, 1,501G)

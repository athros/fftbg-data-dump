Player: !Black
Team: Black Team
Palettes: Black/Red



GrayGhostGaming
Female
Sagittarius
38
64
Chemist
Elemental
MA Save
Equip Bow
Move-MP Up

Ultimus Bow

Red Hood
Mythril Vest
Defense Ring

Potion, Eye Drop, Soft, Holy Water, Phoenix Down
Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Lava Ball



Aldrammech
Male
Leo
69
52
Summoner
Basic Skill
Parry
Concentrate
Waterwalking

Gold Staff

Feather Hat
Robe of Lords
Reflect Ring

Moogle, Titan, Golem, Carbunkle, Odin, Leviathan, Silf, Lich
Heal, Tickle, Yell, Fury, Scream



Omegasuspekt
Male
Taurus
76
57
Chemist
Summon Magic
Brave Save
Concentrate
Jump+2

Main Gauche

Holy Miter
Mythril Vest
108 Gems

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Silf, Lich, Cyclops



DeathTaxesAndAnime
Female
Pisces
63
65
Mediator
Jump
Damage Split
Equip Shield
Move+3

Blaze Gun
Kaiser Plate
Leather Hat
Mythril Vest
Power Wrist

Persuade, Praise, Preach
Level Jump3, Vertical Jump8

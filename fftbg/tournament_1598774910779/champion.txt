Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Sairentozon7
Male
Aries
71
51
Oracle
Draw Out
MP Restore
Equip Shield
Fly

Octagon Rod
Crystal Shield
Leather Hat
Brigandine
Elf Mantle

Blind, Pray Faith, Zombie, Foxbird, Dispel Magic, Paralyze, Sleep
Koutetsu, Murasame, Heaven's Cloud



Krombobreaker
Male
Serpentarius
40
70
Time Mage
Draw Out
Absorb Used MP
Magic Defense UP
Move-HP Up

Rainbow Staff

Thief Hat
Judo Outfit
Elf Mantle

Haste, Stop, Quick, Demi, Demi 2, Stabilize Time
Bizen Boat, Kiyomori, Kikuichimoji



Starfire6789
Male
Taurus
70
67
Summoner
Time Magic
Sunken State
Short Charge
Move-MP Up

Wizard Rod

Cachusha
Silk Robe
Spike Shoes

Shiva, Titan, Bahamut, Odin
Haste, Haste 2, Slow, Slow 2, Stop, Reflect, Quick, Demi, Stabilize Time



RobotOcelot
Male
Gemini
72
59
Geomancer
Summon Magic
Mana Shield
Secret Hunt
Move-MP Up

Slasher
Escutcheon
Holy Miter
Mystic Vest
Setiemson

Pitfall, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Silf

Player: !Black
Team: Black Team
Palettes: Black/Red



Go2sleepTV
Female
Capricorn
48
76
Monk
Black Magic
Arrow Guard
Magic Defense UP
Lava Walking



Barette
Adaman Vest
Feather Boots

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Fire 2, Fire 4, Bolt 2, Bolt 4, Ice, Ice 3, Death



TeaTime29
Female
Libra
61
55
Knight
Jump
Dragon Spirit
Maintenance
Move+2

Coral Sword
Aegis Shield
Cross Helmet
Genji Armor
Small Mantle

Shield Break, Magic Break
Level Jump8, Vertical Jump7



Gorgewall
Female
Libra
50
42
Dancer
Draw Out
Counter
Equip Gun
Lava Walking

Romanda Gun

Black Hood
Black Robe
Feather Boots

Slow Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon
Asura, Koutetsu



Lowlf
Male
Capricorn
43
71
Priest
Black Magic
Critical Quick
Equip Gun
Swim

Madlemgen

Red Hood
Adaman Vest
Angel Ring

Cure, Raise 2, Esuna, Holy
Bolt 3, Ice, Ice 3, Flare

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lythe Caraker
Male
Aquarius
78
55
Knight
Sing
HP Restore
Equip Sword
Move-HP Up

Murasame
Gold Shield
Circlet
Linen Cuirass
Elf Mantle

Head Break, Armor Break, Shield Break, Speed Break, Power Break
Last Song, Diamond Blade, Sky Demon



Mirapoix
Male
Aries
75
79
Mime

Distribute
Sicken
Teleport



Leather Hat
Power Sleeve
Genji Gauntlet

Mimic




Daveb
Female
Pisces
60
66
Ninja
Charge
Damage Split
Equip Knife
Jump+1

Short Edge
Air Knife
Holy Miter
Judo Outfit
Dracula Mantle

Shuriken, Bomb, Knife
Charge+4, Charge+5, Charge+20



Jeeboheebo
Female
Gemini
55
63
Lancer
Elemental
Brave Save
Doublehand
Swim

Partisan

Genji Helmet
Wizard Robe
Jade Armlet

Level Jump4, Vertical Jump2
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

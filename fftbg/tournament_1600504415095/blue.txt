Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HASTERIOUS
Female
Scorpio
74
54
Time Mage
Draw Out
Counter
Halve MP
Jump+1

White Staff

Flash Hat
Wizard Robe
Magic Gauntlet

Slow, Slow 2, Stop, Float, Demi 2, Stabilize Time
Kikuichimoji



Sweetsga III
Monster
Virgo
45
64
Skeleton










Go2sleepTV
Female
Taurus
63
45
Samurai
Dance
Dragon Spirit
Equip Axe
Jump+3

Flame Whip

Circlet
Bronze Armor
Sprint Shoes

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Obsidian Blade, Void Storage



Chuckolator
Female
Capricorn
60
68
Calculator
Time Magic
Counter
Equip Knife
Move-HP Up

Spell Edge

Holy Miter
Leather Outfit
Cherche

CT, Height, Prime Number, 5, 3
Immobilize, Stabilize Time

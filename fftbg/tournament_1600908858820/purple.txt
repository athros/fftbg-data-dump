Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Fenaen
Male
Scorpio
78
47
Samurai
White Magic
Earplug
Halve MP
Levitate

Mythril Spear

Mythril Helmet
Carabini Mail
108 Gems

Koutetsu, Heaven's Cloud, Masamune
Raise, Reraise, Esuna



HASTERIOUS
Female
Libra
75
54
Geomancer
Draw Out
Counter Flood
Secret Hunt
Move+1

Giant Axe
Bronze Shield
Red Hood
Brigandine
Red Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Heaven's Cloud



ItsHyrax
Male
Aries
62
68
Samurai
White Magic
Parry
Equip Sword
Levitate

Rune Blade

Cross Helmet
Mythril Armor
Magic Ring

Asura, Koutetsu, Kiyomori
Cure 2, Cure 3, Raise, Reraise, Protect



Rikrizen
Female
Leo
51
45
Priest
Item
Brave Save
Short Charge
Teleport

Rainbow Staff

Feather Hat
Silk Robe
Cursed Ring

Cure, Cure 2, Raise, Raise 2, Regen, Shell, Wall, Esuna
Potion, Eye Drop, Soft, Holy Water

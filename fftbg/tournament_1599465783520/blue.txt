Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Firesheath
Male
Scorpio
73
65
Priest
Black Magic
Caution
Short Charge
Jump+3

Morning Star

Headgear
White Robe
108 Gems

Cure, Cure 3, Protect, Protect 2, Shell, Wall, Esuna
Fire 2, Fire 3, Bolt 2, Ice 2



NicoSavoy
Female
Aries
58
70
Archer
Basic Skill
MP Restore
Magic Attack UP
Move-HP Up

Ultimus Bow

Black Hood
Wizard Outfit
Dracula Mantle

Charge+1, Charge+10
Accumulate, Dash



StealthModeLocke
Female
Aries
76
45
Archer
Summon Magic
Abandon
Equip Bow
Jump+3

Ice Bow

Flash Hat
Leather Outfit
Genji Gauntlet

Charge+2, Charge+4, Charge+7, Charge+10
Moogle, Odin, Leviathan, Silf, Fairy, Cyclops



Khelor
Male
Virgo
77
55
Mediator
Charge
Distribute
Maintenance
Swim

Glacier Gun

Holy Miter
Light Robe
Magic Gauntlet

Praise, Solution, Insult, Negotiate, Mimic Daravon
Charge+1, Charge+5, Charge+10

Final Bets: red - 9 bets for 5,446G (56.9%, x0.76); black - 8 bets for 4,129G (43.1%, x1.32)

red bets:
Greggernaut: 2,000G (36.7%, 37,215G)
BirbBrainsBot: 1,000G (18.4%, 72,930G)
Sietaha: 562G (10.3%, 562G)
BartTradingCompany: 444G (8.2%, 4,440G)
ValensEXP_: 389G (7.1%, 389G)
bigbongsmoker: 380G (7.0%, 380G)
douchetron: 292G (5.4%, 292G)
getthemoneyz: 206G (3.8%, 1,864,020G)
velvet_muffins: 173G (3.2%, 173G)

black bets:
brodeity: 2,000G (48.4%, 30,191G)
CassiePhoenix: 789G (19.1%, 789G)
Raixelol: 540G (13.1%, 6,831G)
CorpusCav: 200G (4.8%, 27,826G)
khelor_: 200G (4.8%, 4,322G)
datadrivenbot: 200G (4.8%, 56,570G)
AllInBot: 100G (2.4%, 100G)
DeathTaxesAndAnime: 100G (2.4%, 6,233G)

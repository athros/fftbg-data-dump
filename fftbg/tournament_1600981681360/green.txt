Player: !Green
Team: Green Team
Palettes: Green/White



Sairentozon7
Male
Capricorn
56
47
Mediator
Yin Yang Magic
HP Restore
Dual Wield
Teleport

Bestiary
Papyrus Codex
Flash Hat
Light Robe
Germinas Boots

Praise, Threaten, Insult, Negotiate, Rehabilitate
Poison, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Sleep



Mesmaster
Male
Leo
42
52
Samurai
Charge
MA Save
Magic Defense UP
Jump+1

Spear

Platinum Helmet
Chameleon Robe
Genji Gauntlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Charge+1, Charge+2, Charge+7



3ngag3
Male
Taurus
76
43
Archer
Punch Art
Distribute
Dual Wield
Move-MP Up

Romanda Gun
Blaze Gun
Ribbon
Chain Vest
Defense Armlet

Charge+7
Spin Fist, Pummel, Earth Slash, Purification, Revive, Seal Evil



HaateXIII
Female
Scorpio
77
60
Geomancer
Draw Out
Regenerator
Equip Polearm
Ignore Height

Dragon Whisker
Platinum Shield
Green Beret
Chameleon Robe
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm
Murasame, Muramasa

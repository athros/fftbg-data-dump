Player: !zChamp
Team: Champion Team
Palettes: Green/White



MemoriesofFinal
Female
Aquarius
52
72
Squire
Steal
HP Restore
Equip Bow
Move-MP Up

Snipe Bow
Hero Shield
Twist Headband
Adaman Vest
Vanish Mantle

Accumulate, Tickle, Cheer Up
Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Upvla
Male
Leo
65
75
Ninja
Basic Skill
Brave Save
Attack UP
Move+2

Flail
Kunai
Triangle Hat
Secret Clothes
Spike Shoes

Shuriken
Accumulate, Dash, Cheer Up, Fury



OneHundredFists
Male
Aries
65
59
Archer
Sing
Caution
Doublehand
Move-MP Up

Lightning Bow

Feather Hat
Brigandine
Sprint Shoes

Charge+1, Charge+3, Charge+4, Charge+10, Charge+20
Angel Song, Magic Song, Nameless Song, Sky Demon, Hydra Pit



Higgins
Male
Scorpio
73
67
Monk
Steal
Absorb Used MP
Concentrate
Move+2



Green Beret
Judo Outfit
Bracer

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Steal Helmet, Steal Accessory, Leg Aim

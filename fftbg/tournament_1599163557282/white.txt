Player: !White
Team: White Team
Palettes: White/Blue



Formerlydrdong
Female
Leo
63
46
Dancer
Jump
PA Save
Short Charge
Ignore Height

Cashmere

Green Beret
Wizard Robe
Defense Armlet

Polka Polka, Disillusion
Level Jump8, Vertical Jump7



Smashy
Female
Capricorn
57
49
Dancer
Black Magic
HP Restore
Dual Wield
Jump+1

Cute Bag
Cute Bag
Green Beret
Light Robe
Magic Gauntlet

Polka Polka, Disillusion, Last Dance
Bolt, Ice, Ice 4



Aldrammech
Female
Pisces
55
44
Squire
Black Magic
Critical Quick
Short Status
Jump+1

Night Killer
Platinum Shield
Black Hood
Mythril Vest
Dracula Mantle

Accumulate, Heal, Yell, Fury
Bolt 2, Ice, Ice 2, Ice 4



JumbocactuarX27
Male
Gemini
58
72
Squire
Punch Art
Critical Quick
Equip Gun
Move+1

Glacier Gun
Diamond Shield
Twist Headband
Genji Armor
Genji Gauntlet

Dash, Heal, Yell, Wish
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil

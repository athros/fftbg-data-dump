Player: !Black
Team: Black Team
Palettes: Black/Red



Galkife
Female
Capricorn
72
39
Thief
Item
Sunken State
Equip Polearm
Move+3

Javelin

Flash Hat
Earth Clothes
Salty Rage

Gil Taking, Steal Heart, Steal Shield, Arm Aim
Potion, Antidote, Echo Grass, Holy Water, Phoenix Down



Old Overholt
Male
Aquarius
52
69
Monk
Battle Skill
Brave Save
Concentrate
Waterbreathing

Panther Bag

Green Beret
Chain Vest
Angel Ring

Pummel, Purification, Revive
Shield Break, Speed Break, Power Break, Mind Break, Night Sword



CosmicTactician
Male
Gemini
55
63
Chemist
Talk Skill
Arrow Guard
Attack UP
Jump+3

Dagger

Black Hood
Earth Clothes
Feather Mantle

Potion, Eye Drop, Remedy, Phoenix Down
Invitation, Persuade, Praise, Preach, Death Sentence, Mimic Daravon, Refute



Lokenwow
Monster
Capricorn
81
71
Plague








Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



VolgraTheMoose
Male
Pisces
75
52
Knight
Jump
Counter Magic
Equip Sword
Move+3

Chirijiraden
Genji Shield
Circlet
Silk Robe
Reflect Ring

Head Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Dark Sword, Surging Sword
Level Jump5, Vertical Jump7



Chuckolator
Male
Taurus
73
51
Samurai
Time Magic
Abandon
Equip Armor
Waterwalking

Murasame

Red Hood
Mythril Vest
Genji Gauntlet

Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa, Chirijiraden
Haste, Demi, Stabilize Time, Meteor



Sinnyil2
Male
Scorpio
63
74
Geomancer
Time Magic
Distribute
Doublehand
Move-HP Up

Coral Sword

Thief Hat
White Robe
Chantage

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Haste, Float, Reflect, Demi 2, Stabilize Time



Joewcarson
Male
Scorpio
77
66
Samurai
Black Magic
Critical Quick
Maintenance
Move+1

Kikuichimoji

Diamond Helmet
Chain Mail
Angel Ring

Asura, Koutetsu, Kiyomori, Masamune
Fire, Fire 4, Bolt, Bolt 3, Ice 2, Death, Flare

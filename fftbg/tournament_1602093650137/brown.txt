Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeniorBunk
Male
Taurus
75
78
Summoner
Jump
Critical Quick
Short Charge
Jump+2

Wizard Rod

Holy Miter
Light Robe
Sprint Shoes

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Salamander, Silf, Fairy
Level Jump2, Vertical Jump8



Reddwind
Male
Scorpio
75
60
Knight
Basic Skill
Auto Potion
Secret Hunt
Jump+3

Ice Brand
Bronze Shield
Cross Helmet
White Robe
Bracer

Head Break, Stasis Sword, Justice Sword
Accumulate, Dash, Heal



Khelor
Female
Sagittarius
74
73
Archer
Battle Skill
Critical Quick
Equip Gun
Waterbreathing

Papyrus Codex
Flame Shield
Cross Helmet
Mythril Vest
Salty Rage

Charge+1, Charge+4, Charge+5
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Surging Sword



Sairentozon7
Male
Serpentarius
65
79
Bard
Basic Skill
Parry
Defense UP
Ignore Terrain

Ramia Harp

Holy Miter
Bronze Armor
Bracer

Angel Song
Heal, Tickle, Yell, Wish

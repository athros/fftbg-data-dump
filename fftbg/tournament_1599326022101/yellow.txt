Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Technominari
Female
Aries
47
71
Priest
Basic Skill
Sunken State
Short Charge
Move-MP Up

Morning Star

Leather Hat
Light Robe
Rubber Shoes

Cure, Cure 3, Cure 4, Raise, Regen, Shell 2, Wall, Esuna
Accumulate, Dash, Throw Stone, Heal, Yell



Arumz
Male
Scorpio
50
44
Calculator
Black Magic
Counter
Concentrate
Waterbreathing

Battle Folio

Headgear
Judo Outfit
N-Kai Armlet

CT, Height, 3
Fire, Ice 3



Mirapoix
Male
Sagittarius
70
75
Priest
Yin Yang Magic
Damage Split
Martial Arts
Fly



Golden Hairpin
Wizard Robe
Sprint Shoes

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Shell, Shell 2, Holy
Poison, Spell Absorb, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep



Run With Stone GUNs
Male
Pisces
72
58
Samurai
Basic Skill
HP Restore
Beastmaster
Levitate

Partisan

Platinum Helmet
Leather Armor
Magic Gauntlet

Bizen Boat, Murasame, Kikuichimoji
Dash, Heal, Tickle

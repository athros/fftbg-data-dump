Player: !Green
Team: Green Team
Palettes: Green/White



Brokenknight201
Female
Pisces
67
64
Calculator
Gore Skill
Auto Potion
Martial Arts
Ignore Terrain


Diamond Shield
Crystal Helmet
Reflect Mail
Cursed Ring

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Nizaha
Female
Aquarius
45
70
Calculator
Foul Skill
Meatbone Slash
Secret Hunt
Move-MP Up

Bestiary

Platinum Helmet
Crystal Mail
Sprint Shoes

Blue Magic
Straight Dash, Oink, Toot, Snort, Bequeath Bacon, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



Phi Sig
Male
Capricorn
74
45
Oracle
Throw
PA Save
Doublehand
Teleport

Battle Bamboo

Triangle Hat
Black Costume
Magic Gauntlet

Doubt Faith, Silence Song, Dispel Magic, Sleep, Dark Holy
Shuriken, Knife, Staff, Wand



CorpusCav
Female
Leo
59
47
Chemist
Dance
Auto Potion
Equip Bow
Retreat

Mythril Bow

Triangle Hat
Adaman Vest
Salty Rage

Hi-Potion, Ether, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Wiznaibus, Polka Polka, Disillusion

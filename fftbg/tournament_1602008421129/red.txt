Player: !Red
Team: Red Team
Palettes: Red/Brown



Chihuahua Charity
Male
Aries
62
43
Calculator
Mighty Sword
Meatbone Slash
Dual Wield
Swim

Battle Bamboo
Musk Rod
Thief Hat
Power Sleeve
Sprint Shoes

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



DavenIII
Female
Libra
76
74
Summoner
White Magic
Critical Quick
Short Charge
Jump+1

Flame Rod

Triangle Hat
Light Robe
Salty Rage

Moogle, Golem, Odin, Fairy
Cure 2, Raise, Protect 2, Shell, Shell 2, Esuna



LDSkinny
Female
Pisces
77
67
Oracle
Jump
Counter Tackle
Short Charge
Levitate

Octagon Rod

Headgear
Rubber Costume
Sprint Shoes

Blind, Pray Faith, Zombie, Dispel Magic
Level Jump8, Vertical Jump7



Thyrandaal
Male
Virgo
67
46
Knight
Summon Magic
Counter
Equip Bow
Teleport

Night Killer
Hero Shield
Grand Helmet
Robe of Lords
Sprint Shoes

Armor Break, Shield Break, Stasis Sword, Night Sword
Ramuh, Ifrit, Titan, Bahamut, Odin, Silf, Fairy, Lich

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Moonliquor
Female
Scorpio
65
52
Samurai
Black Magic
MP Restore
Short Charge
Swim

Bizen Boat

Gold Helmet
Genji Armor
Sprint Shoes

Asura, Koutetsu, Murasame, Masamune
Fire 2, Bolt 2, Ice 2, Ice 3, Ice 4, Flare



AltimaMantoid
Female
Aries
55
71
Calculator
White Magic
Counter
Defense UP
Levitate

Papyrus Codex

Flash Hat
Light Robe
Magic Gauntlet

CT, Height, Prime Number, 5
Cure 3, Protect 2, Shell, Holy



Just Here2
Monster
Virgo
67
47
Black Chocobo










Latebit
Male
Aries
46
71
Archer
Battle Skill
Counter Tackle
Beastmaster
Retreat

Mythril Gun
Ice Shield
Leather Helmet
Adaman Vest
Magic Gauntlet

Charge+1, Charge+2, Charge+4, Charge+7, Charge+20
Shield Break, Speed Break, Justice Sword

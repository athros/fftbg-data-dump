Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TurtleFactz
Monster
Cancer
53
72
Black Chocobo










Galkife
Male
Scorpio
44
41
Oracle
Item
Counter Magic
Magic Attack UP
Lava Walking

Gokuu Rod

Triangle Hat
Black Robe
Diamond Armlet

Life Drain, Doubt Faith, Blind Rage, Foxbird
Potion, Soft, Phoenix Down



Cryptopsy70
Female
Libra
51
53
Squire
Elemental
Regenerator
Equip Knife
Swim

Short Edge
Genji Shield
Barbuta
Crystal Mail
Feather Mantle

Throw Stone, Heal, Cheer Up
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball



Phi Sig
Male
Cancer
66
45
Chemist
Throw
Mana Shield
Equip Bow
Waterbreathing

Lightning Bow

Headgear
Brigandine
Power Wrist

X-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Bomb, Staff

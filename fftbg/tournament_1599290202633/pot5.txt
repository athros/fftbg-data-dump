Final Bets: red - 5 bets for 2,538G (20.0%, x3.99); green - 10 bets for 10,129G (80.0%, x0.25)

red bets:
BartTradingCompany: 894G (35.2%, 8,941G)
AltimaMantoid: 686G (27.0%, 1,986G)
thunderducker: 444G (17.5%, 2,432G)
KasugaiRoastedPeas: 300G (11.8%, 5,600G)
getthemoneyz: 214G (8.4%, 1,819,471G)

green bets:
SkylerBunny: 3,000G (29.6%, 56,787G)
NicoSavoy: 2,030G (20.0%, 2,030G)
Snowfats: 1,698G (16.8%, 1,698G)
BirbBrainsBot: 1,000G (9.9%, 150,412G)
Leonidusx: 879G (8.7%, 879G)
Raixelol: 504G (5.0%, 11,019G)
resjudicata3: 468G (4.6%, 468G)
PuzzleSecretary: 250G (2.5%, 452G)
datadrivenbot: 200G (2.0%, 58,528G)
DeathTaxesAndAnime: 100G (1.0%, 7,024G)

Player: !Green
Team: Green Team
Palettes: Green/White



Hotpocketsofficial
Male
Virgo
41
76
Archer
Yin Yang Magic
Abandon
Equip Gun
Retreat

Madlemgen
Aegis Shield
Black Hood
Power Sleeve
Battle Boots

Charge+4, Charge+7
Life Drain, Silence Song, Confusion Song, Dispel Magic, Sleep



Ruleof5
Female
Pisces
69
56
Archer
Black Magic
Blade Grasp
Dual Wield
Swim

Glacier Gun
Blast Gun
Green Beret
Brigandine
Cursed Ring

Charge+1, Charge+10, Charge+20
Fire, Bolt 3, Bolt 4, Ice 2, Ice 4, Frog, Flare



Mesmaster
Female
Leo
61
41
Samurai
Time Magic
Speed Save
Magic Attack UP
Swim

Chirijiraden

Iron Helmet
Plate Mail
Rubber Shoes

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Haste, Quick, Demi, Stabilize Time



RNdOrchestra
Female
Libra
67
39
Thief
Basic Skill
Brave Save
Magic Defense UP
Move+3

Orichalcum

Red Hood
Leather Outfit
Magic Ring

Gil Taking, Steal Heart, Steal Armor
Heal, Fury, Wish

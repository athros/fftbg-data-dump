Player: !Black
Team: Black Team
Palettes: Black/Red



Xeiphyer
Female
Leo
56
77
Knight
Basic Skill
Arrow Guard
Magic Defense UP
Waterbreathing

Save the Queen
Bronze Shield
Diamond Helmet
Plate Mail
Magic Ring

Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Dark Sword
Accumulate, Throw Stone, Heal, Yell



Musashi45
Female
Libra
54
41
Archer
Dance
Faith Save
Sicken
Retreat

Ice Bow

Headgear
Brigandine
Reflect Ring

Charge+4
Slow Dance, Polka Polka, Nameless Dance, Nether Demon



Escobro
Female
Scorpio
41
71
Lancer
Steal
Counter
Secret Hunt
Swim

Ivory Rod
Ice Shield
Platinum Helmet
Diamond Armor
N-Kai Armlet

Level Jump4, Vertical Jump7
Steal Heart, Steal Shield, Steal Accessory, Leg Aim



Technominari
Female
Aries
64
68
Priest
Basic Skill
Faith Save
Short Status
Lava Walking

Flail

Headgear
Linen Robe
108 Gems

Cure 3, Reraise, Regen, Esuna
Accumulate, Throw Stone, Heal, Yell, Fury

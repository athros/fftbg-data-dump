Player: !Green
Team: Green Team
Palettes: Green/White



SQUiDSQUARKLIN
Female
Libra
73
46
Mime

HP Restore
Magic Attack UP
Move+3



Cachusha
White Robe
Reflect Ring

Mimic




Chuckolator
Monster
Libra
50
75
Apanda










Zeroroute
Male
Capricorn
70
41
Archer
Steal
Parry
Halve MP
Move+3

Mythril Bow

Black Hood
Power Sleeve
Bracer

Charge+20
Steal Shield



King Smashington
Male
Aries
50
75
Ninja
Elemental
Counter Magic
Equip Shield
Fly

Kunai
Flame Shield
Leather Hat
Mythril Vest
Feather Mantle

Shuriken, Hammer
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

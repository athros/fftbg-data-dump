Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Musashi45
Male
Cancer
54
52
Priest
Item
Hamedo
Secret Hunt
Teleport 2

Gold Staff

Red Hood
Earth Clothes
108 Gems

Cure, Cure 2, Cure 4, Raise, Shell, Wall
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down



Just Here2
Female
Cancer
79
59
Dancer
Basic Skill
Damage Split
Short Charge
Jump+1

Cashmere

Twist Headband
Chameleon Robe
Feather Mantle

Witch Hunt, Polka Polka, Disillusion, Last Dance
Dash, Throw Stone, Heal, Cheer Up, Fury, Wish, Scream



Willjin
Male
Scorpio
63
76
Chemist
Sing
Counter Flood
Equip Knife
Fly

Ninja Edge

Flash Hat
Chain Vest
Feather Mantle

Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Phoenix Down
Angel Song, Life Song, Last Song



HASTERIOUS
Male
Aquarius
73
71
Lancer
Elemental
Dragon Spirit
Equip Armor
Fly

Javelin
Ice Shield
Holy Miter
Light Robe
Bracer

Level Jump3, Vertical Jump4
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

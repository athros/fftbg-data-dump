Player: !Red
Team: Red Team
Palettes: Red/Brown



Zachara
Female
Cancer
57
45
Mime

Auto Potion
Equip Armor
Move-MP Up



Bronze Helmet
Plate Mail
Power Wrist

Mimic




HASTERIOUS
Male
Gemini
51
76
Geomancer
Summon Magic
Counter Tackle
Attack UP
Move-MP Up

Long Sword
Diamond Shield
Barette
Chameleon Robe
Salty Rage

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shiva, Ramuh, Ifrit, Golem, Silf, Cyclops



Thunderducker
Monster
Taurus
52
44
Ultima Demon










Skyridge0
Female
Pisces
78
67
Oracle
Punch Art
Caution
Dual Wield
Swim

Papyrus Codex
Madlemgen
Holy Miter
Brigandine
Bracer

Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Foxbird, Paralyze, Petrify
Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil

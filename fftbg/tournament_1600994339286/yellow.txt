Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ribbiks
Female
Gemini
53
70
Ninja
Yin Yang Magic
Critical Quick
Equip Bow
Move-MP Up

Night Killer
Hunting Bow
Leather Hat
Mythril Vest
Defense Ring

Shuriken, Bomb, Wand
Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Foxbird, Dispel Magic, Paralyze, Sleep



3ngag3
Male
Cancer
54
49
Chemist
Yin Yang Magic
Faith Save
Equip Shield
Levitate

Hydra Bag
Buckler
Golden Hairpin
Leather Outfit
Small Mantle

Potion, Echo Grass, Phoenix Down
Spell Absorb, Life Drain, Pray Faith, Silence Song, Dispel Magic, Paralyze



ValensEXP
Male
Cancer
75
49
Knight
Jump
Brave Save
Equip Axe
Move-MP Up

Giant Axe
Mythril Shield
Leather Helmet
Gold Armor
Feather Mantle

Head Break, Shield Break, Weapon Break, Magic Break, Mind Break
Level Jump2, Vertical Jump8



Just Here2
Female
Leo
58
39
Time Mage
Item
Speed Save
Throw Item
Levitate

Ivory Rod

Black Hood
Linen Robe
Magic Gauntlet

Haste, Slow, Stop, Immobilize, Float, Quick
Potion, Hi-Potion, Antidote

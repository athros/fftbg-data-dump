Player: !Green
Team: Green Team
Palettes: Green/White



GenZealot
Male
Virgo
50
61
Ninja
Talk Skill
Damage Split
Defend
Move-MP Up

Flame Whip
Flail
Golden Hairpin
Brigandine
Feather Mantle

Shuriken
Preach, Mimic Daravon, Rehabilitate



VolgraTheMoose
Monster
Pisces
75
73
Iron Hawk










DocZombiee
Male
Gemini
78
52
Calculator
Lucavi Skill
Catch
Sicken
Waterwalking

Battle Folio

Holy Miter
Chameleon Robe
Defense Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Lyner87
Male
Leo
62
75
Summoner
Talk Skill
Counter Tackle
Equip Sword
Retreat

Muramasa

Green Beret
Wizard Robe
108 Gems

Moogle, Shiva, Ramuh, Carbunkle, Salamander, Silf
Persuade, Praise, Preach, Insult, Negotiate, Refute, Rehabilitate

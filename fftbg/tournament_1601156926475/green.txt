Player: !Green
Team: Green Team
Palettes: Green/White



Latebit
Male
Gemini
45
71
Lancer
Steal
Auto Potion
Martial Arts
Levitate


Mythril Shield
Leather Helmet
Black Robe
Cursed Ring

Level Jump2, Vertical Jump4
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Arm Aim



Krombobreaker
Female
Leo
62
64
Samurai
Elemental
Counter Tackle
Equip Bow
Move-MP Up

Long Bow

Mythril Helmet
Light Robe
Magic Gauntlet

Koutetsu, Murasame, Kiyomori, Kikuichimoji, Masamune
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Phi Sig
Female
Virgo
67
61
Time Mage
Jump
Damage Split
Doublehand
Fly

Gokuu Rod

Flash Hat
Power Sleeve
Feather Boots

Haste 2, Slow, Stabilize Time, Meteor
Level Jump8, Vertical Jump5



Eoika
Male
Leo
56
52
Thief
Jump
Brave Save
Long Status
Lava Walking

Ancient Sword

Red Hood
Leather Outfit
Wizard Mantle

Leg Aim
Level Jump8, Vertical Jump8

Final Bets: green - 10 bets for 7,813G (78.6%, x0.27); yellow - 4 bets for 2,127G (21.4%, x3.67)

green bets:
almost__sane: 4,000G (51.2%, 14,887G)
Ruvelia_BibeI: 1,000G (12.8%, 12,983G)
heropong: 703G (9.0%, 703G)
Mushufasa_: 500G (6.4%, 9,053G)
ColetteMSLP: 496G (6.3%, 13,233G)
Zachara: 322G (4.1%, 117,822G)
Tithonus: 292G (3.7%, 292G)
AllInBot: 200G (2.6%, 200G)
datadrivenbot: 200G (2.6%, 80,612G)
Qaz12301: 100G (1.3%, 7,113G)

yellow bets:
upvla: 600G (28.2%, 600G)
BirbBrainsBot: 569G (26.8%, 70,077G)
LeoNightFury: 500G (23.5%, 8,288G)
getthemoneyz: 458G (21.5%, 2,163,747G)

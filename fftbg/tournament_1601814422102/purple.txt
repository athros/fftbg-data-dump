Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LeoNightFury
Male
Cancer
70
53
Ninja
Black Magic
Mana Shield
Concentrate
Fly

Sasuke Knife
Short Edge
Twist Headband
Judo Outfit
Dracula Mantle

Shuriken, Dictionary
Fire 4, Ice, Ice 2, Ice 3, Ice 4, Frog



Smashy
Male
Gemini
65
43
Samurai
Jump
Blade Grasp
Dual Wield
Move+2

Obelisk
Obelisk
Circlet
Chain Mail
Feather Boots

Koutetsu
Level Jump4, Vertical Jump4



Almost Sane
Male
Cancer
71
69
Knight
Steal
Damage Split
Short Status
Jump+2

Long Sword
Round Shield
Crystal Helmet
Light Robe
Dracula Mantle

Armor Break, Weapon Break, Magic Break
Steal Status



Zachara
Male
Pisces
47
67
Knight
Draw Out
HP Restore
Secret Hunt
Ignore Terrain

Slasher
Ice Shield
Genji Helmet
Bronze Armor
Dracula Mantle

Armor Break, Shield Break, Speed Break, Mind Break, Stasis Sword
Asura, Bizen Boat, Murasame

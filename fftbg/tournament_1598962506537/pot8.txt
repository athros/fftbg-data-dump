Final Bets: white - 10 bets for 8,445G (74.7%, x0.34); champion - 5 bets for 2,854G (25.3%, x2.96)

white bets:
VolgraTheMoose: 2,001G (23.7%, 145,664G)
Lord_Burrah: 1,500G (17.8%, 118,547G)
snkey: 1,257G (14.9%, 12,572G)
BirbBrainsBot: 1,000G (11.8%, 38,513G)
E_Ballard: 824G (9.8%, 96,186G)
powergems: 777G (9.2%, 15,129G)
FriendlySkeleton: 600G (7.1%, 1,343G)
pepperbuster: 220G (2.6%, 220G)
getthemoneyz: 166G (2.0%, 1,808,898G)
carnivalnights: 100G (1.2%, 100G)

champion bets:
rocl: 1,000G (35.0%, 65,092G)
DavenIII: 846G (29.6%, 846G)
WhiteTigress: 500G (17.5%, 12,159G)
SephDarkheart: 308G (10.8%, 308G)
datadrivenbot: 200G (7.0%, 65,595G)

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Nok
Female
Capricorn
69
56
Samurai
Black Magic
Mana Shield
Magic Attack UP
Move-HP Up

Chirijiraden

Crystal Helmet
Light Robe
Power Wrist

Asura, Koutetsu, Murasame, Kikuichimoji
Fire 2, Ice 2, Ice 3, Empower



DouglasDragonThePoet
Female
Leo
39
52
Archer
Summon Magic
Catch
Doublehand
Move+3

Poison Bow

Flash Hat
Brigandine
Elf Mantle

Charge+3, Charge+5
Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Leviathan, Fairy



Leonidusx
Male
Taurus
80
56
Knight
Summon Magic
Auto Potion
Equip Armor
Move+3

Defender
Round Shield
Grand Helmet
Brigandine
Spike Shoes

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Dark Sword
Moogle, Carbunkle, Odin, Leviathan, Silf



VolgraTheMoose
Male
Scorpio
64
69
Lancer
Black Magic
Distribute
Dual Wield
Fly

Obelisk
Holy Lance
Circlet
Linen Robe
Feather Mantle

Level Jump4, Vertical Jump5
Fire 3, Ice, Ice 2, Empower, Death

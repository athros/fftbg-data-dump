Final Bets: blue - 5 bets for 3,498G (27.8%, x2.60); green - 12 bets for 9,090G (72.2%, x0.38)

blue bets:
bruubarg: 2,422G (69.2%, 2,422G)
DHaveWord: 412G (11.8%, 10,026G)
helpimabug: 264G (7.5%, 264G)
Leonidusx: 200G (5.7%, 84,925G)
xsifthewolf: 200G (5.7%, 2,421G)

green bets:
AllInBot: 2,980G (32.8%, 2,980G)
upvla: 1,200G (13.2%, 8,036G)
BirbBrainsBot: 1,000G (11.0%, 54,452G)
getthemoneyz: 842G (9.3%, 1,974,896G)
Rune339: 781G (8.6%, 7,811G)
HaplessOne: 572G (6.3%, 572G)
ColetteMSLP: 400G (4.4%, 4,884G)
ericzubat: 367G (4.0%, 367G)
resjudicata3: 332G (3.7%, 332G)
hotpocketsofficial: 316G (3.5%, 316G)
datadrivenbot: 200G (2.2%, 65,441G)
MemoriesofFinal: 100G (1.1%, 37,787G)

Final Bets: purple - 7 bets for 2,101G (34.3%, x1.91); brown - 9 bets for 4,016G (65.7%, x0.52)

purple bets:
Raixelol: 580G (27.6%, 26,354G)
ColetteMSLP: 400G (19.0%, 2,579G)
TheSabretoothe: 332G (15.8%, 332G)
bigbongsmoker: 308G (14.7%, 4,044G)
MemoriesofFinal: 262G (12.5%, 3,634G)
datadrivenbot: 200G (9.5%, 58,434G)
BartTradingCompany: 19G (0.9%, 191G)

brown bets:
BirbBrainsBot: 1,000G (24.9%, 181,038G)
Mesmaster: 1,000G (24.9%, 236,402G)
bruubarg: 600G (14.9%, 600G)
getthemoneyz: 368G (9.2%, 1,905,190G)
Neech: 288G (7.2%, 772G)
ValensEXP_: 248G (6.2%, 1,423G)
Ogema_theDream: 212G (5.3%, 212G)
fantastadon: 200G (5.0%, 2,275G)
AllInBot: 100G (2.5%, 100G)

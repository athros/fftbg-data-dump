Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Twisted Nutsatchel
Monster
Sagittarius
52
51
Malboro










Aldrammech
Monster
Leo
61
51
Wyvern










Douchetron
Male
Capricorn
67
70
Monk
Throw
Counter Flood
Dual Wield
Jump+1



Flash Hat
Leather Outfit
Cursed Ring

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Shuriken, Bomb



Lijarkh
Male
Scorpio
67
72
Monk
Steal
Critical Quick
Short Status
Jump+2



Barette
Judo Outfit
Wizard Mantle

Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Steal Armor, Arm Aim

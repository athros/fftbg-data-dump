Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HASTERIOUS
Female
Gemini
43
51
Geomancer
Draw Out
Counter
Defend
Ignore Height

Giant Axe
Flame Shield
Holy Miter
Wizard Outfit
N-Kai Armlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Heaven's Cloud, Kiyomori, Muramasa



Thyrandaal
Female
Pisces
49
79
Summoner
Charge
HP Restore
Short Charge
Retreat

Cute Bag

Red Hood
Mystic Vest
Magic Ring

Shiva, Ifrit, Carbunkle, Salamander, Fairy, Lich
Charge+1, Charge+2



Gorgewall
Female
Cancer
37
52
Summoner
White Magic
Meatbone Slash
Equip Axe
Waterwalking

Oak Staff

Black Hood
Leather Outfit
Bracer

Moogle, Ramuh, Golem, Carbunkle, Leviathan, Silf, Cyclops
Cure, Cure 2, Raise, Reraise, Wall, Holy



Bigbongsmoker
Male
Libra
42
53
Archer
Item
Meatbone Slash
Doublehand
Levitate

Snipe Bow

Leather Helmet
Leather Outfit
Power Wrist

Charge+20
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

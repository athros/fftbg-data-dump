Player: !Black
Team: Black Team
Palettes: Black/Red



Randgridr
Male
Scorpio
79
47
Monk
Draw Out
PA Save
Dual Wield
Move+3



Headgear
Adaman Vest
Dracula Mantle

Spin Fist, Secret Fist, Purification, Revive
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa



Genkidou
Male
Capricorn
53
66
Knight
Sing
Brave Save
Dual Wield
Move-MP Up

Long Sword
Long Sword
Gold Helmet
Bronze Armor
N-Kai Armlet

Armor Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Life Song, Nameless Song, Hydra Pit



NovaKnight21
Male
Aries
60
58
Archer
Yin Yang Magic
Brave Save
Doublehand
Jump+1

Stone Gun

Crystal Helmet
Brigandine
Diamond Armlet

Charge+1
Blind, Life Drain, Confusion Song, Dispel Magic, Paralyze



CassiePhoenix
Male
Libra
70
49
Priest
Sing
Critical Quick
Doublehand
Teleport

Wizard Staff

Leather Hat
Light Robe
Magic Gauntlet

Raise, Shell 2, Esuna, Holy
Angel Song, Diamond Blade, Sky Demon

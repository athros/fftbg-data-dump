Player: !White
Team: White Team
Palettes: White/Blue



Vahn Blade
Monster
Aquarius
46
62
Wyvern










Leonidusx
Male
Libra
49
53
Knight
Black Magic
Counter Tackle
Equip Armor
Move+2

Ancient Sword
Genji Shield
Feather Hat
Mythril Vest
Angel Ring

Speed Break, Power Break, Justice Sword
Fire, Ice 4, Frog



DeathTaxesAndAnime
Female
Aquarius
69
63
Mediator
Throw
Dragon Spirit
Halve MP
Ignore Terrain

Battle Folio

Barette
Clothes
N-Kai Armlet

Invitation, Threaten, Mimic Daravon, Refute
Hammer



Formerlydrdong
Male
Cancer
46
68
Thief
Summon Magic
Mana Shield
Attack UP
Fly

Ice Brand

Green Beret
Clothes
Power Wrist

Steal Heart, Steal Shield, Steal Weapon
Moogle, Ramuh, Ifrit, Titan, Golem, Leviathan, Salamander

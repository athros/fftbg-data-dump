Player: !Black
Team: Black Team
Palettes: Black/Red



NovaKnight21
Male
Aries
58
62
Monk
Yin Yang Magic
Faith Save
Dual Wield
Lava Walking



Black Hood
Brigandine
Magic Gauntlet

Wave Fist, Earth Slash, Chakra, Revive
Pray Faith, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep



VolgraTheMoose
Male
Libra
68
52
Ninja
Jump
Counter Flood
Equip Armor
Move+2

Dagger
Blind Knife
Leather Helmet
Black Robe
Sprint Shoes

Shuriken, Knife, Staff, Wand
Level Jump8, Vertical Jump7



3ngag3
Male
Scorpio
49
72
Lancer
Battle Skill
Counter
Defend
Move+1

Spear
Bronze Shield
Gold Helmet
Chain Mail
Angel Ring

Level Jump8, Vertical Jump7
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword, Surging Sword



DeathTaxesAndAnime
Female
Aquarius
35
51
Thief
Draw Out
Mana Shield
Equip Polearm
Ignore Height

Gokuu Rod

Ribbon
Mystic Vest
Elf Mantle

Steal Status, Arm Aim
Bizen Boat, Murasame, Heaven's Cloud, Muramasa

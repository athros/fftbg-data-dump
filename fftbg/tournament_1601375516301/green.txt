Player: !Green
Team: Green Team
Palettes: Green/White



LivingHitokiri
Male
Scorpio
53
74
Lancer
Time Magic
Faith Save
Dual Wield
Move-HP Up

Mythril Spear
Partisan
Bronze Helmet
Diamond Armor
Dracula Mantle

Level Jump5, Vertical Jump8
Haste 2, Quick, Demi, Demi 2, Meteor



BaronHaynes
Female
Cancer
57
60
Dancer
Black Magic
Caution
Long Status
Jump+2

Persia

Triangle Hat
Brigandine
Battle Boots

Slow Dance, Polka Polka, Disillusion, Last Dance
Fire 3, Fire 4, Bolt, Ice 2, Ice 3



Panushenko
Monster
Aries
48
52
Great Malboro










Sharosa
Male
Aries
63
73
Squire
White Magic
Counter
Equip Polearm
Move-MP Up

Cypress Rod
Buckler
Red Hood
Adaman Vest
Chantage

Throw Stone, Heal, Yell
Cure 4, Raise, Shell, Esuna

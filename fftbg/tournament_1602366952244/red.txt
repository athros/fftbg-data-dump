Player: !Red
Team: Red Team
Palettes: Red/Brown



DeathTaxesAndAnime
Female
Scorpio
59
44
Thief
Elemental
Caution
Equip Bow
Jump+3

Night Killer

Flash Hat
Mythril Vest
Leather Mantle

Steal Heart, Steal Helmet
Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Smashy
Male
Aquarius
42
46
Geomancer
Battle Skill
Parry
Long Status
Move+3

Giant Axe
Bronze Shield
Feather Hat
Linen Robe
Red Shoes

Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Head Break, Magic Break



Just Here2
Female
Serpentarius
53
62
Priest
Battle Skill
Blade Grasp
Equip Gun
Move-MP Up

Romanda Gun

Holy Miter
Wizard Robe
Reflect Ring

Cure 3, Cure 4, Raise, Regen, Protect 2, Shell, Esuna, Holy
Power Break, Surging Sword



ANFz
Male
Taurus
56
64
Chemist
Sing
PA Save
Beastmaster
Jump+2

Panther Bag

Leather Hat
Mystic Vest
Jade Armlet

Potion, Hi-Potion, Elixir, Eye Drop, Maiden's Kiss, Phoenix Down
Life Song, Nameless Song, Sky Demon

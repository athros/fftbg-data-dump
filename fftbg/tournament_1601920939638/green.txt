Player: !Green
Team: Green Team
Palettes: Green/White



E Ballard
Male
Capricorn
78
64
Monk
Charge
Sunken State
Equip Shield
Ignore Height


Flame Shield
Barette
Clothes
Magic Ring

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra
Charge+2, Charge+10



Acid Flashback
Female
Aquarius
52
72
Ninja
Elemental
MA Save
Concentrate
Ignore Terrain

Flail
Morning Star
Headgear
Leather Outfit
Leather Mantle

Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Daveb
Male
Aries
54
65
Ninja
Item
MP Restore
Martial Arts
Levitate

Morning Star
Iga Knife
Feather Hat
Rubber Costume
Spike Shoes

Stick, Wand
Potion, Hi-Potion, Ether, Hi-Ether, Phoenix Down



LDSkinny
Female
Libra
80
65
Oracle
Throw
Mana Shield
Dual Wield
Teleport 2

Gokuu Rod
Octagon Rod
Red Hood
White Robe
Magic Ring

Blind, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Shuriken

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DavenIII
Female
Aries
69
69
Time Mage
White Magic
Damage Split
Short Charge
Move-MP Up

Gokuu Rod

Flash Hat
Judo Outfit
Jade Armlet

Haste, Slow 2, Immobilize, Float, Quick, Demi 2, Meteor
Cure, Cure 2, Cure 4, Raise 2, Regen



Joewcarson
Female
Scorpio
39
68
Mime

Hamedo
Martial Arts
Swim



Leather Hat
Secret Clothes
Defense Ring

Mimic




Maakur
Female
Sagittarius
63
57
Squire
Steal
Counter Magic
Sicken
Waterbreathing

Rune Blade
Crystal Shield
Headgear
Wizard Outfit
Reflect Ring

Accumulate, Tickle, Yell, Cheer Up, Scream
Steal Armor, Steal Shield, Steal Accessory, Steal Status, Arm Aim



CosmicTactician
Female
Aquarius
67
62
Time Mage
Item
Blade Grasp
Equip Polearm
Move-MP Up

Musk Rod

Headgear
Wizard Robe
Elf Mantle

Haste, Stop, Immobilize, Galaxy Stop
X-Potion, Antidote, Phoenix Down

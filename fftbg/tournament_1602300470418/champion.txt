Player: !zChamp
Team: Champion Team
Palettes: Green/White



Evewho
Female
Libra
50
69
Priest
Math Skill
Speed Save
Short Charge
Move-MP Up

Rainbow Staff

Thief Hat
Light Robe
Magic Gauntlet

Cure, Raise, Raise 2, Shell, Wall, Esuna
CT, Height, Prime Number, 5



CapnChaos12
Male
Libra
78
76
Bard
Steal
MP Restore
Doublehand
Move-MP Up

Ramia Harp

Twist Headband
Leather Armor
Small Mantle

Life Song, Magic Song
Steal Helmet, Steal Accessory, Arm Aim, Leg Aim



HASTERIOUS
Male
Scorpio
72
53
Lancer
Time Magic
Dragon Spirit
Defend
Fly

Partisan
Flame Shield
Diamond Helmet
Light Robe
Rubber Shoes

Level Jump3, Vertical Jump3
Stop, Float, Quick, Stabilize Time, Galaxy Stop



E Ballard
Male
Pisces
79
66
Ninja
Elemental
Meatbone Slash
Equip Armor
Ignore Terrain

Hidden Knife
Flame Whip
Green Beret
Genji Armor
Magic Ring

Shuriken, Staff
Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand

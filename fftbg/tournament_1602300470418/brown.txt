Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lijarkh
Male
Cancer
65
68
Monk
Summon Magic
Distribute
Attack UP
Move+2



Feather Hat
Leather Outfit
Magic Gauntlet

Spin Fist, Purification, Revive
Moogle, Ifrit, Odin, Leviathan, Salamander, Lich



Lowlf
Female
Aquarius
71
71
Squire
White Magic
Meatbone Slash
Equip Armor
Lava Walking

Ice Brand
Mythril Shield
Feather Hat
Linen Robe
Dracula Mantle

Accumulate, Dash, Heal, Tickle, Wish, Scream
Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Shell, Shell 2, Esuna



E Ballard
Male
Serpentarius
57
42
Squire
Punch Art
Parry
Equip Sword
Teleport

Long Sword
Kaiser Plate
Golden Hairpin
Leather Armor
Small Mantle

Accumulate, Dash, Throw Stone, Cheer Up, Fury
Purification, Revive



Runeseeker22
Male
Aquarius
69
66
Squire
Battle Skill
Faith Save
Magic Attack UP
Move+2

Blood Sword
Buckler
Gold Helmet
Mythril Armor
Cursed Ring

Dash, Heal, Yell, Cheer Up, Wish
Shield Break, Weapon Break, Power Break

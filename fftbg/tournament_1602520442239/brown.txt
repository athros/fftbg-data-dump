Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Demon Lord Josh
Male
Virgo
72
45
Geomancer
Summon Magic
MA Save
Maintenance
Ignore Terrain

Ice Brand
Aegis Shield
Flash Hat
Earth Clothes
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Leviathan



Bruubarg
Monster
Scorpio
53
50
Ahriman










Forkmore
Female
Aries
71
79
Oracle
White Magic
Mana Shield
Martial Arts
Jump+1



Thief Hat
Earth Clothes
Magic Gauntlet

Zombie, Foxbird, Dispel Magic, Paralyze, Sleep
Cure, Cure 4, Raise, Reraise, Protect, Shell, Wall



JarlBrolaf
Female
Sagittarius
38
78
Wizard
Summon Magic
Parry
Doublehand
Teleport

Air Knife

Golden Hairpin
Brigandine
Sprint Shoes

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Empower, Death
Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Silf, Fairy, Lich

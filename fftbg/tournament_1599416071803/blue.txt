Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SephDarkheart
Female
Libra
63
65
Mediator
Summon Magic
Brave Save
Concentrate
Move-MP Up

Papyrus Codex

Flash Hat
White Robe
Diamond Armlet

Invitation, Threaten, Death Sentence, Negotiate, Refute
Silf, Lich



LDSkinny
Female
Virgo
68
77
Oracle
Throw
Regenerator
Dual Wield
Waterwalking

Ivory Rod
Ivory Rod
Golden Hairpin
Silk Robe
Jade Armlet

Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Bomb



Roofiepops
Female
Sagittarius
71
63
Calculator
Black Magic
MP Restore
Short Charge
Jump+3

Bestiary

Leather Hat
Light Robe
Bracer

Height, 5, 4, 3
Fire 4, Bolt 2, Ice, Ice 4, Death, Flare



Runeseeker22
Male
Libra
74
74
Time Mage
Charge
Hamedo
Long Status
Move+3

White Staff

Feather Hat
Clothes
Magic Ring

Immobilize, Demi 2, Stabilize Time
Charge+4, Charge+5, Charge+7

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Latebit
Male
Cancer
60
58
Bard
Draw Out
Damage Split
Equip Shield
Jump+3

Silver Bow
Bronze Shield
Red Hood
Adaman Vest
Genji Gauntlet

Angel Song, Cheer Song, Nameless Song
Kiyomori, Chirijiraden



Pplvee1
Male
Aquarius
40
80
Knight
Sing
Brave Save
Concentrate
Waterwalking

Defender
Flame Shield
Barbuta
Diamond Armor
Cherche

Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Dark Sword
Angel Song, Life Song, Magic Song



Highelectricaltemperature
Male
Gemini
46
73
Bard
Time Magic
Parry
Sicken
Waterbreathing

Ramia Harp

Triangle Hat
Gold Armor
Small Mantle

Angel Song, Life Song, Battle Song
Haste, Reflect, Stabilize Time



Lijarkh
Male
Virgo
63
67
Monk
Black Magic
HP Restore
Attack UP
Move+2



Green Beret
Black Costume
Leather Mantle

Wave Fist, Purification, Chakra, Revive
Fire 2, Bolt 2, Bolt 3, Ice 2, Ice 3, Ice 4, Empower, Flare

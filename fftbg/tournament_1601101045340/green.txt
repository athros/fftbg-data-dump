Player: !Green
Team: Green Team
Palettes: Green/White



Arcblazer23
Male
Taurus
38
50
Chemist
Elemental
Damage Split
Defense UP
Move+2

Blaze Gun

Holy Miter
Chain Vest
Power Wrist

Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Lava Ball



Jabuamba
Monster
Libra
59
61
Iron Hawk










Scsuperstar
Female
Scorpio
79
75
Squire
Black Magic
Mana Shield
Equip Knife
Move+3

Zorlin Shape
Genji Shield
Leather Helmet
Carabini Mail
Magic Gauntlet

Accumulate, Dash, Heal, Tickle, Yell, Fury, Wish
Fire 3, Ice 2



Lowlf
Male
Scorpio
77
57
Knight
Elemental
Dragon Spirit
Attack UP
Fly

Nagrarock
Mythril Shield
Barbuta
Genji Armor
Battle Boots

Head Break, Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Surging Sword
Pitfall, Sand Storm, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ruleof5
Male
Scorpio
64
51
Thief
Elemental
Arrow Guard
Equip Sword
Retreat

Defender

Golden Hairpin
Clothes
Spike Shoes

Steal Heart, Steal Helmet, Steal Armor, Steal Status
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Lava Ball



ExplanationMarkWow
Female
Leo
66
42
Mime

Regenerator
Equip Armor
Lava Walking



Red Hood
White Robe
108 Gems

Mimic




SeniorBunk
Male
Leo
69
78
Lancer
Charge
Dragon Spirit
Beastmaster
Move-MP Up

Mythril Spear
Mythril Shield
Platinum Helmet
Silk Robe
Red Shoes

Level Jump2, Vertical Jump4
Charge+1, Charge+3, Charge+10, Charge+20



Roofiepops
Female
Pisces
55
69
Calculator
Time Magic
Caution
Equip Polearm
Move+3

Gokuu Rod

Feather Hat
Black Costume
Sprint Shoes

Height, 5, 3
Haste 2, Slow, Quick, Demi, Demi 2, Stabilize Time, Meteor

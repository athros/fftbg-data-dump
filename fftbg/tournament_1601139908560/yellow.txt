Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SQUiDSQUARKLIN
Male
Taurus
69
63
Geomancer
Battle Skill
PA Save
Concentrate
Waterbreathing

Sleep Sword
Ice Shield
Golden Hairpin
Mystic Vest
Reflect Ring

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shield Break, Speed Break



Run With Stone GUNs
Male
Leo
44
58
Monk
Item
Counter Flood
Magic Defense UP
Lava Walking



Flash Hat
Judo Outfit
Jade Armlet

Pummel, Purification
Potion, Hi-Ether, Maiden's Kiss, Phoenix Down



Sharknaldson
Female
Scorpio
51
65
Mime

Speed Save
Martial Arts
Swim



Golden Hairpin
Earth Clothes
Wizard Mantle

Mimic




Lord Burrah
Male
Capricorn
60
57
Chemist
Throw
PA Save
Sicken
Swim

Star Bag

Holy Miter
Earth Clothes
Elf Mantle

Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Shuriken, Staff

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Krombobreaker
Female
Pisces
74
51
Geomancer
Time Magic
Distribute
Maintenance
Move+1

Rune Blade
Escutcheon
Triangle Hat
Mythril Vest
Dracula Mantle

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Haste 2, Stop, Reflect, Demi



Powergems
Male
Virgo
65
56
Wizard
Talk Skill
Critical Quick
Short Charge
Swim

Blind Knife

Triangle Hat
White Robe
Dracula Mantle

Fire 4, Ice, Ice 2, Ice 4
Persuade, Negotiate, Mimic Daravon, Refute



King Smashington
Male
Cancer
52
52
Ninja
Yin Yang Magic
Dragon Spirit
Equip Gun
Move+2

Bestiary
Papyrus Codex
Leather Hat
Secret Clothes
Genji Gauntlet

Staff, Spear
Blind, Poison, Zombie, Confusion Song, Paralyze, Sleep, Petrify, Dark Holy



E Ballard
Male
Sagittarius
70
57
Monk
Sing
MA Save
Equip Gun
Jump+2

Madlemgen

Barette
Adaman Vest
Rubber Shoes

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Magic Song, Last Song

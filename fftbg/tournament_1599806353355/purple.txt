Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



BuffaloCrunch
Female
Sagittarius
75
69
Squire
Battle Skill
Sunken State
Short Charge
Move+1

Coral Sword
Buckler
Twist Headband
Secret Clothes
Power Wrist

Accumulate, Heal, Tickle, Fury
Magic Break, Speed Break, Night Sword



Treafa
Female
Scorpio
56
77
Monk
Basic Skill
Dragon Spirit
Long Status
Retreat



Triangle Hat
Mythril Vest
Reflect Ring

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Accumulate, Dash, Throw Stone



Lythe Caraker
Monster
Cancer
46
63
Skeleton










IBardic
Male
Taurus
62
69
Knight
Basic Skill
Distribute
Dual Wield
Ignore Terrain

Blood Sword
Rune Blade
Diamond Helmet
Bronze Armor
Red Shoes

Armor Break, Weapon Break, Mind Break, Dark Sword
Throw Stone, Heal, Cheer Up, Wish

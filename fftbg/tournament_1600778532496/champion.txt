Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Actual JP
Female
Scorpio
75
60
Ninja
Talk Skill
Counter Tackle
Magic Attack UP
Move+3

Hidden Knife
Spell Edge
Twist Headband
Chain Vest
Vanish Mantle

Shuriken, Wand
Invitation, Threaten, Solution, Insult, Negotiate



Maakur
Male
Gemini
57
48
Lancer
Charge
MA Save
Dual Wield
Move+3

Obelisk
Partisan
Leather Helmet
Mythril Armor
Vanish Mantle

Level Jump8, Vertical Jump5
Charge+2, Charge+5, Charge+7, Charge+20



Shalloween
Male
Aquarius
64
47
Mime

Meatbone Slash
Martial Arts
Move-MP Up



Headgear
Secret Clothes
Red Shoes

Mimic




Daveb
Male
Leo
65
76
Ninja
Summon Magic
MA Save
Equip Gun
Waterbreathing

Papyrus Codex
Papyrus Codex
Twist Headband
Mystic Vest
Chantage

Shuriken, Bomb, Knife, Dictionary
Moogle, Titan, Golem, Carbunkle, Silf, Lich, Cyclops

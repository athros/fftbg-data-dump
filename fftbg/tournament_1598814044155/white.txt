Player: !White
Team: White Team
Palettes: White/Blue



Lydian C
Female
Cancer
66
37
Wizard
Math Skill
Blade Grasp
Defend
Ignore Height

Dagger

Black Hood
Wizard Outfit
Elf Mantle

Fire 3, Bolt, Bolt 2, Ice, Ice 2, Ice 3, Empower, Frog
CT, Height, 5, 4, 3



Ruleof5
Female
Scorpio
52
56
Archer
White Magic
Catch
Halve MP
Swim

Long Bow

Barette
Secret Clothes
Magic Gauntlet

Charge+1, Charge+2, Charge+5, Charge+7
Cure, Cure 3, Cure 4, Reraise, Protect, Shell, Esuna



Redmage4evah
Male
Serpentarius
50
77
Priest
Yin Yang Magic
Counter
Martial Arts
Waterbreathing



Black Hood
Robe of Lords
Spike Shoes

Cure, Cure 2, Cure 3, Raise, Raise 2, Shell, Shell 2, Esuna
Confusion Song, Sleep, Dark Holy



Dogsandcatsand
Female
Taurus
63
71
Wizard
Draw Out
Hamedo
Equip Armor
Ignore Terrain

Dragon Rod

Iron Helmet
Carabini Mail
Dracula Mantle

Fire, Fire 4, Bolt, Bolt 4, Ice 2, Empower, Flare
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Lowlf
Female
Capricorn
62
61
Samurai
Item
Mana Shield
Martial Arts
Move-MP Up



Mythril Helmet
Light Robe
Rubber Shoes

Asura, Bizen Boat, Chirijiraden
Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down



Sairentozon7
Female
Aquarius
78
60
Ninja
Summon Magic
Catch
Equip Polearm
Ignore Terrain

Partisan
Cypress Rod
Leather Hat
Mystic Vest
Magic Gauntlet

Shuriken, Bomb, Axe
Moogle, Shiva, Ifrit, Titan, Carbunkle, Leviathan, Silf



Seaweed B
Male
Cancer
65
57
Mediator
Summon Magic
Meatbone Slash
Dual Wield
Lava Walking

Romanda Gun
Blaze Gun
Black Hood
Leather Outfit
Red Shoes

Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Shiva, Salamander



OneHundredFists
Monster
Pisces
74
58
Bull Demon








Final Bets: yellow - 12 bets for 23,189G (52.0%, x0.92); white - 15 bets for 21,389G (48.0%, x1.08)

yellow bets:
DuPlatypus: 12,912G (55.7%, 12,912G)
Thyrandaal: 5,555G (24.0%, 241,752G)
VolgraTheMoose: 892G (3.8%, 20,654G)
HaateXIII: 720G (3.1%, 720G)
lotharek: 600G (2.6%, 12,799G)
ColetteMSLP: 556G (2.4%, 556G)
DHaveWord: 460G (2.0%, 460G)
TheBrett: 398G (1.7%, 398G)
Cryptopsy70: 396G (1.7%, 1,041G)
randgridr: 344G (1.5%, 344G)
datadrivenbot: 200G (0.9%, 78,620G)
ayeCoop: 156G (0.7%, 156G)

white bets:
latebit: 6,069G (28.4%, 75,868G)
nhammen: 5,000G (23.4%, 43,829G)
Shalloween: 3,661G (17.1%, 7,323G)
SQUiDSQUARKLIN: 1,159G (5.4%, 1,159G)
BirbBrainsBot: 1,000G (4.7%, 50,153G)
getthemoneyz: 1,000G (4.7%, 2,093,669G)
sireaulou: 1,000G (4.7%, 9,663G)
Willjin: 600G (2.8%, 42,093G)
ExplanationMarkWow: 500G (2.3%, 18,008G)
AllInBot: 487G (2.3%, 487G)
Riley331: 312G (1.5%, 312G)
otakutaylor: 200G (0.9%, 1,747G)
skillomono: 200G (0.9%, 2,957G)
gorgewall: 101G (0.5%, 6,185G)
Reddash9: 100G (0.5%, 1,475G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Seppu777
Male
Leo
54
59
Lancer
Charge
Speed Save
Equip Polearm
Move+2

Octagon Rod
Mythril Shield
Leather Helmet
Linen Robe
Reflect Ring

Level Jump8, Vertical Jump3
Charge+2, Charge+3, Charge+10



Vorap
Female
Leo
63
71
Knight
Jump
Parry
Equip Knife
Move+2

Thunder Rod
Gold Shield
Leather Helmet
Linen Robe
Sprint Shoes

Head Break, Shield Break, Magic Break, Justice Sword, Dark Sword
Level Jump4, Vertical Jump6



DrAntiSocial
Monster
Leo
78
60
Grenade










Zagorsek
Male
Capricorn
56
79
Monk
Sing
Earplug
Short Charge
Jump+1



Twist Headband
Earth Clothes
Defense Ring

Wave Fist, Purification, Revive
Angel Song, Cheer Song, Battle Song, Magic Song, Last Song, Diamond Blade

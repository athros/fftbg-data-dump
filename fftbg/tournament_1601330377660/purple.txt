Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Pplvee1
Male
Scorpio
43
49
Ninja
Time Magic
Caution
Equip Sword
Fly

Ragnarok
Save the Queen
Black Hood
Judo Outfit
Magic Ring

Shuriken, Wand
Haste, Haste 2, Float, Reflect, Quick, Demi



DeathTaxesAndAnime
Female
Leo
53
80
Thief
Item
Distribute
Throw Item
Lava Walking

Iron Sword

Golden Hairpin
Leather Outfit
Small Mantle

Steal Helmet, Steal Accessory
Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down



DHaveWord
Female
Aquarius
75
69
Time Mage
Draw Out
Auto Potion
Long Status
Ignore Height

White Staff

Feather Hat
Wizard Robe
Battle Boots

Haste, Slow, Float, Reflect, Demi, Meteor
Asura, Bizen Boat, Muramasa



Phi Sig
Male
Taurus
78
79
Squire
Charge
Speed Save
Sicken
Fly

Sleep Sword
Bronze Shield
Holy Miter
Wizard Outfit
Bracer

Accumulate, Dash, Tickle, Yell, Wish
Charge+2, Charge+20

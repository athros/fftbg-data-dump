Player: !White
Team: White Team
Palettes: White/Blue



MelTorefas
Female
Pisces
36
77
Ninja
Elemental
Caution
Equip Gun
Fly

Papyrus Codex
Papyrus Codex
Red Hood
Chain Vest
Magic Ring

Shuriken
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Escobro
Female
Virgo
66
68
Samurai
Item
Parry
Magic Attack UP
Ignore Height

Muramasa

Bronze Helmet
Wizard Robe
Magic Ring

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Antidote, Eye Drop, Maiden's Kiss, Phoenix Down



FatTunaKing
Male
Pisces
54
54
Monk
Yin Yang Magic
Distribute
Dual Wield
Move+3



Twist Headband
Adaman Vest
Defense Ring

Spin Fist, Earth Slash, Purification, Seal Evil
Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Dark Holy



3ngag3
Male
Libra
76
42
Geomancer
Battle Skill
Counter Tackle
Short Status
Waterbreathing

Kikuichimoji
Escutcheon
Golden Hairpin
Silk Robe
Elf Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp
Weapon Break, Speed Break, Power Break, Night Sword

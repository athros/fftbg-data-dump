Final Bets: purple - 10 bets for 9,650G (78.7%, x0.27); brown - 6 bets for 2,604G (21.3%, x3.71)

purple bets:
Snowfats: 5,000G (51.8%, 35,749G)
DeathTaxesAndAnime: 1,702G (17.6%, 1,702G)
Zachara: 1,000G (10.4%, 118,000G)
lowlf: 648G (6.7%, 648G)
scsuperstar: 459G (4.8%, 459G)
Ruvelia_BibeI: 240G (2.5%, 240G)
datadrivenbot: 200G (2.1%, 79,233G)
VynxYukida: 200G (2.1%, 5,432G)
gorgewall: 101G (1.0%, 4,522G)
Tithonus: 100G (1.0%, 1,658G)

brown bets:
BirbBrainsBot: 1,000G (38.4%, 65,583G)
AllInBot: 563G (21.6%, 563G)
seppu777: 555G (21.3%, 6,536G)
getthemoneyz: 226G (8.7%, 2,160,879G)
Evewho: 200G (7.7%, 5,629G)
7Cerulean7: 60G (2.3%, 1,644G)

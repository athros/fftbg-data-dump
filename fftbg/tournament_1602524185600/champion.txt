Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Khelor
Monster
Taurus
78
71
Ultima Demon










Zetchryn
Male
Leo
65
48
Monk
Elemental
Auto Potion
Magic Attack UP
Waterwalking



Red Hood
Judo Outfit
Cherche

Secret Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand



Leonidusx
Male
Capricorn
54
40
Knight
Punch Art
Damage Split
Sicken
Move+3

Rune Blade
Diamond Shield
Diamond Helmet
Silk Robe
Magic Ring

Head Break, Armor Break, Mind Break, Stasis Sword
Pummel, Purification, Revive, Seal Evil



Randgridr
Female
Leo
66
66
Dancer
Time Magic
Arrow Guard
Concentrate
Levitate

Cute Bag

Golden Hairpin
Brigandine
Germinas Boots

Wiznaibus, Slow Dance, Disillusion, Void Storage
Haste, Slow, Quick, Stabilize Time

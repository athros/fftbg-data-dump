Player: !Green
Team: Green Team
Palettes: Green/White



Lube Squid
Monster
Aquarius
73
79
Pisco Demon










Mesmaster
Female
Scorpio
44
61
Geomancer
Battle Skill
Caution
Equip Armor
Move-MP Up

Diamond Sword
Round Shield
Crystal Helmet
Wizard Outfit
Feather Boots

Water Ball, Local Quake, Quicksand, Sand Storm
Shield Break, Magic Break, Speed Break, Stasis Sword, Night Sword



LivingHitokiri
Male
Aquarius
66
63
Knight
Throw
Sunken State
Attack UP
Lava Walking

Defender
Mythril Shield
Genji Helmet
Linen Robe
108 Gems

Head Break, Armor Break, Magic Break, Power Break, Stasis Sword, Surging Sword
Shuriken, Bomb, Sword, Staff, Dictionary



BuffaloCrunch
Male
Pisces
77
75
Oracle
Punch Art
Earplug
Dual Wield
Waterwalking

Ivory Rod
Musk Rod
Black Hood
White Robe
Cursed Ring

Blind, Spell Absorb, Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification

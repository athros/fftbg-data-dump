Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MemoriesofFinal
Female
Gemini
49
47
Monk
Dance
Brave Save
Concentrate
Ignore Height



Black Hood
Clothes
Defense Ring

Earth Slash, Purification
Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Obsidian Blade



BaronHaynes
Male
Cancer
61
59
Squire
Summon Magic
Abandon
Magic Defense UP
Move-HP Up

Giant Axe
Diamond Shield
Triangle Hat
Adaman Vest
Battle Boots

Throw Stone, Heal, Tickle, Cheer Up, Wish
Shiva, Ramuh, Ifrit, Titan, Carbunkle, Fairy, Lich



Cryptopsy70
Male
Gemini
46
55
Knight
Sing
Parry
Secret Hunt
Retreat

Save the Queen
Flame Shield
Barbuta
Gold Armor
Power Wrist

Head Break, Armor Break, Shield Break, Magic Break
Life Song, Cheer Song, Magic Song, Nameless Song, Sky Demon



DeathTaxesAndAnime
Female
Taurus
67
54
Mediator
Charge
Counter Flood
Magic Defense UP
Teleport 2

Glacier Gun

Flash Hat
White Robe
Genji Gauntlet

Invitation, Persuade, Praise, Death Sentence
Charge+2, Charge+3

Final Bets: white - 11 bets for 8,697G (47.0%, x1.13); brown - 7 bets for 9,800G (53.0%, x0.89)

white bets:
Zerguzen: 2,000G (23.0%, 11,980G)
NicoSavoy: 2,000G (23.0%, 13,365G)
rocl: 1,000G (11.5%, 25,862G)
xtiltedjedix: 1,000G (11.5%, 1,000G)
BirbBrainsBot: 1,000G (11.5%, 175,202G)
CT_5_Holy: 553G (6.4%, 11,063G)
TheMurkGnome: 500G (5.7%, 2,857G)
RonaldoTheGypsy: 250G (2.9%, 1,473G)
datadrivenbot: 200G (2.3%, 62,009G)
pepperbuster: 100G (1.1%, 100G)
getthemoneyz: 94G (1.1%, 1,783,962G)

brown bets:
BaronHaynes: 5,000G (51.0%, 67,041G)
Leonidusx: 2,000G (20.4%, 8,611G)
thunderducker: 811G (8.3%, 811G)
gorgewall: 688G (7.0%, 688G)
placidphoenix: 656G (6.7%, 656G)
BartTradingCompany: 445G (4.5%, 4,453G)
AbandonedHall: 200G (2.0%, 3,508G)

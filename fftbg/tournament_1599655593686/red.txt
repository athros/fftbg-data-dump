Player: !Red
Team: Red Team
Palettes: Red/Brown



Run With Stone GUNs
Male
Scorpio
37
77
Archer
Throw
Brave Save
Doublehand
Move-MP Up

Windslash Bow

Green Beret
Earth Clothes
Genji Gauntlet

Charge+2, Charge+4, Charge+5
Spear



Lushifah
Male
Cancer
79
70
Priest
Talk Skill
Abandon
Magic Attack UP
Swim

Flail

Flash Hat
Light Robe
Wizard Mantle

Cure 2, Raise, Reraise, Shell 2, Wall, Esuna
Threaten, Insult, Negotiate



Gorgewall
Female
Capricorn
54
43
Priest
Battle Skill
Brave Save
Sicken
Move+1

Rainbow Staff

Headgear
Secret Clothes
Small Mantle

Cure 3, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna
Weapon Break, Speed Break, Power Break, Justice Sword



Willjin
Male
Libra
79
62
Wizard
Item
Abandon
Short Charge
Move+1

Blind Knife

Headgear
Power Sleeve
Magic Ring

Fire, Ice 2, Ice 3, Flare
Potion, Holy Water, Remedy, Phoenix Down

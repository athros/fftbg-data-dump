Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dogsandcatsand
Female
Capricorn
41
49
Wizard
Charge
Meatbone Slash
Short Charge
Retreat

Flame Rod

Green Beret
Chameleon Robe
Bracer

Fire 4, Bolt 2, Bolt 3, Ice, Ice 2, Frog
Charge+1, Charge+3



0v3rr8d
Female
Gemini
43
66
Oracle
Battle Skill
Dragon Spirit
Magic Attack UP
Move-HP Up

Bestiary

Black Hood
Leather Outfit
Rubber Shoes

Poison, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Weapon Break, Mind Break, Justice Sword



Pplvee1
Male
Capricorn
58
76
Knight
Steal
Abandon
Dual Wield
Move+2

Giant Axe
Materia Blade
Barbuta
Chain Mail
Sprint Shoes

Shield Break, Weapon Break, Speed Break, Mind Break
Steal Heart, Steal Shield, Steal Accessory



Brenogarwin
Female
Pisces
67
61
Geomancer
Throw
Distribute
Equip Gun
Jump+1

Mythril Gun
Escutcheon
Headgear
Robe of Lords
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Bomb, Knife, Staff, Axe

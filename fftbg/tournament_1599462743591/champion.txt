Player: !zChamp
Team: Champion Team
Palettes: Green/White



Kronikle
Female
Scorpio
72
48
Calculator
White Magic
Arrow Guard
Equip Sword
Move+3

Mythril Sword

Twist Headband
Black Costume
N-Kai Armlet

CT, 4, 3
Raise, Reraise, Protect 2, Wall, Esuna



Upvla
Male
Cancer
57
80
Monk
Yin Yang Magic
Dragon Spirit
Attack UP
Levitate



Golden Hairpin
Judo Outfit
Rubber Shoes

Spin Fist, Pummel, Earth Slash, Purification, Revive, Seal Evil
Blind, Life Drain, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify



Resjudicata3
Female
Libra
77
77
Chemist
Black Magic
Arrow Guard
Magic Defense UP
Move-MP Up

Panther Bag

Black Hood
Secret Clothes
Elf Mantle

Potion, X-Potion, Ether, Hi-Ether, Soft, Phoenix Down
Fire 2, Bolt 2



AniZero
Female
Pisces
52
72
Mediator
Dance
Counter Magic
Dual Wield
Jump+3

Romanda Gun
Glacier Gun
Black Hood
Linen Robe
Rubber Shoes

Praise, Death Sentence, Negotiate, Refute, Rehabilitate
Wiznaibus, Slow Dance, Void Storage, Dragon Pit

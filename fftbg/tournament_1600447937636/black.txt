Player: !Black
Team: Black Team
Palettes: Black/Red



Firesheath
Female
Scorpio
52
76
Dancer
Summon Magic
Counter Magic
Defense UP
Lava Walking

Ryozan Silk

Flash Hat
Leather Outfit
Defense Ring

Slow Dance, Polka Polka, Disillusion, Void Storage
Ifrit, Silf



SQUiDSQUARKLIN
Monster
Capricorn
60
58
Minotaur










Gelwain
Male
Cancer
64
71
Archer
Sing
Meatbone Slash
Magic Defense UP
Levitate

Ultimus Bow

Barette
Wizard Outfit
Elf Mantle

Charge+1, Charge+3, Charge+7, Charge+20
Angel Song, Battle Song, Sky Demon



Dogsandcatsand
Female
Pisces
67
60
Wizard
Steal
Parry
Short Charge
Fly

Mythril Knife

Leather Hat
Mythril Vest
Dracula Mantle

Fire 2, Bolt 2, Empower, Frog
Gil Taking, Steal Helmet, Steal Status, Leg Aim

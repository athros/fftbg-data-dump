Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Dogsandcatsand
Female
Leo
77
57
Wizard
Draw Out
Counter Tackle
Long Status
Lava Walking

Orichalcum

Headgear
White Robe
Small Mantle

Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 4, Empower
Koutetsu, Murasame



Silentkaster
Male
Pisces
79
47
Priest
Elemental
Catch
Defend
Move-HP Up

Wizard Staff

Holy Miter
Leather Outfit
Magic Ring

Cure, Raise, Reraise, Protect, Protect 2, Esuna
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Daveb
Female
Leo
62
57
Ninja
Yin Yang Magic
Auto Potion
Defense UP
Lava Walking

Koga Knife
Flame Whip
Twist Headband
Brigandine
Magic Gauntlet

Shuriken, Bomb
Pray Faith, Zombie, Dark Holy



Fattunaking
Male
Gemini
54
71
Monk
Talk Skill
Catch
Dual Wield
Jump+2



Holy Miter
Power Sleeve
Magic Ring

Wave Fist, Purification
Persuade, Praise, Insult

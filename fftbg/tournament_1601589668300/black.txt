Player: !Black
Team: Black Team
Palettes: Black/Red



VolgraTheMoose
Female
Scorpio
74
59
Summoner
Punch Art
PA Save
Short Charge
Move-MP Up

Thunder Rod

Flash Hat
Chameleon Robe
Sprint Shoes

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Salamander, Silf
Wave Fist, Purification, Chakra, Revive, Seal Evil



Lythe Caraker
Monster
Gemini
36
63
Porky










EmperorBeef
Male
Sagittarius
71
61
Summoner
Jump
Parry
Short Charge
Waterbreathing

Poison Rod

Holy Miter
Black Robe
Wizard Mantle

Moogle, Ramuh, Golem, Leviathan, Lich
Level Jump5, Vertical Jump5



Sinnyil2
Female
Cancer
59
73
Summoner
Yin Yang Magic
Counter
Secret Hunt
Jump+2

Ice Rod

Twist Headband
Linen Robe
Defense Armlet

Moogle, Shiva, Ramuh, Carbunkle, Lich
Poison, Zombie, Dispel Magic

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CosmicTactician
Female
Taurus
78
65
Time Mage
Elemental
Abandon
Equip Gun
Retreat

Stone Gun

Red Hood
Wizard Outfit
Spike Shoes

Slow 2, Float, Demi 2, Stabilize Time
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



CorpusCav
Female
Taurus
51
60
Chemist
Dance
Faith Save
Dual Wield
Move+3

Hydra Bag
Orichalcum
Thief Hat
Clothes
Red Shoes

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Wiznaibus, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Dragon Pit



Ruleof5
Female
Taurus
61
48
Archer
Steal
Sunken State
Halve MP
Move+3

Ice Bow

Twist Headband
Mythril Vest
Feather Mantle

Charge+1, Charge+4, Charge+10
Steal Heart, Steal Shield, Steal Accessory



Brokenknight201
Male
Taurus
63
56
Knight
Steal
Caution
Equip Knife
Waterbreathing

Hidden Knife
Diamond Shield
Barbuta
Linen Cuirass
Jade Armlet

Weapon Break, Speed Break, Stasis Sword, Night Sword
Steal Armor

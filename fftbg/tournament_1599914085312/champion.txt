Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Lokenwow
Monster
Scorpio
62
57
Ahriman










ColetteMSLP
Female
Aries
68
72
Mediator
White Magic
Counter Magic
Magic Attack UP
Lava Walking

Blaze Gun

Headgear
Light Robe
Defense Ring

Invitation, Persuade, Praise, Threaten, Preach, Mimic Daravon, Refute
Cure, Raise, Shell 2, Wall, Esuna



TheDapperChangeling
Female
Capricorn
78
61
Samurai
White Magic
Sunken State
Dual Wield
Move-HP Up

Mythril Spear
Holy Lance
Leather Helmet
Mythril Armor
Angel Ring

Murasame, Muramasa, Kikuichimoji
Cure 2, Cure 3, Raise, Raise 2, Regen, Shell, Wall, Esuna



Gamesage53
Female
Gemini
79
58
Time Mage
Steal
Caution
Attack UP
Move-HP Up

Sage Staff

Ribbon
White Robe
Sprint Shoes

Haste, Slow, Slow 2, Stop, Quick, Stabilize Time
Steal Armor, Steal Shield, Steal Accessory, Steal Status

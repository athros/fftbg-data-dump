Player: !White
Team: White Team
Palettes: White/Blue



Upvla
Male
Scorpio
42
53
Ninja
Item
PA Save
Throw Item
Teleport

Kunai
Cultist Dagger
Golden Hairpin
Power Sleeve
Diamond Armlet

Shuriken, Staff
Potion, Antidote, Holy Water, Phoenix Down



Aldrammech
Male
Cancer
51
50
Oracle
Talk Skill
MA Save
Equip Axe
Move-HP Up

Battle Axe

Green Beret
Light Robe
Reflect Ring

Spell Absorb, Pray Faith, Confusion Song, Dispel Magic, Dark Holy
Persuade, Death Sentence, Negotiate



Willjin
Male
Libra
61
42
Thief
Punch Art
Meatbone Slash
Sicken
Jump+2

Air Knife

Twist Headband
Mythril Vest
Leather Mantle

Gil Taking, Steal Helmet
Spin Fist, Pummel



E Ballard
Female
Leo
42
55
Dancer
Draw Out
Speed Save
Sicken
Lava Walking

Cashmere

Feather Hat
Secret Clothes
Feather Boots

Slow Dance, Polka Polka, Nameless Dance, Last Dance, Void Storage
Bizen Boat, Muramasa

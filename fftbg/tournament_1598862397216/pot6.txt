Final Bets: white - 10 bets for 7,960G (60.9%, x0.64); brown - 5 bets for 5,107G (39.1%, x1.56)

white bets:
NicoSavoy: 2,000G (25.1%, 4,635G)
Willjin: 2,000G (25.1%, 94,957G)
DeathTaxesAndAnime: 1,000G (12.6%, 101,449G)
BirbBrainsBot: 1,000G (12.6%, 174,640G)
TheMurkGnome: 500G (6.3%, 4,328G)
bruubarg: 500G (6.3%, 31,075G)
Raixelol: 340G (4.3%, 1,394G)
rocl: 220G (2.8%, 28,440G)
LeoNightFury: 204G (2.6%, 204G)
getthemoneyz: 196G (2.5%, 1,784,394G)

brown bets:
iBardic: 4,027G (78.9%, 4,027G)
thunderducker: 496G (9.7%, 496G)
BartTradingCompany: 284G (5.6%, 2,848G)
datadrivenbot: 200G (3.9%, 62,132G)
chronoxtrigger: 100G (2.0%, 1,995G)

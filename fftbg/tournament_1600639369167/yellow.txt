Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



NovaKnight21
Male
Scorpio
61
68
Lancer
Yin Yang Magic
Damage Split
Attack UP
Waterbreathing

Obelisk
Bronze Shield
Circlet
Crystal Mail
Angel Ring

Level Jump8, Vertical Jump8
Blind, Blind Rage, Dispel Magic, Petrify



Powergems
Male
Aquarius
48
63
Archer
Steal
Parry
Doublehand
Jump+3

Night Killer

Black Hood
Judo Outfit
Rubber Shoes

Charge+1, Charge+2, Charge+7
Gil Taking, Steal Heart, Steal Status



Willjin
Male
Sagittarius
69
65
Archer
Steal
Mana Shield
Doublehand
Teleport

Night Killer

Golden Hairpin
Earth Clothes
Angel Ring

Charge+2, Charge+4, Charge+5
Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Arm Aim



HuffFlex
Female
Aquarius
61
74
Dancer
Time Magic
Auto Potion
Short Charge
Swim

Persia

Golden Hairpin
Chain Vest
Sprint Shoes

Wiznaibus, Slow Dance, Disillusion, Last Dance, Void Storage
Haste, Stop

Player: !White
Team: White Team
Palettes: White/Blue



E Ballard
Female
Leo
76
50
Knight
Time Magic
Distribute
Defend
Levitate

Blood Sword
Escutcheon
Circlet
Diamond Armor
Salty Rage

Armor Break, Power Break, Mind Break, Stasis Sword
Stop, Demi 2



Dogsandcatsand
Female
Pisces
55
72
Wizard
Dance
Faith Save
Attack UP
Swim

Dragon Rod

Thief Hat
Linen Robe
Reflect Ring

Fire 2, Fire 4, Bolt 2, Ice 2, Ice 3, Ice 4
Slow Dance, Polka Polka, Dragon Pit



Mindblownnnn
Male
Virgo
63
64
Lancer
Yin Yang Magic
Speed Save
Defend
Jump+3

Javelin
Ice Shield
Diamond Helmet
Leather Armor
Magic Ring

Level Jump4, Vertical Jump6
Blind, Life Drain, Blind Rage, Foxbird, Dispel Magic, Paralyze



Evontno
Male
Cancer
43
68
Thief
Basic Skill
Caution
Equip Bow
Waterwalking

Hunting Bow

Twist Headband
Mystic Vest
Feather Boots

Gil Taking, Steal Helmet, Steal Weapon
Dash, Throw Stone, Heal, Tickle, Fury

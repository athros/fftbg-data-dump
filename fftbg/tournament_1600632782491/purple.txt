Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DaveStrider55
Female
Serpentarius
65
39
Monk
Jump
Brave Save
Equip Bow
Teleport

Ultimus Bow

Holy Miter
Adaman Vest
Battle Boots

Spin Fist, Wave Fist, Purification, Chakra, Revive
Level Jump5, Vertical Jump7



Forkmore
Male
Leo
77
57
Monk
Summon Magic
Counter Magic
Concentrate
Move-HP Up



Leather Hat
Black Costume
Rubber Shoes

Pummel, Purification, Revive
Moogle, Shiva, Ifrit, Salamander, Silf, Fairy, Cyclops



Gobizku
Male
Pisces
61
68
Squire
Jump
HP Restore
Defense UP
Move+2

Mythril Sword
Flame Shield
Circlet
Mythril Vest
Small Mantle

Heal, Fury
Level Jump8, Vertical Jump7



Farseli
Female
Cancer
76
44
Chemist
Dance
Arrow Guard
Equip Knife
Levitate

Ice Rod

Leather Hat
Black Costume
Angel Ring

Antidote, Eye Drop, Phoenix Down
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Last Dance, Obsidian Blade, Void Storage

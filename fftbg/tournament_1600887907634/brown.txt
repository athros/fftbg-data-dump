Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Chuckolator
Female
Virgo
71
51
Calculator
Time Magic
Parry
Dual Wield
Teleport

Ice Rod
Dragon Rod
Triangle Hat
Mystic Vest
Leather Mantle

CT, Height, Prime Number, 5, 4
Haste, Slow, Stop, Immobilize, Float, Stabilize Time



Mirapoix
Male
Aries
42
78
Ninja
Charge
Catch
Sicken
Levitate

Mythril Knife
Cultist Dagger
Ribbon
Mystic Vest
Sprint Shoes

Shuriken, Bomb, Knife, Hammer
Charge+5



Dogsandcatsand
Female
Aquarius
58
61
Wizard
Draw Out
Damage Split
Defend
Move+3

Thunder Rod

Thief Hat
Linen Robe
Wizard Mantle

Fire 4, Bolt, Bolt 2, Ice 3
Murasame, Heaven's Cloud, Muramasa



Itsonlyspencer
Male
Scorpio
39
66
Knight
Sing
MA Save
Martial Arts
Ignore Terrain

Defender
Bronze Shield
Barbuta
Gold Armor
Magic Gauntlet

Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Night Sword
Angel Song, Battle Song, Hydra Pit

Final Bets: red - 7 bets for 8,791G (62.2%, x0.61); blue - 9 bets for 5,337G (37.8%, x1.65)

red bets:
DLJuggernaut: 5,000G (56.9%, 63,229G)
CorpusCav: 1,400G (15.9%, 59,166G)
HaplessOne: 1,111G (12.6%, 26,272G)
Mushufasa_: 500G (5.7%, 21,146G)
Arcblazer23: 380G (4.3%, 380G)
AllInBot: 200G (2.3%, 200G)
datadrivenbot: 200G (2.3%, 83,464G)

blue bets:
BirbBrainsBot: 1,000G (18.7%, 188,057G)
Sairentozon7: 862G (16.2%, 862G)
lowlf: 696G (13.0%, 696G)
Willjin: 600G (11.2%, 33,019G)
ColetteMSLP: 520G (9.7%, 888G)
gorgewall: 501G (9.4%, 4,500G)
Evewho: 500G (9.4%, 19,239G)
2b_yorha_b: 500G (9.4%, 33,780G)
getthemoneyz: 158G (3.0%, 2,200,762G)

Final Bets: red - 8 bets for 10,181G (71.0%, x0.41); yellow - 10 bets for 4,164G (29.0%, x2.45)

red bets:
DLJuggernaut: 5,000G (49.1%, 82,662G)
CorpusCav: 1,499G (14.7%, 60,276G)
Mushufasa_: 1,234G (12.1%, 21,719G)
NovaKnight21: 900G (8.8%, 900G)
HaplessOne: 888G (8.7%, 25,807G)
AllInBot: 360G (3.5%, 360G)
datadrivenbot: 200G (2.0%, 83,268G)
heropong: 100G (1.0%, 1,993G)

yellow bets:
BirbBrainsBot: 1,000G (24.0%, 184,198G)
Willjin: 600G (14.4%, 33,056G)
ColetteMSLP: 520G (12.5%, 937G)
2b_yorha_b: 500G (12.0%, 31,780G)
LeoNightFury: 481G (11.6%, 481G)
Genkidou: 280G (6.7%, 1,287G)
gorgewall: 201G (4.8%, 4,630G)
Evewho: 200G (4.8%, 18,654G)
Zachara: 200G (4.8%, 111,000G)
getthemoneyz: 182G (4.4%, 2,200,099G)

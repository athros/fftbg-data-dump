Final Bets: black - 12 bets for 13,526G (74.9%, x0.33); purple - 7 bets for 4,526G (25.1%, x2.99)

black bets:
DLJuggernaut: 5,000G (37.0%, 84,707G)
HaplessOne: 1,500G (11.1%, 26,170G)
NovaKnight21: 1,268G (9.4%, 1,268G)
Mushufasa_: 1,234G (9.1%, 22,224G)
CorpusCav: 1,200G (8.9%, 60,889G)
Evewho: 1,000G (7.4%, 18,454G)
YowaneHaku: 895G (6.6%, 895G)
Willjin: 600G (4.4%, 32,456G)
Lythe_Caraker: 328G (2.4%, 328G)
gorgewall: 201G (1.5%, 4,429G)
datadrivenbot: 200G (1.5%, 83,350G)
heropong: 100G (0.7%, 2,034G)

purple bets:
seppu777: 1,129G (24.9%, 1,129G)
BirbBrainsBot: 1,000G (22.1%, 183,198G)
ColetteMSLP: 520G (11.5%, 520G)
AllInBot: 507G (11.2%, 507G)
2b_yorha_b: 500G (11.0%, 31,280G)
3ngag3: 456G (10.1%, 17,690G)
getthemoneyz: 414G (9.1%, 2,199,917G)

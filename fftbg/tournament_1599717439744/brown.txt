Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Reddwind
Male
Aquarius
65
49
Knight
Black Magic
Arrow Guard
Attack UP
Jump+2

Defender
Diamond Shield
Platinum Helmet
Chain Mail
Feather Boots

Head Break, Weapon Break, Night Sword
Fire, Fire 3, Bolt 2, Ice



Minorfffanatic1
Female
Capricorn
60
74
Ninja
Elemental
Dragon Spirit
Magic Defense UP
Levitate

Main Gauche
Flail
Green Beret
Clothes
Cursed Ring

Shuriken, Knife, Ninja Sword
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Lava Ball



Raixelol
Female
Virgo
52
61
Thief
Battle Skill
Caution
Equip Gun
Move-HP Up

Romanda Gun

Red Hood
Leather Outfit
Dracula Mantle

Steal Heart, Steal Accessory, Steal Status, Arm Aim
Magic Break, Mind Break



Sairentozon7
Female
Capricorn
54
53
Knight
Item
Sunken State
Throw Item
Move+2

Ice Brand
Mythril Shield
Circlet
Genji Armor
Defense Armlet

Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword
Potion, X-Potion, Remedy

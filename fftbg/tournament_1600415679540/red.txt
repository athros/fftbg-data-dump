Player: !Red
Team: Red Team
Palettes: Red/Brown



Bruubarg
Female
Virgo
46
48
Ninja
Item
PA Save
Concentrate
Ignore Terrain

Sasuke Knife
Short Edge
Leather Hat
Leather Outfit
Magic Gauntlet

Bomb, Knife
Potion, Echo Grass, Holy Water, Phoenix Down



OneHundredFists
Male
Virgo
56
72
Squire
Draw Out
Sunken State
Dual Wield
Ignore Height

Morning Star
Mythril Sword
Genji Helmet
Power Sleeve
Diamond Armlet

Heal
Koutetsu, Heaven's Cloud, Muramasa



Kronikle
Male
Capricorn
43
64
Lancer
Steal
Parry
Doublehand
Move-HP Up

Partisan

Crystal Helmet
Linen Cuirass
Rubber Shoes

Level Jump5, Vertical Jump2
Steal Helmet, Leg Aim



Lowlf
Male
Scorpio
49
70
Time Mage
Sing
Parry
Dual Wield
Waterbreathing

Oak Staff
Wizard Staff
Green Beret
Chameleon Robe
Defense Armlet

Slow 2, Immobilize, Quick, Demi, Demi 2, Stabilize Time, Meteor
Sky Demon

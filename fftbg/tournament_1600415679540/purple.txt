Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Resjudicata3
Female
Aries
80
39
Oracle
Battle Skill
Meatbone Slash
Equip Armor
Waterbreathing

Cypress Rod

Iron Helmet
Crystal Mail
Sprint Shoes

Blind, Poison, Life Drain, Pray Faith, Dispel Magic, Petrify
Mind Break, Stasis Sword, Justice Sword, Night Sword



Ruleof5
Female
Virgo
63
74
Archer
Talk Skill
Arrow Guard
Monster Talk
Move+2

Ultimus Bow

Thief Hat
Adaman Vest
Jade Armlet

Charge+1, Charge+5, Charge+20
Threaten, Insult, Negotiate, Mimic Daravon



Mesmaster
Male
Capricorn
65
44
Monk
Summon Magic
HP Restore
Attack UP
Jump+1



Triangle Hat
Judo Outfit
Jade Armlet

Spin Fist, Pummel, Purification, Revive
Moogle, Titan, Carbunkle, Cyclops



Raixelol
Monster
Aquarius
62
78
Ultima Demon








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Segomod
Female
Scorpio
65
70
Squire
Item
Auto Potion
Equip Knife
Jump+2

Flame Rod
Escutcheon
Feather Hat
Platinum Armor
Red Shoes

Throw Stone, Heal, Cheer Up
Hi-Potion, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Blorpy
Female
Gemini
53
78
Geomancer
White Magic
Parry
Secret Hunt
Waterwalking

Giant Axe
Platinum Shield
Holy Miter
Earth Clothes
Bracer

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Cure 4, Reraise, Shell, Esuna



Reinoe
Male
Capricorn
68
50
Samurai
Black Magic
PA Save
Defense UP
Ignore Height

Kiyomori

Crystal Helmet
Linen Robe
Reflect Ring

Heaven's Cloud, Kikuichimoji
Bolt 2, Bolt 4, Ice 3



Spartan Paladin
Male
Cancer
58
60
Knight
Throw
Meatbone Slash
Equip Polearm
Ignore Terrain

Ryozan Silk
Ice Shield
Genji Helmet
Platinum Armor
Cursed Ring

Shield Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword
Shuriken, Bomb, Staff

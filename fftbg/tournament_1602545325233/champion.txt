Player: !zChamp
Team: Champion Team
Palettes: Green/White



3ngag3
Male
Scorpio
62
54
Thief
Basic Skill
Counter
Equip Shield
Ignore Terrain

Iron Sword
Flame Shield
Triangle Hat
Mystic Vest
Rubber Shoes

Steal Helmet, Steal Accessory, Arm Aim
Heal, Tickle, Cheer Up, Fury



Nizaha
Male
Sagittarius
50
59
Geomancer
White Magic
Dragon Spirit
Concentrate
Lava Walking

Giant Axe
Gold Shield
Headgear
Black Robe
N-Kai Armlet

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Cure, Cure 2, Raise, Shell 2, Wall, Esuna



Widewalk
Female
Serpentarius
48
79
Samurai
Elemental
Meatbone Slash
Equip Shield
Move+2

Asura Knife
Round Shield
Cross Helmet
Black Robe
Dracula Mantle

Murasame, Heaven's Cloud, Muramasa
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Lord Gwarth
Male
Cancer
63
45
Knight
Time Magic
Absorb Used MP
Equip Armor
Move+3

Save the Queen
Genji Shield
Leather Hat
Earth Clothes
Sprint Shoes

Armor Break, Power Break, Dark Sword
Haste, Slow 2, Quick, Demi, Stabilize Time

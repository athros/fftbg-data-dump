Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Male
Capricorn
79
58
Summoner
Punch Art
Counter Tackle
Magic Attack UP
Jump+2

Thunder Rod

Thief Hat
Chameleon Robe
Wizard Mantle

Shiva, Ramuh, Bahamut, Leviathan, Silf
Pummel, Purification, Chakra, Revive



Starfire6789
Male
Leo
57
62
Samurai
Steal
Speed Save
Defense UP
Ignore Terrain

Murasame

Crystal Helmet
Bronze Armor
Battle Boots

Asura, Koutetsu, Kiyomori, Muramasa, Kikuichimoji
Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Ruleof5
Female
Aries
54
49
Archer
Steal
Hamedo
Doublehand
Ignore Height

Stone Gun

Triangle Hat
Adaman Vest
N-Kai Armlet

Charge+3, Charge+5
Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



NicoSavoy
Female
Capricorn
56
74
Monk
Yin Yang Magic
Absorb Used MP
Magic Attack UP
Jump+3



Red Hood
Judo Outfit
Bracer

Pummel, Earth Slash, Secret Fist, Purification, Seal Evil
Blind, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MemoriesofFinal
Female
Capricorn
45
52
Squire
Draw Out
Catch
Equip Sword
Levitate

Blood Sword
Mythril Shield
Triangle Hat
Platinum Armor
Bracer

Throw Stone, Yell, Cheer Up, Wish, Scream
Koutetsu, Bizen Boat, Murasame



Powergems
Male
Leo
55
49
Samurai
Throw
Counter Magic
Magic Defense UP
Move-HP Up

Kiyomori

Circlet
Crystal Mail
Feather Boots

Asura, Koutetsu, Bizen Boat
Shuriken, Spear, Wand



Technominari
Female
Aries
78
60
Priest
Draw Out
Counter Tackle
Equip Knife
Retreat

Sasuke Knife

Headgear
Black Robe
Defense Armlet

Cure, Raise, Raise 2, Protect, Esuna
Asura, Heaven's Cloud



ValensEXP
Female
Virgo
63
49
Priest
Elemental
Dragon Spirit
Equip Knife
Waterbreathing

Thunder Rod

Leather Hat
Black Robe
Defense Ring

Cure 3, Raise, Regen, Protect, Wall, Esuna, Holy
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ArchKnightX
Male
Gemini
73
41
Oracle
Time Magic
Sunken State
Short Charge
Move+2

Madlemgen

Flash Hat
Black Robe
Small Mantle

Poison, Spell Absorb, Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify
Haste, Haste 2, Slow 2, Float, Stabilize Time



RonaldoTheGypsy
Male
Virgo
43
53
Ninja
Talk Skill
Hamedo
Martial Arts
Move-MP Up

Spell Edge
Sasuke Knife
Thief Hat
Rubber Costume
Germinas Boots

Shuriken, Staff
Invitation, Praise, Preach, Death Sentence, Refute



Brokenknight201
Female
Leo
43
55
Dancer
Battle Skill
Faith Save
Dual Wield
Lava Walking

Ryozan Silk
Persia
Golden Hairpin
Clothes
Spike Shoes

Witch Hunt, Disillusion, Nameless Dance, Nether Demon, Dragon Pit
Head Break, Weapon Break, Power Break, Mind Break, Surging Sword



RunicMagus
Female
Leo
48
56
Dancer
Charge
Parry
Equip Sword
Levitate

Kikuichimoji

Leather Hat
Adaman Vest
Dracula Mantle

Witch Hunt, Slow Dance, Nameless Dance, Last Dance, Dragon Pit
Charge+5, Charge+7, Charge+20

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Just Here2
Female
Cancer
41
72
Chemist
Basic Skill
Regenerator
Magic Defense UP
Jump+1

Hydra Bag

Leather Hat
Mythril Vest
Small Mantle

Hi-Potion, Antidote, Remedy, Phoenix Down
Fury



Vorap
Male
Cancer
44
68
Summoner
Time Magic
Counter
Dual Wield
Jump+3

Battle Folio
Bestiary
Leather Hat
Power Sleeve
108 Gems

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Silf
Haste 2



Aldrammech
Female
Virgo
60
80
Monk
Dance
HP Restore
Maintenance
Move-MP Up



Flash Hat
Mystic Vest
Salty Rage

Spin Fist, Secret Fist, Purification, Revive, Seal Evil
Wiznaibus, Disillusion, Dragon Pit



Xignealx
Female
Virgo
47
79
Lancer
Draw Out
Meatbone Slash
Dual Wield
Move+3

Mythril Spear
Holy Lance
Leather Helmet
Gold Armor
Defense Ring

Level Jump4, Vertical Jump5
Heaven's Cloud

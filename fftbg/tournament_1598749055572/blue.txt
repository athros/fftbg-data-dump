Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Firesheath
Male
Virgo
44
77
Wizard
Jump
Brave Save
Equip Sword
Teleport

Kikuichimoji

Leather Hat
White Robe
Cherche

Fire, Bolt 3, Bolt 4, Death
Level Jump8, Vertical Jump5



IBardic
Female
Leo
69
74
Geomancer
Yin Yang Magic
Blade Grasp
Doublehand
Lava Walking

Giant Axe

Thief Hat
Mythril Vest
Power Wrist

Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Blind, Spell Absorb, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Ruleof5
Female
Scorpio
59
49
Archer
Dance
Auto Potion
Secret Hunt
Levitate

Ice Bow

Cachusha
Earth Clothes
Red Shoes

Charge+5, Charge+20
Polka Polka



Reddwind
Male
Aquarius
41
43
Knight
Talk Skill
Critical Quick
Attack UP
Move+1

Long Sword
Platinum Shield
Platinum Helmet
Gold Armor
Magic Gauntlet

Armor Break, Speed Break, Mind Break
Invitation, Persuade, Preach, Negotiate, Refute, Rehabilitate

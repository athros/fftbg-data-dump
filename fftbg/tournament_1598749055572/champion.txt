Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



HASTERIOUS
Female
Leo
77
45
Summoner
Charge
Dragon Spirit
Short Charge
Waterbreathing

Dragon Rod

Holy Miter
Power Sleeve
Feather Boots

Golem, Carbunkle, Salamander
Charge+1, Charge+2, Charge+20



Smashy
Male
Taurus
52
52
Bard
Elemental
Arrow Guard
Dual Wield
Waterbreathing

Windslash Bow

Red Hood
Power Sleeve
Magic Gauntlet

Angel Song, Life Song, Cheer Song, Last Song
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Sairentozon7
Male
Cancer
49
75
Lancer
Summon Magic
Speed Save
Secret Hunt
Move-MP Up

Spear
Gold Shield
Iron Helmet
Leather Armor
Dracula Mantle

Level Jump5, Vertical Jump3
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Fairy



Dogsandcatsand
Female
Aries
63
67
Wizard
Math Skill
PA Save
Short Charge
Move+1

Poison Rod

Triangle Hat
Wizard Outfit
Reflect Ring

Fire 3, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 3, Flare
CT, 5

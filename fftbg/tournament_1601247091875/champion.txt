Player: !zChamp
Team: Champion Team
Palettes: White/Blue



DeathTaxesAndAnime
Female
Libra
70
65
Thief
Punch Art
Caution
Short Status
Fly

Mythril Knife

Triangle Hat
Wizard Outfit
Germinas Boots

Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Wave Fist, Secret Fist, Chakra, Revive



Run With Stone GUNs
Monster
Leo
76
70
Sekhret










Krombobreaker
Female
Leo
51
63
Summoner
Punch Art
Caution
Short Charge
Waterbreathing

Dragon Rod

Leather Hat
Brigandine
Jade Armlet

Moogle, Ifrit, Carbunkle, Leviathan, Silf, Cyclops
Earth Slash, Secret Fist, Purification, Revive



J2DaBibbles
Male
Gemini
46
78
Chemist
Black Magic
Counter
Equip Armor
Jump+1

Cute Bag

Gold Helmet
Adaman Vest
Leather Mantle

Eye Drop, Remedy, Phoenix Down
Bolt 3, Bolt 4, Ice 3, Ice 4, Empower

Player: !Red
Team: Red Team
Palettes: Red/Brown



DeathTaxesAndAnime
Female
Libra
70
73
Thief
Basic Skill
Mana Shield
Defend
Levitate

Air Knife

Headgear
Mystic Vest
Spike Shoes

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Accumulate, Dash, Throw Stone, Heal, Cheer Up, Wish



Dasutin23
Female
Virgo
60
64
Monk
Yin Yang Magic
HP Restore
Concentrate
Move-MP Up



Flash Hat
Clothes
Rubber Shoes

Earth Slash, Purification, Chakra, Revive
Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Sleep, Petrify



Error72
Female
Aquarius
41
40
Calculator
Bird Skill
HP Restore
Equip Shield
Move+3

Battle Folio
Platinum Shield
Barbuta
Leather Outfit
Genji Gauntlet

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



Daveb
Female
Taurus
45
74
Ninja
Talk Skill
Damage Split
Short Status
Fly

Cultist Dagger
Dagger
Twist Headband
Wizard Outfit
Salty Rage

Shuriken, Bomb, Staff, Stick
Praise, Insult, Negotiate, Refute

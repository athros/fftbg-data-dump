Final Bets: brown - 8 bets for 7,937G (47.5%, x1.11); champion - 16 bets for 8,774G (52.5%, x0.90)

brown bets:
BappleTrees: 3,586G (45.2%, 3,586G)
ser_pyrro: 1,135G (14.3%, 1,135G)
BirbBrainsBot: 1,000G (12.6%, 142,517G)
OneHundredFists: 1,000G (12.6%, 10,076G)
rocl: 408G (5.1%, 4,355G)
Neech: 312G (3.9%, 312G)
hotpocketsofficial: 264G (3.3%, 264G)
mindblownnnn: 232G (2.9%, 232G)

champion bets:
DeathTaxesAndAnime: 2,402G (27.4%, 4,710G)
CorpusCav: 1,250G (14.2%, 19,773G)
LivingHitokiri: 1,000G (11.4%, 3,758G)
nifboy: 744G (8.5%, 5,379G)
Raixelol: 600G (6.8%, 18,005G)
seppu777: 424G (4.8%, 424G)
ColetteMSLP: 350G (4.0%, 2,954G)
superdevon1: 328G (3.7%, 328G)
AltimaMantoid: 308G (3.5%, 308G)
radishchef: 300G (3.4%, 3,506G)
ValensEXP_: 296G (3.4%, 8,774G)
MemoriesofFinal: 200G (2.3%, 8,897G)
datadrivenbot: 200G (2.3%, 60,541G)
getthemoneyz: 172G (2.0%, 1,942,361G)
AllInBot: 100G (1.1%, 100G)
BartTradingCompany: 100G (1.1%, 7,836G)

Final Bets: blue - 12 bets for 7,898G (80.3%, x0.24); green - 8 bets for 1,935G (19.7%, x4.08)

blue bets:
NovaKnight21: 2,000G (25.3%, 7,060G)
upvla: 2,000G (25.3%, 5,670G)
AllInBot: 1,264G (16.0%, 1,264G)
HaplessOne: 572G (7.2%, 572G)
lijarkh: 393G (5.0%, 2,261G)
Carledo: 333G (4.2%, 136,340G)
resjudicata3: 332G (4.2%, 332G)
CassiePhoenix: 300G (3.8%, 4,501G)
0v3rr8d: 300G (3.8%, 12,678G)
datadrivenbot: 200G (2.5%, 65,606G)
xsifthewolf: 104G (1.3%, 104G)
Bluebirdzontwitch: 100G (1.3%, 2,262G)

green bets:
IaibOaob: 750G (38.8%, 2,900G)
hotpocketsofficial: 312G (16.1%, 312G)
getthemoneyz: 270G (14.0%, 1,974,677G)
helpimabug: 264G (13.6%, 264G)
MemoriesofFinal: 100G (5.2%, 36,636G)
king_smashington: 100G (5.2%, 810G)
peeronid: 98G (5.1%, 2,821G)
BirbBrainsBot: 41G (2.1%, 54,309G)

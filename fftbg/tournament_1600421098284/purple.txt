Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



PopsLIVE
Female
Pisces
54
78
Monk
White Magic
Faith Save
Equip Bow
Jump+1

Silver Bow

Headgear
Adaman Vest
108 Gems

Pummel, Wave Fist, Secret Fist, Revive, Seal Evil
Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Regen, Protect, Shell 2



Lijarkh
Female
Leo
77
79
Monk
Talk Skill
Abandon
Equip Shield
Levitate


Escutcheon
Red Hood
Judo Outfit
Dracula Mantle

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Persuade, Praise, Solution, Death Sentence, Mimic Daravon, Rehabilitate



Mesmaster
Male
Scorpio
40
61
Samurai
Throw
Auto Potion
Doublehand
Jump+1

Muramasa

Barbuta
Gold Armor
Battle Boots

Heaven's Cloud, Kiyomori
Shuriken, Bomb, Wand, Dictionary



HaplessOne
Male
Taurus
69
48
Knight
Item
Absorb Used MP
Equip Armor
Fly

Defender
Bronze Shield
Flash Hat
Bronze Armor
Magic Gauntlet

Head Break, Magic Break, Mind Break, Dark Sword
Potion, Hi-Potion, Antidote, Soft, Holy Water, Phoenix Down

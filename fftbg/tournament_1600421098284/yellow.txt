Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Helpimabug
Male
Scorpio
69
57
Samurai
Steal
Sunken State
Dual Wield
Jump+1

Mythril Spear
Partisan
Diamond Helmet
Maximillian
Magic Ring

Asura, Koutetsu, Heaven's Cloud
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Leg Aim



IBardic
Male
Sagittarius
49
68
Bard
Throw
Regenerator
Short Status
Jump+2

Ramia Harp

Feather Hat
Carabini Mail
Elf Mantle

Angel Song, Life Song, Battle Song, Nameless Song, Last Song, Sky Demon, Hydra Pit
Wand



ColetteMSLP
Monster
Capricorn
80
42
Plague










Resjudicata3
Female
Gemini
63
78
Samurai
Steal
HP Restore
Equip Sword
Swim

Defender

Genji Helmet
Diamond Armor
Red Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Steal Shield, Steal Accessory, Steal Status, Arm Aim

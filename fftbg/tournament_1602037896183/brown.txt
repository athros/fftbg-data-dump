Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BuffaloCrunch
Male
Pisces
42
64
Squire
Black Magic
MP Restore
Halve MP
Jump+2

Diamond Sword
Mythril Shield
Leather Helmet
Platinum Armor
Sprint Shoes

Dash, Heal, Tickle, Yell, Cheer Up
Bolt, Bolt 2, Bolt 4, Ice, Ice 2



B0shii
Male
Pisces
75
77
Thief
Time Magic
Arrow Guard
Equip Knife
Jump+2

Dagger

Flash Hat
Chain Vest
Small Mantle

Steal Heart, Steal Armor, Leg Aim
Haste, Haste 2, Slow, Slow 2, Stabilize Time



SkylerBunny
Female
Cancer
39
42
Dancer
Punch Art
Counter Flood
Attack UP
Jump+1

Persia

Barette
Wizard Robe
Reflect Ring

Wiznaibus, Polka Polka, Disillusion, Void Storage
Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



GrindFox1
Male
Cancer
51
62
Thief
Battle Skill
Regenerator
Martial Arts
Move-MP Up

Main Gauche

Thief Hat
Rubber Costume
Sprint Shoes

Steal Heart, Steal Armor, Steal Accessory, Leg Aim
Speed Break

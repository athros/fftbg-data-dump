Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nhammen
Female
Virgo
60
50
Geomancer
Talk Skill
Counter Magic
Concentrate
Move+3

Battle Axe
Round Shield
Twist Headband
Silk Robe
Sprint Shoes

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Persuade, Preach, Solution, Negotiate, Mimic Daravon, Refute



E Ballard
Male
Scorpio
54
42
Priest
Battle Skill
Hamedo
Equip Shield
Move+2

Oak Staff
Genji Shield
Feather Hat
Black Costume
Angel Ring

Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Shell 2, Wall, Esuna
Armor Break, Shield Break, Weapon Break, Magic Break



PopsLIVE
Male
Leo
65
45
Mime

Speed Save
Equip Shield
Jump+2


Buckler
Thief Hat
Brigandine
Feather Mantle

Mimic




Kronikle
Male
Taurus
56
78
Priest
Steal
Meatbone Slash
Long Status
Swim

Rainbow Staff

Holy Miter
Light Robe
Wizard Mantle

Cure, Cure 3, Raise, Raise 2, Protect, Shell 2, Wall
Steal Heart, Steal Helmet, Steal Armor, Leg Aim

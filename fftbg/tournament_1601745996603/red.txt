Player: !Red
Team: Red Team
Palettes: Red/Brown



CorpusCav
Monster
Cancer
55
45
Apanda










ExplanationMarkWow
Male
Leo
52
69
Calculator
Yin Yang Magic
Damage Split
Equip Armor
Lava Walking

Ice Rod

Mythril Helmet
Gold Armor
Vanish Mantle

CT, Prime Number, 5
Poison, Foxbird, Dispel Magic



MoonSugarFiend
Male
Libra
73
74
Archer
Summon Magic
Arrow Guard
Attack UP
Fly

Windslash Bow

Gold Helmet
Chain Vest
Rubber Shoes

Charge+1, Charge+7
Moogle, Shiva, Carbunkle, Odin, Salamander, Silf, Fairy



Sireaulou
Female
Cancer
79
39
Knight
Black Magic
Damage Split
Long Status
Waterbreathing

Broad Sword
Flame Shield
Genji Helmet
Platinum Armor
Elf Mantle

Armor Break, Weapon Break, Power Break, Stasis Sword, Justice Sword
Fire, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice 2, Frog, Death

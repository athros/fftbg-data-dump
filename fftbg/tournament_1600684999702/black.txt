Player: !Black
Team: Black Team
Palettes: Black/Red



E Ballard
Female
Scorpio
61
61
Thief
Jump
Speed Save
Maintenance
Waterwalking

Blind Knife

Barette
Leather Outfit
Genji Gauntlet

Gil Taking, Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim
Level Jump3, Vertical Jump2



Forkmore
Female
Libra
58
58
Summoner
Draw Out
Mana Shield
Attack UP
Move-HP Up

Wizard Rod

Golden Hairpin
Light Robe
Magic Gauntlet

Moogle, Titan, Leviathan, Silf, Fairy
Asura, Koutetsu, Bizen Boat



SeniorBunk
Female
Aquarius
43
63
Mime

Counter Magic
Defend
Move-MP Up



Feather Hat
Light Robe
Small Mantle

Mimic




VolgraTheMoose
Male
Scorpio
43
52
Lancer
Black Magic
Parry
Attack UP
Move-MP Up

Holy Lance
Diamond Shield
Genji Helmet
Mythril Armor
Sprint Shoes

Level Jump2, Vertical Jump6
Fire 2, Fire 3, Bolt 3, Ice, Ice 4

Player: !Green
Team: Green Team
Palettes: Green/White



Reddwind
Female
Libra
48
53
Knight
Throw
Counter Magic
Magic Defense UP
Jump+3

Defender
Platinum Shield
Barbuta
White Robe
Red Shoes

Weapon Break, Speed Break, Stasis Sword, Night Sword
Shuriken, Knife, Stick



Khelor
Male
Libra
73
50
Bard
Steal
MA Save
Doublehand
Move-HP Up

Ramia Harp

Red Hood
Mythril Armor
Wizard Mantle

Nameless Song, Diamond Blade, Space Storage, Sky Demon, Hydra Pit
Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Leg Aim



Zephyrik
Female
Taurus
44
47
Knight
Summon Magic
Hamedo
Martial Arts
Waterwalking


Genji Shield
Gold Helmet
Carabini Mail
Feather Mantle

Magic Break, Speed Break, Power Break, Explosion Sword
Moogle, Ifrit, Golem, Silf, Fairy



TheSabretoothe
Monster
Aquarius
56
61
Revenant








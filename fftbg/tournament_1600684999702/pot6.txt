Final Bets: white - 5 bets for 1,848G (36.0%, x1.77); purple - 8 bets for 3,280G (64.0%, x0.56)

white bets:
BirbBrainsBot: 1,000G (54.1%, 190,996G)
OneHundredFists: 528G (28.6%, 528G)
AllInBot: 200G (10.8%, 200G)
lwtest: 100G (5.4%, 5,104G)
getthemoneyz: 20G (1.1%, 2,019,145G)

purple bets:
Error72: 1,001G (30.5%, 27,557G)
ser_pyrro: 582G (17.7%, 582G)
Rytor: 516G (15.7%, 516G)
ColetteMSLP: 400G (12.2%, 3,247G)
otakutaylor: 280G (8.5%, 280G)
MemoriesofFinal: 200G (6.1%, 30,928G)
datadrivenbot: 200G (6.1%, 71,268G)
gorgewall: 101G (3.1%, 2,620G)

Player: !White
Team: White Team
Palettes: White/Blue



Chuckolator
Female
Leo
77
39
Priest
Math Skill
Dragon Spirit
Equip Bow
Waterwalking

Bow Gun

Flash Hat
Silk Robe
Leather Mantle

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna, Magic Barrier
Height, Prime Number, 3



Victoriolue
Male
Scorpio
46
61
Thief
Throw
HP Restore
Defend
Swim

Mythril Knife

Golden Hairpin
Rubber Costume
Leather Mantle

Steal Heart, Steal Armor, Steal Shield, Leg Aim
Wand



Jabuamba
Female
Aquarius
68
55
Archer
White Magic
Counter
Secret Hunt
Move+3

Stone Gun
Escutcheon
Green Beret
Earth Clothes
Power Wrist

Charge+2, Charge+3, Charge+4, Charge+7, Charge+10, Charge+20
Cure, Cure 4, Raise, Shell, Wall, Esuna



DaveStrider55
Monster
Leo
58
52
Grenade








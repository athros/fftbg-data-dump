Player: !Red
Team: Red Team
Palettes: Red/Brown



Reddwind
Male
Taurus
51
46
Knight
Punch Art
Auto Potion
Magic Defense UP
Jump+1

Slasher
Escutcheon
Leather Helmet
Platinum Armor
Jade Armlet

Armor Break, Shield Break, Speed Break, Mind Break
Wave Fist, Purification, Seal Evil



J 52
Female
Scorpio
52
68
Samurai
Yin Yang Magic
Abandon
Sicken
Move-MP Up

Heaven's Cloud

Crystal Helmet
Black Robe
Genji Gauntlet

Asura, Bizen Boat, Heaven's Cloud, Kikuichimoji
Blind, Poison, Dispel Magic, Paralyze, Dark Holy



Gingerfoo69
Female
Capricorn
49
71
Time Mage
Punch Art
Counter Flood
Halve MP
Lava Walking

Oak Staff

Holy Miter
White Robe
Red Shoes

Haste, Haste 2, Slow, Stop, Reflect, Demi, Demi 2, Stabilize Time
Pummel, Secret Fist, Purification, Chakra, Seal Evil



Hotpocketsofficial
Female
Taurus
70
72
Mediator
Item
HP Restore
Halve MP
Waterbreathing

Romanda Gun

Headgear
Silk Robe
Chantage

Invitation, Praise, Threaten, Solution, Death Sentence, Mimic Daravon, Refute
Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Silentkaster
Male
Aries
76
64
Mime

HP Restore
Dual Wield
Jump+3



Leather Hat
Black Robe
Dracula Mantle

Mimic




SQUiDSQUARKLIN
Male
Capricorn
55
57
Ninja
Basic Skill
Counter Magic
Equip Sword
Lava Walking

Kunai
Asura Knife
Feather Hat
Secret Clothes
Sprint Shoes

Shuriken, Knife, Wand
Throw Stone, Heal, Cheer Up



Ribbiks
Female
Taurus
44
65
Samurai
Black Magic
Damage Split
Attack UP
Teleport

Asura Knife

Diamond Helmet
Carabini Mail
Small Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Fire, Fire 2, Fire 3, Bolt, Bolt 2, Ice



CosmicTactician
Female
Virgo
80
52
Time Mage
Draw Out
Auto Potion
Equip Armor
Lava Walking

Octagon Rod

Genji Helmet
Judo Outfit
Bracer

Haste, Slow 2, Stop, Quick
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji

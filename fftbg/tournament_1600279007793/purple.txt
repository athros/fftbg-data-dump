Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CapnChaos12
Male
Aries
70
62
Monk
Elemental
Meatbone Slash
Equip Bow
Lava Walking

Snipe Bow

Black Hood
Earth Clothes
Bracer

Pummel, Secret Fist, Purification, Chakra, Revive
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Kellios11
Female
Leo
46
73
Oracle
Summon Magic
Faith Save
Defense UP
Levitate

Cypress Rod

Twist Headband
Rubber Costume
108 Gems

Blind, Spell Absorb, Zombie, Silence Song, Blind Rage, Confusion Song, Paralyze
Moogle, Shiva, Titan, Carbunkle, Odin, Salamander, Silf



Nizaha
Male
Aquarius
63
50
Thief
Draw Out
Distribute
Dual Wield
Move-HP Up

Air Knife
Main Gauche
Red Hood
Brigandine
Feather Boots

Steal Helmet, Steal Status, Leg Aim
Asura



Actual JP
Female
Leo
39
72
Ninja
Talk Skill
Brave Save
Equip Shield
Levitate

Spell Edge
Crystal Shield
Holy Miter
Mystic Vest
Germinas Boots

Stick
Threaten, Preach, Solution, Refute

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ArrenJevleth
Female
Aries
67
51
Dancer
Talk Skill
Counter
Monster Talk
Waterbreathing

Ryozan Silk

Thief Hat
Power Sleeve
Red Shoes

Wiznaibus, Obsidian Blade
Invitation, Preach, Refute, Rehabilitate



Powergems
Female
Capricorn
78
78
Archer
Elemental
Critical Quick
Doublehand
Move-HP Up

Long Bow

Leather Hat
Earth Clothes
Feather Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7
Local Quake, Static Shock, Sand Storm, Gusty Wind



AbandonedHall
Female
Taurus
65
50
Lancer
Elemental
Absorb Used MP
Martial Arts
Levitate


Aegis Shield
Gold Helmet
Mythril Armor
Sprint Shoes

Level Jump4, Vertical Jump6
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Rytor
Female
Virgo
48
69
Dancer
Elemental
Distribute
Equip Sword
Ignore Terrain

Ragnarok

Twist Headband
Adaman Vest
Rubber Shoes

Slow Dance, Polka Polka, Disillusion
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Meta Five
Male
Scorpio
48
50
Summoner
Steal
Dragon Spirit
Sicken
Move-HP Up

Poison Rod

Golden Hairpin
Chameleon Robe
Power Wrist

Moogle, Carbunkle, Odin, Leviathan
Steal Heart, Steal Shield, Steal Accessory, Arm Aim



Chuckolator
Female
Aries
65
49
Knight
White Magic
Counter Tackle
Equip Knife
Move+1

Rod
Escutcheon
Genji Helmet
Linen Robe
Feather Mantle

Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Cure, Raise, Reraise, Protect 2



Killth3kid
Male
Taurus
46
43
Calculator
Marsh Skill
MP Restore
Short Charge
Move+3

Ivory Rod

Barette
Platinum Armor
Spike Shoes

Blue Magic
Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



Upvla
Male
Aries
43
76
Ninja
Item
Auto Potion
Doublehand
Waterbreathing

Kunai

Thief Hat
Black Costume
Feather Boots

Staff
Potion, X-Potion, Phoenix Down

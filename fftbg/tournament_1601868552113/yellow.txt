Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galkife
Monster
Scorpio
65
80
Great Malboro










Meta Five
Female
Pisces
79
53
Dancer
Charge
Abandon
Long Status
Move+2

Persia

Golden Hairpin
Mystic Vest
Angel Ring

Polka Polka, Last Dance, Nether Demon
Charge+1, Charge+5, Charge+7, Charge+10, Charge+20



OneHundredFists
Female
Scorpio
64
71
Thief
White Magic
Distribute
Halve MP
Jump+2

Spell Edge

Red Hood
Wizard Outfit
Power Wrist

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Cure 3, Regen, Protect, Shell, Wall, Esuna, Holy



KasugaiRoastedPeas
Female
Scorpio
51
78
Dancer
Item
Parry
Magic Defense UP
Retreat

Cashmere

Feather Hat
Chain Vest
Bracer

Polka Polka, Disillusion, Last Dance, Void Storage, Nether Demon
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy

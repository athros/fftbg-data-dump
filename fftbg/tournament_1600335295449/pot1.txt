Final Bets: red - 8 bets for 5,191G (43.3%, x1.31); blue - 11 bets for 6,811G (56.7%, x0.76)

red bets:
AllInBot: 1,367G (26.3%, 1,367G)
BirbBrainsBot: 1,000G (19.3%, 7,646G)
DaveStrider55: 801G (15.4%, 801G)
nok____: 566G (10.9%, 566G)
Mushufasa_: 500G (9.6%, 47,486G)
Songdeh: 461G (8.9%, 461G)
ser_pyrro: 396G (7.6%, 396G)
xsifthewolf: 100G (1.9%, 447G)

blue bets:
bruubarg: 3,700G (54.3%, 3,700G)
getthemoneyz: 706G (10.4%, 1,961,405G)
Raixelol: 600G (8.8%, 8,455G)
rocl: 420G (6.2%, 6,607G)
3ngag3: 340G (5.0%, 15,109G)
Neech: 320G (4.7%, 386G)
PuzzleSecretary: 225G (3.3%, 430G)
datadrivenbot: 200G (2.9%, 63,443G)
BartTradingCompany: 100G (1.5%, 10,763G)
DeathTaxesAndAnime: 100G (1.5%, 21,076G)
VolgraTheMoose: 100G (1.5%, 3,285G)

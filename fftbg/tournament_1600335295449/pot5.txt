Final Bets: red - 9 bets for 4,203G (49.3%, x1.03); green - 8 bets for 4,328G (50.7%, x0.97)

red bets:
NovaKnight21: 1,324G (31.5%, 1,324G)
Songdeh: 1,000G (23.8%, 2,254G)
Mushufasa_: 500G (11.9%, 49,858G)
DaveStrider55: 400G (9.5%, 3,963G)
PuzzleSecretary: 250G (5.9%, 426G)
ImmortalMage: 229G (5.4%, 229G)
MemoriesofFinal: 200G (4.8%, 28,657G)
datadrivenbot: 200G (4.8%, 63,001G)
xsifthewolf: 100G (2.4%, 578G)

green bets:
BirbBrainsBot: 1,000G (23.1%, 9,512G)
AllInBot: 908G (21.0%, 908G)
getthemoneyz: 666G (15.4%, 1,962,651G)
Raixelol: 600G (13.9%, 11,211G)
seppu777: 555G (12.8%, 26,773G)
Rune339: 399G (9.2%, 8,383G)
BartTradingCompany: 100G (2.3%, 10,507G)
lijarkh: 100G (2.3%, 2,142G)

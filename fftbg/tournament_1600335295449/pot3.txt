Final Bets: white - 7 bets for 3,000G (44.2%, x1.26); black - 11 bets for 3,787G (55.8%, x0.79)

white bets:
bruubarg: 1,100G (36.7%, 1,100G)
hotpocketsofficial: 800G (26.7%, 800G)
Raixelol: 600G (20.0%, 11,311G)
Songdeh: 200G (6.7%, 2,154G)
BartTradingCompany: 100G (3.3%, 10,707G)
victoriolue: 100G (3.3%, 94,044G)
ImmortalMage: 100G (3.3%, 329G)

black bets:
BirbBrainsBot: 1,000G (26.4%, 9,232G)
DaveStrider55: 600G (15.8%, 2,488G)
Mushufasa_: 500G (13.2%, 49,362G)
rocl: 420G (11.1%, 6,267G)
3ngag3: 340G (9.0%, 14,929G)
PuzzleSecretary: 333G (8.8%, 333G)
AllInBot: 200G (5.3%, 200G)
datadrivenbot: 200G (5.3%, 63,043G)
MemoriesofFinal: 100G (2.6%, 28,378G)
formerlydrdong: 50G (1.3%, 228G)
getthemoneyz: 44G (1.2%, 1,961,139G)

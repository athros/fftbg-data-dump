Player: !Black
Team: Black Team
Palettes: Black/Red



Krombobreaker
Male
Leo
47
53
Priest
Charge
Parry
Equip Bow
Fly

Hunting Bow

Ribbon
Chain Vest
Magic Ring

Cure, Raise, Raise 2, Reraise, Regen, Shell, Esuna, Holy
Charge+2, Charge+3, Charge+4, Charge+7, Charge+10



Murderclan
Male
Capricorn
68
62
Samurai
Throw
Brave Save
Secret Hunt
Ignore Terrain

Murasame

Bronze Helmet
Genji Armor
Spike Shoes

Koutetsu, Bizen Boat, Murasame, Muramasa
Bomb, Knife



Arcblazer23
Female
Taurus
59
58
Squire
Steal
Regenerator
Defend
Move-MP Up

Snipe Bow
Buckler
Red Hood
Crystal Mail
Diamond Armlet

Accumulate, Dash, Throw Stone, Cheer Up, Wish
Steal Accessory, Leg Aim



DrAntiSocial
Female
Virgo
62
56
Priest
Steal
Damage Split
Equip Knife
Move-MP Up

Faith Rod

Cachusha
Black Robe
Magic Ring

Cure 2, Raise 2, Reraise, Protect 2, Wall, Esuna
Steal Armor

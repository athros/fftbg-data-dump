Player: !Red
Team: Red Team
Palettes: Red/Brown



Firesheath
Female
Aquarius
41
51
Priest
Summon Magic
Damage Split
Concentrate
Move+1

Flame Whip

Green Beret
Wizard Robe
Power Wrist

Cure, Cure 4, Raise, Raise 2, Protect 2, Shell, Esuna
Carbunkle, Bahamut, Odin, Fairy



ANFz
Male
Scorpio
70
67
Geomancer
Black Magic
Brave Save
Equip Axe
Ignore Terrain

Slasher
Bronze Shield
Leather Hat
Wizard Robe
Chantage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind
Ice 2



CorpusCav
Male
Cancer
70
48
Samurai
Yin Yang Magic
Brave Save
Beastmaster
Move+1

Koutetsu Knife

Mythril Helmet
Black Robe
Vanish Mantle

Asura
Blind Rage, Foxbird, Confusion Song, Sleep



ALY327
Female
Cancer
65
64
Squire
Punch Art
Mana Shield
Secret Hunt
Move+2

Broad Sword
Platinum Shield
Triangle Hat
Crystal Mail
108 Gems

Accumulate, Dash, Tickle
Spin Fist, Secret Fist, Chakra

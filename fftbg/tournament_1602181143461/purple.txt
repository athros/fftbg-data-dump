Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ruleof5
Female
Virgo
46
69
Archer
Steal
Caution
Martial Arts
Teleport


Aegis Shield
Black Hood
Secret Clothes
Leather Mantle

Charge+3, Charge+5, Charge+20
Gil Taking, Steal Status, Leg Aim



Killth3kid
Male
Pisces
65
55
Lancer
Sing
Brave Save
Concentrate
Waterwalking

Obelisk
Diamond Shield
Gold Helmet
Mythril Armor
Genji Gauntlet

Level Jump4, Vertical Jump8
Angel Song, Magic Song, Nameless Song



E Ballard
Male
Scorpio
37
64
Summoner
Item
Brave Save
Dual Wield
Lava Walking

Thunder Rod
Flame Rod
Feather Hat
Clothes
Germinas Boots

Ramuh, Titan, Carbunkle, Leviathan, Salamander
Ether, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Ogema TheDream
Female
Taurus
53
51
Time Mage
Throw
Parry
Long Status
Jump+1

White Staff

Feather Hat
Chain Vest
Leather Mantle

Slow, Slow 2, Stop, Quick, Demi, Demi 2, Stabilize Time, Meteor
Knife, Axe

Final Bets: red - 25 bets for 24,346G (79.6%, x0.26); blue - 8 bets for 6,226G (20.4%, x3.91)

red bets:
DavenIII: 4,310G (17.7%, 21,554G)
gorgewall: 3,595G (14.8%, 3,595G)
skipsandwiches: 2,020G (8.3%, 108,958G)
Nizaha: 1,764G (7.2%, 7,057G)
ValensEXP_: 1,697G (7.0%, 1,697G)
VolgraTheMoose: 1,650G (6.8%, 3,300G)
maakur_: 1,508G (6.2%, 1,508G)
Lord_Gwarth: 1,000G (4.1%, 4,516G)
DarrenDinosaurs: 911G (3.7%, 911G)
holdenmagronik: 794G (3.3%, 794G)
Zagorsek: 588G (2.4%, 588G)
CosmicTactician: 581G (2.4%, 581G)
krombobreaker: 500G (2.1%, 37,937G)
AnthroMetal: 500G (2.1%, 13,523G)
2b_yorha_b: 500G (2.1%, 30,548G)
Mushufasa_: 500G (2.1%, 41,086G)
old_overholt_: 500G (2.1%, 30,922G)
Cryptopsy70: 424G (1.7%, 749G)
Musashi45: 304G (1.2%, 1,269G)
datadrivenbot: 200G (0.8%, 83,514G)
BirbBrainsBot: 193G (0.8%, 189,618G)
Firesheath: 100G (0.4%, 2,809G)
heropong: 100G (0.4%, 2,005G)
FuzzyTigers: 100G (0.4%, 717G)
Chuckolator: 7G (0.0%, 2,503G)

blue bets:
E_Ballard: 2,627G (42.2%, 2,627G)
EnemyController: 1,111G (17.8%, 1,846,432G)
lowlf: 662G (10.6%, 662G)
SephDarkheart: 592G (9.5%, 7,524G)
tinytittyfiend: 458G (7.4%, 458G)
CapnChaos12: 300G (4.8%, 13,902G)
getthemoneyz: 276G (4.4%, 2,201,530G)
AllInBot: 200G (3.2%, 200G)

Player: !White
Team: White Team
Palettes: White/Blue



Soren Of Tyto
Male
Virgo
68
42
Mediator
Summon Magic
HP Restore
Doublehand
Jump+2

Blast Gun

Barette
Light Robe
Red Shoes

Invitation, Persuade, Preach, Solution, Negotiate, Mimic Daravon, Refute
Moogle, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Cyclops



Phi Sig
Male
Cancer
55
70
Calculator
Black Magic
Auto Potion
Equip Knife
Jump+3

Ice Rod

Feather Hat
White Robe
Magic Gauntlet

CT, Height, Prime Number, 5, 4
Fire 2, Bolt 3, Ice 3, Ice 4, Empower



Poorest Hobo
Male
Sagittarius
81
75
Wizard
Steal
Distribute
Dual Wield
Move-MP Up

Rod
Rod
Golden Hairpin
Robe of Lords
Bracer

Fire 2, Fire 4, Ice 4, Empower, Frog
Steal Heart



3ngag3
Female
Aries
64
75
Geomancer
Battle Skill
Parry
Sicken
Waterwalking

Sleep Sword
Buckler
Leather Hat
Chain Vest
Small Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Magic Break, Power Break, Stasis Sword

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BooshPlays
Male
Aquarius
47
54
Mime

Abandon
Magic Defense UP
Waterbreathing



Triangle Hat
Mythril Vest
Rubber Shoes

Mimic




KerikXI
Female
Leo
65
47
Calculator
Time Magic
MA Save
Equip Axe
Teleport 2

Slasher

Flash Hat
White Robe
Power Wrist

CT, Height, Prime Number, 4
Slow 2, Immobilize, Quick, Stabilize Time



MemoriesofFinal
Male
Leo
49
71
Geomancer
Steal
Faith Save
Defend
Teleport

Murasame
Platinum Shield
Triangle Hat
Judo Outfit
Vanish Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon



VolgraTheMoose
Male
Capricorn
49
70
Archer
Time Magic
Sunken State
Doublehand
Ignore Terrain

Stone Gun

Green Beret
Mystic Vest
Leather Mantle

Charge+1, Charge+2
Slow 2, Stop, Float, Demi 2

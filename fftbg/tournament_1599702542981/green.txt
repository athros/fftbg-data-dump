Player: !Green
Team: Green Team
Palettes: Green/White



Upvla
Male
Taurus
63
59
Monk
Charge
Damage Split
Magic Attack UP
Ignore Height



Leather Hat
Adaman Vest
Magic Gauntlet

Earth Slash, Purification, Chakra, Revive
Charge+5, Charge+7



ALY327
Monster
Scorpio
44
44
Gobbledeguck










Zeroroute
Male
Aquarius
53
59
Time Mage
Throw
MP Restore
Short Charge
Swim

Musk Rod

Flash Hat
Silk Robe
Leather Mantle

Haste 2, Stop, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Shuriken, Bomb



Redmage4evah
Male
Scorpio
72
59
Wizard
Item
Faith Save
Throw Item
Move+3

Poison Rod

Green Beret
Power Sleeve
Leather Mantle

Fire 3, Bolt 4, Ice 3, Death
Potion, X-Potion, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down

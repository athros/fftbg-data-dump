Player: !Red
Team: Red Team
Palettes: Red/Brown



Thyrandaal
Female
Taurus
74
56
Time Mage
Draw Out
MA Save
Equip Armor
Move-MP Up

Rainbow Staff

Circlet
Linen Robe
Bracer

Haste, Float
Asura, Bizen Boat, Muramasa



ColetteMSLP
Male
Taurus
47
48
Ninja
Black Magic
Distribute
Equip Gun
Jump+3

Papyrus Codex
Bloody Strings
Red Hood
Adaman Vest
Leather Mantle

Shuriken
Fire 4, Bolt, Bolt 3, Ice, Ice 2, Ice 3, Frog



Wonser
Male
Sagittarius
78
74
Bard
Summon Magic
Damage Split
Equip Axe
Jump+1

Sage Staff

Feather Hat
Diamond Armor
Small Mantle

Angel Song, Life Song, Diamond Blade, Sky Demon, Hydra Pit
Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Fairy, Cyclops



Lyonslegacy
Female
Scorpio
78
48
Knight
Yin Yang Magic
Faith Save
Defend
Jump+2

Slasher
Ice Shield
Barbuta
Light Robe
Feather Boots

Armor Break, Shield Break, Speed Break, Power Break, Mind Break, Dark Sword
Blind, Life Drain, Blind Rage, Confusion Song, Dark Holy

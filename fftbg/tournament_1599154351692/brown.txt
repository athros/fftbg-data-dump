Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Arumz
Male
Sagittarius
58
69
Time Mage
Steal
MA Save
Short Status
Fly

Wizard Staff

Twist Headband
Earth Clothes
Bracer

Haste 2, Stop, Float, Quick, Demi, Stabilize Time
Steal Heart



CapnChaos12
Female
Cancer
56
62
Samurai
Jump
Blade Grasp
Defense UP
Move+2

Spear

Platinum Helmet
Bronze Armor
Power Wrist

Bizen Boat, Kikuichimoji
Level Jump8, Vertical Jump2



DavenIII
Male
Leo
78
57
Archer
Battle Skill
Blade Grasp
Equip Sword
Swim

Ragnarok

Flash Hat
Mystic Vest
Bracer

Charge+1, Charge+2, Charge+4, Charge+5, Charge+20
Armor Break, Speed Break, Stasis Sword



Brokenknight201
Female
Scorpio
68
58
Geomancer
Draw Out
Parry
Equip Bow
Fly

Silver Bow

Holy Miter
Chain Vest
Feather Boots

Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind
Murasame, Muramasa

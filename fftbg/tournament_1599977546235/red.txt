Player: !Red
Team: Red Team
Palettes: Red/Brown



ALY327
Male
Aries
67
70
Lancer
Elemental
Dragon Spirit
Equip Gun
Fly

Bestiary
Escutcheon
Mythril Helmet
Reflect Mail
Cursed Ring

Level Jump4, Vertical Jump5
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Krombobreaker
Male
Cancer
58
63
Chemist
Yin Yang Magic
Counter
Doublehand
Move+2

Hydra Bag

Golden Hairpin
Adaman Vest
Power Wrist

Potion, Hi-Potion, Holy Water, Phoenix Down
Blind, Poison, Doubt Faith, Zombie, Foxbird, Dark Holy



Upvla
Female
Aries
60
55
Geomancer
Time Magic
Abandon
Doublehand
Fly

Mythril Sword

Flash Hat
Mythril Vest
Diamond Armlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Haste, Stop, Demi 2, Meteor



InskipJester
Male
Leo
57
46
Geomancer
Charge
PA Save
Equip Axe
Waterbreathing

Battle Axe
Platinum Shield
Flash Hat
Mystic Vest
Power Wrist

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm
Charge+2, Charge+3, Charge+7, Charge+20

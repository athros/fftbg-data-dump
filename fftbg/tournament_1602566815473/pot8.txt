Final Bets: white - 16 bets for 8,417G (38.8%, x1.58); champion - 16 bets for 13,265G (61.2%, x0.63)

white bets:
CorpusCav: 2,000G (23.8%, 97,380G)
BirbBrainsBot: 1,000G (11.9%, 148,226G)
squideater_: 1,000G (11.9%, 14,543G)
MoonSugarFiend: 761G (9.0%, 761G)
getthemoneyz: 552G (6.6%, 2,235,294G)
Treafa: 500G (5.9%, 6,343G)
duckfacebro: 432G (5.1%, 432G)
superdevon1: 404G (4.8%, 6,079G)
JethroThrul: 400G (4.8%, 400G)
verythinice: 323G (3.8%, 323G)
Magicandy: 322G (3.8%, 16,838G)
seppu777: 222G (2.6%, 7,463G)
AllInBot: 200G (2.4%, 200G)
Jehudy7: 200G (2.4%, 2,210G)
J_52: 100G (1.2%, 2,399G)
Chuckolator: 1G (0.0%, 3,816G)

champion bets:
Zetchryn: 4,016G (30.3%, 4,016G)
ruleof5: 2,069G (15.6%, 2,069G)
YowaneHaku: 1,500G (11.3%, 5,186G)
HorusTaurus: 1,220G (9.2%, 1,220G)
Snowfats: 1,000G (7.5%, 62,477G)
Creggers: 920G (6.9%, 920G)
DLJuggernaut: 673G (5.1%, 673G)
Lydian_C: 560G (4.2%, 5,088G)
kilrazin: 220G (1.7%, 220G)
resjudicata3: 200G (1.5%, 5,831G)
datadrivenbot: 200G (1.5%, 84,486G)
NicoSavoy: 200G (1.5%, 189,062G)
porkchop5158: 164G (1.2%, 164G)
LuckyLuckLuc2: 123G (0.9%, 1,996G)
Rytor: 100G (0.8%, 6,649G)
TheBrett: 100G (0.8%, 18,110G)

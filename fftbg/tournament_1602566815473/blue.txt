Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powergems
Female
Taurus
66
39
Knight
Elemental
Sunken State
Defend
Teleport

Long Sword
Mythril Shield
Genji Helmet
Crystal Mail
Magic Ring

Armor Break, Power Break, Mind Break, Stasis Sword, Explosion Sword
Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



DLJuggernaut
Monster
Libra
39
68
Red Panther










ALY327
Female
Libra
68
79
Thief
Throw
Brave Save
Beastmaster
Ignore Terrain

Sasuke Knife

Thief Hat
Wizard Outfit
Angel Ring

Steal Heart
Shuriken, Knife



CorpusCav
Female
Gemini
48
41
Chemist
Time Magic
Critical Quick
Dual Wield
Teleport

Cute Bag
Blind Knife
Green Beret
Power Sleeve
Feather Mantle

Hi-Potion, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Quick, Meteor

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lolthsmeat
Monster
Aries
74
78
Red Chocobo










SephDarkheart
Female
Sagittarius
63
74
Squire
Summon Magic
Distribute
Defend
Retreat

Battle Axe
Crystal Shield
Bronze Helmet
Mystic Vest
Spike Shoes

Accumulate, Heal, Yell, Cheer Up, Fury
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Silf, Fairy, Cyclops



MemoriesofFinal
Male
Virgo
81
79
Thief
White Magic
PA Save
Attack UP
Jump+3

Spell Edge

Holy Miter
Judo Outfit
Red Shoes

Gil Taking, Steal Heart, Steal Shield, Steal Weapon
Protect, Shell, Wall



IaibOaob
Male
Aries
60
44
Knight
Steal
Speed Save
Magic Attack UP
Move+1

Ragnarok
Gold Shield
Iron Helmet
Chameleon Robe
Elf Mantle

Head Break, Shield Break, Speed Break
Steal Helmet, Steal Shield, Steal Weapon

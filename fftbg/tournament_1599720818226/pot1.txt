Final Bets: red - 12 bets for 6,212G (60.8%, x0.64); blue - 8 bets for 4,000G (39.2%, x1.55)

red bets:
NicoSavoy: 2,000G (32.2%, 31,622G)
BirbBrainsBot: 1,000G (16.1%, 173,711G)
MrUbiq: 600G (9.7%, 11,769G)
Raixelol: 580G (9.3%, 24,065G)
lowlf: 496G (8.0%, 3,516G)
ApplesNP: 328G (5.3%, 328G)
bigbongsmoker: 308G (5.0%, 3,074G)
evdoggity: 300G (4.8%, 300G)
iBardic: 200G (3.2%, 18,751G)
datadrivenbot: 200G (3.2%, 57,195G)
LadyCyan: 100G (1.6%, 100G)
minorfffanatic1: 100G (1.6%, 751G)

blue bets:
BappleTrees: 2,000G (50.0%, 22,756G)
DeathTaxesAndAnime: 1,151G (28.8%, 1,151G)
ColetteMSLP: 400G (10.0%, 3,642G)
aceof86: 200G (5.0%, 2,437G)
getthemoneyz: 120G (3.0%, 1,904,805G)
AllInBot: 100G (2.5%, 100G)
BartTradingCompany: 28G (0.7%, 280G)
Chuckolator: 1G (0.0%, 25,332G)

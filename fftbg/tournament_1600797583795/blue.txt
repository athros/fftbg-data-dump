Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Firesheath
Male
Aries
69
61
Calculator
Black Magic
Meatbone Slash
Equip Knife
Ignore Height

Orichalcum

Triangle Hat
Linen Robe
Sprint Shoes

Height, Prime Number, 4
Fire 4, Bolt 2, Ice, Ice 2, Ice 3, Ice 4



StealthModeLocke
Male
Taurus
45
70
Summoner
Basic Skill
Brave Save
Maintenance
Move-MP Up

Gold Staff

Holy Miter
Adaman Vest
Rubber Shoes

Ifrit, Carbunkle, Silf
Dash, Heal, Tickle, Cheer Up, Fury, Wish



Maakur
Male
Capricorn
50
54
Lancer
Elemental
Sunken State
Defend
Move+3

Holy Lance
Flame Shield
Iron Helmet
Diamond Armor
108 Gems

Level Jump8, Vertical Jump5
Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball



Mirapoix
Male
Gemini
42
44
Lancer
Draw Out
Auto Potion
Long Status
Jump+1

Octagon Rod
Crystal Shield
Platinum Helmet
Linen Cuirass
Reflect Ring

Level Jump5, Vertical Jump4
Koutetsu, Muramasa

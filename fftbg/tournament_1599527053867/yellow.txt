Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lord Gwarth
Male
Capricorn
75
48
Calculator
Undeath Skill
Brave Save
Magic Defense UP
Swim

Gokuu Rod

Crystal Helmet
Crystal Mail
Bracer

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Worldly Unbrain
Female
Virgo
77
76
Archer
Draw Out
PA Save
Secret Hunt
Lava Walking

Night Killer
Round Shield
Platinum Helmet
Mythril Vest
Reflect Ring

Charge+1, Charge+4, Charge+5, Charge+10
Koutetsu, Murasame, Heaven's Cloud



Seppu777
Female
Capricorn
59
50
Geomancer
Draw Out
Damage Split
Short Charge
Move+3

Coral Sword
Escutcheon
Red Hood
Wizard Robe
Battle Boots

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Asura, Bizen Boat



BooshPlays
Female
Leo
73
76
Thief
Elemental
Absorb Used MP
Magic Attack UP
Move-MP Up

Main Gauche

Holy Miter
Earth Clothes
Rubber Shoes

Steal Heart, Steal Accessory
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball

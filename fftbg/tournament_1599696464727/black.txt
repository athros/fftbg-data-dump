Player: !Black
Team: Black Team
Palettes: Black/Red



E Ballard
Male
Aries
75
60
Knight
White Magic
Caution
Equip Sword
Levitate

Muramasa
Gold Shield
Bronze Helmet
Carabini Mail
Genji Gauntlet

Head Break, Armor Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Surging Sword, Explosion Sword
Cure 2, Protect 2, Wall, Esuna



Nhammen
Female
Aquarius
80
44
Time Mage
Basic Skill
Abandon
Equip Shield
Retreat

Gold Staff
Platinum Shield
Holy Miter
Judo Outfit
Dracula Mantle

Slow, Stop, Immobilize, Float, Reflect, Stabilize Time
Accumulate, Throw Stone, Heal, Yell, Cheer Up



DavenIII
Male
Sagittarius
81
60
Archer
Draw Out
Counter Magic
Maintenance
Jump+3

Glacier Gun
Flame Shield
Feather Hat
Adaman Vest
Sprint Shoes

Charge+3, Charge+5
Murasame, Kikuichimoji



Acid Flashback
Female
Sagittarius
75
68
Priest
Talk Skill
Parry
Martial Arts
Waterbreathing



Twist Headband
Chain Vest
Power Wrist

Cure, Raise, Protect, Protect 2, Shell, Esuna, Holy
Invitation, Persuade, Threaten, Death Sentence, Negotiate

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zachara
Male
Gemini
53
59
Lancer
Draw Out
Dragon Spirit
Doublehand
Move+2

Iron Fan

Circlet
Diamond Armor
Feather Mantle

Level Jump3, Vertical Jump8
Asura, Heaven's Cloud



Lord Gwarth
Male
Leo
63
71
Knight
Elemental
Damage Split
Equip Bow
Jump+1

Ice Bow
Platinum Shield
Gold Helmet
Black Robe
Battle Boots

Power Break, Surging Sword
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



ShadowNinja25
Male
Cancer
61
46
Calculator
Black Magic
Abandon
Equip Shield
Retreat

Papyrus Codex
Buckler
Red Hood
Leather Outfit
Wizard Mantle

CT, 4, 3
Fire, Bolt, Bolt 4, Empower, Flare



Killth3kid
Male
Libra
78
72
Archer
Basic Skill
Critical Quick
Equip Gun
Retreat

Papyrus Codex
Venetian Shield
Leather Helmet
Secret Clothes
N-Kai Armlet

Charge+1, Charge+4, Charge+7, Charge+20
Heal, Tickle, Wish

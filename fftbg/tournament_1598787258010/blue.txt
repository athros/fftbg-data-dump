Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SephDarkheart
Male
Gemini
43
67
Summoner
Black Magic
Meatbone Slash
Defense UP
Move-HP Up

Battle Folio

Feather Hat
Light Robe
Magic Ring

Shiva, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Fairy, Lich
Fire 2, Fire 3, Bolt 4, Ice, Ice 3, Flare



Mesmaster
Male
Aquarius
68
47
Knight
Summon Magic
Critical Quick
Magic Attack UP
Swim

Slasher
Diamond Shield
Circlet
Linen Robe
Setiemson

Speed Break
Moogle, Golem, Carbunkle



Absalom 20
Male
Aquarius
63
42
Bard
Elemental
Mana Shield
Sicken
Move+2

Fairy Harp

Triangle Hat
Mystic Vest
Power Wrist

Life Song, Last Song, Hydra Pit
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Lava Ball



Ko2q
Male
Pisces
72
77
Mime

Auto Potion
Attack UP
Teleport



Diamond Helmet
Platinum Armor
Germinas Boots

Mimic


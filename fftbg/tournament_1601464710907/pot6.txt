Final Bets: black - 10 bets for 4,130G (34.3%, x1.92); purple - 6 bets for 7,924G (65.7%, x0.52)

black bets:
BirbBrainsBot: 1,000G (24.2%, 99,421G)
AllInBot: 586G (14.2%, 586G)
TheSabretoothe: 553G (13.4%, 553G)
krombobreaker: 500G (12.1%, 17,193G)
pplvee1: 448G (10.8%, 448G)
Lolthsmeat: 354G (8.6%, 354G)
VynxYukida: 249G (6.0%, 249G)
datadrivenbot: 200G (4.8%, 82,347G)
Sharosa: 140G (3.4%, 140G)
readdesert: 100G (2.4%, 974G)

purple bets:
HaplessOne: 5,246G (66.2%, 10,287G)
Ruvelia_BibeI: 1,082G (13.7%, 1,082G)
dogsandcatsand: 784G (9.9%, 10,228G)
SephDarkheart: 444G (5.6%, 444G)
CosmicTactician: 200G (2.5%, 42,608G)
getthemoneyz: 168G (2.1%, 2,112,333G)

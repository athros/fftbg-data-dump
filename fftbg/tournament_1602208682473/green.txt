Player: !Green
Team: Green Team
Palettes: Green/White



2b Yorha B
Female
Aquarius
57
54
Knight
Dance
PA Save
Equip Knife
Move-MP Up

Hidden Knife
Ice Shield
Diamond Helmet
Plate Mail
Vanish Mantle

Head Break, Justice Sword
Witch Hunt, Nameless Dance, Nether Demon



Diyahressem
Monster
Taurus
44
56
Holy Dragon










Lowlf
Female
Taurus
61
79
Oracle
Dance
Abandon
Equip Armor
Waterbreathing

Iron Fan

Iron Helmet
Wizard Outfit
Defense Armlet

Blind, Poison, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep
Wiznaibus, Dragon Pit



Ko2q
Female
Scorpio
71
48
Samurai
Time Magic
Distribute
Magic Attack UP
Jump+1

Kiyomori

Gold Helmet
Carabini Mail
Sprint Shoes

Koutetsu, Bizen Boat, Murasame
Haste 2, Stop, Immobilize, Reflect, Stabilize Time

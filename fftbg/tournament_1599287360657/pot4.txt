Final Bets: purple - 12 bets for 6,948G (38.8%, x1.58); brown - 7 bets for 10,966G (61.2%, x0.63)

purple bets:
Greggernaut: 3,000G (43.2%, 33,488G)
EnemyController: 1,111G (16.0%, 1,690,115G)
DLJuggernaut: 769G (11.1%, 769G)
Raixelol: 504G (7.3%, 11,964G)
Treafa: 388G (5.6%, 388G)
Runeseeker22: 264G (3.8%, 264G)
bigbongsmoker: 244G (3.5%, 5,684G)
MemoriesofFinal: 200G (2.9%, 2,136G)
datadrivenbot: 200G (2.9%, 57,349G)
FriendlySkeleton: 128G (1.8%, 1,250G)
CorpusCav: 100G (1.4%, 9,208G)
BirbBrainsBot: 40G (0.6%, 152,661G)

brown bets:
sinnyil2: 4,305G (39.3%, 8,611G)
HaateXIII: 1,999G (18.2%, 1,999G)
NicoSavoy: 1,136G (10.4%, 1,136G)
BartTradingCompany: 1,000G (9.1%, 11,512G)
Thyrandaal: 1,000G (9.1%, 78,905G)
Leonidusx: 842G (7.7%, 842G)
getthemoneyz: 684G (6.2%, 1,823,528G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Laserman1000
Male
Cancer
56
51
Geomancer
Item
PA Save
Throw Item
Move+1

Coral Sword
Mythril Shield
Green Beret
Secret Clothes
108 Gems

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Potion, Antidote



Sinnyil2
Female
Capricorn
54
58
Calculator
Yin Yang Magic
Catch
Magic Attack UP
Move-HP Up

Battle Folio

Green Beret
Light Robe
Leather Mantle

CT, Height, Prime Number, 4, 3
Silence Song, Sleep



Byrdturbo
Male
Serpentarius
46
78
Wizard
Draw Out
Counter Flood
Short Charge
Levitate

Rod

Green Beret
Robe of Lords
Leather Mantle

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Empower
Muramasa, Kikuichimoji



BoneMiser
Female
Taurus
44
49
Knight
Charge
Damage Split
Short Status
Waterbreathing

Slasher
Gold Shield
Cross Helmet
Platinum Armor
Jade Armlet

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Justice Sword
Charge+10

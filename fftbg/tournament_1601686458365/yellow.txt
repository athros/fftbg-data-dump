Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DeathTaxesAndAnime
Female
Scorpio
45
76
Thief
Summon Magic
Counter Tackle
Magic Defense UP
Jump+1

Koga Knife

Green Beret
Mythril Vest
Angel Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Moogle, Ifrit, Carbunkle



Catfeath3r
Male
Serpentarius
73
75
Geomancer
White Magic
PA Save
Defense UP
Move+2

Giant Axe
Platinum Shield
Flash Hat
Mythril Vest
Rubber Shoes

Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Raise 2, Shell 2, Esuna, Magic Barrier



ALY327
Male
Serpentarius
59
62
Mime

HP Restore
Martial Arts
Move+1


Mythril Shield
Golden Hairpin
Mystic Vest
Red Shoes

Mimic




Acid Flashback
Male
Scorpio
65
80
Thief
Talk Skill
Counter Magic
Magic Defense UP
Move-HP Up

Kunai

Green Beret
Wizard Outfit
Elf Mantle

Steal Armor, Steal Shield, Steal Weapon
Invitation, Persuade, Insult, Mimic Daravon, Refute

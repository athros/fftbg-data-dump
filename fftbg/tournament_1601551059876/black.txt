Player: !Black
Team: Black Team
Palettes: Black/Red



Bigbongsmoker
Female
Aquarius
66
55
Oracle
Basic Skill
Earplug
Equip Bow
Fly

Night Killer

Green Beret
Linen Robe
Germinas Boots

Spell Absorb, Life Drain, Doubt Faith, Dispel Magic, Sleep, Petrify
Dash, Heal, Cheer Up



DeathTaxesAndAnime
Female
Aries
60
44
Thief
Battle Skill
Blade Grasp
Doublehand
Move+2

Long Sword

Triangle Hat
Adaman Vest
Defense Armlet

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Weapon Break, Magic Break, Speed Break, Power Break, Surging Sword



Dogsandcatsand
Female
Leo
64
72
Wizard
Throw
Counter Flood
Defense UP
Move-HP Up

Rod

Headgear
Black Robe
Magic Gauntlet

Fire, Fire 2, Fire 4, Ice, Ice 3
Shuriken, Wand



Rica
Female
Leo
78
60
Oracle
Draw Out
Faith Save
Beastmaster
Jump+1

Gokuu Rod

Holy Miter
Light Robe
Cursed Ring

Poison, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Murasame, Muramasa

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CorpusCav
Female
Aquarius
52
41
Chemist
Battle Skill
Brave Save
Short Status
Move-HP Up

Romanda Gun

Black Hood
Adaman Vest
N-Kai Armlet

Potion, Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Power Break, Mind Break, Explosion Sword



StealthModeLocke
Female
Cancer
44
73
Monk
Draw Out
Counter Tackle
Secret Hunt
Move+2



Feather Hat
Black Costume
Feather Boots

Pummel, Secret Fist, Purification, Revive, Seal Evil
Koutetsu, Heaven's Cloud, Kiyomori



CosmicTactician
Female
Scorpio
68
78
Time Mage
Throw
Regenerator
Short Charge
Jump+1

White Staff

Triangle Hat
Black Robe
Chantage

Stop, Quick, Demi
Shuriken, Bomb



Chuckolator
Female
Virgo
62
71
Calculator
White Magic
Counter Flood
Equip Sword
Move+1

Ragnarok

Golden Hairpin
Leather Outfit
Feather Boots

Height, Prime Number, 5
Cure 2, Cure 3, Raise, Reraise, Regen, Protect, Protect 2, Shell, Shell 2, Esuna

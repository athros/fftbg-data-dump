Final Bets: red - 8 bets for 3,920G (46.3%, x1.16); blue - 10 bets for 4,554G (53.7%, x0.86)

red bets:
BirbBrainsBot: 1,000G (25.5%, 185,194G)
Deathmaker06: 1,000G (25.5%, 36,114G)
Nizaha: 652G (16.6%, 7,313G)
holdenmagronik: 492G (12.6%, 5,149G)
AllInBot: 258G (6.6%, 258G)
2b_yorha_b: 200G (5.1%, 1,488G)
acid_flashback: 200G (5.1%, 4,045G)
getthemoneyz: 118G (3.0%, 2,131,109G)

blue bets:
RunicMagus: 1,020G (22.4%, 25,779G)
just_here2: 516G (11.3%, 16,039G)
technominari: 500G (11.0%, 87,271G)
regularguy428: 500G (11.0%, 778G)
king_smashington: 498G (10.9%, 498G)
Cryptopsy70: 404G (8.9%, 539G)
CorpusCav: 400G (8.8%, 30,213G)
Lolthsmeat: 316G (6.9%, 2,764G)
AnthroMetal: 200G (4.4%, 1,024G)
datadrivenbot: 200G (4.4%, 81,034G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Phi Sig
Male
Pisces
47
80
Summoner
Time Magic
Dragon Spirit
Magic Attack UP
Retreat

Gold Staff

Holy Miter
Brigandine
Feather Boots

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Lich, Cyclops
Haste, Slow, Slow 2, Immobilize, Reflect, Demi, Demi 2, Stabilize Time



Gorgewall
Female
Aquarius
66
45
Calculator
White Magic
Faith Save
Short Charge
Ignore Terrain

Bestiary

Barette
Adaman Vest
Sprint Shoes

CT, Prime Number, 4
Cure, Cure 2, Cure 3, Cure 4, Raise, Shell, Esuna, Holy



Helpimabug
Female
Sagittarius
66
55
Samurai
Black Magic
Auto Potion
Equip Bow
Move+3

Windslash Bow

Mythril Helmet
Reflect Mail
Germinas Boots

Koutetsu, Murasame, Heaven's Cloud, Kiyomori
Fire 2, Ice, Ice 4



Lolthsmeat
Female
Libra
52
63
Wizard
Draw Out
Distribute
Doublehand
Ignore Height

Wizard Rod

Triangle Hat
Clothes
Germinas Boots

Fire, Fire 3, Bolt, Bolt 2, Ice 3
Koutetsu

Final Bets: purple - 11 bets for 5,320G (40.8%, x1.45); brown - 8 bets for 7,714G (59.2%, x0.69)

purple bets:
Skyridge0: 1,500G (28.2%, 45,998G)
reinoe: 1,000G (18.8%, 43,609G)
NicoSavoy: 528G (9.9%, 528G)
resjudicata3: 500G (9.4%, 4,409G)
ColetteMSLP: 476G (8.9%, 476G)
Lolthsmeat: 296G (5.6%, 1,515G)
Tithonus: 280G (5.3%, 280G)
Ruvelia_BibeI: 236G (4.4%, 236G)
AllInBot: 200G (3.8%, 200G)
datadrivenbot: 200G (3.8%, 78,333G)
grimthegameking: 104G (2.0%, 104G)

brown bets:
LivingHitokiri: 2,000G (25.9%, 176,288G)
randgridr: 1,552G (20.1%, 1,552G)
Zachara: 1,500G (19.4%, 135,500G)
BirbBrainsBot: 1,000G (13.0%, 136,133G)
GeNoFPaniC: 734G (9.5%, 734G)
pepperbuster: 500G (6.5%, 24,000G)
getthemoneyz: 228G (3.0%, 2,128,458G)
ValensEXP_: 200G (2.6%, 9,310G)

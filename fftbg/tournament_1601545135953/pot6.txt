Final Bets: white - 8 bets for 4,685G (59.4%, x0.68); purple - 5 bets for 3,200G (40.6%, x1.46)

white bets:
Lolthsmeat: 1,944G (41.5%, 1,944G)
BirbBrainsBot: 1,000G (21.3%, 135,536G)
pepperbuster: 911G (19.4%, 24,911G)
getthemoneyz: 226G (4.8%, 2,127,716G)
datadrivenbot: 200G (4.3%, 78,423G)
Genkidou: 200G (4.3%, 1,279G)
grimthegameking: 104G (2.2%, 104G)
Sharosa: 100G (2.1%, 1,737G)

purple bets:
reinoe: 2,000G (62.5%, 45,462G)
LivingHitokiri: 500G (15.6%, 175,094G)
ColetteMSLP: 300G (9.4%, 866G)
AllInBot: 200G (6.2%, 200G)
resjudicata3: 200G (6.2%, 4,934G)

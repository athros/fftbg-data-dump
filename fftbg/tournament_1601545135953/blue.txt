Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Pepperbuster
Male
Sagittarius
53
52
Monk
Draw Out
MA Save
Equip Gun
Move+2

Stone Gun

Flash Hat
Judo Outfit
Defense Ring

Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Koutetsu, Bizen Boat, Murasame, Muramasa



Gorgewall
Female
Gemini
53
81
Chemist
Punch Art
HP Restore
Secret Hunt
Ignore Height

Mythril Gun

Triangle Hat
Brigandine
Bracer

Potion, Hi-Potion, Antidote, Holy Water, Remedy, Phoenix Down
Secret Fist, Purification, Chakra, Revive



LivingHitokiri
Female
Scorpio
46
80
Monk
Battle Skill
Arrow Guard
Maintenance
Levitate



Flash Hat
Power Sleeve
N-Kai Armlet

Spin Fist, Earth Slash, Chakra, Revive
Power Break, Surging Sword



NovaKnight21
Male
Scorpio
78
57
Knight
Time Magic
Auto Potion
Dual Wield
Ignore Terrain

Defender
Diamond Sword
Leather Helmet
Black Robe
Leather Mantle

Head Break, Armor Break, Stasis Sword, Justice Sword, Night Sword
Slow, Immobilize, Reflect, Quick, Demi 2, Stabilize Time

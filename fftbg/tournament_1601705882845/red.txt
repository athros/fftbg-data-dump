Player: !Red
Team: Red Team
Palettes: Red/Brown



Gelwain
Male
Taurus
73
53
Wizard
Elemental
Counter Flood
Equip Axe
Move-MP Up

White Staff

Headgear
Mystic Vest
Defense Ring

Fire, Fire 3, Bolt 2, Ice 4, Flare
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp



ALY327
Monster
Aries
76
68
Pisco Demon










RikiNom
Female
Gemini
62
72
Wizard
Talk Skill
Damage Split
Equip Polearm
Retreat

Javelin

Triangle Hat
Wizard Robe
Germinas Boots

Fire 3, Fire 4, Bolt 2, Ice, Empower, Death
Threaten, Insult, Refute



NovaKnight21
Female
Sagittarius
42
77
Ninja
Item
MA Save
Magic Defense UP
Jump+2

Sasuke Knife
Short Edge
Thief Hat
Judo Outfit
Angel Ring

Shuriken, Knife, Wand
Potion, X-Potion, Antidote, Soft, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



VolgraTheMoose
Male
Capricorn
52
59
Ninja
Talk Skill
Arrow Guard
Monster Talk
Move-MP Up

Short Edge
Assassin Dagger
Golden Hairpin
Mystic Vest
Sprint Shoes

Bomb, Sword, Wand
Invitation, Persuade, Negotiate, Mimic Daravon



CorpusCav
Female
Capricorn
70
48
Chemist
Elemental
MA Save
Magic Attack UP
Retreat

Star Bag

Feather Hat
Leather Outfit
Defense Armlet

Potion, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand



King Smashington
Male
Capricorn
70
59
Ninja
Battle Skill
Parry
Maintenance
Levitate

Morning Star
Kunai
Leather Hat
Brigandine
Leather Mantle

Shuriken, Bomb
Armor Break, Shield Break, Weapon Break, Magic Break



Dogsandcatsand
Female
Leo
80
63
Wizard
Dance
Arrow Guard
Equip Polearm
Ignore Height

Mythril Spear

Barette
Earth Clothes
Wizard Mantle

Fire, Fire 2, Fire 4, Ice, Ice 2
Slow Dance, Polka Polka

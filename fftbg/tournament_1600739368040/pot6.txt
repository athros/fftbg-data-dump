Final Bets: black - 9 bets for 6,647G (46.6%, x1.14); purple - 13 bets for 7,604G (53.4%, x0.87)

black bets:
roofiepops: 1,369G (20.6%, 1,369G)
gorgewall: 1,324G (19.9%, 1,324G)
resjudicata3: 940G (14.1%, 1,845G)
silentkaster: 764G (11.5%, 764G)
Runeseeker22: 630G (9.5%, 630G)
killth3kid: 624G (9.4%, 624G)
krombobreaker: 532G (8.0%, 20,197G)
ValensEXP_: 364G (5.5%, 1,495G)
nhammen: 100G (1.5%, 265,551G)

purple bets:
AllInBot: 1,103G (14.5%, 1,103G)
Mushufasa_: 1,000G (13.2%, 71,543G)
BirbBrainsBot: 1,000G (13.2%, 12,199G)
Shalloween: 1,000G (13.2%, 16,593G)
getthemoneyz: 914G (12.0%, 2,031,330G)
DanielRKnealeArt: 607G (8.0%, 607G)
CorpusCav: 420G (5.5%, 6,982G)
3ngag3: 360G (4.7%, 2,301G)
robespyah: 300G (3.9%, 300G)
KinniQuacks: 300G (3.9%, 7,228G)
ZephXrotH: 300G (3.9%, 344G)
ribbiks: 200G (2.6%, 4,373G)
scsuperstar: 100G (1.3%, 1,840G)

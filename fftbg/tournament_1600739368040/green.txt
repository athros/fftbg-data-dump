Player: !Green
Team: Green Team
Palettes: Green/White



0v3rr8d
Male
Scorpio
57
58
Thief
Black Magic
Catch
Sicken
Retreat

Cultist Dagger

Headgear
Black Costume
Magic Gauntlet

Steal Heart, Steal Armor, Steal Shield
Fire, Bolt 2, Bolt 3, Bolt 4, Ice 3



Gorgewall
Male
Libra
57
63
Lancer
Time Magic
Distribute
Martial Arts
Lava Walking

Musk Rod
Crystal Shield
Iron Helmet
Carabini Mail
Magic Ring

Level Jump2, Vertical Jump7
Haste 2, Slow, Float, Reflect, Quick, Demi 2



MemoriesofFinal
Female
Sagittarius
55
67
Mime

Damage Split
Equip Shield
Move+2


Platinum Shield
Twist Headband
Black Robe
Magic Ring

Mimic




OneHundredFists
Male
Leo
57
64
Monk
Basic Skill
Hamedo
Halve MP
Jump+3



Flash Hat
Power Sleeve
Battle Boots

Pummel, Secret Fist, Chakra, Seal Evil
Accumulate, Heal, Cheer Up, Fury

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evewho
Female
Capricorn
76
67
Samurai
Steal
Blade Grasp
Magic Attack UP
Waterwalking

Koutetsu Knife

Cross Helmet
Diamond Armor
Elf Mantle

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim



Rune339
Male
Gemini
69
56
Lancer
Battle Skill
Abandon
Equip Armor
Lava Walking

Obelisk
Bronze Shield
Barette
Plate Mail
Sprint Shoes

Level Jump5, Vertical Jump8
Shield Break, Power Break, Mind Break, Justice Sword



Ar Tactic
Male
Capricorn
62
69
Knight
Black Magic
Catch
Equip Gun
Ignore Terrain

Stone Gun
Mythril Shield
Circlet
Gold Armor
Feather Mantle

Head Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Night Sword
Fire, Bolt 4, Ice, Ice 4



MemoriesofFinal
Male
Libra
79
53
Lancer
Black Magic
Absorb Used MP
Dual Wield
Move-MP Up

Obelisk
Partisan
Barbuta
Leather Armor
Wizard Mantle

Level Jump2, Vertical Jump7
Fire 2, Fire 3, Ice, Ice 2, Ice 4, Death

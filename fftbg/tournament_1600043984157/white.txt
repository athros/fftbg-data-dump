Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Female
Leo
50
67
Thief
Battle Skill
MA Save
Attack UP
Waterbreathing

Spell Edge

Headgear
Black Costume
Diamond Armlet

Steal Heart, Steal Weapon
Head Break, Armor Break, Speed Break



Galkife
Male
Leo
55
79
Priest
Time Magic
MP Restore
Halve MP
Move+2

White Staff

Flash Hat
Adaman Vest
Germinas Boots

Cure, Raise, Wall, Esuna
Haste 2, Slow, Demi, Demi 2, Meteor



Tripaplex
Female
Scorpio
78
48
Summoner
Charge
MA Save
Defense UP
Jump+3

Thunder Rod

Feather Hat
Wizard Outfit
Red Shoes

Moogle, Shiva, Ifrit, Golem, Leviathan, Salamander
Charge+2, Charge+3, Charge+4, Charge+10, Charge+20



Aceof86
Male
Capricorn
63
67
Squire
Item
Earplug
Equip Knife
Waterbreathing

Ice Rod
Crystal Shield
Black Hood
Chain Vest
Power Wrist

Accumulate, Dash, Throw Stone, Fury, Scream
Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down

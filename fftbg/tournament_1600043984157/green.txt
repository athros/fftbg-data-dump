Player: !Green
Team: Green Team
Palettes: Green/White



Lythe Caraker
Female
Aquarius
46
43
Geomancer
Basic Skill
Caution
Short Charge
Ignore Terrain

Battle Axe
Kaiser Plate
Black Hood
Mythril Vest
N-Kai Armlet

Pitfall, Hell Ivy, Quicksand, Gusty Wind, Lava Ball
Accumulate, Dash, Heal, Yell



CrazyLou64
Male
Taurus
49
70
Geomancer
Yin Yang Magic
Abandon
Defense UP
Move-HP Up

Asura Knife
Gold Shield
Headgear
Light Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Blind Rage, Foxbird, Confusion Song



Redmage4evah
Female
Virgo
69
72
Ninja
Item
Meatbone Slash
Equip Polearm
Levitate

Battle Bamboo
Battle Bamboo
Black Hood
Wizard Outfit
Bracer

Shuriken, Sword, Axe
Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Remedy, Phoenix Down



Kellios11
Female
Sagittarius
77
79
Mime

Caution
Martial Arts
Move+1



Diamond Helmet
Power Sleeve
Angel Ring

Mimic


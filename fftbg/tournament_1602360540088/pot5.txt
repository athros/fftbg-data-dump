Final Bets: blue - 14 bets for 8,275G (68.3%, x0.46); yellow - 10 bets for 3,839G (31.7%, x2.16)

blue bets:
CorpusCav: 2,000G (24.2%, 93,563G)
SeniorBunk: 1,000G (12.1%, 99,638G)
murderclan: 819G (9.9%, 819G)
Demon_Lord_Josh: 688G (8.3%, 688G)
Sairentozon7: 676G (8.2%, 676G)
Willjin: 600G (7.3%, 29,969G)
krombobreaker: 500G (6.0%, 34,300G)
Breakdown777: 500G (6.0%, 2,871G)
Laserman1000: 500G (6.0%, 5,300G)
otakutaylor: 340G (4.1%, 340G)
getthemoneyz: 252G (3.0%, 2,214,865G)
datadrivenbot: 200G (2.4%, 84,044G)
Tithonus: 100G (1.2%, 934G)
FuzzyTigers: 100G (1.2%, 591G)

yellow bets:
E_Ballard: 988G (25.7%, 988G)
AllInBot: 582G (15.2%, 582G)
ValensEXP_: 512G (13.3%, 21,834G)
FriendSquirrel: 416G (10.8%, 416G)
Shalloween: 400G (10.4%, 88,135G)
DrAntiSocial: 388G (10.1%, 388G)
Evewho: 200G (5.2%, 70,585G)
BirbBrainsBot: 153G (4.0%, 56,424G)
Runeseeker22: 100G (2.6%, 33,199G)
CosmicTactician: 100G (2.6%, 2,807G)

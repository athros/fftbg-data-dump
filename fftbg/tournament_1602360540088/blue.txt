Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Murderclan
Monster
Aquarius
68
77
Red Dragon










SeniorBunk
Male
Sagittarius
52
60
Calculator
Time Magic
Critical Quick
Long Status
Move+3

Dragon Rod

Golden Hairpin
Linen Robe
N-Kai Armlet

CT, Prime Number
Haste, Haste 2, Slow, Slow 2, Reflect, Quick, Demi 2, Stabilize Time



Willjin
Male
Taurus
47
52
Monk
Elemental
Mana Shield
Short Status
Move+1

Star Bag

Triangle Hat
Power Sleeve
Angel Ring

Pummel, Earth Slash, Purification, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



Laserman1000
Male
Scorpio
49
64
Calculator
Demon Skill
Counter
Defense UP
Levitate

Bestiary
Diamond Shield
Triangle Hat
Linen Robe
Sprint Shoes

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2

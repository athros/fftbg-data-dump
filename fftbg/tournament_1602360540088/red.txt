Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Male
Virgo
61
40
Monk
White Magic
Hamedo
Equip Knife
Move-HP Up

Mage Masher

Golden Hairpin
Mystic Vest
Jade Armlet

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Cure, Cure 3, Raise, Reraise, Protect 2



Silentkaster
Female
Aries
54
42
Calculator
Time Magic
Parry
Halve MP
Jump+2

Papyrus Codex

Black Hood
White Robe
Cursed Ring

CT, Height, 4, 3
Float, Demi 2, Stabilize Time, Meteor



FatTunaKing
Male
Aquarius
74
54
Monk
Sing
Faith Save
Beastmaster
Retreat



Headgear
Wizard Outfit
108 Gems

Spin Fist, Pummel, Purification, Revive, Seal Evil
Angel Song, Cheer Song, Magic Song, Last Song



Shalloween
Male
Leo
43
62
Bard
Basic Skill
Auto Potion
Doublehand
Fly

Bloody Strings

Barette
Mythril Armor
Germinas Boots

Angel Song, Cheer Song, Battle Song, Sky Demon
Throw Stone, Heal, Tickle, Fury

Player: !Black
Team: Black Team
Palettes: Black/Red



Aldrammech
Male
Virgo
46
54
Time Mage
Punch Art
Absorb Used MP
Equip Armor
Move-MP Up

Sage Staff

Genji Helmet
Mystic Vest
Jade Armlet

Haste, Slow, Stop, Immobilize, Demi, Demi 2
Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive



Gelwain
Female
Pisces
54
66
Knight
Black Magic
Critical Quick
Secret Hunt
Waterwalking

Iron Sword
Round Shield
Mythril Helmet
Crystal Mail
Leather Mantle

Head Break, Shield Break, Weapon Break, Mind Break, Stasis Sword, Night Sword
Fire, Fire 2, Fire 3, Bolt, Bolt 3, Ice 3, Frog



CorpusCav
Male
Pisces
76
56
Monk
Draw Out
Auto Potion
Attack UP
Retreat



Green Beret
Mythril Vest
Chantage

Pummel, Wave Fist, Purification, Chakra, Revive
Asura, Koutetsu, Bizen Boat, Kiyomori



Actual JP
Female
Aries
50
45
Ninja
Punch Art
Caution
Equip Gun
Lava Walking

Bestiary
Battle Folio
Twist Headband
Secret Clothes
Spike Shoes

Shuriken, Staff
Wave Fist, Earth Slash, Purification, Chakra, Seal Evil

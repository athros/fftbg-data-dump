Player: !Red
Team: Red Team
Palettes: Red/Brown



SephDarkheart
Male
Scorpio
76
79
Bard
Basic Skill
Dragon Spirit
Equip Shield
Ignore Terrain

Bloody Strings
Escutcheon
Flash Hat
Brigandine
Jade Armlet

Cheer Song, Hydra Pit
Throw Stone



Blastinus
Female
Sagittarius
62
55
Chemist
Jump
MP Restore
Short Charge
Fly

Hydra Bag

Black Hood
Wizard Outfit
Setiemson

Potion, Hi-Potion, X-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Level Jump8, Vertical Jump8



Ruleof5
Female
Scorpio
57
80
Archer
Time Magic
Distribute
Doublehand
Move+2

Night Killer

Leather Hat
Chain Vest
Angel Ring

Charge+2, Charge+5, Charge+7
Haste, Haste 2, Float, Reflect, Quick, Demi 2



Latebit
Male
Scorpio
41
69
Monk
Throw
Arrow Guard
Equip Polearm
Move+1

Cypress Rod

Feather Hat
Mythril Vest
Small Mantle

Pummel, Purification, Revive
Shuriken, Bomb

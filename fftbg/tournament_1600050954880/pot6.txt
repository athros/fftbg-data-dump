Final Bets: white - 13 bets for 4,675G (26.6%, x2.77); purple - 19 bets for 12,928G (73.4%, x0.36)

white bets:
QuestMoreLand: 1,690G (36.1%, 3,380G)
BirbBrainsBot: 901G (19.3%, 134,073G)
OneHundredFists: 530G (11.3%, 530G)
killth3kid: 500G (10.7%, 34,038G)
Zerguzen: 244G (5.2%, 244G)
Chaide: 212G (4.5%, 212G)
BartTradingCompany: 100G (2.1%, 7,535G)
MoonSugarFiend: 100G (2.1%, 100G)
BirbJuicy: 100G (2.1%, 1,000G)
douchetron: 100G (2.1%, 14,137G)
nhammen: 100G (2.1%, 429,643G)
getthemoneyz: 78G (1.7%, 1,937,119G)
alcibiadeez: 20G (0.4%, 689G)

purple bets:
reinoe: 2,000G (15.5%, 19,841G)
resjudicata3: 1,667G (12.9%, 1,667G)
upvla: 1,234G (9.5%, 4,192G)
CorpusCav: 1,200G (9.3%, 12,289G)
sinnyil2: 1,200G (9.3%, 113,233G)
Thyrandaal: 1,000G (7.7%, 56,022G)
BoneMiser: 846G (6.5%, 846G)
prince_rogers_nelson_: 600G (4.6%, 600G)
seppu777: 555G (4.3%, 4,171G)
krombobreaker: 500G (3.9%, 4,542G)
AllInBot: 424G (3.3%, 424G)
Kingby: 358G (2.8%, 358G)
ValensEXP_: 292G (2.3%, 6,918G)
Regular_Obbi: 252G (1.9%, 252G)
khelor_: 200G (1.5%, 7,747G)
datadrivenbot: 200G (1.5%, 58,711G)
grishhammer: 200G (1.5%, 3,430G)
latebit: 100G (0.8%, 1,713G)
CosmicTactician: 100G (0.8%, 31,622G)

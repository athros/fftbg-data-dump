Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PoroTact
Male
Pisces
44
63
Oracle
Throw
Catch
Short Charge
Fly

Battle Bamboo

Leather Hat
Mythril Vest
Magic Ring

Blind, Poison, Life Drain, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Shuriken



E Ballard
Female
Taurus
48
51
Time Mage
White Magic
Distribute
Dual Wield
Jump+1

Mace of Zeus
Oak Staff
Triangle Hat
Silk Robe
108 Gems

Haste 2, Slow 2, Immobilize, Demi 2
Cure 3, Raise, Raise 2, Regen, Protect, Esuna, Holy



Mesmaster
Male
Virgo
46
54
Mime

Counter Tackle
Dual Wield
Move+1



Thief Hat
Wizard Outfit
Reflect Ring

Mimic




Actual JP
Male
Capricorn
57
79
Lancer
Elemental
Caution
Doublehand
Ignore Terrain

Spear

Leather Helmet
Linen Robe
Sprint Shoes

Level Jump5, Vertical Jump2
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Genkidou
Female
Gemini
72
54
Thief
White Magic
Parry
Defense UP
Move+1

Blind Knife

Black Hood
Mystic Vest
108 Gems

Steal Shield, Steal Status
Raise, Raise 2, Regen, Protect 2, Esuna, Holy



Leonidusx
Female
Sagittarius
43
72
Oracle
Dance
Parry
Dual Wield
Swim

Battle Bamboo
Ivory Rod
Twist Headband
Chain Vest
Feather Boots

Blind, Spell Absorb, Foxbird, Confusion Song, Dispel Magic, Paralyze
Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Last Dance



Nizaha
Male
Libra
74
48
Thief
Summon Magic
Counter Tackle
Sicken
Levitate

Spell Edge

Flash Hat
Leather Outfit
Dracula Mantle

Gil Taking, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Ramuh, Ifrit, Carbunkle, Odin, Leviathan, Salamander, Fairy, Lich



ValensEXP
Male
Capricorn
69
69
Monk
Black Magic
Auto Potion
Magic Attack UP
Move-HP Up



Flash Hat
Brigandine
Defense Armlet

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Fire 2, Fire 4

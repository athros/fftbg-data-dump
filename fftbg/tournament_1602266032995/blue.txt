Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



King Smashington
Male
Capricorn
50
45
Time Mage
Talk Skill
Mana Shield
Monster Talk
Levitate

Gold Staff

Headgear
Judo Outfit
Jade Armlet

Haste, Stop, Quick, Demi
Persuade, Praise, Preach, Death Sentence



JonnyCue
Male
Scorpio
54
55
Knight
Summon Magic
MA Save
Dual Wield
Ignore Terrain

Battle Axe
Giant Axe
Diamond Helmet
Crystal Mail
Power Wrist

Shield Break, Speed Break, Justice Sword
Moogle, Ifrit, Carbunkle, Fairy



Ruleof5
Female
Taurus
42
46
Archer
Summon Magic
Abandon
Equip Axe
Teleport

Morning Star
Gold Shield
Leather Helmet
Wizard Outfit
Wizard Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Moogle, Ifrit, Titan, Bahamut, Odin, Silf



Sharosa
Female
Scorpio
60
49
Squire
Time Magic
Brave Save
Magic Defense UP
Waterbreathing

Poison Bow
Platinum Shield
Gold Helmet
Mystic Vest
Feather Mantle

Accumulate
Slow, Stop, Immobilize, Stabilize Time

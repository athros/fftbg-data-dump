Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DHaveWord
Male
Aries
47
67
Knight
Basic Skill
Distribute
Dual Wield
Levitate

Defender
Mythril Sword
Genji Helmet
Chain Mail
Dracula Mantle

Armor Break, Stasis Sword, Dark Sword
Dash, Throw Stone, Heal, Tickle



Skipsandwiches
Male
Gemini
53
42
Thief
Elemental
Damage Split
Equip Gun
Retreat

Stone Gun

Triangle Hat
Clothes
Defense Armlet

Gil Taking, Steal Heart, Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Krombobreaker
Female
Cancer
72
41
Calculator
Black Magic
Faith Save
Concentrate
Move+3

Battle Folio

Ribbon
Black Costume
Elf Mantle

CT, Height, Prime Number, 4
Fire 2, Bolt, Ice 2, Ice 3, Ice 4, Flare



Lyner87
Male
Capricorn
71
77
Oracle
Basic Skill
Critical Quick
Defense UP
Move+2

Ivory Rod

Twist Headband
Chameleon Robe
Genji Gauntlet

Blind, Poison, Zombie, Blind Rage, Confusion Song, Paralyze, Sleep, Dark Holy
Yell, Cheer Up, Wish

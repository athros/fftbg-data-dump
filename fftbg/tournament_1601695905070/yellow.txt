Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Brokenknight201
Female
Libra
68
45
Dancer
Charge
Counter Flood
Doublehand
Jump+2

Cashmere

Holy Miter
Power Sleeve
108 Gems

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance
Charge+1, Charge+2, Charge+5



RikiNom
Monster
Aries
67
71
Red Chocobo










Resjudicata3
Male
Gemini
63
45
Knight
Charge
Catch
Equip Sword
Fly

Ice Brand
Aegis Shield
Barbuta
Bronze Armor
Magic Gauntlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Surging Sword
Charge+2



Thyrandaal
Male
Virgo
48
56
Ninja
Punch Art
Auto Potion
Equip Gun
Fly

Ramia Harp
Battle Folio
Headgear
Leather Outfit
Magic Ring

Shuriken, Bomb, Wand, Dictionary
Wave Fist, Earth Slash, Purification, Chakra, Revive

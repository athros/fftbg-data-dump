Final Bets: green - 17 bets for 9,266G (53.5%, x0.87); yellow - 8 bets for 8,048G (46.5%, x1.15)

green bets:
MoonSugarFiend: 2,000G (21.6%, 22,135G)
WireLord: 1,000G (10.8%, 3,389G)
BirbBrainsBot: 1,000G (10.8%, 69,167G)
dem0nj0ns: 1,000G (10.8%, 7,563G)
MemoriesofFinal: 1,000G (10.8%, 3,101G)
FatTunaKing: 633G (6.8%, 633G)
Mushufasa_: 396G (4.3%, 396G)
seppu777: 333G (3.6%, 7,847G)
Lolthsmeat: 324G (3.5%, 2,023G)
ammobox69: 295G (3.2%, 4,403G)
scsuperstar: 252G (2.7%, 252G)
DustBirdEX: 200G (2.2%, 1,873G)
CorpusCav: 200G (2.2%, 40,123G)
datadrivenbot: 200G (2.2%, 79,183G)
VynxYukida: 200G (2.2%, 3,781G)
bdb_goldenleo: 133G (1.4%, 4,441G)
Tithonus: 100G (1.1%, 1,176G)

yellow bets:
Snowfats: 5,000G (62.1%, 30,308G)
Zachara: 1,200G (14.9%, 116,700G)
JayceXIV: 500G (6.2%, 10,653G)
HorusTaurus: 475G (5.9%, 475G)
HASTERIOUS: 391G (4.9%, 7,829G)
Ruvelia_BibeI: 240G (3.0%, 240G)
AllInBot: 200G (2.5%, 200G)
getthemoneyz: 42G (0.5%, 2,161,374G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



CassiePhoenix
Male
Scorpio
65
53
Knight
Summon Magic
Meatbone Slash
Attack UP
Ignore Height

Defender
Diamond Shield
Iron Helmet
Gold Armor
Vanish Mantle

Head Break, Shield Break
Moogle, Ifrit, Carbunkle, Leviathan, Cyclops



Mushufasa
Male
Leo
79
44
Monk
Elemental
Counter Tackle
Magic Attack UP
Move+1



Black Hood
Adaman Vest
Defense Armlet

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



Lijarkh
Male
Sagittarius
72
72
Monk
Steal
Parry
Attack UP
Levitate



Feather Hat
Clothes
Defense Armlet

Earth Slash, Purification, Chakra
Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory



VynxYukida
Male
Gemini
63
70
Thief
Item
Counter Magic
Doublehand
Teleport

Sasuke Knife

Holy Miter
Wizard Outfit
Red Shoes

Gil Taking, Steal Helmet, Steal Shield, Steal Accessory
Potion, X-Potion, Eye Drop, Remedy, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



Dem0nj0ns
Male
Leo
57
69
Ninja
Charge
Auto Potion
Defend
Move+2

Blind Knife
Main Gauche
Thief Hat
Leather Outfit
Battle Boots

Spear
Charge+4, Charge+5



Almost Sane
Male
Aquarius
75
52
Geomancer
Sing
Distribute
Maintenance
Jump+3

Coral Sword
Ice Shield
Red Hood
Judo Outfit
Power Wrist

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Nameless Song, Space Storage, Sky Demon, Hydra Pit



DeathTaxesAndAnime
Female
Aquarius
46
53
Thief
Item
Abandon
Long Status
Waterbreathing

Assassin Dagger

Thief Hat
Adaman Vest
108 Gems

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory
Potion, Ether, Echo Grass, Holy Water, Phoenix Down



Scsuperstar
Female
Virgo
58
46
Oracle
Steal
Arrow Guard
Concentrate
Waterwalking

Ivory Rod

Leather Hat
Light Robe
Cursed Ring

Poison, Dispel Magic, Paralyze
Steal Heart, Steal Helmet, Steal Armor, Steal Status

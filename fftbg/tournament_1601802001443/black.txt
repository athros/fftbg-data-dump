Player: !Black
Team: Black Team
Palettes: Black/Red



Tithonus
Female
Virgo
57
63
Dancer
Yin Yang Magic
Speed Save
Equip Shield
Retreat

Ryozan Silk
Platinum Shield
Headgear
Wizard Robe
Jade Armlet

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Dragon Pit
Blind, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify



Heropong
Female
Virgo
50
50
Calculator
Time Magic
Counter Tackle
Defend
Move+3

Faith Rod

Headgear
Silk Robe
Wizard Mantle

CT, Height, Prime Number, 5, 4
Haste, Haste 2, Quick, Demi



Bigbongsmoker
Female
Capricorn
46
73
Squire
Dance
MP Restore
Equip Knife
Retreat

Short Edge
Buckler
Diamond Helmet
Mystic Vest
Dracula Mantle

Accumulate, Heal, Cheer Up, Fury
Witch Hunt, Polka Polka, Nether Demon



MemoriesofFinal
Male
Virgo
71
47
Mime

Abandon
Martial Arts
Waterwalking



Barette
Earth Clothes
Angel Ring

Mimic


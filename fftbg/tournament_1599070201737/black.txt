Player: !Black
Team: Black Team
Palettes: Black/Red



JumbocactuarX27
Male
Leo
49
61
Bard
Throw
Meatbone Slash
Equip Shield
Move+3

Bloody Strings
Genji Shield
Flash Hat
Mythril Vest
Reflect Ring

Life Song, Cheer Song, Battle Song, Nameless Song
Knife



Kohlingen
Female
Taurus
54
75
Priest
Time Magic
Meatbone Slash
Defend
Jump+1

Scorpion Tail

Headgear
Black Robe
Small Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Shell 2, Wall, Esuna
Haste, Stop, Float, Reflect, Demi



Latebit
Male
Libra
71
44
Ninja
Battle Skill
Counter
Halve MP
Ignore Height

Short Edge
Short Edge
Triangle Hat
Power Sleeve
Wizard Mantle

Shuriken, Staff, Ninja Sword, Spear
Head Break, Magic Break, Speed Break, Stasis Sword



Neerrm
Female
Pisces
65
79
Summoner
Black Magic
Distribute
Equip Armor
Fly

Healing Staff

Barbuta
Leather Armor
Magic Ring

Moogle, Carbunkle, Bahamut, Silf
Fire 2, Fire 4, Bolt, Bolt 2, Ice, Ice 3, Ice 4, Empower

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Roofiepops
Male
Cancer
76
48
Ninja
Time Magic
Regenerator
Sicken
Fly

Cultist Dagger
Air Knife
Cachusha
Power Sleeve
Elf Mantle

Shuriken, Stick
Slow 2, Demi 2, Stabilize Time



DaveStrider55
Male
Sagittarius
51
45
Knight
Charge
Distribute
Halve MP
Move+3

Battle Axe
Genji Shield
Iron Helmet
Maximillian
Feather Boots

Head Break, Magic Break, Speed Break, Dark Sword
Charge+1, Charge+2, Charge+4, Charge+20



RobotOcelot
Male
Libra
80
78
Thief
Draw Out
Faith Save
Martial Arts
Teleport



Golden Hairpin
Adaman Vest
Wizard Mantle

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Status, Leg Aim
Asura, Bizen Boat



Laserman1000
Male
Cancer
51
65
Mediator
Black Magic
Mana Shield
Equip Polearm
Waterwalking

Obelisk

Leather Hat
Power Sleeve
Leather Mantle

Persuade, Preach, Insult, Refute
Fire 2, Fire 3, Bolt 4, Ice 2

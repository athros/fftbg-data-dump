Final Bets: blue - 10 bets for 20,759G (80.1%, x0.25); purple - 7 bets for 5,147G (19.9%, x4.03)

blue bets:
scsuperstar: 6,723G (32.4%, 6,723G)
LivingHitokiri: 5,000G (24.1%, 123,364G)
upvla: 3,456G (16.6%, 24,031G)
xGlaive: 2,766G (13.3%, 2,766G)
RikiNom: 1,000G (4.8%, 1,112G)
BirbBrainsBot: 1,000G (4.8%, 25,939G)
Zachara: 314G (1.5%, 138,914G)
AllInBot: 200G (1.0%, 200G)
datadrivenbot: 200G (1.0%, 80,373G)
Sharosa: 100G (0.5%, 2,477G)

purple bets:
fattunaking: 3,120G (60.6%, 3,120G)
Lydian_C: 1,026G (19.9%, 1,026G)
goth_Applebees_hostess: 350G (6.8%, 3,989G)
mindblownnnn: 296G (5.8%, 296G)
CorpusCav: 200G (3.9%, 31,347G)
getthemoneyz: 128G (2.5%, 2,093,522G)
AtmaDragoon: 27G (0.5%, 149G)

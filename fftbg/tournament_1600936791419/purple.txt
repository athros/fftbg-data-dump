Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Raixelol
Female
Taurus
75
60
Lancer
Throw
Earplug
Doublehand
Fly

Holy Lance

Iron Helmet
Mythril Armor
Jade Armlet

Level Jump2, Vertical Jump3
Bomb



Upvla
Male
Taurus
42
53
Ninja
Battle Skill
Counter Magic
Equip Gun
Jump+3

Bestiary
Papyrus Codex
Golden Hairpin
Mystic Vest
Power Wrist

Shuriken, Bomb
Shield Break, Power Break



Reddwind
Male
Sagittarius
58
50
Knight
Draw Out
Counter Tackle
Equip Sword
Fly

Asura Knife
Crystal Shield
Gold Helmet
Chain Mail
Magic Ring

Speed Break, Mind Break
Bizen Boat



Aldric
Male
Libra
77
62
Samurai
Throw
Auto Potion
Short Charge
Move+3

Koutetsu Knife

Barbuta
Linen Robe
Battle Boots

Kiyomori
Shuriken

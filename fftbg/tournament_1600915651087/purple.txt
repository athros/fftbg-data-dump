Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ANFz
Male
Libra
43
54
Bard
Elemental
Blade Grasp
Equip Armor
Move+1

Ramia Harp

Platinum Helmet
Diamond Armor
Feather Mantle

Battle Song, Sky Demon
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Gusty Wind



XerxesMindbreaker
Male
Virgo
67
77
Priest
Throw
MP Restore
Magic Attack UP
Move+3

Rainbow Staff

Green Beret
White Robe
Defense Armlet

Cure 4, Raise, Regen, Shell 2, Wall
Shuriken, Bomb, Staff, Ninja Sword



Genkidou
Female
Cancer
58
69
Archer
Battle Skill
Counter Flood
Dual Wield
Swim

Ultimus Bow

Holy Miter
Mythril Vest
Feather Mantle

Charge+1, Charge+2, Charge+4, Charge+7
Mind Break, Stasis Sword



Nhammen
Male
Sagittarius
75
62
Monk
Basic Skill
Counter
Sicken
Jump+3

Hydra Bag

Thief Hat
Earth Clothes
Wizard Mantle

Spin Fist, Pummel, Secret Fist, Chakra, Revive
Accumulate, Heal, Fury

Player: !Red
Team: Red Team
Palettes: Red/Brown



Onehitslay
Male
Scorpio
74
66
Wizard
Battle Skill
Speed Save
Secret Hunt
Waterbreathing

Flame Rod

Red Hood
Wizard Outfit
Vanish Mantle

Fire 3, Bolt, Ice 3, Ice 4, Empower, Flare
Head Break, Armor Break, Magic Break, Power Break, Stasis Sword, Explosion Sword



Dasutin23
Monster
Cancer
51
75
Dragon










Technominari
Female
Libra
52
39
Priest
Draw Out
Counter Magic
Defense UP
Retreat

Flame Whip

Green Beret
Mythril Vest
Chantage

Cure 4, Raise, Protect, Shell, Wall, Esuna
Asura, Koutetsu, Bizen Boat



Fenaen
Female
Capricorn
42
72
Summoner
Talk Skill
Critical Quick
Secret Hunt
Teleport

Gold Staff

Headgear
Silk Robe
Chantage

Moogle, Shiva, Ramuh, Titan, Carbunkle, Leviathan, Salamander, Fairy, Lich, Cyclops
Praise, Insult

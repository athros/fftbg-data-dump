Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CT 5 Holy
Female
Virgo
78
63
Calculator
Black Magic
Regenerator
Magic Attack UP
Jump+3

Battle Folio

Black Hood
Black Robe
Red Shoes

Height, 3
Fire 2, Frog



VolgraTheMoose
Female
Virgo
73
60
Geomancer
Draw Out
Meatbone Slash
Long Status
Move-MP Up

Slasher
Aegis Shield
Twist Headband
Secret Clothes
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Muramasa



CassiePhoenix
Female
Cancer
79
79
Samurai
Time Magic
Counter Flood
Magic Attack UP
Move+3

Spear

Iron Helmet
Bronze Armor
Cherche

Koutetsu, Muramasa, Kikuichimoji
Haste 2, Stop, Immobilize, Float



LeoNightFury
Male
Taurus
63
60
Wizard
Charge
Arrow Guard
Short Charge
Levitate

Dragon Rod

Flash Hat
Secret Clothes
108 Gems

Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Empower, Death
Charge+4

Player: !Red
Team: Red Team
Palettes: Red/Brown



Dinin991
Male
Gemini
62
48
Thief
Jump
Speed Save
Equip Bow
Fly

Night Killer

Feather Hat
Mystic Vest
Spike Shoes

Steal Heart, Steal Armor, Steal Accessory
Level Jump2, Vertical Jump8



Just Here2
Female
Capricorn
78
41
Chemist
Summon Magic
Regenerator
Secret Hunt
Move+2

Panther Bag

Black Hood
Mystic Vest
Sprint Shoes

Hi-Potion, X-Potion, Ether, Echo Grass, Remedy, Phoenix Down
Moogle, Bahamut, Fairy, Zodiac



Raixelol
Male
Aries
67
78
Mime

Mana Shield
Martial Arts
Move+3



Black Hood
Brigandine
Elf Mantle

Mimic




ValensEXP
Male
Capricorn
77
82
Monk
Basic Skill
Damage Split
Magic Defense UP
Jump+3



Holy Miter
Mythril Vest
Defense Ring

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Throw Stone, Heal, Yell, Fury, Wish

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Male
Aquarius
47
69
Ninja
Basic Skill
Faith Save
Concentrate
Fly

Blind Knife
Short Edge
Feather Hat
Judo Outfit
Angel Ring

Shuriken, Ninja Sword, Wand
Tickle



0v3rr8d
Female
Capricorn
68
65
Knight
Jump
MP Restore
Doublehand
Retreat

Save the Queen

Circlet
Diamond Armor
Red Shoes

Head Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Night Sword
Level Jump8, Vertical Jump4



Skipsandwiches
Male
Aquarius
60
59
Bard
Item
Dragon Spirit
Equip Axe
Move+3

Rainbow Staff

Feather Hat
Power Sleeve
Elf Mantle

Angel Song, Cheer Song, Magic Song, Space Storage, Sky Demon
Potion, Ether, Echo Grass, Remedy



Rocl
Male
Pisces
57
74
Lancer
Item
Meatbone Slash
Concentrate
Ignore Terrain

Javelin
Genji Shield
Cross Helmet
Linen Cuirass
Red Shoes

Level Jump5, Vertical Jump8
Hi-Potion, Antidote, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Gemini
74
76
Lancer
Basic Skill
Hamedo
Doublehand
Teleport

Holy Lance

Leather Helmet
Platinum Armor
Genji Gauntlet

Level Jump8, Vertical Jump3
Heal, Tickle, Wish



Mirapoix
Male
Libra
46
68
Knight
Black Magic
Counter Tackle
Equip Gun
Move+2

Star Bag
Mythril Shield
Gold Helmet
Linen Cuirass
Cherche

Armor Break, Weapon Break, Magic Break, Speed Break, Dark Sword
Fire 4, Ice 2, Ice 4, Flare



Run With Stone GUNs
Female
Sagittarius
62
45
Wizard
Punch Art
Damage Split
Short Charge
Jump+2

Blind Knife

Red Hood
Wizard Robe
Cursed Ring

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Ice 3, Death, Flare
Secret Fist, Chakra, Revive



Daveb
Female
Gemini
70
66
Ninja
Elemental
PA Save
Equip Gun
Jump+3

Papyrus Codex
Bestiary
Feather Hat
Clothes
Red Shoes

Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball

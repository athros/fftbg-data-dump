Player: !White
Team: White Team
Palettes: White/Blue



LivingHitokiri
Male
Taurus
75
73
Archer
Basic Skill
MA Save
Attack UP
Fly

Silver Bow

Flash Hat
Wizard Outfit
Rubber Shoes

Charge+3, Charge+7
Heal, Tickle, Yell, Cheer Up, Wish



Khelor
Male
Capricorn
66
62
Calculator
Yin Yang Magic
Counter Flood
Concentrate
Move+1

Whale Whisker

Red Hood
Chain Vest
Bracer

CT, Height, Prime Number, 5, 4
Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Paralyze, Petrify



AnthroMetal
Female
Capricorn
65
58
Dancer
Elemental
Mana Shield
Equip Knife
Swim

Rod

Green Beret
Judo Outfit
108 Gems

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Void Storage
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball



Sairentozon7
Male
Pisces
59
70
Thief
Draw Out
MA Save
Dual Wield
Retreat

Ninja Edge
Ice Brand
Black Hood
Earth Clothes
Diamond Armlet

Steal Heart, Steal Shield, Steal Weapon, Steal Accessory
Murasame, Muramasa

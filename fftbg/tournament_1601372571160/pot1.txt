Final Bets: red - 10 bets for 5,313G (54.3%, x0.84); blue - 7 bets for 4,468G (45.7%, x1.19)

red bets:
NovaKnight21: 1,919G (36.1%, 1,919G)
NicoSavoy: 1,008G (19.0%, 1,008G)
Tithonus: 613G (11.5%, 613G)
LivingHitokiri: 500G (9.4%, 146,158G)
lijarkh: 308G (5.8%, 6,041G)
Lolthsmeat: 288G (5.4%, 288G)
AllInBot: 200G (3.8%, 200G)
datadrivenbot: 200G (3.8%, 81,670G)
daveb_: 177G (3.3%, 4,625G)
Sharosa: 100G (1.9%, 2,795G)

blue bets:
mirapoix: 1,168G (26.1%, 1,168G)
getthemoneyz: 1,000G (22.4%, 2,102,209G)
BirbBrainsBot: 1,000G (22.4%, 81,454G)
HaplessOne: 588G (13.2%, 588G)
3ngag3: 412G (9.2%, 6,100G)
Evewho: 200G (4.5%, 1,837G)
Palladigm: 100G (2.2%, 10,477G)

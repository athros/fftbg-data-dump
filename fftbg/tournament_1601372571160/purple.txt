Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zachara
Female
Cancer
39
48
Samurai
Time Magic
Auto Potion
Equip Polearm
Fly

Mythril Spear

Circlet
Chameleon Robe
Small Mantle

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Reflect, Stabilize Time



Gorgewall
Male
Virgo
47
43
Geomancer
Summon Magic
Absorb Used MP
Concentrate
Waterwalking

Giant Axe
Hero Shield
Leather Hat
Power Sleeve
Setiemson

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Moogle, Ramuh, Carbunkle, Bahamut, Odin, Leviathan



Khelor
Monster
Capricorn
44
71
Trent










NicoSavoy
Female
Taurus
77
45
Monk
Battle Skill
Distribute
Equip Shield
Move+1


Crystal Shield
Green Beret
Secret Clothes
108 Gems

Pummel, Earth Slash, Purification, Chakra, Revive
Head Break, Armor Break, Magic Break, Speed Break, Power Break, Justice Sword, Night Sword

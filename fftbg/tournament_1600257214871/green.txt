Player: !Green
Team: Green Team
Palettes: Green/White



SephDarkheart
Female
Aries
58
73
Ninja
Talk Skill
Sunken State
Beastmaster
Lava Walking

Flail
Kunai
Twist Headband
Wizard Outfit
Dracula Mantle

Bomb, Hammer, Staff, Wand
Persuade, Praise, Threaten, Death Sentence, Negotiate, Rehabilitate



CosmicTactician
Female
Serpentarius
43
73
Time Mage
Yin Yang Magic
Counter
Magic Defense UP
Move-MP Up

Battle Bamboo

Triangle Hat
Silk Robe
Dracula Mantle

Haste, Haste 2, Reflect, Quick, Stabilize Time, Meteor
Blind Rage, Confusion Song, Petrify



Ar Tactic
Female
Aries
53
53
Archer
Time Magic
Absorb Used MP
Defend
Jump+2

Long Bow

Circlet
Leather Outfit
Jade Armlet

Charge+2, Charge+5, Charge+7, Charge+10
Haste, Haste 2, Stop, Immobilize, Demi 2, Stabilize Time



Sairentozon7
Male
Taurus
64
66
Archer
Punch Art
Counter Tackle
Sicken
Move-HP Up

Poison Bow
Hero Shield
Ribbon
Adaman Vest
Elf Mantle

Charge+4, Charge+5
Spin Fist, Pummel, Wave Fist, Seal Evil

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Aquarius
46
76
Mediator
Time Magic
Abandon
Equip Sword
Move-MP Up

Defender

Feather Hat
Brigandine
N-Kai Armlet

Invitation, Persuade, Praise, Threaten, Preach, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate
Haste, Float, Reflect, Demi, Demi 2, Stabilize Time, Meteor



J MacC1991
Male
Aquarius
74
75
Squire
Time Magic
Counter Magic
Martial Arts
Swim


Escutcheon
Red Hood
Judo Outfit
Wizard Mantle

Throw Stone, Heal, Tickle, Cheer Up, Fury, Wish
Haste, Slow, Stop, Immobilize, Stabilize Time



Maakur
Male
Virgo
66
53
Lancer
Draw Out
Critical Quick
Maintenance
Retreat

Spear
Buckler
Platinum Helmet
Crystal Mail
Magic Ring

Level Jump8, Vertical Jump2
Koutetsu, Kikuichimoji



Rune339
Male
Taurus
57
54
Archer
Time Magic
Catch
Equip Polearm
Move+3

Ryozan Silk
Flame Shield
Twist Headband
Brigandine
Rubber Shoes

Charge+2, Charge+3
Haste, Stabilize Time

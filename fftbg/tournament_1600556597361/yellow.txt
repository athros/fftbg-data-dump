Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lwtest
Male
Leo
43
50
Ninja
Time Magic
Dragon Spirit
Beastmaster
Retreat

Spell Edge
Kunai
Red Hood
Power Sleeve
Wizard Mantle

Shuriken, Bomb, Knife
Haste, Slow, Reflect, Demi, Stabilize Time



Gilgamensche
Male
Scorpio
80
49
Mime

Regenerator
Long Status
Swim



Holy Miter
Leather Outfit
Chantage

Mimic




Mushufasa
Male
Sagittarius
70
80
Summoner
Punch Art
Speed Save
Martial Arts
Jump+2

Thunder Rod

Black Hood
Black Robe
Small Mantle

Ifrit, Titan, Golem, Silf
Wave Fist, Purification, Chakra, Revive, Seal Evil



Technominari
Female
Serpentarius
46
70
Priest
Summon Magic
PA Save
Maintenance
Teleport

Healing Staff

Twist Headband
Linen Robe
Leather Mantle

Cure 2, Cure 4, Reraise, Protect, Protect 2, Shell, Shell 2, Esuna, Holy
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Odin, Salamander, Silf

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kohlingen
Male
Scorpio
77
70
Monk
Sing
Parry
Equip Sword
Teleport

Rune Blade

Leather Hat
Mystic Vest
Feather Boots

Pummel, Secret Fist, Purification, Revive
Life Song, Magic Song, Last Song, Sky Demon



AbandonedHall
Male
Pisces
55
71
Samurai
Battle Skill
Regenerator
Secret Hunt
Jump+1

Spear

Cross Helmet
Gold Armor
Small Mantle

Bizen Boat, Murasame, Muramasa
Head Break, Shield Break, Weapon Break, Magic Break



Bruubarg
Male
Libra
66
43
Mime

Absorb Used MP
Attack UP
Retreat



Triangle Hat
Mystic Vest
Jade Armlet

Mimic




Mindblownnnn
Female
Sagittarius
50
75
Lancer
White Magic
Auto Potion
Equip Polearm
Ignore Height

Ryozan Silk
Diamond Shield
Bronze Helmet
Chameleon Robe
Bracer

Level Jump8, Vertical Jump5
Cure 2, Cure 3, Raise, Reraise, Wall

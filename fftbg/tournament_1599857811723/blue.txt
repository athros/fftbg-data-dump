Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Upvla
Female
Libra
45
52
Geomancer
Throw
Damage Split
Long Status
Jump+1

Bizen Boat
Diamond Shield
Green Beret
Wizard Robe
N-Kai Armlet

Pitfall, Water Ball, Hallowed Ground, Quicksand, Gusty Wind, Lava Ball
Knife



Run With Stone GUNs
Female
Libra
52
73
Oracle
Steal
Abandon
Short Charge
Jump+3

Gokuu Rod

Feather Hat
Earth Clothes
108 Gems

Spell Absorb, Pray Faith, Zombie, Foxbird, Paralyze, Sleep, Petrify
Steal Helmet



Brokenknight201
Female
Aquarius
45
63
Calculator
Lucavi Skill
Counter Magic
Magic Defense UP
Waterbreathing

Madlemgen
Escutcheon
Gold Helmet
Chameleon Robe
Wizard Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Ruleof5
Female
Leo
57
44
Archer
Throw
MP Restore
Secret Hunt
Levitate

Ultimus Bow

Triangle Hat
Adaman Vest
Bracer

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Axe

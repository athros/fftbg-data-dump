Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Cryptopsy70
Female
Leo
67
47
Lancer
Draw Out
Catch
Dual Wield
Retreat

Spear
Obelisk
Gold Helmet
Light Robe
Jade Armlet

Level Jump5, Vertical Jump3
Koutetsu, Bizen Boat, Muramasa, Kikuichimoji



Reddwind
Male
Scorpio
47
73
Knight
Basic Skill
Abandon
Equip Knife
Move+3

Hidden Knife
Round Shield
Bronze Helmet
Platinum Armor
Cursed Ring

Head Break, Speed Break, Mind Break, Surging Sword
Dash, Throw Stone, Heal, Cheer Up, Fury, Scream



KasugaiRoastedPeas
Female
Serpentarius
48
78
Calculator
Tentacle Skill
Arrow Guard
Sicken
Move+1

Ice Rod
Buckler
Feather Hat
Platinum Armor
Red Shoes

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



Baconbacon1207
Male
Aquarius
65
45
Ninja
Draw Out
Regenerator
Equip Gun
Retreat

Ramia Harp
Papyrus Codex
Red Hood
Black Costume
Red Shoes

Shuriken, Knife, Wand
Murasame

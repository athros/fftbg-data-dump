Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Pplvee1
Male
Cancer
47
80
Lancer
Summon Magic
Counter
Defend
Retreat

Javelin
Flame Shield
Grand Helmet
Genji Armor
Setiemson

Level Jump8, Vertical Jump5
Moogle, Shiva, Odin, Fairy



VolgraTheMoose
Monster
Gemini
42
43
Holy Dragon










JumbocactuarX27
Female
Virgo
48
59
Chemist
Basic Skill
Speed Save
Defend
Move-HP Up

Mythril Gun

Black Hood
Mystic Vest
Magic Ring

Potion, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Accumulate, Tickle, Fury, Wish



PoroTact
Male
Capricorn
79
75
Archer
Battle Skill
Parry
Equip Axe
Jump+2

Giant Axe
Round Shield
Barette
Clothes
Setiemson

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10
Magic Break, Dark Sword

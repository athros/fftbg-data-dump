Player: !Black
Team: Black Team
Palettes: Black/Red



StealthModeLocke
Female
Scorpio
79
53
Samurai
Yin Yang Magic
Caution
Magic Attack UP
Move+3

Bizen Boat

Genji Helmet
Black Robe
Red Shoes

Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Blind, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Sleep, Dark Holy



Zagorsek
Male
Taurus
67
80
Monk
Draw Out
Auto Potion
Equip Axe
Fly

Gold Staff

Flash Hat
Wizard Outfit
Reflect Ring

Spin Fist, Secret Fist, Chakra, Revive
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



Dogsandcatsand
Female
Aries
45
53
Wizard
White Magic
Critical Quick
Short Charge
Ignore Height

Panther Bag

Leather Hat
Light Robe
Feather Mantle

Fire 2, Ice 2, Ice 3, Empower
Cure, Regen, Esuna



CapnChaos12
Male
Gemini
56
78
Bard
Elemental
Faith Save
Doublehand
Levitate

Bloody Strings

Green Beret
Black Costume
N-Kai Armlet

Angel Song, Sky Demon
Hallowed Ground, Will-O-Wisp

Player: !Black
Team: Black Team
Palettes: Black/Red



Upvla
Male
Sagittarius
60
54
Ninja
Elemental
Speed Save
Beastmaster
Move-HP Up

Iga Knife
Dagger
Triangle Hat
Secret Clothes
Magic Gauntlet

Shuriken, Bomb, Dictionary
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Gelwain
Male
Leo
52
61
Thief
Punch Art
Mana Shield
Attack UP
Move+3

Sasuke Knife

Green Beret
Judo Outfit
Reflect Ring

Steal Helmet, Leg Aim
Spin Fist, Purification, Revive, Seal Evil



Ruleof5
Female
Gemini
80
47
Archer
Jump
Caution
Equip Gun
Move+3

Bloody Strings
Escutcheon
Red Hood
Judo Outfit
Feather Boots

Charge+1, Charge+4, Charge+7, Charge+10
Level Jump8, Vertical Jump6



Go2sleepTV
Female
Aquarius
67
65
Mime

Auto Potion
Equip Shield
Retreat


Aegis Shield
Green Beret
Platinum Armor
Red Shoes

Mimic


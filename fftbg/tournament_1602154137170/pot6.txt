Final Bets: white - 6 bets for 7,115G (60.2%, x0.66); brown - 8 bets for 4,712G (39.8%, x1.51)

white bets:
reinoe: 4,560G (64.1%, 4,560G)
BirbBrainsBot: 1,000G (14.1%, 180,518G)
nifboy: 896G (12.6%, 896G)
TheSabretoothe: 336G (4.7%, 336G)
datadrivenbot: 200G (2.8%, 81,062G)
LuckyLuckLuc2: 123G (1.7%, 1,131G)

brown bets:
AllInBot: 2,757G (58.5%, 2,757G)
Mushufasa_: 500G (10.6%, 13,506G)
MemoriesofFinal: 500G (10.6%, 32,156G)
HaplessOne: 333G (7.1%, 8,738G)
resjudicata3: 300G (6.4%, 4,739G)
FuzzyTigers: 132G (2.8%, 132G)
CosmicTactician: 100G (2.1%, 3,031G)
getthemoneyz: 90G (1.9%, 2,211,385G)

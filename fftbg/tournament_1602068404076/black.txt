Player: !Black
Team: Black Team
Palettes: Black/Red



Dinin991
Female
Libra
56
73
Wizard
Basic Skill
Parry
Magic Attack UP
Move-HP Up

Ice Rod

Feather Hat
Secret Clothes
Elf Mantle

Bolt, Bolt 4, Ice
Throw Stone, Heal, Cheer Up, Wish



Mesmaster
Male
Sagittarius
47
63
Archer
White Magic
Brave Save
Doublehand
Fly

Ice Bow

Headgear
Chain Vest
Reflect Ring

Charge+1, Charge+2, Charge+4, Charge+7, Charge+20
Cure 2, Cure 3, Regen, Protect, Shell, Esuna



HASTERIOUS
Female
Scorpio
68
58
Geomancer
Draw Out
Counter
Equip Armor
Retreat

Battle Axe
Crystal Shield
Circlet
Silk Robe
Magic Gauntlet

Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Murasame, Heaven's Cloud, Kikuichimoji



ColetteMSLP
Female
Leo
79
73
Chemist
Time Magic
Sunken State
Short Charge
Retreat

Stone Gun

Holy Miter
Power Sleeve
Battle Boots

Potion, Hi-Potion, Ether, Antidote, Holy Water, Remedy, Phoenix Down
Haste 2, Slow, Immobilize, Float, Reflect, Stabilize Time

Final Bets: blue - 12 bets for 10,301G (78.2%, x0.28); green - 5 bets for 2,868G (21.8%, x3.59)

blue bets:
DeathTaxesAndAnime: 3,820G (37.1%, 7,491G)
Mushufasa_: 1,234G (12.0%, 18,169G)
MemoriesofFinal: 1,000G (9.7%, 18,843G)
dogsandcatsand: 928G (9.0%, 928G)
BirbBrainsBot: 872G (8.5%, 155,985G)
HorusTaurus: 691G (6.7%, 691G)
getthemoneyz: 506G (4.9%, 2,198,735G)
krombobreaker: 500G (4.9%, 42,857G)
BrandNewFP: 250G (2.4%, 1,617G)
datadrivenbot: 200G (1.9%, 82,279G)
CosmicTactician: 200G (1.9%, 1,618G)
readdesert: 100G (1.0%, 1,952G)

green bets:
AllInBot: 905G (31.6%, 905G)
ValensEXP_: 618G (21.5%, 618G)
ColetteMSLP: 512G (17.9%, 7,675G)
Synahel: 500G (17.4%, 2,713G)
Humble_Fabio: 333G (11.6%, 3,800G)

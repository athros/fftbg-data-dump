Player: !Green
Team: Green Team
Palettes: Green/White



Heropong
Female
Capricorn
71
59
Chemist
White Magic
Blade Grasp
Dual Wield
Move-HP Up

Stone Gun
Romanda Gun
Leather Hat
Mythril Vest
Power Wrist

Hi-Potion, Echo Grass, Phoenix Down
Cure 3, Raise, Regen, Protect, Esuna



Latebit
Male
Sagittarius
47
45
Knight
Time Magic
Regenerator
Doublehand
Teleport 2

Rune Blade

Circlet
Chain Mail
N-Kai Armlet

Shield Break, Weapon Break, Speed Break, Mind Break
Slow 2, Float, Reflect, Demi 2, Stabilize Time, Meteor



ValensEXP
Male
Scorpio
55
60
Knight
Summon Magic
Arrow Guard
Equip Knife
Ignore Height

Flame Rod
Genji Shield
Gold Helmet
White Robe
Elf Mantle

Head Break, Stasis Sword, Justice Sword, Dark Sword
Salamander, Fairy, Lich



Evewho
Female
Gemini
47
51
Samurai
Item
Counter
Magic Attack UP
Move-HP Up

Bizen Boat

Bronze Helmet
Diamond Armor
Elf Mantle

Asura, Muramasa
Potion, Hi-Ether, Echo Grass, Phoenix Down

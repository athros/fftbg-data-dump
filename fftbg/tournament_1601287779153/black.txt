Player: !Black
Team: Black Team
Palettes: Black/Red



Touchingthefuzzy
Male
Aries
74
75
Mediator
Sing
Counter
Equip Polearm
Move+1

Spear

Black Hood
Robe of Lords
Dracula Mantle

Preach, Solution, Death Sentence, Insult, Negotiate, Rehabilitate
Life Song, Hydra Pit



Rune339
Female
Serpentarius
45
57
Summoner
Basic Skill
Damage Split
Short Charge
Teleport

Flame Rod

Headgear
Mystic Vest
Genji Gauntlet

Ifrit, Carbunkle, Leviathan, Lich
Accumulate, Throw Stone, Heal, Fury



TheSabretoothe
Male
Aquarius
70
47
Geomancer
Basic Skill
MA Save
Halve MP
Move+3

Giant Axe
Escutcheon
Feather Hat
Linen Robe
Defense Ring

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Accumulate, Heal, Tickle, Cheer Up, Wish



Mesmaster
Female
Gemini
44
46
Wizard
Draw Out
HP Restore
Maintenance
Move+3

Wizard Rod

Red Hood
White Robe
Dracula Mantle

Fire 3, Ice 2, Ice 3, Empower, Frog
Koutetsu, Kiyomori, Muramasa, Kikuichimoji

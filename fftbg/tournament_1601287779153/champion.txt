Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ALY327
Female
Gemini
76
75
Mediator
Steal
Counter Tackle
Dual Wield
Fly

Mythril Gun
Romanda Gun
Headgear
Wizard Robe
Angel Ring

Invitation, Threaten, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Rehabilitate
Steal Armor



OneHundredFists
Male
Scorpio
54
57
Monk
Throw
Dragon Spirit
Long Status
Jump+1



Twist Headband
Black Costume
N-Kai Armlet

Wave Fist, Purification, Revive
Shuriken, Bomb, Spear



HASTERIOUS
Male
Pisces
64
51
Lancer
Time Magic
Dragon Spirit
Martial Arts
Lava Walking


Genji Shield
Diamond Helmet
Genji Armor
Sprint Shoes

Level Jump5, Vertical Jump6
Haste, Haste 2, Stop, Reflect, Demi 2, Meteor



Lijarkh
Male
Sagittarius
76
79
Monk
Item
Speed Save
Magic Attack UP
Waterbreathing



Triangle Hat
Clothes
Defense Armlet

Spin Fist, Earth Slash, Chakra, Revive, Seal Evil
Hi-Potion, Soft, Phoenix Down

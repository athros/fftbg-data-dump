Final Bets: purple - 17 bets for 9,609G (67.1%, x0.49); brown - 9 bets for 4,715G (32.9%, x2.04)

purple bets:
thunderducker: 4,000G (41.6%, 15,562G)
dogsandcatsand: 1,000G (10.4%, 26,123G)
reinoe: 1,000G (10.4%, 83,425G)
BaronHaynes: 600G (6.2%, 75,476G)
technominari: 500G (5.2%, 77,893G)
krombobreaker: 500G (5.2%, 3,915G)
resjudicata3: 416G (4.3%, 416G)
blorpy_: 362G (3.8%, 4,534G)
Greggernaut: 208G (2.2%, 208G)
datadrivenbot: 200G (2.1%, 58,762G)
CorpusCav: 200G (2.1%, 5,506G)
Lydian_C: 123G (1.3%, 9,901G)
HS39: 100G (1.0%, 2,447G)
Tarheels218: 100G (1.0%, 3,825G)
ko2q: 100G (1.0%, 6,618G)
Tougou: 100G (1.0%, 5,625G)
Leonidusx: 100G (1.0%, 21,110G)

brown bets:
Thyrandaal: 2,000G (42.4%, 35,908G)
BirbBrainsBot: 850G (18.0%, 148,000G)
Lumberinjack: 451G (9.6%, 451G)
goth_Applebees_hostess: 400G (8.5%, 1,979G)
getthemoneyz: 326G (6.9%, 1,778,891G)
AbandonedHall: 200G (4.2%, 2,000G)
KasugaiRoastedPeas: 200G (4.2%, 5,249G)
AllInBot: 188G (4.0%, 188G)
cloudycube: 100G (2.1%, 842G)

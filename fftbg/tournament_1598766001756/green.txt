Player: !Green
Team: Green Team
Palettes: Green/White



ALY327
Male
Libra
39
54
Squire
White Magic
Arrow Guard
Long Status
Ignore Height

Star Bag
Flame Shield
Thief Hat
Clothes
Defense Ring

Accumulate, Heal, Cheer Up, Fury
Cure, Raise, Regen, Protect, Shell 2, Wall, Esuna



Eweaselbeth
Male
Aquarius
64
45
Archer
Summon Magic
Regenerator
Dual Wield
Move-HP Up

Ultimus Bow

Headgear
Mystic Vest
Small Mantle

Charge+2, Charge+4
Moogle, Salamander, Lich



Laserman1000
Monster
Libra
52
68
Gobbledeguck










Thunderducker
Female
Aries
49
61
Thief
Elemental
Mana Shield
Equip Polearm
Jump+3

Cashmere

Red Hood
Power Sleeve
Angel Ring

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Status
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Female
Leo
41
62
Oracle
Draw Out
Counter Magic
Doublehand
Waterwalking

Gokuu Rod

Headgear
Judo Outfit
Small Mantle

Blind, Spell Absorb, Doubt Faith, Foxbird, Dispel Magic, Petrify
Asura, Koutetsu, Murasame, Kikuichimoji



TruthDefined
Male
Libra
46
54
Archer
Jump
Mana Shield
Sicken
Fly

Ice Bow

Flash Hat
Power Sleeve
Germinas Boots

Charge+3, Charge+7, Charge+10
Level Jump8, Vertical Jump8



Lwtest
Female
Scorpio
44
68
Samurai
Charge
Critical Quick
Equip Axe
Teleport

Wizard Staff

Bronze Helmet
Linen Cuirass
Leather Mantle

Koutetsu, Murasame, Heaven's Cloud
Charge+2, Charge+5, Charge+7, Charge+10



Pplvee1
Female
Sagittarius
70
52
Mediator
Basic Skill
Absorb Used MP
Short Charge
Swim

Glacier Gun

Twist Headband
Chain Vest
Small Mantle

Invitation, Persuade, Praise, Preach, Death Sentence, Insult, Rehabilitate
Heal, Fury, Scream

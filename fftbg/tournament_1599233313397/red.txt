Player: !Red
Team: Red Team
Palettes: Red/Brown



Smashy
Male
Aquarius
61
71
Monk
Basic Skill
Faith Save
Halve MP
Swim



Holy Miter
Earth Clothes
Feather Mantle

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive
Accumulate, Dash, Throw Stone, Yell, Fury, Wish



Actual JP
Female
Taurus
42
79
Mime

Arrow Guard
Secret Hunt
Jump+3



Leather Hat
Adaman Vest
Diamond Armlet

Mimic




ALY327
Male
Capricorn
67
57
Knight
Throw
Arrow Guard
Magic Defense UP
Ignore Height

Broad Sword
Flame Shield
Diamond Helmet
Robe of Lords
Chantage

Justice Sword, Night Sword
Knife, Hammer



Latebit
Male
Pisces
67
61
Squire
Black Magic
Counter
Beastmaster
Ignore Height

Giant Axe
Diamond Shield
Thief Hat
Judo Outfit
Magic Gauntlet

Throw Stone, Heal, Tickle, Yell
Fire 2, Bolt, Bolt 4, Ice 2, Ice 4

Player: !White
Team: White Team
Palettes: White/Blue



Old Overholt
Female
Aquarius
58
66
Priest
Time Magic
Abandon
Equip Gun
Lava Walking

Fairy Harp

Red Hood
Wizard Robe
108 Gems

Raise, Protect 2, Shell 2, Wall, Esuna
Haste, Haste 2, Float, Stabilize Time



Scsuperstar
Male
Capricorn
54
46
Geomancer
Summon Magic
Parry
Equip Axe
Waterbreathing

Giant Axe
Genji Shield
Feather Hat
Chain Vest
Elf Mantle

Pitfall, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Titan, Golem, Carbunkle, Leviathan



NightwolfXVI
Female
Pisces
65
76
Archer
Throw
MA Save
Dual Wield
Fly

Lightning Bow

Headgear
Power Sleeve
Cursed Ring

Charge+1, Charge+7, Charge+20
Shuriken, Ninja Sword



Mesmaster
Female
Gemini
47
67
Summoner
Draw Out
Caution
Magic Attack UP
Move+2

Battle Folio

Flash Hat
Chameleon Robe
Sprint Shoes

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Bahamut, Salamander
Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji

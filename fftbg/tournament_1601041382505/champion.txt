Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Upvla
Female
Gemini
56
50
Ninja
Item
MP Restore
Equip Gun
Jump+3

Fairy Harp
Bloody Strings
Thief Hat
Leather Outfit
Dracula Mantle

Shuriken, Bomb
Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



AbandonedHall
Female
Gemini
51
47
Lancer
Talk Skill
PA Save
Equip Axe
Move+2

Slasher
Bronze Shield
Circlet
Chain Mail
Small Mantle

Level Jump5, Vertical Jump6
Persuade, Death Sentence, Mimic Daravon, Refute, Rehabilitate



VynxYukida
Monster
Sagittarius
76
54
Ultima Demon










Zachara
Male
Capricorn
48
68
Lancer
Steal
Faith Save
Doublehand
Teleport

Spear

Genji Helmet
Plate Mail
Vanish Mantle

Level Jump4, Vertical Jump8
Steal Heart, Steal Armor, Steal Accessory

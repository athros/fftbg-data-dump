Final Bets: purple - 7 bets for 21,766G (45.5%, x1.20); brown - 20 bets for 26,119G (54.5%, x0.83)

purple bets:
biske13: 20,000G (91.9%, 201,159G)
KasugaiRoastedPeas: 540G (2.5%, 540G)
WhiteDog420: 444G (2.0%, 1,836G)
resjudicata3: 324G (1.5%, 324G)
killth3kid: 250G (1.1%, 9,803G)
SnarkMage: 108G (0.5%, 566G)
nhammen: 100G (0.5%, 239,968G)

brown bets:
Mesmaster: 9,426G (36.1%, 9,426G)
Raixelol: 5,828G (22.3%, 5,828G)
NicoSavoy: 2,000G (7.7%, 105,946G)
Thyrandaal: 2,000G (7.7%, 64,557G)
CorpusCav: 1,500G (5.7%, 65,141G)
BirbBrainsBot: 1,000G (3.8%, 48,125G)
getthemoneyz: 1,000G (3.8%, 1,970,670G)
reinoe: 668G (2.6%, 668G)
robespyah: 566G (2.2%, 566G)
krombobreaker: 500G (1.9%, 4,126G)
lijarkh: 364G (1.4%, 364G)
AllInBot: 234G (0.9%, 234G)
iBardic: 200G (0.8%, 14,782G)
datadrivenbot: 200G (0.8%, 63,069G)
DHaveWord: 200G (0.8%, 10,016G)
markese: 132G (0.5%, 4,017G)
OneHundredFists: 100G (0.4%, 1,450G)
dire_drenz: 100G (0.4%, 1,176G)
MemoriesofFinal: 100G (0.4%, 28,985G)
latebit: 1G (0.0%, 17,462G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ruleof5
Female
Capricorn
62
63
Archer
Talk Skill
Parry
Long Status
Jump+2

Stone Gun
Hero Shield
Green Beret
Rubber Costume
Genji Gauntlet

Charge+2, Charge+3, Charge+4
Praise, Mimic Daravon, Refute



Sairentozon7
Female
Virgo
70
44
Ninja
Jump
Meatbone Slash
Defend
Jump+2

Main Gauche
Blind Knife
Golden Hairpin
Mythril Vest
Feather Boots

Shuriken, Bomb
Level Jump4, Vertical Jump7



Defaultlybrave
Female
Gemini
55
75
Monk
Draw Out
Parry
Doublehand
Teleport



Triangle Hat
Earth Clothes
Battle Boots

Spin Fist, Pummel, Purification, Chakra, Revive
Koutetsu, Bizen Boat



Moonliquor
Female
Libra
77
78
Wizard
Throw
Meatbone Slash
Magic Attack UP
Jump+2

Dagger

Feather Hat
Wizard Robe
Reflect Ring

Fire 2, Fire 4, Bolt, Bolt 2, Ice, Ice 4, Empower, Flare
Hammer

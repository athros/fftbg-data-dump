Player: !Red
Team: Red Team
Palettes: Red/Brown



YaBoy125
Male
Libra
48
70
Samurai
Throw
Faith Save
Equip Axe
Jump+2

Flail

Mythril Helmet
Linen Cuirass
Defense Ring

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Shuriken, Bomb, Ninja Sword



Just Here2
Male
Sagittarius
57
72
Squire
Item
Sunken State
Attack UP
Teleport

Coral Sword
Crystal Shield
Black Hood
Rubber Costume
Bracer

Dash, Heal, Cheer Up
Hi-Potion, X-Potion, Ether, Holy Water, Phoenix Down



Phi Sig
Male
Gemini
47
64
Knight
Draw Out
Meatbone Slash
Attack UP
Levitate

Defender
Hero Shield
Bronze Helmet
Carabini Mail
Spike Shoes

Shield Break, Mind Break, Justice Sword, Dark Sword, Explosion Sword
Koutetsu, Murasame, Kiyomori, Muramasa



Reddwind
Female
Virgo
77
49
Mediator
Punch Art
Sunken State
Beastmaster
Jump+3

Romanda Gun

Twist Headband
Silk Robe
Germinas Boots

Invitation, Mimic Daravon, Refute, Rehabilitate
Spin Fist, Wave Fist, Secret Fist, Revive, Seal Evil

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Actual JP
Female
Aries
68
47
Mime

Abandon
Equip Armor
Move+1



Gold Helmet
Earth Clothes
Elf Mantle

Mimic




CosmicTactician
Male
Pisces
64
65
Chemist
Basic Skill
PA Save
Equip Bow
Waterwalking

Bow Gun

Flash Hat
Leather Outfit
Elf Mantle

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down
Throw Stone, Heal, Yell



Lord Gwarth
Male
Libra
42
47
Knight
Sing
Damage Split
Equip Gun
Move+1

Blast Gun
Genji Shield
Bronze Helmet
Crystal Mail
Power Wrist

Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Night Sword
Angel Song, Cheer Song, Battle Song, Space Storage, Hydra Pit



Chuckolator
Female
Gemini
61
79
Squire
Time Magic
Catch
Beastmaster
Move-HP Up

Gastrafitis
Round Shield
Leather Helmet
Chain Vest
Cursed Ring

Heal
Haste, Haste 2, Stop, Immobilize, Demi, Meteor

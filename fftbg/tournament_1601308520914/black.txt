Player: !Black
Team: Black Team
Palettes: Black/Red



Lolthsmeat
Male
Capricorn
49
55
Knight
Item
Catch
Beastmaster
Teleport

Ragnarok
Buckler
Diamond Helmet
Mythril Armor
Dracula Mantle

Armor Break, Shield Break, Speed Break, Mind Break
Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Firesheath
Female
Sagittarius
46
39
Archer
Time Magic
Absorb Used MP
Attack UP
Retreat

Snipe Bow
Gold Shield
Mythril Helmet
Chain Vest
Leather Mantle

Charge+3, Charge+20
Haste, Slow 2, Stop



PoroTact
Female
Gemini
56
53
Samurai
Talk Skill
Parry
Doublehand
Move-HP Up

Chirijiraden

Leather Helmet
Platinum Armor
Spike Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori
Invitation, Negotiate, Mimic Daravon, Rehabilitate



AltimaMantoid
Female
Gemini
57
78
Summoner
Jump
Speed Save
Halve MP
Waterbreathing

Wizard Rod

Cachusha
Mystic Vest
Genji Gauntlet

Moogle, Golem, Carbunkle, Bahamut, Leviathan
Level Jump2, Vertical Jump8

Player: !Green
Team: Green Team
Palettes: Green/White



TheSabretoothe
Male
Virgo
78
64
Mime

Counter
Dual Wield
Jump+3



Golden Hairpin
Chain Vest
Leather Mantle

Mimic




Nizaha
Male
Sagittarius
76
47
Calculator
Gore Skill
Auto Potion
Short Status
Move+1

Papyrus Codex

Genji Helmet
Black Robe
Chantage

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



RunicMagus
Male
Taurus
39
51
Mediator
Steal
Faith Save
Dual Wield
Jump+1

Romanda Gun
Blast Gun
Green Beret
Leather Outfit
Magic Gauntlet

Praise, Preach, Solution, Refute, Rehabilitate
Gil Taking, Steal Armor, Steal Shield



LDSkinny
Female
Capricorn
75
81
Oracle
Elemental
Parry
Equip Sword
Fly

Heaven's Cloud

Twist Headband
Silk Robe
Magic Gauntlet

Blind, Poison, Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep, Dark Holy
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

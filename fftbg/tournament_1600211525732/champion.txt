Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Galkife
Female
Pisces
72
80
Mime

Counter
Defend
Levitate



Ribbon
Secret Clothes
Leather Mantle

Mimic




FriendSquirrel
Female
Virgo
45
58
Dancer
Throw
MP Restore
Equip Knife
Ignore Height

Thunder Rod

Headgear
Leather Outfit
Setiemson

Wiznaibus, Polka Polka, Last Dance
Shuriken



Latebit
Female
Libra
67
80
Wizard
Punch Art
Faith Save
Equip Sword
Move+2

Rune Blade

Twist Headband
Chameleon Robe
108 Gems

Fire, Fire 2, Fire 4, Bolt 3, Ice 4, Frog
Pummel, Wave Fist, Chakra, Seal Evil



Nekojin
Male
Libra
46
79
Calculator
Black Magic
Counter Tackle
Beastmaster
Teleport

Papyrus Codex

Red Hood
Linen Robe
Spike Shoes

CT, Height, Prime Number, 5, 4
Fire 3, Bolt, Ice 4, Frog, Death, Flare

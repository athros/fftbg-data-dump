Player: !White
Team: White Team
Palettes: White/Blue



ALY327
Female
Leo
65
68
Dancer
White Magic
Meatbone Slash
Equip Polearm
Teleport

Ivory Rod

Flash Hat
Black Robe
Elf Mantle

Disillusion, Nether Demon
Cure 2, Cure 4, Raise, Raise 2, Shell 2, Wall, Esuna



SeniorBunk
Female
Cancer
48
76
Calculator
Black Magic
Brave Save
Sicken
Ignore Terrain

Thunder Rod

Green Beret
Clothes
Sprint Shoes

CT, Height, Prime Number, 4
Fire, Fire 2, Bolt 2, Ice, Ice 3, Death



Zagorsek
Male
Pisces
80
62
Lancer
Yin Yang Magic
Caution
Magic Defense UP
Lava Walking

Mythril Spear
Diamond Shield
Genji Helmet
Linen Robe
Cursed Ring

Level Jump8, Vertical Jump7
Blind, Life Drain, Zombie, Paralyze, Petrify



ArchKnightX
Female
Aquarius
51
47
Archer
White Magic
Damage Split
Halve MP
Move-MP Up

Snipe Bow
Kaiser Plate
Feather Hat
Black Costume
Small Mantle

Charge+4, Charge+5, Charge+20
Cure 2, Cure 3, Raise, Reraise, Regen, Shell, Esuna

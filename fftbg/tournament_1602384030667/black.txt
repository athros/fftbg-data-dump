Player: !Black
Team: Black Team
Palettes: Black/Red



JonnyCue
Male
Cancer
49
42
Mediator
Yin Yang Magic
Distribute
Dual Wield
Lava Walking

Papyrus Codex
Papyrus Codex
Twist Headband
Leather Outfit
Wizard Mantle

Invitation, Death Sentence, Mimic Daravon, Rehabilitate
Poison, Spell Absorb, Life Drain, Foxbird, Paralyze, Dark Holy



DarrenDinosaurs
Male
Libra
45
74
Bard
Draw Out
Auto Potion
Beastmaster
Retreat

Ramia Harp

Triangle Hat
Chain Vest
Feather Mantle

Battle Song, Nameless Song, Last Song, Diamond Blade
Bizen Boat, Heaven's Cloud, Kiyomori



Lowlf
Male
Aquarius
74
71
Priest
Sing
Abandon
Attack UP
Teleport 2

Flail

Feather Hat
Wizard Outfit
Small Mantle

Cure 4, Raise, Protect, Shell, Shell 2, Wall, Esuna
Life Song, Battle Song, Sky Demon



ANFz
Male
Capricorn
59
69
Chemist
Elemental
Abandon
Maintenance
Move+2

Panther Bag

Headgear
Brigandine
Sprint Shoes

Potion, Ether, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Pitfall, Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball

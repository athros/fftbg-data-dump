Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Seppu777
Male
Capricorn
73
71
Lancer
Talk Skill
Brave Save
Attack UP
Waterwalking

Spear
Buckler
Diamond Helmet
Carabini Mail
Small Mantle

Level Jump3, Vertical Jump7
Praise, Preach, Death Sentence, Refute



Gorgewall
Female
Aries
55
70
Summoner
Punch Art
Caution
Magic Attack UP
Move+3

Wizard Staff

Holy Miter
Wizard Robe
Genji Gauntlet

Moogle, Shiva, Ramuh, Ifrit, Carbunkle
Pummel, Purification, Chakra, Revive



King Smashington
Male
Sagittarius
72
39
Geomancer
Punch Art
Counter Flood
Maintenance
Waterbreathing

Chirijiraden
Mythril Shield
Golden Hairpin
Linen Robe
Bracer

Water Ball, Quicksand, Gusty Wind
Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive



ChiChiRodriguez
Female
Sagittarius
62
71
Samurai
Charge
Meatbone Slash
Equip Shield
Jump+1

Obelisk
Round Shield
Leather Helmet
Wizard Robe
Magic Gauntlet

Koutetsu, Murasame, Heaven's Cloud, Muramasa
Charge+1, Charge+2

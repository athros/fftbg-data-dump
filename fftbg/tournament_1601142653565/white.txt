Player: !White
Team: White Team
Palettes: White/Blue



Run With Stone GUNs
Male
Aquarius
43
61
Bard
Time Magic
Counter Flood
Doublehand
Waterbreathing

Ramia Harp

Holy Miter
Mystic Vest
Genji Gauntlet

Battle Song, Magic Song, Last Song, Sky Demon
Haste 2, Slow 2, Stop, Quick, Demi, Stabilize Time, Meteor



Gelwain
Female
Taurus
39
68
Archer
Yin Yang Magic
MP Restore
Secret Hunt
Move-MP Up

Perseus Bow

Platinum Helmet
Wizard Outfit
Spike Shoes

Charge+1, Charge+2, Charge+5
Spell Absorb, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Ayeayex3
Male
Taurus
59
78
Mime

Caution
Martial Arts
Move-HP Up



Holy Miter
Earth Clothes
Cursed Ring

Mimic




Zachara
Male
Aquarius
80
64
Ninja
Charge
Brave Save
Equip Gun
Move-HP Up

Mythril Gun
Romanda Gun
Flash Hat
Clothes
Power Wrist

Shuriken, Bomb, Knife, Staff
Charge+1, Charge+5

Final Bets: white - 5 bets for 281,314G (90.3%, x0.11); black - 30 bets for 30,082G (9.7%, x9.35)

white bets:
nhammen: 277,365G (98.6%, 277,365G)
latebit: 2,139G (0.8%, 2,139G)
Zachara: 1,000G (0.4%, 146,500G)
resjudicata3: 610G (0.2%, 610G)
AllInBot: 200G (0.1%, 200G)

black bets:
Lord_Burrah: 10,000G (33.2%, 130,000G)
dogsandcatsand: 2,218G (7.4%, 2,218G)
otakutaylor: 1,676G (5.6%, 1,676G)
silentkaster: 1,500G (5.0%, 9,586G)
ShadowNinja25: 1,265G (4.2%, 1,265G)
Error72: 1,001G (3.3%, 4,513G)
nekojin: 1,000G (3.3%, 3,372G)
krombobreaker: 1,000G (3.3%, 23,858G)
leaferrm: 1,000G (3.3%, 2,403G)
Thyrandaal: 1,000G (3.3%, 173,410G)
SQUiDSQUARKLIN: 855G (2.8%, 855G)
gorgewall: 788G (2.6%, 788G)
mirapoix: 780G (2.6%, 780G)
Nizaha: 608G (2.0%, 9,533G)
lowlf: 604G (2.0%, 15,963G)
Willjin: 600G (2.0%, 600G)
ExplanationMarkWow: 500G (1.7%, 4,140G)
nok____: 500G (1.7%, 3,423G)
joewcarson: 460G (1.5%, 460G)
scsuperstar: 445G (1.5%, 445G)
pplvee1: 432G (1.4%, 432G)
getthemoneyz: 322G (1.1%, 2,088,061G)
BirbBrainsBot: 308G (1.0%, 308G)
wiznaibusthis: 300G (1.0%, 696G)
CosmicTactician: 300G (1.0%, 25,898G)
datadrivenbot: 200G (0.7%, 75,125G)
Lolthsmeat: 120G (0.4%, 120G)
RunicMagus: 100G (0.3%, 23,020G)
TakzSeason: 100G (0.3%, 100G)
king_smashington: 100G (0.3%, 3,132G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



ShadowNinja25
Male
Aquarius
56
79
Wizard
Charge
Counter
Maintenance
Waterbreathing

Air Knife

Black Hood
White Robe
Small Mantle

Fire 4, Bolt, Bolt 2, Bolt 3, Empower, Flare
Charge+1, Charge+2, Charge+3, Charge+5, Charge+10



Killth3kid
Male
Capricorn
44
53
Samurai
Sing
Speed Save
Defense UP
Levitate

Mythril Spear

Barbuta
Plate Mail
Small Mantle

Koutetsu, Bizen Boat, Murasame
Cheer Song, Last Song, Diamond Blade



Error72
Male
Gemini
56
47
Ninja
Time Magic
Meatbone Slash
Concentrate
Move+1

Spell Edge
Short Edge
Twist Headband
Adaman Vest
Elf Mantle

Shuriken, Stick
Haste, Slow, Reflect



Farseli
Male
Libra
60
50
Ninja
Black Magic
Hamedo
Equip Axe
Move+1

Sage Staff
Slasher
Twist Headband
Rubber Costume
Small Mantle

Sword, Wand
Bolt 3, Bolt 4

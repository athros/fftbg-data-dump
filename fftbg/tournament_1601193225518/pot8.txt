Final Bets: red - 7 bets for 3,132G (14.1%, x6.09); champion - 12 bets for 19,068G (85.9%, x0.16)

red bets:
BirbBrainsBot: 1,000G (31.9%, 22,934G)
OneHundredFists: 1,000G (31.9%, 8,322G)
CorpusCav: 400G (12.8%, 14,342G)
helpimabug: 276G (8.8%, 1,223G)
getthemoneyz: 206G (6.6%, 2,102,180G)
MemoriesofFinal: 200G (6.4%, 1,273G)
lotharek: 50G (1.6%, 1,381G)

champion bets:
iBluntDouble: 9,822G (51.5%, 9,822G)
LivingHitokiri: 5,000G (26.2%, 170,881G)
ko2q: 1,142G (6.0%, 2,284G)
ghazzzzzz: 754G (4.0%, 754G)
HaplessOne: 576G (3.0%, 576G)
AllInBot: 421G (2.2%, 421G)
lijarkh: 396G (2.1%, 1,892G)
LadyCyan: 300G (1.6%, 2,957G)
Lolthsmeat: 228G (1.2%, 228G)
datadrivenbot: 200G (1.0%, 78,222G)
DeathTaxesAndAnime: 125G (0.7%, 2,294G)
ayeCoop: 104G (0.5%, 104G)

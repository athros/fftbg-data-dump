Final Bets: black - 8 bets for 7,002G (70.3%, x0.42); purple - 7 bets for 2,958G (29.7%, x2.37)

black bets:
Lythe_Caraker: 3,974G (56.8%, 3,974G)
iBluntDouble: 1,000G (14.3%, 8,916G)
LivingHitokiri: 500G (7.1%, 171,170G)
gargola2498: 500G (7.1%, 1,946G)
CorpusCav: 400G (5.7%, 13,979G)
Lolthsmeat: 228G (3.3%, 228G)
AllInBot: 200G (2.9%, 200G)
datadrivenbot: 200G (2.9%, 78,338G)

purple bets:
BirbBrainsBot: 1,000G (33.8%, 23,450G)
bruubarg: 600G (20.3%, 600G)
HaplessOne: 576G (19.5%, 576G)
getthemoneyz: 382G (12.9%, 2,102,531G)
lotharek: 200G (6.8%, 1,484G)
OneHundredFists: 100G (3.4%, 8,922G)
MemoriesofFinal: 100G (3.4%, 955G)

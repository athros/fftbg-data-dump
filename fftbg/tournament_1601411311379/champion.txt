Player: !zChamp
Team: Champion Team
Palettes: Black/Red



HaateXIII
Male
Scorpio
83
93
Thief
Chaos Skill
Critical Quick
Doublehand
Fly

Zorlin Shape

Twist Headband
Adaman Vest
Dracula Mantle

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate



Escobro
Male
Cancer
54
89
Summoner
Tentacle Skill
Counter Magic
Secret Hunt
Move-MP Up

Dragon Rod

Leather Hat
Chameleon Robe
Defense Armlet

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



AbandonedHall
Female
Pisces
48
79
Chemist
Lucavi Skill
Caution
Concentrate
Teleport 2

Hydra Bag

Golden Hairpin
Black Costume
Leather Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Ruleof5
Female
Aquarius
88
47
Thief
Catfish Skill
PA Save
Dual Wield
Jump+1

Blind Knife
Cultist Dagger
Thief Hat
Wizard Outfit
108 Gems

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast

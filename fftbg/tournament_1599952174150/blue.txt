Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ANFz
Male
Gemini
64
70
Geomancer
White Magic
Regenerator
Equip Knife
Move+3

Orichalcum
Escutcheon
Golden Hairpin
Leather Outfit
Genji Gauntlet

Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind, Lava Ball
Cure 4, Raise, Reraise, Shell 2, Esuna



Reddwind
Male
Leo
82
45
Knight
Throw
Auto Potion
Equip Gun
Move+2

Romanda Gun
Gold Shield
Barbuta
Chain Mail
Power Wrist

Head Break, Armor Break, Power Break, Justice Sword
Hammer, Staff



Smashy
Female
Sagittarius
58
45
Mediator
Charge
Faith Save
Equip Shield
Levitate

Battle Folio
Genji Shield
Flash Hat
Black Costume
Dracula Mantle

Invitation, Praise, Preach, Refute
Charge+3, Charge+4



Run With Stone GUNs
Male
Gemini
45
54
Mediator
Elemental
Faith Save
Dual Wield
Ignore Terrain

Bestiary
Papyrus Codex
Holy Miter
Chameleon Robe
Small Mantle

Invitation, Persuade, Praise, Threaten, Solution, Insult, Refute
Pitfall, Water Ball, Hell Ivy, Static Shock, Blizzard, Lava Ball

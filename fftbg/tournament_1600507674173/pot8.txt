Final Bets: red - 6 bets for 32,452G (90.4%, x0.11); champion - 5 bets for 3,442G (9.6%, x9.43)

red bets:
fattunaking: 22,964G (70.8%, 22,964G)
hotpocketsofficial: 8,194G (25.2%, 8,194G)
arumz: 500G (1.5%, 37,572G)
AllInBot: 354G (1.1%, 354G)
BappleTrees: 240G (0.7%, 240G)
datadrivenbot: 200G (0.6%, 71,594G)

champion bets:
BirbBrainsBot: 1,000G (29.1%, 95,247G)
ruleof5: 1,000G (29.1%, 27,708G)
getthemoneyz: 642G (18.7%, 1,987,071G)
Raixelol: 600G (17.4%, 7,473G)
MemoriesofFinal: 200G (5.8%, 31,548G)

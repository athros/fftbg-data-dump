Player: !Red
Team: Red Team
Palettes: Red/Brown



Arumz
Female
Scorpio
43
78
Time Mage
Talk Skill
Counter Tackle
Long Status
Lava Walking

Iron Fan

Green Beret
Light Robe
Defense Ring

Haste 2, Slow, Stop, Immobilize, Quick, Demi 2, Stabilize Time
Praise, Preach, Solution, Refute



MemoriesofFinal
Female
Pisces
40
49
Wizard
Talk Skill
Arrow Guard
Halve MP
Ignore Terrain

Mythril Knife

Twist Headband
Mystic Vest
Power Wrist

Fire, Bolt 2, Bolt 3, Ice, Empower, Flare
Solution, Negotiate, Refute



Hotpocketsofficial
Female
Libra
40
72
Priest
Jump
Meatbone Slash
Equip Gun
Jump+2

Mythril Gun

Black Hood
Adaman Vest
N-Kai Armlet

Cure, Cure 3, Cure 4, Raise, Esuna
Level Jump4, Vertical Jump6



Fattunaking
Male
Leo
67
45
Monk
Charge
Damage Split
Long Status
Jump+1



Holy Miter
Adaman Vest
Bracer

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Charge+10

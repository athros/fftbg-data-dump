Player: !White
Team: White Team
Palettes: White/Blue



Leaferrm
Female
Pisces
76
73
Geomancer
Steal
Catch
Magic Defense UP
Swim

Giant Axe
Crystal Shield
Black Hood
Wizard Robe
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Steal Heart, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



Chuckolator
Female
Sagittarius
47
42
Mime

Meatbone Slash
Martial Arts
Swim



Black Hood
Chain Vest
Power Wrist

Mimic




NicoSavoy
Male
Cancer
54
62
Archer
Black Magic
Auto Potion
Equip Bow
Waterwalking

Bow Gun
Mythril Shield
Red Hood
Secret Clothes
Defense Armlet

Charge+1, Charge+3, Charge+4
Fire 3, Bolt, Ice 3, Death, Flare



Bradly
Male
Gemini
71
49
Chemist
Jump
Regenerator
Magic Defense UP
Jump+3

Star Bag

Holy Miter
Mystic Vest
Dracula Mantle

Potion, Ether, Remedy, Phoenix Down
Level Jump3, Vertical Jump7

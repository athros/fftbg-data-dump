Final Bets: red - 6 bets for 17,737G (74.2%, x0.35); blue - 10 bets for 6,177G (25.8%, x2.87)

red bets:
fattunaking: 12,633G (71.2%, 12,633G)
hotpocketsofficial: 2,364G (13.3%, 2,364G)
NicoSavoy: 2,000G (11.3%, 121,739G)
MemoriesofFinal: 300G (1.7%, 31,316G)
BappleTrees: 240G (1.4%, 240G)
datadrivenbot: 200G (1.1%, 70,905G)

blue bets:
DouglasDragonThePoet: 1,481G (24.0%, 1,481G)
BirbBrainsBot: 1,000G (16.2%, 96,868G)
ruleof5: 1,000G (16.2%, 27,793G)
bigbongsmoker: 764G (12.4%, 764G)
Raixelol: 600G (9.7%, 6,592G)
skipsandwiches: 428G (6.9%, 3,843G)
getthemoneyz: 368G (6.0%, 1,985,383G)
AllInBot: 200G (3.2%, 200G)
arumz: 200G (3.2%, 38,133G)
FriendlySkeleton: 136G (2.2%, 521G)

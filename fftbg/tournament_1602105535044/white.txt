Player: !White
Team: White Team
Palettes: White/Blue



Resjudicata3
Female
Aries
42
46
Summoner
Charge
Mana Shield
Short Charge
Ignore Terrain

Papyrus Codex

Triangle Hat
Clothes
Elf Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Fairy, Lich, Cyclops
Charge+4, Charge+5



Chihuahua Charity
Male
Virgo
41
65
Bard
Talk Skill
Meatbone Slash
Monster Talk
Jump+1

Bloody Strings

Feather Hat
Linen Cuirass
Battle Boots

Cheer Song, Battle Song, Sky Demon, Hydra Pit
Invitation, Threaten, Preach, Negotiate



DLJuggernaut
Female
Libra
46
47
Ninja
Time Magic
Meatbone Slash
Short Status
Waterbreathing

Flail
Hidden Knife
Holy Miter
Secret Clothes
Feather Mantle

Shuriken, Bomb
Haste



Technominari
Female
Gemini
51
43
Priest
Talk Skill
Dragon Spirit
Monster Talk
Waterbreathing

Gold Staff

Golden Hairpin
Chameleon Robe
Reflect Ring

Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Esuna, Holy
Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Rehabilitate

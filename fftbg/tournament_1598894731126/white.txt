Player: !White
Team: White Team
Palettes: White/Blue



Thyrandaal
Male
Serpentarius
59
55
Knight
Sing
Regenerator
Equip Axe
Move+2

Giant Axe
Kaiser Plate
Platinum Helmet
Maximillian
Magic Ring

Shield Break, Weapon Break, Power Break, Surging Sword
Cheer Song, Magic Song



StealthModeLocke
Female
Cancer
43
81
Knight
Punch Art
Caution
Equip Bow
Swim

Night Killer
Flame Shield
Iron Helmet
Chameleon Robe
Wizard Mantle

Shield Break, Magic Break, Speed Break, Mind Break, Justice Sword, Night Sword
Pummel, Wave Fist, Secret Fist, Purification, Revive



WhiteTigress
Male
Pisces
49
75
Lancer
Steal
Arrow Guard
Concentrate
Waterbreathing

Obelisk
Round Shield
Diamond Helmet
Silk Robe
Power Wrist

Level Jump3, Vertical Jump8
Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Steal Status



RunicMagus
Male
Gemini
52
69
Mediator
Time Magic
HP Restore
Dual Wield
Waterwalking

Blaze Gun
Glacier Gun
Holy Miter
Earth Clothes
Battle Boots

Praise, Threaten, Preach, Negotiate, Refute
Haste, Haste 2, Reflect, Quick, Demi 2

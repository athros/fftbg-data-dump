Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Laserman1000
Female
Cancer
57
70
Dancer
Talk Skill
Parry
Monster Talk
Lava Walking

Cute Bag

Thief Hat
Clothes
Leather Mantle

Disillusion, Last Dance
Persuade, Negotiate, Rehabilitate



Ko2q
Female
Scorpio
73
52
Samurai
Summon Magic
Catch
Equip Bow
Ignore Height

Windslash Bow

Barbuta
Genji Armor
Wizard Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa
Moogle, Shiva, Ifrit, Titan, Carbunkle, Cyclops



Gorgewall
Male
Taurus
47
46
Archer
Sing
Mana Shield
Defense UP
Move-HP Up

Windslash Bow

Bronze Helmet
Brigandine
Bracer

Charge+1, Charge+3, Charge+4
Last Song



DarrenDinosaurs
Female
Virgo
68
59
Summoner
Yin Yang Magic
HP Restore
Secret Hunt
Swim

Oak Staff

Triangle Hat
Clothes
Wizard Mantle

Moogle, Ifrit, Bahamut, Odin, Leviathan
Blind, Poison, Silence Song, Foxbird, Dispel Magic, Dark Holy

Final Bets: purple - 10 bets for 7,017G (11.0%, x8.09); champion - 13 bets for 56,781G (89.0%, x0.12)

purple bets:
hotpocketsofficial: 2,418G (34.5%, 2,418G)
BirbBrainsBot: 1,000G (14.3%, 199,048G)
ser_pyrro: 1,000G (14.3%, 7,287G)
Leonidusx: 1,000G (14.3%, 34,167G)
reddwind_: 432G (6.2%, 3,919G)
ColetteMSLP: 400G (5.7%, 7,436G)
getthemoneyz: 252G (3.6%, 1,945,009G)
HASTERIOUS: 215G (3.1%, 21,530G)
MemoriesofFinal: 200G (2.9%, 20,937G)
AllInBot: 100G (1.4%, 100G)

champion bets:
DeathTaxesAndAnime: 44,000G (77.5%, 48,503G)
LivingHitokiri: 5,926G (10.4%, 5,926G)
PopsLIVE: 1,992G (3.5%, 1,992G)
Raixelol: 1,463G (2.6%, 1,463G)
Arcblazer23: 931G (1.6%, 931G)
resjudicata3: 596G (1.0%, 596G)
rocl: 420G (0.7%, 6,664G)
Snowfats: 344G (0.6%, 344G)
ValensEXP_: 316G (0.6%, 6,623G)
ImmortalMage: 293G (0.5%, 293G)
khelor_: 200G (0.4%, 2,336G)
datadrivenbot: 200G (0.4%, 62,722G)
BartTradingCompany: 100G (0.2%, 9,718G)

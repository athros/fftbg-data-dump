Player: !Green
Team: Green Team
Palettes: Green/White



Wyonearth
Female
Pisces
79
61
Geomancer
Yin Yang Magic
Distribute
Martial Arts
Levitate


Venetian Shield
Leather Hat
Black Costume
Wizard Mantle

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Blind, Silence Song, Dispel Magic, Sleep, Petrify



BappleTrees
Male
Aries
74
77
Geomancer
White Magic
Absorb Used MP
Dual Wield
Ignore Terrain

Giant Axe
Giant Axe
Triangle Hat
Chameleon Robe
Magic Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Cure, Cure 3, Reraise, Regen, Protect, Shell, Wall, Esuna, Magic Barrier



Go2sleepTV
Monster
Serpentarius
52
69
Black Chocobo










Nekojin
Female
Leo
52
62
Monk
Item
Counter Flood
Attack UP
Move-MP Up



Red Hood
Mythril Vest
Magic Gauntlet

Wave Fist, Purification, Revive, Seal Evil
Potion, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down

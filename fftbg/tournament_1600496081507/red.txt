Player: !Red
Team: Red Team
Palettes: Red/Brown



BooshPlays
Male
Pisces
59
43
Priest
Charge
Caution
Magic Defense UP
Levitate

Sage Staff

Headgear
Wizard Robe
Sprint Shoes

Raise, Protect, Shell, Shell 2, Esuna
Charge+5



Reddwind
Male
Gemini
43
75
Knight
Draw Out
Caution
Magic Defense UP
Fly

Platinum Sword
Escutcheon
Leather Helmet
Crystal Mail
Battle Boots

Speed Break, Power Break, Stasis Sword
Murasame



Latebit
Male
Taurus
51
64
Mediator
Punch Art
Sunken State
Equip Armor
Swim

Stone Gun

Bronze Helmet
Brigandine
Cursed Ring

Praise, Death Sentence, Insult, Mimic Daravon
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Tithonus
Female
Aries
55
51
Geomancer
Dance
Counter Magic
Short Charge
Jump+2

Rune Blade
Bronze Shield
Barette
Linen Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Witch Hunt, Wiznaibus, Disillusion, Dragon Pit

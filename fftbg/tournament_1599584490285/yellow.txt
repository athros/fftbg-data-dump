Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sietaha
Female
Gemini
48
66
Samurai
Punch Art
Counter Magic
Dual Wield
Move+2

Masamune
Murasame
Gold Helmet
Genji Armor
Genji Gauntlet

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Pummel, Purification, Chakra, Seal Evil



E Ballard
Male
Libra
66
67
Calculator
Nether Skill
Caution
Short Charge
Move-HP Up

Dragon Rod

Cachusha
Diamond Armor
Feather Boots

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



CorpusCav
Female
Aries
45
57
Ninja
Draw Out
Faith Save
Equip Armor
Move+1

Scorpion Tail
Flame Whip
Circlet
Platinum Armor
Battle Boots

Shuriken, Bomb, Dictionary
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Lowlf
Male
Sagittarius
79
69
Bard
Item
MP Restore
Martial Arts
Move+3

Bloody Strings

Red Hood
Brigandine
Battle Boots

Battle Song, Magic Song, Nameless Song, Diamond Blade, Hydra Pit
X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss

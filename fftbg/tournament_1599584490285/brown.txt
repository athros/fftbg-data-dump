Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CapnChaos12
Female
Libra
54
44
Calculator
Limit
Counter Flood
Martial Arts
Teleport



Thief Hat
Mythril Armor
Salty Rage

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



J2DaBibbles
Male
Aquarius
48
70
Monk
Jump
Arrow Guard
Halve MP
Ignore Height



Flash Hat
Judo Outfit
Dracula Mantle

Wave Fist, Earth Slash, Purification, Revive
Level Jump4, Vertical Jump6



Moonliquor
Female
Capricorn
62
54
Knight
Time Magic
Abandon
Maintenance
Ignore Height

Mythril Sword
Mythril Shield
Circlet
Linen Robe
Diamond Armlet

Head Break, Stasis Sword, Justice Sword
Haste 2, Slow 2, Reflect, Demi



WhiteTigress
Male
Pisces
60
56
Squire
Draw Out
Faith Save
Equip Bow
Move+3

Long Bow

Cross Helmet
Chain Vest
Dracula Mantle

Heal, Scream
Asura, Bizen Boat, Heaven's Cloud, Muramasa

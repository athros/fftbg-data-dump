Player: !Red
Team: Red Team
Palettes: Red/Brown



E Ballard
Male
Libra
65
72
Archer
Yin Yang Magic
Counter Tackle
Equip Gun
Move+3

Bestiary
Escutcheon
Genji Helmet
Leather Outfit
Small Mantle

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Spell Absorb, Life Drain, Dispel Magic, Paralyze, Petrify



VolgraTheMoose
Male
Libra
50
64
Knight
Jump
Sunken State
Equip Knife
Move+3

Sasuke Knife
Escutcheon
Iron Helmet
Gold Armor
Jade Armlet

Head Break, Magic Break, Power Break, Mind Break
Level Jump2, Vertical Jump8



Forkmore
Female
Pisces
66
50
Wizard
Draw Out
Counter
Equip Shield
Lava Walking

Dragon Rod
Buckler
Leather Hat
Mystic Vest
Magic Gauntlet

Fire 3, Bolt 3, Flare
Heaven's Cloud, Kikuichimoji



CorpusCav
Female
Pisces
76
49
Chemist
Time Magic
MA Save
Long Status
Lava Walking

Panther Bag

Holy Miter
Clothes
N-Kai Armlet

Potion, X-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Haste, Haste 2, Stop, Immobilize, Reflect, Stabilize Time

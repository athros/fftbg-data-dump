Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Shalloween
Male
Taurus
54
58
Knight
Time Magic
Brave Save
Short Charge
Teleport 2

Slasher
Genji Shield
Crystal Helmet
Crystal Mail
Battle Boots

Head Break, Armor Break, Shield Break, Speed Break, Justice Sword
Haste, Slow, Immobilize, Quick, Demi, Meteor



Blaster Bomb
Monster
Gemini
46
43
Cockatrice










BooshPlays
Female
Virgo
71
70
Chemist
Draw Out
Regenerator
Equip Armor
Fly

Cultist Dagger

Triangle Hat
Diamond Armor
Battle Boots

Potion, Hi-Potion, X-Potion, Ether, Antidote, Soft, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori



DavenIII
Male
Gemini
52
66
Archer
Basic Skill
HP Restore
Maintenance
Jump+2

Snipe Bow
Crystal Shield
Green Beret
Black Costume
Battle Boots

Charge+2, Charge+5, Charge+20
Dash, Wish

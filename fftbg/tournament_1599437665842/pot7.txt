Final Bets: blue - 21 bets for 12,689G (42.1%, x1.37); purple - 17 bets for 17,434G (57.9%, x0.73)

blue bets:
DavenIII: 3,658G (28.8%, 7,316G)
latebit: 2,751G (21.7%, 2,751G)
BirbBrainsBot: 1,000G (7.9%, 60,581G)
Shalloween: 920G (7.3%, 1,840G)
OneHundredFists: 696G (5.5%, 696G)
AllInBot: 598G (4.7%, 598G)
Rikrizen: 382G (3.0%, 7,649G)
Lord_Gwarth: 350G (2.8%, 2,530G)
killth3kid: 250G (2.0%, 20,822G)
minorfffanatic1: 216G (1.7%, 216G)
BartTradingCompany: 200G (1.6%, 2,000G)
Blaster_Bomb: 200G (1.6%, 3,801G)
resjudicata3: 200G (1.6%, 2,232G)
ArrenJevleth: 200G (1.6%, 2,988G)
Sietaha: 200G (1.6%, 12,222G)
dinin991: 200G (1.6%, 3,570G)
Leonidusx: 200G (1.6%, 21,045G)
seppu777: 200G (1.6%, 1,883G)
RunicMagus: 100G (0.8%, 8,747G)
brenogarwin: 100G (0.8%, 790G)
getthemoneyz: 68G (0.5%, 1,858,218G)

purple bets:
Nickyfive: 10,000G (57.4%, 87,631G)
snkey: 2,993G (17.2%, 29,929G)
Zeroroute: 1,629G (9.3%, 4,793G)
highelectricaltemperature: 480G (2.8%, 3,699G)
SephDarkheart: 368G (2.1%, 8,740G)
krombobreaker: 328G (1.9%, 328G)
Runeseeker22: 280G (1.6%, 280G)
brodeity: 200G (1.1%, 1,143G)
datadrivenbot: 200G (1.1%, 54,216G)
Yangusburger: 200G (1.1%, 5,288G)
ValensEXP_: 200G (1.1%, 1,223G)
n4j1m14j1mu: 124G (0.7%, 124G)
PantherIscariot: 111G (0.6%, 4,526G)
gorgewall: 101G (0.6%, 3,915G)
DeathTaxesAndAnime: 100G (0.6%, 8,160G)
AbandonedHall: 100G (0.6%, 3,801G)
alcibiadeez: 20G (0.1%, 672G)

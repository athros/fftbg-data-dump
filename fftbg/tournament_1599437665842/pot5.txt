Final Bets: blue - 26 bets for 15,116G (45.6%, x1.19); green - 7 bets for 18,057G (54.4%, x0.84)

blue bets:
DavenIII: 2,290G (15.1%, 4,580G)
snkey: 2,279G (15.1%, 22,793G)
latebit: 1,709G (11.3%, 1,709G)
omegasuspekt: 1,618G (10.7%, 3,237G)
Shalloween: 1,152G (7.6%, 2,304G)
BirbBrainsBot: 1,000G (6.6%, 60,386G)
brodeity: 440G (2.9%, 440G)
minorfffanatic1: 410G (2.7%, 820G)
getthemoneyz: 376G (2.5%, 1,857,727G)
SephDarkheart: 368G (2.4%, 7,664G)
Lord_Gwarth: 350G (2.3%, 2,462G)
seppu777: 333G (2.2%, 1,101G)
krombobreaker: 328G (2.2%, 328G)
CorpusCav: 300G (2.0%, 19,763G)
dinin991: 300G (2.0%, 3,212G)
killth3kid: 250G (1.7%, 20,773G)
ValensEXP_: 204G (1.3%, 204G)
Blaster_Bomb: 200G (1.3%, 3,762G)
ArrenJevleth: 200G (1.3%, 2,949G)
datadrivenbot: 200G (1.3%, 54,177G)
Leonidusx: 200G (1.3%, 21,006G)
BartTradingCompany: 198G (1.3%, 1,985G)
PantherIscariot: 111G (0.7%, 4,393G)
AllInBot: 100G (0.7%, 100G)
DeathTaxesAndAnime: 100G (0.7%, 8,141G)
Yangusburger: 100G (0.7%, 4,996G)

green bets:
OneHundredFists: 13,375G (74.1%, 13,375G)
Zeroroute: 1,956G (10.8%, 5,149G)
Runeseeker22: 1,255G (7.0%, 1,255G)
VolgraTheMoose: 1,001G (5.5%, 128,360G)
highelectricaltemperature: 350G (1.9%, 3,504G)
AbandonedHall: 100G (0.6%, 3,728G)
alcibiadeez: 20G (0.1%, 657G)

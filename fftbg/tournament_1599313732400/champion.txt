Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Killth5
Monster
Virgo
71
45
Wyvern










Leonidusx
Male
Serpentarius
41
72
Knight
Jump
PA Save
Dual Wield
Ignore Height

Defender
Save the Queen
Bronze Helmet
Genji Armor
Defense Ring

Armor Break, Magic Break, Power Break, Mind Break
Level Jump2, Vertical Jump7



Error72
Female
Leo
50
72
Dancer
Punch Art
Distribute
Short Charge
Fly

Cashmere

Cachusha
Mystic Vest
Red Shoes

Slow Dance, Polka Polka
Earth Slash, Purification, Chakra, Revive, Seal Evil



Krombobreaker
Male
Pisces
72
55
Archer
Sing
Caution
Beastmaster
Jump+2

Mythril Bow

Genji Helmet
Secret Clothes
Red Shoes

Charge+2, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Angel Song, Life Song, Battle Song, Space Storage, Sky Demon

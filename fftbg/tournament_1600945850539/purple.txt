Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Just Here2
Male
Cancer
59
69
Archer
White Magic
Critical Quick
Doublehand
Retreat

Blast Gun

Barette
Judo Outfit
Salty Rage

Charge+2, Charge+20
Cure 2, Cure 4, Raise, Raise 2, Protect 2



ALY327
Male
Taurus
68
55
Knight
White Magic
Counter Flood
Doublehand
Move+1

Broad Sword

Crystal Helmet
Plate Mail
Wizard Mantle

Head Break, Shield Break, Mind Break, Dark Sword
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Protect, Shell 2, Esuna, Holy



NicoSavoy
Female
Scorpio
63
43
Summoner
Yin Yang Magic
Absorb Used MP
Doublehand
Jump+1

Healing Staff

Twist Headband
Judo Outfit
Red Shoes

Shiva, Ifrit, Bahamut, Lich, Cyclops
Poison, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy



Silentkaster
Male
Aquarius
65
41
Squire
Yin Yang Magic
Caution
Concentrate
Waterwalking

Rune Blade
Diamond Shield
Bronze Helmet
Clothes
Feather Mantle

Accumulate, Throw Stone, Heal, Tickle
Poison, Spell Absorb, Blind Rage, Dispel Magic, Paralyze, Petrify

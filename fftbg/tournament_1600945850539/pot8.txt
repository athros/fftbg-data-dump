Final Bets: black - 8 bets for 9,875G (59.5%, x0.68); champion - 12 bets for 6,720G (40.5%, x1.47)

black bets:
ser_pyrro: 2,527G (25.6%, 4,955G)
resjudicata3: 2,000G (20.3%, 17,341G)
peeronid: 1,813G (18.4%, 1,813G)
pplvee1: 1,180G (11.9%, 1,180G)
Zbgs: 1,000G (10.1%, 7,241G)
VolgraTheMoose: 555G (5.6%, 12,198G)
nok____: 500G (5.1%, 1,619G)
CosmicTactician: 300G (3.0%, 26,556G)

champion bets:
Lydian_C: 1,102G (16.4%, 1,102G)
lwtest: 1,000G (14.9%, 7,143G)
BirbBrainsBot: 1,000G (14.9%, 105,445G)
MemoriesofFinal: 1,000G (14.9%, 47,433G)
getthemoneyz: 654G (9.7%, 2,051,983G)
krombobreaker: 500G (7.4%, 3,521G)
SephDarkheart: 474G (7.1%, 474G)
wyonearth: 300G (4.5%, 4,823G)
HorusTaurus: 228G (3.4%, 228G)
Lolthsmeat: 212G (3.2%, 212G)
Genkidou: 150G (2.2%, 1,557G)
Error72: 100G (1.5%, 1,962G)

Player: !White
Team: White Team
Palettes: White/Blue



TheSabretoothe
Monster
Virgo
66
55
Hydra










DHaveWord
Male
Aries
69
55
Samurai
Black Magic
Damage Split
Beastmaster
Move+3

Chirijiraden

Bronze Helmet
Platinum Armor
Power Wrist

Muramasa
Death



E Ballard
Male
Leo
55
57
Thief
Yin Yang Magic
Arrow Guard
Long Status
Move-MP Up

Main Gauche

Leather Hat
Secret Clothes
Spike Shoes

Steal Helmet, Steal Armor, Steal Accessory
Blind, Poison, Pray Faith, Petrify



Mesmaster
Female
Libra
50
70
Wizard
Draw Out
Critical Quick
Defense UP
Jump+1

Dagger

Cachusha
White Robe
Defense Armlet

Fire 3, Fire 4, Bolt 3, Ice 3, Death, Flare
Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji

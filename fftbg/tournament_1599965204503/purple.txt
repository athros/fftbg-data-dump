Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gorgewall
Female
Virgo
47
79
Oracle
Jump
HP Restore
Short Charge
Retreat

Bestiary

Feather Hat
Silk Robe
Bracer

Life Drain, Zombie, Silence Song, Blind Rage, Paralyze
Level Jump8, Vertical Jump8



ANFz
Male
Cancer
52
52
Geomancer
Throw
Mana Shield
Equip Polearm
Move+3

Octagon Rod
Round Shield
Black Hood
Mythril Vest
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Shuriken, Bomb, Dictionary



Lwtest
Female
Aries
43
72
Geomancer
Summon Magic
Parry
Equip Armor
Move-HP Up

Broad Sword
Aegis Shield
Holy Miter
Crystal Mail
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Fairy, Cyclops



BooshPlays
Male
Aquarius
74
64
Mediator
Time Magic
Dragon Spirit
Halve MP
Move+2

Glacier Gun

Headgear
Linen Robe
Rubber Shoes

Threaten, Negotiate, Mimic Daravon, Refute
Haste, Immobilize, Reflect, Quick, Demi, Demi 2

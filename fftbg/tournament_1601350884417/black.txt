Player: !Black
Team: Black Team
Palettes: Black/Red



Smashy
Male
Gemini
44
61
Monk
Draw Out
Counter
Equip Gun
Move+3

Bestiary

Leather Hat
Brigandine
Bracer

Wave Fist, Earth Slash, Secret Fist, Chakra, Seal Evil
Koutetsu, Heaven's Cloud



Gelwain
Male
Virgo
42
52
Ninja
Yin Yang Magic
Mana Shield
Attack UP
Fly

Spell Edge
Hidden Knife
Barette
Mystic Vest
Angel Ring

Shuriken, Knife, Sword
Spell Absorb, Pray Faith, Blind Rage, Dispel Magic, Paralyze, Dark Holy



Mushufasa
Monster
Aries
43
50
Ahriman










DLJuggernaut
Male
Libra
77
66
Ninja
Elemental
Counter Flood
Equip Polearm
Retreat

Cashmere
Ryozan Silk
Flash Hat
Rubber Costume
Red Shoes

Shuriken, Bomb
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

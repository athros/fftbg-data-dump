Final Bets: purple - 12 bets for 8,539G (28.7%, x2.48); brown - 6 bets for 21,187G (71.3%, x0.40)

purple bets:
E_Ballard: 2,911G (34.1%, 2,911G)
BirbBrainsBot: 1,000G (11.7%, 5,076G)
3ngag3: 1,000G (11.7%, 17,810G)
NicoSavoy: 1,000G (11.7%, 11,907G)
seppu777: 500G (5.9%, 1,579G)
Sietaha: 500G (5.9%, 3,847G)
getthemoneyz: 456G (5.3%, 1,843,327G)
upvla: 456G (5.3%, 11,556G)
ColetteMSLP: 300G (3.5%, 4,759G)
fantastadon: 200G (2.3%, 3,438G)
BartTradingCompany: 116G (1.4%, 1,169G)
ValensEXP_: 100G (1.2%, 1,010G)

brown bets:
Mushufasa_: 19,106G (90.2%, 19,106G)
dem0nj0ns: 626G (3.0%, 626G)
Raixelol: 524G (2.5%, 7,093G)
ZPawZ: 500G (2.4%, 8,128G)
AltimaMantoid: 330G (1.6%, 4,930G)
gorgewall: 101G (0.5%, 3,610G)

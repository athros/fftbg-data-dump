Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zagorsek
Monster
Scorpio
75
51
Holy Dragon










LDSkinny
Female
Capricorn
53
48
Oracle
Basic Skill
Mana Shield
Long Status
Levitate

Battle Bamboo

Triangle Hat
Earth Clothes
Red Shoes

Poison, Life Drain, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Accumulate, Heal, Fury, Wish



Smashy
Male
Cancer
64
55
Mime

Absorb Used MP
Martial Arts
Fly



Twist Headband
Rubber Costume
Genji Gauntlet

Mimic




Nizaha
Female
Scorpio
59
66
Priest
Throw
Counter Flood
Maintenance
Ignore Height

Gold Staff

Ribbon
Leather Outfit
Diamond Armlet

Cure, Cure 2, Raise, Regen, Protect 2, Shell, Esuna, Holy
Bomb

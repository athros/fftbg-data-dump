Player: !White
Team: White Team
Palettes: White/Blue



Galkife
Female
Scorpio
57
63
Chemist
Yin Yang Magic
Meatbone Slash
Short Charge
Move+3

Cute Bag

Twist Headband
Adaman Vest
Power Wrist

Potion, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Poison, Petrify, Dark Holy



Shalloween
Male
Taurus
77
73
Samurai
Item
Auto Potion
Magic Defense UP
Move-MP Up

Asura Knife

Circlet
Maximillian
Red Shoes

Asura, Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji
Potion, Hi-Potion, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Thunderducker
Monster
Scorpio
52
57
Chocobo










Deathmaker06
Male
Scorpio
75
68
Time Mage
Black Magic
Meatbone Slash
Secret Hunt
Lava Walking

Wizard Staff

Black Hood
Chameleon Robe
Cursed Ring

Slow 2, Stop, Reflect, Demi, Stabilize Time, Meteor
Bolt 2, Bolt 3, Ice

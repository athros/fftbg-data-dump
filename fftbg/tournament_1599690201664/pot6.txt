Final Bets: black - 13 bets for 11,853G (39.3%, x1.54); purple - 20 bets for 18,307G (60.7%, x0.65)

black bets:
Thyrandaal: 3,989G (33.7%, 7,823G)
Mesmaster: 2,000G (16.9%, 223,496G)
CapnChaos12: 1,929G (16.3%, 1,929G)
DeathTaxesAndAnime: 1,052G (8.9%, 1,052G)
HaateXIII: 936G (7.9%, 936G)
thunderducker: 576G (4.9%, 576G)
old_overholt_: 500G (4.2%, 21,288G)
seppu777: 333G (2.8%, 2,673G)
thebluesman: 212G (1.8%, 212G)
heyBtw: 116G (1.0%, 116G)
AllInBot: 100G (0.8%, 100G)
maakur_: 100G (0.8%, 6,709G)
BartTradingCompany: 10G (0.1%, 107G)

purple bets:
reinoe: 5,000G (27.3%, 21,365G)
Wrath189: 2,000G (10.9%, 10,021G)
Lord_Burrah: 2,000G (10.9%, 114,000G)
VolgraTheMoose: 1,458G (8.0%, 1,458G)
dogsandcatsand: 1,428G (7.8%, 2,801G)
snkey: 1,087G (5.9%, 10,865G)
BirbBrainsBot: 1,000G (5.5%, 175,598G)
E_Ballard: 544G (3.0%, 544G)
krombobreaker: 500G (2.7%, 1,369G)
Smashy: 500G (2.7%, 7,931G)
mindblownnnn: 500G (2.7%, 2,006G)
Yangusburger: 500G (2.7%, 7,456G)
TheMurkGnome: 498G (2.7%, 11,982G)
Cryptopsy70: 360G (2.0%, 597G)
ValensEXP_: 244G (1.3%, 2,748G)
PuzzleSecretary: 200G (1.1%, 528G)
datadrivenbot: 200G (1.1%, 58,031G)
aceof86: 104G (0.6%, 104G)
miles5custom: 100G (0.5%, 100G)
getthemoneyz: 84G (0.5%, 1,898,638G)

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Seppu777
Female
Aries
41
67
Ninja
Item
Caution
Sicken
Retreat

Sasuke Knife
Hidden Knife
Black Hood
Power Sleeve
Power Wrist

Wand, Dictionary
Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Phoenix Down



Run With Stone GUNs
Male
Leo
69
53
Bard
Draw Out
Damage Split
Doublehand
Jump+3

Ultimus Bow

Golden Hairpin
Earth Clothes
Red Shoes

Angel Song, Battle Song, Diamond Blade, Space Storage
Koutetsu, Muramasa



Silentkaster
Female
Taurus
39
54
Priest
Throw
Faith Save
Short Charge
Retreat

Oak Staff

Leather Hat
Wizard Robe
N-Kai Armlet

Cure 2, Cure 3, Raise, Raise 2, Wall, Esuna
Spear, Wand



DeathTaxesAndAnime
Female
Cancer
51
51
Thief
Charge
Counter Tackle
Equip Gun
Waterbreathing

Mythril Gun

Black Hood
Judo Outfit
Magic Ring

Gil Taking, Steal Heart, Steal Shield
Charge+2

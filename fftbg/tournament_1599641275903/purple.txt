Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ruleof5
Female
Cancer
62
56
Archer
Steal
PA Save
Doublehand
Move+2

Snipe Bow

Golden Hairpin
Earth Clothes
Salty Rage

Charge+3, Charge+5, Charge+7
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Status, Leg Aim



Grininda
Female
Gemini
38
61
Priest
Draw Out
Distribute
Sicken
Swim

White Staff

Flash Hat
Wizard Robe
Red Shoes

Cure, Cure 3, Cure 4, Raise, Raise 2, Shell, Shell 2, Esuna
Muramasa



Leonidusx
Male
Taurus
53
49
Knight
Steal
Auto Potion
Equip Axe
Move-MP Up

Healing Staff
Crystal Shield
Cross Helmet
Plate Mail
Magic Gauntlet

Armor Break, Power Break
Steal Helmet, Leg Aim



Kirbie
Female
Capricorn
66
71
Geomancer
Summon Magic
Caution
Equip Knife
Ignore Height

Sasuke Knife
Bronze Shield
Flash Hat
Wizard Robe
N-Kai Armlet

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin, Salamander, Silf, Fairy, Cyclops

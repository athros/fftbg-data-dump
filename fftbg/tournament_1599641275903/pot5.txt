Final Bets: red - 7 bets for 3,791G (34.8%, x1.87); yellow - 9 bets for 7,095G (65.2%, x0.53)

red bets:
NicoSavoy: 2,000G (52.8%, 41,902G)
BirbBrainsBot: 1,000G (26.4%, 157,583G)
MemoriesofFinal: 215G (5.7%, 3,679G)
CassiePhoenix: 200G (5.3%, 5,035G)
getthemoneyz: 168G (4.4%, 1,895,011G)
Ogema_theDream: 108G (2.8%, 108G)
AllInBot: 100G (2.6%, 100G)

yellow bets:
NovaKnight21: 3,000G (42.3%, 13,747G)
Lythe_Caraker: 1,000G (14.1%, 168,014G)
Arcblazer23: 1,000G (14.1%, 6,157G)
Leonidusx: 615G (8.7%, 615G)
Raixelol: 568G (8.0%, 26,684G)
Neech: 448G (6.3%, 2,128G)
AltimaMantoid: 250G (3.5%, 5,750G)
datadrivenbot: 200G (2.8%, 57,172G)
BartTradingCompany: 14G (0.2%, 141G)

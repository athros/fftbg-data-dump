Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Male
Leo
50
66
Bard
Item
Distribute
Dual Wield
Ignore Height

Ice Bow

Red Hood
Mythril Armor
Elf Mantle

Life Song, Battle Song
Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



HorusTaurus
Male
Virgo
69
79
Monk
White Magic
Auto Potion
Equip Bow
Waterbreathing

Yoichi Bow

Feather Hat
Judo Outfit
Magic Gauntlet

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Esuna



BuffaloCrunch
Male
Virgo
81
63
Ninja
Yin Yang Magic
Mana Shield
Equip Gun
Retreat

Glacier Gun
Glacier Gun
Black Hood
Brigandine
Feather Boots

Staff, Stick
Blind, Pray Faith, Doubt Faith, Dispel Magic, Dark Holy



Run With Stone GUNs
Male
Gemini
75
52
Mediator
Draw Out
Counter Flood
Dual Wield
Move+2

Blaze Gun
Blast Gun
Ribbon
Light Robe
Feather Boots

Persuade, Preach, Solution, Death Sentence
Koutetsu

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Blaster Bomb
Female
Virgo
59
68
Time Mage
Elemental
Abandon
Short Charge
Move-MP Up

Cypress Rod

Ribbon
Linen Robe
Feather Boots

Haste, Haste 2, Slow 2, Reflect, Quick, Stabilize Time, Meteor
Hallowed Ground, Local Quake, Blizzard, Lava Ball



ValensEXP
Male
Libra
47
72
Samurai
Sing
MA Save
Attack UP
Move+1

Kiyomori

Gold Helmet
Chain Mail
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame
Life Song, Cheer Song, Battle Song, Hydra Pit



Yangusburger
Female
Taurus
43
49
Ninja
Charge
Caution
Equip Gun
Fly

Battle Folio
Papyrus Codex
Feather Hat
Earth Clothes
Germinas Boots

Shuriken, Ninja Sword, Wand
Charge+3, Charge+7



RagequitSA
Female
Serpentarius
70
45
Dancer
Item
Sunken State
Secret Hunt
Levitate

Cashmere

Black Hood
Black Robe
Dracula Mantle

Witch Hunt, Wiznaibus, Disillusion, Nameless Dance, Last Dance
Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down

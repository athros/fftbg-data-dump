Final Bets: black - 9 bets for 2,733G (19.3%, x4.19); brown - 13 bets for 11,442G (80.7%, x0.24)

black bets:
ApplesNP: 1,000G (36.6%, 12,629G)
Snowfats: 500G (18.3%, 5,940G)
ColetteMSLP: 400G (14.6%, 3,657G)
getthemoneyz: 246G (9.0%, 1,903,011G)
BirbBrainsBot: 236G (8.6%, 173,356G)
Morsigil: 104G (3.8%, 104G)
Yangusburger: 100G (3.7%, 3,609G)
farside00831: 100G (3.7%, 2,020G)
BartTradingCompany: 47G (1.7%, 471G)

brown bets:
DeathTaxesAndAnime: 3,092G (27.0%, 6,063G)
BappleTrees: 2,491G (21.8%, 2,491G)
loveyouallfriends: 1,538G (13.4%, 1,538G)
HaateXIII: 940G (8.2%, 940G)
Riley331: 723G (6.3%, 723G)
TheSabretoothe: 564G (4.9%, 564G)
OneHundredFists: 554G (4.8%, 554G)
lowlf: 492G (4.3%, 3,667G)
ribbiks: 252G (2.2%, 252G)
ValensEXP_: 248G (2.2%, 3,266G)
helpimabug: 248G (2.2%, 2,214G)
datadrivenbot: 200G (1.7%, 57,128G)
AllInBot: 100G (0.9%, 100G)

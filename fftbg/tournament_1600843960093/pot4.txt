Final Bets: purple - 12 bets for 15,234G (69.1%, x0.45); brown - 12 bets for 6,823G (30.9%, x2.23)

purple bets:
ser_pyrro: 10,400G (68.3%, 20,393G)
Lythe_Caraker: 1,000G (6.6%, 182,920G)
BirbBrainsBot: 1,000G (6.6%, 45,942G)
Sairentozon7: 1,000G (6.6%, 38,088G)
XerxesMindbreaker: 500G (3.3%, 1,203G)
Setsaku: 323G (2.1%, 323G)
ColetteMSLP: 300G (2.0%, 3,651G)
AllInBot: 200G (1.3%, 200G)
datadrivenbot: 200G (1.3%, 73,741G)
NicoSavoy: 111G (0.7%, 140,872G)
J_52: 100G (0.7%, 1,429G)
killabandaid: 100G (0.7%, 3,545G)

brown bets:
DeathTaxesAndAnime: 1,608G (23.6%, 1,608G)
OneHundredFists: 1,141G (16.7%, 1,141G)
Mushufasa_: 1,000G (14.7%, 83,725G)
upvla: 600G (8.8%, 600G)
Mhorzak: 500G (7.3%, 1,090G)
CorpusCav: 500G (7.3%, 5,935G)
ValensEXP_: 372G (5.5%, 5,867G)
getthemoneyz: 352G (5.2%, 2,042,841G)
Genkidou: 250G (3.7%, 2,172G)
Caster_Daily: 200G (2.9%, 1,567G)
chrisk103: 200G (2.9%, 3,100G)
MemoriesofFinal: 100G (1.5%, 38,426G)

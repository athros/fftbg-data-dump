Final Bets: black - 16 bets for 17,702G (52.7%, x0.90); brown - 19 bets for 15,876G (47.3%, x1.12)

black bets:
resjudicata3: 8,241G (46.6%, 16,160G)
J_MacC1991: 2,568G (14.5%, 2,568G)
Willjin: 2,000G (11.3%, 42,042G)
thunderducker: 1,200G (6.8%, 5,256G)
snkey: 1,000G (5.6%, 6,932G)
Rytor: 500G (2.8%, 500G)
idioSyncr4zy: 437G (2.5%, 8,751G)
roofiepops: 404G (2.3%, 404G)
brokenknight201: 400G (2.3%, 28,796G)
Blaster_Bomb: 250G (1.4%, 4,675G)
datadrivenbot: 200G (1.1%, 61,714G)
Chuckolator: 112G (0.6%, 24,635G)
BartTradingCompany: 100G (0.6%, 7,609G)
RunicMagus: 100G (0.6%, 9,870G)
VolgraTheMoose: 100G (0.6%, 4,233G)
7Cerulean7: 90G (0.5%, 4,418G)

brown bets:
Thyrandaal: 3,403G (21.4%, 3,403G)
OneHundredFists: 1,953G (12.3%, 1,953G)
Zerguzen: 1,573G (9.9%, 1,573G)
SkylerBunny: 1,441G (9.1%, 1,441G)
BoneMiser: 1,074G (6.8%, 1,074G)
BirbBrainsBot: 1,000G (6.3%, 90,680G)
loveyouallfriends: 1,000G (6.3%, 36,506G)
Shalloween: 832G (5.2%, 832G)
latebit: 654G (4.1%, 654G)
technominari: 500G (3.1%, 77,913G)
krombobreaker: 500G (3.1%, 13,103G)
Oreo_Pizza: 500G (3.1%, 239,489G)
AllInBot: 374G (2.4%, 374G)
ArrenJevleth: 300G (1.9%, 10,224G)
ValensEXP_: 280G (1.8%, 1,329G)
DAC169: 200G (1.3%, 2,074G)
DHaveWord: 100G (0.6%, 6,761G)
MemoriesofFinal: 100G (0.6%, 7,891G)
getthemoneyz: 92G (0.6%, 1,925,104G)

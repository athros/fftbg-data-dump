Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Heropong
Male
Aries
54
77
Thief
Black Magic
Meatbone Slash
Attack UP
Retreat

Dagger

Black Hood
Leather Outfit
Battle Boots

Steal Heart, Steal Armor, Steal Shield, Steal Status, Arm Aim
Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Ice 3



Smashy
Female
Cancer
41
56
Monk
Jump
Speed Save
Equip Armor
Move+2



Crystal Helmet
Platinum Armor
Power Wrist

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification
Level Jump3, Vertical Jump3



Reddwind
Male
Virgo
44
72
Knight
Jump
Meatbone Slash
Attack UP
Jump+1

Ice Brand
Bronze Shield
Platinum Helmet
White Robe
108 Gems

Shield Break, Weapon Break, Speed Break, Power Break, Justice Sword, Night Sword
Level Jump8, Vertical Jump8



Lolthsmeat
Female
Aries
70
63
Time Mage
Draw Out
Speed Save
Equip Shield
Ignore Height

Oak Staff
Platinum Shield
Headgear
Robe of Lords
Genji Gauntlet

Haste, Haste 2, Stop, Float, Reflect, Demi 2, Stabilize Time
Koutetsu, Bizen Boat

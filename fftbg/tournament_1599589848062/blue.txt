Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Snkey
Female
Capricorn
53
65
Dancer
Draw Out
Distribute
Doublehand
Move-HP Up

Ryozan Silk

Ribbon
Judo Outfit
Red Shoes

Witch Hunt, Slow Dance, Polka Polka, Obsidian Blade, Nether Demon, Dragon Pit
Asura, Koutetsu, Murasame, Kiyomori, Kikuichimoji



Lifebregin
Male
Aries
69
55
Calculator
Byblos
Mana Shield
Concentrate
Teleport

Madlemgen
Diamond Shield
Holy Miter
Plate Mail
Elf Mantle

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Farside00831
Female
Leo
78
60
Chemist
Charge
Speed Save
Beastmaster
Jump+1

Hydra Bag

Golden Hairpin
Mystic Vest
Magic Gauntlet

Potion, Hi-Potion, Soft, Phoenix Down
Charge+1, Charge+4



Powergems
Female
Aries
42
51
Mime

Meatbone Slash
Dual Wield
Waterbreathing



Headgear
Linen Cuirass
Wizard Mantle

Mimic


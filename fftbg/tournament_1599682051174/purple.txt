Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Error72
Male
Taurus
60
43
Thief
Item
Counter
Throw Item
Move-HP Up

Ninja Edge

Flash Hat
Mystic Vest
Battle Boots

Steal Armor, Steal Accessory, Steal Status
Potion, Hi-Ether, Antidote, Eye Drop, Holy Water



Gorgewall
Male
Capricorn
53
46
Knight
Black Magic
PA Save
Doublehand
Ignore Terrain

Battle Axe

Genji Helmet
Plate Mail
Elf Mantle

Head Break, Armor Break, Speed Break, Dark Sword
Bolt 3, Ice, Ice 3



LanseDM
Male
Capricorn
51
63
Samurai
Jump
MA Save
Equip Axe
Jump+3

Battle Axe

Gold Helmet
Platinum Armor
Genji Gauntlet

Asura, Koutetsu, Murasame, Muramasa
Level Jump3, Vertical Jump4



Lowlf
Female
Aquarius
53
43
Calculator
Demon Skill
Faith Save
Equip Bow
Move-MP Up

Silver Bow

Crystal Helmet
Gold Armor
Wizard Mantle

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2

Final Bets: green - 14 bets for 12,383G (73.7%, x0.36); yellow - 9 bets for 4,417G (26.3%, x2.80)

green bets:
VolgraTheMoose: 2,410G (19.5%, 4,821G)
Lord_Burrah: 1,776G (14.3%, 94,776G)
upvla: 1,535G (12.4%, 1,535G)
Sairentozon7: 1,000G (8.1%, 27,677G)
PoroTact: 876G (7.1%, 876G)
Runeseeker22: 858G (6.9%, 8,588G)
nifboy: 780G (6.3%, 9,416G)
Zbgs: 700G (5.7%, 700G)
dogsandcatsand: 668G (5.4%, 43,994G)
FoeSquirrel: 624G (5.0%, 1,249G)
robespyah: 556G (4.5%, 556G)
AllInBot: 200G (1.6%, 200G)
khelor_: 200G (1.6%, 3,170G)
datadrivenbot: 200G (1.6%, 71,870G)

yellow bets:
BirbBrainsBot: 1,000G (22.6%, 98,078G)
Nizaha: 860G (19.5%, 78,674G)
reinoe: 676G (15.3%, 676G)
Raixelol: 600G (13.6%, 7,603G)
getthemoneyz: 412G (9.3%, 1,983,076G)
ericzubat: 400G (9.1%, 10,892G)
ser_pyrro: 319G (7.2%, 319G)
Zerguzen: 100G (2.3%, 17,851G)
Lord_Gwarth: 50G (1.1%, 6,732G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Chuckolator
Female
Scorpio
53
78
Samurai
Jump
Hamedo
Martial Arts
Move+3



Crystal Helmet
Crystal Mail
Sprint Shoes

Bizen Boat, Muramasa, Kikuichimoji
Level Jump4, Vertical Jump7



BlackAceBB
Female
Libra
65
76
Time Mage
Throw
Counter Flood
Maintenance
Move-MP Up

Battle Bamboo

Red Hood
Judo Outfit
108 Gems

Haste, Float, Quick
Sword, Staff



Nifboy
Female
Gemini
64
74
Geomancer
Talk Skill
Counter Tackle
Equip Bow
Teleport 2

Mythril Bow

Twist Headband
Brigandine
Diamond Armlet

Hallowed Ground, Will-O-Wisp, Blizzard
Solution, Death Sentence, Insult, Refute, Rehabilitate



Actual JP
Female
Cancer
55
62
Lancer
Elemental
Critical Quick
Attack UP
Jump+3

Obelisk
Genji Shield
Diamond Helmet
Reflect Mail
N-Kai Armlet

Level Jump3, Vertical Jump6
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball

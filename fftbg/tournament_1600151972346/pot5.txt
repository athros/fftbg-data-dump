Final Bets: red - 17 bets for 40,358G (47.1%, x1.12); green - 13 bets for 45,344G (52.9%, x0.89)

red bets:
DeathTaxesAndAnime: 28,384G (70.3%, 55,655G)
Sairentozon7: 2,838G (7.0%, 12,838G)
SkylerBunny: 2,032G (5.0%, 2,032G)
old_overholt_: 2,000G (5.0%, 28,039G)
MemoriesofFinal: 1,000G (2.5%, 15,849G)
BirbBrainsBot: 1,000G (2.5%, 158,931G)
lowlf: 516G (1.3%, 516G)
rocl: 412G (1.0%, 412G)
DaveStrider55: 368G (0.9%, 368G)
superdevon1: 332G (0.8%, 332G)
khelor_: 300G (0.7%, 1,021G)
hotpocketsofficial: 272G (0.7%, 272G)
Jabuamba: 261G (0.6%, 261G)
Zachara: 243G (0.6%, 152,243G)
datadrivenbot: 200G (0.5%, 60,657G)
BartTradingCompany: 100G (0.2%, 9,688G)
gingerfoo69: 100G (0.2%, 1,040G)

green bets:
AllInBot: 33,688G (74.3%, 33,688G)
OneHundredFists: 5,000G (11.0%, 56,969G)
Wonser: 2,953G (6.5%, 5,791G)
Raixelol: 600G (1.3%, 600G)
Laserman1000: 500G (1.1%, 5,700G)
J_52: 450G (1.0%, 5,326G)
prince_rogers_nelson_: 420G (0.9%, 420G)
PolkaGrandma: 345G (0.8%, 345G)
Neech: 316G (0.7%, 316G)
ValensEXP_: 308G (0.7%, 1,947G)
ColetteMSLP: 300G (0.7%, 480G)
getthemoneyz: 264G (0.6%, 1,944,825G)
Rune339: 200G (0.4%, 3,092G)

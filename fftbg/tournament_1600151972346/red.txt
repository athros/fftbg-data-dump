Player: !Red
Team: Red Team
Palettes: Red/Brown



Prince Rogers Nelson
Male
Leo
79
53
Time Mage
Black Magic
Brave Save
Long Status
Jump+3

Octagon Rod

Headgear
Light Robe
Elf Mantle

Haste, Haste 2, Reflect, Demi 2
Fire, Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Death



SkylerBunny
Male
Virgo
74
62
Lancer
Time Magic
Critical Quick
Attack UP
Move+2

Star Bag
Flame Shield
Diamond Helmet
Platinum Armor
Defense Ring

Level Jump8, Vertical Jump8
Haste, Immobilize, Quick, Demi, Demi 2



Reddwind
Male
Aries
49
61
Knight
Time Magic
Counter Flood
Magic Attack UP
Ignore Terrain

Sleep Sword
Round Shield
Barbuta
Crystal Mail
Battle Boots

Head Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword
Haste 2, Slow 2, Float, Reflect, Quick, Demi 2, Stabilize Time



DeathTaxesAndAnime
Female
Leo
40
63
Mediator
Steal
MP Restore
Dual Wield
Fly

Madlemgen
Bestiary
Triangle Hat
Power Sleeve
Reflect Ring

Persuade, Praise, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon
Steal Heart, Steal Accessory, Steal Status, Arm Aim

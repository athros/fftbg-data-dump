Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Sinnyil2
Monster
Virgo
78
45
Ultima Demon










Deathmaker06
Male
Serpentarius
71
64
Archer
Item
Counter Magic
Equip Gun
Retreat

Stone Gun
Crystal Shield
Flash Hat
Black Costume
Wizard Mantle

Charge+2, Charge+5, Charge+20
Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down



Zeroroute
Male
Libra
78
60
Geomancer
Jump
Brave Save
Attack UP
Waterbreathing

Giant Axe
Escutcheon
Cachusha
White Robe
108 Gems

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump3, Vertical Jump8



Dasutin23
Female
Scorpio
69
70
Calculator
Gore Skill
Brave Save
Doublehand
Levitate

Gokuu Rod

Leather Hat
Wizard Outfit
Red Shoes

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bilabrin
Female
Gemini
59
49
Archer
Dance
Speed Save
Attack UP
Ignore Height

Night Killer
Ice Shield
Feather Hat
Clothes
Magic Gauntlet

Charge+3, Charge+4, Charge+10
Wiznaibus, Last Dance, Dragon Pit



SeniorBunk
Male
Cancer
54
77
Knight
Black Magic
Arrow Guard
Attack UP
Ignore Height

Battle Axe
Platinum Shield
Iron Helmet
Linen Robe
Cursed Ring

Armor Break, Weapon Break, Speed Break, Mind Break
Fire, Fire 4, Bolt 3, Ice 3, Frog, Flare



VolgraTheMoose
Female
Aquarius
70
51
Summoner
Draw Out
Damage Split
Defense UP
Move-HP Up

Flame Rod

Holy Miter
Judo Outfit
Small Mantle

Ramuh, Titan, Carbunkle, Fairy
Bizen Boat, Muramasa, Masamune, Chirijiraden



Snkey
Male
Leo
51
46
Mime

Mana Shield
Sicken
Retreat


Hero Shield
Flash Hat
Adaman Vest
Elf Mantle

Mimic


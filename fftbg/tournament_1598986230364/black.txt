Player: !Black
Team: Black Team
Palettes: Black/Red



Krombobreaker
Female
Aquarius
72
73
Priest
Black Magic
Distribute
Equip Knife
Levitate

Ice Rod

Red Hood
Black Costume
Magic Ring

Cure, Cure 2, Cure 4, Shell, Esuna
Fire 2, Bolt, Ice 2, Ice 3, Ice 4, Death



Latebit
Male
Aries
42
63
Lancer
Black Magic
Caution
Secret Hunt
Jump+1

Obelisk
Platinum Shield
Cross Helmet
Diamond Armor
Jade Armlet

Level Jump5, Vertical Jump7
Fire 4, Ice, Ice 2, Ice 3



RobotOcelot
Female
Cancer
71
53
Ninja
Basic Skill
Counter
Equip Sword
Jump+3

Murasame
Asura Knife
Ribbon
Wizard Outfit
Spike Shoes

Shuriken, Bomb, Stick
Cheer Up, Fury, Scream



Giganticdwarves
Male
Cancer
76
59
Ninja
Sing
Counter
Equip Bow
Move+2

Hunting Bow
Poison Bow
Leather Hat
Mystic Vest
Power Wrist

Bomb
Battle Song, Magic Song, Diamond Blade

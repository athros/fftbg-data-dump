Final Bets: purple - 17 bets for 12,932G (65.4%, x0.53); brown - 9 bets for 6,832G (34.6%, x1.89)

purple bets:
SephDarkheart: 2,294G (17.7%, 2,294G)
VolgraTheMoose: 2,001G (15.5%, 121,039G)
HaateXIII: 1,310G (10.1%, 1,310G)
DeathTaxesAndAnime: 1,001G (7.7%, 9,915G)
BirbBrainsBot: 1,000G (7.7%, 81,478G)
getthemoneyz: 968G (7.5%, 1,817,948G)
Nizaha: 816G (6.3%, 82,538G)
Laserman1000: 748G (5.8%, 748G)
RunicMagus: 500G (3.9%, 9,570G)
Zeroroute: 491G (3.8%, 491G)
ColetteMSLP: 428G (3.3%, 428G)
minorfffanatic1: 406G (3.1%, 812G)
acid_flashback: 358G (2.8%, 358G)
CosmicTactician: 200G (1.5%, 49,193G)
datadrivenbot: 200G (1.5%, 64,877G)
PantherIscariot: 111G (0.9%, 2,873G)
rottings0ul: 100G (0.8%, 1,362G)

brown bets:
Mesmaster: 2,000G (29.3%, 182,323G)
Thyrandaal: 1,201G (17.6%, 1,201G)
E_Ballard: 868G (12.7%, 131,277G)
AltimaMantoid: 597G (8.7%, 597G)
nekojin: 536G (7.8%, 536G)
CorpusCav: 500G (7.3%, 5,200G)
Greggernaut: 500G (7.3%, 16,604G)
Lord_Gwarth: 350G (5.1%, 5,283G)
krombobreaker: 280G (4.1%, 7,619G)

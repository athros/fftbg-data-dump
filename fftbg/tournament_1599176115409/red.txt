Player: !Red
Team: Red Team
Palettes: Red/Brown



Thunderducker
Female
Taurus
54
79
Lancer
Time Magic
Sunken State
Equip Armor
Waterbreathing

Javelin
Aegis Shield
Twist Headband
Wizard Outfit
108 Gems

Level Jump8, Vertical Jump4
Haste, Haste 2, Slow, Stop



Zerguzen
Female
Scorpio
48
64
Priest
Time Magic
Damage Split
Doublehand
Ignore Height

Rainbow Staff

Leather Hat
Adaman Vest
Feather Boots

Cure, Cure 2, Raise, Protect 2, Shell 2, Wall
Haste, Slow, Stop, Demi, Stabilize Time



Shalloween
Male
Capricorn
66
66
Monk
Throw
Absorb Used MP
Dual Wield
Jump+2



Feather Hat
Chain Vest
Cursed Ring

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Shuriken, Bomb



ANFz
Male
Gemini
49
62
Knight
Charge
Counter Tackle
Attack UP
Move+2

Giant Axe
Bronze Shield
Barbuta
Chameleon Robe
Magic Ring

Armor Break, Weapon Break, Magic Break, Power Break, Justice Sword
Charge+2, Charge+4

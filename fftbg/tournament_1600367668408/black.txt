Player: !Black
Team: Black Team
Palettes: Black/Red



Just Here2
Male
Scorpio
73
44
Calculator
White Magic
Distribute
Equip Shield
Levitate

Wizard Rod
Diamond Shield
Holy Miter
White Robe
Rubber Shoes

CT, Height, Prime Number, 5, 4, 3
Cure 2, Cure 3, Raise, Regen, Protect 2, Holy



Latebit
Female
Libra
39
47
Summoner
Basic Skill
Earplug
Halve MP
Move+2

Gold Staff

Holy Miter
Black Robe
Diamond Armlet

Ramuh, Ifrit, Carbunkle, Leviathan
Accumulate, Dash, Heal, Yell, Fury, Scream



SephDarkheart
Female
Sagittarius
43
64
Chemist
White Magic
Earplug
Equip Gun
Jump+1

Mythril Gun

Flash Hat
Brigandine
Cursed Ring

Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Cure, Raise, Raise 2, Reraise, Shell 2, Wall, Esuna



Roofiepops
Female
Virgo
42
78
Summoner
Time Magic
Absorb Used MP
Short Charge
Jump+1

Wizard Staff

Black Hood
White Robe
Magic Gauntlet

Ramuh, Leviathan
Haste 2, Slow 2, Demi

Player: !Red
Team: Red Team
Palettes: Red/Brown



Smashy
Male
Capricorn
67
49
Squire
Charge
Dragon Spirit
Secret Hunt
Retreat

Platinum Sword
Escutcheon
Green Beret
Diamond Armor
Reflect Ring

Heal, Yell
Charge+1, Charge+7, Charge+10



Old Overholt
Male
Sagittarius
61
63
Lancer
Talk Skill
Distribute
Doublehand
Waterwalking

Partisan

Bronze Helmet
Leather Armor
Salty Rage

Level Jump5, Vertical Jump5
Threaten, Negotiate, Mimic Daravon, Refute, Rehabilitate



IdioSyncr4zy
Male
Aquarius
66
45
Wizard
Elemental
Hamedo
Magic Attack UP
Move+3

Dragon Rod

Red Hood
Rubber Costume
Leather Mantle

Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Empower, Frog, Death, Flare
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Brokenknight201
Female
Capricorn
68
54
Samurai
Time Magic
Distribute
Magic Attack UP
Waterbreathing

Kiyomori

Gold Helmet
Genji Armor
Magic Gauntlet

Heaven's Cloud, Kiyomori
Haste, Haste 2, Stop, Immobilize, Reflect, Galaxy Stop

Player: !Green
Team: Green Team
Palettes: Green/White



Krombobreaker
Female
Cancer
48
68
Archer
Draw Out
Critical Quick
Doublehand
Jump+2

Hunting Bow

Diamond Helmet
Secret Clothes
Elf Mantle

Charge+3, Charge+5, Charge+7, Charge+10
Koutetsu



J2DaBibbles
Female
Scorpio
63
80
Oracle
White Magic
Caution
Short Charge
Jump+1

Musk Rod

Twist Headband
Light Robe
Small Mantle

Blind, Life Drain, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Paralyze, Sleep
Cure 4, Raise 2, Reraise, Protect, Protect 2, Shell 2, Esuna



Muffin Money
Male
Aries
53
49
Oracle
Talk Skill
Faith Save
Monster Talk
Move+1

Octagon Rod

Black Hood
Brigandine
Magic Ring

Poison, Life Drain, Foxbird, Confusion Song, Paralyze, Sleep
Persuade, Insult, Rehabilitate



Poorest Hobo
Male
Cancer
43
54
Calculator
Bio
Earplug
Doublehand
Ignore Height

Bestiary

Crystal Helmet
Carabini Mail
Magic Ring

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Joewcarson
Male
Virgo
73
58
Knight
Yin Yang Magic
Absorb Used MP
Defense UP
Swim

Defender
Platinum Shield
Circlet
Silk Robe
Jade Armlet

Head Break, Weapon Break, Mind Break, Surging Sword
Blind, Poison, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic



PoroTact
Male
Aquarius
37
67
Knight
Item
Parry
Equip Bow
Jump+2

Night Killer
Buckler
Iron Helmet
Chameleon Robe
Genji Gauntlet

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Potion, Phoenix Down



Maakur
Female
Scorpio
48
61
Samurai
Item
HP Restore
Maintenance
Lava Walking

Kiyomori

Gold Helmet
Diamond Armor
Rubber Shoes

Asura, Kiyomori
X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Phi Sig
Female
Capricorn
50
60
Wizard
Battle Skill
Regenerator
Short Charge
Jump+3

Rod

Triangle Hat
Wizard Robe
Feather Boots

Fire, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 4, Empower, Frog
Speed Break, Power Break, Mind Break

Player: !Red
Team: Red Team
Palettes: Red/Brown



HASTERIOUS
Male
Cancer
77
46
Lancer
Charge
MA Save
Equip Axe
Move-HP Up

Battle Axe
Ice Shield
Diamond Helmet
Gold Armor
Red Shoes

Level Jump8, Vertical Jump6
Charge+2, Charge+3, Charge+5, Charge+7, Charge+10, Charge+20



Lwtest
Female
Pisces
54
65
Archer
Punch Art
Meatbone Slash
Equip Polearm
Move+2

Partisan
Buckler
Cross Helmet
Earth Clothes
Dracula Mantle

Charge+1, Charge+3
Pummel, Wave Fist, Purification, Revive



Roofiepops
Male
Scorpio
71
47
Ninja
White Magic
Parry
Equip Sword
Jump+3

Murasame
Flame Whip
Green Beret
Chain Vest
Feather Boots

Shuriken, Bomb, Wand
Cure 2, Raise, Protect, Esuna



0v3rr8d
Male
Serpentarius
57
50
Lancer
Talk Skill
Counter Tackle
Equip Axe
Move+2

Slasher
Escutcheon
Gold Helmet
Plate Mail
Feather Boots

Level Jump2, Vertical Jump8
Persuade, Praise, Threaten

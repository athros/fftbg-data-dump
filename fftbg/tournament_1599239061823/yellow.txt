Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HaateXIII
Female
Sagittarius
73
50
Summoner
Elemental
Catch
Maintenance
Waterbreathing

Healing Staff

Green Beret
Linen Robe
Vanish Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Cyclops
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Joewcarson
Male
Pisces
67
79
Lancer
Draw Out
HP Restore
Equip Knife
Move+3

Thunder Rod
Genji Shield
Iron Helmet
Plate Mail
Angel Ring

Level Jump2, Vertical Jump8
Koutetsu, Muramasa, Kikuichimoji



Mushufasa
Male
Leo
78
72
Archer
Jump
Mana Shield
Equip Bow
Ignore Height

Hunting Bow
Ice Shield
Gold Helmet
Adaman Vest
Salty Rage

Charge+1, Charge+3, Charge+5, Charge+10
Level Jump5, Vertical Jump8



Forkmore
Male
Virgo
63
49
Samurai
White Magic
Dragon Spirit
Magic Attack UP
Jump+1

Muramasa

Diamond Helmet
Bronze Armor
Defense Ring

Koutetsu, Heaven's Cloud
Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Shell 2, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



Pplvee1
Female
Taurus
53
69
Knight
Elemental
Absorb Used MP
Doublehand
Move-HP Up

Defender

Leather Helmet
Light Robe
Bracer

Armor Break, Shield Break, Speed Break, Mind Break, Stasis Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind



Ko2q
Female
Cancer
58
46
Thief
Time Magic
Brave Save
Beastmaster
Ignore Terrain

Main Gauche

Thief Hat
Power Sleeve
Jade Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Steal Status
Haste, Slow 2, Demi, Stabilize Time



Shalloween
Male
Scorpio
79
61
Archer
Battle Skill
Dragon Spirit
Doublehand
Move+1

Snipe Bow

Cross Helmet
Power Sleeve
Germinas Boots

Charge+1, Charge+2, Charge+4, Charge+5
Armor Break, Shield Break, Surging Sword



Nhammen
Male
Leo
62
58
Ninja
Punch Art
Auto Potion
Equip Shield
Move+1

Kunai
Round Shield
Flash Hat
Black Costume
Cherche

Shuriken, Bomb, Wand
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil

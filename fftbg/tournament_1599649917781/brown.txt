Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeniorBunk
Monster
Libra
62
67
Ghost










Ogema TheDream
Male
Gemini
50
59
Squire
Summon Magic
Mana Shield
Attack UP
Ignore Terrain

Morning Star
Bronze Shield
Headgear
Power Sleeve
Cherche

Heal, Tickle, Cheer Up, Fury, Wish
Moogle, Odin, Salamander, Fairy



Khelor
Male
Aries
66
67
Time Mage
Punch Art
PA Save
Short Charge
Fly

Rainbow Staff

Green Beret
Wizard Robe
Dracula Mantle

Haste, Haste 2, Slow 2, Stop, Float, Meteor
Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive



Aceof86
Male
Libra
52
45
Monk
White Magic
Parry
Dual Wield
Fly

Cute Bag
Star Bag
Green Beret
Adaman Vest
Battle Boots

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Cure 4, Raise, Protect 2, Shell 2, Wall, Esuna

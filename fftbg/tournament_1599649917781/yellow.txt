Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Bruubarg
Male
Capricorn
63
53
Chemist
Jump
Counter Flood
Attack UP
Jump+3

Panther Bag

Green Beret
Judo Outfit
Dracula Mantle

X-Potion, Ether, Antidote, Phoenix Down
Level Jump4, Vertical Jump6



Willjin
Male
Leo
50
44
Squire
Battle Skill
Earplug
Short Status
Ignore Height

Snipe Bow
Hero Shield
Holy Miter
Mystic Vest
Cherche

Accumulate
Head Break, Stasis Sword



Leonidusx
Female
Aries
53
56
Time Mage
Black Magic
Damage Split
Secret Hunt
Ignore Terrain

Oak Staff

Thief Hat
Judo Outfit
Cursed Ring

Haste, Slow 2, Immobilize, Float, Quick, Demi 2, Stabilize Time
Fire 3, Bolt, Bolt 3



Just Here2
Male
Scorpio
67
75
Monk
Summon Magic
Dragon Spirit
Attack UP
Jump+2



Red Hood
Adaman Vest
Cursed Ring

Spin Fist, Pummel, Earth Slash, Purification
Titan, Carbunkle, Odin, Fairy

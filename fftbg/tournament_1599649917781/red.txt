Player: !Red
Team: Red Team
Palettes: Red/Brown



CassiePhoenix
Female
Aries
72
51
Calculator
Black Magic
Sunken State
Magic Attack UP
Waterbreathing

Bestiary

Thief Hat
Chameleon Robe
Cherche

CT, Height, Prime Number, 4, 3
Bolt, Bolt 2, Bolt 4, Ice 4, Death



ColetteMSLP
Monster
Cancer
74
65
Minotaur










SephDarkheart
Female
Libra
61
74
Thief
Time Magic
Dragon Spirit
Long Status
Jump+3

Hidden Knife

Red Hood
Power Sleeve
Leather Mantle

Gil Taking, Steal Heart, Steal Shield, Steal Status, Leg Aim
Stabilize Time



Reddwind
Male
Cancer
52
74
Knight
Elemental
Sunken State
Defense UP
Fly

Defender
Aegis Shield
Diamond Helmet
Plate Mail
Magic Gauntlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Stasis Sword
Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Final Bets: green - 8 bets for 6,959G (70.9%, x0.41); yellow - 8 bets for 2,854G (29.1%, x2.44)

green bets:
CosmicTactician: 3,000G (43.1%, 29,526G)
BirbBrainsBot: 1,000G (14.4%, 163,212G)
getthemoneyz: 922G (13.2%, 1,893,914G)
AllInBot: 899G (12.9%, 899G)
Leonidusx: 619G (8.9%, 619G)
AltimaMantoid: 396G (5.7%, 5,796G)
Ogema_theDream: 108G (1.6%, 108G)
BartTradingCompany: 15G (0.2%, 158G)

yellow bets:
dem0nj0ns: 915G (32.1%, 915G)
snkey: 636G (22.3%, 636G)
FoeSquirrel: 389G (13.6%, 778G)
khelor_: 200G (7.0%, 3,543G)
datadrivenbot: 200G (7.0%, 57,962G)
aceof86: 200G (7.0%, 2,140G)
MemoriesofFinal: 161G (5.6%, 2,525G)
hotpocketsofficial: 153G (5.4%, 153G)

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



ExplanationMarkWow
Monster
Taurus
79
48
Wyvern










Jeeboheebo
Female
Cancer
49
60
Summoner
Punch Art
Mana Shield
Beastmaster
Retreat

Oak Staff

Twist Headband
Black Robe
Magic Ring

Moogle, Shiva, Titan, Golem, Odin, Fairy
Pummel, Earth Slash, Purification, Chakra, Revive, Seal Evil



ChiChiRodriguez
Male
Virgo
70
62
Monk
Jump
Auto Potion
Maintenance
Teleport



Red Hood
Judo Outfit
Defense Ring

Spin Fist, Earth Slash, Purification, Chakra, Revive
Level Jump4, Vertical Jump4



Reddash9
Female
Scorpio
73
64
Summoner
Item
Counter Magic
Equip Bow
Jump+3

Bow Gun

Black Hood
Light Robe
N-Kai Armlet

Shiva, Carbunkle, Leviathan, Salamander
Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down

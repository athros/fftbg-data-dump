Player: !Red
Team: Red Team
Palettes: Red/Brown



Loveyouallfriends
Male
Aquarius
75
57
Bard
Throw
Parry
Sicken
Move-MP Up

Ultimus Bow

Black Hood
Adaman Vest
Chantage

Angel Song, Magic Song, Last Song, Sky Demon, Hydra Pit
Knife



SkylerBunny
Female
Pisces
47
61
Knight
Basic Skill
Counter Tackle
Doublehand
Fly

Defender

Bronze Helmet
Bronze Armor
Rubber Shoes

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Mind Break
Dash, Throw Stone, Heal, Tickle, Cheer Up, Fury



Thunderducker
Female
Libra
55
70
Oracle
Summon Magic
Abandon
Defense UP
Ignore Height

Bestiary

Cachusha
Power Sleeve
Jade Armlet

Poison, Blind Rage, Confusion Song, Dispel Magic, Sleep, Petrify, Dark Holy
Moogle, Titan, Carbunkle, Leviathan, Fairy, Lich, Cyclops



Omegasuspekt
Female
Virgo
60
74
Dancer
Throw
Counter Magic
Sicken
Retreat

Cute Bag

Golden Hairpin
White Robe
Germinas Boots

Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Dragon Pit
Bomb, Spear, Stick

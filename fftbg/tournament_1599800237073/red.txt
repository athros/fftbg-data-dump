Player: !Red
Team: Red Team
Palettes: Red/Brown



Seaweed B
Monster
Taurus
52
53
Dark Behemoth










DAC169
Female
Capricorn
75
67
Geomancer
Battle Skill
Counter Magic
Defense UP
Fly

Giant Axe
Buckler
Flash Hat
Power Sleeve
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind
Armor Break, Weapon Break



Ruleof5
Female
Gemini
75
65
Archer
Steal
Meatbone Slash
Equip Armor
Ignore Height

Snipe Bow
Platinum Shield
Genji Helmet
White Robe
Chantage

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Steal Accessory, Steal Status



Shalloween
Male
Aquarius
54
43
Monk
Black Magic
Damage Split
Dual Wield
Fly



Thief Hat
Power Sleeve
Leather Mantle

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification
Fire, Fire 4, Ice 3

Player: !Green
Team: Green Team
Palettes: Green/White



DeathTaxesAndAnime
Female
Taurus
44
48
Thief
Talk Skill
Mana Shield
Secret Hunt
Fly

Iron Sword

Triangle Hat
Adaman Vest
Bracer

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Threaten, Negotiate, Rehabilitate



Kilrazin
Female
Gemini
70
49
Wizard
Talk Skill
MP Restore
Long Status
Jump+1

Thunder Rod

Triangle Hat
Rubber Costume
Wizard Mantle

Fire 4, Bolt 2, Ice, Ice 2, Death
Threaten, Preach, Solution, Death Sentence, Insult, Mimic Daravon



Genkidou
Male
Gemini
67
60
Ninja
Jump
Auto Potion
Equip Bow
Lava Walking

Poison Bow
Hunting Bow
Holy Miter
Brigandine
108 Gems

Dictionary
Level Jump8, Vertical Jump2



Latebit
Female
Taurus
71
55
Time Mage
Basic Skill
Parry
Maintenance
Move+2

Wizard Staff

Black Hood
Wizard Robe
Battle Boots

Haste, Haste 2, Float, Quick, Demi 2
Throw Stone

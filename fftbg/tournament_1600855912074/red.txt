Player: !Red
Team: Red Team
Palettes: Red/Brown



Rune339
Male
Aquarius
48
51
Knight
Item
Damage Split
Equip Knife
Teleport

Kunai
Aegis Shield
Cross Helmet
Plate Mail
Jade Armlet

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword
Potion, Hi-Ether, Soft, Phoenix Down



MemoriesofFinal
Male
Pisces
63
73
Monk
Talk Skill
Critical Quick
Magic Defense UP
Retreat



Flash Hat
Adaman Vest
Spike Shoes

Earth Slash, Purification, Revive, Seal Evil
Preach, Negotiate, Refute



Ser Pyrro
Female
Aries
70
72
Time Mage
Draw Out
Counter
Dual Wield
Levitate

Octagon Rod
Musk Rod
Twist Headband
Silk Robe
Leather Mantle

Haste, Slow 2, Stop, Stabilize Time, Meteor
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



ALY327
Female
Taurus
58
60
Summoner
Draw Out
Dragon Spirit
Short Charge
Ignore Terrain

Ice Rod

Twist Headband
Wizard Outfit
Magic Gauntlet

Moogle, Odin, Fairy
Bizen Boat, Heaven's Cloud

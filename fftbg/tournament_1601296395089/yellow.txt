Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RunicMagus
Female
Pisces
69
62
Oracle
Black Magic
Caution
Doublehand
Fly

Bestiary

Black Hood
Light Robe
Defense Ring

Poison, Spell Absorb, Life Drain, Sleep
Bolt 3, Ice 2, Empower, Flare



HASTERIOUS
Male
Gemini
72
70
Knight
Punch Art
Parry
Dual Wield
Teleport

Defender
Platinum Sword
Leather Helmet
Carabini Mail
Chantage

Shield Break, Power Break, Stasis Sword, Justice Sword
Spin Fist, Pummel, Wave Fist, Purification, Revive



ALY327
Female
Scorpio
65
52
Dancer
Punch Art
Brave Save
Sicken
Fly

Cashmere

Barette
Adaman Vest
Sprint Shoes

Witch Hunt, Slow Dance, Disillusion, Nameless Dance, Last Dance, Dragon Pit
Pummel, Purification, Revive, Seal Evil



Zachara
Male
Cancer
56
49
Chemist
Elemental
Regenerator
Secret Hunt
Lava Walking

Hydra Bag

Green Beret
Adaman Vest
Leather Mantle

Antidote, Holy Water, Phoenix Down
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

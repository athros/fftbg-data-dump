Player: !Green
Team: Green Team
Palettes: Green/White



Arumz
Male
Sagittarius
68
67
Chemist
Jump
Caution
Sicken
Move-MP Up

Star Bag

Leather Hat
Black Costume
Small Mantle

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Level Jump8, Vertical Jump6



Lemonjohns
Female
Scorpio
51
68
Calculator
White Magic
Counter
Defend
Jump+2

Battle Folio

Holy Miter
Wizard Robe
Diamond Armlet

CT, 5
Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Esuna



Actual JP
Male
Aries
69
81
Ninja
Basic Skill
Parry
Equip Axe
Jump+1

Flame Whip
Giant Axe
Golden Hairpin
Secret Clothes
Power Wrist

Shuriken, Bomb, Hammer, Ninja Sword
Accumulate, Throw Stone, Tickle, Yell, Fury



Lord Gwarth
Female
Cancer
41
65
Calculator
Black Magic
Caution
Equip Gun
Move+1

Glacier Gun

Flash Hat
Judo Outfit
Red Shoes

CT, Height, Prime Number, 5, 3
Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4

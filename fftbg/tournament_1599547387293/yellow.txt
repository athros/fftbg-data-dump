Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mindblownnnn
Monster
Sagittarius
77
64
Red Chocobo










Yangusburger
Female
Serpentarius
49
60
Archer
Black Magic
Catch
Magic Defense UP
Move+3

Poison Bow
Aegis Shield
Holy Miter
Mythril Vest
108 Gems

Charge+2, Charge+5, Charge+7, Charge+10
Fire, Fire 2, Fire 3, Ice, Ice 2, Ice 3, Empower, Frog, Death



Lowlf
Female
Taurus
48
78
Summoner
Basic Skill
Dragon Spirit
Equip Polearm
Ignore Height

Persia

Barette
Black Robe
Magic Ring

Shiva, Ramuh, Golem, Carbunkle, Leviathan, Salamander, Fairy, Lich
Accumulate, Heal



Raixelol
Male
Taurus
80
73
Monk
Time Magic
Counter Magic
Equip Armor
Ignore Height



Leather Hat
Wizard Robe
Spike Shoes

Pummel, Revive
Haste 2, Slow 2, Stop, Float, Reflect

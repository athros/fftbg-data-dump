Final Bets: purple - 9 bets for 5,147G (66.3%, x0.51); brown - 9 bets for 2,613G (33.7%, x1.97)

purple bets:
DeathTaxesAndAnime: 1,016G (19.7%, 1,016G)
NicoSavoy: 1,000G (19.4%, 11,587G)
lijarkh: 844G (16.4%, 844G)
getthemoneyz: 550G (10.7%, 1,875,182G)
Lythe_Caraker: 500G (9.7%, 166,409G)
krombobreaker: 500G (9.7%, 7,718G)
seppu777: 333G (6.5%, 9,406G)
AllInBot: 304G (5.9%, 304G)
farside00831: 100G (1.9%, 212G)

brown bets:
WildVol: 715G (27.4%, 715G)
Raixelol: 552G (21.1%, 30,711G)
BirbBrainsBot: 290G (11.1%, 115,485G)
RobotOcelot: 256G (9.8%, 256G)
CorpusCav: 250G (9.6%, 23,524G)
Yangusburger: 200G (7.7%, 8,040G)
datadrivenbot: 200G (7.7%, 56,887G)
Axciom: 100G (3.8%, 523G)
BartTradingCompany: 50G (1.9%, 504G)

Final Bets: red - 9 bets for 5,704G (62.3%, x0.61); purple - 8 bets for 3,455G (37.7%, x1.65)

red bets:
KasugaiRoastedPeas: 1,132G (19.8%, 1,132G)
DeathTaxesAndAnime: 1,016G (17.8%, 1,016G)
BirbBrainsBot: 1,000G (17.5%, 117,241G)
upvla: 891G (15.6%, 45,813G)
RobotOcelot: 517G (9.1%, 517G)
krombobreaker: 500G (8.8%, 8,994G)
Wonser: 348G (6.1%, 8,197G)
datadrivenbot: 200G (3.5%, 57,096G)
Yangusburger: 100G (1.8%, 8,045G)

purple bets:
YaBoy125: 1,330G (38.5%, 5,320G)
Raixelol: 552G (16.0%, 29,607G)
farside00831: 500G (14.5%, 533G)
getthemoneyz: 474G (13.7%, 1,876,230G)
CorpusCav: 250G (7.2%, 23,285G)
iieunueii: 204G (5.9%, 204G)
AllInBot: 100G (2.9%, 100G)
BartTradingCompany: 45G (1.3%, 450G)

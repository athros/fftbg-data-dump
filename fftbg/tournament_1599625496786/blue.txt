Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grininda
Male
Aquarius
64
74
Archer
Summon Magic
Mana Shield
Doublehand
Move-HP Up

Hunting Bow

Twist Headband
Power Sleeve
108 Gems

Charge+3, Charge+4
Moogle, Ramuh, Ifrit, Bahamut, Lich



Zeroroute
Female
Aries
67
61
Samurai
Item
MP Restore
Throw Item
Teleport

Masamune

Barbuta
Mythril Armor
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Potion, X-Potion, Echo Grass, Holy Water, Phoenix Down



Latebit
Monster
Capricorn
57
57
Black Goblin










Lowlf
Female
Taurus
41
66
Ninja
Jump
Caution
Short Status
Ignore Height

Cultist Dagger
Short Edge
Headgear
Judo Outfit
Spike Shoes

Shuriken, Bomb, Wand
Level Jump2, Vertical Jump5

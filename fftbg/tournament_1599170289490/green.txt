Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Male
Pisces
67
75
Bard
Charge
MP Restore
Maintenance
Move+1

Fairy Harp

Leather Hat
Wizard Outfit
Dracula Mantle

Angel Song, Battle Song, Magic Song, Nameless Song
Charge+2



Arumz
Male
Leo
72
78
Priest
Throw
Parry
Equip Axe
Jump+2

Slasher

Green Beret
Black Costume
Magic Ring

Cure, Cure 3, Cure 4, Raise, Shell, Shell 2, Esuna, Holy
Shuriken, Bomb



DeathRtopper
Female
Cancer
69
73
Thief
Charge
Parry
Secret Hunt
Move+1

Blood Sword

Black Hood
Secret Clothes
Reflect Ring

Steal Heart, Steal Shield, Steal Status
Charge+3, Charge+7, Charge+10



Just Here2
Male
Gemini
69
45
Samurai
Charge
Arrow Guard
Halve MP
Move-HP Up

Kiyomori

Diamond Helmet
Crystal Mail
Dracula Mantle

Koutetsu, Bizen Boat, Kiyomori
Charge+2, Charge+4, Charge+7, Charge+10

Player: !Red
Team: Red Team
Palettes: Red/Brown



Robespyah
Male
Scorpio
60
75
Samurai
Elemental
Faith Save
Equip Knife
Fly

Hidden Knife

Diamond Helmet
Carabini Mail
Defense Ring

Asura, Heaven's Cloud, Kiyomori, Muramasa
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Lava Ball



Zachara
Female
Capricorn
63
45
Thief
Battle Skill
Meatbone Slash
Doublehand
Teleport

Short Edge

Golden Hairpin
Black Costume
N-Kai Armlet

Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Steal Status
Shield Break, Magic Break, Speed Break, Night Sword



GenZealot
Female
Leo
60
42
Geomancer
Steal
MP Restore
Equip Sword
Jump+3

Sleep Sword
Platinum Shield
Twist Headband
Silk Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Lava Ball
Steal Heart, Steal Weapon, Steal Accessory



YowaneHaku
Female
Aries
49
41
Geomancer
Jump
PA Save
Magic Defense UP
Jump+1

Slasher
Ice Shield
Leather Hat
Wizard Robe
Small Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Level Jump3, Vertical Jump5

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



PopsLIVE
Female
Scorpio
61
76
Dancer
Basic Skill
Arrow Guard
Attack UP
Teleport

Persia

Black Hood
Linen Robe
108 Gems

Witch Hunt, Wiznaibus, Slow Dance, Nether Demon, Dragon Pit
Dash, Throw Stone, Yell, Cheer Up, Fury, Scream



GenZealot
Male
Capricorn
53
51
Archer
Yin Yang Magic
Caution
Sicken
Move-MP Up

Glacier Gun
Flame Shield
Green Beret
Brigandine
Defense Armlet

Charge+1, Charge+2, Charge+3, Charge+7
Poison, Spell Absorb, Life Drain, Pray Faith, Confusion Song, Dispel Magic, Paralyze



Lijarkh
Male
Virgo
58
44
Monk
Steal
Brave Save
Equip Armor
Lava Walking



Platinum Helmet
Silk Robe
Wizard Mantle

Earth Slash, Secret Fist, Purification, Revive
Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Smashy
Female
Sagittarius
80
54
Chemist
Dance
Mana Shield
Defend
Ignore Terrain

Cute Bag

Black Hood
Chain Vest
Feather Mantle

Potion, Hi-Potion, Hi-Ether, Soft, Phoenix Down
Witch Hunt, Void Storage

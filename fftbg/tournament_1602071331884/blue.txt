Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mirapoix
Female
Sagittarius
67
69
Thief
Jump
Earplug
Short Charge
Move+1

Dagger

Twist Headband
Mythril Vest
Spike Shoes

Steal Helmet, Steal Shield
Level Jump8, Vertical Jump8



HorusTaurus
Male
Aries
70
57
Samurai
Battle Skill
Critical Quick
Equip Axe
Jump+2

Morning Star

Genji Helmet
Wizard Robe
Battle Boots

Asura, Koutetsu, Masamune
Magic Break, Speed Break, Dark Sword



Ar Tactic
Male
Sagittarius
47
47
Thief
Black Magic
Dragon Spirit
Equip Sword
Jump+1

Sleep Sword

Red Hood
Wizard Outfit
Defense Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Leg Aim
Fire, Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Flare



Actual JP
Male
Cancer
46
50
Monk
Battle Skill
Counter Magic
Concentrate
Levitate



Twist Headband
Mystic Vest
Spike Shoes

Pummel, Earth Slash, Secret Fist, Purification, Chakra
Head Break, Armor Break, Speed Break, Power Break, Justice Sword, Night Sword

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Capricorn
55
75
Thief
White Magic
Auto Potion
Dual Wield
Move-HP Up

Diamond Sword
Sasuke Knife
Twist Headband
Mythril Vest
Reflect Ring

Steal Shield, Steal Accessory, Steal Status
Raise, Reraise, Regen, Protect, Wall, Esuna



ColetteMSLP
Female
Capricorn
67
57
Thief
Yin Yang Magic
Dragon Spirit
Secret Hunt
Jump+3

Orichalcum

Black Hood
Mystic Vest
Reflect Ring

Gil Taking, Steal Armor, Arm Aim, Leg Aim
Zombie, Blind Rage, Dispel Magic, Sleep



Rytor
Monster
Scorpio
69
40
Behemoth










TheSabretoothe
Female
Gemini
49
44
Dancer
Punch Art
Abandon
Equip Gun
Waterbreathing

Battle Folio

Headgear
Power Sleeve
Leather Mantle

Wiznaibus, Disillusion, Last Dance, Void Storage
Secret Fist, Chakra, Revive

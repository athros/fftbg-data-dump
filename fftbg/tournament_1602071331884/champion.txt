Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Chaosdesigned
Male
Capricorn
77
54
Thief
Time Magic
Auto Potion
Equip Shield
Swim

Main Gauche
Bronze Shield
Feather Hat
Earth Clothes
Magic Ring

Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Arm Aim
Demi 2, Stabilize Time



Just Here2
Female
Capricorn
76
61
Priest
Yin Yang Magic
Parry
Maintenance
Move+1

Mace of Zeus

Holy Miter
Light Robe
108 Gems

Cure, Raise, Raise 2, Reraise, Regen, Protect 2, Esuna
Spell Absorb, Life Drain, Doubt Faith, Foxbird, Confusion Song



NovaKnight21
Male
Taurus
56
61
Monk
Draw Out
Speed Save
Dual Wield
Lava Walking



Twist Headband
Mythril Vest
Setiemson

Wave Fist, Earth Slash, Secret Fist, Chakra
Asura, Bizen Boat, Heaven's Cloud, Muramasa



Mushufasa
Male
Libra
62
75
Geomancer
Talk Skill
MP Restore
Magic Attack UP
Move+3

Sleep Sword
Genji Shield
Holy Miter
Clothes
Small Mantle

Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Praise, Threaten, Preach, Refute

Player: !Red
Team: Red Team
Palettes: Red/Brown



Evewho
Female
Scorpio
47
49
Squire
Elemental
HP Restore
Defend
Waterbreathing

Hydra Bag
Escutcheon
Barbuta
Earth Clothes
Jade Armlet

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up
Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



OneHundredFists
Female
Aries
56
55
Dancer
Steal
Dragon Spirit
Short Charge
Lava Walking

Persia

Red Hood
Wizard Robe
Defense Ring

Slow Dance, Disillusion, Nameless Dance, Last Dance
Steal Heart, Leg Aim



MemoriesofFinal
Male
Cancer
71
65
Mime

Damage Split
Dual Wield
Move+2



Twist Headband
Mythril Vest
Reflect Ring

Mimic




Gorgewall
Female
Taurus
72
51
Dancer
Time Magic
Faith Save
Equip Bow
Move+3

Lightning Bow

Green Beret
Silk Robe
Jade Armlet

Witch Hunt, Slow Dance, Polka Polka, Disillusion
Slow, Slow 2, Stop, Float, Demi 2, Meteor

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ruleof5
Female
Capricorn
62
64
Archer
Time Magic
Damage Split
Doublehand
Lava Walking

Poison Bow

Feather Hat
Chain Vest
Magic Gauntlet

Charge+5, Charge+20
Haste, Slow 2, Stop, Reflect, Quick, Stabilize Time



E Ballard
Male
Pisces
70
58
Time Mage
Punch Art
PA Save
Equip Axe
Fly

Slasher

Flash Hat
Clothes
Genji Gauntlet

Haste, Slow, Stop, Immobilize, Reflect, Quick, Stabilize Time, Meteor
Pummel, Secret Fist, Purification, Revive



Reddwind
Male
Aquarius
50
70
Knight
White Magic
Auto Potion
Concentrate
Jump+1

Battle Axe
Escutcheon
Cross Helmet
Chain Mail
Genji Gauntlet

Mind Break
Raise, Raise 2, Protect, Wall, Holy



Nelli
Female
Capricorn
67
58
Squire
Summon Magic
Earplug
Magic Defense UP
Move-MP Up

Snipe Bow
Aegis Shield
Twist Headband
Adaman Vest
Sprint Shoes

Accumulate, Heal, Yell, Cheer Up
Shiva, Ifrit, Titan, Golem, Lich, Cyclops

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Tapemeasure
Male
Capricorn
60
43
Chemist
Talk Skill
Brave Save
Doublehand
Move-HP Up

Orichalcum

Headgear
Wizard Outfit
N-Kai Armlet

Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Phoenix Down
Invitation, Praise, Preach, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate



Fattunaking
Male
Pisces
52
49
Monk
Item
Auto Potion
Short Status
Move+3



Holy Miter
Earth Clothes
Power Wrist

Secret Fist, Purification, Revive
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water



Max
Male
Sagittarius
63
45
Mime

Abandon
Martial Arts
Retreat



Flash Hat
Adaman Vest
Cursed Ring

Mimic




Charmian
Female
Aries
64
39
Ninja
White Magic
Faith Save
Equip Sword
Swim

Bizen Boat
Asura Knife
Red Hood
Mythril Vest
Red Shoes

Shuriken, Knife, Hammer, Stick
Cure, Raise, Regen, Protect, Wall, Esuna

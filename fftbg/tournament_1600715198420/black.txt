Player: !Black
Team: Black Team
Palettes: Black/Red



Lyner87
Male
Cancer
46
69
Chemist
Steal
Mana Shield
Magic Defense UP
Retreat

Mythril Gun

Leather Hat
Judo Outfit
Feather Mantle

Potion, Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Phoenix Down
Steal Helmet, Steal Shield



Joewcarson
Male
Scorpio
66
82
Thief
White Magic
Earplug
Equip Gun
Swim

Stone Gun

Green Beret
Power Sleeve
Battle Boots

Steal Helmet, Steal Shield, Arm Aim
Cure 2, Raise, Reraise, Wall, Esuna



Resjudicata3
Female
Gemini
50
72
Geomancer
Time Magic
Brave Save
Defend
Fly

Slasher
Buckler
Black Hood
Rubber Costume
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Slow, Slow 2, Demi 2



SQUiDSQUARKLIN
Male
Cancer
43
72
Mime

PA Save
Attack UP
Jump+1



Green Beret
Earth Clothes
Feather Mantle

Mimic


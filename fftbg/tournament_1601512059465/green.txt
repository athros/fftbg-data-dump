Player: !Green
Team: Green Team
Palettes: Green/White



Virilikus
Male
Capricorn
77
55
Lancer
Battle Skill
Mana Shield
Equip Knife
Waterwalking

Poison Rod
Genji Shield
Genji Helmet
Mythril Armor
Feather Boots

Level Jump2, Vertical Jump8
Head Break, Mind Break



Silentkaster
Male
Pisces
43
43
Archer
Item
Parry
Throw Item
Waterwalking

Long Bow

Genji Helmet
Wizard Outfit
Leather Mantle

Charge+4, Charge+7
Potion, Hi-Ether, Echo Grass



ColetteMSLP
Female
Aries
44
67
Ninja
Time Magic
Damage Split
Concentrate
Waterbreathing

Hidden Knife
Short Edge
Leather Hat
Earth Clothes
Defense Armlet

Shuriken, Bomb, Ninja Sword
Haste, Haste 2, Slow, Stabilize Time



Gelwain
Male
Aquarius
68
48
Archer
Sing
Counter Magic
Attack UP
Jump+3

Bow Gun
Crystal Shield
Red Hood
Secret Clothes
Diamond Armlet

Charge+3, Charge+5
Cheer Song, Last Song

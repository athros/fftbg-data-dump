Final Bets: green - 17 bets for 18,296G (36.7%, x1.72); yellow - 21 bets for 31,520G (63.3%, x0.58)

green bets:
Shalloween: 6,267G (34.3%, 12,534G)
Mesmaster: 5,000G (27.3%, 19,369G)
Nizaha: 1,077G (5.9%, 8,286G)
BirbBrainsBot: 1,000G (5.5%, 121,246G)
getthemoneyz: 1,000G (5.5%, 2,121,790G)
Lord_Gwarth: 1,000G (5.5%, 1,894G)
fenaen: 652G (3.6%, 12,600G)
maakur_: 620G (3.4%, 620G)
MemoriesofFinal: 441G (2.4%, 441G)
Reddash9: 257G (1.4%, 257G)
TheBrett: 236G (1.3%, 5,454G)
Evewho: 200G (1.1%, 3,081G)
Girfred: 126G (0.7%, 126G)
LuckyLuckLuc2: 111G (0.6%, 7,030G)
KGenjy: 108G (0.6%, 108G)
gorgewall: 101G (0.6%, 13,656G)
Firesheath: 100G (0.5%, 2,855G)

yellow bets:
Virilikus: 5,000G (15.9%, 167,881G)
silentkaster: 4,750G (15.1%, 24,350G)
latebit: 4,661G (14.8%, 66,592G)
AniZero: 3,333G (10.6%, 39,360G)
seppu777: 1,867G (5.9%, 1,867G)
dogsandcatsand: 1,678G (5.3%, 1,678G)
ApplesNP: 1,000G (3.2%, 69,804G)
Xenomorith: 1,000G (3.2%, 18,489G)
tripaplex: 996G (3.2%, 996G)
ColetteMSLP: 947G (3.0%, 947G)
AllInBot: 911G (2.9%, 911G)
VolgraTheMoose: 908G (2.9%, 27,326G)
killth3kid: 792G (2.5%, 35,759G)
lijarkh: 786G (2.5%, 786G)
resjudicata3: 635G (2.0%, 635G)
ExplanationMarkWow: 500G (1.6%, 17,576G)
krombobreaker: 500G (1.6%, 20,202G)
old_overholt_: 500G (1.6%, 56,290G)
CapnChaos12: 300G (1.0%, 4,644G)
Rune339: 256G (0.8%, 256G)
lotharek: 200G (0.6%, 5,399G)

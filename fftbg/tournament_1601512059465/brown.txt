Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Male
Capricorn
46
79
Monk
Item
Counter
Equip Knife
Move-MP Up

Mythril Knife

Triangle Hat
Leather Outfit
Setiemson

Wave Fist, Earth Slash, Purification, Chakra
Potion, Ether, Eye Drop, Echo Grass, Remedy



ValensEXP
Male
Sagittarius
59
68
Geomancer
Draw Out
Faith Save
Equip Gun
Lava Walking

Stone Gun
Mythril Shield
Twist Headband
Silk Robe
Defense Ring

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Murasame, Kiyomori, Kikuichimoji



Girfred
Male
Capricorn
68
67
Thief
Item
Counter Magic
Equip Polearm
Retreat

Cypress Rod

Black Hood
Power Sleeve
Dracula Mantle

Steal Heart, Leg Aim
Hi-Potion, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down



Xenomorith
Male
Aries
51
67
Monk
Elemental
Arrow Guard
Defend
Jump+2



Headgear
Clothes
Cursed Ring

Spin Fist, Purification, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

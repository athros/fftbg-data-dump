Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Heropong
Male
Pisces
81
66
Archer
Basic Skill
Counter
Equip Shield
Jump+1

Bow Gun
Gold Shield
Holy Miter
Secret Clothes
Rubber Shoes

Charge+1, Charge+2, Charge+10
Throw Stone, Heal, Cheer Up, Wish



Mushufasa
Female
Aquarius
60
57
Wizard
Battle Skill
MP Restore
Magic Attack UP
Move+1

Ice Rod

Barette
Judo Outfit
Setiemson

Fire, Fire 3, Bolt, Bolt 4, Ice, Ice 3, Ice 4, Flare
Head Break, Shield Break, Speed Break, Night Sword



LeoNightFury
Male
Scorpio
55
53
Oracle
Summon Magic
Parry
Equip Polearm
Move-MP Up

Iron Fan

Flash Hat
White Robe
Bracer

Poison, Life Drain, Pray Faith, Silence Song, Petrify
Moogle, Shiva, Golem



Lord Gwarth
Female
Scorpio
53
50
Priest
Draw Out
Counter Tackle
Maintenance
Levitate

Rainbow Staff

Feather Hat
Earth Clothes
Elf Mantle

Raise, Regen, Protect, Shell, Shell 2, Wall, Esuna
Muramasa

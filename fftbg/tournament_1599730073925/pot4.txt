Final Bets: purple - 6 bets for 5,732G (63.1%, x0.59); brown - 6 bets for 3,354G (36.9%, x1.71)

purple bets:
Willjin: 3,000G (52.3%, 84,896G)
BirbBrainsBot: 1,000G (17.4%, 184,261G)
Raixelol: 584G (10.2%, 23,197G)
ColetteMSLP: 452G (7.9%, 452G)
getthemoneyz: 408G (7.1%, 1,905,333G)
Neech: 288G (5.0%, 3,904G)

brown bets:
iBardic: 2,000G (59.6%, 15,727G)
AllInBot: 586G (17.5%, 586G)
Kurtis_E_Flush_: 300G (8.9%, 4,006G)
ValensEXP_: 252G (7.5%, 1,977G)
datadrivenbot: 200G (6.0%, 58,258G)
BartTradingCompany: 16G (0.5%, 168G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Dasutin23
Female
Aries
66
69
Wizard
Battle Skill
Damage Split
Dual Wield
Move-HP Up

Mage Masher
Dragon Rod
Red Hood
Wizard Outfit
Small Mantle

Fire, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 4
Justice Sword



Upvla
Male
Leo
79
74
Calculator
Black Magic
Critical Quick
Attack UP
Lava Walking

Faith Rod

Cachusha
Wizard Robe
Magic Gauntlet

CT, 5, 3
Fire 2, Fire 3, Empower



Laserman1000
Male
Scorpio
61
66
Lancer
Summon Magic
Distribute
Martial Arts
Teleport


Escutcheon
Diamond Helmet
Genji Armor
Salty Rage

Level Jump3, Vertical Jump8
Moogle, Titan, Golem, Carbunkle, Leviathan



HeyBtw
Male
Virgo
78
70
Knight
Punch Art
Mana Shield
Long Status
Jump+3

Defender
Diamond Shield
Circlet
Diamond Armor
Bracer

Magic Break, Dark Sword
Secret Fist, Purification, Chakra, Revive

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



WhiteTigress
Male
Capricorn
44
78
Squire
Throw
PA Save
Dual Wield
Ignore Height

Flame Whip
Battle Axe
Diamond Helmet
Judo Outfit
Magic Gauntlet

Accumulate, Heal, Wish
Shuriken



Mesmaster
Male
Libra
76
55
Lancer
Elemental
Distribute
Doublehand
Move+1

Holy Lance

Iron Helmet
Leather Armor
Sprint Shoes

Level Jump8, Vertical Jump8
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind



NicoSavoy
Male
Leo
77
76
Mime

Mana Shield
Equip Shield
Retreat


Flame Shield
Leather Hat
Black Costume
Spike Shoes

Mimic




Curtice
Male
Taurus
40
71
Bard
Yin Yang Magic
Distribute
Defense UP
Move+2

Fairy Harp

Holy Miter
Diamond Armor
Cursed Ring

Angel Song, Cheer Song, Diamond Blade
Blind, Life Drain, Zombie, Foxbird, Paralyze

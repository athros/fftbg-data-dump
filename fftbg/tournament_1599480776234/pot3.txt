Final Bets: white - 12 bets for 6,720G (71.6%, x0.40); black - 6 bets for 2,663G (28.4%, x2.52)

white bets:
gorgewall: 1,300G (19.3%, 1,300G)
brodeity: 1,051G (15.6%, 1,051G)
VolgraTheMoose: 1,001G (14.9%, 118,980G)
BirbBrainsBot: 1,000G (14.9%, 87,908G)
ForagerCats: 500G (7.4%, 12,809G)
CosmicTactician: 400G (6.0%, 32,157G)
SephDarkheart: 368G (5.5%, 7,648G)
BartTradingCompany: 321G (4.8%, 3,216G)
killth3kid: 250G (3.7%, 19,577G)
AllInBot: 229G (3.4%, 229G)
datadrivenbot: 200G (3.0%, 58,072G)
Bongomon7: 100G (1.5%, 2,593G)

black bets:
SeniorBunk: 1,400G (52.6%, 12,529G)
getthemoneyz: 438G (16.4%, 1,870,523G)
ValensEXP_: 216G (8.1%, 1,012G)
PuzzleSecretary: 209G (7.8%, 209G)
khelor_: 200G (7.5%, 3,677G)
Sietaha: 200G (7.5%, 2,694G)

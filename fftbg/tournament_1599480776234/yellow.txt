Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thebluesman
Female
Aries
49
66
Oracle
Battle Skill
Meatbone Slash
Secret Hunt
Ignore Terrain

Musk Rod

Holy Miter
Black Costume
Feather Boots

Poison, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Sleep, Petrify, Dark Holy
Weapon Break, Justice Sword, Dark Sword



Aldrammech
Monster
Gemini
65
48
Red Chocobo










TheSabretoothe
Male
Sagittarius
77
60
Oracle
Battle Skill
Counter Tackle
Defend
Fly

Bestiary

Triangle Hat
Silk Robe
Feather Boots

Blind, Life Drain, Zombie, Silence Song, Confusion Song, Petrify
Head Break, Armor Break



PuzzleSecretary
Female
Sagittarius
45
77
Time Mage
White Magic
Mana Shield
Long Status
Jump+3

Wizard Staff

Thief Hat
Chameleon Robe
Red Shoes

Slow, Stop, Reflect, Demi, Stabilize Time
Cure, Cure 2, Cure 3, Regen, Protect, Holy

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Just Here2
Male
Capricorn
46
80
Archer
Sing
Brave Save
Short Status
Jump+3

Silver Bow

Holy Miter
Brigandine
Feather Boots

Charge+1, Charge+20
Last Song, Sky Demon, Hydra Pit



Technominari
Female
Sagittarius
77
78
Priest
Throw
Blade Grasp
Attack UP
Lava Walking

Healing Staff

Ribbon
Earth Clothes
Wizard Mantle

Cure 2, Raise, Reraise, Regen, Protect 2, Shell 2, Esuna
Shuriken, Staff, Axe, Wand



Verriand
Male
Gemini
43
59
Archer
Yin Yang Magic
HP Restore
Martial Arts
Move+1

Hunting Bow
Bronze Shield
Green Beret
Mystic Vest
Wizard Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10
Blind, Poison, Foxbird, Confusion Song, Dispel Magic



HASTERIOUS
Female
Taurus
49
72
Calculator
White Magic
Faith Save
Equip Armor
Move-HP Up

Thunder Rod

Mythril Helmet
Plate Mail
Jade Armlet

CT, Prime Number, 5, 4
Raise, Reraise, Regen, Protect 2, Wall

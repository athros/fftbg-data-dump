Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



AltimaMantoid
Female
Sagittarius
71
62
Chemist
Talk Skill
Counter
Magic Defense UP
Ignore Height

Stone Gun

Golden Hairpin
Brigandine
Sprint Shoes

Hi-Potion, Soft, Remedy, Phoenix Down
Invitation, Threaten, Solution, Mimic Daravon, Refute



Rune339
Female
Leo
69
43
Ninja
Battle Skill
Damage Split
Equip Axe
Swim

Short Edge
Slasher
Leather Hat
Wizard Outfit
Sprint Shoes

Shuriken, Bomb, Stick
Shield Break, Power Break, Dark Sword, Night Sword



Lord Gwarth
Male
Cancer
77
57
Samurai
Elemental
Counter Tackle
Dual Wield
Move+1

Bizen Boat
Murasame
Bronze Helmet
Wizard Robe
Defense Ring

Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Ser Pyrro
Male
Cancer
53
39
Thief
Throw
Dragon Spirit
Sicken
Jump+2

Orichalcum

Green Beret
Clothes
Battle Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Shield
Shuriken

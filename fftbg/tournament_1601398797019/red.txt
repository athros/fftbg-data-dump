Player: !Red
Team: Red Team
Palettes: Red/Brown



PolkaGrandma
Female
Capricorn
75
59
Squire
Charge
Abandon
Attack UP
Waterbreathing

Broad Sword
Platinum Shield
Leather Hat
Earth Clothes
Red Shoes

Accumulate, Dash, Yell, Cheer Up, Wish
Charge+2, Charge+3, Charge+4, Charge+5, Charge+10



E Ballard
Male
Gemini
65
79
Summoner
Elemental
MA Save
Equip Gun
Swim

Bloody Strings

Green Beret
Power Sleeve
Magic Gauntlet

Moogle, Shiva, Ramuh, Titan, Golem, Leviathan, Silf
Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Projekt Mike
Male
Gemini
75
77
Knight
White Magic
Parry
Doublehand
Lava Walking

Ragnarok

Bronze Helmet
Bronze Armor
Rubber Shoes

Weapon Break, Magic Break, Power Break
Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise



Gooseyourself
Male
Serpentarius
60
57
Bard
White Magic
Damage Split
Equip Axe
Levitate

Flame Whip

Leather Hat
Reflect Mail
Germinas Boots

Angel Song, Life Song, Cheer Song, Battle Song, Nameless Song, Last Song, Sky Demon
Cure 3, Raise, Protect, Wall, Esuna

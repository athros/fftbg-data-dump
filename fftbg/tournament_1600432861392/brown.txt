Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Male
Sagittarius
56
55
Lancer
Punch Art
Regenerator
Defend
Lava Walking

Obelisk
Bronze Shield
Bronze Helmet
Linen Robe
Battle Boots

Level Jump8, Vertical Jump8
Spin Fist, Pummel, Earth Slash, Purification, Seal Evil



Thyrandaal
Monster
Virgo
59
67
Red Chocobo










MemoriesofFinal
Female
Leo
65
73
Geomancer
Summon Magic
Counter Tackle
Maintenance
Move-MP Up

Asura Knife
Round Shield
Twist Headband
Brigandine
Rubber Shoes

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Golem, Leviathan, Salamander, Silf



Error72
Male
Aries
43
66
Lancer
Steal
Regenerator
Defense UP
Ignore Terrain

Obelisk
Platinum Shield
Gold Helmet
White Robe
Sprint Shoes

Level Jump8, Vertical Jump8
Steal Helmet, Steal Shield

Final Bets: black - 11 bets for 8,155G (56.9%, x0.76); purple - 8 bets for 6,176G (43.1%, x1.32)

black bets:
dogsandcatsand: 3,993G (49.0%, 30,000G)
Leonidusx: 2,000G (24.5%, 84,153G)
Lord_Gwarth: 500G (6.1%, 3,597G)
run_with_stone_GUNs: 420G (5.2%, 8,983G)
Chihuahua_Charity: 264G (3.2%, 264G)
highelectricaltemperature: 228G (2.8%, 494G)
ericzubat: 200G (2.5%, 4,947G)
datadrivenbot: 200G (2.5%, 65,638G)
ar_tactic: 150G (1.8%, 99,394G)
MemoriesofFinal: 100G (1.2%, 40,838G)
maakur_: 100G (1.2%, 2,833G)

purple bets:
LivingHitokiri: 2,000G (32.4%, 63,481G)
biske13: 1,000G (16.2%, 227,601G)
BirbBrainsBot: 1,000G (16.2%, 66,099G)
TheKittenWithaPancake: 1,000G (16.2%, 10,583G)
RunicMagus: 800G (13.0%, 14,143G)
AllInBot: 200G (3.2%, 200G)
Dodecadeath: 112G (1.8%, 112G)
getthemoneyz: 64G (1.0%, 1,977,027G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LivingHitokiri
Male
Capricorn
48
64
Archer
Steal
Faith Save
Doublehand
Waterwalking

Bow Gun

Twist Headband
Wizard Outfit
Defense Ring

Charge+4, Charge+7
Steal Heart, Steal Helmet, Steal Weapon, Steal Status



RunicMagus
Female
Aries
73
70
Chemist
Draw Out
Mana Shield
Equip Axe
Retreat

Battle Axe

Feather Hat
Brigandine
Wizard Mantle

Potion, Hi-Potion, X-Potion, Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Koutetsu, Murasame, Muramasa



Xsifthewolf
Female
Capricorn
52
65
Lancer
Elemental
Distribute
Short Status
Fly

Gungnir
Ice Shield
Platinum Helmet
Mythril Armor
Vanish Mantle

Level Jump2, Vertical Jump8
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard



Actual JP
Male
Serpentarius
49
54
Mediator
Elemental
Hamedo
Dual Wield
Fly

Bestiary
Papyrus Codex
Golden Hairpin
Chameleon Robe
Power Wrist

Persuade, Praise, Threaten, Insult
Water Ball, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind, Lava Ball

Final Bets: purple - 7 bets for 4,432G (42.4%, x1.36); brown - 12 bets for 6,016G (57.6%, x0.74)

purple bets:
LivingHitokiri: 2,000G (45.1%, 59,600G)
RunicMagus: 600G (13.5%, 13,212G)
BirbBrainsBot: 532G (12.0%, 65,457G)
Lyner87: 500G (11.3%, 107,058G)
ColetteMSLP: 500G (11.3%, 4,077G)
AllInBot: 200G (4.5%, 200G)
xsifthewolf: 100G (2.3%, 4,367G)

brown bets:
dogsandcatsand: 1,111G (18.5%, 32,137G)
Error72: 1,001G (16.6%, 42,767G)
Chihuahua_Charity: 989G (16.4%, 989G)
J2DaBibbles: 824G (13.7%, 824G)
helpimabug: 764G (12.7%, 764G)
ericzubat: 500G (8.3%, 4,864G)
highelectricaltemperature: 228G (3.8%, 228G)
datadrivenbot: 200G (3.3%, 65,605G)
Dodecadeath: 112G (1.9%, 112G)
gorgewall: 101G (1.7%, 6,670G)
MemoriesofFinal: 100G (1.7%, 40,817G)
getthemoneyz: 86G (1.4%, 1,976,535G)

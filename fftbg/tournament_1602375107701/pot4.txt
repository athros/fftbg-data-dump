Final Bets: purple - 10 bets for 5,716G (19.3%, x4.19); brown - 17 bets for 23,945G (80.7%, x0.24)

purple bets:
Runeseeker22: 1,532G (26.8%, 30,649G)
BirbBrainsBot: 1,000G (17.5%, 64,398G)
reinoe: 1,000G (17.5%, 20,374G)
getthemoneyz: 544G (9.5%, 2,216,871G)
krombobreaker: 500G (8.7%, 34,118G)
tinytittyfiend: 360G (6.3%, 360G)
escobro: 280G (4.9%, 4,729G)
ExplanationMarkWow: 250G (4.4%, 8,303G)
Lord_Gwarth: 150G (2.6%, 3,437G)
Spartan_Paladin: 100G (1.7%, 6,600G)

brown bets:
Mushufasa_: 10,000G (41.8%, 60,412G)
CorpusCav: 3,500G (14.6%, 105,900G)
Smashy: 2,609G (10.9%, 2,609G)
DeathTaxesAndAnime: 1,111G (4.6%, 36,050G)
Breakdown777: 1,024G (4.3%, 1,024G)
verythinice: 1,000G (4.2%, 6,012G)
TheBrett: 1,000G (4.2%, 34,583G)
YowaneHaku: 926G (3.9%, 926G)
Kronikle: 700G (2.9%, 29,230G)
WhiteDog420: 500G (2.1%, 976G)
DLJuggernaut: 400G (1.7%, 400G)
seppu777: 333G (1.4%, 2,528G)
Demon_Lord_Josh: 240G (1.0%, 240G)
gorgewall: 201G (0.8%, 7,402G)
AllInBot: 200G (0.8%, 200G)
datadrivenbot: 200G (0.8%, 86,108G)
fistfulofbees13: 1G (0.0%, 1,032G)

Final Bets: red - 10 bets for 5,482G (21.2%, x3.72); yellow - 17 bets for 20,382G (78.8%, x0.27)

red bets:
nekojin: 3,718G (67.8%, 7,292G)
acrusade: 305G (5.6%, 305G)
ColetteMSLP: 300G (5.5%, 1,519G)
AltimaMantoid: 276G (5.0%, 276G)
killth3kid: 250G (4.6%, 15,930G)
PuzzleSecretary: 184G (3.4%, 1,252G)
roqqqpsi: 149G (2.7%, 1,663G)
projekt_mike: 100G (1.8%, 100G)
Lemonjohns: 100G (1.8%, 3,962G)
TheSabretoothe: 100G (1.8%, 100G)

yellow bets:
reinoe: 10,000G (49.1%, 24,324G)
VolgraTheMoose: 1,501G (7.4%, 124,299G)
Dasutin23: 1,000G (4.9%, 9,963G)
BirbBrainsBot: 1,000G (4.9%, 123,372G)
NicoSavoy: 1,000G (4.9%, 4,184G)
DeathTaxesAndAnime: 968G (4.7%, 968G)
E_Ballard: 888G (4.4%, 130,026G)
Shalloween: 764G (3.7%, 764G)
CorpusCav: 617G (3.0%, 617G)
Mushufasa_: 600G (2.9%, 33,723G)
old_overholt_: 500G (2.5%, 15,716G)
LordTomS: 500G (2.5%, 23,048G)
krombobreaker: 324G (1.6%, 324G)
bigbongsmoker: 236G (1.2%, 1,126G)
Yangusburger: 200G (1.0%, 12,319G)
datadrivenbot: 200G (1.0%, 59,793G)
getthemoneyz: 84G (0.4%, 1,815,075G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheBrett
Male
Aquarius
81
72
Ninja
Battle Skill
Counter Tackle
Equip Gun
Move+1

Papyrus Codex
Bestiary
Golden Hairpin
Leather Outfit
Reflect Ring

Shuriken, Wand
Shield Break, Mind Break, Justice Sword



King Smashington
Female
Aries
65
64
Calculator
Gore Skill
Counter Tackle
Defense UP
Waterbreathing

Dragon Rod

Barbuta
Genji Armor
Sprint Shoes

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Mushufasa
Monster
Aquarius
73
74
Bomb










Tinytittyfiend
Female
Aries
60
68
Dancer
Yin Yang Magic
Regenerator
Equip Knife
Ignore Height

Zorlin Shape

Feather Hat
Black Costume
N-Kai Armlet

Witch Hunt, Slow Dance, Obsidian Blade, Void Storage
Blind, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze

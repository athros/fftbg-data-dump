Player: !zChamp
Team: Champion Team
Palettes: White/Blue



HuffFlex
Male
Gemini
67
63
Lancer
Item
Critical Quick
Beastmaster
Swim

Gungnir
Round Shield
Mythril Helmet
Diamond Armor
Wizard Mantle

Level Jump5, Vertical Jump8
Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Phoenix Down



Maakur
Male
Virgo
57
57
Lancer
Item
Counter
Throw Item
Ignore Height

Spear
Mythril Shield
Iron Helmet
Linen Robe
Cursed Ring

Level Jump5, Vertical Jump4
Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water



Old Overholt
Female
Pisces
62
53
Priest
Yin Yang Magic
Earplug
Short Charge
Jump+3

Healing Staff

Flash Hat
Linen Robe
Wizard Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Regen, Shell, Esuna
Life Drain, Zombie, Silence Song, Foxbird, Sleep, Petrify



Dodecadeath
Monster
Serpentarius
54
44
Serpentarius








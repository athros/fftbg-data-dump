Player: !Red
Team: Red Team
Palettes: Red/Brown



HASTERIOUS
Male
Cancer
75
57
Lancer
White Magic
Distribute
Maintenance
Jump+1

Battle Bamboo
Mythril Shield
Grand Helmet
Genji Armor
Diamond Armlet

Level Jump8, Vertical Jump4
Cure 2, Raise, Raise 2, Protect, Esuna, Holy



Biske13
Female
Scorpio
64
49
Calculator
Black Magic
Parry
Halve MP
Swim

Musk Rod

Flash Hat
Silk Robe
Dracula Mantle

CT, Prime Number, 5
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice 2



HeyBtw
Male
Capricorn
57
49
Archer
Steal
Meatbone Slash
Maintenance
Move-MP Up

Glacier Gun
Buckler
Twist Headband
Mythril Vest
Battle Boots

Charge+1, Charge+2
Steal Helmet, Steal Shield, Steal Status, Leg Aim



NovaKnight21
Female
Capricorn
74
75
Geomancer
Jump
MP Restore
Attack UP
Waterwalking

Slasher
Platinum Shield
Green Beret
Adaman Vest
Cursed Ring

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Level Jump4, Vertical Jump4

Player: !White
Team: White Team
Palettes: White/Blue



Ryjelsum
Female
Libra
44
52
Mediator
White Magic
MA Save
Doublehand
Teleport

Blast Gun

Golden Hairpin
Wizard Outfit
Feather Mantle

Invitation, Persuade, Preach, Negotiate, Mimic Daravon, Refute
Raise 2, Regen, Protect, Protect 2, Shell, Shell 2, Wall, Esuna, Holy



Kronikle
Male
Serpentarius
68
48
Knight
Charge
Counter Tackle
Attack UP
Waterbreathing

Defender
Flame Shield
Iron Helmet
Carabini Mail
Defense Ring

Weapon Break, Magic Break, Speed Break, Stasis Sword
Charge+1, Charge+3, Charge+4, Charge+20



Chuckolator
Male
Sagittarius
61
47
Geomancer
Draw Out
Parry
Defend
Move-HP Up

Giant Axe
Diamond Shield
Feather Hat
Mystic Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Murasame



Mesmaster
Male
Aquarius
48
47
Geomancer
Sing
Earplug
Concentrate
Jump+2

Heaven's Cloud
Crystal Shield
Barette
Leather Outfit
Genji Gauntlet

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cheer Song, Magic Song, Nameless Song

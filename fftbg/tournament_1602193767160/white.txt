Player: !White
Team: White Team
Palettes: White/Blue



ChiChiRodriguez
Female
Cancer
49
79
Samurai
Throw
Arrow Guard
Doublehand
Ignore Height

Partisan

Crystal Helmet
Genji Armor
108 Gems

Asura, Muramasa, Kikuichimoji
Bomb, Staff



Leonidusx
Male
Cancer
71
67
Ninja
Draw Out
Faith Save
Equip Gun
Swim

Bestiary
Papyrus Codex
Golden Hairpin
Judo Outfit
Small Mantle

Knife, Staff, Ninja Sword, Wand
Murasame



Fenaen
Male
Libra
73
66
Monk
Charge
Counter Tackle
Concentrate
Waterwalking



Triangle Hat
Clothes
Bracer

Spin Fist, Purification
Charge+2, Charge+3, Charge+5



King Smashington
Male
Capricorn
64
64
Thief
Item
Regenerator
Throw Item
Move+2

Broad Sword

Thief Hat
Wizard Outfit
108 Gems

Steal Helmet, Steal Accessory, Arm Aim
Ether, Hi-Ether, Eye Drop, Holy Water, Remedy

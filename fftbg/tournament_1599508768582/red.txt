Player: !Red
Team: Red Team
Palettes: Red/Brown



Lord Gwarth
Male
Libra
67
72
Archer
Steal
Damage Split
Dual Wield
Move+3

Night Killer
Bow Gun
Golden Hairpin
Black Costume
Bracer

Charge+1, Charge+2, Charge+10
Gil Taking, Steal Heart, Steal Armor, Steal Shield



Dasutin23
Female
Scorpio
50
44
Lancer
Charge
MP Restore
Dual Wield
Waterbreathing

Obelisk
Cypress Rod
Barbuta
Platinum Armor
Feather Mantle

Level Jump2, Vertical Jump4
Charge+2, Charge+3



Bigbongsmoker
Male
Pisces
72
55
Priest
Black Magic
PA Save
Magic Attack UP
Teleport

Flail

Headgear
Earth Clothes
Jade Armlet

Cure, Cure 4, Protect 2
Fire 3, Bolt, Ice 2, Ice 3



Technominari
Female
Gemini
59
47
Priest
Summon Magic
Regenerator
Martial Arts
Waterwalking

Morning Star

Black Hood
White Robe
Reflect Ring

Cure, Cure 3, Raise, Reraise, Protect 2, Wall, Esuna
Moogle, Shiva, Ramuh, Titan, Golem, Leviathan, Silf

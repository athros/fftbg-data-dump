Player: !White
Team: White Team
Palettes: White/Blue



Seppu777
Male
Aries
70
43
Priest
Sing
Brave Save
Doublehand
Waterbreathing

Flail

Triangle Hat
Linen Robe
Defense Ring

Cure 2, Cure 3, Cure 4, Raise, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna, Holy
Life Song, Magic Song



OneHundredFists
Female
Aquarius
55
53
Knight
Black Magic
Auto Potion
Doublehand
Waterbreathing

Defender

Crystal Helmet
Black Robe
Genji Gauntlet

Head Break, Power Break, Dark Sword
Fire 2, Fire 3, Bolt, Ice 2, Ice 4



ArrenJevleth
Female
Aries
58
52
Lancer
Item
Dragon Spirit
Defend
Ignore Terrain

Obelisk
Flame Shield
Barbuta
Leather Armor
Spike Shoes

Level Jump2, Vertical Jump2
Hi-Potion, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



W0RLDEND
Male
Cancer
49
44
Bard
Steal
Arrow Guard
Magic Attack UP
Move+3

Fairy Harp

Red Hood
Secret Clothes
Bracer

Angel Song, Life Song, Cheer Song, Nameless Song, Diamond Blade, Sky Demon
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim

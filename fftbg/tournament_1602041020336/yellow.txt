Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DeathTaxesAndAnime
Female
Capricorn
67
54
Thief
Jump
Catch
Dual Wield
Ignore Height

Sasuke Knife
Dagger
Green Beret
Mythril Vest
Magic Ring

Steal Armor, Steal Shield, Steal Status
Level Jump8, Vertical Jump8



Khelor
Male
Pisces
68
49
Time Mage
Battle Skill
Parry
Maintenance
Move-HP Up

White Staff

Twist Headband
Leather Outfit
Red Shoes

Haste, Slow, Stop, Quick
Head Break, Armor Break, Shield Break, Magic Break, Stasis Sword, Dark Sword



Thyrandaal
Female
Scorpio
49
76
Wizard
Throw
PA Save
Sicken
Swim

Faith Rod

Flash Hat
Black Robe
Cursed Ring

Bolt 2, Bolt 3, Ice 2, Empower, Frog, Death, Flare
Shuriken, Bomb



Error72
Male
Libra
76
45
Ninja
Draw Out
Brave Save
Concentrate
Fly

Flail
Hidden Knife
Green Beret
Chain Vest
Angel Ring

Bomb, Axe
Koutetsu, Kiyomori

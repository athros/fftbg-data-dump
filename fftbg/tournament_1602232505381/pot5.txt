Final Bets: blue - 11 bets for 4,711G (52.9%, x0.89); green - 4 bets for 4,191G (47.1%, x1.12)

blue bets:
HaplessOne: 1,111G (23.6%, 39,750G)
BirbBrainsBot: 789G (16.7%, 182,349G)
ColetteMSLP: 520G (11.0%, 1,099G)
2b_yorha_b: 500G (10.6%, 30,368G)
3ngag3: 456G (9.7%, 17,556G)
iBardic: 408G (8.7%, 408G)
getthemoneyz: 232G (4.9%, 2,198,638G)
datadrivenbot: 200G (4.2%, 83,040G)
Evewho: 200G (4.2%, 13,079G)
Zachara: 194G (4.1%, 110,594G)
gorgewall: 101G (2.1%, 3,936G)

green bets:
CorpusCav: 2,000G (47.7%, 58,012G)
Sairentozon7: 1,290G (30.8%, 1,290G)
Lolthsmeat: 701G (16.7%, 701G)
AllInBot: 200G (4.8%, 200G)

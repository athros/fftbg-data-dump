Player: !Red
Team: Red Team
Palettes: Red/Brown



Old Overholt
Male
Aries
43
45
Knight
Throw
Absorb Used MP
Short Status
Move+1

Long Sword
Ice Shield
Cross Helmet
Gold Armor
Cherche

Shield Break, Speed Break, Power Break, Justice Sword, Surging Sword
Shuriken



DLJuggernaut
Female
Leo
57
46
Summoner
Punch Art
Counter
Halve MP
Move+1

Healing Staff

Headgear
Linen Robe
Sprint Shoes

Shiva, Carbunkle, Salamander, Fairy, Lich
Spin Fist, Secret Fist, Revive



StealthModeLocke
Female
Sagittarius
49
57
Calculator
White Magic
MA Save
Equip Knife
Ignore Height

Assassin Dagger

Holy Miter
Mystic Vest
Magic Gauntlet

Height, Prime Number, 5, 4
Cure, Raise, Protect, Shell 2, Esuna



Chuckolator
Female
Capricorn
44
43
Mime

Abandon
Dual Wield
Move+3



Flash Hat
Mystic Vest
Dracula Mantle

Mimic


Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lythe Caraker
Male
Serpentarius
67
59
Chemist
Talk Skill
Parry
Short Charge
Levitate

Star Bag

Barette
Mystic Vest
Germinas Boots

Ether, Echo Grass, Soft, Phoenix Down
Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon



Smashy
Male
Aquarius
62
66
Archer
Yin Yang Magic
Parry
Doublehand
Move+2

Blaze Gun

Green Beret
Judo Outfit
Chantage

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Blind, Poison, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Dark Holy



HaateXIII
Male
Serpentarius
53
68
Monk
Jump
Brave Save
Short Status
Fly



Thief Hat
Mystic Vest
Feather Mantle

Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Level Jump3, Vertical Jump7



Holdenmagronik
Male
Capricorn
68
59
Chemist
Yin Yang Magic
Regenerator
Dual Wield
Waterwalking

Panther Bag
Star Bag
Holy Miter
Judo Outfit
N-Kai Armlet

Potion, Hi-Potion, X-Potion, Eye Drop, Holy Water, Phoenix Down
Life Drain, Pray Faith, Dispel Magic, Sleep

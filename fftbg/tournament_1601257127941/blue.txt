Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Willjin
Female
Gemini
54
55
Geomancer
Basic Skill
PA Save
Magic Attack UP
Jump+3

Giant Axe
Escutcheon
Triangle Hat
Adaman Vest
Elf Mantle

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Accumulate, Throw Stone, Heal, Yell, Wish



Zeroroute
Female
Virgo
71
52
Archer
White Magic
HP Restore
Equip Gun
Jump+1

Battle Folio
Mythril Shield
Barette
Brigandine
Spike Shoes

Charge+2
Cure 2, Raise, Protect 2, Wall, Esuna



Lord Gwarth
Female
Scorpio
45
71
Priest
Basic Skill
Parry
Magic Attack UP
Move-HP Up

Gold Staff

Golden Hairpin
White Robe
Spike Shoes

Cure, Cure 4, Raise, Raise 2, Protect, Shell, Wall, Esuna
Dash, Heal, Tickle, Cheer Up, Fury, Ultima



ANFz
Male
Pisces
52
49
Geomancer
Sing
PA Save
Short Charge
Ignore Height

Battle Axe
Ice Shield
Feather Hat
Chain Vest
Bracer

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Angel Song, Life Song, Space Storage, Sky Demon

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Whelming Wave
Female
Pisces
71
56
Calculator
Black Magic
Catch
Equip Armor
Swim

Star Bag

Circlet
Gold Armor
Red Shoes

CT, 5, 4, 3
Fire 2, Fire 3, Fire 4, Bolt, Bolt 4, Ice, Ice 3, Empower



Chuckolator
Male
Gemini
79
71
Thief
Black Magic
Counter Tackle
Equip Shield
Ignore Terrain

Blind Knife
Bronze Shield
Golden Hairpin
Power Sleeve
Cursed Ring

Gil Taking, Steal Armor, Steal Shield, Leg Aim
Fire 3, Bolt 4, Ice 3, Death, Flare



NovaKnight21
Male
Cancer
74
56
Chemist
Sing
Counter
Equip Bow
Teleport

Poison Bow

Thief Hat
Mystic Vest
Bracer

Potion, Hi-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Nameless Song, Last Song, Space Storage, Sky Demon



Dogsandcatsand
Female
Libra
77
77
Wizard
Yin Yang Magic
Absorb Used MP
Beastmaster
Waterwalking

Mythril Knife

Holy Miter
Mythril Vest
Genji Gauntlet

Fire 2, Bolt, Bolt 3, Ice, Empower, Frog
Blind, Spell Absorb, Silence Song, Blind Rage, Dispel Magic, Paralyze, Petrify, Dark Holy

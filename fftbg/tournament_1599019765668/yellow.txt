Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Silentkaster
Female
Capricorn
65
51
Calculator
White Magic
Critical Quick
Magic Defense UP
Move+2

Papyrus Codex

Twist Headband
Mystic Vest
Rubber Shoes

CT, Height, Prime Number, 5
Cure 3, Raise, Esuna



Windsah
Male
Sagittarius
76
71
Monk
Steal
Auto Potion
Long Status
Move+1



Red Hood
Mythril Vest
Wizard Mantle

Pummel, Wave Fist, Earth Slash, Purification, Chakra
Gil Taking, Leg Aim



CorpusCav
Male
Leo
68
46
Summoner
Battle Skill
Distribute
Equip Armor
Move+2

Rainbow Staff

Leather Helmet
Chain Mail
Defense Armlet

Moogle, Carbunkle
Head Break, Shield Break, Speed Break, Power Break



Roofiepops
Male
Libra
41
61
Calculator
Yin Yang Magic
Auto Potion
Magic Attack UP
Move+2

Bestiary

Headgear
Earth Clothes
Wizard Mantle

CT, Prime Number, 5
Confusion Song, Sleep

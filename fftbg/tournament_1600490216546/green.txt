Player: !Green
Team: Green Team
Palettes: Green/White



Roofiepops
Female
Pisces
52
57
Lancer
Talk Skill
Auto Potion
Equip Sword
Fly

Save the Queen

Crystal Helmet
Linen Robe
Germinas Boots

Level Jump8, Vertical Jump2
Persuade, Preach, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate



Gelwain
Female
Leo
73
70
Mime

Counter
Magic Attack UP
Swim



Red Hood
Power Sleeve
Feather Boots

Mimic




Resjudicata3
Female
Aquarius
77
71
Time Mage
Talk Skill
Counter Tackle
Magic Attack UP
Levitate

Healing Staff

Golden Hairpin
Clothes
Power Wrist

Slow, Slow 2, Stop, Float, Quick, Demi
Invitation, Praise, Threaten, Preach, Negotiate, Mimic Daravon, Refute



Phi Sig
Female
Pisces
67
48
Summoner
Time Magic
Abandon
Short Charge
Waterwalking

Poison Rod

Flash Hat
White Robe
Magic Gauntlet

Moogle, Ifrit, Carbunkle, Cyclops
Haste, Stop, Demi 2, Stabilize Time, Galaxy Stop

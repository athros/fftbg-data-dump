Player: !Green
Team: Green Team
Palettes: Green/White



Gobizku
Female
Virgo
50
54
Geomancer
Dance
Sunken State
Defense UP
Levitate

Slasher
Bronze Shield
Flash Hat
Chameleon Robe
Spike Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Witch Hunt, Slow Dance, Dragon Pit



PoroTact
Female
Virgo
41
54
Oracle
Dance
Auto Potion
Defense UP
Retreat

Octagon Rod

Red Hood
Earth Clothes
Sprint Shoes

Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Foxbird, Dispel Magic
Last Dance, Obsidian Blade, Nether Demon, Dragon Pit



Ar Tactic
Male
Pisces
68
59
Ninja
Yin Yang Magic
Hamedo
Sicken
Move+2

Air Knife
Assassin Dagger
Triangle Hat
Judo Outfit
Power Wrist

Shuriken
Blind, Spell Absorb



ChiChiRodriguez
Male
Scorpio
40
53
Wizard
Jump
Mana Shield
Sicken
Move-HP Up

Poison Rod

Twist Headband
Robe of Lords
Salty Rage

Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4
Level Jump5, Vertical Jump4

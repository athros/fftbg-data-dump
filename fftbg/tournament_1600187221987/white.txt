Player: !White
Team: White Team
Palettes: White/Blue



Error72
Male
Sagittarius
63
59
Oracle
Black Magic
Speed Save
Beastmaster
Move+1

Musk Rod

Headgear
Chain Vest
Power Wrist

Poison, Life Drain, Pray Faith, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify, Dark Holy
Fire, Fire 3, Fire 4, Bolt, Ice 4, Death



Craymel Mage
Male
Gemini
70
64
Time Mage
Charge
Absorb Used MP
Defend
Move+3

Musk Rod

Thief Hat
White Robe
Angel Ring

Stop, Demi 2, Stabilize Time
Charge+1, Charge+3, Charge+4



DavenIII
Male
Aries
42
69
Archer
Punch Art
Meatbone Slash
Magic Defense UP
Move+3

Long Bow

Triangle Hat
Leather Outfit
Defense Ring

Charge+1, Charge+2, Charge+3, Charge+7
Pummel, Purification, Revive



Snkey
Male
Capricorn
54
75
Mime

Abandon
Magic Defense UP
Levitate



Twist Headband
Adaman Vest
Germinas Boots

Mimic


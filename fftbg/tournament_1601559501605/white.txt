Player: !White
Team: White Team
Palettes: White/Blue



CosmicTactician
Male
Aquarius
39
79
Squire
Jump
Distribute
Dual Wield
Waterwalking

Snipe Bow
Hunting Bow
Flash Hat
Rubber Costume
Salty Rage

Accumulate, Dash, Heal, Fury, Wish
Level Jump8, Vertical Jump2



Galkife
Female
Gemini
52
65
Squire
Steal
Speed Save
Martial Arts
Move+2


Flame Shield
Headgear
Reflect Mail
Feather Boots

Accumulate, Heal, Tickle
Steal Armor, Steal Weapon, Steal Status



Roofiepops
Male
Aries
79
68
Oracle
Draw Out
Counter Tackle
Equip Armor
Jump+3

Battle Bamboo

Iron Helmet
Silk Robe
Sprint Shoes

Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Bizen Boat, Heaven's Cloud, Chirijiraden



Actual JP
Male
Scorpio
45
44
Bard
White Magic
Hamedo
Doublehand
Move+2

Ramia Harp

Holy Miter
Power Sleeve
Feather Boots

Angel Song, Cheer Song, Nameless Song, Last Song, Space Storage
Cure 2, Cure 4, Raise, Protect 2, Shell 2, Esuna, Holy

Player: !Green
Team: Green Team
Palettes: Green/White



Nhammen
Male
Scorpio
43
78
Monk
Jump
Counter
Secret Hunt
Move-HP Up



Cachusha
Brigandine
Elf Mantle

Spin Fist, Pummel, Secret Fist, Purification, Chakra
Level Jump3, Vertical Jump4



TheBrett
Male
Sagittarius
79
49
Oracle
Talk Skill
Catch
Attack UP
Retreat

Octagon Rod

Barette
Clothes
Red Shoes

Dispel Magic
Invitation, Insult, Rehabilitate



Sairentozon7
Female
Aquarius
73
43
Ninja
White Magic
Caution
Equip Polearm
Levitate

Javelin
Holy Lance
Leather Hat
Chain Vest
Defense Armlet

Bomb, Staff
Cure 3, Raise, Reraise, Protect, Protect 2, Wall



Chihuahua Charity
Female
Aquarius
53
72
Lancer
Punch Art
Damage Split
Beastmaster
Jump+1

Obelisk
Round Shield
Platinum Helmet
Genji Armor
Cherche

Level Jump5, Vertical Jump2
Earth Slash, Purification, Seal Evil

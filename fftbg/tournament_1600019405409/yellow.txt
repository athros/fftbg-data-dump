Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Reinoe
Male
Pisces
56
56
Mime

MP Restore
Martial Arts
Move-HP Up



Barette
Power Sleeve
Rubber Shoes

Mimic




Kohlingen
Female
Sagittarius
56
70
Monk
Charge
MA Save
Long Status
Move-MP Up



Twist Headband
Mystic Vest
Magic Ring

Chakra, Revive
Charge+1, Charge+3, Charge+4



Fattunaking
Male
Cancer
45
54
Monk
Elemental
Distribute
Sicken
Lava Walking



Leather Hat
Earth Clothes
Bracer

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Resjudicata3
Female
Cancer
75
77
Chemist
Steal
Caution
Halve MP
Levitate

Hydra Bag

Feather Hat
Secret Clothes
Wizard Mantle

Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy
Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim

Player: !Green
Team: Green Team
Palettes: Green/White



Gorgewall
Male
Virgo
48
80
Thief
Battle Skill
Regenerator
Sicken
Move+1

Air Knife

Leather Hat
Black Costume
Cursed Ring

Steal Heart
Head Break, Armor Break, Power Break, Mind Break



Spartan Paladin
Male
Taurus
48
53
Knight
Time Magic
Speed Save
Equip Polearm
Move-HP Up

Partisan
Genji Shield
Leather Helmet
Wizard Robe
Feather Boots

Head Break, Shield Break, Speed Break, Power Break, Justice Sword, Dark Sword
Haste, Haste 2, Slow, Immobilize, Float, Quick, Meteor



Upvla
Male
Leo
57
41
Chemist
Jump
Parry
Equip Shield
Jump+1

Stone Gun
Bronze Shield
Twist Headband
Wizard Outfit
Rubber Shoes

Hi-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Level Jump8, Vertical Jump7



IdioSyncr4zy
Male
Cancer
79
56
Monk
Yin Yang Magic
Counter
Defense UP
Waterbreathing



Flash Hat
Mythril Vest
Salty Rage

Wave Fist, Earth Slash, Secret Fist
Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Dispel Magic

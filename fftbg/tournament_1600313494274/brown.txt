Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tripaplex
Male
Gemini
71
69
Wizard
Charge
Auto Potion
Equip Knife
Lava Walking

Spell Edge

Leather Hat
Light Robe
Defense Ring

Fire, Fire 3, Ice 4, Empower
Charge+1, Charge+4, Charge+7, Charge+10



J MacC1991
Male
Cancer
56
57
Wizard
Punch Art
Meatbone Slash
Martial Arts
Ignore Height

Dragon Rod

Leather Hat
Power Sleeve
Bracer

Fire, Fire 2, Fire 3, Bolt 2, Bolt 4, Ice 3
Spin Fist, Pummel, Secret Fist, Purification, Seal Evil



Douchetron
Male
Aries
49
70
Calculator
Yin Yang Magic
Earplug
Martial Arts
Lava Walking



Leather Hat
Brigandine
Jade Armlet

CT, Height, 5, 3
Pray Faith, Zombie, Blind Rage, Paralyze, Dark Holy



Silentkaster
Female
Libra
52
56
Samurai
Battle Skill
Speed Save
Short Charge
Move+1

Kiyomori

Cross Helmet
Black Robe
N-Kai Armlet

Asura, Heaven's Cloud, Kiyomori
Weapon Break, Power Break, Mind Break

Player: !Green
Team: Green Team
Palettes: Green/White



Bongomon7
Male
Aries
61
70
Oracle
Throw
Meatbone Slash
Halve MP
Move+1

Papyrus Codex

Twist Headband
Wizard Robe
Magic Gauntlet

Poison, Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Shuriken, Bomb



DavenIII
Male
Capricorn
76
74
Monk
Time Magic
HP Restore
Halve MP
Jump+3

Star Bag

Green Beret
Wizard Outfit
Bracer

Secret Fist
Haste, Slow 2, Stop, Immobilize, Quick, Demi 2, Galaxy Stop



E Ballard
Female
Serpentarius
79
59
Ninja
Basic Skill
Sunken State
Short Status
Move+1

Orichalcum
Flame Whip
Flash Hat
Leather Outfit
Small Mantle

Shuriken, Bomb, Knife, Staff
Accumulate, Dash, Heal



Highelectricaltemperature
Male
Libra
58
79
Chemist
Black Magic
Catch
Sicken
Move+3

Star Bag

Black Hood
Wizard Outfit
Power Wrist

Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down
Fire, Fire 2, Bolt 2, Ice 4, Death

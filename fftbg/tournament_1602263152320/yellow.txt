Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Bruubarg
Female
Pisces
48
58
Geomancer
Charge
Meatbone Slash
Equip Axe
Move+1

Battle Axe
Round Shield
Triangle Hat
Power Sleeve
Battle Boots

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Charge+2



Exigency 44
Female
Libra
41
80
Geomancer
Steal
Parry
Sicken
Move+3

Battle Axe
Gold Shield
Thief Hat
Adaman Vest
Diamond Armlet

Water Ball, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim



RunicMagus
Male
Serpentarius
67
69
Ninja
Summon Magic
Catch
Magic Attack UP
Jump+3

Flame Whip
Hydra Bag
Holy Miter
Leather Outfit
Cursed Ring

Shuriken
Ramuh, Titan, Carbunkle, Salamander, Fairy



Resjudicata3
Female
Leo
68
52
Mime

Dragon Spirit
Equip Armor
Swim



Iron Helmet
Silk Robe
Small Mantle

Mimic


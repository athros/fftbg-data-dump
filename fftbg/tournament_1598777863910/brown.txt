Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Randgridr
Monster
Aries
54
48
Dryad










Who Lio42
Male
Virgo
38
53
Bard
Summon Magic
Auto Potion
Halve MP
Waterbreathing

Ramia Harp

Green Beret
Crystal Mail
Magic Gauntlet

Nameless Song
Shiva, Ifrit, Titan, Leviathan, Lich, Cyclops



NicoSavoy
Female
Taurus
73
43
Summoner
Elemental
Damage Split
Equip Shield
Jump+1

Bestiary
Platinum Shield
Flash Hat
Silk Robe
Feather Boots

Shiva, Ramuh, Carbunkle, Leviathan, Fairy
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Halstein
Male
Cancer
58
64
Mediator
Summon Magic
Counter Magic
Maintenance
Move+1

Mythril Gun

Holy Miter
Black Costume
Genji Gauntlet

Persuade, Praise, Preach, Insult, Refute
Moogle, Ramuh, Titan, Golem, Carbunkle, Leviathan, Salamander, Lich

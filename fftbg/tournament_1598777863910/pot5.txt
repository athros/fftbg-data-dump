Final Bets: red - 7 bets for 6,254G (71.3%, x0.40); yellow - 5 bets for 2,518G (28.7%, x2.48)

red bets:
DavenIII: 2,500G (40.0%, 16,900G)
randgridr: 1,842G (29.5%, 1,842G)
E_Ballard: 804G (12.9%, 95,760G)
BoneMiser: 577G (9.2%, 577G)
Vultuous: 231G (3.7%, 231G)
datadrivenbot: 200G (3.2%, 57,772G)
AllInBot: 100G (1.6%, 100G)

yellow bets:
CaptainAdmiralSPATULA: 1,000G (39.7%, 5,719G)
BirbBrainsBot: 827G (32.8%, 161,546G)
MemoriesofFinal: 373G (14.8%, 373G)
J2Reed: 204G (8.1%, 204G)
getthemoneyz: 114G (4.5%, 1,779,122G)

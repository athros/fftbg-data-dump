Final Bets: red - 8 bets for 5,743G (60.4%, x0.66); purple - 5 bets for 3,769G (39.6%, x1.52)

red bets:
DavenIII: 3,000G (52.2%, 17,907G)
Seaweed_B: 1,000G (17.4%, 49,418G)
E_Ballard: 804G (14.0%, 96,084G)
Vultuous: 406G (7.1%, 406G)
datadrivenbot: 200G (3.5%, 57,903G)
AllInBot: 175G (3.0%, 175G)
Absalom_20: 120G (2.1%, 9,836G)
getthemoneyz: 38G (0.7%, 1,778,820G)

purple bets:
Zachara: 1,208G (32.1%, 136,708G)
BirbBrainsBot: 1,000G (26.5%, 159,719G)
gorgewall: 842G (22.3%, 842G)
TheMurkGnome: 619G (16.4%, 619G)
Nine07: 100G (2.7%, 200G)

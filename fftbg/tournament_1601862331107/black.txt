Player: !Black
Team: Black Team
Palettes: Black/Red



Lyner87
Monster
Aries
76
45
Chocobo










StealthModeLocke
Female
Gemini
59
51
Chemist
Draw Out
Counter Flood
Doublehand
Jump+3

Hydra Bag

Triangle Hat
Earth Clothes
Feather Boots

Potion, X-Potion, Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down
Koutetsu



MoonSugarFiend
Female
Taurus
73
70
Samurai
Basic Skill
Auto Potion
Doublehand
Retreat

Asura Knife

Genji Helmet
Gold Armor
Power Wrist

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Accumulate, Throw Stone, Heal, Tickle, Cheer Up



Roofiepops
Female
Cancer
57
72
Ninja
Basic Skill
Critical Quick
Magic Defense UP
Teleport

Flail
Zorlin Shape
Twist Headband
Chain Vest
Leather Mantle

Shuriken, Bomb, Knife, Hammer, Staff, Spear, Wand
Throw Stone, Tickle, Cheer Up, Scream

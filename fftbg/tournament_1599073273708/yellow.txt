Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



E Ballard
Male
Libra
79
45
Monk
Battle Skill
Parry
Concentrate
Lava Walking



Feather Hat
Earth Clothes
Dracula Mantle

Secret Fist, Purification, Revive
Shield Break, Speed Break, Power Break



HS39
Male
Pisces
48
45
Monk
Summon Magic
Critical Quick
Equip Gun
Move-MP Up

Papyrus Codex

Headgear
Earth Clothes
Diamond Armlet

Pummel, Wave Fist, Earth Slash, Purification
Shiva, Titan, Carbunkle, Leviathan



Galkife
Female
Scorpio
80
47
Lancer
Draw Out
Counter Flood
Maintenance
Move+3

Battle Bamboo
Mythril Shield
Diamond Helmet
Crystal Mail
Bracer

Level Jump5, Vertical Jump7
Asura, Koutetsu, Kiyomori



CosmicTactician
Female
Aquarius
43
77
Squire
Battle Skill
Damage Split
Equip Knife
Retreat

Assassin Dagger
Ice Shield
Leather Hat
Brigandine
Magic Ring

Throw Stone, Heal, Tickle, Cheer Up
Weapon Break, Speed Break, Mind Break, Stasis Sword

Player: !Green
Team: Green Team
Palettes: Green/White



Aggroberserker
Female
Scorpio
47
78
Oracle
Jump
MP Restore
Beastmaster
Jump+3

Cypress Rod

Green Beret
Chameleon Robe
Battle Boots

Blind, Spell Absorb, Pray Faith, Zombie, Confusion Song, Paralyze, Dark Holy
Level Jump4, Vertical Jump7



StealthModeLocke
Monster
Sagittarius
48
73
Apanda










Brokenknight201
Female
Aquarius
80
65
Geomancer
Draw Out
Critical Quick
Defense UP
Teleport

Battle Axe
Genji Shield
Green Beret
Wizard Robe
Red Shoes

Pitfall, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Murasame, Kiyomori



Dogsandcatsand
Female
Sagittarius
78
37
Wizard
Math Skill
Counter Magic
Equip Knife
Jump+2

Cultist Dagger

Green Beret
Light Robe
Magic Ring

Bolt 2, Ice 4, Flare
CT, Height, Prime Number, 3

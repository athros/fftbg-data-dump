Player: !Red
Team: Red Team
Palettes: Red/Brown



HASTERIOUS
Male
Virgo
68
55
Lancer
Yin Yang Magic
Distribute
Equip Sword
Retreat

Masamune
Crystal Shield
Iron Helmet
Wizard Robe
Germinas Boots

Level Jump5, Vertical Jump8
Blind, Poison, Pray Faith, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Paralyze, Dark Holy



LuckyLuckLuc2
Female
Libra
74
52
Chemist
Punch Art
MP Restore
Martial Arts
Waterwalking



Leather Hat
Chain Vest
Spike Shoes

Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive



Exigency 44
Male
Pisces
45
76
Thief
Throw
Arrow Guard
Equip Bow
Jump+1

Night Killer

Leather Hat
Mythril Vest
Battle Boots

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Shuriken, Bomb



AniZero
Male
Taurus
50
44
Archer
Item
MA Save
Dual Wield
Retreat

Gastrafitis
Bow Gun
Flash Hat
Black Costume
Small Mantle

Charge+3, Charge+4, Charge+20
Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down

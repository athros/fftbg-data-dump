Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LDSkinny
Female
Leo
73
65
Oracle
Item
Distribute
Throw Item
Retreat

Musk Rod

Black Hood
White Robe
Feather Boots

Blind, Spell Absorb, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy
Potion, Hi-Potion, Hi-Ether, Phoenix Down



Krombobreaker
Male
Gemini
58
79
Ninja
Draw Out
Mana Shield
Attack UP
Retreat

Ninja Edge
Flail
Flash Hat
Brigandine
Wizard Mantle

Bomb, Staff
Koutetsu, Heaven's Cloud, Masamune



Powergems
Female
Capricorn
55
51
Wizard
Punch Art
Faith Save
Beastmaster
Teleport

Thunder Rod

Black Hood
Wizard Robe
Magic Gauntlet

Bolt 3, Bolt 4
Wave Fist, Secret Fist, Purification, Chakra, Revive



Phi Sig
Female
Libra
41
76
Calculator
Animal Skill
Caution
Magic Attack UP
Jump+1

Bestiary
Buckler
Platinum Helmet
Judo Outfit
Defense Armlet

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon

Player: !Green
Team: Green Team
Palettes: Green/White



Old Overholt
Monster
Gemini
48
43
Tiamat










Roofiepops
Male
Capricorn
44
71
Ninja
Time Magic
Critical Quick
Equip Axe
Jump+3

Rainbow Staff
Mythril Knife
Twist Headband
Earth Clothes
Small Mantle

Shuriken, Bomb, Hammer, Wand
Slow 2, Stop, Demi 2, Stabilize Time



DracoknightZero
Monster
Scorpio
59
56
Steel Giant










Technominari
Female
Taurus
69
79
Priest
Throw
Dragon Spirit
Attack UP
Fly

Gold Staff

Red Hood
Linen Robe
Rubber Shoes

Raise, Reraise, Protect 2, Shell 2, Esuna
Bomb

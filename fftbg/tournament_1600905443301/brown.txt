Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DanielRKnealeArt
Male
Gemini
51
78
Ninja
Punch Art
Mana Shield
Equip Sword
Move+1

Broad Sword
Broad Sword
Headgear
Wizard Outfit
Magic Ring

Knife
Pummel, Wave Fist, Secret Fist



ColetteMSLP
Female
Sagittarius
80
73
Oracle
Jump
Dragon Spirit
Equip Bow
Retreat

Night Killer

Triangle Hat
Leather Outfit
Cursed Ring

Poison, Spell Absorb, Pray Faith, Silence Song, Petrify
Level Jump4, Vertical Jump4



GiggleStik
Male
Virgo
63
68
Priest
Summon Magic
MA Save
Equip Polearm
Lava Walking

Octagon Rod

Leather Hat
Mystic Vest
Leather Mantle

Cure 4, Raise, Raise 2, Shell, Esuna
Leviathan, Fairy



DarrenDinosaurs
Female
Scorpio
71
78
Geomancer
Draw Out
Caution
Equip Bow
Jump+3

Snipe Bow
Mythril Shield
Barette
Brigandine
Cursed Ring

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Heaven's Cloud

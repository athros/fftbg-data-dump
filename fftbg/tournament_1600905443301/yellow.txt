Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dogsandcatsand
Female
Leo
70
69
Wizard
Elemental
Counter
Equip Polearm
Fly

Iron Fan

Red Hood
Judo Outfit
Wizard Mantle

Fire 3, Bolt 3, Bolt 4, Ice, Ice 2, Frog
Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Lava Ball



Nhammen
Female
Leo
77
53
Priest
Draw Out
Sunken State
Magic Attack UP
Retreat

Morning Star

Triangle Hat
Chain Vest
Diamond Armlet

Cure 3, Cure 4, Raise, Regen, Protect, Esuna, Holy
Kikuichimoji



0v3rr8d
Male
Sagittarius
46
55
Archer
Black Magic
Arrow Guard
Magic Defense UP
Move+3

Night Killer
Diamond Shield
Triangle Hat
Adaman Vest
Magic Gauntlet

Charge+2, Charge+3, Charge+5
Fire, Fire 2, Bolt 2, Ice, Ice 2, Empower



ArchKnightX
Male
Leo
46
59
Knight
Talk Skill
PA Save
Maintenance
Jump+2

Broad Sword
Buckler
Circlet
Mythril Armor
108 Gems

Shield Break, Weapon Break
Invitation, Persuade, Insult, Refute, Rehabilitate

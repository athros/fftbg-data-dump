Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



VolgraTheMoose
Female
Serpentarius
72
68
Thief
Time Magic
Hamedo
Sicken
Swim

Sasuke Knife

Triangle Hat
Power Sleeve
Bracer

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory
Slow 2, Float, Reflect, Stabilize Time, Galaxy Stop



Silentkaster
Male
Leo
64
50
Knight
Punch Art
Auto Potion
Dual Wield
Move+2

Giant Axe
Sleep Sword
Genji Helmet
Chameleon Robe
Magic Gauntlet

Weapon Break, Speed Break, Justice Sword, Dark Sword
Pummel, Secret Fist, Revive, Seal Evil



Lokenwow
Monster
Scorpio
58
71
Wyvern










Nok
Female
Aries
41
53
Summoner
Yin Yang Magic
Blade Grasp
Martial Arts
Lava Walking



Golden Hairpin
Light Robe
Feather Boots

Moogle, Ifrit, Golem, Carbunkle, Salamander, Silf, Fairy
Spell Absorb, Life Drain, Foxbird, Dark Holy

Final Bets: green - 14 bets for 11,208G (39.6%, x1.52); yellow - 6 bets for 17,075G (60.4%, x0.66)

green bets:
BaronHaynes: 5,000G (44.6%, 16,008G)
BirbBrainsBot: 1,000G (8.9%, 92,197G)
ser_pyrro: 800G (7.1%, 2,861G)
upvla: 777G (6.9%, 4,658G)
nifboy: 740G (6.6%, 9,588G)
LivingHitokiri: 548G (4.9%, 548G)
ninjaboy3234: 455G (4.1%, 910G)
GoatShow2: 404G (3.6%, 404G)
getthemoneyz: 372G (3.3%, 1,931,337G)
SephDarkheart: 324G (2.9%, 324G)
ValensEXP_: 288G (2.6%, 7,428G)
gongonono: 200G (1.8%, 3,943G)
datadrivenbot: 200G (1.8%, 62,950G)
BartTradingCompany: 100G (0.9%, 8,871G)

yellow bets:
run_with_stone_GUNs: 12,031G (70.5%, 24,062G)
Leonidusx: 2,000G (11.7%, 10,042G)
silentkaster: 1,390G (8.1%, 1,390G)
VolgraTheMoose: 748G (4.4%, 748G)
Raixelol: 600G (3.5%, 17,910G)
AllInBot: 306G (1.8%, 306G)

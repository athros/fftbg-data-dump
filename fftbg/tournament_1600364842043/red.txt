Player: !Red
Team: Red Team
Palettes: Red/Brown



Gelwain
Female
Aquarius
67
49
Time Mage
Throw
MP Restore
Equip Bow
Lava Walking

Snipe Bow

Flash Hat
Power Sleeve
Magic Gauntlet

Slow, Float, Quick, Stabilize Time
Knife



Douchetron
Monster
Serpentarius
51
80
Porky










CosmicTactician
Female
Virgo
42
54
Time Mage
Talk Skill
Counter Magic
Defense UP
Fly

White Staff

Holy Miter
Earth Clothes
Feather Boots

Stop, Reflect, Stabilize Time
Persuade



Biske13
Female
Gemini
44
51
Geomancer
Draw Out
Catch
Doublehand
Move+3

Battle Axe

Black Hood
Wizard Outfit
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Asura, Koutetsu, Murasame, Heaven's Cloud

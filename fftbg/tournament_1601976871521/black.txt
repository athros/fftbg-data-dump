Player: !Black
Team: Black Team
Palettes: Black/Red



Reddwind
Male
Aquarius
76
66
Knight
Yin Yang Magic
Catch
Halve MP
Waterbreathing

Giant Axe
Crystal Shield
Bronze Helmet
Linen Cuirass
N-Kai Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword
Blind, Poison, Silence Song, Paralyze



Khelor
Monster
Virgo
56
47
Malboro










NicoSavoy
Male
Capricorn
68
70
Mime

Hamedo
Martial Arts
Levitate



Holy Miter
Brigandine
Bracer

Mimic




Nicolina
Female
Virgo
74
51
Geomancer
Throw
Regenerator
Short Status
Waterbreathing

Broad Sword
Platinum Shield
Feather Hat
Black Costume
Feather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shuriken, Knife

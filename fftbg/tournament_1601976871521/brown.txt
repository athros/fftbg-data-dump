Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lolthsmeat
Male
Taurus
80
77
Lancer
Draw Out
Regenerator
Doublehand
Teleport

Gokuu Rod

Bronze Helmet
Light Robe
Rubber Shoes

Level Jump5, Vertical Jump8
Asura, Koutetsu, Heaven's Cloud, Muramasa



Smallboops
Female
Virgo
44
74
Dancer
Charge
Distribute
Martial Arts
Move-HP Up

Persia

Red Hood
Secret Clothes
Power Wrist

Slow Dance, Void Storage
Charge+1, Charge+5



Lijarkh
Female
Capricorn
77
79
Monk
Draw Out
Caution
Defend
Move+1



Triangle Hat
Mystic Vest
Battle Boots

Earth Slash, Secret Fist, Chakra, Revive
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa



Ruddie
Male
Gemini
45
69
Mediator
Punch Art
Earplug
Equip Bow
Waterwalking

Poison Bow

Golden Hairpin
Wizard Robe
Feather Mantle

Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Purification

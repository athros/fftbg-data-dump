Player: !Red
Team: Red Team
Palettes: Red/Brown



Fenaen
Female
Scorpio
57
43
Geomancer
Basic Skill
Faith Save
Martial Arts
Fly


Flame Shield
Leather Hat
Light Robe
Leather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Heal, Tickle



Old Overholt
Male
Sagittarius
77
47
Lancer
Steal
Mana Shield
Dual Wield
Retreat

Javelin
Javelin
Iron Helmet
Chain Mail
Germinas Boots

Level Jump4, Vertical Jump7
Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory



FriendSquirrel
Female
Libra
69
46
Dancer
Draw Out
Critical Quick
Magic Attack UP
Levitate

Hydra Bag

Flash Hat
Clothes
Defense Ring

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Void Storage
Koutetsu, Bizen Boat, Murasame, Kiyomori



CrazyLou64
Male
Virgo
41
56
Ninja
Item
Hamedo
Concentrate
Ignore Height

Sasuke Knife
Short Edge
Holy Miter
Wizard Outfit
Chantage

Wand
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass

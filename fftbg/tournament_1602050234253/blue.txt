Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CorpusCav
Male
Virgo
77
74
Samurai
Punch Art
Counter Flood
Doublehand
Fly

Heaven's Cloud

Crystal Helmet
Linen Cuirass
Germinas Boots

Kiyomori
Revive, Seal Evil



Ribbiks
Female
Libra
64
48
Squire
Dance
Sunken State
Doublehand
Levitate

Battle Axe

Black Hood
Black Costume
Power Wrist

Dash, Throw Stone, Heal, Cheer Up, Ultima
Wiznaibus, Slow Dance, Disillusion, Nether Demon, Dragon Pit



RunicMagus
Female
Leo
38
72
Ninja
Steal
Auto Potion
Short Charge
Fly

Morning Star
Morning Star
Flash Hat
Brigandine
Red Shoes

Axe
Steal Heart, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



Victoriolue
Female
Pisces
65
61
Ninja
Charge
Auto Potion
Equip Gun
Teleport

Bestiary
Bestiary
Black Hood
Mystic Vest
Jade Armlet

Knife
Charge+1, Charge+2

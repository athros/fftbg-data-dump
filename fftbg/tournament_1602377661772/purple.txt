Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evewho
Female
Scorpio
51
47
Priest
Steal
Hamedo
Short Charge
Waterbreathing

Wizard Staff

Feather Hat
Linen Robe
Feather Mantle

Cure, Cure 4, Raise, Regen, Protect, Shell, Wall, Esuna
Steal Helmet, Steal Shield, Steal Status



DrAntiSocial
Female
Aries
66
55
Time Mage
Basic Skill
Mana Shield
Defense UP
Ignore Terrain

Oak Staff

Twist Headband
Earth Clothes
Small Mantle

Haste, Slow, Slow 2, Stop, Immobilize, Stabilize Time, Meteor
Accumulate, Heal, Tickle, Yell



SealPoulpe
Female
Cancer
56
44
Ninja
Draw Out
Counter
Short Status
Move+3

Ninja Edge
Morning Star
Twist Headband
Black Costume
Feather Boots

Shuriken, Staff, Dictionary
Bizen Boat, Heaven's Cloud, Kikuichimoji



Sairentozon7
Female
Scorpio
52
59
Dancer
Battle Skill
Dragon Spirit
Short Status
Move+2

Ryozan Silk

Feather Hat
Leather Outfit
Spike Shoes

Nameless Dance, Nether Demon
Head Break, Weapon Break, Dark Sword

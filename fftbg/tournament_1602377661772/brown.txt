Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MelTorefas
Female
Scorpio
55
67
Oracle
Black Magic
Damage Split
Beastmaster
Jump+1

Battle Bamboo

Golden Hairpin
Linen Robe
Setiemson

Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Sleep
Fire, Fire 2, Bolt, Bolt 2, Ice, Ice 4, Flare



RunicMagus
Male
Aries
70
75
Knight
Charge
HP Restore
Equip Bow
Jump+2

Night Killer
Aegis Shield
Gold Helmet
Plate Mail
Jade Armlet

Head Break, Weapon Break, Speed Break, Mind Break, Stasis Sword, Surging Sword
Charge+3



Run With Stone GUNs
Monster
Scorpio
44
54
Wyvern










Squideater
Male
Leo
55
75
Summoner
Elemental
HP Restore
Doublehand
Lava Walking

Rod

Barette
Silk Robe
Germinas Boots

Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Silf, Fairy, Cyclops
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball

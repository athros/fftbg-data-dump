Player: !White
Team: White Team
Palettes: White/Blue



Smugzug
Female
Taurus
64
53
Geomancer
Talk Skill
Faith Save
Equip Bow
Swim

Ultimus Bow

Twist Headband
Wizard Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Refute



SephDarkheart
Male
Leo
46
62
Geomancer
Basic Skill
Counter
Equip Bow
Move+3

Snipe Bow
Flame Shield
Green Beret
Mystic Vest
Jade Armlet

Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind
Dash, Throw Stone, Heal, Scream



Inosukeanu
Male
Gemini
53
70
Wizard
Yin Yang Magic
Counter
Beastmaster
Retreat

Thunder Rod

Thief Hat
Mystic Vest
N-Kai Armlet

Fire, Fire 2, Fire 3, Fire 4, Ice, Ice 2, Ice 3, Ice 4, Empower
Blind, Pray Faith, Foxbird, Confusion Song, Paralyze, Sleep



Lowlf
Female
Capricorn
53
52
Oracle
Draw Out
Catch
Equip Gun
Move-HP Up

Fairy Harp

Flash Hat
Leather Outfit
Bracer

Blind, Poison, Doubt Faith, Silence Song, Dispel Magic, Sleep
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa, Masamune

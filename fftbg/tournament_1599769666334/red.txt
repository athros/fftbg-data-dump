Player: !Red
Team: Red Team
Palettes: Red/Brown



FriendSquirrel
Female
Sagittarius
45
39
Dancer
Throw
Blade Grasp
Equip Polearm
Jump+1

Ivory Rod

Holy Miter
Power Sleeve
Jade Armlet

Witch Hunt, Slow Dance, Polka Polka, Void Storage
Shuriken, Stick, Dictionary



W0RLDEND
Female
Cancer
76
63
Monk
Yin Yang Magic
Damage Split
Defend
Move-MP Up



Holy Miter
Chain Vest
Sprint Shoes

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Blind, Poison, Pray Faith, Zombie, Silence Song, Dispel Magic, Paralyze



RuneS 77
Female
Taurus
57
46
Monk
Elemental
Catch
Magic Attack UP
Waterwalking

Panther Bag

Leather Hat
Adaman Vest
Feather Mantle

Wave Fist, Earth Slash, Secret Fist, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



HASTERIOUS
Female
Sagittarius
58
43
Geomancer
Draw Out
Sunken State
Equip Bow
Waterwalking

Poison Bow
Escutcheon
Flash Hat
Chameleon Robe
Dracula Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Kikuichimoji

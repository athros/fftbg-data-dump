Final Bets: red - 7 bets for 5,371G (49.7%, x1.01); blue - 11 bets for 5,435G (50.3%, x0.99)

red bets:
Smashy: 4,222G (78.6%, 4,222G)
getthemoneyz: 276G (5.1%, 1,903,213G)
ValensEXP_: 256G (4.8%, 5,034G)
mindblownnnn: 216G (4.0%, 216G)
gongonono: 200G (3.7%, 5,240G)
gorgewall: 101G (1.9%, 4,183G)
AllInBot: 100G (1.9%, 100G)

blue bets:
snkey: 1,124G (20.7%, 11,240G)
BirbBrainsBot: 1,000G (18.4%, 194,289G)
Thyrandaal: 910G (16.7%, 910G)
Zachara: 500G (9.2%, 132,500G)
loveyouallfriends: 435G (8.0%, 435G)
bigbongsmoker: 316G (5.8%, 2,990G)
SephDarkheart: 300G (5.5%, 1,750G)
CosmicTactician: 300G (5.5%, 30,084G)
projekt_mike: 250G (4.6%, 4,946G)
datadrivenbot: 200G (3.7%, 58,743G)
BartTradingCompany: 100G (1.8%, 286G)

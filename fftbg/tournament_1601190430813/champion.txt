Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Mirapoix
Female
Sagittarius
80
67
Calculator
Lucavi Skill
Sunken State
Long Status
Jump+1

Papyrus Codex
Gold Shield
Cross Helmet
Genji Armor
Vanish Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



IBluntDouble
Monster
Virgo
65
40
Steel Giant










Firesheath
Monster
Gemini
66
38
Taiju










JonnyCue
Female
Taurus
75
75
Mediator
Elemental
Counter Flood
Long Status
Teleport

Blaze Gun

Holy Miter
Light Robe
Jade Armlet

Invitation, Persuade, Threaten, Preach, Death Sentence, Insult, Mimic Daravon, Refute
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Lava Ball

Final Bets: red - 16 bets for 10,373G (53.9%, x0.86); blue - 19 bets for 8,882G (46.1%, x1.17)

red bets:
Zeroroute: 1,386G (13.4%, 1,386G)
gorgewall: 1,189G (11.5%, 1,189G)
killth3kid: 1,124G (10.8%, 1,124G)
thunderducker: 1,100G (10.6%, 1,100G)
BirbBrainsBot: 1,000G (9.6%, 15,343G)
Sairentozon7: 1,000G (9.6%, 46,974G)
JumbocactuarX27: 1,000G (9.6%, 8,414G)
dogsandcatsand: 692G (6.7%, 5,755G)
roofiepops: 474G (4.6%, 2,477G)
douchetron: 404G (3.9%, 404G)
Shalloween: 300G (2.9%, 9,302G)
otakutaylor: 200G (1.9%, 2,725G)
ericzubat: 200G (1.9%, 22,487G)
LightSCT: 104G (1.0%, 104G)
Firesheath: 100G (1.0%, 3,577G)
Thyrandaal: 100G (1.0%, 110,425G)

blue bets:
reinoe: 2,000G (22.5%, 20,091G)
LDSkinny: 1,000G (11.3%, 18,658G)
Nizaha: 868G (9.8%, 69,598G)
nifboy: 816G (9.2%, 10,359G)
nekojin: 588G (6.6%, 588G)
resjudicata3: 573G (6.5%, 573G)
Smashy: 572G (6.4%, 572G)
Rytor: 516G (5.8%, 516G)
CorpusCav: 500G (5.6%, 5,449G)
ValensEXP_: 360G (4.1%, 8,704G)
AllInBot: 200G (2.3%, 200G)
datadrivenbot: 200G (2.3%, 71,748G)
ribbiks: 200G (2.3%, 5,477G)
VolgraTheMoose: 150G (1.7%, 7,777G)
maakur_: 100G (1.1%, 6,600G)
RunicMagus: 100G (1.1%, 5,131G)
skipsandwiches: 100G (1.1%, 24,982G)
getthemoneyz: 38G (0.4%, 2,028,480G)
latebit: 1G (0.0%, 4,200G)

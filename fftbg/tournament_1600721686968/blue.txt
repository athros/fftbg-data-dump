Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LDSkinny
Female
Gemini
49
75
Oracle
Time Magic
Counter
Dual Wield
Move+3

Bestiary
Bestiary
Red Hood
Chain Vest
Cursed Ring

Blind, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Foxbird
Haste, Slow 2, Stop, Reflect, Demi, Stabilize Time



Nekojin
Male
Scorpio
48
67
Lancer
Elemental
Faith Save
Dual Wield
Move+2

Javelin
Partisan
Mythril Helmet
Reflect Mail
Genji Gauntlet

Level Jump5, Vertical Jump2
Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



LightSCT
Female
Capricorn
63
61
Chemist
Elemental
Counter Magic
Halve MP
Ignore Terrain

Orichalcum

Green Beret
Power Sleeve
Bracer

Potion, Echo Grass, Maiden's Kiss, Phoenix Down
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Rytor
Female
Pisces
55
64
Squire
Punch Art
PA Save
Equip Axe
Move-HP Up

Gold Staff
Bronze Shield
Triangle Hat
Judo Outfit
Feather Boots

Accumulate, Heal, Yell, Scream
Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive

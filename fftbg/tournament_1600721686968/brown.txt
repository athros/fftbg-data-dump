Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MemoriesofFinal
Female
Scorpio
70
63
Time Mage
Jump
Catch
Attack UP
Move-MP Up

Octagon Rod

Leather Hat
White Robe
Elf Mantle

Haste, Slow, Slow 2, Stop, Float, Reflect, Demi 2, Stabilize Time
Level Jump5, Vertical Jump3



DavenIII
Male
Libra
61
77
Archer
Talk Skill
Mana Shield
Equip Bow
Move-MP Up

Cute Bag
Flame Shield
Twist Headband
Judo Outfit
Diamond Armlet

Charge+1, Charge+5, Charge+10
Threaten, Preach, Refute



Ericzubat
Female
Pisces
49
44
Knight
Dance
HP Restore
Defend
Waterwalking

Defender
Genji Shield
Crystal Helmet
White Robe
Reflect Ring

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword, Dark Sword, Night Sword
Polka Polka, Disillusion, Last Dance, Obsidian Blade, Void Storage



Daveb
Male
Cancer
69
58
Ninja
Time Magic
Counter
Defend
Waterbreathing

Flail
Flame Whip
Holy Miter
Judo Outfit
Small Mantle

Axe
Haste 2, Immobilize, Demi, Demi 2, Stabilize Time

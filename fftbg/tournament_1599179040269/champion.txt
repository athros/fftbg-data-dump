Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Yangusburger
Male
Leo
46
67
Squire
Elemental
Damage Split
Dual Wield
Retreat

Giant Axe
Rune Blade
Cross Helmet
Linen Cuirass
Red Shoes

Heal, Cheer Up, Fury
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Just Here2
Male
Virgo
61
61
Mediator
White Magic
Counter Tackle
Concentrate
Jump+1

Bestiary

Headgear
Wizard Outfit
Bracer

Persuade, Praise, Threaten, Preach, Solution, Insult, Refute
Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell, Magic Barrier



Baconbacon1207
Male
Cancer
46
59
Ninja
Item
Abandon
Equip Gun
Move-HP Up

Madlemgen
Bestiary
Feather Hat
Power Sleeve
Feather Mantle

Shuriken, Knife, Ninja Sword, Wand
Potion, X-Potion, Antidote, Maiden's Kiss, Soft, Phoenix Down



Sinnyil2
Male
Leo
80
52
Mime

Caution
Martial Arts
Move+3



Feather Hat
Wizard Outfit
Magic Gauntlet

Mimic


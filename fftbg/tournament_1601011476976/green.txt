Player: !Green
Team: Green Team
Palettes: Green/White



WhiteTigress
Female
Libra
57
80
Calculator
Time Magic
Counter Tackle
Concentrate
Move+3

Musk Rod

Thief Hat
Chain Vest
108 Gems

CT, Height, Prime Number, 4, 3
Haste, Haste 2, Slow, Slow 2, Immobilize, Stabilize Time, Meteor



DeathTaxesAndAnime
Female
Sagittarius
66
71
Thief
Basic Skill
Critical Quick
Equip Gun
Waterbreathing

Blast Gun

Green Beret
Chain Vest
Angel Ring

Steal Shield, Steal Status
Accumulate, Heal, Tickle, Cheer Up



Mindblownnnn
Male
Aries
53
61
Archer
Talk Skill
Damage Split
Dual Wield
Levitate

Ultimus Bow

Twist Headband
Judo Outfit
Feather Boots

Charge+7, Charge+10
Persuade, Solution, Insult, Negotiate, Refute



ValensEXP
Male
Pisces
78
79
Knight
Charge
MP Restore
Short Charge
Lava Walking

Slasher
Bronze Shield
Crystal Helmet
Genji Armor
Sprint Shoes

Shield Break, Speed Break, Power Break, Mind Break, Justice Sword
Charge+2, Charge+10, Charge+20

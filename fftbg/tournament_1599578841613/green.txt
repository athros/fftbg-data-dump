Player: !Green
Team: Green Team
Palettes: Green/White



Run With Stone GUNs
Male
Gemini
73
58
Ninja
Yin Yang Magic
Sunken State
Equip Gun
Waterwalking

Fairy Harp
Bestiary
Feather Hat
Mystic Vest
Setiemson

Dictionary
Pray Faith, Foxbird, Dispel Magic, Sleep



Just Here2
Male
Sagittarius
60
74
Samurai
Steal
Critical Quick
Long Status
Teleport

Kikuichimoji

Cross Helmet
Mythril Armor
Genji Gauntlet

Bizen Boat, Heaven's Cloud
Steal Helmet, Steal Armor, Steal Weapon, Steal Status



Macradarkstar
Female
Virgo
70
70
Lancer
Yin Yang Magic
Critical Quick
Long Status
Jump+3

Partisan
Escutcheon
Bronze Helmet
Chain Mail
Diamond Armlet

Level Jump8, Vertical Jump7
Pray Faith, Dispel Magic, Sleep, Dark Holy



Powergems
Male
Capricorn
46
52
Ninja
Summon Magic
Absorb Used MP
Equip Gun
Ignore Terrain

Battle Folio
Bestiary
Red Hood
Earth Clothes
Angel Ring

Shuriken, Bomb
Moogle, Titan

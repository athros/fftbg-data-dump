Player: !Black
Team: Black Team
Palettes: Black/Red



DLJuggernaut
Male
Cancer
57
62
Oracle
Jump
Counter Tackle
Equip Sword
Move+1

Defender

Golden Hairpin
Wizard Outfit
Genji Gauntlet

Life Drain, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep, Dark Holy
Level Jump8, Vertical Jump8



Skillomono
Male
Taurus
63
44
Archer
Draw Out
Critical Quick
Doublehand
Waterwalking

Bow Gun

Feather Hat
Mystic Vest
Diamond Armlet

Charge+10, Charge+20
Asura, Koutetsu, Bizen Boat, Kiyomori



FoeSquirrel
Female
Sagittarius
62
42
Squire
Talk Skill
Absorb Used MP
Magic Attack UP
Move-MP Up

Platinum Sword
Platinum Shield
Cross Helmet
Crystal Mail
Vanish Mantle

Dash, Heal, Tickle, Yell, Cheer Up, Fury
Invitation, Praise, Threaten, Rehabilitate



Captainmilestw
Female
Scorpio
45
66
Wizard
Item
Absorb Used MP
Beastmaster
Move+1

Poison Rod

Leather Hat
White Robe
Cursed Ring

Fire 4, Bolt, Empower, Frog
Hi-Ether, Maiden's Kiss, Phoenix Down

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



YowaneHaku
Female
Aquarius
69
75
Calculator
Marsh Skill
Catch
Magic Defense UP
Waterwalking

Battle Folio
Flame Shield
Barbuta
Reflect Mail
Magic Gauntlet

Blue Magic
Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



Lord Gwarth
Male
Pisces
80
47
Ninja
Elemental
Meatbone Slash
Equip Gun
Move+1

Papyrus Codex
Bestiary
Twist Headband
Earth Clothes
Magic Ring

Shuriken
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard



Fenaen
Female
Virgo
70
49
Calculator
Limit
Parry
Equip Knife
Retreat

Kunai

Leather Helmet
Silk Robe
Genji Gauntlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



MoonSugarFiend
Male
Capricorn
56
52
Monk
Item
Hamedo
Equip Polearm
Waterbreathing

Gokuu Rod

Green Beret
Mystic Vest
Defense Armlet

Earth Slash, Purification, Revive, Seal Evil
Potion, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ValensEXP
Female
Cancer
73
74
Time Mage
Yin Yang Magic
Counter Tackle
Concentrate
Waterwalking

Oak Staff

Triangle Hat
White Robe
Wizard Mantle

Haste, Slow 2, Stop, Reflect
Blind, Poison, Zombie, Silence Song, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy



CorpusCav
Male
Cancer
63
42
Samurai
Jump
Earplug
Equip Axe
Retreat

Giant Axe

Circlet
Black Robe
Spike Shoes

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Level Jump5, Vertical Jump3



Sharosa
Male
Taurus
70
79
Mime

HP Restore
Martial Arts
Teleport


Aegis Shield
Flash Hat
Chain Vest
Dracula Mantle

Mimic




Zachara
Female
Capricorn
70
75
Geomancer
Draw Out
Dragon Spirit
Halve MP
Jump+1

Muramasa
Flame Shield
Cachusha
Light Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Asura, Kiyomori, Kikuichimoji

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lowlf
Female
Cancer
62
66
Oracle
White Magic
Counter Flood
Doublehand
Teleport

Cypress Rod

Feather Hat
Power Sleeve
Reflect Ring

Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Petrify
Cure, Cure 3, Cure 4, Raise, Reraise, Regen, Shell 2, Esuna



Gelwain
Female
Aries
62
59
Mediator
Item
Regenerator
Throw Item
Teleport

Blast Gun

Feather Hat
Light Robe
Germinas Boots

Invitation, Persuade, Threaten, Preach, Death Sentence, Mimic Daravon, Refute
Ether, Hi-Ether, Eye Drop, Echo Grass, Remedy



CosmicTactician
Male
Capricorn
52
59
Time Mage
Sing
Counter Flood
Magic Attack UP
Fly

Iron Fan

Headgear
Robe of Lords
Feather Boots

Haste, Quick, Demi 2, Stabilize Time
Battle Song, Nameless Song



TheBrett
Male
Leo
38
57
Mediator
Steal
Catch
Dual Wield
Move+2

Papyrus Codex
Battle Folio
Thief Hat
White Robe
Leather Mantle

Invitation, Preach, Solution, Rehabilitate
Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Leg Aim

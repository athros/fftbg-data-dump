Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Female
Aries
68
42
Time Mage
Elemental
Damage Split
Equip Sword
Jump+3

Mythril Sword

Leather Hat
Chameleon Robe
Sprint Shoes

Haste, Slow 2, Stop, Demi, Stabilize Time
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



E Ballard
Female
Leo
78
56
Oracle
Summon Magic
Regenerator
Defense UP
Ignore Terrain

Musk Rod

Flash Hat
Earth Clothes
Germinas Boots

Blind, Zombie, Confusion Song, Paralyze, Sleep
Moogle, Ramuh, Golem, Odin, Leviathan, Fairy



Helllyesss
Monster
Pisces
49
54
Skeleton










Lijarkh
Male
Aries
81
65
Monk
Battle Skill
MP Restore
Attack UP
Move+3



Black Hood
Wizard Outfit
Sprint Shoes

Pummel, Earth Slash, Revive
Power Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword

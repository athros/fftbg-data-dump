Final Bets: white - 8 bets for 3,396G (36.3%, x1.75); brown - 7 bets for 5,947G (63.7%, x0.57)

white bets:
Sharosa: 1,313G (38.7%, 1,313G)
DirkC_Fenrir84: 500G (14.7%, 1,831G)
randgridr: 500G (14.7%, 4,228G)
Deeps_one: 400G (11.8%, 2,820G)
TheBrett: 268G (7.9%, 5,988G)
datadrivenbot: 200G (5.9%, 82,408G)
seppu777: 111G (3.3%, 496G)
bdb_goldenleo: 104G (3.1%, 104G)

brown bets:
b0lt: 3,508G (59.0%, 3,508G)
BirbBrainsBot: 1,000G (16.8%, 9,746G)
IaibOaob: 678G (11.4%, 23,026G)
getthemoneyz: 250G (4.2%, 2,141,567G)
AllInBot: 200G (3.4%, 200G)
resjudicata3: 200G (3.4%, 1,425G)
PantherIscariot: 111G (1.9%, 2,490G)

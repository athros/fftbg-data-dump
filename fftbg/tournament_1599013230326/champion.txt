Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Ruleof5
Female
Sagittarius
39
58
Archer
Item
MA Save
Dual Wield
Move+3

Mythril Bow

Diamond Helmet
Clothes
Angel Ring

Charge+1
Potion, Hi-Ether, Eye Drop, Phoenix Down



HaateXIII
Female
Cancer
82
44
Summoner
Charge
Counter
Halve MP
Lava Walking

Papyrus Codex

Twist Headband
Leather Outfit
Dracula Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle, Odin, Salamander, Silf, Lich
Charge+1, Charge+3



DeathTaxesAndAnime
Female
Leo
80
62
Mediator
Yin Yang Magic
Arrow Guard
Equip Armor
Waterbreathing

Blast Gun

Circlet
Linen Cuirass
Salty Rage

Persuade, Praise, Preach, Refute, Rehabilitate
Poison, Life Drain, Zombie, Foxbird, Confusion Song, Paralyze



Lord Gwarth
Male
Leo
70
79
Lancer
Yin Yang Magic
Counter Magic
Doublehand
Move-HP Up

Obelisk

Grand Helmet
Gold Armor
Red Shoes

Level Jump4, Vertical Jump8
Spell Absorb, Silence Song, Foxbird, Paralyze, Sleep, Petrify

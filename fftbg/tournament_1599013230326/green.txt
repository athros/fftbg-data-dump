Player: !Green
Team: Green Team
Palettes: Green/White



DAC169
Female
Libra
78
61
Calculator
Yin Yang Magic
Auto Potion
Equip Knife
Move+3

Assassin Dagger

Twist Headband
Light Robe
Red Shoes

CT, Height, 5, 4, 3
Poison, Spell Absorb, Dispel Magic, Paralyze, Sleep



Spartan Paladin
Male
Virgo
67
76
Knight
White Magic
Counter
Equip Gun
Jump+1

Bestiary
Diamond Shield
Circlet
Black Robe
Leather Mantle

Head Break, Shield Break, Speed Break, Mind Break
Cure 3, Cure 4, Reraise, Shell 2, Esuna



NicoSavoy
Female
Capricorn
77
75
Thief
Draw Out
Counter
Magic Attack UP
Ignore Terrain

Orichalcum

Headgear
Adaman Vest
Small Mantle

Steal Shield, Steal Accessory, Steal Status, Arm Aim
Koutetsu, Muramasa, Kikuichimoji, Masamune



Smashy
Male
Aries
77
62
Knight
Item
Earplug
Throw Item
Retreat

Save the Queen
Gold Shield
Leather Helmet
Carabini Mail
108 Gems

Armor Break, Shield Break, Weapon Break, Power Break, Justice Sword
Potion, X-Potion, Ether, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down

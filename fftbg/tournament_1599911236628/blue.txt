Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ayeayex3
Female
Libra
54
60
Time Mage
Yin Yang Magic
Regenerator
Defense UP
Move+1

Ivory Rod

Green Beret
Judo Outfit
Cursed Ring

Haste, Haste 2, Slow 2, Stop
Blind, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Resjudicata3
Female
Sagittarius
77
74
Squire
Dance
Counter
Equip Gun
Ignore Terrain

Madlemgen
Genji Shield
Feather Hat
Leather Outfit
Defense Armlet

Throw Stone, Heal, Yell, Cheer Up, Fury
Last Dance, Obsidian Blade



EizanTayama
Female
Aries
80
44
Archer
Elemental
Mana Shield
Defend
Waterwalking

Long Bow

Triangle Hat
Earth Clothes
Defense Armlet

Charge+2, Charge+4, Charge+5, Charge+7, Charge+10
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Ogema TheDream
Male
Taurus
59
40
Mime

Counter
Sicken
Move-HP Up



Feather Hat
Power Sleeve
Battle Boots

Mimic


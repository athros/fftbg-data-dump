Player: !White
Team: White Team
Palettes: White/Blue



MemoriesofFinal
Female
Virgo
59
39
Priest
Item
Counter
Equip Shield
Retreat

Gold Staff
Gold Shield
Golden Hairpin
Adaman Vest
Germinas Boots

Cure, Cure 2, Cure 3, Raise, Reraise, Protect, Esuna, Magic Barrier
Hi-Potion, Remedy, Phoenix Down



Just Here2
Female
Sagittarius
44
73
Chemist
Black Magic
Parry
Defend
Move+1

Panther Bag

Green Beret
Earth Clothes
Sprint Shoes

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Empower, Frog



Moshyhero
Male
Virgo
44
45
Mediator
Throw
Mana Shield
Defend
Move+2

Madlemgen

Flash Hat
Leather Outfit
Leather Mantle

Persuade, Threaten, Preach, Solution, Rehabilitate
Bomb



BaronHaynes
Male
Virgo
62
42
Lancer
Throw
Caution
Short Charge
Jump+3

Mythril Spear
Round Shield
Leather Helmet
Platinum Armor
Battle Boots

Level Jump8, Vertical Jump2
Shuriken, Bomb

Final Bets: red - 7 bets for 3,604G (32.5%, x2.07); yellow - 12 bets for 7,472G (67.5%, x0.48)

red bets:
BirbBrainsBot: 1,000G (27.7%, 75,111G)
snkey: 791G (21.9%, 7,916G)
VolgraTheMoose: 732G (20.3%, 732G)
Raixelol: 600G (16.6%, 12,335G)
brenogarwin: 300G (8.3%, 300G)
grishhammer: 100G (2.8%, 1,136G)
TheDapperChangeling: 81G (2.2%, 6,833G)

yellow bets:
Leonidusx: 4,961G (66.4%, 4,961G)
E_Ballard: 664G (8.9%, 664G)
upvla: 500G (6.7%, 500G)
ColetteMSLP: 400G (5.4%, 7,513G)
datadrivenbot: 200G (2.7%, 59,250G)
CosmicTactician: 200G (2.7%, 31,666G)
gorgewall: 101G (1.4%, 7,102G)
BartTradingCompany: 100G (1.3%, 5,368G)
AllInBot: 100G (1.3%, 100G)
MemoriesofFinal: 100G (1.3%, 6,864G)
BaronHaynes: 100G (1.3%, 22,542G)
getthemoneyz: 46G (0.6%, 1,914,533G)

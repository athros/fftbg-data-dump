Final Bets: white - 10 bets for 3,676G (33.4%, x1.99); black - 8 bets for 7,333G (66.6%, x0.50)

white bets:
VolgraTheMoose: 864G (23.5%, 11,264G)
resjudicata3: 660G (18.0%, 660G)
Blakelmenakle: 500G (13.6%, 13,468G)
CorpusCav: 400G (10.9%, 12,085G)
bigbongsmoker: 348G (9.5%, 11,347G)
CapnChaos12: 300G (8.2%, 5,486G)
AllInBot: 200G (5.4%, 200G)
datadrivenbot: 200G (5.4%, 75,184G)
wiznaibusthis: 104G (2.8%, 104G)
DeathTaxesAndAnime: 100G (2.7%, 1,786G)

black bets:
MemoriesofFinal: 2,000G (27.3%, 4,604G)
HorusTaurus: 2,000G (27.3%, 5,800G)
BirbBrainsBot: 1,000G (13.6%, 173,955G)
mindblownnnn: 789G (10.8%, 789G)
gorgewall: 776G (10.6%, 776G)
getthemoneyz: 368G (5.0%, 2,088,724G)
ColetteMSLP: 300G (4.1%, 6,621G)
Belkra: 100G (1.4%, 1,081G)

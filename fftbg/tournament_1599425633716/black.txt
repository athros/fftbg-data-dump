Player: !Black
Team: Black Team
Palettes: Black/Red



Laserman1000
Male
Scorpio
44
42
Mime

Hamedo
Magic Defense UP
Move+3



Twist Headband
Earth Clothes
Small Mantle

Mimic




Silentkaster
Female
Sagittarius
66
59
Archer
Black Magic
Faith Save
Halve MP
Jump+3

Night Killer
Flame Shield
Red Hood
Brigandine
Germinas Boots

Charge+1, Charge+3, Charge+7
Fire 3, Ice 2, Empower, Death



Douchetron
Female
Aquarius
47
76
Oracle
Battle Skill
Blade Grasp
Secret Hunt
Move-MP Up

Hydra Bag

Golden Hairpin
Linen Robe
Dracula Mantle

Poison, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic
Armor Break, Shield Break, Mind Break



MMagnetMan
Male
Aries
62
80
Thief
Basic Skill
HP Restore
Dual Wield
Teleport

Orichalcum
Orichalcum
Feather Hat
Brigandine
Magic Gauntlet

Gil Taking, Steal Helmet, Steal Armor, Arm Aim, Leg Aim
Throw Stone, Heal, Tickle, Yell, Wish

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



OneHundredFists
Female
Scorpio
78
49
Thief
Throw
Brave Save
Equip Sword
Ignore Height

Broad Sword

Thief Hat
Adaman Vest
Jade Armlet

Gil Taking, Steal Shield, Steal Accessory, Arm Aim
Shuriken, Knife



Solomongrundy85
Male
Cancer
54
70
Ninja
Charge
Counter Tackle
Attack UP
Lava Walking

Morning Star
Mage Masher
Green Beret
Black Costume
Magic Gauntlet

Shuriken, Bomb, Knife
Charge+5, Charge+7



Reinoe
Female
Virgo
48
76
Squire
Time Magic
Regenerator
Martial Arts
Move+1

Giant Axe
Diamond Shield
Thief Hat
Secret Clothes
Diamond Armlet

Accumulate, Throw Stone, Heal, Fury
Haste 2, Stop, Immobilize, Stabilize Time



Deathmaker06
Male
Aries
56
47
Thief
Throw
Abandon
Equip Shield
Ignore Terrain

Dagger
Round Shield
Twist Headband
Clothes
Small Mantle

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Shuriken, Axe, Wand, Dictionary

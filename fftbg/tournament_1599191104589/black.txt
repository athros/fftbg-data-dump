Player: !Black
Team: Black Team
Palettes: Black/Red



Rytor
Female
Cancer
58
72
Wizard
Basic Skill
Brave Save
Defend
Move-MP Up

Poison Rod

Leather Hat
Clothes
Feather Mantle

Fire, Fire 2, Ice, Empower, Frog
Dash, Heal, Wish



Thunderducker
Male
Pisces
65
74
Summoner
Basic Skill
Damage Split
Short Charge
Jump+1

Rainbow Staff

Thief Hat
Chameleon Robe
Reflect Ring

Ramuh, Golem, Salamander, Fairy, Cyclops
Scream



ANFz
Male
Cancer
68
71
Geomancer
Steal
MP Restore
Equip Axe
Move+1

Healing Staff
Hero Shield
Red Hood
Secret Clothes
Defense Armlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Lifebregin
Male
Scorpio
60
76
Chemist
Basic Skill
MP Restore
Equip Gun
Levitate

Glacier Gun

Headgear
Earth Clothes
Feather Mantle

Potion, X-Potion, Echo Grass, Remedy, Phoenix Down
Accumulate, Dash, Throw Stone, Heal, Wish

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Smashy
Female
Capricorn
72
49
Samurai
Dance
Abandon
Equip Polearm
Jump+2

Ryozan Silk

Mythril Helmet
Light Robe
Genji Gauntlet

Kiyomori
Witch Hunt



DHaveWord
Male
Capricorn
54
62
Knight
Elemental
MP Restore
Equip Gun
Teleport 2

Fairy Harp
Flame Shield
Mythril Helmet
Reflect Mail
Battle Boots

Armor Break, Shield Break, Magic Break, Mind Break, Surging Sword
Local Quake, Will-O-Wisp, Sand Storm



Catfeath3r
Female
Cancer
42
80
Geomancer
Black Magic
Parry
Equip Bow
Move+3

Hunting Bow
Buckler
Feather Hat
Earth Clothes
Dracula Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Fire 2, Bolt 3, Ice 2, Frog, Flare



Cryptopsy70
Female
Virgo
68
70
Priest
Steal
Catch
Equip Shield
Jump+3

White Staff
Gold Shield
Black Hood
Wizard Outfit
Leather Mantle

Cure 3, Cure 4, Regen, Protect 2, Shell, Shell 2, Wall, Esuna
Gil Taking, Arm Aim

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Latebit
Female
Capricorn
77
56
Samurai
Talk Skill
Counter Tackle
Concentrate
Swim

Muramasa

Barbuta
Linen Robe
Genji Gauntlet

Koutetsu, Murasame, Heaven's Cloud, Kiyomori
Threaten, Negotiate, Mimic Daravon, Refute



Galkife
Male
Aquarius
48
77
Chemist
Throw
Brave Save
Secret Hunt
Teleport

Hydra Bag

Barette
Black Costume
Battle Boots

Potion, Hi-Ether, Soft
Knife



Nifboy
Female
Pisces
69
44
Dancer
Battle Skill
Abandon
Magic Defense UP
Retreat

Cashmere

Flash Hat
Earth Clothes
Rubber Shoes

Polka Polka, Disillusion, Obsidian Blade, Dragon Pit
Head Break, Speed Break, Power Break, Mind Break, Surging Sword



Silentkaster
Female
Aries
53
49
Oracle
Black Magic
Hamedo
Martial Arts
Retreat



Cachusha
Chameleon Robe
Magic Gauntlet

Blind, Paralyze, Sleep
Fire 2, Fire 4, Bolt, Bolt 4, Ice, Ice 4

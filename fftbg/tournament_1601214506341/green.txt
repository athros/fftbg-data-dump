Player: !Green
Team: Green Team
Palettes: Green/White



Phi Sig
Monster
Aquarius
44
71
Blue Dragon










BlizzWulf
Male
Libra
59
63
Squire
Summon Magic
Arrow Guard
Halve MP
Move+3

Giant Axe
Diamond Shield
Diamond Helmet
Mythril Vest
Leather Mantle

Heal, Tickle, Yell, Cheer Up
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Leviathan, Silf, Fairy, Lich



ALY327
Male
Cancer
53
42
Bard
Yin Yang Magic
Arrow Guard
Equip Knife
Ignore Terrain

Cultist Dagger

Golden Hairpin
Black Costume
Sprint Shoes

Cheer Song, Battle Song, Last Song, Hydra Pit
Zombie, Confusion Song, Paralyze



Thyrandaal
Female
Aquarius
79
77
Dancer
Item
Faith Save
Throw Item
Move+1

Panther Bag

Flash Hat
Adaman Vest
Small Mantle

Witch Hunt, Wiznaibus, Polka Polka, Disillusion
Hi-Potion, X-Potion, Ether, Echo Grass, Soft, Phoenix Down

Final Bets: purple - 10 bets for 5,587G (51.6%, x0.94); brown - 7 bets for 5,236G (48.4%, x1.07)

purple bets:
resjudicata3: 1,834G (32.8%, 3,668G)
Mushufasa_: 1,234G (22.1%, 14,737G)
TheBrett: 1,000G (17.9%, 21,364G)
getthemoneyz: 414G (7.4%, 2,210,853G)
Jehudy7: 288G (5.2%, 288G)
b0lt: 216G (3.9%, 216G)
ValensEXP_: 200G (3.6%, 2,382G)
datadrivenbot: 200G (3.6%, 81,557G)
gorgewall: 101G (1.8%, 7,648G)
dfunk3d: 100G (1.8%, 433G)

brown bets:
reinoe: 2,000G (38.2%, 9,835G)
AllInBot: 1,045G (20.0%, 1,045G)
BirbBrainsBot: 1,000G (19.1%, 178,067G)
HaplessOne: 555G (10.6%, 5,067G)
TheSabretoothe: 336G (6.4%, 336G)
NicoSavoy: 200G (3.8%, 6,986G)
FuzzyTigers: 100G (1.9%, 572G)

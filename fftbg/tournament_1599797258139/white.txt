Player: !White
Team: White Team
Palettes: White/Blue



TheSabretoothe
Female
Aquarius
75
68
Dancer
Item
Damage Split
Martial Arts
Fly

Persia

Red Hood
Earth Clothes
Magic Gauntlet

Wiznaibus, Polka Polka, Void Storage, Dragon Pit
Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



NicoSavoy
Female
Cancer
64
76
Archer
Talk Skill
Counter Magic
Equip Armor
Levitate

Silver Bow

Gold Helmet
Chain Mail
Bracer

Charge+1, Charge+5, Charge+20
Preach, Death Sentence, Insult, Refute



Galkife
Female
Gemini
54
78
Archer
Time Magic
Absorb Used MP
Equip Bow
Jump+2

Windslash Bow

Feather Hat
Black Costume
Angel Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+20
Haste 2, Slow 2, Immobilize, Float, Demi 2



Seaweed B
Female
Aries
53
65
Geomancer
Draw Out
MA Save
Equip Armor
Fly

Battle Axe
Platinum Shield
Platinum Helmet
Bronze Armor
Feather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Kiyomori

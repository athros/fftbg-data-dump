Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Krombobreaker
Female
Cancer
74
51
Wizard
Summon Magic
Earplug
Halve MP
Teleport

Wizard Rod

Feather Hat
Light Robe
Spike Shoes

Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 2, Ice 3, Ice 4, Flare
Moogle, Shiva, Ramuh, Golem, Bahamut, Leviathan, Fairy



RunicMagus
Male
Taurus
63
57
Archer
Battle Skill
Faith Save
Magic Attack UP
Waterbreathing

Silver Bow

Black Hood
Secret Clothes
Defense Ring

Charge+1, Charge+4, Charge+5
Mind Break, Night Sword, Explosion Sword



Sietaha
Female
Aries
64
57
Dancer
Punch Art
PA Save
Defend
Move+1

Cute Bag

Black Hood
Black Robe
Diamond Armlet

Slow Dance, Dragon Pit
Wave Fist, Earth Slash, Secret Fist, Purification, Revive



Brokenknight201
Female
Virgo
56
81
Knight
Talk Skill
MA Save
Monster Talk
Swim

Ancient Sword
Hero Shield
Platinum Helmet
Chameleon Robe
Feather Boots

Armor Break, Weapon Break, Mind Break, Stasis Sword
Invitation, Praise, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute

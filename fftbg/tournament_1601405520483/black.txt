Player: !Black
Team: Black Team
Palettes: Black/Red



Bruubarg
Female
Cancer
46
80
Knight
Item
Critical Quick
Long Status
Retreat

Battle Axe
Platinum Shield
Cross Helmet
Platinum Armor
Cursed Ring

Head Break, Shield Break, Speed Break, Mind Break, Justice Sword
Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Holy Water, Phoenix Down



Mirapoix
Male
Cancer
44
44
Calculator
Yin Yang Magic
Meatbone Slash
Dual Wield
Ignore Height

Papyrus Codex
Bestiary
Golden Hairpin
Black Robe
Leather Mantle

CT, 3
Spell Absorb, Life Drain, Zombie, Blind Rage, Confusion Song, Dark Holy



Willjin
Male
Aries
66
70
Geomancer
Charge
PA Save
Halve MP
Ignore Height

Bizen Boat
Mythril Shield
Twist Headband
Leather Outfit
Small Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind
Charge+3, Charge+4, Charge+5, Charge+10



JonnyCue
Female
Virgo
79
70
Knight
Jump
Meatbone Slash
Long Status
Ignore Height

Defender
Buckler
Iron Helmet
Diamond Armor
Small Mantle

Head Break, Speed Break, Justice Sword
Level Jump5, Vertical Jump3

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Phi Sig
Male
Aries
54
57
Thief
Basic Skill
Regenerator
Equip Gun
Waterbreathing

Battle Folio

Triangle Hat
Wizard Outfit
Sprint Shoes

Steal Helmet, Steal Accessory, Leg Aim
Accumulate, Heal, Wish



HASTERIOUS
Male
Sagittarius
56
57
Chemist
Elemental
Parry
Equip Armor
Levitate

Star Bag

Triangle Hat
Wizard Robe
Cursed Ring

Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Killth3kid
Female
Virgo
61
56
Samurai
Elemental
Parry
Secret Hunt
Ignore Height

Bizen Boat

Diamond Helmet
Leather Armor
Defense Ring

Murasame
Pitfall, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



KinniQuacks
Male
Virgo
60
62
Chemist
Black Magic
Regenerator
Equip Knife
Jump+3

Mythril Knife

Barette
Earth Clothes
Chantage

Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Phoenix Down
Fire, Bolt, Bolt 2, Bolt 3, Ice 4

Player: !Green
Team: Green Team
Palettes: Green/White



3ngag3
Female
Cancer
72
68
Summoner
Steal
Critical Quick
Halve MP
Move+1

Rainbow Staff

Headgear
Chameleon Robe
Setiemson

Moogle, Ramuh, Golem, Carbunkle, Bahamut, Odin, Cyclops
Steal Helmet, Steal Shield, Steal Status



CosmicTactician
Female
Aquarius
66
49
Time Mage
Item
PA Save
Equip Gun
Jump+3

Ramia Harp

Thief Hat
Wizard Robe
Reflect Ring

Haste, Haste 2, Stop, Immobilize, Float, Demi 2, Stabilize Time
Potion, X-Potion, Hi-Ether, Eye Drop, Phoenix Down



Laserman1000
Male
Libra
62
79
Time Mage
Jump
Counter
Short Charge
Teleport

Sage Staff

Feather Hat
Brigandine
108 Gems

Haste, Slow 2, Quick, Demi, Stabilize Time
Level Jump8, Vertical Jump7



Forkmore
Male
Pisces
72
73
Thief
Punch Art
Parry
Sicken
Levitate

Orichalcum

Red Hood
Mythril Vest
Salty Rage

Steal Armor, Steal Shield, Steal Accessory
Pummel, Purification, Seal Evil

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



FriendSquirrel
Female
Aries
69
72
Dancer
Steal
Speed Save
Equip Armor
Ignore Height

Ryozan Silk

Black Hood
Linen Cuirass
Magic Ring

Witch Hunt, Wiznaibus, Slow Dance, Nether Demon
Gil Taking, Steal Helmet, Steal Shield



DeathTaxesAndAnime
Female
Cancer
46
59
Mediator
Dance
Counter
Dual Wield
Retreat

Papyrus Codex
Papyrus Codex
Twist Headband
Chain Vest
Elf Mantle

Threaten, Solution, Insult, Mimic Daravon
Wiznaibus



Reinoe
Female
Pisces
82
48
Wizard
Battle Skill
Distribute
Short Charge
Move+2

Dagger

Thief Hat
Adaman Vest
Cursed Ring

Fire, Bolt 4, Ice 2, Empower, Flare
Armor Break, Weapon Break, Speed Break, Justice Sword



Ar Tactic
Male
Sagittarius
59
63
Ninja
Summon Magic
Hamedo
Magic Defense UP
Jump+2

Kunai
Spell Edge
Green Beret
Earth Clothes
Magic Gauntlet

Bomb, Ninja Sword, Wand
Shiva, Ifrit, Carbunkle, Fairy

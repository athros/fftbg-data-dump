Player: !Green
Team: Green Team
Palettes: Green/White



Pepperbuster
Female
Pisces
61
70
Samurai
Charge
Regenerator
Doublehand
Waterwalking

Muramasa

Bronze Helmet
Crystal Mail
Defense Armlet

Heaven's Cloud, Kiyomori, Muramasa
Charge+2, Charge+3



Nhammen
Male
Cancer
62
80
Lancer
Item
Earplug
Throw Item
Move+1

Obelisk
Kaiser Plate
Mythril Helmet
Plate Mail
Dracula Mantle

Level Jump8, Vertical Jump4
Potion, Hi-Potion, Maiden's Kiss, Remedy, Phoenix Down



RunicMagus
Female
Virgo
53
81
Geomancer
Yin Yang Magic
Abandon
Equip Axe
Teleport

Flail
Round Shield
Holy Miter
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind
Spell Absorb, Dispel Magic



Bruubarg
Female
Aries
45
48
Monk
Throw
Arrow Guard
Equip Bow
Swim

Lightning Bow

Black Hood
Black Costume
Diamond Armlet

Purification, Revive, Seal Evil
Bomb

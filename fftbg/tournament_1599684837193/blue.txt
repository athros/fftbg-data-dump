Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Panushenko
Male
Gemini
46
73
Samurai
Summon Magic
HP Restore
Short Status
Waterwalking

Koutetsu Knife

Cross Helmet
Chain Mail
Magic Gauntlet

Asura, Koutetsu, Bizen Boat, Murasame, Muramasa, Kikuichimoji
Moogle, Shiva, Ifrit, Titan, Odin, Silf, Fairy



Actual JP
Male
Gemini
51
38
Geomancer
Item
MA Save
Equip Axe
Move+3

Flail
Flame Shield
Ribbon
Black Robe
Red Shoes

Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Potion, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Zagorsek
Monster
Scorpio
73
70
Draugr










Galkife
Male
Gemini
67
50
Knight
Charge
Arrow Guard
Martial Arts
Swim


Diamond Shield
Crystal Helmet
Diamond Armor
Wizard Mantle

Weapon Break, Speed Break, Power Break
Charge+3, Charge+7

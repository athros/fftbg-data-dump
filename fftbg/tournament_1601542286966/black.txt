Player: !Black
Team: Black Team
Palettes: Black/Red



Heropong
Monster
Sagittarius
54
61
Dryad










CassiePhoenix
Female
Taurus
70
75
Summoner
Charge
Parry
Short Charge
Levitate

Rod

Thief Hat
Silk Robe
Wizard Mantle

Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Salamander, Silf
Charge+1, Charge+5, Charge+10



Grimthegameking
Male
Pisces
72
75
Ninja
Time Magic
Mana Shield
Short Status
Teleport

Kunai
Short Edge
Ribbon
Black Costume
Reflect Ring

Shuriken, Staff
Stop, Immobilize, Reflect, Stabilize Time



IBardic
Male
Gemini
61
62
Bard
Summon Magic
Critical Quick
Equip Armor
Waterbreathing

Fairy Harp

Barbuta
Clothes
108 Gems

Angel Song, Nameless Song, Hydra Pit
Shiva, Ramuh, Titan, Leviathan, Salamander, Silf, Fairy, Cyclops

Player: !Black
Team: Black Team
Palettes: Black/Red



Forkmore
Female
Leo
54
56
Geomancer
Time Magic
Counter Magic
Defense UP
Move-HP Up

Battle Axe
Aegis Shield
Flash Hat
Silk Robe
Diamond Armlet

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand
Haste, Haste 2, Slow, Slow 2, Float, Reflect, Quick, Demi, Demi 2



Killth3kid
Male
Scorpio
51
71
Archer
Black Magic
Counter Tackle
Short Status
Move-MP Up

Windslash Bow

Headgear
Power Sleeve
Feather Mantle

Charge+1, Charge+5, Charge+10
Fire, Fire 2, Bolt 4, Ice, Flare



OneHundredFists
Female
Pisces
69
58
Geomancer
Yin Yang Magic
Hamedo
Martial Arts
Jump+1

Giant Axe
Escutcheon
Holy Miter
Power Sleeve
Diamond Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Blizzard, Lava Ball
Spell Absorb, Blind Rage, Foxbird, Sleep, Dark Holy



Krombobreaker
Male
Capricorn
67
79
Archer
Basic Skill
Mana Shield
Doublehand
Fly

Hunting Bow

Twist Headband
Leather Outfit
Defense Armlet

Charge+1, Charge+3, Charge+10
Heal, Wish

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ogema TheDream
Female
Gemini
69
61
Summoner
Elemental
Earplug
Equip Sword
Move+1

Defender

Golden Hairpin
White Robe
Genji Gauntlet

Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Fairy
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



OneHundredFists
Monster
Sagittarius
51
49
Dryad










Gelwain
Male
Aries
41
52
Samurai
Basic Skill
Faith Save
Equip Bow
Levitate

Lightning Bow

Diamond Helmet
Mythril Armor
N-Kai Armlet

Heaven's Cloud, Kiyomori
Throw Stone, Heal, Cheer Up, Scream



Prince Rogers Nelson
Female
Aries
71
74
Monk
Yin Yang Magic
MA Save
Magic Attack UP
Move+1



Red Hood
Earth Clothes
Magic Gauntlet

Revive
Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic

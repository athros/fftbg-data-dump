Player: !zChamp
Team: Champion Team
Palettes: Black/Red



MoonSugarFiend
Male
Leo
53
70
Knight
Item
Dragon Spirit
Magic Defense UP
Ignore Terrain

Slasher
Round Shield
Iron Helmet
Bronze Armor
Sprint Shoes

Armor Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Night Sword
Ether, Hi-Ether, Remedy, Phoenix Down



Thunderducker
Female
Cancer
53
80
Priest
Draw Out
Critical Quick
Equip Shield
Ignore Height

Mace of Zeus
Platinum Shield
Golden Hairpin
Robe of Lords
Reflect Ring

Cure, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell 2, Esuna
Kiyomori, Muramasa, Kikuichimoji



Genkidou
Male
Scorpio
79
39
Archer
Yin Yang Magic
Dragon Spirit
Short Charge
Move+2

Ultimus Bow

Grand Helmet
Judo Outfit
Feather Boots

Charge+2, Charge+3, Charge+7, Charge+10
Spell Absorb, Silence Song, Dispel Magic, Paralyze, Sleep



Gorgewall
Male
Leo
55
78
Archer
Jump
Counter
Attack UP
Teleport

Mythril Bow

Holy Miter
Adaman Vest
Sprint Shoes

Charge+3, Charge+4
Level Jump5, Vertical Jump8

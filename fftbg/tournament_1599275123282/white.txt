Player: !White
Team: White Team
Palettes: White/Blue



NicoSavoy
Male
Libra
43
59
Knight
Steal
Parry
Martial Arts
Retreat

Excalibur
Aegis Shield
Bronze Helmet
Chameleon Robe
Leather Mantle

Head Break, Power Break, Night Sword, Explosion Sword
Steal Weapon



Daveb
Male
Aquarius
67
52
Ninja
Talk Skill
Auto Potion
Short Status
Teleport

Orichalcum
Ninja Edge
Thief Hat
Brigandine
Jade Armlet

Shuriken, Wand, Dictionary
Invitation, Threaten, Preach, Insult



Thunderducker
Monster
Scorpio
56
47
Ochu










Greggernaut
Female
Taurus
45
80
Geomancer
White Magic
Critical Quick
Defense UP
Swim

Giant Axe
Flame Shield
Black Hood
Secret Clothes
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Lava Ball
Cure 3, Raise, Raise 2, Protect 2, Shell, Esuna

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Old Overholt
Female
Cancer
51
49
Priest
Yin Yang Magic
Arrow Guard
Equip Armor
Jump+2

Oak Staff

Gold Helmet
Linen Cuirass
Reflect Ring

Cure, Cure 3, Raise, Reraise, Shell, Wall, Esuna
Pray Faith, Confusion Song, Sleep



Galkife
Monster
Virgo
62
80
Holy Dragon










Shalloween
Female
Gemini
55
71
Wizard
Draw Out
Critical Quick
Martial Arts
Ignore Height



Holy Miter
Chameleon Robe
Defense Ring

Fire, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Ice 4, Death
Bizen Boat, Kiyomori



LadyCyan
Female
Virgo
47
50
Samurai
Steal
Meatbone Slash
Short Charge
Move-HP Up

Holy Lance

Genji Helmet
Linen Cuirass
Chantage

Asura
Steal Status

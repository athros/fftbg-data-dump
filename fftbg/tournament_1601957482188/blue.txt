Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JonnyCue
Male
Serpentarius
40
71
Calculator
Time Magic
Caution
Attack UP
Move+1

Cypress Rod

Green Beret
Wizard Robe
Rubber Shoes

CT, Height, Prime Number, 4
Haste, Immobilize, Float, Reflect, Quick, Stabilize Time



Kronikle
Female
Gemini
70
75
Calculator
Yin Yang Magic
Absorb Used MP
Equip Armor
Teleport

Papyrus Codex

Iron Helmet
Mystic Vest
Sprint Shoes

CT, Height, Prime Number, 5, 4
Blind, Poison, Pray Faith, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze



NovaKnight21
Male
Aquarius
42
52
Ninja
Black Magic
Arrow Guard
Attack UP
Jump+3

Iga Knife
Kunai
Feather Hat
Adaman Vest
Magic Gauntlet

Bomb
Fire 3, Fire 4, Bolt, Bolt 3, Empower



WireLord
Female
Libra
71
65
Lancer
Talk Skill
Critical Quick
Sicken
Move-MP Up

Holy Lance
Genji Shield
Bronze Helmet
Chameleon Robe
Sprint Shoes

Level Jump4, Vertical Jump8
Persuade, Praise, Mimic Daravon, Refute

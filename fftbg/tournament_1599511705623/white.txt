Player: !White
Team: White Team
Palettes: White/Blue



StealthModeLocke
Female
Libra
58
59
Dancer
Throw
Parry
Equip Knife
Ignore Height

Wizard Rod

Headgear
Robe of Lords
Rubber Shoes

Disillusion, Dragon Pit
Knife



Thyrandaal
Female
Aquarius
53
59
Samurai
Summon Magic
PA Save
Magic Attack UP
Teleport

Muramasa

Bronze Helmet
Black Robe
Wizard Mantle

Asura, Kiyomori
Ifrit, Titan, Odin, Leviathan



Leonidusx
Monster
Taurus
48
78
Blue Dragon










Ruleof5
Female
Sagittarius
42
60
Archer
Punch Art
Counter Flood
Equip Armor
Jump+3

Ultimus Bow

Leather Hat
Platinum Armor
Sprint Shoes

Charge+1, Charge+4
Revive

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Farside00831
Male
Capricorn
53
49
Lancer
Item
Arrow Guard
Throw Item
Move+2

Octagon Rod
Kaiser Plate
Circlet
Genji Armor
Germinas Boots

Level Jump4, Vertical Jump5
Potion, Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down



CosmicTactician
Female
Taurus
73
76
Mediator
Yin Yang Magic
Faith Save
Dual Wield
Waterbreathing

Mythril Gun
Blast Gun
Red Hood
Wizard Robe
Magic Gauntlet

Invitation, Praise, Threaten, Solution, Death Sentence, Negotiate
Blind, Poison, Spell Absorb, Pray Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep



Lord Gwarth
Female
Scorpio
72
63
Calculator
Black Magic
Earplug
Defense UP
Lava Walking

Papyrus Codex

Golden Hairpin
Chameleon Robe
N-Kai Armlet

CT, Height, Prime Number
Bolt 3, Ice, Flare



Meta Five
Male
Libra
68
72
Archer
Item
Abandon
Martial Arts
Waterwalking

Mythril Bow

Leather Hat
Leather Outfit
Leather Mantle

Charge+2
Hi-Ether, Antidote, Eye Drop, Echo Grass, Phoenix Down

Player: !Red
Team: Red Team
Palettes: Red/Brown



MemoriesofFinal
Female
Cancer
59
78
Chemist
Steal
Counter Tackle
Equip Armor
Move+1

Mythril Gun

Cross Helmet
Carabini Mail
Spike Shoes

Potion, Hi-Ether, Antidote, Echo Grass, Phoenix Down
Steal Heart, Steal Armor, Steal Status



Skillomono
Male
Libra
76
80
Samurai
Jump
PA Save
Magic Defense UP
Retreat

Asura Knife

Iron Helmet
White Robe
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Level Jump4, Vertical Jump7



Jeeboheebo
Monster
Gemini
81
63
Steel Giant










IciclePop0
Male
Leo
50
53
Lancer
Time Magic
MP Restore
Attack UP
Swim

Obelisk
Round Shield
Iron Helmet
Crystal Mail
Angel Ring

Level Jump4, Vertical Jump6
Haste, Haste 2, Stop, Reflect, Demi, Stabilize Time, Meteor

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Run With Stone GUNs
Male
Leo
56
44
Mediator
Draw Out
Mana Shield
Dual Wield
Ignore Terrain

Papyrus Codex
Battle Folio
Green Beret
Linen Robe
Leather Mantle

Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Koutetsu, Bizen Boat, Kiyomori



ALY327
Female
Taurus
54
64
Mime

Earplug
Equip Armor
Move+2



Gold Helmet
Platinum Armor
Diamond Armlet

Mimic




Actual JP
Male
Gemini
44
57
Ninja
Sing
Parry
Defend
Levitate

Blind Knife
Flame Whip
Golden Hairpin
Clothes
Power Wrist

Knife, Sword, Staff, Ninja Sword
Life Song, Cheer Song



Khelor
Female
Aries
72
56
Monk
White Magic
Counter
Sicken
Ignore Terrain



Feather Hat
Mystic Vest
Feather Boots

Spin Fist, Wave Fist, Purification, Revive
Cure 3, Cure 4, Raise, Protect 2, Shell 2, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



CorpusCav
Female
Scorpio
71
49
Chemist
Talk Skill
Faith Save
Equip Armor
Ignore Terrain

Dagger

Gold Helmet
Gold Armor
Spike Shoes

Potion, Hi-Potion, X-Potion, Hi-Ether, Phoenix Down
Threaten, Solution, Insult, Rehabilitate



Lowlf
Monster
Libra
49
76
Ahriman










Chuckolator
Female
Libra
42
55
Ninja
Charge
Sunken State
Equip Gun
Move+1

Stone Gun
Mythril Gun
Green Beret
Power Sleeve
Genji Gauntlet

Bomb
Charge+1, Charge+3, Charge+10



Projekt Mike
Male
Pisces
75
61
Mediator
Item
Caution
Short Charge
Jump+1

Mythril Gun

Golden Hairpin
Wizard Robe
Dracula Mantle

Preach, Insult, Negotiate
Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TheSabretoothe
Male
Scorpio
72
43
Geomancer
Talk Skill
MP Restore
Defend
Jump+1

Battle Axe
Gold Shield
Golden Hairpin
Leather Outfit
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon



Ogema TheDream
Male
Pisces
61
38
Oracle
Jump
MP Restore
Long Status
Fly

Battle Folio

Leather Hat
Black Costume
Salty Rage

Blind, Poison, Spell Absorb, Life Drain, Zombie, Dispel Magic, Paralyze, Sleep, Dark Holy
Level Jump3, Vertical Jump4



Ar Tactic
Female
Libra
47
50
Geomancer
Draw Out
Mana Shield
Attack UP
Levitate

Slasher
Round Shield
Leather Hat
Judo Outfit
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji



ColetteMSLP
Male
Virgo
64
72
Bard
Draw Out
Meatbone Slash
Short Status
Move+1

Fairy Harp

Leather Hat
Chain Vest
Elf Mantle

Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Last Song, Space Storage
Koutetsu, Bizen Boat

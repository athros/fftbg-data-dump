Player: !Red
Team: Red Team
Palettes: Red/Brown



Rottings0ul
Female
Taurus
56
67
Samurai
Steal
Distribute
Equip Polearm
Jump+2

Octagon Rod

Grand Helmet
Plate Mail
Battle Boots

Asura, Bizen Boat, Heaven's Cloud
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim



Mushufasa
Male
Cancer
55
75
Archer
Steal
Auto Potion
Equip Shield
Lava Walking

Silver Bow
Escutcheon
Green Beret
Mystic Vest
108 Gems

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Arm Aim



IBardic
Female
Cancer
54
48
Knight
Jump
HP Restore
Long Status
Levitate

Chaos Blade
Mythril Shield
Diamond Helmet
Diamond Armor
Angel Ring

Head Break, Armor Break, Shield Break, Mind Break, Stasis Sword, Surging Sword
Level Jump2, Vertical Jump5



Neech
Male
Libra
48
62
Wizard
Elemental
Dragon Spirit
Martial Arts
Waterwalking



Headgear
Wizard Robe
Feather Mantle

Bolt, Bolt 3, Ice 2, Ice 3, Ice 4, Empower, Death, Flare
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball

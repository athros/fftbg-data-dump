Player: !Black
Team: Black Team
Palettes: Black/Red



SephDarkheart
Male
Gemini
67
49
Knight
Item
Parry
Equip Axe
Retreat

Flail
Flame Shield
Bronze Helmet
Diamond Armor
N-Kai Armlet

Weapon Break, Speed Break, Power Break, Justice Sword
Potion, Eye Drop, Echo Grass, Remedy



Upvla
Female
Taurus
62
52
Wizard
Charge
Damage Split
Short Charge
Levitate

Poison Rod

Holy Miter
Linen Robe
Jade Armlet

Bolt 2, Bolt 4
Charge+1, Charge+3, Charge+7, Charge+20



Dem0nj0ns
Male
Leo
67
51
Mediator
Summon Magic
Speed Save
Maintenance
Swim

Panther Bag

Golden Hairpin
Light Robe
Power Wrist

Invitation, Praise, Solution, Negotiate, Refute, Rehabilitate
Shiva, Ramuh, Bahamut, Odin, Leviathan, Cyclops



Grininda
Female
Leo
52
47
Summoner
Item
Dragon Spirit
Short Charge
Jump+1

Ice Rod

Leather Hat
White Robe
Elf Mantle

Moogle, Carbunkle, Bahamut, Lich, Cyclops
Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Phoenix Down

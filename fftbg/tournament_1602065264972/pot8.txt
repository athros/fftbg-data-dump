Final Bets: black - 6 bets for 3,177G (36.0%, x1.78); champion - 9 bets for 5,654G (64.0%, x0.56)

black bets:
BirbBrainsBot: 1,000G (31.5%, 157,348G)
Romebot: 1,000G (31.5%, 1,000G)
Rune339: 484G (15.2%, 4,846G)
Humble_Fabio: 337G (10.6%, 4,467G)
chaosdesigned: 200G (6.3%, 3,312G)
getthemoneyz: 156G (4.9%, 2,201,075G)

champion bets:
upvla: 1,276G (22.6%, 1,276G)
Mushufasa_: 1,234G (21.8%, 20,091G)
DeathTaxesAndAnime: 976G (17.3%, 976G)
SephDarkheart: 580G (10.3%, 21,119G)
ValensEXP_: 480G (8.5%, 480G)
HorusTaurus: 408G (7.2%, 408G)
silentkaster: 300G (5.3%, 29,182G)
AllInBot: 200G (3.5%, 200G)
datadrivenbot: 200G (3.5%, 82,558G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JumbocactuarX27
Male
Cancer
41
44
Bard
Steal
Regenerator
Equip Armor
Jump+3

Bloody Strings

Leather Hat
Chameleon Robe
Power Wrist

Cheer Song, Battle Song, Diamond Blade, Hydra Pit
Gil Taking, Steal Accessory, Steal Status



Helpimabug
Male
Taurus
66
73
Knight
Yin Yang Magic
Arrow Guard
Magic Defense UP
Lava Walking

Long Sword
Mythril Shield
Cross Helmet
Chameleon Robe
Dracula Mantle

Head Break, Shield Break, Weapon Break, Speed Break
Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep



Willjin
Male
Aries
73
50
Bard
Basic Skill
Critical Quick
Secret Hunt
Waterbreathing

Fairy Harp

Golden Hairpin
Mystic Vest
Magic Gauntlet

Last Song, Space Storage, Sky Demon, Hydra Pit
Accumulate, Heal, Scream



VolgraTheMoose
Female
Cancer
48
76
Summoner
Item
Counter Flood
Short Charge
Move+3

White Staff

Golden Hairpin
Power Sleeve
Genji Gauntlet

Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Odin, Leviathan, Silf, Fairy
Potion, X-Potion, Hi-Ether, Remedy

Player: !Red
Team: Red Team
Palettes: Red/Brown



Rocl
Male
Scorpio
56
46
Calculator
Farm Skill
Counter Tackle
Attack UP
Waterbreathing

Battle Folio

Circlet
Carabini Mail
Defense Ring

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



SephDarkheart
Female
Aries
79
64
Knight
White Magic
Counter Tackle
Halve MP
Move+2

Coral Sword
Venetian Shield
Gold Helmet
Bronze Armor
Small Mantle

Head Break, Armor Break, Shield Break, Speed Break, Mind Break, Dark Sword
Cure, Cure 2, Raise, Reraise, Shell 2, Esuna



RunicMagus
Male
Aquarius
49
41
Ninja
White Magic
Sunken State
Defend
Waterwalking

Orichalcum
Kunai
Thief Hat
Mystic Vest
Genji Gauntlet

Bomb
Cure, Cure 2, Raise, Wall



DeathTaxesAndAnime
Female
Taurus
80
52
Mediator
Item
Blade Grasp
Throw Item
Swim

Bestiary

Feather Hat
Light Robe
Feather Boots

Persuade, Preach, Death Sentence, Insult, Mimic Daravon, Refute
Potion, Ether, Echo Grass, Maiden's Kiss, Phoenix Down

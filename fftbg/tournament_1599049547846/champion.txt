Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Yangusburger
Female
Sagittarius
57
60
Mime

MA Save
Maintenance
Swim



Leather Hat
Black Costume
Defense Ring

Mimic




Vorap
Female
Pisces
44
53
Priest
Steal
Counter Flood
Short Charge
Jump+2

Wizard Staff

Flash Hat
Black Robe
Small Mantle

Cure, Cure 2, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna, Holy
Steal Heart, Steal Shield, Steal Accessory



Sairentozon7
Female
Aquarius
75
63
Thief
Yin Yang Magic
Sunken State
Equip Armor
Jump+1

Hidden Knife

Barbuta
Chain Vest
Magic Ring

Gil Taking, Steal Heart, Steal Helmet
Blind, Zombie, Confusion Song, Dispel Magic, Paralyze, Petrify



MemoriesofFinal
Male
Pisces
62
78
Calculator
Black Magic
Critical Quick
Concentrate
Levitate

Madlemgen

Black Hood
Silk Robe
Germinas Boots

CT, Height, Prime Number, 4, 3
Fire 4, Bolt 2, Bolt 4, Ice, Ice 2, Ice 4, Frog, Flare

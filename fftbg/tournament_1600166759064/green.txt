Player: !Green
Team: Green Team
Palettes: Green/White



Prince Rogers Nelson
Male
Gemini
53
70
Lancer
Basic Skill
Counter
Attack UP
Jump+1

Mythril Spear
Ice Shield
Platinum Helmet
Gold Armor
Diamond Armlet

Level Jump4, Vertical Jump8
Accumulate, Dash, Throw Stone, Heal, Fury



Jeeboheebo
Male
Taurus
46
58
Mime

Counter
Magic Defense UP
Waterbreathing



Black Hood
Leather Outfit
Dracula Mantle

Mimic




Dreygyron
Female
Aries
45
49
Samurai
Time Magic
Catch
Equip Axe
Jump+3

Wizard Staff

Cross Helmet
Genji Armor
Feather Mantle

Heaven's Cloud
Immobilize, Float, Quick, Demi 2, Stabilize Time



Gingerfoo69
Male
Leo
46
64
Samurai
Item
Hamedo
Maintenance
Move+2

Kiyomori

Barbuta
Robe of Lords
Feather Boots

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori
Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy

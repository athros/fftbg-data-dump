Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Raixelol
Female
Aries
71
75
Mediator
Dance
Catch
Equip Armor
Waterbreathing

Blast Gun

Genji Helmet
Gold Armor
108 Gems

Insult, Refute, Rehabilitate
Witch Hunt, Polka Polka, Last Dance



Hotpocketsofficial
Male
Scorpio
59
40
Calculator
Mighty Sword
PA Save
Long Status
Levitate

Bestiary

Flash Hat
Black Robe
Sprint Shoes

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



DeathTaxesAndAnime
Female
Gemini
65
59
Wizard
Math Skill
Sunken State
Doublehand
Move-HP Up

Rod

Golden Hairpin
Wizard Outfit
Leather Mantle

Fire 4, Bolt 4, Ice 3, Ice 4, Flare
CT, 5, 3



Tessy
Female
Virgo
76
65
Geomancer
Black Magic
Dragon Spirit
Short Status
Teleport

Battle Axe
Gold Shield
Golden Hairpin
Earth Clothes
Reflect Ring

Hell Ivy, Static Shock, Quicksand, Gusty Wind, Lava Ball
Fire, Fire 3, Bolt 2, Ice, Ice 3, Death, Flare

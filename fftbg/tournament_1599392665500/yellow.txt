Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DeathTaxesAndAnime
Female
Scorpio
79
57
Mediator
Summon Magic
MA Save
Magic Defense UP
Jump+3

Romanda Gun

Golden Hairpin
Chameleon Robe
Small Mantle

Invitation, Threaten, Death Sentence, Insult
Moogle, Shiva, Titan, Bahamut, Leviathan, Fairy



Neech
Monster
Aries
72
79
Archaic Demon










Raixelol
Male
Taurus
55
77
Archer
Basic Skill
Speed Save
Equip Shield
Fly

Night Killer
Round Shield
Feather Hat
Earth Clothes
Jade Armlet

Charge+1, Charge+2, Charge+7, Charge+10
Dash, Throw Stone, Heal, Tickle, Yell, Wish



SephDarkheart
Female
Cancer
43
72
Monk
Elemental
Hamedo
Equip Polearm
Move+2

Mythril Spear

Green Beret
Brigandine
Diamond Armlet

Pummel, Earth Slash, Secret Fist, Purification
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !White
Team: White Team
Palettes: White/Blue



TheSabretoothe
Male
Taurus
71
42
Knight
Item
Dragon Spirit
Equip Sword
Move+3

Platinum Sword
Genji Shield
Circlet
Crystal Mail
Red Shoes

Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Justice Sword, Dark Sword, Surging Sword
Potion, X-Potion, Eye Drop, Maiden's Kiss



Ogema TheDream
Male
Aries
54
54
Time Mage
Jump
PA Save
Equip Axe
Ignore Height

Flame Whip

Green Beret
Judo Outfit
Magic Ring

Haste 2, Float, Reflect, Stabilize Time
Level Jump5, Vertical Jump8



Victoriolue
Female
Aries
57
67
Archer
Punch Art
Regenerator
Doublehand
Lava Walking

Silver Bow

Golden Hairpin
Judo Outfit
N-Kai Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Spin Fist, Wave Fist, Purification, Chakra, Revive



MemoriesofFinal
Female
Aquarius
71
47
Lancer
Time Magic
HP Restore
Equip Bow
Teleport

Lightning Bow

Barbuta
Genji Armor
Genji Gauntlet

Level Jump8, Vertical Jump8
Haste, Slow, Reflect, Demi 2, Stabilize Time

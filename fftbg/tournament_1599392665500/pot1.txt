Final Bets: red - 6 bets for 9,884G (66.6%, x0.50); blue - 11 bets for 4,956G (33.4%, x1.99)

red bets:
bruubarg: 7,305G (73.9%, 7,305G)
DavenIII: 1,178G (11.9%, 2,356G)
Neech: 1,000G (10.1%, 3,010G)
gongonono: 200G (2.0%, 3,271G)
gorgewall: 101G (1.0%, 4,211G)
rottings0ul: 100G (1.0%, 1,878G)

blue bets:
BirbBrainsBot: 1,000G (20.2%, 15,318G)
nhammen: 1,000G (20.2%, 438,311G)
Mushufasa_: 500G (10.1%, 3,867G)
ForagerCats: 500G (10.1%, 13,162G)
Ogema_theDream: 470G (9.5%, 2,673G)
Sietaha: 400G (8.1%, 5,319G)
getthemoneyz: 382G (7.7%, 1,849,412G)
BartTradingCompany: 312G (6.3%, 3,127G)
ValensEXP_: 200G (4.0%, 769G)
DeathTaxesAndAnime: 100G (2.0%, 8,006G)
roqqqpsi: 92G (1.9%, 2,094G)

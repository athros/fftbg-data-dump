Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dasutin23
Female
Gemini
73
44
Knight
Draw Out
Regenerator
Secret Hunt
Teleport

Rune Blade
Round Shield
Leather Helmet
Carabini Mail
108 Gems

Head Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Heaven's Cloud, Muramasa



Raixelol
Male
Scorpio
48
60
Bard
Basic Skill
HP Restore
Doublehand
Jump+3

Bloody Strings

Feather Hat
Reflect Mail
Small Mantle

Angel Song, Life Song, Magic Song, Nameless Song
Heal, Yell, Scream



Prince Rogers Nelson
Male
Cancer
43
57
Samurai
Battle Skill
Arrow Guard
Equip Polearm
Move+1

Musk Rod

Crystal Helmet
Bronze Armor
Elf Mantle

Bizen Boat, Murasame, Muramasa
Head Break, Weapon Break, Mind Break



Ogema TheDream
Male
Sagittarius
69
73
Priest
Summon Magic
Abandon
Equip Knife
Move-MP Up

Ninja Edge

Golden Hairpin
Black Robe
108 Gems

Cure 4, Raise, Reraise, Shell, Wall, Esuna
Ramuh, Fairy

Player: !White
Team: White Team
Palettes: White/Blue



Shalloween
Male
Virgo
65
78
Mediator
Time Magic
Meatbone Slash
Halve MP
Jump+3

Bestiary

Headgear
Secret Clothes
Salty Rage

Persuade, Preach, Death Sentence, Insult, Negotiate, Rehabilitate
Haste 2, Slow, Slow 2, Immobilize, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time



Phi Sig
Female
Capricorn
44
78
Priest
Talk Skill
Damage Split
Equip Sword
Ignore Terrain

Save the Queen

Flash Hat
Black Robe
Spike Shoes

Cure 3, Cure 4, Raise, Regen, Protect 2, Esuna, Holy
Persuade, Solution, Death Sentence, Negotiate, Refute



HaateXIII
Male
Cancer
62
47
Summoner
Talk Skill
Meatbone Slash
Monster Talk
Lava Walking

Wizard Rod

Green Beret
Wizard Outfit
Jade Armlet

Moogle, Ifrit, Golem, Salamander, Lich
Invitation, Solution, Refute, Rehabilitate



Lord Gwarth
Male
Sagittarius
80
76
Ninja
Draw Out
PA Save
Equip Gun
Move-MP Up

Bestiary
Battle Folio
Golden Hairpin
Adaman Vest
N-Kai Armlet

Shuriken, Knife
Koutetsu

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DavenIII
Male
Capricorn
77
49
Archer
Basic Skill
Abandon
Beastmaster
Move+3

Ice Bow

Crystal Helmet
Black Costume
N-Kai Armlet

Charge+1, Charge+3, Charge+7
Dash, Throw Stone, Tickle, Yell, Cheer Up



FoeSquirrel
Male
Virgo
79
44
Calculator
Demon Skill
Absorb Used MP
Concentrate
Jump+3

Battle Folio
Round Shield
Twist Headband
Carabini Mail
Spike Shoes

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Raixelol
Female
Libra
54
60
Chemist
Yin Yang Magic
Catch
Equip Armor
Waterwalking

Star Bag

Leather Helmet
Wizard Robe
Diamond Armlet

Potion, Hi-Potion, X-Potion, Phoenix Down
Doubt Faith, Confusion Song, Dispel Magic



Dogsandcatsand
Female
Gemini
61
52
Wizard
White Magic
Absorb Used MP
Short Charge
Waterbreathing

Flame Rod

Green Beret
Adaman Vest
Battle Boots

Fire 2, Bolt, Bolt 2, Ice, Ice 2
Cure 3, Cure 4, Regen, Protect, Shell, Shell 2, Esuna, Holy

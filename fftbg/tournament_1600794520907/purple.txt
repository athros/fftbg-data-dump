Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ser Pyrro
Female
Pisces
63
50
Wizard
Item
Mana Shield
Short Charge
Move+2

Flame Rod

Twist Headband
Black Robe
Defense Armlet

Fire 3, Bolt 3, Bolt 4, Ice 4, Flare
Ether, Hi-Ether, Phoenix Down



Holdenmagronik
Female
Taurus
61
55
Wizard
Dance
HP Restore
Short Charge
Jump+1

Mage Masher

Barette
Linen Robe
108 Gems

Fire, Fire 2, Fire 3, Bolt 3, Ice 2, Empower
Witch Hunt, Disillusion, Nameless Dance



Pplvee1
Male
Aquarius
63
55
Thief
Item
Critical Quick
Magic Defense UP
Move+1

Assassin Dagger

Triangle Hat
Adaman Vest
Bracer

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Shalloween
Male
Libra
78
72
Summoner
Charge
HP Restore
Doublehand
Move-MP Up

Oak Staff

Triangle Hat
Black Robe
Feather Boots

Moogle, Shiva, Ifrit, Carbunkle, Fairy
Charge+2, Charge+3, Charge+7

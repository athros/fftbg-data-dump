Player: !Black
Team: Black Team
Palettes: Black/Red



StealthModeLocke
Monster
Leo
80
57
Blue Dragon










CosmicTactician
Female
Leo
76
76
Time Mage
Draw Out
Parry
Defense UP
Teleport 2

Wizard Staff

Twist Headband
Chameleon Robe
Small Mantle

Haste, Stop, Immobilize, Float, Reflect, Quick, Demi
Bizen Boat, Kiyomori



Roofiepops
Female
Capricorn
45
75
Calculator
White Magic
MA Save
Sicken
Move-HP Up

Bestiary

Thief Hat
Light Robe
Germinas Boots

CT, Prime Number, 5, 3
Cure 4, Raise, Raise 2, Wall



Just Here2
Male
Libra
67
60
Priest
Basic Skill
Hamedo
Beastmaster
Ignore Height

Rainbow Staff

Leather Hat
Wizard Outfit
Reflect Ring

Cure 2, Cure 3, Raise, Reraise, Protect, Esuna
Dash, Heal, Yell, Fury

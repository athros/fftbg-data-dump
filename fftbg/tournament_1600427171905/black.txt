Player: !Black
Team: Black Team
Palettes: Black/Red



Ericzubat
Female
Leo
66
49
Dancer
Summon Magic
MA Save
Maintenance
Waterbreathing

Ryozan Silk

Triangle Hat
White Robe
Feather Mantle

Wiznaibus, Slow Dance, Disillusion, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit
Shiva, Ramuh, Ifrit, Golem, Carbunkle, Cyclops



Resjudicata3
Male
Aquarius
59
55
Squire
White Magic
Catch
Equip Gun
Fly

Bestiary
Flame Shield
Bronze Helmet
Judo Outfit
Cherche

Dash, Throw Stone, Heal, Cheer Up, Fury, Scream
Cure 2, Raise, Regen, Protect, Protect 2, Esuna, Holy



Jeeboheebo
Monster
Libra
70
69
Steel Giant










Smashy
Female
Gemini
62
54
Thief
Elemental
Counter Magic
Equip Axe
Move+1

Morning Star

Flash Hat
Brigandine
Feather Mantle

Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind

Player: !White
Team: White Team
Palettes: White/Blue



Ar Tactic
Female
Pisces
64
59
Dancer
Summon Magic
Parry
Equip Shield
Move+3

Ryozan Silk
Gold Shield
Thief Hat
Light Robe
Leather Mantle

Wiznaibus, Obsidian Blade, Void Storage, Nether Demon
Golem, Cyclops



0v3rr8d
Male
Sagittarius
66
64
Lancer
Throw
MP Restore
Halve MP
Teleport

Partisan
Flame Shield
Platinum Helmet
Plate Mail
Red Shoes

Level Jump3, Vertical Jump7
Shuriken, Bomb, Knife



ZCKaiser
Male
Capricorn
58
80
Knight
Sing
Speed Save
Attack UP
Swim

Blood Sword
Platinum Shield
Diamond Helmet
Plate Mail
Magic Gauntlet

Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Night Sword
Cheer Song, Last Song, Sky Demon



Rune339
Male
Leo
43
68
Thief
Summon Magic
Regenerator
Sicken
Move+3

Blind Knife

Green Beret
Brigandine
Dracula Mantle

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Moogle, Ramuh, Carbunkle

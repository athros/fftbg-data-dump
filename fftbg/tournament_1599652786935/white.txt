Player: !White
Team: White Team
Palettes: White/Blue



Lijarkh
Female
Leo
66
66
Monk
White Magic
Critical Quick
Concentrate
Retreat



Feather Hat
Judo Outfit
Dracula Mantle

Chakra, Revive, Seal Evil
Cure 2, Raise, Wall, Esuna



Gorgewall
Female
Aquarius
71
70
Samurai
Yin Yang Magic
Mana Shield
Doublehand
Move+1

Javelin

Bronze Helmet
Bronze Armor
N-Kai Armlet

Asura, Bizen Boat, Murasame, Heaven's Cloud
Blind, Life Drain, Zombie, Foxbird, Dispel Magic, Paralyze, Petrify



Ogema TheDream
Female
Capricorn
41
69
Calculator
Black Magic
Distribute
Sicken
Waterbreathing

Dragon Rod

Flash Hat
Wizard Outfit
Elf Mantle

CT, Height, 5, 4
Fire 2, Fire 4, Ice 2, Frog, Death



Leonidusx
Male
Scorpio
63
54
Knight
Basic Skill
Parry
Equip Gun
Retreat

Bloody Strings
Genji Shield
Crystal Helmet
Platinum Armor
Small Mantle

Head Break, Shield Break, Magic Break
Dash, Heal, Cheer Up, Scream

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Error72
Male
Cancer
72
44
Bard
Jump
Regenerator
Doublehand
Jump+2

Mythril Bow

Flash Hat
Gold Armor
Cursed Ring

Cheer Song, Hydra Pit
Level Jump4, Vertical Jump6



0v3rr8d
Male
Libra
44
55
Lancer
Black Magic
Parry
Magic Defense UP
Move+2

Obelisk
Gold Shield
Genji Helmet
Gold Armor
Magic Ring

Level Jump3, Vertical Jump8
Fire, Fire 4, Ice



Reddwind
Male
Capricorn
57
49
Knight
Elemental
Brave Save
Magic Defense UP
Move+3

Defender
Flame Shield
Genji Helmet
Genji Armor
Red Shoes

Armor Break, Power Break, Mind Break, Justice Sword, Night Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



J2DaBibbles
Female
Libra
58
49
Mediator
Punch Art
Brave Save
Dual Wield
Teleport

Battle Folio
Papyrus Codex
Leather Hat
Earth Clothes
Power Wrist

Invitation, Praise, Solution, Death Sentence, Insult, Rehabilitate
Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil

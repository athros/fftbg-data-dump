Final Bets: white - 8 bets for 3,255G (36.0%, x1.77); black - 9 bets for 5,775G (64.0%, x0.56)

white bets:
Zachara: 1,054G (32.4%, 144,054G)
skipsandwiches: 1,001G (30.8%, 91,235G)
SephDarkheart: 400G (12.3%, 14,135G)
datadrivenbot: 200G (6.1%, 75,661G)
ser_pyrro: 200G (6.1%, 1,143G)
VynxYukida: 200G (6.1%, 947G)
Lord_Gwarth: 100G (3.1%, 5,113G)
Error72: 100G (3.1%, 3,078G)

black bets:
Chihuahua_Charity: 1,455G (25.2%, 1,455G)
BirbBrainsBot: 1,000G (17.3%, 153,390G)
MemoriesofFinal: 1,000G (17.3%, 74,265G)
getthemoneyz: 520G (9.0%, 2,067,025G)
CosmicTactician: 500G (8.7%, 25,587G)
Songdeh: 500G (8.7%, 2,364G)
ColetteMSLP: 300G (5.2%, 7,421G)
silentkaster: 300G (5.2%, 2,807G)
AllInBot: 200G (3.5%, 200G)

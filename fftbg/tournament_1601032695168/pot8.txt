Final Bets: green - 5 bets for 1,722G (12.5%, x7.01); champion - 9 bets for 12,063G (87.5%, x0.14)

green bets:
BirbBrainsBot: 1,000G (58.1%, 156,703G)
getthemoneyz: 254G (14.8%, 2,068,194G)
MemoriesofFinal: 200G (11.6%, 74,923G)
TheSabretoothe: 168G (9.8%, 168G)
CosmicTactician: 100G (5.8%, 25,602G)

champion bets:
HorusTaurus: 5,854G (48.5%, 5,854G)
Zachara: 3,063G (25.4%, 146,063G)
Error72: 1,001G (8.3%, 3,326G)
VynxYukida: 1,000G (8.3%, 1,174G)
bigbongsmoker: 340G (2.8%, 2,074G)
silentkaster: 250G (2.1%, 2,379G)
AllInBot: 200G (1.7%, 200G)
datadrivenbot: 200G (1.7%, 75,731G)
scsuperstar: 155G (1.3%, 471G)

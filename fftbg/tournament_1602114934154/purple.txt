Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Blastinus
Monster
Virgo
46
64
Byblos










Rocl
Male
Scorpio
59
66
Squire
White Magic
Catch
Dual Wield
Retreat

Coral Sword
Mythril Sword
Leather Helmet
Diamond Armor
Red Shoes

Heal, Yell, Wish
Cure 4, Raise, Raise 2, Protect, Shell 2, Wall, Esuna



TheBrett
Male
Leo
70
77
Samurai
Steal
Absorb Used MP
Magic Defense UP
Ignore Terrain

Kiyomori

Gold Helmet
Bronze Armor
Magic Ring

Asura, Kikuichimoji
Steal Weapon, Leg Aim



Forkmore
Male
Libra
37
52
Geomancer
White Magic
MP Restore
Magic Attack UP
Move-MP Up

Slasher
Round Shield
Twist Headband
Light Robe
Angel Ring

Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure, Cure 3, Cure 4, Raise, Raise 2, Protect, Shell 2, Esuna

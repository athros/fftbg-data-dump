Player: !Green
Team: Green Team
Palettes: Green/White



Mizzmoonshine
Female
Leo
54
51
Oracle
White Magic
Counter Tackle
Short Charge
Ignore Terrain

Star Bag

Holy Miter
Black Robe
Genji Gauntlet

Blind, Dispel Magic, Paralyze
Cure 3, Cure 4, Raise, Regen, Protect 2, Shell, Shell 2, Esuna, Holy



Galkife
Male
Leo
52
63
Monk
Basic Skill
Faith Save
Equip Armor
Jump+1



Genji Helmet
Earth Clothes
Bracer

Wave Fist, Purification, Chakra, Revive
Dash, Heal, Yell, Cheer Up, Fury, Scream



Dinin991
Male
Aquarius
50
46
Time Mage
Sing
Speed Save
Equip Knife
Move+3

Koga Knife

Green Beret
Clothes
Jade Armlet

Haste, Stop, Immobilize, Float, Quick, Demi, Demi 2, Meteor
Cheer Song, Battle Song, Magic Song, Last Song



MyFakeLife
Female
Leo
59
47
Knight
Elemental
Mana Shield
Equip Polearm
Waterwalking

Persia
Ice Shield
Diamond Helmet
Diamond Armor
Magic Gauntlet

Shield Break, Magic Break, Speed Break, Mind Break, Justice Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind

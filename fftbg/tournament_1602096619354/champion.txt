Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Dogsandcatsand
Male
Cancer
70
39
Ninja
Basic Skill
MP Restore
Martial Arts
Move-HP Up



Headgear
Earth Clothes
Bracer

Bomb, Knife
Heal, Cheer Up, Fury



Latebit
Male
Aries
55
46
Chemist
Time Magic
HP Restore
Equip Shield
Move-HP Up

Dagger
Escutcheon
Golden Hairpin
Black Costume
Spike Shoes

Potion, Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Slow, Stop, Float, Demi, Stabilize Time, Meteor



Chuckolator
Male
Cancer
48
40
Archer
Black Magic
Parry
Equip Armor
Move-HP Up

Mythril Gun
Aegis Shield
Ribbon
Gold Armor
Feather Mantle

Charge+2, Charge+3, Charge+5
Fire 2, Fire 3, Fire 4, Ice 2, Ice 3, Ice 4



Sharosa
Male
Libra
80
63
Squire
Item
Faith Save
Throw Item
Levitate

Star Bag
Escutcheon
Red Hood
Clothes
Power Wrist

Dash, Heal, Yell, Fury, Wish
Potion, Antidote, Soft, Phoenix Down

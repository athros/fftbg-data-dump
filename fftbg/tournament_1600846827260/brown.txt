Player: !Brown
Team: Brown Team
Palettes: Brown/Green



OneHundredFists
Female
Aquarius
56
52
Dancer
Time Magic
Sunken State
Doublehand
Move-HP Up

Star Bag

Golden Hairpin
Judo Outfit
Magic Gauntlet

Last Dance, Void Storage, Dragon Pit
Haste, Quick, Demi 2, Stabilize Time, Meteor



Zerguzen
Female
Sagittarius
55
54
Lancer
Summon Magic
Counter
Halve MP
Jump+2

Obelisk
Platinum Shield
Cross Helmet
Mythril Armor
Vanish Mantle

Level Jump3, Vertical Jump3
Ramuh, Ifrit, Titan, Salamander, Silf, Lich, Cyclops



Setsaku
Female
Aries
61
58
Summoner
Talk Skill
PA Save
Equip Armor
Ignore Terrain

Thunder Rod

Thief Hat
Leather Armor
Dracula Mantle

Moogle, Shiva, Ifrit, Carbunkle, Silf, Fairy, Lich
Invitation, Persuade, Preach, Insult, Refute



IamEradicate
Female
Scorpio
49
44
Priest
Basic Skill
Regenerator
Short Status
Ignore Height

Gold Staff

Triangle Hat
Wizard Robe
Wizard Mantle

Cure, Cure 2, Raise, Raise 2, Reraise, Regen, Wall, Esuna
Accumulate, Throw Stone, Heal, Tickle, Ultima

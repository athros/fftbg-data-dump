Player: !Red
Team: Red Team
Palettes: Red/Brown



Lolthsmeat
Female
Scorpio
80
53
Archer
Talk Skill
Caution
Attack UP
Waterbreathing

Snipe Bow
Crystal Shield
Feather Hat
Judo Outfit
Genji Gauntlet

Charge+4, Charge+5, Charge+20
Preach, Rehabilitate



Thyrandaal
Female
Libra
64
60
Priest
Math Skill
Hamedo
Equip Shield
Move+3

Rainbow Staff
Diamond Shield
Feather Hat
White Robe
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Protect, Shell, Wall, Esuna
CT, 5, 4, 3



Galkife
Female
Pisces
67
56
Thief
Time Magic
PA Save
Maintenance
Move+1

Orichalcum

Barette
Wizard Outfit
Small Mantle

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Slow 2, Float, Quick, Demi, Stabilize Time



FatTunaKing
Male
Pisces
44
71
Monk
Jump
Hamedo
Halve MP
Ignore Height



Twist Headband
Leather Outfit
N-Kai Armlet

Pummel, Purification
Level Jump8, Vertical Jump6

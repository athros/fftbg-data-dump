Player: !White
Team: White Team
Palettes: White/Blue



Khelor
Monster
Aquarius
60
69
Dragon










MemoriesofFinal
Female
Aries
50
66
Dancer
Steal
Abandon
Short Charge
Lava Walking

Hydra Bag

Thief Hat
Wizard Outfit
Magic Ring

Witch Hunt, Wiznaibus
Steal Helmet, Steal Weapon, Arm Aim



FatTunaKing
Male
Taurus
57
77
Monk
Draw Out
Speed Save
Short Status
Swim



Golden Hairpin
Judo Outfit
Bracer

Purification, Chakra, Revive, Seal Evil
Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



Technominari
Female
Virgo
48
52
Priest
Charge
Hamedo
Magic Defense UP
Move-MP Up

Flame Whip

Green Beret
White Robe
Wizard Mantle

Cure 3, Cure 4, Regen, Protect, Esuna
Charge+1, Charge+2, Charge+3, Charge+4, Charge+7

Player: !White
Team: White Team
Palettes: White/Blue



Bruubarg
Male
Aries
43
66
Summoner
Time Magic
Faith Save
Equip Bow
Retreat

Silver Bow

Golden Hairpin
Secret Clothes
Dracula Mantle

Moogle, Ifrit, Titan, Golem, Carbunkle, Leviathan, Cyclops
Haste, Slow, Immobilize, Quick, Stabilize Time, Meteor



MemoriesofFinal
Male
Libra
62
79
Thief
Basic Skill
Critical Quick
Equip Bow
Move+1

Ultimus Bow

Headgear
Chain Vest
Jade Armlet

Steal Heart, Steal Shield, Steal Weapon, Arm Aim
Tickle, Wish



CassiePhoenix
Male
Aquarius
54
40
Thief
Throw
Caution
Dual Wield
Ignore Height

Blind Knife
Air Knife
Twist Headband
Wizard Outfit
108 Gems

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Shuriken, Staff



Withpaint
Female
Leo
80
57
Mediator
Summon Magic
PA Save
Equip Sword
Lava Walking

Broad Sword

Golden Hairpin
White Robe
Red Shoes

Persuade, Praise, Refute
Moogle, Carbunkle, Leviathan

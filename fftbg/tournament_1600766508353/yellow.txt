Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Go2sleepTV
Male
Gemini
50
55
Samurai
White Magic
Parry
Dual Wield
Lava Walking

Mythril Spear
Javelin
Cross Helmet
Chain Mail
Angel Ring

Murasame
Cure, Raise, Raise 2, Reraise, Protect 2, Shell 2, Esuna



NemhainRuaidhe
Male
Pisces
79
80
Time Mage
Summon Magic
MP Restore
Maintenance
Move-HP Up

Rainbow Staff

Ribbon
Black Costume
Leather Mantle

Haste, Slow, Stop, Float, Quick, Demi, Demi 2
Carbunkle, Silf



NicoSavoy
Male
Capricorn
68
54
Monk
Throw
Brave Save
Equip Gun
Move-HP Up

Blaze Gun

Twist Headband
Power Sleeve
Germinas Boots

Spin Fist, Wave Fist, Purification, Chakra, Seal Evil
Bomb, Wand



Setsaku
Male
Capricorn
39
64
Monk
Item
Brave Save
Equip Polearm
Move-HP Up

Ryozan Silk

Red Hood
Earth Clothes
Feather Boots

Pummel, Wave Fist, Earth Slash, Secret Fist, Chakra
Hi-Potion, Hi-Ether, Eye Drop, Maiden's Kiss

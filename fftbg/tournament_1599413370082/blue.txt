Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DouglasDragonThePoet
Female
Taurus
81
40
Archer
Battle Skill
Absorb Used MP
Martial Arts
Waterwalking

Glacier Gun
Bronze Shield
Black Hood
Earth Clothes
Chantage

Charge+2, Charge+5, Charge+10
Weapon Break, Power Break



Foamsoldier
Female
Gemini
74
51
Monk
Elemental
PA Save
Equip Armor
Ignore Terrain



Crystal Helmet
Chain Mail
Dracula Mantle

Wave Fist, Secret Fist, Purification, Chakra, Revive
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Just Here2
Female
Taurus
55
66
Wizard
Draw Out
Brave Save
Defend
Jump+3

Poison Rod

Red Hood
Mystic Vest
Wizard Mantle

Fire 2, Fire 3, Bolt 2, Ice, Ice 3, Frog
Kiyomori



AniZero
Female
Gemini
52
58
Samurai
Summon Magic
HP Restore
Magic Attack UP
Waterwalking

Bizen Boat

Diamond Helmet
Linen Robe
108 Gems

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Moogle, Shiva, Ramuh, Golem, Leviathan

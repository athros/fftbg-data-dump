Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ANFz
Male
Virgo
71
49
Geomancer
Steal
Sunken State
Equip Bow
Lava Walking

Bow Gun
Buckler
Flash Hat
Mythril Vest
Dracula Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Shield, Steal Weapon, Steal Status, Leg Aim



Omegasuspekt
Female
Libra
63
36
Priest
Charge
Auto Potion
Short Charge
Move-MP Up

Flame Whip

Headgear
Chameleon Robe
Jade Armlet

Cure, Cure 3, Raise, Raise 2, Reraise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna, Holy
Charge+1, Charge+2



Resjudicata3
Female
Cancer
43
58
Wizard
Draw Out
Meatbone Slash
Short Charge
Move-HP Up

Poison Rod

Red Hood
Silk Robe
Bracer

Bolt 2, Bolt 3
Koutetsu, Murasame, Kiyomori



DLJuggernaut
Female
Aquarius
54
41
Wizard
Dance
PA Save
Martial Arts
Waterbreathing

Rod

Triangle Hat
Chameleon Robe
Small Mantle

Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice 3, Empower, Frog
Witch Hunt, Wiznaibus, Polka Polka, Last Dance, Void Storage

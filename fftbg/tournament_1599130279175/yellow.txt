Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Brenogarwin
Female
Cancer
58
46
Chemist
White Magic
Auto Potion
Equip Armor
Swim

Assassin Dagger

Golden Hairpin
Silk Robe
Battle Boots

Ether, Eye Drop, Phoenix Down
Cure, Cure 2, Cure 3, Raise, Regen, Protect, Shell, Shell 2, Esuna, Holy



Lijarkh
Male
Cancer
60
47
Monk
Talk Skill
MA Save
Doublehand
Move+3



Green Beret
Earth Clothes
Feather Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Praise, Solution, Mimic Daravon, Rehabilitate



AltimaMantoid
Male
Taurus
75
49
Oracle
Summon Magic
MA Save
Magic Attack UP
Retreat

Papyrus Codex

Red Hood
Judo Outfit
Sprint Shoes

Blind, Life Drain, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Moogle, Ramuh, Odin, Leviathan, Silf, Lich



Lorita
Female
Virgo
57
62
Chemist
Draw Out
Hamedo
Equip Knife
Jump+1

Hidden Knife

Twist Headband
Judo Outfit
Reflect Ring

Potion, Hi-Potion, Ether, Phoenix Down
Asura, Koutetsu, Kikuichimoji

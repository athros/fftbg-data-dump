Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



FriendSquirrel
Male
Libra
58
78
Time Mage
Throw
Abandon
Equip Bow
Move-MP Up

Mythril Bow

Triangle Hat
Light Robe
Vanish Mantle

Slow 2, Float, Reflect, Demi, Stabilize Time
Bomb, Staff, Axe



Smashy
Male
Leo
74
69
Ninja
Time Magic
Distribute
Defense UP
Teleport

Short Edge
Kunai
Headgear
Adaman Vest
Power Wrist

Shuriken, Bomb, Hammer
Slow 2, Stop, Reflect, Quick, Demi 2, Stabilize Time, Meteor



Zagorsek
Female
Cancer
67
78
Mime

Caution
Equip Armor
Jump+3



Diamond Helmet
Diamond Armor
Diamond Armlet

Mimic




Leonidusx
Male
Gemini
41
67
Lancer
Summon Magic
Auto Potion
Magic Defense UP
Teleport

Holy Lance
Round Shield
Circlet
Bronze Armor
Sprint Shoes

Level Jump4, Vertical Jump8
Moogle, Golem, Odin, Lich, Cyclops

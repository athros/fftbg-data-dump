Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BaronHaynes
Male
Pisces
67
61
Chemist
Elemental
Critical Quick
Sicken
Jump+2

Panther Bag

Headgear
Secret Clothes
Magic Gauntlet

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Holy Water, Phoenix Down
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



3ngag3
Female
Cancer
56
75
Thief
Jump
Critical Quick
Sicken
Teleport

Ancient Sword

Thief Hat
Mystic Vest
Defense Ring

Steal Helmet, Steal Armor, Steal Weapon, Arm Aim
Level Jump8, Vertical Jump3



Yangusburger
Male
Libra
58
47
Chemist
Talk Skill
Earplug
Equip Gun
Move-HP Up

Romanda Gun

Green Beret
Adaman Vest
Dracula Mantle

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down
Preach, Death Sentence, Refute



DeathTaxesAndAnime
Female
Gemini
64
49
Mediator
Item
Catch
Equip Sword
Move+2

Platinum Sword

Red Hood
Wizard Robe
Elf Mantle

Praise, Preach, Insult, Refute, Rehabilitate
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Remedy, Phoenix Down

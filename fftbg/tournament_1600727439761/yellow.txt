Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Phi Sig
Female
Libra
55
50
Calculator
Black Magic
Abandon
Equip Bow
Move-HP Up

Windslash Bow

Leather Hat
Black Robe
Dracula Mantle

CT, Prime Number, 4, 3
Fire, Fire 3, Bolt 2, Bolt 4, Ice, Ice 2, Ice 4



Willjin
Male
Sagittarius
54
47
Samurai
Jump
MA Save
Equip Axe
Lava Walking

Wizard Staff

Barbuta
Plate Mail
Cherche

Asura, Bizen Boat, Heaven's Cloud
Level Jump5, Vertical Jump8



Lord Gwarth
Male
Gemini
49
50
Archer
Battle Skill
Abandon
Doublehand
Ignore Height

Windslash Bow

Headgear
Black Costume
Feather Boots

Charge+10, Charge+20
Head Break, Weapon Break, Magic Break, Power Break



Leonidusx
Male
Capricorn
41
57
Lancer
Battle Skill
Dragon Spirit
Equip Armor
Jump+2

Holy Lance
Mythril Shield
Holy Miter
Earth Clothes
Bracer

Level Jump3, Vertical Jump8
Head Break, Speed Break, Night Sword

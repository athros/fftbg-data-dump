Final Bets: black - 9 bets for 3,561G (51.4%, x0.95); brown - 8 bets for 3,368G (48.6%, x1.06)

black bets:
Sairentozon7: 1,000G (28.1%, 30,134G)
BirbBrainsBot: 945G (26.5%, 170,704G)
FriendSquirrel: 400G (11.2%, 400G)
getthemoneyz: 296G (8.3%, 2,076,052G)
scsuperstar: 220G (6.2%, 220G)
AllInBot: 200G (5.6%, 200G)
VynxYukida: 200G (5.6%, 1,019G)
HorusTaurus: 200G (5.6%, 8,411G)
Genkidou: 100G (2.8%, 1,322G)

brown bets:
brokenknight201: 900G (26.7%, 25,317G)
Error72: 625G (18.6%, 625G)
latebit: 616G (18.3%, 616G)
bigbongsmoker: 340G (10.1%, 11,244G)
MemoriesofFinal: 300G (8.9%, 300G)
Zachara: 287G (8.5%, 152,087G)
datadrivenbot: 200G (5.9%, 75,554G)
CosmicTactician: 100G (3.0%, 26,299G)

Player: !White
Team: White Team
Palettes: White/Blue



Genkidou
Female
Virgo
54
46
Lancer
Summon Magic
Caution
Equip Knife
Teleport

Ninja Edge
Aegis Shield
Grand Helmet
White Robe
Feather Boots

Level Jump4, Vertical Jump5
Titan, Golem, Carbunkle, Odin, Lich



Scsuperstar
Male
Taurus
44
74
Calculator
Limit
Absorb Used MP
Short Status
Ignore Height

Thunder Rod
Aegis Shield
Holy Miter
Bronze Armor
Bracer

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Silentkaster
Female
Capricorn
45
58
Priest
Elemental
Abandon
Short Charge
Move-HP Up

Flame Whip

Holy Miter
Chameleon Robe
Defense Armlet

Cure, Cure 4, Raise, Reraise, Regen, Protect 2, Esuna, Holy
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind



Error72
Male
Virgo
50
42
Knight
Item
Abandon
Sicken
Waterbreathing

Defender
Ice Shield
Circlet
Silk Robe
Leather Mantle

Weapon Break, Speed Break
Potion, Hi-Potion, Ether, Antidote, Phoenix Down

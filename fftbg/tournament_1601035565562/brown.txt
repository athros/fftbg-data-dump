Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SephDarkheart
Female
Cancer
77
77
Priest
Summon Magic
Speed Save
Equip Sword
Fly

Defender

Leather Hat
Silk Robe
Magic Ring

Cure, Cure 2, Cure 3, Raise 2, Protect, Shell, Shell 2, Esuna, Holy
Moogle, Ifrit, Titan



Bigbongsmoker
Female
Pisces
77
62
Thief
Time Magic
Absorb Used MP
Equip Bow
Ignore Height

Snipe Bow

Triangle Hat
Wizard Outfit
Dracula Mantle

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Leg Aim
Haste 2, Slow 2, Immobilize, Reflect, Demi 2, Meteor



Thyrandaal
Male
Taurus
53
63
Mime

MA Save
Dual Wield
Jump+3



Leather Hat
Chain Vest
Wizard Mantle

Mimic




MemoriesofFinal
Male
Scorpio
65
75
Archer
Elemental
Earplug
Doublehand
Jump+2

Lightning Bow

Triangle Hat
Mystic Vest
Rubber Shoes

Charge+3, Charge+4, Charge+5, Charge+20
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

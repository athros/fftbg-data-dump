Final Bets: red - 8 bets for 56,228G (94.6%, x0.06); blue - 8 bets for 3,192G (5.4%, x17.62)

red bets:
MemoriesofFinal: 54,323G (96.6%, 54,323G)
ForagerCats: 500G (0.9%, 5,148G)
SephDarkheart: 400G (0.7%, 14,845G)
Galeronis: 276G (0.5%, 276G)
AllInBot: 229G (0.4%, 229G)
datadrivenbot: 200G (0.4%, 75,760G)
silentkaster: 200G (0.4%, 2,015G)
CosmicTactician: 100G (0.2%, 25,302G)

blue bets:
skipsandwiches: 1,001G (31.4%, 90,451G)
BirbBrainsBot: 754G (23.6%, 155,703G)
getthemoneyz: 396G (12.4%, 2,067,940G)
bigbongsmoker: 340G (10.7%, 1,723G)
Carledo: 333G (10.4%, 118,608G)
TheSabretoothe: 168G (5.3%, 168G)
Bongomon7: 100G (3.1%, 3,419G)
HorusTaurus: 100G (3.1%, 6,590G)

Final Bets: white - 9 bets for 5,600G (41.9%, x1.38); black - 10 bets for 7,755G (58.1%, x0.72)

white bets:
ser_pyrro: 1,406G (25.1%, 1,406G)
resjudicata3: 1,353G (24.2%, 2,654G)
upvla: 777G (13.9%, 1,971G)
bruubarg: 600G (10.7%, 600G)
HaplessOne: 568G (10.1%, 568G)
hotpocketsofficial: 312G (5.6%, 312G)
king_smashington: 216G (3.9%, 216G)
datadrivenbot: 200G (3.6%, 64,417G)
peeronid: 168G (3.0%, 168G)

black bets:
Arcblazer23: 3,692G (47.6%, 3,692G)
AllInBot: 1,921G (24.8%, 1,921G)
Lythe_Caraker: 500G (6.4%, 151,768G)
BirbBrainsBot: 426G (5.5%, 52,393G)
ColetteMSLP: 400G (5.2%, 3,676G)
Chihuahua_Charity: 260G (3.4%, 260G)
getthemoneyz: 256G (3.3%, 1,974,114G)
DeathTaxesAndAnime: 100G (1.3%, 8,700G)
MemoriesofFinal: 100G (1.3%, 34,851G)
victoriolue: 100G (1.3%, 93,971G)

Final Bets: red - 7 bets for 5,426G (44.6%, x1.24); yellow - 11 bets for 6,733G (55.4%, x0.81)

red bets:
NovaKnight21: 3,000G (55.3%, 10,062G)
hotpocketsofficial: 812G (15.0%, 812G)
king_smashington: 716G (13.2%, 716G)
3ngag3: 348G (6.4%, 13,962G)
IaibOaob: 250G (4.6%, 1,763G)
AllInBot: 200G (3.7%, 200G)
DeathTaxesAndAnime: 100G (1.8%, 8,600G)

yellow bets:
HaplessOne: 1,738G (25.8%, 3,408G)
BirbBrainsBot: 1,000G (14.9%, 52,741G)
upvla: 1,000G (14.9%, 3,547G)
peeronid: 901G (13.4%, 901G)
ser_pyrro: 500G (7.4%, 3,353G)
Lythe_Caraker: 500G (7.4%, 153,042G)
rocl: 424G (6.3%, 8,292G)
resjudicata3: 200G (3.0%, 2,721G)
datadrivenbot: 200G (3.0%, 64,849G)
getthemoneyz: 170G (2.5%, 1,973,676G)
MemoriesofFinal: 100G (1.5%, 35,328G)

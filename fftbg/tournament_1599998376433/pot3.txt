Final Bets: white - 8 bets for 3,051G (37.5%, x1.67); black - 9 bets for 5,089G (62.5%, x0.60)

white bets:
nifboy: 736G (24.1%, 6,371G)
BaronHaynes: 500G (16.4%, 17,581G)
ser_pyrro: 500G (16.4%, 5,378G)
ninjaboy3234: 396G (13.0%, 396G)
skipsandwiches: 392G (12.8%, 7,308G)
ValensEXP_: 288G (9.4%, 8,126G)
getthemoneyz: 138G (4.5%, 1,932,572G)
gorgewall: 101G (3.3%, 10,606G)

black bets:
VolgraTheMoose: 1,688G (33.2%, 1,688G)
BirbBrainsBot: 1,000G (19.7%, 93,654G)
Leonidusx: 1,000G (19.7%, 8,688G)
Raixelol: 600G (11.8%, 16,957G)
AllInBot: 201G (3.9%, 201G)
datadrivenbot: 200G (3.9%, 62,956G)
Zachara: 200G (3.9%, 144,700G)
BartTradingCompany: 100G (2.0%, 8,660G)
Error72: 100G (2.0%, 54,204G)

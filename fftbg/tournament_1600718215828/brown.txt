Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Run With Stone GUNs
Male
Scorpio
54
66
Mediator
Basic Skill
Faith Save
Magic Attack UP
Levitate

Blast Gun

Headgear
Linen Robe
Reflect Ring

Persuade, Threaten, Solution, Insult, Negotiate, Mimic Daravon, Rehabilitate
Throw Stone, Cheer Up, Fury, Wish, Scream, Ultima



BoneMiser
Monster
Scorpio
44
66
Hydra










Vorap
Male
Aquarius
67
53
Oracle
Charge
Sunken State
Equip Axe
Waterbreathing

Morning Star

Holy Miter
Silk Robe
Bracer

Blind, Pray Faith, Dispel Magic, Petrify, Dark Holy
Charge+1, Charge+2, Charge+7



JonnyCue
Female
Pisces
78
71
Summoner
Punch Art
Parry
Long Status
Waterbreathing

Papyrus Codex

Holy Miter
White Robe
Feather Mantle

Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Odin, Salamander, Silf, Fairy, Lich
Pummel, Secret Fist, Purification, Revive

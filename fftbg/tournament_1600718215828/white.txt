Player: !White
Team: White Team
Palettes: White/Blue



Error72
Male
Aquarius
62
36
Knight
Elemental
Speed Save
Equip Bow
Retreat

Long Bow
Round Shield
Leather Helmet
Linen Robe
Magic Gauntlet

Shield Break, Stasis Sword
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Just Here2
Male
Aquarius
72
62
Monk
Item
Critical Quick
Short Status
Levitate



Green Beret
Mystic Vest
Red Shoes

Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil
X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down



Tripaplex
Female
Aquarius
64
67
Time Mage
Item
HP Restore
Equip Sword
Move+2

Defender

Red Hood
Black Robe
Leather Mantle

Slow, Slow 2, Immobilize, Float, Reflect, Demi, Demi 2
Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down



DeathTaxesAndAnime
Female
Leo
38
69
Thief
Dance
Critical Quick
Halve MP
Waterwalking

Coral Sword

Leather Hat
Secret Clothes
Leather Mantle

Steal Helmet, Steal Shield, Steal Weapon
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Void Storage

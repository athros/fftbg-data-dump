Player: !White
Team: White Team
Palettes: White/Blue



Ser Pyrro
Male
Virgo
58
53
Chemist
White Magic
MA Save
Maintenance
Jump+3

Cute Bag

Twist Headband
Adaman Vest
Feather Boots

Potion, Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Raise, Protect, Shell 2, Wall



Sairentozon7
Female
Gemini
55
71
Calculator
Black Magic
Caution
Equip Armor
Ignore Terrain

Papyrus Codex

Leather Helmet
Leather Armor
Defense Ring

CT, Height, Prime Number, 4, 3
Bolt, Ice 3, Ice 4, Death



Khelor
Female
Scorpio
51
47
Chemist
Charge
Faith Save
Long Status
Move-HP Up

Hydra Bag

Headgear
Chain Vest
Jade Armlet

Potion, Hi-Potion, X-Potion, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Charge+2, Charge+3, Charge+5



FriendlySkeleton
Monster
Capricorn
43
72
Great Malboro








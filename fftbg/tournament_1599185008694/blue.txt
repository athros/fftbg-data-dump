Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Redmage4evah
Male
Libra
77
58
Monk
Elemental
Critical Quick
Concentrate
Jump+3



Green Beret
Black Costume
Rubber Shoes

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Leonidusx
Male
Libra
51
59
Thief
Draw Out
Sunken State
Equip Gun
Move+2

Papyrus Codex

Twist Headband
Clothes
Magic Ring

Steal Helmet, Steal Status
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji



Butterbelljedi
Female
Virgo
64
57
Priest
Jump
Auto Potion
Beastmaster
Swim

White Staff

Black Hood
Black Robe
Dracula Mantle

Cure, Cure 2, Raise 2, Reraise, Protect 2
Level Jump8, Vertical Jump7



FriendSquirrel
Female
Taurus
54
50
Summoner
Yin Yang Magic
Arrow Guard
Short Charge
Waterbreathing

Flame Rod

Headgear
Wizard Robe
Defense Ring

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin, Leviathan, Salamander
Spell Absorb, Life Drain, Foxbird, Confusion Song, Dispel Magic, Sleep

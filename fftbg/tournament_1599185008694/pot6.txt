Final Bets: white - 23 bets for 14,081G (70.7%, x0.41); brown - 14 bets for 5,841G (29.3%, x2.41)

white bets:
SkylerBunny: 2,100G (14.9%, 97,528G)
BaronHaynes: 1,000G (7.1%, 71,615G)
Virilikus: 1,000G (7.1%, 220,749G)
Dasutin23: 967G (6.9%, 967G)
Spartan_Paladin: 890G (6.3%, 17,394G)
E_Ballard: 872G (6.2%, 132,719G)
NicoSavoy: 761G (5.4%, 761G)
Laserman1000: 748G (5.3%, 748G)
Zeroroute: 734G (5.2%, 734G)
letdowncity: 556G (3.9%, 48,883G)
CosmicTactician: 500G (3.6%, 45,861G)
RobotOcelot: 500G (3.6%, 17,041G)
Yangusburger: 500G (3.6%, 15,044G)
CorpusCav: 500G (3.6%, 7,000G)
Arcblazer23: 412G (2.9%, 412G)
thunderducker: 333G (2.4%, 1,466G)
MemoriesofFinal: 328G (2.3%, 328G)
krombobreaker: 284G (2.0%, 7,593G)
Runeseeker22: 252G (1.8%, 559G)
killth3kid: 250G (1.8%, 15,512G)
poorest_hobo: 244G (1.7%, 244G)
datadrivenbot: 200G (1.4%, 64,274G)
formerlydrdong: 150G (1.1%, 1,879G)

brown bets:
BirbBrainsBot: 1,000G (17.1%, 88,942G)
Mesmaster: 1,000G (17.1%, 168,637G)
Thyrandaal: 612G (10.5%, 612G)
reinoe: 548G (9.4%, 548G)
minorfffanatic1: 484G (8.3%, 484G)
Zerguzen: 441G (7.6%, 441G)
dogsandcatsand: 436G (7.5%, 1,372G)
Blackpapalink: 300G (5.1%, 1,274G)
getthemoneyz: 220G (3.8%, 1,814,849G)
KasugaiRoastedPeas: 200G (3.4%, 1,799G)
FFMaster: 200G (3.4%, 3,735G)
Leonidusx: 200G (3.4%, 940G)
greengreenss: 100G (1.7%, 367G)
Twisted_Nutsatchel: 100G (1.7%, 18,984G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Highelectricaltemperature
Female
Libra
55
64
Oracle
Math Skill
Meatbone Slash
Equip Sword
Move+3

Muramasa

Ribbon
Power Sleeve
N-Kai Armlet

Blind, Poison, Silence Song, Foxbird, Dispel Magic, Sleep
CT, Prime Number, 5



Dogsandcatsand
Female
Pisces
71
46
Wizard
Punch Art
Parry
Defend
Levitate

Air Knife

Green Beret
Mythril Vest
Defense Armlet

Fire 2, Bolt 3, Bolt 4, Ice, Ice 4
Secret Fist, Purification, Revive



Maakur
Male
Aries
49
50
Priest
Black Magic
Counter Tackle
Equip Shield
Teleport

Flame Whip
Round Shield
Red Hood
Silk Robe
Wizard Mantle

Cure 3, Raise, Raise 2, Regen, Shell 2
Fire, Ice 3, Ice 4, Frog



Killth3kid
Male
Leo
67
61
Chemist
Jump
MA Save
Equip Polearm
Lava Walking

Spear

Green Beret
Chain Vest
Wizard Mantle

Potion, Ether, Hi-Ether, Eye Drop
Level Jump3, Vertical Jump8

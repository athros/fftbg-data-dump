Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Aceof86
Male
Taurus
75
47
Archer
Talk Skill
Regenerator
Equip Sword
Move+2

Ragnarok

Triangle Hat
Wizard Outfit
Magic Gauntlet

Charge+1, Charge+3, Charge+7, Charge+10
Invitation, Praise, Preach, Negotiate, Refute



Humbertogeovane
Female
Gemini
50
52
Mediator
Punch Art
Critical Quick
Equip Sword
Jump+3

Koutetsu Knife

Holy Miter
Power Sleeve
Magic Ring

Invitation, Persuade, Preach, Death Sentence, Insult, Mimic Daravon, Refute
Purification, Chakra, Revive



Targaso
Female
Sagittarius
56
55
Squire
Charge
Hamedo
Equip Sword
Jump+1

Save the Queen

Leather Helmet
Mystic Vest
Jade Armlet

Accumulate, Dash, Yell, Cheer Up, Fury, Ultima
Charge+1, Charge+3, Charge+7, Charge+20



Mushufasa
Female
Pisces
66
52
Oracle
White Magic
Mana Shield
Defend
Ignore Height

Ivory Rod

Feather Hat
Chameleon Robe
Leather Mantle

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Cure 2, Cure 4, Raise, Shell, Wall, Esuna

Player: !White
Team: White Team
Palettes: White/Blue



RunicMagus
Female
Sagittarius
72
40
Squire
Talk Skill
Speed Save
Short Charge
Jump+3

Long Sword
Venetian Shield
Triangle Hat
Judo Outfit
Wizard Mantle

Accumulate, Throw Stone, Heal, Fury, Wish, Scream
Invitation, Persuade, Threaten, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate



Seppu777
Monster
Virgo
72
53
Plague










ArchKnightX
Female
Gemini
74
67
Priest
Summon Magic
Mana Shield
Concentrate
Ignore Height

Healing Staff

Headgear
Chain Vest
Genji Gauntlet

Cure, Cure 2, Cure 3, Raise, Regen, Protect, Esuna
Moogle, Shiva, Ifrit, Golem, Carbunkle, Odin, Leviathan, Salamander, Silf, Cyclops



Just Here2
Male
Capricorn
53
77
Ninja
White Magic
PA Save
Equip Gun
Jump+2

Mythril Gun
Blaze Gun
Barette
Power Sleeve
Elf Mantle

Bomb, Stick
Cure 2, Cure 3, Raise 2, Protect 2, Esuna

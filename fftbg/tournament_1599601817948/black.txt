Player: !Black
Team: Black Team
Palettes: Black/Red



Error72
Female
Virgo
54
48
Mediator
Draw Out
Critical Quick
Dual Wield
Teleport

Bestiary
Battle Folio
Red Hood
Wizard Robe
Reflect Ring

Invitation, Persuade, Refute, Rehabilitate
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



W0RLDEND
Female
Libra
55
63
Lancer
Elemental
Earplug
Equip Knife
Ignore Terrain

Ninja Edge
Crystal Shield
Mythril Helmet
Linen Robe
Cursed Ring

Level Jump5, Vertical Jump5
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



Silentkaster
Female
Scorpio
40
40
Summoner
Elemental
Mana Shield
Short Charge
Jump+1

Wizard Staff

Ribbon
Black Robe
Spike Shoes

Moogle, Ifrit, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Lich
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Old Overholt
Male
Scorpio
47
48
Knight
Time Magic
Damage Split
Concentrate
Jump+3

Save the Queen
Mythril Shield
Barbuta
Leather Armor
Bracer

Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword
Haste, Haste 2, Stabilize Time

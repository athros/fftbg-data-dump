Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



King Smashington
Female
Aquarius
51
64
Summoner
Talk Skill
Brave Save
Beastmaster
Teleport

Papyrus Codex

Flash Hat
Black Robe
Bracer

Moogle, Shiva, Ifrit, Titan, Golem, Lich
Invitation, Persuade, Praise, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



Zagorsek
Female
Aries
69
79
Mime

MP Restore
Doublehand
Move-MP Up



Leather Hat
Judo Outfit
Wizard Mantle

Mimic




Krombobreaker
Female
Libra
73
56
Samurai
Throw
Meatbone Slash
Beastmaster
Waterbreathing

Dragon Whisker

Leather Helmet
Wizard Robe
Magic Gauntlet

Bizen Boat, Heaven's Cloud, Kikuichimoji
Shuriken



AbandonedHall
Male
Aries
41
49
Squire
Time Magic
MP Restore
Short Status
Jump+2

Slasher
Genji Shield
Holy Miter
Leather Armor
Power Wrist

Accumulate, Heal
Haste 2, Stop, Galaxy Stop

Player: !White
Team: White Team
Palettes: White/Blue



Widewalk
Female
Capricorn
42
77
Archer
Summon Magic
Distribute
Defend
Ignore Terrain

Blast Gun
Bronze Shield
Leather Helmet
Earth Clothes
Magic Ring

Charge+2, Charge+7
Moogle, Shiva, Ramuh, Titan, Carbunkle, Odin, Leviathan, Salamander



Gorgewall
Male
Sagittarius
51
45
Monk
Elemental
Counter
Long Status
Move+2



Twist Headband
Adaman Vest
Jade Armlet

Pummel, Purification, Revive
Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard



Mirapoix
Male
Pisces
73
73
Ninja
Talk Skill
Damage Split
Monster Talk
Move+3

Sasuke Knife
Kunai
Holy Miter
Chain Vest
Genji Gauntlet

Shuriken, Bomb
Praise, Refute, Rehabilitate



OneHundredFists
Male
Pisces
59
51
Bard
Item
Parry
Throw Item
Move+2

Fairy Harp

Holy Miter
Wizard Outfit
Feather Mantle

Life Song, Cheer Song, Battle Song, Nameless Song
Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down

Final Bets: red - 17 bets for 16,751G (54.0%, x0.85); blue - 22 bets for 14,253G (46.0%, x1.18)

red bets:
Zagorsek: 5,148G (30.7%, 5,148G)
DeathTaxesAndAnime: 2,222G (13.3%, 22,001G)
E_Ballard: 2,001G (11.9%, 2,001G)
CorpusCav: 2,000G (11.9%, 101,898G)
Nizaha: 1,252G (7.5%, 2,505G)
DHaveWord: 837G (5.0%, 837G)
mirapoix: 693G (4.1%, 693G)
technominari: 500G (3.0%, 95,133G)
Zetchryn: 492G (2.9%, 492G)
lowlf: 420G (2.5%, 2,188G)
ChiChiRodriguez: 276G (1.6%, 276G)
Demon_Lord_Josh: 260G (1.6%, 260G)
datadrivenbot: 200G (1.2%, 86,357G)
DouglasDragonThePoet: 150G (0.9%, 3,053G)
king_smashington: 100G (0.6%, 2,874G)
RunicMagus: 100G (0.6%, 32,991G)
DocZombiee: 100G (0.6%, 1,646G)

blue bets:
HorusTaurus: 2,827G (19.8%, 2,827G)
nifboy: 1,373G (9.6%, 1,373G)
BirbBrainsBot: 1,000G (7.0%, 125,944G)
getthemoneyz: 1,000G (7.0%, 2,228,348G)
DavenIII: 1,000G (7.0%, 7,846G)
killth3kid: 876G (6.1%, 28,134G)
Thyrandaal: 864G (6.1%, 864G)
Mudrockk: 737G (5.2%, 737G)
Sairentozon7: 716G (5.0%, 716G)
dogsandcatsand: 592G (4.2%, 5,565G)
old_overholt_: 500G (3.5%, 8,569G)
joharbelwey: 500G (3.5%, 617G)
Lionhermit: 500G (3.5%, 5,598G)
DLJuggernaut: 412G (2.9%, 412G)
TheBrett: 348G (2.4%, 3,970G)
Zachara: 204G (1.4%, 2,004G)
AllInBot: 200G (1.4%, 200G)
squideater_: 200G (1.4%, 4,488G)
roofiepops: 155G (1.1%, 2,443G)
Firesheath: 100G (0.7%, 8,004G)
FuzzyTigers: 100G (0.7%, 756G)
goth_Applebees_hostess: 49G (0.3%, 449G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Dogsandcatsand
Male
Libra
51
46
Calculator
White Magic
Counter
Equip Gun
Waterwalking

Ramia Harp

Flash Hat
Earth Clothes
Feather Mantle

CT, Prime Number, 5, 4
Cure 2, Raise, Protect, Protect 2, Esuna



StealthModeLocke
Female
Serpentarius
59
55
Time Mage
White Magic
Faith Save
Equip Polearm
Lava Walking

Iron Fan

Green Beret
Robe of Lords
Cursed Ring

Haste, Slow 2, Immobilize, Float, Quick, Demi, Meteor
Raise 2, Reraise, Protect



SephDarkheart
Female
Sagittarius
77
72
Archer
Throw
Absorb Used MP
Equip Bow
Move+2

Hydra Bag
Ice Shield
Black Hood
Leather Outfit
Cursed Ring

Charge+1, Charge+2, Charge+4, Charge+7
Spear



Just Here2
Female
Virgo
80
70
Priest
Punch Art
Counter Flood
Equip Polearm
Swim

Musk Rod

Flash Hat
Silk Robe
Feather Boots

Cure 3, Raise, Regen, Protect, Shell 2, Wall, Esuna
Spin Fist, Purification, Revive

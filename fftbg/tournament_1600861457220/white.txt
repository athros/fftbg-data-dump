Player: !White
Team: White Team
Palettes: White/Blue



Error72
Male
Libra
60
68
Archer
Talk Skill
Sunken State
Monster Talk
Levitate

Stone Gun
Platinum Shield
Leather Hat
Mystic Vest
Vanish Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Persuade, Preach, Solution, Death Sentence, Refute



King Smashington
Male
Sagittarius
47
53
Summoner
Punch Art
Arrow Guard
Concentrate
Jump+1

Battle Folio

Thief Hat
Linen Robe
Defense Armlet

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Odin, Fairy, Cyclops
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Otakutaylor
Monster
Virgo
69
41
Trent










AltimaMantoid
Female
Aquarius
48
54
Mediator
Battle Skill
Caution
Attack UP
Levitate

Stone Gun

Twist Headband
Clothes
Magic Gauntlet

Invitation, Praise, Threaten, Solution, Death Sentence, Insult, Refute
Head Break, Armor Break, Magic Break, Speed Break, Mind Break, Justice Sword

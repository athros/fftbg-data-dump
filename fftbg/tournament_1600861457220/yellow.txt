Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ALY327
Female
Sagittarius
55
49
Mediator
Yin Yang Magic
Critical Quick
Dual Wield
Move+1

Papyrus Codex
Bestiary
Leather Hat
White Robe
Feather Mantle

Threaten, Preach, Solution, Insult, Negotiate, Refute
Life Drain, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze



Smashy
Female
Sagittarius
55
50
Calculator
Black Magic
Counter
Short Charge
Waterbreathing

Battle Folio

Golden Hairpin
Light Robe
Reflect Ring

CT, Prime Number
Fire, Fire 2, Fire 3, Fire 4, Bolt



Chihuahua Charity
Female
Libra
78
42
Lancer
Battle Skill
Earplug
Martial Arts
Ignore Height


Genji Shield
Mythril Helmet
Light Robe
Spike Shoes

Level Jump3, Vertical Jump4
Shield Break, Magic Break, Power Break, Stasis Sword, Night Sword



CassiePhoenix
Female
Pisces
50
64
Mediator
Throw
Hamedo
Maintenance
Swim

Papyrus Codex

Black Hood
Chameleon Robe
Leather Mantle

Solution, Negotiate, Mimic Daravon
Shuriken, Spear

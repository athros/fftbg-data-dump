Final Bets: blue - 8 bets for 11,044G (67.1%, x0.49); brown - 11 bets for 5,403G (32.9%, x2.04)

blue bets:
OneHundredFists: 4,069G (36.8%, 66,772G)
b0lt: 4,069G (36.8%, 18,194G)
BirbBrainsBot: 842G (7.6%, 190,501G)
Zagorsek: 580G (5.3%, 580G)
2b_yorha_b: 500G (4.5%, 35,478G)
Runeseeker22: 449G (4.1%, 449G)
superdevon1: 435G (3.9%, 435G)
smallboops: 100G (0.9%, 2,940G)

brown bets:
CorpusCav: 2,000G (37.0%, 54,844G)
Snowfats: 1,000G (18.5%, 55,725G)
lowlf: 692G (12.8%, 2,321G)
krombobreaker: 500G (9.3%, 37,974G)
YowaneHaku: 400G (7.4%, 5,110G)
AllInBot: 200G (3.7%, 200G)
datadrivenbot: 200G (3.7%, 83,187G)
xUnhingedMoose: 192G (3.6%, 192G)
gorgewall: 101G (1.9%, 5,585G)
nhammen: 100G (1.9%, 19,130G)
getthemoneyz: 18G (0.3%, 2,205,133G)

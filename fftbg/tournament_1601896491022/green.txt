Player: !Green
Team: Green Team
Palettes: Green/White



Genkidou
Male
Capricorn
66
80
Lancer
Battle Skill
Distribute
Secret Hunt
Levitate

Dragon Whisker
Buckler
Genji Helmet
Carabini Mail
Power Wrist

Level Jump8, Vertical Jump8
Head Break, Armor Break, Weapon Break, Speed Break, Mind Break



Evewho
Female
Scorpio
62
65
Knight
Dance
Abandon
Concentrate
Ignore Height

Broad Sword
Platinum Shield
Cross Helmet
Mythril Armor
Magic Gauntlet

Shield Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword, Explosion Sword
Disillusion, Dragon Pit



DeathTaxesAndAnime
Female
Capricorn
73
47
Thief
Jump
Caution
Short Status
Move+3

Blood Sword

Cachusha
Judo Outfit
Diamond Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Level Jump3, Vertical Jump7



DAC169
Female
Aquarius
59
65
Knight
Time Magic
Earplug
Equip Polearm
Teleport

Javelin
Genji Shield
Mythril Helmet
Carabini Mail
Cursed Ring

Shield Break, Weapon Break, Mind Break
Haste, Slow, Reflect, Quick, Demi 2, Stabilize Time

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Just Here2
Male
Aquarius
71
46
Priest
Black Magic
Meatbone Slash
Defend
Move+1

Gold Staff

Black Hood
Chameleon Robe
Dracula Mantle

Cure 3, Raise, Raise 2, Regen, Protect 2, Shell, Esuna, Holy
Fire, Fire 4, Bolt 3, Bolt 4



Dogsandcatsand
Female
Virgo
60
62
Wizard
Jump
Critical Quick
Sicken
Ignore Height

Dragon Rod

Headgear
Mystic Vest
Reflect Ring

Fire 2, Ice, Ice 2, Ice 4, Death
Level Jump8, Vertical Jump5



Error72
Female
Aries
65
60
Wizard
Time Magic
Catch
Short Charge
Levitate

Panther Bag

Headgear
Mystic Vest
Magic Gauntlet

Fire, Fire 2, Bolt, Bolt 2, Ice, Ice 4, Empower
Haste 2, Slow, Stop, Quick, Stabilize Time



Ruebyy
Male
Sagittarius
52
59
Archer
Yin Yang Magic
MP Restore
Equip Bow
Move-MP Up

Long Bow

Twist Headband
Secret Clothes
Spike Shoes

Charge+4, Charge+5, Charge+7
Blind, Spell Absorb, Life Drain, Dispel Magic, Sleep

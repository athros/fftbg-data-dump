Final Bets: blue - 9 bets for 9,795G (60.9%, x0.64); green - 8 bets for 6,279G (39.1%, x1.56)

blue bets:
gingerfoo69: 3,390G (34.6%, 3,390G)
hotpocketsofficial: 1,718G (17.5%, 1,718G)
seppu777: 1,405G (14.3%, 1,405G)
Zachara: 1,000G (10.2%, 147,500G)
ValensEXP_: 1,000G (10.2%, 7,668G)
Raixelol: 600G (6.1%, 18,375G)
AllInBot: 382G (3.9%, 382G)
datadrivenbot: 200G (2.0%, 60,330G)
BartTradingCompany: 100G (1.0%, 7,381G)

green bets:
HASTERIOUS: 1,753G (27.9%, 1,753G)
BirbBrainsBot: 1,000G (15.9%, 146,110G)
Sairentozon7: 1,000G (15.9%, 18,827G)
Seaweed_B: 1,000G (15.9%, 57,542G)
getthemoneyz: 514G (8.2%, 1,944,284G)
upvla: 500G (8.0%, 500G)
Neech: 312G (5.0%, 744G)
Leonidusx: 200G (3.2%, 36,586G)

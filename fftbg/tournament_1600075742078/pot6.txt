Final Bets: white - 11 bets for 5,985G (59.8%, x0.67); purple - 7 bets for 4,027G (40.2%, x1.49)

white bets:
OneHundredFists: 2,000G (33.4%, 11,949G)
BirbBrainsBot: 1,000G (16.7%, 145,110G)
nifboy: 744G (12.4%, 3,435G)
Ogema_theDream: 694G (11.6%, 694G)
Raixelol: 600G (10.0%, 18,760G)
ValensEXP_: 300G (5.0%, 8,309G)
datadrivenbot: 200G (3.3%, 60,458G)
Zachara: 141G (2.4%, 148,141G)
getthemoneyz: 106G (1.8%, 1,943,770G)
BartTradingCompany: 100G (1.7%, 7,445G)
MemoriesofFinal: 100G (1.7%, 8,346G)

purple bets:
ColetteMSLP: 1,141G (28.3%, 1,141G)
Leonidusx: 1,000G (24.8%, 36,386G)
AllInBot: 627G (15.6%, 627G)
upvla: 500G (12.4%, 500G)
Neech: 432G (10.7%, 432G)
jaykaye: 227G (5.6%, 227G)
gingerfoo69: 100G (2.5%, 5,563G)

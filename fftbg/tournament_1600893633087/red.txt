Player: !Red
Team: Red Team
Palettes: Red/Brown



Daveb
Male
Aries
64
63
Ninja
Battle Skill
Damage Split
Equip Axe
Move+1

Healing Staff
Battle Axe
Black Hood
Judo Outfit
Rubber Shoes

Shuriken, Stick
Speed Break, Stasis Sword



Actual JP
Female
Scorpio
77
50
Ninja
Jump
Arrow Guard
Equip Gun
Retreat

Papyrus Codex
Madlemgen
Flash Hat
Black Costume
Magic Ring

Shuriken, Bomb
Level Jump8, Vertical Jump7



Nizaha
Male
Gemini
65
67
Thief
Yin Yang Magic
Damage Split
Dual Wield
Retreat

Orichalcum
Cultist Dagger
Feather Hat
Leather Outfit
Diamond Armlet

Steal Armor, Steal Weapon, Arm Aim
Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze



Mazairasama
Male
Pisces
43
40
Samurai
Jump
Counter Magic
Long Status
Move-HP Up

Asura Knife

Leather Helmet
Reflect Mail
Red Shoes

Asura, Muramasa
Level Jump3, Vertical Jump6

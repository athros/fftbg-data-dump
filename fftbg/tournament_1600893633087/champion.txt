Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Galkife
Female
Cancer
75
45
Dancer
Elemental
MP Restore
Short Charge
Fly

Ryozan Silk

Headgear
Mystic Vest
Dracula Mantle

Witch Hunt, Slow Dance, Nether Demon, Dragon Pit
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm



Error72
Female
Aries
54
78
Calculator
Chaos Skill
Mana Shield
Magic Attack UP
Levitate

Battle Folio
Buckler
Cross Helmet
Diamond Armor
Defense Armlet

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate



E Ballard
Male
Pisces
72
50
Monk
Elemental
Parry
Defense UP
Move+1



Barette
Mystic Vest
Red Shoes

Wave Fist, Secret Fist, Purification, Chakra
Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard



JumbocactuarX27
Male
Gemini
62
61
Chemist
Elemental
Caution
Equip Knife
Ignore Terrain

Ninja Edge

Holy Miter
Power Sleeve
Spike Shoes

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard

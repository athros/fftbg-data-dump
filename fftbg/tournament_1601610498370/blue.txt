Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Deathmaker06
Male
Libra
63
60
Time Mage
White Magic
Damage Split
Equip Sword
Ignore Terrain

Defender

Green Beret
Wizard Robe
Red Shoes

Haste 2, Slow, Immobilize
Cure 2, Shell, Shell 2, Esuna



Mesmaster
Female
Cancer
76
48
Samurai
Item
Distribute
Equip Knife
Teleport

Orichalcum

Bronze Helmet
Leather Armor
Cherche

Murasame, Kiyomori, Muramasa, Kikuichimoji
Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Laserman1000
Male
Capricorn
50
72
Calculator
Disease Skill
Hamedo
Short Status
Move-MP Up

Papyrus Codex
Buckler
Iron Helmet
Silk Robe
Cursed Ring

Blue Magic
Tendrils, Lick, Goo, Bad Breath, Moldball Virus, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Ominnous
Male
Leo
48
77
Lancer
Elemental
Catch
Maintenance
Teleport

Dragon Whisker
Genji Shield
Grand Helmet
Black Robe
Setiemson

Level Jump8, Vertical Jump5
Pitfall, Hell Ivy, Sand Storm, Blizzard, Lava Ball

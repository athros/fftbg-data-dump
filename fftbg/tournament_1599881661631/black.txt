Player: !Black
Team: Black Team
Palettes: Black/Red



Latebit
Female
Sagittarius
73
80
Ninja
Draw Out
Counter Tackle
Sicken
Jump+3

Flail
Short Edge
Twist Headband
Mythril Vest
Dracula Mantle

Bomb, Stick
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



Aldrammech
Female
Aries
62
67
Squire
Time Magic
Counter Tackle
Defend
Waterbreathing

Giant Axe
Mythril Shield
Mythril Helmet
Carabini Mail
Red Shoes

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish, Scream
Haste, Stop, Immobilize, Reflect, Stabilize Time



Douchetron
Female
Sagittarius
67
43
Thief
Time Magic
MP Restore
Sicken
Fly

Coral Sword

Flash Hat
Chain Vest
Spike Shoes

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim
Slow, Slow 2, Float, Reflect, Demi 2, Stabilize Time



Reinoe
Male
Libra
52
46
Geomancer
Time Magic
Mana Shield
Defend
Move+2

Ancient Sword
Aegis Shield
Flash Hat
Light Robe
Genji Gauntlet

Pitfall, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind
Haste, Immobilize, Demi 2, Stabilize Time

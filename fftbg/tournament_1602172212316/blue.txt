Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dogsandcatsand
Female
Libra
44
51
Wizard
Basic Skill
Speed Save
Long Status
Swim

Rod

Holy Miter
Brigandine
Magic Gauntlet

Fire, Fire 2, Fire 4, Bolt, Ice 2, Frog, Death, Flare
Accumulate, Heal



Fenaen
Female
Aries
37
58
Time Mage
Item
Faith Save
Martial Arts
Jump+1



Twist Headband
Chameleon Robe
Magic Ring

Haste, Haste 2, Slow, Slow 2, Quick, Stabilize Time
Potion, Hi-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down



FuzzyTigers
Male
Scorpio
41
75
Priest
Battle Skill
Distribute
Equip Polearm
Swim

Holy Lance

Triangle Hat
Earth Clothes
Angel Ring

Cure, Raise, Wall, Esuna
Shield Break, Weapon Break, Mind Break, Stasis Sword, Dark Sword



ALY327
Female
Pisces
69
44
Mime

Absorb Used MP
Dual Wield
Waterbreathing



Holy Miter
Power Sleeve
N-Kai Armlet

Mimic


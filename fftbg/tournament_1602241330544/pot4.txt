Final Bets: purple - 7 bets for 5,943G (54.5%, x0.83); brown - 9 bets for 4,959G (45.5%, x1.20)

purple bets:
ZPawZ: 2,504G (42.1%, 2,504G)
ValensEXP_: 988G (16.6%, 988G)
Error72: 779G (13.1%, 779G)
HaplessOne: 600G (10.1%, 600G)
ColetteMSLP: 524G (8.8%, 5,864G)
TheSabretoothe: 348G (5.9%, 348G)
datadrivenbot: 200G (3.4%, 82,803G)

brown bets:
AllInBot: 1,368G (27.6%, 1,368G)
BirbBrainsBot: 1,000G (20.2%, 191,341G)
WireLord: 696G (14.0%, 696G)
CosmicTactician: 617G (12.4%, 1,234G)
2b_yorha_b: 500G (10.1%, 34,340G)
Zachara: 373G (7.5%, 1,373G)
getthemoneyz: 188G (3.8%, 2,200,061G)
FE34923: 116G (2.3%, 116G)
DrAntiSocial: 101G (2.0%, 3,310G)

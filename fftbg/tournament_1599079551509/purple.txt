Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thyrandaal
Male
Libra
40
55
Chemist
Sing
Auto Potion
Equip Armor
Move-MP Up

Star Bag

Leather Hat
Leather Armor
Genji Gauntlet

Potion, Hi-Potion, Ether, Hi-Ether, Echo Grass, Soft, Phoenix Down
Battle Song, Nameless Song, Last Song



Jeeboheebo
Male
Virgo
58
67
Geomancer
Draw Out
Meatbone Slash
Concentrate
Jump+3

Long Sword
Mythril Shield
Feather Hat
Chain Vest
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Asura, Koutetsu, Kiyomori, Muramasa



Kellios11
Male
Gemini
60
55
Oracle
Talk Skill
Brave Save
Monster Talk
Move+2

Bestiary

Headgear
Secret Clothes
Jade Armlet

Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Persuade, Praise, Insult, Negotiate



Rocl
Male
Virgo
64
49
Lancer
Elemental
Arrow Guard
Sicken
Lava Walking

Spear
Bronze Shield
Mythril Helmet
Linen Cuirass
Dracula Mantle

Level Jump8, Vertical Jump7
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !White
Team: White Team
Palettes: White/Blue



Ar Tactic
Male
Leo
69
75
Ninja
Punch Art
Regenerator
Doublehand
Levitate

Orichalcum

Black Hood
Power Sleeve
Spike Shoes

Bomb, Staff, Spear
Pummel, Earth Slash, Secret Fist, Purification, Seal Evil



Reddwind
Male
Aquarius
38
67
Knight
Summon Magic
HP Restore
Equip Knife
Move-MP Up

Wizard Rod
Flame Shield
Diamond Helmet
Chain Mail
Feather Mantle

Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Surging Sword
Moogle, Ramuh, Titan, Fairy



Xsifthewolf
Male
Aries
62
67
Time Mage
Item
Parry
Concentrate
Fly

Oak Staff

Red Hood
Chameleon Robe
Salty Rage

Haste 2, Slow 2, Demi, Meteor
Potion, Hi-Potion, Antidote, Phoenix Down



MilkaChoko
Male
Virgo
55
58
Ninja
Basic Skill
Absorb Used MP
Concentrate
Jump+2

Kunai
Flail
Red Hood
Leather Outfit
Dracula Mantle

Shuriken, Sword
Throw Stone, Heal

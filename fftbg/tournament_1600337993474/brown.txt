Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheSabretoothe
Female
Serpentarius
60
59
Dancer
Battle Skill
MA Save
Magic Defense UP
Move-MP Up

Cashmere

Feather Hat
Light Robe
Reflect Ring

Polka Polka, Dragon Pit
Head Break, Shield Break, Speed Break, Mind Break, Night Sword



Go2sleepTV
Female
Scorpio
79
43
Time Mage
Throw
PA Save
Short Charge
Move+3

White Staff

Headgear
White Robe
Spike Shoes

Stop, Float, Quick
Shuriken, Bomb, Knife, Dictionary



Smashy
Male
Aries
63
77
Ninja
Charge
Mana Shield
Sicken
Jump+1

Blind Knife
Short Edge
Leather Hat
Black Costume
108 Gems

Shuriken, Bomb
Charge+1, Charge+7



Peeronid
Female
Cancer
57
46
Archer
Item
Catch
Doublehand
Jump+1

Snipe Bow

Genji Helmet
Judo Outfit
Diamond Armlet

Charge+1, Charge+7
Potion, Hi-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down

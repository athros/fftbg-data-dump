Final Bets: brown - 14 bets for 14,262G (22.3%, x3.49); champion - 22 bets for 49,735G (77.7%, x0.29)

brown bets:
Rytor: 3,999G (28.0%, 7,998G)
Qaz12301: 2,794G (19.6%, 5,589G)
Raixelol: 2,306G (16.2%, 2,306G)
mykelwolfscar: 1,000G (7.0%, 1,657G)
DavenIII: 1,000G (7.0%, 10,447G)
killth3kid: 640G (4.5%, 20,438G)
Craymel_Mage: 574G (4.0%, 574G)
killabandaid: 451G (3.2%, 451G)
SephDarkheart: 396G (2.8%, 4,555G)
getthemoneyz: 340G (2.4%, 2,056,922G)
Forkmore: 304G (2.1%, 10,551G)
Chihuahua_Charity: 288G (2.0%, 288G)
Lord_Gwarth: 150G (1.1%, 1,209G)
Scotty297: 20G (0.1%, 3,018G)

champion bets:
LivingHitokiri: 20,000G (40.2%, 126,843G)
Shalloween: 7,042G (14.2%, 7,042G)
AllInBot: 5,180G (10.4%, 5,180G)
Zachara: 3,600G (7.2%, 143,600G)
CosmicTactician: 3,500G (7.0%, 20,888G)
latebit: 2,567G (5.2%, 5,034G)
Smugzug: 1,082G (2.2%, 1,082G)
AnthroMetal: 1,022G (2.1%, 1,022G)
JumbocactuarX27: 1,000G (2.0%, 10,768G)
Nizaha: 892G (1.8%, 57,981G)
Laserman1000: 700G (1.4%, 15,482G)
upvla: 600G (1.2%, 600G)
BirbBrainsBot: 561G (1.1%, 121,439G)
CorpusCav: 500G (1.0%, 11,305G)
CapnChaos12: 300G (0.6%, 1,939G)
old_overholt_: 300G (0.6%, 28,720G)
Riley331: 260G (0.5%, 260G)
ChiChiRodriguez: 228G (0.5%, 228G)
gorgewall: 101G (0.2%, 5,874G)
maakur_: 100G (0.2%, 5,824G)
galkife: 100G (0.2%, 81,286G)
skipsandwiches: 100G (0.2%, 89,342G)

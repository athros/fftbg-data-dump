Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Chuckolator
Female
Sagittarius
41
60
Calculator
White Magic
Damage Split
Sicken
Lava Walking

Octagon Rod

Black Hood
Adaman Vest
Diamond Armlet

CT, Prime Number, 3
Cure, Raise, Reraise, Protect 2, Esuna, Holy



Fattunaking
Male
Libra
53
46
Monk
Charge
Speed Save
Dual Wield
Jump+1



Twist Headband
Earth Clothes
108 Gems

Wave Fist, Purification, Chakra, Seal Evil
Charge+2, Charge+10



Ruleof5
Female
Gemini
69
51
Archer
Steal
Mana Shield
Dual Wield
Move-HP Up

Mythril Gun
Glacier Gun
Red Hood
Brigandine
Cursed Ring

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Steal Weapon



Daveb
Male
Capricorn
74
44
Ninja
Time Magic
Caution
Equip Knife
Ignore Terrain

Thunder Rod
Assassin Dagger
Thief Hat
Secret Clothes
Wizard Mantle

Shuriken
Haste 2, Slow, Slow 2, Immobilize, Float, Reflect, Stabilize Time

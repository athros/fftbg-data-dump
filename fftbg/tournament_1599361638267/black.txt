Player: !Black
Team: Black Team
Palettes: Black/Red



Smashy
Female
Gemini
40
57
Dancer
Battle Skill
Counter
Dual Wield
Move+1

Hydra Bag
Star Bag
Golden Hairpin
Black Robe
Battle Boots

Last Dance, Obsidian Blade
Head Break, Shield Break, Magic Break, Surging Sword



Aldrammech
Male
Sagittarius
47
77
Samurai
Charge
Blade Grasp
Equip Knife
Waterbreathing

Thunder Rod

Iron Helmet
Chain Mail
Genji Gauntlet

Heaven's Cloud, Kikuichimoji
Charge+1, Charge+7



DLJuggernaut
Male
Scorpio
44
43
Monk
Battle Skill
Counter Flood
Equip Axe
Waterbreathing

Slasher

Triangle Hat
Mythril Vest
Chantage

Earth Slash, Secret Fist, Revive
Shield Break, Mind Break, Stasis Sword



Redmage4evah
Female
Aries
61
61
Mime

Caution
Secret Hunt
Move+3



Triangle Hat
Mystic Vest
Reflect Ring

Mimic


Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CorpusCav
Female
Scorpio
44
54
Chemist
Charge
Faith Save
Equip Axe
Jump+1

Healing Staff

Leather Hat
Brigandine
Elf Mantle

Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Remedy
Charge+2



HASTERIOUS
Female
Cancer
55
72
Wizard
Throw
Damage Split
Short Charge
Teleport

Thunder Rod

Feather Hat
Linen Robe
Small Mantle

Fire, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4, Empower
Bomb



Khelor
Male
Taurus
78
58
Oracle
Summon Magic
Counter Magic
Maintenance
Move+1

Papyrus Codex

Holy Miter
Black Costume
Bracer

Poison, Doubt Faith, Blind Rage, Dispel Magic
Ramuh, Ifrit, Titan, Carbunkle



Douchetron
Female
Sagittarius
76
62
Priest
Summon Magic
Counter Tackle
Short Charge
Teleport

Flail

Green Beret
Silk Robe
Angel Ring

Cure 4, Raise, Protect 2, Shell, Shell 2, Esuna
Moogle, Ramuh, Carbunkle

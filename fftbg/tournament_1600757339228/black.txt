Player: !Black
Team: Black Team
Palettes: Black/Red



BuffaloCrunch
Male
Aries
58
78
Samurai
White Magic
Damage Split
Equip Sword
Move-HP Up

Ice Brand

Circlet
Leather Armor
Dracula Mantle

Asura, Koutetsu, Murasame, Kiyomori, Muramasa
Cure, Raise, Raise 2, Shell, Shell 2, Wall, Esuna, Holy



Sairentozon7
Female
Pisces
61
76
Wizard
Steal
Caution
Sicken
Jump+1

Thunder Rod

Golden Hairpin
Rubber Costume
Red Shoes

Fire 2, Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Frog
Steal Shield, Steal Weapon



0v3rr8d
Female
Gemini
56
44
Geomancer
Throw
Hamedo
Beastmaster
Fly

Ancient Sword
Mythril Shield
Leather Hat
Secret Clothes
108 Gems

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Shuriken, Bomb, Dictionary



Lythe Caraker
Female
Taurus
51
71
Wizard
Battle Skill
Catch
Equip Gun
Levitate

Ramia Harp

Golden Hairpin
Light Robe
Magic Gauntlet

Fire, Fire 4, Bolt, Ice 3, Ice 4, Death, Flare
Head Break, Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gorgewall
Female
Leo
50
70
Mime

Dragon Spirit
Attack UP
Teleport



Red Hood
Rubber Costume
Cherche

Mimic




ColetteMSLP
Female
Pisces
51
77
Summoner
Battle Skill
Parry
Equip Armor
Jump+1

Gold Staff

Iron Helmet
Black Robe
N-Kai Armlet

Moogle, Shiva, Ifrit, Titan, Golem, Salamander, Silf
Head Break, Shield Break, Power Break, Stasis Sword, Justice Sword



Evewho
Female
Sagittarius
45
78
Chemist
Time Magic
Critical Quick
Equip Gun
Retreat

Papyrus Codex

Red Hood
Judo Outfit
Sprint Shoes

Potion, Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down
Haste, Float, Stabilize Time



Zachara
Male
Scorpio
59
46
Ninja
Black Magic
Faith Save
Equip Gun
Move+3

Bestiary
Battle Folio
Twist Headband
Power Sleeve
108 Gems

Shuriken, Staff, Ninja Sword
Fire 3, Fire 4, Bolt 4

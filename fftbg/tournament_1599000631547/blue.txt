Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Just Here2
Female
Libra
74
49
Calculator
Nether Skill
Caution
Secret Hunt
Jump+1

Bestiary

Crystal Helmet
Bronze Armor
Small Mantle

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Ar Tactic
Male
Aquarius
57
56
Oracle
Sing
Blade Grasp
Equip Bow
Levitate

Perseus Bow

Green Beret
Light Robe
Bracer

Blind, Spell Absorb, Life Drain, Foxbird, Confusion Song, Dispel Magic, Petrify
Space Storage, Sky Demon



E Ballard
Male
Cancer
52
76
Geomancer
Punch Art
Brave Save
Sicken
Levitate

Battle Axe
Buckler
Leather Hat
Mystic Vest
Elf Mantle

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Seal Evil



Brenogarwin
Male
Virgo
80
75
Mediator
Steal
Speed Save
Beastmaster
Waterwalking

Battle Folio

Golden Hairpin
Earth Clothes
Cursed Ring

Praise, Threaten, Negotiate, Refute
Steal Heart, Steal Weapon, Arm Aim, Leg Aim

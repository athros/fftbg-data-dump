Player: !Red
Team: Red Team
Palettes: Red/Brown



SephDarkheart
Male
Leo
71
52
Monk
Elemental
Counter
Equip Axe
Retreat

Rainbow Staff

Golden Hairpin
Leather Outfit
Battle Boots

Spin Fist, Purification, Chakra, Revive
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Error72
Female
Taurus
65
76
Priest
Draw Out
Counter Magic
Equip Bow
Jump+3

Ultimus Bow

Red Hood
Black Robe
Defense Armlet

Cure, Cure 3, Raise, Raise 2, Reraise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Bizen Boat



NicoSavoy
Female
Capricorn
66
65
Knight
Punch Art
HP Restore
Doublehand
Ignore Terrain

Long Sword

Barbuta
Silk Robe
Angel Ring

Head Break, Shield Break, Power Break
Secret Fist, Purification



Nhammen
Female
Taurus
67
60
Wizard
Draw Out
PA Save
Short Status
Waterwalking

Dragon Rod

Leather Hat
Chain Vest
Magic Ring

Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Empower
Heaven's Cloud

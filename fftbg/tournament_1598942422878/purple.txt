Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ar Tactic
Female
Gemini
56
80
Oracle
Time Magic
Faith Save
Equip Armor
Ignore Height

Battle Bamboo

Holy Miter
Genji Armor
Spike Shoes

Poison, Life Drain, Foxbird, Confusion Song, Dispel Magic
Haste, Slow 2, Stop, Float, Quick, Demi 2, Stabilize Time



Mesmaster
Male
Serpentarius
72
45
Geomancer
Talk Skill
Critical Quick
Equip Sword
Jump+2

Chaos Blade

Thief Hat
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Negotiate, Refute, Rehabilitate



Yangusburger
Monster
Scorpio
79
72
Skeleton










Victoriolue
Male
Aquarius
60
73
Lancer
Item
Faith Save
Equip Knife
Move-HP Up

Iga Knife
Bronze Shield
Cross Helmet
Diamond Armor
Leather Mantle

Level Jump8, Vertical Jump7
Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down

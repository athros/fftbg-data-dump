Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Omegasuspekt
Male
Capricorn
47
42
Ninja
Steal
Caution
Defend
Retreat

Kunai
Sasuke Knife
Leather Hat
Brigandine
Jade Armlet

Shuriken
Steal Heart, Steal Helmet



Seaweed B
Female
Sagittarius
72
39
Geomancer
Draw Out
Brave Save
Equip Axe
Ignore Height

Healing Staff
Diamond Shield
Red Hood
Power Sleeve
Power Wrist

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Kiyomori



Lolprinze
Female
Gemini
75
66
Samurai
Talk Skill
Sunken State
Monster Talk
Retreat

Mythril Spear

Circlet
Linen Robe
Cursed Ring

Koutetsu, Murasame, Kiyomori
Invitation, Praise, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Chuckolator
Female
Taurus
74
47
Archer
Basic Skill
Parry
Doublehand
Jump+1

Snipe Bow

Diamond Helmet
Clothes
Vanish Mantle

Charge+2, Charge+10
Accumulate, Heal, Tickle, Yell, Cheer Up

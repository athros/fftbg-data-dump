Player: !White
Team: White Team
Palettes: White/Blue



Zerguzen
Male
Aries
48
68
Wizard
Battle Skill
Dragon Spirit
Halve MP
Move-HP Up

Mythril Knife

Twist Headband
Chameleon Robe
Cursed Ring

Fire 4, Bolt 4, Ice 2, Ice 4, Empower
Head Break, Shield Break, Magic Break, Power Break



Lyonslegacy
Female
Libra
67
59
Lancer
Punch Art
Counter Tackle
Sicken
Lava Walking

Obelisk
Kaiser Plate
Bronze Helmet
Linen Cuirass
Feather Mantle

Level Jump4, Vertical Jump8
Pummel, Wave Fist, Secret Fist, Chakra, Revive



PantherIscariot
Monster
Scorpio
62
46
Malboro










Gorgewall
Female
Gemini
61
77
Squire
Charge
Counter Tackle
Long Status
Jump+2

Night Killer
Gold Shield
Golden Hairpin
Clothes
Dracula Mantle

Accumulate, Dash, Heal, Tickle, Yell, Wish
Charge+5, Charge+7

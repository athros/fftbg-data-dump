Player: !Green
Team: Green Team
Palettes: Green/White



DAC169
Male
Libra
44
76
Knight
Yin Yang Magic
Speed Save
Dual Wield
Fly

Defender
Blood Sword
Cross Helmet
Plate Mail
Small Mantle

Head Break, Armor Break, Magic Break, Stasis Sword, Justice Sword
Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Paralyze



Sinnyil2
Male
Cancer
49
76
Monk
Black Magic
Abandon
Equip Armor
Lava Walking



Triangle Hat
Robe of Lords
Feather Boots

Purification, Chakra, Revive
Ice, Death



DaveStrider55
Male
Aquarius
79
58
Chemist
Jump
MA Save
Short Status
Move-MP Up

Cultist Dagger

Headgear
Earth Clothes
Dracula Mantle

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Level Jump3, Vertical Jump6



Lijarkh
Male
Taurus
42
69
Monk
Item
Parry
Dual Wield
Ignore Terrain



Headgear
Adaman Vest
Spike Shoes

Purification, Seal Evil
Hi-Potion, X-Potion, Ether, Antidote, Soft, Remedy, Phoenix Down

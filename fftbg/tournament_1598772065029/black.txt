Player: !Black
Team: Black Team
Palettes: Black/Red



Silentkaster
Monster
Capricorn
42
71
Gobbledeguck










Nhammen
Female
Scorpio
59
71
Time Mage
Punch Art
Caution
Attack UP
Jump+3

Rainbow Staff

Flash Hat
Black Robe
Battle Boots

Haste 2, Slow 2, Reflect, Demi, Demi 2, Stabilize Time
Pummel, Secret Fist, Purification, Chakra, Revive



Strumisgod
Female
Libra
52
51
Monk
Throw
Distribute
Defense UP
Ignore Height



Flash Hat
Chain Vest
Leather Mantle

Purification, Revive, Seal Evil
Bomb



StealthModeLocke
Male
Capricorn
63
57
Squire
Item
Absorb Used MP
Magic Attack UP
Waterwalking

Ancient Sword
Flame Shield
Red Hood
Mystic Vest
Dracula Mantle

Accumulate, Dash, Heal, Tickle, Yell, Fury, Wish
Potion, Antidote, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down

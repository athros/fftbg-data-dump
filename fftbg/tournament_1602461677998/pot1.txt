Final Bets: red - 20 bets for 42,993G (62.6%, x0.60); blue - 21 bets for 25,726G (37.4%, x1.67)

red bets:
Sairentozon7: 34,493G (80.2%, 34,493G)
DLJuggernaut: 1,244G (2.9%, 2,441G)
Deathmaker06: 1,000G (2.3%, 32,889G)
BirbBrainsBot: 1,000G (2.3%, 94,971G)
NightwolfXVI: 799G (1.9%, 799G)
Lydian_C: 656G (1.5%, 656G)
krombobreaker: 500G (1.2%, 37,012G)
superdevon1: 396G (0.9%, 2,906G)
DrAntiSocial: 396G (0.9%, 396G)
getthemoneyz: 384G (0.9%, 2,212,395G)
king_smashington: 353G (0.8%, 353G)
Twisted_Nutsatchel: 300G (0.7%, 18,094G)
Breakdown777: 272G (0.6%, 272G)
duckfacebro: 200G (0.5%, 200G)
ValensEXP_: 200G (0.5%, 23,128G)
Grayv87: 200G (0.5%, 800G)
datadrivenbot: 200G (0.5%, 85,234G)
NicoSavoy: 200G (0.5%, 221,212G)
squideater_: 100G (0.2%, 4,024G)
MrJamDango: 100G (0.2%, 4,196G)

blue bets:
murderclan: 3,405G (13.2%, 3,405G)
DeathTaxesAndAnime: 2,835G (11.0%, 5,560G)
HorusTaurus: 2,741G (10.7%, 2,741G)
Forkmore: 2,321G (9.0%, 4,643G)
Evewho: 2,000G (7.8%, 42,452G)
Lolthsmeat: 2,000G (7.8%, 47,815G)
Zagorsek: 1,522G (5.9%, 1,522G)
dogsandcatsand: 1,042G (4.1%, 1,042G)
VolgraTheMoose: 1,008G (3.9%, 1,008G)
Lionhermit: 1,000G (3.9%, 6,037G)
WireLord: 1,000G (3.9%, 8,836G)
coralreeferz: 950G (3.7%, 4,833G)
YowaneHaku: 811G (3.2%, 811G)
Laserman1000: 792G (3.1%, 14,492G)
AllInBot: 634G (2.5%, 634G)
Jehudy7: 500G (1.9%, 1,686G)
TheBrett: 340G (1.3%, 340G)
ChiChiRodriguez: 268G (1.0%, 268G)
Demon_Lord_Josh: 256G (1.0%, 256G)
gorgewall: 201G (0.8%, 7,925G)
RunicMagus: 100G (0.4%, 2,359G)

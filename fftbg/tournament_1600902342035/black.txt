Player: !Black
Team: Black Team
Palettes: Black/Red



Vorap
Male
Aries
74
55
Bard
Time Magic
Regenerator
Equip Knife
Move-MP Up

Dragon Rod

Headgear
Crystal Mail
Sprint Shoes

Life Song, Nameless Song
Haste, Haste 2, Float, Quick, Demi, Stabilize Time



Sinnyil2
Male
Cancer
44
37
Lancer
Charge
Speed Save
Dual Wield
Jump+3

Mythril Spear
Obelisk
Platinum Helmet
Platinum Armor
Diamond Armlet

Level Jump8, Vertical Jump2
Charge+1, Charge+3, Charge+20



Thyrandaal
Female
Capricorn
72
70
Priest
Math Skill
Arrow Guard
Halve MP
Fly

Flail

Triangle Hat
Light Robe
Reflect Ring

Cure, Raise, Raise 2, Protect, Shell 2, Wall, Esuna
CT, 5



RunicMagus
Male
Taurus
67
42
Archer
Yin Yang Magic
Counter Flood
Doublehand
Waterbreathing

Glacier Gun

Circlet
Judo Outfit
Red Shoes

Charge+3, Charge+5
Blind, Spell Absorb, Foxbird, Dispel Magic, Paralyze

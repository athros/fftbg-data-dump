Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Laserman1000
Male
Pisces
72
72
Calculator
Black Magic
Brave Save
Equip Armor
Fly

Iron Fan

Leather Helmet
Bronze Armor
Feather Mantle

Height, Prime Number, 4, 3
Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Death, Flare



Technominari
Female
Capricorn
70
72
Priest
Charge
Counter Tackle
Defend
Levitate

Flame Whip

Thief Hat
Silk Robe
Cursed Ring

Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Esuna
Charge+1, Charge+2, Charge+3



StealthModeLocke
Male
Taurus
71
67
Knight
Talk Skill
Speed Save
Equip Armor
Waterbreathing

Broad Sword
Diamond Shield
Triangle Hat
Wizard Outfit
Jade Armlet

Head Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Persuade, Praise, Mimic Daravon



TheSabretoothe
Female
Gemini
71
44
Calculator
Spirit Skill
Earplug
Defense UP
Waterwalking

Battle Folio

Gold Helmet
Light Robe
Vanish Mantle

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch, Bite, Self Destruct, Flame Attack, Small Bomb, Spark

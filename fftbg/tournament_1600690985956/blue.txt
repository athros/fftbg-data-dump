Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SephDarkheart
Female
Cancer
63
65
Mime

MP Restore
Equip Shield
Swim


Crystal Shield
Flash Hat
Power Sleeve
Magic Ring

Mimic




Docmitchphd
Male
Aquarius
53
65
Geomancer
Yin Yang Magic
Counter Magic
Defend
Move-HP Up

Slasher
Bronze Shield
Red Hood
White Robe
Germinas Boots

Hell Ivy, Static Shock, Sand Storm, Blizzard, Lava Ball
Spell Absorb, Doubt Faith, Foxbird, Dispel Magic, Petrify



RunicMagus
Female
Pisces
76
69
Chemist
Jump
Speed Save
Doublehand
Jump+3

Panther Bag

Green Beret
Mystic Vest
Wizard Mantle

Potion, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Level Jump4, Vertical Jump8



Galkife
Male
Scorpio
75
43
Priest
Sing
Earplug
Sicken
Lava Walking

Flail

Green Beret
Light Robe
Elf Mantle

Cure, Cure 3, Cure 4, Raise, Raise 2, Shell, Esuna
Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade, Sky Demon

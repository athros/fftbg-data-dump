Player: !Green
Team: Green Team
Palettes: Green/White



Smashy
Female
Capricorn
75
54
Dancer
Black Magic
Counter Tackle
Short Charge
Jump+3

Ryozan Silk

Leather Hat
Black Robe
Genji Gauntlet

Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Last Dance, Obsidian Blade
Fire 3, Bolt 4, Ice, Ice 2, Ice 3, Empower, Flare



MemoriesofFinal
Female
Serpentarius
56
74
Oracle
Black Magic
Regenerator
Maintenance
Move-HP Up

Cypress Rod

Golden Hairpin
Mythril Vest
Magic Gauntlet

Poison, Spell Absorb, Life Drain, Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep
Fire, Bolt 2, Bolt 4, Ice, Ice 2, Frog, Flare



DeathTaxesAndAnime
Monster
Scorpio
79
58
Taiju










Thyrandaal
Male
Gemini
58
58
Thief
Yin Yang Magic
Regenerator
Equip Gun
Ignore Terrain

Romanda Gun

Green Beret
Black Costume
N-Kai Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Weapon
Poison, Life Drain, Zombie, Confusion Song, Dispel Magic

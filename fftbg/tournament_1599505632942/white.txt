Player: !White
Team: White Team
Palettes: White/Blue



Sietaha
Female
Sagittarius
55
70
Thief
Talk Skill
Absorb Used MP
Equip Bow
Waterwalking

Ice Bow

Holy Miter
Mystic Vest
Diamond Armlet

Steal Heart, Steal Helmet, Steal Weapon, Arm Aim, Leg Aim
Invitation, Persuade, Praise, Threaten, Solution, Mimic Daravon, Refute



Latebit
Male
Sagittarius
57
67
Squire
White Magic
MA Save
Equip Sword
Move+2

Muramasa
Crystal Shield
Cross Helmet
Bronze Armor
Small Mantle

Throw Stone, Heal, Tickle, Fury, Scream
Raise, Regen, Protect, Shell, Wall, Esuna



Laserman1000
Male
Aries
72
74
Samurai
Steal
Sunken State
Equip Bow
Ignore Height

Ice Bow

Crystal Helmet
Chameleon Robe
108 Gems

Bizen Boat, Heaven's Cloud, Muramasa
Gil Taking, Steal Heart, Leg Aim



W0RLDEND
Female
Gemini
68
68
Samurai
Punch Art
MP Restore
Equip Knife
Jump+2

Hidden Knife

Bronze Helmet
Linen Cuirass
Elf Mantle

Heaven's Cloud, Muramasa
Spin Fist, Pummel, Purification, Revive

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Raixelol
Male
Aries
52
56
Archer
White Magic
Parry
Equip Armor
Fly

Snipe Bow
Round Shield
Iron Helmet
Gold Armor
Magic Gauntlet

Charge+3, Charge+4, Charge+5
Cure 3, Reraise, Regen, Shell 2, Wall, Esuna, Holy



DaveStrider55
Female
Sagittarius
49
40
Oracle
Talk Skill
Parry
Defend
Ignore Height

Battle Bamboo

Flash Hat
Earth Clothes
Diamond Armlet

Blind, Poison, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Sleep
Praise, Insult, Mimic Daravon, Rehabilitate



Macradarkstar
Female
Capricorn
46
64
Squire
Black Magic
Catch
Concentrate
Waterbreathing

Assassin Dagger
Mythril Shield
Feather Hat
Crystal Mail
Sprint Shoes

Accumulate, Throw Stone, Heal, Yell, Cheer Up, Wish
Fire 2, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 3, Flare



Yangusburger
Female
Taurus
51
75
Lancer
Charge
Critical Quick
Martial Arts
Retreat

Gungnir
Venetian Shield
Iron Helmet
Linen Cuirass
Elf Mantle

Level Jump8, Vertical Jump7
Charge+1, Charge+3, Charge+4, Charge+5

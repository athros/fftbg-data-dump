Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



RobotOcelot
Male
Scorpio
79
63
Oracle
Throw
Speed Save
Equip Axe
Move-HP Up

Giant Axe

Golden Hairpin
Black Robe
Feather Mantle

Blind, Silence Song, Foxbird, Dispel Magic, Paralyze, Dark Holy
Shuriken



Lijarkh
Male
Libra
59
40
Monk
Item
Caution
Throw Item
Move+2



Cachusha
Mystic Vest
Angel Ring

Pummel, Chakra
Potion, X-Potion, Ether



Mesmaster
Female
Gemini
77
46
Priest
Jump
Arrow Guard
Equip Armor
Jump+3

Morning Star

Platinum Helmet
Reflect Mail
Defense Armlet

Cure, Reraise, Wall, Esuna, Holy
Level Jump8, Vertical Jump7



Gorgewall
Male
Aquarius
46
57
Mediator
Sing
Faith Save
Defend
Ignore Terrain

Blaze Gun

Red Hood
Wizard Robe
Feather Boots

Invitation, Persuade, Praise, Insult, Negotiate, Refute
Last Song

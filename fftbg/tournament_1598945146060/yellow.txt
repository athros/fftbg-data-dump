Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Greggernaut
Male
Aries
59
45
Mime

Damage Split
Secret Hunt
Fly



Green Beret
Leather Outfit
Jade Armlet

Mimic




Lokenwow
Female
Cancer
76
46
Chemist
White Magic
Absorb Used MP
Long Status
Move+2

Orichalcum

Black Hood
Wizard Outfit
Bracer

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Raise 2, Esuna



DracoknightZero
Female
Virgo
61
43
Knight
Item
Abandon
Equip Bow
Swim

Yoichi Bow
Flame Shield
Circlet
Plate Mail
Sprint Shoes

Surging Sword
Potion, X-Potion, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Seaweed B
Male
Virgo
65
77
Mediator
Throw
PA Save
Dual Wield
Lava Walking

Stone Gun
Mythril Gun
Triangle Hat
Secret Clothes
Spike Shoes

Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Spear

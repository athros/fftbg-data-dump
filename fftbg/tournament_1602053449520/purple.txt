Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Capricorn
53
51
Thief
Jump
Counter Tackle
Equip Gun
Waterbreathing

Stone Gun

Flash Hat
Power Sleeve
Reflect Ring

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Level Jump5, Vertical Jump2



ALY327
Female
Cancer
62
69
Geomancer
Punch Art
Damage Split
Doublehand
Levitate

Battle Axe

Green Beret
Chain Vest
Reflect Ring

Pitfall, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Purification, Chakra, Revive



Khelor
Female
Cancer
48
80
Oracle
Dance
Faith Save
Martial Arts
Swim

Ivory Rod

Flash Hat
Silk Robe
Angel Ring

Blind, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Wiznaibus, Slow Dance, Disillusion, Nether Demon, Dragon Pit



Baumgrattler
Male
Scorpio
50
79
Wizard
Summon Magic
Dragon Spirit
Equip Shield
Move+2

Poison Rod
Bronze Shield
Barette
Mythril Vest
Reflect Ring

Fire 2, Fire 3, Fire 4, Bolt, Bolt 4, Ice, Ice 3, Ice 4
Moogle, Shiva, Golem, Carbunkle, Silf, Fairy

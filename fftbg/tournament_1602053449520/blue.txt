Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Evontno
Male
Libra
77
66
Thief
Battle Skill
Regenerator
Halve MP
Jump+1

Cultist Dagger

Leather Hat
Judo Outfit
Sprint Shoes

Steal Helmet, Steal Shield, Steal Weapon
Magic Break, Stasis Sword, Surging Sword



ValensEXP
Male
Capricorn
47
80
Squire
Black Magic
Caution
Magic Defense UP
Swim

Coral Sword
Ice Shield
Triangle Hat
Bronze Armor
Genji Gauntlet

Accumulate, Heal, Cheer Up
Fire, Fire 3, Bolt 3, Bolt 4



Zachara
Female
Cancer
44
51
Samurai
Steal
Parry
Equip Polearm
Move+3

Ryozan Silk

Crystal Helmet
White Robe
108 Gems

Koutetsu, Heaven's Cloud
Steal Shield, Steal Status, Leg Aim



WireLord
Female
Cancer
76
75
Priest
Yin Yang Magic
MP Restore
Sicken
Waterbreathing

Oak Staff

Thief Hat
Mythril Vest
Elf Mantle

Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Shell, Esuna
Poison, Blind Rage, Dispel Magic, Dark Holy

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



FuzzyTigers
Male
Leo
50
60
Samurai
Basic Skill
PA Save
Beastmaster
Ignore Terrain

Heaven's Cloud

Circlet
Light Robe
Cursed Ring

Asura, Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji
Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up



Khelor
Male
Scorpio
74
73
Thief
Summon Magic
Brave Save
Equip Knife
Waterbreathing

Ninja Edge

Ribbon
Brigandine
Bracer

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Status, Leg Aim
Moogle, Ramuh, Titan, Carbunkle, Salamander, Silf, Lich



Breakdown777
Male
Aquarius
80
56
Knight
Yin Yang Magic
Damage Split
Attack UP
Retreat

Defender
Escutcheon
Circlet
Bronze Armor
Elf Mantle

Speed Break, Justice Sword, Dark Sword, Night Sword
Spell Absorb, Pray Faith, Dispel Magic, Dark Holy



HorusTaurus
Male
Gemini
69
63
Mediator
Black Magic
Auto Potion
Equip Shield
Retreat

Bestiary
Round Shield
Leather Hat
Mystic Vest
Angel Ring

Invitation, Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate
Fire 3, Bolt 4, Ice, Ice 2, Ice 3, Death

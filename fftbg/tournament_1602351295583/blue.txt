Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bruubarg
Male
Capricorn
66
70
Samurai
Talk Skill
Counter Tackle
Short Status
Move-MP Up

Holy Lance

Crystal Helmet
Silk Robe
Dracula Mantle

Asura, Koutetsu, Murasame, Muramasa, Kikuichimoji
Praise, Threaten, Death Sentence, Insult



Zachara
Male
Leo
75
40
Chemist
Black Magic
Parry
Long Status
Ignore Terrain

Orichalcum

Golden Hairpin
Clothes
Power Wrist

X-Potion, Maiden's Kiss, Holy Water, Phoenix Down
Fire 3, Bolt, Bolt 2, Ice, Empower, Frog, Flare



Lord Gwarth
Male
Sagittarius
44
77
Knight
Elemental
Auto Potion
Equip Gun
Levitate

Bestiary
Buckler
Platinum Helmet
Black Robe
Spike Shoes

Armor Break, Magic Break, Night Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



WireLord
Female
Taurus
77
74
Knight
Throw
Abandon
Equip Bow
Retreat

Windslash Bow
Flame Shield
Iron Helmet
Platinum Armor
Vanish Mantle

Head Break, Magic Break, Speed Break, Mind Break, Stasis Sword
Bomb

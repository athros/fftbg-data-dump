Player: !White
Team: White Team
Palettes: White/Blue



DeathTaxesAndAnime
Female
Aries
59
69
Mediator
Charge
Speed Save
Equip Armor
Move+3

Papyrus Codex

Barbuta
Diamond Armor
Bracer

Praise, Threaten, Solution, Death Sentence, Insult, Negotiate, Refute
Charge+2, Charge+5, Charge+10



Chuckolator
Female
Capricorn
59
76
Calculator
Nether Skill
Abandon
Magic Defense UP
Ignore Height

Papyrus Codex

Leather Helmet
Silk Robe
Cherche

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Laserman1000
Male
Libra
59
43
Lancer
Elemental
Arrow Guard
Defense UP
Move+2

Obelisk
Ice Shield
Crystal Helmet
White Robe
Sprint Shoes

Level Jump4, Vertical Jump8
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind



Dogsandcatsand
Female
Gemini
54
49
Wizard
Item
MA Save
Magic Attack UP
Lava Walking

Main Gauche

Twist Headband
Mystic Vest
Feather Boots

Fire, Fire 4, Bolt 2, Bolt 3, Ice, Ice 3, Ice 4, Flare
Potion, Elixir, Maiden's Kiss, Phoenix Down

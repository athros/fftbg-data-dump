Player: !Red
Team: Red Team
Palettes: Red/Brown



FatTunaKing
Female
Scorpio
79
47
Priest
Talk Skill
Earplug
Short Charge
Fly

Wizard Staff

Headgear
Wizard Outfit
Spike Shoes

Cure, Cure 2, Raise, Raise 2, Reraise, Shell, Esuna, Holy
Invitation, Persuade, Preach, Death Sentence, Negotiate, Refute



Thunderducker
Female
Aquarius
60
55
Archer
Black Magic
Speed Save
Equip Bow
Swim

Ice Bow

Red Hood
Mythril Vest
Magic Ring

Charge+1, Charge+3, Charge+4, Charge+7
Fire 4, Bolt, Bolt 4, Ice 3, Frog



Genkidou
Female
Virgo
77
80
Samurai
Summon Magic
Auto Potion
Long Status
Fly

Masamune

Mythril Helmet
Linen Robe
Red Shoes

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Ramuh, Titan, Golem, Bahamut, Silf, Cyclops



Ruleof5
Female
Cancer
75
66
Archer
Basic Skill
Counter Magic
Equip Axe
Fly

Wizard Staff
Escutcheon
Leather Hat
Earth Clothes
Magic Ring

Charge+2, Charge+4, Charge+7, Charge+10
Accumulate, Dash, Throw Stone, Heal, Tickle

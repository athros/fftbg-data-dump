Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Duckfacebro
Female
Capricorn
47
79
Oracle
Summon Magic
PA Save
Short Charge
Jump+2

Battle Folio

Leather Hat
White Robe
Wizard Mantle

Doubt Faith, Confusion Song, Dispel Magic, Sleep, Petrify, Dark Holy
Moogle, Shiva, Golem, Carbunkle, Leviathan, Salamander, Cyclops



SeniorBunk
Male
Leo
74
49
Calculator
Robosnake Skill
Earplug
Equip Gun
Move-MP Up

Papyrus Codex
Platinum Shield
Twist Headband
Black Costume
Spike Shoes

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Arcblazer23
Female
Sagittarius
58
64
Priest
Summon Magic
Counter Tackle
Sicken
Jump+1

Healing Staff

Triangle Hat
Mystic Vest
Defense Armlet

Raise, Regen, Shell 2, Esuna
Moogle, Shiva, Titan, Golem, Carbunkle, Cyclops



TheBrett
Female
Pisces
38
39
Wizard
Time Magic
Counter Flood
Short Charge
Move-HP Up

Mage Masher

Leather Hat
Linen Robe
Germinas Boots

Fire 3, Bolt, Bolt 3, Ice 2, Empower, Frog
Immobilize, Reflect, Demi 2, Stabilize Time, Meteor

Player: !Red
Team: Red Team
Palettes: Red/Brown



ALY327
Male
Leo
57
54
Wizard
White Magic
Counter
Short Charge
Lava Walking

Faith Rod

Golden Hairpin
White Robe
Genji Gauntlet

Fire, Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4
Reraise, Regen, Protect 2, Shell, Esuna



Dogsandcatsand
Monster
Leo
79
60
Ultima Demon










CorpusCav
Female
Gemini
75
61
Archer
Throw
Regenerator
Attack UP
Levitate

Mythril Gun
Crystal Shield
Flash Hat
Secret Clothes
Vanish Mantle

Charge+1, Charge+2, Charge+3, Charge+7
Bomb, Staff



Phi Sig
Female
Aries
51
74
Time Mage
Draw Out
Brave Save
Beastmaster
Teleport

Iron Fan

Red Hood
Linen Robe
Rubber Shoes

Haste 2, Immobilize, Reflect, Demi, Stabilize Time
Asura, Koutetsu, Bizen Boat, Kiyomori

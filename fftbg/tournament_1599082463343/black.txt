Player: !Black
Team: Black Team
Palettes: Black/Red



CapnChaos12
Male
Libra
77
44
Oracle
Jump
Counter Flood
Concentrate
Move+3

Cypress Rod

Twist Headband
Silk Robe
Bracer

Spell Absorb, Pray Faith, Silence Song, Dispel Magic, Paralyze, Dark Holy
Level Jump4, Vertical Jump2



Ruleof5
Female
Libra
65
73
Archer
Draw Out
HP Restore
Martial Arts
Swim


Mythril Shield
Headgear
Earth Clothes
Elf Mantle

Charge+1, Charge+3
Bizen Boat, Heaven's Cloud



Forkmore
Female
Aquarius
51
63
Calculator
Yin Yang Magic
Arrow Guard
Long Status
Move+3

Cypress Rod

Flash Hat
Wizard Robe
Power Wrist

CT, Prime Number, 5, 4
Blind, Poison, Pray Faith, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy



Smashy
Male
Aries
63
44
Monk
Elemental
Speed Save
Attack UP
Jump+3



Triangle Hat
Adaman Vest
Cursed Ring

Pummel, Earth Slash, Secret Fist, Purification, Chakra
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind

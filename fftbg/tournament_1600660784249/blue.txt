Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rune339
Female
Capricorn
41
38
Oracle
White Magic
Counter
Magic Attack UP
Move+3

Iron Fan

Headgear
Earth Clothes
Power Wrist

Zombie, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify
Cure 2, Protect, Protect 2, Shell, Esuna



BuffaloCrunch
Female
Virgo
66
39
Dancer
Time Magic
Catch
Equip Bow
Swim

Yoichi Bow

Triangle Hat
Light Robe
Defense Ring

Polka Polka, Disillusion, Obsidian Blade, Nether Demon
Slow 2, Immobilize, Float, Reflect, Demi, Stabilize Time



Douchetron
Female
Leo
68
48
Summoner
Throw
Critical Quick
Equip Axe
Waterwalking

Healing Staff

Green Beret
Black Costume
Bracer

Shiva, Ramuh, Golem, Carbunkle, Lich
Shuriken



Mrfripps
Male
Capricorn
57
45
Oracle
Time Magic
Speed Save
Magic Attack UP
Move+3

Battle Bamboo

Red Hood
Light Robe
108 Gems

Poison, Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep, Dark Holy
Haste 2, Slow, Stop, Quick, Demi, Stabilize Time

Player: !Red
Team: Red Team
Palettes: Red/Brown



Error72
Female
Sagittarius
72
70
Oracle
Talk Skill
Meatbone Slash
Equip Axe
Lava Walking

Wizard Staff

Leather Hat
Wizard Robe
Feather Boots

Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep
Persuade, Praise, Threaten, Death Sentence, Insult



Gorgewall
Male
Libra
71
59
Archer
Elemental
Absorb Used MP
Secret Hunt
Move+2

Blaze Gun
Ice Shield
Holy Miter
Judo Outfit
Feather Mantle

Charge+2, Charge+3, Charge+7
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Willjin
Female
Capricorn
40
54
Priest
Time Magic
Sunken State
Magic Attack UP
Levitate

Morning Star

Ribbon
Leather Outfit
Angel Ring

Cure, Cure 2, Raise, Wall, Esuna
Stop, Immobilize, Float, Demi 2, Stabilize Time, Meteor



Silentkaster
Male
Sagittarius
60
40
Chemist
Summon Magic
Mana Shield
Defense UP
Ignore Terrain

Hydra Bag

Headgear
Earth Clothes
Jade Armlet

Hi-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Moogle, Ifrit, Carbunkle, Odin, Leviathan, Fairy

Player: !Green
Team: Green Team
Palettes: Green/White



JonnyCue
Female
Libra
51
78
Thief
Talk Skill
Meatbone Slash
Long Status
Move+1

Broad Sword

Headgear
Clothes
Bracer

Steal Weapon
Persuade, Praise, Preach, Solution, Insult, Refute



Upvla
Male
Aries
58
63
Mime

HP Restore
Martial Arts
Jump+1



Green Beret
Power Sleeve
Diamond Armlet

Mimic




ZCKaiser
Monster
Capricorn
68
54
Ultima Demon










Old Overholt
Female
Scorpio
75
50
Priest
Yin Yang Magic
Counter
Short Charge
Lava Walking

Flail

Green Beret
Light Robe
Vanish Mantle

Cure, Cure 3, Cure 4, Shell, Shell 2, Esuna
Blind, Life Drain, Confusion Song, Sleep

Player: !Black
Team: Black Team
Palettes: Black/Red



HASTERIOUS
Female
Libra
66
79
Summoner
Throw
Hamedo
Short Charge
Retreat

Flame Rod

Barette
Silk Robe
Wizard Mantle

Moogle, Ramuh, Carbunkle, Salamander, Silf, Fairy, Zodiac
Bomb, Dictionary



Bigbongsmoker
Female
Pisces
37
45
Dancer
Steal
Mana Shield
Doublehand
Jump+3

Cashmere

Headgear
Wizard Outfit
Elf Mantle

Slow Dance, Polka Polka, Nameless Dance
Steal Helmet, Leg Aim



MoonSugarFiend
Female
Cancer
63
48
Mime

Dragon Spirit
Magic Attack UP
Move-MP Up



Leather Hat
Power Sleeve
Dracula Mantle

Mimic




MemoriesofFinal
Male
Gemini
73
60
Wizard
Time Magic
Auto Potion
Short Charge
Lava Walking

Main Gauche

Thief Hat
Chameleon Robe
Spike Shoes

Ice, Ice 2, Empower
Immobilize, Float, Reflect, Demi 2, Stabilize Time

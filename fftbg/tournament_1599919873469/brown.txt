Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Just Here2
Female
Aries
61
68
Archer
Dance
Catch
Equip Shield
Swim

Hunting Bow
Platinum Shield
Feather Hat
Mythril Vest
Wizard Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Disillusion



ZCKaiser
Female
Pisces
58
67
Oracle
Black Magic
Regenerator
Long Status
Ignore Height

Battle Bamboo

Black Hood
Linen Robe
Feather Mantle

Blind, Pray Faith, Zombie, Blind Rage, Foxbird
Ice 2, Empower



Brokenknight201
Female
Aries
48
54
Geomancer
Talk Skill
HP Restore
Magic Attack UP
Move-MP Up

Bizen Boat
Aegis Shield
Black Hood
Adaman Vest
Defense Armlet

Pitfall, Water Ball, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Persuade, Negotiate, Refute



FriendSquirrel
Female
Capricorn
77
69
Dancer
Basic Skill
MA Save
Short Charge
Fly

Ryozan Silk

Black Hood
Black Robe
N-Kai Armlet

Witch Hunt, Wiznaibus, Slow Dance, Last Dance, Obsidian Blade
Dash, Tickle, Yell, Scream

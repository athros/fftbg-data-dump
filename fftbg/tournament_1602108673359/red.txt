Player: !Red
Team: Red Team
Palettes: Red/Brown



Blaster Bomb
Female
Libra
35
68
Ninja
Talk Skill
Counter Flood
Concentrate
Swim

Ninja Edge
Flail
Cachusha
Black Costume
Sprint Shoes

Shuriken
Persuade, Preach, Solution, Death Sentence, Negotiate, Refute



Rocl
Male
Libra
58
45
Knight
Time Magic
Dragon Spirit
Equip Axe
Move+3

Flame Whip
Venetian Shield
Barbuta
Reflect Mail
Magic Ring

Weapon Break, Mind Break, Stasis Sword, Dark Sword, Night Sword
Haste, Haste 2, Slow 2, Stop, Float, Reflect



Mesmaster
Female
Sagittarius
75
74
Wizard
Draw Out
Hamedo
Sicken
Move-HP Up

Poison Rod

Green Beret
Chameleon Robe
Battle Boots

Fire, Bolt 3, Ice 2, Empower
Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji



Thunderducker
Male
Aries
40
78
Priest
Battle Skill
Parry
Equip Armor
Fly

Gold Staff

Circlet
Adaman Vest
Bracer

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Shell 2, Wall, Esuna
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Surging Sword

Player: !Red
Team: Red Team
Palettes: Red/Brown



Cryptopsy70
Monster
Leo
38
71
Byblos










ALY327
Female
Aquarius
45
55
Squire
Yin Yang Magic
Brave Save
Sicken
Move+2

Sleep Sword
Ice Shield
Feather Hat
Carabini Mail
Angel Ring

Accumulate, Heal
Spell Absorb, Life Drain, Paralyze



WireLord
Female
Aries
52
68
Wizard
Math Skill
Parry
Magic Attack UP
Retreat

Ice Rod

Black Hood
Chameleon Robe
Magic Gauntlet

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Empower, Death
CT, Height, 5, 3



DavenIII
Male
Taurus
65
69
Bard
Jump
HP Restore
Dual Wield
Levitate

Ramia Harp
Bloody Strings
Barette
Chain Mail
Dracula Mantle

Angel Song, Life Song, Cheer Song, Battle Song
Level Jump3, Vertical Jump6

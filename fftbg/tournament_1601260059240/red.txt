Player: !Red
Team: Red Team
Palettes: Red/Brown



DanielRKnealeArt
Male
Aries
60
65
Ninja
Steal
Abandon
Equip Sword
Move+2

Spell Edge
Heaven's Cloud
Green Beret
Judo Outfit
108 Gems

Shuriken, Bomb, Knife, Hammer, Axe
Steal Weapon, Steal Accessory, Arm Aim



Einswolf
Monster
Taurus
73
77
Black Chocobo










3ngag3
Female
Leo
48
80
Oracle
White Magic
Meatbone Slash
Martial Arts
Jump+3

Octagon Rod

Golden Hairpin
Silk Robe
Spike Shoes

Poison, Zombie, Blind Rage, Confusion Song
Cure 2, Cure 3, Cure 4, Raise, Protect, Protect 2, Shell 2, Esuna



ExplanationMarkWow
Male
Capricorn
54
76
Monk
Talk Skill
Critical Quick
Monster Talk
Fly



Triangle Hat
Wizard Outfit
Reflect Ring

Secret Fist, Purification, Chakra
Preach, Insult, Mimic Daravon, Refute

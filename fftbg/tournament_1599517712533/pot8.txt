Final Bets: blue - 13 bets for 4,410G (33.1%, x2.02); champion - 22 bets for 8,907G (66.9%, x0.50)

blue bets:
VolgraTheMoose: 1,001G (22.7%, 118,799G)
Zagorsek: 672G (15.2%, 672G)
KasugaiRoastedPeas: 520G (11.8%, 520G)
Lifebregin: 500G (11.3%, 26,819G)
Leonidusx: 500G (11.3%, 6,822G)
rocl: 329G (7.5%, 3,292G)
seppu777: 222G (5.0%, 2,598G)
Rytor: 200G (4.5%, 8,511G)
AllInBot: 100G (2.3%, 100G)
Chaide: 100G (2.3%, 245G)
Yangusburger: 100G (2.3%, 6,442G)
CosmicTactician: 100G (2.3%, 32,771G)
getthemoneyz: 66G (1.5%, 1,878,074G)

champion bets:
Shalloween: 1,815G (20.4%, 3,631G)
BirbBrainsBot: 1,000G (11.2%, 104,603G)
roofiepops: 676G (7.6%, 676G)
DeathTaxesAndAnime: 650G (7.3%, 2,558G)
run_with_stone_GUNs: 588G (6.6%, 22,249G)
krombobreaker: 500G (5.6%, 2,421G)
akash7: 500G (5.6%, 2,707G)
TsukikageRyu: 500G (5.6%, 3,555G)
Dasutin23: 456G (5.1%, 14,081G)
OneHundredFists: 408G (4.6%, 408G)
poorest_hobo: 252G (2.8%, 252G)
ValensEXP_: 220G (2.5%, 1,687G)
Miku_Shikhu: 200G (2.2%, 6,469G)
Laserman1000: 200G (2.2%, 1,400G)
datadrivenbot: 200G (2.2%, 57,505G)
BartTradingCompany: 141G (1.6%, 1,419G)
gorgewall: 101G (1.1%, 2,233G)
farside00831: 100G (1.1%, 108G)
Twisted_Nutsatchel: 100G (1.1%, 10,841G)
lijarkh: 100G (1.1%, 635G)
Thyrandaal: 100G (1.1%, 187,795G)
DracoknightZero: 100G (1.1%, 2,439G)

Final Bets: purple - 5 bets for 2,908G (53.2%, x0.88); champion - 7 bets for 2,555G (46.8%, x1.14)

purple bets:
BirbBrainsBot: 1,000G (34.4%, 103,750G)
NicoSavoy: 1,000G (34.4%, 5,536G)
iBardic: 356G (12.2%, 356G)
blorpy_: 344G (11.8%, 344G)
getthemoneyz: 208G (7.2%, 1,813,417G)

champion bets:
Raixelol: 981G (38.4%, 1,962G)
acrusade: 500G (19.6%, 1,000G)
Lemonjohns: 420G (16.4%, 3,316G)
BaronHaynes: 250G (9.8%, 65,209G)
datadrivenbot: 200G (7.8%, 62,591G)
Rune339: 104G (4.1%, 104G)
pepperbuster: 100G (3.9%, 2,300G)

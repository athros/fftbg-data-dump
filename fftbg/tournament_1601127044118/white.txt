Player: !White
Team: White Team
Palettes: White/Blue



Thyrandaal
Monster
Cancer
74
70
Red Chocobo










Reddwind
Male
Virgo
43
69
Knight
Summon Magic
Caution
Defense UP
Swim

Defender
Buckler
Gold Helmet
Bronze Armor
Wizard Mantle

Head Break, Speed Break, Power Break, Mind Break, Night Sword
Moogle, Ramuh, Ifrit, Carbunkle



Evewho
Female
Libra
80
42
Chemist
Jump
Distribute
Dual Wield
Jump+2

Zorlin Shape
Air Knife
Holy Miter
Wizard Outfit
Leather Mantle

Potion, Ether, Antidote, Echo Grass, Phoenix Down
Level Jump3, Vertical Jump8



Ar Tactic
Male
Virgo
45
60
Archer
Punch Art
Counter Magic
Dual Wield
Move-MP Up

Stone Gun
Mythril Gun
Barette
Secret Clothes
Germinas Boots

Charge+1, Charge+3, Charge+7
Wave Fist, Purification, Revive

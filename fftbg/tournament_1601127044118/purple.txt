Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lolthsmeat
Female
Pisces
77
60
Lancer
Time Magic
Blade Grasp
Defense UP
Move-MP Up

Partisan
Escutcheon
Leather Helmet
Plate Mail
Dracula Mantle

Level Jump8, Vertical Jump5
Slow 2, Stabilize Time



Silentkaster
Male
Libra
64
70
Squire
Black Magic
HP Restore
Short Charge
Waterbreathing

Sleep Sword
Diamond Shield
Flash Hat
Gold Armor
Dracula Mantle

Accumulate, Heal, Yell
Fire, Fire 4, Ice 2, Ice 3, Empower, Flare



Leonidusx
Male
Taurus
64
74
Knight
Summon Magic
Arrow Guard
Halve MP
Teleport

Ancient Sword
Aegis Shield
Iron Helmet
Genji Armor
Vanish Mantle

Shield Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Night Sword
Shiva, Titan, Lich



Ayeayex3
Male
Scorpio
73
53
Thief
Basic Skill
Counter
Equip Gun
Ignore Terrain

Blaze Gun

Flash Hat
Leather Outfit
Genji Gauntlet

Steal Armor, Steal Shield
Throw Stone, Yell

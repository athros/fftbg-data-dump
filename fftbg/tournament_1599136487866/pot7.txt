Final Bets: green - 9 bets for 4,978G (42.7%, x1.34); purple - 10 bets for 6,683G (57.3%, x0.74)

green bets:
DavenIII: 2,000G (40.2%, 4,935G)
BirbBrainsBot: 1,000G (20.1%, 74,604G)
dogsandcatsand: 700G (14.1%, 1,524G)
Rune339: 382G (7.7%, 382G)
rocl: 352G (7.1%, 1,261G)
fantastadon: 200G (4.0%, 1,765G)
bigbongsmoker: 120G (2.4%, 4,895G)
Absalom_20: 120G (2.4%, 2,216G)
getthemoneyz: 104G (2.1%, 1,816,089G)

purple bets:
latebit: 2,463G (36.9%, 4,830G)
Thyrandaal: 1,234G (18.5%, 16,305G)
Lord_Burrah: 1,000G (15.0%, 122,000G)
BaronHaynes: 500G (7.5%, 68,557G)
Raixelol: 446G (6.7%, 446G)
SephDarkheart: 389G (5.8%, 389G)
FFMaster: 251G (3.8%, 251G)
datadrivenbot: 200G (3.0%, 66,567G)
lwtest: 100G (1.5%, 685G)
Kellios11: 100G (1.5%, 2,034G)

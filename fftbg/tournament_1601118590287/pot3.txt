Final Bets: white - 8 bets for 2,685G (38.9%, x1.57); black - 6 bets for 4,223G (61.1%, x0.64)

white bets:
AllInBot: 438G (16.3%, 438G)
Zachara: 421G (15.7%, 153,921G)
BoneMiser: 380G (14.2%, 380G)
HorusTaurus: 356G (13.3%, 356G)
bigbongsmoker: 352G (13.1%, 352G)
getthemoneyz: 338G (12.6%, 2,084,947G)
DouglasDragonThePoet: 200G (7.4%, 3,855G)
datadrivenbot: 200G (7.4%, 76,710G)

black bets:
DHaveWord: 1,068G (25.3%, 1,068G)
resjudicata3: 1,010G (23.9%, 1,010G)
BirbBrainsBot: 1,000G (23.7%, 173,416G)
MemoriesofFinal: 682G (16.1%, 682G)
Lolthsmeat: 359G (8.5%, 359G)
JermZzZzZz: 104G (2.5%, 104G)

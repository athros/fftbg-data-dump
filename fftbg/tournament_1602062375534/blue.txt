Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WireLord
Female
Pisces
50
48
Priest
Black Magic
MA Save
Dual Wield
Move-HP Up

Wizard Staff
Rainbow Staff
Green Beret
Mythril Vest
Feather Mantle

Cure, Cure 4, Raise, Regen, Protect, Shell, Wall, Esuna, Holy
Fire 3, Fire 4, Bolt 2, Ice 3, Ice 4, Empower



VolgraTheMoose
Male
Aquarius
47
47
Ninja
Steal
Faith Save
Equip Axe
Ignore Height

Hidden Knife
Rainbow Staff
Leather Hat
Adaman Vest
Bracer

Shuriken, Bomb, Staff, Ninja Sword, Wand
Gil Taking, Steal Helmet



Lawndough
Female
Taurus
73
59
Mediator
Throw
Catch
Equip Shield
Ignore Terrain

Romanda Gun
Genji Shield
Golden Hairpin
Adaman Vest
Leather Mantle

Threaten, Death Sentence, Insult, Mimic Daravon, Refute
Shuriken, Bomb, Spear



CassiePhoenix
Female
Aries
55
51
Summoner
Basic Skill
Regenerator
Short Charge
Waterbreathing

Battle Folio

Golden Hairpin
Light Robe
Elf Mantle

Moogle, Shiva, Odin, Salamander, Fairy
Accumulate, Wish

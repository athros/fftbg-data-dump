Player: !White
Team: White Team
Palettes: White/Blue



Tithonus
Male
Capricorn
71
77
Mime

Abandon
Equip Armor
Move-HP Up



Gold Helmet
Light Robe
N-Kai Armlet

Mimic




Tripaplex
Male
Cancer
64
80
Knight
Summon Magic
Hamedo
Dual Wield
Jump+1

Defender
Iron Sword
Crystal Helmet
Mythril Armor
Leather Mantle

Weapon Break, Surging Sword, Explosion Sword
Moogle, Shiva, Titan, Leviathan, Salamander



Powergems
Female
Serpentarius
49
53
Thief
Talk Skill
Abandon
Magic Attack UP
Move-MP Up

Ancient Sword

Green Beret
Earth Clothes
Power Wrist

Gil Taking, Steal Armor, Steal Status
Threaten, Mimic Daravon, Refute



Meta Five
Male
Serpentarius
80
57
Mediator
White Magic
Dragon Spirit
Defend
Waterbreathing

Romanda Gun

Feather Hat
Wizard Robe
Magic Ring

Solution, Negotiate, Mimic Daravon
Raise, Raise 2, Shell 2, Holy

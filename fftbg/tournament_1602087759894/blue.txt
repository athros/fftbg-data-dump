Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Brokenknight201
Male
Aquarius
46
56
Summoner
Basic Skill
Critical Quick
Maintenance
Levitate

Flame Rod

Twist Headband
Brigandine
Angel Ring

Shiva, Ifrit, Titan, Golem, Carbunkle, Odin, Leviathan, Salamander, Fairy, Lich
Accumulate, Yell



Thyrandaal
Female
Aquarius
45
62
Lancer
Yin Yang Magic
Faith Save
Equip Sword
Ignore Height

Bizen Boat
Gold Shield
Iron Helmet
Light Robe
Small Mantle

Level Jump4, Vertical Jump8
Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Confusion Song, Dispel Magic, Sleep



Daveb
Male
Libra
48
78
Ninja
Draw Out
MA Save
Equip Gun
Move+1

Romanda Gun
Blaze Gun
Black Hood
Black Costume
Elf Mantle

Bomb, Knife
Asura, Muramasa



Fenaen
Male
Sagittarius
50
78
Archer
Talk Skill
Counter
Doublehand
Jump+3

Hunting Bow

Black Hood
Brigandine
Sprint Shoes

Charge+1, Charge+2, Charge+3, Charge+10
Persuade, Praise, Death Sentence, Negotiate, Rehabilitate

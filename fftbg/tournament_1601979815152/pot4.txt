Final Bets: purple - 8 bets for 4,395G (43.4%, x1.31); brown - 10 bets for 5,736G (56.6%, x0.77)

purple bets:
HorusTaurus: 2,000G (45.5%, 9,157G)
Lolthsmeat: 808G (18.4%, 808G)
ColetteMSLP: 504G (11.5%, 15,811G)
AllInBot: 292G (6.6%, 292G)
Aaron2Nerdy: 228G (5.2%, 228G)
resjudicata3: 200G (4.6%, 11,768G)
datadrivenbot: 200G (4.6%, 88,804G)
FuzzyTigers: 163G (3.7%, 163G)

brown bets:
BirbBrainsBot: 1,000G (17.4%, 97,332G)
MemoriesofFinal: 1,000G (17.4%, 18,006G)
E_Ballard: 912G (15.9%, 912G)
WireLord: 676G (11.8%, 676G)
upvla: 600G (10.5%, 600G)
Mushufasa_: 500G (8.7%, 27,167G)
khelor_: 500G (8.7%, 11,961G)
getthemoneyz: 236G (4.1%, 2,175,859G)
silentkaster: 200G (3.5%, 29,833G)
FE34923: 112G (2.0%, 112G)

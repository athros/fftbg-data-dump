Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DAC169
Female
Cancer
58
38
Dancer
Battle Skill
Caution
Equip Bow
Ignore Terrain

Bow Gun

Thief Hat
Adaman Vest
Magic Ring

Wiznaibus, Last Dance, Obsidian Blade, Nether Demon, Dragon Pit
Magic Break, Night Sword



Phi Sig
Female
Leo
69
70
Time Mage
Draw Out
Arrow Guard
Equip Sword
Jump+1

Sleep Sword

Red Hood
Mythril Vest
Defense Armlet

Haste, Haste 2, Slow, Slow 2, Demi 2, Stabilize Time
Koutetsu



Maakur
Male
Leo
57
46
Mediator
Black Magic
Auto Potion
Doublehand
Swim

Stone Gun

Golden Hairpin
Wizard Outfit
Germinas Boots

Persuade, Praise, Threaten, Preach, Solution, Insult, Mimic Daravon, Refute
Bolt 4, Ice, Ice 3, Empower, Frog, Death



WhiteTigress
Female
Libra
49
79
Wizard
Summon Magic
Meatbone Slash
Short Charge
Levitate

Ice Rod

Red Hood
Linen Robe
Leather Mantle

Ice, Ice 2, Ice 3, Death, Flare
Moogle, Carbunkle, Salamander, Silf, Lich

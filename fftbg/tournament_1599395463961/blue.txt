Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Leonidusx
Male
Scorpio
51
75
Knight
Charge
Auto Potion
Equip Polearm
Fly

Obelisk
Kaiser Plate
Cross Helmet
Chain Mail
Rubber Shoes

Head Break, Armor Break, Magic Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10



Mushufasa
Female
Leo
59
43
Archer
Steal
HP Restore
Defense UP
Teleport

Lightning Bow

Circlet
Secret Clothes
Sprint Shoes

Charge+1, Charge+4, Charge+5, Charge+20
Steal Heart, Steal Shield, Steal Status, Leg Aim



MemoriesofFinal
Male
Aries
43
72
Calculator
Black Magic
Abandon
Magic Defense UP
Move+1

Papyrus Codex

Headgear
Light Robe
N-Kai Armlet

Height, Prime Number, 5
Fire 2, Fire 4, Bolt 3, Bolt 4, Ice 3, Empower



Bruubarg
Female
Aquarius
39
48
Time Mage
White Magic
Hamedo
Defend
Jump+3

Battle Bamboo

Red Hood
Chameleon Robe
Rubber Shoes

Reflect, Demi 2
Cure 4, Raise, Shell, Esuna, Holy, Magic Barrier

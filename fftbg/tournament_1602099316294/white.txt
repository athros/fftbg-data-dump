Player: !White
Team: White Team
Palettes: White/Blue



Error72
Female
Aquarius
55
60
Summoner
Basic Skill
Parry
Short Charge
Waterbreathing

Wizard Rod

Black Hood
Wizard Robe
Reflect Ring

Moogle, Ramuh, Titan, Golem, Carbunkle
Accumulate, Throw Stone, Heal, Tickle



Nifboy
Male
Aries
48
70
Mediator
Black Magic
Meatbone Slash
Halve MP
Move-HP Up

Bestiary

Thief Hat
Adaman Vest
Cherche

Persuade, Praise, Solution, Death Sentence
Fire 2, Fire 3, Bolt, Ice 2, Ice 3, Flare



Fenaen
Male
Capricorn
79
79
Lancer
Basic Skill
Counter Magic
Equip Sword
Jump+3

Bizen Boat
Round Shield
Crystal Helmet
Chain Mail
Feather Boots

Level Jump8, Vertical Jump8
Dash, Yell, Cheer Up, Scream



Reddwind
Male
Scorpio
71
73
Ninja
Punch Art
Caution
Equip Sword
Teleport

Sasuke Knife
Blood Sword
Red Hood
Power Sleeve
Feather Boots

Bomb, Staff
Wave Fist, Revive

Player: !White
Team: White Team
Palettes: White/Blue



Lowlf
Male
Gemini
81
75
Archer
Draw Out
Damage Split
Equip Polearm
Fly

Mythril Spear
Bronze Shield
Holy Miter
Earth Clothes
Defense Ring

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10
Heaven's Cloud, Kiyomori



CorpusCav
Female
Cancer
62
53
Chemist
Punch Art
Dragon Spirit
Secret Hunt
Ignore Terrain

Star Bag

Ribbon
Earth Clothes
Elf Mantle

Potion, Eye Drop, Maiden's Kiss
Pummel, Wave Fist, Purification, Chakra



Wiznaibusthis
Male
Libra
65
41
Ninja
Elemental
Critical Quick
Equip Gun
Fly

Bestiary
Papyrus Codex
Red Hood
Black Costume
Power Wrist

Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock



ValensEXP
Male
Aquarius
54
68
Bard
Item
Mana Shield
Dual Wield
Move-MP Up

Ramia Harp
Bloody Strings
Holy Miter
Power Sleeve
Feather Mantle

Angel Song, Life Song, Cheer Song, Magic Song, Nameless Song, Diamond Blade, Space Storage, Hydra Pit
X-Potion, Ether, Antidote, Echo Grass, Remedy, Phoenix Down

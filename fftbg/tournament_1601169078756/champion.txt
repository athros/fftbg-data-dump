Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Genkidou
Female
Leo
68
43
Time Mage
Black Magic
Catch
Magic Attack UP
Waterbreathing

Wizard Staff

Cachusha
Wizard Robe
Elf Mantle

Haste, Haste 2, Slow, Immobilize, Float, Quick, Stabilize Time, Meteor
Fire 3, Bolt 3, Ice 2, Flare



Lowlf
Male
Aries
65
68
Thief
Throw
Parry
Beastmaster
Jump+2

Air Knife

Feather Hat
Adaman Vest
Dracula Mantle

Steal Helmet, Steal Accessory, Steal Status, Arm Aim
Ninja Sword



Gorgewall
Male
Libra
59
56
Summoner
Steal
Auto Potion
Doublehand
Retreat

Healing Staff

Thief Hat
Earth Clothes
Defense Ring

Moogle, Ifrit, Golem, Carbunkle, Odin, Leviathan, Silf, Fairy
Gil Taking, Steal Helmet, Leg Aim



Just Here2
Male
Taurus
44
80
Time Mage
Draw Out
Arrow Guard
Equip Gun
Ignore Height

Mythril Gun

Flash Hat
Wizard Robe
Leather Mantle

Haste, Haste 2, Slow, Immobilize, Float, Reflect, Quick, Stabilize Time
Bizen Boat, Kiyomori, Muramasa

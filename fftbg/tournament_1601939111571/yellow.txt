Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ExplanationMarkWow
Male
Leo
74
64
Mediator
Yin Yang Magic
PA Save
Dual Wield
Waterwalking

Blast Gun
Blaze Gun
Headgear
Mythril Vest
N-Kai Armlet

Invitation, Persuade, Praise, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Blind, Pray Faith, Confusion Song, Dispel Magic, Petrify



Lowlf
Male
Gemini
68
39
Samurai
Battle Skill
Auto Potion
Equip Axe
Move+2

Flail

Platinum Helmet
Plate Mail
Rubber Shoes

Koutetsu, Heaven's Cloud
Magic Break, Speed Break, Justice Sword, Night Sword, Explosion Sword



ValensEXP
Male
Cancer
50
73
Squire
Elemental
Earplug
Defend
Jump+1

Iron Sword
Platinum Shield
Leather Hat
Power Sleeve
108 Gems

Throw Stone, Heal, Fury
Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Musashi45
Female
Cancer
46
75
Priest
Dance
Regenerator
Short Charge
Waterwalking

Rainbow Staff

Feather Hat
Light Robe
Rubber Shoes

Raise, Raise 2, Shell, Shell 2, Esuna
Wiznaibus, Polka Polka, Void Storage, Nether Demon

Final Bets: red - 13 bets for 6,862G (30.5%, x2.28); blue - 25 bets for 15,628G (69.5%, x0.44)

red bets:
acid_flashback: 2,000G (29.1%, 79,960G)
BirbBrainsBot: 1,000G (14.6%, 103,475G)
Sairentozon7: 1,000G (14.6%, 73,331G)
lowlf: 660G (9.6%, 12,075G)
Shalloween: 500G (7.3%, 51,115G)
2b_yorha_b: 400G (5.8%, 19,508G)
heropong: 308G (4.5%, 308G)
Musashi45: 288G (4.2%, 7,700G)
getthemoneyz: 206G (3.0%, 2,176,811G)
AllInBot: 200G (2.9%, 200G)
maakur_: 100G (1.5%, 3,599G)
Demon_Lord_Josh: 100G (1.5%, 2,180G)
rkane398: 100G (1.5%, 373G)

blue bets:
HorusTaurus: 1,691G (10.8%, 1,691G)
3ngag3: 1,543G (9.9%, 19,543G)
CorpusCav: 1,500G (9.6%, 64,916G)
resjudicata3: 1,342G (8.6%, 1,342G)
MemoriesofFinal: 1,000G (6.4%, 17,878G)
VolgraTheMoose: 956G (6.1%, 13,358G)
mrfripps: 800G (5.1%, 13,083G)
Thyrandaal: 772G (4.9%, 772G)
DirkC_Fenrir84: 750G (4.8%, 750G)
Nizaha: 664G (4.2%, 6,365G)
ko2q: 615G (3.9%, 1,231G)
DHaveWord: 505G (3.2%, 505G)
murderclan: 500G (3.2%, 1,353G)
old_overholt_: 500G (3.2%, 52,265G)
roofiepops: 488G (3.1%, 9,028G)
Cryptopsy70: 416G (2.7%, 2,479G)
Ogema_theDream: 248G (1.6%, 864G)
daveb_: 223G (1.4%, 7,122G)
Evewho: 200G (1.3%, 8,034G)
ValensEXP_: 200G (1.3%, 4,947G)
datadrivenbot: 200G (1.3%, 86,606G)
Chuckolator: 174G (1.1%, 1,162G)
SnarkMage: 140G (0.9%, 140G)
gorgewall: 101G (0.6%, 7,678G)
maximalplus: 100G (0.6%, 100G)

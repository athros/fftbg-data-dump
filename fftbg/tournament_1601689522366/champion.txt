Player: !zChamp
Team: Champion Team
Palettes: Green/White



Seppu777
Male
Sagittarius
47
53
Samurai
Elemental
Meatbone Slash
Doublehand
Jump+3

Dragon Whisker

Cross Helmet
Linen Robe
Sprint Shoes

Asura
Pitfall, Water Ball, Hell Ivy, Static Shock, Sand Storm, Blizzard, Lava Ball



SeniorBunk
Male
Gemini
54
50
Archer
Steal
Speed Save
Martial Arts
Fly

Mythril Bow

Black Hood
Adaman Vest
Vanish Mantle

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Steal Accessory, Arm Aim, Leg Aim



MemoriesofFinal
Male
Libra
66
82
Calculator
White Magic
Brave Save
Dual Wield
Move-HP Up

Ice Rod
Rod
Holy Miter
Black Costume
Defense Armlet

CT, Height, Prime Number, 5, 4
Cure, Raise, Raise 2, Protect 2, Esuna



Rhyser76
Female
Virgo
79
65
Mediator
White Magic
Critical Quick
Magic Attack UP
Ignore Height

Papyrus Codex

Twist Headband
Wizard Robe
Magic Gauntlet

Praise, Insult, Negotiate, Mimic Daravon, Refute
Cure, Cure 2, Cure 3, Raise, Raise 2, Shell, Shell 2, Wall

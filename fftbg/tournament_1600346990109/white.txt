Player: !White
Team: White Team
Palettes: White/Blue



Nifboy
Female
Cancer
77
60
Lancer
Talk Skill
Catch
Attack UP
Retreat

Spear
Aegis Shield
Gold Helmet
Mythril Armor
Red Shoes

Level Jump8, Vertical Jump2
Persuade, Preach, Solution, Mimic Daravon, Refute



ValensEXP
Male
Cancer
54
51
Knight
Black Magic
MP Restore
Dual Wield
Waterwalking

Ragnarok
Rune Blade
Diamond Helmet
Crystal Mail
Bracer

Weapon Break, Dark Sword
Fire 3, Bolt 4, Ice 2



Pplvee1
Male
Aries
69
57
Calculator
Bird Skill
HP Restore
Equip Polearm
Fly

Ryozan Silk

Crystal Helmet
Chameleon Robe
Diamond Armlet

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



Kellios11
Female
Serpentarius
65
78
Calculator
Undeath Skill
Counter Flood
Halve MP
Jump+1

Thunder Rod

Feather Hat
Bronze Armor
Defense Ring

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tripaplex
Male
Taurus
40
75
Wizard
Math Skill
Counter Magic
Halve MP
Jump+2

Rod

Golden Hairpin
White Robe
N-Kai Armlet

Fire, Fire 3, Bolt 4, Ice 2, Ice 3, Ice 4, Flare
CT, Height, Prime Number, 4, 3



TheQueenfuta
Male
Pisces
48
59
Bard
Charge
Speed Save
Equip Knife
Teleport 2

Cultist Dagger

Feather Hat
Brigandine
108 Gems

Angel Song, Cheer Song, Magic Song, Last Song
Charge+2, Charge+4, Charge+7



Heroebal
Male
Leo
77
76
Oracle
Time Magic
Counter Magic
Equip Gun
Waterwalking

Papyrus Codex

Twist Headband
Wizard Robe
Wizard Mantle

Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Dispel Magic, Dark Holy
Haste 2, Slow 2, Reflect, Demi, Demi 2, Stabilize Time, Meteor



Sairentozon7
Male
Taurus
61
61
Lancer
White Magic
Catch
Martial Arts
Move-HP Up


Aegis Shield
Bronze Helmet
Platinum Armor
Small Mantle

Level Jump4, Vertical Jump8
Regen, Shell, Shell 2

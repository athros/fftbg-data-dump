Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tripaplex
Male
Gemini
38
64
Thief
Time Magic
Auto Potion
Dual Wield
Swim

Mythril Sword
Mythril Knife
Flash Hat
Mystic Vest
N-Kai Armlet

Gil Taking, Steal Heart, Steal Weapon, Arm Aim
Reflect, Quick, Demi, Stabilize Time



Helpimabug
Female
Libra
67
52
Calculator
Time Magic
Mana Shield
Secret Hunt
Waterbreathing

Whale Whisker

Holy Miter
Wizard Robe
Reflect Ring

Height, Prime Number, 3
Haste, Slow, Stop, Float, Demi, Demi 2, Stabilize Time



Foamsoldier
Female
Cancer
57
52
Thief
Charge
Catch
Equip Armor
Retreat

Long Sword

Gold Helmet
Carabini Mail
Angel Ring

Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Charge+2, Charge+4, Charge+7



SeniorBunk
Male
Libra
49
75
Lancer
Battle Skill
PA Save
Sicken
Ignore Height

Cypress Rod
Escutcheon
Iron Helmet
Plate Mail
Defense Ring

Level Jump3, Vertical Jump7
Shield Break, Magic Break, Mind Break, Justice Sword

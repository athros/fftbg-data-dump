Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Goth Applebees Hostess
Male
Aries
74
49
Mime

Speed Save
Sicken
Move-HP Up



Green Beret
Chain Mail
Wizard Mantle

Mimic




Reddash9
Male
Aries
80
65
Priest
Elemental
Distribute
Equip Shield
Move+2

Wizard Staff
Round Shield
Golden Hairpin
Robe of Lords
Power Wrist

Cure, Cure 2, Cure 3, Reraise, Protect, Shell, Wall, Esuna
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Just Here2
Female
Sagittarius
57
74
Time Mage
Item
Counter
Equip Armor
Fly

Gokuu Rod

Twist Headband
Carabini Mail
Diamond Armlet

Haste, Haste 2, Stop, Float, Reflect, Demi 2, Stabilize Time
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Technominari
Female
Leo
69
52
Priest
Basic Skill
MP Restore
Equip Knife
Waterbreathing

Poison Rod

Headgear
White Robe
Power Wrist

Cure 4, Reraise, Protect, Shell, Esuna, Holy
Accumulate, Yell

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Serpentarius
74
63
Thief
Elemental
Damage Split
Equip Armor
Move+2

Dagger

Golden Hairpin
White Robe
Defense Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Arm Aim, Leg Aim
Pitfall, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Killth3kid
Male
Leo
63
64
Archer
Summon Magic
Absorb Used MP
Attack UP
Move-HP Up

Silver Bow

Ribbon
Adaman Vest
Red Shoes

Charge+2, Charge+10
Moogle, Ifrit, Carbunkle, Cyclops



Sireaulou
Male
Leo
36
42
Time Mage
Math Skill
MA Save
Short Charge
Jump+1

Wizard Staff

Golden Hairpin
Silk Robe
Jade Armlet

Slow, Immobilize, Reflect, Quick, Stabilize Time
Height, Prime Number, 4



Zbgs
Female
Gemini
50
72
Summoner
Punch Art
MP Restore
Concentrate
Lava Walking

Papyrus Codex

Black Hood
Mythril Vest
Red Shoes

Moogle, Golem, Carbunkle
Purification, Revive

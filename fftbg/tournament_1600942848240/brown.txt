Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Reddwind
Male
Leo
74
66
Knight
Basic Skill
Absorb Used MP
Magic Attack UP
Swim

Defender
Round Shield
Circlet
Plate Mail
Spike Shoes

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword, Surging Sword
Heal



HASTERIOUS
Female
Gemini
53
75
Wizard
Elemental
Counter Tackle
Short Charge
Move-HP Up

Dragon Rod

Green Beret
Mystic Vest
Defense Ring

Fire 2, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4
Pitfall, Water Ball, Lava Ball



NovaKnight21
Female
Pisces
78
64
Oracle
Time Magic
HP Restore
Magic Defense UP
Move+2

Iron Fan

Black Hood
Black Robe
Red Shoes

Blind, Poison, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy
Haste 2, Slow 2, Reflect, Quick, Demi 2, Stabilize Time, Meteor, Galaxy Stop



Ayeayex3
Female
Cancer
52
77
Wizard
Battle Skill
Damage Split
Equip Sword
Waterwalking

Coral Sword

Barette
Wizard Robe
Elf Mantle

Fire, Fire 4, Bolt 2, Bolt 4, Ice, Ice 3, Empower, Death, Flare
Armor Break, Shield Break, Weapon Break, Power Break, Stasis Sword, Night Sword, Surging Sword

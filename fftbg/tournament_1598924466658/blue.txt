Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Resjudicata3
Male
Virgo
47
76
Lancer
Summon Magic
MA Save
Maintenance
Levitate

Cypress Rod
Flame Shield
Crystal Helmet
Robe of Lords
Red Shoes

Level Jump4, Vertical Jump8
Moogle, Shiva, Ifrit, Carbunkle, Odin, Salamander, Cyclops



Redmage4evah
Male
Aries
55
78
Summoner
Basic Skill
Dragon Spirit
Concentrate
Ignore Height

Thunder Rod

Flash Hat
Black Robe
Salty Rage

Moogle, Ramuh, Golem, Leviathan, Silf, Cyclops
Dash, Throw Stone, Heal, Yell, Wish, Scream



Gorgewall
Male
Libra
54
66
Samurai
White Magic
Dragon Spirit
Equip Sword
Ignore Terrain

Excalibur

Crystal Helmet
Bronze Armor
Angel Ring

Asura, Koutetsu, Bizen Boat, Kikuichimoji
Cure 2, Raise, Raise 2, Shell 2, Wall, Esuna, Holy



BooshPlays
Female
Virgo
79
76
Monk
Yin Yang Magic
Auto Potion
Defense UP
Lava Walking



Thief Hat
Judo Outfit
Reflect Ring

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Sleep

Player: !Red
Team: Red Team
Palettes: Red/Brown



Dasutin23
Male
Scorpio
41
56
Ninja
Summon Magic
Distribute
Equip Gun
Lava Walking

Papyrus Codex
Battle Folio
Flash Hat
Mystic Vest
Genji Gauntlet

Bomb, Staff
Moogle, Titan, Carbunkle, Leviathan



DeathTaxesAndAnime
Female
Taurus
74
77
Mediator
Yin Yang Magic
Counter
Dual Wield
Jump+3

Bestiary
Bestiary
Golden Hairpin
Black Costume
Battle Boots

Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Refute, Rehabilitate
Poison, Spell Absorb, Life Drain, Pray Faith, Dispel Magic, Paralyze



Tsuike
Female
Leo
80
46
Samurai
Elemental
MA Save
Equip Armor
Ignore Terrain

Masamune

Leather Hat
Judo Outfit
Dracula Mantle

Asura, Murasame, Heaven's Cloud, Kikuichimoji
Pitfall, Water Ball, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Lava Ball



Lord Gwarth
Male
Virgo
47
70
Lancer
Punch Art
Catch
Attack UP
Retreat

Mythril Spear
Round Shield
Diamond Helmet
Linen Cuirass
Diamond Armlet

Level Jump2, Vertical Jump8
Purification, Revive, Seal Evil

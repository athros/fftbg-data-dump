Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Maakur
Monster
Libra
77
55
Byblos










WhiteTigress
Male
Serpentarius
64
46
Squire
Battle Skill
Counter
Secret Hunt
Move-HP Up

Diamond Sword
Gold Shield
Mythril Helmet
Earth Clothes
Magic Gauntlet

Dash, Heal, Tickle, Cheer Up, Fury
Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword



Galkife
Male
Cancer
51
75
Oracle
Black Magic
Parry
Maintenance
Levitate

Gokuu Rod

Golden Hairpin
Black Costume
Angel Ring

Life Drain, Blind Rage, Foxbird, Dispel Magic
Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3



Joewcarson
Male
Capricorn
57
44
Samurai
Elemental
Arrow Guard
Equip Bow
Swim

Silver Bow

Cross Helmet
Chain Mail
108 Gems

Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ANFz
Male
Capricorn
53
73
Geomancer
Black Magic
PA Save
Martial Arts
Move+1


Escutcheon
Red Hood
Light Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire, Fire 2, Fire 3, Bolt 2, Ice 2, Ice 4



StealthModeLocke
Male
Scorpio
52
63
Squire
Steal
Speed Save
Attack UP
Move-MP Up

Bow Gun
Bronze Shield
Circlet
Chain Mail
Jade Armlet

Throw Stone, Heal, Tickle, Yell, Wish
Steal Helmet, Steal Status, Arm Aim, Leg Aim



Resjudicata3
Female
Sagittarius
49
67
Summoner
Draw Out
Counter Flood
Equip Gun
Move-HP Up

Blaze Gun

Black Hood
Black Costume
Rubber Shoes

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Leviathan, Salamander, Silf, Fairy, Zodiac
Bizen Boat, Heaven's Cloud, Kikuichimoji



Powergems
Male
Libra
64
42
Wizard
Math Skill
Regenerator
Equip Armor
Ignore Terrain

Rod

Crystal Helmet
Chain Mail
N-Kai Armlet

Bolt, Bolt 2, Frog, Flare
CT, 5, 4

Player: !Red
Team: Red Team
Palettes: Red/Brown



YowaneHaku
Male
Libra
71
39
Bard
Summon Magic
Auto Potion
Concentrate
Waterbreathing

Ice Bow

Black Hood
Diamond Armor
Spike Shoes

Battle Song, Magic Song, Diamond Blade, Space Storage
Moogle, Shiva, Golem, Bahamut, Leviathan



Ogema TheDream
Female
Sagittarius
54
43
Dancer
Yin Yang Magic
Counter
Defense UP
Swim

Cashmere

Black Hood
Light Robe
Setiemson

Nameless Dance, Last Dance, Void Storage, Nether Demon, Dragon Pit
Spell Absorb, Life Drain, Silence Song, Blind Rage, Foxbird, Dispel Magic, Dark Holy



ValensEXP
Male
Libra
62
61
Time Mage
Item
Dragon Spirit
Throw Item
Ignore Terrain

Mace of Zeus

Barette
Wizard Robe
Magic Gauntlet

Haste, Stop, Float, Quick, Stabilize Time
Potion, Hi-Potion, Antidote, Remedy, Phoenix Down



GenZealot
Male
Pisces
52
49
Archer
Yin Yang Magic
Caution
Attack UP
Move+3

Blaze Gun
Bronze Shield
Leather Hat
Mystic Vest
Genji Gauntlet

Charge+2, Charge+3, Charge+4, Charge+10
Poison, Silence Song, Dark Holy

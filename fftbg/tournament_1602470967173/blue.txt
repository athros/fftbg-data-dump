Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Laserman1000
Male
Cancer
53
80
Calculator
Ranch Skill
Mana Shield
Martial Arts
Teleport 2

Papyrus Codex

Bronze Helmet
Chain Vest
Germinas Boots

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor



DrAntiSocial
Male
Aquarius
67
71
Chemist
Elemental
Caution
Dual Wield
Move+2

Romanda Gun
Blaze Gun
Green Beret
Judo Outfit
Magic Gauntlet

Potion, Hi-Potion, Hi-Ether, Antidote, Phoenix Down
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Gusty Wind, Lava Ball



ArchKnightX
Female
Cancer
50
58
Archer
Time Magic
Mana Shield
Magic Attack UP
Levitate

Silver Bow

Green Beret
Power Sleeve
Small Mantle

Charge+1, Charge+3, Charge+5
Stop, Quick, Stabilize Time



Nickyfive
Female
Pisces
63
66
Time Mage
Draw Out
Counter
Defense UP
Jump+1

Gokuu Rod

Feather Hat
Chain Vest
Defense Ring

Haste, Slow, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time, Galaxy Stop
Heaven's Cloud, Kiyomori

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sinnyil2
Male
Leo
44
47
Bard
Talk Skill
Brave Save
Doublehand
Jump+1

Ramia Harp

Twist Headband
Brigandine
N-Kai Armlet

Cheer Song, Battle Song, Magic Song
Persuade, Praise, Preach, Mimic Daravon, Refute, Rehabilitate



RunicMagus
Female
Gemini
56
54
Calculator
Black Magic
MP Restore
Doublehand
Waterwalking

Musk Rod

Holy Miter
Chain Vest
Defense Ring

CT, Height, 5, 4, 3
Fire, Fire 3, Fire 4, Bolt, Death



Forkmore
Female
Libra
67
53
Thief
Battle Skill
Critical Quick
Halve MP
Waterwalking

Ancient Sword

Feather Hat
Judo Outfit
Battle Boots

Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Arm Aim
Mind Break



Mrfripps
Male
Scorpio
65
63
Monk
Draw Out
Earplug
Long Status
Jump+1



Cachusha
Black Costume
Magic Gauntlet

Earth Slash, Seal Evil
Asura, Muramasa

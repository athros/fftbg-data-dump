Player: !Black
Team: Black Team
Palettes: Black/Red



Khelor
Female
Scorpio
65
48
Squire
Battle Skill
Absorb Used MP
Equip Gun
Fly

Bestiary
Hero Shield
Cross Helmet
Diamond Armor
Wizard Mantle

Dash, Yell, Cheer Up, Wish
Speed Break, Stasis Sword, Justice Sword



Regularguy428
Female
Aries
59
64
Mediator
Summon Magic
Auto Potion
Long Status
Retreat

Papyrus Codex

Green Beret
White Robe
Bracer

Praise, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute
Titan, Golem, Lich



Chuckolator
Male
Serpentarius
49
78
Samurai
Yin Yang Magic
Arrow Guard
Magic Attack UP
Waterwalking

Heaven's Cloud

Gold Helmet
Gold Armor
Defense Armlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Blind, Spell Absorb, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Sleep



HASTERIOUS
Male
Capricorn
46
52
Ninja
Elemental
Distribute
Beastmaster
Jump+3

Scorpion Tail
Flame Whip
Holy Miter
Clothes
Small Mantle

Hammer
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !White
Team: White Team
Palettes: White/Blue



Nwalme583
Male
Aries
47
80
Squire
Black Magic
Brave Save
Equip Sword
Waterwalking

Koutetsu Knife
Flame Shield
Circlet
Diamond Armor
Power Wrist

Dash, Throw Stone, Heal, Tickle
Fire 2, Bolt, Bolt 2, Ice, Ice 3, Flare



Jeeboheebo
Female
Libra
36
46
Lancer
Basic Skill
Damage Split
Doublehand
Move-HP Up

Gokuu Rod

Leather Helmet
Diamond Armor
Dracula Mantle

Level Jump8, Vertical Jump8
Throw Stone, Heal, Yell, Wish



Grishhammer
Female
Scorpio
72
78
Geomancer
Basic Skill
Dragon Spirit
Attack UP
Fly

Panther Bag
Mythril Shield
Thief Hat
Brigandine
Genji Gauntlet

Pitfall, Hell Ivy, Static Shock, Quicksand, Sand Storm, Gusty Wind
Throw Stone, Tickle



Just Here2
Male
Capricorn
49
70
Squire
Elemental
Caution
Equip Sword
Move+2

Koutetsu Knife
Crystal Shield
Leather Helmet
Linen Cuirass
Feather Mantle

Tickle, Wish
Pitfall, Water Ball, Will-O-Wisp, Blizzard

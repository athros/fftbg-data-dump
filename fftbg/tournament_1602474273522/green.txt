Player: !Green
Team: Green Team
Palettes: Green/White



JethroThrul
Female
Scorpio
56
54
Ninja
Basic Skill
Catch
Short Status
Move+3

Iga Knife
Spell Edge
Leather Hat
Power Sleeve
108 Gems

Shuriken, Bomb
Accumulate, Dash, Throw Stone, Heal, Tickle, Fury



Laserman1000
Male
Aquarius
72
75
Calculator
Lucavi Skill
MP Restore
Long Status
Move+3

Rod

Green Beret
Earth Clothes
Angel Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



RunicMagus
Monster
Scorpio
63
50
Juravis










Mirapoix
Female
Scorpio
77
41
Mediator
Yin Yang Magic
Caution
Secret Hunt
Lava Walking

Battle Folio

Holy Miter
Judo Outfit
108 Gems

Invitation, Praise, Preach
Blind, Doubt Faith, Dispel Magic, Paralyze

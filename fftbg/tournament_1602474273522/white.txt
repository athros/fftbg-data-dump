Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Pisces
75
78
Knight
Throw
MP Restore
Beastmaster
Move+3

Slasher
Diamond Shield
Bronze Helmet
Leather Armor
Defense Ring

Armor Break, Shield Break, Speed Break
Shuriken, Knife, Sword



Krombobreaker
Female
Aries
78
60
Geomancer
Summon Magic
HP Restore
Magic Attack UP
Jump+3

Giant Axe
Bronze Shield
Black Hood
Black Robe
Power Wrist

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Lava Ball
Moogle, Titan, Golem, Carbunkle, Bahamut, Odin, Salamander, Fairy, Lich



IBardic
Male
Virgo
72
45
Knight
Steal
Speed Save
Dual Wield
Ignore Terrain

Ragnarok
Sleep Sword
Cross Helmet
Carabini Mail
Rubber Shoes

Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Night Sword, Surging Sword
Steal Heart, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Silentperogy
Female
Cancer
70
62
Mediator
Draw Out
MA Save
Long Status
Jump+3

Papyrus Codex

Red Hood
Leather Outfit
Defense Ring

Persuade, Death Sentence, Negotiate, Mimic Daravon, Refute
Asura, Heaven's Cloud

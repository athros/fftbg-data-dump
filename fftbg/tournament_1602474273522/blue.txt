Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ar Tactic
Female
Pisces
82
53
Geomancer
Black Magic
Counter
Halve MP
Teleport

Slasher
Ice Shield
Red Hood
Linen Robe
Red Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Fire 3, Bolt 2, Bolt 3, Ice 2, Ice 4, Death



SQUiDSQUARKLIN
Monster
Scorpio
70
80
Holy Dragon










3ngag3
Female
Pisces
67
75
Mediator
Item
Critical Quick
Throw Item
Retreat

Blaze Gun

Twist Headband
Brigandine
Small Mantle

Persuade, Preach, Death Sentence, Refute
Potion, Hi-Ether, Antidote



ColetteMSLP
Female
Sagittarius
71
51
Chemist
Time Magic
Dragon Spirit
Dual Wield
Waterbreathing

Nagrarock
Star Bag
Ribbon
Judo Outfit
Battle Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Haste 2, Stop, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time

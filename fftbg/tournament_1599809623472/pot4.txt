Final Bets: purple - 9 bets for 4,649G (36.2%, x1.76); brown - 10 bets for 8,195G (63.8%, x0.57)

purple bets:
upvla: 1,628G (35.0%, 1,628G)
BirbBrainsBot: 562G (12.1%, 47,688G)
Firesheath: 548G (11.8%, 548G)
ser_pyrro: 476G (10.2%, 476G)
AllInBot: 457G (9.8%, 457G)
getthemoneyz: 430G (9.2%, 1,913,584G)
hotpocketsofficial: 232G (5.0%, 232G)
iBardic: 200G (4.3%, 19,651G)
Riley331: 116G (2.5%, 116G)

brown bets:
thunderducker: 2,725G (33.3%, 2,725G)
DeathTaxesAndAnime: 2,518G (30.7%, 4,938G)
fattunaking: 1,000G (12.2%, 51,282G)
Raixelol: 592G (7.2%, 15,403G)
PolkaGrandma: 400G (4.9%, 2,461G)
ArrenJevleth: 300G (3.7%, 5,683G)
ValensEXP_: 260G (3.2%, 260G)
datadrivenbot: 200G (2.4%, 58,192G)
BartTradingCompany: 100G (1.2%, 4,168G)
DHaveWord: 100G (1.2%, 3,324G)

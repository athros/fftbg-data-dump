Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Helpimabug
Male
Aquarius
69
45
Lancer
Draw Out
Dragon Spirit
Sicken
Move-MP Up

Holy Lance
Escutcheon
Platinum Helmet
Mythril Armor
Cursed Ring

Level Jump8, Vertical Jump6
Koutetsu, Muramasa



NicoSavoy
Female
Aquarius
76
51
Mime

Counter
Martial Arts
Levitate



Holy Miter
Secret Clothes
Bracer

Mimic




Bruubarg
Female
Libra
68
79
Knight
Steal
Counter Tackle
Defend
Move-MP Up

Giant Axe
Genji Shield
Leather Helmet
Platinum Armor
Defense Armlet

Weapon Break, Power Break, Mind Break, Justice Sword, Explosion Sword
Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Leg Aim



Upvla
Male
Scorpio
58
54
Lancer
Steal
Parry
Dual Wield
Jump+1

Partisan
Spear
Genji Helmet
Gold Armor
Germinas Boots

Level Jump2, Vertical Jump5
Steal Helmet, Steal Shield, Steal Status

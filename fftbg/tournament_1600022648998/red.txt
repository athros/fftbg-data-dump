Player: !Red
Team: Red Team
Palettes: Red/Brown



Ericzubat
Male
Taurus
72
53
Squire
Summon Magic
Counter
Equip Gun
Move-MP Up

Romanda Gun
Flame Shield
Flash Hat
Brigandine
Defense Armlet

Throw Stone, Heal, Yell, Fury
Moogle, Ramuh, Carbunkle, Leviathan, Fairy, Lich



Fattunaking
Male
Aries
63
58
Monk
Charge
Abandon
Attack UP
Fly



Holy Miter
Judo Outfit
Power Wrist

Spin Fist, Earth Slash, Revive
Charge+1, Charge+2, Charge+7



HuffFlex
Monster
Scorpio
53
58
Grenade










CrazyLou64
Male
Aquarius
59
40
Wizard
Punch Art
Critical Quick
Equip Shield
Move+1

Star Bag
Aegis Shield
Leather Hat
Judo Outfit
Elf Mantle

Fire, Bolt, Bolt 4, Death, Flare
Wave Fist, Secret Fist, Purification

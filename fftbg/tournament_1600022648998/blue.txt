Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sharknaldson
Male
Leo
54
60
Bard
Punch Art
Earplug
Magic Attack UP
Levitate

Silver Bow

Holy Miter
Diamond Armor
Angel Ring

Angel Song, Life Song, Diamond Blade
Purification



Fenaen
Male
Aries
44
80
Lancer
Battle Skill
Speed Save
Defend
Move+2

Iron Fan
Platinum Shield
Diamond Helmet
Black Robe
Cherche

Level Jump4, Vertical Jump2
Weapon Break



CorpusCav
Male
Libra
49
74
Ninja
Steal
Mana Shield
Magic Defense UP
Jump+3

Sasuke Knife
Ninja Edge
Green Beret
Clothes
Power Wrist

Shuriken, Staff
Steal Helmet, Steal Accessory



Douchetron
Male
Virgo
38
77
Knight
Time Magic
Absorb Used MP
Long Status
Move+1

Materia Blade
Platinum Shield
Cross Helmet
Mythril Armor
108 Gems

Armor Break, Shield Break, Mind Break, Stasis Sword
Haste, Haste 2, Slow, Slow 2, Stop, Float, Stabilize Time

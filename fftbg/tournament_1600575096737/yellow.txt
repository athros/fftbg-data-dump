Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nok
Female
Leo
53
59
Samurai
White Magic
HP Restore
Magic Defense UP
Move+3

Kikuichimoji

Barbuta
Linen Robe
Angel Ring

Asura, Murasame, Kiyomori, Muramasa, Kikuichimoji
Cure 4, Raise, Reraise, Shell, Esuna



Lyner87
Male
Virgo
61
44
Thief
Item
Counter
Throw Item
Move+1

Long Sword

Golden Hairpin
Clothes
Rubber Shoes

Steal Helmet, Steal Armor
Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



Khelor
Monster
Taurus
56
56
Steel Giant










Zenlion
Male
Capricorn
62
61
Time Mage
Talk Skill
PA Save
Secret Hunt
Move-HP Up

White Staff

Feather Hat
Chameleon Robe
Jade Armlet

Slow, Slow 2, Immobilize, Float, Quick, Demi, Stabilize Time
Threaten, Preach, Solution, Refute, Rehabilitate

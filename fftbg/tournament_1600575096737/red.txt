Player: !Red
Team: Red Team
Palettes: Red/Brown



Lifebregin
Female
Libra
57
48
Knight
Dance
Arrow Guard
Equip Polearm
Move+2

Javelin
Crystal Shield
Barbuta
Bronze Armor
Genji Gauntlet

Armor Break, Magic Break, Mind Break, Surging Sword
Obsidian Blade, Dragon Pit



PopsLIVE
Male
Sagittarius
78
73
Time Mage
Black Magic
Distribute
Defense UP
Waterwalking

Gokuu Rod

Feather Hat
Mystic Vest
Feather Boots

Haste, Haste 2, Slow 2, Quick, Stabilize Time
Bolt, Bolt 3, Ice 2



OneHundredFists
Male
Sagittarius
61
75
Summoner
Talk Skill
Critical Quick
Equip Armor
Jump+1

Bestiary

Cross Helmet
White Robe
Bracer

Moogle, Shiva, Ramuh, Carbunkle, Bahamut
Solution, Mimic Daravon, Refute, Rehabilitate



MemoriesofFinal
Female
Capricorn
62
54
Calculator
Lucavi Skill
Counter
Equip Armor
Waterbreathing

Battle Folio
Flame Shield
Thief Hat
White Robe
Angel Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima

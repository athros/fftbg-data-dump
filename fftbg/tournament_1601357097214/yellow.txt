Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Latebit
Male
Serpentarius
52
75
Archer
Talk Skill
Mana Shield
Equip Axe
Retreat

Gold Staff
Buckler
Feather Hat
Black Costume
Germinas Boots

Charge+1, Charge+3
Praise, Preach, Death Sentence, Negotiate, Mimic Daravon



TheBrett
Female
Scorpio
69
63
Thief
Summon Magic
Abandon
Beastmaster
Move-MP Up

Coral Sword

Feather Hat
Chain Vest
Battle Boots

Steal Helmet, Steal Accessory, Arm Aim
Moogle, Ramuh, Titan, Carbunkle, Leviathan, Salamander, Cyclops



Thunderducker
Male
Libra
72
64
Lancer
Battle Skill
Caution
Equip Armor
Jump+1

Star Bag
Genji Shield
Feather Hat
Wizard Robe
108 Gems

Level Jump8, Vertical Jump8
Shield Break, Magic Break, Power Break, Night Sword



Thyrandaal
Female
Aries
54
76
Calculator
White Magic
PA Save
Equip Polearm
Move-MP Up

Holy Lance

Ribbon
White Robe
Magic Gauntlet

CT, 5
Cure, Raise, Regen, Esuna

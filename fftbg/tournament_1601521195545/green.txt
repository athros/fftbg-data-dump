Player: !Green
Team: Green Team
Palettes: Green/White



ANFz
Male
Virgo
82
62
Geomancer
Charge
Faith Save
Magic Attack UP
Move+2

Kiyomori
Platinum Shield
Headgear
Secret Clothes
Genji Gauntlet

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+2, Charge+4, Charge+7



TheBrett
Male
Gemini
79
71
Summoner
Draw Out
Dragon Spirit
Equip Knife
Waterwalking

Spell Edge

Red Hood
Black Robe
Angel Ring

Moogle, Ifrit, Golem, Carbunkle, Salamander, Lich
Asura



Aldrammech
Female
Capricorn
69
40
Mediator
Charge
Abandon
Equip Gun
Waterwalking

Mythril Gun

Green Beret
Clothes
Small Mantle

Persuade, Insult, Mimic Daravon
Charge+5



Krombobreaker
Male
Taurus
75
52
Archer
Elemental
Counter Flood
Doublehand
Jump+3

Mythril Gun

Leather Hat
Black Costume
Rubber Shoes

Charge+1, Charge+3, Charge+20
Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind, Lava Ball

Player: !White
Team: White Team
Palettes: White/Blue



Smashy
Female
Pisces
76
46
Samurai
Throw
Parry
Defend
Retreat

Kiyomori

Leather Helmet
Platinum Armor
Jade Armlet

Asura, Bizen Boat, Muramasa, Kikuichimoji
Spear



Yangusburger
Male
Serpentarius
43
54
Bard
Draw Out
Regenerator
Maintenance
Ignore Height

Bloody Strings

Golden Hairpin
Gold Armor
Leather Mantle

Magic Song, Last Song, Space Storage, Hydra Pit
Murasame, Heaven's Cloud



ArchKnightX
Male
Virgo
47
72
Lancer
Item
Caution
Throw Item
Retreat

Holy Lance
Venetian Shield
Mythril Helmet
Leather Armor
Cursed Ring

Level Jump8, Vertical Jump8
Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



Nhammen
Male
Virgo
63
51
Lancer
White Magic
Auto Potion
Defense UP
Ignore Terrain

Javelin
Escutcheon
Circlet
Plate Mail
Vanish Mantle

Level Jump5, Vertical Jump7
Cure, Cure 2, Raise, Reraise, Regen, Protect 2, Shell, Wall, Esuna

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Forkmore
Male
Sagittarius
45
41
Archer
Talk Skill
Meatbone Slash
Concentrate
Swim

Lightning Bow

Holy Miter
Adaman Vest
Rubber Shoes

Charge+1, Charge+10
Invitation, Praise, Insult, Refute, Rehabilitate



CapnChaos12
Male
Cancer
64
45
Thief
White Magic
Speed Save
Equip Gun
Move+1

Papyrus Codex

Flash Hat
Brigandine
Bracer

Gil Taking, Steal Weapon, Steal Accessory, Steal Status
Cure 3, Cure 4, Raise, Reraise, Protect, Wall, Esuna



Rytor
Male
Cancer
55
48
Monk
Draw Out
Mana Shield
Equip Bow
Move-HP Up

Mythril Bow

Triangle Hat
Brigandine
Bracer

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Asura, Koutetsu



Laserman1000
Male
Capricorn
79
52
Calculator
Demon Skill
Speed Save
Attack UP
Move-HP Up

Battle Folio
Gold Shield
Leather Hat
Plate Mail
Sprint Shoes

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2

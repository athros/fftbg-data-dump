Final Bets: green - 16 bets for 12,127G (48.7%, x1.06); yellow - 15 bets for 12,796G (51.3%, x0.95)

green bets:
Mesmaster: 5,000G (41.2%, 209,453G)
reinoe: 1,261G (10.4%, 1,261G)
ANFz: 1,098G (9.1%, 1,098G)
BirbBrainsBot: 1,000G (8.2%, 135,555G)
dogsandcatsand: 1,000G (8.2%, 1,961G)
Thyrandaal: 708G (5.8%, 708G)
SkylerBunny: 660G (5.4%, 660G)
getthemoneyz: 280G (2.3%, 1,878,317G)
brodeity: 256G (2.1%, 256G)
CorpusCav: 250G (2.1%, 24,922G)
datadrivenbot: 200G (1.6%, 58,863G)
gorgewall: 101G (0.8%, 2,775G)
AllInBot: 100G (0.8%, 100G)
Lushifah: 100G (0.8%, 717G)
maakur_: 100G (0.8%, 6,549G)
BartTradingCompany: 13G (0.1%, 130G)

yellow bets:
DeathTaxesAndAnime: 5,149G (40.2%, 10,097G)
snkey: 3,600G (28.1%, 94,050G)
VolgraTheMoose: 1,001G (7.8%, 108,297G)
SephDarkheart: 620G (4.8%, 620G)
krombobreaker: 500G (3.9%, 5,908G)
ColetteMSLP: 400G (3.1%, 5,597G)
CapnChaos12: 300G (2.3%, 2,777G)
Strifu: 218G (1.7%, 428G)
thebluesman: 208G (1.6%, 208G)
macradarkstar: 200G (1.6%, 797G)
Morsigil: 200G (1.6%, 989G)
nhammen: 100G (0.8%, 402,440G)
Miku_Shikhu: 100G (0.8%, 7,652G)
Yangusburger: 100G (0.8%, 8,259G)
formerlydrdong: 100G (0.8%, 2,137G)

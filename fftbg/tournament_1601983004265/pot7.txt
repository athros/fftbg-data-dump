Final Bets: green - 5 bets for 2,477G (36.2%, x1.77); black - 8 bets for 4,372G (63.8%, x0.57)

green bets:
Zachara: 900G (36.3%, 109,400G)
mindblownnnn: 777G (31.4%, 22,933G)
krombobreaker: 500G (20.2%, 40,993G)
datadrivenbot: 200G (8.1%, 88,889G)
MemoriesofFinal: 100G (4.0%, 23,571G)

black bets:
Mushufasa_: 1,234G (28.2%, 22,684G)
BirbBrainsBot: 1,000G (22.9%, 103,613G)
HorusTaurus: 1,000G (22.9%, 3,869G)
khelor_: 500G (11.4%, 14,054G)
AllInBot: 200G (4.6%, 200G)
resjudicata3: 200G (4.6%, 5,777G)
getthemoneyz: 126G (2.9%, 2,178,933G)
FuzzyTigers: 112G (2.6%, 112G)

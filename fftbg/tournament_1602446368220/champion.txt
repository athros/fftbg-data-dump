Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



King Smashington
Male
Sagittarius
69
44
Archer
Battle Skill
Arrow Guard
Attack UP
Move+3

Stone Gun
Platinum Shield
Flash Hat
Mythril Vest
Diamond Armlet

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Head Break, Explosion Sword



Zbgs
Male
Libra
53
76
Summoner
Basic Skill
Parry
Equip Knife
Levitate

Dagger

Twist Headband
Chameleon Robe
Magic Gauntlet

Titan, Carbunkle, Silf
Accumulate, Dash, Tickle



XXTysongXx
Female
Cancer
47
61
Thief
Basic Skill
Counter
Dual Wield
Levitate

Diamond Sword
Hydra Bag
Flash Hat
Brigandine
Leather Mantle

Steal Heart, Steal Armor, Steal Shield, Steal Status, Leg Aim
Dash, Heal, Tickle, Yell, Scream



Sairentozon7
Male
Gemini
80
71
Monk
Time Magic
Faith Save
Magic Defense UP
Jump+2



Red Hood
Leather Outfit
Cherche

Wave Fist, Earth Slash, Secret Fist, Chakra, Revive
Stop, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time

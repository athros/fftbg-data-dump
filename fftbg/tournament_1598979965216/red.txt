Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Male
Virgo
64
47
Knight
Sing
Earplug
Halve MP
Teleport 2

Coral Sword
Buckler
Bronze Helmet
Black Robe
Spike Shoes

Armor Break, Power Break, Mind Break, Justice Sword
Life Song, Battle Song, Hydra Pit



Zachara
Male
Taurus
50
66
Ninja
Talk Skill
HP Restore
Equip Gun
Move+3

Mythril Gun
Mythril Gun
Black Hood
Power Sleeve
Magic Ring

Shuriken, Bomb, Knife, Dictionary
Invitation, Preach, Negotiate, Refute



DavenIII
Female
Capricorn
53
58
Oracle
Math Skill
Earplug
Halve MP
Move+2

Ivory Rod

Feather Hat
Chameleon Robe
Defense Ring

Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic
CT, Prime Number, 5



Thyrandaal
Male
Libra
78
64
Knight
Throw
Faith Save
Dual Wield
Move+3

Save the Queen
Coral Sword
Diamond Helmet
Leather Armor
Bracer

Armor Break, Weapon Break, Stasis Sword
Shuriken, Bomb, Staff

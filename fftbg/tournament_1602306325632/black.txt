Player: !Black
Team: Black Team
Palettes: Black/Red



Mesmaster
Female
Libra
67
49
Archer
Summon Magic
Counter Tackle
Equip Polearm
Ignore Height

Gokuu Rod
Round Shield
Circlet
Clothes
Defense Ring

Charge+1
Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Leviathan, Salamander, Lich



Meanderandreturn
Female
Taurus
72
47
Calculator
Time Magic
Parry
Equip Axe
Levitate

Giant Axe

Holy Miter
Black Costume
Battle Boots

CT, Height, Prime Number, 5, 4, 3
Haste, Slow, Reflect, Demi 2, Stabilize Time



Evewho
Female
Taurus
42
69
Chemist
White Magic
Counter Flood
Dual Wield
Waterwalking

Hydra Bag
Air Knife
Holy Miter
Clothes
Germinas Boots

Potion, Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Cure, Cure 3, Raise, Reraise, Wall, Esuna



NovaKnight21
Male
Leo
64
69
Lancer
Sing
Mana Shield
Short Status
Move-MP Up

Spear
Mythril Shield
Platinum Helmet
Leather Armor
Jade Armlet

Level Jump8, Vertical Jump7
Life Song, Cheer Song, Diamond Blade, Sky Demon

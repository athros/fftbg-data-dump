Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DLJuggernaut
Female
Capricorn
61
77
Calculator
Time Magic
Counter Magic
Maintenance
Teleport

Papyrus Codex

Feather Hat
Light Robe
Sprint Shoes

CT, Height, Prime Number, 4
Haste, Stop, Float, Reflect, Demi 2, Stabilize Time



Aldrammech
Female
Cancer
62
74
Chemist
White Magic
Distribute
Equip Knife
Ignore Height

Spell Edge

Barette
Leather Outfit
Dracula Mantle

Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Remedy
Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Wall, Esuna



ExplanationMarkWow
Male
Leo
51
71
Samurai
White Magic
Distribute
Beastmaster
Jump+1

Kikuichimoji

Iron Helmet
Light Robe
Rubber Shoes

Asura, Koutetsu, Murasame
Cure, Cure 4, Raise, Raise 2, Reraise, Regen, Shell, Esuna, Holy



Chuckolator
Male
Sagittarius
81
64
Mediator
Basic Skill
HP Restore
Equip Axe
Fly

Oak Staff

Triangle Hat
Leather Outfit
Genji Gauntlet

Praise, Threaten, Mimic Daravon
Dash, Throw Stone, Heal, Scream

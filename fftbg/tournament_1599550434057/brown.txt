Player: !Brown
Team: Brown Team
Palettes: Brown/Green



RobotOcelot
Monster
Aquarius
67
78
Chocobo










Lythe Caraker
Male
Capricorn
74
60
Archer
White Magic
Meatbone Slash
Halve MP
Move-HP Up

Ultimus Bow

Cross Helmet
Earth Clothes
Germinas Boots

Charge+2, Charge+3, Charge+10
Raise, Reraise, Regen, Wall, Esuna



Yangusburger
Male
Leo
79
59
Monk
Summon Magic
Counter
Equip Axe
Teleport

Morning Star

Headgear
Adaman Vest
Small Mantle

Purification, Revive
Ifrit, Golem, Carbunkle



DeathTaxesAndAnime
Female
Aries
69
65
Mediator
Jump
HP Restore
Dual Wield
Jump+3

Papyrus Codex
Bestiary
Black Hood
Chameleon Robe
Reflect Ring

Persuade, Threaten, Preach
Level Jump5, Vertical Jump5

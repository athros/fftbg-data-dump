Player: !Brown
Team: Brown Team
Palettes: Brown/Green



LASERJESUS1337
Female
Gemini
44
78
Mediator
Basic Skill
Absorb Used MP
Dual Wield
Waterwalking

Stone Gun
Glacier Gun
Triangle Hat
Adaman Vest
Feather Mantle

Persuade, Praise, Threaten, Solution, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Dash, Heal, Tickle, Cheer Up



ArchKnightX
Male
Scorpio
43
54
Squire
Elemental
Counter
Magic Attack UP
Retreat

Hunting Bow
Bronze Shield
Green Beret
Chain Vest
Reflect Ring

Dash, Throw Stone, Tickle, Yell, Fury, Wish
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Scsuperstar
Male
Virgo
63
64
Calculator
Yin Yang Magic
Catch
Defend
Jump+3

Battle Folio

Golden Hairpin
Silk Robe
Germinas Boots

CT, Height, 5, 3
Poison, Blind Rage, Paralyze, Petrify



Mhorzak
Female
Sagittarius
56
56
Summoner
Battle Skill
Distribute
Maintenance
Ignore Height

Papyrus Codex

Feather Hat
Brigandine
Magic Ring

Moogle, Shiva, Ifrit, Golem, Leviathan, Salamander
Head Break, Justice Sword

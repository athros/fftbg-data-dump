Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ser Pyrro
Female
Cancer
66
64
Summoner
Yin Yang Magic
MA Save
Equip Axe
Lava Walking

Battle Axe

Holy Miter
Wizard Outfit
Elf Mantle

Moogle, Shiva, Carbunkle, Odin, Silf, Fairy, Lich
Pray Faith, Silence Song, Blind Rage, Dispel Magic, Sleep, Dark Holy



HASTERIOUS
Male
Aquarius
64
77
Samurai
Jump
Arrow Guard
Equip Knife
Waterwalking

Iga Knife

Crystal Helmet
Light Robe
Reflect Ring

Koutetsu, Muramasa
Level Jump2, Vertical Jump7



OneHundredFists
Male
Libra
52
82
Calculator
Dragon Skill
MP Restore
Secret Hunt
Retreat

Papyrus Codex

Feather Hat
Light Robe
Reflect Ring

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Prince Rogers Nelson
Male
Scorpio
77
79
Lancer
Throw
Brave Save
Defend
Retreat

Spear
Diamond Shield
Diamond Helmet
Carabini Mail
Defense Ring

Level Jump3, Vertical Jump2
Shuriken, Bomb, Wand

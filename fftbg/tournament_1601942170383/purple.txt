Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



E Ballard
Male
Sagittarius
57
47
Ninja
Draw Out
Mana Shield
Concentrate
Levitate

Flame Whip
Flail
Red Hood
Mythril Vest
Defense Armlet

Shuriken
Koutetsu, Heaven's Cloud, Muramasa



HuffFlex
Female
Virgo
79
54
Lancer
Item
Regenerator
Maintenance
Move+1

Obelisk
Hero Shield
Leather Helmet
Crystal Mail
Cursed Ring

Level Jump5, Vertical Jump8
Potion, Eye Drop, Echo Grass, Soft, Phoenix Down



Maakur
Female
Gemini
43
57
Lancer
Throw
Dragon Spirit
Martial Arts
Move-HP Up


Hero Shield
Leather Helmet
Diamond Armor
Cursed Ring

Level Jump5, Vertical Jump2
Shuriken, Bomb, Staff



VolgraTheMoose
Female
Pisces
53
60
Summoner
Battle Skill
Parry
Short Charge
Swim

Dragon Rod

Red Hood
Mystic Vest
Cursed Ring

Shiva, Ramuh, Ifrit, Odin, Silf, Lich, Cyclops
Weapon Break, Magic Break, Justice Sword, Dark Sword

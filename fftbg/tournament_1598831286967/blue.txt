Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Escobro
Female
Capricorn
80
56
Summoner
Draw Out
Earplug
Doublehand
Fly

Sage Staff

Thief Hat
Mystic Vest
Sprint Shoes

Moogle, Ifrit, Golem, Carbunkle, Salamander, Lich
Asura, Bizen Boat



Thunderducker
Male
Aquarius
60
63
Lancer
White Magic
Critical Quick
Concentrate
Lava Walking

Gungnir
Flame Shield
Crystal Helmet
White Robe
Defense Armlet

Level Jump3, Vertical Jump8
Raise, Regen, Protect 2, Wall, Esuna



Technominari
Female
Pisces
70
81
Priest
Charge
PA Save
Equip Gun
Move+1

Glacier Gun

Twist Headband
Wizard Robe
Magic Ring

Cure, Cure 2, Raise, Reraise, Shell, Shell 2, Esuna
Charge+5, Charge+7



Runeseeker22
Male
Cancer
57
70
Archer
Draw Out
HP Restore
Secret Hunt
Swim

Night Killer
Flame Shield
Diamond Helmet
Leather Outfit
Cursed Ring

Charge+1, Charge+2, Charge+5, Charge+7
Asura, Koutetsu, Murasame, Kiyomori, Muramasa

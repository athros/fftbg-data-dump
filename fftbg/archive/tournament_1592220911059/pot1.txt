Final Bets: red - 16 bets for 8,535G (41.8%, x1.39); blue - 9 bets for 11,882G (58.2%, x0.72)

red bets:
Breakdown777: 1,500G (17.6%, 72,492G)
LordSDB: 1,000G (11.7%, 10,724G)
edgehead62888: 1,000G (11.7%, 29,608G)
BirbBrainsBot: 1,000G (11.7%, 51,576G)
OppressivelyMinty: 1,000G (11.7%, 3,307G)
Belkra: 661G (7.7%, 661G)
reddwind_: 500G (5.9%, 1,950G)
Bryon_W: 472G (5.5%, 472G)
getthemoneyz: 412G (4.8%, 958,843G)
Lythe_Caraker: 250G (2.9%, 109,842G)
AltimaMantoid: 204G (2.4%, 204G)
dinin991: 136G (1.6%, 136G)
ApplesauceBoss: 100G (1.2%, 7,320G)
E_Ballard: 100G (1.2%, 9,421G)
datadrivenbot: 100G (1.2%, 40,808G)
CosmicTactician: 100G (1.2%, 5,105G)

blue bets:
UmaiJam: 8,107G (68.2%, 8,107G)
Mesmaster: 1,000G (8.4%, 100,376G)
nifboy: 560G (4.7%, 560G)
TheChainNerd: 528G (4.4%, 5,528G)
KittymanAdvance: 500G (4.2%, 1,066G)
prince_rogers_nelson_: 500G (4.2%, 11,009G)
gorgewall: 401G (3.4%, 19,422G)
AllInBot: 186G (1.6%, 186G)
jonassimple: 100G (0.8%, 100G)

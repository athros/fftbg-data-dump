Player: !Green
Team: Green Team
Palettes: Green/White



Coralreeferz
Male
Gemini
75
74
Lancer
Summon Magic
Distribute
Concentrate
Move+2

Holy Lance
Hero Shield
Gold Helmet
Chameleon Robe
Elf Mantle

Level Jump5, Vertical Jump8
Moogle, Titan, Carbunkle, Cyclops



RunicMagus
Male
Taurus
44
46
Calculator
Black Magic
Catch
Dual Wield
Jump+2

Octagon Rod
Battle Bamboo
Twist Headband
Linen Robe
Angel Ring

CT, Prime Number, 5, 4
Fire 2, Bolt 3, Ice, Ice 3, Frog, Death



ArlanKels
Monster
Sagittarius
52
67
Pisco Demon










Calajo
Male
Virgo
74
66
Geomancer
Steal
Counter Tackle
Attack UP
Lava Walking

Giant Axe
Buckler
Headgear
Wizard Outfit
Magic Gauntlet

Pitfall, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim

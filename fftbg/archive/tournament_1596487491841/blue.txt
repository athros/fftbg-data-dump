Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JonnyCue
Male
Pisces
50
72
Monk
Elemental
Abandon
Secret Hunt
Move+1



Feather Hat
Brigandine
Red Shoes

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind



Letdowncity
Male
Gemini
74
72
Ninja
Draw Out
Hamedo
Equip Gun
Jump+1

Stone Gun
Mythril Gun
Twist Headband
Earth Clothes
Small Mantle

Bomb, Knife
Koutetsu



Dtrain332
Female
Virgo
45
53
Monk
Elemental
Parry
Equip Axe
Move-MP Up

Giant Axe

Black Hood
Adaman Vest
N-Kai Armlet

Wave Fist, Earth Slash, Secret Fist, Purification
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Nifboy
Male
Gemini
47
55
Priest
Sing
Distribute
Short Status
Move-MP Up

Flail

Cachusha
Linen Robe
Cursed Ring

Cure 4, Raise, Raise 2, Reraise, Protect, Shell
Angel Song, Battle Song, Magic Song, Nameless Song, Sky Demon

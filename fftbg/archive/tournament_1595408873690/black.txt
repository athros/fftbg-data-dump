Player: !Black
Team: Black Team
Palettes: Black/Red



SomthingMore
Male
Aquarius
50
73
Calculator
White Magic
Absorb Used MP
Equip Sword
Ignore Height

Save the Queen

Holy Miter
Adaman Vest
Vanish Mantle

CT, Height, Prime Number, 5, 4
Cure, Raise, Raise 2, Regen, Protect 2, Esuna, Holy, Magic Barrier



UmaiJam
Male
Virgo
78
66
Archer
Throw
Faith Save
Doublehand
Move+1

Yoichi Bow

Thief Hat
Power Sleeve
Rubber Shoes

Charge+1, Charge+5
Shuriken



Lowlf
Male
Leo
68
65
Monk
Summon Magic
Brave Save
Maintenance
Waterwalking



Green Beret
Power Sleeve
Bracer

Earth Slash, Purification, Chakra, Revive
Moogle, Ramuh, Ifrit, Carbunkle, Bahamut



Lynn
Male
Capricorn
73
58
Bard
Black Magic
Meatbone Slash
Equip Shield
Lava Walking

Bloody Strings
Flame Shield
Black Hood
Mythril Armor
Leather Mantle

Angel Song, Battle Song, Last Song, Sky Demon
Fire, Fire 2, Ice 3

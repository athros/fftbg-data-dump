Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Thechosenkitty
Female
Sagittarius
65
41
Chemist
White Magic
HP Restore
Magic Defense UP
Lava Walking

Mythril Gun

Golden Hairpin
Black Costume
Genji Gauntlet

Potion, Hi-Potion, X-Potion, Maiden's Kiss, Soft, Phoenix Down
Cure 2, Cure 3, Protect, Shell, Shell 2



RunicMagus
Male
Libra
41
64
Monk
Elemental
Hamedo
Beastmaster
Jump+2



Feather Hat
Judo Outfit
Dracula Mantle

Pummel, Wave Fist, Secret Fist, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



BlaqkElectric
Female
Scorpio
44
66
Chemist
Charge
Dragon Spirit
Short Charge
Teleport

Mage Masher

Headgear
Leather Outfit
N-Kai Armlet

Hi-Ether, Antidote, Echo Grass, Maiden's Kiss
Charge+7, Charge+10



Skellibean
Female
Aquarius
45
76
Monk
Draw Out
Critical Quick
Halve MP
Jump+1



Twist Headband
Adaman Vest
108 Gems

Spin Fist, Chakra, Revive, Seal Evil
Asura, Bizen Boat

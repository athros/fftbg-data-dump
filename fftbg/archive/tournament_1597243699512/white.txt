Player: !White
Team: White Team
Palettes: White/Blue



Silentkaster
Male
Scorpio
56
80
Lancer
Steal
Hamedo
Halve MP
Move-MP Up

Mythril Spear
Flame Shield
Genji Helmet
Platinum Armor
Battle Boots

Level Jump2, Vertical Jump2
Steal Helmet



Maakur
Male
Cancer
69
67
Mediator
Draw Out
Distribute
Sicken
Jump+3

Battle Folio

Red Hood
Chain Vest
Elf Mantle

Invitation, Praise, Solution, Insult, Negotiate, Mimic Daravon, Refute
Asura, Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji



JonnyCue
Male
Cancer
80
61
Ninja
Jump
Caution
Equip Bow
Jump+2

Mythril Bow

Leather Hat
Black Costume
Sprint Shoes

Ninja Sword, Wand
Level Jump5, Vertical Jump8



NWOW 44
Male
Sagittarius
67
63
Monk
Sing
Critical Quick
Sicken
Jump+1



Leather Hat
Brigandine
Germinas Boots

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Nameless Song, Space Storage, Hydra Pit

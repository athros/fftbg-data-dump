Player: !Green
Team: Green Team
Palettes: Green/White



Vorap
Male
Aries
62
57
Oracle
Summon Magic
MP Restore
Magic Defense UP
Jump+1

Octagon Rod

Thief Hat
Linen Robe
Spike Shoes

Blind, Poison, Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep
Golem, Carbunkle, Bahamut, Odin



ShintaroNayaka
Female
Sagittarius
50
50
Time Mage
Draw Out
Speed Save
Halve MP
Move+2

Oak Staff

Red Hood
Rubber Costume
Dracula Mantle

Haste 2, Stop, Quick, Demi, Stabilize Time
Heaven's Cloud, Kiyomori



Roqqqpsi
Male
Taurus
74
50
Squire
Elemental
Meatbone Slash
Sicken
Teleport

Iron Sword
Buckler
Mythril Helmet
Brigandine
Rubber Shoes

Dash, Heal, Cheer Up, Wish, Scream
Pitfall, Water Ball, Hallowed Ground, Quicksand, Blizzard, Gusty Wind



Baron Von Scrub
Male
Leo
45
43
Geomancer
Punch Art
Arrow Guard
Short Charge
Move+2

Battle Axe
Flame Shield
Flash Hat
Mystic Vest
Small Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Pummel, Purification, Revive

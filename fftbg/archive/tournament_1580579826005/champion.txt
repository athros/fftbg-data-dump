Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Omegasuspekt
Female
Capricorn
46
78
Mediator
Draw Out
HP Restore
Dual Wield
Move+2

Mythril Gun
Romanda Gun
Green Beret
Chameleon Robe
Magic Gauntlet

Persuade, Praise, Threaten, Solution, Mimic Daravon, Refute
Asura, Koutetsu, Murasame



Lionhermit
Male
Aries
80
63
Mediator
Summon Magic
Counter Flood
Long Status
Jump+2

Romanda Gun

Headgear
White Robe
Reflect Ring

Persuade, Negotiate, Mimic Daravon, Refute
Moogle, Titan, Carbunkle, Odin, Leviathan, Lich



Sprudds
Female
Pisces
44
79
Knight
Jump
Counter Tackle
Equip Polearm
Move+1

Octagon Rod
Aegis Shield
Iron Helmet
Gold Armor
Small Mantle

Head Break, Shield Break, Mind Break, Dark Sword, Explosion Sword
Level Jump8, Vertical Jump2



MrJamDango
Monster
Virgo
51
54
Red Chocobo








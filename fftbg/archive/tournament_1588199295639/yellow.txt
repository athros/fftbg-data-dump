Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CosmicTactician
Male
Aquarius
62
67
Wizard
Basic Skill
MP Restore
Dual Wield
Retreat

Wizard Rod
Faith Rod
Barette
Adaman Vest
Magic Gauntlet

Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Flare
Accumulate, Heal, Wish, Scream



Old Overholt
Female
Cancer
68
57
Calculator
Yin Yang Magic
Catch
Attack UP
Jump+1

Bestiary

Thief Hat
Chain Vest
Spike Shoes

Height, Prime Number, 5, 4
Poison, Spell Absorb, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy



GreatRedDragon
Female
Leo
58
66
Priest
Elemental
Speed Save
Halve MP
Ignore Height

Gold Staff

Green Beret
Chameleon Robe
Setiemson

Cure 3, Raise, Reraise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna, Holy
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Lordminsc
Male
Virgo
65
56
Bard
Summon Magic
Parry
Equip Sword
Move+2

Ragnarok

Twist Headband
Genji Armor
Bracer

Life Song, Cheer Song, Magic Song, Last Song
Moogle, Golem, Carbunkle, Salamander, Lich

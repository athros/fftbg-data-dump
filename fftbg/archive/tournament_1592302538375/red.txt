Player: !Red
Team: Red Team
Palettes: Red/Brown



Reddwind
Male
Aquarius
79
48
Knight
Punch Art
Parry
Secret Hunt
Jump+1

Broad Sword
Genji Shield
Gold Helmet
Silk Robe
108 Gems

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Night Sword
Spin Fist, Wave Fist, Secret Fist, Purification, Chakra



HASTERIOUS
Female
Scorpio
45
42
Time Mage
Black Magic
Dragon Spirit
Defense UP
Move+1

Sage Staff

Headgear
Chameleon Robe
Small Mantle

Haste 2, Slow, Slow 2, Immobilize, Float, Reflect, Demi, Stabilize Time, Meteor
Fire 3, Ice 2, Ice 3, Empower, Flare



ShintaroNayaka
Male
Aries
47
53
Archer
Time Magic
Auto Potion
Magic Defense UP
Retreat

Night Killer
Platinum Shield
Cachusha
Mythril Vest
Power Wrist

Charge+1, Charge+7, Charge+10
Haste, Stop, Reflect, Demi 2, Stabilize Time



CassiePhoenix
Female
Libra
60
43
Dancer
Jump
Earplug
Sicken
Jump+3

Ryozan Silk

Triangle Hat
Rubber Costume
Germinas Boots

Wiznaibus, Polka Polka, Disillusion, Last Dance
Level Jump3, Vertical Jump3

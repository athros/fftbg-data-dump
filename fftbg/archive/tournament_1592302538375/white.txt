Player: !White
Team: White Team
Palettes: White/Blue



Serperemagus
Male
Taurus
40
65
Bard
Throw
Parry
Equip Shield
Move-MP Up

Bloody Strings
Aegis Shield
Green Beret
Brigandine
Sprint Shoes

Battle Song, Magic Song, Nameless Song, Hydra Pit
Shuriken, Spear



DeathTaxesAndAnime
Female
Cancer
79
44
Lancer
Basic Skill
Critical Quick
Dual Wield
Ignore Terrain

Partisan
Obelisk
Cross Helmet
Carabini Mail
Small Mantle

Level Jump4, Vertical Jump5
Accumulate, Dash, Throw Stone, Heal, Tickle, Fury, Wish



Reinoe
Monster
Virgo
51
45
Hydra










Vorap
Male
Leo
54
64
Samurai
Time Magic
Parry
Equip Bow
Move-HP Up

Bow Gun

Bronze Helmet
Carabini Mail
Magic Gauntlet

Koutetsu, Murasame, Heaven's Cloud, Muramasa
Haste 2, Slow, Float, Quick, Demi, Stabilize Time, Galaxy Stop

Final Bets: white - 9 bets for 5,714G (46.4%, x1.15); black - 11 bets for 6,598G (53.6%, x0.87)

white bets:
UmaiJam: 1,700G (29.8%, 75,114G)
RaIshtar: 1,001G (17.5%, 1,001G)
BirbBrainsBot: 1,000G (17.5%, 74,742G)
prince_rogers_nelson_: 572G (10.0%, 572G)
Bururururu: 504G (8.8%, 504G)
Lanshaft: 504G (8.8%, 19,378G)
getthemoneyz: 210G (3.7%, 1,389,350G)
Lydian_C: 123G (2.2%, 21,737G)
Scurg: 100G (1.8%, 3,959G)

black bets:
twelfthrootoftwo: 2,440G (37.0%, 4,785G)
VolgraTheMoose: 1,199G (18.2%, 2,398G)
Nizaha: 584G (8.9%, 22,630G)
roqqqpsi: 514G (7.8%, 1,471G)
Xoomwaffle: 500G (7.6%, 10,172G)
Zagorsek: 463G (7.0%, 926G)
Mudrockk: 272G (4.1%, 272G)
AllInBot: 200G (3.0%, 200G)
datadrivenbot: 200G (3.0%, 41,433G)
ArlanKels: 126G (1.9%, 2,534G)
Musclestache: 100G (1.5%, 176G)

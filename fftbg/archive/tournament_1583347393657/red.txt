Player: !Red
Team: Red Team
Palettes: Red/Brown



DeathTaxesAndAnime
Female
Sagittarius
44
63
Chemist
Punch Art
Parry
Equip Axe
Waterwalking

Morning Star

Leather Hat
Power Sleeve
Sprint Shoes

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Phoenix Down
Pummel, Purification, Chakra, Revive, Seal Evil



Mellichor
Male
Pisces
44
73
Lancer
Sing
Meatbone Slash
Magic Attack UP
Swim

Obelisk
Platinum Shield
Barbuta
White Robe
N-Kai Armlet

Level Jump8, Vertical Jump8
Nameless Song, Sky Demon, Hydra Pit



Dirty Naise
Female
Taurus
54
62
Oracle
Dance
Meatbone Slash
Equip Armor
Retreat

Cypress Rod

Iron Helmet
Bronze Armor
Defense Armlet

Poison, Pray Faith, Zombie, Silence Song, Dispel Magic, Sleep, Dark Holy
Witch Hunt, Last Dance, Dragon Pit



JustDoomathon
Male
Aquarius
64
71
Geomancer
Black Magic
Mana Shield
Defend
Jump+2

Giant Axe
Crystal Shield
Leather Hat
Chameleon Robe
Small Mantle

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Fire 2, Bolt 3, Ice 4, Frog

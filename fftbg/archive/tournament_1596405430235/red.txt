Player: !Red
Team: Red Team
Palettes: Red/Brown



Laserman1000
Male
Gemini
49
46
Monk
Battle Skill
Parry
Long Status
Jump+1

Cute Bag

Flash Hat
Power Sleeve
Small Mantle

Spin Fist, Pummel, Revive
Head Break, Armor Break, Weapon Break, Power Break, Justice Sword, Surging Sword



AugnosMusic
Male
Pisces
65
55
Ninja
Elemental
Meatbone Slash
Martial Arts
Ignore Terrain



Black Hood
Power Sleeve
Angel Ring

Bomb, Knife, Spear
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Brokenknight201
Female
Pisces
45
42
Knight
Summon Magic
Counter Magic
Equip Polearm
Jump+2

Whale Whisker
Bronze Shield
Gold Helmet
Genji Armor
Sprint Shoes

Weapon Break, Speed Break, Mind Break, Justice Sword, Dark Sword
Moogle, Shiva, Ifrit, Carbunkle, Fairy



Dogsandcatsand
Male
Aquarius
55
51
Lancer
Basic Skill
Auto Potion
Martial Arts
Ignore Terrain

Dragon Whisker
Genji Shield
Circlet
Gold Armor
Dracula Mantle

Level Jump4, Vertical Jump8
Yell

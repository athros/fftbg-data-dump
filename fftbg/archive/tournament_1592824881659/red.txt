Player: !Red
Team: Red Team
Palettes: Red/Brown



ScurvyMitch
Female
Cancer
60
54
Ninja
Summon Magic
Meatbone Slash
Equip Axe
Move+1

Oak Staff
Scorpion Tail
Twist Headband
Clothes
Diamond Armlet

Shuriken, Bomb, Spear
Ramuh, Bahamut



Breakdown777
Male
Libra
37
45
Knight
Charge
PA Save
Defense UP
Waterwalking

Ice Brand
Ice Shield
Diamond Helmet
Mythril Armor
Battle Boots

Weapon Break, Speed Break, Power Break, Justice Sword
Charge+1, Charge+5



Prince Rogers Nelson
Male
Virgo
62
40
Geomancer
Punch Art
Auto Potion
Equip Gun
Fly

Papyrus Codex
Genji Shield
Holy Miter
Chameleon Robe
Germinas Boots

Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Pummel, Secret Fist, Purification



Ardith
Female
Pisces
72
66
Time Mage
Throw
Damage Split
Sicken
Move-MP Up

Wizard Staff

Headgear
Judo Outfit
Spike Shoes

Slow, Float, Reflect, Demi 2, Galaxy Stop
Shuriken, Bomb

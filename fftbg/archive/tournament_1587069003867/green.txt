Player: !Green
Team: Green Team
Palettes: Green/White



Volgrathemoose
Male
Pisces
52
65
Archer
White Magic
Parry
Doublehand
Waterwalking

Poison Bow

Green Beret
Leather Outfit
Bracer

Charge+2, Charge+3, Charge+5, Charge+7, Charge+10
Cure, Cure 2, Raise, Raise 2, Protect, Shell, Esuna, Holy



GingerDynomite
Male
Libra
72
70
Chemist
Charge
Abandon
Equip Sword
Waterwalking

Save the Queen

Triangle Hat
Judo Outfit
Genji Gauntlet

Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Charge+1, Charge+4, Charge+7



Galkife
Female
Aries
62
42
Time Mage
Talk Skill
Earplug
Dual Wield
Jump+1

Mace of Zeus
Rainbow Staff
Golden Hairpin
Brigandine
Jade Armlet

Haste 2, Slow, Stop, Demi, Demi 2, Stabilize Time
Invitation, Persuade, Negotiate, Refute, Rehabilitate



Pie108
Male
Libra
44
60
Monk
Charge
Faith Up
Sicken
Move+3



Triangle Hat
Clothes
Defense Armlet

Spin Fist, Pummel, Wave Fist, Purification
Charge+1, Charge+3, Charge+5, Charge+10

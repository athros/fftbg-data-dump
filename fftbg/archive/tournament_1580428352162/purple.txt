Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ominnous
Male
Aries
46
80
Archer
Draw Out
Caution
Doublehand
Move+3

Perseus Bow

Golden Hairpin
Black Costume
Reflect Ring

Charge+4, Charge+5, Charge+10
Muramasa, Kikuichimoji



Vivithegr8
Male
Aquarius
46
79
Time Mage
Basic Skill
PA Save
Equip Polearm
Jump+3

Cypress Rod

Red Hood
Clothes
Angel Ring

Slow 2, Immobilize, Reflect, Quick, Stabilize Time
Heal



Omegasuspekt
Male
Aries
44
52
Mediator
Elemental
Speed Save
Dual Wield
Retreat

Blaze Gun
Romanda Gun
Feather Hat
White Robe
Wizard Mantle

Persuade, Preach, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



EtherealFlux
Female
Cancer
62
57
Monk
Summon Magic
Catch
Equip Axe
Move-MP Up

Giant Axe

Feather Hat
Chain Vest
Feather Mantle

Pummel, Purification, Chakra, Revive
Moogle, Titan, Golem, Odin, Cyclops

Final Bets: red - 10 bets for 10,604G (73.7%, x0.36); blue - 6 bets for 3,783G (26.3%, x2.80)

red bets:
prince_rogers_nelson_: 5,000G (47.2%, 31,192G)
ko2q: 2,000G (18.9%, 13,359G)
Digitalsocrates: 1,000G (9.4%, 52,740G)
ShintaroNayaka: 1,000G (9.4%, 13,549G)
joewcarson: 500G (4.7%, 43,056G)
CrownOfHorns: 304G (2.9%, 304G)
electric_algus: 300G (2.8%, 16,621G)
SQUiDSQUARKLIN: 300G (2.8%, 8,225G)
AllInBot: 100G (0.9%, 100G)
fluffskull: 100G (0.9%, 768G)

blue bets:
DuraiPapers: 1,058G (28.0%, 1,058G)
BirbBrainsBot: 1,000G (26.4%, 40,794G)
ColetteMSLP: 825G (21.8%, 825G)
reinoe: 600G (15.9%, 600G)
CassiePhoenix: 200G (5.3%, 1,328G)
d4rr1n: 100G (2.6%, 7,934G)

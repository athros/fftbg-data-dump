Final Bets: purple - 10 bets for 16,646G (83.2%, x0.20); brown - 4 bets for 3,351G (16.8%, x4.97)

purple bets:
ShintaroNayaka: 7,506G (45.1%, 7,506G)
prince_rogers_nelson_: 5,000G (30.0%, 27,976G)
reinoe: 1,592G (9.6%, 1,592G)
Spuzzmocker: 669G (4.0%, 669G)
AllInBot: 594G (3.6%, 594G)
joewcarson: 500G (3.0%, 42,884G)
gorgewall: 401G (2.4%, 18,703G)
Arcblazer23: 180G (1.1%, 180G)
DuraiPapers: 104G (0.6%, 104G)
ko2q: 100G (0.6%, 14,303G)

brown bets:
CassiePhoenix: 1,062G (31.7%, 1,062G)
Digitalsocrates: 1,000G (29.8%, 55,401G)
BirbBrainsBot: 1,000G (29.8%, 40,448G)
Zachara: 289G (8.6%, 110,089G)

Final Bets: white - 9 bets for 2,457G (20.9%, x3.79); purple - 9 bets for 9,317G (79.1%, x0.26)

white bets:
CT_5_Holy: 540G (22.0%, 1,248G)
ShintaroNayaka: 500G (20.4%, 13,208G)
CassiePhoenix: 400G (16.3%, 400G)
Spuzzmocker: 252G (10.3%, 252G)
TeaTime29: 200G (8.1%, 12,611G)
BirbBrainsBot: 185G (7.5%, 38,448G)
soren_of_tyto: 160G (6.5%, 160G)
porkchop5158: 120G (4.9%, 120G)
d4rr1n: 100G (4.1%, 7,834G)

purple bets:
prince_rogers_nelson_: 5,000G (53.7%, 15,983G)
AllInBot: 1,172G (12.6%, 1,172G)
Digitalsocrates: 1,000G (10.7%, 55,042G)
ColetteMSLP: 624G (6.7%, 624G)
joewcarson: 500G (5.4%, 43,305G)
CrownOfHorns: 412G (4.4%, 412G)
gorgewall: 401G (4.3%, 19,041G)
DuraiPapers: 108G (1.2%, 108G)
ko2q: 100G (1.1%, 8,964G)

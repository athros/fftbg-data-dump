Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Pisces
66
80
Priest
Math Skill
Brave Save
Halve MP
Ignore Terrain

Sage Staff

Triangle Hat
Earth Clothes
Bracer

Cure, Cure 3, Raise, Regen, Protect, Protect 2, Shell 2, Esuna
CT, Height, Prime Number, 5, 3



Just Here2
Female
Sagittarius
69
58
Time Mage
Charge
MA Save
Magic Attack UP
Lava Walking

Rainbow Staff

Headgear
Earth Clothes
Magic Ring

Slow, Immobilize, Float, Quick
Charge+1, Charge+2, Charge+5



FriendSquirrel
Male
Cancer
52
65
Mediator
Time Magic
Distribute
Defend
Levitate

Romanda Gun

Twist Headband
Judo Outfit
Spike Shoes

Invitation, Persuade, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute
Slow 2, Stop, Float, Reflect, Quick, Meteor



GrayGhostGaming
Male
Capricorn
58
70
Thief
Item
MA Save
Beastmaster
Swim

Air Knife

Flash Hat
Mythril Vest
Bracer

Steal Shield
Potion, Ether, Remedy

Player: !White
Team: White Team
Palettes: White/Blue



Krasny1944
Male
Aries
64
61
Oracle
Item
MA Save
Throw Item
Jump+3

Iron Fan

Holy Miter
Judo Outfit
108 Gems

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Silence Song, Paralyze
Potion, Antidote, Eye Drop



Tougou
Male
Scorpio
44
47
Knight
Item
Abandon
Defend
Levitate

Diamond Sword
Crystal Shield
Iron Helmet
Genji Armor
Genji Gauntlet

Speed Break, Power Break
Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water



Aeonicvector
Male
Scorpio
56
63
Archer
Draw Out
Parry
Defense UP
Ignore Height

Long Bow

Triangle Hat
Brigandine
Magic Gauntlet

Charge+2, Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Asura, Kiyomori



SarrgeQc
Male
Cancer
50
79
Bard
Summon Magic
Catch
Equip Gun
Move+3

Romanda Gun

Leather Hat
Earth Clothes
Jade Armlet

Angel Song, Life Song, Battle Song, Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Moogle, Golem, Carbunkle, Silf

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Phi Sig
Female
Sagittarius
48
72
Priest
Draw Out
Parry
Secret Hunt
Fly

White Staff

Twist Headband
Chameleon Robe
Jade Armlet

Raise, Raise 2, Protect, Protect 2, Shell 2, Wall, Esuna
Asura



OneHundredFists
Male
Pisces
43
51
Calculator
Yin Yang Magic
Counter Flood
Equip Armor
Move+3

Thunder Rod

Ribbon
Gold Armor
Sprint Shoes

CT, Height, Prime Number, 5, 4
Blind, Zombie, Foxbird, Confusion Song



Lanshaft
Male
Aries
54
53
Mediator
Basic Skill
MA Save
Halve MP
Levitate

Stone Gun

Holy Miter
Black Robe
Bracer

Praise, Mimic Daravon
Dash, Throw Stone, Heal, Scream



ThePineappleSalesman
Male
Aquarius
56
67
Summoner
Throw
Counter Magic
Sicken
Jump+1

Wizard Rod

Headgear
Black Robe
Feather Boots

Moogle, Shiva, Ifrit, Golem, Bahamut, Leviathan, Fairy
Shuriken, Bomb, Knife, Dictionary

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Male
Serpentarius
52
62
Summoner
Draw Out
Auto Potion
Equip Shield
Ignore Height

Wizard Staff
Buckler
Holy Miter
Linen Robe
Elf Mantle

Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Silf, Cyclops
Bizen Boat, Murasame



Jampck
Male
Capricorn
80
48
Oracle
Elemental
Abandon
Sicken
Move+3

Gokuu Rod

Twist Headband
Wizard Outfit
Cursed Ring

Blind, Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



GrandmasterFrankerZ
Male
Gemini
78
62
Mediator
Basic Skill
Counter Magic
Equip Polearm
Move+3

Ryozan Silk

Golden Hairpin
Black Robe
Wizard Mantle

Praise, Preach, Solution, Death Sentence, Insult, Refute, Rehabilitate
Accumulate, Tickle, Yell, Fury



Solomongrundy85
Male
Aquarius
60
44
Ninja
Time Magic
Damage Split
Equip Gun
Retreat

Papyrus Codex
Papyrus Codex
Triangle Hat
Power Sleeve
Rubber Shoes

Shuriken, Bomb, Ninja Sword, Dictionary
Demi

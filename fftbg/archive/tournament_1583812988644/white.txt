Player: !White
Team: White Team
Palettes: White/Blue



Upvla
Male
Libra
70
48
Archer
Draw Out
Counter Tackle
Doublehand
Ignore Terrain

Night Killer

Platinum Helmet
Mystic Vest
Setiemson

Charge+5, Charge+10
Koutetsu, Bizen Boat



Zebobz
Monster
Sagittarius
49
66
Ghost










Monopool
Male
Cancer
67
74
Samurai
Throw
Meatbone Slash
Attack UP
Fly

Asura Knife

Cross Helmet
Platinum Armor
Germinas Boots

Koutetsu, Bizen Boat
Sword, Wand



Smegma Sorcerer
Male
Libra
52
51
Samurai
Summon Magic
Sunken State
Short Charge
Move+1

Kiyomori

Bronze Helmet
Bronze Armor
Elf Mantle

Asura, Koutetsu
Titan, Golem, Leviathan, Salamander, Silf, Cyclops

Player: !Green
Team: Green Team
Palettes: Green/White



NovaKnight21
Male
Sagittarius
77
51
Oracle
Item
Auto Potion
Equip Armor
Ignore Height

Iron Fan

Genji Helmet
Platinum Armor
Cursed Ring

Blind, Spell Absorb, Foxbird, Dispel Magic, Paralyze, Sleep
Potion, X-Potion, Phoenix Down



Vorap
Male
Libra
71
50
Samurai
Sing
Brave Up
Concentrate
Levitate

Heaven's Cloud

Platinum Helmet
Diamond Armor
Dracula Mantle

Asura, Bizen Boat, Kiyomori, Muramasa
Life Song, Cheer Song, Last Song



Leakimiko
Male
Leo
74
50
Geomancer
Draw Out
Abandon
Short Status
Move+3

Battle Axe
Diamond Shield
Triangle Hat
Silk Robe
Setiemson

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Muramasa



Lawnboxer
Male
Sagittarius
69
79
Summoner
Draw Out
Sunken State
Short Status
Lava Walking

Oak Staff

Headgear
Robe of Lords
Feather Mantle

Moogle, Shiva, Leviathan, Silf, Fairy, Lich
Asura, Bizen Boat, Kiyomori, Muramasa

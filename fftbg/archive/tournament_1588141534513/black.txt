Player: !Black
Team: Black Team
Palettes: Black/Red



Sogeki Dan
Male
Gemini
51
43
Lancer
Charge
Auto Potion
Short Charge
Swim

Partisan
Platinum Shield
Bronze Helmet
Diamond Armor
Defense Ring

Level Jump8, Vertical Jump2
Charge+1, Charge+2, Charge+5



Lijarkh
Male
Cancer
79
37
Monk
Elemental
PA Save
Dual Wield
Lava Walking



Holy Miter
Clothes
Defense Ring

Spin Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Sairentozon7
Monster
Pisces
54
40
Draugr










Moonliquor
Male
Capricorn
78
69
Geomancer
Talk Skill
Brave Up
Halve MP
Ignore Terrain

Iron Sword
Gold Shield
Green Beret
Wizard Robe
Angel Ring

Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Praise, Threaten, Negotiate, Mimic Daravon, Refute

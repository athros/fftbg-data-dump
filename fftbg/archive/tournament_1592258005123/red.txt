Player: !Red
Team: Red Team
Palettes: Red/Brown



Daveb
Female
Serpentarius
75
60
Ninja
Black Magic
Auto Potion
Equip Gun
Teleport

Papyrus Codex
Bestiary
Black Hood
Mystic Vest
Defense Armlet

Hammer
Fire 2, Bolt 3, Flare



Kyune
Male
Leo
41
76
Geomancer
Basic Skill
Distribute
Equip Bow
Ignore Height

Mythril Bow

Golden Hairpin
White Robe
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Heal, Cheer Up, Wish



Fenaen
Male
Aries
74
65
Ninja
Black Magic
Counter Magic
Equip Gun
Move-HP Up

Bestiary
Bloody Strings
Thief Hat
Black Costume
Angel Ring

Shuriken, Knife, Stick
Fire, Bolt 4, Ice, Ice 2, Ice 3



Vorap
Female
Leo
62
48
Knight
Charge
Hamedo
Equip Sword
Move-HP Up

Muramasa
Gold Shield
Iron Helmet
Linen Cuirass
108 Gems

Shield Break, Magic Break, Speed Break, Power Break
Charge+1, Charge+2, Charge+7

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ArchKnightX
Male
Leo
71
63
Wizard
Jump
Caution
Magic Defense UP
Jump+1

Rod

Thief Hat
Earth Clothes
Rubber Shoes

Fire, Bolt, Bolt 3, Bolt 4, Ice, Empower, Flare
Level Jump8, Vertical Jump8



Meta Five
Female
Leo
43
60
Mediator
Steal
Abandon
Dual Wield
Retreat

Battle Folio
Papyrus Codex
Green Beret
Adaman Vest
Bracer

Persuade, Preach, Negotiate, Refute
Gil Taking, Steal Helmet, Arm Aim



Nickyfive
Female
Scorpio
52
67
Samurai
Battle Skill
Earplug
Doublehand
Waterbreathing

Asura Knife

Circlet
Plate Mail
Angel Ring

Asura, Koutetsu, Kiyomori, Chirijiraden
Weapon Break, Mind Break



ColetteMSLP
Male
Cancer
80
60
Wizard
Item
Parry
Short Charge
Move+2

Ice Rod

Leather Hat
Clothes
Dracula Mantle

Fire, Bolt 2, Ice, Empower
Potion, Hi-Potion, X-Potion, Soft, Holy Water, Phoenix Down

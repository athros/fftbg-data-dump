Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Tougou
Male
Scorpio
47
77
Lancer
Draw Out
Catch
Sicken
Teleport

Partisan
Venetian Shield
Diamond Helmet
Mythril Armor
Magic Ring

Level Jump8, Vertical Jump8
Koutetsu, Heaven's Cloud



Fenaen
Male
Scorpio
79
60
Thief
Black Magic
Absorb Used MP
Dual Wield
Move+1

Long Sword
Broad Sword
Red Hood
Mystic Vest
Battle Boots

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Fire 2, Bolt 3, Ice, Flare



Killth3kid
Male
Aries
62
77
Calculator
Wildfire Skill
Counter Flood
Equip Armor
Move+1

Bestiary
Genji Shield
Diamond Helmet
Crystal Mail
Dracula Mantle

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit



ZombiFrancis
Female
Pisces
51
45
Calculator
White Magic
Mana Shield
Dual Wield
Move+3

Papyrus Codex
Battle Folio
Thief Hat
Chameleon Robe
Sprint Shoes

CT, Prime Number, 5, 4, 3
Cure 2, Cure 3, Raise, Raise 2, Reraise, Protect 2, Shell 2, Wall, Holy

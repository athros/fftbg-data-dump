Player: !White
Team: White Team
Palettes: White/Blue



Randgridr
Male
Aries
69
54
Summoner
Black Magic
MA Save
Equip Shield
Jump+1

Poison Rod
Genji Shield
Leather Hat
Chameleon Robe
Elf Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Odin, Fairy
Fire 3, Fire 4, Ice 2



Moonliquor
Monster
Aquarius
76
42
Tiamat










NIghtdew14
Female
Sagittarius
57
70
Dancer
Basic Skill
Earplug
Doublehand
Swim

Cashmere

Flash Hat
White Robe
Battle Boots

Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Dragon Pit
Accumulate, Dash, Throw Stone, Heal



AsceticGamer
Female
Cancer
70
74
Samurai
Item
Counter Tackle
Throw Item
Waterwalking

Bizen Boat

Leather Helmet
Linen Robe
Bracer

Asura, Koutetsu
Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down

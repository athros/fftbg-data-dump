Player: !Black
Team: Black Team
Palettes: Black/Red



The Pengwin
Female
Taurus
62
47
Archer
Draw Out
Counter Tackle
Doublehand
Move+1

Romanda Gun

Leather Hat
Wizard Outfit
Leather Mantle

Charge+1, Charge+5, Charge+10, Charge+20
Koutetsu, Kiyomori, Kikuichimoji



SeedSC
Male
Aquarius
44
73
Monk
Black Magic
Mana Shield
Dual Wield
Ignore Terrain



Feather Hat
Clothes
Diamond Armlet

Wave Fist, Purification, Seal Evil
Fire 2, Bolt 4, Frog



Rassyu
Male
Libra
64
61
Oracle
Jump
HP Restore
Equip Sword
Jump+3

Coral Sword

Holy Miter
Black Costume
Wizard Mantle

Poison, Zombie, Silence Song, Foxbird, Confusion Song, Paralyze
Level Jump5, Vertical Jump5



K1ngofthechill
Male
Libra
62
42
Archer
Black Magic
Regenerator
Equip Bow
Move+1

Ultimus Bow

Gold Helmet
Clothes
Power Wrist

Charge+2, Charge+4, Charge+5, Charge+7, Charge+10
Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 2, Ice 4, Empower, Death

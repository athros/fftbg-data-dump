Player: !Red
Team: Red Team
Palettes: Red/Brown



DLJuggernaut
Male
Cancer
47
60
Time Mage
Yin Yang Magic
Counter
Beastmaster
Move-MP Up

White Staff

Red Hood
Light Robe
Reflect Ring

Float, Reflect, Demi, Demi 2, Stabilize Time
Poison, Silence Song, Foxbird, Dark Holy



Gorgewall
Male
Virgo
53
60
Chemist
Elemental
Catch
Sicken
Jump+2

Stone Gun

Golden Hairpin
Power Sleeve
Leather Mantle

Potion, Hi-Potion, Ether, Antidote, Holy Water, Phoenix Down
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Volgrathemoose
Male
Aquarius
71
49
Archer
Steal
Arrow Guard
Doublehand
Move-MP Up

Bow Gun

Cachusha
Brigandine
Power Wrist

Charge+2, Charge+7, Charge+10
Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim



Digitalsocrates
Monster
Leo
46
60
Pisco Demon








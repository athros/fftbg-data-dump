Player: !Green
Team: Green Team
Palettes: Green/White



Gorgewall
Male
Gemini
41
60
Knight
Punch Art
Counter Tackle
Short Status
Move-HP Up

Ice Brand
Aegis Shield
Circlet
Diamond Armor
Germinas Boots

Armor Break, Shield Break, Power Break
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive



Lawnboxer
Female
Aries
46
68
Knight
Time Magic
Counter Flood
Long Status
Swim

Platinum Sword
Gold Shield
Diamond Helmet
Diamond Armor
Reflect Ring

Head Break, Shield Break, Weapon Break, Speed Break, Dark Sword, Night Sword
Haste, Haste 2, Quick, Demi 2



Zmoses
Male
Virgo
74
43
Knight
Basic Skill
Counter
Halve MP
Waterwalking

Save the Queen
Platinum Shield
Crystal Helmet
Mythril Armor
Setiemson

Weapon Break, Power Break, Justice Sword
Heal, Tickle, Cheer Up



0v3rr8d
Female
Gemini
46
61
Time Mage
Dance
Regenerator
Short Charge
Ignore Terrain

Octagon Rod

Leather Hat
Wizard Robe
Dracula Mantle

Haste, Haste 2, Slow, Float, Demi, Stabilize Time
Obsidian Blade

Player: !Red
Team: Red Team
Palettes: Red/Brown



FreedomNM
Male
Capricorn
55
46
Mime

Catch
Defend
Retreat



Black Hood
Mystic Vest
Magic Gauntlet

Mimic




SeedSC
Male
Gemini
69
49
Ninja
Basic Skill
Counter
Equip Shield
Teleport 2

Assassin Dagger
Escutcheon
Feather Hat
Mystic Vest
Elf Mantle

Shuriken, Wand
Dash, Throw Stone, Heal, Cheer Up



Zeroroute
Monster
Libra
80
78
Red Chocobo










Noosanaut
Female
Aries
56
55
Monk
Throw
Dragon Spirit
Maintenance
Move-HP Up



Holy Miter
Leather Outfit
Cherche

Spin Fist, Pummel, Earth Slash, Purification, Revive
Knife

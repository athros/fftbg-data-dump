Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ququroon
Female
Aries
73
73
Summoner
Elemental
Meatbone Slash
Defend
Fly

Flame Rod

Ribbon
Chain Vest
Power Wrist

Moogle, Carbunkle, Odin, Salamander
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Ominnous
Female
Libra
48
64
Mediator
Black Magic
Counter
Dual Wield
Jump+1

Bestiary
Battle Folio
Leather Hat
Linen Robe
Dracula Mantle

Preach, Refute, Rehabilitate
Fire 3, Ice, Ice 2



Hp199297
Monster
Leo
60
70
Serpentarius










Djorama
Female
Pisces
38
77
Priest
Time Magic
Hamedo
Equip Bow
Jump+2

Poison Bow

Headgear
Brigandine
Bracer

Cure, Cure 4, Raise, Raise 2, Shell 2, Esuna
Haste, Immobilize, Float, Reflect, Stabilize Time, Meteor

Player: !Red
Team: Red Team
Palettes: Red/Brown



L2 Sentinel
Male
Sagittarius
60
42
Bard
Talk Skill
Caution
Dual Wield
Waterbreathing

Long Bow

Flash Hat
Platinum Armor
Reflect Ring

Life Song, Last Song
Invitation, Praise, Preach, Solution, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate



Hzor
Male
Gemini
47
52
Wizard
Steal
Counter
Magic Attack UP
Fly

Assassin Dagger

Leather Hat
White Robe
Leather Mantle

Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 2, Ice 3
Steal Heart



Upvla
Female
Aries
62
71
Ninja
Talk Skill
Dragon Spirit
Equip Gun
Fly

Ninja Edge
Blind Knife
Black Hood
Adaman Vest
Feather Boots

Bomb, Knife, Ninja Sword
Threaten



Landman
Female
Gemini
52
40
Archer
Steal
Dragon Spirit
Long Status
Jump+3

Lightning Bow

Holy Miter
Power Sleeve
Genji Gauntlet

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Gil Taking, Steal Heart, Steal Helmet, Steal Armor

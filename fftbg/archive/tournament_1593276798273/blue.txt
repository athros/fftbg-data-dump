Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DeathTaxesAndAnime
Female
Sagittarius
54
56
Lancer
Punch Art
Sunken State
Beastmaster
Move-MP Up

Obelisk
Buckler
Barbuta
Light Robe
Dracula Mantle

Level Jump8, Vertical Jump6
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Seal Evil



Omegasuspekt
Male
Gemini
50
80
Mime

Damage Split
Equip Armor
Waterwalking



Leather Helmet
White Robe
Diamond Armlet

Mimic




Kyver
Male
Libra
67
63
Geomancer
Time Magic
Counter Tackle
Short Status
Jump+1

Battle Axe
Genji Shield
Golden Hairpin
Wizard Robe
108 Gems

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind
Slow 2, Float, Quick, Demi, Stabilize Time



Cam ATS
Male
Libra
61
72
Archer
Sing
Catch
Doublehand
Move-HP Up

Mythril Bow

Crystal Helmet
Chain Vest
Small Mantle

Charge+1, Charge+2, Charge+5
Life Song, Sky Demon, Hydra Pit

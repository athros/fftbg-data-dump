Final Bets: red - 13 bets for 7,341G (53.1%, x0.88); blue - 11 bets for 6,489G (46.9%, x1.13)

red bets:
rabbitlogik: 2,512G (34.2%, 2,512G)
fenaen: 1,600G (21.8%, 30,509G)
Baron_von_Scrub: 512G (7.0%, 512G)
killth3kid: 500G (6.8%, 8,746G)
GladiatorLupe: 500G (6.8%, 9,562G)
Aldrammech: 372G (5.1%, 372G)
cactus_tony_: 326G (4.4%, 326G)
Forkmore: 311G (4.2%, 311G)
JLinkletter: 200G (2.7%, 1,791G)
RampagingRobot: 172G (2.3%, 172G)
neocarbuncle: 136G (1.9%, 136G)
lastly: 100G (1.4%, 27,218G)
datadrivenbot: 100G (1.4%, 46,486G)

blue bets:
DuraiPapers: 1,500G (23.1%, 10,364G)
getthemoneyz: 1,000G (15.4%, 1,077,133G)
BirbBrainsBot: 1,000G (15.4%, 62,683G)
Laserman1000: 700G (10.8%, 7,200G)
cam_ATS: 555G (8.6%, 18,172G)
GrayGhostGaming: 552G (8.5%, 552G)
sparker9: 400G (6.2%, 8,133G)
nhammen: 280G (4.3%, 280G)
Rilgon: 202G (3.1%, 405G)
Grandlanzer: 200G (3.1%, 32,139G)
nifboy: 100G (1.5%, 3,848G)

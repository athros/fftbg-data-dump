Player: !Red
Team: Red Team
Palettes: Red/Brown



StealthModeLocke
Female
Sagittarius
55
40
Priest
Elemental
Catch
Magic Defense UP
Jump+2

White Staff

Twist Headband
Linen Robe
Wizard Mantle

Cure 2, Cure 3, Cure 4, Raise 2, Regen, Shell, Wall, Esuna
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Jeeboheebo
Female
Leo
44
62
Mime

PA Save
Doublehand
Teleport 2



Gold Helmet
Adaman Vest
Feather Mantle

Mimic




Run With Stone GUNs
Female
Taurus
64
69
Wizard
Draw Out
Abandon
Doublehand
Move+2

Poison Rod

Green Beret
Light Robe
Reflect Ring

Fire, Fire 2, Fire 3, Ice 2, Ice 3, Empower
Bizen Boat



NIghtdew14
Female
Pisces
54
63
Geomancer
Punch Art
Counter Magic
Martial Arts
Move+2

Giant Axe
Platinum Shield
Triangle Hat
Secret Clothes
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil

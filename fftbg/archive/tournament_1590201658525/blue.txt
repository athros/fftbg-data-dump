Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Meta Five
Monster
Virgo
58
61
Serpentarius










Pandasforsale
Male
Leo
79
63
Lancer
Battle Skill
PA Save
Short Status
Ignore Height

Gungnir
Round Shield
Circlet
Maximillian
Sprint Shoes

Level Jump2, Vertical Jump7
Shield Break, Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword



Lythe Caraker
Female
Libra
46
48
Mediator
Punch Art
Faith Save
Short Charge
Fly

Papyrus Codex

Flash Hat
White Robe
Leather Mantle

Praise, Threaten, Preach, Negotiate
Spin Fist, Purification, Revive



Tougou
Male
Taurus
42
73
Lancer
Charge
Counter Flood
Equip Gun
Swim

Glacier Gun
Buckler
Leather Helmet
Mythril Armor
108 Gems

Level Jump3, Vertical Jump6
Charge+1, Charge+5

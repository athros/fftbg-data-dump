Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Nekojin
Male
Taurus
58
50
Bard
Item
Earplug
Sicken
Levitate

Ramia Harp

Headgear
Black Costume
Power Wrist

Angel Song, Battle Song, Diamond Blade, Space Storage
Potion, Hi-Potion, Hi-Ether, Phoenix Down



TheMurkGnome
Male
Sagittarius
66
74
Calculator
Time Magic
Catch
Beastmaster
Retreat

Rod

Holy Miter
Black Robe
Magic Ring

CT, Height, 4, 3
Haste, Haste 2, Slow, Immobilize, Float, Stabilize Time, Meteor



LAGBOT30000
Male
Gemini
58
72
Lancer
Punch Art
PA Save
Equip Axe
Teleport

Slasher
Buckler
Cross Helmet
Leather Armor
Elf Mantle

Level Jump8, Vertical Jump7
Wave Fist, Purification, Revive, Seal Evil



Firesheath
Male
Leo
43
74
Priest
Yin Yang Magic
Catch
Equip Polearm
Ignore Height

Iron Fan

Black Hood
Linen Robe
Magic Gauntlet

Cure, Raise, Regen, Shell 2, Esuna
Blind, Zombie, Silence Song, Blind Rage, Foxbird

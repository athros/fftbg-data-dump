Final Bets: red - 10 bets for 6,173G (56.2%, x0.78); green - 9 bets for 4,802G (43.8%, x1.29)

red bets:
DeathTaxesAndAnime: 1,865G (30.2%, 3,657G)
ko2q: 1,598G (25.9%, 1,598G)
BirbBrainsBot: 1,000G (16.2%, 62,411G)
PantherIscariot: 444G (7.2%, 4,312G)
Rook512: 300G (4.9%, 1,657G)
Aeriyah: 208G (3.4%, 208G)
LordTomS: 200G (3.2%, 1,089G)
datadrivenbot: 200G (3.2%, 50,665G)
twelfthrootoftwo: 200G (3.2%, 564G)
getthemoneyz: 158G (2.6%, 1,513,258G)

green bets:
superdevon1: 1,838G (38.3%, 45,974G)
Jaritras: 681G (14.2%, 681G)
AllInBot: 558G (11.6%, 558G)
NovaKnight21: 548G (11.4%, 548G)
blorpy_: 457G (9.5%, 5,721G)
beastattic: 300G (6.2%, 1,260G)
Drusiform: 220G (4.6%, 220G)
Firesheath: 100G (2.1%, 26,877G)
douchetron: 100G (2.1%, 20,874G)

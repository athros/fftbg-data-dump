Player: !Red
Team: Red Team
Palettes: Red/Brown



Mistblade
Monster
Aquarius
42
50
Sekhret










Deathmaker06
Female
Aquarius
75
56
Chemist
Charge
Critical Quick
Maintenance
Move+3

Cute Bag

Headgear
Clothes
Diamond Armlet

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down
Charge+1, Charge+2, Charge+3, Charge+4, Charge+7



KasugaiRoastedPeas
Female
Aries
76
61
Wizard
Talk Skill
Absorb Used MP
Secret Hunt
Move-HP Up

Dragon Rod

Leather Hat
Black Robe
Power Wrist

Fire 4, Ice, Ice 2, Ice 4, Empower, Death
Invitation, Praise, Threaten, Solution, Mimic Daravon, Refute



Typicalfanboy
Female
Capricorn
79
65
Lancer
White Magic
Auto Potion
Dual Wield
Swim

Whale Whisker
Spear
Diamond Helmet
Chameleon Robe
Elf Mantle

Level Jump8, Vertical Jump4
Cure, Cure 3, Raise 2, Protect 2, Wall, Esuna, Holy

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Douchetron
Female
Serpentarius
38
79
Dancer
Draw Out
Dragon Spirit
Doublehand
Move+1

Ryozan Silk

Holy Miter
Leather Outfit
Dracula Mantle

Last Dance, Obsidian Blade, Nether Demon
Asura, Koutetsu, Kikuichimoji



DarrenDinosaurs
Male
Pisces
48
48
Priest
Jump
Distribute
Dual Wield
Waterbreathing

Healing Staff
Mace of Zeus
Green Beret
Black Robe
Power Wrist

Cure 2, Raise, Raise 2, Protect 2, Shell, Esuna
Level Jump8, Vertical Jump3



E Ballard
Male
Aquarius
46
48
Archer
Item
Earplug
Martial Arts
Lava Walking

Hunting Bow
Ice Shield
Ribbon
Mythril Vest
Genji Gauntlet

Charge+4, Charge+20
Potion, X-Potion, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Phi Sig
Female
Sagittarius
69
46
Wizard
Basic Skill
Hamedo
Short Charge
Teleport

Ice Rod

Twist Headband
Black Robe
Cursed Ring

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Ice, Frog
Accumulate, Dash, Throw Stone, Heal, Yell, Cheer Up

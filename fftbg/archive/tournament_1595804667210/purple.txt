Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nhammen
Male
Taurus
46
41
Knight
Basic Skill
Counter Magic
Short Charge
Move+3

Blood Sword
Mythril Shield
Bronze Helmet
Carabini Mail
Power Wrist

Shield Break, Weapon Break, Mind Break, Justice Sword
Heal, Wish



Nekojin
Male
Gemini
69
54
Knight
Charge
Parry
Dual Wield
Move+3

Rune Blade
Iron Sword
Cross Helmet
Diamond Armor
Sprint Shoes

Head Break, Shield Break, Weapon Break, Mind Break, Justice Sword, Dark Sword
Charge+1, Charge+2



Gorgewall
Female
Aries
41
52
Dancer
Black Magic
Hamedo
Doublehand
Teleport

Cultist Dagger

Green Beret
Chameleon Robe
Spike Shoes

Wiznaibus, Polka Polka, Obsidian Blade, Nether Demon
Fire 3, Bolt 2, Ice 2, Ice 3



Benticore
Male
Libra
67
70
Ninja
Yin Yang Magic
Counter
Equip Gun
Ignore Height

Papyrus Codex
Battle Folio
Black Hood
Rubber Costume
Bracer

Shuriken, Staff
Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy

Player: !Red
Team: Red Team
Palettes: Red/Brown



TheMM42
Male
Leo
44
52
Mediator
Punch Art
MA Save
Beastmaster
Move+1

Bestiary

Green Beret
Black Costume
N-Kai Armlet

Threaten, Death Sentence, Negotiate, Refute
Revive, Seal Evil



Maakur
Female
Sagittarius
61
76
Summoner
Black Magic
Dragon Spirit
Short Charge
Move+3

Ice Rod

Headgear
Linen Robe
Dracula Mantle

Shiva, Ifrit, Carbunkle, Salamander, Silf
Fire, Fire 3, Ice 2



Flowinprose
Male
Gemini
64
59
Knight
Jump
MP Restore
Attack UP
Waterbreathing

Save the Queen
Escutcheon
Crystal Helmet
Plate Mail
Wizard Mantle

Armor Break, Shield Break, Weapon Break, Speed Break, Justice Sword, Surging Sword
Level Jump8, Vertical Jump6



Evewho
Female
Leo
49
54
Knight
Jump
Counter Magic
Equip Armor
Levitate

Ice Brand
Flame Shield
Triangle Hat
Light Robe
Feather Mantle

Armor Break, Shield Break, Power Break
Level Jump2, Vertical Jump2

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Dothan89
Female
Virgo
65
58
Geomancer
Steal
Abandon
Halve MP
Swim

Slasher
Bronze Shield
Cachusha
Clothes
108 Gems

Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Accessory, Steal Status, Arm Aim



AniZero
Male
Cancer
68
59
Knight
Summon Magic
MA Save
Beastmaster
Teleport

Rune Blade
Round Shield
Diamond Helmet
Plate Mail
Defense Armlet

Head Break, Speed Break, Mind Break
Shiva, Carbunkle, Fairy



Lawnboxer
Male
Aries
72
73
Time Mage
Jump
Faith Up
Magic Defense UP
Move+1

Rainbow Staff

Golden Hairpin
Leather Outfit
Reflect Ring

Haste 2, Slow, Slow 2, Stop, Reflect, Quick, Demi, Demi 2, Stabilize Time
Level Jump2, Vertical Jump8



Sinnyil2
Male
Capricorn
69
64
Lancer
Throw
Counter
Defend
Teleport 2

Javelin
Genji Shield
Circlet
Leather Armor
Small Mantle

Level Jump4, Vertical Jump7
Sword, Wand

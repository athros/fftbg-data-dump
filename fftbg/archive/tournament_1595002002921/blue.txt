Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Evontno
Female
Capricorn
57
43
Wizard
Punch Art
Abandon
Halve MP
Move-MP Up

Dragon Rod

Red Hood
Wizard Robe
Sprint Shoes

Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3
Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive



Nhammen
Male
Aquarius
62
58
Bard
Throw
PA Save
Doublehand
Fly

Bloody Strings

Leather Hat
Genji Armor
Jade Armlet

Life Song, Cheer Song, Battle Song, Nameless Song, Sky Demon
Bomb



Laserman1000
Male
Gemini
62
73
Ninja
White Magic
MA Save
Attack UP
Jump+3

Kunai
Flail
Golden Hairpin
Leather Outfit
Angel Ring

Bomb
Raise, Protect, Shell, Wall, Esuna, Holy



Xoomwaffle
Female
Virgo
50
72
Archer
Dance
Earplug
Equip Polearm
Move+3

Javelin
Bronze Shield
Platinum Helmet
Judo Outfit
Defense Armlet

Charge+1, Charge+3, Charge+4, Charge+5
Disillusion, Nether Demon

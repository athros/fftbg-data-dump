Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaateXIII
Female
Gemini
58
50
Geomancer
Steal
Meatbone Slash
Magic Defense UP
Waterwalking

Platinum Sword
Flame Shield
Golden Hairpin
Silk Robe
Feather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Steal Heart, Arm Aim, Leg Aim



SephDarkheart
Male
Virgo
66
54
Mime

HP Restore
Maintenance
Ignore Terrain



Headgear
Brigandine
Rubber Shoes

Mimic




TasisSai
Male
Aries
75
67
Lancer
White Magic
Speed Save
Doublehand
Move+3

Battle Bamboo

Diamond Helmet
Leather Armor
Chantage

Level Jump2, Vertical Jump8
Cure 3, Cure 4, Raise, Raise 2, Protect, Esuna



Bongomon7
Male
Pisces
61
43
Archer
Yin Yang Magic
Auto Potion
Doublehand
Jump+1

Bow Gun

Golden Hairpin
Black Costume
Defense Ring

Charge+2, Charge+5, Charge+20
Life Drain, Doubt Faith, Dispel Magic

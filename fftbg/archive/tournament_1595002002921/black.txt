Player: !Black
Team: Black Team
Palettes: Black/Red



RagequitSA
Male
Taurus
48
72
Lancer
Item
Faith Save
Equip Polearm
Waterbreathing

Cypress Rod
Ice Shield
Barbuta
Robe of Lords
N-Kai Armlet

Level Jump4, Vertical Jump3
Potion, Ether, Antidote, Echo Grass, Soft, Remedy, Phoenix Down



Fenixcrest
Male
Gemini
62
57
Ninja
Draw Out
Caution
Equip Gun
Teleport

Stone Gun
Glacier Gun
Leather Hat
Mystic Vest
Defense Ring

Bomb, Staff, Dictionary
Koutetsu, Heaven's Cloud



ZCKaiser
Monster
Leo
73
46
Holy Dragon










Miku Shikhu
Female
Capricorn
37
50
Mediator
Elemental
Abandon
Equip Knife
Move+2

Short Edge

Red Hood
Wizard Outfit
Elf Mantle

Praise, Threaten, Insult, Negotiate, Refute
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

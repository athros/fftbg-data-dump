Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheChainNerd
Male
Gemini
68
62
Knight
Punch Art
Abandon
Equip Bow
Jump+2

Poison Bow
Buckler
Mythril Helmet
Silk Robe
Reflect Ring

Armor Break, Weapon Break, Magic Break, Surging Sword
Earth Slash, Secret Fist, Purification, Chakra



Tapemeasure
Female
Aquarius
76
77
Samurai
Elemental
Arrow Guard
Maintenance
Jump+2

Koutetsu Knife

Barbuta
Leather Armor
Jade Armlet

Koutetsu, Heaven's Cloud, Kiyomori
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



Actual JP
Female
Leo
51
59
Chemist
Time Magic
Counter Flood
Halve MP
Swim

Hydra Bag

Green Beret
Brigandine
Setiemson

Potion, Hi-Ether, Antidote, Holy Water, Phoenix Down
Haste, Immobilize, Demi 2



Kronikle
Male
Sagittarius
48
70
Ninja
Black Magic
Critical Quick
Equip Sword
Teleport 2

Defender
Ragnarok
Feather Hat
Power Sleeve
Sprint Shoes

Shuriken, Bomb, Dictionary
Fire 2, Fire 4, Bolt, Frog

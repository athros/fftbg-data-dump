Final Bets: green - 28 bets for 13,368G (57.2%, x0.75); yellow - 16 bets for 10,008G (42.8%, x1.34)

green bets:
DeathTaxesAndAnime: 1,779G (13.3%, 1,779G)
Nizaha: 1,373G (10.3%, 2,747G)
upvla: 1,219G (9.1%, 11,040G)
ShintaroNayaka: 800G (6.0%, 7,272G)
mpghappiness: 720G (5.4%, 720G)
RubenFlonne: 703G (5.3%, 703G)
Lanshaft: 600G (4.5%, 600G)
RunicMagus: 584G (4.4%, 584G)
sinnyil2: 512G (3.8%, 512G)
Pie108: 500G (3.7%, 12,323G)
alrightbye: 500G (3.7%, 6,356G)
galkife: 500G (3.7%, 91,687G)
CorpusCav: 500G (3.7%, 11,206G)
kingchadking: 476G (3.6%, 476G)
KetabrakBemos: 400G (3.0%, 1,831G)
mivvim: 354G (2.6%, 354G)
Firesheath: 328G (2.5%, 328G)
HaateXIII: 316G (2.4%, 316G)
AllInBot: 269G (2.0%, 269G)
rico_flex: 200G (1.5%, 2,368G)
DrAntiSocial: 130G (1.0%, 3,253G)
friendsquirrel: 100G (0.7%, 6,815G)
ApplesauceBoss: 100G (0.7%, 3,462G)
RughSontos: 100G (0.7%, 1,731G)
maakur_: 100G (0.7%, 4,618G)
wollise89: 100G (0.7%, 1,187G)
datadrivenbot: 100G (0.7%, 24,108G)
ko2q: 5G (0.0%, 8,145G)

yellow bets:
Shakarak: 2,000G (20.0%, 30,582G)
Lodrak: 1,926G (19.2%, 1,926G)
BirbBrainsBot: 1,000G (10.0%, 14,919G)
SeedSC: 1,000G (10.0%, 252,929G)
Slowbrofist: 925G (9.2%, 925G)
Faplo: 702G (7.0%, 702G)
getthemoneyz: 578G (5.8%, 452,306G)
mexskacin: 428G (4.3%, 428G)
ColetteMSLP: 400G (4.0%, 12,656G)
Bulleta: 300G (3.0%, 5,506G)
moonliquor: 228G (2.3%, 228G)
ZephyrTempest: 215G (2.1%, 42,112G)
NUMBER1DB: 112G (1.1%, 112G)
loveyouallfriends: 100G (1.0%, 21,340G)
Sup_Johnny: 50G (0.5%, 635G)
roqqqpsi: 44G (0.4%, 3,952G)

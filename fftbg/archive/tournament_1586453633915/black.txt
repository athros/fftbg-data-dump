Player: !Black
Team: Black Team
Palettes: Black/Red



Fenixcrest
Female
Gemini
46
51
Priest
Battle Skill
Abandon
Equip Armor
Retreat

White Staff

Barbuta
Clothes
Setiemson

Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect, Protect 2, Esuna, Holy
Head Break, Power Break, Surging Sword



Maldoree
Female
Aries
45
56
Summoner
Item
Sunken State
Halve MP
Swim

Rod

Thief Hat
Black Robe
Red Shoes

Moogle, Shiva, Carbunkle, Bahamut, Odin, Salamander, Fairy
Hi-Potion, Ether, Phoenix Down



Moonliquor
Female
Capricorn
47
69
Calculator
Time Magic
HP Restore
Equip Armor
Teleport

Papyrus Codex

Leather Helmet
Brigandine
Red Shoes

CT, Height, 4, 3
Haste, Haste 2, Quick, Stabilize Time



DLJuggernaut
Female
Aquarius
80
70
Chemist
Throw
Mana Shield
Maintenance
Jump+2

Hydra Bag

Flash Hat
Clothes
Genji Gauntlet

Potion, Ether, Soft, Phoenix Down
Shuriken, Knife, Wand

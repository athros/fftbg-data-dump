Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mrfripps
Male
Libra
76
52
Priest
Battle Skill
Mana Shield
Equip Shield
Jump+1

Morning Star
Escutcheon
Feather Hat
White Robe
Red Shoes

Cure, Cure 3, Reraise, Regen, Shell, Shell 2, Wall, Esuna
Head Break



Amiture
Male
Leo
70
65
Knight
Charge
Parry
Equip Knife
Ignore Height

Cultist Dagger
Buckler
Gold Helmet
Bronze Armor
Angel Ring

Power Break, Justice Sword, Night Sword, Explosion Sword
Charge+3, Charge+7



DesertWooder
Female
Sagittarius
79
68
Thief
Summon Magic
Dragon Spirit
Short Status
Waterbreathing

Mage Masher

Red Hood
Black Costume
Battle Boots

Arm Aim
Moogle, Shiva, Ifrit, Lich



StealthModeLocke
Female
Virgo
56
73
Wizard
Talk Skill
Parry
Monster Talk
Ignore Terrain

Poison Rod

Twist Headband
Chameleon Robe
Feather Mantle

Fire, Bolt 2, Ice 3, Ice 4
Threaten, Solution, Refute

Final Bets: yellow - 7 bets for 5,197G (42.4%, x1.36); brown - 8 bets for 7,051G (57.6%, x0.74)

yellow bets:
nhammen: 1,949G (37.5%, 3,891G)
killth3kid: 1,000G (19.2%, 69,681G)
BirbBrainsBot: 1,000G (19.2%, 65,873G)
DustBirdEX: 472G (9.1%, 472G)
Forkmore: 296G (5.7%, 3,651G)
randgridr: 272G (5.2%, 272G)
getthemoneyz: 208G (4.0%, 1,264,802G)

brown bets:
midnight_blues3: 2,000G (28.4%, 20,221G)
Evewho: 1,663G (23.6%, 3,262G)
VolgraTheMoose: 1,287G (18.3%, 1,287G)
upvla: 678G (9.6%, 1,856G)
ACSpree: 623G (8.8%, 623G)
AlenaZarek: 500G (7.1%, 39,023G)
datadrivenbot: 200G (2.8%, 62,723G)
CosmicTactician: 100G (1.4%, 43,608G)

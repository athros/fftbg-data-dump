Player: !White
Team: White Team
Palettes: White/Blue



JonnRPG
Male
Scorpio
62
62
Knight
Jump
Speed Save
Defend
Ignore Height

Defender

Genji Helmet
Chain Mail
Genji Gauntlet

Armor Break, Weapon Break, Speed Break, Stasis Sword, Dark Sword
Level Jump4, Vertical Jump2



Maakur
Female
Cancer
70
61
Thief
Dance
Regenerator
Long Status
Jump+2

Spell Edge

Barbuta
Judo Outfit
N-Kai Armlet

Steal Helmet, Steal Status
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nether Demon



DaveStrider55
Male
Aries
57
39
Knight
Summon Magic
Mana Shield
Magic Attack UP
Move+3

Battle Axe
Flame Shield
Bronze Helmet
Light Robe
Vanish Mantle

Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Moogle, Ramuh, Carbunkle, Bahamut, Lich



Deciimos
Male
Gemini
71
79
Archer
Elemental
Parry
Dual Wield
Move+3

Cross Bow
Poison Bow
Twist Headband
Black Costume
Jade Armlet

Charge+1, Charge+2, Charge+3, Charge+5, Charge+20
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard

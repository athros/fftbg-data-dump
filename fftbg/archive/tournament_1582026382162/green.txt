Player: !Green
Team: Green Team
Palettes: Green/White



Laserman1000
Female
Libra
68
48
Thief
Yin Yang Magic
Earplug
Equip Gun
Retreat

Blaze Gun

Holy Miter
Adaman Vest
Magic Ring

Steal Heart, Steal Armor, Steal Accessory, Steal Status
Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Petrify



KantsugaLoL
Male
Pisces
78
74
Geomancer
Jump
Counter Flood
Attack UP
Jump+2

Giant Axe
Buckler
Green Beret
Mystic Vest
N-Kai Armlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Level Jump4, Vertical Jump6



Tsarman
Female
Cancer
54
72
Knight
Basic Skill
Absorb Used MP
Secret Hunt
Waterbreathing

Battle Axe
Mythril Shield
Crystal Helmet
Genji Armor
Magic Gauntlet

Armor Break, Shield Break, Magic Break, Power Break, Stasis Sword, Dark Sword, Surging Sword
Yell, Cheer Up, Scream



Mtmcl
Female
Leo
79
41
Oracle
Throw
Dragon Spirit
Doublehand
Move+3

Bestiary

Holy Miter
Wizard Outfit
Cursed Ring

Life Drain, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Shuriken, Stick

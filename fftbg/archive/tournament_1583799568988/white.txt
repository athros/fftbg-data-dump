Player: !White
Team: White Team
Palettes: White/Blue



Chokestomp
Male
Taurus
78
62
Time Mage
Draw Out
Counter
Secret Hunt
Move-HP Up

White Staff

Golden Hairpin
Clothes
Rubber Shoes

Haste 2, Demi, Stabilize Time
Koutetsu



Genoguy
Monster
Capricorn
62
79
Coeurl










Dantetouhou
Female
Capricorn
60
60
Archer
Steal
Counter
Secret Hunt
Move+3

Silver Bow

Red Hood
Clothes
Battle Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+10
Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim



Kl0kwurk
Male
Pisces
67
51
Ninja
Basic Skill
PA Save
Secret Hunt
Move-MP Up

Koga Knife
Flame Whip
Red Hood
Mythril Vest
Rubber Shoes

Shuriken, Knife, Staff, Stick
Accumulate, Heal

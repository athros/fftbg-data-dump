Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Toka222
Male
Scorpio
38
65
Archer
Black Magic
Speed Save
Dual Wield
Move+3

Hunting Bow
Night Killer
Golden Hairpin
Mythril Vest
Defense Armlet

Charge+2, Charge+3, Charge+4
Fire 4, Bolt 2, Empower



Ayntlerz
Male
Sagittarius
53
78
Calculator
Black Magic
Speed Save
Magic Defense UP
Jump+2

Faith Rod

Leather Hat
Silk Robe
Jade Armlet

CT, Height, 4, 3
Fire 3, Fire 4, Bolt, Bolt 4, Ice 3, Flare



Goust18
Female
Sagittarius
76
44
Archer
Battle Skill
MA Save
Equip Armor
Move-HP Up

Night Killer
Mythril Shield
Barbuta
Silk Robe
N-Kai Armlet

Charge+1, Charge+7, Charge+10
Magic Break, Speed Break, Mind Break, Justice Sword



MoxOb
Male
Virgo
71
44
Chemist
Throw
MA Save
Magic Attack UP
Swim

Glacier Gun

Twist Headband
Earth Clothes
Diamond Armlet

Potion, Ether, Soft, Holy Water, Phoenix Down
Shuriken, Bomb, Knife, Sword

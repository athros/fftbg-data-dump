Player: !Green
Team: Green Team
Palettes: Green/White



Twisted Nutsatchel
Female
Pisces
54
51
Thief
Item
Counter
Maintenance
Levitate

Coral Sword

Red Hood
Mythril Vest
Feather Boots

Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Potion, Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down



RunicMagus
Male
Cancer
54
65
Samurai
White Magic
Abandon
Martial Arts
Waterbreathing

Kikuichimoji

Grand Helmet
Black Robe
Battle Boots

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Cure, Cure 2, Cure 3, Raise, Protect, Protect 2, Shell 2, Holy



Redmage4evah
Male
Scorpio
63
66
Oracle
Jump
Counter
Attack UP
Waterwalking

Ivory Rod

Red Hood
Chain Vest
Magic Ring

Blind, Life Drain, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Level Jump5, Vertical Jump2



Go2sleepTV
Male
Gemini
79
47
Ninja
Battle Skill
Mana Shield
Halve MP
Move-MP Up

Spell Edge
Sasuke Knife
Thief Hat
Clothes
Reflect Ring

Shuriken, Knife
Head Break, Shield Break, Speed Break, Mind Break, Justice Sword, Night Sword

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



UmaiJam
Male
Serpentarius
55
60
Monk
Elemental
Counter Magic
Defense UP
Ignore Terrain



Golden Hairpin
Brigandine
Rubber Shoes

Pummel, Earth Slash, Purification, Seal Evil
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Sairentozon7
Male
Pisces
68
49
Thief
Jump
Arrow Guard
Equip Armor
Levitate

Mythril Knife

Feather Hat
Chameleon Robe
Dracula Mantle

Gil Taking, Steal Shield, Leg Aim
Level Jump5, Vertical Jump7



Wyonearth
Male
Aquarius
42
45
Mime

Counter Flood
Martial Arts
Move+1


Aegis Shield
Black Hood
Clothes
Germinas Boots

Mimic




CosmicTactician
Female
Scorpio
44
77
Dancer
Charge
Caution
Short Charge
Jump+3

Ryozan Silk

Holy Miter
Black Costume
Small Mantle

Witch Hunt, Slow Dance, Polka Polka, Nameless Dance, Last Dance, Dragon Pit
Charge+3, Charge+7

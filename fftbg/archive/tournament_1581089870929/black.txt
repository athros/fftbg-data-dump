Player: !Black
Team: Black Team
Palettes: Black/Red



AlysSariel
Male
Sagittarius
65
53
Thief
Summon Magic
Caution
Sicken
Retreat

Broad Sword

Golden Hairpin
Clothes
Cursed Ring

Gil Taking, Steal Helmet, Steal Armor, Steal Status
Moogle, Ifrit, Titan, Golem, Carbunkle, Salamander, Silf



Lionhermit
Monster
Cancer
64
55
Vampire










ZephyrTempest
Female
Virgo
48
46
Dancer
Basic Skill
Dragon Spirit
Equip Gun
Jump+3

Panther Bag

Ribbon
Secret Clothes
108 Gems

Witch Hunt, Void Storage, Dragon Pit
Accumulate, Dash, Throw Stone, Yell



Jhalem
Female
Leo
75
58
Time Mage
Elemental
MA Save
Short Status
Lava Walking

Oak Staff

Triangle Hat
Chain Vest
Dracula Mantle

Haste, Haste 2, Immobilize, Demi 2, Stabilize Time, Meteor
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Lava Ball

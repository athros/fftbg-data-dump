Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



I3ahamut Prime
Monster
Virgo
66
51
Gobbledeguck










Losain
Male
Capricorn
65
58
Ninja
Black Magic
Counter Magic
Equip Gun
Jump+2

Romanda Gun
Blaze Gun
Flash Hat
Secret Clothes
Feather Boots

Sword
Fire 4, Bolt 3



Sypheck
Male
Aries
62
80
Thief
Throw
MA Save
Martial Arts
Jump+1



Flash Hat
Power Sleeve
Feather Mantle

Steal Heart, Steal Helmet, Steal Status, Arm Aim
Shuriken, Staff, Axe



Galkife
Male
Virgo
67
73
Wizard
Time Magic
Counter Tackle
Dual Wield
Fly

Ice Rod
Rod
Green Beret
Black Robe
Sprint Shoes

Bolt 4, Ice, Frog
Haste, Reflect, Quick, Stabilize Time

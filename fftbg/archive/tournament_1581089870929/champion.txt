Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Mtmcl
Female
Taurus
46
62
Ninja
Item
PA Save
Throw Item
Levitate

Kunai
Sasuke Knife
Thief Hat
Brigandine
108 Gems

Bomb, Staff, Wand
Potion, Ether, Hi-Ether, Soft, Remedy



DieInObscurity
Female
Scorpio
75
68
Mediator
Draw Out
Damage Split
Magic Attack UP
Move+1

Mythril Gun

Golden Hairpin
Leather Outfit
Reflect Ring

Persuade, Negotiate, Mimic Daravon, Refute, Rehabilitate
Kiyomori, Masamune



MrJamDango
Male
Virgo
50
75
Geomancer
White Magic
Mana Shield
Defend
Move-HP Up

Heaven's Cloud
Crystal Shield
Black Hood
Power Sleeve
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball
Cure, Cure 2, Cure 4, Protect, Shell 2, Wall, Esuna



Dynasti
Monster
Aquarius
65
74
Vampire








Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Male
Libra
44
56
Time Mage
Charge
Damage Split
Long Status
Waterbreathing

Musk Rod

Holy Miter
Linen Robe
Magic Gauntlet

Haste, Slow, Immobilize, Demi, Stabilize Time
Charge+1, Charge+3, Charge+10



Cam ATS
Female
Taurus
65
83
Mediator
Basic Skill
Arrow Guard
Concentrate
Jump+2

Romanda Gun

Leather Hat
Leather Outfit
Germinas Boots

Invitation, Praise, Preach, Refute
Throw Stone, Heal, Yell, Fury



Sinnyil2
Female
Cancer
62
63
Knight
Yin Yang Magic
Abandon
Halve MP
Move+1

Defender
Escutcheon
Diamond Helmet
Genji Armor
Leather Mantle

Weapon Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Poison, Spell Absorb, Life Drain, Dispel Magic, Paralyze, Sleep, Petrify



Galkife
Male
Taurus
70
46
Thief
Sing
Mana Shield
Short Charge
Move+3

Blind Knife

Thief Hat
Chain Vest
Diamond Armlet

Steal Helmet, Steal Accessory, Leg Aim
Angel Song, Magic Song

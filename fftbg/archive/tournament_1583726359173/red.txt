Player: !Red
Team: Red Team
Palettes: Red/Brown



Technominari
Female
Pisces
53
54
Priest
Battle Skill
Caution
Maintenance
Waterwalking

Flail

Holy Miter
Robe of Lords
Battle Boots

Raise, Raise 2, Shell 2, Esuna, Holy
Head Break, Magic Break, Power Break, Justice Sword



Oreo Pizza
Male
Virgo
67
44
Summoner
Basic Skill
Catch
Short Charge
Jump+1

Dragon Rod

Flash Hat
Adaman Vest
Leather Mantle

Moogle, Shiva, Ifrit, Carbunkle, Salamander, Silf
Accumulate, Dash, Throw Stone, Heal, Tickle, Ultima



Z32o
Female
Taurus
64
66
Chemist
Yin Yang Magic
Counter
Equip Knife
Retreat

Orichalcum

Feather Hat
Adaman Vest
Jade Armlet

Potion, Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Dispel Magic



IncredibleInept
Female
Gemini
46
43
Wizard
Basic Skill
Parry
Equip Bow
Waterwalking

Hunting Bow

Flash Hat
Wizard Robe
Defense Ring

Fire 2, Bolt, Ice, Ice 4
Accumulate, Heal, Wish

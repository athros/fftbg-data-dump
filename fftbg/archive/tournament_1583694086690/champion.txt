Player: !zChamp
Team: Champion Team
Palettes: Green/White



Sinnyil2
Female
Pisces
59
62
Ninja
Charge
PA Save
Equip Gun
Swim

Mythril Gun
Mythril Gun
Holy Miter
Judo Outfit
Elf Mantle

Shuriken
Charge+5



Omegasuspekt
Male
Gemini
74
67
Ninja
Yin Yang Magic
Critical Quick
Equip Sword
Teleport

Cute Bag
Panther Bag
Ribbon
Adaman Vest
Cherche

Shuriken, Bomb
Spell Absorb, Doubt Faith, Dispel Magic



Breakdown777
Female
Aquarius
62
51
Samurai
Steal
Counter Tackle
Defend
Move+3

Bizen Boat

Barbuta
Gold Armor
Power Wrist

Koutetsu, Kiyomori, Muramasa, Masamune
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim



GeekDadMan
Male
Capricorn
71
77
Chemist
Jump
Arrow Guard
Short Status
Move+2

Stone Gun

Leather Hat
Judo Outfit
Diamond Armlet

Potion, X-Potion, Hi-Ether, Echo Grass, Holy Water, Phoenix Down
Level Jump4, Vertical Jump7

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zebobz
Female
Gemini
60
76
Archer
Yin Yang Magic
Meatbone Slash
Equip Shield
Move-HP Up

Cross Bow
Platinum Shield
Golden Hairpin
Black Costume
Cursed Ring

Charge+1
Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Dispel Magic



KSops
Female
Gemini
74
75
Ninja
Summon Magic
Parry
Martial Arts
Retreat

Spell Edge
Hidden Knife
Headgear
Black Costume
Cursed Ring

Bomb, Sword, Hammer
Moogle, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Fairy, Cyclops



Pie108
Female
Virgo
55
65
Oracle
Throw
Blade Grasp
Equip Armor
Fly

Cypress Rod

Green Beret
Silk Robe
Sprint Shoes

Blind, Pray Faith, Foxbird, Dispel Magic, Dark Holy
Staff



Phi Sig
Female
Pisces
40
60
Oracle
Black Magic
Counter Flood
Equip Armor
Ignore Height

Ivory Rod

Leather Helmet
Leather Armor
Magic Gauntlet

Poison, Silence Song, Blind Rage, Confusion Song, Petrify
Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 3, Empower, Frog

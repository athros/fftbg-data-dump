Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Omegasuspekt
Female
Pisces
77
47
Priest
Steal
Faith Up
Short Charge
Move+1

Flail

Twist Headband
Judo Outfit
Feather Mantle

Cure, Raise, Raise 2, Regen, Shell 2, Esuna
Steal Armor, Steal Status



Eltejongrande
Female
Cancer
54
55
Summoner
Elemental
Auto Potion
Equip Bow
Waterwalking

Faith Rod

Thief Hat
Chameleon Robe
Genji Gauntlet

Ramuh, Golem, Carbunkle, Salamander, Silf, Fairy, Cyclops
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Upvla
Male
Aquarius
48
38
Wizard
Elemental
Parry
Attack UP
Jump+3

Ice Rod

Green Beret
Chain Vest
Reflect Ring

Fire 4, Bolt, Bolt 2
Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball



Blackshepherdd
Female
Aries
63
78
Geomancer
Battle Skill
PA Save
Equip Bow
Jump+1

Ultimus Bow

Leather Hat
Black Costume
Vanish Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Speed Break, Dark Sword, Night Sword

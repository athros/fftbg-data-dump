Final Bets: white - 10 bets for 9,089G (75.5%, x0.32); black - 8 bets for 2,953G (24.5%, x3.08)

white bets:
Lydian_C: 4,200G (46.2%, 47,275G)
NicoSavoy: 1,000G (11.0%, 230,057G)
BirbBrainsBot: 904G (9.9%, 97,649G)
josephiroth_143: 717G (7.9%, 717G)
ZicaX: 708G (7.8%, 708G)
roofiepops: 480G (5.3%, 480G)
dtrain332: 398G (4.4%, 398G)
AllInBot: 306G (3.4%, 306G)
datadrivenbot: 200G (2.2%, 34,912G)
Yarby: 176G (1.9%, 176G)

black bets:
solomongrundy85: 604G (20.5%, 604G)
getthemoneyz: 530G (17.9%, 1,332,252G)
dem0nj0ns: 500G (16.9%, 3,958G)
randgridr: 416G (14.1%, 416G)
KasugaiRoastedPeas: 400G (13.5%, 12,318G)
ayeayex3: 300G (10.2%, 3,694G)
NeonTarget: 153G (5.2%, 153G)
douchetron: 50G (1.7%, 1,059G)

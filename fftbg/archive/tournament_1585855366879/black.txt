Player: !Black
Team: Black Team
Palettes: Black/Red



HaateXIII
Male
Aquarius
43
41
Ninja
Elemental
Mana Shield
Equip Bow
Move+2

Hunting Bow
Night Killer
Holy Miter
Chain Vest
Elf Mantle

Shuriken, Knife
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind



Lunaticruin
Male
Libra
75
61
Geomancer
Talk Skill
Distribute
Short Charge
Lava Walking

Blood Sword
Gold Shield
Twist Headband
Power Sleeve
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Praise



SeniorBunk
Monster
Cancer
56
54
Dark Behemoth










MoonSlayerRS
Male
Scorpio
76
47
Thief
Battle Skill
MA Save
Attack UP
Jump+1

Assassin Dagger

Green Beret
Mythril Vest
Angel Ring

Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Leg Aim
Head Break, Armor Break, Shield Break, Weapon Break, Night Sword

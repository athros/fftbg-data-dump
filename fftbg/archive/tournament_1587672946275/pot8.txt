Final Bets: black - 18 bets for 20,991G (44.5%, x1.25); champion - 25 bets for 26,202G (55.5%, x0.80)

black bets:
itzover9001: 10,000G (47.6%, 46,860G)
Zeroroute: 2,539G (12.1%, 2,539G)
Lordminsc: 1,505G (7.2%, 53,730G)
JonnyCue: 1,000G (4.8%, 36,297G)
ungabunga_bot: 1,000G (4.8%, 215,700G)
BirbBrainsBot: 1,000G (4.8%, 152,034G)
Grimmace45: 837G (4.0%, 1,674G)
HASTERIOUS: 805G (3.8%, 16,109G)
Spartan_Paladin: 680G (3.2%, 54,683G)
mayormcfunbags: 500G (2.4%, 2,306G)
madming25: 228G (1.1%, 228G)
xYungAnarchyx: 200G (1.0%, 739G)
ewan_e: 200G (1.0%, 3,388G)
AllInBot: 147G (0.7%, 147G)
Ashintar: 100G (0.5%, 209G)
monkgonnacarry: 100G (0.5%, 1,990G)
CandyTamer: 100G (0.5%, 1,241G)
tmo50x: 50G (0.2%, 6,022G)

champion bets:
EnemyController: 3,500G (13.4%, 80,083G)
roofiepops: 3,200G (12.2%, 27,972G)
fenaen: 3,000G (11.4%, 52,451G)
AlenaZarek: 2,500G (9.5%, 33,981G)
Pie108: 2,000G (7.6%, 97,859G)
waterwatereverywhere: 1,420G (5.4%, 1,420G)
Shalloween: 1,300G (5.0%, 39,438G)
summmmmmmmmm: 1,083G (4.1%, 1,083G)
volgrathemoose: 1,001G (3.8%, 10,174G)
LDSkinny: 1,000G (3.8%, 13,491G)
JumbocactuarX27: 1,000G (3.8%, 5,415G)
MantisFinch: 1,000G (3.8%, 13,652G)
feDoran: 1,000G (3.8%, 6,816G)
nedryerson01: 812G (3.1%, 812G)
Laserman1000: 600G (2.3%, 48,800G)
HaateXIII: 500G (1.9%, 36,811G)
TheGuesty: 472G (1.8%, 24,473G)
nifboy: 266G (1.0%, 6,661G)
ZephyrTempest: 101G (0.4%, 182,126G)
DeathTaxesAndAnime: 100G (0.4%, 12,087G)
Evewho: 100G (0.4%, 2,799G)
datadrivenbot: 100G (0.4%, 1,545G)
RunicMagus: 100G (0.4%, 33,221G)
getthemoneyz: 44G (0.2%, 567,763G)
daveb_: 3G (0.0%, 1,691G)

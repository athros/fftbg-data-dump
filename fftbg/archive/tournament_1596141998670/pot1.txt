Final Bets: red - 25 bets for 32,955G (76.0%, x0.32); blue - 13 bets for 10,434G (24.0%, x3.16)

red bets:
coralreeferz: 6,296G (19.1%, 12,592G)
nifboy: 5,805G (17.6%, 11,611G)
sparker9: 3,000G (9.1%, 16,104G)
reinoe: 3,000G (9.1%, 148,065G)
NIghtdew14: 2,431G (7.4%, 2,431G)
KyleWonToLiveForever: 1,528G (4.6%, 1,528G)
gorgewall: 1,084G (3.3%, 1,084G)
Aldrammech: 1,000G (3.0%, 61,943G)
NotBalrog: 1,000G (3.0%, 20,716G)
BirbBrainsBot: 1,000G (3.0%, 38,424G)
Nizaha: 923G (2.8%, 923G)
dogsandcatsand: 920G (2.8%, 920G)
VolgraTheMoose: 842G (2.6%, 1,684G)
roqqqpsi: 662G (2.0%, 1,651G)
sinnyil2: 538G (1.6%, 1,076G)
Rytor: 500G (1.5%, 17,023G)
Miku_Shikhu: 427G (1.3%, 427G)
ArlanKels: 379G (1.2%, 379G)
getthemoneyz: 324G (1.0%, 1,446,797G)
CapnChaos12: 300G (0.9%, 16,342G)
AllInBot: 296G (0.9%, 296G)
TasisSai: 200G (0.6%, 10,770G)
datadrivenbot: 200G (0.6%, 46,042G)
reddwind_: 200G (0.6%, 31,195G)
holdenmagronik: 100G (0.3%, 1,104G)

blue bets:
E_Ballard: 2,848G (27.3%, 2,848G)
Zeroroute: 1,230G (11.8%, 3,385G)
LDSkinny: 1,000G (9.6%, 2,351G)
amiture: 1,000G (9.6%, 36,145G)
HaateXIII: 965G (9.2%, 965G)
Firesheath: 882G (8.5%, 882G)
superdevon1: 740G (7.1%, 37,013G)
SephDarkheart: 432G (4.1%, 6,716G)
Chambs12: 355G (3.4%, 355G)
kahjeethaswares: 304G (2.9%, 304G)
Gelwain: 300G (2.9%, 6,812G)
DAC169: 200G (1.9%, 1,278G)
maximumcrit: 178G (1.7%, 178G)

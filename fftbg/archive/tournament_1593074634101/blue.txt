Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Arcblazer23
Female
Leo
41
45
Ninja
Dance
Auto Potion
Short Status
Teleport

Orichalcum
Mage Masher
Headgear
Leather Outfit
Spike Shoes

Bomb, Staff
Nameless Dance, Nether Demon



Reddwind
Male
Gemini
80
71
Knight
Yin Yang Magic
Catch
Attack UP
Move-MP Up

Ragnarok
Diamond Shield
Crystal Helmet
Diamond Armor
Reflect Ring

Armor Break, Weapon Break, Speed Break, Mind Break
Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Dark Holy



DeathTaxesAndAnime
Female
Aries
59
65
Lancer
Punch Art
Hamedo
Dual Wield
Move+1

Holy Lance
Holy Lance
Iron Helmet
Diamond Armor
Cherche

Level Jump8, Vertical Jump4
Spin Fist, Wave Fist, Purification, Revive



ACSpree
Male
Pisces
66
49
Knight
Throw
HP Restore
Equip Knife
Move+2

Sasuke Knife
Round Shield
Gold Helmet
Silk Robe
Jade Armlet

Head Break, Shield Break, Magic Break, Power Break, Dark Sword, Surging Sword
Shuriken, Wand

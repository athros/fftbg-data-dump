Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Libra
44
65
Dancer
Time Magic
Damage Split
Secret Hunt
Teleport

Cultist Dagger

Holy Miter
Wizard Outfit
Germinas Boots

Polka Polka, Obsidian Blade, Void Storage
Haste, Haste 2, Slow, Slow 2, Float, Reflect, Quick, Demi 2, Stabilize Time



Gorgewall
Male
Capricorn
56
51
Ninja
Elemental
Caution
Secret Hunt
Jump+2

Short Edge
Spell Edge
Twist Headband
Leather Outfit
Battle Boots

Bomb, Knife
Pitfall, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind



UmaiJam
Male
Cancer
70
46
Archer
Jump
Counter
Doublehand
Fly

Glacier Gun

Flash Hat
Earth Clothes
Feather Mantle

Charge+3
Level Jump5, Vertical Jump5



Tim
Male
Aquarius
44
47
Bard
Jump
Speed Save
Equip Shield
Move+3

Ramia Harp
Crystal Shield
Flash Hat
Power Sleeve
Setiemson

Angel Song, Cheer Song, Diamond Blade, Hydra Pit
Level Jump5, Vertical Jump7

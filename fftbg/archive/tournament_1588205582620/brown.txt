Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Primeval 33
Male
Gemini
64
74
Monk
Elemental
Caution
Long Status
Waterwalking



Headgear
Clothes
Magic Ring

Spin Fist, Secret Fist, Purification, Revive, Seal Evil
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Carsenhk
Female
Pisces
46
71
Monk
Talk Skill
Catch
Magic Attack UP
Fly



Black Hood
Clothes
Feather Boots

Spin Fist, Pummel
Invitation, Persuade, Praise, Preach, Death Sentence, Insult, Refute



Jaritras
Male
Aries
75
53
Chemist
Yin Yang Magic
Auto Potion
Magic Attack UP
Jump+3

Orichalcum

Headgear
Mythril Vest
Feather Boots

Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Phoenix Down
Pray Faith, Silence Song, Confusion Song, Dispel Magic, Paralyze



Fenixcrest
Female
Gemini
54
73
Samurai
Summon Magic
Critical Quick
Dual Wield
Teleport

Koutetsu Knife
Bizen Boat
Circlet
Silk Robe
Magic Ring

Asura, Koutetsu, Kiyomori, Masamune
Ramuh, Titan, Carbunkle, Leviathan, Salamander, Lich

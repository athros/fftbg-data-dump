Player: !White
Team: White Team
Palettes: White/Blue



Bryan792
Male
Taurus
56
47
Ninja
Talk Skill
Arrow Guard
Equip Gun
Fly

Stone Gun
Romanda Gun
Leather Hat
Mystic Vest
N-Kai Armlet

Shuriken
Threaten, Insult, Mimic Daravon, Refute, Rehabilitate



Shakarak
Female
Pisces
66
69
Geomancer
Draw Out
Auto Potion
Attack UP
Move-HP Up

Diamond Sword
Round Shield
Triangle Hat
Black Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Asura, Koutetsu, Murasame



Lijarkh
Male
Aries
45
62
Monk
Steal
Absorb Used MP
Attack UP
Levitate



Headgear
Leather Outfit
Battle Boots

Pummel, Wave Fist, Purification, Revive
Steal Weapon, Steal Accessory, Steal Status, Arm Aim



Phytik
Male
Sagittarius
79
51
Squire
Item
Brave Up
Equip Knife
Swim

Spell Edge
Flame Shield
Genji Helmet
Black Costume
Bracer

Accumulate, Heal, Tickle, Wish, Ultima
Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down

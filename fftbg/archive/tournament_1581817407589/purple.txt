Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Upvla
Female
Libra
76
60
Summoner
Draw Out
Brave Up
Short Charge
Jump+3

Poison Rod

Golden Hairpin
Chain Vest
Magic Gauntlet

Shiva, Ramuh, Golem, Carbunkle, Bahamut, Fairy, Lich, Cyclops
Asura, Bizen Boat, Kikuichimoji



Beastmages
Male
Cancer
61
70
Lancer
Steal
Regenerator
Dual Wield
Jump+3

Partisan
Javelin
Diamond Helmet
Genji Armor
N-Kai Armlet

Level Jump4, Vertical Jump6
Steal Helmet, Steal Accessory, Steal Status



Denamda
Female
Capricorn
44
66
Samurai
Steal
Damage Split
Equip Bow
Jump+3

Kikuichimoji

Bronze Helmet
Linen Cuirass
Power Wrist

Koutetsu, Murasame, Muramasa, Kikuichimoji
Steal Armor, Steal Status



MagicBottle
Female
Scorpio
51
45
Calculator
Black Magic
Counter Tackle
Martial Arts
Move+2



Green Beret
Adaman Vest
Germinas Boots

CT, Prime Number, 5, 4, 3
Fire, Ice 2

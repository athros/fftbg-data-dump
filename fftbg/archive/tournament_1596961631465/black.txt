Player: !Black
Team: Black Team
Palettes: Black/Red



TheMM42
Monster
Virgo
69
70
Floating Eye










Randgridr
Male
Aquarius
50
67
Summoner
Elemental
HP Restore
Equip Bow
Jump+3

Mythril Bow

Feather Hat
Earth Clothes
108 Gems

Ifrit, Leviathan, Silf, Fairy
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



LAGBOT30000
Female
Cancer
75
55
Oracle
Throw
PA Save
Dual Wield
Ignore Terrain

Star Bag
Panther Bag
Red Hood
Linen Robe
Vanish Mantle

Blind, Life Drain, Zombie, Silence Song, Dispel Magic, Sleep, Petrify, Dark Holy
Shuriken



Go2sleepTV
Female
Scorpio
62
47
Ninja
Black Magic
Parry
Equip Sword
Levitate

Rune Blade
Kiyomori
Black Hood
Black Costume
Setiemson

Shuriken, Spear
Fire 2, Fire 4, Bolt 2, Bolt 3, Bolt 4, Empower

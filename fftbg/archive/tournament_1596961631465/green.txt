Player: !Green
Team: Green Team
Palettes: Green/White



Srdonko2098
Monster
Virgo
65
46
Red Panther










Fattunaking
Male
Leo
41
67
Monk
Draw Out
Abandon
Dual Wield
Move+3



Headgear
Chain Vest
Elf Mantle

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive
Koutetsu, Murasame, Muramasa



Gorgewall
Monster
Libra
67
52
Red Panther










Zenlion
Male
Capricorn
63
42
Thief
Yin Yang Magic
MA Save
Equip Gun
Jump+2

Blaze Gun

Holy Miter
Earth Clothes
Jade Armlet

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Silence Song, Confusion Song, Dispel Magic, Sleep, Petrify

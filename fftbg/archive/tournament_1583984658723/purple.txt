Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Spartan Paladin
Male
Aries
61
78
Knight
Sing
Parry
Short Status
Move-HP Up

Platinum Sword
Escutcheon
Genji Helmet
Plate Mail
Sprint Shoes

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword
Angel Song, Cheer Song, Last Song



Basmal
Male
Serpentarius
72
63
Ninja
Black Magic
Absorb Used MP
Equip Axe
Teleport

Morning Star
Morning Star
Red Hood
Black Costume
N-Kai Armlet

Shuriken
Fire 2, Ice 4, Empower



Gelwain
Female
Aries
80
41
Archer
Dance
Sunken State
Short Charge
Teleport

Long Bow

Triangle Hat
Mystic Vest
Magic Ring

Charge+1, Charge+4, Charge+20
Wiznaibus, Polka Polka, Disillusion, Last Dance, Dragon Pit



Red Celt
Female
Aries
74
39
Calculator
Yin Yang Magic
Counter Flood
Long Status
Jump+1

Thunder Rod

Leather Hat
Black Robe
Red Shoes

CT, Height, Prime Number, 5
Blind, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze

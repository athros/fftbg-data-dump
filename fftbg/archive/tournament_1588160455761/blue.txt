Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powergems
Male
Taurus
71
48
Geomancer
Jump
Regenerator
Maintenance
Fly

Battle Axe
Ice Shield
Thief Hat
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Gusty Wind
Level Jump8, Vertical Jump7



Solomongrundy85
Male
Sagittarius
46
43
Knight
Item
Abandon
Short Status
Jump+3

Defender
Platinum Shield
Genji Helmet
Carabini Mail
Rubber Shoes

Magic Break, Power Break
Potion, Ether, Maiden's Kiss, Holy Water, Remedy



Fenaen
Female
Gemini
49
55
Geomancer
Draw Out
Brave Up
Equip Polearm
Jump+1

Octagon Rod
Gold Shield
Flash Hat
Light Robe
Reflect Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



SaintOmerville
Female
Libra
48
55
Wizard
Punch Art
Hamedo
Short Charge
Waterbreathing

Poison Rod

Golden Hairpin
White Robe
Wizard Mantle

Fire, Fire 2, Fire 3, Bolt 4, Ice 3
Wave Fist, Purification, Revive, Seal Evil

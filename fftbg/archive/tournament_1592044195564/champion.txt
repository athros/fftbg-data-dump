Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



TeaTime29
Monster
Capricorn
53
59
Byblos










CassiePhoenix
Male
Aquarius
53
77
Archer
Item
Hamedo
Equip Axe
Move+1

Healing Staff
Crystal Shield
Platinum Helmet
Power Sleeve
Elf Mantle

Charge+5, Charge+20
Potion, Holy Water, Remedy, Phoenix Down



Prince Rogers Nelson
Female
Gemini
70
78
Summoner
Steal
MA Save
Short Charge
Retreat

Bestiary

Golden Hairpin
Silk Robe
Small Mantle

Moogle, Shiva, Ramuh, Titan
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory



DeathTaxesAndAnime
Female
Gemini
79
68
Lancer
Item
Abandon
Throw Item
Waterwalking

Obelisk
Gold Shield
Cross Helmet
Bronze Armor
Red Shoes

Level Jump4, Vertical Jump7
Potion, X-Potion, Hi-Ether, Soft, Phoenix Down

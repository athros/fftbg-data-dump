Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Bad1dea
Male
Cancer
55
56
Mime

Auto Potion
Long Status
Jump+3



Golden Hairpin
Secret Clothes
Wizard Mantle

Mimic




Lockwood
Male
Taurus
71
58
Ninja
Draw Out
Damage Split
Equip Axe
Move+2

Gold Staff
Iga Knife
Green Beret
Earth Clothes
Wizard Mantle

Shuriken, Bomb, Staff, Axe
Koutetsu, Bizen Boat



Grininda
Monster
Cancer
62
41
Red Chocobo










Shani
Female
Sagittarius
68
67
Dancer
Draw Out
Faith Up
Concentrate
Jump+1

Cute Bag

Holy Miter
Wizard Outfit
Germinas Boots

Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Last Dance
Bizen Boat

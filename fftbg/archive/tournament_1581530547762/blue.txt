Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grashmu
Female
Aries
76
42
Samurai
Summon Magic
Hamedo
Equip Shield
Swim

Javelin
Crystal Shield
Iron Helmet
Linen Robe
Dracula Mantle

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Golem, Carbunkle, Lich, Cyclops



MrUbiq
Male
Virgo
43
64
Bard
Yin Yang Magic
Counter Magic
Halve MP
Ignore Terrain

Bloody Strings

Black Hood
Crystal Mail
Magic Ring

Angel Song, Cheer Song, Space Storage, Hydra Pit
Blind, Spell Absorb, Confusion Song, Paralyze



Omegasuspekt
Male
Virgo
44
73
Ninja
Punch Art
Faith Up
Magic Defense UP
Move+3

Iga Knife
Hidden Knife
Black Hood
Chain Vest
Germinas Boots

Bomb
Earth Slash, Secret Fist, Purification, Chakra, Revive



KupoKel
Female
Cancer
44
51
Dancer
Throw
Parry
Short Charge
Jump+2

Persia

Leather Hat
Wizard Robe
Germinas Boots

Wiznaibus, Slow Dance, Disillusion, Obsidian Blade, Nether Demon
Shuriken, Bomb, Ninja Sword

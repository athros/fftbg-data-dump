Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Holyonline
Male
Leo
78
62
Bard
Charge
Speed Save
Martial Arts
Levitate

Fairy Harp

Headgear
Leather Outfit
Spike Shoes

Life Song, Magic Song
Charge+1, Charge+3, Charge+20



Ctharvey
Male
Cancer
80
78
Bard
Throw
Counter Magic
Equip Shield
Move-HP Up

Silver Bow
Gold Shield
Barette
Plate Mail
Wizard Mantle

Life Song, Cheer Song, Nameless Song, Last Song, Sky Demon, Hydra Pit
Knife, Spear



KasugaiRoastedPeas
Male
Pisces
78
50
Squire
Punch Art
Earplug
Equip Axe
Lava Walking

Mace of Zeus
Diamond Shield
Triangle Hat
Clothes
108 Gems

Dash, Fury, Wish
Wave Fist, Secret Fist, Purification



RunicMagus
Female
Aquarius
79
57
Wizard
Charge
HP Restore
Magic Attack UP
Move+3

Orichalcum

Golden Hairpin
Adaman Vest
Magic Ring

Fire 4, Bolt, Bolt 3, Empower, Frog
Charge+2, Charge+3

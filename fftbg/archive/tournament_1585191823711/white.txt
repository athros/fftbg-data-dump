Player: !White
Team: White Team
Palettes: White/Blue



Harriette
Female
Aquarius
78
68
Archer
Time Magic
Meatbone Slash
Secret Hunt
Ignore Height

Long Bow

Green Beret
Black Costume
Defense Armlet

Charge+4, Charge+5, Charge+7, Charge+20
Haste, Reflect, Stabilize Time



Shaylah
Monster
Sagittarius
73
76
Sekhret










Malorie
Female
Virgo
65
73
Wizard
Steal
Faith Up
Defense UP
Lava Walking

Ice Rod

Red Hood
Wizard Robe
Bracer

Fire 3, Fire 4
Steal Shield, Steal Weapon, Steal Accessory



Hector
Male
Leo
46
48
Archer
Jump
Damage Split
Halve MP
Waterbreathing

Mythril Gun
Genji Shield
Cross Helmet
Mystic Vest
Power Wrist

Charge+1, Charge+2, Charge+7, Charge+20
Level Jump4, Vertical Jump6

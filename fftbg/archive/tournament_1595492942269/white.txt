Player: !White
Team: White Team
Palettes: White/Blue



Legitimized
Male
Capricorn
76
47
Calculator
White Magic
Sunken State
Martial Arts
Move+3

Gokuu Rod

Twist Headband
Clothes
Battle Boots

CT, 5, 4, 3
Regen, Shell 2, Wall, Esuna



NovaKnight21
Male
Leo
75
42
Priest
Jump
Faith Save
Short Charge
Jump+1

Morning Star

Black Hood
White Robe
Cherche

Cure 2, Cure 3, Protect, Shell, Wall
Level Jump8, Vertical Jump7



Superdevon1
Female
Aries
65
46
Time Mage
Talk Skill
MA Save
Magic Attack UP
Ignore Height

Battle Bamboo

Golden Hairpin
Linen Robe
Power Wrist

Slow 2, Stop, Immobilize, Float, Reflect, Demi 2, Stabilize Time
Threaten, Preach, Solution, Negotiate, Mimic Daravon



CT 5 Holy
Male
Scorpio
52
67
Wizard
Yin Yang Magic
Counter Tackle
Magic Attack UP
Ignore Height

Dragon Rod

Flash Hat
Light Robe
Sprint Shoes

Fire 2, Fire 3, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Frog
Life Drain, Doubt Faith, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy

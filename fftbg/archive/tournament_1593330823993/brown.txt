Player: !Brown
Team: Brown Team
Palettes: Brown/Green



OneHundredFists
Female
Libra
67
78
Squire
Charge
Blade Grasp
Halve MP
Move-HP Up

Dagger
Ice Shield
Feather Hat
Leather Outfit
Defense Armlet

Accumulate, Throw Stone, Tickle, Wish
Charge+1, Charge+3, Charge+7



SeniorBunk
Male
Pisces
68
57
Ninja
Time Magic
Absorb Used MP
Equip Bow
Levitate

Snipe Bow
Hunting Bow
Twist Headband
Power Sleeve
Wizard Mantle

Ninja Sword
Haste, Stop, Immobilize, Quick, Demi, Meteor



DeathTaxesAndAnime
Female
Libra
60
70
Lancer
Yin Yang Magic
MP Restore
Defend
Ignore Height

Ivory Rod
Flame Shield
Crystal Helmet
Bronze Armor
Dracula Mantle

Level Jump8, Vertical Jump3
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Confusion Song, Dark Holy



Gamesage53
Male
Virgo
35
59
Bard
Talk Skill
Mana Shield
Equip Shield
Ignore Height

Fairy Harp
Crystal Shield
Golden Hairpin
Brigandine
Angel Ring

Last Song, Sky Demon
Praise, Threaten, Negotiate, Mimic Daravon, Refute

Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Male
Virgo
48
70
Calculator
Yin Yang Magic
Catch
Secret Hunt
Ignore Height

Iron Fan

Headgear
Light Robe
Jade Armlet

CT, Prime Number, 5, 4, 3
Blind, Poison, Life Drain, Confusion Song, Paralyze, Sleep



Galkife
Female
Sagittarius
45
42
Summoner
Elemental
Arrow Guard
Long Status
Move+2

Wizard Rod

Flash Hat
Black Robe
Angel Ring

Moogle, Ifrit, Titan, Golem, Carbunkle, Odin
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



SheWas18
Female
Taurus
48
77
Samurai
Battle Skill
Speed Save
Long Status
Ignore Terrain

Koutetsu Knife

Cross Helmet
Diamond Armor
Red Shoes

Asura, Koutetsu, Murasame, Muramasa, Kikuichimoji
Weapon Break, Magic Break, Power Break, Night Sword



GrayGhostGaming
Male
Taurus
75
74
Geomancer
Draw Out
Caution
Concentrate
Jump+3

Giant Axe
Buckler
Holy Miter
Leather Outfit
Defense Armlet

Pitfall, Hell Ivy, Static Shock, Sand Storm, Gusty Wind, Lava Ball
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji

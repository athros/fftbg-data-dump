Player: !Green
Team: Green Team
Palettes: Green/White



TasisSai
Male
Capricorn
75
75
Bard
Talk Skill
PA Save
Monster Talk
Jump+3

Fairy Harp

Red Hood
Secret Clothes
108 Gems

Angel Song, Last Song, Space Storage, Sky Demon, Hydra Pit
Invitation, Persuade, Preach, Solution, Insult, Negotiate, Refute



Nekojin
Female
Libra
60
43
Chemist
Summon Magic
MA Save
Magic Defense UP
Move+2

Mage Masher

Feather Hat
Leather Outfit
Jade Armlet

Potion, X-Potion, Ether, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down
Moogle, Titan, Odin



Nizaha
Male
Libra
47
63
Thief
Draw Out
Catch
Magic Attack UP
Move+1

Kunai

Thief Hat
Brigandine
Battle Boots

Gil Taking, Steal Shield, Arm Aim
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Chirijiraden



PantherIscariot
Female
Scorpio
54
61
Monk
Dance
Hamedo
Equip Gun
Levitate

Papyrus Codex

Holy Miter
Clothes
Sprint Shoes

Spin Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Wiznaibus, Slow Dance, Polka Polka, Obsidian Blade

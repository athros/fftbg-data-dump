Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



JonnyCue
Monster
Gemini
50
48
Serpentarius










LDSkinny
Female
Aries
79
79
Oracle
Punch Art
Damage Split
Magic Defense UP
Move+1

Cypress Rod

Twist Headband
Silk Robe
Dracula Mantle

Blind, Life Drain, Pray Faith, Foxbird, Dispel Magic
Revive



Redmage4evah
Male
Virgo
61
57
Lancer
Steal
Hamedo
Short Charge
Retreat

Holy Lance
Bronze Shield
Leather Helmet
Black Robe
Reflect Ring

Level Jump8, Vertical Jump6
Steal Heart, Steal Armor, Steal Weapon



Just Here2
Female
Virgo
66
79
Squire
Yin Yang Magic
Regenerator
Equip Polearm
Lava Walking

Octagon Rod
Crystal Shield
Ribbon
Leather Armor
Genji Gauntlet

Dash, Heal, Cheer Up
Spell Absorb, Pray Faith, Zombie, Dispel Magic, Paralyze, Dark Holy

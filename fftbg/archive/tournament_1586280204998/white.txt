Player: !White
Team: White Team
Palettes: White/Blue



Jeeboheebo
Female
Capricorn
61
54
Geomancer
Summon Magic
Counter Flood
Attack UP
Jump+3

Materia Blade
Round Shield
Green Beret
Brigandine
Germinas Boots

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Ifrit, Silf, Lich



Lunaticruin
Female
Capricorn
71
58
Wizard
Draw Out
Arrow Guard
Dual Wield
Waterbreathing

Flame Rod
Dragon Rod
Black Hood
Chameleon Robe
Sprint Shoes

Fire 3, Empower, Death, Flare
Asura, Koutetsu, Bizen Boat



Tithonus
Female
Aquarius
68
55
Samurai
Jump
Absorb Used MP
Attack UP
Teleport

Murasame

Leather Helmet
Wizard Robe
Dracula Mantle

Asura, Bizen Boat
Level Jump2, Vertical Jump8



Reinoe
Monster
Aries
72
44
Ultima Demon








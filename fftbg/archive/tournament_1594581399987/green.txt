Player: !Green
Team: Green Team
Palettes: Green/White



PoroTact
Male
Scorpio
67
50
Chemist
Basic Skill
Critical Quick
Equip Polearm
Ignore Terrain

Musk Rod

Headgear
Judo Outfit
N-Kai Armlet

Potion, Echo Grass, Soft, Remedy, Phoenix Down
Throw Stone, Tickle, Cheer Up, Wish



Randgridr
Male
Taurus
45
50
Archer
Item
PA Save
Equip Shield
Retreat

Hunting Bow
Bronze Shield
Leather Hat
Black Costume
Feather Boots

Charge+1, Charge+5, Charge+10, Charge+20
Potion, Hi-Potion, X-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down



WillFitzgerald
Female
Virgo
37
65
Summoner
Steal
Faith Save
Attack UP
Move-HP Up

Gold Staff

Red Hood
Silk Robe
Germinas Boots

Moogle, Shiva, Ramuh, Carbunkle, Odin, Leviathan, Lich
Steal Armor, Steal Weapon



Sairentozon7
Monster
Sagittarius
67
70
Behemoth








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galkife
Female
Sagittarius
57
67
Knight
Steal
Counter Tackle
Equip Polearm
Teleport

Javelin
Mythril Shield
Barbuta
Linen Cuirass
Cherche

Armor Break, Night Sword
Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim



Lythe Caraker
Female
Libra
41
40
Priest
Time Magic
Arrow Guard
Martial Arts
Move-HP Up

Healing Staff

Golden Hairpin
Chameleon Robe
Sprint Shoes

Cure 2, Raise 2, Shell 2, Wall, Holy
Haste, Reflect, Demi, Demi 2



ANFz
Male
Aquarius
54
51
Ninja
Basic Skill
Distribute
Attack UP
Retreat

Morning Star
Sasuke Knife
Feather Hat
Earth Clothes
Genji Gauntlet

Bomb
Throw Stone



Laserman1000
Female
Scorpio
57
52
Summoner
Yin Yang Magic
Regenerator
Short Charge
Move+2

Poison Rod

Golden Hairpin
Chameleon Robe
Feather Mantle

Moogle, Shiva, Ifrit, Lich, Cyclops
Poison, Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Furrytomahawkk
Female
Aries
46
65
Wizard
Battle Skill
Parry
Equip Axe
Lava Walking

Flail

Headgear
Rubber Costume
Chantage

Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 3, Ice 4
Armor Break, Shield Break, Mind Break, Stasis Sword, Justice Sword



Mysteriousdewd
Male
Aries
51
72
Priest
Time Magic
Parry
Attack UP
Retreat

White Staff

Headgear
Chameleon Robe
Diamond Armlet

Cure 2, Cure 3, Cure 4, Raise 2, Regen, Protect, Shell, Shell 2, Esuna, Holy
Haste, Slow, Immobilize, Float, Stabilize Time



Roqqqpsi
Female
Scorpio
43
50
Dancer
Item
Earplug
Throw Item
Lava Walking

Persia

Ribbon
Clothes
Power Wrist

Slow Dance, Polka Polka, Disillusion
Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down



TasisSai
Male
Gemini
51
67
Knight
Jump
HP Restore
Equip Sword
Move-MP Up

Ancient Sword
Buckler
Leather Helmet
Carabini Mail
Leather Mantle

Head Break, Speed Break, Power Break, Stasis Sword
Level Jump5, Vertical Jump7

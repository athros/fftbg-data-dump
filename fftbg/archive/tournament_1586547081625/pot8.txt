Final Bets: white - 19 bets for 9,739G (8.3%, x11.04); champion - 37 bets for 107,473G (91.7%, x0.09)

white bets:
Lanshaft: 1,610G (16.5%, 1,610G)
mirapoix: 1,459G (15.0%, 1,459G)
kai_shee: 1,001G (10.3%, 127,578G)
KasugaiRoastedPeas: 1,000G (10.3%, 4,792G)
BirbBrainsBot: 1,000G (10.3%, 53,099G)
Shalloween: 700G (7.2%, 26,636G)
RunicMagus: 633G (6.5%, 633G)
DudeMonkey77: 348G (3.6%, 348G)
joewcarson: 300G (3.1%, 3,123G)
MrDiggs49: 256G (2.6%, 256G)
ZephyrTempest: 246G (2.5%, 48,424G)
JackOnFire1: 244G (2.5%, 244G)
old_overholt_: 200G (2.1%, 9,085G)
silentperogy: 200G (2.1%, 18,821G)
roqqqpsi: 123G (1.3%, 1,120G)
byrdturbo: 111G (1.1%, 6,987G)
wollise89: 108G (1.1%, 560G)
RughSontos: 100G (1.0%, 2,533G)
Jennero: 100G (1.0%, 936G)

champion bets:
Ragnarok784: 72,585G (67.5%, 72,585G)
Laserman1000: 4,500G (4.2%, 22,800G)
sinnyil2: 4,139G (3.9%, 8,279G)
CCRUNNER149UWP: 3,333G (3.1%, 8,427G)
DeathTaxesAndAnime: 2,816G (2.6%, 5,523G)
catfashions: 2,000G (1.9%, 38,289G)
kekkerscrew: 2,000G (1.9%, 8,828G)
reinoe: 2,000G (1.9%, 21,403G)
HASTERIOUS: 1,566G (1.5%, 1,566G)
Firesheath: 1,179G (1.1%, 1,179G)
Bulleta: 1,167G (1.1%, 1,167G)
Ausuri: 1,008G (0.9%, 15,008G)
Lazerkick: 1,000G (0.9%, 4,223G)
Mtueni: 1,000G (0.9%, 179,987G)
datadrivenbot: 831G (0.8%, 25,629G)
ungabunga_bot: 809G (0.8%, 88,985G)
Pie108: 700G (0.7%, 16,718G)
ApplesauceBoss: 508G (0.5%, 508G)
nifboy: 500G (0.5%, 3,959G)
Goust18: 500G (0.5%, 10,294G)
V3rdeni: 500G (0.5%, 3,299G)
stukerk: 400G (0.4%, 10,018G)
Cryptopsy70: 356G (0.3%, 9,420G)
Tithonus: 300G (0.3%, 15,085G)
fenaen: 300G (0.3%, 5,033G)
Conome: 240G (0.2%, 240G)
Faplo: 236G (0.2%, 236G)
LordMaxoss: 216G (0.2%, 216G)
Snowfats: 132G (0.1%, 132G)
organizing_secrets: 100G (0.1%, 100G)
Tougou: 100G (0.1%, 5,916G)
friendsquirrel: 100G (0.1%, 2,561G)
maakur_: 100G (0.1%, 2,596G)
blinkmachine: 100G (0.1%, 10,845G)
IndecisiveNinja: 100G (0.1%, 4,271G)
b0shii: 50G (0.0%, 3,993G)
ko2q: 2G (0.0%, 1,416G)

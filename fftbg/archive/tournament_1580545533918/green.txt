Player: !Green
Team: Green Team
Palettes: Green/White



HV5H
Female
Scorpio
46
59
Priest
Draw Out
Damage Split
Concentrate
Move+2

Flail

Green Beret
Chameleon Robe
Magic Gauntlet

Cure, Cure 4, Raise, Raise 2, Esuna
Asura, Bizen Boat



Deafbarro
Female
Libra
44
48
Archer
Black Magic
Sunken State
Short Status
Jump+3

Glacier Gun
Escutcheon
Ribbon
Wizard Outfit
Feather Boots

Charge+1, Charge+3, Charge+10
Fire 2, Bolt 2, Bolt 4, Ice 2, Frog, Death



Ruvelia BibeI
Monster
Scorpio
51
72
Ahriman










Scionith
Female
Pisces
40
78
Thief
Throw
Arrow Guard
Beastmaster
Move+3

Dagger

Golden Hairpin
Leather Outfit
Jade Armlet

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Knife, Dictionary

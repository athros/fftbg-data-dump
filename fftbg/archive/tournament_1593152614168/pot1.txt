Final Bets: red - 14 bets for 11,478G (73.6%, x0.36); blue - 13 bets for 4,116G (26.4%, x2.79)

red bets:
sinnyil2: 6,280G (54.7%, 12,560G)
Laserman1000: 1,347G (11.7%, 2,643G)
SkylerBunny: 1,300G (11.3%, 502,643G)
Zetchryn: 404G (3.5%, 404G)
reddwind_: 392G (3.4%, 392G)
Phytik: 348G (3.0%, 348G)
LAGBOT30000: 300G (2.6%, 4,779G)
Chuckolator: 241G (2.1%, 124,700G)
KasugaiRoastedPeas: 200G (1.7%, 633G)
Evewho: 200G (1.7%, 9,383G)
Coolguye: 150G (1.3%, 2,504G)
fluffskull: 108G (0.9%, 3,512G)
Mermydon: 108G (0.9%, 108G)
datadrivenbot: 100G (0.9%, 49,456G)

blue bets:
KyleWonToLiveForever: 938G (22.8%, 938G)
helpimabug: 500G (12.1%, 5,471G)
KaLam1ty: 500G (12.1%, 16,113G)
ColetteMSLP: 400G (9.7%, 5,769G)
DuraiPapers: 352G (8.6%, 352G)
Lythe_Caraker: 250G (6.1%, 143,149G)
getthemoneyz: 240G (5.8%, 1,052,176G)
placidphoenix: 236G (5.7%, 236G)
CelestialLancer: 200G (4.9%, 50,814G)
BirbBrainsBot: 200G (4.9%, 200G)
superdevon1: 100G (2.4%, 2,554G)
Tithonus: 100G (2.4%, 3,076G)
LunaArgenteus: 100G (2.4%, 100G)

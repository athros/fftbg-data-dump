Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MLWebz0r
Female
Sagittarius
59
66
Thief
Draw Out
Counter Magic
Short Status
Move+1

Ancient Sword

Flash Hat
Leather Outfit
Bracer

Gil Taking, Steal Heart, Steal Armor
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



Lali Lulelo
Female
Libra
72
56
Priest
Battle Skill
Catch
Short Status
Jump+2

Flail

Twist Headband
White Robe
Diamond Armlet

Cure, Raise, Raise 2, Regen, Protect 2, Shell, Esuna
Head Break, Shield Break, Power Break, Mind Break, Stasis Sword, Justice Sword



KasugaiRoastedPeas
Female
Aries
77
77
Time Mage
Dance
Mana Shield
Equip Sword
Waterbreathing

Bizen Boat

Thief Hat
Judo Outfit
Angel Ring

Haste, Slow, Float, Reflect, Quick, Demi 2, Stabilize Time
Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Last Dance, Nether Demon, Dragon Pit



DAC169
Male
Taurus
70
43
Calculator
White Magic
HP Restore
Short Charge
Retreat

Gokuu Rod

Leather Hat
Judo Outfit
Wizard Mantle

CT, 5, 3
Cure 3, Raise 2, Reraise, Regen, Magic Barrier

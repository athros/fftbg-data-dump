Player: !Green
Team: Green Team
Palettes: Green/White



ThanatosXRagnarok
Male
Cancer
54
44
Summoner
Item
Caution
Dual Wield
Move-MP Up

Rainbow Staff
Flame Rod
Flash Hat
Chameleon Robe
108 Gems

Moogle, Shiva, Golem, Carbunkle, Silf, Cyclops
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Bojanges
Female
Aquarius
73
53
Chemist
Steal
Absorb Used MP
Magic Attack UP
Teleport

Main Gauche

Headgear
Brigandine
Dracula Mantle

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down
Gil Taking, Steal Weapon, Steal Accessory, Steal Status



Sinnyil2
Female
Pisces
73
79
Oracle
Basic Skill
Dragon Spirit
Defense UP
Teleport

Gokuu Rod

Twist Headband
Silk Robe
Small Mantle

Foxbird, Dispel Magic
Heal



Gorgewall
Female
Sagittarius
51
50
Geomancer
Punch Art
Parry
Long Status
Teleport

Long Sword
Aegis Shield
Golden Hairpin
Leather Outfit
Setiemson

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard
Spin Fist, Wave Fist, Secret Fist, Purification, Seal Evil

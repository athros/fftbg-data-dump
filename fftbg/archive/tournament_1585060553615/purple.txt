Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gus Hobbleton
Male
Aries
50
67
Archer
Throw
Speed Save
Defend
Move+2

Bow Gun
Hero Shield
Golden Hairpin
Black Costume
Feather Mantle

Charge+1, Charge+4
Shuriken, Bomb, Wand



Evolvingdev
Female
Scorpio
39
43
Thief
Draw Out
Abandon
Equip Gun
Jump+1

Bestiary

Red Hood
Chain Vest
Reflect Ring

Gil Taking, Steal Shield
Asura, Koutetsu, Murasame, Muramasa



Zagorsek
Female
Cancer
58
69
Geomancer
Draw Out
Auto Potion
Martial Arts
Waterwalking

Giant Axe
Round Shield
Leather Hat
Wizard Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Asura, Koutetsu



Lowlf
Male
Capricorn
43
70
Time Mage
Steal
MA Save
Martial Arts
Move-MP Up



Red Hood
White Robe
N-Kai Armlet

Slow, Slow 2, Quick, Demi, Stabilize Time
Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Arm Aim

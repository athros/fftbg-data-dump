Player: !White
Team: White Team
Palettes: White/Blue



Mesmaster
Female
Libra
41
73
Chemist
White Magic
Caution
Doublehand
Move+2

Assassin Dagger

Headgear
Wizard Outfit
Red Shoes

Potion, Hi-Potion, Soft, Phoenix Down
Cure 3, Cure 4, Raise, Raise 2, Shell 2, Esuna



Sairentozon7
Male
Libra
58
54
Ninja
Elemental
Hamedo
Equip Axe
Teleport 2

Oak Staff
White Staff
Holy Miter
Mystic Vest
Magic Gauntlet

Shuriken, Knife
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard



Cougboi
Male
Scorpio
66
81
Oracle
Summon Magic
Auto Potion
Doublehand
Ignore Height

Battle Folio

Flash Hat
Light Robe
Elf Mantle

Blind, Life Drain, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Moogle, Ifrit, Carbunkle, Lich, Cyclops



R Raynos
Female
Sagittarius
60
58
Mediator
Summon Magic
Counter
Defend
Ignore Height

Mythril Gun

Feather Hat
Light Robe
Magic Ring

Praise, Death Sentence, Negotiate, Refute, Rehabilitate
Moogle, Shiva, Ifrit, Golem, Fairy

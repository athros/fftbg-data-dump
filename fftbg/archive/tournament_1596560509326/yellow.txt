Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Chuckolator
Monster
Gemini
80
56
Ochu










TasisSai
Female
Aquarius
45
61
Calculator
Byblos
HP Restore
Martial Arts
Lava Walking

Madlemgen

Green Beret
Leather Outfit
Diamond Armlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Fenaen
Female
Aries
80
62
Wizard
Charge
Sunken State
Equip Polearm
Swim

Musk Rod

Holy Miter
Brigandine
Angel Ring

Fire 3, Fire 4, Bolt, Bolt 4, Ice
Charge+1, Charge+2, Charge+4



Lokenwow
Male
Serpentarius
67
74
Ninja
Steal
Abandon
Equip Bow
Jump+3

Ultimus Bow

Leather Hat
Chain Vest
Sprint Shoes

Shuriken, Bomb, Staff, Spear
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Leg Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



Mexskacin
Male
Taurus
77
48
Geomancer
Draw Out
Parry
Equip Axe
Fly

Sage Staff
Escutcheon
Green Beret
Adaman Vest
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Murasame, Kiyomori, Muramasa, Masamune



Lanshaft
Male
Cancer
80
43
Archer
Talk Skill
Caution
Equip Shield
Jump+1

Mythril Bow
Escutcheon
Crystal Helmet
Adaman Vest
Diamond Armlet

Charge+2, Charge+5, Charge+7
Praise, Threaten, Negotiate, Refute



Arkreaver
Female
Aquarius
65
73
Mediator
Throw
Mana Shield
Doublehand
Jump+1

Papyrus Codex

Green Beret
Silk Robe
Spike Shoes

Persuade, Praise, Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute
Bomb



Dexef
Male
Aries
56
72
Samurai
Charge
Meatbone Slash
Magic Defense UP
Move+3

Heaven's Cloud

Platinum Helmet
Mythril Armor
Defense Armlet

Koutetsu, Muramasa, Chirijiraden
Charge+1, Charge+4, Charge+20

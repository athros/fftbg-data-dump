Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Lijarkh
Male
Libra
53
55
Monk
Elemental
Arrow Guard
Equip Axe
Jump+1

Morning Star

Leather Hat
Black Costume
Power Wrist

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball



HASTERIOUS
Male
Capricorn
52
77
Priest
Elemental
Dragon Spirit
Doublehand
Jump+3

Wizard Staff

Flash Hat
White Robe
Angel Ring

Cure 2, Cure 4, Raise, Reraise, Regen, Wall, Esuna, Holy
Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



ALY327
Female
Leo
73
64
Monk
Summon Magic
Counter Tackle
Long Status
Move-HP Up



Black Hood
Brigandine
Angel Ring

Spin Fist, Earth Slash, Purification
Moogle, Shiva, Ifrit, Titan, Carbunkle, Silf, Fairy



Upvla
Male
Sagittarius
80
74
Ninja
Time Magic
Auto Potion
Sicken
Swim

Assassin Dagger
Flail
Twist Headband
Adaman Vest
108 Gems

Shuriken, Bomb
Haste, Slow, Slow 2, Quick, Demi 2, Stabilize Time, Meteor

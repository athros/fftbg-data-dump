Player: !zChamp
Team: Champion Team
Palettes: Black/Red



LivingHitokiri
Male
Sagittarius
65
42
Lancer
Draw Out
Brave Up
Doublehand
Retreat

Dragon Whisker

Crystal Helmet
Carabini Mail
Feather Boots

Level Jump3, Vertical Jump8
Koutetsu, Bizen Boat, Murasame



DustBirdEX
Female
Scorpio
50
42
Archer
Dance
Mana Shield
Beastmaster
Waterwalking

Ice Bow

Leather Hat
Chain Vest
Chantage

Charge+1, Charge+5, Charge+7
Disillusion, Nameless Dance, Nether Demon



ApplesauceBoss
Male
Scorpio
46
41
Monk
Talk Skill
Counter
Attack UP
Jump+3



Twist Headband
Power Sleeve
Feather Boots

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Invitation, Mimic Daravon, Refute



Nifboy
Male
Virgo
75
47
Ninja
Draw Out
Distribute
Equip Polearm
Teleport

Spear
Octagon Rod
Twist Headband
Judo Outfit
Red Shoes

Bomb, Staff
Murasame

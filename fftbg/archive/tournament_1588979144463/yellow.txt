Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HaateXIII
Male
Leo
45
64
Squire
Summon Magic
Earplug
Equip Gun
Fly

Blaze Gun
Round Shield
Black Hood
Judo Outfit
Feather Boots

Heal, Wish
Shiva, Ramuh, Ifrit, Carbunkle, Leviathan, Silf



Maakur
Male
Aquarius
77
78
Knight
Draw Out
Counter
Magic Defense UP
Move+3

Platinum Sword
Round Shield
Genji Helmet
Reflect Mail
Cherche

Armor Break, Shield Break, Mind Break, Stasis Sword, Justice Sword
Kiyomori, Muramasa, Kikuichimoji



Volgrathemoose
Male
Pisces
71
44
Archer
Basic Skill
Abandon
Doublehand
Swim

Bow Gun

Platinum Helmet
Leather Outfit
N-Kai Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+10
Accumulate, Heal, Fury, Scream



HASTERIOUS
Male
Virgo
45
61
Ninja
Basic Skill
Speed Save
Martial Arts
Fly



Leather Hat
Adaman Vest
Angel Ring

Shuriken, Bomb, Dictionary
Accumulate, Heal, Fury, Wish

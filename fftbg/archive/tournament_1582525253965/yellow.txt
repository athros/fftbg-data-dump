Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Defaultlybrave
Male
Aquarius
50
52
Lancer
Summon Magic
Earplug
Attack UP
Teleport

Partisan
Aegis Shield
Gold Helmet
Mythril Armor
Red Shoes

Level Jump5, Vertical Jump7
Shiva, Golem, Carbunkle, Odin, Fairy



SkyridgeZero
Female
Gemini
78
59
Chemist
Dance
Meatbone Slash
Long Status
Lava Walking

Blast Gun

Flash Hat
Black Costume
Magic Ring

Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Wiznaibus, Disillusion, Obsidian Blade, Nether Demon



Harbinger605
Male
Taurus
44
78
Archer
White Magic
Parry
Equip Knife
Jump+2

Mythril Knife
Diamond Shield
Green Beret
Power Sleeve
Sprint Shoes

Charge+2, Charge+3, Charge+4, Charge+7, Charge+10
Raise, Raise 2, Protect 2, Wall, Esuna



Alaylle
Male
Scorpio
71
60
Bard
Time Magic
Counter Magic
Doublehand
Waterwalking

Bloody Strings

Twist Headband
Crystal Mail
Jade Armlet

Angel Song, Cheer Song, Battle Song, Last Song, Sky Demon, Hydra Pit
Haste, Slow, Stop, Float, Quick, Stabilize Time

Player: !Red
Team: Red Team
Palettes: Red/Brown



Arch8000
Female
Scorpio
43
59
Thief
Dance
Critical Quick
Martial Arts
Waterbreathing

Rune Blade

Leather Hat
Earth Clothes
Small Mantle

Steal Weapon, Steal Accessory, Steal Status
Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Obsidian Blade



Thiccshoes
Monster
Libra
47
71
Serpentarius










AzuriteReaction
Female
Libra
60
65
Mediator
Throw
Mana Shield
Doublehand
Move+3

Romanda Gun

Holy Miter
Light Robe
Red Shoes

Persuade, Preach, Mimic Daravon
Shuriken, Knife, Staff



Alacor
Male
Virgo
54
69
Knight
Time Magic
Parry
Equip Gun
Move+1

Romanda Gun
Flame Shield
Gold Helmet
Diamond Armor
Sprint Shoes

Stasis Sword, Surging Sword
Haste 2, Slow, Slow 2, Stop, Float, Quick, Demi 2, Meteor

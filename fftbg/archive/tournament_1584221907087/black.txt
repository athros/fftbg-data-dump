Player: !Black
Team: Black Team
Palettes: Black/Red



Gelwain
Female
Taurus
73
80
Archer
Steal
Counter
Equip Knife
Teleport

Spell Edge
Mythril Shield
Iron Helmet
Mythril Vest
Wizard Mantle

Charge+1, Charge+7
Gil Taking, Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



OmnibotGamma
Female
Gemini
68
64
Wizard
Talk Skill
Absorb Used MP
Monster Talk
Move+2

Poison Rod

Feather Hat
Linen Robe
Rubber Shoes

Fire, Fire 2, Bolt, Ice 2, Flare
Praise, Threaten, Refute



LanseDM
Female
Aries
68
54
Mime

Counter
Attack UP
Move-HP Up



Leather Hat
Chameleon Robe
Power Wrist

Mimic




MyFakeLife
Female
Taurus
60
42
Dancer
Time Magic
Parry
Magic Defense UP
Ignore Terrain

Cashmere

Feather Hat
Chain Vest
N-Kai Armlet

Witch Hunt, Wiznaibus, Disillusion
Slow 2, Immobilize, Float, Stabilize Time

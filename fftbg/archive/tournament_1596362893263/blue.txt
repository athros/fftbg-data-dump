Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Randgridr
Female
Libra
66
58
Oracle
Summon Magic
Speed Save
Maintenance
Jump+1

Papyrus Codex

Flash Hat
Wizard Robe
Defense Armlet

Poison, Blind Rage, Dispel Magic, Sleep, Petrify
Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Lich, Cyclops



Djorama
Male
Gemini
54
64
Calculator
Yin Yang Magic
Counter Tackle
Magic Defense UP
Move+2

Papyrus Codex

Red Hood
Chain Vest
Power Wrist

Height, Prime Number, 5
Pray Faith, Doubt Faith, Silence Song



Fluffskull
Male
Sagittarius
71
41
Monk
Item
Caution
Short Charge
Move+1



Triangle Hat
Leather Outfit
N-Kai Armlet

Pummel, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil
Potion, X-Potion, Ether, Echo Grass, Soft, Phoenix Down



Ar Tactic
Male
Virgo
48
79
Knight
Talk Skill
Auto Potion
Defense UP
Lava Walking

Diamond Sword
Bronze Shield
Leather Helmet
Wizard Robe
N-Kai Armlet

Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword, Surging Sword
Persuade, Praise, Preach, Solution, Refute

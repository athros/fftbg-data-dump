Final Bets: green - 7 bets for 11,449G (78.7%, x0.27); yellow - 6 bets for 3,100G (21.3%, x3.69)

green bets:
KasugaiRoastedPeas: 6,895G (60.2%, 6,895G)
E_Ballard: 1,811G (15.8%, 1,811G)
DeathTaxesAndAnime: 1,242G (10.8%, 2,436G)
randgridr: 600G (5.2%, 600G)
dem0nj0ns: 500G (4.4%, 4,236G)
gorgewall: 201G (1.8%, 9,191G)
datadrivenbot: 200G (1.7%, 50,114G)

yellow bets:
BirbBrainsBot: 1,000G (32.3%, 131,542G)
getthemoneyz: 1,000G (32.3%, 1,474,537G)
maximumcrit: 500G (16.1%, 14,767G)
AllInBot: 200G (6.5%, 200G)
StrawbreeRed: 200G (6.5%, 1,361G)
SensualADC: 200G (6.5%, 2,569G)

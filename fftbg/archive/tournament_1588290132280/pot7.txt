Final Bets: blue - 16 bets for 7,978G (39.4%, x1.54); brown - 24 bets for 12,284G (60.6%, x0.65)

blue bets:
volgrathemoose: 1,001G (12.5%, 15,849G)
Aldrammech: 1,000G (12.5%, 8,897G)
ungabunga_bot: 1,000G (12.5%, 293,218G)
BirbBrainsBot: 1,000G (12.5%, 170,400G)
TrueRhyme: 847G (10.6%, 847G)
Baron_von_Scrub: 647G (8.1%, 1,269G)
getthemoneyz: 514G (6.4%, 611,145G)
vorap: 500G (6.3%, 33,518G)
EnemyController: 500G (6.3%, 162,019G)
astrozin11: 268G (3.4%, 1,891G)
Nizaha: 201G (2.5%, 14,443G)
vampyreinabox: 100G (1.3%, 100G)
ANFz: 100G (1.3%, 25,406G)
Tougou: 100G (1.3%, 12,815G)
Triskiut: 100G (1.3%, 577G)
Bioticism: 100G (1.3%, 3,672G)

brown bets:
jakeduhhsnake: 2,000G (16.3%, 72,271G)
Odin007_88: 1,613G (13.1%, 1,613G)
sinnyil2: 1,200G (9.8%, 12,028G)
HorusTaurus: 1,000G (8.1%, 2,840G)
rico_flex: 1,000G (8.1%, 24,970G)
dorman_777: 673G (5.5%, 673G)
DeathTaxesAndAnime: 632G (5.1%, 632G)
FriendSquirrel: 536G (4.4%, 22,088G)
twelfthrootoftwo: 500G (4.1%, 6,051G)
ZephyrTempest: 456G (3.7%, 3,835G)
Chuckolator: 449G (3.7%, 20,991G)
joewcarson: 380G (3.1%, 380G)
SQUiDSQUARKLIN: 256G (2.1%, 256G)
nifboy: 250G (2.0%, 9,087G)
cgMcWhiskers: 244G (2.0%, 244G)
alecttox: 229G (1.9%, 9,229G)
SarrgeQc: 208G (1.7%, 208G)
eudes89: 108G (0.9%, 108G)
DrAntiSocial: 100G (0.8%, 9,879G)
Evewho: 100G (0.8%, 5,614G)
maakur_: 100G (0.8%, 113,353G)
datadrivenbot: 100G (0.8%, 11,281G)
ko2q: 100G (0.8%, 3,228G)
b0shii: 50G (0.4%, 13,731G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Oogthecaveman
Female
Libra
66
73
Chemist
Basic Skill
Sunken State
Short Charge
Waterwalking

Panther Bag

Red Hood
Wizard Outfit
Sprint Shoes

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Heal, Yell, Wish, Scream



Lanshaft
Female
Libra
73
53
Summoner
Time Magic
PA Save
Equip Shield
Move-HP Up

Dragon Rod
Gold Shield
Red Hood
Chameleon Robe
Bracer

Moogle, Shiva, Titan, Bahamut, Salamander, Fairy
Slow, Immobilize, Reflect, Demi 2



Dynasti
Male
Gemini
51
38
Mediator
Punch Art
Brave Up
Dual Wield
Levitate

Blaze Gun
Blaze Gun
Black Hood
Wizard Robe
Magic Gauntlet

Praise, Threaten, Solution, Death Sentence, Insult, Negotiate
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive



Jethrothrul
Male
Pisces
59
60
Bard
Summon Magic
Auto Potion
Doublehand
Jump+3

Fairy Harp

Leather Hat
Clothes
108 Gems

Life Song, Battle Song, Last Song, Sky Demon
Moogle, Shiva, Ramuh, Ifrit, Bahamut, Odin, Cyclops

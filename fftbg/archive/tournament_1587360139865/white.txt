Player: !White
Team: White Team
Palettes: White/Blue



Skysa250
Male
Gemini
71
53
Chemist
Battle Skill
Parry
Defense UP
Move-HP Up

Mythril Gun

Holy Miter
Brigandine
Jade Armlet

Potion, Hi-Potion, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Head Break, Armor Break, Magic Break



ANFz
Female
Libra
41
61
Oracle
Basic Skill
Dragon Spirit
Equip Polearm
Ignore Terrain

Ryozan Silk

Green Beret
Silk Robe
Feather Mantle

Spell Absorb, Life Drain, Pray Faith, Zombie, Dispel Magic
Dash, Heal, Yell, Fury



Dictatorhowells
Female
Libra
40
71
Calculator
Time Magic
MA Save
Short Charge
Levitate

Dragon Rod

Flash Hat
Judo Outfit
N-Kai Armlet

CT, Height, Prime Number, 5, 3
Stop, Demi, Stabilize Time, Meteor



HaplessOne
Male
Libra
77
71
Ninja
Sing
Mana Shield
Short Charge
Levitate

Kunai
Hidden Knife
Green Beret
Judo Outfit
N-Kai Armlet

Shuriken, Bomb, Axe, Dictionary
Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade

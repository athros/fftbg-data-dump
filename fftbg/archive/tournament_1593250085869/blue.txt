Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kronikle
Female
Taurus
66
61
Lancer
Dance
Catch
Concentrate
Jump+2

Spear
Round Shield
Crystal Helmet
Reflect Mail
Chantage

Level Jump8, Vertical Jump5
Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Last Dance



CrownOfHorns
Female
Gemini
63
46
Monk
Battle Skill
Counter
Maintenance
Teleport

Panther Bag

Feather Hat
Judo Outfit
Magic Gauntlet

Earth Slash, Purification, Revive
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Justice Sword



Mesmaster
Female
Leo
43
59
Priest
Draw Out
Blade Grasp
Dual Wield
Jump+1

Flail
Hydra Bag
Leather Hat
Black Robe
Rubber Shoes

Cure 2, Raise, Protect 2
Bizen Boat, Heaven's Cloud



Juan
Male
Taurus
47
77
Samurai
Black Magic
Catch
Secret Hunt
Jump+3

Spear

Gold Helmet
Gold Armor
Feather Mantle

Koutetsu, Kiyomori
Fire 4, Bolt 2, Death

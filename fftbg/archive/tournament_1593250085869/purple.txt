Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Reddwind
Male
Libra
46
48
Knight
Time Magic
Parry
Equip Axe
Move+2

Morning Star
Flame Shield
Diamond Helmet
Platinum Armor
Reflect Ring

Head Break, Speed Break, Power Break
Haste



BlackFireUK
Female
Libra
45
64
Samurai
Jump
Parry
Maintenance
Move+1

Muramasa

Circlet
Linen Cuirass
Sprint Shoes

Asura, Muramasa, Kikuichimoji
Level Jump8, Vertical Jump7



Hasterious
Female
Aries
52
52
Summoner
Black Magic
Meatbone Slash
Short Charge
Move+2

Gold Staff

Black Hood
Adaman Vest
Battle Boots

Moogle, Shiva, Titan, Leviathan, Silf, Fairy
Fire, Bolt 2, Bolt 3, Ice 2, Ice 3, Empower, Frog, Flare



Philipa
Female
Scorpio
75
48
Archer
Jump
Abandon
Magic Defense UP
Fly

Snipe Bow
Aegis Shield
Triangle Hat
Mythril Vest
Genji Gauntlet

Charge+4, Charge+5, Charge+10, Charge+20
Level Jump5, Vertical Jump2

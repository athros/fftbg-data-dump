Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Scadooshlol
Male
Leo
50
68
Ninja
Steal
Distribute
Equip Bow
Waterwalking

Mythril Bow

Golden Hairpin
Black Costume
N-Kai Armlet

Knife, Wand
Steal Accessory, Leg Aim



CassiePhoenix
Monster
Aquarius
60
58
Steel Giant










Sinnyil2
Male
Aquarius
68
76
Oracle
Summon Magic
HP Restore
Equip Knife
Teleport

Ice Rod

Feather Hat
Wizard Robe
Feather Mantle

Life Drain, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Sleep, Dark Holy
Moogle, Titan, Carbunkle, Lich



Sairentozon7
Male
Leo
60
63
Knight
Time Magic
Faith Up
Short Status
Ignore Terrain

Defender
Buckler
Gold Helmet
Platinum Armor
108 Gems

Head Break, Weapon Break, Speed Break
Haste, Slow, Slow 2, Stop, Reflect

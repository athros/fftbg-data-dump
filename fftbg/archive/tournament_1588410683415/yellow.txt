Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Belife42
Female
Virgo
50
61
Mediator
Steal
Counter
Maintenance
Jump+1

Dagger

Golden Hairpin
Black Costume
Angel Ring

Invitation, Solution, Death Sentence, Refute
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory



KasugaiRoastedPeas
Male
Taurus
60
80
Archer
Time Magic
Counter
Doublehand
Ignore Height

Night Killer

Holy Miter
Power Sleeve
Rubber Shoes

Charge+2, Charge+4, Charge+7
Haste 2, Stop, Reflect, Quick, Stabilize Time, Meteor



Twelfthrootoftwo
Female
Aquarius
58
65
Calculator
Time Magic
Parry
Equip Sword
Jump+2

Koutetsu Knife

Holy Miter
Black Robe
Defense Ring

Height, Prime Number, 5, 4, 3
Haste, Slow, Immobilize, Reflect, Quick, Demi



Solomongrundy85
Female
Gemini
81
80
Time Mage
Draw Out
Critical Quick
Long Status
Waterwalking

White Staff

Black Hood
Wizard Robe
Germinas Boots

Haste, Slow, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time
Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji

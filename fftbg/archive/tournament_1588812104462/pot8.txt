Final Bets: green - 18 bets for 14,394G (41.3%, x1.42); champion - 29 bets for 20,424G (58.7%, x0.70)

green bets:
madming25: 3,333G (23.2%, 8,668G)
HaateXIII: 1,225G (8.5%, 2,402G)
Zeroroute: 1,196G (8.3%, 1,196G)
RunicMagus: 1,100G (7.6%, 52,006G)
FNCardascia_: 1,034G (7.2%, 1,034G)
JumbocactuarX27: 1,000G (6.9%, 6,109G)
Aldrammech: 1,000G (6.9%, 30,456G)
Baron_von_Scrub: 725G (5.0%, 725G)
dictatorhowells: 666G (4.6%, 17,166G)
joewcarson: 500G (3.5%, 6,530G)
ColetteMSLP: 500G (3.5%, 20,201G)
ZephyrTempest: 456G (3.2%, 1,579G)
luminarii: 440G (3.1%, 13,196G)
Cryptopsy70: 400G (2.8%, 13,437G)
caprinovoa: 319G (2.2%, 319G)
arch8000: 300G (2.1%, 5,246G)
maakur_: 100G (0.7%, 123,672G)
Firesheath: 100G (0.7%, 11,149G)

champion bets:
Mesmaster: 3,076G (15.1%, 6,152G)
ApplesNP: 3,000G (14.7%, 20,075G)
killth3kid: 2,000G (9.8%, 77,291G)
genericco: 1,958G (9.6%, 1,958G)
HaplessOne: 1,500G (7.3%, 31,485G)
Shalloween: 1,000G (4.9%, 18,716G)
EnemyController: 1,000G (4.9%, 192,349G)
BirbBrainsBot: 1,000G (4.9%, 58,615G)
Lionhermit: 1,000G (4.9%, 16,492G)
leakimiko: 858G (4.2%, 8,586G)
Evewho: 508G (2.5%, 508G)
nifboy: 508G (2.5%, 508G)
Breakdown777: 500G (2.4%, 18,917G)
Laserman1000: 404G (2.0%, 404G)
twelfthrootoftwo: 300G (1.5%, 2,186G)
SQUiDSQUARKLIN: 276G (1.4%, 276G)
evdoggity: 200G (1.0%, 6,622G)
getthemoneyz: 182G (0.9%, 630,578G)
Kooks_R_Us: 174G (0.9%, 174G)
Xenomorith: 120G (0.6%, 120G)
ungabunga_bot: 109G (0.5%, 379,800G)
dinin991: 101G (0.5%, 1,568G)
vampyreinabox: 100G (0.5%, 1,869G)
RestIessNight: 100G (0.5%, 592G)
Kyune: 100G (0.5%, 1,014G)
datadrivenbot: 100G (0.5%, 12,556G)
jwchapm: 100G (0.5%, 558G)
GiggleStik: 100G (0.5%, 6,439G)
b0shii: 50G (0.2%, 8,608G)

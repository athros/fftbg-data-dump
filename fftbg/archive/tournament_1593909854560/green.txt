Player: !Green
Team: Green Team
Palettes: Green/White



Reinoe
Male
Pisces
63
77
Oracle
Charge
Meatbone Slash
Concentrate
Ignore Terrain

Gokuu Rod

Golden Hairpin
Black Costume
Rubber Shoes

Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy
Charge+3, Charge+10



Nifboy
Female
Libra
79
53
Ninja
Time Magic
Counter Tackle
Concentrate
Jump+1

Spell Edge
Koga Knife
Feather Hat
Brigandine
Sprint Shoes

Shuriken, Bomb
Haste, Slow 2, Stop, Immobilize, Float, Reflect, Demi 2, Stabilize Time



Lydian C
Female
Scorpio
60
57
Samurai
Jump
Auto Potion
Equip Axe
Move+3

Healing Staff

Gold Helmet
Linen Cuirass
108 Gems

Koutetsu, Kiyomori, Muramasa
Level Jump4, Vertical Jump2



O Heyno
Male
Gemini
76
48
Priest
Talk Skill
Dragon Spirit
Concentrate
Waterwalking

Oak Staff

Red Hood
White Robe
Power Wrist

Cure, Cure 3, Cure 4, Reraise, Shell, Esuna, Holy
Invitation, Persuade, Preach, Mimic Daravon, Refute

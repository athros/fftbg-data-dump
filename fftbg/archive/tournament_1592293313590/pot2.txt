Final Bets: green - 6 bets for 5,152G (24.3%, x3.12); yellow - 13 bets for 16,084G (75.7%, x0.32)

green bets:
Mesmaster: 3,000G (58.2%, 78,825G)
BirbBrainsBot: 1,000G (19.4%, 39,086G)
DeathTaxesAndAnime: 636G (12.3%, 636G)
Zachara: 216G (4.2%, 107,716G)
sososteve: 200G (3.9%, 3,502G)
AllInBot: 100G (1.9%, 100G)

yellow bets:
BoneMiser: 6,402G (39.8%, 6,402G)
prince_rogers_nelson_: 5,000G (31.1%, 17,060G)
SkyridgeZero: 1,000G (6.2%, 31,977G)
Umbrellaexile: 780G (4.8%, 780G)
reinoe: 600G (3.7%, 600G)
vorap: 500G (3.1%, 152,937G)
WitchHunterIX: 500G (3.1%, 13,773G)
metagameface: 400G (2.5%, 400G)
DuraiPapers: 259G (1.6%, 259G)
Evewho: 200G (1.2%, 1,909G)
porkchop5158: 200G (1.2%, 3,782G)
AltimaMantoid: 143G (0.9%, 543G)
ko2q: 100G (0.6%, 15,271G)

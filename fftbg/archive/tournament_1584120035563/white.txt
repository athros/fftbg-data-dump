Player: !White
Team: White Team
Palettes: White/Blue



RoamingUragaan
Male
Gemini
46
77
Archer
Punch Art
Catch
Short Charge
Ignore Terrain

Gastrafitis
Platinum Shield
Green Beret
Judo Outfit
Spike Shoes

Charge+2, Charge+5, Charge+10
Purification, Revive



Bryon W
Male
Sagittarius
80
74
Time Mage
Talk Skill
Counter Flood
Monster Talk
Fly

White Staff

Triangle Hat
Silk Robe
Feather Mantle

Haste 2, Reflect, Demi, Stabilize Time
Negotiate, Mimic Daravon, Refute



RughSontos
Monster
Virgo
60
48
Chocobo










WalkerNash
Male
Gemini
70
59
Monk
Yin Yang Magic
Mana Shield
Equip Gun
Jump+2

Bloody Strings

Holy Miter
Chain Vest
Spike Shoes

Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Zombie, Foxbird, Dispel Magic

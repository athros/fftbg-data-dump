Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Gooseyourself
Female
Libra
60
61
Oracle
Elemental
Regenerator
Short Charge
Move+2

Battle Bamboo

Holy Miter
Mythril Vest
Wizard Mantle

Poison, Spell Absorb, Pray Faith, Foxbird, Paralyze, Petrify
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



ApplesauceBoss
Female
Cancer
73
64
Calculator
Robosnake Skill
Faith Up
Sicken
Move-HP Up

Octagon Rod
Bronze Shield
Grand Helmet
Carabini Mail
N-Kai Armlet

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Volgrathemoose
Female
Pisces
56
50
Summoner
Throw
Arrow Guard
Short Charge
Waterbreathing

Battle Folio

Twist Headband
Linen Robe
108 Gems

Moogle, Shiva, Titan, Carbunkle, Odin, Leviathan, Fairy, Lich
Sword, Wand



Baron Von Scrub
Male
Gemini
80
43
Monk
Time Magic
Mana Shield
Long Status
Swim



Green Beret
Judo Outfit
Bracer

Pummel, Wave Fist, Secret Fist, Purification, Revive
Haste, Slow, Demi, Stabilize Time, Meteor

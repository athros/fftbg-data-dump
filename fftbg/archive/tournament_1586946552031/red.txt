Player: !Red
Team: Red Team
Palettes: Red/Brown



Treapvort
Male
Taurus
51
64
Lancer
Sing
Mana Shield
Equip Gun
Waterbreathing

Bestiary
Gold Shield
Cross Helmet
Linen Robe
Sprint Shoes

Level Jump3, Vertical Jump7
Life Song, Cheer Song, Sky Demon



Mayormcfunbags
Male
Libra
63
46
Thief
Charge
Faith Up
Short Status
Swim

Mage Masher

Golden Hairpin
Brigandine
Spike Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Status, Leg Aim
Charge+7



Waterwatereverywhere
Female
Leo
53
63
Wizard
Jump
Arrow Guard
Short Charge
Teleport

Thunder Rod

Headgear
Linen Robe
Germinas Boots

Fire, Fire 3, Bolt, Bolt 2, Ice 2, Ice 3, Ice 4, Frog
Level Jump2, Vertical Jump6



Laserman1000
Male
Scorpio
41
63
Squire
Steal
Caution
Long Status
Waterwalking

Iron Sword
Platinum Shield
Leather Hat
Diamond Armor
108 Gems

Accumulate, Tickle
Gil Taking, Steal Heart

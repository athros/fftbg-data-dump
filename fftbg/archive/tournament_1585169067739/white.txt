Player: !White
Team: White Team
Palettes: White/Blue



Zange1
Female
Aries
46
64
Squire
Yin Yang Magic
Meatbone Slash
Short Status
Jump+3

Flame Whip
Genji Shield
Flash Hat
Judo Outfit
Diamond Armlet

Accumulate, Dash
Zombie, Confusion Song, Paralyze, Sleep



The Pengwin
Female
Aries
64
77
Chemist
Elemental
Damage Split
Secret Hunt
Ignore Terrain

Cute Bag

Feather Hat
Black Costume
Wizard Mantle

Hi-Potion, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Technominari
Female
Capricorn
57
54
Priest
Yin Yang Magic
Caution
Equip Gun
Ignore Height

Stone Gun

Holy Miter
Black Robe
Red Shoes

Cure 3, Cure 4, Raise 2, Protect, Protect 2, Wall, Esuna
Pray Faith, Zombie, Dispel Magic, Petrify, Dark Holy



Zeroroute
Male
Aquarius
43
80
Samurai
Yin Yang Magic
Absorb Used MP
Long Status
Ignore Height

Murasame

Grand Helmet
Black Robe
Jade Armlet

Asura, Murasame, Kiyomori
Poison, Doubt Faith, Dispel Magic, Paralyze

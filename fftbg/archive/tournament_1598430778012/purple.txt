Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mesmaster
Female
Libra
53
73
Samurai
Item
Earplug
Equip Bow
Waterwalking

Night Killer

Mythril Helmet
Wizard Robe
Elf Mantle

Murasame, Kiyomori, Muramasa, Kikuichimoji
Hi-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down



LeoNightFury
Female
Capricorn
61
52
Ninja
Charge
Meatbone Slash
Magic Defense UP
Lava Walking

Short Edge
Mythril Knife
Triangle Hat
Leather Outfit
Dracula Mantle

Shuriken, Dictionary
Charge+2, Charge+4, Charge+10



Chuckolator
Male
Sagittarius
56
53
Samurai
Charge
Meatbone Slash
Equip Sword
Fly

Defender

Leather Helmet
Genji Armor
Genji Gauntlet

Asura, Koutetsu, Bizen Boat
Charge+5



Jessika
Female
Libra
59
40
Time Mage
Talk Skill
Faith Save
Monster Talk
Move-HP Up

Rainbow Staff

Flash Hat
Robe of Lords
Small Mantle

Quick, Stabilize Time, Meteor
Praise, Death Sentence, Mimic Daravon, Refute

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



GiggleStik
Female
Cancer
57
47
Wizard
Jump
Parry
Martial Arts
Levitate



Flash Hat
Linen Robe
Bracer

Fire, Fire 2, Fire 4, Bolt, Bolt 3, Ice, Ice 3, Flare
Level Jump8, Vertical Jump4



XalSmellsBad
Male
Libra
45
52
Knight
Punch Art
Parry
Martial Arts
Fly

Ancient Sword
Escutcheon
Bronze Helmet
Linen Cuirass
Rubber Shoes

Head Break, Armor Break, Speed Break, Power Break
Pummel, Chakra, Seal Evil



BuffaloCrunch
Female
Scorpio
66
61
Geomancer
Yin Yang Magic
Counter Flood
Defense UP
Lava Walking

Platinum Sword
Ice Shield
Feather Hat
Light Robe
Rubber Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Solomongrundy85
Female
Serpentarius
45
39
Time Mage
Draw Out
MA Save
Equip Gun
Move-HP Up

Ramia Harp

Golden Hairpin
Silk Robe
Cursed Ring

Haste 2, Stop, Float, Reflect, Quick, Demi
Koutetsu, Kiyomori

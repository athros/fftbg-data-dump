Final Bets: black - 9 bets for 5,333G (48.2%, x1.08); purple - 10 bets for 5,734G (51.8%, x0.93)

black bets:
prince_rogers_nelson_: 1,094G (20.5%, 2,188G)
reinoe: 1,000G (18.8%, 23,194G)
bruubarg: 1,000G (18.8%, 9,195G)
Mesmaster: 1,000G (18.8%, 28,977G)
Evewho: 652G (12.2%, 652G)
AllInBot: 287G (5.4%, 287G)
rednecknazgul: 100G (1.9%, 2,216G)
fluffskull: 100G (1.9%, 1,401G)
Ring_Wyrm: 100G (1.9%, 2,190G)

purple bets:
serperemagus: 1,718G (30.0%, 1,718G)
NovaKnight21: 1,240G (21.6%, 1,240G)
BirbBrainsBot: 1,000G (17.4%, 104,364G)
Lydian_C: 731G (12.7%, 1,434G)
ColetteMSLP: 300G (5.2%, 933G)
AkiriXx: 204G (3.6%, 204G)
gorgewall: 201G (3.5%, 14,940G)
getthemoneyz: 140G (2.4%, 903,678G)
lastly: 100G (1.7%, 19,700G)
datadrivenbot: 100G (1.7%, 36,464G)

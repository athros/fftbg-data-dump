Player: !Red
Team: Red Team
Palettes: Red/Brown



Bahumat989
Monster
Cancer
40
39
Holy Dragon










Solomongrundy85
Male
Sagittarius
68
46
Bard
Steal
Counter
Dual Wield
Retreat

Bloody Strings
Ramia Harp
Flash Hat
Leather Outfit
Germinas Boots

Battle Song, Magic Song, Diamond Blade
Gil Taking, Steal Armor



L2 Sentinel
Male
Sagittarius
63
58
Monk
Sing
Regenerator
Dual Wield
Retreat



Flash Hat
Adaman Vest
Bracer

Spin Fist, Pummel, Purification, Revive
Angel Song, Cheer Song, Battle Song, Diamond Blade



Digitalsocrates
Female
Sagittarius
42
65
Mediator
Steal
Meatbone Slash
Attack UP
Retreat

Bestiary

Leather Hat
Secret Clothes
Magic Gauntlet

Persuade, Mimic Daravon
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory

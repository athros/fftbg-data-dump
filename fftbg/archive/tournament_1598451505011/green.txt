Player: !Green
Team: Green Team
Palettes: Green/White



Lyner87
Male
Gemini
50
76
Oracle
Talk Skill
Earplug
Equip Gun
Ignore Height

Battle Folio

Thief Hat
Clothes
Spike Shoes

Poison, Spell Absorb, Pray Faith, Silence Song, Foxbird, Dispel Magic, Paralyze, Dark Holy
Invitation, Threaten, Solution, Negotiate



Sairentozon7
Female
Aquarius
64
49
Calculator
Curse Skill
Brave Save
Doublehand
Jump+2

Ice Rod

Cross Helmet
Mythril Armor
Defense Armlet

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Error72
Male
Gemini
63
79
Lancer
Steal
Parry
Defend
Waterbreathing

Obelisk
Bronze Shield
Genji Helmet
Plate Mail
Sprint Shoes

Level Jump5, Vertical Jump5
Steal Armor, Steal Weapon, Steal Accessory



Just Here2
Female
Capricorn
45
69
Ninja
Draw Out
Parry
Equip Gun
Move-MP Up

Romanda Gun
Blast Gun
Red Hood
Chain Vest
Angel Ring

Shuriken, Sword
Bizen Boat, Muramasa

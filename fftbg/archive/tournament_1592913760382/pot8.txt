Final Bets: brown - 7 bets for 11,531G (53.1%, x0.88); champion - 12 bets for 10,184G (46.9%, x1.13)

brown bets:
E_Ballard: 7,909G (68.6%, 7,909G)
prince_rogers_nelson_: 1,151G (10.0%, 1,151G)
extinctrational: 909G (7.9%, 909G)
Hasterious: 762G (6.6%, 76,216G)
Shinzutalos: 500G (4.3%, 2,573G)
ZCKaiser: 200G (1.7%, 10,052G)
FriendSquirrel: 100G (0.9%, 6,472G)

champion bets:
DuraiPapers: 3,000G (29.5%, 30,440G)
JumbocactuarX27: 2,000G (19.6%, 6,824G)
getthemoneyz: 1,000G (9.8%, 1,010,509G)
BirbBrainsBot: 1,000G (9.8%, 75,457G)
Zalerah: 844G (8.3%, 844G)
ForestMagi: 840G (8.2%, 840G)
run_with_stone_GUNs: 500G (4.9%, 34,242G)
twelfthrootoftwo: 500G (4.9%, 4,270G)
Evewho: 200G (2.0%, 4,315G)
RunicMagus: 100G (1.0%, 43,731G)
datadrivenbot: 100G (1.0%, 49,006G)
kliffw: 100G (1.0%, 8,882G)

Player: !Green
Team: Green Team
Palettes: Green/White



Breakdown777
Male
Taurus
72
56
Chemist
Battle Skill
Catch
Equip Polearm
Jump+3

Mythril Spear

Red Hood
Leather Outfit
Defense Ring

Potion, Eye Drop, Soft, Phoenix Down
Shield Break, Speed Break, Mind Break, Justice Sword, Night Sword



PoroTact
Monster
Sagittarius
55
66
Ochu










CassiePhoenix
Male
Taurus
62
59
Knight
Draw Out
Regenerator
Equip Polearm
Retreat

Javelin
Platinum Shield
Crystal Helmet
Reflect Mail
Reflect Ring

Armor Break, Shield Break, Magic Break, Power Break, Justice Sword
Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji



UmaiJam
Female
Pisces
64
46
Wizard
Battle Skill
Speed Save
Equip Axe
Lava Walking

Battle Axe

Twist Headband
Clothes
Bracer

Fire 2, Fire 4, Bolt, Bolt 2, Ice, Death, Flare
Head Break, Weapon Break, Mind Break, Justice Sword

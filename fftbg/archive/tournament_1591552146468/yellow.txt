Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BruddaShrine
Male
Aquarius
77
45
Mediator
Yin Yang Magic
Damage Split
Sicken
Levitate

Bestiary

Red Hood
Leather Outfit
Defense Ring

Praise, Preach, Negotiate, Mimic Daravon, Rehabilitate
Life Drain, Pray Faith, Doubt Faith, Zombie, Dispel Magic, Paralyze



Kohlingen
Male
Gemini
70
67
Oracle
Time Magic
Speed Save
Sicken
Retreat

Cypress Rod

Red Hood
Light Robe
Battle Boots

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Foxbird, Sleep
Haste, Haste 2, Slow, Float, Demi 2, Stabilize Time



Laserman1000
Female
Leo
54
61
Wizard
Punch Art
MP Restore
Short Charge
Jump+1

Main Gauche

Red Hood
Wizard Robe
Jade Armlet

Fire 2, Fire 3, Bolt, Bolt 2, Ice, Ice 2
Purification, Revive, Seal Evil



Aldrammech
Male
Capricorn
58
76
Time Mage
Yin Yang Magic
Counter
Magic Defense UP
Move+3

Wizard Staff

Golden Hairpin
Silk Robe
Diamond Armlet

Slow, Immobilize, Reflect, Quick, Demi
Poison, Spell Absorb, Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Paralyze

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SQUiDSQUARKLIN
Male
Leo
58
49
Samurai
Summon Magic
Critical Quick
Secret Hunt
Swim

Holy Lance

Leather Helmet
Platinum Armor
Genji Gauntlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Moogle, Bahamut, Odin, Leviathan



Lawnboxer
Female
Capricorn
65
50
Squire
Battle Skill
Meatbone Slash
Doublehand
Move-HP Up

Assassin Dagger

Headgear
Power Sleeve
108 Gems

Accumulate, Dash, Tickle, Fury
Head Break, Speed Break, Justice Sword



Fenixcrest
Female
Gemini
44
67
Knight
Talk Skill
HP Restore
Equip Gun
Fly

Madlemgen
Flame Shield
Cross Helmet
Crystal Mail
Magic Gauntlet

Head Break, Weapon Break, Power Break, Surging Sword, Explosion Sword
Invitation, Threaten, Preach, Negotiate, Mimic Daravon



Reinoe
Male
Sagittarius
53
72
Bard
Throw
Counter
Maintenance
Lava Walking

Bloody Strings

Thief Hat
Mystic Vest
Small Mantle

Angel Song, Last Song, Space Storage
Shuriken, Bomb

Player: !Red
Team: Red Team
Palettes: Red/Brown



TheNGUYENNER
Male
Aquarius
57
65
Thief
Draw Out
Counter Tackle
Magic Defense UP
Move+1

Hidden Knife

Headgear
Black Costume
Defense Ring

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Koutetsu, Heaven's Cloud, Muramasa



Sinnyil2
Male
Leo
68
74
Mime

Abandon
Equip Shield
Lava Walking


Flame Shield
Leather Hat
Wizard Outfit
Reflect Ring

Mimic




Twelfthrootoftwo
Female
Libra
51
43
Wizard
Summon Magic
Faith Up
Equip Sword
Ignore Height

Chaos Blade

Black Hood
Light Robe
Leather Mantle

Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 4, Death
Moogle, Shiva, Titan, Leviathan, Silf, Lich



Evewho
Female
Cancer
61
80
Archer
Draw Out
Meatbone Slash
Defend
Jump+3

Mythril Bow

Twist Headband
Judo Outfit
Defense Ring

Charge+5, Charge+7
Koutetsu, Murasame, Kiyomori

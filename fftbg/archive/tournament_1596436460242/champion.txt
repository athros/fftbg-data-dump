Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Phi Sig
Male
Taurus
45
80
Squire
White Magic
Meatbone Slash
Defend
Levitate

Diamond Sword
Mythril Shield
Red Hood
Diamond Armor
Red Shoes

Throw Stone, Heal, Yell, Fury, Scream
Cure 2, Raise, Raise 2, Reraise, Protect 2, Shell, Shell 2, Esuna



Lydian C
Female
Scorpio
69
73
Knight
Jump
HP Restore
Magic Attack UP
Waterwalking

Defender
Round Shield
Genji Helmet
Plate Mail
Setiemson

Head Break, Armor Break, Shield Break, Weapon Break
Level Jump3, Vertical Jump7



Oreo Pizza
Female
Sagittarius
74
59
Geomancer
Item
Counter Magic
Equip Gun
Fly

Papyrus Codex
Hero Shield
Green Beret
Black Robe
Leather Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



G1nger4le
Monster
Leo
80
76
Cockatrice








Player: !Green
Team: Green Team
Palettes: Green/White



BenYuPoker
Female
Leo
64
53
Wizard
Draw Out
Meatbone Slash
Secret Hunt
Move+3

Ice Rod

Feather Hat
White Robe
Wizard Mantle

Bolt 2, Bolt 3, Ice, Ice 3, Ice 4
Koutetsu, Bizen Boat, Murasame, Kiyomori, Masamune



Lyner87
Male
Libra
67
40
Geomancer
Black Magic
Critical Quick
Magic Attack UP
Jump+2

Heaven's Cloud
Bronze Shield
Triangle Hat
Black Robe
Reflect Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire 2, Ice 3, Frog



Aldrammech
Monster
Sagittarius
77
62
Porky










Reinoe
Male
Sagittarius
52
48
Monk
Sing
MA Save
Sicken
Jump+3



Leather Hat
Wizard Outfit
Defense Armlet

Spin Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Angel Song, Diamond Blade, Sky Demon, Hydra Pit

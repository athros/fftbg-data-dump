Player: !White
Team: White Team
Palettes: White/Blue



Powerpinch
Male
Taurus
42
72
Time Mage
Yin Yang Magic
Arrow Guard
Equip Gun
Move-MP Up

Mythril Gun

Headgear
Linen Robe
Reflect Ring

Haste, Slow, Immobilize, Reflect, Demi, Stabilize Time
Pray Faith, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify



Azelgrim
Male
Taurus
60
67
Bard
Elemental
Counter
Equip Shield
Teleport

Bloody Strings
Ice Shield
Headgear
Linen Cuirass
Genji Gauntlet

Angel Song, Life Song, Cheer Song
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Benticore
Male
Leo
66
47
Oracle
Talk Skill
Auto Potion
Martial Arts
Ignore Height



Green Beret
Wizard Robe
Magic Ring

Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy
Invitation, Persuade, Threaten, Preach, Death Sentence, Refute



Nhammen
Female
Sagittarius
73
78
Squire
Steal
Hamedo
Sicken
Move-MP Up

Flame Whip
Genji Shield
Mythril Helmet
Leather Outfit
Genji Gauntlet

Heal, Yell, Fury
Gil Taking, Steal Helmet, Steal Armor

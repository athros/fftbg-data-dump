Final Bets: purple - 10 bets for 5,650G (52.9%, x0.89); brown - 9 bets for 5,030G (47.1%, x1.12)

purple bets:
CassiePhoenix: 2,191G (38.8%, 2,191G)
rechaun: 1,142G (20.2%, 1,142G)
leakimiko: 671G (11.9%, 4,474G)
BirbBrainsBot: 626G (11.1%, 147,080G)
RubenFlonne: 500G (8.8%, 22,742G)
nifboy: 145G (2.6%, 3,638G)
ungabunga_bot: 125G (2.2%, 221,963G)
lastly: 100G (1.8%, 3,576G)
datadrivenbot: 100G (1.8%, 2,459G)
jtbirdACC: 50G (0.9%, 844G)

brown bets:
Willjin: 1,500G (29.8%, 12,982G)
HaplessOne: 1,111G (22.1%, 143,004G)
getthemoneyz: 800G (15.9%, 567,861G)
aStatue: 600G (11.9%, 600G)
megaman2202: 583G (11.6%, 583G)
perce90: 200G (4.0%, 1,601G)
Namillus: 104G (2.1%, 1,876G)
roqqqpsi: 82G (1.6%, 4,132G)
DrAntiSocial: 50G (1.0%, 26,764G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Male
Libra
43
54
Mime

MA Save
Defense UP
Jump+3



Thief Hat
Mythril Armor
Sprint Shoes

Mimic




Rechaun
Female
Cancer
56
74
Wizard
Basic Skill
Counter Tackle
Equip Sword
Move+2

Iron Sword

Flash Hat
Linen Robe
Defense Ring

Fire 3, Bolt 2, Ice 4
Dash, Heal



Mikumari12
Female
Libra
55
81
Time Mage
Yin Yang Magic
Critical Quick
Beastmaster
Swim

Bestiary

Twist Headband
White Robe
Diamond Armlet

Slow, Slow 2, Stop, Immobilize, Quick, Stabilize Time
Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird



BoneMiser
Male
Virgo
66
38
Knight
Draw Out
Parry
Defend
Ignore Height

Long Sword
Ice Shield
Grand Helmet
Genji Armor
Dracula Mantle

Head Break, Armor Break, Shield Break, Power Break, Stasis Sword
Bizen Boat, Kiyomori, Muramasa, Masamune

Final Bets: red - 11 bets for 3,170G (40.9%, x1.44); blue - 8 bets for 4,576G (59.1%, x0.69)

red bets:
Evewho: 1,000G (31.5%, 17,930G)
Laserman1000: 605G (19.1%, 6,805G)
foofermoofer: 280G (8.8%, 280G)
reddwind_: 278G (8.8%, 556G)
nekojin: 264G (8.3%, 264G)
CorpusCav: 200G (6.3%, 1,009G)
HawkSick: 200G (6.3%, 3,467G)
Lydian_C: 120G (3.8%, 22,792G)
cheesy_e: 100G (3.2%, 163G)
getthemoneyz: 66G (2.1%, 992,552G)
BirbBrainsBot: 57G (1.8%, 117,325G)

blue bets:
solomongrundy85: 1,413G (30.9%, 1,413G)
Digitalsocrates: 1,000G (21.9%, 44,810G)
prince_rogers_nelson_: 863G (18.9%, 863G)
twelfthrootoftwo: 300G (6.6%, 2,573G)
notsashasia: 250G (5.5%, 1,911G)
Lythe_Caraker: 250G (5.5%, 108,598G)
HaateXIII: 250G (5.5%, 3,561G)
DeathTaxesAndAnime: 250G (5.5%, 3,549G)

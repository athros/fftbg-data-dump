Player: !Green
Team: Green Team
Palettes: Green/White



NWOW 44
Female
Capricorn
80
39
Geomancer
Draw Out
Parry
Doublehand
Ignore Terrain

Slasher

Feather Hat
Linen Robe
Feather Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa



MagicBottle
Male
Pisces
48
68
Knight
Item
Speed Save
Dual Wield
Ignore Height

Battle Axe
Slasher
Diamond Helmet
Gold Armor
Sprint Shoes

Stasis Sword
Potion, Ether, Hi-Ether, Soft, Phoenix Down



VolgraTheMoose
Male
Gemini
61
80
Knight
Time Magic
Distribute
Concentrate
Move-MP Up

Battle Axe
Platinum Shield
Gold Helmet
Diamond Armor
Magic Gauntlet

Shield Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Dark Sword
Haste 2, Slow 2, Immobilize, Stabilize Time, Meteor



Calajo
Female
Leo
69
51
Geomancer
Basic Skill
HP Restore
Equip Sword
Ignore Height

Iron Sword
Platinum Shield
Flash Hat
White Robe
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Dash, Throw Stone, Heal, Cheer Up, Wish

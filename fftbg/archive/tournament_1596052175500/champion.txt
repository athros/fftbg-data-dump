Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



HolyDragoonXIV
Male
Libra
76
57
Monk
Summon Magic
MA Save
Maintenance
Retreat



Thief Hat
Clothes
Jade Armlet

Purification, Chakra, Revive
Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Salamander



SeniorBunk
Female
Leo
61
61
Monk
Steal
Earplug
Doublehand
Ignore Terrain



Red Hood
Secret Clothes
Elf Mantle

Pummel, Earth Slash, Secret Fist, Purification, Seal Evil
Steal Heart, Steal Armor, Steal Shield, Steal Weapon



Nhammen
Female
Sagittarius
70
57
Summoner
Basic Skill
Counter Magic
Short Charge
Ignore Height

Bestiary

Red Hood
Light Robe
Diamond Armlet

Moogle, Shiva, Ramuh, Ifrit, Titan
Accumulate, Heal, Cheer Up



Deathmaker06
Male
Gemini
76
64
Lancer
Punch Art
Earplug
Dual Wield
Levitate

Holy Lance
Mythril Spear
Mythril Helmet
Bronze Armor
Leather Mantle

Level Jump8, Vertical Jump6
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil

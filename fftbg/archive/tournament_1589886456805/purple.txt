Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Aneyus
Male
Aquarius
72
55
Oracle
Sing
Catch
Defend
Move-HP Up

Bestiary

Leather Hat
Power Sleeve
N-Kai Armlet

Spell Absorb, Blind Rage, Dispel Magic, Sleep
Cheer Song, Hydra Pit



ExecutedGiraffe
Male
Aries
51
54
Mime

Speed Save
Magic Defense UP
Move+1



Leather Hat
Judo Outfit
Bracer

Mimic




Powergems
Female
Sagittarius
79
51
Mime

Earplug
Doublehand
Jump+1



Flash Hat
Power Sleeve
Cherche

Mimic




ScurvyMitch
Female
Leo
72
44
Archer
Summon Magic
Caution
Defense UP
Fly

Ultimus Bow

Red Hood
Earth Clothes
Defense Ring

Charge+1, Charge+3, Charge+5, Charge+7
Moogle, Shiva, Carbunkle, Leviathan, Salamander, Fairy

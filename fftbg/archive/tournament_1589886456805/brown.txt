Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HASTERIOUS
Male
Aquarius
60
43
Lancer
Sing
PA Save
Equip Polearm
Teleport

Ryozan Silk
Round Shield
Gold Helmet
Genji Armor
Feather Boots

Level Jump4, Vertical Jump8
Life Song, Cheer Song, Hydra Pit



CosmicTactician
Male
Capricorn
75
70
Time Mage
Punch Art
Arrow Guard
Short Charge
Ignore Terrain

Wizard Staff

Golden Hairpin
Silk Robe
Diamond Armlet

Haste, Slow, Quick, Stabilize Time, Meteor
Secret Fist, Purification, Revive



Zeando
Female
Aries
74
77
Wizard
Summon Magic
Arrow Guard
Short Status
Fly

Flame Rod

Feather Hat
Chameleon Robe
108 Gems

Bolt, Bolt 2, Ice 3
Ramuh, Ifrit, Titan, Carbunkle



Mesmaster
Female
Virgo
49
62
Summoner
Item
MA Save
Short Charge
Swim

Wizard Staff

Green Beret
White Robe
Diamond Armlet

Moogle, Ramuh, Golem, Salamander, Silf, Fairy
Potion, Hi-Potion, Antidote, Echo Grass, Remedy, Phoenix Down

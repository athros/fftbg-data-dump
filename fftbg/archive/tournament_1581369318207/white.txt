Player: !White
Team: White Team
Palettes: White/Blue



Haaaaaank
Female
Taurus
45
61
Wizard
Math Skill
Catch
Defense UP
Jump+1

Panther Bag

Twist Headband
Light Robe
Defense Ring

Fire 4, Bolt 2, Bolt 3, Ice 4, Empower
CT, Height, Prime Number, 4, 3



MrJamDango
Female
Scorpio
40
67
Lancer
White Magic
Catch
Dual Wield
Levitate

Javelin
Dragon Whisker
Gold Helmet
Chameleon Robe
Power Wrist

Level Jump8, Vertical Jump8
Cure 4, Raise 2, Reraise, Protect



Gandrin
Male
Scorpio
64
65
Archer
Jump
Abandon
Dual Wield
Move+3

Mythril Bow

Golden Hairpin
Mythril Vest
Chantage

Charge+2, Charge+3, Charge+4, Charge+7
Level Jump4, Vertical Jump2



AzuriteReaction
Male
Virgo
49
80
Samurai
Yin Yang Magic
MP Restore
Equip Axe
Move+3

Flail

Platinum Helmet
Chameleon Robe
Leather Mantle

Koutetsu, Bizen Boat, Murasame
Pray Faith, Doubt Faith, Zombie, Blind Rage, Paralyze

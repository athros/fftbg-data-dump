Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Xenomorith
Male
Aquarius
65
71
Wizard
Steal
Parry
Equip Axe
Teleport 2

Battle Axe

Thief Hat
Secret Clothes
Bracer

Fire 2, Fire 3, Bolt 3, Ice 2, Ice 3, Death
Gil Taking, Steal Helmet



Denamda
Male
Aquarius
77
69
Lancer
Charge
Distribute
Defense UP
Move-HP Up

Octagon Rod
Aegis Shield
Bronze Helmet
Silk Robe
Leather Mantle

Level Jump4, Vertical Jump2
Charge+1, Charge+2, Charge+3



Darikis
Male
Capricorn
53
67
Chemist
Talk Skill
Meatbone Slash
Magic Defense UP
Retreat

Star Bag

Headgear
Rubber Costume
108 Gems

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Remedy, Phoenix Down
Death Sentence, Refute



Kamikaze Plague
Female
Aries
43
76
Calculator
Limit
Abandon
Dual Wield
Lava Walking

Dragon Rod
Faith Rod
Platinum Helmet
Wizard Robe
Germinas Boots

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

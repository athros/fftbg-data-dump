Player: !Brown
Team: Brown Team
Palettes: Brown/Green



LDSkinny
Female
Cancer
66
52
Oracle
Black Magic
Parry
Magic Attack UP
Levitate

Battle Folio

Green Beret
Leather Outfit
Sprint Shoes

Blind, Poison, Pray Faith, Zombie, Confusion Song, Sleep
Fire, Bolt 4, Ice, Ice 2, Frog, Death



HASTERIOUS
Female
Capricorn
69
63
Priest
Battle Skill
Critical Quick
Short Status
Ignore Height

Gold Staff

Holy Miter
Wizard Robe
Defense Armlet

Cure 2, Protect, Shell 2
Armor Break, Mind Break, Night Sword



FoeSquirrel
Male
Scorpio
76
46
Priest
Charge
Counter Magic
Equip Polearm
Move-MP Up

Holy Lance

Red Hood
Mythril Vest
Reflect Ring

Cure 4, Raise, Raise 2, Reraise, Regen
Charge+1, Charge+3



Marrrrrrrrrrrrrrrrrrrr
Female
Scorpio
74
54
Chemist
Battle Skill
Absorb Used MP
Beastmaster
Levitate

Stone Gun

Green Beret
Judo Outfit
Cursed Ring

Potion, Ether, Elixir, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DrAntiSocial
Female
Libra
71
43
Archer
Black Magic
Distribute
Equip Armor
Move+3

Bow Gun
Crystal Shield
Platinum Helmet
Light Robe
N-Kai Armlet

Charge+2, Charge+7
Fire, Bolt, Bolt 3, Ice 2, Ice 3, Death



Joewcarson
Female
Pisces
46
45
Summoner
White Magic
Arrow Guard
Equip Polearm
Move-MP Up

Javelin

Twist Headband
Mythril Vest
Angel Ring

Moogle, Shiva, Carbunkle, Odin, Salamander, Silf, Lich, Cyclops
Cure, Cure 3, Cure 4, Raise 2, Reraise, Esuna



Lastly
Female
Gemini
70
49
Wizard
Charge
PA Save
Short Status
Jump+1

Poison Rod

Red Hood
Adaman Vest
Battle Boots

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower
Charge+2, Charge+4, Charge+5, Charge+20



OneHundredFists
Female
Aquarius
62
47
Monk
Steal
PA Save
Short Status
Move+3



Leather Hat
Leather Outfit
Feather Boots

Purification, Chakra, Revive
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield

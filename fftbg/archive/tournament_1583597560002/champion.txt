Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Numbersborne
Female
Libra
79
41
Dancer
Basic Skill
Counter Magic
Equip Shield
Move-HP Up

Ryozan Silk
Bronze Shield
Leather Hat
Black Robe
Jade Armlet

Last Dance
Throw Stone, Yell



Maeveen
Female
Taurus
46
60
Geomancer
Draw Out
Damage Split
Dual Wield
Move+3

Blood Sword
Iron Sword
Red Hood
Wizard Robe
Feather Mantle

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Kikuichimoji



ThanatosXRagnarok
Female
Aries
64
75
Chemist
Battle Skill
Regenerator
Martial Arts
Waterbreathing



Twist Headband
Chain Vest
Power Wrist

Potion, Hi-Potion, Hi-Ether, Eye Drop, Phoenix Down
Head Break, Armor Break, Shield Break, Stasis Sword, Dark Sword



Volgrathemoose
Male
Aries
54
43
Lancer
Item
Catch
Doublehand
Move+2

Spear

Mythril Helmet
Mythril Armor
Leather Mantle

Level Jump8, Vertical Jump6
Potion, Hi-Potion, Hi-Ether, Antidote, Phoenix Down

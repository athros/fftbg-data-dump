Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Phi Sig
Male
Cancer
64
65
Archer
Talk Skill
Mana Shield
Doublehand
Lava Walking

Blast Gun

Holy Miter
Earth Clothes
Feather Boots

Charge+1, Charge+7
Invitation, Persuade, Praise, Threaten, Solution, Refute, Rehabilitate



Lord Gwarth
Male
Sagittarius
55
52
Ninja
Yin Yang Magic
HP Restore
Equip Gun
Jump+3

Papyrus Codex
Battle Folio
Feather Hat
Chain Vest
Diamond Armlet

Shuriken, Staff, Axe
Life Drain, Pray Faith, Zombie, Blind Rage, Paralyze, Sleep



Reddwind
Male
Capricorn
46
64
Knight
Draw Out
Faith Save
Concentrate
Move+2

Excalibur
Aegis Shield
Crystal Helmet
Plate Mail
Small Mantle

Magic Break, Stasis Sword, Dark Sword, Night Sword
Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



JonnyCue
Male
Capricorn
75
73
Priest
Charge
MP Restore
Doublehand
Move+2

Flame Whip

Golden Hairpin
Light Robe
Dracula Mantle

Cure 2, Raise 2, Protect, Shell, Wall, Esuna
Charge+2, Charge+3, Charge+5

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Blahz0r
Male
Aquarius
51
60
Monk
Basic Skill
PA Save
Equip Axe
Jump+1

Wizard Staff

Ribbon
Brigandine
Small Mantle

Pummel, Secret Fist, Chakra, Revive, Seal Evil
Dash, Heal, Tickle, Wish



Perkisle
Male
Virgo
78
43
Ninja
Elemental
Absorb Used MP
Attack UP
Lava Walking

Hidden Knife
Dagger
Holy Miter
Mystic Vest
Magic Ring

Shuriken, Bomb, Stick
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



JoeykinsGaming
Male
Capricorn
62
53
Thief
Black Magic
MP Restore
Equip Knife
Jump+3

Hidden Knife

Black Hood
Rubber Costume
Bracer

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Fire 3, Bolt 4, Ice 2, Ice 3, Death, Flare



Eryzion
Female
Serpentarius
79
74
Chemist
Yin Yang Magic
Absorb Used MP
Equip Sword
Fly

Chaos Blade

Headgear
Black Costume
Elf Mantle

Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Phoenix Down
Blind, Confusion Song

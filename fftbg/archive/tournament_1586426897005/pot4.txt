Final Bets: purple - 9 bets for 8,762G (57.9%, x0.73); brown - 10 bets for 6,361G (42.1%, x1.38)

purple bets:
eltejongrande: 3,000G (34.2%, 18,315G)
sinnyil2: 2,809G (32.1%, 5,619G)
JustSuperish: 606G (6.9%, 606G)
NovaKnight21: 500G (5.7%, 5,447G)
kingchadking: 468G (5.3%, 468G)
upvla: 461G (5.3%, 1,172G)
ColetteMSLP: 400G (4.6%, 12,703G)
mayormcfunbags: 296G (3.4%, 296G)
roqqqpsi: 222G (2.5%, 1,933G)

brown bets:
leakimiko: 1,311G (20.6%, 26,231G)
BirbBrainsBot: 1,000G (15.7%, 294,756G)
getthemoneyz: 1,000G (15.7%, 447,209G)
WireLord: 1,000G (15.7%, 26,097G)
SeedSC: 773G (12.2%, 181,273G)
LAGBOT30000: 501G (7.9%, 10,035G)
DHaveWord: 360G (5.7%, 360G)
KasugaiRoastedPeas: 200G (3.1%, 12,293G)
Kalcurz: 116G (1.8%, 116G)
AllInBot: 100G (1.6%, 100G)

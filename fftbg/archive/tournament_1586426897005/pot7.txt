Final Bets: yellow - 10 bets for 11,894G (49.9%, x1.01); black - 13 bets for 11,965G (50.1%, x0.99)

yellow bets:
rechaun: 6,255G (52.6%, 6,255G)
kingchadking: 2,550G (21.4%, 2,550G)
WireLord: 1,000G (8.4%, 31,062G)
Rhuarc_Redhammer: 500G (4.2%, 762G)
HASTERIOUS: 480G (4.0%, 942G)
JustSuperish: 327G (2.7%, 327G)
waterwatereverywhere: 308G (2.6%, 308G)
roqqqpsi: 222G (1.9%, 1,667G)
AllInBot: 126G (1.1%, 126G)
NineNoeru: 126G (1.1%, 633G)

black bets:
eltejongrande: 5,000G (41.8%, 16,604G)
SeedSC: 1,500G (12.5%, 180,504G)
KasugaiRoastedPeas: 1,234G (10.3%, 13,234G)
BirbBrainsBot: 1,000G (8.4%, 294,133G)
CCRUNNER149UWP: 888G (7.4%, 4,066G)
sinnyil2: 639G (5.3%, 639G)
NovaKnight21: 600G (5.0%, 4,947G)
getthemoneyz: 212G (1.8%, 447,810G)
L2_Sentinel: 200G (1.7%, 13,929G)
jackkrause: 200G (1.7%, 784G)
BlinkFS: 200G (1.7%, 1,212G)
JIDkomu: 192G (1.6%, 192G)
SaintOmerville: 100G (0.8%, 810G)

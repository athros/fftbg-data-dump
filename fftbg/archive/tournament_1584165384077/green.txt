Player: !Green
Team: Green Team
Palettes: Green/White



ANFz
Male
Pisces
43
75
Squire
Steal
Counter Tackle
Martial Arts
Waterwalking

Flail
Crystal Shield
Cachusha
Wizard Outfit
Angel Ring

Dash, Heal, Tickle
Gil Taking, Steal Heart, Steal Shield, Steal Accessory



Yoshima75
Female
Aquarius
64
42
Lancer
Item
Arrow Guard
Throw Item
Move+2

Spear
Round Shield
Crystal Helmet
Plate Mail
Reflect Ring

Level Jump8, Vertical Jump8
Potion, Ether, Antidote, Phoenix Down



Xarelia
Female
Pisces
82
51
Archer
Throw
Caution
Short Charge
Fly

Blaze Gun
Diamond Shield
Red Hood
Chain Vest
Power Wrist

Charge+4, Charge+10
Axe, Spear, Stick



Lonfyre
Male
Gemini
39
63
Thief
Draw Out
Counter
Defense UP
Waterwalking

Ninja Edge

Flash Hat
Secret Clothes
Magic Gauntlet

Gil Taking, Steal Helmet, Steal Shield, Steal Status
Asura, Bizen Boat, Murasame, Kiyomori, Kikuichimoji

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Numbersborne
Male
Pisces
49
73
Oracle
Draw Out
Earplug
Equip Bow
Move+2

Long Bow

Golden Hairpin
Brigandine
Angel Ring

Doubt Faith, Foxbird, Sleep, Dark Holy
Bizen Boat, Heaven's Cloud, Kiyomori



Lanshaft
Male
Gemini
48
71
Oracle
Sing
Meatbone Slash
Concentrate
Waterwalking

Gokuu Rod

Thief Hat
Chameleon Robe
Sprint Shoes

Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep
Angel Song, Life Song



SkylerBunny
Female
Pisces
61
67
Squire
Talk Skill
Counter
Equip Bow
Swim

Ice Bow

Barbuta
Earth Clothes
Wizard Mantle

Accumulate, Dash, Throw Stone, Heal, Tickle, Fury
Invitation, Preach, Death Sentence, Negotiate, Refute



HaychDub
Male
Sagittarius
67
58
Chemist
Throw
Counter
Equip Shield
Jump+1

Star Bag
Bronze Shield
Red Hood
Black Costume
Setiemson

Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Shuriken, Staff

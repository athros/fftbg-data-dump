Player: !zChamp
Team: Champion Team
Palettes: Green/White



EnemyController
Male
Taurus
77
65
Bard
Item
HP Restore
Throw Item
Waterbreathing

Ramia Harp

Flash Hat
Rubber Costume
Feather Boots

Nameless Song, Last Song, Space Storage, Sky Demon
Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Eudes89
Female
Pisces
77
77
Archer
Dance
Meatbone Slash
Equip Sword
Waterwalking

Koutetsu Knife
Bronze Shield
Grand Helmet
Judo Outfit
Reflect Ring

Charge+2, Charge+7
Wiznaibus, Disillusion



GeeGXP
Male
Serpentarius
64
56
Geomancer
Throw
Mana Shield
Defense UP
Move+2

Giant Axe
Bronze Shield
Headgear
Clothes
Sprint Shoes

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Bomb, Axe



DavenIII
Male
Sagittarius
55
43
Thief
Talk Skill
Counter Flood
Dual Wield
Retreat

Ice Brand
Mythril Knife
Twist Headband
Wizard Outfit
Sprint Shoes

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Invitation, Preach, Solution, Mimic Daravon, Rehabilitate

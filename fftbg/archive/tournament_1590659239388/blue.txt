Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



NicoSavoy
Female
Leo
57
54
Lancer
Time Magic
Parry
Attack UP
Levitate

Javelin
Platinum Shield
Crystal Helmet
Linen Robe
Power Wrist

Level Jump8, Vertical Jump3
Slow, Demi 2, Stabilize Time



Sinnyil2
Male
Gemini
45
69
Thief
Throw
Hamedo
Equip Sword
Move+3

Diamond Sword

Flash Hat
Black Costume
Rubber Shoes

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Bomb, Stick, Dictionary



DustBirdEX
Female
Virgo
74
53
Dancer
Black Magic
PA Save
Magic Defense UP
Lava Walking

Hydra Bag

Green Beret
Mystic Vest
N-Kai Armlet

Witch Hunt, Wiznaibus, Disillusion, Obsidian Blade, Nether Demon, Dragon Pit
Fire, Fire 2, Fire 4, Bolt, Ice, Flare



UmaiJam
Female
Scorpio
43
62
Geomancer
White Magic
Counter
Equip Axe
Swim

Flame Whip
Platinum Shield
Ribbon
Earth Clothes
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Cure, Raise, Raise 2, Protect 2, Shell, Shell 2, Wall, Esuna, Holy

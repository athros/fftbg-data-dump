Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ChaoreLance
Female
Gemini
69
57
Calculator
Disease Skill
Auto Potion
Long Status
Jump+1

Battle Bamboo
Crystal Shield
Mythril Helmet
Carabini Mail
Angel Ring

Blue Magic
Tendrils, Lick, Goo, Bad Breath, Moldball Virus, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Thyrandaal
Female
Pisces
75
48
Calculator
Dungeon Skill
Auto Potion
Magic Defense UP
Retreat

Bestiary
Aegis Shield
Iron Helmet
Chain Mail
Cherche

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



Chuckolator
Male
Scorpio
53
42
Calculator
Robosnake Skill
Faith Save
Defend
Retreat

Rod
Crystal Shield
Circlet
Wizard Robe
Genji Gauntlet

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Gorgewall
Male
Aries
73
54
Calculator
Limit
Distribute
Concentrate
Move+3

Iron Fan
Genji Shield
Genji Helmet
Linen Cuirass
Sprint Shoes

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

Player: !Red
Team: Red Team
Palettes: Red/Brown



Safira125
Male
Sagittarius
53
47
Calculator
Limit
Mana Shield
Short Charge
Swim

Bestiary
Ice Shield
Cross Helmet
Brigandine
Defense Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Hasterious
Male
Virgo
54
63
Calculator
Curse Skill
Arrow Guard
Equip Shield
Fly

Thunder Rod
Flame Shield
Holy Miter
Rubber Costume
Spike Shoes

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Miku Shikhu
Female
Aquarius
75
47
Calculator
Bird Skill
Absorb Used MP
Short Charge
Move+2

Octagon Rod
Platinum Shield
Gold Helmet
Bronze Armor
Jade Armlet

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



SkylerBunny
Female
Aries
49
41
Calculator
Byblos
Abandon
Magic Defense UP
Waterwalking

Ice Rod
Platinum Shield
Feather Hat
Genji Armor
Reflect Ring

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken

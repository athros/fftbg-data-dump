Player: !Green
Team: Green Team
Palettes: Green/White



MrUbiq
Male
Serpentarius
44
51
Knight
Steal
Speed Save
Short Status
Move+2

Defender

Barbuta
Linen Cuirass
Salty Rage

Head Break, Mind Break, Justice Sword, Dark Sword
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory



Glitchminded
Female
Leo
48
60
Priest
Battle Skill
Abandon
Short Status
Retreat

Rainbow Staff

Twist Headband
Chain Vest
Elf Mantle

Cure 2, Cure 3, Regen, Shell 2, Wall, Esuna
Shield Break, Weapon Break



HughJeffner
Male
Virgo
46
76
Bard
Draw Out
Mana Shield
Defense UP
Move-HP Up

Long Bow

Thief Hat
Brigandine
Reflect Ring

Life Song, Battle Song, Diamond Blade
Asura, Koutetsu, Kikuichimoji



ShadowedFlames
Female
Capricorn
62
47
Time Mage
Black Magic
Abandon
Martial Arts
Teleport



Holy Miter
Linen Robe
Dracula Mantle

Slow, Stop, Quick, Demi 2, Stabilize Time
Fire 2, Fire 4, Bolt 2, Bolt 4, Ice 4, Empower

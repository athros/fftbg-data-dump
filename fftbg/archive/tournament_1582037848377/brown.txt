Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Iciunoy
Male
Gemini
65
57
Knight
Throw
Counter Tackle
Attack UP
Jump+3

Long Sword
Flame Shield
Cross Helmet
Light Robe
Diamond Armlet

Head Break, Shield Break, Weapon Break, Speed Break
Staff, Dictionary



Stoww
Monster
Aries
56
53
Mindflayer










Zachara
Female
Aries
80
62
Thief
Talk Skill
PA Save
Monster Talk
Waterbreathing

Dagger

Feather Hat
Wizard Outfit
Angel Ring

Steal Heart, Steal Helmet, Steal Weapon
Invitation, Threaten, Preach, Solution, Refute



TheStinkyYiffman
Female
Sagittarius
60
49
Oracle
Basic Skill
Auto Potion
Equip Bow
Move+2

Cross Bow

Feather Hat
Light Robe
Genji Gauntlet

Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep
Dash, Throw Stone, Cheer Up, Wish

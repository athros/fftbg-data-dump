Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HaateXIII
Male
Leo
60
65
Priest
Battle Skill
Abandon
Martial Arts
Move+2



Holy Miter
Wizard Robe
Chantage

Cure 3, Raise, Raise 2, Reraise, Shell
Head Break, Night Sword, Surging Sword



Thehotrefrigerator
Female
Taurus
67
64
Mediator
Charge
MP Restore
Short Status
Lava Walking

Romanda Gun

Flash Hat
Linen Robe
Feather Mantle

Praise, Preach, Rehabilitate
Charge+3, Charge+4, Charge+5



Zeando
Female
Sagittarius
48
72
Calculator
White Magic
Regenerator
Dual Wield
Swim

Bestiary
Papyrus Codex
Leather Hat
Chain Vest
Elf Mantle

CT, 4, 3
Cure 2, Cure 3, Raise, Raise 2, Wall, Esuna



Lastly
Female
Gemini
44
73
Squire
Yin Yang Magic
Catch
Doublehand
Ignore Terrain

Mythril Knife

Platinum Helmet
Mythril Vest
Bracer

Dash, Yell
Blind, Poison, Life Drain, Zombie, Silence Song, Paralyze, Petrify

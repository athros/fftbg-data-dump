Player: !Black
Team: Black Team
Palettes: Black/Red



Cocoaking09
Male
Capricorn
44
77
Mediator
Charge
Damage Split
Defend
Waterwalking

Papyrus Codex

Leather Hat
White Robe
Leather Mantle

Threaten, Preach, Insult, Rehabilitate
Charge+3, Charge+4, Charge+5, Charge+10, Charge+20



Hirameki85
Female
Pisces
49
78
Priest
Elemental
Auto Potion
Equip Armor
Jump+2

White Staff

Cross Helmet
Plate Mail
Rubber Shoes

Cure, Cure 3, Cure 4, Raise, Reraise, Shell
Pitfall, Hallowed Ground, Will-O-Wisp, Sand Storm, Gusty Wind



Powergems
Monster
Aries
69
68
Minotaur










Skipsandwiches
Female
Libra
52
70
Priest
Time Magic
Abandon
Equip Shield
Jump+3

Rainbow Staff
Flame Shield
Flash Hat
Black Robe
Small Mantle

Cure, Raise, Regen, Shell 2, Wall
Haste 2, Slow 2, Float, Reflect, Stabilize Time

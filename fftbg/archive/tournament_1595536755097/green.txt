Player: !Green
Team: Green Team
Palettes: Green/White



DustBirdEX
Female
Gemini
45
75
Calculator
Limit
Counter Flood
Equip Polearm
Jump+2

Spear

Headgear
Chain Vest
Feather Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Dinin991
Male
Serpentarius
66
42
Archer
Summon Magic
Regenerator
Doublehand
Ignore Height

Ice Bow

Golden Hairpin
Chain Vest
Sprint Shoes

Charge+1, Charge+4, Charge+10
Moogle, Carbunkle, Odin, Leviathan



Xoomwaffle
Male
Aries
60
60
Mime

PA Save
Equip Shield
Levitate


Aegis Shield
Headgear
Power Sleeve
Diamond Armlet

Mimic




Holdenmagronik
Female
Taurus
77
51
Ninja
Jump
MP Restore
Attack UP
Levitate

Sasuke Knife
Blind Knife
Feather Hat
Judo Outfit
Bracer

Shuriken, Knife
Level Jump4, Vertical Jump2

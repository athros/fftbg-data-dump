Player: !Red
Team: Red Team
Palettes: Red/Brown



Da Aa Ve
Male
Capricorn
61
79
Ninja
Summon Magic
Counter Tackle
Magic Defense UP
Retreat

Kunai
Sasuke Knife
Red Hood
Black Costume
Salty Rage

Bomb
Shiva, Ramuh, Ifrit, Golem, Carbunkle, Salamander, Silf, Fairy



Time  Mage
Female
Leo
63
72
Chemist
Yin Yang Magic
Counter
Equip Polearm
Fly

Iron Fan

Headgear
Earth Clothes
Diamond Armlet

Potion, X-Potion, Soft, Phoenix Down
Spell Absorb, Foxbird, Confusion Song, Sleep



Tikotikotiko
Female
Aries
59
42
Chemist
Black Magic
Damage Split
Martial Arts
Ignore Height

Panther Bag

Flash Hat
Black Costume
Dracula Mantle

Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down
Bolt 3, Ice 2



AStatue
Male
Aries
68
80
Chemist
Charge
Counter Magic
Short Charge
Waterwalking

Cute Bag

Feather Hat
Judo Outfit
Reflect Ring

Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down
Charge+2, Charge+5

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Reddwind
Female
Gemini
67
45
Lancer
Charge
Earplug
Doublehand
Move+2

Partisan

Circlet
Chameleon Robe
Red Shoes

Level Jump3, Vertical Jump5
Charge+1, Charge+3, Charge+20



Sinnyil2
Monster
Virgo
69
58
Wyvern










NBD Rab
Male
Capricorn
55
77
Archer
White Magic
Damage Split
Attack UP
Ignore Height

Gastrafitis
Gold Shield
Holy Miter
Earth Clothes
Dracula Mantle

Charge+2, Charge+3, Charge+5
Raise 2, Protect 2, Esuna, Holy



Aeonicvector
Female
Sagittarius
41
69
Ninja
Time Magic
Parry
Equip Gun
Ignore Terrain

Papyrus Codex
Papyrus Codex
Headgear
Brigandine
Power Wrist

Shuriken, Bomb, Knife
Haste, Float, Demi 2

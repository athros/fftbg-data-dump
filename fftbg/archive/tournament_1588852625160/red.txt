Player: !Red
Team: Red Team
Palettes: Red/Brown



SeniorBunk
Monster
Virgo
71
57
Black Goblin










ThePineappleSalesman
Female
Libra
38
64
Oracle
Item
Distribute
Equip Armor
Move+1

Papyrus Codex

Iron Helmet
Crystal Mail
Feather Mantle

Poison, Spell Absorb, Life Drain, Confusion Song, Petrify
Hi-Potion, X-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down



Legitimized
Female
Taurus
61
66
Samurai
Throw
Abandon
Magic Attack UP
Jump+3

Asura Knife

Genji Helmet
Platinum Armor
Chantage

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Shuriken



SaintOmerville
Male
Aquarius
43
52
Geomancer
Punch Art
Arrow Guard
Attack UP
Ignore Terrain

Slasher
Diamond Shield
Twist Headband
Silk Robe
Wizard Mantle

Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Lava Ball
Pummel, Wave Fist, Secret Fist, Purification, Chakra

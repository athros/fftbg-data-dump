Player: !Green
Team: Green Team
Palettes: Green/White



MaverickUmbracato
Male
Taurus
49
37
Wizard
Yin Yang Magic
MP Restore
Attack UP
Jump+1

Wizard Rod

Green Beret
Black Costume
Elf Mantle

Fire, Bolt 2, Bolt 4, Ice 3, Ice 4, Empower, Flare
Poison, Spell Absorb, Pray Faith, Dispel Magic, Sleep



Itsadud
Male
Scorpio
52
64
Samurai
Jump
Blade Grasp
Dual Wield
Move+2

Koutetsu Knife
Asura Knife
Crystal Helmet
Wizard Robe
Spike Shoes

Asura, Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji
Level Jump2, Vertical Jump6



Mesmaster
Female
Capricorn
78
45
Samurai
Charge
MA Save
Equip Polearm
Jump+2

Octagon Rod

Cross Helmet
Leather Armor
Feather Mantle

Koutetsu, Kiyomori
Charge+4, Charge+5, Charge+7



Imakuni
Female
Capricorn
44
62
Wizard
Talk Skill
Hamedo
Equip Armor
Move+3

Thunder Rod

Platinum Helmet
Rubber Costume
Genji Gauntlet

Bolt, Bolt 4, Ice 2
Persuade, Praise, Preach, Insult, Negotiate, Refute, Rehabilitate

Final Bets: red - 18 bets for 9,944G (26.1%, x2.83); blue - 22 bets for 28,145G (73.9%, x0.35)

red bets:
LanseDM: 1,500G (15.1%, 13,558G)
Estan_AD: 1,500G (15.1%, 17,231G)
HaateXIII: 1,356G (13.6%, 1,356G)
FoeSquirrel: 879G (8.8%, 1,758G)
HaplessOne: 666G (6.7%, 15,887G)
DeathTaxesAndAnime: 504G (5.1%, 504G)
volgrathemoose: 501G (5.0%, 12,600G)
Breakdown777: 500G (5.0%, 8,131G)
Baron_von_Scrub: 474G (4.8%, 12,933G)
Rislyeu: 456G (4.6%, 913G)
cupholderr: 286G (2.9%, 286G)
TimeJannies: 264G (2.7%, 264G)
InOzWeTrust: 264G (2.7%, 264G)
gorgewall: 201G (2.0%, 5,108G)
tempren: 200G (2.0%, 1,592G)
getthemoneyz: 194G (2.0%, 656,816G)
ANFz: 100G (1.0%, 32,527G)
CorpusCav: 99G (1.0%, 4,011G)

blue bets:
reinoe: 14,953G (53.1%, 14,953G)
Mesmaster: 3,000G (10.7%, 60,768G)
Lordminsc: 2,316G (8.2%, 4,632G)
superdevon1: 1,401G (5.0%, 1,401G)
maakur_: 1,261G (4.5%, 1,261G)
BirbBrainsBot: 1,000G (3.6%, 165,658G)
SeniorBunk: 723G (2.6%, 723G)
Laserman1000: 537G (1.9%, 5,637G)
EnemyController: 500G (1.8%, 234,842G)
victoriolue: 420G (1.5%, 420G)
ungabunga_bot: 420G (1.5%, 429,280G)
HaychDub: 300G (1.1%, 7,748G)
Rytor: 236G (0.8%, 236G)
bobc777: 200G (0.7%, 200G)
blastacez: 195G (0.7%, 195G)
NicoSavoy: 132G (0.5%, 132G)
Jaritras: 101G (0.4%, 1,201G)
ApplesauceBoss: 100G (0.4%, 9,226G)
wisponp: 100G (0.4%, 100G)
datadrivenbot: 100G (0.4%, 12,356G)
Kyune: 100G (0.4%, 1,362G)
moonliquor: 50G (0.2%, 13,119G)

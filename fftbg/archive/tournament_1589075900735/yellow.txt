Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Waterwatereverywhere
Female
Virgo
46
49
Ninja
Charge
Catch
Equip Knife
Jump+2

Ninja Edge
Koga Knife
Triangle Hat
Leather Outfit
108 Gems

Shuriken
Charge+10



HaplessOne
Female
Aries
47
67
Summoner
Time Magic
MP Restore
Martial Arts
Jump+1



Green Beret
Earth Clothes
Cherche

Moogle, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Lich
Haste, Demi, Demi 2



Jaritras
Female
Aquarius
42
45
Summoner
Time Magic
Critical Quick
Equip Shield
Move+2

Ice Rod
Round Shield
Headgear
Power Sleeve
Feather Mantle

Shiva, Titan, Carbunkle, Bahamut, Fairy, Cyclops
Haste, Haste 2, Demi 2, Stabilize Time, Galaxy Stop



ArchKnightX
Female
Capricorn
77
41
Geomancer
Battle Skill
Earplug
Concentrate
Ignore Terrain

Diamond Sword
Escutcheon
Triangle Hat
Wizard Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp
Shield Break, Speed Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword

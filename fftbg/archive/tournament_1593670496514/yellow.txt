Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Alekzanndrr
Male
Gemini
69
77
Bard
Elemental
Meatbone Slash
Sicken
Move+2

Bloody Strings

Cachusha
Power Sleeve
Angel Ring

Angel Song, Last Song, Space Storage
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Enkikavlar
Male
Pisces
70
68
Chemist
Punch Art
MP Restore
Equip Gun
Fly

Ramia Harp

Green Beret
Adaman Vest
Germinas Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive



Powergems
Female
Scorpio
65
75
Squire
Draw Out
Regenerator
Beastmaster
Fly

Giant Axe
Escutcheon
Thief Hat
Wizard Outfit
N-Kai Armlet

Heal
Koutetsu, Kiyomori, Kikuichimoji, Chirijiraden



OneHundredFists
Female
Aries
51
71
Knight
Black Magic
HP Restore
Doublehand
Fly

Blood Sword

Diamond Helmet
Diamond Armor
Defense Ring

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Night Sword
Fire 2, Bolt 2, Bolt 4, Ice 2, Ice 3, Ice 4, Death, Flare

Player: !Black
Team: Black Team
Palettes: Black/Red



Lowlf
Female
Scorpio
40
43
Priest
Draw Out
Critical Quick
Beastmaster
Move+2

White Staff

Headgear
Mystic Vest
Spike Shoes

Cure, Raise, Regen, Protect, Protect 2, Shell 2, Esuna
Heaven's Cloud, Muramasa



Moony Dragonsol
Monster
Sagittarius
74
40
Holy Dragon










Zachara
Male
Libra
78
70
Knight
Black Magic
Parry
Doublehand
Move-MP Up

Ragnarok

Mythril Helmet
Carabini Mail
Leather Mantle

Night Sword, Surging Sword
Fire, Bolt 4, Ice, Death



ALY327
Male
Cancer
49
40
Wizard
Summon Magic
Arrow Guard
Equip Polearm
Ignore Terrain

Whale Whisker

Headgear
Linen Robe
Spike Shoes

Bolt 2, Ice, Ice 2, Ice 3, Ice 4, Empower, Death, Flare
Ramuh, Titan, Golem, Carbunkle

Player: !White
Team: White Team
Palettes: White/Blue



DaveStrider55
Female
Libra
59
53
Samurai
Summon Magic
Mana Shield
Equip Shield
Move-HP Up

Spear
Escutcheon
Platinum Helmet
Carabini Mail
Defense Ring

Bizen Boat, Murasame
Moogle, Shiva, Ifrit, Carbunkle, Leviathan, Silf, Fairy, Lich



Clippopo
Monster
Pisces
51
59
Ochu










ZergTwitch
Male
Leo
48
57
Mediator
Punch Art
Arrow Guard
Halve MP
Fly

Orichalcum

Black Hood
White Robe
Magic Gauntlet

Persuade, Solution, Mimic Daravon, Refute
Spin Fist, Pummel, Chakra, Revive



Basmal
Male
Aquarius
60
64
Ninja
Charge
Counter
Equip Bow
Ignore Terrain

Bow Gun
Cross Bow
Red Hood
Chain Vest
Diamond Armlet

Shuriken
Charge+5, Charge+7, Charge+10

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Hyvi
Male
Gemini
48
74
Oracle
Draw Out
Distribute
Short Status
Ignore Height

Battle Bamboo

Golden Hairpin
Chameleon Robe
Elf Mantle

Spell Absorb, Pray Faith, Zombie, Blind Rage, Sleep, Petrify, Dark Holy
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



Yure1x
Male
Libra
69
54
Thief
Throw
Faith Up
Defense UP
Move-MP Up

Ancient Sword

Twist Headband
Mythril Vest
Defense Armlet

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Bomb, Spear, Wand



ALY327
Monster
Virgo
80
59
Ghoul










ArchKnightX
Male
Aries
39
68
Samurai
Time Magic
Speed Save
Doublehand
Move-HP Up

Murasame

Platinum Helmet
Wizard Robe
Feather Boots

Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Haste, Haste 2, Slow 2, Float, Reflect, Demi

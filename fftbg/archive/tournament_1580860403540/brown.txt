Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MrGoldark
Monster
Cancer
51
53
Trent










Kl0kwurk
Male
Leo
57
65
Geomancer
Battle Skill
Damage Split
Equip Shield
Fly

Kiyomori
Flame Shield
Thief Hat
Wizard Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Magic Break, Speed Break, Stasis Sword



Jam77
Male
Serpentarius
69
66
Summoner
Battle Skill
Arrow Guard
Defend
Jump+1

Gold Staff

Headgear
Judo Outfit
Battle Boots

Shiva, Ramuh, Ifrit, Golem, Carbunkle, Odin, Leviathan, Salamander, Silf
Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword, Night Sword



Cpt Qc
Male
Taurus
58
73
Lancer
Summon Magic
Dragon Spirit
Defense UP
Move-HP Up

Mythril Spear
Diamond Shield
Iron Helmet
Diamond Armor
Genji Gauntlet

Level Jump5, Vertical Jump2
Moogle, Ramuh, Golem, Carbunkle, Bahamut, Leviathan, Silf, Lich

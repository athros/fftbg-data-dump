Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bulleta
Female
Sagittarius
65
55
Chemist
Elemental
Parry
Equip Axe
Teleport

Wizard Staff

Leather Hat
Black Costume
Salty Rage

Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball



Mpghappiness
Female
Taurus
78
66
Oracle
Talk Skill
Counter Flood
Monster Talk
Move+2

Octagon Rod

Flash Hat
White Robe
Wizard Mantle

Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Paralyze, Sleep, Petrify
Praise, Solution, Death Sentence, Insult, Negotiate, Refute, Rehabilitate



Firesheath
Female
Virgo
76
66
Ninja
Battle Skill
Counter
Attack UP
Jump+1

Scorpion Tail
Flame Whip
Golden Hairpin
Mystic Vest
108 Gems

Shuriken, Ninja Sword, Wand
Head Break, Armor Break, Shield Break, Power Break, Mind Break, Justice Sword, Dark Sword



Laserman1000
Female
Capricorn
43
79
Dancer
Talk Skill
Distribute
Monster Talk
Waterwalking

Cashmere

Feather Hat
Judo Outfit
Magic Ring

Void Storage, Dragon Pit
Threaten, Refute, Rehabilitate

Player: !White
Team: White Team
Palettes: White/Blue



Doctrweird
Monster
Scorpio
56
79
Vampire










Losain
Male
Taurus
46
52
Mediator
Charge
Brave Up
Short Status
Swim

Dagger

Thief Hat
Chameleon Robe
Reflect Ring

Invitation, Praise, Preach, Solution, Mimic Daravon, Refute, Rehabilitate
Charge+1, Charge+3, Charge+7



Bilabrin
Monster
Libra
80
54
Wild Boar










Tearshang
Male
Serpentarius
65
61
Geomancer
Sing
Earplug
Equip Polearm
Waterwalking

Javelin
Round Shield
Leather Hat
White Robe
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm
Cheer Song, Sky Demon

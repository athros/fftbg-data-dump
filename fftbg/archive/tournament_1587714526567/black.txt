Player: !Black
Team: Black Team
Palettes: Black/Red



Sinnyil2
Male
Taurus
71
71
Mime

Caution
Martial Arts
Move+2



Green Beret
Linen Cuirass
Red Shoes

Mimic




Jahshine
Male
Capricorn
63
46
Mediator
Charge
Dragon Spirit
Maintenance
Waterwalking

Blaze Gun

Golden Hairpin
Chameleon Robe
Diamond Armlet

Invitation, Praise, Threaten, Solution
Charge+2, Charge+3, Charge+4, Charge+10



Sairentozon7
Male
Leo
67
50
Priest
Item
Parry
Throw Item
Teleport

Flame Whip

Twist Headband
Leather Outfit
Germinas Boots

Cure, Cure 2, Raise, Protect 2, Wall, Esuna
Potion, Hi-Potion, Antidote, Eye Drop, Holy Water, Phoenix Down



WireLord
Female
Leo
44
73
Time Mage
Steal
Meatbone Slash
Magic Defense UP
Retreat

Rainbow Staff

Feather Hat
Linen Robe
Red Shoes

Slow 2, Immobilize, Float, Quick, Demi, Demi 2
Gil Taking, Steal Accessory, Steal Status, Arm Aim

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Galkife
Monster
Leo
59
80
Swine










Lord Burrah
Female
Virgo
64
64
Geomancer
Item
Mana Shield
Beastmaster
Teleport

Giant Axe
Genji Shield
Black Hood
Black Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Antidote, Echo Grass, Soft, Holy Water, Phoenix Down



Leakimiko
Female
Taurus
55
80
Ninja
Summon Magic
HP Restore
Equip Gun
Waterbreathing

Stone Gun
Blast Gun
Ribbon
Clothes
Angel Ring

Shuriken, Bomb, Sword
Ramuh, Ifrit, Carbunkle, Bahamut, Odin, Leviathan, Lich



FoeSquirrel
Female
Aries
59
48
Mediator
Throw
HP Restore
Attack UP
Swim

Blast Gun

Black Hood
Wizard Robe
Wizard Mantle

Invitation, Persuade, Preach, Insult, Negotiate, Mimic Daravon, Refute
Shuriken, Bomb

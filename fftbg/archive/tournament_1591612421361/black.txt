Player: !Black
Team: Black Team
Palettes: Black/Red



Mesmaster
Male
Aries
72
68
Knight
Charge
Meatbone Slash
Beastmaster
Lava Walking

Battle Axe
Diamond Shield
Gold Helmet
Platinum Armor
Feather Boots

Head Break, Armor Break, Shield Break, Power Break, Dark Sword
Charge+7



Serperemagus
Female
Leo
52
49
Thief
Basic Skill
Counter
Secret Hunt
Teleport

Sasuke Knife

Golden Hairpin
Judo Outfit
Small Mantle

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Accumulate, Heal, Tickle



KyleWonToLiveForever
Male
Gemini
55
71
Priest
Jump
Distribute
Attack UP
Move-MP Up

Flame Whip

Thief Hat
Brigandine
Feather Boots

Cure, Regen, Wall, Esuna
Level Jump2, Vertical Jump7



Prince Rogers Nelson
Male
Scorpio
37
47
Mime

Mana Shield
Martial Arts
Waterbreathing



Holy Miter
Clothes
Bracer

Mimic


Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Cancer
47
59
Monk
Charge
Parry
Magic Defense UP
Ignore Terrain



Feather Hat
Leather Outfit
Defense Ring

Spin Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Charge+4



Sinnyil2
Male
Scorpio
39
76
Archer
Punch Art
Critical Quick
Equip Shield
Jump+2

Bow Gun
Round Shield
Black Hood
Chain Vest
Leather Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Wave Fist, Purification, Chakra



SkylerBunny
Female
Aries
58
43
Mime

Arrow Guard
Long Status
Retreat



Green Beret
Brigandine
Genji Gauntlet

Mimic




TheMM42
Male
Scorpio
56
51
Samurai
Item
Counter Flood
Magic Defense UP
Ignore Height

Masamune

Platinum Helmet
Mythril Armor
Wizard Mantle

Asura, Kiyomori
Potion, Hi-Ether, Antidote

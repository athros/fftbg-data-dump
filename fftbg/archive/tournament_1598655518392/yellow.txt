Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Maakur
Male
Aquarius
71
77
Mime

Dragon Spirit
Equip Armor
Move+1



Cross Helmet
Platinum Armor
Leather Mantle

Mimic




Gorgewall
Male
Scorpio
39
43
Lancer
Battle Skill
Meatbone Slash
Dual Wield
Ignore Height

Mythril Spear
Spear
Platinum Helmet
Crystal Mail
Wizard Mantle

Level Jump3, Vertical Jump5
Magic Break, Power Break, Mind Break, Surging Sword



Aldrammech
Male
Capricorn
71
80
Time Mage
Punch Art
Counter
Equip Gun
Retreat

Bestiary

Ribbon
Chameleon Robe
Spike Shoes

Haste, Slow, Immobilize, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Pummel, Secret Fist, Purification, Chakra



Holandrix
Female
Libra
57
78
Summoner
Yin Yang Magic
Parry
Doublehand
Lava Walking

Wizard Staff

Leather Hat
White Robe
Leather Mantle

Moogle, Shiva, Titan, Carbunkle, Odin, Leviathan, Fairy
Poison, Spell Absorb, Paralyze, Sleep, Dark Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Drako7z
Male
Serpentarius
65
42
Oracle
Steal
Mana Shield
Long Status
Move-HP Up

Bestiary

Flash Hat
Judo Outfit
Leather Mantle

Blind, Spell Absorb, Life Drain, Doubt Faith, Confusion Song, Dispel Magic
Gil Taking, Leg Aim



HaychDub
Male
Virgo
78
75
Bard
Talk Skill
Absorb Used MP
Monster Talk
Jump+3

Yoichi Bow

Feather Hat
Genji Armor
Battle Boots

Sky Demon
Threaten, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Slowbrofist
Female
Aries
67
41
Geomancer
Steal
Counter Magic
Short Status
Jump+1

Mythril Sword
Aegis Shield
Golden Hairpin
Adaman Vest
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim



Mayormcfunbags
Female
Leo
60
73
Ninja
Time Magic
Distribute
Secret Hunt
Move+3

Short Edge
Flame Whip
Feather Hat
Mythril Vest
Spike Shoes

Shuriken, Knife, Sword, Staff
Haste, Immobilize, Reflect, Demi, Stabilize Time, Meteor

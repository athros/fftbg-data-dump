Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SeedSC
Female
Pisces
67
66
Mediator
Battle Skill
Counter Tackle
Dual Wield
Move+2

Blaze Gun
Blast Gun
Holy Miter
White Robe
Battle Boots

Praise, Preach, Solution, Refute, Rehabilitate
Head Break, Armor Break, Weapon Break, Magic Break, Speed Break, Dark Sword



J2DaBibbles
Female
Pisces
80
45
Oracle
Black Magic
Catch
Equip Armor
Ignore Height

Octagon Rod

Twist Headband
Crystal Mail
Magic Gauntlet

Pray Faith, Foxbird, Dispel Magic, Paralyze, Dark Holy
Fire 2, Fire 4, Ice 2



Meyples
Male
Libra
60
73
Lancer
Black Magic
Counter
Equip Gun
Waterbreathing

Fairy Harp
Platinum Shield
Genji Helmet
Diamond Armor
Elf Mantle

Level Jump3, Vertical Jump8
Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4



Roofiepops
Male
Aquarius
50
55
Ninja
Jump
MA Save
Attack UP
Lava Walking

Flame Whip
Morning Star
Holy Miter
Judo Outfit
Defense Armlet

Knife, Wand, Dictionary
Level Jump5, Vertical Jump8

Player: !Green
Team: Green Team
Palettes: Green/White



Phrossi V
Male
Leo
65
80
Bard
White Magic
Damage Split
Equip Sword
Move+2

Koutetsu Knife

Flash Hat
Crystal Mail
Germinas Boots

Cheer Song, Battle Song, Nameless Song, Hydra Pit
Cure, Cure 3, Raise, Regen, Protect 2, Wall, Esuna, Holy



Kai Shee
Male
Serpentarius
62
70
Archer
Steal
MP Restore
Equip Sword
Waterbreathing

Broad Sword
Kaiser Plate
Headgear
Rubber Costume
Small Mantle

Charge+3, Charge+7, Charge+20
Steal Helmet, Steal Armor



Catfashions
Male
Serpentarius
75
49
Chemist
Jump
MP Restore
Equip Gun
Move+3

Fairy Harp

Flash Hat
Mystic Vest
Red Shoes

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Remedy
Level Jump8, Vertical Jump7



AzuriteReaction
Male
Libra
62
78
Calculator
Tentacle Skill
Parry
Equip Bow
Retreat

Cross Bow
Genji Shield
Iron Helmet
Genji Armor
Feather Mantle

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus

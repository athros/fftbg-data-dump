Player: !Black
Team: Black Team
Palettes: Black/Red



Baron Von Scrub
Female
Taurus
52
66
Summoner
Charge
Speed Save
Short Charge
Ignore Terrain

Rainbow Staff

Green Beret
Light Robe
Small Mantle

Moogle, Ifrit, Titan, Golem, Carbunkle, Odin, Salamander
Charge+5, Charge+10



ArchKnightX
Male
Libra
74
50
Mediator
Time Magic
Parry
Short Charge
Jump+1

Bestiary

Headgear
Robe of Lords
Battle Boots

Solution, Refute, Rehabilitate
Haste, Stop, Immobilize, Quick, Demi 2, Stabilize Time



Genericco
Female
Serpentarius
78
61
Calculator
Black Magic
Abandon
Dual Wield
Move+3

Madlemgen
Battle Folio
Flash Hat
Chameleon Robe
Leather Mantle

CT, Height, Prime Number
Fire 4, Bolt 4, Ice, Ice 3, Ice 4, Flare



Maldoree
Female
Virgo
49
60
Calculator
Yin Yang Magic
Parry
Equip Gun
Jump+1

Blaze Gun

Feather Hat
Chain Vest
Feather Boots

CT, Prime Number, 5
Poison, Life Drain, Pray Faith, Dispel Magic

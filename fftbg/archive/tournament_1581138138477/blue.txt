Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dorkovsky
Male
Gemini
70
52
Time Mage
Talk Skill
Damage Split
Monster Talk
Jump+1

Bestiary

Green Beret
Silk Robe
Jade Armlet

Haste, Haste 2, Stop, Quick, Demi, Stabilize Time
Threaten, Insult, Negotiate, Mimic Daravon, Refute



Gabbagooluigi
Male
Gemini
77
40
Monk
Item
Brave Up
Doublehand
Move+3



Twist Headband
Wizard Outfit
Defense Ring

Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft



Blain Cooper
Male
Leo
62
43
Ninja
Punch Art
Parry
Attack UP
Waterbreathing

Hidden Knife
Hidden Knife
Triangle Hat
Brigandine
Diamond Armlet

Shuriken
Earth Slash, Secret Fist, Purification, Revive



Wizard 01
Female
Virgo
76
57
Archer
Time Magic
Dragon Spirit
Secret Hunt
Lava Walking

Ice Bow

Barette
Black Costume
Reflect Ring

Charge+1, Charge+7, Charge+10
Haste, Haste 2, Stop, Quick, Stabilize Time

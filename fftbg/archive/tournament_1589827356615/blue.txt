Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CapnChaos12
Female
Aries
47
62
Summoner
Elemental
Auto Potion
Equip Armor
Move+1

Wizard Rod

Circlet
Chain Mail
Wizard Mantle

Moogle, Leviathan
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



ThePineappleSalesman
Male
Aries
69
62
Summoner
Elemental
HP Restore
Magic Defense UP
Move-MP Up

Flame Rod

Triangle Hat
Chain Vest
Rubber Shoes

Moogle, Shiva, Titan, Golem, Leviathan, Salamander, Silf
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Coralreeferz
Male
Taurus
76
57
Archer
Elemental
Counter
Doublehand
Ignore Terrain

Blast Gun

Green Beret
Rubber Costume
Defense Ring

Charge+2, Charge+4, Charge+7, Charge+20
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball



Reltz
Female
Scorpio
73
53
Archer
Draw Out
Counter
Equip Gun
Lava Walking

Bestiary
Aegis Shield
Iron Helmet
Mystic Vest
Diamond Armlet

Charge+1, Charge+3
Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji, Masamune

Player: !Black
Team: Black Team
Palettes: Black/Red



Forkmore
Male
Pisces
48
43
Ninja
Battle Skill
Dragon Spirit
Equip Axe
Jump+2

Wizard Staff
Slasher
Ribbon
Adaman Vest
Dracula Mantle

Shuriken, Staff, Dictionary
Head Break, Shield Break, Weapon Break



Reddwind
Male
Taurus
52
69
Knight
Summon Magic
Sunken State
Short Status
Move+3

Defender
Crystal Shield
Crystal Helmet
Crystal Mail
Dracula Mantle

Head Break, Shield Break, Magic Break, Speed Break, Justice Sword, Dark Sword
Ifrit, Salamander



DLJuggernaut
Male
Aries
72
60
Chemist
Jump
Brave Save
Maintenance
Jump+3

Blind Knife

Twist Headband
Adaman Vest
108 Gems

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Phoenix Down
Level Jump5, Vertical Jump2



Just Here2
Male
Sagittarius
47
50
Bard
Talk Skill
Damage Split
Equip Polearm
Jump+3

Octagon Rod

Green Beret
Leather Outfit
Jade Armlet

Angel Song, Cheer Song, Last Song, Sky Demon
Invitation, Persuade, Praise, Threaten, Solution, Death Sentence, Insult, Mimic Daravon, Refute

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SQUiDSQUARKLIN
Male
Gemini
57
65
Ninja
Draw Out
Caution
Equip Bow
Lava Walking

Cross Bow
Hunting Bow
Leather Hat
Wizard Outfit
Cherche

Shuriken, Staff, Ninja Sword
Kiyomori, Muramasa



Galkife
Male
Gemini
41
36
Mediator
Charge
PA Save
Concentrate
Move-MP Up

Main Gauche

Golden Hairpin
Mystic Vest
Bracer

Invitation, Praise, Preach, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Charge+5



ThePineappleSalesman
Female
Pisces
65
61
Summoner
Black Magic
Parry
Short Charge
Waterbreathing

Rainbow Staff

Feather Hat
Robe of Lords
Spike Shoes

Moogle, Shiva, Ramuh, Odin, Leviathan, Salamander, Fairy
Bolt 3, Bolt 4, Ice, Empower, Frog



TheGuesty
Female
Scorpio
43
63
Lancer
Summon Magic
Sunken State
Secret Hunt
Retreat

Javelin
Bronze Shield
Gold Helmet
Diamond Armor
Small Mantle

Level Jump8, Vertical Jump3
Moogle, Ramuh, Titan, Carbunkle, Silf

Player: !Black
Team: Black Team
Palettes: Black/Red



Lijarkh
Female
Scorpio
53
69
Monk
Charge
Sunken State
Maintenance
Move+2



Ribbon
Adaman Vest
Elf Mantle

Wave Fist, Earth Slash, Purification, Revive
Charge+3, Charge+20



ApplesauceBoss
Male
Scorpio
61
45
Monk
Black Magic
Mana Shield
Short Status
Move+3



Triangle Hat
Power Sleeve
Defense Armlet

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive
Fire, Fire 2, Fire 4, Bolt 2, Ice, Death



Volgrathemoose
Male
Aries
64
45
Monk
White Magic
Arrow Guard
Equip Bow
Fly

Long Bow

Triangle Hat
Earth Clothes
Bracer

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Cure 2, Cure 4, Raise, Raise 2, Protect 2, Esuna



Joewcarson
Female
Virgo
64
62
Priest
Yin Yang Magic
Caution
Dual Wield
Retreat

White Staff
Rainbow Staff
Red Hood
Silk Robe
Dracula Mantle

Cure 3, Protect, Shell 2, Wall, Esuna
Poison, Silence Song, Sleep

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Arunic
Female
Sagittarius
70
72
Time Mage
Black Magic
Catch
Magic Attack UP
Move+3

Gold Staff

Feather Hat
Adaman Vest
Feather Mantle

Slow, Immobilize, Quick, Stabilize Time
Fire, Bolt 2, Ice 2, Ice 4, Flare



Mtueni
Female
Capricorn
69
37
Knight
Black Magic
PA Save
Attack UP
Waterbreathing

Excalibur
Diamond Shield
Mythril Helmet
Linen Cuirass
Sprint Shoes

Armor Break, Magic Break, Power Break
Bolt 2, Ice 3, Ice 4



Luminarii
Female
Scorpio
80
46
Summoner
Black Magic
Meatbone Slash
Short Charge
Retreat

Poison Rod

Leather Hat
Mythril Vest
Vanish Mantle

Moogle, Ramuh, Titan, Carbunkle, Salamander, Fairy
Fire, Bolt 2, Bolt 3, Ice, Empower, Death



Nedryerson01
Female
Cancer
78
52
Thief
Throw
Catch
Equip Bow
Move+2

Mythril Bow

Cachusha
Adaman Vest
Cursed Ring

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Shuriken, Bomb, Sword

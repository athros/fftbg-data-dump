Player: !Red
Team: Red Team
Palettes: Red/Brown



DrAntiSocial
Male
Cancer
43
69
Ninja
Battle Skill
Counter
Equip Sword
Move-MP Up

Koutetsu Knife
Defender
Feather Hat
Secret Clothes
Bracer

Bomb, Dictionary
Mind Break, Dark Sword, Surging Sword



InOzWeTrust
Male
Aquarius
65
63
Oracle
Summon Magic
Counter Tackle
Maintenance
Ignore Height

Papyrus Codex

Leather Hat
Wizard Robe
N-Kai Armlet

Blind, Life Drain, Zombie, Blind Rage, Dispel Magic, Sleep
Moogle, Ifrit, Titan, Golem, Silf



TimeJannies
Male
Cancer
60
51
Lancer
Charge
Abandon
Equip Sword
Waterbreathing

Defender

Platinum Helmet
Platinum Armor
Sprint Shoes

Level Jump4, Vertical Jump7
Charge+3



Sairentozon7
Male
Taurus
54
61
Time Mage
Charge
Counter Flood
Halve MP
Move-HP Up

White Staff

Feather Hat
Wizard Outfit
108 Gems

Slow 2, Reflect, Quick, Stabilize Time
Charge+4

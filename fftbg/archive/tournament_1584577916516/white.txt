Player: !White
Team: White Team
Palettes: White/Blue



RunicMagus
Female
Virgo
41
59
Calculator
Bird Skill
Damage Split
Equip Gun
Jump+1

Bestiary
Aegis Shield
Diamond Helmet
Carabini Mail
Feather Mantle

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



I Nod My Head When I Lose
Male
Gemini
44
47
Mime

Earplug
Maintenance
Move-MP Up



Green Beret
Earth Clothes
Spike Shoes

Mimic




Thefyeman
Male
Cancer
68
67
Lancer
Throw
Mana Shield
Sicken
Retreat

Dragon Whisker
Aegis Shield
Circlet
White Robe
Angel Ring

Level Jump8, Vertical Jump7
Bomb, Dictionary



Arch8000
Male
Virgo
58
48
Wizard
White Magic
Earplug
Dual Wield
Move+2

Wizard Rod
Dragon Rod
Cachusha
Mythril Vest
Reflect Ring

Fire 3, Fire 4, Bolt, Bolt 3, Ice 4, Death
Raise, Raise 2, Regen, Protect, Protect 2, Shell, Shell 2, Esuna

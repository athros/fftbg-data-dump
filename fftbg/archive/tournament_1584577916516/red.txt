Player: !Red
Team: Red Team
Palettes: Red/Brown



FullMetalScrubby
Female
Leo
79
79
Samurai
Throw
Parry
Equip Gun
Move+3

Bestiary

Mythril Helmet
Plate Mail
Bracer

Asura, Murasame
Shuriken, Ninja Sword



Sotehr
Female
Aries
42
77
Summoner
Black Magic
Mana Shield
Short Charge
Move+1

Gold Staff

Leather Hat
Black Costume
Magic Gauntlet

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Silf
Fire 3, Fire 4, Bolt 3, Ice, Ice 3, Flare



Byrdturbo
Male
Serpentarius
45
72
Wizard
Elemental
Catch
Equip Shield
Jump+2

Flame Rod
Gold Shield
Green Beret
Chameleon Robe
Elf Mantle

Fire, Bolt 4, Ice 2, Death
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Davarian
Female
Cancer
74
67
Dancer
Yin Yang Magic
Dragon Spirit
Dual Wield
Teleport

Ryozan Silk
Persia
Leather Hat
Earth Clothes
Magic Gauntlet

Wiznaibus, Polka Polka, Nether Demon
Blind, Poison, Spell Absorb, Life Drain, Silence Song, Foxbird, Dispel Magic

Player: !Black
Team: Black Team
Palettes: Black/Red



Vultuous
Male
Sagittarius
69
80
Samurai
Jump
Dragon Spirit
Attack UP
Jump+3

Mythril Spear

Platinum Helmet
Crystal Mail
Salty Rage

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Level Jump5, Vertical Jump8



Klednar21
Female
Cancer
62
65
Archer
Draw Out
Brave Save
Secret Hunt
Move+1

Mythril Bow

Feather Hat
Wizard Outfit
108 Gems

Charge+1, Charge+7, Charge+10
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa



NicoSavoy
Male
Capricorn
52
60
Archer
Basic Skill
Counter Tackle
Equip Polearm
Jump+2

Obelisk
Crystal Shield
Mythril Helmet
Mystic Vest
108 Gems

Charge+1, Charge+2, Charge+5, Charge+20
Dash, Heal, Cheer Up



TeaTime29
Male
Virgo
62
65
Calculator
Yin Yang Magic
Counter Magic
Magic Defense UP
Jump+1

Gokuu Rod

Feather Hat
Linen Robe
Angel Ring

CT, 5, 4
Blind, Poison, Pray Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Tugboat1
Male
Virgo
57
70
Archer
Sing
Faith Up
Doublehand
Move-HP Up

Lightning Bow

Flash Hat
Power Sleeve
Germinas Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+20
Cheer Song, Magic Song, Last Song



Ranmilia
Female
Aquarius
47
60
Lancer
Punch Art
Dragon Spirit
Maintenance
Jump+1

Partisan
Escutcheon
Platinum Helmet
Plate Mail
Reflect Ring

Level Jump4, Vertical Jump5
Purification, Revive



UmbraKnights
Female
Leo
67
70
Knight
Draw Out
Abandon
Magic Defense UP
Move+1

Ragnarok

Iron Helmet
Plate Mail
Red Shoes

Head Break, Weapon Break, Mind Break, Night Sword, Surging Sword
Heaven's Cloud, Muramasa, Kikuichimoji



Treapvort
Female
Sagittarius
53
53
Geomancer
Draw Out
MA Save
Beastmaster
Move+1

Battle Axe
Platinum Shield
Triangle Hat
Brigandine
Setiemson

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Murasame

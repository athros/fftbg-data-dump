Player: !Black
Team: Black Team
Palettes: Black/Red



DesertWooder
Female
Gemini
77
49
Time Mage
Item
Hamedo
Short Charge
Waterbreathing

Sage Staff

Ribbon
Chameleon Robe
Elf Mantle

Haste, Slow 2, Stabilize Time
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down



Just Here2
Male
Pisces
52
67
Knight
Summon Magic
Faith Save
Equip Polearm
Move-MP Up

Spear
Round Shield
Leather Helmet
Black Robe
Chantage

Magic Break, Power Break, Mind Break, Justice Sword, Night Sword
Golem, Carbunkle, Salamander, Cyclops



TheDeeyo
Male
Cancer
51
63
Thief
Elemental
Mana Shield
Maintenance
Teleport

Ancient Sword

Green Beret
Adaman Vest
Reflect Ring

Steal Helmet, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



Latebit
Male
Scorpio
74
78
Samurai
Basic Skill
Arrow Guard
Equip Polearm
Ignore Terrain

Holy Lance

Genji Helmet
Crystal Mail
Dracula Mantle

Asura, Bizen Boat, Murasame
Dash, Throw Stone, Heal, Tickle, Fury

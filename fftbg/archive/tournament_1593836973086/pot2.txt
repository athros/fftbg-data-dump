Final Bets: green - 8 bets for 3,285G (31.1%, x2.21); yellow - 9 bets for 7,261G (68.9%, x0.45)

green bets:
reinoe: 1,000G (30.4%, 56,508G)
twelfthrootoftwo: 500G (15.2%, 6,987G)
MagicBottle: 492G (15.0%, 18,307G)
Firesheath: 408G (12.4%, 408G)
Shalloween: 300G (9.1%, 64,353G)
DaKoopa49: 285G (8.7%, 285G)
Flameatron: 200G (6.1%, 6,811G)
ANFz: 100G (3.0%, 25,965G)

yellow bets:
RunicMagus: 3,000G (41.3%, 62,505G)
BirbBrainsBot: 1,000G (13.8%, 175,633G)
DeathTaxesAndAnime: 848G (11.7%, 848G)
Rytor: 811G (11.2%, 1,622G)
Ring_Wyrm: 508G (7.0%, 508G)
Deathmaker06: 500G (6.9%, 9,497G)
getthemoneyz: 294G (4.0%, 1,171,486G)
alecttox: 200G (2.8%, 42,879G)
Rikrizen: 100G (1.4%, 6,060G)

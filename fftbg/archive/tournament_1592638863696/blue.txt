Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



D4rr1n
Male
Leo
80
43
Archer
White Magic
Speed Save
Doublehand
Jump+1

Long Bow

Black Hood
Chain Vest
Germinas Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Cure, Cure 2, Cure 3, Raise, Regen, Shell 2, Esuna



Reddwind
Female
Aquarius
43
51
Knight
Charge
Earplug
Equip Polearm
Move+3

Holy Lance
Diamond Shield
Leather Helmet
Plate Mail
Wizard Mantle

Head Break, Power Break
Charge+4



ALY327
Female
Cancer
72
44
Time Mage
Charge
Earplug
Beastmaster
Fly

Gokuu Rod

Golden Hairpin
Leather Outfit
N-Kai Armlet

Haste, Float, Reflect, Quick, Demi 2, Stabilize Time
Charge+4, Charge+5, Charge+10



BuffaloCrunch
Female
Capricorn
80
49
Thief
White Magic
Caution
Equip Gun
Jump+2

Romanda Gun

Black Hood
Earth Clothes
Cursed Ring

Steal Helmet, Steal Accessory
Raise, Regen, Protect 2, Shell, Esuna

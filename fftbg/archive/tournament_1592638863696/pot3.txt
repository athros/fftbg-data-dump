Final Bets: white - 6 bets for 3,574G (54.0%, x0.85); black - 8 bets for 3,040G (46.0%, x1.18)

white bets:
NovaKnight21: 1,000G (28.0%, 5,786G)
Digitalsocrates: 1,000G (28.0%, 42,703G)
BirbBrainsBot: 1,000G (28.0%, 127,637G)
Coolguye: 250G (7.0%, 3,623G)
getthemoneyz: 224G (6.3%, 993,771G)
rednecknazgul: 100G (2.8%, 1,352G)

black bets:
reinoe: 600G (19.7%, 600G)
prince_rogers_nelson_: 592G (19.5%, 592G)
HaateXIII: 540G (17.8%, 540G)
Zachara: 500G (16.4%, 109,500G)
CorpusCav: 400G (13.2%, 3,846G)
redrhinofever: 200G (6.6%, 2,564G)
Kaynin_Shadowwalker: 108G (3.6%, 108G)
Neo_Exodus: 100G (3.3%, 557G)

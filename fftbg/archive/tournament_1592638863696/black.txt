Player: !Black
Team: Black Team
Palettes: Black/Red



Evewho
Female
Aries
67
55
Chemist
Yin Yang Magic
Counter Magic
Equip Sword
Jump+2

Defender

Feather Hat
Brigandine
Angel Ring

Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Holy Water
Blind, Pray Faith, Paralyze, Dark Holy



FriendSquirrel
Female
Taurus
72
64
Priest
Battle Skill
Faith Save
Dual Wield
Move+2

Rainbow Staff
White Staff
Green Beret
Black Costume
Defense Armlet

Cure 3, Cure 4, Raise, Shell 2, Wall, Esuna
Head Break, Shield Break, Mind Break, Explosion Sword



TeaTime29
Male
Taurus
74
53
Mime

Counter
Martial Arts
Swim



Leather Hat
Leather Outfit
Wizard Mantle

Mimic




Redrhinofever
Female
Libra
41
78
Dancer
Battle Skill
Distribute
Martial Arts
Jump+2



Black Hood
Wizard Outfit
108 Gems

Wiznaibus, Polka Polka, Last Dance, Nether Demon
Shield Break, Magic Break, Mind Break

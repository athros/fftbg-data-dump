Final Bets: purple - 9 bets for 6,029G (49.1%, x1.04); brown - 6 bets for 6,251G (50.9%, x0.96)

purple bets:
prince_rogers_nelson_: 1,288G (21.4%, 1,288G)
Zachara: 1,088G (18.0%, 110,088G)
NovaKnight21: 1,000G (16.6%, 4,786G)
BirbBrainsBot: 1,000G (16.6%, 126,637G)
redrhinofever: 500G (8.3%, 2,799G)
serperemagus: 468G (7.8%, 468G)
Coolguye: 250G (4.1%, 3,373G)
Kaynin_Shadowwalker: 235G (3.9%, 235G)
CorpusCav: 200G (3.3%, 4,316G)

brown bets:
Digitalsocrates: 4,000G (64.0%, 41,703G)
reinoe: 1,305G (20.9%, 1,305G)
RaIshtar: 500G (8.0%, 3,990G)
getthemoneyz: 246G (3.9%, 993,547G)
rednecknazgul: 100G (1.6%, 1,252G)
ApplesauceBoss: 100G (1.6%, 13,425G)

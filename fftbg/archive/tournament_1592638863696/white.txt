Player: !White
Team: White Team
Palettes: White/Blue



0v3rr8d
Female
Aries
73
80
Ninja
Basic Skill
MA Save
Maintenance
Jump+2

Morning Star
Mage Masher
Headgear
Chain Vest
Wizard Mantle

Shuriken, Bomb, Dictionary
Throw Stone, Heal, Cheer Up, Wish



OpHendoslice
Male
Scorpio
71
77
Knight
Punch Art
Catch
Equip Bow
Move-MP Up

Long Bow
Round Shield
Gold Helmet
Chain Mail
Red Shoes

Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword
Purification, Revive, Seal Evil



Ruebyy
Male
Virgo
65
58
Knight
Sing
Parry
Defend
Move-MP Up

Ice Brand
Ice Shield
Cross Helmet
Reflect Mail
Germinas Boots

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Dark Sword
Angel Song, Nameless Song, Sky Demon



Vorap
Female
Leo
68
43
Mime

Caution
Equip Armor
Waterbreathing



Crystal Helmet
Chameleon Robe
108 Gems

Mimic


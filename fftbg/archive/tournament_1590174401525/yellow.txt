Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Aznwanderor
Male
Capricorn
59
65
Calculator
Yin Yang Magic
MA Save
Secret Hunt
Move-HP Up

Bestiary

Triangle Hat
Light Robe
Reflect Ring

CT, Height, Prime Number, 5, 3
Zombie, Silence Song, Confusion Song, Dispel Magic



Vorap
Female
Pisces
59
51
Oracle
Jump
Mana Shield
Magic Attack UP
Move-HP Up

Battle Folio

Triangle Hat
Silk Robe
Elf Mantle

Blind, Spell Absorb, Life Drain, Dispel Magic, Paralyze, Sleep
Level Jump5, Vertical Jump8



Kronikle
Male
Aquarius
71
79
Knight
Summon Magic
Caution
Secret Hunt
Swim

Save the Queen
Buckler
Barbuta
Wizard Robe
Battle Boots

Head Break, Magic Break, Speed Break, Power Break, Dark Sword, Surging Sword
Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Odin, Leviathan



FoeSquirrel
Female
Sagittarius
49
78
Lancer
Time Magic
MP Restore
Dual Wield
Fly

Holy Lance
Ivory Rod
Diamond Helmet
Bronze Armor
Cursed Ring

Level Jump3, Vertical Jump6
Haste, Haste 2, Stop, Reflect

Player: !Green
Team: Green Team
Palettes: Green/White



DouglasDragonThePoet
Male
Taurus
66
55
Squire
Time Magic
Speed Save
Dual Wield
Move+1

Iron Sword
Ice Brand
Flash Hat
Adaman Vest
Battle Boots

Accumulate, Throw Stone, Heal, Yell, Wish
Quick, Meteor, Galaxy Stop



Prince Rogers Nelson
Female
Aries
49
36
Thief
Item
Catch
Halve MP
Swim

Mage Masher

Twist Headband
Secret Clothes
Magic Ring

Steal Helmet, Steal Accessory
Potion, Elixir, Antidote, Echo Grass, Soft, Holy Water, Remedy



NIghtdew14
Female
Libra
78
41
Dancer
Yin Yang Magic
Dragon Spirit
Dual Wield
Move+1

Persia
Ryozan Silk
Thief Hat
Judo Outfit
Feather Mantle

Witch Hunt, Slow Dance, Polka Polka, Void Storage, Nether Demon
Blind, Sleep



Ollie
Female
Pisces
78
65
Chemist
Time Magic
Counter Flood
Secret Hunt
Swim

Star Bag

Headgear
Judo Outfit
Sprint Shoes

Potion, X-Potion, Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Demi

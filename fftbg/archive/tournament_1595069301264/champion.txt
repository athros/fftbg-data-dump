Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



StealthModeLocke
Monster
Pisces
77
69
Tiamat










MuguTheMangler
Female
Serpentarius
76
69
Wizard
Battle Skill
Counter Tackle
Equip Bow
Teleport

Poison Bow

Leather Hat
Silk Robe
Reflect Ring

Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Death, Flare
Head Break, Shield Break, Weapon Break, Magic Break, Mind Break



Byrdturbo
Male
Scorpio
74
57
Bard
Elemental
Counter Tackle
Dual Wield
Levitate

Fairy Harp
Fairy Harp
Golden Hairpin
Carabini Mail
Bracer

Cheer Song, Last Song, Space Storage
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Upvla
Male
Capricorn
64
65
Mediator
Black Magic
Faith Save
Dual Wield
Move-MP Up

Papyrus Codex
Papyrus Codex
Ribbon
Light Robe
Battle Boots

Negotiate
Bolt, Ice, Ice 2, Ice 4, Death

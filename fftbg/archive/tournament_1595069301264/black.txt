Player: !Black
Team: Black Team
Palettes: Black/Red



Macrosoft Inc
Female
Virgo
76
72
Ninja
Item
Earplug
Throw Item
Jump+2

Morning Star
Ninja Edge
Leather Hat
Rubber Costume
Red Shoes

Bomb, Sword, Dictionary
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Randgridr
Monster
Taurus
78
61
Chocobo










VolgraTheMoose
Female
Cancer
48
58
Calculator
White Magic
Regenerator
Secret Hunt
Waterbreathing

Papyrus Codex

Red Hood
Wizard Robe
Genji Gauntlet

CT, 4, 3
Cure 2, Cure 3, Cure 4, Raise, Shell 2



Micheline
Female
Virgo
63
59
Wizard
Battle Skill
Caution
Equip Sword
Jump+1

Broad Sword

Leather Hat
Power Sleeve
Defense Ring

Fire, Fire 2, Bolt, Bolt 4, Ice 3, Empower
Power Break, Mind Break

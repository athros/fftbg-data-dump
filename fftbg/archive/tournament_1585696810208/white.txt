Player: !White
Team: White Team
Palettes: White/Blue



UmaiJam
Male
Aquarius
81
55
Time Mage
Battle Skill
Sunken State
Beastmaster
Jump+3

White Staff

Leather Hat
Black Robe
Cursed Ring

Haste, Slow, Reflect, Demi, Stabilize Time
Shield Break, Weapon Break, Magic Break, Power Break, Surging Sword



Daveb
Female
Virgo
43
71
Ninja
Battle Skill
Caution
Equip Gun
Teleport

Madlemgen
Bloody Strings
Headgear
Mythril Vest
Cursed Ring

Staff
Weapon Break, Power Break, Justice Sword, Night Sword



Lordminsc
Monster
Sagittarius
76
45
Ghost










SpaZmodeus
Male
Sagittarius
78
47
Mediator
Charge
Brave Up
Dual Wield
Move+2

Blind Knife
Mythril Knife
Flash Hat
Silk Robe
Power Wrist

Persuade, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Charge+1

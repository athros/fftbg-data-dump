Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Elelor
Male
Capricorn
58
80
Summoner
Steal
Caution
Equip Bow
Swim

Windslash Bow

Red Hood
Linen Robe
Feather Boots

Titan, Golem, Carbunkle, Bahamut, Odin, Leviathan, Silf
Gil Taking, Steal Accessory, Arm Aim, Leg Aim



Trix04
Male
Cancer
77
54
Calculator
Bird Skill
Sunken State
Secret Hunt
Move-MP Up

Battle Folio

Mythril Helmet
Chameleon Robe
Dracula Mantle

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



E Ballard
Female
Sagittarius
64
69
Time Mage
Steal
HP Restore
Maintenance
Lava Walking

Mace of Zeus

Leather Hat
Silk Robe
Power Wrist

Haste, Immobilize, Float, Stabilize Time
Steal Shield, Steal Weapon, Arm Aim



Imranimran
Male
Gemini
43
72
Knight
Item
Absorb Used MP
Magic Attack UP
Lava Walking

Ragnarok

Cross Helmet
Plate Mail
Defense Ring

Head Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword
Potion, Hi-Potion, X-Potion, Echo Grass, Holy Water, Phoenix Down

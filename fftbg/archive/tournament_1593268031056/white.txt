Player: !White
Team: White Team
Palettes: White/Blue



Fenaen
Female
Capricorn
71
56
Calculator
Giant Skill
MP Restore
Secret Hunt
Jump+2

Papyrus Codex

Feather Hat
Mythril Armor
Elf Mantle

Blue Magic
Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest



Cam ATS
Female
Taurus
45
54
Geomancer
Time Magic
Parry
Concentrate
Move+2

Sleep Sword
Bronze Shield
Holy Miter
Wizard Outfit
108 Gems

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Stop, Reflect, Demi 2, Stabilize Time



Nifboy
Female
Serpentarius
52
57
Monk
Dance
Critical Quick
Sicken
Fly



Red Hood
Mythril Vest
Germinas Boots

Wave Fist, Chakra
Slow Dance, Polka Polka, Obsidian Blade



Grininda
Male
Virgo
55
80
Mediator
Steal
MP Restore
Equip Knife
Jump+3

Sasuke Knife

Triangle Hat
Wizard Outfit
Power Wrist

Threaten, Death Sentence, Mimic Daravon, Refute
Steal Heart, Steal Accessory

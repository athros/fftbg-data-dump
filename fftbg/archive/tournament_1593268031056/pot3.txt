Final Bets: white - 9 bets for 4,193G (43.8%, x1.29); black - 7 bets for 5,389G (56.2%, x0.78)

white bets:
evontno: 1,301G (31.0%, 1,301G)
fenaen: 1,200G (28.6%, 35,639G)
killth3kid: 500G (11.9%, 10,994G)
Zagorsek: 323G (7.7%, 323G)
getthemoneyz: 268G (6.4%, 1,077,144G)
gorgewall: 201G (4.8%, 25,656G)
Forkmore: 200G (4.8%, 200G)
Blastinus: 100G (2.4%, 727G)
datadrivenbot: 100G (2.4%, 46,762G)

black bets:
RampagingRobot: 1,025G (19.0%, 1,025G)
GladiatorLupe: 1,000G (18.6%, 9,980G)
BirbBrainsBot: 1,000G (18.6%, 67,467G)
E_Ballard: 932G (17.3%, 932G)
prince_rogers_nelson_: 832G (15.4%, 832G)
CosmicTactician: 500G (9.3%, 20,054G)
letdowncity: 100G (1.9%, 9,904G)

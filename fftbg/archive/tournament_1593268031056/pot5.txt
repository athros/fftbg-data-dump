Final Bets: red - 9 bets for 4,183G (65.7%, x0.52); yellow - 10 bets for 2,186G (34.3%, x1.91)

red bets:
DeathTaxesAndAnime: 1,092G (26.1%, 1,092G)
BirbBrainsBot: 1,000G (23.9%, 67,245G)
prince_rogers_nelson_: 600G (14.3%, 600G)
serperemagus: 552G (13.2%, 552G)
alterworlds: 438G (10.5%, 438G)
gorgewall: 201G (4.8%, 25,609G)
nifboy: 100G (2.4%, 2,139G)
letdowncity: 100G (2.4%, 9,882G)
datadrivenbot: 100G (2.4%, 46,562G)

yellow bets:
dogsandcatsand: 500G (22.9%, 20,000G)
roqqqpsi: 323G (14.8%, 32,337G)
Zagorsek: 320G (14.6%, 320G)
getthemoneyz: 220G (10.1%, 1,077,642G)
Forkmore: 200G (9.1%, 200G)
GladiatorLupe: 200G (9.1%, 10,762G)
RampagingRobot: 112G (5.1%, 112G)
Baron_von_Scrub: 111G (5.1%, 9,443G)
Evewho: 100G (4.6%, 1,742G)
CosmicTactician: 100G (4.6%, 20,596G)

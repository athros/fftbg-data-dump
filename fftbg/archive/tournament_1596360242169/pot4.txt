Final Bets: purple - 13 bets for 16,364G (83.2%, x0.20); brown - 6 bets for 3,301G (16.8%, x4.96)

purple bets:
douchetron: 5,072G (31.0%, 9,946G)
randgridr: 3,727G (22.8%, 3,727G)
DeathTaxesAndAnime: 1,397G (8.5%, 2,741G)
CT_5_Holy: 1,226G (7.5%, 1,226G)
ColetteMSLP: 1,000G (6.1%, 23,767G)
WhattayaBrian: 1,000G (6.1%, 12,196G)
dem0nj0ns: 922G (5.6%, 922G)
ZephyrBurst83: 500G (3.1%, 5,416G)
Mister_Quof: 500G (3.1%, 3,930G)
Lemonjohns: 320G (2.0%, 320G)
maximumcrit: 300G (1.8%, 12,409G)
AllInBot: 200G (1.2%, 200G)
datadrivenbot: 200G (1.2%, 50,146G)

brown bets:
BirbBrainsBot: 1,000G (30.3%, 125,047G)
getthemoneyz: 1,000G (30.3%, 1,468,544G)
fluffskull: 500G (15.1%, 19,368G)
Seaweed_B: 500G (15.1%, 26,972G)
gorgewall: 201G (6.1%, 7,294G)
StrawbreeRed: 100G (3.0%, 100G)

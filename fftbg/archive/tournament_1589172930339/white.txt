Player: !White
Team: White Team
Palettes: White/Blue



Jaritras
Female
Leo
51
52
Chemist
Jump
Counter Flood
Defend
Move-HP Up

Zorlin Shape

Golden Hairpin
Brigandine
Leather Mantle

Potion, Hi-Potion, X-Potion, Holy Water, Phoenix Down
Level Jump5, Vertical Jump5



Theanonymouslurker
Female
Cancer
60
59
Knight
Summon Magic
Abandon
Dual Wield
Move+1

Battle Axe
Battle Axe
Circlet
Mythril Armor
Defense Armlet

Head Break, Shield Break, Dark Sword
Shiva, Titan, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Cyclops



Sukotsuto
Female
Sagittarius
78
56
Chemist
White Magic
Arrow Guard
Equip Polearm
Jump+2

Ryozan Silk

Black Hood
Brigandine
Wizard Mantle

Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down
Cure, Reraise, Esuna



JIDkomu
Female
Cancer
72
61
Wizard
Basic Skill
MP Restore
Secret Hunt
Move+3

Rod

Golden Hairpin
Black Costume
Setiemson

Fire 2, Fire 4, Bolt, Bolt 4, Ice 3, Ice 4, Death
Accumulate, Dash, Tickle, Yell, Fury, Ultima

Player: !Red
Team: Red Team
Palettes: Red/Brown



Luna XIV
Female
Sagittarius
74
52
Time Mage
Item
Caution
Dual Wield
Jump+1

Cypress Rod
Battle Bamboo
Black Hood
Light Robe
Spike Shoes

Haste, Haste 2, Slow, Stop, Float, Reflect, Quick, Stabilize Time
Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down



Estan AD
Female
Sagittarius
56
42
Priest
Dance
Arrow Guard
Magic Defense UP
Swim

Gold Staff

Black Hood
Linen Robe
N-Kai Armlet

Cure 2, Cure 4, Raise, Protect 2, Esuna, Magic Barrier
Polka Polka, Void Storage



TimeJannies
Male
Serpentarius
73
75
Ninja
Item
Dragon Spirit
Equip Gun
Move-HP Up

Papyrus Codex
Ramia Harp
Flash Hat
Adaman Vest
Magic Gauntlet

Shuriken, Bomb, Wand
Potion, Hi-Potion, X-Potion, Ether, Phoenix Down



Powergems
Male
Scorpio
50
62
Squire
White Magic
Arrow Guard
Attack UP
Ignore Terrain

Bow Gun
Buckler
Thief Hat
Chain Mail
Feather Mantle

Dash, Heal, Wish
Cure, Cure 3, Protect 2, Shell, Shell 2

Player: !Red
Team: Red Team
Palettes: Red/Brown



Braveboyblue
Male
Sagittarius
45
46
Thief
Draw Out
Brave Save
Equip Shield
Jump+2

Iga Knife
Diamond Shield
Feather Hat
Leather Outfit
Small Mantle

Steal Heart, Arm Aim
Bizen Boat, Murasame, Masamune



Randgridr
Male
Taurus
55
58
Chemist
Battle Skill
Abandon
Martial Arts
Ignore Terrain

Blind Knife

Thief Hat
Mystic Vest
Genji Gauntlet

Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Magic Break



Deathmaker06
Male
Scorpio
73
52
Ninja
Battle Skill
Damage Split
Equip Gun
Teleport

Bloody Strings
Battle Folio
Twist Headband
Chain Vest
Bracer

Shuriken, Knife, Hammer, Staff
Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword



Laserman1000
Female
Pisces
55
68
Mime

Counter
Maintenance
Move+1



Black Hood
Power Sleeve
Magic Gauntlet

Mimic


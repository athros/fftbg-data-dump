Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Luminarii
Male
Gemini
61
68
Samurai
Steal
Absorb Used MP
Martial Arts
Move+3

Koutetsu Knife

Circlet
Crystal Mail
Bracer

Bizen Boat, Heaven's Cloud, Kiyomori
Gil Taking, Steal Shield, Steal Accessory



Gelwain
Monster
Sagittarius
43
57
Pisco Demon










Dexef
Female
Sagittarius
53
60
Wizard
Summon Magic
Earplug
Magic Attack UP
Move-MP Up

Wizard Rod

Green Beret
Adaman Vest
Small Mantle

Fire 2, Fire 4, Bolt 4, Ice 2, Ice 4
Moogle, Ramuh, Ifrit, Carbunkle, Silf, Cyclops



Ricky Ortease
Female
Virgo
60
64
Summoner
Battle Skill
Absorb Used MP
Magic Defense UP
Ignore Height

Sage Staff

Holy Miter
White Robe
Power Wrist

Moogle, Shiva, Bahamut, Leviathan
Head Break, Armor Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Justice Sword

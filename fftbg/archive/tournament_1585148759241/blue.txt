Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Thed63
Female
Aquarius
51
42
Mime

Counter
Attack UP
Fly



Headgear
Judo Outfit
Power Wrist

Mimic




Ryokubaka
Female
Aries
51
55
Lancer
Throw
PA Save
Equip Gun
Move+2

Blaze Gun
Crystal Shield
Genji Helmet
Diamond Armor
Power Wrist

Level Jump2, Vertical Jump3
Sword, Staff



Bryon W
Male
Gemini
55
56
Chemist
Time Magic
Meatbone Slash
Sicken
Teleport

Panther Bag

Red Hood
Rubber Costume
Power Wrist

Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Haste, Haste 2, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Demi, Demi 2



MalakoFox
Female
Sagittarius
44
52
Thief
Yin Yang Magic
Absorb Used MP
Magic Defense UP
Retreat

Main Gauche

Red Hood
Mythril Vest
Feather Boots

Gil Taking, Steal Heart, Steal Armor, Steal Shield
Life Drain, Zombie, Foxbird, Paralyze, Dark Holy

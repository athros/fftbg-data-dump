Player: !Green
Team: Green Team
Palettes: Green/White



Draconis345
Male
Cancer
57
52
Monk
Sing
Counter
Dual Wield
Move-HP Up



Black Hood
Earth Clothes
Rubber Shoes

Spin Fist, Wave Fist, Purification, Chakra
Magic Song, Diamond Blade



Rocl
Male
Scorpio
49
51
Priest
Time Magic
Meatbone Slash
Sicken
Jump+2

Morning Star

Ribbon
Wizard Robe
Elf Mantle

Cure, Cure 3, Raise, Reraise, Regen, Protect 2, Wall, Esuna
Stop, Immobilize, Float, Quick, Demi 2, Stabilize Time



TheKillerNacho
Male
Pisces
71
50
Oracle
Throw
MA Save
Equip Axe
Levitate

Scorpion Tail

Flash Hat
White Robe
Feather Boots

Poison, Doubt Faith, Zombie, Dispel Magic, Paralyze
Axe, Spear, Dictionary



Greghatch
Male
Leo
39
51
Ninja
Basic Skill
Blade Grasp
Equip Polearm
Waterwalking

Obelisk
Musk Rod
Golden Hairpin
Leather Outfit
Power Wrist

Shuriken
Heal, Tickle, Fury

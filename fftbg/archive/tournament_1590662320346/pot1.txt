Final Bets: red - 9 bets for 2,259G (31.4%, x2.19); blue - 9 bets for 4,940G (68.6%, x0.46)

red bets:
rednecknazgul: 497G (22.0%, 497G)
BirbBrainsBot: 477G (21.1%, 39,836G)
ShintaroNayaka: 380G (16.8%, 380G)
thenextlantern: 204G (9.0%, 204G)
gorgewall: 201G (8.9%, 20,345G)
Zachara: 200G (8.9%, 91,000G)
victoriolue: 100G (4.4%, 5,544G)
datadrivenbot: 100G (4.4%, 27,802G)
Sharosa: 100G (4.4%, 759G)

blue bets:
SeniorBunk: 1,000G (20.2%, 17,329G)
NicoSavoy: 1,000G (20.2%, 19,304G)
josephiroth_143: 1,000G (20.2%, 2,491G)
CrownOfHorns: 463G (9.4%, 463G)
DAC169: 444G (9.0%, 1,548G)
HASTERIOUS: 369G (7.5%, 7,384G)
LivingHitokiri: 364G (7.4%, 364G)
Klednar21: 200G (4.0%, 323G)
AllInBot: 100G (2.0%, 100G)

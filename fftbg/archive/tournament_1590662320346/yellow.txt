Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



NovaKnight21
Male
Aquarius
77
80
Mediator
Item
Arrow Guard
Short Status
Levitate

Glacier Gun

Golden Hairpin
Secret Clothes
Defense Ring

Praise, Threaten, Preach, Mimic Daravon, Refute, Rehabilitate
Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



Ar Tactic
Monster
Aquarius
45
78
Cockatrice










Josephiroth 143
Female
Scorpio
75
74
Archer
Yin Yang Magic
Catch
Halve MP
Move+1

Blaze Gun
Diamond Shield
Genji Helmet
Wizard Outfit
Reflect Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7, Charge+20
Life Drain, Blind Rage, Foxbird, Petrify



Gorgewall
Female
Taurus
59
67
Oracle
Basic Skill
Absorb Used MP
Halve MP
Levitate

Papyrus Codex

Thief Hat
Black Robe
Defense Ring

Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze
Accumulate, Dash, Heal, Cheer Up, Fury

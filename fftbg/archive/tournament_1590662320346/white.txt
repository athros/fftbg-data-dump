Player: !White
Team: White Team
Palettes: White/Blue



CosmicTactician
Female
Sagittarius
48
69
Chemist
Jump
Catch
Equip Armor
Jump+1

Panther Bag

Thief Hat
Chameleon Robe
N-Kai Armlet

X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Level Jump2, Vertical Jump8



ColetteMSLP
Female
Leo
49
74
Time Mage
Yin Yang Magic
Critical Quick
Dual Wield
Waterbreathing

Iron Fan
Octagon Rod
Holy Miter
Wizard Robe
Defense Armlet

Stop, Float, Reflect, Stabilize Time, Meteor
Poison, Silence Song, Paralyze, Dark Holy



Kyune
Male
Sagittarius
75
63
Summoner
Basic Skill
Auto Potion
Short Charge
Retreat

Bestiary

Golden Hairpin
Light Robe
Spike Shoes

Moogle, Ramuh, Titan, Golem, Carbunkle
Dash, Heal, Tickle, Cheer Up



AniZero
Female
Leo
62
76
Chemist
Battle Skill
Absorb Used MP
Dual Wield
Lava Walking

Hydra Bag
Cute Bag
Feather Hat
Mythril Vest
Power Wrist

Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Head Break, Power Break, Justice Sword

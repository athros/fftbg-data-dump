Player: !Red
Team: Red Team
Palettes: Red/Brown



LordPaimon
Male
Cancer
60
47
Mediator
Punch Art
Earplug
Equip Armor
Move+2

Orichalcum

Barbuta
Leather Outfit
Magic Gauntlet

Invitation, Threaten, Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate
Spin Fist, Purification, Chakra, Revive



Adualion
Male
Aries
70
44
Samurai
Summon Magic
HP Restore
Doublehand
Waterbreathing

Murasame

Bronze Helmet
Linen Cuirass
Small Mantle

Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji
Moogle, Ramuh, Ifrit, Golem, Leviathan, Lich



Zalivandis
Male
Virgo
55
49
Priest
Item
Parry
Throw Item
Swim

Oak Staff

Barette
Linen Robe
108 Gems

Cure 3, Raise, Reraise, Regen, Esuna
Potion, Ether, Antidote, Holy Water, Remedy



00greenbean00
Female
Sagittarius
53
40
Priest
Draw Out
Critical Quick
Defend
Move+1

Gold Staff

Holy Miter
Adaman Vest
Bracer

Cure 2, Cure 4, Raise, Regen, Protect
Asura, Koutetsu, Bizen Boat, Murasame, Kikuichimoji

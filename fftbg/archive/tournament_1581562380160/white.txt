Player: !White
Team: White Team
Palettes: White/Blue



ShinoGuy
Male
Scorpio
68
69
Mime

Counter Magic
Dual Wield
Move+2



Feather Hat
Brigandine
Cursed Ring

Mimic




DeathTaxesAndAnime
Female
Gemini
81
45
Ninja
Jump
Abandon
Equip Bow
Levitate

Silver Bow

Leather Hat
Adaman Vest
Cursed Ring

Shuriken, Knife
Level Jump2, Vertical Jump5



RRazza
Male
Gemini
57
60
Time Mage
Talk Skill
MP Restore
Beastmaster
Move+1

Rainbow Staff

Green Beret
Earth Clothes
108 Gems

Haste, Haste 2, Float, Reflect, Quick, Stabilize Time
Invitation, Threaten, Solution, Death Sentence, Refute



Dasutin23
Female
Virgo
77
79
Ninja
Jump
Auto Potion
Secret Hunt
Teleport

Short Edge
Sasuke Knife
Black Hood
Clothes
Genji Gauntlet

Knife, Ninja Sword, Dictionary
Level Jump2, Vertical Jump8

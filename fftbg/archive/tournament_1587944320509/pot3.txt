Final Bets: white - 22 bets for 14,296G (53.5%, x0.87); black - 23 bets for 12,412G (46.5%, x1.15)

white bets:
coralreeferz: 3,500G (24.5%, 29,214G)
Mtueni: 1,000G (7.0%, 286,986G)
Aldrammech: 1,000G (7.0%, 56,424G)
BirbBrainsBot: 1,000G (7.0%, 40,215G)
gooseyourself: 1,000G (7.0%, 29,569G)
Zeroroute: 869G (6.1%, 869G)
ungabunga_bot: 785G (5.5%, 265,342G)
NovaKnight21: 637G (4.5%, 637G)
TheMurkGnome: 510G (3.6%, 15,410G)
JumbocactuarX27: 500G (3.5%, 5,243G)
TrueRhyme: 500G (3.5%, 966G)
pandasforsale: 444G (3.1%, 27,490G)
DLJuggernaut: 400G (2.8%, 9,605G)
KasugaiRoastedPeas: 400G (2.8%, 4,103G)
getthemoneyz: 360G (2.5%, 584,168G)
ANFz: 256G (1.8%, 19,139G)
FoxtrotNovemberCharlie: 256G (1.8%, 256G)
TheRylander: 240G (1.7%, 240G)
skuldar: 239G (1.7%, 239G)
RunicMagus: 200G (1.4%, 52,132G)
Bojanges: 100G (0.7%, 5,002G)
DontBackseatGame: 100G (0.7%, 2,128G)

black bets:
holyonline: 2,450G (19.7%, 4,901G)
nuka1120: 1,365G (11.0%, 1,365G)
WireLord: 1,000G (8.1%, 18,279G)
fenixcrest: 946G (7.6%, 9,462G)
Laserman1000: 649G (5.2%, 50,749G)
LordSDB: 603G (4.9%, 603G)
LivingHitokiri: 500G (4.0%, 2,040G)
run_with_STONE_GUNS: 500G (4.0%, 4,905G)
joewcarson: 500G (4.0%, 11,928G)
Breakdown777: 500G (4.0%, 26,751G)
HaateXIII: 500G (4.0%, 78,469G)
mirapoix: 444G (3.6%, 444G)
ZephyrTempest: 400G (3.2%, 920G)
Anethum: 400G (3.2%, 4,468G)
volgrathemoose: 360G (2.9%, 360G)
HaplessOne: 304G (2.4%, 304G)
twelfthrootoftwo: 200G (1.6%, 866G)
Tetsuwan_Melty: 197G (1.6%, 197G)
arch8000: 162G (1.3%, 5,433G)
TheJonTerp: 132G (1.1%, 132G)
Conome: 100G (0.8%, 1,635G)
Jerboj: 100G (0.8%, 56,946G)
datadrivenbot: 100G (0.8%, 4,010G)

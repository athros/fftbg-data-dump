Final Bets: red - 10 bets for 6,480G (64.4%, x0.55); blue - 9 bets for 3,575G (35.6%, x1.81)

red bets:
ForagerCats: 1,000G (15.4%, 9,161G)
porkchop5158: 1,000G (15.4%, 38,106G)
Sairentozon7: 1,000G (15.4%, 15,793G)
WitchHunterIX: 1,000G (15.4%, 19,961G)
randgridr: 569G (8.8%, 569G)
BoneMiser: 511G (7.9%, 511G)
Evewho: 500G (7.7%, 4,390G)
ShintaroNayaka: 400G (6.2%, 8,105G)
DustBirdEX: 300G (4.6%, 5,188G)
datadrivenbot: 200G (3.1%, 29,104G)

blue bets:
BirbBrainsBot: 1,000G (28.0%, 189,076G)
twelfthrootoftwo: 548G (15.3%, 548G)
prince_rogers_nelson_: 511G (14.3%, 511G)
Lanshaft: 488G (13.7%, 19,339G)
superdevon1: 444G (12.4%, 8,888G)
JLinkletter: 239G (6.7%, 239G)
AllInBot: 200G (5.6%, 200G)
Lydian_C: 121G (3.4%, 82,755G)
getthemoneyz: 24G (0.7%, 1,304,820G)

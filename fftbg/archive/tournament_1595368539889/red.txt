Player: !Red
Team: Red Team
Palettes: Red/Brown



DeathTaxesAndAnime
Female
Cancer
51
79
Mime

Damage Split
Doublehand
Jump+3



Triangle Hat
Secret Clothes
Leather Mantle

Mimic




Laserman1000
Male
Leo
60
69
Lancer
Throw
Abandon
Equip Knife
Ignore Terrain

Dragon Rod
Bronze Shield
Cross Helmet
Robe of Lords
Spike Shoes

Level Jump8, Vertical Jump6
Ninja Sword



Shalloween
Female
Taurus
54
46
Mime

MP Restore
Dual Wield
Waterbreathing



Leather Hat
Mythril Vest
Feather Boots

Mimic




NotBalrog
Male
Scorpio
62
66
Archer
Punch Art
MA Save
Equip Bow
Jump+3

Cute Bag
Mythril Shield
Golden Hairpin
Brigandine
Salty Rage

Charge+5, Charge+7
Wave Fist, Purification, Revive, Seal Evil

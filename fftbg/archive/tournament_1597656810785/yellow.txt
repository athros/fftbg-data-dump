Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Starfire6789
Monster
Virgo
55
41
Mindflayer










Nine07
Female
Aries
67
79
Calculator
Foul Skill
Dragon Spirit
Equip Axe
Waterwalking

Flame Whip
Round Shield
Diamond Helmet
Crystal Mail
Dracula Mantle

Blue Magic
Straight Dash, Oink, Toot, Snort, Bequeath Bacon, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



ArfArfArfArfArfArfArfArf
Female
Aries
42
54
Mediator
Steal
Dragon Spirit
Defense UP
Jump+1

Battle Folio

Holy Miter
Black Costume
108 Gems

Invitation, Persuade, Praise, Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Steal Heart, Steal Status



Chaddie
Male
Capricorn
69
53
Priest
Steal
HP Restore
Sicken
Levitate

Morning Star

Holy Miter
Wizard Robe
Dracula Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Shell 2, Esuna
Steal Weapon, Steal Accessory

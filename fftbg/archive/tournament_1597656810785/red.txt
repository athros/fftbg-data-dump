Player: !Red
Team: Red Team
Palettes: Red/Brown



Bioticism
Female
Gemini
56
61
Lancer
Time Magic
Arrow Guard
Long Status
Move+1

Holy Lance
Bronze Shield
Mythril Helmet
Reflect Mail
Jade Armlet

Level Jump4, Vertical Jump6
Haste, Slow, Quick, Stabilize Time



Ko2q
Female
Leo
39
74
Oracle
Black Magic
Counter
Short Status
Swim

Musk Rod

Ribbon
Mystic Vest
Cursed Ring

Poison, Life Drain, Silence Song, Dispel Magic, Paralyze, Petrify, Dark Holy
Fire 2, Ice 4



VolgraTheMoose
Male
Aquarius
71
48
Knight
Throw
MA Save
Dual Wield
Jump+3

Slasher
Giant Axe
Leather Helmet
Plate Mail
Elf Mantle

Head Break, Armor Break, Shield Break, Speed Break, Power Break, Mind Break, Justice Sword
Ninja Sword, Wand



Spiro
Male
Aries
60
63
Chemist
Punch Art
Earplug
Martial Arts
Ignore Terrain

Cultist Dagger

Twist Headband
Black Costume
Angel Ring

Potion, Ether, Eye Drop, Remedy, Phoenix Down
Earth Slash, Secret Fist, Chakra

Final Bets: green - 19 bets for 7,874G (35.9%, x1.79); yellow - 16 bets for 14,069G (64.1%, x0.56)

green bets:
TheChainNerd: 1,000G (12.7%, 7,952G)
NIghtdew14: 1,000G (12.7%, 9,012G)
BirbBrainsBot: 1,000G (12.7%, 46,902G)
UmaiJam: 1,000G (12.7%, 3,436G)
JumbocactuarX27: 844G (10.7%, 844G)
Kezia444: 500G (6.4%, 3,523G)
Cryptopsy70: 452G (5.7%, 4,100G)
DavenIII: 446G (5.7%, 892G)
Estan_AD: 328G (4.2%, 328G)
CapnChaos12: 300G (3.8%, 1,868G)
goodnightrobo: 244G (3.1%, 244G)
skillomono: 200G (2.5%, 1,494G)
bluuuuuuuh: 100G (1.3%, 4,766G)
OneHundredFists: 100G (1.3%, 1,699G)
Nizaha: 100G (1.3%, 15,112G)
datadrivenbot: 100G (1.3%, 24,536G)
Kyune: 100G (1.3%, 16,248G)
l0rdspug: 50G (0.6%, 401G)
getthemoneyz: 10G (0.1%, 685,633G)

yellow bets:
CorpusCav: 3,791G (26.9%, 7,582G)
Zeroroute: 1,666G (11.8%, 1,666G)
nifboy: 1,451G (10.3%, 1,451G)
DustBirdEX: 1,234G (8.8%, 32,913G)
Tsuike: 1,160G (8.2%, 2,321G)
EnemyController: 1,000G (7.1%, 287,188G)
FriendSquirrel: 744G (5.3%, 744G)
Firesheath: 648G (4.6%, 648G)
E_Ballard: 615G (4.4%, 615G)
holyonline: 500G (3.6%, 7,856G)
bruubarg: 353G (2.5%, 1,153G)
HorusTaurus: 312G (2.2%, 312G)
Drusic: 244G (1.7%, 244G)
brokenknight201: 200G (1.4%, 911G)
itszaane: 100G (0.7%, 1,026G)
DrAntiSocial: 51G (0.4%, 18,442G)

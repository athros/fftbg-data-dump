Player: !Brown
Team: Brown Team
Palettes: Brown/Green



RubenFlonne
Female
Scorpio
50
51
Summoner
Time Magic
Damage Split
Short Charge
Ignore Terrain

Wizard Rod

Black Hood
Silk Robe
Magic Gauntlet

Moogle, Shiva, Ifrit, Golem, Bahamut, Leviathan, Fairy
Haste, Immobilize, Float



ZephyrTempest
Male
Aries
52
79
Mediator
Basic Skill
HP Restore
Maintenance
Move+1

Main Gauche

Black Hood
Black Robe
Leather Mantle

Preach, Refute
Heal, Fury, Ultima



Daveb
Female
Libra
67
76
Ninja
Summon Magic
Sunken State
Short Status
Move+3

Hydra Bag
Flail
Triangle Hat
Brigandine
Bracer

Shuriken, Dictionary
Moogle, Shiva, Ramuh, Titan, Carbunkle, Bahamut, Odin, Salamander, Fairy, Lich



Cataphract116
Female
Virgo
70
65
Chemist
Talk Skill
Counter
Beastmaster
Lava Walking

Panther Bag

Green Beret
Adaman Vest
Sprint Shoes

Potion, X-Potion, Ether, Phoenix Down
Praise, Refute

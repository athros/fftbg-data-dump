Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thaetreis
Monster
Leo
58
46
Blue Dragon










MalakoFox
Female
Taurus
68
60
Chemist
Draw Out
Counter
Equip Armor
Jump+3

Mythril Gun

Circlet
Plate Mail
Genji Gauntlet

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Asura, Koutetsu, Muramasa, Kikuichimoji



Hasterious
Male
Serpentarius
66
51
Samurai
Black Magic
Parry
Attack UP
Move+3

Murasame

Iron Helmet
Platinum Armor
Elf Mantle

Asura, Murasame, Heaven's Cloud, Kikuichimoji
Bolt, Ice 2, Flare



Nickelbank
Female
Sagittarius
69
68
Geomancer
Jump
Regenerator
Magic Defense UP
Levitate

Slasher
Diamond Shield
Twist Headband
Wizard Robe
Rubber Shoes

Pitfall, Water Ball, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump8

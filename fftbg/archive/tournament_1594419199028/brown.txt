Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Cam ATS
Female
Sagittarius
45
59
Time Mage
Draw Out
Meatbone Slash
Concentrate
Waterbreathing

Wizard Staff

Holy Miter
Black Robe
Red Shoes

Haste, Haste 2, Immobilize, Float, Demi 2, Stabilize Time, Meteor
Koutetsu, Bizen Boat, Kiyomori



MaraDyne
Female
Cancer
75
37
Dancer
Throw
Absorb Used MP
Equip Shield
Swim

Persia
Diamond Shield
Headgear
Light Robe
Wizard Mantle

Wiznaibus, Nameless Dance, Obsidian Blade
Shuriken, Knife



HaateXIII
Male
Capricorn
39
64
Time Mage
Summon Magic
Faith Save
Magic Defense UP
Jump+1

Healing Staff

Golden Hairpin
Wizard Outfit
N-Kai Armlet

Haste, Slow 2, Stop, Immobilize, Reflect, Quick, Demi
Moogle, Ramuh, Ifrit, Titan, Salamander, Silf, Lich



ArchKnightX
Male
Virgo
56
60
Squire
Charge
MP Restore
Magic Defense UP
Move+2

Coral Sword
Crystal Shield
Barbuta
Chain Vest
Sprint Shoes

Accumulate, Heal
Charge+1, Charge+3, Charge+4, Charge+20

Player: !Black
Team: Black Team
Palettes: Black/Red



Gogofromtogo
Male
Libra
80
51
Monk
Black Magic
Distribute
Short Charge
Move-HP Up



Leather Hat
Secret Clothes
Red Shoes

Secret Fist, Purification, Revive
Fire 3, Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Ice 4



Conradharlock
Male
Gemini
61
57
Oracle
Basic Skill
Abandon
Equip Bow
Move+2

Lightning Bow

Cachusha
Leather Outfit
Magic Ring

Pray Faith, Doubt Faith, Silence Song, Confusion Song, Paralyze, Dark Holy
Heal, Yell, Wish, Scream



Just Here2
Male
Aquarius
62
81
Archer
Elemental
Dragon Spirit
Doublehand
Levitate

Bow Gun

Feather Hat
Wizard Outfit
Germinas Boots

Charge+1, Charge+2
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Deathmaker06
Female
Sagittarius
60
51
Time Mage
Dance
Counter
Equip Sword
Fly

Koutetsu Knife

Thief Hat
Black Robe
Rubber Shoes

Haste 2, Quick, Stabilize Time
Polka Polka, Disillusion, Obsidian Blade

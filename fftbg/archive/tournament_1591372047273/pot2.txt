Final Bets: green - 17 bets for 7,147G (56.4%, x0.77); yellow - 9 bets for 5,529G (43.6%, x1.29)

green bets:
Saldarin: 1,100G (15.4%, 1,100G)
Spuzzmocker: 1,086G (15.2%, 1,086G)
TheChainNerd: 725G (10.1%, 3,625G)
NovaKnight21: 688G (9.6%, 688G)
Laserman1000: 600G (8.4%, 26,600G)
ThePineappleSalesman: 384G (5.4%, 384G)
raventwynn: 331G (4.6%, 2,209G)
killth3kid: 328G (4.6%, 5,644G)
joewcarson: 300G (4.2%, 29,553G)
CapnChaos12: 300G (4.2%, 24,145G)
Grandlanzer: 288G (4.0%, 288G)
mistermastergreg: 250G (3.5%, 876G)
MiseryKoss: 247G (3.5%, 2,475G)
Flameatron: 200G (2.8%, 4,899G)
Lydian_C: 120G (1.7%, 11,835G)
lastly: 100G (1.4%, 14,374G)
datadrivenbot: 100G (1.4%, 32,663G)

yellow bets:
DavenIII: 1,388G (25.1%, 1,388G)
mpghappiness: 1,001G (18.1%, 7,024G)
BirbBrainsBot: 1,000G (18.1%, 90,754G)
DudeMonkey77: 816G (14.8%, 816G)
DustBirdEX: 588G (10.6%, 588G)
FriendSquirrel: 426G (7.7%, 2,374G)
getthemoneyz: 110G (2.0%, 829,799G)
ruleof5: 100G (1.8%, 1,995G)
El_Stavos: 100G (1.8%, 2,045G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nifboy
Female
Cancer
53
37
Summoner
White Magic
Damage Split
Equip Bow
Levitate

Night Killer

Black Hood
Secret Clothes
Feather Mantle

Shiva, Ifrit, Titan, Golem, Carbunkle, Odin
Cure, Cure 3, Cure 4, Raise, Reraise, Wall, Esuna



Kronikle
Female
Aquarius
37
57
Geomancer
Draw Out
Absorb Used MP
Magic Attack UP
Move+3

Muramasa
Hero Shield
Feather Hat
Chameleon Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Bizen Boat, Muramasa



Flameatron
Male
Scorpio
79
50
Lancer
Draw Out
Regenerator
Dual Wield
Move+2

Battle Bamboo
Mythril Spear
Cross Helmet
Leather Armor
Elf Mantle

Level Jump4, Vertical Jump8
Murasame



TheMM42
Female
Virgo
48
41
Priest
Time Magic
Speed Save
Magic Defense UP
Levitate

Wizard Staff

Red Hood
Mystic Vest
Dracula Mantle

Raise, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna
Haste, Immobilize, Reflect, Demi, Demi 2, Stabilize Time

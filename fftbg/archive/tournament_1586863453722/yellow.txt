Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Defaultlybrave
Female
Taurus
64
60
Knight
Item
HP Restore
Throw Item
Swim

Battle Axe
Flame Shield
Circlet
Platinum Armor
Red Shoes

Speed Break, Justice Sword
Hi-Potion, Hi-Ether, Holy Water, Remedy



404namesnotfound
Female
Aries
78
54
Geomancer
Item
HP Restore
Throw Item
Swim

Battle Axe
Diamond Shield
Holy Miter
Brigandine
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Ether, Elixir, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



Leakimiko
Male
Leo
75
68
Archer
Elemental
Faith Up
Dual Wield
Move+3

Cross Bow
Bow Gun
Circlet
Earth Clothes
Elf Mantle

Charge+1, Charge+2, Charge+3, Charge+4
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Lava Ball



Mtueni
Female
Scorpio
78
64
Priest
Steal
Abandon
Doublehand
Swim

Rainbow Staff

Black Hood
Black Robe
Reflect Ring

Cure 3, Raise, Raise 2, Reraise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna, Magic Barrier
Gil Taking, Steal Shield

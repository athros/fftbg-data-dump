Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



NovaKnight21
Male
Gemini
62
51
Time Mage
Battle Skill
Earplug
Short Charge
Move+1

Oak Staff

Red Hood
Earth Clothes
Germinas Boots

Haste 2, Immobilize, Float, Reflect
Shield Break, Weapon Break, Magic Break, Speed Break, Stasis Sword



Tithonus
Male
Virgo
80
60
Bard
Draw Out
Abandon
Sicken
Levitate

Bloody Strings

Headgear
Reflect Mail
Sprint Shoes

Life Song, Cheer Song, Magic Song, Nameless Song, Diamond Blade, Space Storage
Koutetsu, Murasame, Kiyomori



Cataphract116
Female
Gemini
57
74
Mediator
Summon Magic
HP Restore
Attack UP
Waterbreathing

Madlemgen

Headgear
Brigandine
Genji Gauntlet

Negotiate, Refute, Rehabilitate
Moogle, Ifrit, Titan, Carbunkle, Silf, Fairy



HaplessOne
Female
Pisces
44
74
Wizard
Talk Skill
Brave Up
Short Charge
Teleport

Thunder Rod

Red Hood
Light Robe
Cursed Ring

Fire, Fire 2, Fire 3, Bolt, Bolt 4, Ice, Ice 2, Ice 3, Empower
Persuade, Praise, Mimic Daravon, Refute

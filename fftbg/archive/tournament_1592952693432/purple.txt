Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Taurus
77
63
Lancer
Battle Skill
Speed Save
Equip Gun
Levitate

Glacier Gun
Platinum Shield
Crystal Helmet
Mythril Armor
Germinas Boots

Level Jump8, Vertical Jump3
Weapon Break, Mind Break, Justice Sword, Night Sword, Explosion Sword



Rurk
Male
Taurus
77
59
Knight
Throw
Dragon Spirit
Dual Wield
Swim

Long Sword
Blood Sword
Platinum Helmet
Linen Cuirass
Leather Mantle

Shield Break, Magic Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Shuriken, Wand



Nekojin
Female
Aries
50
67
Mediator
Dance
Damage Split
Equip Gun
Move-MP Up

Blaze Gun

Headgear
Silk Robe
Spike Shoes

Invitation, Persuade, Preach, Refute
Disillusion, Obsidian Blade



Kronikle
Male
Cancer
57
76
Calculator
Giant Skill
Blade Grasp
Attack UP
Jump+3

Battle Folio

Cachusha
Gold Armor
108 Gems

Blue Magic
Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest

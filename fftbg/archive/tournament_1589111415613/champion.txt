Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



OverlordCuddles
Female
Libra
40
49
Geomancer
Item
Mana Shield
Halve MP
Move+3

Muramasa
Mythril Shield
Red Hood
Clothes
Angel Ring

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Potion, Antidote, Remedy, Phoenix Down



HASTERIOUS
Female
Gemini
71
81
Summoner
Throw
Sunken State
Short Charge
Jump+3

Wizard Rod

Green Beret
Brigandine
Germinas Boots

Moogle, Shiva, Ramuh, Golem, Bahamut, Salamander, Silf
Shuriken, Sword, Axe



Dictatorhowells
Male
Taurus
80
76
Lancer
Draw Out
Damage Split
Dual Wield
Swim

Spear
Ivory Rod
Gold Helmet
White Robe
Rubber Shoes

Level Jump4, Vertical Jump8
Asura, Heaven's Cloud, Kiyomori



Dexsana
Monster
Capricorn
71
80
Dark Behemoth








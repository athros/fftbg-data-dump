Final Bets: red - 12 bets for 17,949G (71.1%, x0.41); green - 11 bets for 7,278G (28.9%, x2.47)

red bets:
HaplessOne: 5,349G (29.8%, 21,397G)
theNGUYENNER: 5,000G (27.9%, 26,606G)
cam_ATS: 1,804G (10.1%, 1,804G)
roqqqpsi: 1,496G (8.3%, 2,721G)
ungabunga_bot: 1,000G (5.6%, 428,999G)
BirbBrainsBot: 1,000G (5.6%, 160,083G)
cupholderr: 1,000G (5.6%, 18,507G)
zmoses: 500G (2.8%, 6,085G)
Mudrockk: 500G (2.8%, 8,589G)
Arkreaver: 100G (0.6%, 9,932G)
datadrivenbot: 100G (0.6%, 11,680G)
foofermoofer: 100G (0.6%, 800G)

green bets:
Estan_AD: 1,760G (24.2%, 3,521G)
Realitydown: 1,384G (19.0%, 1,384G)
E_Ballard: 1,000G (13.7%, 6,142G)
getthemoneyz: 558G (7.7%, 656,123G)
volgrathemoose: 501G (6.9%, 10,450G)
Artea_: 500G (6.9%, 14,138G)
ForagerCats: 500G (6.9%, 15,300G)
ApplesNP: 475G (6.5%, 475G)
Miyokari: 350G (4.8%, 5,972G)
Eyepoor_: 200G (2.7%, 1,179G)
DrAntiSocial: 50G (0.7%, 14,343G)

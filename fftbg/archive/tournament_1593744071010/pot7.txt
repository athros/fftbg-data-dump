Final Bets: yellow - 12 bets for 8,206G (49.3%, x1.03); purple - 12 bets for 8,431G (50.7%, x0.97)

yellow bets:
DudeMonkey77: 3,248G (39.6%, 3,248G)
killth3kid: 1,226G (14.9%, 122,600G)
SkylerBunny: 938G (11.4%, 938G)
ANFz: 520G (6.3%, 520G)
Sans_from_Snowdin: 500G (6.1%, 24,974G)
Grandlanzer: 400G (4.9%, 45,070G)
Rytor: 300G (3.7%, 3,066G)
twelfthrootoftwo: 300G (3.7%, 11,561G)
loveyouallfriends: 274G (3.3%, 274G)
Aquios_: 200G (2.4%, 1,994G)
datadrivenbot: 200G (2.4%, 50,890G)
CosmicTactician: 100G (1.2%, 48,431G)

purple bets:
Mesmaster: 3,000G (35.6%, 35,485G)
flacococo: 1,012G (12.0%, 1,012G)
BirbBrainsBot: 1,000G (11.9%, 153,758G)
RampagingRobot: 744G (8.8%, 4,744G)
nifboy: 619G (7.3%, 619G)
WASD_Buttons: 520G (6.2%, 520G)
megakim: 500G (5.9%, 8,101G)
ColetteMSLP: 400G (4.7%, 3,558G)
getthemoneyz: 236G (2.8%, 1,163,319G)
ArashiKurobara: 200G (2.4%, 11,525G)
nhammen: 100G (1.2%, 8,458G)
Firesheath: 100G (1.2%, 400G)

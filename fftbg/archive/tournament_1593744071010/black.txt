Player: !Black
Team: Black Team
Palettes: Black/Red



0v3rr8d
Female
Leo
50
48
Priest
Summon Magic
Speed Save
Equip Axe
Retreat

Flail

Headgear
Judo Outfit
Feather Boots

Cure 3, Raise, Regen, Esuna
Moogle, Shiva, Ramuh, Carbunkle, Fairy, Lich



Aldrammech
Male
Gemini
39
42
Ninja
Steal
Regenerator
Equip Knife
Jump+3

Iga Knife
Spell Edge
Holy Miter
Mystic Vest
Jade Armlet

Bomb, Axe, Stick
Gil Taking, Steal Heart



Loveyouallfriends
Female
Gemini
61
77
Chemist
Draw Out
Damage Split
Equip Sword
Move-HP Up

Koutetsu Knife

Black Hood
Adaman Vest
Rubber Shoes

Potion, Ether, Antidote, Eye Drop, Soft, Phoenix Down
Asura, Muramasa



Bilabrin
Female
Libra
48
80
Wizard
Summon Magic
PA Save
Attack UP
Levitate

Rod

Green Beret
Wizard Robe
Magic Gauntlet

Fire, Fire 2, Fire 4, Bolt, Bolt 3, Ice, Ice 3
Moogle, Ramuh, Titan, Golem, Salamander

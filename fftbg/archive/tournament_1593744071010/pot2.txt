Final Bets: green - 13 bets for 6,575G (51.2%, x0.95); yellow - 12 bets for 6,272G (48.8%, x1.05)

green bets:
DeathTaxesAndAnime: 1,678G (25.5%, 1,678G)
ANFz: 1,627G (24.7%, 1,627G)
CITOtheMOSQUITO: 1,000G (15.2%, 4,471G)
prince_rogers_nelson_: 600G (9.1%, 600G)
ColetteMSLP: 400G (6.1%, 3,958G)
benticore: 250G (3.8%, 3,098G)
Mathlexis: 220G (3.3%, 220G)
datadrivenbot: 200G (3.0%, 51,328G)
Rytor: 200G (3.0%, 1,824G)
nhammen: 100G (1.5%, 8,677G)
enkikavlar: 100G (1.5%, 2,804G)
Quadh0nk: 100G (1.5%, 2,197G)
DuneMeta: 100G (1.5%, 3,550G)

yellow bets:
killth3kid: 1,210G (19.3%, 121,014G)
BirbBrainsBot: 1,000G (15.9%, 151,639G)
Mesmaster: 1,000G (15.9%, 35,547G)
Bilabrin: 750G (12.0%, 1,500G)
SkylerBunny: 648G (10.3%, 648G)
CosmicTactician: 500G (8.0%, 47,816G)
getthemoneyz: 314G (5.0%, 1,162,381G)
twelfthrootoftwo: 300G (4.8%, 10,839G)
Lythe_Caraker: 250G (4.0%, 145,132G)
nifboy: 100G (1.6%, 1,487G)
Firesheath: 100G (1.6%, 1,492G)
Sans_from_Snowdin: 100G (1.6%, 24,969G)

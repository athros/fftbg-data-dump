Player: !Black
Team: Black Team
Palettes: Black/Red



Powerpinch
Female
Capricorn
67
73
Knight
Black Magic
Faith Save
Equip Axe
Swim

Morning Star
Aegis Shield
Iron Helmet
Leather Armor
Sprint Shoes

Dark Sword, Night Sword, Surging Sword
Bolt 3



Gelwain
Male
Serpentarius
67
46
Oracle
Time Magic
Distribute
Dual Wield
Move+1

Octagon Rod
Whale Whisker
Holy Miter
Power Sleeve
Sprint Shoes

Blind, Poison, Silence Song, Foxbird, Sleep, Petrify, Dark Holy
Haste, Slow, Slow 2, Float, Reflect, Stabilize Time



Cam ATS
Female
Pisces
48
72
Archer
Dance
Critical Quick
Martial Arts
Ignore Terrain

Cross Bow
Escutcheon
Holy Miter
Clothes
Genji Gauntlet

Charge+1, Charge+3, Charge+10
Slow Dance, Obsidian Blade, Nether Demon



Sinnyil2
Male
Libra
63
53
Thief
Charge
Counter Flood
Equip Bow
Move-MP Up

Night Killer

Flash Hat
Mythril Vest
Rubber Shoes

Steal Weapon, Leg Aim
Charge+1, Charge+2, Charge+4, Charge+7

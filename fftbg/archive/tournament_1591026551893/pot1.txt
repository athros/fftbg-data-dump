Final Bets: red - 12 bets for 15,110G (36.9%, x1.71); blue - 28 bets for 25,825G (63.1%, x0.59)

red bets:
TheChainNerd: 9,076G (60.1%, 29,700G)
Belkra: 1,000G (6.6%, 2,005G)
beowulfrulez: 995G (6.6%, 995G)
maakur_: 831G (5.5%, 831G)
Lali_Lulelo: 710G (4.7%, 710G)
powergems: 624G (4.1%, 624G)
killth3kid: 500G (3.3%, 40,789G)
pplvee1: 500G (3.3%, 1,124G)
Baron_von_Scrub: 474G (3.1%, 22,763G)
MiseryKoss: 200G (1.3%, 6,743G)
ALY327: 100G (0.7%, 51,331G)
Flameatron: 100G (0.7%, 2,203G)

blue bets:
nifboy: 4,763G (18.4%, 4,763G)
toka222: 4,500G (17.4%, 116,567G)
Candina: 4,000G (15.5%, 41,501G)
NovaKnight21: 2,000G (7.7%, 11,866G)
coralreeferz: 1,180G (4.6%, 1,180G)
BirbBrainsBot: 1,000G (3.9%, 130,807G)
MagicBottle: 884G (3.4%, 884G)
megaman2202: 755G (2.9%, 1,511G)
TeaTime29: 629G (2.4%, 629G)
Laserman1000: 613G (2.4%, 26,552G)
LDSkinny: 556G (2.2%, 556G)
UmaiJam: 500G (1.9%, 500G)
rechaun: 500G (1.9%, 3,165G)
Lionhermit: 500G (1.9%, 4,893G)
fenaen: 492G (1.9%, 492G)
gogofromtogo: 402G (1.6%, 402G)
Grandlanzer: 400G (1.5%, 2,731G)
getthemoneyz: 360G (1.4%, 787,641G)
joewcarson: 300G (1.2%, 4,473G)
benticore: 300G (1.2%, 2,599G)
reddwind_: 268G (1.0%, 268G)
mattcorn7: 210G (0.8%, 210G)
Meta_Five: 200G (0.8%, 11,960G)
outer_monologue: 108G (0.4%, 108G)
theshinyakuma: 104G (0.4%, 104G)
Firesheath: 101G (0.4%, 7,171G)
RuneS_77: 100G (0.4%, 7,748G)
datadrivenbot: 100G (0.4%, 29,611G)

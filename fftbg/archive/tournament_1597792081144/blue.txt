Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CosmicTactician
Female
Taurus
75
69
Dancer
Yin Yang Magic
Damage Split
Equip Axe
Jump+2

Slasher

Feather Hat
Linen Robe
Spike Shoes

Slow Dance
Poison, Spell Absorb, Doubt Faith, Silence Song, Dispel Magic



ThreeMileIsland
Male
Aquarius
37
62
Knight
Jump
Auto Potion
Magic Defense UP
Move+1

Diamond Sword
Bronze Shield
Barbuta
Linen Cuirass
Reflect Ring

Armor Break, Weapon Break, Power Break, Mind Break, Dark Sword
Level Jump2, Vertical Jump4



SephDarkheart
Male
Libra
70
49
Oracle
Time Magic
Sunken State
Doublehand
Fly

Cypress Rod

Feather Hat
Black Robe
Angel Ring

Blind, Life Drain, Pray Faith, Zombie, Blind Rage, Dispel Magic
Haste, Slow 2, Float, Quick, Demi



Lord Gwarth
Female
Aries
49
58
Knight
Talk Skill
Meatbone Slash
Dual Wield
Teleport

Diamond Sword
Broad Sword
Genji Helmet
Leather Armor
Genji Gauntlet

Armor Break, Speed Break, Dark Sword
Invitation, Threaten, Solution, Death Sentence, Negotiate, Refute

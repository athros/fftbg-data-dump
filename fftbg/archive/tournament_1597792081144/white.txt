Player: !White
Team: White Team
Palettes: White/Blue



ArchKnightX
Male
Sagittarius
47
69
Geomancer
Summon Magic
Counter
Concentrate
Jump+1

Battle Axe
Flame Shield
Golden Hairpin
Linen Robe
Spike Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Titan, Carbunkle, Salamander



Sinnyil2
Female
Leo
80
69
Squire
Draw Out
PA Save
Short Status
Swim

Ancient Sword
Genji Shield
Headgear
Power Sleeve
Battle Boots

Accumulate, Yell, Cheer Up, Fury
Asura, Muramasa



Amorial
Female
Aquarius
77
46
Oracle
Charge
Dragon Spirit
Secret Hunt
Levitate

Papyrus Codex

Triangle Hat
White Robe
Power Wrist

Zombie, Blind Rage, Dispel Magic, Sleep
Charge+4, Charge+7



Resjudicata3
Male
Sagittarius
49
79
Priest
Charge
Distribute
Magic Attack UP
Swim

Oak Staff

Green Beret
Silk Robe
Feather Boots

Cure 2, Raise, Protect, Esuna, Holy
Charge+2, Charge+4, Charge+5, Charge+7, Charge+20

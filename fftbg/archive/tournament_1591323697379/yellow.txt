Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CapnChaos12
Male
Scorpio
52
55
Mime

Regenerator
Martial Arts
Lava Walking


Mythril Shield
Headgear
Adaman Vest
Feather Mantle

Mimic




Laserman1000
Male
Taurus
50
69
Bard
Item
Parry
Throw Item
Swim

Bloody Strings

Black Hood
Wizard Outfit
Sprint Shoes

Angel Song, Sky Demon
Antidote, Eye Drop, Holy Water



PezPezzington
Male
Aries
52
69
Knight
Steal
Dragon Spirit
Sicken
Move+2

Save the Queen
Aegis Shield
Iron Helmet
Chain Mail
Defense Ring

Weapon Break, Stasis Sword, Justice Sword, Night Sword
Steal Helmet, Steal Armor, Steal Accessory



Kronikle
Male
Sagittarius
43
73
Knight
Charge
Auto Potion
Dual Wield
Move+3

Ancient Sword
Broad Sword
Mythril Helmet
Reflect Mail
Small Mantle

Armor Break, Speed Break, Power Break
Charge+1, Charge+2, Charge+3, Charge+10

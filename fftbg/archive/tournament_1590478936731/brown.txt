Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lowlf
Female
Aries
56
67
Oracle
White Magic
Counter Tackle
Magic Defense UP
Jump+2

Battle Folio

Triangle Hat
Light Robe
Elf Mantle

Blind, Poison, Pray Faith, Silence Song, Dispel Magic, Petrify
Cure 2, Cure 3, Raise, Reraise, Esuna



Phytik
Male
Gemini
73
74
Archer
Throw
Speed Save
Equip Shield
Waterbreathing

Cross Bow
Escutcheon
Gold Helmet
Mythril Vest
Spike Shoes

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Sword



Lawndough
Male
Aries
44
48
Wizard
Battle Skill
Counter Magic
Equip Armor
Levitate

Rod

Genji Helmet
Mythril Armor
Angel Ring

Fire 3, Bolt, Bolt 2, Ice 3, Death, Flare
Weapon Break, Magic Break, Surging Sword



Sinnyil2
Male
Cancer
71
46
Ninja
Time Magic
Damage Split
Equip Gun
Move+2

Papyrus Codex
Bestiary
Red Hood
Leather Outfit
Germinas Boots

Knife, Sword, Wand
Demi, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



OmnibotGamma
Male
Virgo
50
48
Lancer
Battle Skill
Earplug
Equip Armor
Move+1

Gokuu Rod
Ice Shield
Platinum Helmet
Power Sleeve
Battle Boots

Level Jump8, Vertical Jump2
Head Break, Weapon Break, Magic Break, Power Break, Stasis Sword



Run With Stone GUNs
Male
Aries
62
71
Knight
Steal
Arrow Guard
Attack UP
Waterbreathing

Defender
Aegis Shield
Diamond Helmet
Linen Robe
Defense Ring

Weapon Break, Magic Break, Night Sword
Steal Armor, Steal Shield, Steal Weapon, Leg Aim



Sairentozon7
Male
Serpentarius
47
59
Thief
Basic Skill
Dragon Spirit
Equip Gun
Waterbreathing

Ramia Harp

Black Hood
Leather Outfit
Chantage

Gil Taking, Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim
Accumulate, Heal, Yell, Scream



Theseawolf1
Male
Pisces
64
68
Priest
Summon Magic
Dragon Spirit
Halve MP
Teleport

White Staff

Barette
Mythril Vest
Elf Mantle

Cure, Cure 2, Cure 4, Raise, Regen, Protect
Shiva, Ramuh, Ifrit, Leviathan

Final Bets: red - 22 bets for 11,139G (37.1%, x1.69); black - 24 bets for 18,879G (62.9%, x0.59)

red bets:
Lodrak: 2,553G (22.9%, 2,553G)
theatheologist: 2,222G (19.9%, 91,826G)
BirbBrainsBot: 1,000G (9.0%, 148,007G)
bad1dea: 1,000G (9.0%, 365,234G)
Lanshaft: 600G (5.4%, 600G)
DarrenDinosaurs: 500G (4.5%, 6,109G)
waterwatereverywhere: 400G (3.6%, 5,650G)
WireLord: 380G (3.4%, 380G)
DonCardenio: 342G (3.1%, 342G)
evdoggity: 300G (2.7%, 4,879G)
MrDiggs49: 288G (2.6%, 288G)
ZephyrTempest: 232G (2.1%, 19,972G)
NagginNaggers: 200G (1.8%, 1,394G)
joewcarson: 200G (1.8%, 4,321G)
b_bot: 200G (1.8%, 2,459G)
getthemoneyz: 122G (1.1%, 415,470G)
BadBlanket: 100G (0.9%, 4,993G)
Conome: 100G (0.9%, 10,550G)
chronoxtrigger: 100G (0.9%, 4,248G)
silentperogy: 100G (0.9%, 7,009G)
Melbraega: 100G (0.9%, 104G)
SeedSC: 100G (0.9%, 134,479G)

black bets:
AllInBot: 3,139G (16.6%, 3,139G)
Shakarak: 3,000G (15.9%, 10,481G)
RongRongArts: 2,314G (12.3%, 4,629G)
Virilikus: 2,000G (10.6%, 29,770G)
sinnyil2: 1,200G (6.4%, 62,924G)
ApplesauceBoss: 1,080G (5.7%, 1,080G)
I_nod_my_head_when_I_lose: 750G (4.0%, 19,806G)
Zeroroute: 686G (3.6%, 686G)
Ranmilia: 664G (3.5%, 664G)
b0shii: 552G (2.9%, 552G)
Shalloween: 500G (2.6%, 74,287G)
HaplessOne: 500G (2.6%, 32,142G)
ANFz: 396G (2.1%, 396G)
CorpusCav: 372G (2.0%, 5,373G)
electric_glass: 300G (1.6%, 4,688G)
TycerNova: 274G (1.5%, 274G)
ugoplatamia: 200G (1.1%, 19,521G)
loveyouallfriends: 200G (1.1%, 15,697G)
DrAntiSocial: 200G (1.1%, 908G)
dolzytwitch: 150G (0.8%, 2,127G)
ungabunga_bot: 102G (0.5%, 4,270G)
Chompie: 100G (0.5%, 7,637G)
datadrivenbot: 100G (0.5%, 15,880G)
BurlapChampion: 100G (0.5%, 216G)

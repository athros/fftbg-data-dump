Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lavian94
Female
Aries
49
48
Mime

Abandon
Long Status
Jump+3



Mythril Helmet
Mythril Vest
N-Kai Armlet

Mimic




EnemyController
Female
Aries
45
47
Squire
Dance
Damage Split
Doublehand
Jump+2

Slasher

Bronze Helmet
Black Costume
Bracer

Heal, Tickle, Wish
Witch Hunt, Disillusion, Nameless Dance, Obsidian Blade



Meta Five
Male
Leo
56
53
Squire
Jump
PA Save
Equip Bow
Waterbreathing

Poison Bow
Bronze Shield
Black Hood
Leather Armor
Spike Shoes

Accumulate, Throw Stone, Heal, Wish, Scream
Level Jump8, Vertical Jump2



B Bot
Female
Leo
70
58
Knight
Steal
Counter Magic
Doublehand
Levitate

Battle Axe

Gold Helmet
Black Robe
Spike Shoes

Head Break, Stasis Sword, Justice Sword
Gil Taking, Steal Helmet

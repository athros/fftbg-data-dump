Player: !zChamp
Team: Champion Team
Palettes: Green/White



Cryptopsy70
Monster
Libra
55
50
Bomb










CapnChaos12
Female
Pisces
71
70
Wizard
Yin Yang Magic
Dragon Spirit
Short Charge
Move+3

Rod

Flash Hat
Adaman Vest
Diamond Armlet

Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice, Death
Poison, Silence Song, Confusion Song, Paralyze



Jeeboheebo
Female
Virgo
61
64
Thief
Dance
Sunken State
Dual Wield
Waterbreathing

Air Knife
Spell Edge
Green Beret
Secret Clothes
Angel Ring

Steal Heart, Steal Weapon, Steal Accessory
Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Void Storage, Nether Demon



Oreo Pizza
Male
Virgo
53
75
Wizard
Summon Magic
Counter Flood
Equip Gun
Move+2

Papyrus Codex

Black Hood
White Robe
Magic Gauntlet

Bolt, Bolt 3, Ice, Ice 4
Moogle, Ramuh, Titan, Carbunkle, Odin, Fairy, Cyclops

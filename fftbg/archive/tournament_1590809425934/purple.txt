Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Laserman1000
Male
Scorpio
52
77
Squire
Charge
Brave Save
Secret Hunt
Fly

Coral Sword
Mythril Shield
Leather Hat
Leather Armor
108 Gems

Accumulate, Throw Stone, Heal, Tickle, Cheer Up, Wish
Charge+7



Evdoggity
Male
Sagittarius
57
61
Monk
Sing
Counter
Short Charge
Move+3



Feather Hat
Power Sleeve
Red Shoes

Earth Slash, Purification, Revive
Angel Song, Magic Song



CosmicTactician
Male
Virgo
49
49
Squire
White Magic
Critical Quick
Concentrate
Move+3

Bow Gun
Mythril Shield
Leather Hat
Secret Clothes
Battle Boots

Dash, Heal, Yell, Scream
Cure 3, Cure 4, Raise, Reraise, Protect, Protect 2, Shell 2, Esuna



Sinnyil2
Female
Capricorn
76
66
Summoner
Item
Mana Shield
Short Charge
Swim

Poison Rod

Holy Miter
Chain Vest
Spike Shoes

Golem, Carbunkle, Bahamut, Fairy
Potion, Ether, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down

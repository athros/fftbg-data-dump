Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CosmicTactician
Female
Aquarius
62
71
Knight
Throw
Meatbone Slash
Martial Arts
Waterwalking

Ragnarok
Ice Shield
Barbuta
Genji Armor
Power Wrist

Head Break, Shield Break, Speed Break, Power Break, Mind Break, Night Sword, Surging Sword
Shuriken, Bomb, Stick



Choco Joe
Male
Aquarius
75
56
Ninja
Time Magic
Critical Quick
Equip Gun
Ignore Terrain

Bestiary
Madlemgen
Twist Headband
Mythril Vest
Dracula Mantle

Shuriken, Staff
Haste, Haste 2, Stop, Reflect, Quick



ValtonZenola
Female
Pisces
52
54
Archer
White Magic
Counter Magic
Attack UP
Lava Walking

Bow Gun
Diamond Shield
Feather Hat
Power Sleeve
Jade Armlet

Charge+7, Charge+10, Charge+20
Cure 3, Cure 4, Raise, Reraise, Protect 2, Esuna



E Ballard
Male
Taurus
54
73
Calculator
Beast Skill
Dragon Spirit
Short Charge
Jump+2

Battle Folio

Black Hood
Genji Armor
Defense Ring

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest

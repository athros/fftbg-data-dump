Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Ququroon
Female
Libra
55
62
Knight
Item
MP Restore
Throw Item
Ignore Terrain

Ragnarok
Genji Shield
Crystal Helmet
Platinum Armor
Feather Mantle

Head Break, Weapon Break, Stasis Sword, Justice Sword, Surging Sword, Explosion Sword
Potion, Hi-Potion, Antidote, Echo Grass



Pah J
Female
Aries
42
48
Geomancer
Draw Out
Counter
Short Status
Move-HP Up

Platinum Sword
Ice Shield
Red Hood
Linen Robe
Power Wrist

Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Heaven's Cloud



I Nod My Head When I Lose
Female
Leo
70
75
Calculator
Time Magic
Catch
Equip Knife
Move+1

Ice Rod

Headgear
Mythril Vest
Rubber Shoes

CT, Height, Prime Number, 5, 4
Haste 2, Stop, Demi, Stabilize Time



EnemyController
Male
Cancer
80
78
Squire
Jump
MP Restore
Martial Arts
Ignore Terrain

Battle Axe
Genji Shield
Leather Helmet
Gold Armor
Red Shoes

Throw Stone, Heal, Wish
Level Jump2, Vertical Jump2

Final Bets: purple - 7 bets for 11,391G (55.2%, x0.81); brown - 12 bets for 9,251G (44.8%, x1.23)

purple bets:
DeathTaxesAndAnime: 5,810G (51.0%, 11,394G)
actual_JP: 2,370G (20.8%, 2,370G)
Aldrammech: 1,207G (10.6%, 1,207G)
Forkmore: 1,000G (8.8%, 26,309G)
Firesheath: 404G (3.5%, 404G)
xLilAsia: 400G (3.5%, 5,851G)
danielrogercardoso: 200G (1.8%, 1,074G)

brown bets:
skillomono: 3,000G (32.4%, 19,428G)
HaateXIII: 2,204G (23.8%, 2,204G)
BirbBrainsBot: 1,000G (10.8%, 170,104G)
getthemoneyz: 912G (9.9%, 1,170,498G)
WalkerNash: 500G (5.4%, 36,588G)
RampagingRobot: 452G (4.9%, 1,952G)
Mathlexis: 450G (4.9%, 1,243G)
n2ash: 212G (2.3%, 212G)
nok____: 200G (2.2%, 2,700G)
Lydian_C: 121G (1.3%, 146,334G)
ChaoreLance: 100G (1.1%, 2,081G)
DuneMeta: 100G (1.1%, 5,894G)

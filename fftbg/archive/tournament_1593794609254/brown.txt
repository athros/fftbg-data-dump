Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Baconbacon1207
Female
Scorpio
50
53
Wizard
Punch Art
Parry
Beastmaster
Waterbreathing

Blind Knife

Flash Hat
White Robe
Jade Armlet

Fire 2, Fire 3, Fire 4, Bolt 2, Ice 2
Purification, Chakra, Revive, Seal Evil



HaateXIII
Female
Libra
71
60
Monk
Black Magic
Speed Save
Short Status
Move-MP Up



Twist Headband
Mystic Vest
Jade Armlet

Pummel, Purification, Chakra, Revive, Seal Evil
Fire, Fire 4, Bolt, Bolt 3, Bolt 4, Death, Flare



ZCKaiser
Male
Serpentarius
64
58
Priest
Elemental
Absorb Used MP
Equip Axe
Move+2

Flame Whip

Twist Headband
Black Robe
Feather Boots

Cure, Raise, Raise 2, Reraise, Regen, Protect, Shell, Esuna, Holy
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Skillomono
Male
Aries
50
67
Knight
Charge
Abandon
Martial Arts
Move-HP Up

Ragnarok
Ice Shield
Cross Helmet
Chameleon Robe
Elf Mantle

Head Break, Shield Break, Magic Break, Mind Break, Justice Sword
Charge+4, Charge+10

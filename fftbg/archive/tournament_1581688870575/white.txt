Player: !White
Team: White Team
Palettes: White/Blue



E7bbk
Monster
Sagittarius
50
76
Ultima Demon










Gamesage53
Male
Aquarius
49
59
Summoner
White Magic
Counter
Short Charge
Ignore Height

Wizard Staff

Red Hood
Brigandine
Wizard Mantle

Ramuh, Leviathan, Salamander, Silf, Fairy
Raise, Reraise, Regen, Shell, Esuna, Magic Barrier



Zbgs
Male
Gemini
62
66
Monk
Sing
Brave Up
Doublehand
Waterbreathing



Cachusha
Chain Vest
Leather Mantle

Spin Fist, Purification, Revive, Seal Evil
Magic Song, Last Song



HysteriaDays
Male
Libra
44
45
Squire
Time Magic
Hamedo
Equip Knife
Ignore Height

Hunting Bow

Holy Miter
Wizard Outfit
Feather Boots

Accumulate, Dash, Cheer Up, Wish
Haste, Haste 2, Float, Quick, Stabilize Time

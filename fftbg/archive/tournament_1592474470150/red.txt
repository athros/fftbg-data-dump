Player: !Red
Team: Red Team
Palettes: Red/Brown



NovaKnight21
Female
Aries
56
61
Chemist
Time Magic
Regenerator
Attack UP
Waterbreathing

Star Bag

Triangle Hat
Earth Clothes
108 Gems

Potion, X-Potion, Antidote, Eye Drop, Soft, Phoenix Down
Haste, Stop, Float, Quick, Demi, Stabilize Time



CrownOfHorns
Male
Virgo
49
47
Time Mage
Charge
Parry
Martial Arts
Jump+1



Headgear
Silk Robe
Magic Gauntlet

Haste, Slow, Reflect, Quick, Stabilize Time, Meteor
Charge+1, Charge+2, Charge+5



Grininda
Male
Scorpio
47
46
Wizard
Summon Magic
Faith Save
Equip Sword
Teleport

Kikuichimoji

Black Hood
Linen Robe
Magic Ring

Fire, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Frog
Moogle, Shiva, Carbunkle, Bahamut, Odin, Leviathan, Silf, Fairy, Lich



HASTERIOUS
Female
Sagittarius
39
72
Summoner
White Magic
Mana Shield
Equip Armor
Move+1

Gold Staff

Bronze Helmet
Leather Armor
Genji Gauntlet

Moogle, Ifrit, Silf, Lich
Cure 2, Cure 3, Protect 2, Esuna

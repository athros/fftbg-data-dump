Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TeaTime29
Male
Virgo
54
79
Knight
Basic Skill
Caution
Equip Polearm
Ignore Terrain

Spear
Buckler
Bronze Helmet
Bronze Armor
Power Wrist

Head Break, Armor Break, Power Break, Stasis Sword, Justice Sword
Accumulate, Heal, Tickle, Wish



DuraiPapers
Male
Taurus
61
47
Chemist
Punch Art
Abandon
Magic Defense UP
Jump+1

Panther Bag

Triangle Hat
Brigandine
Feather Mantle

Potion, X-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Earth Slash, Purification, Chakra, Revive



Twelfthrootoftwo
Male
Aries
51
73
Archer
Battle Skill
Distribute
Dual Wield
Levitate

Hunting Bow
Night Killer
Twist Headband
Wizard Outfit
Rubber Shoes

Charge+4
Head Break, Magic Break, Speed Break, Night Sword, Surging Sword



CT 5 Holy
Male
Scorpio
74
79
Mime

Auto Potion
Equip Shield
Fly


Escutcheon
Triangle Hat
Diamond Armor
Feather Mantle

Mimic


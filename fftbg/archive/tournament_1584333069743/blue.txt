Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zenovox
Female
Cancer
69
75
Priest
Elemental
Hamedo
Equip Armor
Move+3

Gold Staff

Green Beret
Plate Mail
Elf Mantle

Cure 2, Cure 3, Cure 4, Raise, Regen
Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



TheManInPlaid
Male
Leo
38
66
Summoner
Draw Out
MA Save
Defense UP
Jump+1

Poison Rod

Green Beret
White Robe
Battle Boots

Moogle, Shiva, Golem, Carbunkle, Bahamut, Odin, Salamander, Fairy
Asura, Koutetsu, Murasame, Heaven's Cloud



Creggers
Monster
Scorpio
65
52
Vampire










Toka222
Female
Gemini
52
45
Monk
Steal
Hamedo
Concentrate
Levitate



Holy Miter
Leather Outfit
Feather Boots

Secret Fist, Purification, Chakra
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Steal Status

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ALY327
Female
Sagittarius
60
72
Time Mage
Summon Magic
Earplug
Martial Arts
Ignore Terrain

Rainbow Staff

Black Hood
Mystic Vest
Defense Ring

Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time, Meteor
Moogle, Golem, Carbunkle, Silf, Lich



HeroponThrawn
Female
Leo
53
45
Mime

Brave Up
Defense UP
Waterbreathing



Black Hood
Judo Outfit
Reflect Ring

Mimic




GingerDynomite
Female
Sagittarius
47
73
Lancer
Battle Skill
Counter
Concentrate
Move-HP Up

Iron Fan
Round Shield
Platinum Helmet
Gold Armor
Magic Gauntlet

Level Jump5, Vertical Jump8
Armor Break, Weapon Break



DrAntiSocial
Male
Scorpio
65
50
Summoner
Sing
Brave Up
Short Charge
Move-HP Up

Poison Rod

Headgear
Chameleon Robe
Rubber Shoes

Moogle, Shiva, Titan, Carbunkle
Space Storage

Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Monster
Scorpio
76
61
Apanda










ThePuss
Male
Cancer
44
56
Calculator
Black Magic
PA Save
Equip Shield
Ignore Height

Ice Rod
Mythril Shield
Black Hood
White Robe
Spike Shoes

CT, Height, Prime Number, 3
Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3



Lawnboxer
Female
Scorpio
78
52
Lancer
White Magic
Absorb Used MP
Dual Wield
Move+3

Obelisk
Holy Lance
Diamond Helmet
Black Robe
Genji Gauntlet

Level Jump8, Vertical Jump6
Cure 3, Cure 4, Protect 2, Shell, Shell 2, Wall, Esuna



Aldrammech
Female
Cancer
69
61
Ninja
Battle Skill
Counter
Equip Knife
Move+2

Thunder Rod
Hidden Knife
Triangle Hat
Black Costume
Reflect Ring

Spear
Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Surging Sword

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HaplessOne
Male
Cancer
44
76
Lancer
Sing
Speed Save
Attack UP
Waterwalking

Cypress Rod
Ice Shield
Diamond Helmet
Gold Armor
Battle Boots

Level Jump3, Vertical Jump5
Angel Song, Diamond Blade



Miyokari
Male
Sagittarius
81
74
Chemist
Summon Magic
HP Restore
Defense UP
Ignore Terrain

Orichalcum

Triangle Hat
Wizard Outfit
Rubber Shoes

X-Potion, Hi-Ether, Echo Grass, Holy Water, Remedy, Phoenix Down
Moogle, Ifrit, Carbunkle, Silf, Lich



Cam ATS
Female
Gemini
77
59
Mediator
Summon Magic
Sunken State
Defend
Move+3

Battle Folio

Twist Headband
Linen Robe
Feather Boots

Persuade, Solution, Insult, Mimic Daravon
Shiva, Ramuh, Titan, Bahamut, Odin, Cyclops



DrAntiSocial
Female
Sagittarius
63
67
Knight
Draw Out
Auto Potion
Attack UP
Move+1

Diamond Sword
Bronze Shield
Diamond Helmet
Genji Armor
Elf Mantle

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Surging Sword
Asura, Murasame

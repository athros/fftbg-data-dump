Final Bets: blue - 16 bets for 10,058G (50.7%, x0.97); yellow - 19 bets for 9,780G (49.3%, x1.03)

blue bets:
gorgewall: 2,792G (27.8%, 2,792G)
VySaika: 1,000G (9.9%, 5,572G)
ungabunga_bot: 1,000G (9.9%, 356,259G)
BirbBrainsBot: 1,000G (9.9%, 67,455G)
Lionhermit: 1,000G (9.9%, 16,743G)
arch8000: 892G (8.9%, 4,463G)
mirapoix: 529G (5.3%, 529G)
OmnibotGamma: 500G (5.0%, 4,507G)
genericco: 380G (3.8%, 380G)
Eyepoor_: 250G (2.5%, 3,549G)
YaBoy125: 200G (2.0%, 2,276G)
MeotorMang: 150G (1.5%, 3,050G)
ANFz: 111G (1.1%, 30,069G)
nifboy: 100G (1.0%, 9,130G)
RestIessNight: 100G (1.0%, 897G)
getthemoneyz: 54G (0.5%, 631,171G)

yellow bets:
killth3kid: 2,000G (20.4%, 48,724G)
Shalloween: 1,245G (12.7%, 2,442G)
Shakarak: 1,200G (12.3%, 7,167G)
Mesmaster: 1,200G (12.3%, 57,042G)
Laserman1000: 1,062G (10.9%, 108,962G)
jakeduhhsnake: 1,000G (10.2%, 54,896G)
fenaen: 500G (5.1%, 1,658G)
Who_lio42: 300G (3.1%, 3,158G)
twelfthrootoftwo: 300G (3.1%, 957G)
Sungazes: 150G (1.5%, 204G)
ACSpree: 150G (1.5%, 30,606G)
vampyreinabox: 100G (1.0%, 874G)
electric_glass: 100G (1.0%, 1,387G)
datadrivenbot: 100G (1.0%, 11,567G)
Kyune: 100G (1.0%, 5,471G)
caprinovoa: 100G (1.0%, 196G)
ZephyrTempest: 73G (0.7%, 1,471G)
DrAntiSocial: 50G (0.5%, 8,555G)
cougboi: 50G (0.5%, 19,084G)

Final Bets: white - 8 bets for 8,166G (58.9%, x0.70); brown - 8 bets for 5,704G (41.1%, x1.43)

white bets:
Evewho: 2,000G (24.5%, 10,913G)
prince_rogers_nelson_: 1,619G (19.8%, 1,619G)
thewondertrickster: 1,435G (17.6%, 2,870G)
lowlf: 1,212G (14.8%, 92,852G)
BirbBrainsBot: 1,000G (12.2%, 115,670G)
KasugaiRoastedPeas: 700G (8.6%, 17,651G)
Ring_Wyrm: 100G (1.2%, 2,915G)
datadrivenbot: 100G (1.2%, 47,860G)

brown bets:
SkylerBunny: 2,000G (35.1%, 507,918G)
serperemagus: 1,532G (26.9%, 1,532G)
Chuckolator: 1,212G (21.2%, 130,906G)
gamesage53: 362G (6.3%, 362G)
getthemoneyz: 258G (4.5%, 1,105,409G)
superdevon1: 140G (2.5%, 1,409G)
Klednar21: 100G (1.8%, 132G)
SuzakuReii: 100G (1.8%, 4,807G)

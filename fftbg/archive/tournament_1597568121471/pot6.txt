Final Bets: white - 8 bets for 4,272G (58.7%, x0.70); brown - 10 bets for 3,009G (41.3%, x1.42)

white bets:
amiture: 1,000G (23.4%, 12,096G)
Mesmaster: 1,000G (23.4%, 1,229G)
TheMurkGnome: 714G (16.7%, 8,182G)
SeniorBunk: 452G (10.6%, 452G)
Mister_Quof: 367G (8.6%, 367G)
WhiteTigress: 360G (8.4%, 360G)
datadrivenbot: 200G (4.7%, 65,666G)
3ngag3: 179G (4.2%, 1,429G)

brown bets:
Lydian_C: 588G (19.5%, 588G)
BirbBrainsBot: 552G (18.3%, 170,968G)
ZephyrBurst83: 500G (16.6%, 9,298G)
WhattayaBrian: 427G (14.2%, 427G)
lowlf: 304G (10.1%, 3,495G)
AllInBot: 171G (5.7%, 171G)
getthemoneyz: 166G (5.5%, 1,657,818G)
gorgewall: 101G (3.4%, 8,121G)
ko2q: 100G (3.3%, 2,105G)
Drusiform: 100G (3.3%, 8,081G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dexef
Female
Aquarius
60
78
Chemist
Talk Skill
Meatbone Slash
Monster Talk
Levitate

Cute Bag

Cachusha
Clothes
Rubber Shoes

Potion, Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Invitation, Solution, Mimic Daravon



Powergems
Female
Cancer
51
76
Calculator
Black Magic
Earplug
Short Status
Move+1

Battle Folio

Twist Headband
Light Robe
Angel Ring

CT, Prime Number
Fire, Bolt, Bolt 4



Laserman1000
Male
Aries
57
59
Calculator
Yin Yang Magic
MA Save
Dual Wield
Move+1

Papyrus Codex
Battle Folio
Black Hood
Mystic Vest
Dracula Mantle

CT, Height, Prime Number, 3
Blind, Poison, Foxbird, Dispel Magic, Sleep



Samtimemc
Female
Aries
54
57
Time Mage
Battle Skill
Parry
Dual Wield
Ignore Terrain

Healing Staff
White Staff
Red Hood
Chameleon Robe
Salty Rage

Haste, Haste 2, Slow 2, Immobilize, Float, Reflect, Demi, Stabilize Time
Shield Break, Power Break, Mind Break, Justice Sword

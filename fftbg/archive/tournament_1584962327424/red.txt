Player: !Red
Team: Red Team
Palettes: Red/Brown



Lydian C
Female
Aquarius
69
59
Samurai
Jump
Earplug
Magic Attack UP
Ignore Height

Partisan

Crystal Helmet
Crystal Mail
Rubber Shoes

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Level Jump5, Vertical Jump7



Alc Trinity
Female
Aries
73
47
Squire
Elemental
Caution
Equip Polearm
Move-HP Up

Octagon Rod
Buckler
Feather Hat
Linen Cuirass
Defense Ring

Dash, Tickle, Yell, Scream
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



UmaiJam
Male
Sagittarius
54
70
Samurai
Throw
Caution
Magic Defense UP
Jump+2

Spear

Genji Helmet
Bronze Armor
Bracer

Asura, Bizen Boat, Heaven's Cloud
Shuriken, Bomb



Volgrathemoose
Male
Taurus
61
44
Oracle
White Magic
Counter Flood
Equip Shield
Waterwalking

Cypress Rod
Crystal Shield
Leather Hat
Linen Robe
Battle Boots

Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic
Cure 3, Raise, Raise 2, Reraise, Shell 2, Wall, Esuna

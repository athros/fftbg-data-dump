Player: !Red
Team: Red Team
Palettes: Red/Brown



GeeGXP
Male
Capricorn
78
58
Geomancer
Sing
Sunken State
Long Status
Move+1

Sleep Sword
Gold Shield
Feather Hat
Mythril Vest
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Lava Ball
Angel Song, Life Song, Cheer Song, Battle Song, Hydra Pit



OperatorTheory
Monster
Gemini
49
53
Revenant










Dogsandcatsand
Female
Libra
62
45
Samurai
Item
Distribute
Halve MP
Fly

Holy Lance

Leather Helmet
Linen Cuirass
Angel Ring

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Hi-Potion, Echo Grass, Soft



ScurvyMitch
Female
Aries
80
67
Monk
Draw Out
Brave Save
Equip Sword
Move+1

Defender

Red Hood
Mystic Vest
Sprint Shoes

Pummel, Earth Slash, Purification, Chakra, Revive, Seal Evil
Murasame, Heaven's Cloud

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Serperemagus
Male
Aries
73
60
Thief
Battle Skill
Arrow Guard
Maintenance
Move+1

Diamond Sword

Holy Miter
Power Sleeve
Feather Boots

Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Weapon Break, Power Break, Stasis Sword



Solomongrundy85
Male
Scorpio
66
43
Knight
Yin Yang Magic
Abandon
Long Status
Waterwalking

Giant Axe
Flame Shield
Iron Helmet
Mythril Armor
Dracula Mantle

Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword
Blind, Life Drain, Dispel Magic



EnemyController
Male
Sagittarius
51
53
Priest
Charge
Parry
Equip Armor
Teleport

Healing Staff

Platinum Helmet
Chain Mail
Rubber Shoes

Cure 2, Cure 3, Raise, Shell 2, Esuna
Charge+1, Charge+4, Charge+7



CrownOfHorns
Female
Capricorn
51
75
Geomancer
Yin Yang Magic
HP Restore
Defense UP
Teleport

Masamune
Gold Shield
Triangle Hat
Wizard Robe
Rubber Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Zombie, Silence Song, Dark Holy

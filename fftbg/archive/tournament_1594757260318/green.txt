Player: !Green
Team: Green Team
Palettes: Green/White



HaateXIII
Female
Sagittarius
45
47
Wizard
Dance
Brave Save
Sicken
Jump+1

Wizard Rod

Flash Hat
Wizard Robe
Sprint Shoes

Fire 4, Bolt, Ice 3, Flare
Slow Dance, Last Dance, Dragon Pit



BdiDima
Male
Scorpio
76
51
Oracle
Item
Sunken State
Equip Gun
Lava Walking

Blaze Gun

Leather Hat
Black Robe
Cursed Ring

Blind, Spell Absorb, Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Holy Water, Remedy, Phoenix Down



Lyans00
Monster
Aries
80
44
Hydra










Furrytomahawkk
Female
Taurus
79
67
Time Mage
Yin Yang Magic
Counter Flood
Short Status
Move+1

Ivory Rod

Green Beret
Wizard Robe
Wizard Mantle

Haste, Slow, Stop, Float, Reflect, Demi, Stabilize Time, Meteor, Galaxy Stop
Blind, Dispel Magic, Paralyze

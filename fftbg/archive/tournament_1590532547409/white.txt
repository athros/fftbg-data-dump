Player: !White
Team: White Team
Palettes: White/Blue



OmnibotGamma
Male
Aquarius
70
67
Bard
Basic Skill
Abandon
Equip Shield
Ignore Terrain

Fairy Harp
Mythril Shield
Headgear
Clothes
Angel Ring

Life Song, Battle Song, Last Song, Sky Demon
Accumulate, Dash, Throw Stone, Heal, Yell, Fury



Mesmaster
Female
Leo
80
75
Priest
Draw Out
Dragon Spirit
Short Charge
Fly

Wizard Staff

Ribbon
Earth Clothes
Magic Gauntlet

Cure 3, Raise, Raise 2, Regen, Wall, Esuna, Holy
Asura, Koutetsu, Bizen Boat



Fenixcrest
Male
Taurus
69
69
Knight
Talk Skill
Faith Save
Attack UP
Jump+1

Sleep Sword
Escutcheon
Leather Helmet
Gold Armor
Bracer

Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword, Surging Sword
Praise, Threaten, Death Sentence, Mimic Daravon, Rehabilitate



Hamborn
Female
Cancer
81
63
Oracle
Item
Damage Split
Short Status
Move+2

Battle Bamboo

Holy Miter
Chameleon Robe
Defense Armlet

Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep, Petrify
Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down

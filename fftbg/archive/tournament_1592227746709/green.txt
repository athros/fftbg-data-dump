Player: !Green
Team: Green Team
Palettes: Green/White



JumbocactuarX27
Male
Cancer
63
51
Chemist
Jump
Caution
Secret Hunt
Jump+1

Glacier Gun

Black Hood
Wizard Outfit
Feather Mantle

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Level Jump5, Vertical Jump7



CosmicTactician
Female
Aries
67
78
Time Mage
Punch Art
Meatbone Slash
Equip Axe
Jump+3

Slasher

Triangle Hat
Wizard Robe
Feather Boots

Slow, Stop, Reflect, Stabilize Time
Pummel, Purification, Chakra, Revive



Azelgrim
Male
Sagittarius
65
51
Time Mage
Item
Critical Quick
Equip Knife
Fly

Poison Rod

Leather Hat
Silk Robe
Sprint Shoes

Haste 2, Slow, Stop, Float, Stabilize Time
Hi-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Phoenix Down



BlackFireUK
Female
Virgo
42
58
Knight
Draw Out
Absorb Used MP
Attack UP
Jump+1

Chaos Blade
Crystal Shield
Genji Helmet
Crystal Mail
Diamond Armlet

Magic Break, Power Break, Stasis Sword
Bizen Boat

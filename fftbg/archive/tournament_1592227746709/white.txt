Player: !White
Team: White Team
Palettes: White/Blue



VolgraTheMoose
Male
Pisces
80
74
Ninja
Sing
Caution
Equip Gun
Ignore Height

Battle Folio
Papyrus Codex
Barette
Chain Vest
Genji Gauntlet

Shuriken, Sword
Magic Song



Fenixcrest
Female
Taurus
68
42
Knight
Jump
Hamedo
Magic Defense UP
Move+3

Long Sword
Buckler
Platinum Helmet
Mythril Armor
N-Kai Armlet

Magic Break, Mind Break, Justice Sword
Level Jump5, Vertical Jump6



Zachara
Male
Cancer
71
71
Archer
Time Magic
Counter
Short Charge
Jump+1

Cross Bow
Mythril Shield
Black Hood
Leather Outfit
Small Mantle

Charge+1, Charge+7, Charge+20
Stop, Immobilize, Reflect, Stabilize Time



LordSDB
Female
Taurus
62
71
Ninja
Punch Art
Absorb Used MP
Secret Hunt
Jump+1

Koga Knife
Ninja Edge
Headgear
Mystic Vest
Wizard Mantle

Shuriken
Spin Fist, Pummel, Secret Fist, Purification, Revive, Seal Evil

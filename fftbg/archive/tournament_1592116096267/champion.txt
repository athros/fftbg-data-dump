Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Sinnyil2
Male
Sagittarius
65
44
Lancer
Sing
Hamedo
Magic Defense UP
Move-HP Up

Javelin
Escutcheon
Mythril Helmet
Linen Cuirass
108 Gems

Level Jump8, Vertical Jump5
Angel Song, Magic Song, Space Storage, Sky Demon



MadnessR
Female
Cancer
46
69
Thief
Summon Magic
MP Restore
Attack UP
Retreat

Mage Masher

Green Beret
Wizard Outfit
Defense Armlet

Steal Helmet, Steal Shield, Steal Accessory
Moogle, Titan, Golem, Leviathan



KyleWonToLiveForever
Male
Virgo
40
61
Monk
Draw Out
Counter
Sicken
Lava Walking



Twist Headband
Earth Clothes
Battle Boots

Pummel, Chakra, Seal Evil
Asura, Bizen Boat, Muramasa



Pandasforsale
Male
Virgo
52
71
Samurai
Throw
Damage Split
Short Status
Levitate

Bizen Boat

Leather Helmet
Black Robe
Defense Armlet

Koutetsu, Bizen Boat, Kiyomori, Muramasa
Shuriken, Bomb, Staff, Spear, Dictionary

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ArchKnightX
Female
Taurus
64
80
Mediator
Punch Art
Distribute
Magic Defense UP
Move+3

Blaze Gun

Black Hood
Brigandine
Defense Ring

Invitation, Praise, Preach, Insult, Mimic Daravon, Refute, Rehabilitate
Secret Fist, Purification, Revive



Thyrandaal
Monster
Pisces
71
43
Serpentarius










MadnessR
Male
Aquarius
78
45
Mediator
Draw Out
Absorb Used MP
Maintenance
Ignore Height

Glacier Gun

Headgear
Judo Outfit
Defense Armlet

Invitation, Refute, Rehabilitate
Asura



Dexsana
Male
Aquarius
65
50
Archer
Black Magic
Sunken State
Concentrate
Ignore Height

Blaze Gun
Platinum Shield
Diamond Helmet
Mythril Vest
Sprint Shoes

Charge+2, Charge+10
Fire 2, Fire 4, Ice 4, Empower

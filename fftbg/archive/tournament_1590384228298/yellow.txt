Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



EnemyController
Female
Sagittarius
47
61
Ninja
Battle Skill
Dragon Spirit
Sicken
Ignore Height

Hidden Knife
Flail
Triangle Hat
Brigandine
Bracer

Shuriken, Bomb, Knife, Spear, Dictionary
Head Break, Armor Break, Weapon Break, Magic Break, Mind Break



BlaqkElectric
Male
Sagittarius
53
77
Lancer
Battle Skill
Mana Shield
Maintenance
Teleport

Musk Rod
Diamond Shield
Genji Helmet
Genji Armor
Sprint Shoes

Level Jump3, Vertical Jump6
Armor Break, Magic Break, Speed Break, Power Break, Surging Sword



TJ FordCDN
Male
Taurus
68
59
Priest
Black Magic
Absorb Used MP
Equip Bow
Move+3

Bow Gun

Triangle Hat
Brigandine
108 Gems

Cure 2, Cure 3, Cure 4, Raise 2, Esuna, Holy
Bolt 2, Ice 4



Waterwatereverywhere
Female
Libra
78
74
Time Mage
Draw Out
Faith Save
Equip Axe
Move+1

Battle Axe

Feather Hat
Silk Robe
Angel Ring

Haste, Haste 2, Slow 2, Reflect, Demi, Stabilize Time, Meteor
Koutetsu, Murasame, Muramasa, Kikuichimoji

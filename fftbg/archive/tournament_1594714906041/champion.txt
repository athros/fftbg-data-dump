Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



OpHendoslice
Male
Virgo
64
36
Archer
Punch Art
Sunken State
Doublehand
Ignore Terrain

Silver Bow

Green Beret
Power Sleeve
Genji Gauntlet

Charge+1, Charge+3, Charge+7, Charge+20
Pummel, Wave Fist, Secret Fist, Purification



ALY327
Male
Leo
56
45
Bard
Time Magic
Speed Save
Attack UP
Jump+3

Bloody Strings

Green Beret
Brigandine
Vanish Mantle

Angel Song, Diamond Blade, Space Storage
Haste 2, Slow 2, Float, Reflect, Demi 2, Meteor



Coolguye
Male
Cancer
71
68
Mime

Dragon Spirit
Defend
Ignore Terrain



Golden Hairpin
Wizard Outfit
N-Kai Armlet

Mimic




Byrdturbo
Male
Sagittarius
51
61
Mediator
Draw Out
Distribute
Equip Sword
Move-MP Up

Kiyomori

Triangle Hat
Mystic Vest
Salty Rage

Persuade, Threaten, Death Sentence, Negotiate, Refute, Rehabilitate
Asura, Koutetsu, Bizen Boat, Murasame, Kikuichimoji

Player: !Red
Team: Red Team
Palettes: Red/Brown



NicoSavoy
Female
Pisces
40
69
Monk
Jump
MA Save
Equip Knife
Ignore Height

Wizard Rod

Red Hood
Brigandine
Rubber Shoes

Pummel, Purification, Chakra
Level Jump4, Vertical Jump8



Firesheath
Female
Aries
72
51
Lancer
Battle Skill
HP Restore
Dual Wield
Ignore Terrain

Javelin
Obelisk
Mythril Helmet
Linen Cuirass
Vanish Mantle

Level Jump8, Vertical Jump5
Head Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Surging Sword



Soren Of Tyto
Male
Taurus
51
78
Monk
Sing
Earplug
Magic Attack UP
Fly



Headgear
Judo Outfit
Small Mantle

Secret Fist, Purification, Chakra, Revive, Seal Evil
Battle Song, Magic Song, Last Song, Hydra Pit



Sinnyil2
Male
Sagittarius
43
47
Bard
Black Magic
Meatbone Slash
Equip Axe
Jump+1

Oak Staff

Green Beret
Leather Armor
Cherche

Angel Song, Life Song, Magic Song, Space Storage, Sky Demon
Fire, Fire 2, Bolt 3, Bolt 4, Ice 2, Flare

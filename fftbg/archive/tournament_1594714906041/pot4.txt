Final Bets: purple - 4 bets for 4,516G (41.1%, x1.43); brown - 11 bets for 6,460G (58.9%, x0.70)

purple bets:
Lydian_C: 3,400G (75.3%, 83,008G)
Evewho: 500G (11.1%, 7,857G)
randgridr: 500G (11.1%, 4,636G)
dtrain332: 116G (2.6%, 116G)

brown bets:
dantayystv: 2,000G (31.0%, 24,078G)
getthemoneyz: 1,000G (15.5%, 1,276,072G)
BirbBrainsBot: 1,000G (15.5%, 141,401G)
twelfthrootoftwo: 520G (8.0%, 520G)
hiros13gts: 427G (6.6%, 427G)
CT_5_Holy: 380G (5.9%, 380G)
Arcblazer23: 368G (5.7%, 368G)
superdevon1: 276G (4.3%, 5,538G)
datadrivenbot: 200G (3.1%, 57,125G)
Powermhero: 189G (2.9%, 23,189G)
Firesheath: 100G (1.5%, 3,179G)

Final Bets: blue - 10 bets for 9,982G (75.5%, x0.32); green - 7 bets for 3,237G (24.5%, x3.08)

blue bets:
randgridr: 2,677G (26.8%, 5,355G)
Lydian_C: 2,400G (24.0%, 87,872G)
dantayystv: 2,000G (20.0%, 22,078G)
Powermhero: 1,000G (10.0%, 23,000G)
Hasterious: 696G (7.0%, 696G)
twelfthrootoftwo: 520G (5.2%, 520G)
hiros13gts: 232G (2.3%, 232G)
BirbBrainsBot: 207G (2.1%, 140,401G)
datadrivenbot: 200G (2.0%, 56,925G)
getthemoneyz: 50G (0.5%, 1,275,072G)

green bets:
upvla: 1,000G (30.9%, 8,053G)
Evewho: 1,000G (30.9%, 8,572G)
ColetteMSLP: 400G (12.4%, 9,199G)
CT_5_Holy: 380G (11.7%, 380G)
NicoSavoy: 200G (6.2%, 184,214G)
dtrain332: 141G (4.4%, 282G)
ayeayex3: 116G (3.6%, 116G)

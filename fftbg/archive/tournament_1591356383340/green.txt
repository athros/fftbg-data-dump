Player: !Green
Team: Green Team
Palettes: Green/White



Rislyeu
Female
Aries
51
78
Mime

MA Save
Monster Talk
Move+2



Holy Miter
Chain Vest
Reflect Ring

Mimic




Zeando
Female
Gemini
56
67
Dancer
Basic Skill
Critical Quick
Equip Bow
Waterbreathing

Cross Bow

Barette
Light Robe
Genji Gauntlet

Witch Hunt, Wiznaibus, Slow Dance, Nameless Dance, Obsidian Blade, Nether Demon
Accumulate, Throw Stone, Yell



HaateXIII
Female
Aries
62
79
Wizard
Dance
Arrow Guard
Concentrate
Levitate

Ice Rod

Triangle Hat
Mystic Vest
Battle Boots

Fire, Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Frog
Polka Polka, Nether Demon



Nelrith
Male
Scorpio
42
51
Bard
Throw
Parry
Equip Axe
Move+3

Flail

Golden Hairpin
Black Costume
Red Shoes

Life Song, Magic Song
Bomb, Staff, Wand

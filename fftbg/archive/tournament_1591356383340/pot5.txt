Final Bets: red - 11 bets for 6,930G (59.0%, x0.70); green - 8 bets for 4,825G (41.0%, x1.44)

red bets:
Sairentozon7: 1,952G (28.2%, 1,952G)
BirbBrainsBot: 1,000G (14.4%, 71,448G)
Bryon_W: 865G (12.5%, 865G)
getthemoneyz: 688G (9.9%, 823,279G)
TheChainNerd: 672G (9.7%, 672G)
volgrathemoose: 555G (8.0%, 4,336G)
just_here2: 500G (7.2%, 6,014G)
LeoNightFury: 398G (5.7%, 398G)
E_Ballard: 100G (1.4%, 5,048G)
CosmicTactician: 100G (1.4%, 14,179G)
datadrivenbot: 100G (1.4%, 33,099G)

green bets:
Rislyeu: 1,200G (24.9%, 8,869G)
maakur_: 1,136G (23.5%, 1,136G)
Nelrith: 881G (18.3%, 3,524G)
gogofromtogo: 484G (10.0%, 484G)
ShintaroNayaka: 400G (8.3%, 3,576G)
Zachara: 374G (7.8%, 102,674G)
Anak761: 250G (5.2%, 4,485G)
Saldarin: 100G (2.1%, 2,194G)

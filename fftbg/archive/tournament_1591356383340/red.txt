Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Male
Scorpio
61
69
Mime

Damage Split
Martial Arts
Teleport



Twist Headband
Mythril Vest
Jade Armlet

Mimic




Sairentozon7
Male
Taurus
68
42
Lancer
Charge
Counter
Equip Sword
Move+1

Defender

Mythril Helmet
Bronze Armor
Jade Armlet

Level Jump8, Vertical Jump8
Charge+1, Charge+7, Charge+20



Volgrathemoose
Male
Pisces
67
75
Knight
Black Magic
Auto Potion
Equip Axe
Lava Walking

Slasher
Escutcheon
Grand Helmet
Diamond Armor
108 Gems

Armor Break, Weapon Break, Magic Break, Power Break, Justice Sword
Fire, Fire 2, Fire 3



Error72
Female
Pisces
47
58
Time Mage
Draw Out
Faith Save
Dual Wield
Teleport

Octagon Rod
Ivory Rod
Twist Headband
White Robe
Reflect Ring

Haste, Haste 2, Stop, Float, Reflect, Demi, Stabilize Time
Asura, Koutetsu, Heaven's Cloud

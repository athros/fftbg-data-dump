Player: !Black
Team: Black Team
Palettes: Black/Red



Nifboy
Monster
Capricorn
55
80
Gobbledeguck










Goust18
Male
Sagittarius
45
70
Archer
Steal
HP Restore
Dual Wield
Jump+1

Stone Gun
Blast Gun
Circlet
Brigandine
Rubber Shoes

Charge+5, Charge+10
Steal Armor, Leg Aim



Corwinambrose
Male
Capricorn
54
61
Squire
Item
Speed Save
Equip Sword
Lava Walking

Sleep Sword
Bronze Shield
Genji Helmet
Diamond Armor
Elf Mantle

Throw Stone, Yell, Wish
Potion, Antidote, Maiden's Kiss, Phoenix Down



HaateXIII
Male
Sagittarius
55
73
Calculator
Wildfire Skill
Damage Split
Equip Knife
Lava Walking

Short Edge
Escutcheon
Circlet
Carabini Mail
Feather Mantle

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit

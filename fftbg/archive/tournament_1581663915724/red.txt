Player: !Red
Team: Red Team
Palettes: Red/Brown



Extinctosaurus
Male
Pisces
44
56
Mime

Meatbone Slash
Maintenance
Jump+1



Crystal Helmet
Leather Outfit
Elf Mantle

Mimic




ZantetsukenX
Female
Aries
58
52
Monk
Basic Skill
Counter Tackle
Dual Wield
Retreat



Cachusha
Clothes
Bracer

Pummel, Purification, Revive, Seal Evil
Throw Stone, Heal, Cheer Up, Wish



Maeveen
Male
Virgo
58
37
Lancer
Time Magic
Brave Up
Defense UP
Move-MP Up

Spear
Bronze Shield
Leather Helmet
Linen Robe
Defense Armlet

Level Jump2, Vertical Jump6
Slow 2, Float, Demi, Demi 2, Meteor



Arachness
Male
Libra
65
65
Bard
Jump
Mana Shield
Equip Bow
Move+3

Lightning Bow

Feather Hat
Rubber Costume
Magic Ring

Angel Song, Life Song, Cheer Song
Level Jump5, Vertical Jump2

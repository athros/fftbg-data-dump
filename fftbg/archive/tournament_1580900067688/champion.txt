Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Tithonus
Male
Scorpio
68
73
Priest
Summon Magic
Faith Up
Doublehand
Move+3

Morning Star

Triangle Hat
Robe of Lords
Battle Boots

Cure 2, Raise, Shell, Shell 2, Wall, Esuna
Moogle, Carbunkle, Odin, Salamander, Cyclops



Adenis222
Male
Libra
70
68
Knight
Steal
Parry
Attack UP
Move+2

Defender

Genji Helmet
Carabini Mail
Bracer

Head Break, Shield Break, Power Break
Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Steal Status



Feffle
Male
Taurus
47
63
Samurai
Black Magic
MA Save
Martial Arts
Levitate

Asura Knife

Iron Helmet
Linen Robe
Feather Mantle

Koutetsu, Bizen Boat, Murasame
Fire 2, Fire 4, Bolt, Ice, Ice 4, Frog



Sypheck
Male
Leo
44
53
Geomancer
Time Magic
Blade Grasp
Maintenance
Lava Walking

Battle Axe
Diamond Shield
Thief Hat
Adaman Vest
Cherche

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Haste, Slow 2, Stop, Float, Meteor

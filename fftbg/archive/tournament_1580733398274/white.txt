Player: !White
Team: White Team
Palettes: White/Blue



RaitoGG
Male
Leo
48
70
Lancer
Steal
Meatbone Slash
Dual Wield
Move+3

Battle Bamboo
Octagon Rod
Diamond Helmet
Silk Robe
Defense Ring

Level Jump2, Vertical Jump8
Steal Helmet, Steal Weapon



Paperthinhere
Male
Capricorn
57
75
Ninja
White Magic
Counter Flood
Sicken
Waterbreathing

Flame Whip
Flail
Golden Hairpin
Chain Vest
Vanish Mantle

Knife, Wand
Cure 2, Cure 4, Raise, Shell, Wall, Esuna



La Boomstick
Male
Virgo
43
69
Mediator
Black Magic
Sunken State
Short Status
Move+3

Bestiary

Triangle Hat
Leather Outfit
Reflect Ring

Solution, Negotiate, Mimic Daravon, Refute
Fire, Fire 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Empower, Death, Flare



Brondius
Female
Sagittarius
51
50
Summoner
Punch Art
Earplug
Short Charge
Move+3

Wizard Staff

Red Hood
Secret Clothes
Defense Armlet

Moogle, Ifrit, Odin
Spin Fist, Earth Slash, Purification, Chakra, Revive

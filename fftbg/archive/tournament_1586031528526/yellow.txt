Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DudeMonkey77
Male
Libra
55
51
Lancer
Throw
Brave Up
Long Status
Swim

Partisan
Escutcheon
Leather Helmet
Linen Robe
Genji Gauntlet

Level Jump8, Vertical Jump7
Bomb



DrAntiSocial
Male
Taurus
74
60
Wizard
Time Magic
Damage Split
Magic Attack UP
Teleport

Rod

Headgear
Black Costume
Wizard Mantle

Fire, Fire 3, Fire 4, Bolt 2, Ice 3, Ice 4, Frog, Death
Haste, Haste 2, Slow 2, Immobilize, Float, Demi



DHaveWord
Female
Libra
68
39
Mime

Sunken State
Monster Talk
Move+1



Headgear
Judo Outfit
Defense Ring

Mimic




KonzeraLive
Male
Taurus
49
73
Mediator
Time Magic
Distribute
Equip Knife
Move+1

Mage Masher

Headgear
Mystic Vest
Magic Gauntlet

Threaten, Negotiate, Mimic Daravon, Refute
Haste, Slow 2, Immobilize, Float, Reflect, Stabilize Time

Final Bets: purple - 18 bets for 12,580G (59.6%, x0.68); brown - 18 bets for 8,532G (40.4%, x1.47)

purple bets:
TheMM42: 2,000G (15.9%, 40,547G)
potgodtopdog: 1,628G (12.9%, 1,628G)
LanseDM: 1,500G (11.9%, 3,642G)
HaplessOne: 1,500G (11.9%, 116,800G)
mirapoix: 915G (7.3%, 915G)
AllInBot: 768G (6.1%, 768G)
NoNotBees: 700G (5.6%, 1,988G)
jhazor: 621G (4.9%, 12,425G)
TheMurkGnome: 604G (4.8%, 18,464G)
reinoe: 600G (4.8%, 600G)
Shakarak: 600G (4.8%, 600G)
JackOnFire1: 288G (2.3%, 288G)
ungabunga_bot: 234G (1.9%, 175,132G)
Ross_from_Cali: 222G (1.8%, 7,484G)
twelfthrootoftwo: 100G (0.8%, 3,779G)
elijahrockers: 100G (0.8%, 100G)
definitely_not_shroud: 100G (0.8%, 100G)
Treapvort: 100G (0.8%, 12,634G)

brown bets:
rockmem21: 1,402G (16.4%, 1,402G)
BirbBrainsBot: 1,000G (11.7%, 33,352G)
Anox_Skell: 1,000G (11.7%, 4,745G)
getthemoneyz: 754G (8.8%, 521,603G)
SpookyCookieMonster: 558G (6.5%, 558G)
MalakoFox: 500G (5.9%, 49,127G)
Shalloween: 400G (4.7%, 51,785G)
Dexsana: 400G (4.7%, 18,324G)
alrightbye: 400G (4.7%, 41,821G)
leakimiko: 308G (3.6%, 308G)
R_Raynos: 304G (3.6%, 304G)
DeathTaxesAndAnime: 300G (3.5%, 12,335G)
waterwatereverywhere: 300G (3.5%, 1,596G)
ThePuss: 256G (3.0%, 1,966G)
Estan_AD: 250G (2.9%, 5,270G)
RubenFlonne: 200G (2.3%, 17,727G)
kaidykat: 100G (1.2%, 885G)
gorgewall: 100G (1.2%, 1,794G)

Player: !White
Team: White Team
Palettes: White/Blue



KaLam1ty
Female
Aries
75
46
Mime

PA Save
Secret Hunt
Retreat



Ribbon
Judo Outfit
Battle Boots

Mimic




Electric Glass
Female
Aries
53
70
Lancer
Throw
Auto Potion
Short Status
Ignore Height

Iron Fan
Genji Shield
Circlet
Crystal Mail
Defense Ring

Level Jump5, Vertical Jump6
Shuriken, Bomb



MalakoFox
Monster
Taurus
64
70
King Behemoth










Alacor
Male
Libra
57
65
Knight
Summon Magic
Counter Flood
Short Charge
Jump+2

Rune Blade
Diamond Shield
Genji Helmet
Leather Armor
Magic Gauntlet

Shield Break, Magic Break, Justice Sword
Moogle, Titan, Carbunkle, Salamander, Fairy

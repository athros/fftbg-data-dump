Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rockmem21
Female
Libra
57
66
Monk
Item
PA Save
Equip Knife
Ignore Height

Dragon Rod

Golden Hairpin
Rubber Costume
Feather Boots

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Potion, Hi-Potion, Maiden's Kiss, Holy Water



0v3rr8d
Male
Capricorn
77
49
Bard
Item
Parry
Magic Attack UP
Waterbreathing

Ramia Harp

Feather Hat
Genji Armor
Feather Boots

Life Song, Battle Song, Nameless Song, Last Song, Space Storage
Potion, Hi-Potion, X-Potion, Eye Drop, Holy Water, Phoenix Down



Chuckolator
Female
Pisces
72
54
Squire
White Magic
Abandon
Beastmaster
Teleport

Blood Sword
Escutcheon
Crystal Helmet
Reflect Mail
108 Gems

Dash, Heal, Tickle, Yell, Wish
Raise 2, Reraise, Shell, Wall, Esuna



Leakimiko
Male
Leo
54
69
Oracle
Draw Out
Regenerator
Equip Sword
Move+3

Bizen Boat

Leather Hat
Judo Outfit
N-Kai Armlet

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Asura, Koutetsu, Muramasa, Kikuichimoji, Masamune

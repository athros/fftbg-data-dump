Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Zachara
Female
Pisces
55
47
Chemist
Talk Skill
Arrow Guard
Magic Attack UP
Fly

Dagger

Twist Headband
Black Costume
N-Kai Armlet

Potion, X-Potion, Hi-Ether, Antidote, Phoenix Down
Death Sentence, Insult



Electric Glass
Male
Libra
79
59
Ninja
Draw Out
Abandon
Magic Attack UP
Fly

Spell Edge
Spell Edge
Holy Miter
Wizard Outfit
Elf Mantle

Bomb, Hammer
Asura, Koutetsu, Murasame



Skysa250
Male
Pisces
43
42
Geomancer
Summon Magic
Meatbone Slash
Doublehand
Waterwalking

Battle Axe

Headgear
Chameleon Robe
Spike Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Moogle, Shiva, Golem, Carbunkle, Bahamut, Salamander, Silf



Itzover9001
Male
Capricorn
52
76
Priest
Draw Out
Caution
Defense UP
Jump+1

Oak Staff

Green Beret
Linen Robe
Feather Boots

Raise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Heaven's Cloud, Muramasa

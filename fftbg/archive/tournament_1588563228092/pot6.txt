Final Bets: white - 17 bets for 9,079G (38.3%, x1.61); purple - 21 bets for 14,656G (61.7%, x0.62)

white bets:
DarrenDinosaurs: 2,073G (22.8%, 2,073G)
SkylerBunny: 1,500G (16.5%, 47,110G)
Mesmaster: 1,000G (11.0%, 110,953G)
twelfthrootoftwo: 864G (9.5%, 864G)
Laserman1000: 803G (8.8%, 88,803G)
pandasforsale: 666G (7.3%, 33,811G)
Breakdown777: 500G (5.5%, 15,787G)
gorgewall: 300G (3.3%, 20,932G)
genericco: 300G (3.3%, 1,771G)
DonCardenio: 200G (2.2%, 26,682G)
ungabunga_bot: 173G (1.9%, 326,561G)
getthemoneyz: 172G (1.9%, 610,383G)
Kyune: 128G (1.4%, 128G)
Heroebal: 100G (1.1%, 6,153G)
Arlum: 100G (1.1%, 775G)
Setdevildog: 100G (1.1%, 271G)
Bioticism: 100G (1.1%, 1,044G)

purple bets:
jakeduhhsnake: 2,000G (13.6%, 78,912G)
Zeroroute: 1,676G (11.4%, 3,961G)
sinnyil2: 1,200G (8.2%, 9,827G)
HaplessOne: 1,111G (7.6%, 29,891G)
EnemyController: 1,000G (6.8%, 169,762G)
BirbBrainsBot: 1,000G (6.8%, 3,648G)
TheMurkGnome: 1,000G (6.8%, 4,420G)
Lionhermit: 1,000G (6.8%, 29,911G)
concles: 1,000G (6.8%, 19,159G)
Link1nr: 652G (4.4%, 652G)
SarrgeQc: 515G (3.5%, 2,860G)
Bloody_Nips: 500G (3.4%, 2,612G)
Zetchryn: 500G (3.4%, 5,548G)
Realitydown: 352G (2.4%, 352G)
Malicious52: 300G (2.0%, 551G)
ttLYNCH09: 300G (2.0%, 2,010G)
KasugaiRoastedPeas: 200G (1.4%, 2,931G)
Evewho: 100G (0.7%, 8,166G)
TimeJannies: 100G (0.7%, 3,711G)
datadrivenbot: 100G (0.7%, 12,357G)
letdowncity: 50G (0.3%, 419G)

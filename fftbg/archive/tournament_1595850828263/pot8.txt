Final Bets: red - 14 bets for 16,154G (51.7%, x0.93); champion - 14 bets for 15,078G (48.3%, x1.07)

red bets:
SephDarkheart: 5,709G (35.3%, 11,418G)
NIghtdew14: 2,000G (12.4%, 34,956G)
UmaiJam: 1,900G (11.8%, 90,517G)
prince_rogers_nelson_: 1,494G (9.2%, 1,494G)
TheChainNerd: 1,257G (7.8%, 5,029G)
BirbBrainsBot: 1,000G (6.2%, 128,450G)
E_Ballard: 664G (4.1%, 664G)
AllInBot: 492G (3.0%, 492G)
dogsandcatsand: 360G (2.2%, 360G)
DavenIII: 348G (2.2%, 348G)
douchetron: 344G (2.1%, 344G)
Smashy: 300G (1.9%, 3,861G)
getthemoneyz: 186G (1.2%, 1,405,887G)
neerrm: 100G (0.6%, 20,887G)

champion bets:
RaIshtar: 8,360G (55.4%, 8,360G)
MLWebz0r: 1,182G (7.8%, 11,821G)
Thyrandaal: 1,111G (7.4%, 7,803G)
ColetteMSLP: 1,000G (6.6%, 31,109G)
ShintaroNayaka: 800G (5.3%, 7,976G)
ohhinm: 709G (4.7%, 709G)
randgridr: 500G (3.3%, 9,581G)
run_with_stone_GUNs: 392G (2.6%, 5,612G)
DouglasDragonThePoet: 276G (1.8%, 276G)
RyanTheRebs: 236G (1.6%, 236G)
datadrivenbot: 200G (1.3%, 45,084G)
Netmonmatt: 112G (0.7%, 112G)
Chambs12: 100G (0.7%, 100G)
Bongomon7: 100G (0.7%, 8,298G)

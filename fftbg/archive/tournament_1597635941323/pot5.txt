Final Bets: red - 14 bets for 11,020G (33.5%, x1.98); yellow - 9 bets for 21,858G (66.5%, x0.50)

red bets:
silentkaster: 3,027G (27.5%, 3,027G)
SkylerBunny: 2,050G (18.6%, 2,050G)
Shalloween: 1,500G (13.6%, 17,089G)
BirbBrainsBot: 1,000G (9.1%, 163,236G)
SeniorBunk: 900G (8.2%, 5,042G)
killth3kid: 732G (6.6%, 19,712G)
resjudicata3: 572G (5.2%, 572G)
brando84back: 244G (2.2%, 244G)
getthemoneyz: 216G (2.0%, 1,655,700G)
datadrivenbot: 200G (1.8%, 67,708G)
Treafa: 195G (1.8%, 1,568G)
Tugboat1: 164G (1.5%, 164G)
Lifebregin: 120G (1.1%, 1,120G)
CosmicTactician: 100G (0.9%, 24,395G)

yellow bets:
sinnyil2: 13,155G (60.2%, 26,311G)
MonchoStrife: 5,000G (22.9%, 35,161G)
iBardic: 1,200G (5.5%, 4,436G)
MagicBottle: 572G (2.6%, 572G)
Phi_Sig: 555G (2.5%, 146,245G)
silentperogy: 500G (2.3%, 47,320G)
reinoe: 500G (2.3%, 233,742G)
Tarheels218: 276G (1.3%, 276G)
AllInBot: 100G (0.5%, 100G)

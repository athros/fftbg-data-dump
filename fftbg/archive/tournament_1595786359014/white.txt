Player: !White
Team: White Team
Palettes: White/Blue



Miku Shikhu
Female
Leo
60
61
Summoner
Punch Art
HP Restore
Short Charge
Waterwalking

Poison Rod

Twist Headband
Adaman Vest
Angel Ring

Moogle, Ramuh, Titan, Golem, Bahamut, Leviathan, Salamander, Silf, Lich
Earth Slash, Secret Fist, Revive



KasugaiRoastedPeas
Male
Capricorn
73
70
Thief
Battle Skill
Dragon Spirit
Beastmaster
Move+3

Sleep Sword

Flash Hat
Chain Vest
Germinas Boots

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status
Stasis Sword, Justice Sword, Night Sword



Butterbelljedi
Male
Gemini
58
72
Ninja
Basic Skill
Faith Save
Equip Knife
Swim

Air Knife
Assassin Dagger
Headgear
Black Costume
Feather Mantle

Shuriken, Bomb, Spear, Wand
Throw Stone, Heal, Scream, Ultima



Fenaen
Male
Aquarius
60
40
Mime

MA Save
Defend
Move+2



Headgear
Power Sleeve
Spike Shoes

Mimic


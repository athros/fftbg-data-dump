Player: !Red
Team: Red Team
Palettes: Red/Brown



Mrfripps
Monster
Libra
62
61
Wild Boar










MagicBottle
Female
Capricorn
69
59
Wizard
Throw
Counter
Short Status
Jump+1

Blind Knife

Flash Hat
Linen Robe
Feather Mantle

Bolt 4, Empower, Flare
Knife, Sword



Ring Wyrm
Male
Leo
73
72
Geomancer
Punch Art
Counter Tackle
Maintenance
Waterbreathing

Diamond Sword
Ice Shield
Headgear
Linen Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Purification, Chakra, Revive



Snoopiku
Female
Aries
75
51
Ninja
Punch Art
Faith Save
Equip Gun
Teleport

Mythril Gun
Mythril Gun
Flash Hat
Black Costume
Small Mantle

Shuriken, Knife
Spin Fist, Purification, Chakra, Revive, Seal Evil

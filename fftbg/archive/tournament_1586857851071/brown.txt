Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Volgrathemoose
Male
Sagittarius
46
61
Ninja
Sing
Catch
Defense UP
Ignore Height

Spell Edge
Air Knife
Holy Miter
Chain Vest
Leather Mantle

Dictionary
Angel Song, Battle Song, Space Storage, Hydra Pit



Skysa250
Female
Capricorn
51
40
Ninja
Elemental
Parry
Attack UP
Move+2

Spell Edge
Kunai
Red Hood
Mystic Vest
Magic Gauntlet

Wand
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Chode Ular
Female
Aries
63
47
Chemist
Throw
Counter Flood
Magic Attack UP
Ignore Height

Hydra Bag

Holy Miter
Earth Clothes
Magic Gauntlet

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Holy Water, Remedy, Phoenix Down
Shuriken, Staff, Ninja Sword



Leakimiko
Male
Cancer
42
41
Bard
White Magic
Sunken State
Doublehand
Move+2

Fairy Harp

Black Hood
Mythril Armor
108 Gems

Angel Song, Life Song, Battle Song
Cure, Cure 2, Reraise, Regen, Protect 2, Wall, Esuna

Final Bets: white - 11 bets for 8,043G (62.1%, x0.61); brown - 8 bets for 4,908G (37.9%, x1.64)

white bets:
volgrathemoose: 1,503G (18.7%, 1,503G)
HaplessOne: 1,500G (18.6%, 78,496G)
superdevon1: 1,096G (13.6%, 3,132G)
ungabunga_bot: 1,000G (12.4%, 126,590G)
HASTERIOUS: 953G (11.8%, 953G)
datadrivenbot: 651G (8.1%, 7,161G)
jame3200: 500G (6.2%, 500G)
PotionDweller: 470G (5.8%, 470G)
AerodyneX: 160G (2.0%, 160G)
getthemoneyz: 110G (1.4%, 483,243G)
Primeval_33: 100G (1.2%, 100G)

brown bets:
leakimiko: 1,604G (32.7%, 1,604G)
BirbBrainsBot: 1,000G (20.4%, 115,688G)
WireLord: 904G (18.4%, 904G)
ShintaroNayaka: 600G (12.2%, 11,062G)
chode_ular: 300G (6.1%, 717G)
Tithonus: 200G (4.1%, 14,154G)
Evewho: 200G (4.1%, 3,845G)
AllInBot: 100G (2.0%, 100G)

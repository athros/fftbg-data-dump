Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Darkbringer813
Female
Sagittarius
45
71
Geomancer
Punch Art
Dragon Spirit
Magic Attack UP
Move+2

Ice Brand
Platinum Shield
Thief Hat
Brigandine
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Spin Fist, Wave Fist, Chakra



ALY327
Male
Taurus
80
59
Thief
Time Magic
Catch
Short Charge
Move+2

Kunai

Headgear
Leather Outfit
Diamond Armlet

Steal Shield, Steal Accessory
Haste, Slow



Dexsana
Female
Pisces
47
63
Summoner
Yin Yang Magic
Counter Flood
Equip Sword
Fly

Rune Blade

Green Beret
Leather Outfit
Bracer

Golem, Carbunkle, Odin, Leviathan, Fairy, Lich, Cyclops
Life Drain, Pray Faith, Silence Song, Dispel Magic



Sairentozon7
Male
Virgo
76
39
Oracle
Basic Skill
Damage Split
Defend
Ignore Terrain

Ivory Rod

Green Beret
Adaman Vest
Defense Ring

Poison, Paralyze, Petrify, Dark Holy
Heal, Tickle, Yell, Wish, Scream

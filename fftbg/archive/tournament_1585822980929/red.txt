Player: !Red
Team: Red Team
Palettes: Red/Brown



Tempo Tantrum
Male
Aries
69
43
Thief
Black Magic
Counter
Defend
Jump+1

Air Knife

Golden Hairpin
Wizard Outfit
Reflect Ring

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory
Fire, Fire 4, Bolt 2, Bolt 4



Rexamajinx
Female
Cancer
78
43
Knight
Draw Out
Counter
Attack UP
Waterbreathing

Long Sword
Crystal Shield
Genji Helmet
Plate Mail
Diamond Armlet

Armor Break, Shield Break, Magic Break, Mind Break, Justice Sword, Surging Sword
Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa



Mirapoix
Female
Gemini
68
66
Geomancer
Item
Auto Potion
Throw Item
Waterwalking

Battle Axe
Mythril Shield
Flash Hat
Black Robe
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Potion, Ether, Hi-Ether, Antidote, Holy Water



SeedSC
Female
Pisces
44
77
Dancer
Draw Out
PA Save
Equip Shield
Move+2

Cashmere
Genji Shield
Green Beret
Silk Robe
Wizard Mantle

Witch Hunt, Wiznaibus, Void Storage
Koutetsu

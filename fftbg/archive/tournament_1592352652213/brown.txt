Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zbgs
Female
Capricorn
38
71
Thief
Time Magic
Brave Save
Dual Wield
Waterbreathing

Mythril Knife
Orichalcum
Headgear
Chain Vest
Spike Shoes

Steal Accessory, Leg Aim
Haste, Haste 2, Slow 2, Stabilize Time



Soren Of Tyto
Male
Taurus
49
64
Samurai
Charge
Brave Save
Magic Defense UP
Move-HP Up

Koutetsu Knife

Gold Helmet
Carabini Mail
Red Shoes

Asura, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Charge+1, Charge+2, Charge+3



Kaidykat
Female
Pisces
73
74
Dancer
White Magic
Counter Flood
Equip Sword
Waterwalking

Asura Knife

Golden Hairpin
Clothes
Elf Mantle

Witch Hunt, Last Dance, Nether Demon
Raise, Protect, Esuna



BStarTV
Male
Pisces
73
59
Knight
Throw
Caution
Equip Sword
Move+2

Platinum Sword
Ice Shield
Genji Helmet
Light Robe
Magic Ring

Head Break, Weapon Break
Knife

Final Bets: red - 28 bets for 28,046G (84.7%, x0.18); blue - 14 bets for 5,085G (15.3%, x5.52)

red bets:
Mesmaster: 6,000G (21.4%, 47,763G)
roofiepops: 4,000G (14.3%, 46,401G)
rico_flex: 3,000G (10.7%, 22,924G)
nifboy: 2,234G (8.0%, 2,234G)
Lordminsc: 1,306G (4.7%, 1,306G)
DustBirdEX: 1,234G (4.4%, 13,180G)
Draconis345: 1,000G (3.6%, 74,640G)
Kezia444: 1,000G (3.6%, 1,827G)
BirbBrainsBot: 1,000G (3.6%, 139,695G)
EnemyController: 1,000G (3.6%, 242,435G)
Error72: 1,000G (3.6%, 5,643G)
Zeroroute: 818G (2.9%, 818G)
volgrathemoose: 600G (2.1%, 600G)
CorpusCav: 535G (1.9%, 535G)
passermine: 500G (1.8%, 4,575G)
KasugaiRoastedPeas: 488G (1.7%, 488G)
luminarii: 450G (1.6%, 13,329G)
ANFz: 328G (1.2%, 328G)
vorap: 300G (1.1%, 54,119G)
joewcarson: 300G (1.1%, 7,168G)
tronfonne: 200G (0.7%, 2,419G)
Arbitae: 153G (0.5%, 153G)
Tsuike: 100G (0.4%, 424G)
a_mo22: 100G (0.4%, 882G)
silverelmdor: 100G (0.4%, 845G)
datadrivenbot: 100G (0.4%, 19,138G)
ungabunga_bot: 100G (0.4%, 532,751G)
moocaotao: 100G (0.4%, 814G)

blue bets:
MMagnetMan: 1,000G (19.7%, 3,352G)
Lionhermit: 1,000G (19.7%, 4,400G)
Moshyhero: 764G (15.0%, 764G)
Maldoree: 689G (13.5%, 689G)
DeathTaxesAndAnime: 532G (10.5%, 532G)
Saldarin: 200G (3.9%, 3,026G)
fenaen: 200G (3.9%, 2,337G)
Tougou: 100G (2.0%, 5,041G)
Firesheath: 100G (2.0%, 2,771G)
mirapoix: 100G (2.0%, 2,151G)
holyonline: 100G (2.0%, 8,105G)
AUrato: 100G (2.0%, 3,379G)
Dymntd: 100G (2.0%, 1,348G)
FlohIristhrae: 100G (2.0%, 2,082G)

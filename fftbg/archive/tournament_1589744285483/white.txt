Player: !White
Team: White Team
Palettes: White/Blue



LDSkinny
Female
Sagittarius
61
69
Oracle
Throw
Dragon Spirit
Halve MP
Ignore Terrain

Bestiary

Holy Miter
Mystic Vest
Vanish Mantle

Dispel Magic, Paralyze, Sleep, Dark Holy
Shuriken, Knife, Staff



Nifboy
Female
Aries
46
74
Knight
Steal
Faith Save
Short Status
Waterwalking

Star Bag
Genji Shield
Genji Helmet
Genji Armor
Battle Boots

Armor Break, Shield Break, Magic Break, Speed Break, Power Break
Steal Armor



Roofiepops
Male
Gemini
48
43
Time Mage
Throw
Arrow Guard
Short Charge
Levitate

Sage Staff

Black Hood
Chameleon Robe
Germinas Boots

Haste, Stop, Demi, Stabilize Time
Shuriken



HaplessOne
Male
Leo
75
79
Thief
Elemental
Regenerator
Dual Wield
Jump+3

Assassin Dagger
Dagger
Feather Hat
Leather Outfit
Feather Boots

Steal Armor, Steal Shield, Arm Aim
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

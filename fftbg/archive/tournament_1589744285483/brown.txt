Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fenaen
Female
Aries
79
73
Dancer
Draw Out
Counter Magic
Equip Sword
Fly

Asura Knife

Twist Headband
Leather Outfit
Battle Boots

Wiznaibus, Slow Dance, Disillusion, Last Dance
Asura, Koutetsu, Bizen Boat



Kyune
Male
Aries
63
73
Geomancer
Yin Yang Magic
Brave Save
Secret Hunt
Fly

Giant Axe
Platinum Shield
Green Beret
Wizard Robe
Power Wrist

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Dark Holy



AUrato
Female
Gemini
52
61
Lancer
Charge
Speed Save
Maintenance
Jump+1

Spear
Crystal Shield
Diamond Helmet
Gold Armor
N-Kai Armlet

Level Jump8, Vertical Jump8
Charge+1, Charge+4, Charge+7



Kezia444
Male
Sagittarius
60
77
Calculator
Pet Skill
Dragon Spirit
Short Charge
Move+3

Thunder Rod

Headgear
Linen Cuirass
108 Gems

Blue Magic
Scratch Up, Feather Bomb, Shine Lover, Peck, Beak, Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck

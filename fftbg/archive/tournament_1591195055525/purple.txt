Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nhammen
Male
Virgo
45
71
Priest
Steal
Hamedo
Doublehand
Move-MP Up

Flail

Golden Hairpin
Judo Outfit
108 Gems

Cure, Cure 3, Regen, Protect, Protect 2, Shell, Shell 2
Steal Heart, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim



Brokenknight201
Male
Sagittarius
43
42
Samurai
Time Magic
HP Restore
Sicken
Jump+2

Asura Knife

Mythril Helmet
White Robe
Diamond Armlet

Koutetsu, Bizen Boat, Kikuichimoji, Masamune
Stop, Reflect, Meteor



UmaiJam
Female
Libra
50
38
Summoner
Yin Yang Magic
Meatbone Slash
Short Charge
Move-HP Up

Thunder Rod

Triangle Hat
Clothes
Bracer

Moogle, Shiva, Ramuh, Titan, Golem, Silf, Lich
Life Drain, Silence Song, Confusion Song, Dispel Magic



Sinnyil2
Female
Scorpio
63
49
Oracle
Math Skill
Mana Shield
Magic Defense UP
Ignore Terrain

Cypress Rod

Leather Hat
Power Sleeve
Rubber Shoes

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Dispel Magic
CT, Height, Prime Number, 4

Final Bets: green - 6 bets for 3,534G (43.2%, x1.31); yellow - 8 bets for 4,642G (56.8%, x0.76)

green bets:
BirbBrainsBot: 1,000G (28.3%, 72,524G)
DuraiPapers: 1,000G (28.3%, 28,971G)
getthemoneyz: 486G (13.8%, 1,007,988G)
ColetteMSLP: 424G (12.0%, 424G)
CT_5_Holy: 324G (9.2%, 324G)
twelfthrootoftwo: 300G (8.5%, 1,588G)

yellow bets:
ShintaroNayaka: 1,594G (34.3%, 15,941G)
prince_rogers_nelson_: 1,529G (32.9%, 1,529G)
solomongrundy85: 548G (11.8%, 548G)
gorgewall: 301G (6.5%, 5,242G)
extinctrational: 250G (5.4%, 1,027G)
ar_tactic: 200G (4.3%, 55,233G)
Lydian_C: 120G (2.6%, 151,874G)
datadrivenbot: 100G (2.2%, 48,790G)

Final Bets: white - 5 bets for 2,967G (51.7%, x0.93); black - 9 bets for 2,771G (48.3%, x1.07)

white bets:
DeathTaxesAndAnime: 1,199G (40.4%, 1,199G)
BirbBrainsBot: 1,000G (33.7%, 73,838G)
solomongrundy85: 548G (18.5%, 548G)
Lydian_C: 120G (4.0%, 151,754G)
Spuzzmocker: 100G (3.4%, 1,505G)

black bets:
prince_rogers_nelson_: 600G (21.7%, 600G)
ShintaroNayaka: 400G (14.4%, 14,347G)
getthemoneyz: 320G (11.5%, 1,008,626G)
gorgewall: 301G (10.9%, 4,941G)
serperemagus: 300G (10.8%, 28,608G)
twelfthrootoftwo: 300G (10.8%, 1,982G)
extinctrational: 250G (9.0%, 777G)
ar_tactic: 200G (7.2%, 55,033G)
datadrivenbot: 100G (3.6%, 48,690G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



GreatRedDragon
Female
Libra
64
59
Mediator
Jump
Parry
Equip Armor
Retreat

Assassin Dagger

Red Hood
Mythril Vest
Defense Ring

Invitation, Praise, Threaten, Refute, Rehabilitate
Level Jump5, Vertical Jump5



Soudan
Male
Gemini
38
67
Wizard
Punch Art
Faith Up
Long Status
Levitate

Poison Rod

Green Beret
Chameleon Robe
Rubber Shoes

Fire 2, Bolt 2, Ice 3, Empower, Frog
Earth Slash, Purification, Revive, Seal Evil



CgMcWhiskers
Male
Pisces
42
58
Ninja
White Magic
Hamedo
Magic Defense UP
Move-HP Up

Hidden Knife
Iga Knife
Golden Hairpin
Earth Clothes
Angel Ring

Sword
Cure, Cure 3, Raise, Raise 2, Reraise, Regen, Protect 2, Wall



Belivalan
Male
Taurus
68
65
Priest
Talk Skill
Arrow Guard
Monster Talk
Waterwalking

Flame Whip

Black Hood
White Robe
Germinas Boots

Cure, Cure 2, Cure 3, Raise, Shell 2
Praise, Insult, Refute, Rehabilitate

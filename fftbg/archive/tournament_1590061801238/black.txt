Player: !Black
Team: Black Team
Palettes: Black/Red



Powergems
Female
Virgo
37
77
Priest
Item
Critical Quick
Attack UP
Ignore Terrain

Oak Staff

Barette
Adaman Vest
Wizard Mantle

Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Wall
Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



ApplesauceBoss
Female
Virgo
37
77
Priest
Item
Critical Quick
Attack UP
Ignore Terrain

Oak Staff

Barette
Adaman Vest
Wizard Mantle

Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Wall
Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



AUrato
Female
Virgo
37
77
Priest
Item
Critical Quick
Attack UP
Ignore Terrain

Oak Staff

Barette
Adaman Vest
Wizard Mantle

Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Wall
Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



CosmicTactician
Female
Virgo
37
77
Priest
Item
Critical Quick
Attack UP
Ignore Terrain

Oak Staff

Barette
Adaman Vest
Wizard Mantle

Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Wall
Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



Oreo Pizza
Male
Aries
63
76
Oracle
Charge
Meatbone Slash
Long Status
Move+1

Musk Rod

Triangle Hat
Chain Vest
Feather Mantle

Blind, Poison, Life Drain, Silence Song, Confusion Song
Charge+3, Charge+10



Shs
Female
Gemini
72
46
Squire
Black Magic
Counter
Concentrate
Levitate

Poison Bow
Round Shield
Twist Headband
Wizard Outfit
Power Wrist

Accumulate, Throw Stone, Heal, Cheer Up, Scream
Fire, Bolt 2, Ice, Ice 3, Empower



LAGBOT30000
Female
Sagittarius
50
44
Time Mage
Steal
Earplug
Defense UP
Jump+2

Wizard Staff

Golden Hairpin
Chameleon Robe
Battle Boots

Haste, Haste 2, Slow, Immobilize
Steal Heart, Steal Helmet, Steal Shield



HaychDub
Female
Capricorn
53
52
Dancer
Item
Counter Magic
Throw Item
Ignore Height

Ryozan Silk

Thief Hat
Black Robe
Defense Armlet

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance
Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft

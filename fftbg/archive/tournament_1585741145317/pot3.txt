Final Bets: white - 11 bets for 5,575G (32.6%, x2.07); black - 18 bets for 11,527G (67.4%, x0.48)

white bets:
BirbBrainsBot: 1,000G (17.9%, 136,937G)
bad1dea: 1,000G (17.9%, 351,313G)
Treapvort: 658G (11.8%, 658G)
ForagerCats: 616G (11.0%, 1,232G)
z32o: 568G (10.2%, 568G)
waterwatereverywhere: 400G (7.2%, 5,678G)
RezzaDV: 366G (6.6%, 732G)
kingchadking: 356G (6.4%, 356G)
getthemoneyz: 234G (4.2%, 404,200G)
ugoplatamia: 200G (3.6%, 28,033G)
superdevon1: 177G (3.2%, 3,552G)

black bets:
ShintaroNayaka: 1,623G (14.1%, 1,623G)
mirapoix: 1,419G (12.3%, 5,678G)
LuckyLuckLuc2: 1,111G (9.6%, 40,190G)
maakur_: 1,068G (9.3%, 1,068G)
Bryon_W: 1,000G (8.7%, 1,911G)
kai_shee: 900G (7.8%, 36,764G)
leakimiko: 856G (7.4%, 28,557G)
ko2q: 670G (5.8%, 670G)
RongRongArts: 508G (4.4%, 508G)
DeathTaxesAndAnime: 504G (4.4%, 504G)
TheChainNerd: 500G (4.3%, 29,731G)
PatSouI: 328G (2.8%, 328G)
rechaun: 300G (2.6%, 300G)
roqqqpsi: 222G (1.9%, 1,427G)
AllInBot: 218G (1.9%, 218G)
Error72: 100G (0.9%, 4,837G)
Rexamajinx: 100G (0.9%, 3,225G)
datadrivenbot: 100G (0.9%, 14,679G)

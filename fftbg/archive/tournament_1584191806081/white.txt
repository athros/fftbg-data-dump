Player: !White
Team: White Team
Palettes: White/Blue



Ritch01
Male
Libra
79
42
Geomancer
Throw
Speed Save
Short Charge
Levitate

Murasame
Gold Shield
Holy Miter
Mystic Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Bomb, Staff



Bpc2163
Female
Scorpio
62
70
Priest
Jump
Dragon Spirit
Attack UP
Teleport

Morning Star

Leather Hat
Light Robe
Bracer

Cure 2, Cure 4, Raise, Reraise, Shell, Esuna, Holy
Level Jump8, Vertical Jump6



Breakdown777
Male
Pisces
77
72
Squire
Talk Skill
Meatbone Slash
Magic Defense UP
Teleport

Bow Gun
Hero Shield
Barbuta
Brigandine
Defense Armlet

Accumulate, Heal, Yell, Wish
Praise, Threaten, Solution, Mimic Daravon



Haaaaaank
Male
Scorpio
51
69
Chemist
Charge
Damage Split
Short Status
Waterwalking

Hydra Bag

Holy Miter
Power Sleeve
Bracer

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft
Charge+1, Charge+3, Charge+4

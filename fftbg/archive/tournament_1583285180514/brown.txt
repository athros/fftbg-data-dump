Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gabbagooluigi
Male
Leo
57
75
Archer
Elemental
MP Restore
Doublehand
Ignore Terrain

Poison Bow

Barette
Leather Outfit
Rubber Shoes

Charge+1, Charge+3, Charge+4, Charge+7
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Oogthecaveman
Male
Libra
42
63
Lancer
Battle Skill
Critical Quick
Halve MP
Jump+3

Javelin
Mythril Shield
Platinum Helmet
Crystal Mail
Bracer

Level Jump2, Vertical Jump3
Head Break, Magic Break, Speed Break, Power Break, Explosion Sword



Spartan Paladin
Male
Gemini
69
58
Knight
Elemental
Counter Tackle
Doublehand
Lava Walking

Slasher

Circlet
Gold Armor
Red Shoes

Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword, Night Sword
Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Gusty Wind



Rassyu
Female
Libra
68
67
Samurai
Summon Magic
Faith Up
Magic Attack UP
Move+2

Murasame

Leather Helmet
Crystal Mail
Germinas Boots

Bizen Boat, Heaven's Cloud, Muramasa
Moogle, Shiva, Carbunkle

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Bryan792
Monster
Sagittarius
56
41
Ultima Demon










UmaiJam
Male
Capricorn
51
55
Lancer
Steal
Arrow Guard
Long Status
Ignore Height

Javelin
Diamond Shield
Iron Helmet
Chameleon Robe
Diamond Armlet

Level Jump8, Vertical Jump7
Gil Taking, Steal Armor, Steal Weapon, Steal Status



Zmoses
Female
Scorpio
72
69
Mediator
Punch Art
Distribute
Equip Axe
Lava Walking

Wizard Staff

Black Hood
Wizard Outfit
Wizard Mantle

Death Sentence
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive



Leakimiko
Female
Leo
70
66
Ninja
Punch Art
Earplug
Equip Gun
Move+2

Mythril Gun
Blast Gun
Flash Hat
Adaman Vest
Diamond Armlet

Shuriken
Wave Fist, Chakra, Revive, Seal Evil

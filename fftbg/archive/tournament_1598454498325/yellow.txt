Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ConformORdie
Female
Sagittarius
64
40
Chemist
White Magic
Damage Split
Halve MP
Teleport

Dagger

Headgear
Leather Outfit
Power Wrist

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Shell, Shell 2, Esuna



IndecisiveNinja
Female
Taurus
74
50
Wizard
White Magic
HP Restore
Short Charge
Move-HP Up

Flame Rod

Leather Hat
Linen Robe
Magic Ring

Fire, Bolt 4, Ice, Ice 3, Death
Cure, Cure 4, Raise 2, Regen, Protect, Shell, Esuna, Holy



Fenaen
Male
Serpentarius
54
74
Ninja
Black Magic
HP Restore
Concentrate
Move-HP Up

Flail
Morning Star
Feather Hat
Secret Clothes
Battle Boots

Shuriken, Sword, Dictionary
Bolt, Bolt 2, Bolt 3, Bolt 4, Flare



LDSkinny
Female
Libra
37
60
Oracle
Time Magic
Hamedo
Equip Gun
Retreat

Papyrus Codex

Golden Hairpin
Linen Robe
Spike Shoes

Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic
Haste 2, Slow, Stop, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time

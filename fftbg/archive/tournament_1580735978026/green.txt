Player: !Green
Team: Green Team
Palettes: Green/White



TheKillerNacho
Female
Gemini
60
54
Dancer
Goblin Skill
Catch
Equip Knife
Fly

Persia

Thief Hat
Secret Clothes
Power Wrist

Slow Dance, Nameless Dance, Last Dance, Nether Demon




Willjin
Female
Aries
76
53
Priest
Battle Skill
Parry
Short Charge
Swim

Rainbow Staff

Black Hood
Power Sleeve
Chantage

Cure 3, Protect, Shell, Esuna
Shield Break, Speed Break, Justice Sword



Ramuzai
Male
Libra
54
46
Samurai
Battle Skill
Speed Save
Short Charge
Jump+1

Muramasa

Leather Helmet
Bronze Armor
Power Wrist

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Armor Break, Magic Break, Mind Break, Justice Sword



Paperthinhere
Female
Capricorn
65
57
Calculator
Time Magic
Parry
Doublehand
Ignore Height

Octagon Rod

Holy Miter
Chameleon Robe
Spike Shoes

CT, Height, Prime Number
Haste, Stop, Immobilize, Float, Quick, Demi, Stabilize Time

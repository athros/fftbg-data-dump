Player: !White
Team: White Team
Palettes: White/Blue



CapnChaos12
Monster
Aries
45
47
King Behemoth










CorpusCav
Female
Scorpio
75
46
Oracle
Math Skill
Counter
Dual Wield
Move+1

Cypress Rod
Octagon Rod
Triangle Hat
Light Robe
Small Mantle

Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify
CT, Height, Prime Number, 4, 3



Powergems
Male
Aquarius
58
46
Oracle
Math Skill
Parry
Equip Shield
Lava Walking

Ivory Rod
Escutcheon
Ribbon
Earth Clothes
Diamond Armlet

Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Dark Holy
Height, Prime Number, 4



Omegasuspekt
Male
Serpentarius
60
61
Squire
Summon Magic
MP Restore
Sicken
Waterbreathing

Scorpion Tail
Escutcheon
Headgear
Black Costume
Cherche

Accumulate, Heal
Moogle, Ifrit, Salamander, Silf

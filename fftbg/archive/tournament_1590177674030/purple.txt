Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vorap
Female
Virgo
53
44
Oracle
Punch Art
Distribute
Equip Shield
Swim

Octagon Rod
Gold Shield
Triangle Hat
Black Robe
Cursed Ring

Silence Song, Dispel Magic, Sleep, Dark Holy
Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification



Skillomono
Male
Pisces
47
61
Archer
Jump
PA Save
Doublehand
Ignore Height

Night Killer

Feather Hat
Mythril Vest
Small Mantle

Charge+1, Charge+4
Level Jump3, Vertical Jump2



Nedryerson01
Male
Aries
53
57
Knight
Item
Critical Quick
Equip Sword
Lava Walking

Kikuichimoji
Mythril Shield
Mythril Helmet
Reflect Mail
Cursed Ring

Head Break, Armor Break, Shield Break, Stasis Sword, Night Sword, Explosion Sword
Potion, Ether, Hi-Ether, Elixir, Antidote, Soft, Holy Water



Elelor
Male
Scorpio
48
71
Monk
Time Magic
Caution
Dual Wield
Swim



Leather Hat
Wizard Outfit
Jade Armlet

Wave Fist, Revive, Seal Evil
Haste, Immobilize, Float, Reflect, Stabilize Time

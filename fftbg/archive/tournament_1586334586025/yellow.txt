Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SeedSC
Male
Capricorn
70
59
Archer
Punch Art
Absorb Used MP
Doublehand
Jump+2

Long Bow

Feather Hat
Power Sleeve
Cherche

Charge+1, Charge+3, Charge+5
Wave Fist, Secret Fist, Chakra



Sairentozon7
Male
Taurus
48
63
Ninja
Black Magic
MP Restore
Equip Shield
Move-MP Up

Flail
Aegis Shield
Feather Hat
Chain Vest
Reflect Ring

Shuriken, Bomb, Hammer
Fire, Fire 2, Fire 4, Bolt, Frog



Chuckolator
Monster
Aquarius
59
52
Ghost










Rhuarc Redhammer
Male
Aquarius
70
77
Wizard
Item
Critical Quick
Throw Item
Jump+2

Flame Rod

Cachusha
Clothes
Magic Ring

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3
Potion, Hi-Potion, Ether, Antidote, Eye Drop, Phoenix Down

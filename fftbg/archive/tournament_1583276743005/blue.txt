Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Trowaba
Male
Scorpio
70
44
Lancer
Elemental
Brave Up
Maintenance
Levitate

Holy Lance
Crystal Shield
Gold Helmet
White Robe
Bracer

Level Jump8, Vertical Jump7
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



LanseDM
Monster
Leo
53
56
Vampire










Oreo Pizza
Male
Aquarius
68
73
Wizard
Basic Skill
Abandon
Short Charge
Move+1

Mythril Knife

Headgear
Linen Robe
Spike Shoes

Fire 2, Fire 4, Bolt 2, Frog, Flare
Accumulate, Throw Stone, Heal, Yell, Cheer Up



1twistedpuppy
Female
Leo
67
41
Ninja
Charge
Auto Potion
Equip Bow
Swim

Mythril Bow

Leather Hat
Rubber Costume
Diamond Armlet

Shuriken, Dictionary
Charge+3, Charge+5, Charge+10

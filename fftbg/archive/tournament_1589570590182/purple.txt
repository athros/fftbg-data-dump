Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheUnforgivenRage
Female
Libra
70
69
Monk
Summon Magic
Auto Potion
Magic Defense UP
Ignore Height



Twist Headband
Power Sleeve
Bracer

Wave Fist, Purification
Moogle, Shiva, Ramuh, Golem, Bahamut



Mirapoix
Female
Taurus
76
63
Geomancer
Item
Counter Tackle
Throw Item
Jump+1

Giant Axe
Mythril Shield
Cachusha
Light Robe
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Potion, Soft, Holy Water, Remedy, Phoenix Down



MalakiGenesys
Monster
Taurus
66
50
Ahriman










MibudemoneyeKyo
Female
Pisces
71
51
Knight
White Magic
Critical Quick
Long Status
Levitate

Blood Sword
Gold Shield
Leather Helmet
Linen Cuirass
Dracula Mantle

Magic Break, Power Break, Mind Break
Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect, Wall, Esuna

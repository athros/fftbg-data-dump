Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ultimainferno
Female
Sagittarius
48
79
Mime

Abandon
Magic Defense UP
Fly



Green Beret
Linen Robe
Red Shoes

Mimic




DarrenDinosaurs
Male
Sagittarius
42
45
Ninja
Draw Out
Earplug
Attack UP
Fly

Main Gauche
Short Edge
Triangle Hat
Brigandine
Defense Ring

Shuriken
Asura, Bizen Boat, Murasame, Kiyomori, Muramasa



WhiteTigress
Female
Aries
62
76
Wizard
Summon Magic
MP Restore
Short Charge
Fly

Rod

Ribbon
Earth Clothes
Red Shoes

Fire 2, Fire 3, Bolt 2, Bolt 4, Ice, Frog
Golem, Carbunkle, Bahamut, Lich, Cyclops



Claytonio
Male
Gemini
76
48
Monk
Item
Absorb Used MP
Equip Gun
Fly

Battle Folio

Black Hood
Secret Clothes
Elf Mantle

Wave Fist, Secret Fist, Purification, Chakra
Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Phoenix Down

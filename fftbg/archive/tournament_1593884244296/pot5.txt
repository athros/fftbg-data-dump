Final Bets: red - 15 bets for 5,840G (33.1%, x2.02); green - 13 bets for 11,805G (66.9%, x0.49)

red bets:
sinnyil2: 1,135G (19.4%, 1,135G)
BirbBrainsBot: 1,000G (17.1%, 15,112G)
amiture: 542G (9.3%, 542G)
neocarbuncle: 510G (8.7%, 4,031G)
MagicBottle: 492G (8.4%, 18,601G)
getthemoneyz: 410G (7.0%, 1,181,120G)
nekojin: 400G (6.8%, 3,434G)
dogsandcatsand: 333G (5.7%, 1,966G)
gorgewall: 201G (3.4%, 5,371G)
KasugaiRoastedPeas: 200G (3.4%, 9,071G)
GatsbysGhost: 200G (3.4%, 5,034G)
DustBirdEX: 113G (1.9%, 961G)
skipsandwiches: 104G (1.8%, 104G)
nifboy: 100G (1.7%, 13,384G)
ANFz: 100G (1.7%, 25,541G)

green bets:
toka222: 6,500G (55.1%, 92,343G)
roqqqpsi: 1,380G (11.7%, 138,056G)
TheChainNerd: 630G (5.3%, 6,307G)
Baron_von_Scrub: 600G (5.1%, 600G)
maakur_: 508G (4.3%, 508G)
Laserman1000: 500G (4.2%, 22,300G)
ArashiKurobara: 500G (4.2%, 10,377G)
Rilgon: 354G (3.0%, 354G)
xLilAsia: 208G (1.8%, 208G)
Rytor: 200G (1.7%, 15,237G)
RageImmortaI: 172G (1.5%, 172G)
SephDarkheart: 153G (1.3%, 153G)
Error72: 100G (0.8%, 11,849G)

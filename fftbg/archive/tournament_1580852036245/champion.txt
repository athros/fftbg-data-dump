Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Stratosthegreek
Male
Sagittarius
43
42
Archer
Punch Art
Speed Save
Concentrate
Lava Walking

Lightning Bow

Headgear
Power Sleeve
Jade Armlet

Charge+3, Charge+7, Charge+10, Charge+20
Earth Slash, Secret Fist, Purification, Revive



CorpusCav
Male
Taurus
79
59
Squire
Yin Yang Magic
Sunken State
Concentrate
Move+2

Flail

Leather Hat
Mythril Vest
Red Shoes

Accumulate, Heal
Doubt Faith, Silence Song, Dispel Magic, Dark Holy



Brondius
Female
Aquarius
78
70
Geomancer
Draw Out
Counter Tackle
Defense UP
Waterbreathing

Slasher
Genji Shield
Thief Hat
Light Robe
Jade Armlet

Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Koutetsu, Bizen Boat, Heaven's Cloud, Masamune



AniZero
Female
Gemini
68
68
Oracle
White Magic
Mana Shield
Concentrate
Fly

Bestiary

Twist Headband
Silk Robe
Elf Mantle

Blind, Poison, Foxbird, Confusion Song, Dispel Magic, Sleep
Raise, Regen, Shell 2, Esuna

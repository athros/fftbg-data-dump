Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Coralreeferz
Male
Leo
53
75
Archer
Basic Skill
Meatbone Slash
Doublehand
Move-MP Up

Silver Bow

Holy Miter
Brigandine
Defense Ring

Charge+1, Charge+3
Accumulate, Dash, Heal



Astrozin11
Female
Pisces
67
50
Summoner
Punch Art
Counter Tackle
Short Charge
Jump+3

Rod

Thief Hat
Chameleon Robe
108 Gems

Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Cyclops
Pummel, Secret Fist, Purification, Revive



Laserman1000
Male
Sagittarius
62
56
Squire
White Magic
Counter Flood
Martial Arts
Swim


Flame Shield
Circlet
Mystic Vest
Dracula Mantle

Dash, Throw Stone, Tickle, Wish
Raise, Raise 2, Reraise, Regen, Protect



Byrdturbo
Male
Virgo
75
58
Wizard
Sing
Distribute
Equip Axe
Move-MP Up

Flame Whip

Barette
Silk Robe
Feather Mantle

Fire, Bolt 2, Ice, Ice 4, Frog, Death, Flare
Cheer Song, Battle Song

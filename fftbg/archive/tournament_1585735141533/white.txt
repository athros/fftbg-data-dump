Player: !White
Team: White Team
Palettes: White/Blue



Byrdturbo
Male
Aries
41
45
Wizard
Talk Skill
Counter
Attack UP
Move-HP Up

Ice Rod

Black Hood
White Robe
Rubber Shoes

Fire 2, Fire 3, Bolt 2, Ice, Ice 2, Ice 3, Ice 4, Flare
Invitation, Threaten, Solution, Insult, Refute



Defaultlybrave
Female
Taurus
57
43
Chemist
White Magic
Earplug
Defense UP
Move+1

Blaze Gun

Twist Headband
Black Costume
Dracula Mantle

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down
Cure, Regen, Shell 2, Esuna



BuffaloCrunch
Male
Pisces
77
64
Ninja
Sing
Parry
Beastmaster
Move-MP Up

Flail
Sasuke Knife
Barette
Power Sleeve
Leather Mantle

Shuriken, Bomb, Knife
Life Song, Space Storage



Leakimiko
Female
Sagittarius
50
45
Priest
Draw Out
Regenerator
Long Status
Jump+1

Wizard Staff

Twist Headband
Black Robe
Wizard Mantle

Cure, Cure 2, Cure 4, Raise, Raise 2, Shell 2, Wall, Esuna, Holy
Asura, Bizen Boat, Kiyomori, Kikuichimoji

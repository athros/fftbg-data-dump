Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



UmbraKnights
Male
Taurus
71
53
Chemist
Sing
Earplug
Equip Armor
Ignore Height

Cute Bag

Platinum Helmet
Leather Outfit
Magic Ring

Potion, Hi-Potion, Eye Drop, Holy Water, Remedy
Life Song, Cheer Song, Diamond Blade, Hydra Pit



Volgrathemoose
Female
Virgo
47
44
Ninja
Black Magic
Abandon
Equip Gun
Jump+2

Papyrus Codex
Bestiary
Red Hood
Black Costume
108 Gems

Shuriken
Fire, Fire 3, Bolt, Ice, Ice 2, Ice 3, Empower



Superdevon1
Female
Virgo
46
50
Calculator
Black Magic
MA Save
Equip Gun
Ignore Terrain

Mythril Gun

Golden Hairpin
White Robe
Germinas Boots

CT, Height, Prime Number, 4, 3
Bolt, Bolt 2, Ice 3



ThePineappleSalesman
Female
Aries
39
64
Samurai
Punch Art
Speed Save
Beastmaster
Jump+1

Holy Lance

Circlet
Reflect Mail
N-Kai Armlet

Koutetsu
Pummel, Wave Fist, Earth Slash, Purification

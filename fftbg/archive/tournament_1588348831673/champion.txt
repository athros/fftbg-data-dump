Player: !zChamp
Team: Champion Team
Palettes: White/Blue



RubenFlonne
Female
Taurus
70
47
Chemist
Summon Magic
HP Restore
Magic Attack UP
Waterwalking

Cultist Dagger

Flash Hat
Power Sleeve
Magic Gauntlet

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Moogle, Ramuh, Golem, Odin, Silf, Lich, Cyclops



Solomongrundy85
Female
Libra
53
69
Samurai
Jump
PA Save
Magic Attack UP
Waterbreathing

Kiyomori

Crystal Helmet
Chameleon Robe
Battle Boots

Asura, Koutetsu, Murasame, Muramasa, Kikuichimoji
Level Jump8, Vertical Jump3



UmaiJam
Male
Sagittarius
74
51
Archer
Draw Out
Counter
Doublehand
Move+3

Cross Bow

Circlet
Earth Clothes
Diamond Armlet

Charge+3, Charge+4, Charge+5, Charge+10
Heaven's Cloud



Realitydown
Male
Cancer
55
54
Time Mage
Item
Auto Potion
Attack UP
Teleport

Madlemgen

Ribbon
Wizard Outfit
Wizard Mantle

Slow 2, Stop, Float, Reflect, Quick, Demi, Demi 2
Hi-Potion, Elixir, Maiden's Kiss, Holy Water

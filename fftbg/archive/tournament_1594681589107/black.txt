Player: !Black
Team: Black Team
Palettes: Black/Red



Powergems
Monster
Aries
61
42
Pisco Demon










Rurk
Female
Serpentarius
61
72
Geomancer
Steal
Mana Shield
Defense UP
Move-HP Up

Giant Axe
Escutcheon
Black Hood
Chameleon Robe
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Gil Taking, Steal Accessory, Steal Status, Arm Aim



Joewcarson
Male
Taurus
47
79
Samurai
Elemental
Sunken State
Short Status
Retreat

Asura Knife

Gold Helmet
Genji Armor
Genji Gauntlet

Koutetsu, Murasame, Kiyomori
Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Nizaha
Female
Taurus
79
58
Thief
Elemental
MA Save
Concentrate
Levitate

Diamond Sword

Black Hood
Leather Outfit
Chantage

Gil Taking, Steal Heart, Steal Weapon, Arm Aim
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

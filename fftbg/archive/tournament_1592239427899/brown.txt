Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CosmicTactician
Female
Cancer
70
76
Lancer
Talk Skill
Catch
Halve MP
Move-MP Up

Cypress Rod
Mythril Shield
Grand Helmet
Linen Robe
Dracula Mantle

Level Jump3, Vertical Jump6
Solution, Death Sentence, Refute



Run With Stone GUNs
Male
Aries
76
64
Thief
Time Magic
Regenerator
Equip Gun
Move+2

Glacier Gun

Thief Hat
Adaman Vest
Genji Gauntlet

Steal Heart, Steal Armor, Steal Accessory, Leg Aim
Haste, Stop, Immobilize, Float, Demi, Meteor



ShintaroNayaka
Female
Sagittarius
70
60
Dancer
White Magic
Damage Split
Equip Axe
Waterbreathing

Flail

Triangle Hat
Judo Outfit
Rubber Shoes

Wiznaibus, Slow Dance, Polka Polka, Last Dance, Void Storage
Cure 2, Raise, Regen, Protect, Shell, Wall, Esuna, Holy



Kronikle
Male
Leo
65
66
Lancer
Steal
Counter Flood
Doublehand
Move+2

Mythril Spear

Genji Helmet
White Robe
Dracula Mantle

Level Jump2, Vertical Jump7
Steal Weapon, Steal Status, Leg Aim

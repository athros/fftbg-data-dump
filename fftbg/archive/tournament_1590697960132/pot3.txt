Final Bets: white - 24 bets for 10,191G (36.4%, x1.74); black - 15 bets for 17,777G (63.6%, x0.57)

white bets:
metagameface: 1,829G (17.9%, 1,829G)
noopockets: 778G (7.6%, 778G)
HaateXIII: 750G (7.4%, 25,010G)
Aldrammech: 600G (5.9%, 600G)
Laserman1000: 600G (5.9%, 27,100G)
skillomono: 502G (4.9%, 502G)
killth3kid: 500G (4.9%, 31,125G)
Baron_von_Scrub: 474G (4.7%, 20,780G)
CosmicTactician: 464G (4.6%, 464G)
volgrathemoose: 452G (4.4%, 452G)
YaBoy125: 420G (4.1%, 31,342G)
Firesheath: 388G (3.8%, 388G)
coralreeferz: 376G (3.7%, 376G)
kaelsun: 372G (3.7%, 372G)
CapnChaos12: 300G (2.9%, 18,681G)
AllInBot: 282G (2.8%, 282G)
goodnightrobo: 252G (2.5%, 252G)
Nizaha: 201G (2.0%, 8,619G)
Kyune: 200G (2.0%, 32,274G)
gorgewall: 101G (1.0%, 19,191G)
bluuuuuuuh: 100G (1.0%, 5,640G)
datadrivenbot: 100G (1.0%, 28,535G)
Flameatron: 100G (1.0%, 312G)
Sharosa: 50G (0.5%, 294G)

black bets:
E_Ballard: 7,109G (40.0%, 7,109G)
toka222: 5,099G (28.7%, 109,409G)
Rislyeu: 1,129G (6.4%, 1,129G)
BirbBrainsBot: 1,000G (5.6%, 60,621G)
TinchoT: 582G (3.3%, 582G)
Jinxzers: 500G (2.8%, 8,229G)
lifeguard_dan: 478G (2.7%, 478G)
Deathmaker06: 396G (2.2%, 396G)
StealthModeLocke: 364G (2.0%, 364G)
DLJuggernaut: 350G (2.0%, 7,549G)
benticore: 250G (1.4%, 1,250G)
getthemoneyz: 220G (1.2%, 762,617G)
OneHundredFists: 100G (0.6%, 28,452G)
ravingsockmonkey: 100G (0.6%, 330G)
Evewho: 100G (0.6%, 11,763G)

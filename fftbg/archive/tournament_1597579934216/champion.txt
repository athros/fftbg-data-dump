Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



HASTERIOUS
Female
Libra
71
44
Geomancer
Draw Out
Dragon Spirit
Long Status
Levitate

Ancient Sword
Ice Shield
Triangle Hat
Secret Clothes
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Asura, Bizen Boat, Heaven's Cloud



Gorgewall
Male
Taurus
42
52
Archer
Talk Skill
Auto Potion
Sicken
Move-HP Up

Mythril Bow

Black Hood
Mythril Vest
Feather Mantle

Charge+1, Charge+5, Charge+10
Invitation, Persuade, Threaten, Preach, Refute



Creggers
Monster
Aries
36
43
Holy Dragon










Willjin
Male
Aries
74
73
Knight
Charge
Parry
Halve MP
Swim

Defender
Crystal Shield
Grand Helmet
Leather Armor
Sprint Shoes

Head Break, Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Night Sword, Explosion Sword
Charge+3, Charge+20

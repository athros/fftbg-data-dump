Player: !Black
Team: Black Team
Palettes: Black/Red



SephDarkheart
Male
Cancer
64
57
Samurai
Summon Magic
Absorb Used MP
Equip Axe
Fly

Battle Axe

Gold Helmet
Light Robe
Sprint Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud
Moogle, Ramuh, Ifrit, Carbunkle



Actual JP
Male
Gemini
40
61
Knight
Time Magic
Counter Magic
Short Status
Ignore Terrain

Mythril Sword
Diamond Shield
Iron Helmet
Chameleon Robe
Dracula Mantle

Armor Break, Magic Break, Mind Break, Explosion Sword
Haste, Slow 2, Immobilize, Float, Reflect, Demi, Demi 2, Meteor



Panushenko
Female
Capricorn
57
72
Priest
Dance
MP Restore
Equip Sword
Move-HP Up

Ragnarok

Leather Hat
Wizard Outfit
Wizard Mantle

Cure 3, Raise, Wall, Esuna
Slow Dance, Nameless Dance, Last Dance, Obsidian Blade, Nether Demon



E Ballard
Female
Taurus
44
59
Calculator
Mighty Sword
Arrow Guard
Halve MP
Move+2

Papyrus Codex
Gold Shield
Twist Headband
Crystal Mail
Elf Mantle

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite

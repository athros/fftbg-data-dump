Final Bets: purple - 7 bets for 4,225G (43.8%, x1.28); brown - 9 bets for 5,414G (56.2%, x0.78)

purple bets:
getthemoneyz: 1,000G (23.7%, 1,658,912G)
BirbBrainsBot: 1,000G (23.7%, 172,567G)
dogsandcatsand: 942G (22.3%, 942G)
VolgraTheMoose: 501G (11.9%, 5,477G)
roqqqpsi: 442G (10.5%, 3,164G)
Panushenko: 212G (5.0%, 212G)
arctodus13: 128G (3.0%, 128G)

brown bets:
Mesmaster: 2,000G (36.9%, 16,691G)
Smashy: 1,000G (18.5%, 8,778G)
ko2q: 1,000G (18.5%, 1,622G)
SeniorBunk: 452G (8.3%, 452G)
WhattayaBrian: 225G (4.2%, 225G)
datadrivenbot: 200G (3.7%, 66,065G)
Saldarin: 200G (3.7%, 8,406G)
gongonono: 181G (3.3%, 181G)
AllInBot: 156G (2.9%, 156G)

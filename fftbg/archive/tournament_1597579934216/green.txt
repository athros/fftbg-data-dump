Player: !Green
Team: Green Team
Palettes: Green/White



Gongonono
Female
Taurus
43
61
Archer
Talk Skill
Absorb Used MP
Doublehand
Move+2

Bow Gun

Black Hood
Mythril Vest
108 Gems

Charge+1, Charge+3, Charge+5
Persuade, Threaten, Insult, Refute



Willjin
Female
Gemini
55
45
Chemist
Throw
Distribute
Equip Polearm
Swim

Spear

Green Beret
Judo Outfit
Diamond Armlet

Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Bomb



Mesmaster
Female
Scorpio
67
38
Samurai
White Magic
Regenerator
Doublehand
Retreat

Kikuichimoji

Platinum Helmet
Bronze Armor
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Cure 2, Cure 3, Raise, Regen, Protect, Protect 2, Shell, Esuna



TheMurkGnome
Male
Virgo
78
54
Thief
Basic Skill
Abandon
Equip Bow
Jump+1

Bow Gun

Feather Hat
Leather Outfit
Power Wrist

Gil Taking, Steal Helmet, Steal Weapon
Accumulate, Throw Stone, Tickle

Final Bets: red - 14 bets for 7,370G (62.5%, x0.60); blue - 9 bets for 4,430G (37.5%, x1.66)

red bets:
Shakarak: 2,354G (31.9%, 2,354G)
leakimiko: 1,554G (21.1%, 31,082G)
z32o: 753G (10.2%, 753G)
RongRongArts: 594G (8.1%, 594G)
Cataphract116: 591G (8.0%, 591G)
HaplessOne: 500G (6.8%, 41,655G)
DustBirdEX: 320G (4.3%, 320G)
jhazor: 104G (1.4%, 104G)
chaddong: 100G (1.4%, 100G)
OverlordCuddles: 100G (1.4%, 100G)
ko2q: 100G (1.4%, 871G)
datadrivenbot: 100G (1.4%, 17,869G)
Treapvort: 100G (1.4%, 18,734G)
PiCurious: 100G (1.4%, 5,197G)

blue bets:
getthemoneyz: 1,000G (22.6%, 402,769G)
inzo_bettingbot: 1,000G (22.6%, 42,425G)
BirbBrainsBot: 1,000G (22.6%, 23,152G)
bad1dea: 1,000G (22.6%, 250,788G)
Egil72: 283G (6.4%, 283G)
WinnerBit: 50G (1.1%, 1,041G)
dreadnxught: 50G (1.1%, 4,880G)
Meowyster_: 25G (0.6%, 182G)
roqqqpsi: 22G (0.5%, 1,995G)

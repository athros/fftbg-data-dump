Player: !Green
Team: Green Team
Palettes: Green/White



Djorama
Female
Sagittarius
60
49
Time Mage
Draw Out
MP Restore
Short Charge
Waterwalking

Rainbow Staff

Twist Headband
Secret Clothes
Cursed Ring

Haste 2, Slow, Stop, Immobilize, Float, Reflect, Quick, Demi, Stabilize Time
Murasame, Muramasa



Gelwain
Female
Scorpio
36
61
Ninja
Elemental
Counter Magic
Equip Polearm
Retreat

Ivory Rod
Ivory Rod
Feather Hat
Brigandine
Power Wrist

Shuriken, Bomb, Axe, Wand
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



HASTERIOUS
Female
Sagittarius
79
68
Squire
Draw Out
Absorb Used MP
Short Charge
Waterwalking

Star Bag
Escutcheon
Genji Helmet
Power Sleeve
Defense Ring

Throw Stone, Heal, Yell, Cheer Up, Wish
Murasame, Heaven's Cloud



LOKITHUS
Male
Leo
74
54
Knight
Jump
Caution
Sicken
Move+1

Sleep Sword
Hero Shield
Bronze Helmet
Gold Armor
Cherche

Head Break, Shield Break, Speed Break, Mind Break, Dark Sword, Night Sword
Level Jump8, Vertical Jump5

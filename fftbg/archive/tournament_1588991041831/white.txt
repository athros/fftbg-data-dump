Player: !White
Team: White Team
Palettes: White/Blue



Dymntd
Female
Sagittarius
57
60
Summoner
Battle Skill
Counter Tackle
Short Charge
Levitate

White Staff

Holy Miter
Black Costume
Reflect Ring

Ramuh, Golem, Carbunkle, Leviathan, Fairy
Surging Sword



TheNGUYENNER
Male
Leo
50
48
Knight
Charge
Catch
Equip Sword
Waterwalking

Rune Blade
Crystal Shield
Platinum Helmet
White Robe
Defense Armlet

Head Break, Shield Break, Speed Break, Mind Break, Stasis Sword, Dark Sword
Charge+1, Charge+3, Charge+4, Charge+10



Fenixcrest
Female
Aquarius
58
47
Dancer
Item
Hamedo
Concentrate
Teleport

Cashmere

Thief Hat
Brigandine
Germinas Boots

Slow Dance, Last Dance, Void Storage
Potion, Hi-Potion, Eye Drop, Maiden's Kiss, Soft



Lythe Caraker
Female
Scorpio
76
50
Squire
Elemental
PA Save
Equip Gun
Ignore Height

Stone Gun
Gold Shield
Flash Hat
Leather Armor
Magic Gauntlet

Dash, Heal, Yell
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Lava Ball

Player: !Black
Team: Black Team
Palettes: Black/Red



Lionhermit
Monster
Gemini
80
58
Blue Dragon










JTB
Male
Virgo
44
65
Monk
Item
Arrow Guard
Magic Defense UP
Retreat

Cute Bag

Black Hood
Secret Clothes
Vanish Mantle

Pummel, Wave Fist, Secret Fist, Purification, Chakra
Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Shalloween
Male
Pisces
47
72
Archer
Item
Regenerator
Doublehand
Levitate

Poison Bow

Headgear
Mystic Vest
Bracer

Charge+1, Charge+5, Charge+7
Potion, X-Potion, Ether, Hi-Ether, Soft, Phoenix Down



WhiteTigress
Male
Virgo
65
51
Archer
Battle Skill
Caution
Doublehand
Ignore Terrain

Bow Gun

Thief Hat
Power Sleeve
Spike Shoes

Charge+1, Charge+5, Charge+10
Head Break, Armor Break, Shield Break, Power Break, Explosion Sword

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Letdowncity
Male
Capricorn
62
63
Samurai
Basic Skill
HP Restore
Magic Attack UP
Jump+2

Bizen Boat

Iron Helmet
Plate Mail
Rubber Shoes

Kiyomori, Muramasa
Accumulate, Throw Stone, Heal, Cheer Up



YaBoy125
Male
Capricorn
69
55
Knight
Punch Art
Auto Potion
Dual Wield
Ignore Terrain

Materia Blade
Coral Sword
Mythril Helmet
Chain Mail
Battle Boots

Head Break, Shield Break, Magic Break, Stasis Sword, Night Sword
Purification, Chakra, Revive



Nizaha
Female
Gemini
64
78
Samurai
Punch Art
Damage Split
Magic Defense UP
Ignore Height

Gungnir

Cross Helmet
Chain Mail
Red Shoes

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Pummel, Purification, Chakra, Revive



Candina
Monster
Aries
80
78
Holy Dragon








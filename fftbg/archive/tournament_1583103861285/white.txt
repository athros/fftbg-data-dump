Player: !White
Team: White Team
Palettes: White/Blue



Vivithegr8
Female
Aries
69
55
Geomancer
Battle Skill
Arrow Guard
Magic Defense UP
Jump+3

Rune Blade
Round Shield
Headgear
Chain Vest
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball
Head Break, Weapon Break, Speed Break, Mind Break



Riniom
Monster
Virgo
80
56
Byblos










Goust18
Female
Leo
68
75
Archer
Time Magic
Counter
Attack UP
Teleport

Night Killer
Round Shield
Platinum Helmet
Leather Outfit
Spike Shoes

Charge+4
Haste 2, Slow 2, Stop, Reflect, Demi 2, Stabilize Time



Khao5s
Male
Aquarius
62
43
Lancer
Throw
Counter Magic
Concentrate
Swim

Javelin
Diamond Shield
Bronze Helmet
Platinum Armor
Genji Gauntlet

Level Jump4, Vertical Jump2
Shuriken

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Butterbelljedi
Female
Leo
79
45
Priest
Draw Out
Abandon
Equip Armor
Teleport

Rainbow Staff

Gold Helmet
Diamond Armor
Germinas Boots

Cure 4, Raise, Raise 2, Regen, Shell 2, Wall, Esuna, Holy
Asura, Murasame



Leonidusx
Male
Serpentarius
65
78
Knight
Summon Magic
MA Save
Magic Attack UP
Fly

Defender
Diamond Shield
Genji Helmet
Linen Cuirass
Red Shoes

Weapon Break, Speed Break, Power Break, Justice Sword, Night Sword
Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Fairy



SkylerBunny
Female
Scorpio
50
59
Geomancer
Time Magic
Damage Split
Dual Wield
Ignore Terrain

Giant Axe
Ancient Sword
Flash Hat
Chain Vest
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Haste, Immobilize, Reflect, Quick



Sinnyil2
Male
Cancer
73
78
Priest
Sing
HP Restore
Defense UP
Ignore Terrain

Flame Whip

Golden Hairpin
White Robe
N-Kai Armlet

Cure, Raise, Protect, Esuna
Battle Song, Magic Song, Sky Demon, Hydra Pit

Player: !White
Team: White Team
Palettes: White/Blue



DrAntiSocial
Male
Libra
47
64
Mediator
Steal
Mana Shield
Sicken
Teleport

Mythril Gun

Flash Hat
Mystic Vest
Feather Boots

Praise, Threaten, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Gil Taking, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



Baron Von Scrub
Male
Cancer
57
67
Squire
Item
Counter
Defend
Levitate

Flail
Mythril Shield
Circlet
Power Sleeve
Red Shoes

Accumulate, Dash, Throw Stone, Yell, Wish
Potion, Hi-Potion, X-Potion, Soft, Phoenix Down



Maakur
Female
Scorpio
76
63
Mime

Counter
Magic Defense UP
Ignore Terrain


Round Shield
Golden Hairpin
Judo Outfit
Defense Armlet

Mimic




HeroponThrawn
Male
Sagittarius
46
44
Lancer
Elemental
HP Restore
Equip Axe
Levitate

Battle Axe
Platinum Shield
Genji Helmet
Chain Mail
Reflect Ring

Level Jump8, Vertical Jump6
Pitfall, Static Shock, Will-O-Wisp, Sand Storm

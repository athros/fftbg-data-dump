Final Bets: red - 17 bets for 11,655G (41.7%, x1.40); champion - 14 bets for 16,279G (58.3%, x0.72)

red bets:
SkylerBunny: 6,002G (51.5%, 6,002G)
BirbBrainsBot: 1,000G (8.6%, 113,899G)
Sairentozon7: 739G (6.3%, 739G)
lowlf: 735G (6.3%, 735G)
Runeseeker22: 650G (5.6%, 3,350G)
killth3kid: 548G (4.7%, 1,550G)
J2Reed: 340G (2.9%, 340G)
KasugaiRoastedPeas: 300G (2.6%, 10,911G)
helpimabug: 228G (2.0%, 228G)
regios91: 212G (1.8%, 2,846G)
datadrivenbot: 200G (1.7%, 59,508G)
krombobreaker: 200G (1.7%, 5,731G)
gorgewall: 101G (0.9%, 1,473G)
AllInBot: 100G (0.9%, 100G)
Moshyhero: 100G (0.9%, 4,378G)
FFMaster: 100G (0.9%, 1,628G)
jquantho: 100G (0.9%, 133G)

champion bets:
reinoe: 5,000G (30.7%, 62,344G)
Leonidusx: 4,826G (29.6%, 4,826G)
thunderducker: 1,500G (9.2%, 3,753G)
Vahn_Blade: 1,000G (6.1%, 13,642G)
lijarkh: 842G (5.2%, 842G)
getthemoneyz: 626G (3.8%, 1,770,422G)
latebit: 508G (3.1%, 508G)
3ngag3: 500G (3.1%, 14,500G)
Chuckolator: 445G (2.7%, 12,201G)
Raixelol: 328G (2.0%, 1,310G)
baconbacon1207: 300G (1.8%, 8,064G)
minorfffanatic1: 204G (1.3%, 204G)
Twisted_Nutsatchel: 100G (0.6%, 23,639G)
Firesheath: 100G (0.6%, 1,285G)

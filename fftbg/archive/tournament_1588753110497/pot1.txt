Final Bets: red - 7 bets for 4,801G (49.9%, x1.00); blue - 11 bets for 4,821G (50.1%, x1.00)

red bets:
pandasforsale: 2,222G (46.3%, 25,389G)
KasugaiRoastedPeas: 1,000G (20.8%, 3,149G)
Estan_AD: 500G (10.4%, 4,409G)
ko2q: 481G (10.0%, 481G)
midori_ribbon: 276G (5.7%, 276G)
ungabunga_bot: 222G (4.6%, 360,686G)
datadrivenbot: 100G (2.1%, 12,433G)

blue bets:
Dexsana: 1,000G (20.7%, 8,204G)
BirbBrainsBot: 672G (13.9%, 67,671G)
sinnyil2: 600G (12.4%, 15,425G)
R_Raynos: 511G (10.6%, 511G)
xxnamexx: 464G (9.6%, 464G)
getthemoneyz: 446G (9.3%, 630,469G)
superdevon1: 434G (9.0%, 4,343G)
Eyepoor_: 250G (5.2%, 4,533G)
crimson_hellkite: 160G (3.3%, 11,060G)
Sinikt: 148G (3.1%, 148G)
Practice_Pad: 136G (2.8%, 136G)

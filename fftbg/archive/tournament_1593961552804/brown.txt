Player: !Brown
Team: Brown Team
Palettes: Brown/Green



RampagingRobot
Female
Taurus
70
53
Lancer
Elemental
Counter
Equip Sword
Move+2

Bizen Boat
Gold Shield
Genji Helmet
Chameleon Robe
Magic Gauntlet

Level Jump5, Vertical Jump2
Pitfall, Hell Ivy, Local Quake, Quicksand, Gusty Wind, Lava Ball



ArashiKurobara
Female
Libra
58
56
Samurai
Dance
Parry
Short Status
Move-HP Up

Bizen Boat

Iron Helmet
Silk Robe
Bracer

Koutetsu, Bizen Boat, Kiyomori
Witch Hunt, Slow Dance, Nameless Dance, Last Dance



Enkikavlar
Male
Sagittarius
80
51
Time Mage
Yin Yang Magic
Distribute
Maintenance
Ignore Terrain

Healing Staff

Thief Hat
Wizard Robe
Dracula Mantle

Haste, Slow, Slow 2, Immobilize, Quick, Meteor
Poison, Life Drain, Silence Song, Blind Rage, Paralyze, Petrify



PoroTact
Male
Gemini
44
43
Ninja
Summon Magic
Counter
Concentrate
Teleport

Flame Whip
Flail
Feather Hat
Clothes
Defense Armlet

Bomb, Staff, Spear, Stick
Moogle, Titan, Carbunkle, Silf, Lich, Cyclops

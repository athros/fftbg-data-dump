Player: !Red
Team: Red Team
Palettes: Red/Brown



Actual JP
Monster
Libra
57
72
Bomb










Thyrandaal
Female
Libra
66
61
Dancer
Talk Skill
Parry
Monster Talk
Move+3

Persia

Golden Hairpin
Mystic Vest
Angel Ring

Witch Hunt, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon
Persuade, Insult, Negotiate, Mimic Daravon, Refute



ACSpree
Female
Pisces
59
65
Thief
Jump
HP Restore
Equip Sword
Move+1

Asura Knife

Leather Hat
Power Sleeve
Magic Ring

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim
Level Jump2, Vertical Jump7



Mpghappiness
Male
Cancer
62
48
Ninja
Battle Skill
Brave Save
Defense UP
Move-HP Up

Flame Whip
Flail
Feather Hat
Adaman Vest
Power Wrist

Shuriken, Staff
Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword

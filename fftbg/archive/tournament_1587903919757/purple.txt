Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesauceBoss
Male
Virgo
59
71
Archer
Talk Skill
Hamedo
Doublehand
Ignore Height

Poison Bow

Flash Hat
Power Sleeve
Leather Mantle

Charge+2, Charge+5
Praise, Solution, Refute



Twelfthrootoftwo
Female
Sagittarius
72
79
Knight
Charge
Sunken State
Long Status
Teleport 2

Long Sword
Kaiser Plate
Mythril Helmet
White Robe
Feather Boots

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Dark Sword
Charge+1, Charge+4, Charge+5, Charge+10, Charge+20



Repellentboot6
Monster
Virgo
77
76
Great Malboro










Zachara
Male
Capricorn
64
59
Summoner
Black Magic
MP Restore
Equip Knife
Jump+3

Ninja Edge

Leather Hat
Wizard Outfit
Dracula Mantle

Moogle, Ramuh, Golem, Carbunkle, Silf, Cyclops
Fire 3, Bolt 3, Bolt 4, Ice 3, Empower

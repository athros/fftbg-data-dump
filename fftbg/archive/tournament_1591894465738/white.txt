Player: !White
Team: White Team
Palettes: White/Blue



ALY327
Female
Capricorn
51
44
Lancer
Battle Skill
Counter Magic
Secret Hunt
Waterbreathing

Mythril Spear
Hero Shield
Genji Helmet
Reflect Mail
Elf Mantle

Level Jump8, Vertical Jump2
Armor Break, Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword



Superdevon1
Female
Sagittarius
68
69
Dancer
Throw
Damage Split
Equip Polearm
Move+3

Ryozan Silk

Feather Hat
Judo Outfit
Magic Ring

Slow Dance, Polka Polka, Disillusion, Nether Demon
Shuriken, Bomb, Axe



Fenaen
Female
Virgo
64
62
Oracle
Elemental
Earplug
Concentrate
Move+1

Papyrus Codex

Golden Hairpin
Power Sleeve
Bracer

Spell Absorb, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Frozentomato
Male
Gemini
47
53
Lancer
White Magic
Caution
Attack UP
Ignore Terrain

Octagon Rod
Venetian Shield
Cross Helmet
Diamond Armor
108 Gems

Level Jump8, Vertical Jump8
Cure 4, Raise 2, Regen, Protect, Shell, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



MilesDong
Monster
Capricorn
52
67
Trent










Treafa
Female
Aquarius
44
53
Geomancer
White Magic
Meatbone Slash
Equip Gun
Waterwalking

Battle Folio
Buckler
Cachusha
Mythril Vest
Red Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 2, Cure 4, Raise, Reraise, Protect, Protect 2



Reddwind
Male
Leo
69
74
Knight
Draw Out
Distribute
Sicken
Ignore Terrain

Sleep Sword
Gold Shield
Circlet
Crystal Mail
Defense Armlet

Magic Break, Explosion Sword
Asura, Koutetsu, Kiyomori



Lydian C
Female
Aries
63
50
Calculator
Forest Skill
Sunken State
Beastmaster
Move-MP Up

Papyrus Codex
Round Shield
Flash Hat
Linen Robe
Feather Boots

Blue Magic
Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak

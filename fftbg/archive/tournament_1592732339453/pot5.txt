Final Bets: blue - 11 bets for 5,873G (46.8%, x1.14); green - 6 bets for 6,674G (53.2%, x0.88)

blue bets:
Digitalsocrates: 1,000G (17.0%, 21,286G)
BirbBrainsBot: 1,000G (17.0%, 56,683G)
BlackFireUK: 1,000G (17.0%, 31,124G)
Evewho: 504G (8.6%, 504G)
prince_rogers_nelson_: 500G (8.5%, 13,280G)
serperemagus: 480G (8.2%, 480G)
Spuzzmocker: 423G (7.2%, 423G)
ColetteMSLP: 300G (5.1%, 995G)
twelfthrootoftwo: 300G (5.1%, 6,817G)
getthemoneyz: 266G (4.5%, 1,017,559G)
datadrivenbot: 100G (1.7%, 44,866G)

green bets:
Lydian_C: 4,200G (62.9%, 203,152G)
VolgraTheMoose: 1,001G (15.0%, 12,956G)
roqqqpsi: 655G (9.8%, 5,963G)
gorgewall: 401G (6.0%, 17,566G)
pancake11112: 217G (3.3%, 217G)
Ring_Wyrm: 200G (3.0%, 1,719G)

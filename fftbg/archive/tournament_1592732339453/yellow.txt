Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



StealthModeLocke
Monster
Capricorn
42
69
Dragon










Evewho
Female
Libra
45
78
Knight
Yin Yang Magic
Damage Split
Maintenance
Move-HP Up

Sleep Sword
Aegis Shield
Bronze Helmet
Wizard Robe
Feather Mantle

Armor Break, Weapon Break, Magic Break, Speed Break, Power Break
Poison, Life Drain, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Dark Holy



Serperemagus
Male
Taurus
54
65
Archer
Punch Art
Mana Shield
Short Charge
Levitate

Cross Bow
Flame Shield
Green Beret
Chain Vest
Elf Mantle

Charge+10
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification



CassiePhoenix
Female
Taurus
80
75
Monk
Black Magic
Dragon Spirit
Dual Wield
Lava Walking



Red Hood
Mystic Vest
Wizard Mantle

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Bolt 2, Ice 4, Empower

Player: !White
Team: White Team
Palettes: White/Blue



Ranmilia
Male
Cancer
55
50
Archer
Draw Out
Faith Up
Dual Wield
Teleport 2

Windslash Bow

Headgear
Judo Outfit
Magic Gauntlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+10
Asura



SeriousFaceFace
Male
Gemini
76
71
Samurai
Time Magic
Catch
Defend
Teleport

Muramasa

Gold Helmet
Plate Mail
Sprint Shoes

Asura, Kiyomori, Masamune
Haste, Slow, Demi, Demi 2, Stabilize Time



SchwerMuta
Female
Virgo
41
63
Priest
Draw Out
Absorb Used MP
Dual Wield
Lava Walking

Oak Staff
Sage Staff
Flash Hat
Rubber Costume
Diamond Armlet

Cure 2, Cure 3, Raise 2, Shell, Esuna
Koutetsu, Heaven's Cloud



Alrightbye
Male
Taurus
72
51
Archer
Sing
Parry
Equip Sword
Levitate

Ancient Sword
Aegis Shield
Triangle Hat
Power Sleeve
Dracula Mantle

Charge+1, Charge+3, Charge+5
Battle Song

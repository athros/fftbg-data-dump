Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



PleXmito
Female
Scorpio
57
62
Thief
Item
Counter Tackle
Concentrate
Waterwalking

Ninja Edge

Triangle Hat
Black Costume
Magic Ring

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Potion, X-Potion, Eye Drop, Remedy, Phoenix Down



Ominnous
Male
Sagittarius
51
47
Oracle
Elemental
Hamedo
Equip Gun
Teleport

Bloody Strings

Green Beret
Wizard Robe
Sprint Shoes

Poison, Blind Rage
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Lava Ball



HRak050
Male
Gemini
77
73
Archer
Time Magic
Parry
Maintenance
Jump+1

Ice Bow

Triangle Hat
Black Costume
Germinas Boots

Charge+1, Charge+2, Charge+4, Charge+7
Stop, Immobilize, Float, Quick, Demi 2, Stabilize Time, Galaxy Stop



Silentperogy
Male
Sagittarius
54
40
Lancer
Basic Skill
MP Restore
Equip Axe
Move+2

Scorpion Tail
Buckler
Cross Helmet
Genji Armor
Power Wrist

Level Jump8, Vertical Jump7
Heal, Yell

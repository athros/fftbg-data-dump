Player: !Red
Team: Red Team
Palettes: Red/Brown



L2 Sentinel
Male
Capricorn
51
47
Lancer
Elemental
Arrow Guard
Equip Bow
Move+1

Poison Bow
Crystal Shield
Genji Helmet
Platinum Armor
Reflect Ring

Level Jump8, Vertical Jump7
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball



Holyonline
Female
Aquarius
73
43
Knight
Item
Sunken State
Doublehand
Ignore Terrain

Ice Brand

Crystal Helmet
Gold Armor
Jade Armlet

Head Break, Shield Break, Weapon Break, Speed Break, Power Break, Surging Sword
Hi-Ether, Eye Drop, Remedy



Phi Sig
Male
Taurus
73
67
Chemist
Basic Skill
Dragon Spirit
Short Charge
Jump+2

Panther Bag

Holy Miter
Adaman Vest
Elf Mantle

Ether, Echo Grass, Soft, Remedy, Phoenix Down
Accumulate, Dash, Yell, Wish



Boojob
Male
Aquarius
43
38
Ninja
Basic Skill
PA Save
Attack UP
Levitate

Sasuke Knife
Sasuke Knife
Ribbon
Earth Clothes
Spike Shoes

Shuriken, Axe
Heal, Tickle, Yell, Fury, Wish

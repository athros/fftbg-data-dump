Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SQUiDSQUARKLIN
Male
Virgo
72
78
Ninja
Sing
MA Save
Equip Sword
Move+3

Muramasa
Muramasa
Green Beret
Chain Vest
Diamond Armlet

Shuriken, Ninja Sword
Angel Song, Life Song, Battle Song, Magic Song, Sky Demon



Narcius
Male
Libra
75
65
Squire
Yin Yang Magic
Critical Quick
Short Status
Move+3

Diamond Sword
Escutcheon
Thief Hat
Judo Outfit
Battle Boots

Accumulate, Heal, Yell
Spell Absorb, Life Drain, Pray Faith, Foxbird, Dispel Magic, Paralyze, Dark Holy



ALY327
Female
Sagittarius
68
60
Calculator
Time Magic
Counter
Short Charge
Move-HP Up

Papyrus Codex

Triangle Hat
Leather Outfit
Feather Boots

Height, 4, 3
Immobilize, Float, Stabilize Time, Meteor



Gelwain
Female
Libra
66
57
Oracle
White Magic
Absorb Used MP
Attack UP
Move+1

Cypress Rod

Triangle Hat
Silk Robe
Magic Gauntlet

Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep, Dark Holy
Raise, Reraise, Protect 2, Esuna

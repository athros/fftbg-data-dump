Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TheMurkGnome
Female
Cancer
56
39
Mime

Counter
Maintenance
Move+2



Feather Hat
Black Costume
Setiemson

Mimic




Randgridr
Female
Leo
78
57
Summoner
Time Magic
Catch
Beastmaster
Retreat

Bestiary

Triangle Hat
Judo Outfit
Rubber Shoes

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Odin, Salamander, Lich
Haste 2, Stop, Immobilize, Stabilize Time, Galaxy Stop



Mysteriousdewd
Female
Sagittarius
45
43
Monk
Jump
Counter Tackle
Magic Defense UP
Levitate



Red Hood
Adaman Vest
Magic Gauntlet

Secret Fist, Purification
Level Jump2, Vertical Jump2



E Ballard
Male
Aquarius
48
68
Knight
White Magic
Parry
Concentrate
Retreat

Ragnarok
Escutcheon
Diamond Helmet
Carabini Mail
Small Mantle

Speed Break, Power Break, Mind Break
Cure 3, Raise, Raise 2, Regen, Protect 2, Wall, Esuna, Holy

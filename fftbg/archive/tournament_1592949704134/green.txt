Player: !Green
Team: Green Team
Palettes: Green/White



UmaiJam
Female
Libra
53
46
Wizard
Yin Yang Magic
Absorb Used MP
Secret Hunt
Fly

Faith Rod

Flash Hat
Wizard Robe
Diamond Armlet

Fire 4, Bolt, Bolt 2, Empower
Blind, Dispel Magic, Sleep



HaychDub
Female
Pisces
69
71
Oracle
Black Magic
Meatbone Slash
Equip Shield
Move-HP Up

Iron Fan
Aegis Shield
Twist Headband
Black Costume
Rubber Shoes

Poison, Life Drain, Doubt Faith, Dispel Magic, Paralyze
Fire, Flare



Mesmaster
Male
Scorpio
67
67
Thief
Jump
Arrow Guard
Equip Shield
Move+1

Ninja Edge
Aegis Shield
Leather Hat
Earth Clothes
Red Shoes

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Level Jump5, Vertical Jump8



Butterbelljedi
Male
Libra
54
43
Priest
Time Magic
Damage Split
Halve MP
Jump+3

Gold Staff

Headgear
Silk Robe
Bracer

Cure, Cure 2, Cure 3, Raise, Protect, Protect 2, Wall, Esuna
Slow, Slow 2, Immobilize, Reflect, Demi 2, Stabilize Time

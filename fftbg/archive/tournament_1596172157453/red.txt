Player: !Red
Team: Red Team
Palettes: Red/Brown



Roofiepops
Female
Pisces
75
79
Summoner
Black Magic
MA Save
Short Charge
Move-HP Up

Papyrus Codex

Red Hood
Light Robe
Setiemson

Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Silf, Fairy
Fire, Fire 2, Ice, Death



HaateXIII
Male
Taurus
48
80
Priest
Sing
Counter Flood
Equip Polearm
Levitate

Spear

Red Hood
Wizard Robe
Defense Ring

Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Protect 2, Wall
Cheer Song, Sky Demon



Sairentozon7
Male
Aquarius
60
75
Thief
Item
Dragon Spirit
Throw Item
Jump+1

Mage Masher

Barette
Wizard Outfit
Sprint Shoes

Steal Heart, Steal Helmet, Steal Shield, Steal Accessory
Potion, Hi-Ether, Antidote, Phoenix Down



ThreeMileIsland
Female
Cancer
40
79
Knight
Jump
Counter Tackle
Long Status
Move+3

Rune Blade
Flame Shield
Barbuta
Chameleon Robe
Defense Armlet

Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword
Level Jump5, Vertical Jump4

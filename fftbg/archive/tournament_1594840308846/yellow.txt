Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Hasterious
Male
Aries
62
41
Lancer
White Magic
Hamedo
Defend
Levitate

Holy Lance
Flame Shield
Crystal Helmet
Reflect Mail
Germinas Boots

Level Jump2, Vertical Jump7
Cure 2, Raise, Raise 2, Reraise, Protect 2, Shell, Wall, Esuna



TasisSai
Female
Taurus
63
47
Lancer
Throw
Faith Save
Maintenance
Ignore Height

Cypress Rod
Round Shield
Gold Helmet
White Robe
Bracer

Level Jump4, Vertical Jump4
Bomb, Spear



Vorap
Male
Taurus
52
47
Summoner
Basic Skill
Critical Quick
Long Status
Lava Walking

Battle Folio

Flash Hat
Wizard Robe
Power Wrist

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Silf, Lich
Accumulate, Throw Stone, Heal, Tickle, Cheer Up, Wish, Scream



Snowfats
Female
Capricorn
65
72
Mime

Brave Save
Equip Shield
Jump+3


Aegis Shield
Green Beret
Clothes
Small Mantle

Mimic


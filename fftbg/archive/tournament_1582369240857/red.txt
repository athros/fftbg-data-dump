Player: !Red
Team: Red Team
Palettes: Red/Brown



Ar Tactic
Monster
Libra
70
38
Blue Dragon










Mightygerm1
Female
Aries
79
60
Summoner
Charge
Arrow Guard
Equip Gun
Ignore Terrain

Blaze Gun

Feather Hat
Brigandine
Diamond Armlet

Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Leviathan, Lich
Charge+1, Charge+2, Charge+4, Charge+5



Hyvi
Female
Virgo
53
83
Geomancer
Basic Skill
Faith Up
Dual Wield
Jump+2

Sleep Sword
Blood Sword
Black Hood
Wizard Robe
Spike Shoes

Water Ball, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Dash, Heal, Tickle, Yell



Readdesert
Male
Leo
75
37
Oracle
Time Magic
Damage Split
Magic Attack UP
Lava Walking

Octagon Rod

Feather Hat
Light Robe
Genji Gauntlet

Blind, Doubt Faith, Dispel Magic, Petrify, Dark Holy
Haste, Haste 2, Reflect, Quick, Stabilize Time

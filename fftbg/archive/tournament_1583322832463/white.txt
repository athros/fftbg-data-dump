Player: !White
Team: White Team
Palettes: White/Blue



Black Sheep 0213
Male
Gemini
52
57
Lancer
Punch Art
Counter
Equip Armor
Waterbreathing

Gungnir
Kaiser Plate
Headgear
White Robe
Dracula Mantle

Level Jump2, Vertical Jump2
Spin Fist, Earth Slash



Lyronmkii
Female
Aries
50
43
Ninja
Talk Skill
HP Restore
Equip Polearm
Move+3

Javelin
Musk Rod
Feather Hat
Brigandine
Feather Boots

Bomb, Stick
Invitation, Persuade, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Ar Tactic
Male
Sagittarius
78
74
Thief
White Magic
Auto Potion
Equip Sword
Fly

Save the Queen

Black Hood
Judo Outfit
Magic Ring

Steal Helmet, Steal Weapon, Leg Aim
Raise, Raise 2, Regen, Protect 2, Shell, Wall, Esuna



Powergems
Female
Gemini
62
62
Ninja
Yin Yang Magic
Earplug
Sicken
Swim

Main Gauche
Iga Knife
Headgear
Secret Clothes
Bracer

Shuriken, Bomb, Knife, Axe
Blind, Poison, Doubt Faith, Silence Song, Foxbird, Confusion Song

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Treapvort
Monster
Cancer
67
53
Ghoul










AngelOnizuka
Male
Virgo
62
46
Knight
Punch Art
Counter Magic
Sicken
Waterwalking

Ragnarok

Mythril Helmet
Leather Armor
Magic Ring

Weapon Break, Justice Sword, Surging Sword
Spin Fist, Secret Fist, Seal Evil



Bad1dea
Female
Virgo
42
76
Dancer
Steal
Faith Up
Secret Hunt
Waterwalking

Cashmere

Leather Hat
Secret Clothes
Spike Shoes

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance
Steal Heart, Steal Armor, Steal Shield, Arm Aim



DeathTaxesAndAnime
Female
Serpentarius
67
81
Summoner
Steal
Distribute
Short Charge
Move-MP Up

Oak Staff

Flash Hat
White Robe
Bracer

Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Fairy
Steal Heart, Steal Status, Leg Aim

Player: !White
Team: White Team
Palettes: White/Blue



Kamikaze Plague
Female
Cancer
79
57
Calculator
Time Magic
Distribute
Dual Wield
Teleport

Cypress Rod
Ivory Rod
Feather Hat
Robe of Lords
Diamond Armlet

CT, 5, 4, 3
Haste, Haste 2, Stop, Demi 2, Stabilize Time



Boojob
Male
Sagittarius
65
53
Lancer
Basic Skill
Parry
Concentrate
Move-MP Up

Mythril Spear
Escutcheon
Genji Helmet
Reflect Mail
N-Kai Armlet

Level Jump4, Vertical Jump6
Heal, Tickle, Wish



Peebs
Female
Leo
70
68
Priest
Throw
MP Restore
Equip Armor
Move+3

Flame Whip

Feather Hat
Linen Robe
Bracer

Cure 4, Raise, Reraise, Protect 2, Shell, Shell 2, Wall
Bomb, Knife, Spear



Lwtest
Female
Pisces
55
68
Monk
Black Magic
Mana Shield
Long Status
Move+1



Headgear
Wizard Outfit
Magic Ring

Purification, Chakra, Revive
Fire, Fire 4, Bolt 4, Ice 3, Frog, Death

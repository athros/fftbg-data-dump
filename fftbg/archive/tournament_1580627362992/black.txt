Player: !Black
Team: Black Team
Palettes: Black/Red



Readdesert
Male
Virgo
41
51
Geomancer
Draw Out
Counter
Beastmaster
Move+1

Platinum Sword
Round Shield
Golden Hairpin
Chameleon Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Eschwen
Male
Gemini
75
60
Oracle
Battle Skill
Earplug
Sicken
Retreat

Whale Whisker

Golden Hairpin
Power Sleeve
Jade Armlet

Poison, Spell Absorb, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Paralyze, Petrify
Head Break, Shield Break, Power Break



Aneyus
Male
Taurus
51
47
Ninja
Basic Skill
MP Restore
Equip Bow
Jump+3

Ice Bow

Twist Headband
Leather Vest
108 Gems

Shuriken, Bomb, Knife, Hammer, Staff, Spear, Dictionary
Accumulate, Dash, Heal, Yell



Grininda
Male
Taurus
58
49
Ninja
Item
Hamedo
Throw Item
Levitate

Flail
Kunai
Feather Hat
Leather Outfit
Dracula Mantle

Shuriken, Bomb
Potion, Ether, Elixir, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

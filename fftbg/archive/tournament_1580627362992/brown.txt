Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Powermhero
Male
Taurus
64
55
Chemist
Elemental
Speed Save
Equip Bow
Jump+2

Long Bow

Feather Hat
Brigandine
Defense Ring

Potion, Hi-Potion, Antidote, Remedy, Phoenix Down
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



CCRUNNER149UWP
Male
Gemini
36
57
Priest
Item
Regenerator
Concentrate
Retreat

Morning Star

Triangle Hat
Silk Robe
Feather Mantle

Cure 2, Cure 4, Raise, Protect 2, Shell, Wall, Esuna
Hi-Potion, Antidote, Holy Water, Phoenix Down



Thunderducker
Male
Scorpio
44
73
Geomancer
Draw Out
MA Save
Attack UP
Lava Walking

Giant Axe
Round Shield
Thief Hat
Mythril Vest
Bracer

Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Kiyomori



AuroraCL
Female
Leo
48
40
Chemist
Basic Skill
Earplug
Equip Gun
Jump+3

Blaze Gun

Golden Hairpin
Wizard Outfit
Power Wrist

Potion, Ether, Hi-Ether, Elixir, Antidote, Phoenix Down
Accumulate, Dash, Throw Stone, Heal

Player: !Red
Team: Red Team
Palettes: Red/Brown



EnemyController
Female
Cancer
69
50
Archer
Dance
Abandon
Martial Arts
Ignore Terrain


Buckler
Ribbon
Clothes
Feather Mantle

Charge+3, Charge+7
Slow Dance, Nameless Dance, Void Storage, Nether Demon



MetalZebra
Male
Libra
72
81
Knight
Yin Yang Magic
Caution
Concentrate
Move-MP Up

Battle Axe
Diamond Shield
Grand Helmet
Black Robe
Jade Armlet

Head Break, Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword
Spell Absorb, Silence Song, Confusion Song, Dispel Magic, Petrify, Dark Holy



MuadDib Usul
Female
Capricorn
43
41
Monk
Elemental
Counter Magic
Short Status
Move-MP Up



Holy Miter
Leather Vest
Rubber Shoes

Spin Fist, Secret Fist, Purification, Revive
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Artifact911
Male
Serpentarius
74
78
Squire
Summon Magic
Abandon
Secret Hunt
Waterwalking

Mythril Sword

Platinum Helmet
Chain Mail
Bracer

Heal, Tickle
Golem, Odin, Leviathan, Silf, Cyclops

Player: !White
Team: White Team
Palettes: White/Blue



Gelwain
Male
Leo
78
69
Oracle
Charge
Parry
Secret Hunt
Ignore Height

Papyrus Codex

Headgear
Clothes
Reflect Ring

Doubt Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy
Charge+1, Charge+4, Charge+10, Charge+20



StryderSeven
Female
Gemini
67
47
Chemist
Steal
Brave Up
Defend
Move+2

Orichalcum

Triangle Hat
Leather Outfit
N-Kai Armlet

Potion, Hi-Potion, Elixir, Echo Grass, Phoenix Down
Leg Aim



Yoshima75
Female
Cancer
57
53
Geomancer
Time Magic
Counter Magic
Sicken
Move+1

Murasame
Gold Shield
Golden Hairpin
Mystic Vest
Spike Shoes

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Stop, Reflect, Quick, Demi, Demi 2, Stabilize Time



ZephyrTempest
Female
Taurus
60
72
Time Mage
Charge
Absorb Used MP
Beastmaster
Fly

Madlemgen

Red Hood
Silk Robe
Magic Ring

Haste 2, Slow 2, Demi 2
Charge+1, Charge+4, Charge+5, Charge+7

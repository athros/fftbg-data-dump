Final Bets: black - 9 bets for 23,755G (66.3%, x0.51); purple - 10 bets for 12,075G (33.7%, x1.97)

black bets:
rechaun: 19,334G (81.4%, 19,334G)
WalkerNash: 1,658G (7.0%, 33,168G)
BirbBrainsBot: 1,000G (4.2%, 85,743G)
megaman2202: 500G (2.1%, 70,000G)
getthemoneyz: 426G (1.8%, 1,015,661G)
benticore: 337G (1.4%, 6,753G)
JethroThrul: 200G (0.8%, 899G)
KasugaiRoastedPeas: 200G (0.8%, 9,683G)
nifboy: 100G (0.4%, 3,113G)

purple bets:
J2DaBibbles: 4,499G (37.3%, 8,822G)
skillomono: 3,188G (26.4%, 3,188G)
serperemagus: 2,175G (18.0%, 2,175G)
RaIshtar: 974G (8.1%, 974G)
E_Ballard: 568G (4.7%, 568G)
Nizaha: 201G (1.7%, 8,418G)
RunicMagus: 150G (1.2%, 28,885G)
Lydian_C: 120G (1.0%, 171,229G)
maakur_: 100G (0.8%, 74,100G)
datadrivenbot: 100G (0.8%, 45,921G)

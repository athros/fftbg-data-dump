Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Actual JP
Male
Taurus
71
68
Samurai
Jump
Faith Save
Magic Attack UP
Ignore Terrain

Muramasa

Crystal Helmet
Reflect Mail
Cursed Ring

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Level Jump8, Vertical Jump6



Choco Joe
Male
Cancer
77
51
Mediator
Elemental
Counter Tackle
Martial Arts
Ignore Terrain

Romanda Gun

Red Hood
Chameleon Robe
Magic Ring

Persuade, Death Sentence, Insult, Refute, Rehabilitate
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Reddwind
Male
Aquarius
47
49
Ninja
Battle Skill
Meatbone Slash
Equip Sword
Waterbreathing

Ragnarok
Iron Sword
Feather Hat
Mythril Vest
Feather Mantle

Shuriken, Dictionary
Armor Break



DudeMonkey77
Female
Taurus
69
41
Thief
Item
Regenerator
Equip Armor
Ignore Terrain

Mythril Sword

Iron Helmet
Leather Armor
Diamond Armlet

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon
Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

Final Bets: purple - 25 bets for 15,270G (48.4%, x1.07); brown - 18 bets for 16,282G (51.6%, x0.94)

purple bets:
Lordminsc: 1,518G (9.9%, 1,518G)
aStatue: 1,314G (8.6%, 1,314G)
Zeroroute: 1,314G (8.6%, 1,314G)
Baron_von_Scrub: 1,111G (7.3%, 82,181G)
DrAntiSocial: 1,000G (6.5%, 1,999G)
Billybones5150: 1,000G (6.5%, 26,868G)
Lionhermit: 1,000G (6.5%, 46,343G)
fenaen: 965G (6.3%, 965G)
DustBirdEX: 963G (6.3%, 963G)
getthemoneyz: 710G (4.6%, 612,153G)
Grimmace45: 647G (4.2%, 3,236G)
Aldrammech: 600G (3.9%, 600G)
volgrathemoose: 501G (3.3%, 18,446G)
itsdigitalbro: 464G (3.0%, 464G)
Shalloween: 400G (2.6%, 26,238G)
eudes89: 339G (2.2%, 339G)
hydroshade: 250G (1.6%, 996G)
Nova_riety: 244G (1.6%, 244G)
WrathfulRemy: 200G (1.3%, 1,491G)
museofdoom: 200G (1.3%, 41,995G)
ll_aja_ll: 180G (1.2%, 3,180G)
ko2q: 100G (0.7%, 1,661G)
sandball: 100G (0.7%, 1,649G)
datadrivenbot: 100G (0.7%, 10,743G)
Bioticism: 50G (0.3%, 3,542G)

brown bets:
DavenIII: 5,049G (31.0%, 5,049G)
Chrysaors: 4,074G (25.0%, 33,957G)
Pie108: 2,000G (12.3%, 186,151G)
BirbBrainsBot: 1,000G (6.1%, 136,988G)
JumbocactuarX27: 1,000G (6.1%, 18,131G)
ungabunga_bot: 521G (3.2%, 289,193G)
nifboy: 432G (2.7%, 432G)
TheRylander: 400G (2.5%, 1,504G)
coralreeferz: 350G (2.1%, 2,371G)
lobsterlobster: 240G (1.5%, 240G)
ACSpree: 232G (1.4%, 232G)
Clydethecamel: 228G (1.4%, 228G)
KasugaiRoastedPeas: 200G (1.2%, 3,940G)
MagicBottle: 188G (1.2%, 4,712G)
Jerboj: 100G (0.6%, 59,143G)
bairen0: 100G (0.6%, 100G)
babieapple: 100G (0.6%, 1,198G)
ZephyrTempest: 68G (0.4%, 456G)

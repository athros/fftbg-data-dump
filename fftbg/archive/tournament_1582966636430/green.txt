Player: !Green
Team: Green Team
Palettes: Green/White



YaBoy125
Female
Scorpio
77
63
Chemist
Punch Art
Counter Flood
Short Charge
Move-MP Up

Star Bag

Twist Headband
Wizard Outfit
Diamond Armlet

Potion, X-Potion, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Pummel, Wave Fist, Purification, Revive, Seal Evil



KitchTowel
Female
Sagittarius
46
60
Chemist
Basic Skill
Counter Flood
Doublehand
Move-MP Up

Star Bag

Twist Headband
Clothes
Power Wrist

Potion, X-Potion, Ether, Eye Drop, Holy Water, Remedy, Phoenix Down
Throw Stone, Tickle, Cheer Up, Wish, Scream



Linkn
Male
Leo
75
73
Chemist
Steal
Mana Shield
Maintenance
Teleport

Panther Bag

Holy Miter
Leather Outfit
Chantage

Potion, X-Potion, Remedy, Phoenix Down
Steal Weapon, Steal Status, Leg Aim



RjA0zcOQ96
Female
Aries
52
58
Thief
Yin Yang Magic
Abandon
Short Charge
Jump+2

Coral Sword

Black Hood
Wizard Outfit
Diamond Armlet

Gil Taking, Steal Heart, Steal Shield, Steal Status, Leg Aim
Life Drain, Zombie, Foxbird, Dispel Magic

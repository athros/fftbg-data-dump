Player: !Red
Team: Red Team
Palettes: Red/Brown



LDSkinny
Female
Leo
77
53
Oracle
Time Magic
Arrow Guard
Martial Arts
Jump+3

Cypress Rod

Feather Hat
Leather Outfit
Vanish Mantle

Blind, Poison, Life Drain, Doubt Faith, Dispel Magic, Sleep, Dark Holy
Slow, Float, Demi, Stabilize Time



Roqqqpsi
Male
Gemini
61
61
Time Mage
Basic Skill
Caution
Beastmaster
Jump+1

Healing Staff

Flash Hat
Power Sleeve
Spike Shoes

Haste, Reflect
Wish



Coralreeferz
Male
Pisces
72
54
Monk
Draw Out
Mana Shield
Maintenance
Move-HP Up



Holy Miter
Rubber Costume
Magic Ring

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Muramasa, Kikuichimoji



Just Here2
Male
Cancer
55
57
Mediator
Jump
Damage Split
Magic Attack UP
Fly

Madlemgen

Headgear
Secret Clothes
Magic Gauntlet

Threaten, Preach, Refute, Rehabilitate
Level Jump8, Vertical Jump7

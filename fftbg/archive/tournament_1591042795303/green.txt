Player: !Green
Team: Green Team
Palettes: Green/White



TheApprentice251
Female
Taurus
57
63
Knight
Throw
Earplug
Magic Defense UP
Lava Walking

Mythril Sword
Crystal Shield
Genji Helmet
Silk Robe
Reflect Ring

Head Break, Speed Break
Shuriken, Ninja Sword



DudeMonkey77
Female
Leo
54
46
Geomancer
Basic Skill
Faith Save
Secret Hunt
Waterwalking

Slasher
Platinum Shield
Black Hood
Adaman Vest
Reflect Ring

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Throw Stone, Heal, Tickle, Yell, Fury



Powermhero
Male
Aries
73
54
Archer
Black Magic
Counter
Secret Hunt
Jump+3

Gastrafitis
Flame Shield
Golden Hairpin
Clothes
Elf Mantle

Charge+1, Charge+2, Charge+5
Fire 3, Fire 4, Bolt, Ice, Empower



OneHundredFists
Female
Aquarius
64
45
Oracle
Time Magic
Parry
Maintenance
Retreat

Gokuu Rod

Holy Miter
Black Robe
Power Wrist

Spell Absorb, Life Drain, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Petrify
Haste, Slow, Demi

Player: !Green
Team: Green Team
Palettes: Green/White



Sparker9
Male
Aquarius
65
80
Lancer
Item
Parry
Equip Bow
Teleport

Hunting Bow
Crystal Shield
Mythril Helmet
Silk Robe
Power Wrist

Level Jump5, Vertical Jump6
Potion, X-Potion, Eye Drop, Echo Grass, Phoenix Down



FriendSquirrel
Female
Leo
43
54
Priest
Charge
HP Restore
Equip Armor
Move-HP Up

Rainbow Staff

Mythril Helmet
Leather Armor
Red Shoes

Cure 4, Raise, Raise 2, Shell, Esuna, Holy
Charge+2, Charge+3, Charge+5, Charge+7



EnemyController
Male
Taurus
80
67
Samurai
Jump
Critical Quick
Short Charge
Move+2

Bizen Boat

Mythril Helmet
Gold Armor
Spike Shoes

Asura, Bizen Boat, Heaven's Cloud, Kiyomori
Level Jump4, Vertical Jump4



VolgraTheMoose
Male
Aries
75
61
Knight
Draw Out
MA Save
Halve MP
Fly

Giant Axe
Ice Shield
Diamond Helmet
Bronze Armor
Genji Gauntlet

Weapon Break, Mind Break
Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji

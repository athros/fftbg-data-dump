Final Bets: red - 11 bets for 7,102G (28.3%, x2.53); green - 21 bets for 17,959G (71.7%, x0.40)

red bets:
leakimiko: 2,542G (35.8%, 50,849G)
Evewho: 1,203G (16.9%, 2,360G)
theNGUYENNER: 1,000G (14.1%, 10,859G)
waterwatereverywhere: 601G (8.5%, 601G)
megamax667: 500G (7.0%, 8,500G)
Dymntd: 500G (7.0%, 20,461G)
CapnChaos12: 316G (4.4%, 316G)
getthemoneyz: 140G (2.0%, 660,260G)
Realitydown: 100G (1.4%, 3,392G)
BuffaloCrunch: 100G (1.4%, 1,999G)
OneHundredFists: 100G (1.4%, 4,510G)

green bets:
Estan_AD: 4,317G (24.0%, 8,634G)
Mesmaster: 4,000G (22.3%, 138,454G)
cupholderr: 2,000G (11.1%, 29,613G)
HaplessOne: 1,111G (6.2%, 4,503G)
CorpusCav: 860G (4.8%, 4,300G)
Lydian_C: 821G (4.6%, 821G)
Laserman1000: 621G (3.5%, 3,421G)
BirbBrainsBot: 610G (3.4%, 183,358G)
RughSontos: 562G (3.1%, 562G)
Jaritras: 413G (2.3%, 3,413G)
cougboi: 400G (2.2%, 9,534G)
ko2q: 372G (2.1%, 372G)
pstud: 322G (1.8%, 322G)
troublesometree: 301G (1.7%, 301G)
twelfthrootoftwo: 300G (1.7%, 957G)
ungabunga_bot: 241G (1.3%, 447,285G)
theanonymouslurker: 208G (1.2%, 208G)
TheJonTerp: 200G (1.1%, 3,908G)
SulliedGraves: 100G (0.6%, 3,299G)
datadrivenbot: 100G (0.6%, 12,219G)
Practice_Pad: 100G (0.6%, 1,195G)

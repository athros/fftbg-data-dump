Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lydian C
Male
Capricorn
75
47
Monk
Charge
MA Save
Beastmaster
Teleport



Holy Miter
Wizard Outfit
Power Wrist

Purification, Chakra, Revive, Seal Evil
Charge+1, Charge+3, Charge+4, Charge+5, Charge+7



CorpusCav
Female
Gemini
67
58
Samurai
Yin Yang Magic
Parry
Magic Attack UP
Levitate

Asura Knife

Gold Helmet
Crystal Mail
Sprint Shoes

Murasame, Muramasa
Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic



Firesheath
Female
Taurus
47
64
Thief
Item
MA Save
Magic Defense UP
Swim

Rune Blade

Flash Hat
Judo Outfit
Angel Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status
Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



CustomGameMaster
Male
Pisces
44
58
Knight
Black Magic
Caution
Doublehand
Move+2

Ice Brand

Crystal Helmet
Plate Mail
Vanish Mantle

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Justice Sword, Dark Sword, Night Sword, Surging Sword
Empower

Player: !Red
Team: Red Team
Palettes: Red/Brown



CT 5 Holy
Female
Libra
81
71
Mediator
Summon Magic
Parry
Short Charge
Jump+3

Battle Folio

Black Hood
Earth Clothes
Diamond Armlet

Invitation, Negotiate, Mimic Daravon, Refute
Moogle, Ramuh, Odin, Leviathan, Salamander



Cryptopsy70
Male
Aries
71
52
Samurai
Sing
PA Save
Attack UP
Move-HP Up

Holy Lance

Gold Helmet
Platinum Armor
Wizard Mantle

Asura, Koutetsu, Kiyomori
Magic Song, Diamond Blade, Hydra Pit



Avin Chaos
Female
Scorpio
57
68
Geomancer
Dance
Faith Up
Concentrate
Move-HP Up

Battle Axe
Kaiser Plate
Red Hood
Wizard Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Last Dance, Dragon Pit



Kronikle
Female
Leo
44
68
Calculator
Black Magic
MA Save
Equip Shield
Move-HP Up

Papyrus Codex
Round Shield
Flash Hat
Chain Vest
Diamond Armlet

CT, Height, Prime Number, 5
Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 4

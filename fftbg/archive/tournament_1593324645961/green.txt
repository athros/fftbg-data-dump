Player: !Green
Team: Green Team
Palettes: Green/White



DAC169
Male
Scorpio
55
69
Thief
White Magic
HP Restore
Equip Sword
Teleport

Mythril Sword

Leather Hat
Black Costume
Cursed Ring

Steal Armor
Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Esuna, Holy



Omegasuspekt
Female
Pisces
60
56
Geomancer
Black Magic
Blade Grasp
Equip Polearm
Move+1

Persia
Platinum Shield
Twist Headband
Black Costume
Bracer

Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Ice, Ice 2



Grininda
Female
Scorpio
45
40
Ninja
Yin Yang Magic
MP Restore
Equip Gun
Jump+2

Fairy Harp
Bloody Strings
Twist Headband
Wizard Outfit
108 Gems

Shuriken, Bomb, Staff, Stick, Dictionary
Blind, Life Drain, Zombie, Silence Song, Dispel Magic



HuffFlex
Female
Sagittarius
60
70
Geomancer
Basic Skill
Abandon
Equip Armor
Swim

Slasher
Round Shield
Platinum Helmet
White Robe
Leather Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Dash, Cheer Up, Fury, Scream

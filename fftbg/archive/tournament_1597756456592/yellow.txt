Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Powergems
Female
Aries
59
59
Summoner
Basic Skill
Hamedo
Equip Gun
Move+3

Mythril Gun

Ribbon
Wizard Robe
Jade Armlet

Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander
Accumulate, Heal, Tickle, Yell, Cheer Up, Wish



NIghtdew14
Female
Taurus
44
43
Dancer
Elemental
MA Save
Equip Axe
Move+2

Flame Whip

Feather Hat
Wizard Outfit
Magic Gauntlet

Witch Hunt, Polka Polka, Last Dance, Dragon Pit
Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Pplvee1
Female
Pisces
74
55
Priest
Elemental
Dragon Spirit
Equip Gun
Lava Walking

Battle Folio

Thief Hat
Silk Robe
N-Kai Armlet

Cure 2, Cure 3, Reraise, Esuna
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



StealthModeLocke
Female
Virgo
55
69
Wizard
Summon Magic
Damage Split
Equip Sword
Move+2

Koutetsu Knife

Green Beret
Leather Outfit
N-Kai Armlet

Fire, Fire 3, Bolt, Ice 3
Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander, Silf, Fairy

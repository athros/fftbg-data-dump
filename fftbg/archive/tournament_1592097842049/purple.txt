Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Grandlanzer
Male
Cancer
42
79
Monk
Yin Yang Magic
Caution
Defense UP
Ignore Height



Green Beret
Clothes
Cherche

Pummel, Earth Slash, Secret Fist, Purification, Revive
Spell Absorb, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Sleep



Reddwind
Male
Sagittarius
47
79
Knight
Basic Skill
Mana Shield
Equip Polearm
Move-HP Up

Partisan
Round Shield
Diamond Helmet
White Robe
Feather Mantle

Armor Break, Power Break
Accumulate, Dash, Throw Stone, Heal



Galkife
Male
Cancer
52
49
Priest
Draw Out
Abandon
Defense UP
Ignore Height

Healing Staff

Leather Hat
White Robe
Sprint Shoes

Cure, Cure 4, Raise, Protect, Shell, Esuna, Holy
Asura, Murasame, Masamune



Nifboy
Female
Aries
70
78
Samurai
Basic Skill
Faith Save
Doublehand
Waterwalking

Murasame

Bronze Helmet
Carabini Mail
Spike Shoes

Koutetsu, Kiyomori
Accumulate, Throw Stone, Heal, Tickle, Yell, Wish

Player: !Red
Team: Red Team
Palettes: Red/Brown



Trowaba
Male
Libra
79
62
Archer
Steal
Sunken State
Secret Hunt
Swim

Blast Gun
Flame Shield
Genji Helmet
Brigandine
Magic Gauntlet

Charge+1, Charge+4, Charge+5, Charge+7
Steal Weapon, Arm Aim, Leg Aim



Maeveen
Female
Leo
52
54
Summoner
Punch Art
Caution
Short Charge
Move-HP Up

Dragon Rod

Black Hood
Chameleon Robe
Bracer

Carbunkle, Salamander
Pummel



Vapetrail
Male
Aquarius
65
48
Priest
Talk Skill
Catch
Defense UP
Levitate

Wizard Staff

Cachusha
Earth Clothes
Magic Ring

Raise, Raise 2, Protect 2, Shell, Esuna
Invitation, Threaten, Insult, Negotiate, Refute



Gabbagooluigi
Male
Sagittarius
78
68
Ninja
Item
MP Restore
Equip Gun
Lava Walking

Papyrus Codex
Battle Folio
Holy Miter
Judo Outfit
Defense Ring

Shuriken, Bomb, Wand
Eye Drop, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



Volgrathemoose
Male
Cancer
48
43
Bard
Basic Skill
Counter
Equip Gun
Swim

Bestiary

Flash Hat
Black Costume
Bracer

Life Song, Cheer Song, Magic Song, Diamond Blade, Space Storage
Accumulate, Throw Stone, Heal, Yell, Wish



Cheezybuda
Male
Cancer
69
52
Archer
Talk Skill
Hamedo
Long Status
Jump+1

Mythril Bow

Leather Helmet
Earth Clothes
Reflect Ring

Charge+2, Charge+3, Charge+4, Charge+10
Invitation, Praise, Threaten, Negotiate



Ultraballer2000
Female
Cancer
59
52
Wizard
Elemental
Meatbone Slash
Equip Axe
Levitate

Gold Staff

Black Hood
Adaman Vest
Rubber Shoes

Bolt 2, Bolt 4
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Lava Ball



Bryan792
Male
Sagittarius
44
39
Ninja
Battle Skill
Brave Up
Attack UP
Waterbreathing

Spell Edge
Dagger
Flash Hat
Mythril Vest
108 Gems

Shuriken, Bomb
Power Break, Mind Break, Justice Sword, Night Sword

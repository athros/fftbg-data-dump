Player: !Red
Team: Red Team
Palettes: Red/Brown



Error72
Male
Sagittarius
76
44
Knight
Elemental
Brave Save
Magic Attack UP
Swim

Defender
Aegis Shield
Grand Helmet
Light Robe
Bracer

Weapon Break, Stasis Sword, Justice Sword
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Grininda
Male
Cancer
70
73
Lancer
Battle Skill
Auto Potion
Equip Axe
Move+3

Battle Axe
Flame Shield
Gold Helmet
Light Robe
N-Kai Armlet

Level Jump8, Vertical Jump7
Weapon Break



Ruebyy
Female
Capricorn
55
56
Mime

Caution
Equip Armor
Move+2



Circlet
White Robe
Defense Armlet

Mimic




Lyner87
Female
Taurus
74
49
Thief
Charge
Faith Save
Magic Attack UP
Teleport

Diamond Sword

Flash Hat
Brigandine
Feather Mantle

Gil Taking, Steal Helmet, Steal Shield, Arm Aim
Charge+1, Charge+5, Charge+7, Charge+20

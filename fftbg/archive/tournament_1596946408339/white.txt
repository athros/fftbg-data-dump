Player: !White
Team: White Team
Palettes: White/Blue



Rytor
Female
Aquarius
76
79
Knight
Draw Out
Parry
Beastmaster
Jump+2

Nagrarock
Bronze Shield
Crystal Helmet
Linen Cuirass
Magic Gauntlet

Armor Break, Weapon Break, Speed Break
Koutetsu, Bizen Boat



Zenlion
Monster
Taurus
49
45
Explosive










Mirapoix
Female
Virgo
47
63
Lancer
Punch Art
PA Save
Martial Arts
Jump+1

Partisan
Flame Shield
Crystal Helmet
Plate Mail
Feather Mantle

Level Jump2, Vertical Jump4
Spin Fist, Earth Slash, Revive



Randgridr
Female
Gemini
41
52
Mediator
Steal
Sunken State
Dual Wield
Lava Walking

Mythril Gun
Mythril Gun
Thief Hat
Linen Robe
Salty Rage

Refute
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim

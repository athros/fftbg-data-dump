Final Bets: red - 18 bets for 8,452G (51.7%, x0.94); yellow - 12 bets for 7,906G (48.3%, x1.07)

red bets:
HaateXIII: 1,700G (20.1%, 1,700G)
cam_ATS: 1,111G (13.1%, 45,003G)
RunicMagus: 909G (10.8%, 909G)
SkylerBunny: 800G (9.5%, 9,294G)
killth3kid: 680G (8.0%, 70,981G)
Lemonjohns: 500G (5.9%, 2,011G)
dantayystv: 500G (5.9%, 4,238G)
roofiepops: 448G (5.3%, 448G)
loveyouallfriends: 336G (4.0%, 336G)
dem0nj0ns: 216G (2.6%, 216G)
gorgewall: 201G (2.4%, 8,344G)
maddrave09: 200G (2.4%, 20,306G)
datadrivenbot: 200G (2.4%, 62,952G)
oooSancheZooo: 200G (2.4%, 2,774G)
TasisSai: 128G (1.5%, 252G)
Lydian_C: 123G (1.5%, 110,711G)
Treafa: 100G (1.2%, 100G)
ANFz: 100G (1.2%, 41,705G)

yellow bets:
Evewho: 2,278G (28.8%, 4,468G)
BirbBrainsBot: 1,000G (12.6%, 50,091G)
Forkmore: 1,000G (12.6%, 6,519G)
NicoSavoy: 1,000G (12.6%, 195,220G)
getthemoneyz: 504G (6.4%, 1,261,735G)
evontno: 501G (6.3%, 17,946G)
DustBirdEX: 400G (5.1%, 4,268G)
gooseyourself: 328G (4.1%, 328G)
ve11ion: 302G (3.8%, 302G)
Shalloween: 300G (3.8%, 59,568G)
RageImmortaI: 288G (3.6%, 1,827G)
moonliquor: 5G (0.1%, 1,327G)

Final Bets: purple - 5 bets for 3,322G (42.0%, x1.38); champion - 9 bets for 4,592G (58.0%, x0.72)

purple bets:
Mesmaster: 2,000G (60.2%, 88,381G)
BirbBrainsBot: 706G (21.3%, 9,838G)
ColetteMSLP: 400G (12.0%, 1,973G)
getthemoneyz: 158G (4.8%, 1,059,909G)
Hasterious: 58G (1.7%, 5,898G)

champion bets:
serperemagus: 1,533G (33.4%, 1,533G)
VolgraTheMoose: 1,145G (24.9%, 2,291G)
ShintaroNayaka: 764G (16.6%, 764G)
Spuzzmocker: 500G (10.9%, 4,425G)
Evewho: 200G (4.4%, 7,708G)
ar_tactic: 150G (3.3%, 55,561G)
nifboy: 100G (2.2%, 6,246G)
Ring_Wyrm: 100G (2.2%, 5,232G)
datadrivenbot: 100G (2.2%, 49,562G)

Player: !White
Team: White Team
Palettes: White/Blue



Ring Wyrm
Male
Libra
59
66
Time Mage
Draw Out
Hamedo
Magic Attack UP
Move-MP Up

White Staff

Thief Hat
Mystic Vest
Battle Boots

Haste, Slow, Slow 2, Stop, Immobilize, Demi, Demi 2, Stabilize Time, Meteor
Kiyomori



NovaKnight21
Female
Sagittarius
66
71
Geomancer
Talk Skill
Counter Tackle
Equip Armor
Ignore Height

Koutetsu Knife
Kaiser Plate
Green Beret
Diamond Armor
Magic Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Negotiate, Refute



BlackFireUK
Female
Aquarius
69
80
Time Mage
Basic Skill
PA Save
Short Charge
Fly

Rainbow Staff

Golden Hairpin
Earth Clothes
Sprint Shoes

Haste, Immobilize
Tickle, Yell, Fury



CassiePhoenix
Male
Gemini
45
77
Geomancer
Jump
Catch
Concentrate
Levitate

Kikuichimoji
Ice Shield
Golden Hairpin
Silk Robe
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Lava Ball
Level Jump8, Vertical Jump7

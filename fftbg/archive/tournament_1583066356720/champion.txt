Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



TheBlobZ
Male
Libra
68
45
Monk
Charge
HP Restore
Short Status
Lava Walking



Green Beret
Clothes
Dracula Mantle

Secret Fist, Purification
Charge+1, Charge+2, Charge+4



Smegma Sorcerer
Monster
Cancer
52
61
King Behemoth










Upvla
Male
Aquarius
77
78
Calculator
Black Magic
Earplug
Dual Wield
Move+1

Papyrus Codex
Bestiary
Golden Hairpin
Leather Outfit
Elf Mantle

CT, Height, Prime Number, 4, 3
Fire, Fire 3, Fire 4, Bolt, Bolt 4, Ice 2, Ice 4, Empower



Thaetreis
Female
Gemini
57
46
Lancer
Item
Distribute
Attack UP
Fly

Mythril Spear
Mythril Shield
Crystal Helmet
Light Robe
Small Mantle

Level Jump5, Vertical Jump7
Potion, Hi-Ether, Elixir, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down

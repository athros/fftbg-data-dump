Final Bets: green - 12 bets for 4,459G (49.2%, x1.03); yellow - 11 bets for 4,607G (50.8%, x0.97)

green bets:
MagicBottle: 1,000G (22.4%, 11,765G)
BirbBrainsBot: 1,000G (22.4%, 102,613G)
EnemyController: 500G (11.2%, 619,519G)
killth3kid: 314G (7.0%, 3,147G)
DuraiPapers: 300G (6.7%, 3,326G)
benticore: 300G (6.7%, 3,067G)
getthemoneyz: 244G (5.5%, 987,184G)
Nizaha: 201G (4.5%, 13,140G)
Kronikle: 200G (4.5%, 12,973G)
nok____: 200G (4.5%, 2,900G)
CosmicTactician: 100G (2.2%, 5,724G)
Bongomon7: 100G (2.2%, 9,364G)

yellow bets:
Sairentozon7: 1,018G (22.1%, 1,018G)
pplvee1: 768G (16.7%, 768G)
Nickyfive: 600G (13.0%, 600G)
TheChainNerd: 500G (10.9%, 6,500G)
Oreo_Pizza: 500G (10.9%, 7,682G)
CapnChaos12: 300G (6.5%, 18,839G)
alterworlds: 267G (5.8%, 267G)
ericzubat: 250G (5.4%, 2,087G)
darnitkevin: 204G (4.4%, 204G)
maakur_: 100G (2.2%, 24,371G)
E_Ballard: 100G (2.2%, 9,277G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DavenIII
Male
Aries
44
49
Thief
Talk Skill
Parry
Dual Wield
Move+3

Assassin Dagger
Zorlin Shape
Green Beret
Earth Clothes
Bracer

Steal Heart, Steal Accessory, Steal Status
Invitation, Solution, Negotiate, Rehabilitate



Run With Stone GUNs
Female
Leo
75
51
Mime

Meatbone Slash
Defend
Ignore Terrain



Ribbon
Clothes
Sprint Shoes

Mimic




HaychDub
Male
Capricorn
51
75
Knight
Item
Faith Save
Defense UP
Ignore Height

Save the Queen
Gold Shield
Diamond Helmet
Gold Armor
Wizard Mantle

Head Break, Armor Break, Shield Break, Justice Sword, Night Sword
Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



Kronikle
Female
Scorpio
65
44
Wizard
Yin Yang Magic
Absorb Used MP
Short Charge
Ignore Height

Thunder Rod

Flash Hat
White Robe
Dracula Mantle

Fire, Fire 4, Bolt 3, Ice 2, Ice 3, Empower
Spell Absorb, Pray Faith, Zombie, Dispel Magic, Sleep, Petrify, Dark Holy

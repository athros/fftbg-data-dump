Player: !Red
Team: Red Team
Palettes: Red/Brown



Pandasforsale
Female
Aquarius
58
57
Archer
Yin Yang Magic
Counter
Sicken
Fly

Ultimus Bow

Feather Hat
Brigandine
Battle Boots

Charge+2, Charge+5, Charge+7, Charge+20
Pray Faith, Blind Rage



RoachCarnival
Male
Leo
73
46
Chemist
Black Magic
HP Restore
Dual Wield
Teleport

Cultist Dagger
Mythril Knife
Red Hood
Leather Outfit
Elf Mantle

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down
Bolt 3, Ice 4, Flare



DarrenDinosaurs
Male
Taurus
43
54
Mime

Earplug
Secret Hunt
Levitate



Green Beret
Adaman Vest
Diamond Armlet

Mimic




Rocl
Female
Libra
80
46
Time Mage
Draw Out
Damage Split
Martial Arts
Waterbreathing



Twist Headband
Light Robe
108 Gems

Haste, Stop, Immobilize, Reflect, Stabilize Time
Koutetsu, Heaven's Cloud, Muramasa

Player: !White
Team: White Team
Palettes: White/Blue



Negativedivide
Monster
Sagittarius
55
52
Bomb










Zmanwise
Male
Scorpio
64
56
Archer
Jump
Caution
Equip Bow
Jump+3

Ultimus Bow

Black Hood
Chain Vest
Reflect Ring

Charge+4, Charge+5, Charge+10, Charge+20
Level Jump2, Vertical Jump2



Zachara
Male
Sagittarius
43
79
Mediator
Yin Yang Magic
Arrow Guard
Equip Knife
Move+2

Spell Edge

Thief Hat
Wizard Robe
Angel Ring

Persuade, Praise, Preach, Insult, Refute
Blind, Pray Faith, Dark Holy



Reinoe
Male
Scorpio
40
37
Knight
Steal
Meatbone Slash
Doublehand
Move-MP Up

Defender

Iron Helmet
Genji Armor
N-Kai Armlet

Armor Break, Magic Break, Mind Break
Gil Taking, Steal Heart, Steal Weapon, Steal Status, Arm Aim

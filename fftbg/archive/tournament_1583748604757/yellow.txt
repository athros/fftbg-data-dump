Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Upvla
Male
Pisces
59
72
Archer
Punch Art
Parry
Doublehand
Swim

Mythril Gun

Holy Miter
Chain Vest
Angel Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7, Charge+20
Spin Fist, Secret Fist, Purification, Revive, Seal Evil



Draaaan
Male
Leo
49
75
Wizard
Yin Yang Magic
Dragon Spirit
Defend
Move+1

Thunder Rod

Golden Hairpin
Adaman Vest
Feather Boots

Bolt 4, Ice 2, Ice 3, Ice 4, Frog, Flare
Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird



Maeveen
Female
Taurus
51
74
Thief
Throw
HP Restore
Equip Gun
Swim

Blaze Gun

Red Hood
Black Costume
Angel Ring

Steal Heart, Arm Aim, Leg Aim
Axe, Wand



Shs
Female
Taurus
40
58
Thief
Item
MP Restore
Equip Axe
Teleport

Giant Axe

Green Beret
Mythril Vest
Vanish Mantle

Steal Helmet, Steal Accessory, Steal Status
Potion, Hi-Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down

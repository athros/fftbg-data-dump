Player: !Red
Team: Red Team
Palettes: Red/Brown



Treapvort
Male
Cancer
71
68
Squire
Item
Counter Tackle
Equip Gun
Lava Walking

Papyrus Codex

Diamond Helmet
Chain Mail
Wizard Mantle

Dash, Heal, Tickle, Wish
Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Phoenix Down



Jeeboheebo
Female
Pisces
47
46
Summoner
Charge
Abandon
Short Charge
Waterwalking

Thunder Rod

Twist Headband
Chain Vest
Sprint Shoes

Moogle, Ifrit, Leviathan, Silf, Fairy, Lich
Charge+4



Nickelbank
Female
Sagittarius
77
67
Dancer
Item
PA Save
Secret Hunt
Fly

Ryozan Silk

Twist Headband
Power Sleeve
Magic Gauntlet

Witch Hunt, Wiznaibus, Slow Dance, Nameless Dance, Last Dance, Obsidian Blade, Nether Demon
Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Junkyard Dave
Male
Gemini
53
45
Chemist
Battle Skill
Arrow Guard
Equip Sword
Waterbreathing

Defender

Headgear
Mythril Vest
Wizard Mantle

Potion, X-Potion, Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Justice Sword

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



GimmickRPG
Female
Taurus
50
36
Mime

Counter Tackle
Martial Arts
Fly



Flash Hat
Adaman Vest
Genji Gauntlet

Mimic




SkylerBunny
Female
Aquarius
52
53
Mime

Speed Save
Magic Attack UP
Waterwalking



Twist Headband
Power Sleeve
Leather Mantle

Mimic




Volgrathemoose
Male
Aquarius
74
67
Archer
Talk Skill
HP Restore
Doublehand
Teleport

Long Bow

Black Hood
Clothes
Rubber Shoes

Charge+1, Charge+2, Charge+4, Charge+10
Solution, Insult, Mimic Daravon, Refute



Technominari
Female
Pisces
55
67
Priest
Draw Out
Meatbone Slash
Magic Defense UP
Move+2

Gold Staff

Flash Hat
Linen Robe
Cherche

Raise, Raise 2, Protect, Shell, Esuna
Asura, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji

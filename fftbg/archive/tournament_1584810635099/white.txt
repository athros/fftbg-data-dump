Player: !White
Team: White Team
Palettes: White/Blue



Caster Daily
Monster
Aries
70
78
Ghost










Galkife
Female
Serpentarius
67
55
Calculator
Yin Yang Magic
Meatbone Slash
Equip Polearm
Jump+1

Holy Lance

Headgear
White Robe
Angel Ring

Height, 5
Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dark Holy



Thefyeman
Male
Taurus
66
55
Lancer
Charge
Meatbone Slash
Short Charge
Move+1

Partisan
Flame Shield
Gold Helmet
Genji Armor
Dracula Mantle

Level Jump8, Vertical Jump4
Charge+1, Charge+7, Charge+10



Deesignal
Female
Taurus
52
70
Monk
Steal
Faith Up
Dual Wield
Move-MP Up



Flash Hat
Earth Clothes
Cursed Ring

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Gil Taking, Steal Heart, Arm Aim

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Legitimized
Female
Capricorn
55
57
Mediator
Dance
Brave Up
Dual Wield
Move-MP Up

Orichalcum
Mythril Knife
Feather Hat
Wizard Robe
Reflect Ring

Praise, Threaten, Solution, Insult, Refute
Wiznaibus, Polka Polka, Last Dance



Urieltheflameofgod
Female
Libra
39
79
Archer
Item
Caution
Doublehand
Jump+3

Night Killer

Mythril Helmet
Mythril Vest
Magic Ring

Charge+2, Charge+3, Charge+4
Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Chuckolator
Female
Scorpio
42
37
Knight
Throw
HP Restore
Magic Defense UP
Jump+3

Rune Blade
Aegis Shield
Cross Helmet
Crystal Mail
N-Kai Armlet

Armor Break, Shield Break, Magic Break
Shuriken



Arch8000
Female
Gemini
77
70
Dancer
Charge
HP Restore
Equip Armor
Move+2

Hydra Bag

Red Hood
Platinum Armor
Angel Ring

Witch Hunt, Slow Dance, Polka Polka, Nether Demon
Charge+1, Charge+5, Charge+7, Charge+20

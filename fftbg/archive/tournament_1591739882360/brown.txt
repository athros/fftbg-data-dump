Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kalabain
Female
Leo
78
75
Priest
Talk Skill
Mana Shield
Magic Attack UP
Move+3

Oak Staff

Thief Hat
Light Robe
Battle Boots

Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Shell, Shell 2, Esuna
Praise, Preach, Solution, Mimic Daravon, Refute



Sinnyil2
Male
Aries
71
53
Ninja
Draw Out
Counter Flood
Magic Attack UP
Teleport

Spell Edge
Air Knife
Black Hood
Judo Outfit
Elf Mantle

Bomb, Knife
Asura, Muramasa, Kikuichimoji



Wollip
Male
Gemini
46
78
Monk
Yin Yang Magic
Abandon
Magic Attack UP
Swim



Triangle Hat
Mystic Vest
Defense Ring

Secret Fist, Purification, Revive
Blind, Pray Faith, Confusion Song, Dispel Magic, Sleep



Sairentozon7
Male
Scorpio
46
70
Time Mage
Black Magic
Meatbone Slash
Attack UP
Swim

Rainbow Staff

Red Hood
White Robe
Elf Mantle

Haste, Slow, Float, Demi 2, Stabilize Time
Fire 2, Fire 3, Bolt, Ice 4, Frog

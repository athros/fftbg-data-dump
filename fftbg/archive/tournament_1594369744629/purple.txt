Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Superdevon1
Male
Aries
40
63
Summoner
White Magic
MA Save
Equip Polearm
Waterbreathing

Battle Bamboo

Feather Hat
Mythril Vest
Feather Mantle

Carbunkle, Odin, Leviathan, Silf
Cure 2, Raise, Raise 2, Regen, Protect 2, Shell 2, Esuna



Vultuous
Male
Virgo
70
68
Summoner
Draw Out
MP Restore
Sicken
Lava Walking

Ice Rod

Headgear
Mythril Vest
N-Kai Armlet

Moogle, Shiva, Ramuh, Golem, Bahamut, Salamander, Silf, Fairy, Lich
Koutetsu, Murasame



RoboticNomad
Female
Gemini
51
60
Squire
Elemental
Counter Tackle
Dual Wield
Jump+3

Iron Sword
Iron Sword
Green Beret
Adaman Vest
Feather Boots

Heal, Tickle, Yell, Wish
Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Rufe
Monster
Gemini
57
53
Grenade








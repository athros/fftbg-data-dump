Final Bets: purple - 4 bets for 3,178G (33.2%, x2.01); brown - 11 bets for 6,388G (66.8%, x0.50)

purple bets:
RoboticNomad: 1,000G (31.5%, 2,152G)
TheMurkGnome: 1,000G (31.5%, 5,344G)
upvla: 678G (21.3%, 7,322G)
amiture: 500G (15.7%, 1,570G)

brown bets:
randgridr: 1,348G (21.1%, 1,348G)
Zetchryn: 1,000G (15.7%, 3,206G)
BirbBrainsBot: 1,000G (15.7%, 5,305G)
Evewho: 728G (11.4%, 728G)
getthemoneyz: 670G (10.5%, 1,248,071G)
dantayystv: 500G (7.8%, 901G)
superdevon1: 424G (6.6%, 424G)
dem0nj0ns: 318G (5.0%, 318G)
datadrivenbot: 200G (3.1%, 63,244G)
Vultuous: 100G (1.6%, 4,747G)
Lemonjohns: 100G (1.6%, 1,084G)

Final Bets: green - 8 bets for 4,493G (52.5%, x0.91); yellow - 11 bets for 4,069G (47.5%, x1.10)

green bets:
Zetchryn: 1,102G (24.5%, 1,102G)
TheMurkGnome: 1,000G (22.3%, 5,344G)
Evewho: 728G (16.2%, 728G)
KitchTowel: 647G (14.4%, 647G)
superdevon1: 424G (9.4%, 424G)
DamnThatShark: 292G (6.5%, 292G)
datadrivenbot: 200G (4.5%, 62,958G)
dantayystv: 100G (2.2%, 705G)

yellow bets:
BirbBrainsBot: 909G (22.3%, 5,687G)
bruubarg: 848G (20.8%, 848G)
getthemoneyz: 548G (13.5%, 1,248,528G)
amiture: 500G (12.3%, 1,528G)
RoboticNomad: 300G (7.4%, 2,852G)
randgridr: 252G (6.2%, 1,135G)
dem0nj0ns: 208G (5.1%, 208G)
reinoe: 200G (4.9%, 6,453G)
Chuckolator: 104G (2.6%, 86,973G)
Vultuous: 100G (2.5%, 4,742G)
Lemonjohns: 100G (2.5%, 810G)

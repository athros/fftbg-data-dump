Player: !Green
Team: Green Team
Palettes: Green/White



Cho Pin
Male
Leo
76
40
Samurai
Yin Yang Magic
Faith Save
Beastmaster
Move-MP Up

Muramasa

Platinum Helmet
Chain Mail
Bracer

Asura, Koutetsu, Murasame, Kiyomori
Doubt Faith, Confusion Song, Paralyze, Sleep, Petrify



WhiteTigress
Male
Aquarius
53
58
Squire
Black Magic
Damage Split
Concentrate
Jump+3

Battle Axe
Gold Shield
Flash Hat
Brigandine
Rubber Shoes

Accumulate, Dash, Throw Stone, Heal, Tickle, Scream
Fire, Fire 4, Bolt 2, Ice 2



Escobro
Male
Aquarius
48
55
Lancer
Draw Out
Speed Save
Dual Wield
Jump+1

Partisan
Octagon Rod
Gold Helmet
Mythril Armor
Leather Mantle

Level Jump4, Vertical Jump8
Koutetsu, Murasame



Rhody
Female
Taurus
44
48
Lancer
Throw
Counter
Secret Hunt
Waterwalking

Spear
Buckler
Cross Helmet
Maximillian
Elf Mantle

Level Jump5, Vertical Jump2
Shuriken, Dictionary

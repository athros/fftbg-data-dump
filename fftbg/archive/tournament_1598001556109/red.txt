Player: !Red
Team: Red Team
Palettes: Red/Brown



Superdevon1
Male
Leo
79
54
Knight
Sing
Counter Tackle
Beastmaster
Swim

Defender
Crystal Shield
Leather Helmet
Bronze Armor
Germinas Boots

Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword
Last Song, Sky Demon, Hydra Pit



TeaTime29
Monster
Sagittarius
42
51
Vampire










Seaweed B
Male
Libra
49
80
Knight
Talk Skill
PA Save
Doublehand
Lava Walking

Defender

Diamond Helmet
Mythril Armor
Vanish Mantle

Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Surging Sword, Explosion Sword
Persuade, Praise, Threaten, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate



Gorgewall
Male
Aries
52
62
Geomancer
Talk Skill
Abandon
Sicken
Ignore Height

Diamond Sword
Aegis Shield
Flash Hat
Black Robe
Magic Gauntlet

Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Persuade, Preach, Negotiate

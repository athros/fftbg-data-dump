Player: !Black
Team: Black Team
Palettes: Black/Red



ApplesauceBoss
Male
Cancer
65
73
Archer
Sing
Brave Up
Doublehand
Move+1

Ultimus Bow

Flash Hat
Mythril Vest
Sprint Shoes

Charge+1
Life Song, Battle Song, Space Storage, Sky Demon



Treapvort
Male
Libra
68
68
Lancer
Yin Yang Magic
Regenerator
Dual Wield
Jump+1

Mythril Spear
Javelin
Platinum Helmet
Linen Robe
Jade Armlet

Level Jump2, Vertical Jump7
Poison, Pray Faith, Doubt Faith, Blind Rage, Foxbird



NovaKnight21
Female
Scorpio
54
70
Mime

Abandon
Martial Arts
Move-HP Up



Leather Hat
Carabini Mail
Leather Mantle

Mimic




HaplessOne
Male
Serpentarius
61
56
Knight
Elemental
Meatbone Slash
Doublehand
Move+3

Broad Sword

Iron Helmet
Leather Armor
Angel Ring

Armor Break, Shield Break, Power Break, Justice Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

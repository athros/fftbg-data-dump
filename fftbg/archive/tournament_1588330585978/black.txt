Player: !Black
Team: Black Team
Palettes: Black/Red



Mayormcfunbags
Female
Cancer
70
65
Squire
Draw Out
PA Save
Halve MP
Teleport

Sleep Sword
Buckler
Triangle Hat
Judo Outfit
N-Kai Armlet

Dash, Throw Stone, Tickle, Wish, Scream
Koutetsu, Kikuichimoji



Estan AD
Female
Taurus
82
43
Lancer
Elemental
Critical Quick
Martial Arts
Lava Walking

Mythril Spear
Platinum Shield
Platinum Helmet
Light Robe
Wizard Mantle

Level Jump3, Vertical Jump6
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Mtueni
Female
Pisces
49
78
Ninja
Draw Out
Distribute
Equip Gun
Waterwalking

Bloody Strings
Papyrus Codex
Thief Hat
Wizard Outfit
Battle Boots

Bomb
Heaven's Cloud, Kiyomori, Muramasa



Alberto08bernard
Female
Gemini
73
61
Thief
Draw Out
Speed Save
Sicken
Waterwalking

Coral Sword

Green Beret
Black Costume
Magic Ring

Steal Helmet, Steal Armor, Arm Aim
Koutetsu, Murasame, Heaven's Cloud, Chirijiraden

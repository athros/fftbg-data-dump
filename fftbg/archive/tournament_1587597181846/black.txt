Player: !Black
Team: Black Team
Palettes: Black/Red



DeathTaxesAndAnime
Female
Aries
67
54
Samurai
Charge
Counter Flood
Doublehand
Jump+2

Kiyomori

Circlet
Plate Mail
Wizard Mantle

Koutetsu, Heaven's Cloud
Charge+1, Charge+10



Phrossi V
Female
Scorpio
44
77
Ninja
Draw Out
Abandon
Defend
Ignore Terrain

Morning Star
Flail
Triangle Hat
Adaman Vest
Sprint Shoes

Staff
Koutetsu, Murasame



7Cerulean7
Female
Pisces
48
44
Chemist
White Magic
Regenerator
Dual Wield
Waterwalking

Cute Bag
Panther Bag
Feather Hat
Earth Clothes
Cursed Ring

Potion, X-Potion, Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Raise, Reraise, Regen, Protect 2, Shell, Magic Barrier



SwaffleWaffles
Female
Capricorn
62
64
Oracle
Dance
Dragon Spirit
Magic Defense UP
Move+2

Ivory Rod

Black Hood
Wizard Robe
Dracula Mantle

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Silence Song, Paralyze, Sleep
Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Last Dance

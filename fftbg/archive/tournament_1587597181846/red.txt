Player: !Red
Team: Red Team
Palettes: Red/Brown



0utlier
Male
Aries
64
70
Knight
Steal
Parry
Equip Gun
Retreat

Fairy Harp
Ice Shield
Mythril Helmet
Leather Armor
Power Wrist

Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break
Steal Shield, Steal Weapon, Steal Status, Arm Aim



EnemyController
Male
Libra
53
57
Wizard
Battle Skill
Counter Flood
Defense UP
Lava Walking

Dagger

Green Beret
Light Robe
Defense Ring

Fire, Fire 4, Bolt, Bolt 3, Ice 3, Empower
Weapon Break



Sinnyil2
Male
Cancer
80
54
Thief
Time Magic
Counter Flood
Dual Wield
Lava Walking

Short Edge
Sasuke Knife
Holy Miter
Judo Outfit
Elf Mantle

Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Haste 2, Slow, Quick, Demi, Stabilize Time



Readdesert
Male
Aquarius
79
41
Ninja
White Magic
Earplug
Equip Axe
Levitate

Assassin Dagger
Battle Axe
Black Hood
Power Sleeve
Genji Gauntlet

Shuriken, Hammer, Stick
Raise, Reraise, Wall, Holy

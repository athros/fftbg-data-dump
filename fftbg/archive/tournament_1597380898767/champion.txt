Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



KasugaiRoastedPeas
Male
Serpentarius
50
59
Archer
Talk Skill
Absorb Used MP
Equip Shield
Teleport

Ultimus Bow
Platinum Shield
Flash Hat
Judo Outfit
Red Shoes

Charge+1, Charge+4, Charge+10
Invitation, Persuade, Death Sentence, Mimic Daravon, Refute



Phi Sig
Female
Taurus
51
62
Priest
Draw Out
Faith Save
Beastmaster
Ignore Height

Wizard Staff

Golden Hairpin
Chain Vest
Spike Shoes

Cure, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Wall, Esuna, Magic Barrier
Kiyomori



Skillomono
Male
Gemini
51
79
Geomancer
Punch Art
Critical Quick
Defend
Move+2

Cute Bag
Crystal Shield
Flash Hat
Judo Outfit
Angel Ring

Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Lava Ball
Pummel, Secret Fist, Purification, Revive



Gunz232323
Male
Pisces
74
71
Knight
Basic Skill
Counter Flood
Beastmaster
Jump+3

Ancient Sword
Flame Shield
Genji Helmet
Wizard Robe
Battle Boots

Shield Break, Weapon Break, Speed Break, Mind Break, Night Sword
Heal, Fury

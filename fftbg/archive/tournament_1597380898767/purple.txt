Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Tarheels218
Male
Serpentarius
80
48
Calculator
Labyrinth Skill
Mana Shield
Attack UP
Waterwalking

Faith Rod

Leather Hat
Diamond Armor
Feather Boots

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power



Aremath
Male
Cancer
51
54
Priest
Item
Caution
Defense UP
Levitate

Oak Staff

Golden Hairpin
Black Costume
Rubber Shoes

Cure, Raise, Raise 2, Regen, Protect, Shell, Wall, Esuna
Potion, Ether, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Nifboy
Male
Libra
63
81
Oracle
Math Skill
Counter
Long Status
Ignore Terrain

Bestiary

Headgear
White Robe
Defense Armlet

Blind, Spell Absorb, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify, Dark Holy
CT, 5, 3



HASTERIOUS
Male
Virgo
49
66
Lancer
Talk Skill
Mana Shield
Martial Arts
Swim


Genji Shield
Barbuta
White Robe
Elf Mantle

Level Jump4, Vertical Jump8
Persuade, Insult, Refute

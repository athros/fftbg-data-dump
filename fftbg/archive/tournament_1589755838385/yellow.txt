Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nekojin
Male
Aries
72
71
Ninja
Black Magic
Faith Save
Equip Shield
Ignore Height

Flame Whip
Bronze Shield
Black Hood
Rubber Costume
Reflect Ring

Shuriken, Bomb
Fire, Fire 2, Fire 3, Fire 4, Bolt 2, Ice 2, Ice 3, Ice 4



CorpusCav
Female
Virgo
68
60
Oracle
Summon Magic
Speed Save
Equip Knife
Retreat

Assassin Dagger

Feather Hat
Light Robe
Battle Boots

Blind, Pray Faith, Silence Song, Dispel Magic, Petrify
Moogle, Shiva, Golem, Carbunkle, Fairy



Joewcarson
Male
Scorpio
79
51
Priest
Draw Out
Counter Magic
Magic Attack UP
Ignore Height

Rainbow Staff

Flash Hat
Chameleon Robe
Dracula Mantle

Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect, Protect 2, Wall, Esuna
Asura, Bizen Boat, Kiyomori, Muramasa



Regios91
Female
Leo
55
70
Lancer
Item
Caution
Equip Axe
Move-MP Up

Battle Axe
Platinum Shield
Leather Helmet
Plate Mail
Feather Mantle

Level Jump5, Vertical Jump7
Potion, Antidote, Eye Drop, Echo Grass, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



CynicalRazor
Female
Leo
54
54
Geomancer
Basic Skill
Sunken State
Defense UP
Move+1

Kiyomori
Hero Shield
Holy Miter
Chameleon Robe
Red Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball
Dash, Throw Stone, Heal, Tickle, Yell, Scream



Cellach33
Female
Leo
65
54
Thief
Time Magic
Caution
Long Status
Jump+1

Iron Sword

Golden Hairpin
Chain Vest
Dracula Mantle

Gil Taking, Steal Accessory
Stop, Demi, Meteor



EizanTayama
Male
Pisces
45
45
Archer
Yin Yang Magic
Mana Shield
Halve MP
Lava Walking

Perseus Bow

Feather Hat
Leather Vest
Feather Boots

Charge+1, Charge+7
Blind, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic



Underscoreedit
Male
Virgo
47
78
Oracle
Summon Magic
Parry
Attack UP
Move+1

Bestiary

Holy Miter
Silk Robe
Small Mantle

Zombie, Blind Rage, Confusion Song, Dispel Magic, Petrify
Ramuh, Salamander, Fairy

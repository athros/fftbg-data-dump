Player: !Red
Team: Red Team
Palettes: Red/Brown



Killth3kid
Female
Cancer
60
75
Knight
Draw Out
Meatbone Slash
Equip Armor
Move-MP Up

Platinum Sword
Genji Shield
Black Hood
Clothes
Leather Mantle

Armor Break, Magic Break, Power Break, Surging Sword
Bizen Boat, Kikuichimoji



Flacococo
Male
Libra
60
63
Time Mage
Yin Yang Magic
Dragon Spirit
Defense UP
Levitate

Musk Rod

Thief Hat
White Robe
N-Kai Armlet

Haste 2, Slow 2, Demi 2, Stabilize Time
Poison, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Paralyze, Petrify, Dark Holy



Calajo
Monster
Virgo
48
75
Cockatrice










DavenIII
Male
Aries
65
45
Thief
Charge
Parry
Short Charge
Retreat

Star Bag

Thief Hat
Wizard Outfit
Elf Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Leg Aim
Charge+4, Charge+10

Final Bets: red - 16 bets for 6,907G (45.9%, x1.18); blue - 14 bets for 8,129G (54.1%, x0.85)

red bets:
Aldrammech: 1,000G (14.5%, 19,729G)
ThePineappleSalesman: 765G (11.1%, 765G)
Laserman1000: 685G (9.9%, 6,585G)
HASTERIOUS: 614G (8.9%, 12,293G)
tronfonne: 593G (8.6%, 593G)
bruubarg: 500G (7.2%, 2,409G)
Rislyeu: 500G (7.2%, 6,065G)
serperemagus: 497G (7.2%, 497G)
DustBirdEX: 456G (6.6%, 5,808G)
Lanshaft: 416G (6.0%, 3,634G)
Helllyesss: 281G (4.1%, 281G)
Rintastic: 200G (2.9%, 531G)
E_Ballard: 100G (1.4%, 11,448G)
nifboy: 100G (1.4%, 8,355G)
ANFz: 100G (1.4%, 4,814G)
datadrivenbot: 100G (1.4%, 21,121G)

blue bets:
BoneMiser: 1,263G (15.5%, 1,263G)
Shalloween: 1,176G (14.5%, 1,176G)
Baron_von_Scrub: 1,111G (13.7%, 17,628G)
Meta_Five: 1,000G (12.3%, 12,999G)
DeathTaxesAndAnime: 664G (8.2%, 664G)
getthemoneyz: 536G (6.6%, 717,015G)
Jinxzers: 500G (6.2%, 1,404G)
Saldarin: 500G (6.2%, 1,357G)
YaBoy125: 428G (5.3%, 428G)
BirbBrainsBot: 401G (4.9%, 161,324G)
MADTheta: 250G (3.1%, 1,546G)
AllInBot: 100G (1.2%, 100G)
maakur_: 100G (1.2%, 4,859G)
CorpusCav: 100G (1.2%, 2,309G)

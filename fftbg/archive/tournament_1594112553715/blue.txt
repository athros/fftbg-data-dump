Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Helpimabug
Male
Taurus
60
75
Archer
Battle Skill
Hamedo
Magic Attack UP
Teleport

Lightning Bow

Headgear
Adaman Vest
N-Kai Armlet

Charge+1, Charge+4, Charge+5, Charge+7
Head Break, Armor Break, Power Break, Stasis Sword, Surging Sword



Enkikavlar
Male
Cancer
77
39
Monk
Throw
Counter Tackle
Attack UP
Move-HP Up



Thief Hat
Earth Clothes
Defense Ring

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Bomb, Hammer, Dictionary



ALY327
Male
Sagittarius
73
76
Archer
Throw
Abandon
Equip Sword
Retreat

Heaven's Cloud
Mythril Shield
Bronze Helmet
Leather Outfit
Spike Shoes

Charge+3, Charge+7, Charge+20
Knife



ApplesauceBoss
Female
Sagittarius
65
43
Ninja
Basic Skill
Dragon Spirit
Equip Gun
Jump+2

Papyrus Codex
Battle Folio
Headgear
Earth Clothes
Feather Boots

Knife
Dash, Throw Stone, Heal, Scream

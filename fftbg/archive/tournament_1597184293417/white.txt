Player: !White
Team: White Team
Palettes: White/Blue



VolgraTheMoose
Female
Leo
63
62
Summoner
Yin Yang Magic
Mana Shield
Dual Wield
Waterwalking

Poison Rod
Wizard Staff
Headgear
Chain Vest
108 Gems

Shiva, Fairy
Life Drain, Pray Faith, Doubt Faith, Dispel Magic



Daveb
Male
Aries
68
65
Ninja
Time Magic
Damage Split
Maintenance
Ignore Height

Flail
Dagger
Triangle Hat
Secret Clothes
Reflect Ring

Shuriken
Haste, Reflect, Demi, Stabilize Time



Resjudicata3
Monster
Gemini
44
63
Ultima Demon










CapnChaos12
Female
Capricorn
73
65
Samurai
Battle Skill
Earplug
Magic Attack UP
Fly

Muramasa

Platinum Helmet
Platinum Armor
Rubber Shoes

Asura, Koutetsu, Heaven's Cloud
Head Break, Shield Break

Player: !Green
Team: Green Team
Palettes: Green/White



AniZero
Male
Gemini
78
67
Thief
Summon Magic
PA Save
Halve MP
Jump+2

Short Edge

Headgear
Black Costume
Leather Mantle

Steal Helmet, Steal Armor, Leg Aim
Moogle, Golem, Carbunkle, Salamander, Silf



Powermhero
Male
Virgo
60
51
Mediator
Summon Magic
Damage Split
Equip Sword
Retreat

Koutetsu Knife

Golden Hairpin
Earth Clothes
Elf Mantle

Persuade, Praise, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute
Ramuh, Ifrit, Carbunkle, Bahamut, Odin, Silf



Flameatron
Male
Capricorn
64
44
Lancer
Basic Skill
Faith Save
Equip Armor
Levitate

Spear
Round Shield
Leather Hat
Judo Outfit
Sprint Shoes

Level Jump3, Vertical Jump5
Accumulate, Heal, Tickle, Scream



Evewho
Monster
Taurus
59
41
Swine








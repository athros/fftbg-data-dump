Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



VolgraTheMoose
Male
Cancer
53
39
Thief
Punch Art
Parry
Doublehand
Move+3

Orichalcum

Flash Hat
Wizard Outfit
108 Gems

Steal Heart, Steal Helmet
Wave Fist, Earth Slash, Secret Fist, Purification



Ruebyy
Female
Sagittarius
43
64
Knight
Black Magic
Counter Tackle
Dual Wield
Jump+2

Ragnarok
Panther Bag
Cross Helmet
Diamond Armor
Dracula Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Stasis Sword, Justice Sword, Dark Sword
Ice 2, Ice 3



Laserman1000
Male
Aquarius
78
55
Calculator
Black Magic
HP Restore
Beastmaster
Jump+1

Ice Rod

Leather Hat
Secret Clothes
Angel Ring

CT, Height, 5, 4, 3
Fire, Fire 3, Bolt, Bolt 2, Bolt 3, Ice 2, Ice 3, Frog



PoroTact
Male
Scorpio
57
57
Thief
Charge
HP Restore
Dual Wield
Move-HP Up

Orichalcum
Sleep Sword
Green Beret
Adaman Vest
Diamond Armlet

Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Charge+1, Charge+2, Charge+4, Charge+5, Charge+7

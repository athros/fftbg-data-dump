Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



StealthModeLocke
Male
Pisces
57
64
Priest
Throw
Abandon
Magic Attack UP
Move-MP Up

Flame Whip

Triangle Hat
Light Robe
Power Wrist

Cure 4, Raise, Reraise, Protect
Shuriken



CosmicTactician
Male
Libra
54
48
Mediator
Battle Skill
Auto Potion
Halve MP
Move-MP Up

Stone Gun

Ribbon
Light Robe
Power Wrist

Invitation, Persuade, Praise, Threaten, Solution, Refute
Armor Break, Weapon Break, Magic Break, Justice Sword, Night Sword



CaptainGarlock
Female
Aries
47
44
Thief
Punch Art
Critical Quick
Equip Polearm
Fly

Gokuu Rod

Golden Hairpin
Wizard Outfit
Feather Boots

Steal Heart, Leg Aim
Pummel, Wave Fist, Secret Fist



GladiatorLupe
Male
Leo
58
45
Squire
Talk Skill
Meatbone Slash
Dual Wield
Move-HP Up

Ice Brand
Mage Masher
Holy Miter
Wizard Outfit
N-Kai Armlet

Cheer Up
Invitation, Preach, Insult, Negotiate, Refute, Rehabilitate

Final Bets: white - 21 bets for 11,327G (45.7%, x1.19); black - 23 bets for 13,435G (54.3%, x0.84)

white bets:
ig_ats: 1,616G (14.3%, 1,616G)
FNCardascia_: 1,528G (13.5%, 1,528G)
Aldrammech: 1,000G (8.8%, 27,456G)
BirbBrainsBot: 1,000G (8.8%, 63,758G)
Breakdown777: 1,000G (8.8%, 18,369G)
Lionhermit: 1,000G (8.8%, 21,901G)
CorpusCav: 553G (4.9%, 33,913G)
Estan_AD: 539G (4.8%, 1,078G)
luminarii: 501G (4.4%, 12,935G)
EnemyController: 500G (4.4%, 192,554G)
Laserman1000: 404G (3.6%, 404G)
twelfthrootoftwo: 400G (3.5%, 3,229G)
jwchapm: 300G (2.6%, 328G)
Setdevildog: 208G (1.8%, 208G)
Billybones5150: 200G (1.8%, 22,448G)
DrAntiSocial: 150G (1.3%, 7,787G)
caprinovoa: 108G (1.0%, 108G)
ANFz: 100G (0.9%, 30,590G)
maakur_: 100G (0.9%, 122,572G)
OneHundredFists: 100G (0.9%, 1,072G)
getthemoneyz: 20G (0.2%, 632,167G)

black bets:
Baron_von_Scrub: 2,439G (18.2%, 2,439G)
killth3kid: 2,000G (14.9%, 78,320G)
SQUiDSQUARKLIN: 1,849G (13.8%, 1,849G)
Evode: 1,424G (10.6%, 1,424G)
dinin991: 1,340G (10.0%, 1,340G)
nifboy: 508G (3.8%, 508G)
joewcarson: 500G (3.7%, 5,984G)
ungabunga_bot: 479G (3.6%, 379,476G)
ZephyrTempest: 456G (3.4%, 2,210G)
KasugaiRoastedPeas: 399G (3.0%, 8,235G)
Mesmaster: 344G (2.6%, 344G)
CT_5_Holy: 232G (1.7%, 232G)
Arlum: 228G (1.7%, 228G)
SodaAddict: 200G (1.5%, 3,280G)
RunicMagus: 200G (1.5%, 49,306G)
hisuikohaku: 176G (1.3%, 1,576G)
gorgewall: 100G (0.7%, 6,163G)
TimeJannies: 100G (0.7%, 1,787G)
rjA0zcOQ96: 100G (0.7%, 28,471G)
datadrivenbot: 100G (0.7%, 12,426G)
vampyreinabox: 100G (0.7%, 2,139G)
Firesheath: 100G (0.7%, 10,949G)
Archeious: 61G (0.5%, 123G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SeniorBunk
Monster
Aries
41
40
Red Dragon










Powergems
Female
Scorpio
64
68
Calculator
White Magic
Counter Magic
Doublehand
Jump+1

Battle Folio

Green Beret
Chain Vest
Leather Mantle

CT, Height, Prime Number, 5, 3
Cure, Raise, Regen, Shell, Wall, Esuna, Holy



E Ballard
Male
Cancer
51
52
Samurai
Sing
Faith Save
Equip Bow
Move-HP Up

Yoichi Bow

Mythril Helmet
Bronze Armor
Diamond Armlet

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Angel Song, Cheer Song, Battle Song, Last Song, Sky Demon



UmaiJam
Female
Capricorn
57
72
Archer
Talk Skill
Speed Save
Sicken
Move+2

Poison Bow
Kaiser Plate
Red Hood
Black Costume
Rubber Shoes

Charge+1, Charge+4, Charge+7, Charge+10, Charge+20
Persuade, Praise, Threaten, Preach, Refute

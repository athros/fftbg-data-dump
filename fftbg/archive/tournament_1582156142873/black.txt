Player: !Black
Team: Black Team
Palettes: Black/Red



Typicalfanboy
Male
Scorpio
39
53
Mime

Mana Shield
Short Status
Move-HP Up



Red Hood
Plate Mail
Spike Shoes

Mimic




Zerguzen
Female
Leo
53
55
Ninja
Time Magic
Mana Shield
Halve MP
Ignore Height

Sasuke Knife
Ninja Edge
Green Beret
Mystic Vest
Feather Mantle

Shuriken, Bomb, Knife
Haste, Haste 2, Slow 2, Stop, Immobilize, Reflect, Quick, Stabilize Time



MoonSlayerRS
Male
Libra
51
64
Monk
Elemental
Hamedo
Defend
Move-MP Up



Feather Hat
Clothes
Dracula Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



DieInObscurity
Male
Taurus
72
53
Wizard
Draw Out
Catch
Equip Bow
Ignore Terrain

Flame Rod

Golden Hairpin
Wizard Robe
Jade Armlet

Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4
Murasame, Heaven's Cloud, Muramasa, Kikuichimoji

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Silgryn
Male
Virgo
50
55
Squire
Punch Art
Dragon Spirit
Concentrate
Ignore Height

Long Sword

Crystal Helmet
Leather Outfit
Wizard Mantle

Dash, Tickle, Cheer Up, Wish
Spin Fist, Pummel, Revive



Mizucrux
Male
Leo
66
59
Monk
Talk Skill
Counter
Dual Wield
Retreat



Feather Hat
Mystic Vest
Diamond Armlet

Spin Fist, Earth Slash, Purification
Invitation, Praise, Solution, Death Sentence, Insult, Mimic Daravon, Refute



AlysSariel
Male
Virgo
79
57
Wizard
White Magic
Critical Quick
Equip Axe
Teleport 2

Rainbow Staff

Leather Hat
Mythril Vest
Defense Ring

Fire, Fire 2, Ice, Ice 4, Frog, Flare
Raise, Reraise, Regen, Shell, Wall, Holy



Helpimabug
Male
Aries
80
54
Oracle
Summon Magic
Hamedo
Halve MP
Swim

Musk Rod

Black Hood
Wizard Robe
Chantage

Blind, Life Drain, Zombie, Confusion Song, Sleep
Moogle, Shiva, Titan, Fairy, Lich

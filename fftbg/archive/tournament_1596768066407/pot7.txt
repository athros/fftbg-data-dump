Final Bets: blue - 9 bets for 11,183G (46.8%, x1.13); purple - 16 bets for 12,689G (53.2%, x0.88)

blue bets:
ko2q: 7,280G (65.1%, 14,560G)
TheMurkGnome: 1,000G (8.9%, 1,000G)
BirbBrainsBot: 804G (7.2%, 8,741G)
douchetron: 600G (5.4%, 600G)
Peluroma: 600G (5.4%, 835G)
Smashy: 500G (4.5%, 7,779G)
getthemoneyz: 182G (1.6%, 1,540,352G)
Gunz232323: 116G (1.0%, 116G)
gorgewall: 101G (0.9%, 2,235G)

purple bets:
DeathTaxesAndAnime: 2,359G (18.6%, 4,627G)
KasugaiRoastedPeas: 2,159G (17.0%, 2,159G)
SkylerBunny: 1,600G (12.6%, 312,591G)
Lydian_C: 1,200G (9.5%, 14,542G)
nhammen: 1,000G (7.9%, 193,433G)
AllInBot: 985G (7.8%, 985G)
Chuckolator: 966G (7.6%, 10,576G)
Zeroroute: 600G (4.7%, 600G)
tohim: 492G (3.9%, 492G)
CorpusCav: 300G (2.4%, 3,065G)
PantherIscariot: 234G (1.8%, 1,341G)
datadrivenbot: 200G (1.6%, 49,621G)
twelfthrootoftwo: 200G (1.6%, 4,790G)
murdecat: 200G (1.6%, 724G)
RunicMagus: 100G (0.8%, 5,098G)
xBizzy: 94G (0.7%, 947G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Haaaaaank
Male
Sagittarius
73
70
Squire
Charge
Catch
Equip Polearm
Swim

Gungnir
Flame Shield
Circlet
Linen Cuirass
Leather Mantle

Dash, Heal, Yell
Charge+1, Charge+4, Charge+7, Charge+10, Charge+20



Rechaun
Male
Taurus
68
80
Time Mage
Throw
Counter Flood
Short Status
Move-HP Up

Oak Staff

Red Hood
Leather Outfit
Angel Ring

Haste 2, Slow 2, Stop, Immobilize, Reflect, Demi 2, Stabilize Time
Shuriken, Bomb, Spear, Wand



Faqs McGee
Female
Leo
62
82
Knight
Item
Distribute
Defend
Jump+3

Long Sword
Gold Shield
Circlet
Diamond Armor
Dracula Mantle

Shield Break, Speed Break, Dark Sword
X-Potion, Ether, Echo Grass, Remedy, Phoenix Down



Jeeboheebo
Female
Aquarius
62
56
Ninja
Draw Out
Earplug
Equip Armor
Jump+3

Short Edge
Ninja Edge
Mythril Helmet
Plate Mail
Leather Mantle

Shuriken, Bomb, Knife, Spear
Koutetsu, Muramasa

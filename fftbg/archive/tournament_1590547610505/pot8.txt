Final Bets: brown - 22 bets for 24,345G (35.4%, x1.83); champion - 28 bets for 44,517G (64.6%, x0.55)

brown bets:
NicoSavoy: 11,014G (45.2%, 22,028G)
Baron_von_Scrub: 2,825G (11.6%, 5,651G)
Ungaiii: 1,575G (6.5%, 1,575G)
LASERJESUS1337: 1,500G (6.2%, 63,383G)
EmperorBeef: 1,256G (5.2%, 252,256G)
BirbBrainsBot: 1,000G (4.1%, 188,208G)
Shalloween: 800G (3.3%, 8,842G)
Tsenn: 523G (2.1%, 523G)
Estan_AD: 520G (2.1%, 1,041G)
Dymntd: 500G (2.1%, 26,474G)
Mokura3rd: 435G (1.8%, 435G)
CorpusCav: 403G (1.7%, 9,203G)
Firesheath: 372G (1.5%, 372G)
getthemoneyz: 368G (1.5%, 731,846G)
electric_algus: 300G (1.2%, 7,293G)
gogofromtogo: 204G (0.8%, 204G)
solomongrundy85: 200G (0.8%, 2,149G)
kaidykat: 200G (0.8%, 7,988G)
E_Ballard: 100G (0.4%, 9,997G)
Grandlanzer: 100G (0.4%, 1,563G)
ScurvyMitch: 100G (0.4%, 4,706G)
cougboi: 50G (0.2%, 12,384G)

champion bets:
EnemyController: 10,000G (22.5%, 363,756G)
KyleWonToLiveForever: 8,583G (19.3%, 8,583G)
gooseyourself: 4,831G (10.9%, 9,663G)
rednecknazgul: 3,418G (7.7%, 3,418G)
TheMM42: 3,000G (6.7%, 26,156G)
HaplessOne: 2,500G (5.6%, 326,911G)
Mesmaster: 2,000G (4.5%, 77,203G)
poojamz: 1,893G (4.3%, 1,893G)
LeoNightFury: 1,648G (3.7%, 1,648G)
ValtonZenola: 1,127G (2.5%, 1,127G)
cloud92684: 1,000G (2.2%, 10,793G)
HaychDub: 1,000G (2.2%, 13,107G)
poulol: 700G (1.6%, 9,190G)
TinchoT: 616G (1.4%, 616G)
Xcessive30: 392G (0.9%, 392G)
duckfist: 300G (0.7%, 4,272G)
nifboy: 250G (0.6%, 11,561G)
ravingsockmonkey: 200G (0.4%, 2,948G)
Sairentozon7: 158G (0.4%, 1,038G)
gorgewall: 101G (0.2%, 7,646G)
AllInBot: 100G (0.2%, 100G)
Tougou: 100G (0.2%, 1,022G)
DeathTaxesAndAnime: 100G (0.2%, 5,444G)
CapnChaos12: 100G (0.2%, 2,680G)
danteangel7psn: 100G (0.2%, 100G)
datadrivenbot: 100G (0.2%, 22,435G)
itszaane: 100G (0.2%, 2,223G)
CosmicTactician: 100G (0.2%, 11,134G)

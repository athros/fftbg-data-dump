Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Mesmaster
Male
Sagittarius
57
40
Lancer
White Magic
Counter Tackle
Defense UP
Teleport

Holy Lance
Venetian Shield
Diamond Helmet
White Robe
Magic Ring

Level Jump8, Vertical Jump8
Raise, Wall, Esuna



Volgrathemoose
Female
Taurus
68
51
Samurai
Jump
PA Save
Equip Bow
Waterbreathing

Silver Bow

Bronze Helmet
Maximillian
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Level Jump5, Vertical Jump4



KyleWonToLiveForever
Female
Leo
66
64
Mediator
Elemental
Distribute
Short Status
Move-MP Up

Stone Gun

Cachusha
Wizard Robe
Angel Ring

Invitation, Refute
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Gooseyourself
Female
Virgo
76
51
Mime

Meatbone Slash
Magic Attack UP
Teleport



Green Beret
Adaman Vest
Reflect Ring

Mimic


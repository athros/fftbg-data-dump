Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sebsebseabass
Female
Aquarius
54
78
Summoner
Talk Skill
HP Restore
Monster Talk
Ignore Terrain

Dragon Rod

Black Hood
Wizard Robe
Dracula Mantle

Moogle, Titan, Leviathan, Salamander, Fairy
Preach, Death Sentence, Insult, Refute



Heartofgoldfish
Male
Aquarius
41
61
Lancer
Basic Skill
MA Save
Short Status
Jump+3

Musk Rod
Genji Shield
Circlet
Chameleon Robe
Sprint Shoes

Level Jump5, Vertical Jump8
Heal, Tickle



Mithical9
Male
Aries
74
63
Monk
Yin Yang Magic
Counter
Equip Shield
Move+2


Platinum Shield
Flash Hat
Secret Clothes
Spike Shoes

Pummel, Purification, Revive
Poison, Spell Absorb, Silence Song, Blind Rage, Dispel Magic, Paralyze



RjA0zcOQ96
Male
Libra
78
44
Knight
Throw
PA Save
Long Status
Move+1

Platinum Sword
Round Shield
Genji Helmet
Gold Armor
Feather Boots

Head Break, Magic Break, Power Break, Stasis Sword, Justice Sword, Surging Sword, Explosion Sword
Shuriken, Bomb

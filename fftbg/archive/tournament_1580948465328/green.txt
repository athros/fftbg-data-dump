Player: !Green
Team: Green Team
Palettes: Green/White



Bringmoreknives
Male
Cancer
48
60
Archer
Jump
Catch
Dual Wield
Jump+2

Lightning Bow

Twist Headband
Wizard Outfit
Rubber Shoes

Charge+4, Charge+7, Charge+20
Level Jump2, Vertical Jump6



Gandrin
Female
Sagittarius
69
67
Chemist
Dance
Sunken State
Equip Axe
Fly

Scorpion Tail

Headgear
Earth Clothes
Angel Ring

Potion, Hi-Potion, Antidote, Maiden's Kiss, Remedy
Wiznaibus, Disillusion, Nameless Dance, Last Dance



Blacklistclancy
Female
Aquarius
57
78
Knight
Jump
Hamedo
Equip Gun
Waterbreathing

Mythril Gun
Flame Shield
Circlet
Crystal Mail
Reflect Ring

Dark Sword, Surging Sword
Level Jump2, Vertical Jump8



AuroraCL
Male
Libra
78
76
Thief
Black Magic
Hamedo
Concentrate
Move-MP Up

Blind Knife

Red Hood
Power Sleeve
108 Gems

Gil Taking, Steal Helmet
Fire 2, Bolt, Bolt 3, Ice 2, Empower

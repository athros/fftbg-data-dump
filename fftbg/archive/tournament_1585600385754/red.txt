Player: !Red
Team: Red Team
Palettes: Red/Brown



Sinnyil2
Male
Libra
55
49
Ninja
Time Magic
PA Save
Attack UP
Waterbreathing

Main Gauche
Zorlin Shape
Green Beret
Judo Outfit
108 Gems

Shuriken, Bomb, Stick
Stop, Float, Quick, Galaxy Stop



Tithonus
Female
Libra
69
44
Calculator
Yin Yang Magic
Auto Potion
Magic Attack UP
Jump+1

Iron Fan

Feather Hat
Chain Vest
Magic Gauntlet

CT, Prime Number, 3
Doubt Faith, Blind Rage



Avin Chaos
Male
Pisces
49
57
Ninja
Black Magic
Arrow Guard
Maintenance
Fly

Iga Knife
Sasuke Knife
Red Hood
Power Sleeve
Dracula Mantle

Shuriken, Staff, Spear
Ice 2, Ice 3, Ice 4, Empower



DudeMonkey77
Male
Sagittarius
48
77
Oracle
Time Magic
Mana Shield
Secret Hunt
Move+2

Bestiary

Black Hood
Chameleon Robe
Wizard Mantle

Poison, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Dark Holy
Haste 2, Slow 2, Float, Quick, Stabilize Time

Player: !White
Team: White Team
Palettes: White/Blue



Aznwanderor
Female
Virgo
70
69
Ninja
Item
Mana Shield
Throw Item
Lava Walking

Ninja Edge
Ninja Edge
Red Hood
Black Costume
Sprint Shoes

Shuriken, Bomb, Staff, Dictionary
Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop



CapnChaos12
Female
Leo
57
50
Archer
Punch Art
Counter Tackle
Dual Wield
Levitate

Long Bow

Golden Hairpin
Brigandine
Feather Boots

Charge+5
Spin Fist, Pummel, Purification, Seal Evil



TinchoT
Male
Aquarius
70
55
Geomancer
Charge
PA Save
Beastmaster
Move+1

Koutetsu Knife
Round Shield
Leather Hat
Light Robe
Germinas Boots

Pitfall, Hell Ivy, Static Shock, Sand Storm, Gusty Wind, Lava Ball
Charge+1, Charge+4, Charge+10, Charge+20



PoroTact
Male
Capricorn
79
62
Ninja
Draw Out
Regenerator
Equip Gun
Ignore Terrain

Battle Folio
Battle Folio
Twist Headband
Wizard Outfit
Sprint Shoes

Bomb
Asura, Murasame

Player: !Red
Team: Red Team
Palettes: Red/Brown



DustBirdEX
Female
Scorpio
74
49
Wizard
Throw
MP Restore
Short Charge
Waterbreathing

Mythril Knife

Black Hood
Judo Outfit
Defense Armlet

Fire 2, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower
Shuriken, Knife, Wand



0utlier
Female
Serpentarius
65
46
Calculator
Time Magic
Regenerator
Long Status
Swim

Panther Bag

Ribbon
Silk Robe
Defense Armlet

CT, Height, Prime Number, 5, 4, 3
Slow, Slow 2, Stop, Float, Demi, Stabilize Time



Arch8000
Male
Aries
75
63
Priest
Black Magic
Counter
Magic Defense UP
Jump+2

Gold Staff

Black Hood
Mystic Vest
Jade Armlet

Cure 2, Raise, Raise 2, Esuna
Fire 4, Bolt 3, Bolt 4, Ice, Death



ApplesauceBoss
Female
Gemini
59
65
Dancer
Jump
Critical Quick
Dual Wield
Ignore Height

Ryozan Silk
Cashmere
Twist Headband
Black Robe
Rubber Shoes

Witch Hunt, Polka Polka, Disillusion, Last Dance, Void Storage, Dragon Pit
Level Jump8, Vertical Jump2

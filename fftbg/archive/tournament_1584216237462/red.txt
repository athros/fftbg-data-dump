Player: !Red
Team: Red Team
Palettes: Red/Brown



OneHundredFists
Monster
Capricorn
60
43
Tiamat










Rolodex
Female
Capricorn
75
62
Priest
Battle Skill
Caution
Equip Armor
Jump+2

Morning Star

Iron Helmet
Light Robe
Reflect Ring

Cure, Cure 2, Reraise, Shell, Esuna, Holy
Head Break, Shield Break, Speed Break, Power Break, Justice Sword, Surging Sword



MalakoFox
Female
Capricorn
70
61
Chemist
Time Magic
Counter
Secret Hunt
Waterwalking

Star Bag

Leather Hat
Leather Outfit
Sprint Shoes

Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Holy Water
Haste, Haste 2, Slow, Immobilize, Float, Reflect, Demi, Stabilize Time



HASTERIOUS
Male
Aquarius
48
44
Calculator
Black Magic
Absorb Used MP
Secret Hunt
Move+2

Ivory Rod

Thief Hat
Wizard Outfit
Spike Shoes

CT, Height, Prime Number, 5, 4
Fire 2, Bolt, Ice, Ice 3

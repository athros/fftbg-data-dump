Player: !Red
Team: Red Team
Palettes: Red/Brown



JarMustard
Male
Sagittarius
45
79
Samurai
Elemental
MP Restore
Maintenance
Move-HP Up

Mythril Spear

Cross Helmet
Gold Armor
Dracula Mantle

Koutetsu, Murasame
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Onetrickwolf
Male
Leo
43
76
Summoner
Yin Yang Magic
Catch
Doublehand
Levitate

Flame Rod

Holy Miter
Light Robe
Elf Mantle

Moogle, Golem, Carbunkle, Bahamut, Salamander, Fairy, Lich
Zombie, Foxbird, Dispel Magic, Paralyze



Tougou
Female
Pisces
76
40
Geomancer
Draw Out
MA Save
Concentrate
Teleport

Slasher
Platinum Shield
Feather Hat
Chameleon Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Koutetsu, Murasame, Kiyomori, Kikuichimoji



BenYuPoker
Female
Virgo
73
45
Summoner
Charge
Abandon
Short Charge
Move-HP Up

Thunder Rod

Red Hood
Brigandine
Genji Gauntlet

Shiva, Ramuh, Ifrit, Fairy, Lich
Charge+1

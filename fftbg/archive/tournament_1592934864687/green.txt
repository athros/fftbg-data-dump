Player: !Green
Team: Green Team
Palettes: Green/White



Miku Shikhu
Female
Libra
55
65
Summoner
Yin Yang Magic
Speed Save
Short Charge
Ignore Height

Dragon Rod

Feather Hat
Earth Clothes
Small Mantle

Moogle, Shiva, Golem, Carbunkle, Bahamut, Odin, Salamander
Spell Absorb, Life Drain, Foxbird, Paralyze, Sleep



Run With Stone GUNs
Female
Taurus
61
61
Time Mage
Draw Out
Parry
Equip Bow
Fly

Night Killer

Thief Hat
Mystic Vest
Magic Gauntlet

Haste 2, Slow, Slow 2, Quick, Demi, Stabilize Time
Asura, Bizen Boat, Muramasa, Kikuichimoji



StealthModeLocke
Male
Pisces
68
72
Samurai
Jump
Auto Potion
Equip Sword
Move+1

Ancient Sword

Diamond Helmet
Crystal Mail
Bracer

Asura, Koutetsu, Bizen Boat, Muramasa, Kikuichimoji
Level Jump2, Vertical Jump7



J2DaBibbles
Female
Taurus
57
66
Dancer
Item
Meatbone Slash
Throw Item
Move+1

Persia

Twist Headband
Wizard Robe
Angel Ring

Witch Hunt, Wiznaibus, Slow Dance, Nameless Dance, Dragon Pit
Hi-Potion, Ether, Eye Drop, Holy Water, Phoenix Down

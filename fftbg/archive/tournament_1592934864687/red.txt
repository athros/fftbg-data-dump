Player: !Red
Team: Red Team
Palettes: Red/Brown



Aldrammech
Monster
Sagittarius
72
77
Tiamat










Kohlingen
Female
Pisces
58
46
Knight
Punch Art
Absorb Used MP
Short Charge
Teleport

Chaos Blade
Escutcheon
Crystal Helmet
Bronze Armor
Rubber Shoes

Head Break, Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Surging Sword
Purification, Chakra, Revive



VolgraTheMoose
Male
Capricorn
44
54
Chemist
Elemental
Arrow Guard
Short Status
Levitate

Dagger

Leather Hat
Black Costume
Sprint Shoes

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



O Heyno
Female
Pisces
71
74
Knight
Punch Art
Counter Magic
Equip Polearm
Ignore Terrain

Holy Lance
Mythril Shield
Mythril Helmet
Carabini Mail
Sprint Shoes

Speed Break, Power Break
Secret Fist, Revive

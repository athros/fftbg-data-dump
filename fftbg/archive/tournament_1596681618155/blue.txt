Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zeroroute
Male
Capricorn
64
41
Summoner
Basic Skill
Abandon
Equip Bow
Jump+2

Snipe Bow

Leather Hat
White Robe
Setiemson

Ramuh, Ifrit, Titan, Carbunkle, Fairy
Dash, Heal, Fury, Wish



Superdevon1
Male
Aquarius
60
46
Squire
Jump
Dragon Spirit
Halve MP
Fly

Ancient Sword
Round Shield
Green Beret
Earth Clothes
Red Shoes

Dash, Heal, Tickle, Yell
Level Jump3, Vertical Jump5



Ko2q
Male
Scorpio
62
60
Knight
Summon Magic
Faith Save
Sicken
Teleport 2

Slasher
Bronze Shield
Iron Helmet
Wizard Robe
N-Kai Armlet

Shield Break, Magic Break, Speed Break, Power Break, Justice Sword
Moogle, Shiva, Carbunkle



Dogsandcatsand
Female
Pisces
48
62
Wizard
Elemental
Hamedo
Short Charge
Levitate

Panther Bag

Feather Hat
Silk Robe
Sprint Shoes

Fire 3, Bolt 2, Bolt 3, Ice, Ice 3
Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind

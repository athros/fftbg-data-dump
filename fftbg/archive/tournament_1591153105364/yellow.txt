Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ScurvyMitch
Male
Sagittarius
59
79
Chemist
Elemental
Regenerator
Maintenance
Teleport

Panther Bag

Feather Hat
Power Sleeve
Spike Shoes

Hi-Ether, Soft, Holy Water, Remedy, Phoenix Down
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Hamborn
Male
Taurus
71
65
Chemist
White Magic
Counter
Defend
Move+1

Hydra Bag

Holy Miter
Clothes
Small Mantle

Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down
Cure, Raise 2, Protect, Protect 2, Esuna



Nizaha
Female
Aries
78
69
Monk
Yin Yang Magic
Dragon Spirit
Attack UP
Ignore Height



Twist Headband
Wizard Outfit
Genji Gauntlet

Spin Fist, Purification
Poison, Zombie, Silence Song, Blind Rage, Confusion Song, Paralyze, Petrify



Aldrammech
Female
Capricorn
67
47
Squire
White Magic
Speed Save
Martial Arts
Move+1


Mythril Shield
Green Beret
Power Sleeve
Magic Gauntlet

Dash, Heal, Tickle, Fury, Wish
Cure, Cure 2, Raise, Reraise, Protect, Wall, Esuna

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kaidykat
Female
Virgo
51
46
Chemist
Charge
Dragon Spirit
Equip Polearm
Jump+1

Iron Fan

Triangle Hat
Judo Outfit
Defense Armlet

Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down
Charge+2, Charge+3



Computerghoul
Male
Taurus
56
73
Squire
Sing
Sunken State
Attack UP
Ignore Height

Hunting Bow
Escutcheon
Leather Helmet
Clothes
Dracula Mantle

Accumulate, Throw Stone, Heal, Cheer Up
Angel Song



CorpusCav
Female
Libra
64
62
Squire
Throw
Counter Flood
Magic Defense UP
Levitate

Mage Masher
Crystal Shield
Triangle Hat
Bronze Armor
Defense Ring

Dash, Throw Stone, Heal, Wish
Shuriken, Bomb



Solomongrundy85
Female
Pisces
67
52
Oracle
Draw Out
MP Restore
Equip Knife
Retreat

Main Gauche

Leather Hat
Brigandine
Salty Rage

Blind, Spell Absorb, Zombie, Blind Rage, Dispel Magic, Sleep
Koutetsu, Heaven's Cloud, Muramasa

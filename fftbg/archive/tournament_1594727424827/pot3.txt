Final Bets: white - 5 bets for 2,022G (15.8%, x5.34); black - 11 bets for 10,795G (84.2%, x0.19)

white bets:
TheChainNerd: 1,000G (49.5%, 22,817G)
randgridr: 500G (24.7%, 19,295G)
getthemoneyz: 272G (13.5%, 1,272,865G)
ar_tactic: 150G (7.4%, 68,947G)
SephDarkheart: 100G (4.9%, 22,638G)

black bets:
ShintaroNayaka: 5,051G (46.8%, 10,103G)
dantayystv: 1,479G (13.7%, 1,479G)
BirbBrainsBot: 1,000G (9.3%, 141,167G)
twelfthrootoftwo: 876G (8.1%, 876G)
maakur_: 753G (7.0%, 753G)
dogsandcatsand: 536G (5.0%, 1,367G)
Zenmaza: 500G (4.6%, 5,497G)
Mysteriousdewd: 200G (1.9%, 6,035G)
datadrivenbot: 200G (1.9%, 57,434G)
Ring_Wyrm: 100G (0.9%, 17,137G)
JumbocactuarX27: 100G (0.9%, 29,188G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dantetouhou
Male
Cancer
67
51
Chemist
Summon Magic
Dragon Spirit
Martial Arts
Move+2



Red Hood
Earth Clothes
Leather Mantle

X-Potion, Antidote, Phoenix Down
Moogle, Ramuh, Titan, Carbunkle, Salamander, Silf, Fairy



ZZ Yoshi
Male
Scorpio
58
75
Lancer
Steal
Sunken State
Doublehand
Swim

Partisan

Genji Helmet
Gold Armor
Red Shoes

Level Jump4, Vertical Jump2
Steal Heart, Steal Weapon, Arm Aim



SeedSC
Male
Pisces
41
79
Archer
Black Magic
Catch
Doublehand
Waterbreathing

Cross Bow

Green Beret
Leather Outfit
Cursed Ring

Charge+7, Charge+10
Fire 2, Bolt, Ice, Ice 2, Ice 4



Sairentozon7
Female
Aquarius
59
79
Thief
Time Magic
Parry
Short Charge
Ignore Height

Main Gauche

Headgear
Leather Outfit
Defense Armlet

Gil Taking, Steal Armor, Arm Aim
Haste, Haste 2, Slow, Stabilize Time

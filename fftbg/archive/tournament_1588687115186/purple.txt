Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CosmicTactician
Female
Sagittarius
68
75
Ninja
White Magic
Brave Up
Equip Knife
Move+2

Short Edge
Koga Knife
Black Hood
Adaman Vest
Genji Gauntlet

Shuriken, Bomb, Staff
Cure 4, Raise, Raise 2, Regen, Protect, Shell, Esuna



Arkreaver
Female
Gemini
41
60
Samurai
White Magic
Parry
Magic Attack UP
Move+1

Koutetsu Knife

Circlet
Black Robe
Germinas Boots

Koutetsu, Kiyomori, Kikuichimoji
Cure 2, Cure 4, Raise, Raise 2, Regen, Protect 2, Esuna



Z32o
Male
Serpentarius
73
52
Knight
Basic Skill
Damage Split
Concentrate
Jump+2

Defender
Gold Shield
Iron Helmet
Light Robe
Leather Mantle

Head Break, Mind Break, Justice Sword, Surging Sword
Heal, Tickle, Cheer Up, Wish, Scream



FriendSquirrel
Female
Gemini
63
53
Dancer
Throw
HP Restore
Equip Armor
Swim

Ryozan Silk

Bronze Helmet
Crystal Mail
Germinas Boots

Witch Hunt, Slow Dance, Polka Polka, Disillusion, Last Dance, Nether Demon
Shuriken, Bomb

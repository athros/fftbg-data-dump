Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zagorsek
Female
Gemini
78
50
Wizard
Steal
Hamedo
Magic Defense UP
Move+3

Cultist Dagger

Holy Miter
Silk Robe
Elf Mantle

Fire 4, Bolt, Bolt 2, Ice 2
Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Leg Aim



Loveyouallfriends
Male
Aquarius
55
78
Monk
Talk Skill
Speed Save
Attack UP
Move-HP Up



Holy Miter
Mystic Vest
Magic Ring

Spin Fist, Pummel, Wave Fist, Revive
Invitation, Persuade, Preach, Insult, Negotiate, Refute



ColetteMSLP
Male
Sagittarius
68
70
Priest
Jump
Parry
Equip Knife
Move-HP Up

Air Knife

Flash Hat
Rubber Costume
Defense Ring

Raise, Raise 2, Reraise, Regen, Protect 2, Shell 2
Level Jump5, Vertical Jump8



StealthModeLocke
Female
Pisces
60
71
Dancer
Item
Catch
Doublehand
Move+2

Cute Bag

Headgear
Wizard Outfit
Battle Boots

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Last Dance, Dragon Pit
Potion, Ether, Hi-Ether, Antidote, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Galkife
Male
Gemini
59
57
Archer
Elemental
Damage Split
Doublehand
Jump+1

Stone Gun

Golden Hairpin
Judo Outfit
Magic Ring

Charge+4, Charge+7
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand



Superdevon1
Female
Capricorn
52
59
Samurai
Battle Skill
Meatbone Slash
Maintenance
Ignore Terrain

Kiyomori

Mythril Helmet
Robe of Lords
Red Shoes

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji
Armor Break, Magic Break, Night Sword



Choco Joe
Female
Taurus
53
59
Ninja
Steal
HP Restore
Equip Polearm
Teleport

Partisan
Javelin
Holy Miter
Clothes
Genji Gauntlet

Shuriken, Bomb, Knife, Dictionary
Steal Heart, Steal Armor, Steal Accessory, Arm Aim



OpHendoslice
Male
Scorpio
58
40
Knight
Sing
Mana Shield
Martial Arts
Levitate

Defender
Platinum Shield
Circlet
Reflect Mail
Jade Armlet

Shield Break, Speed Break
Life Song, Cheer Song, Magic Song, Last Song, Hydra Pit

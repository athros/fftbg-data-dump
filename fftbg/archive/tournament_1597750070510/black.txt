Player: !Black
Team: Black Team
Palettes: Black/Red



Brenogarwin
Female
Leo
57
58
Lancer
Dance
Regenerator
Equip Axe
Move+2

Flail
Bronze Shield
Iron Helmet
Chain Mail
N-Kai Armlet

Level Jump4, Vertical Jump7
Disillusion, Void Storage



ForagerCats
Female
Libra
73
58
Priest
Jump
Parry
Halve MP
Jump+3

Oak Staff

Barette
Chameleon Robe
Wizard Mantle

Cure 2, Cure 4, Raise, Protect, Protect 2, Wall, Esuna
Level Jump8, Vertical Jump2



SephDarkheart
Male
Gemini
41
63
Time Mage
Steal
Catch
Dual Wield
Retreat

Ivory Rod
Iron Fan
Red Hood
Linen Robe
Reflect Ring

Haste, Haste 2, Slow, Float, Quick, Meteor
Steal Armor, Steal Shield, Steal Weapon



Gorgewall
Female
Aquarius
55
41
Calculator
Yin Yang Magic
Mana Shield
Equip Sword
Waterbreathing

Ragnarok

Black Hood
Secret Clothes
Battle Boots

CT, Prime Number, 5
Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep

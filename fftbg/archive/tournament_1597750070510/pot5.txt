Final Bets: red - 7 bets for 3,911G (48.7%, x1.05); yellow - 5 bets for 4,122G (51.3%, x0.95)

red bets:
Lydian_C: 1,200G (30.7%, 12,327G)
reddwind_: 1,000G (25.6%, 46,538G)
ko2q: 706G (18.1%, 706G)
ForagerCats: 365G (9.3%, 365G)
WhiteTigress: 300G (7.7%, 2,818G)
datadrivenbot: 200G (5.1%, 70,223G)
AllInBot: 140G (3.6%, 140G)

yellow bets:
Bryon_W: 2,000G (48.5%, 8,860G)
BirbBrainsBot: 1,000G (24.3%, 22,734G)
dogsandcatsand: 636G (15.4%, 4,616G)
E_Ballard: 386G (9.4%, 773G)
MinBetBot: 100G (2.4%, 6,967G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



EnemyController
Male
Cancer
64
77
Ninja
Talk Skill
Regenerator
Equip Armor
Move-MP Up

Mage Masher
Orichalcum
Triangle Hat
Silk Robe
Cursed Ring

Shuriken, Bomb
Threaten, Preach, Negotiate, Mimic Daravon, Refute



DesertWooder
Female
Leo
61
72
Geomancer
Draw Out
Parry
Equip Bow
Jump+3

Lightning Bow

Green Beret
Wizard Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bizen Boat, Murasame



Drusiform
Male
Leo
74
47
Thief
Black Magic
Speed Save
Equip Axe
Ignore Terrain

Flame Whip

Black Hood
Power Sleeve
N-Kai Armlet

Gil Taking, Steal Shield, Arm Aim
Fire, Fire 2, Fire 3, Bolt 3, Ice 2, Death



Maakur
Female
Leo
42
66
Dancer
Item
Speed Save
Beastmaster
Teleport

Ryozan Silk

Headgear
Wizard Robe
Diamond Armlet

Wiznaibus, Slow Dance, Nameless Dance
Potion, Hi-Potion, Antidote, Eye Drop, Remedy

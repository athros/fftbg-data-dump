Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Volgrathemoose
Male
Sagittarius
75
45
Bard
Draw Out
Sunken State
Short Charge
Levitate

Lightning Bow

Black Hood
Black Costume
Bracer

Angel Song, Cheer Song, Last Song
Koutetsu, Bizen Boat, Murasame, Kikuichimoji



K1ngofthechill
Female
Virgo
79
81
Samurai
Time Magic
Auto Potion
Long Status
Waterwalking

Asura Knife

Genji Helmet
Genji Armor
Magic Gauntlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Haste, Haste 2, Slow 2, Stop, Immobilize, Quick, Stabilize Time



PatSouI
Female
Virgo
46
76
Lancer
Draw Out
Parry
Equip Knife
Move-HP Up

Blind Knife
Escutcheon
Gold Helmet
Mythril Armor
Sprint Shoes

Level Jump4, Vertical Jump2
Asura, Murasame, Kikuichimoji



Basmal
Male
Cancer
56
46
Monk
Battle Skill
Caution
Equip Gun
Lava Walking

Bestiary

Triangle Hat
Earth Clothes
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Magic Break, Speed Break, Power Break, Justice Sword, Night Sword

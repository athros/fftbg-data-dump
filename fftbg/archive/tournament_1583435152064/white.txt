Player: !White
Team: White Team
Palettes: White/Blue



Meta Five
Female
Taurus
75
71
Geomancer
Summon Magic
Brave Up
Attack UP
Jump+3

Battle Axe
Mythril Shield
Golden Hairpin
Silk Robe
Reflect Ring

Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Odin



Leakimiko
Female
Virgo
69
75
Summoner
Elemental
Distribute
Short Charge
Ignore Height

Oak Staff

Feather Hat
Linen Robe
Genji Gauntlet

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Silf, Fairy
Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



Jigglefluffenstuff
Male
Aries
75
75
Monk
Charge
HP Restore
Secret Hunt
Ignore Terrain



Thief Hat
Power Sleeve
Battle Boots

Spin Fist, Earth Slash, Purification, Chakra, Revive
Charge+20



GameCorps
Female
Leo
60
61
Wizard
White Magic
Absorb Used MP
Magic Attack UP
Move-MP Up

Mythril Knife

Golden Hairpin
Leather Outfit
N-Kai Armlet

Fire 3, Bolt 3, Bolt 4, Ice, Ice 3, Flare
Cure 2, Cure 3, Raise, Reraise, Protect 2, Shell, Wall, Esuna, Holy

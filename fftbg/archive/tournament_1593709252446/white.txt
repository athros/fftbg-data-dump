Player: !White
Team: White Team
Palettes: White/Blue



Gelwain
Male
Scorpio
59
60
Ninja
Charge
Catch
Equip Armor
Jump+1

Short Edge
Flail
Cachusha
Black Robe
Wizard Mantle

Shuriken, Bomb, Knife, Staff
Charge+1, Charge+7, Charge+20



Phi Sig
Male
Aries
64
49
Knight
Talk Skill
Blade Grasp
Equip Bow
Fly

Long Bow
Gold Shield
Mythril Helmet
Reflect Mail
Battle Boots

Head Break, Armor Break, Speed Break, Power Break, Mind Break, Surging Sword
Threaten, Preach, Death Sentence, Rehabilitate



Silentkaster
Monster
Taurus
66
37
Ultima Demon










OtherBrand
Female
Virgo
70
71
Ninja
Time Magic
Parry
Equip Knife
Jump+2

Ice Rod
Zorlin Shape
Green Beret
Adaman Vest
Defense Ring

Shuriken, Knife, Hammer
Haste, Stop, Reflect, Demi, Stabilize Time, Meteor, Galaxy Stop

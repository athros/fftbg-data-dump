Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Redamaford
Female
Aquarius
59
63
Ninja
Jump
Faith Up
Halve MP
Move+3

Air Knife
Assassin Dagger
Golden Hairpin
Adaman Vest
Feather Mantle

Sword, Hammer, Spear, Stick, Wand
Level Jump8, Vertical Jump8



Lodrak
Monster
Gemini
79
41
Red Dragon










Heatherydee
Male
Virgo
59
67
Chemist
Draw Out
Brave Up
Concentrate
Jump+3

Star Bag

Green Beret
Brigandine
Power Wrist

Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Soft
Asura, Murasame, Heaven's Cloud



Jimmypoopins
Female
Sagittarius
46
53
Archer
Battle Skill
Distribute
Sicken
Levitate

Night Killer
Aegis Shield
Feather Hat
Leather Vest
Setiemson

Charge+4
Head Break, Armor Break, Weapon Break, Magic Break, Justice Sword, Dark Sword

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Boosted420
Female
Gemini
40
43
Mime

Faith Save
Equip Shield
Retreat


Platinum Shield
Feather Hat
Clothes
Elf Mantle

Mimic




Bongomon7
Male
Cancer
60
80
Mediator
Black Magic
Counter Flood
Dual Wield
Move+2

Stone Gun
Blaze Gun
Thief Hat
Linen Robe
Leather Mantle

Invitation, Persuade, Preach, Mimic Daravon, Refute
Fire, Fire 3, Bolt, Bolt 2, Ice, Ice 3, Ice 4, Empower, Frog, Death



Actual JP
Female
Libra
69
49
Lancer
Black Magic
Speed Save
Sicken
Lava Walking

Mythril Spear
Aegis Shield
Bronze Helmet
White Robe
N-Kai Armlet

Level Jump8, Vertical Jump6
Fire, Bolt 4, Ice 2



J2DaBibbles
Male
Libra
41
51
Archer
Sing
MP Restore
Dual Wield
Jump+2

Long Bow

Twist Headband
Mythril Vest
N-Kai Armlet

Charge+2, Charge+3, Charge+4, Charge+7, Charge+20
Life Song

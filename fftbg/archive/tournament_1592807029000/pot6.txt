Final Bets: white - 7 bets for 8,020G (42.0%, x1.38); brown - 12 bets for 11,061G (58.0%, x0.73)

white bets:
Mesmaster: 3,000G (37.4%, 259,883G)
Digitalsocrates: 2,000G (24.9%, 19,216G)
BirbBrainsBot: 1,000G (12.5%, 87,675G)
Evewho: 1,000G (12.5%, 7,272G)
TeaTime29: 500G (6.2%, 5,578G)
Furaime: 400G (5.0%, 2,044G)
Lydian_C: 120G (1.5%, 172,679G)

brown bets:
prince_rogers_nelson_: 3,330G (30.1%, 3,330G)
BStarTV: 2,000G (18.1%, 17,210G)
DuraiPapers: 1,500G (13.6%, 15,213G)
lowlf: 1,222G (11.0%, 76,996G)
helpimabug: 758G (6.9%, 758G)
superdevon1: 600G (5.4%, 2,812G)
gorgewall: 401G (3.6%, 12,040G)
elnobueno: 400G (3.6%, 800G)
Ross_from_Cali: 288G (2.6%, 288G)
getthemoneyz: 262G (2.4%, 1,015,636G)
ar_tactic: 200G (1.8%, 54,643G)
datadrivenbot: 100G (0.9%, 46,031G)

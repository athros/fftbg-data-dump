Final Bets: red - 8 bets for 8,582G (54.3%, x0.84); yellow - 10 bets for 7,209G (45.7%, x1.19)

red bets:
Lydian_C: 4,200G (48.9%, 169,151G)
EnemyController: 1,234G (14.4%, 1,238,766G)
prince_rogers_nelson_: 1,000G (11.7%, 2,490G)
BirbBrainsBot: 1,000G (11.7%, 86,835G)
superdevon1: 600G (7.0%, 2,308G)
helpimabug: 412G (4.8%, 412G)
ar_tactic: 100G (1.2%, 54,559G)
getthemoneyz: 36G (0.4%, 1,015,606G)

yellow bets:
Digitalsocrates: 3,000G (41.6%, 22,216G)
lowlf: 1,222G (17.0%, 78,218G)
Ross_from_Cali: 586G (8.1%, 586G)
TeaTime29: 500G (6.9%, 6,078G)
DuraiPapers: 500G (6.9%, 15,713G)
reinoe: 500G (6.9%, 10,898G)
gorgewall: 401G (5.6%, 12,441G)
Evewho: 200G (2.8%, 7,472G)
elnobueno: 200G (2.8%, 1,000G)
datadrivenbot: 100G (1.4%, 46,131G)

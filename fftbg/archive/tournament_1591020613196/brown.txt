Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Powergems
Female
Leo
52
78
Thief
Talk Skill
Distribute
Equip Gun
Jump+1

Battle Folio

Black Hood
Power Sleeve
Vanish Mantle

Steal Heart, Steal Weapon, Steal Status, Leg Aim
Death Sentence, Insult, Negotiate, Refute



LDSkinny
Female
Leo
74
58
Oracle
Throw
Catch
Equip Knife
Levitate

Hidden Knife

Golden Hairpin
Black Robe
Cherche

Blind, Poison, Life Drain, Pray Faith, Zombie, Silence Song, Confusion Song, Sleep, Petrify, Dark Holy
Shuriken, Sword



Cryptopsy70
Male
Aquarius
57
49
Monk
Time Magic
Damage Split
Doublehand
Ignore Terrain



Triangle Hat
Mythril Vest
Reflect Ring

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Reflect, Quick, Demi, Stabilize Time



DudeMonkey77
Male
Capricorn
45
81
Samurai
Punch Art
Dragon Spirit
Equip Shield
Jump+1

Bizen Boat
Mythril Shield
Leather Helmet
Gold Armor
Bracer

Bizen Boat, Heaven's Cloud, Kiyomori
Wave Fist, Earth Slash, Purification, Revive

Player: !White
Team: White Team
Palettes: White/Blue



Gdreaper1
Monster
Scorpio
56
67
Holy Dragon










L2 Sentinel
Female
Libra
54
63
Lancer
Elemental
Parry
Equip Bow
Move+2

Hunting Bow
Diamond Shield
Bronze Helmet
Plate Mail
Defense Armlet

Level Jump4, Vertical Jump6
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm



Bpc2163
Male
Cancer
76
53
Time Mage
Black Magic
Counter
Defend
Levitate

Bestiary

Holy Miter
Mystic Vest
Red Shoes

Haste, Haste 2, Slow, Stop, Quick, Demi
Fire 3, Bolt, Bolt 2, Bolt 3, Ice 4, Empower, Death, Flare



Victoriolue
Male
Leo
46
59
Thief
Throw
Sunken State
Defend
Ignore Height

Sasuke Knife

Black Hood
Clothes
Defense Ring

Steal Heart
Shuriken, Staff, Axe, Stick

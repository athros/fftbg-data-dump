Player: !Green
Team: Green Team
Palettes: Green/White



Klednar21
Female
Scorpio
37
75
Lancer
Item
Meatbone Slash
Maintenance
Move+1

Ivory Rod
Aegis Shield
Iron Helmet
Linen Cuirass
Defense Ring

Level Jump4, Vertical Jump2
Hi-Potion, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down



Zxpr0jk
Male
Taurus
69
68
Bard
Punch Art
Distribute
Long Status
Retreat

Fairy Harp

Ribbon
Earth Clothes
Magic Gauntlet

Life Song, Battle Song, Nameless Song, Diamond Blade, Hydra Pit
Purification, Chakra



StreamRocket
Female
Libra
58
55
Knight
Item
Critical Quick
Concentrate
Retreat

Ice Brand
Round Shield
Crystal Helmet
Carabini Mail
Reflect Ring

Armor Break, Shield Break, Speed Break, Power Break
Potion, Hi-Potion, X-Potion, Eye Drop, Echo Grass, Soft, Holy Water



Roqqqpsi
Female
Scorpio
52
71
Squire
Summon Magic
Blade Grasp
Equip Gun
Ignore Terrain

Blaze Gun
Round Shield
Triangle Hat
Adaman Vest
Angel Ring

Accumulate, Fury, Wish
Moogle, Ifrit, Titan, Salamander, Fairy, Lich

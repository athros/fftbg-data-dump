Player: !zChamp
Team: Champion Team
Palettes: Black/Red



CassiePhoenix
Female
Sagittarius
70
44
Samurai
Time Magic
Meatbone Slash
Martial Arts
Jump+1

Dragon Whisker

Gold Helmet
Genji Armor
Rubber Shoes

Asura, Koutetsu, Murasame, Muramasa
Haste 2, Demi, Demi 2, Meteor



RaIshtar
Male
Serpentarius
58
54
Samurai
Item
Auto Potion
Equip Bow
Swim

Poison Bow

Platinum Helmet
Maximillian
Reflect Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Chirijiraden
Potion, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Kyune
Female
Taurus
54
77
Mime

Mana Shield
Equip Armor
Waterwalking



Crystal Helmet
Reflect Mail
Feather Mantle

Mimic




OneHundredFists
Male
Scorpio
71
45
Squire
Punch Art
Absorb Used MP
Defense UP
Move-MP Up

Slasher
Mythril Shield
Headgear
Chain Vest
Battle Boots

Throw Stone, Tickle, Wish
Spin Fist, Pummel, Purification, Seal Evil

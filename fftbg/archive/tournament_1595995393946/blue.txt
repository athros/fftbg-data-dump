Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Miku Shikhu
Male
Scorpio
53
68
Samurai
Sing
Parry
Equip Knife
Teleport

Thunder Rod

Barbuta
Gold Armor
Leather Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Angel Song, Sky Demon



Sairentozon7
Female
Scorpio
71
50
Knight
Item
Catch
Equip Polearm
Jump+1

Spear
Bronze Shield
Bronze Helmet
Chameleon Robe
Battle Boots

Head Break, Speed Break, Dark Sword, Explosion Sword
Hi-Potion, Antidote, Phoenix Down



BlackfyreRoy
Female
Taurus
64
45
Knight
Steal
Meatbone Slash
Equip Axe
Retreat

Flame Whip
Escutcheon
Circlet
White Robe
Magic Ring

Weapon Break, Speed Break, Mind Break, Night Sword
Gil Taking, Steal Helmet, Steal Armor, Steal Shield



Mrfripps
Male
Cancer
73
57
Mediator
Battle Skill
Parry
Equip Gun
Move-MP Up

Ramia Harp

Black Hood
Wizard Outfit
Bracer

Invitation, Persuade, Praise, Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate
Shield Break, Magic Break, Speed Break

Player: !Red
Team: Red Team
Palettes: Red/Brown



Flacococo
Female
Virgo
68
38
Dancer
Black Magic
Meatbone Slash
Dual Wield
Swim

Star Bag
Hydra Bag
Black Hood
Clothes
Red Shoes

Witch Hunt, Polka Polka, Nameless Dance, Last Dance
Bolt, Bolt 2, Death



DudeMonkey77
Male
Pisces
56
67
Calculator
Yin Yang Magic
Hamedo
Equip Polearm
Fly

Partisan

Cachusha
Brigandine
Bracer

CT, 4, 3
Poison, Life Drain, Pray Faith, Silence Song, Paralyze, Dark Holy



ThreeMileIsland
Female
Capricorn
45
64
Wizard
Summon Magic
Parry
Doublehand
Fly

Dragon Rod

Feather Hat
White Robe
Magic Ring

Fire, Fire 3, Bolt, Ice, Ice 2, Ice 4
Moogle, Shiva, Ifrit, Titan, Fairy



Brokenknight201
Male
Cancer
67
58
Knight
Punch Art
Speed Save
Sicken
Teleport

Defender
Bronze Shield
Cross Helmet
Mythril Armor
Cherche

Head Break, Armor Break, Shield Break, Speed Break, Power Break, Justice Sword
Spin Fist, Pummel, Secret Fist, Purification, Seal Evil

Final Bets: red - 13 bets for 8,510G (69.2%, x0.45); white - 7 bets for 3,793G (30.8%, x2.24)

red bets:
poGpopE: 2,173G (25.5%, 2,173G)
prince_rogers_nelson_: 1,304G (15.3%, 1,304G)
HaateXIII: 1,040G (12.2%, 1,040G)
douchetron: 793G (9.3%, 793G)
twelfthrootoftwo: 688G (8.1%, 688G)
slotheye17: 588G (6.9%, 588G)
ThreeMileIsland: 500G (5.9%, 10,857G)
BoneMiser: 463G (5.4%, 463G)
mrpickins: 232G (2.7%, 4,987G)
MLWebz0r: 228G (2.7%, 11,206G)
gorgewall: 201G (2.4%, 7,465G)
datadrivenbot: 200G (2.4%, 44,314G)
ribbiks: 100G (1.2%, 8,941G)

white bets:
Belkra: 1,000G (26.4%, 11,841G)
BirbBrainsBot: 1,000G (26.4%, 192,329G)
Chambs12: 609G (16.1%, 2,030G)
Kronikle: 464G (12.2%, 3,093G)
getthemoneyz: 264G (7.0%, 1,426,984G)
Hirameki85: 256G (6.7%, 12,176G)
AllInBot: 200G (5.3%, 200G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



LivingHitokiri
Female
Gemini
72
74
Time Mage
Battle Skill
Dragon Spirit
Concentrate
Jump+1

Bestiary

Flash Hat
Clothes
Cursed Ring

Stop, Immobilize, Float, Quick, Demi, Stabilize Time
Armor Break, Magic Break, Speed Break, Power Break



Miyokari
Female
Cancer
44
46
Ninja
Basic Skill
Counter Magic
Defend
Swim

Dagger
Blind Knife
Twist Headband
Leather Outfit
Spike Shoes

Shuriken, Bomb, Hammer, Staff
Accumulate, Throw Stone, Tickle, Scream



Potgodtopdog
Monster
Aquarius
53
76
Explosive










Bahumat989
Male
Capricorn
55
76
Geomancer
Punch Art
Mana Shield
Halve MP
Move+3

Asura Knife
Genji Shield
Golden Hairpin
Rubber Costume
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification

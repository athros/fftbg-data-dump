Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nizaha
Female
Virgo
60
60
Summoner
Steal
Dragon Spirit
Short Charge
Move-HP Up

Thunder Rod

Green Beret
Black Robe
N-Kai Armlet

Shiva, Ifrit, Golem, Bahamut, Odin, Leviathan, Fairy
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim



Twelfthrootoftwo
Female
Gemini
60
46
Time Mage
Punch Art
Parry
Beastmaster
Lava Walking

Wizard Staff

Black Hood
Mystic Vest
Rubber Shoes

Haste, Slow 2, Stop, Float, Reflect, Demi 2, Stabilize Time
Earth Slash, Purification



Grandlanzer
Female
Scorpio
53
80
Dancer
Elemental
MP Restore
Magic Attack UP
Waterbreathing

Star Bag

Golden Hairpin
Silk Robe
Reflect Ring

Slow Dance, Nameless Dance, Last Dance
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Foofermoofer
Male
Leo
59
41
Summoner
Draw Out
Distribute
Defend
Ignore Terrain

Wizard Rod

Feather Hat
Wizard Robe
Reflect Ring

Moogle, Carbunkle, Lich
Bizen Boat, Heaven's Cloud

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Miku Shikhu
Female
Capricorn
43
66
Oracle
Item
Faith Save
Equip Bow
Move-HP Up

Lightning Bow

Feather Hat
Wizard Robe
Wizard Mantle

Spell Absorb, Pray Faith, Doubt Faith, Dispel Magic, Paralyze, Dark Holy
Potion, X-Potion, Eye Drop, Echo Grass, Remedy, Phoenix Down



Killth3kid
Male
Pisces
62
46
Samurai
Punch Art
Distribute
Doublehand
Move+1

Kiyomori

Gold Helmet
Chain Mail
Diamond Armlet

Koutetsu, Murasame, Kiyomori, Muramasa
Pummel, Purification, Revive



Nohurty
Male
Virgo
42
61
Wizard
Basic Skill
Counter Tackle
Halve MP
Lava Walking

Rod

Holy Miter
Linen Robe
Wizard Mantle

Fire, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 4, Frog
Throw Stone, Tickle



ScurvyMitch
Male
Capricorn
43
68
Lancer
Talk Skill
Regenerator
Long Status
Move-MP Up

Iron Fan
Escutcheon
Platinum Helmet
Linen Cuirass
Feather Mantle

Level Jump2, Vertical Jump8
Persuade, Preach, Rehabilitate

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Twelfthrootoftwo
Male
Taurus
47
47
Lancer
Battle Skill
Abandon
Doublehand
Fly

Cypress Rod

Diamond Helmet
Maximillian
Jade Armlet

Level Jump5, Vertical Jump5
Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword, Night Sword



Neerrm
Male
Aries
46
52
Mime

Regenerator
Sicken
Lava Walking



Triangle Hat
Crystal Mail
Bracer

Mimic




Evewho
Female
Leo
54
59
Knight
Dance
Absorb Used MP
Beastmaster
Move+3

Mythril Sword
Ice Shield
Circlet
Wizard Robe
Feather Mantle

Head Break, Armor Break, Magic Break, Justice Sword, Night Sword
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Last Dance, Obsidian Blade, Nether Demon



Shaxarok
Male
Cancer
59
47
Monk
White Magic
Critical Quick
Defense UP
Jump+1



Twist Headband
Earth Clothes
Magic Ring

Pummel, Secret Fist, Purification, Revive
Cure 4, Raise, Reraise, Shell 2, Wall, Esuna, Holy

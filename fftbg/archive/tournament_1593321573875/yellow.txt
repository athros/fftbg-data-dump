Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Humble Fabio
Female
Aries
77
80
Monk
Item
Speed Save
Equip Gun
Move+2

Papyrus Codex

Holy Miter
Clothes
Reflect Ring

Spin Fist, Earth Slash, Secret Fist, Chakra, Revive, Seal Evil
Potion, Hi-Potion, Eye Drop, Soft, Remedy, Phoenix Down



Oreo Pizza
Male
Taurus
77
75
Mediator
Steal
Parry
Concentrate
Waterbreathing

Glacier Gun

Headgear
Chameleon Robe
Rubber Shoes

Threaten, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Steal Heart, Steal Shield, Steal Weapon, Steal Status



OneHundredFists
Male
Taurus
59
76
Time Mage
Battle Skill
Damage Split
Doublehand
Teleport

Musk Rod

Flash Hat
Black Robe
Defense Armlet

Haste, Slow 2, Stop, Immobilize, Quick
Head Break, Weapon Break, Justice Sword, Night Sword



Sairentozon7
Female
Libra
74
66
Dancer
Summon Magic
Abandon
Attack UP
Lava Walking

Ryozan Silk

Black Hood
Chain Vest
Rubber Shoes

Wiznaibus, Polka Polka, Last Dance
Shiva, Ifrit, Titan, Silf

Player: !White
Team: White Team
Palettes: White/Blue



Bad1dea
Male
Virgo
69
70
Thief
Sing
Brave Up
Maintenance
Teleport

Broad Sword

Leather Hat
Leather Outfit
Defense Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Steal Status, Leg Aim
Angel Song, Life Song, Cheer Song



Zachara
Male
Virgo
69
43
Lancer
Throw
Auto Potion
Attack UP
Waterwalking

Mythril Spear
Round Shield
Iron Helmet
Diamond Armor
Angel Ring

Level Jump8, Vertical Jump8
Shuriken, Knife



ELzukkor
Male
Aries
47
70
Lancer
White Magic
Caution
Dual Wield
Waterbreathing

Cypress Rod
Gungnir
Platinum Helmet
Chain Mail
Rubber Shoes

Level Jump5, Vertical Jump5
Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Esuna



RRazza
Female
Gemini
60
65
Dancer
Jump
Regenerator
Doublehand
Ignore Height

Star Bag

Twist Headband
White Robe
Bracer

Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Obsidian Blade, Void Storage
Level Jump3, Vertical Jump7

Player: !Black
Team: Black Team
Palettes: Black/Red



Ruleof5
Female
Aquarius
51
51
Samurai
Steal
Parry
Magic Attack UP
Swim

Kikuichimoji

Mythril Helmet
Diamond Armor
Diamond Armlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Steal Heart, Steal Shield, Steal Status



VolgraTheMoose
Male
Taurus
70
39
Archer
White Magic
Regenerator
Doublehand
Retreat

Snipe Bow

Crystal Helmet
Chain Vest
Leather Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Raise, Regen, Protect, Protect 2, Shell 2, Esuna, Holy



Pplvee1
Male
Cancer
66
57
Knight
Draw Out
Mana Shield
Defend
Waterwalking

Slasher
Venetian Shield
Platinum Helmet
Black Robe
Germinas Boots

Magic Break, Speed Break, Mind Break, Dark Sword
Asura, Koutetsu, Murasame



Reddwind
Male
Libra
44
69
Knight
Sing
Earplug
Halve MP
Jump+1

Excalibur
Aegis Shield
Genji Helmet
Gold Armor
Reflect Ring

Mind Break
Life Song, Nameless Song, Diamond Blade

Player: !Red
Team: Red Team
Palettes: Red/Brown



TheChainNerd
Male
Cancer
79
68
Priest
Math Skill
Counter Flood
Sicken
Move-HP Up

Flame Whip

Leather Hat
Black Robe
Reflect Ring

Cure, Cure 3, Raise, Protect 2, Shell, Shell 2, Esuna
Height, 5



Run With Stone GUNs
Male
Gemini
76
73
Mediator
Sing
Counter Magic
Dual Wield
Ignore Terrain

Mythril Gun
Glacier Gun
Red Hood
Wizard Robe
Genji Gauntlet

Invitation, Praise, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon
Angel Song, Cheer Song



Mpghappiness
Male
Sagittarius
70
75
Samurai
Throw
Auto Potion
Equip Armor
Move+2

Murasame

Cross Helmet
Black Costume
Feather Boots

Murasame, Kiyomori
Shuriken, Knife



Grininda
Female
Virgo
63
50
Chemist
Battle Skill
Speed Save
Equip Axe
Move+3

Battle Axe

Feather Hat
Leather Outfit
Magic Ring

Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Phoenix Down
Head Break, Weapon Break, Speed Break, Mind Break, Night Sword

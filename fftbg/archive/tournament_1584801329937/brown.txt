Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Hales Bopp It
Female
Libra
63
46
Ninja
Summon Magic
Sunken State
Short Charge
Move+2

Koga Knife
Kunai
Twist Headband
Wizard Outfit
Battle Boots

Shuriken, Spear
Moogle, Shiva, Titan, Carbunkle, Leviathan, Salamander, Silf, Fairy



Rnark
Male
Scorpio
71
40
Chemist
Talk Skill
Distribute
Magic Attack UP
Levitate

Star Bag

Headgear
Wizard Outfit
Dracula Mantle

Potion, X-Potion, Ether, Hi-Ether, Phoenix Down
Preach, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate



Dexef
Female
Virgo
38
79
Calculator
Mighty Sword
MA Save
Equip Armor
Ignore Height

Dragon Rod

Flash Hat
Mythril Vest
Jade Armlet

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Oogthecaveman
Male
Virgo
70
41
Lancer
Elemental
Regenerator
Equip Knife
Jump+1

Poison Rod
Venetian Shield
Barbuta
Platinum Armor
Feather Boots

Level Jump2, Vertical Jump7
Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball

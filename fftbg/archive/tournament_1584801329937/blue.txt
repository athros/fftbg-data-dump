Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lanshaft
Male
Libra
72
65
Squire
Charge
Counter Magic
Sicken
Ignore Height

Slasher
Escutcheon
Crystal Helmet
Platinum Armor
Magic Gauntlet

Dash, Tickle, Wish
Charge+1, Charge+2, Charge+5



Grininda
Male
Libra
47
72
Ninja
Basic Skill
MA Save
Equip Gun
Retreat

Papyrus Codex
Papyrus Codex
Red Hood
Clothes
Jade Armlet

Shuriken, Bomb, Hammer, Axe
Dash, Heal, Cheer Up, Wish



Lordminsc
Female
Gemini
67
78
Calculator
White Magic
Mana Shield
Short Status
Move-HP Up

Bestiary

Flash Hat
Power Sleeve
Power Wrist

CT, Height, 5, 4, 3
Cure 4, Raise 2, Regen, Wall, Esuna



Silentperogy
Female
Leo
50
74
Thief
Battle Skill
MA Save
Equip Shield
Ignore Terrain

Dagger
Diamond Shield
Headgear
Leather Outfit
Magic Ring

Gil Taking, Steal Heart, Steal Armor, Leg Aim
Armor Break, Shield Break, Stasis Sword

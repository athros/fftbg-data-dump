Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mesmaster
Male
Capricorn
70
55
Knight
Summon Magic
Earplug
Defense UP
Fly

Battle Axe
Escutcheon
Circlet
Leather Armor
Feather Boots

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Stasis Sword, Explosion Sword
Moogle, Shiva, Golem, Carbunkle, Odin, Leviathan, Fairy



FriendSquirrel
Female
Sagittarius
64
51
Dancer
Talk Skill
Distribute
Beastmaster
Fly

Cultist Dagger

Green Beret
Black Robe
Bracer

Disillusion, Void Storage
Persuade, Preach, Negotiate, Rehabilitate



Bruubarg
Female
Virgo
48
66
Mime

HP Restore
Equip Armor
Ignore Height



Leather Hat
Plate Mail
N-Kai Armlet

Mimic




Waterwatereverywhere
Male
Aries
51
65
Lancer
White Magic
Auto Potion
Short Charge
Jump+3

Octagon Rod
Platinum Shield
Iron Helmet
Genji Armor
Small Mantle

Level Jump4, Vertical Jump8
Cure 2, Raise, Protect, Shell 2, Esuna

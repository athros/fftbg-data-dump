Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Sankvtw
Monster
Aquarius
62
46
Draugr










Galkife
Female
Sagittarius
59
47
Chemist
Talk Skill
Counter Flood
Defense UP
Move+2

Blind Knife

Holy Miter
Judo Outfit
Cursed Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Death Sentence, Mimic Daravon, Refute, Rehabilitate



ALY327
Male
Aries
77
55
Wizard
Item
Catch
Doublehand
Move+1

Mythril Knife

Headgear
Judo Outfit
108 Gems

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Empower, Flare
Potion, Hi-Potion, Ether, Antidote, Echo Grass, Phoenix Down



Stalskegg
Male
Scorpio
60
80
Bard
Talk Skill
Mana Shield
Dual Wield
Move+2

Bloody Strings
Ramia Harp
Red Hood
Platinum Armor
Battle Boots

Life Song, Magic Song
Invitation, Praise, Preach, Insult, Negotiate, Mimic Daravon

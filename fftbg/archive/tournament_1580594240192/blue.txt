Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powermhero
Male
Pisces
45
44
Archer
Item
Auto Potion
Short Charge
Move+2

Mythril Gun
Diamond Shield
Green Beret
Earth Clothes
Setiemson

Charge+3, Charge+4, Charge+5, Charge+10
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Soft, Phoenix Down



Yamatopi
Female
Gemini
46
57
Calculator
Bone Skill
HP Restore
Equip Bow
Ignore Height

Silver Bow

Thief Hat
Wizard Robe
Diamond Armlet

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Zunath
Female
Virgo
68
62
Geomancer
Summon Magic
Counter
Long Status
Waterwalking

Long Sword
Gold Shield
Leather Hat
Light Robe
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Moogle, Shiva, Ramuh, Carbunkle, Odin, Fairy, Lich



Choboco
Female
Aquarius
47
49
Calculator
Minotaur Skill
Critical Quick
Maintenance
Move+3

Ice Rod

Leather Hat
Robe of Lords
Feather Boots

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power

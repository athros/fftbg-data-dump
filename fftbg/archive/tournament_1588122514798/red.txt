Player: !Red
Team: Red Team
Palettes: Red/Brown



ACSpree
Female
Cancer
54
56
Wizard
Punch Art
Sunken State
Equip Axe
Swim

Flail

Flash Hat
Silk Robe
108 Gems

Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4
Purification, Revive, Seal Evil



HaplessOne
Male
Aquarius
53
69
Ninja
White Magic
HP Restore
Halve MP
Waterbreathing

Short Edge
Ninja Edge
Leather Hat
Brigandine
Spike Shoes

Shuriken, Staff
Cure 2, Raise 2, Shell, Wall, Esuna, Holy



LivingHitokiri
Female
Libra
50
75
Squire
Charge
HP Restore
Sicken
Teleport

Flail
Flame Shield
Cachusha
Chain Vest
Spike Shoes

Dash, Heal, Tickle
Charge+3, Charge+4



KasugaiRoastedPeas
Male
Scorpio
60
52
Knight
Steal
Parry
Equip Sword
Move+3

Broad Sword
Mythril Shield
Bronze Helmet
Chameleon Robe
Angel Ring

Shield Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Steal Helmet, Steal Armor, Arm Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



WireLord
Female
Pisces
50
75
Mediator
Throw
Critical Quick
Dual Wield
Teleport

Mythril Gun
Blast Gun
Green Beret
Leather Outfit
Leather Mantle

Persuade, Threaten, Death Sentence, Negotiate
Shuriken, Bomb, Axe, Wand, Dictionary



Nachtkrieger
Male
Capricorn
50
42
Monk
Charge
Counter Magic
Attack UP
Move-HP Up



Headgear
Leather Outfit
Bracer

Wave Fist, Secret Fist, Purification, Chakra, Revive
Charge+1, Charge+2, Charge+4, Charge+5



Hyvi
Female
Leo
66
80
Time Mage
Punch Art
Counter Flood
Short Charge
Move+2

Gold Staff

Flash Hat
Adaman Vest
Wizard Mantle

Immobilize, Demi, Stabilize Time, Meteor
Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



SkylerBunny
Male
Sagittarius
69
67
Bard
Draw Out
Brave Up
Attack UP
Fly

Bloody Strings

Golden Hairpin
Leather Outfit
Sprint Shoes

Life Song, Battle Song, Magic Song, Sky Demon
Heaven's Cloud, Muramasa

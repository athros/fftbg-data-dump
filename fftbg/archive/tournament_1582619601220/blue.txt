Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Basmal
Male
Pisces
74
61
Mime

Brave Up
Martial Arts
Jump+2



Feather Hat
Clothes
Reflect Ring

Mimic




StealthedChaos
Male
Aries
41
74
Mediator
Item
Counter Magic
Magic Defense UP
Ignore Height

Romanda Gun

Golden Hairpin
Black Robe
Germinas Boots

Invitation, Refute
Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Mtmcl
Male
Taurus
48
59
Geomancer
Talk Skill
Arrow Guard
Dual Wield
Ignore Height

Panther Bag
Giant Axe
Holy Miter
Black Robe
Sprint Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Preach, Death Sentence, Negotiate, Mimic Daravon



Byrdturbo
Male
Scorpio
45
60
Mediator
Punch Art
Caution
Attack UP
Ignore Terrain

Mage Masher

Cachusha
Black Robe
Angel Ring

Invitation, Threaten, Preach, Rehabilitate
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive

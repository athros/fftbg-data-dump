Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Brokenknight201
Male
Gemini
64
73
Monk
Basic Skill
Earplug
Concentrate
Jump+2



Leather Hat
Secret Clothes
Bracer

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Seal Evil
Dash, Heal, Cheer Up



Nizaha
Male
Capricorn
69
42
Thief
Draw Out
HP Restore
Dual Wield
Ignore Height

Assassin Dagger
Coral Sword
Headgear
Chain Vest
Cursed Ring

Steal Heart, Steal Weapon, Arm Aim, Leg Aim
Asura, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji



DeathTaxesAndAnime
Female
Leo
47
63
Mediator
Time Magic
MP Restore
Long Status
Swim

Mythril Gun

Thief Hat
Power Sleeve
108 Gems

Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Insult, Negotiate, Refute
Haste, Haste 2, Slow 2, Immobilize, Float, Demi, Stabilize Time



Lowlf
Female
Sagittarius
40
57
Chemist
White Magic
Parry
Long Status
Move-HP Up

Hydra Bag

Feather Hat
Clothes
Cursed Ring

Hi-Ether, Echo Grass, Soft
Cure, Cure 2, Reraise, Regen, Wall, Esuna

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Pie108
Female
Scorpio
74
52
Dancer
Yin Yang Magic
Meatbone Slash
Secret Hunt
Move+2

Cute Bag

Leather Hat
Leather Outfit
Sprint Shoes

Slow Dance, Last Dance, Dragon Pit
Poison, Spell Absorb, Life Drain, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy



JIDkomu
Male
Libra
80
68
Time Mage
Charge
Counter Flood
Doublehand
Waterbreathing

White Staff

Triangle Hat
Light Robe
108 Gems

Haste, Stop, Stabilize Time, Meteor
Charge+1, Charge+4, Charge+5, Charge+20



LDSkinny
Female
Cancer
45
70
Squire
Summon Magic
Parry
Beastmaster
Move-MP Up

Battle Axe
Ice Shield
Platinum Helmet
Chain Vest
Diamond Armlet

Dash, Heal, Tickle
Moogle, Ifrit, Golem, Carbunkle, Salamander, Lich



ZephyrTempest
Female
Sagittarius
61
71
Priest
Yin Yang Magic
Earplug
Equip Axe
Ignore Terrain

Flame Whip

Twist Headband
Adaman Vest
Reflect Ring

Cure, Cure 2, Cure 4, Raise 2, Regen, Protect, Wall, Esuna
Blind, Spell Absorb, Pray Faith, Zombie, Dispel Magic, Petrify

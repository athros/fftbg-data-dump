Final Bets: red - 20 bets for 13,868G (68.2%, x0.47); blue - 17 bets for 6,468G (31.8%, x2.14)

red bets:
Nizaha: 3,101G (22.4%, 6,202G)
friendsquirrel: 1,657G (11.9%, 1,657G)
HaateXIII: 1,343G (9.7%, 1,343G)
sinnyil2: 1,200G (8.7%, 33,263G)
kai_shee: 1,001G (7.2%, 141,540G)
Pie108: 700G (5.0%, 20,662G)
0v3rr8d: 678G (4.9%, 678G)
AllInBot: 670G (4.8%, 670G)
Lanshaft: 600G (4.3%, 3,782G)
datadrivenbot: 555G (4.0%, 555G)
deenglow: 500G (3.6%, 21,501G)
catfashions: 428G (3.1%, 428G)
roofiepops: 392G (2.8%, 392G)
HeroponThrawn: 227G (1.6%, 227G)
joewcarson: 200G (1.4%, 7,932G)
athros13: 200G (1.4%, 2,739G)
ungabunga_bot: 116G (0.8%, 134,391G)
Tougou: 100G (0.7%, 2,574G)
ko2q: 100G (0.7%, 9,883G)
gary_corson: 100G (0.7%, 368G)

blue bets:
BirbBrainsBot: 1,000G (15.5%, 137,445G)
Mtueni: 1,000G (15.5%, 178,022G)
RunicMagus: 760G (11.8%, 760G)
JIDkomu: 500G (7.7%, 1,369G)
jame3200: 453G (7.0%, 1,453G)
fenaen: 400G (6.2%, 3,435G)
metagameface: 376G (5.8%, 376G)
LDSkinny: 356G (5.5%, 356G)
JumbocactuarX27: 340G (5.3%, 340G)
DLJuggernaut: 250G (3.9%, 8,110G)
organizing_secrets: 212G (3.3%, 212G)
ZephyrTempest: 203G (3.1%, 57,856G)
MrFlabyo: 200G (3.1%, 12,655G)
getthemoneyz: 118G (1.8%, 495,154G)
Lodrak: 100G (1.5%, 5,679G)
ApplesauceBoss: 100G (1.5%, 7,909G)
Lythe_Caraker: 100G (1.5%, 99,415G)

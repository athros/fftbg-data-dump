Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Chuckolator
Monster
Taurus
66
66
Draugr










HaateXIII
Male
Libra
64
73
Bard
Time Magic
MA Save
Attack UP
Swim

Ramia Harp

Twist Headband
Mystic Vest
Elf Mantle

Angel Song, Magic Song, Nameless Song, Diamond Blade
Slow 2, Reflect, Quick, Demi 2



Grininda
Male
Aries
62
71
Lancer
Yin Yang Magic
Caution
Concentrate
Move+3

Gungnir
Genji Shield
Genji Helmet
White Robe
Defense Armlet

Level Jump8, Vertical Jump8
Blind, Poison, Life Drain, Pray Faith, Zombie, Silence Song, Dispel Magic, Dark Holy



Douchetron
Male
Aquarius
61
79
Time Mage
Throw
Sunken State
Attack UP
Teleport

Wizard Staff

Barette
Clothes
Dracula Mantle

Haste, Haste 2, Stop, Stabilize Time
Shuriken, Bomb, Dictionary

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Mtmcl
Male
Gemini
71
38
Thief
Sing
Blade Grasp
Equip Bow
Move-HP Up

Windslash Bow

Triangle Hat
Adaman Vest
N-Kai Armlet

Steal Heart, Steal Helmet, Steal Armor, Leg Aim
Cheer Song, Nameless Song, Diamond Blade, Sky Demon



Dynasti
Male
Virgo
72
53
Thief
White Magic
Critical Quick
Equip Shield
Move+3

Cultist Dagger
Escutcheon
Headgear
Leather Vest
Germinas Boots

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Status, Arm Aim, Leg Aim
Cure, Cure 3, Cure 4, Raise, Reraise, Esuna, Magic Barrier



Dirty Naise
Male
Pisces
63
50
Squire
Yin Yang Magic
Faith Up
Equip Gun
Levitate

Romanda Gun

Triangle Hat
Secret Clothes
Spike Shoes

Dash, Throw Stone, Heal, Cheer Up, Wish
Poison, Zombie, Dispel Magic, Paralyze, Sleep



Dotoptv
Female
Scorpio
74
74
Archer
Draw Out
Distribute
Equip Shield
Teleport

Mythril Bow
Escutcheon
Golden Hairpin
Mythril Vest
Defense Ring

Charge+2, Charge+4, Charge+5, Charge+7
Asura, Kiyomori

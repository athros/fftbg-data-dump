Player: !White
Team: White Team
Palettes: White/Blue



Fenaen
Male
Virgo
48
72
Ninja
Battle Skill
Damage Split
Doublehand
Move-MP Up

Ninja Edge

Golden Hairpin
Mystic Vest
Elf Mantle

Shuriken, Ninja Sword, Wand
Head Break, Weapon Break, Magic Break



Bpc2163
Male
Gemini
80
69
Chemist
Sing
Meatbone Slash
Defend
Ignore Terrain

Panther Bag

Headgear
Adaman Vest
Defense Ring

Hi-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Sky Demon



Ko2q
Male
Virgo
77
56
Knight
Talk Skill
Blade Grasp
Equip Gun
Waterbreathing

Papyrus Codex
Genji Shield
Diamond Helmet
Black Robe
Battle Boots

Head Break, Dark Sword
Invitation, Threaten, Death Sentence, Insult, Refute



ColetteMSLP
Female
Scorpio
63
75
Time Mage
Black Magic
Abandon
Dual Wield
Ignore Terrain

Rainbow Staff
Wizard Staff
Holy Miter
Chameleon Robe
Germinas Boots

Haste, Slow, Immobilize, Float, Quick, Demi 2, Stabilize Time, Meteor
Fire, Fire 2, Fire 3, Bolt 3, Ice 3, Ice 4

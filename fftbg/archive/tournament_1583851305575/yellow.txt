Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Evolvingdev
Male
Gemini
44
45
Ninja
Talk Skill
Caution
Monster Talk
Move-HP Up

Kunai
Sasuke Knife
Feather Hat
Wizard Outfit
Power Wrist

Bomb, Ninja Sword, Dictionary
Praise, Negotiate, Mimic Daravon



UmbraKnights
Male
Scorpio
53
48
Mime

Auto Potion
Equip Shield
Retreat


Buckler
Genji Helmet
Judo Outfit
Genji Gauntlet

Mimic




Zebobz
Female
Aries
78
53
Mediator
Elemental
Catch
Sicken
Levitate

Papyrus Codex

Twist Headband
Secret Clothes
Angel Ring

Solution, Death Sentence, Insult, Refute
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



TheChainNerd
Female
Leo
61
69
Lancer
Time Magic
Counter Magic
Defend
Retreat

Mythril Spear
Gold Shield
Barbuta
Carabini Mail
Angel Ring

Level Jump4, Vertical Jump6
Slow, Slow 2, Float, Quick, Stabilize Time

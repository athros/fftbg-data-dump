Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kohlingen
Female
Taurus
60
63
Time Mage
Talk Skill
Speed Save
Monster Talk
Ignore Terrain

Battle Folio

Feather Hat
Black Robe
Magic Ring

Haste, Haste 2, Slow, Immobilize, Float, Reflect, Meteor
Solution, Insult, Mimic Daravon, Refute



Whiteonrice
Female
Cancer
70
72
Ninja
Time Magic
Counter Magic
Martial Arts
Lava Walking



Ribbon
Wizard Outfit
Diamond Armlet

Shuriken, Staff
Haste 2, Slow 2, Immobilize, Float, Quick



OneHundredFists
Female
Sagittarius
64
58
Wizard
Elemental
Counter
Short Charge
Waterwalking

Blind Knife

Feather Hat
Wizard Robe
Red Shoes

Fire 4, Bolt 3, Ice 2
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



ALY327
Male
Aquarius
57
79
Summoner
Black Magic
Speed Save
Long Status
Ignore Height

Thunder Rod

Leather Hat
Black Robe
Magic Gauntlet

Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Odin, Cyclops
Bolt 4, Ice 3, Ice 4

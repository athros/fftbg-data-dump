Player: !Green
Team: Green Team
Palettes: Green/White



Lawnboxer
Female
Aquarius
68
77
Dancer
Throw
Counter Flood
Sicken
Lava Walking

Ryozan Silk

Twist Headband
Power Sleeve
Cursed Ring

Wiznaibus, Disillusion, Last Dance, Void Storage
Shuriken, Knife, Dictionary



B0shii
Male
Gemini
56
57
Chemist
Steal
Brave Up
Equip Polearm
Lava Walking

Cashmere

Barette
Mythril Vest
Magic Ring

Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Gil Taking, Steal Helmet, Steal Shield, Arm Aim



UmaiJam
Female
Sagittarius
54
77
Summoner
Elemental
Counter
Short Charge
Ignore Terrain

Flame Rod

Headgear
Clothes
Rubber Shoes

Moogle, Shiva, Ifrit, Zodiac
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Blizzard



Wrongchoicez
Male
Gemini
44
50
Ninja
Jump
Mana Shield
Martial Arts
Jump+3

Cultist Dagger
Iga Knife
Feather Hat
Chain Vest
Cursed Ring

Shuriken, Bomb, Spear, Wand
Level Jump8, Vertical Jump7

Final Bets: white - 11 bets for 9,738G (59.8%, x0.67); purple - 7 bets for 6,536G (40.2%, x1.49)

white bets:
TheMurkGnome: 3,000G (30.8%, 9,508G)
NIghtdew14: 2,453G (25.2%, 9,812G)
twelfthrootoftwo: 1,638G (16.8%, 3,212G)
randgridr: 801G (8.2%, 1,603G)
Xoomwaffle: 500G (5.1%, 5,988G)
Macrosoft_Inc: 496G (5.1%, 496G)
CosmicTactician: 300G (3.1%, 48,549G)
AllInBot: 200G (2.1%, 200G)
DouglasDragonThePoet: 150G (1.5%, 808G)
nhammen: 100G (1.0%, 4,312G)
dtrain332: 100G (1.0%, 2,996G)

purple bets:
Lydian_C: 3,400G (52.0%, 40,890G)
BirbBrainsBot: 1,000G (15.3%, 64,341G)
UmaiJam: 1,000G (15.3%, 54,571G)
CT_5_Holy: 500G (7.6%, 4,939G)
ruleof5: 500G (7.6%, 22,928G)
getthemoneyz: 106G (1.6%, 1,318,696G)
roqqqpsi: 30G (0.5%, 3,028G)

Player: !White
Team: White Team
Palettes: White/Blue



Randgridr
Female
Aquarius
75
65
Samurai
Throw
Auto Potion
Attack UP
Move-HP Up

Murasame

Diamond Helmet
Leather Armor
Jade Armlet

Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji, Masamune
Shuriken, Staff, Ninja Sword, Wand



Evewho
Female
Scorpio
77
73
Ninja
Charge
Caution
Short Charge
Lava Walking

Sasuke Knife
Assassin Dagger
Headgear
Chain Vest
Red Shoes

Shuriken, Staff
Charge+2, Charge+20



PLifer
Monster
Cancer
62
71
Dark Behemoth










Bahumat989
Female
Virgo
66
63
Dancer
Time Magic
Critical Quick
Equip Knife
Waterbreathing

Ice Rod

Triangle Hat
Chameleon Robe
Small Mantle

Wiznaibus, Void Storage, Nether Demon
Haste, Haste 2, Slow, Immobilize, Reflect, Quick, Demi, Stabilize Time

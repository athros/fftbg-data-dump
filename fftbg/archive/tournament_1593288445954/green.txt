Player: !Green
Team: Green Team
Palettes: Green/White



Peeronid
Male
Libra
61
52
Archer
Throw
Counter Flood
Doublehand
Retreat

Long Bow

Mythril Helmet
Mystic Vest
Wizard Mantle

Charge+2, Charge+4
Shuriken, Knife, Spear, Wand



Zachara
Monster
Scorpio
52
44
Red Dragon










Thewondertrickster
Male
Scorpio
50
46
Wizard
Battle Skill
Counter
Equip Axe
Retreat

White Staff

Green Beret
Rubber Costume
Genji Gauntlet

Bolt, Bolt 4, Ice, Empower
Armor Break, Explosion Sword



PrancesWithWolves
Female
Serpentarius
64
79
Chemist
Elemental
HP Restore
Magic Defense UP
Swim

Panther Bag

Headgear
Judo Outfit
Defense Armlet

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Elixir, Antidote, Echo Grass, Soft, Remedy, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

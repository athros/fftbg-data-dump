Player: !White
Team: White Team
Palettes: White/Blue



Lunysgwen
Female
Capricorn
71
44
Archer
Black Magic
Parry
Magic Attack UP
Jump+2

Snipe Bow
Escutcheon
Holy Miter
Secret Clothes
Red Shoes

Charge+3, Charge+20
Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Empower



Kliffw
Male
Capricorn
60
71
Time Mage
White Magic
Parry
Dual Wield
Swim

Cypress Rod
Battle Bamboo
Golden Hairpin
Mythril Vest
Diamond Armlet

Haste 2, Immobilize, Float, Reflect, Quick, Demi, Stabilize Time
Cure 2, Regen, Esuna



BlackfyreRoy
Female
Aries
40
59
Monk
Summon Magic
Brave Save
Secret Hunt
Move+3



Flash Hat
Adaman Vest
Reflect Ring

Earth Slash, Secret Fist, Purification, Chakra
Moogle, Golem, Bahamut, Fairy



DavenIII
Male
Aquarius
60
66
Thief
Jump
MP Restore
Dual Wield
Jump+1

Mythril Sword
Kunai
Feather Hat
Clothes
Rubber Shoes

Steal Armor, Steal Shield, Steal Weapon
Level Jump3, Vertical Jump7

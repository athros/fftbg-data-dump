Player: !Green
Team: Green Team
Palettes: Green/White



Willwagnermusic
Female
Libra
52
38
Mediator
Time Magic
Auto Potion
Magic Attack UP
Fly

Blaze Gun

Flash Hat
Clothes
Elf Mantle

Preach, Death Sentence, Insult, Refute, Rehabilitate
Slow 2, Stop, Float, Demi, Stabilize Time, Meteor



Technominari
Female
Libra
48
51
Priest
Steal
Speed Save
Short Charge
Swim

Rainbow Staff

Black Hood
Light Robe
Magic Gauntlet

Cure, Cure 3, Raise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna, Holy
Steal Armor, Steal Accessory, Steal Status



Carledo
Female
Pisces
78
55
Thief
Elemental
Counter Tackle
Beastmaster
Ignore Height

Air Knife

Headgear
Judo Outfit
N-Kai Armlet

Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Hell Ivy, Local Quake, Static Shock, Quicksand



Grininda
Male
Taurus
75
64
Calculator
Lucavi Skill
Counter Flood
Defend
Move-MP Up

Battle Folio
Crystal Shield
Leather Hat
Genji Armor
Magic Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Treafa
Male
Aries
64
65
Samurai
Throw
Counter
Short Status
Swim

Asura Knife

Gold Helmet
Linen Robe
Feather Boots

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Shuriken



Mesmaster
Male
Pisces
61
76
Monk
Throw
Distribute
Equip Knife
Retreat

Wizard Rod

Leather Hat
Judo Outfit
Bracer

Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil
Shuriken



Lord Gwarth
Female
Libra
57
76
Thief
Dance
Auto Potion
Martial Arts
Fly



Leather Hat
Adaman Vest
Feather Mantle

Gil Taking, Steal Heart, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Slow Dance, Last Dance



Powerpinch
Monster
Leo
48
62
Tiamat








Player: !White
Team: White Team
Palettes: White/Blue



Brokenknight201
Male
Scorpio
75
66
Calculator
Yin Yang Magic
Counter Tackle
Magic Attack UP
Move+2

Battle Folio

Holy Miter
Adaman Vest
Magic Ring

Height, 5, 3
Zombie, Foxbird, Paralyze



TheDeeyo
Female
Scorpio
71
67
Chemist
Talk Skill
Counter
Beastmaster
Levitate

Orichalcum

Red Hood
Wizard Outfit
Angel Ring

Potion, X-Potion, Remedy, Phoenix Down
Praise, Death Sentence, Negotiate, Mimic Daravon



DAC169
Female
Virgo
50
59
Priest
Black Magic
Faith Save
Equip Axe
Ignore Terrain

Healing Staff

Feather Hat
Rubber Costume
Reflect Ring

Cure, Cure 2, Cure 4, Raise 2, Wall, Esuna, Holy
Ice, Empower



Nhammen
Male
Cancer
62
70
Lancer
Black Magic
HP Restore
Sicken
Ignore Height

Holy Lance
Ice Shield
Cross Helmet
Plate Mail
108 Gems

Level Jump4, Vertical Jump6
Bolt 3, Empower

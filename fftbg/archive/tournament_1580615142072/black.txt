Player: !Black
Team: Black Team
Palettes: Black/Red



Kellios11
Monster
Sagittarius
69
54
Revenant










PranaDeviI
Male
Serpentarius
74
42
Ninja
Time Magic
MP Restore
Equip Gun
Jump+2

Battle Folio
Ramia Harp
Twist Headband
Adaman Vest
Germinas Boots

Shuriken, Bomb, Ninja Sword, Spear
Stop, Reflect, Quick, Stabilize Time



ShinoGuy
Male
Libra
55
51
Geomancer
Yin Yang Magic
Regenerator
Equip Gun
Jump+3

Muramasa
Buckler
Golden Hairpin
White Robe
Elf Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Poison, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify



Wicheka
Female
Scorpio
70
71
Lancer
Item
Meatbone Slash
Attack UP
Jump+1

Spear
Genji Shield
Mythril Helmet
Linen Robe
Chantage

Level Jump4, Vertical Jump6
Potion, X-Potion, Hi-Ether, Remedy, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



Tronfonne
Female
Leo
78
59
Mediator
Dance
Abandon
Equip Knife
Move+3

Thunder Rod

Leather Hat
Linen Robe
Spike Shoes

Invitation, Threaten, Insult, Negotiate, Refute
Witch Hunt, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Nether Demon



Choco Joe
Female
Aquarius
68
65
Chemist
Punch Art
Catch
Equip Armor
Jump+2

Dagger

Genji Helmet
Gold Armor
Angel Ring

X-Potion, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Spin Fist, Earth Slash, Purification, Chakra, Revive



Dtrain332
Male
Pisces
75
60
Knight
Steal
Meatbone Slash
Equip Sword
Jump+3

Masamune
Gold Shield
Genji Helmet
Diamond Armor
Magic Ring

Head Break, Armor Break, Shield Break, Power Break, Stasis Sword, Justice Sword
Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Steal Status



Josephiroth 143
Male
Libra
69
62
Chemist
Elemental
Faith Save
Maintenance
Fly

Mythril Gun

Headgear
Mystic Vest
Feather Boots

Potion, Hi-Potion, Ether, Holy Water, Remedy, Phoenix Down
Water Ball, Static Shock, Quicksand, Lava Ball

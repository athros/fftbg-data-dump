Player: !Green
Team: Green Team
Palettes: Green/White



Snoopiku
Monster
Capricorn
42
61
Ghoul










Miku Shikhu
Female
Taurus
67
70
Mime

Counter Tackle
Attack UP
Waterbreathing



Flash Hat
Judo Outfit
Power Wrist

Mimic




Deathmaker06
Female
Libra
40
63
Squire
Draw Out
Counter Flood
Equip Armor
Jump+3

Morning Star
Aegis Shield
Crystal Helmet
Light Robe
Red Shoes

Accumulate, Dash, Cheer Up, Fury, Ultima
Asura, Bizen Boat, Kiyomori



Powergems
Female
Leo
68
79
Ninja
Black Magic
Parry
Long Status
Levitate

Kunai
Sasuke Knife
Holy Miter
Power Sleeve
Magic Ring

Shuriken, Bomb, Sword
Fire 2, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 2

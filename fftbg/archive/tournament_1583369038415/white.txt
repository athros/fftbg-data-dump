Player: !White
Team: White Team
Palettes: White/Blue



Neversye
Monster
Sagittarius
52
53
Steel Giant










MoxOb
Female
Gemini
46
48
Samurai
Throw
MP Restore
Short Status
Retreat

Masamune

Diamond Helmet
Mythril Armor
N-Kai Armlet

Koutetsu, Bizen Boat, Kikuichimoji
Shuriken, Bomb, Knife



RyuTsuno
Monster
Aquarius
75
51
King Behemoth










Masta Glenn
Female
Sagittarius
42
61
Wizard
Throw
Counter Magic
Dual Wield
Ignore Terrain

Mage Masher
Flame Rod
Holy Miter
Silk Robe
Bracer

Bolt, Bolt 4, Ice, Ice 2, Frog, Flare
Shuriken, Spear

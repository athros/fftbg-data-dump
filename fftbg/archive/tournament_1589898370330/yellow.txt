Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



NIghtdew14
Female
Taurus
73
64
Ninja
Talk Skill
Counter Magic
Equip Gun
Move-HP Up

Battle Folio
Madlemgen
Triangle Hat
Chain Vest
Sprint Shoes

Bomb
Persuade, Insult



Joewcarson
Male
Virgo
72
72
Knight
Charge
Auto Potion
Beastmaster
Fly

Mythril Sword
Gold Shield
Mythril Helmet
Platinum Armor
Magic Ring

Shield Break, Weapon Break, Mind Break, Stasis Sword, Dark Sword, Surging Sword
Charge+2, Charge+20



Phi Sig
Female
Serpentarius
67
47
Summoner
Time Magic
Absorb Used MP
Secret Hunt
Lava Walking

Dragon Rod

Flash Hat
White Robe
Cursed Ring

Shiva
Haste, Stabilize Time



Mtueni
Male
Libra
61
54
Thief
Summon Magic
Caution
Magic Defense UP
Retreat

Zorlin Shape

Flash Hat
Adaman Vest
Defense Ring

Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Arm Aim
Moogle, Ifrit, Golem, Bahamut

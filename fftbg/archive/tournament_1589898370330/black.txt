Player: !Black
Team: Black Team
Palettes: Black/Red



Tougou
Male
Aries
68
46
Lancer
Draw Out
Counter Flood
Short Charge
Fly

Iron Fan
Flame Shield
Cross Helmet
Reflect Mail
Cherche

Level Jump2, Vertical Jump8
Murasame



Marin1987
Female
Pisces
51
47
Lancer
Basic Skill
Distribute
Sicken
Waterwalking

Octagon Rod
Aegis Shield
Diamond Helmet
Chain Mail
Defense Ring

Level Jump8, Vertical Jump2
Accumulate, Dash, Throw Stone, Heal, Yell



Eudes89
Male
Aquarius
70
57
Oracle
Charge
Counter Flood
Sicken
Move+2

Iron Fan

Feather Hat
Linen Robe
Cursed Ring

Zombie, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Charge+2, Charge+4, Charge+7



LDSkinny
Female
Virgo
60
58
Oracle
Steal
Hamedo
Beastmaster
Waterbreathing

Gokuu Rod

Triangle Hat
Wizard Outfit
Red Shoes

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Zombie, Blind Rage, Dispel Magic
Arm Aim

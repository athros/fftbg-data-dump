Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MantisFinch
Female
Aries
59
50
Dancer
Charge
Auto Potion
Equip Gun
Move+1

Blast Gun

Holy Miter
Mythril Vest
Genji Gauntlet

Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Dragon Pit
Charge+3, Charge+7, Charge+10



CosmicTactician
Female
Virgo
41
49
Knight
Throw
Counter
Short Status
Move-MP Up

Rune Blade
Mythril Shield
Diamond Helmet
Platinum Armor
Leather Mantle

Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword
Shuriken, Bomb, Staff



OneHundredFists
Male
Virgo
73
60
Mime

Counter Tackle
Long Status
Move+3



Leather Hat
Mystic Vest
Feather Mantle

Mimic




Grayeternity
Male
Leo
42
73
Squire
Summon Magic
Sunken State
Short Status
Teleport

Mage Masher
Flame Shield
Black Hood
Earth Clothes
Elf Mantle

Accumulate, Throw Stone, Heal, Cheer Up, Fury
Shiva, Titan, Golem, Salamander, Silf, Fairy

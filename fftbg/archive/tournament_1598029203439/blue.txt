Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Narcius
Female
Scorpio
49
61
Chemist
Summon Magic
Hamedo
Defend
Move+3

Blast Gun

Green Beret
Mythril Vest
N-Kai Armlet

X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Moogle, Ramuh, Carbunkle, Silf



Dogsandcatsand
Female
Virgo
54
46
Wizard
Item
PA Save
Defend
Move-HP Up

Panther Bag

Green Beret
Secret Clothes
Magic Gauntlet

Fire 2, Fire 4, Bolt, Bolt 3, Ice, Empower
Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down



Actual JP
Female
Aries
63
46
Knight
Summon Magic
Dragon Spirit
Equip Gun
Levitate

Battle Folio
Gold Shield
Crystal Helmet
Genji Armor
Dracula Mantle

Head Break, Weapon Break, Magic Break, Mind Break, Dark Sword
Moogle, Ifrit, Golem, Odin, Salamander, Silf



Galkife
Male
Sagittarius
75
63
Lancer
Charge
Regenerator
Halve MP
Waterwalking

Obelisk
Round Shield
Leather Helmet
Genji Armor
Defense Armlet

Level Jump3, Vertical Jump5
Charge+3, Charge+4

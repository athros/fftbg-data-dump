Final Bets: red - 16 bets for 15,995G (71.1%, x0.41); blue - 12 bets for 6,494G (28.9%, x2.46)

red bets:
E_Ballard: 3,082G (19.3%, 3,082G)
sinnyil2: 3,026G (18.9%, 6,053G)
latebit: 2,401G (15.0%, 4,708G)
toka222: 1,500G (9.4%, 239,331G)
BirbBrainsBot: 1,000G (6.3%, 105,597G)
maximumcrit: 868G (5.4%, 868G)
killth3kid: 768G (4.8%, 35,852G)
Nizaha: 756G (4.7%, 15,488G)
DustBirdEX: 636G (4.0%, 636G)
TasisSai: 562G (3.5%, 562G)
pplvee1: 512G (3.2%, 47,712G)
DouglasDragonThePoet: 268G (1.7%, 268G)
helpimabug: 216G (1.4%, 216G)
datadrivenbot: 200G (1.3%, 63,120G)
nifboy: 100G (0.6%, 4,310G)
RuneS_77: 100G (0.6%, 17,159G)

blue bets:
mangowarfare: 1,000G (15.4%, 11,027G)
SkylerBunny: 1,000G (15.4%, 3,630G)
dogsandcatsand: 972G (15.0%, 972G)
LDSkinny: 670G (10.3%, 670G)
SephDarkheart: 600G (9.2%, 124,278G)
JonnyCue: 500G (7.7%, 3,619G)
ruleof5: 500G (7.7%, 17,417G)
WhiteTigress: 498G (7.7%, 498G)
MagicBottle: 308G (4.7%, 308G)
getthemoneyz: 246G (3.8%, 1,704,013G)
AllInBot: 100G (1.5%, 100G)
IndecisiveNinja: 100G (1.5%, 5,869G)

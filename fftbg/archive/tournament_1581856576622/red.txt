Player: !Red
Team: Red Team
Palettes: Red/Brown



Kalibak1
Female
Scorpio
53
47
Calculator
Yin Yang Magic
Parry
Equip Knife
Ignore Height

Poison Rod

Leather Hat
Chameleon Robe
108 Gems

CT, Prime Number, 5
Poison, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy



ClutchCasters
Male
Virgo
68
52
Knight
Time Magic
Earplug
Dual Wield
Levitate

Slasher
Slasher
Gold Helmet
Gold Armor
Leather Mantle

Armor Break, Weapon Break, Power Break, Mind Break, Justice Sword, Night Sword
Reflect, Quick, Demi 2



Ominnous
Male
Gemini
71
69
Lancer
Battle Skill
Absorb Used MP
Equip Gun
Teleport 2

Bestiary
Round Shield
Diamond Helmet
Carabini Mail
Magic Gauntlet

Level Jump5, Vertical Jump5
Shield Break, Mind Break, Justice Sword, Surging Sword



HotpotTheHungry
Male
Cancer
63
72
Oracle
White Magic
Sunken State
Short Status
Jump+1

Cypress Rod

Feather Hat
Wizard Robe
Sprint Shoes

Blind, Poison, Pray Faith, Silence Song, Dark Holy
Cure, Cure 4, Raise, Reraise, Shell, Esuna

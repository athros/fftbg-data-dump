Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



NotoriousThief
Male
Libra
45
45
Priest
Steal
Counter Magic
Doublehand
Ignore Height

Gold Staff

Green Beret
Chain Vest
Feather Boots

Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory



Mtmcl
Male
Taurus
74
66
Ninja
Yin Yang Magic
Arrow Guard
Equip Polearm
Jump+2

Air Knife
Flail
Headgear
Adaman Vest
Battle Boots

Shuriken, Bomb, Knife, Wand, Dictionary
Poison, Doubt Faith, Zombie, Silence Song, Confusion Song, Sleep, Dark Holy



HaplessOne
Male
Aries
77
69
Knight
Punch Art
Sunken State
Dual Wield
Teleport

Ragnarok
Ice Brand
Iron Helmet
Crystal Mail
Dracula Mantle

Armor Break, Shield Break, Speed Break, Dark Sword
Earth Slash, Secret Fist, Chakra, Revive



Mtueni
Male
Scorpio
50
60
Monk
Steal
Earplug
Dual Wield
Ignore Height



Thief Hat
Mystic Vest
Small Mantle

Pummel, Wave Fist, Purification, Revive, Seal Evil
Steal Armor, Arm Aim

Final Bets: purple - 8 bets for 4,845G (19.7%, x4.08); brown - 13 bets for 19,750G (80.3%, x0.25)

purple bets:
killth3kid: 1,418G (29.3%, 1,418G)
dogsandcatsand: 1,171G (24.2%, 1,171G)
DeathTaxesAndAnime: 852G (17.6%, 852G)
Chihuahua_Charity: 500G (10.3%, 7,429G)
maximumcrit: 500G (10.3%, 13,054G)
datadrivenbot: 200G (4.1%, 68,949G)
getthemoneyz: 104G (2.1%, 1,729,713G)
Aldrammech: 100G (2.1%, 37,483G)

brown bets:
DustBirdEX: 7,440G (37.7%, 7,440G)
pplvee1: 2,857G (14.5%, 5,602G)
reinoe: 2,000G (10.1%, 41,999G)
VolgraTheMoose: 1,501G (7.6%, 58,004G)
sinnyil2: 1,289G (6.5%, 2,579G)
BirbBrainsBot: 1,000G (5.1%, 161,774G)
Thyrandaal: 1,000G (5.1%, 11,516G)
Forkmore: 931G (4.7%, 931G)
E_Ballard: 732G (3.7%, 52,629G)
SephDarkheart: 600G (3.0%, 166,649G)
AbandonedHall: 200G (1.0%, 941G)
AllInBot: 100G (0.5%, 100G)
Miku_Shikhu: 100G (0.5%, 5,484G)

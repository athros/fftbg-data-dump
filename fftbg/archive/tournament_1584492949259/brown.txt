Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lythe Caraker
Female
Aquarius
65
39
Chemist
Black Magic
Dragon Spirit
Concentrate
Fly

Romanda Gun

Twist Headband
Mythril Vest
Wizard Mantle

Potion, X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Fire, Fire 2, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Death



Bryan792
Male
Sagittarius
47
79
Ninja
Item
Sunken State
Attack UP
Move-MP Up

Orichalcum
Ninja Edge
Red Hood
Wizard Outfit
Magic Gauntlet

Shuriken, Wand, Dictionary
Potion, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down



Break3r
Male
Pisces
59
42
Squire
Sing
Absorb Used MP
Sicken
Move-HP Up

Rune Blade
Ice Shield
Twist Headband
Wizard Outfit
Germinas Boots

Accumulate, Dash, Heal, Tickle, Cheer Up
Angel Song, Battle Song, Last Song, Diamond Blade, Sky Demon



Panushenko
Male
Pisces
63
45
Geomancer
Throw
Counter Flood
Equip Armor
Move+3

Coral Sword
Genji Shield
Platinum Helmet
Mythril Armor
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Knife, Wand

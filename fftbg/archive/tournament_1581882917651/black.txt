Player: !Black
Team: Black Team
Palettes: Black/Red



Ninesilvers
Monster
Capricorn
53
47
Vampire










RjA0zcOQ96
Monster
Aquarius
41
70
Floating Eye










AdaephonD
Male
Scorpio
50
59
Ninja
Talk Skill
Abandon
Equip Armor
Jump+3

Scorpion Tail
Ninja Edge
Leather Helmet
Light Robe
Battle Boots

Shuriken, Stick
Invitation, Insult, Negotiate, Refute



MeleeWizard
Female
Scorpio
46
53
Wizard
Elemental
Counter Tackle
Defense UP
Move-MP Up

Mythril Knife

Flash Hat
Leather Outfit
Defense Ring

Fire, Fire 2, Bolt 2, Bolt 4, Ice 3
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind

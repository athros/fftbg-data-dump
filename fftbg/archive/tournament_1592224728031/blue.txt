Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Elelor
Female
Pisces
56
47
Thief
Charge
Brave Save
Short Status
Teleport

Orichalcum

Golden Hairpin
Black Costume
Elf Mantle

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon
Charge+1, Charge+2, Charge+7



Lastly
Female
Sagittarius
75
48
Geomancer
Yin Yang Magic
Sunken State
Equip Bow
Jump+1

Hunting Bow
Round Shield
Leather Hat
White Robe
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Poison, Life Drain, Sleep, Dark Holy



Cam ATS
Male
Scorpio
68
40
Monk
Draw Out
Brave Save
Equip Knife
Jump+2

Dagger

Feather Hat
Judo Outfit
Diamond Armlet

Wave Fist, Earth Slash, Secret Fist, Revive
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji



NovaKnight21
Female
Scorpio
56
74
Ninja
Summon Magic
Earplug
Short Charge
Swim

Morning Star
Morning Star
Feather Hat
Power Sleeve
Small Mantle

Shuriken, Knife, Ninja Sword
Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Silf, Lich

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Solomongrundy85
Male
Virgo
49
70
Knight
Punch Art
Abandon
Defense UP
Jump+2

Defender
Buckler
Leather Helmet
Plate Mail
Diamond Armlet

Head Break, Armor Break, Shield Break, Speed Break, Power Break
Spin Fist, Wave Fist, Purification, Chakra



HASTERIOUS
Male
Scorpio
49
46
Bard
Summon Magic
HP Restore
Defense UP
Move+3

Fairy Harp

Twist Headband
Clothes
Germinas Boots

Life Song, Cheer Song
Moogle, Ramuh



ApplesauceBoss
Male
Sagittarius
49
76
Squire
Battle Skill
Counter
Martial Arts
Swim


Round Shield
Headgear
Mystic Vest
Defense Ring

Accumulate, Throw Stone, Heal, Tickle, Cheer Up, Fury, Wish, Scream
Speed Break, Power Break, Stasis Sword, Dark Sword



Grininda
Male
Sagittarius
52
79
Archer
Item
Parry
Throw Item
Jump+3

Blaze Gun
Escutcheon
Twist Headband
Power Sleeve
Magic Ring

Charge+1, Charge+3, Charge+5
Potion, Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

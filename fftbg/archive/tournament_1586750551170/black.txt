Player: !Black
Team: Black Team
Palettes: Black/Red



Artea
Male
Capricorn
51
62
Chemist
Yin Yang Magic
Parry
Equip Armor
Jump+3

Mythril Gun

Cross Helmet
Leather Armor
Bracer

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Phoenix Down
Spell Absorb, Sleep, Petrify, Dark Holy



NoNotBees
Female
Sagittarius
70
49
Ninja
Basic Skill
Counter Tackle
Equip Shield
Levitate

Flail
Gold Shield
Black Hood
Chain Vest
Leather Mantle

Shuriken, Bomb
Heal



The Pengwin
Male
Libra
45
56
Bard
Draw Out
Regenerator
Equip Bow
Swim

Ice Bow

Holy Miter
Earth Clothes
Reflect Ring

Angel Song, Life Song, Cheer Song, Battle Song, Sky Demon
Koutetsu, Bizen Boat



Chompie
Female
Cancer
74
68
Squire
Talk Skill
Auto Potion
Doublehand
Lava Walking

Battle Axe

Triangle Hat
Earth Clothes
Germinas Boots

Accumulate, Heal, Cheer Up
Invitation, Insult, Mimic Daravon, Refute

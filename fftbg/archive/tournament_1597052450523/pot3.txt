Final Bets: white - 4 bets for 2,585G (17.2%, x4.82); black - 13 bets for 12,461G (82.8%, x0.21)

white bets:
Klednar21: 1,000G (38.7%, 2,733G)
BirbBrainsBot: 1,000G (38.7%, 160,254G)
getthemoneyz: 384G (14.9%, 1,579,532G)
gorgewall: 201G (7.8%, 5,538G)

black bets:
reinoe: 5,000G (40.1%, 176,843G)
douchetron: 2,109G (16.9%, 4,137G)
CassiePhoenix: 1,176G (9.4%, 1,176G)
amiture: 1,059G (8.5%, 1,059G)
randgridr: 995G (8.0%, 2,555G)
ko2q: 692G (5.6%, 692G)
Zetchryn: 356G (2.9%, 356G)
blorpy_: 274G (2.2%, 3,432G)
AllInBot: 200G (1.6%, 200G)
datadrivenbot: 200G (1.6%, 54,673G)
Nine07: 200G (1.6%, 200G)
MinBetBot: 100G (0.8%, 20,681G)
DouglasDragonThePoet: 100G (0.8%, 1,453G)

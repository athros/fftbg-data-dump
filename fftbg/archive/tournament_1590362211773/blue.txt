Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DudeMonkey77
Male
Leo
59
49
Geomancer
Draw Out
Faith Save
Defend
Teleport 2

Giant Axe
Round Shield
Twist Headband
Chameleon Robe
Diamond Armlet

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Koutetsu



Firesheath
Female
Taurus
49
45
Chemist
Draw Out
Meatbone Slash
Equip Axe
Move+2

Healing Staff

Triangle Hat
Mystic Vest
Dracula Mantle

Hi-Ether, Phoenix Down
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Digitalsocrates
Male
Aries
50
70
Bard
White Magic
Abandon
Doublehand
Swim

Ramia Harp

Headgear
Genji Armor
Feather Boots

Hydra Pit
Cure, Cure 2, Raise, Protect 2, Shell 2, Esuna, Magic Barrier



Kyune
Male
Scorpio
60
77
Archer
Yin Yang Magic
Sunken State
Dual Wield
Waterbreathing

Lightning Bow

Leather Helmet
Chain Vest
Bracer

Charge+1, Charge+4, Charge+7
Spell Absorb, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy

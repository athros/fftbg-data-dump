Final Bets: red - 16 bets for 6,178G (18.7%, x4.34); purple - 27 bets for 26,818G (81.3%, x0.23)

red bets:
Maldoree: 1,139G (18.4%, 1,139G)
BirbBrainsBot: 1,000G (16.2%, 160,857G)
superdevon1: 712G (11.5%, 7,125G)
Lali_Lulelo: 687G (11.1%, 687G)
DeathTaxesAndAnime: 504G (8.2%, 504G)
TheChainNerd: 452G (7.3%, 452G)
Tithonus: 312G (5.1%, 312G)
NicoSavoy: 302G (4.9%, 302G)
SQUiDSQUARKLIN: 300G (4.9%, 3,257G)
Snoopiku: 250G (4.0%, 1,940G)
DaveKap: 108G (1.7%, 108G)
Kronikle: 100G (1.6%, 2,382G)
Squatting_Bear: 100G (1.6%, 1,810G)
twelfthrootoftwo: 100G (1.6%, 1,455G)
Arkreaver: 100G (1.6%, 11,391G)
getthemoneyz: 12G (0.2%, 655,958G)

purple bets:
moonliquor: 5,000G (18.6%, 20,369G)
HaplessOne: 2,876G (10.7%, 9,588G)
Baron_von_Scrub: 2,500G (9.3%, 18,819G)
catfashions: 2,000G (7.5%, 13,988G)
Evewho: 1,429G (5.3%, 2,803G)
sinnyil2: 1,200G (4.5%, 18,553G)
E_Ballard: 1,096G (4.1%, 1,096G)
CapnChaos12: 1,080G (4.0%, 1,080G)
Kyune: 1,072G (4.0%, 1,072G)
volgrathemoose: 1,001G (3.7%, 14,937G)
powergems: 1,000G (3.7%, 4,335G)
ShintaroNayaka: 1,000G (3.7%, 19,357G)
JustDoomathon: 750G (2.8%, 1,970G)
DustBirdEX: 645G (2.4%, 2,647G)
Jaritras: 641G (2.4%, 641G)
metagameface: 522G (1.9%, 522G)
cupholderr: 503G (1.9%, 503G)
Crimefridge: 500G (1.9%, 1,246G)
DarrenDinosaurs: 460G (1.7%, 460G)
CorpusCav: 444G (1.7%, 3,630G)
Velixtra: 247G (0.9%, 247G)
KasugaiRoastedPeas: 200G (0.7%, 1,458G)
ZZ_Yoshi: 200G (0.7%, 1,717G)
Jampck: 150G (0.6%, 814G)
ungabunga_bot: 102G (0.4%, 425,712G)
MrKockonfaec: 100G (0.4%, 100G)
datadrivenbot: 100G (0.4%, 12,542G)

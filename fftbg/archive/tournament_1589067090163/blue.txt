Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Breakdown777
Male
Capricorn
52
78
Squire
Steal
Parry
Secret Hunt
Teleport

Nagrarock
Crystal Shield
Iron Helmet
Crystal Mail
Defense Ring

Dash, Throw Stone, Heal, Yell, Cheer Up
Steal Shield, Steal Accessory, Steal Status, Leg Aim



HorusTaurus
Male
Aquarius
67
79
Summoner
Draw Out
Auto Potion
Equip Gun
Swim

Bestiary

Red Hood
White Robe
Cursed Ring

Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Odin, Silf, Fairy, Lich
Bizen Boat, Kiyomori, Muramasa



BuffaloCrunch
Female
Capricorn
50
69
Ninja
Steal
Dragon Spirit
Equip Knife
Lava Walking

Thunder Rod
Kunai
Red Hood
Earth Clothes
Germinas Boots

Shuriken, Bomb
Gil Taking, Steal Heart, Steal Helmet, Steal Shield



CorpusCav
Monster
Virgo
41
42
Skeleton








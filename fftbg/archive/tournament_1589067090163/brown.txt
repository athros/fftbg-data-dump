Player: !Brown
Team: Brown Team
Palettes: Brown/Green



LeepingJJ
Female
Taurus
75
76
Squire
Elemental
Regenerator
Equip Axe
Ignore Terrain

Giant Axe
Buckler
Gold Helmet
Platinum Armor
Defense Armlet

Dash, Wish, Scream
Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard



Meta Five
Male
Scorpio
52
69
Bard
Summon Magic
Catch
Magic Attack UP
Lava Walking

Bloody Strings

Thief Hat
Wizard Outfit
Genji Gauntlet

Angel Song, Cheer Song, Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade, Sky Demon
Moogle, Ifrit, Leviathan, Salamander, Fairy, Cyclops



ExecutedGiraffe
Male
Pisces
75
50
Mediator
Throw
Caution
Secret Hunt
Teleport

Blaze Gun

Twist Headband
Power Sleeve
Angel Ring

Persuade, Preach, Death Sentence, Negotiate, Refute
Shuriken



ShintaroNayaka
Female
Sagittarius
57
42
Dancer
Battle Skill
Critical Quick
Equip Sword
Lava Walking

Mythril Sword

Black Hood
Adaman Vest
Red Shoes

Witch Hunt, Disillusion, Nameless Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon
Shield Break, Mind Break

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lali Lulelo
Female
Libra
66
47
Chemist
Battle Skill
MP Restore
Secret Hunt
Ignore Terrain

Cute Bag

Cachusha
Black Costume
Defense Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down
Head Break, Magic Break, Power Break, Mind Break, Dark Sword



HorusTaurus
Female
Aquarius
66
54
Archer
Elemental
Abandon
Short Status
Jump+1

Ultimus Bow

Golden Hairpin
Secret Clothes
Defense Armlet

Charge+1, Charge+2, Charge+5, Charge+7
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



Jeeboheebo
Female
Pisces
72
64
Calculator
Black Magic
PA Save
Equip Armor
Fly

Battle Folio

Platinum Helmet
Light Robe
Chantage

CT, Height, 3
Bolt 4, Ice, Ice 4



Cupholderr
Female
Taurus
53
47
Squire
Talk Skill
Blade Grasp
Halve MP
Fly

Slasher
Crystal Shield
Feather Hat
Chain Vest
Jade Armlet

Accumulate, Throw Stone, Cheer Up, Wish, Scream
Invitation, Praise, Solution, Death Sentence, Negotiate, Refute

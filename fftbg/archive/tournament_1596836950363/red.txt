Player: !Red
Team: Red Team
Palettes: Red/Brown



MonchoStrife
Male
Pisces
74
61
Bard
Yin Yang Magic
Counter Magic
Equip Bow
Jump+2

Windslash Bow

Triangle Hat
Earth Clothes
Germinas Boots

Cheer Song, Battle Song, Magic Song, Last Song
Poison, Spell Absorb, Confusion Song, Dispel Magic, Paralyze, Sleep



Victoriolue
Male
Aries
64
55
Oracle
Jump
HP Restore
Secret Hunt
Swim

Musk Rod

Leather Hat
Linen Robe
Small Mantle

Blind, Life Drain, Silence Song, Dispel Magic, Sleep
Level Jump8, Vertical Jump6



Sairentozon7
Male
Capricorn
46
79
Thief
Summon Magic
Catch
Equip Shield
Jump+1

Mythril Knife
Genji Shield
Black Hood
Mythril Vest
Reflect Ring

Gil Taking, Steal Status
Moogle, Shiva, Ifrit, Titan, Golem, Leviathan, Salamander, Fairy



DesertWooder
Female
Taurus
73
77
Lancer
Item
Abandon
Long Status
Swim

Obelisk
Crystal Shield
Gold Helmet
Mythril Armor
Feather Mantle

Level Jump3, Vertical Jump2
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Phoenix Down

Final Bets: red - 7 bets for 3,594G (37.3%, x1.68); blue - 11 bets for 6,043G (62.7%, x0.59)

red bets:
CT_5_Holy: 1,262G (35.1%, 1,262G)
Lydian_C: 1,200G (33.4%, 3,275G)
getthemoneyz: 674G (18.8%, 1,672,526G)
AllInBot: 153G (4.3%, 153G)
Lemonjohns: 105G (2.9%, 7,824G)
TsukikageRyu: 100G (2.8%, 4,017G)
mirapoix: 100G (2.8%, 3,321G)

blue bets:
CassiePhoenix: 1,438G (23.8%, 1,438G)
NovaKnight21: 1,000G (16.5%, 3,422G)
BirbBrainsBot: 1,000G (16.5%, 50,773G)
Sairentozon7: 1,000G (16.5%, 25,524G)
Lanshaft: 600G (9.9%, 2,377G)
gorgewall: 301G (5.0%, 4,811G)
datadrivenbot: 200G (3.3%, 64,879G)
DeathTaxesAndAnime: 200G (3.3%, 1,955G)
3ngag3: 104G (1.7%, 1,004G)
MinBetBot: 100G (1.7%, 6,803G)
ko2q: 100G (1.7%, 3,290G)

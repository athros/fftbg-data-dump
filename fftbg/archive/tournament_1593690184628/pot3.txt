Final Bets: white - 8 bets for 4,198G (43.2%, x1.31); black - 8 bets for 5,516G (56.8%, x0.76)

white bets:
getthemoneyz: 1,000G (23.8%, 1,158,301G)
Valentine009: 1,000G (23.8%, 4,069G)
cam_ATS: 999G (23.8%, 26,444G)
sparker9: 400G (9.5%, 13,839G)
SaltiestMage: 300G (7.1%, 3,624G)
BirbBrainsBot: 299G (7.1%, 126,817G)
DuneMeta: 100G (2.4%, 7,092G)
datadrivenbot: 100G (2.4%, 51,825G)

black bets:
red__lancer: 2,000G (36.3%, 107,124G)
Dexsana: 925G (16.8%, 925G)
gorgewall: 822G (14.9%, 822G)
NIghtdew14: 750G (13.6%, 14,842G)
TheChainNerd: 655G (11.9%, 6,552G)
amiture: 164G (3.0%, 164G)
nhammen: 100G (1.8%, 9,431G)
CosmicTactician: 100G (1.8%, 49,236G)

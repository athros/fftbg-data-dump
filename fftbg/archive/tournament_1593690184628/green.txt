Player: !Green
Team: Green Team
Palettes: Green/White



ALY327
Male
Aries
68
58
Oracle
Battle Skill
Dragon Spirit
Martial Arts
Lava Walking



Triangle Hat
Silk Robe
Feather Mantle

Poison, Spell Absorb, Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep
Armor Break, Weapon Break, Magic Break, Mind Break, Surging Sword



Cam ATS
Female
Libra
63
65
Time Mage
White Magic
Caution
Secret Hunt
Swim

Mace of Zeus

Triangle Hat
Power Sleeve
Wizard Mantle

Haste 2, Slow, Demi, Demi 2, Stabilize Time, Galaxy Stop
Cure, Protect, Wall, Esuna



ForagerCats
Monster
Sagittarius
60
66
Red Chocobo










SaltiestMage
Female
Taurus
47
78
Wizard
Summon Magic
Speed Save
Concentrate
Move+1

Air Knife

Thief Hat
Black Robe
Wizard Mantle

Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Frog
Moogle, Titan, Golem, Carbunkle, Salamander

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Hyvi
Male
Scorpio
69
54
Bard
Basic Skill
Critical Quick
Equip Gun
Move+3

Mythril Gun

Feather Hat
Clothes
Power Wrist

Battle Song, Magic Song, Last Song, Diamond Blade, Hydra Pit
Accumulate, Dash, Throw Stone, Heal



Waterwatereverywhere
Monster
Aries
79
73
Swine










R Raynos
Male
Libra
48
71
Samurai
Basic Skill
HP Restore
Short Charge
Waterwalking

Javelin

Cross Helmet
Wizard Robe
Magic Ring

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Throw Stone, Heal, Wish



Uyjhm
Male
Leo
67
70
Archer
Basic Skill
Hamedo
Equip Bow
Retreat

Lightning Bow

Red Hood
Earth Clothes
Defense Ring

Charge+1, Charge+5, Charge+7
Throw Stone, Tickle, Wish, Ultima

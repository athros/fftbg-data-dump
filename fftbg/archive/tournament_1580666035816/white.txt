Player: !White
Team: White Team
Palettes: White/Blue



MoonSlayerRS
Female
Leo
62
60
Lancer
Yin Yang Magic
Critical Quick
Equip Polearm
Lava Walking

Javelin
Flame Shield
Cross Helmet
Carabini Mail
Germinas Boots

Level Jump8, Vertical Jump5
Dispel Magic



Irohma
Male
Aries
67
51
Chemist
Sing
Faith Up
Maintenance
Jump+2

Panther Bag

Leather Hat
Power Sleeve
Leather Mantle

Potion, X-Potion, Ether, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Life Song, Magic Song, Nameless Song, Diamond Blade, Space Storage, Hydra Pit



Artea
Male
Capricorn
79
39
Archer
Elemental
Catch
Concentrate
Ignore Height

Bow Gun
Escutcheon
Holy Miter
Judo Outfit
Feather Boots

Charge+3, Charge+4, Charge+5, Charge+7
Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Tactic Fanatic
Female
Leo
52
53
Archer
Battle Skill
HP Restore
Short Status
Waterwalking

Silver Bow

Flash Hat
Wizard Outfit
Jade Armlet

Charge+1, Charge+5
Armor Break, Speed Break, Stasis Sword, Dark Sword

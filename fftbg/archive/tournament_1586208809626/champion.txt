Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Pwlloth
Male
Taurus
74
52
Monk
White Magic
Mana Shield
Defend
Move+1



Triangle Hat
Power Sleeve
Battle Boots

Pummel, Secret Fist, Purification, Revive, Seal Evil
Cure, Cure 2, Cure 3, Raise, Esuna



Nova Riety
Female
Aries
56
47
Dancer
Draw Out
Distribute
Magic Attack UP
Swim

Cashmere

Twist Headband
Power Sleeve
Small Mantle

Witch Hunt, Wiznaibus, Nether Demon
Bizen Boat, Heaven's Cloud



DudeMonkey77
Male
Aquarius
62
45
Priest
Talk Skill
Absorb Used MP
Monster Talk
Waterbreathing

Gold Staff

Black Hood
Robe of Lords
Spike Shoes

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna
Invitation, Preach, Mimic Daravon, Refute



Bryan792
Female
Serpentarius
60
51
Geomancer
Draw Out
Distribute
Maintenance
Jump+2

Battle Axe
Flame Shield
Triangle Hat
Secret Clothes
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Asura, Heaven's Cloud

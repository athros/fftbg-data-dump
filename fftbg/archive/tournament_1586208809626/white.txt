Player: !White
Team: White Team
Palettes: White/Blue



Daveb
Male
Cancer
42
72
Ninja
Yin Yang Magic
MA Save
Equip Knife
Fly

Assassin Dagger
Flail
Thief Hat
Chain Vest
Salty Rage

Sword
Blind, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify



MrFlabyo
Monster
Scorpio
46
50
Steel Giant










Firesheath
Female
Aquarius
71
67
Summoner
Dance
Abandon
Equip Polearm
Ignore Terrain

Ivory Rod

Black Hood
Light Robe
Angel Ring

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Leviathan, Salamander, Silf, Fairy
Wiznaibus, Slow Dance, Disillusion



DrAntiSocial
Female
Virgo
61
56
Squire
Battle Skill
Caution
Halve MP
Jump+3

Materia Blade
Ice Shield
Black Hood
Mystic Vest
Reflect Ring

Heal, Scream
Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Solomongrundy85
Female
Sagittarius
72
77
Summoner
Basic Skill
Meatbone Slash
Short Charge
Lava Walking

Bestiary

Holy Miter
Light Robe
Rubber Shoes

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Lich, Cyclops
Throw Stone, Heal, Fury, Wish



CT 5 Holy
Female
Taurus
78
43
Geomancer
Punch Art
MA Save
Dual Wield
Ignore Terrain

Asura Knife
Kikuichimoji
Triangle Hat
Brigandine
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Secret Fist, Purification, Chakra, Seal Evil



LeoNightFury
Male
Cancer
49
45
Squire
Black Magic
Catch
Magic Attack UP
Ignore Height

Air Knife
Mythril Shield
Flash Hat
Mythril Vest
Power Wrist

Dash, Throw Stone, Heal, Fury
Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Ice 4



Skillomono
Female
Aries
68
66
Wizard
Punch Art
Auto Potion
Short Charge
Swim

Mythril Knife

Twist Headband
Secret Clothes
Battle Boots

Fire 4, Ice 2, Empower
Spin Fist, Pummel, Purification, Revive

Final Bets: green - 15 bets for 7,924G (34.2%, x1.93); champion - 27 bets for 15,276G (65.8%, x0.52)

green bets:
kai_shee: 1,001G (12.6%, 98,335G)
ungabunga_bot: 1,000G (12.6%, 70,923G)
BirbBrainsBot: 1,000G (12.6%, 274,297G)
Lord_Burrah: 1,000G (12.6%, 88,158G)
EnemyController: 600G (7.6%, 29,284G)
Baron_von_Scrub: 555G (7.0%, 11,514G)
DavenIII: 550G (6.9%, 28,546G)
Bryon_W: 500G (6.3%, 36,921G)
mivvim: 450G (5.7%, 1,586G)
AllInBot: 430G (5.4%, 430G)
fenaen: 300G (3.8%, 5,728G)
ZephyrTempest: 238G (3.0%, 25,937G)
SeedSC: 100G (1.3%, 141,884G)
Firesheath: 100G (1.3%, 3,992G)
Mrasin: 100G (1.3%, 8,455G)

champion bets:
leakimiko: 1,692G (11.1%, 33,858G)
DeathTaxesAndAnime: 1,393G (9.1%, 1,393G)
BenYuPoker: 1,164G (7.6%, 2,328G)
Lodrak: 1,156G (7.6%, 1,156G)
TheChainNerd: 1,000G (6.5%, 14,144G)
ugoplatamia: 1,000G (6.5%, 28,806G)
robespyah: 994G (6.5%, 994G)
catfashions: 655G (4.3%, 655G)
Lanshaft: 600G (3.9%, 11,014G)
Laserman1000: 600G (3.9%, 13,200G)
Zeroroute: 600G (3.9%, 600G)
JustSuperish: 564G (3.7%, 564G)
JumbocactuarX27: 500G (3.3%, 2,330G)
OneHundredFists: 500G (3.3%, 1,432G)
kingchadking: 436G (2.9%, 436G)
rubenflonne: 368G (2.4%, 368G)
Draconis345: 332G (2.2%, 332G)
Cataphract116: 300G (2.0%, 7,543G)
ColetteMSLP: 300G (2.0%, 13,539G)
ShintaroNayaka: 300G (2.0%, 3,108G)
mexskacin: 220G (1.4%, 220G)
joewcarson: 132G (0.9%, 132G)
kami_39: 120G (0.8%, 120G)
GrayGhostGaming: 100G (0.7%, 1,989G)
datadrivenbot: 100G (0.7%, 23,516G)
Ausuri: 100G (0.7%, 1,734G)
GingerDynomite: 50G (0.3%, 100G)

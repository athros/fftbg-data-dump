Player: !Green
Team: Green Team
Palettes: Green/White



DudeMonkey77
Female
Scorpio
77
55
Summoner
Dance
Parry
Equip Armor
Lava Walking

Wizard Rod

Headgear
Bronze Armor
Rubber Shoes

Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Silf
Slow Dance, Disillusion



Sotehr
Female
Gemini
79
52
Summoner
Punch Art
Parry
Short Charge
Move-MP Up

White Staff

Holy Miter
Adaman Vest
Sprint Shoes

Ramuh, Titan, Fairy
Pummel, Secret Fist, Purification, Revive, Seal Evil



Chronoxtrigger
Male
Virgo
54
55
Lancer
Battle Skill
Parry
Dual Wield
Lava Walking

Holy Lance
Gokuu Rod
Crystal Helmet
Black Robe
Germinas Boots

Level Jump3, Vertical Jump7
Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Stasis Sword



ZZ Yoshi
Female
Cancer
45
42
Knight
Talk Skill
Earplug
Sicken
Ignore Height

Giant Axe
Escutcheon
Genji Helmet
Chameleon Robe
Sprint Shoes

Shield Break, Weapon Break, Power Break
Persuade, Praise, Threaten, Negotiate, Refute, Rehabilitate

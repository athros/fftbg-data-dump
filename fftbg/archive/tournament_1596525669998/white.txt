Player: !White
Team: White Team
Palettes: White/Blue



Lythe Caraker
Female
Sagittarius
74
69
Time Mage
Elemental
Sunken State
Long Status
Fly

Gold Staff

Black Hood
Silk Robe
Magic Ring

Slow, Stop, Immobilize, Float
Pitfall, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



Douchetron
Female
Aries
53
64
Mime

Distribute
Defend
Move+2



Twist Headband
Clothes
Dracula Mantle

Mimic




Legitimized
Female
Cancer
80
60
Wizard
Basic Skill
Counter
Equip Sword
Teleport

Broad Sword

Barette
Adaman Vest
108 Gems

Ice 3
Accumulate, Heal, Tickle, Fury



LordTomS
Female
Capricorn
68
68
Knight
Black Magic
Dragon Spirit
Equip Knife
Levitate

Ninja Edge
Bronze Shield
Barbuta
Leather Armor
Spike Shoes

Armor Break, Shield Break, Power Break, Dark Sword
Fire, Ice, Ice 2, Empower, Frog, Death, Flare

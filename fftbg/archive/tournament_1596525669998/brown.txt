Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fattunaking
Male
Sagittarius
50
40
Monk
Black Magic
Hamedo
Attack UP
Move+3



Holy Miter
Chain Vest
Small Mantle

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Fire 3, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Ice 4, Death



Sinnyil2
Male
Gemini
68
45
Monk
Steal
Counter Tackle
Attack UP
Fly



Twist Headband
Black Costume
Bracer

Spin Fist, Earth Slash, Purification, Chakra, Revive
Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory



Wooplestein
Female
Gemini
63
75
Knight
Elemental
Auto Potion
Halve MP
Jump+3

Giant Axe
Ice Shield
Circlet
Bronze Armor
Feather Boots

Head Break, Armor Break, Shield Break, Stasis Sword
Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Lava Ball



Firesheath
Male
Serpentarius
60
76
Mime

Regenerator
Short Status
Jump+1



Twist Headband
Judo Outfit
Germinas Boots

Mimic


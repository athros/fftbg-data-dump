Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



RjA0zcOQ96
Female
Taurus
55
77
Oracle
Time Magic
MA Save
Doublehand
Jump+2

Iron Fan

Twist Headband
Wizard Robe
Magic Ring

Blind, Spell Absorb, Pray Faith, Zombie, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Slow, Immobilize, Stabilize Time, Meteor



Kyune
Female
Aquarius
51
53
Priest
Jump
HP Restore
Short Charge
Waterbreathing

Rainbow Staff

Triangle Hat
Adaman Vest
Dracula Mantle

Cure 4, Reraise, Protect, Esuna, Holy, Magic Barrier
Level Jump5, Vertical Jump6



Helpimabug
Female
Aquarius
59
54
Summoner
Yin Yang Magic
Abandon
Short Charge
Waterbreathing

Flame Rod

Golden Hairpin
Chain Vest
Jade Armlet

Moogle, Ramuh, Titan, Fairy, Lich
Blind, Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep



KabukiJake
Male
Leo
43
52
Squire
Talk Skill
Distribute
Dual Wield
Jump+2

Rune Blade
Giant Axe
Iron Helmet
Judo Outfit
Magic Ring

Heal, Fury
Invitation, Persuade, Preach, Negotiate, Refute

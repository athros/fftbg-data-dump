Player: !Black
Team: Black Team
Palettes: Black/Red



Coolguye
Female
Virgo
42
40
Monk
Throw
Distribute
Equip Sword
Jump+1

Save the Queen

Headgear
Clothes
Rubber Shoes

Spin Fist, Earth Slash, Chakra, Revive
Shuriken, Bomb



OpHendoslice
Male
Cancer
54
68
Samurai
Yin Yang Magic
Auto Potion
Doublehand
Move-HP Up

Asura Knife

Gold Helmet
Chameleon Robe
Germinas Boots

Koutetsu, Murasame, Kikuichimoji
Blind, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Dispel Magic



Vorap
Male
Taurus
78
69
Lancer
Steal
Regenerator
Defense UP
Fly

Mythril Spear
Mythril Shield
Bronze Helmet
Chameleon Robe
Sprint Shoes

Level Jump8, Vertical Jump5
Gil Taking



HASTERIOUS
Male
Aries
53
67
Thief
Talk Skill
Absorb Used MP
Dual Wield
Move-MP Up

Air Knife
Mythril Knife
Feather Hat
Mythril Vest
Power Wrist

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Invitation, Praise, Preach, Insult, Mimic Daravon, Refute, Rehabilitate

Final Bets: red - 8 bets for 7,626G (88.2%, x0.13); blue - 6 bets for 1,016G (11.8%, x7.51)

red bets:
Digitalsocrates: 3,000G (39.3%, 41,793G)
serperemagus: 1,602G (21.0%, 1,602G)
reinoe: 869G (11.4%, 869G)
rednecknazgul: 819G (10.7%, 819G)
Kaynin_Shadowwalker: 587G (7.7%, 587G)
nok____: 300G (3.9%, 1,800G)
CorpusCav: 299G (3.9%, 4,999G)
Coolguye: 150G (2.0%, 2,982G)

blue bets:
getthemoneyz: 268G (26.4%, 992,751G)
redrhinofever: 200G (19.7%, 2,731G)
prince_rogers_nelson_: 200G (19.7%, 2,423G)
BirbBrainsBot: 148G (14.6%, 127,481G)
Firesheath: 100G (9.8%, 12,991G)
Neo_Exodus: 100G (9.8%, 620G)

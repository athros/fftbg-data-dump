Final Bets: green - 7 bets for 3,686G (42.6%, x1.35); yellow - 6 bets for 4,976G (57.4%, x0.74)

green bets:
BlackFireUK: 1,000G (27.1%, 42,846G)
BirbBrainsBot: 1,000G (27.1%, 128,592G)
reinoe: 600G (16.3%, 600G)
getthemoneyz: 382G (10.4%, 994,763G)
rednecknazgul: 304G (8.2%, 304G)
Lythe_Caraker: 250G (6.8%, 109,918G)
Coolguye: 150G (4.1%, 1,832G)

yellow bets:
Digitalsocrates: 4,000G (80.4%, 38,793G)
serperemagus: 468G (9.4%, 468G)
redrhinofever: 200G (4.0%, 3,232G)
Kaynin_Shadowwalker: 108G (2.2%, 108G)
CorpusCav: 100G (2.0%, 4,700G)
Neo_Exodus: 100G (2.0%, 1,371G)

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Chuckolator
Monster
Taurus
79
46
Behemoth










Mpghappiness
Male
Libra
80
58
Time Mage
Jump
Counter Magic
Secret Hunt
Move-HP Up

Ivory Rod

Holy Miter
White Robe
Defense Armlet

Haste 2, Slow, Stop, Immobilize, Reflect, Demi 2
Level Jump4, Vertical Jump5



GrandmasterFrankerZ
Female
Virgo
77
44
Geomancer
Draw Out
Critical Quick
Defend
Waterbreathing

Kikuichimoji
Escutcheon
Flash Hat
Black Robe
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Bizen Boat, Muramasa



BlackFireUK
Female
Virgo
61
52
Knight
White Magic
Hamedo
Magic Attack UP
Ignore Height

Chaos Blade
Crystal Shield
Circlet
Gold Armor
Jade Armlet

Armor Break, Shield Break, Mind Break, Explosion Sword
Cure 3, Raise, Regen, Protect, Esuna

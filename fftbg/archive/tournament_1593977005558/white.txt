Player: !White
Team: White Team
Palettes: White/Blue



ArchKnightX
Male
Cancer
58
58
Priest
Summon Magic
Catch
Equip Polearm
Levitate

Javelin

Flash Hat
Wizard Robe
Angel Ring

Raise
Ramuh, Titan, Salamander



DeathTaxesAndAnime
Female
Virgo
57
74
Lancer
Basic Skill
Parry
Doublehand
Jump+2

Javelin

Circlet
Genji Armor
Dracula Mantle

Level Jump5, Vertical Jump4
Accumulate, Dash, Throw Stone, Heal, Fury, Wish



TheMurkGnome
Male
Scorpio
75
58
Oracle
Talk Skill
Counter
Sicken
Waterbreathing

Musk Rod

Leather Hat
Mythril Vest
Feather Boots

Blind, Poison, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze
Threaten, Preach, Solution, Death Sentence, Insult, Rehabilitate



Shalloween
Male
Libra
72
65
Chemist
Draw Out
Mana Shield
Sicken
Waterbreathing

Blind Knife

Black Hood
Black Costume
Spike Shoes

Potion, X-Potion, Antidote, Maiden's Kiss, Phoenix Down
Heaven's Cloud, Kiyomori, Muramasa, Chirijiraden

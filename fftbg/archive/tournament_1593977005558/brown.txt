Player: !Brown
Team: Brown Team
Palettes: Brown/Green



EizanTayama
Female
Taurus
73
59
Archer
Steal
Counter
Defend
Levitate

Mythril Gun
Diamond Shield
Barette
Clothes
Dracula Mantle

Charge+1, Charge+2, Charge+7, Charge+10
Steal Armor, Steal Shield, Steal Status, Arm Aim, Leg Aim



Grandlanzer
Female
Libra
62
79
Archer
Yin Yang Magic
Abandon
Martial Arts
Move+1

Mythril Bow

Cachusha
Secret Clothes
N-Kai Armlet

Charge+1, Charge+3, Charge+10
Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy



Miku Shikhu
Female
Pisces
73
60
Calculator
White Magic
Brave Save
Halve MP
Swim

Gokuu Rod

Red Hood
Linen Robe
Feather Mantle

CT, Prime Number, 5
Cure, Cure 2, Cure 3, Raise, Esuna



Actual JP
Female
Aries
57
81
Monk
Yin Yang Magic
Auto Potion
Maintenance
Move-HP Up



Feather Hat
Rubber Costume
Angel Ring

Purification, Revive
Blind, Zombie, Blind Rage, Foxbird, Paralyze

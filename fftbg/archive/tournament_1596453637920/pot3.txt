Final Bets: white - 5 bets for 6,653G (27.8%, x2.59); black - 14 bets for 17,236G (72.2%, x0.39)

white bets:
NIghtdew14: 3,000G (45.1%, 12,234G)
E_Ballard: 2,228G (33.5%, 2,228G)
douchetron: 1,028G (15.5%, 2,017G)
getthemoneyz: 290G (4.4%, 1,495,347G)
EmmaEnema: 107G (1.6%, 2,507G)

black bets:
AllInBot: 3,961G (23.0%, 3,961G)
maximumcrit: 3,000G (17.4%, 6,881G)
Mesmaster: 2,000G (11.6%, 18,617G)
Error72: 1,842G (10.7%, 1,842G)
Dexsana: 1,720G (10.0%, 1,720G)
VolgraTheMoose: 1,001G (5.8%, 14,038G)
ColetteMSLP: 1,000G (5.8%, 14,223G)
BirbBrainsBot: 1,000G (5.8%, 181,941G)
superdevon1: 571G (3.3%, 28,577G)
letdowncity: 492G (2.9%, 14,134G)
DouglasDragonThePoet: 200G (1.2%, 712G)
datadrivenbot: 200G (1.2%, 48,353G)
xBizzy: 148G (0.9%, 148G)
gorgewall: 101G (0.6%, 7,167G)

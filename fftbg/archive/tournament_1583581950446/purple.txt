Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



NovaKnight21
Male
Leo
49
48
Oracle
Sing
Counter
Sicken
Jump+3

Octagon Rod

Triangle Hat
Linen Robe
Magic Ring

Blind, Spell Absorb, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Life Song, Cheer Song, Last Song, Hydra Pit



NoxeGS
Monster
Gemini
46
54
Grenade










Bryan792
Female
Aries
52
51
Geomancer
Draw Out
Counter
Equip Polearm
Move+1

Dragon Whisker
Genji Shield
Flash Hat
Adaman Vest
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bizen Boat, Murasame, Heaven's Cloud



Ominnous
Male
Scorpio
40
77
Archer
Throw
Sunken State
Doublehand
Retreat

Blast Gun

Mythril Helmet
Black Costume
Magic Ring

Charge+3, Charge+4, Charge+10
Staff

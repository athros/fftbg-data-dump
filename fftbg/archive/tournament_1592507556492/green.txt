Player: !Green
Team: Green Team
Palettes: Green/White



ApplesauceBoss
Male
Scorpio
77
62
Bard
Talk Skill
Catch
Doublehand
Jump+2

Fairy Harp

Flash Hat
Plate Mail
Reflect Ring

Angel Song, Magic Song, Last Song
Invitation, Praise, Negotiate



Anasetsuken
Female
Virgo
73
66
Oracle
Jump
Counter
Long Status
Move+3

Bestiary

Green Beret
Mythril Vest
Magic Ring

Blind, Poison, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Sleep
Level Jump5, Vertical Jump5



L2 Sentinel
Monster
Gemini
72
70
Tiamat










MrFlabyo
Female
Taurus
60
55
Time Mage
Elemental
PA Save
Short Status
Move-HP Up

Healing Staff

Black Hood
White Robe
Sprint Shoes

Haste, Haste 2, Slow 2, Float, Reflect, Demi 2, Stabilize Time
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

Player: !Green
Team: Green Team
Palettes: Green/White



Snowfats
Female
Scorpio
54
42
Mime

Sunken State
Equip Shield
Move+1


Crystal Shield
Twist Headband
Brigandine
Magic Gauntlet

Mimic




Volgrathemoose
Female
Leo
59
42
Summoner
Item
MP Restore
Short Charge
Jump+3

Wizard Staff

Leather Hat
Adaman Vest
Elf Mantle

Shiva, Ramuh, Carbunkle, Leviathan, Silf, Fairy
Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down



CapnChaos12
Male
Aquarius
61
56
Monk
Draw Out
Brave Up
Magic Attack UP
Ignore Terrain



Red Hood
Leather Outfit
Jade Armlet

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Koutetsu, Bizen Boat, Murasame



Roqqqpsi
Male
Aries
55
48
Archer
Battle Skill
Caution
Beastmaster
Move+3

Lightning Bow

Crystal Helmet
Leather Outfit
Bracer

Charge+1, Charge+2, Charge+4
Armor Break, Mind Break, Night Sword

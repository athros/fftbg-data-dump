Player: !Green
Team: Green Team
Palettes: Green/White



Volgrathemoose
Female
Gemini
62
54
Summoner
Elemental
Mana Shield
Short Charge
Teleport 2

Poison Rod

Feather Hat
Wizard Outfit
Small Mantle

Moogle, Ramuh, Golem, Carbunkle, Bahamut, Odin, Leviathan
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



TheMurkGnome
Male
Scorpio
41
48
Oracle
Item
Counter Tackle
Throw Item
Jump+3

Iron Fan

Headgear
Mythril Vest
Battle Boots

Poison, Life Drain, Doubt Faith, Silence Song, Blind Rage, Foxbird, Sleep
Potion, Hi-Ether, Eye Drop, Remedy, Phoenix Down



NWOW 44
Monster
Capricorn
46
76
Cockatrice










DudeMonkey77
Female
Gemini
67
60
Knight
Yin Yang Magic
PA Save
Equip Bow
Ignore Terrain

Poison Bow
Crystal Shield
Circlet
Carabini Mail
Defense Armlet

Head Break, Magic Break, Speed Break, Justice Sword, Surging Sword
Blind, Zombie, Foxbird

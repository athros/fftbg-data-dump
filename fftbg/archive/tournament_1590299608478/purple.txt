Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheMM42
Female
Leo
73
59
Dancer
Punch Art
Counter Magic
Doublehand
Teleport

Ryozan Silk

Red Hood
Chain Vest
Feather Mantle

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Last Dance
Purification, Revive



Passermine
Male
Virgo
44
78
Mime

Regenerator
Defense UP
Waterbreathing



Triangle Hat
Diamond Armor
Jade Armlet

Mimic




Moocaotao
Male
Capricorn
65
75
Monk
Basic Skill
Mana Shield
Equip Axe
Ignore Terrain

White Staff

Triangle Hat
Mythril Vest
Magic Ring

Pummel, Purification, Revive
Accumulate, Dash, Cheer Up



HaplessOne
Female
Libra
73
76
Calculator
Time Magic
Faith Save
Defense UP
Move-HP Up

Battle Folio

Holy Miter
Black Robe
N-Kai Armlet

CT, Height, Prime Number, 5, 3
Haste, Slow, Stop, Immobilize, Float, Quick, Demi, Stabilize Time

Player: !Black
Team: Black Team
Palettes: Black/Red



KasugaiRoastedPeas
Monster
Leo
53
71
Ghoul










Heroebal
Male
Aquarius
51
77
Ninja
Summon Magic
Parry
Equip Gun
Move-MP Up

Battle Folio
Papyrus Codex
Triangle Hat
Rubber Costume
Magic Gauntlet

Shuriken, Knife, Dictionary
Shiva, Titan, Golem, Bahamut, Silf



Rabbitlogik
Female
Taurus
45
51
Calculator
Yin Yang Magic
Critical Quick
Secret Hunt
Move-HP Up

Gokuu Rod

Green Beret
White Robe
Cursed Ring

CT, Height, Prime Number, 3
Blind, Poison, Spell Absorb, Life Drain, Confusion Song, Dispel Magic, Petrify



Gooseyourself
Male
Aries
70
54
Oracle
Elemental
Auto Potion
Doublehand
Ignore Height

Whale Whisker

Triangle Hat
Silk Robe
Vanish Mantle

Blind, Poison, Confusion Song, Dispel Magic
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball

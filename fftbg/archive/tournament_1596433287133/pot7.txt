Final Bets: red - 9 bets for 4,487G (43.0%, x1.33); brown - 10 bets for 5,948G (57.0%, x0.75)

red bets:
Azzy_Omega: 936G (20.9%, 936G)
Strifu: 910G (20.3%, 910G)
DeathTaxesAndAnime: 848G (18.9%, 848G)
randgridr: 600G (13.4%, 600G)
DAC169: 432G (9.6%, 432G)
ShanksMcShiv: 305G (6.8%, 305G)
datadrivenbot: 200G (4.5%, 47,628G)
Phillippus: 156G (3.5%, 156G)
moonliquor: 100G (2.2%, 47,541G)

brown bets:
Nizaha: 1,300G (21.9%, 11,432G)
BirbBrainsBot: 1,000G (16.8%, 175,285G)
LDHaten: 1,000G (16.8%, 13,895G)
reinoe: 1,000G (16.8%, 66,504G)
AllInBot: 556G (9.3%, 556G)
Laserman1000: 404G (6.8%, 2,504G)
rednecknazgul: 200G (3.4%, 3,139G)
AugnosMusic: 200G (3.4%, 1,266G)
getthemoneyz: 188G (3.2%, 1,492,558G)
nemuineru: 100G (1.7%, 1,200G)

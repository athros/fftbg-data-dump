Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Female
Virgo
50
63
Ninja
Elemental
Dragon Spirit
Secret Hunt
Move+1

Flame Whip
Main Gauche
Barette
Mythril Vest
Wizard Mantle

Shuriken
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Fenixcrest
Female
Taurus
50
44
Knight
Charge
Absorb Used MP
Defend
Waterbreathing

Diamond Sword
Buckler
Mythril Helmet
Wizard Robe
Bracer

Head Break, Armor Break, Weapon Break, Speed Break, Justice Sword
Charge+1, Charge+2, Charge+5, Charge+7



OneHundredFists
Monster
Leo
59
42
Holy Dragon










Lowlf
Female
Cancer
47
71
Priest
Summon Magic
Damage Split
Concentrate
Move+3

Morning Star

Thief Hat
Chameleon Robe
Genji Gauntlet

Cure 3, Cure 4, Protect 2, Shell, Esuna
Moogle, Shiva, Titan, Leviathan, Salamander, Lich

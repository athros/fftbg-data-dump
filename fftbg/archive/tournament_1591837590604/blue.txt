Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rislyeu
Female
Pisces
52
57
Mime

Counter Magic
Short Status
Lava Walking



Leather Hat
Brigandine
Feather Mantle

Mimic




Thyrandaal
Female
Aries
46
45
Knight
Basic Skill
Damage Split
Maintenance
Fly

Ice Brand
Diamond Shield
Leather Helmet
Black Robe
Rubber Shoes

Speed Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Dash, Throw Stone, Heal, Yell, Wish



Ar Tactic
Female
Aquarius
51
61
Ninja
Talk Skill
PA Save
Monster Talk
Levitate

Short Edge
Short Edge
Red Hood
Wizard Outfit
Small Mantle

Shuriken, Knife, Staff
Persuade, Praise, Death Sentence, Mimic Daravon, Refute



LDSkinny
Female
Taurus
64
75
Oracle
Throw
Critical Quick
Equip Armor
Jump+2

Octagon Rod

Mythril Helmet
Diamond Armor
Feather Mantle

Blind, Poison, Spell Absorb, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Staff

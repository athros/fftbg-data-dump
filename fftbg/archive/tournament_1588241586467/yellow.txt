Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Bryan792
Female
Scorpio
56
49
Wizard
White Magic
Abandon
Short Charge
Lava Walking

Dragon Rod

Triangle Hat
Black Costume
Red Shoes

Fire 2, Fire 4, Bolt 2, Ice, Ice 3
Cure 2, Raise, Raise 2, Reraise, Protect, Wall, Esuna, Holy



Goust18
Female
Taurus
65
59
Wizard
Time Magic
Catch
Short Charge
Teleport

Poison Rod

Black Hood
Black Robe
Defense Ring

Fire 4, Bolt, Bolt 2, Ice, Ice 2, Ice 3, Ice 4, Frog, Flare
Stop, Float, Demi, Demi 2, Stabilize Time



Ko2q
Male
Cancer
51
80
Thief
Sing
Counter Flood
Maintenance
Teleport 2

Air Knife

Golden Hairpin
Black Costume
Sprint Shoes

Gil Taking, Steal Heart, Steal Accessory
Life Song, Cheer Song, Nameless Song



Anethum
Female
Gemini
50
55
Squire
White Magic
Arrow Guard
Beastmaster
Jump+2

Orichalcum
Crystal Shield
Cachusha
Clothes
Rubber Shoes

Heal, Cheer Up, Scream
Raise, Shell 2, Esuna, Holy

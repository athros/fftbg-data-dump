Player: !White
Team: White Team
Palettes: White/Blue



HASTERIOUS
Male
Cancer
53
65
Mediator
Jump
Speed Save
Short Charge
Retreat

Stone Gun

Feather Hat
Leather Outfit
Battle Boots

Persuade, Threaten, Mimic Daravon, Refute
Level Jump8, Vertical Jump8



Superneol
Male
Scorpio
57
38
Samurai
Punch Art
Sunken State
Equip Gun
Move+2

Battle Folio

Bronze Helmet
Gold Armor
Magic Ring

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Earth Slash, Chakra, Revive



PatSouI
Male
Aquarius
77
58
Ninja
Steal
MP Restore
Equip Gun
Ignore Terrain

Papyrus Codex
Battle Folio
Red Hood
Earth Clothes
Magic Gauntlet

Bomb, Knife, Spear
Steal Helmet, Steal Status, Leg Aim



RubenFlonne
Female
Aquarius
57
42
Chemist
Summon Magic
Catch
Defend
Lava Walking

Romanda Gun

Golden Hairpin
Wizard Outfit
Small Mantle

X-Potion, Maiden's Kiss, Soft, Remedy, Phoenix Down
Moogle, Titan, Carbunkle, Odin

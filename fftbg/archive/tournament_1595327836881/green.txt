Player: !Green
Team: Green Team
Palettes: Green/White



Hasterious
Male
Aquarius
45
69
Monk
Jump
Arrow Guard
Dual Wield
Move+2



Headgear
Earth Clothes
Jade Armlet

Pummel, Earth Slash, Purification, Revive
Level Jump5, Vertical Jump8



Lastly
Female
Gemini
78
80
Mediator
Draw Out
Sunken State
Secret Hunt
Move+3

Battle Folio

Holy Miter
White Robe
Germinas Boots

Invitation, Praise, Refute, Rehabilitate
Koutetsu, Murasame, Kiyomori, Kikuichimoji



Ar Tactic
Female
Leo
79
60
Summoner
Dance
Mana Shield
Long Status
Levitate

Thunder Rod

Golden Hairpin
Robe of Lords
Feather Mantle

Moogle, Carbunkle, Silf, Fairy, Lich
Wiznaibus, Disillusion, Last Dance, Dragon Pit



0v3rr8d
Male
Pisces
67
47
Monk
Sing
Catch
Attack UP
Move-HP Up



Feather Hat
Clothes
Dracula Mantle

Earth Slash, Secret Fist, Revive
Magic Song, Nameless Song, Last Song, Sky Demon

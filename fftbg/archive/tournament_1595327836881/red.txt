Player: !Red
Team: Red Team
Palettes: Red/Brown



Dem0nj0ns
Male
Gemini
78
65
Monk
Throw
Critical Quick
Equip Shield
Swim


Crystal Shield
Holy Miter
Mystic Vest
Magic Ring

Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Shuriken



CassiePhoenix
Female
Taurus
39
70
Time Mage
Summon Magic
Counter Magic
Beastmaster
Ignore Terrain

White Staff

Triangle Hat
Leather Outfit
Diamond Armlet

Haste, Stop, Immobilize, Reflect
Moogle, Ramuh, Golem, Bahamut



Josephiroth 143
Female
Aries
68
50
Time Mage
Summon Magic
Absorb Used MP
Long Status
Retreat

Sage Staff

Green Beret
Clothes
Spike Shoes

Haste, Slow, Slow 2, Stop, Reflect, Demi 2
Moogle, Golem, Carbunkle, Silf, Fairy



NovaKnight21
Male
Aries
50
43
Archer
Basic Skill
Critical Quick
Defend
Ignore Terrain

Snipe Bow
Mythril Shield
Bronze Helmet
Mythril Vest
Power Wrist

Charge+2, Charge+3, Charge+4, Charge+5
Accumulate, Dash, Wish

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WhiteTigress
Female
Capricorn
49
46
Priest
Elemental
Catch
Beastmaster
Swim

Gold Staff

Triangle Hat
Light Robe
Genji Gauntlet

Cure, Raise, Regen, Esuna, Magic Barrier
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand



Jeeboheebo
Female
Pisces
54
58
Mediator
Jump
Speed Save
Equip Knife
Swim

Zorlin Shape

Twist Headband
Brigandine
Jade Armlet

Invitation, Persuade, Preach, Solution, Insult, Refute, Rehabilitate
Level Jump4, Vertical Jump6



LDSkinny
Female
Scorpio
78
69
Oracle
Basic Skill
Faith Save
Defend
Move+2

Battle Bamboo

Feather Hat
Linen Robe
Diamond Armlet

Blind, Spell Absorb, Doubt Faith, Zombie, Silence Song, Confusion Song, Paralyze, Petrify
Accumulate, Throw Stone, Heal, Tickle, Yell, Fury



Latebit
Male
Capricorn
68
76
Mediator
Time Magic
PA Save
Magic Attack UP
Jump+3

Bestiary

Headgear
Leather Outfit
Red Shoes

Persuade, Death Sentence, Insult, Mimic Daravon
Float, Reflect, Quick

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evdoggity
Female
Virgo
75
38
Time Mage
Talk Skill
Brave Save
Monster Talk
Jump+2

Wizard Staff

Triangle Hat
Chain Vest
Small Mantle

Haste, Slow 2, Float, Stabilize Time
Praise, Threaten, Refute



Toka222
Male
Capricorn
71
63
Geomancer
Time Magic
Counter Tackle
Magic Defense UP
Ignore Height

Giant Axe
Buckler
Holy Miter
Chameleon Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind
Haste, Immobilize, Quick, Demi, Stabilize Time



Daveb
Female
Cancer
61
51
Ninja
Time Magic
Auto Potion
Equip Sword
Move-HP Up

Ninja Edge
Broad Sword
Black Hood
Brigandine
Angel Ring

Shuriken, Bomb, Axe
Haste 2, Slow 2, Immobilize, Demi, Stabilize Time



Laserman1000
Male
Cancer
66
67
Calculator
Black Magic
Meatbone Slash
Defend
Swim

Thunder Rod

Twist Headband
Judo Outfit
N-Kai Armlet

CT, Height, Prime Number, 5, 3
Fire, Fire 3, Bolt 2, Bolt 4, Flare

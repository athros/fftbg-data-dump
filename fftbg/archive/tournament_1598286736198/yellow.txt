Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thyrandaal
Male
Sagittarius
62
66
Monk
Charge
Hamedo
Long Status
Lava Walking



Triangle Hat
Earth Clothes
Feather Boots

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Charge+1



SQUiDSQUARKLIN
Female
Leo
80
69
Mime

Mana Shield
Magic Defense UP
Move-HP Up



Cross Helmet
Power Sleeve
Dracula Mantle

Mimic




CapnChaos12
Female
Gemini
77
56
Ninja
Jump
Arrow Guard
Equip Gun
Lava Walking

Stone Gun
Glacier Gun
Holy Miter
Black Costume
Jade Armlet

Shuriken, Bomb
Level Jump8, Vertical Jump8



Chuckolator
Female
Leo
53
60
Calculator
White Magic
MA Save
Concentrate
Move+3

Battle Folio

Triangle Hat
White Robe
Sprint Shoes

CT, Height, Prime Number, 5, 4, 3
Cure 2, Raise, Reraise, Protect, Shell, Wall, Esuna, Holy

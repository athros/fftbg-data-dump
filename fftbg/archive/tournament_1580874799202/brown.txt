Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Shs
Male
Serpentarius
72
55
Chemist
Sing
Caution
Defend
Jump+3

Star Bag

Holy Miter
Judo Outfit
108 Gems

Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Phoenix Down
Angel Song, Life Song, Cheer Song, Last Song



Granbruhmomento
Male
Gemini
61
55
Mime

Catch
Secret Hunt
Retreat



Green Beret
Power Sleeve
Power Wrist

Mimic




Fenixcrest
Female
Leo
54
61
Priest
Steal
MA Save
Maintenance
Lava Walking

Flail

Thief Hat
Clothes
Dracula Mantle

Cure, Cure 2, Raise, Protect 2, Wall, Esuna
Steal Heart, Steal Armor, Arm Aim, Leg Aim



A Perfect Blue
Female
Aquarius
55
72
Geomancer
Draw Out
Absorb Used MP
Maintenance
Jump+3

Battle Axe
Crystal Shield
Red Hood
Wizard Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball
Kiyomori

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lijarkh
Female
Aquarius
71
49
Summoner
Throw
Counter Tackle
Halve MP
Move+2

Poison Rod

Black Hood
Clothes
Defense Ring

Moogle, Shiva, Carbunkle, Bahamut, Leviathan, Cyclops
Knife, Spear



MoonSlayerRS
Male
Leo
50
44
Geomancer
Punch Art
Damage Split
Attack UP
Jump+1

Giant Axe
Diamond Shield
Feather Hat
White Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Spin Fist, Pummel, Purification, Revive



Teg88
Male
Aquarius
46
45
Lancer
Throw
Blade Grasp
Beastmaster
Swim

Mythril Spear
Genji Shield
Leather Helmet
Gold Armor
N-Kai Armlet

Level Jump4, Vertical Jump8
Shuriken



Ko2q
Male
Pisces
71
71
Mediator
Battle Skill
Faith Up
Martial Arts
Lava Walking



Thief Hat
Black Costume
Wizard Mantle

Invitation, Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Head Break, Armor Break, Shield Break, Weapon Break, Justice Sword, Night Sword

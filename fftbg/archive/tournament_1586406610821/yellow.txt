Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



MagicBottle
Male
Scorpio
59
66
Squire
Throw
Arrow Guard
Equip Axe
Ignore Terrain

Morning Star
Ice Shield
Circlet
Judo Outfit
Power Wrist

Heal, Cheer Up, Wish
Shuriken, Knife



Faplo
Male
Scorpio
75
79
Lancer
Battle Skill
Absorb Used MP
Defend
Move+2

Dragon Whisker
Round Shield
Bronze Helmet
Leather Armor
Diamond Armlet

Level Jump2, Vertical Jump5
Head Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword



Waterwatereverywhere
Male
Leo
65
42
Archer
Yin Yang Magic
HP Restore
Magic Defense UP
Retreat

Mythril Bow

Leather Hat
Adaman Vest
Power Wrist

Charge+3, Charge+4, Charge+7
Blind Rage, Confusion Song, Dispel Magic



Oogthecaveman
Male
Taurus
68
48
Knight
Yin Yang Magic
Abandon
Defense UP
Move-MP Up

Coral Sword
Diamond Shield
Cross Helmet
Linen Cuirass
108 Gems

Speed Break, Power Break, Stasis Sword
Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic

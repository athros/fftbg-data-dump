Player: !Green
Team: Green Team
Palettes: Green/White



Laserman1000
Monster
Scorpio
79
54
Skeleton










DrAntiSocial
Female
Pisces
80
72
Mediator
Elemental
Damage Split
Beastmaster
Move+3

Bestiary

Green Beret
Adaman Vest
Feather Boots

Persuade, Death Sentence, Mimic Daravon
Water Ball, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Rytor
Monster
Aries
49
74
Revenant










B0shii
Male
Leo
70
74
Bard
Punch Art
Auto Potion
Dual Wield
Swim

Fairy Harp
Ramia Harp
Triangle Hat
Clothes
Rubber Shoes

Cheer Song, Battle Song, Nameless Song, Space Storage
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification

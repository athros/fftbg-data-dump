Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Deathmaker06
Female
Leo
63
61
Ninja
Dance
Catch
Equip Bow
Jump+2

Ultimus Bow

Headgear
Brigandine
Bracer

Bomb, Hammer, Staff
Wiznaibus, Slow Dance, Polka Polka, Last Dance, Dragon Pit



HuffFlex
Female
Capricorn
55
60
Geomancer
Yin Yang Magic
Distribute
Sicken
Retreat

Slasher
Platinum Shield
Triangle Hat
Mystic Vest
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Pray Faith, Foxbird, Dispel Magic



Brainstew29
Male
Sagittarius
38
41
Geomancer
Throw
Distribute
Equip Sword
Jump+3

Asura Knife
Platinum Shield
Black Hood
Wizard Outfit
Jade Armlet

Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shuriken



AugnosMusic
Male
Taurus
51
55
Thief
Draw Out
Counter Magic
Martial Arts
Lava Walking



Black Hood
Judo Outfit
Defense Ring

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Asura, Bizen Boat

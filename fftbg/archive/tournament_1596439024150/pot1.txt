Final Bets: red - 7 bets for 7,571G (55.6%, x0.80); blue - 7 bets for 6,044G (44.4%, x1.25)

red bets:
DeathTaxesAndAnime: 3,980G (52.6%, 7,804G)
gorgewall: 2,548G (33.7%, 2,548G)
Strifu: 276G (3.6%, 276G)
LAGBOT30000: 267G (3.5%, 5,359G)
cloud_d1: 200G (2.6%, 1,141G)
datadrivenbot: 200G (2.6%, 47,016G)
nemuineru: 100G (1.3%, 1,204G)

blue bets:
NicoSavoy: 1,996G (33.0%, 99,829G)
CT_5_Holy: 1,000G (16.5%, 3,543G)
getthemoneyz: 1,000G (16.5%, 1,492,207G)
BirbBrainsBot: 1,000G (16.5%, 175,584G)
douchetron: 581G (9.6%, 581G)
AllInBot: 344G (5.7%, 344G)
Lydian_C: 123G (2.0%, 11,073G)

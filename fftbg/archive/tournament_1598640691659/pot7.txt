Final Bets: yellow - 12 bets for 16,296G (49.1%, x1.04); black - 20 bets for 16,878G (50.9%, x0.97)

yellow bets:
JonnyCue: 8,000G (49.1%, 8,000G)
Aldrammech: 5,000G (30.7%, 36,753G)
Dasutin23: 1,000G (6.1%, 39,023G)
Runeseeker22: 600G (3.7%, 2,020G)
killth3kid: 544G (3.3%, 544G)
Smokegiant: 252G (1.5%, 252G)
resjudicata3: 200G (1.2%, 1,890G)
KasugaiRoastedPeas: 200G (1.2%, 9,446G)
old_overholt_: 200G (1.2%, 6,599G)
AllInBot: 100G (0.6%, 100G)
Leonidusx: 100G (0.6%, 1,050G)
krombobreaker: 100G (0.6%, 644G)

black bets:
Thyrandaal: 5,000G (29.6%, 97,929G)
SkylerBunny: 1,500G (8.9%, 7,577G)
Shalloween: 1,400G (8.3%, 41,992G)
MattMan119: 1,000G (5.9%, 17,092G)
ruleof5: 1,000G (5.9%, 22,612G)
BirbBrainsBot: 1,000G (5.9%, 82,926G)
Sairentozon7: 918G (5.4%, 918G)
DeathTaxesAndAnime: 884G (5.2%, 884G)
WhiteTigress: 800G (4.7%, 10,826G)
E_Ballard: 788G (4.7%, 67,549G)
SephDarkheart: 600G (3.6%, 165,401G)
thunderducker: 460G (2.7%, 460G)
dogsandcatsand: 456G (2.7%, 1,438G)
CosmicTactician: 200G (1.2%, 44,302G)
arumz: 200G (1.2%, 1,622G)
datadrivenbot: 200G (1.2%, 58,889G)
HS39: 194G (1.1%, 382G)
AbandonedHall: 144G (0.9%, 144G)
ko2q: 100G (0.6%, 4,373G)
getthemoneyz: 34G (0.2%, 1,758,992G)

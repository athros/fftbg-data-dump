Player: !Black
Team: Black Team
Palettes: Black/Red



Maakur
Monster
Cancer
68
75
Steel Giant










Omegasuspekt
Female
Capricorn
54
55
Time Mage
Draw Out
Abandon
Equip Polearm
Move+3

Spear

Thief Hat
Silk Robe
Germinas Boots

Haste, Slow 2, Immobilize, Quick, Stabilize Time, Galaxy Stop
Koutetsu, Bizen Boat



Egrazor
Female
Virgo
54
65
Squire
Steal
Counter Tackle
Secret Hunt
Jump+1

Long Sword

Golden Hairpin
Wizard Robe
Sprint Shoes

Heal, Cheer Up
Gil Taking, Steal Heart, Steal Helmet



Pandasforsale
Female
Virgo
58
58
Calculator
White Magic
Sunken State
Sicken
Lava Walking

Battle Folio

Feather Hat
Leather Outfit
Small Mantle

CT, Prime Number, 5, 3
Cure, Cure 3, Raise, Raise 2, Regen, Shell, Esuna

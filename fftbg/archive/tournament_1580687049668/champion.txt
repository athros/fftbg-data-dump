Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Thunderducker
Female
Cancer
71
72
Mime

Hamedo
Defense UP
Fly



Triangle Hat
Secret Clothes
Sprint Shoes

Mimic




RaitoGG
Male
Sagittarius
46
70
Ninja
White Magic
Brave Up
Equip Gun
Fly

Bloody Strings
Bloody Strings
Golden Hairpin
Earth Clothes
Chantage

Knife
Cure, Cure 3, Raise, Regen, Shell, Shell 2



Goodguy3
Female
Gemini
69
61
Mediator
Item
MA Save
Throw Item
Move+2

Bestiary

Green Beret
Chameleon Robe
Battle Boots

Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate
Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



AND4H
Male
Pisces
63
71
Mediator
White Magic
Distribute
Attack UP
Move+3

Blind Knife

Leather Hat
Leather Outfit
Spike Shoes

Invitation, Death Sentence, Mimic Daravon, Refute
Cure 2, Cure 3, Cure 4, Raise, Reraise, Regen, Protect, Shell, Shell 2, Esuna, Holy

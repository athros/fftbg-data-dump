Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Alrightbye
Male
Gemini
60
61
Archer
White Magic
Regenerator
Sicken
Lava Walking

Mythril Gun
Crystal Shield
Ribbon
Black Costume
Rubber Shoes

Charge+3, Charge+5, Charge+20
Cure, Cure 2, Cure 4, Raise, Raise 2, Protect, Protect 2



Oogthecaveman
Female
Sagittarius
55
70
Priest
Draw Out
Dragon Spirit
Doublehand
Jump+3

Sage Staff

Twist Headband
Light Robe
Elf Mantle

Cure 2, Cure 3, Cure 4, Raise, Shell, Shell 2, Wall, Esuna
Bizen Boat, Heaven's Cloud



StealthModeLocke
Female
Leo
70
41
Mediator
White Magic
Mana Shield
Equip Knife
Move+3

Sasuke Knife

Black Hood
Wizard Robe
Angel Ring

Invitation, Preach, Death Sentence, Refute
Cure 2, Raise, Raise 2, Shell 2, Esuna



ShanksMcShiv
Male
Capricorn
55
67
Priest
Charge
Meatbone Slash
Long Status
Teleport

Gold Staff

Headgear
Wizard Robe
Feather Mantle

Cure, Cure 3, Raise, Protect, Protect 2, Esuna
Charge+1, Charge+3, Charge+7, Charge+20

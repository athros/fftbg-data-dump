Player: !White
Team: White Team
Palettes: White/Blue



Coolguye
Female
Cancer
48
44
Archer
Basic Skill
MA Save
Equip Knife
Move+2

Dagger
Crystal Shield
Gold Helmet
Power Sleeve
Power Wrist

Charge+2, Charge+4
Dash, Heal, Tickle, Fury



TeaTime29
Female
Pisces
73
58
Chemist
Draw Out
HP Restore
Equip Axe
Move+1

Morning Star

Golden Hairpin
Leather Outfit
Leather Mantle

Potion, Hi-Potion, X-Potion, Remedy, Phoenix Down
Koutetsu, Murasame, Heaven's Cloud, Kiyomori



WitchHunterIX
Male
Libra
75
47
Archer
Throw
Arrow Guard
Doublehand
Jump+3

Long Bow

Green Beret
Clothes
108 Gems

Charge+1, Charge+4, Charge+5
Shuriken, Bomb



Walden
Male
Pisces
55
75
Lancer
Time Magic
Dragon Spirit
Secret Hunt
Waterbreathing

Obelisk
Ice Shield
Barbuta
Chain Mail
N-Kai Armlet

Level Jump4, Vertical Jump7
Haste, Haste 2, Immobilize, Demi, Stabilize Time, Meteor

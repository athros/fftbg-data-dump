Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Laserman1000
Male
Libra
79
77
Chemist
Black Magic
Dragon Spirit
Martial Arts
Move+2



Green Beret
Secret Clothes
Feather Boots

Potion, X-Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down
Fire, Fire 2, Fire 4, Bolt 3, Ice 4, Death



Boosted420
Male
Leo
63
76
Mime

Critical Quick
Equip Shield
Jump+1


Genji Shield
Holy Miter
Leather Outfit
Diamond Armlet

Mimic




NIghtdew14
Female
Aquarius
71
58
Ninja
Yin Yang Magic
Damage Split
Equip Gun
Move+1

Battle Folio
Battle Folio
Cachusha
Chain Vest
Vanish Mantle

Bomb, Staff
Blind, Spell Absorb, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Creggers
Male
Taurus
51
57
Ninja
Summon Magic
Counter
Concentrate
Move+3

Mage Masher
Cute Bag
Feather Hat
Mythril Vest
Sprint Shoes

Shuriken, Bomb
Titan, Carbunkle, Silf, Fairy, Lich

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Miku Shikhu
Female
Virgo
51
49
Thief
White Magic
Dragon Spirit
Magic Attack UP
Teleport

Blind Knife

Black Hood
Brigandine
Dracula Mantle

Gil Taking, Steal Heart, Steal Weapon, Steal Status, Arm Aim
Cure 3, Raise, Regen, Wall



Autnagrag
Monster
Sagittarius
65
43
Behemoth










VolgraTheMoose
Male
Sagittarius
63
46
Monk
Elemental
Brave Save
Equip Shield
Move+3


Flame Shield
Feather Hat
Earth Clothes
Cursed Ring

Spin Fist, Purification
Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball



TeaTime29
Female
Leo
44
81
Dancer
Time Magic
Earplug
Equip Sword
Waterwalking

Kiyomori

Flash Hat
Brigandine
Magic Gauntlet

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Nether Demon
Haste, Haste 2, Slow 2, Float, Reflect, Demi 2, Stabilize Time

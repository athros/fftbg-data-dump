Final Bets: red - 9 bets for 6,090G (55.6%, x0.80); blue - 10 bets for 4,854G (44.4%, x1.25)

red bets:
gooseyourself: 2,104G (34.5%, 2,104G)
TheChainNerd: 1,542G (25.3%, 1,542G)
ShintaroNayaka: 800G (13.1%, 11,446G)
CosmicTactician: 500G (8.2%, 25,808G)
Anasetsuken: 421G (6.9%, 421G)
serperemagus: 300G (4.9%, 3,113G)
autnagrag: 167G (2.7%, 4,273G)
KyleWonToLiveForever: 156G (2.6%, 156G)
AllInBot: 100G (1.6%, 100G)

blue bets:
ForagerCats: 1,000G (20.6%, 10,610G)
Error72: 1,000G (20.6%, 8,912G)
prince_rogers_nelson_: 817G (16.8%, 817G)
BirbBrainsBot: 619G (12.8%, 80,494G)
Rislyeu: 500G (10.3%, 3,033G)
getthemoneyz: 292G (6.0%, 892,894G)
Quadh0nk: 225G (4.6%, 225G)
gorgewall: 201G (4.1%, 9,643G)
Miku_Shikhu: 100G (2.1%, 19,126G)
datadrivenbot: 100G (2.1%, 36,740G)

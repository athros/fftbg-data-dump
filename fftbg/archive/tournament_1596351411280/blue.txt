Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



NoonDuarte
Female
Leo
72
69
Geomancer
Dance
Counter Tackle
Beastmaster
Move-HP Up

Battle Axe
Mythril Shield
Black Hood
Wizard Outfit
Chantage

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Obsidian Blade



Ribbiks
Monster
Aries
81
46
Pisco Demon










Djorama
Female
Capricorn
54
44
Time Mage
Draw Out
Counter
Attack UP
Ignore Height

Oak Staff

Barette
White Robe
Defense Armlet

Haste 2, Immobilize, Stabilize Time
Asura, Bizen Boat, Murasame, Muramasa



Fattunaking
Male
Aquarius
62
51
Monk
Draw Out
MA Save
Doublehand
Move+1



Feather Hat
Wizard Outfit
Magic Gauntlet

Earth Slash, Purification, Revive
Koutetsu, Muramasa, Kikuichimoji

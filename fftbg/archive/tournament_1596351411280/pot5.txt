Final Bets: blue - 10 bets for 5,916G (67.3%, x0.48); green - 7 bets for 2,869G (32.7%, x2.06)

blue bets:
josephiroth_143: 1,737G (29.4%, 1,737G)
HaplessOne: 1,111G (18.8%, 4,447G)
BirbBrainsBot: 1,000G (16.9%, 123,129G)
randgridr: 600G (10.1%, 604G)
killth3kid: 592G (10.0%, 9,161G)
Smokegiant: 248G (4.2%, 248G)
datadrivenbot: 200G (3.4%, 48,642G)
silverChangeling: 166G (2.8%, 166G)
NoonDuarte: 150G (2.5%, 330G)
Mister_Quof: 112G (1.9%, 112G)

green bets:
CT_5_Holy: 1,253G (43.7%, 1,253G)
sinnyil2: 500G (17.4%, 4,798G)
byrdturbo: 420G (14.6%, 24,816G)
AllInBot: 320G (11.2%, 320G)
holdenmagronik: 200G (7.0%, 789G)
Drusiform: 100G (3.5%, 100G)
getthemoneyz: 76G (2.6%, 1,465,487G)

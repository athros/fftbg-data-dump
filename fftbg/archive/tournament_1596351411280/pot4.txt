Final Bets: purple - 10 bets for 7,559G (62.5%, x0.60); brown - 6 bets for 4,542G (37.5%, x1.66)

purple bets:
HaplessOne: 2,778G (36.8%, 2,778G)
josephiroth_143: 1,085G (14.4%, 1,085G)
BirbBrainsBot: 1,000G (13.2%, 122,528G)
Sairentozon7: 1,000G (13.2%, 128,176G)
killth3kid: 592G (7.8%, 8,805G)
sinnyil2: 500G (6.6%, 4,498G)
AllInBot: 200G (2.6%, 200G)
datadrivenbot: 200G (2.6%, 48,522G)
silverChangeling: 104G (1.4%, 104G)
nifboy: 100G (1.3%, 6,774G)

brown bets:
randgridr: 1,852G (40.8%, 1,852G)
Smokegiant: 1,023G (22.5%, 1,023G)
DeathTaxesAndAnime: 1,009G (22.2%, 1,009G)
getthemoneyz: 358G (7.9%, 1,465,845G)
holdenmagronik: 200G (4.4%, 989G)
Drusiform: 100G (2.2%, 100G)

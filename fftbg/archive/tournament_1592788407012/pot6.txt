Final Bets: white - 8 bets for 4,468G (48.0%, x1.08); purple - 8 bets for 4,832G (52.0%, x0.92)

white bets:
sinnyil2: 1,200G (26.9%, 26,327G)
BirbBrainsBot: 1,000G (22.4%, 85,977G)
superdevon1: 920G (20.6%, 6,138G)
getthemoneyz: 558G (12.5%, 1,021,785G)
Nizaha: 501G (11.2%, 12,597G)
silentkaster: 188G (4.2%, 7,988G)
Evewho: 100G (2.2%, 7,652G)
TheKillerNacho: 1G (0.0%, 1,892G)

purple bets:
prince_rogers_nelson_: 2,000G (41.4%, 8,021G)
Zeroroute: 1,344G (27.8%, 1,344G)
roofiepops: 888G (18.4%, 2,970G)
Kronikle: 200G (4.1%, 7,733G)
RunicMagus: 100G (2.1%, 28,933G)
Seaweed_B: 100G (2.1%, 100G)
lastly: 100G (2.1%, 27,360G)
datadrivenbot: 100G (2.1%, 44,639G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Female
Aquarius
59
74
Mediator
Battle Skill
Earplug
Equip Knife
Ignore Terrain

Hidden Knife

Leather Hat
Wizard Robe
Power Wrist

Threaten, Solution, Refute
Head Break, Armor Break, Weapon Break



Thyrandaal
Female
Aquarius
45
77
Calculator
Black Magic
Damage Split
Equip Gun
Jump+3

Bloody Strings

Thief Hat
Chameleon Robe
Small Mantle

CT, 5, 4
Bolt 4, Ice 4, Empower



Sukotsuto
Male
Scorpio
60
59
Samurai
Sing
Auto Potion
Long Status
Move+3

Mythril Spear

Barbuta
Carabini Mail
Defense Armlet

Murasame, Muramasa
Angel Song, Life Song, Battle Song, Last Song, Diamond Blade, Hydra Pit



Placidphoenix
Male
Gemini
69
63
Priest
Time Magic
Absorb Used MP
Equip Polearm
Move+3

Cashmere

Golden Hairpin
Black Robe
Spike Shoes

Cure 2, Cure 3, Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna, Holy
Slow, Immobilize, Float, Demi

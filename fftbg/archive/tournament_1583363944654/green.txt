Player: !Green
Team: Green Team
Palettes: Green/White



Upvla
Male
Cancer
36
44
Monk
Time Magic
Hamedo
Doublehand
Fly



Triangle Hat
Mystic Vest
Small Mantle

Pummel, Earth Slash, Revive
Haste, Float, Reflect, Meteor



Roqqqpsi
Monster
Aquarius
80
70
Pisco Demon










Ominnous
Female
Libra
64
46
Samurai
White Magic
Hamedo
Short Charge
Move+3

Kiyomori

Bronze Helmet
Platinum Armor
Magic Ring

Koutetsu, Kiyomori, Muramasa, Kikuichimoji
Raise, Protect, Protect 2, Shell 2, Esuna



UmaiJam
Female
Capricorn
66
76
Oracle
Draw Out
Caution
Short Charge
Levitate

Papyrus Codex

Feather Hat
Silk Robe
Dracula Mantle

Poison, Spell Absorb, Silence Song, Confusion Song, Dispel Magic, Paralyze
Murasame, Heaven's Cloud, Muramasa

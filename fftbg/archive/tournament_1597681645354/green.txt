Player: !Green
Team: Green Team
Palettes: Green/White



Oreo Pizza
Female
Pisces
73
55
Dancer
Steal
Parry
Sicken
Ignore Height

Ryozan Silk

Green Beret
Linen Robe
Leather Mantle

Slow Dance, Nameless Dance, Last Dance, Nether Demon, Dragon Pit
Steal Armor, Steal Shield, Leg Aim



MLWebz0r
Male
Aries
67
50
Monk
Steal
Parry
Secret Hunt
Teleport



Twist Headband
Adaman Vest
Feather Boots

Wave Fist, Secret Fist, Purification, Chakra, Revive
Steal Armor, Steal Weapon



WhiteTigress
Male
Taurus
77
44
Archer
Talk Skill
HP Restore
Concentrate
Move+3

Lightning Bow

Holy Miter
Judo Outfit
Bracer

Charge+2, Charge+4
Persuade, Insult, Refute



Lord Gwarth
Male
Capricorn
68
68
Lancer
White Magic
Critical Quick
Doublehand
Waterbreathing

Holy Lance

Gold Helmet
White Robe
Jade Armlet

Level Jump2, Vertical Jump6
Cure 2, Regen, Protect, Shell

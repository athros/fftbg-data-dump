Player: !Black
Team: Black Team
Palettes: Black/Red



Tarheels218
Monster
Aquarius
71
62
Malboro










J2DaBibbles
Female
Aquarius
57
45
Time Mage
Item
HP Restore
Throw Item
Move+2

Gold Staff

Green Beret
Wizard Robe
Power Wrist

Haste, Slow, Slow 2, Stop, Float, Reflect, Demi, Stabilize Time
Potion, Antidote, Remedy, Phoenix Down



JonnyCue
Female
Aquarius
52
72
Summoner
Draw Out
Meatbone Slash
Short Charge
Fly

Dragon Rod

Golden Hairpin
White Robe
Chantage

Moogle, Ifrit, Titan, Carbunkle, Salamander, Silf, Lich
Asura, Bizen Boat, Masamune



Lowlf
Female
Sagittarius
79
74
Squire
Dance
Meatbone Slash
Equip Polearm
Jump+1

Obelisk
Bronze Shield
Bronze Helmet
Reflect Mail
Cursed Ring

Heal, Fury, Wish
Slow Dance, Disillusion, Nameless Dance, Obsidian Blade, Nether Demon

Final Bets: white - 6 bets for 2,751G (33.2%, x2.01); black - 13 bets for 5,532G (66.8%, x0.50)

white bets:
BirbBrainsBot: 1,000G (36.4%, 109,847G)
waterwatereverywhere: 779G (28.3%, 1,529G)
itsadud: 386G (14.0%, 386G)
getthemoneyz: 366G (13.3%, 647,281G)
midori_ribbon: 120G (4.4%, 2,056G)
crimson_hellkite: 100G (3.6%, 3,457G)

black bets:
Eyepoor_: 1,285G (23.2%, 2,521G)
HaplessOne: 1,111G (20.1%, 25,511G)
superdevon1: 712G (12.9%, 8,910G)
Lali_Lulelo: 576G (10.4%, 576G)
Dexsana: 392G (7.1%, 392G)
bahamutlagooon: 300G (5.4%, 491G)
CorpusCav: 252G (4.6%, 5,252G)
ungabunga_bot: 202G (3.7%, 417,144G)
Practice_Pad: 202G (3.7%, 328G)
ZDarius: 200G (3.6%, 1,733G)
victoriolue: 100G (1.8%, 2,240G)
datadrivenbot: 100G (1.8%, 12,700G)
Arkreaver: 100G (1.8%, 12,320G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Miyokari
Female
Aries
60
41
Knight
Yin Yang Magic
Earplug
Maintenance
Waterwalking

Defender
Round Shield
Platinum Helmet
Black Robe
Red Shoes

Shield Break, Speed Break, Mind Break, Justice Sword, Dark Sword
Blind, Poison, Pray Faith



Lythe Caraker
Male
Cancer
65
44
Samurai
Sing
Hamedo
Equip Shield
Levitate

Asura Knife
Buckler
Barbuta
White Robe
Red Shoes

Asura, Koutetsu, Kiyomori
Angel Song, Cheer Song, Battle Song, Magic Song, Nameless Song



Mudrockk
Male
Cancer
46
80
Squire
Item
Counter Tackle
Defense UP
Move-MP Up

Giant Axe
Buckler
Flash Hat
Reflect Mail
Reflect Ring

Accumulate, Heal, Tickle, Wish, Scream
Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down



Superdevon1
Female
Scorpio
63
49
Mediator
Draw Out
Catch
Long Status
Fly

Main Gauche

Feather Hat
Brigandine
Magic Gauntlet

Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Koutetsu, Bizen Boat, Kiyomori

Final Bets: white - 5 bets for 6,318G (39.1%, x1.56); black - 13 bets for 9,838G (60.9%, x0.64)

white bets:
NIghtdew14: 5,000G (79.1%, 40,723G)
prince_rogers_nelson_: 648G (10.3%, 648G)
SephDarkheart: 420G (6.6%, 16,643G)
datadrivenbot: 200G (3.2%, 46,928G)
DouglasDragonThePoet: 50G (0.8%, 1,108G)

black bets:
Thyrandaal: 2,222G (22.6%, 29,959G)
brokenknight201: 1,853G (18.8%, 1,853G)
dogsandcatsand: 1,108G (11.3%, 2,174G)
RaIshtar: 1,001G (10.2%, 16,451G)
getthemoneyz: 1,000G (10.2%, 1,439,945G)
BirbBrainsBot: 1,000G (10.2%, 25,213G)
sinnyil2: 530G (5.4%, 1,060G)
ShintaroNayaka: 500G (5.1%, 18,233G)
AllInBot: 200G (2.0%, 200G)
Chambs12: 124G (1.3%, 855G)
E_Ballard: 100G (1.0%, 5,290G)
dinin991: 100G (1.0%, 1,106G)
Error72: 100G (1.0%, 5,073G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Chambs12
Monster
Sagittarius
75
80
Red Chocobo










Error72
Male
Aries
67
45
Knight
Item
Dragon Spirit
Secret Hunt
Ignore Height

Nagrarock
Buckler
Platinum Helmet
Plate Mail
Defense Armlet

Armor Break, Shield Break, Magic Break, Speed Break
Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down



Pplvee1
Male
Capricorn
64
62
Archer
Battle Skill
Damage Split
Short Status
Ignore Terrain

Hunting Bow
Gold Shield
Green Beret
Wizard Outfit
Rubber Shoes

Charge+1, Charge+2, Charge+10
Head Break, Power Break, Justice Sword, Night Sword



LDSkinny
Female
Capricorn
49
76
Oracle
Charge
Abandon
Dual Wield
Fly

Bestiary
Bestiary
Holy Miter
Wizard Robe
Spike Shoes

Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze, Dark Holy
Charge+2

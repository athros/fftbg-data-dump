Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Female
Sagittarius
44
70
Thief
Dance
PA Save
Equip Polearm
Ignore Height

Ryozan Silk

Ribbon
Brigandine
N-Kai Armlet

Steal Heart, Steal Weapon, Steal Status, Arm Aim
Disillusion, Obsidian Blade, Void Storage



Mirapoix
Female
Virgo
44
47
Chemist
Black Magic
Counter
Equip Armor
Waterbreathing

Hydra Bag

Mythril Helmet
Mythril Vest
Sprint Shoes

X-Potion, Ether, Antidote, Phoenix Down
Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Ice, Empower



Roofiepops
Male
Gemini
54
40
Ninja
Summon Magic
Abandon
Equip Gun
Fly

Fairy Harp
Ramia Harp
Black Hood
Leather Outfit
Wizard Mantle

Bomb, Sword, Wand
Moogle, Ifrit, Titan, Golem, Carbunkle, Odin, Salamander, Fairy



Estan AD
Male
Aquarius
60
41
Calculator
Pet Skill
Catch
Equip Gun
Move+1

Battle Folio

Bronze Helmet
Platinum Armor
Spike Shoes

Blue Magic
Scratch Up, Feather Bomb, Shine Lover, Peck, Beak, Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck

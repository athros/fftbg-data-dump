Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Aeonicvector
Male
Pisces
49
52
Lancer
Item
Caution
Concentrate
Move+1

Iron Fan
Flame Shield
Gold Helmet
Bronze Armor
Wizard Mantle

Level Jump5, Vertical Jump8
Potion, Hi-Potion, X-Potion, Remedy, Phoenix Down



Ko2q
Female
Capricorn
70
76
Lancer
Basic Skill
Meatbone Slash
Defense UP
Jump+2

Obelisk
Bronze Shield
Gold Helmet
Genji Armor
Jade Armlet

Level Jump5, Vertical Jump4
Accumulate, Heal, Tickle, Yell, Scream



Firesheath
Female
Cancer
75
57
Monk
Talk Skill
Critical Quick
Defense UP
Waterbreathing



Barette
Brigandine
108 Gems

Pummel, Earth Slash, Secret Fist, Purification, Revive
Invitation, Persuade, Preach, Insult, Mimic Daravon, Refute, Rehabilitate



Byrdturbo
Male
Aries
76
66
Thief
Item
Counter
Equip Gun
Ignore Height

Papyrus Codex

Cachusha
Judo Outfit
Leather Mantle

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Potion, Hi-Potion, Hi-Ether, Eye Drop, Phoenix Down

Final Bets: yellow - 11 bets for 10,326G (78.8%, x0.27); black - 7 bets for 2,786G (21.2%, x3.71)

yellow bets:
NicoSavoy: 3,406G (33.0%, 340,615G)
SephDarkheart: 2,734G (26.5%, 2,734G)
Netmonmatt: 1,000G (9.7%, 3,396G)
RaIshtar: 1,000G (9.7%, 13,071G)
E_Ballard: 644G (6.2%, 644G)
ShintaroNayaka: 400G (3.9%, 3,345G)
AugnosMusic: 384G (3.7%, 7,699G)
roqqqpsi: 258G (2.5%, 1,034G)
cho_pin: 200G (1.9%, 3,481G)
datadrivenbot: 200G (1.9%, 40,977G)
Error72: 100G (1.0%, 3,856G)

black bets:
BirbBrainsBot: 1,000G (35.9%, 111,103G)
twelfthrootoftwo: 957G (34.4%, 1,878G)
LAGBOT30000: 338G (12.1%, 6,769G)
AllInBot: 258G (9.3%, 258G)
getthemoneyz: 108G (3.9%, 1,403,061G)
sgslugger: 100G (3.6%, 2,900G)
DouglasDragonThePoet: 25G (0.9%, 2,212G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Solomongrundy85
Male
Aries
60
65
Lancer
Talk Skill
Faith Save
Monster Talk
Fly

Holy Lance
Buckler
Gold Helmet
Leather Armor
Feather Mantle

Level Jump5, Vertical Jump8
Threaten, Insult, Negotiate, Refute



Evewho
Female
Gemini
74
54
Summoner
Punch Art
Regenerator
Doublehand
Move+3

Battle Folio

Green Beret
Linen Robe
108 Gems

Moogle, Titan, Carbunkle, Odin, Silf, Lich
Wave Fist, Earth Slash, Revive



Spootoo
Female
Aries
69
76
Monk
Talk Skill
Meatbone Slash
Monster Talk
Move+3



Feather Hat
Leather Outfit
Feather Mantle

Pummel, Wave Fist, Secret Fist, Purification, Revive
Negotiate, Mimic Daravon, Refute



Gogofromtogo
Male
Gemini
72
61
Ninja
Black Magic
Sunken State
Equip Gun
Swim

Bestiary
Battle Folio
Holy Miter
Judo Outfit
108 Gems

Staff, Ninja Sword
Bolt 4, Ice 2, Ice 4, Empower, Frog

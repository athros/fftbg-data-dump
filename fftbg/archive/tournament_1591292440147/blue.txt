Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TeaTime29
Female
Virgo
62
80
Monk
Draw Out
Counter Magic
Doublehand
Move+2



Flash Hat
Judo Outfit
108 Gems

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Murasame, Heaven's Cloud, Muramasa



Holyonline
Male
Cancer
67
55
Lancer
Draw Out
Counter
Doublehand
Jump+1

Holy Lance

Leather Helmet
Platinum Armor
Chantage

Level Jump8, Vertical Jump7
Kikuichimoji



JumbocactuarX27
Female
Sagittarius
60
66
Lancer
Draw Out
Critical Quick
Equip Bow
Jump+1

Lightning Bow

Genji Helmet
Genji Armor
Spike Shoes

Level Jump8, Vertical Jump5
Koutetsu, Kiyomori



Deathmaker06
Male
Virgo
51
51
Summoner
Jump
Faith Save
Halve MP
Waterwalking

Ice Rod

Triangle Hat
Linen Robe
Magic Ring

Ramuh, Titan, Leviathan, Fairy, Lich
Level Jump8, Vertical Jump8

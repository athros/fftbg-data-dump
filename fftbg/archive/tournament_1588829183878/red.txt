Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Aries
53
39
Wizard
Steal
Counter
Concentrate
Fly

Air Knife

Red Hood
Silk Robe
Cursed Ring

Fire 2, Bolt, Bolt 3, Ice 4, Flare
Steal Accessory



Arkreaver
Female
Virgo
72
50
Geomancer
Steal
Auto Potion
Long Status
Move+1

Bizen Boat
Crystal Shield
Black Hood
Power Sleeve
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Steal Heart, Steal Helmet, Steal Weapon, Steal Status, Arm Aim



Duckfist
Male
Sagittarius
75
78
Mime

Hamedo
Doublehand
Retreat



Leather Hat
Maximillian
N-Kai Armlet

Mimic




DonCardenio
Male
Taurus
70
69
Knight
Talk Skill
Arrow Guard
Doublehand
Waterbreathing

Defender

Iron Helmet
Chain Mail
Feather Boots

Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword, Surging Sword
Invitation, Praise

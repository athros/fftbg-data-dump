Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lastly
Male
Virgo
49
65
Samurai
Punch Art
Sunken State
Dual Wield
Move-MP Up

Koutetsu Knife
Heaven's Cloud
Bronze Helmet
Linen Robe
Sprint Shoes

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



Cataphract116
Male
Leo
75
46
Priest
Jump
Parry
Equip Sword
Ignore Terrain

Broad Sword

Triangle Hat
Silk Robe
Cursed Ring

Cure 4, Raise, Reraise, Regen, Protect, Protect 2, Shell, Shell 2, Esuna
Level Jump8, Vertical Jump8



Treapvort
Male
Leo
51
48
Knight
White Magic
Counter
Concentrate
Move+3

Long Sword
Gold Shield
Iron Helmet
Linen Cuirass
Magic Ring

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Dark Sword, Night Sword
Cure 3, Raise, Regen, Protect 2, Shell, Wall, Esuna, Holy



Jeeboheebo
Male
Sagittarius
60
62
Archer
Talk Skill
Damage Split
Equip Axe
Jump+1

Morning Star
Mythril Shield
Leather Hat
Rubber Costume
108 Gems

Charge+4, Charge+7
Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute

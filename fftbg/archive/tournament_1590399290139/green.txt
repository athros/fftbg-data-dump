Player: !Green
Team: Green Team
Palettes: Green/White



Waterwatereverywhere
Male
Scorpio
52
73
Thief
Talk Skill
Arrow Guard
Concentrate
Move-MP Up

Assassin Dagger

Thief Hat
Secret Clothes
Jade Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Weapon
Invitation, Praise, Preach, Solution, Negotiate, Refute, Rehabilitate



AmaninAmide
Male
Aries
50
51
Knight
Black Magic
Counter
Magic Attack UP
Jump+1

Ragnarok
Crystal Shield
Genji Helmet
Mythril Armor
Germinas Boots

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break
Fire 2, Fire 4, Bolt 2, Ice 2, Ice 3, Ice 4, Frog



Defaultlybrave
Female
Sagittarius
69
71
Ninja
Steal
PA Save
Beastmaster
Teleport

Scorpion Tail
Flail
Red Hood
Judo Outfit
Elf Mantle

Knife
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim



Ar Tactic
Female
Leo
76
77
Knight
Summon Magic
Caution
Equip Axe
Jump+3

Flail
Crystal Shield
Gold Helmet
Genji Armor
Leather Mantle

Speed Break, Power Break, Explosion Sword
Ifrit, Titan, Golem, Bahamut, Odin, Silf

Player: !Black
Team: Black Team
Palettes: Black/Red



DavenIII
Male
Gemini
46
43
Knight
Time Magic
Earplug
Dual Wield
Waterwalking

Defender
Iron Sword
Gold Helmet
Silk Robe
Germinas Boots

Weapon Break, Speed Break, Power Break, Dark Sword, Explosion Sword
Haste, Haste 2, Demi 2



Fenaen
Male
Scorpio
76
65
Mime

Absorb Used MP
Dual Wield
Waterbreathing



Headgear
Power Sleeve
Jade Armlet

Mimic




Pplvee1
Male
Aries
65
51
Ninja
Basic Skill
Parry
Equip Sword
Waterbreathing

Blood Sword
Platinum Sword
Red Hood
Power Sleeve
Feather Boots

Shuriken, Sword, Axe, Wand
Accumulate, Heal, Tickle, Yell, Wish



Douchetron
Female
Libra
78
48
Summoner
Elemental
Brave Save
Short Charge
Fly

Papyrus Codex

Black Hood
Silk Robe
108 Gems

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Odin, Fairy
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind

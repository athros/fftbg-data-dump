Player: !Black
Team: Black Team
Palettes: Black/Red



ArchKnightX
Male
Capricorn
44
41
Monk
Elemental
Caution
Halve MP
Jump+3



Black Hood
Power Sleeve
Red Shoes

Pummel, Earth Slash, Purification, Revive
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind



Rikrizen
Monster
Cancer
78
53
Archaic Demon










Dantayystv
Female
Taurus
65
67
Archer
White Magic
MA Save
Defend
Move-MP Up

Star Bag
Mythril Shield
Twist Headband
Brigandine
Setiemson

Charge+3, Charge+5, Charge+10
Cure, Cure 2, Cure 3, Raise 2, Reraise, Regen, Shell 2, Esuna



PoroTact
Female
Aries
70
67
Monk
Dance
Mana Shield
Secret Hunt
Swim



Green Beret
Black Costume
Small Mantle

Pummel, Wave Fist, Earth Slash, Secret Fist, Revive
Witch Hunt, Wiznaibus, Disillusion, Last Dance

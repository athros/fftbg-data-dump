Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Volgrathemoose
Male
Aquarius
41
69
Lancer
Draw Out
Mana Shield
Long Status
Teleport

Ivory Rod
Platinum Shield
Platinum Helmet
Linen Cuirass
Germinas Boots

Level Jump8, Vertical Jump8
Asura, Bizen Boat, Muramasa



Holyonline
Monster
Sagittarius
43
73
Bomb










GingerDynomite
Male
Gemini
64
59
Lancer
Steal
Meatbone Slash
Defend
Waterbreathing

Obelisk
Buckler
Diamond Helmet
Mythril Armor
N-Kai Armlet

Level Jump2, Vertical Jump2
Steal Accessory, Steal Status



Pochitchi
Male
Aries
45
50
Time Mage
Steal
Distribute
Defend
Jump+1

Papyrus Codex

Feather Hat
Black Robe
Diamond Armlet

Haste, Haste 2, Stop, Reflect, Quick, Stabilize Time
Gil Taking, Steal Helmet, Steal Armor, Arm Aim

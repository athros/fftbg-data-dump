Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Brokenknight201
Male
Gemini
54
46
Knight
Summon Magic
Critical Quick
Equip Sword
Waterwalking

Murasame
Bronze Shield
Gold Helmet
Leather Armor
Battle Boots

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break
Moogle, Ifrit, Carbunkle, Salamander, Silf, Fairy, Cyclops



HASTERIOUS
Female
Sagittarius
70
56
Geomancer
Draw Out
Counter
Beastmaster
Move+2

Slasher
Diamond Shield
Twist Headband
Light Robe
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Murasame, Kiyomori, Muramasa



ZombiFrancis
Female
Pisces
80
70
Summoner
Yin Yang Magic
PA Save
Equip Polearm
Retreat

Whale Whisker

Flash Hat
Silk Robe
108 Gems

Shiva, Titan, Carbunkle, Salamander, Silf, Fairy, Lich
Poison, Silence Song, Blind Rage, Paralyze



Ruleof5
Female
Pisces
42
63
Archer
Basic Skill
MP Restore
Beastmaster
Ignore Height

Snipe Bow
Diamond Shield
Leather Helmet
Adaman Vest
Spike Shoes

Charge+10
Accumulate, Dash, Yell, Cheer Up, Fury

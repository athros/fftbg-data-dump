Player: !Black
Team: Black Team
Palettes: Black/Red



Maximumcrit
Male
Aries
52
76
Summoner
Item
Hamedo
Throw Item
Jump+2

Rod

Thief Hat
Mythril Vest
Rubber Shoes

Ramuh, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Fairy
Potion, Hi-Potion, Holy Water, Remedy, Phoenix Down



SkylerBunny
Female
Cancer
72
71
Oracle
Draw Out
Counter Magic
Magic Defense UP
Move+3

Papyrus Codex

Holy Miter
Judo Outfit
Defense Ring

Blind, Life Drain, Zombie, Silence Song, Foxbird
Asura, Koutetsu, Heaven's Cloud, Muramasa



Evdoggity
Female
Cancer
76
42
Geomancer
Talk Skill
Regenerator
Maintenance
Levitate

Giant Axe
Bronze Shield
Flash Hat
Brigandine
Angel Ring

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Preach, Insult, Refute



Mesmaster
Male
Gemini
38
46
Geomancer
Sing
Counter
Defend
Ignore Height

Bizen Boat
Mythril Shield
Headgear
Earth Clothes
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball
Magic Song, Nameless Song, Last Song

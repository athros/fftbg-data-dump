Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dogsandcatsand
Female
Libra
74
61
Calculator
Yin Yang Magic
Abandon
Equip Shield
Levitate

Battle Folio
Gold Shield
Flash Hat
Black Costume
Vanish Mantle

Height, Prime Number, 5, 3
Zombie, Paralyze



Roofiepops
Male
Virgo
78
43
Ninja
Battle Skill
Auto Potion
Attack UP
Move-MP Up

Spell Edge
Spell Edge
Red Hood
Mystic Vest
Jade Armlet

Shuriken, Spear
Head Break, Armor Break, Weapon Break, Magic Break



Sairentozon7
Female
Pisces
63
56
Time Mage
Punch Art
PA Save
Doublehand
Levitate

White Staff

Flash Hat
Brigandine
Battle Boots

Haste, Haste 2, Slow, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time
Spin Fist, Pummel, Earth Slash, Purification



DesertWooder
Male
Sagittarius
52
46
Bard
Summon Magic
Distribute
Beastmaster
Waterwalking

Bloody Strings

Green Beret
Genji Armor
Magic Gauntlet

Battle Song, Space Storage
Moogle, Titan, Odin, Leviathan, Silf

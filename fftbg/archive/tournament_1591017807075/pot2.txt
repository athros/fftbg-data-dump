Final Bets: green - 11 bets for 6,415G (57.5%, x0.74); yellow - 10 bets for 4,741G (42.5%, x1.35)

green bets:
LDSkinny: 1,712G (26.7%, 1,712G)
TheChainNerd: 1,620G (25.3%, 31,620G)
DeathTaxesAndAnime: 768G (12.0%, 768G)
EnemyController: 500G (7.8%, 460,401G)
Baron_von_Scrub: 474G (7.4%, 22,923G)
RaIshtar: 394G (6.1%, 394G)
powergems: 372G (5.8%, 372G)
MADTheta: 250G (3.9%, 6,717G)
zepharoth89: 125G (1.9%, 6,252G)
Antipathics: 100G (1.6%, 1,534G)
daveb_: 100G (1.6%, 2,713G)

yellow bets:
StealthModeLocke: 1,249G (26.3%, 1,249G)
BirbBrainsBot: 1,000G (21.1%, 125,765G)
ThePineappleSalesman: 825G (17.4%, 825G)
FriendSquirrel: 578G (12.2%, 578G)
pplvee1: 379G (8.0%, 379G)
RunicMagus: 200G (4.2%, 30,579G)
run_with_stone_GUNs: 200G (4.2%, 8,542G)
gogofromtogo: 200G (4.2%, 1,130G)
datadrivenbot: 100G (2.1%, 29,710G)
getthemoneyz: 10G (0.2%, 784,705G)

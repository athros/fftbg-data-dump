Player: !Green
Team: Green Team
Palettes: Green/White



LordTomS
Male
Virgo
70
79
Calculator
Demon Skill
HP Restore
Equip Bow
Teleport

Windslash Bow

Twist Headband
Wizard Robe
Battle Boots

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Shalloween
Male
Libra
71
69
Ninja
Battle Skill
Auto Potion
Martial Arts
Retreat



Ribbon
Mythril Vest
Small Mantle

Spear, Dictionary
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword, Night Sword



Reinoe
Male
Scorpio
60
78
Squire
Elemental
HP Restore
Dual Wield
Move+2

Flame Whip
Platinum Sword
Twist Headband
Plate Mail
Bracer

Heal, Tickle, Wish, Scream
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



CosmicTactician
Female
Aquarius
49
52
Priest
Draw Out
Hamedo
Sicken
Jump+2

Rainbow Staff

Feather Hat
White Robe
Feather Boots

Cure 3, Reraise, Esuna
Koutetsu, Murasame

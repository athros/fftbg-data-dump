Player: !Green
Team: Green Team
Palettes: Green/White



Lydian C
Female
Cancer
47
78
Geomancer
Charge
Critical Quick
Beastmaster
Waterwalking

Giant Axe
Escutcheon
Thief Hat
White Robe
Bracer

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+2



Benticore
Monster
Capricorn
41
73
Byblos










Spuzzmocker
Male
Cancer
63
66
Time Mage
Sing
Absorb Used MP
Short Status
Jump+2

Wizard Staff

Feather Hat
Linen Robe
Small Mantle

Float, Demi
Angel Song, Life Song, Cheer Song, Sky Demon, Hydra Pit



Lowlf
Female
Aries
51
74
Squire
Elemental
Counter Magic
Attack UP
Jump+1

Ancient Sword
Platinum Shield
Diamond Helmet
Mythril Vest
Defense Armlet

Accumulate, Heal, Yell
Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

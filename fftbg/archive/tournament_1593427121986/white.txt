Player: !White
Team: White Team
Palettes: White/Blue



Sparker9
Male
Cancer
38
72
Lancer
Item
Abandon
Magic Defense UP
Lava Walking

Obelisk
Crystal Shield
Bronze Helmet
Linen Robe
Angel Ring

Level Jump8, Vertical Jump2
Potion, Antidote



CassiePhoenix
Female
Libra
57
43
Samurai
Yin Yang Magic
Dragon Spirit
Magic Attack UP
Move+3

Spear

Diamond Helmet
Diamond Armor
Salty Rage

Murasame, Heaven's Cloud
Poison, Pray Faith, Doubt Faith, Zombie, Dispel Magic



Mesmaster
Female
Aries
51
74
Wizard
Punch Art
Absorb Used MP
Short Charge
Move-MP Up

Ice Rod

Triangle Hat
Chain Vest
Elf Mantle

Fire, Fire 2, Fire 3, Fire 4, Bolt 4, Ice, Ice 3, Flare
Spin Fist, Pummel, Earth Slash, Chakra, Revive



Maggee
Female
Libra
76
67
Mediator
Item
Meatbone Slash
Martial Arts
Ignore Terrain



Black Hood
Black Robe
Reflect Ring

Invitation, Preach, Insult, Refute
Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Soft, Remedy

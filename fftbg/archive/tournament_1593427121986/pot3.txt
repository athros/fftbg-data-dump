Final Bets: white - 9 bets for 6,183G (72.3%, x0.38); black - 4 bets for 2,370G (27.7%, x2.61)

white bets:
serperemagus: 2,924G (47.3%, 2,924G)
Mesmaster: 1,000G (16.2%, 132,718G)
BlackFireUK: 1,000G (16.2%, 2,412G)
BirbBrainsBot: 413G (6.7%, 123,928G)
getthemoneyz: 296G (4.8%, 1,107,371G)
SuzakuReii: 200G (3.2%, 1,308G)
ar_tactic: 150G (2.4%, 62,008G)
datadrivenbot: 100G (1.6%, 47,856G)
Ring_Wyrm: 100G (1.6%, 2,253G)

black bets:
VolgraTheMoose: 1,001G (42.2%, 2,563G)
prince_rogers_nelson_: 600G (25.3%, 600G)
ShintaroNayaka: 568G (24.0%, 568G)
gorgewall: 201G (8.5%, 27,580G)

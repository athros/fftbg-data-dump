Final Bets: white - 13 bets for 8,306G (62.9%, x0.59); brown - 8 bets for 4,897G (37.1%, x1.70)

white bets:
Azelgrim: 2,500G (30.1%, 13,464G)
defaultlybrave: 1,000G (12.0%, 10,432G)
TheChainNerd: 992G (11.9%, 992G)
DLJuggernaut: 750G (9.0%, 20,589G)
roqqqpsi: 718G (8.6%, 1,306G)
HaateXIII: 512G (6.2%, 512G)
BlackFireUK: 500G (6.0%, 23,345G)
killth3kid: 450G (5.4%, 5,277G)
Kronikle: 300G (3.6%, 19,714G)
DuraiPapers: 232G (2.8%, 232G)
AllInBot: 152G (1.8%, 152G)
maakur_: 100G (1.2%, 25,257G)
datadrivenbot: 100G (1.2%, 44,442G)

brown bets:
toka222: 1,650G (33.7%, 47,363G)
Thyrandaal: 1,000G (20.4%, 65,296G)
BirbBrainsBot: 1,000G (20.4%, 64,455G)
CorpusCav: 653G (13.3%, 653G)
dogsandcatsand: 364G (7.4%, 364G)
oops25: 100G (2.0%, 1,906G)
Devolant: 100G (2.0%, 2,235G)
getthemoneyz: 30G (0.6%, 968,809G)

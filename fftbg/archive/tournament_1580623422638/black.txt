Player: !Black
Team: Black Team
Palettes: Black/Red



Lube Squid
Female
Leo
56
77
Priest
Dance
Parry
Magic Attack UP
Retreat

White Staff

Twist Headband
Wizard Outfit
Angel Ring

Raise, Raise 2, Regen, Shell, Esuna
Slow Dance, Last Dance



NoxeGS
Female
Aquarius
66
66
Time Mage
White Magic
Distribute
Doublehand
Lava Walking

Gold Staff

Holy Miter
Silk Robe
Battle Boots

Haste 2, Slow 2, Immobilize, Reflect, Demi 2, Stabilize Time, Meteor
Cure, Cure 3, Cure 4, Protect, Wall, Esuna



TheUnforgivenRage
Female
Gemini
45
66
Dancer
Time Magic
Meatbone Slash
Equip Knife
Ignore Terrain

Sasuke Knife

Green Beret
Chain Vest
Sprint Shoes

Wiznaibus, Slow Dance, Disillusion, Obsidian Blade, Void Storage
Haste, Haste 2, Reflect, Demi 2, Stabilize Time, Meteor



Grininda
Female
Virgo
62
46
Lancer
White Magic
Mana Shield
Magic Defense UP
Move+3

Partisan
Flame Shield
Barbuta
Diamond Armor
Genji Gauntlet

Level Jump4, Vertical Jump4
Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect 2, Wall, Holy

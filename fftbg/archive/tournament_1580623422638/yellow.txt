Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CidDurai
Male
Virgo
55
57
Archer
Jump
Counter Tackle
Maintenance
Move+1

Romanda Gun
Mythril Shield
Triangle Hat
Judo Outfit
Diamond Armlet

Charge+3, Charge+7, Charge+10, Charge+20
Level Jump5, Vertical Jump4



Psychomidget
Male
Virgo
51
45
Bard
Black Magic
Parry
Equip Armor
Move+3

Lightning Bow

Green Beret
Secret Clothes
Diamond Armlet

Angel Song, Cheer Song, Battle Song, Nameless Song, Sky Demon
Fire 3, Bolt, Bolt 4, Ice, Ice 3, Empower, Frog



Waytoopale
Male
Aquarius
53
76
Knight
White Magic
Critical Quick
Attack UP
Move-MP Up

Iron Sword
Crystal Shield
Leather Helmet
Chain Mail
Angel Ring

Shield Break, Speed Break, Power Break, Mind Break
Cure, Cure 2, Cure 3, Shell 2, Esuna, Magic Barrier



Farseli
Female
Sagittarius
70
43
Summoner
Punch Art
Meatbone Slash
Short Charge
Levitate

Rainbow Staff

Green Beret
Mythril Vest
Power Wrist

Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Silf, Lich
Spin Fist, Wave Fist, Secret Fist, Purification, Revive, Seal Evil

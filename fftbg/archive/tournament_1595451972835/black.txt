Player: !Black
Team: Black Team
Palettes: Black/Red



Joewcarson
Male
Taurus
79
49
Ninja
Summon Magic
Brave Save
Equip Gun
Levitate

Stone Gun
Romanda Gun
Triangle Hat
Power Sleeve
Power Wrist

Knife
Moogle, Ifrit, Silf, Lich



Roqqqpsi
Monster
Leo
65
47
Porky










HaateXIII
Male
Leo
52
67
Bard
Black Magic
Brave Save
Doublehand
Fly

Bloody Strings

Green Beret
Leather Outfit
Cursed Ring

Cheer Song, Last Song, Diamond Blade, Sky Demon, Hydra Pit
Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Frog



Mirapoix
Female
Leo
60
69
Wizard
Yin Yang Magic
Counter Flood
Magic Defense UP
Ignore Terrain

Dragon Rod

Feather Hat
Brigandine
Reflect Ring

Fire, Fire 2, Bolt 2, Bolt 3, Ice, Ice 3, Ice 4, Empower, Frog, Death
Blind, Zombie, Paralyze, Sleep

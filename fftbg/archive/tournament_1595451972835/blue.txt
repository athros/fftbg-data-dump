Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ribbiks
Male
Gemini
48
39
Archer
Steal
Caution
Equip Bow
Teleport

Panther Bag
Buckler
Feather Hat
Mystic Vest
Diamond Armlet

Charge+1, Charge+4, Charge+7
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Leg Aim



PoroTact
Female
Sagittarius
69
50
Time Mage
Battle Skill
Dragon Spirit
Equip Polearm
Levitate

Ivory Rod

Thief Hat
Chameleon Robe
Feather Boots

Haste 2, Immobilize, Quick, Demi, Demi 2, Stabilize Time, Meteor
Head Break, Weapon Break



Run With Stone GUNs
Female
Sagittarius
71
48
Time Mage
Draw Out
MP Restore
Martial Arts
Move+2



Headgear
Power Sleeve
Rubber Shoes

Haste, Slow, Immobilize, Float, Demi 2
Asura, Heaven's Cloud, Muramasa, Kikuichimoji, Masamune



Shalloween
Female
Aries
42
64
Mime

HP Restore
Martial Arts
Jump+1



Holy Miter
Black Robe
Feather Boots

Mimic


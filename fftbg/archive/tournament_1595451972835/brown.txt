Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Xoomwaffle
Female
Virgo
74
76
Dancer
Battle Skill
Faith Save
Equip Knife
Teleport

Poison Rod

Golden Hairpin
Black Robe
Elf Mantle

Witch Hunt, Disillusion, Nether Demon
Mind Break, Stasis Sword, Dark Sword



Lyner87
Female
Virgo
66
76
Geomancer
Yin Yang Magic
Mana Shield
Equip Polearm
Move-MP Up

Obelisk
Bronze Shield
Triangle Hat
Silk Robe
Small Mantle

Pitfall, Water Ball, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Blind, Spell Absorb, Dispel Magic



Brokenknight201
Male
Virgo
74
76
Lancer
Yin Yang Magic
Abandon
Defense UP
Swim

Dragon Whisker
Crystal Shield
Circlet
Black Robe
Red Shoes

Level Jump8, Vertical Jump8
Spell Absorb, Pray Faith, Blind Rage, Foxbird, Dispel Magic



E Ballard
Female
Taurus
80
53
Chemist
Battle Skill
Counter Magic
Magic Defense UP
Teleport

Blast Gun

Holy Miter
Brigandine
Feather Boots

Antidote, Holy Water, Remedy, Phoenix Down
Head Break, Weapon Break, Stasis Sword

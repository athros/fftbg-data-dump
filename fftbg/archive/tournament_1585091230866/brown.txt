Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ququroon
Female
Aquarius
60
48
Geomancer
Draw Out
Counter Flood
Halve MP
Levitate

Koutetsu Knife
Buckler
Golden Hairpin
Robe of Lords
Leather Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa



Davarian
Female
Virgo
49
59
Mediator
Black Magic
Mana Shield
Equip Polearm
Ignore Terrain

Musk Rod

Leather Hat
Mythril Vest
Battle Boots

Invitation, Persuade, Threaten, Preach, Refute
Fire 3, Empower



DHaveWord
Male
Libra
69
57
Lancer
Charge
MA Save
Dual Wield
Ignore Terrain

Obelisk
Mythril Spear
Crystal Helmet
White Robe
Defense Armlet

Level Jump8, Vertical Jump7
Charge+1, Charge+3, Charge+10



ForagerCats
Male
Leo
64
63
Bard
Basic Skill
HP Restore
Halve MP
Fly

Fairy Harp

Headgear
Leather Armor
Feather Mantle

Angel Song, Last Song
Accumulate, Heal, Tickle

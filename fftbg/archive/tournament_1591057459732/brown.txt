Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Joewcarson
Male
Aries
80
66
Wizard
Talk Skill
Counter Tackle
Short Charge
Move+3

Rod

Triangle Hat
Leather Outfit
Wizard Mantle

Fire 2, Ice, Ice 2, Death
Death Sentence, Refute, Rehabilitate



Twelfthrootoftwo
Female
Aries
47
56
Oracle
White Magic
Blade Grasp
Sicken
Waterbreathing

Octagon Rod

Triangle Hat
Light Robe
Genji Gauntlet

Blind, Silence Song, Blind Rage, Foxbird, Dark Holy
Cure 3, Cure 4, Regen, Shell 2, Esuna



Old Overholt
Female
Libra
40
80
Wizard
Elemental
Speed Save
Short Charge
Ignore Height

Poison Rod

Thief Hat
Silk Robe
Reflect Ring

Bolt, Bolt 3, Ice, Ice 4
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Tougou
Female
Cancer
70
40
Oracle
White Magic
Speed Save
Short Charge
Jump+2

Gokuu Rod

Black Hood
Chameleon Robe
Red Shoes

Spell Absorb, Zombie, Blind Rage, Foxbird, Sleep
Cure 2, Raise 2, Protect, Shell, Shell 2, Esuna

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lythe Caraker
Male
Cancer
69
55
Archer
Time Magic
Dragon Spirit
Attack UP
Levitate

Long Bow

Barbuta
Mystic Vest
Rubber Shoes

Charge+2, Charge+5, Charge+20
Demi, Meteor



Qaz12301
Male
Sagittarius
39
50
Time Mage
Charge
Parry
Long Status
Retreat

Iron Fan

Triangle Hat
Mystic Vest
Spike Shoes

Haste, Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time, Meteor
Charge+3, Charge+4, Charge+5



Nizaha
Female
Leo
64
80
Chemist
Dance
Meatbone Slash
Long Status
Swim

Cute Bag

Green Beret
Rubber Costume
Small Mantle

Potion, Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Holy Water
Witch Hunt, Polka Polka, Disillusion



Evewho
Female
Taurus
59
45
Priest
Draw Out
Counter
Sicken
Retreat

Mace of Zeus

Holy Miter
Linen Robe
Feather Mantle

Raise, Raise 2, Protect, Shell, Shell 2, Esuna
Bizen Boat, Muramasa

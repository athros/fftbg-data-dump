Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



MirrorTwin
Female
Aries
49
54
Chemist
Elemental
Abandon
Equip Axe
Ignore Height

Slasher

Twist Headband
Earth Clothes
Defense Armlet

Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Gusty Wind



DaveStrider55
Female
Taurus
55
74
Monk
Elemental
Counter Flood
Short Charge
Waterbreathing



Barette
Brigandine
Spike Shoes

Pummel, Earth Slash, Secret Fist, Purification, Seal Evil
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Shalloween
Monster
Libra
58
47
Red Dragon










Moonliquor
Male
Taurus
59
62
Calculator
Black Magic
Arrow Guard
Equip Shield
Levitate

Flame Rod
Platinum Shield
Golden Hairpin
Black Robe
Power Wrist

CT, Prime Number, 3
Fire, Fire 3, Bolt 2, Ice 4

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Churio64
Male
Leo
59
58
Squire
White Magic
Faith Up
Equip Knife
Levitate

Air Knife

Flash Hat
Wizard Outfit
Power Wrist

Throw Stone
Cure, Cure 2, Cure 3, Raise, Wall, Esuna



TheGreatGallus
Male
Aries
57
54
Mediator
Jump
Critical Quick
Dual Wield
Move-MP Up

Papyrus Codex
Papyrus Codex
Red Hood
White Robe
Wizard Mantle

Persuade, Mimic Daravon, Refute
Level Jump3, Vertical Jump7



Hzor
Male
Cancer
51
76
Wizard
Punch Art
HP Restore
Secret Hunt
Move+2

Thunder Rod

Leather Hat
Robe of Lords
Small Mantle

Fire, Fire 2, Bolt, Ice 2
Secret Fist, Purification



Maleous2
Male
Sagittarius
66
49
Bard
Time Magic
Damage Split
Equip Knife
Lava Walking

Main Gauche

Headgear
Clothes
Feather Boots

Life Song, Cheer Song, Sky Demon, Hydra Pit
Haste, Stop, Immobilize, Float, Demi, Stabilize Time, Meteor

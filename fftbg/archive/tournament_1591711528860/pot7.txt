Final Bets: green - 10 bets for 4,168G (39.6%, x1.53); purple - 15 bets for 6,365G (60.4%, x0.65)

green bets:
toka222: 750G (18.0%, 31,758G)
RaIshtar: 671G (16.1%, 671G)
TheChainNerd: 568G (13.6%, 2,840G)
killth3kid: 500G (12.0%, 11,493G)
prince_rogers_nelson_: 416G (10.0%, 416G)
NovaKnight21: 416G (10.0%, 416G)
skillomono: 304G (7.3%, 304G)
joewcarson: 300G (7.2%, 45,023G)
AllInBot: 143G (3.4%, 143G)
Anasetsuken: 100G (2.4%, 466G)

purple bets:
mpghappiness: 1,001G (15.7%, 3,295G)
BirbBrainsBot: 1,000G (15.7%, 29,908G)
StealthModeLocke: 630G (9.9%, 630G)
YaBoy125: 557G (8.8%, 6,964G)
DLJuggernaut: 528G (8.3%, 2,643G)
DeadGirlzREasy: 483G (7.6%, 483G)
ShintaroNayaka: 400G (6.3%, 4,948G)
xxbronamastexx: 352G (5.5%, 352G)
JumbocactuarX27: 344G (5.4%, 344G)
DustBirdEX: 336G (5.3%, 336G)
TravisInParis: 320G (5.0%, 932G)
IphoneDarkness: 150G (2.4%, 2,508G)
DeathTaxesAndAnime: 100G (1.6%, 3,454G)
datadrivenbot: 100G (1.6%, 37,836G)
getthemoneyz: 64G (1.0%, 871,163G)

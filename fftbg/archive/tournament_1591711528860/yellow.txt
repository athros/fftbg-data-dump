Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Powergems
Female
Virgo
63
71
Samurai
Battle Skill
Counter Tackle
Short Status
Jump+2

Kiyomori

Leather Helmet
Gold Armor
Leather Mantle

Koutetsu, Bizen Boat
Head Break, Shield Break



ALY327
Female
Sagittarius
74
44
Oracle
Steal
Counter Magic
Equip Axe
Jump+1

White Staff

Cachusha
Brigandine
Power Wrist

Poison, Life Drain, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze
Steal Armor, Steal Shield, Steal Accessory



DudeMonkey77
Female
Virgo
60
79
Geomancer
Talk Skill
Absorb Used MP
Short Charge
Move-MP Up

Long Sword
Round Shield
Flash Hat
Black Robe
Wizard Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Threaten, Death Sentence, Insult, Negotiate



NIghtdew14
Female
Gemini
39
80
Dancer
Summon Magic
Counter Tackle
Doublehand
Waterbreathing

Persia

Triangle Hat
Black Robe
Elf Mantle

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Obsidian Blade
Golem, Salamander

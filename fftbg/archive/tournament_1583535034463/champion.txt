Player: !zChamp
Team: Champion Team
Palettes: Black/Red



FreedomNM
Male
Leo
53
60
Mime

PA Save
Equip Armor
Move+2



Leather Helmet
Bronze Armor
Angel Ring

Mimic




Toka222
Male
Leo
73
56
Ninja
Punch Art
Counter Tackle
Equip Sword
Waterbreathing

Blood Sword
Morning Star
Black Hood
Mythril Vest
Wizard Mantle

Shuriken, Wand
Purification, Seal Evil



KupoKel
Male
Gemini
63
78
Lancer
Elemental
Sunken State
Equip Gun
Retreat

Battle Folio
Round Shield
Crystal Helmet
Bronze Armor
Rubber Shoes

Level Jump8, Vertical Jump6
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Hzor
Male
Aquarius
65
56
Lancer
Item
Distribute
Throw Item
Retreat

Mythril Spear
Platinum Shield
Genji Helmet
Diamond Armor
Spike Shoes

Level Jump5, Vertical Jump6
Potion, X-Potion, Echo Grass, Holy Water, Phoenix Down

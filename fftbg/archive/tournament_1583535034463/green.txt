Player: !Green
Team: Green Team
Palettes: Green/White



Kl0kwurk
Female
Virgo
48
49
Lancer
Yin Yang Magic
Auto Potion
Beastmaster
Jump+1

Gokuu Rod
Round Shield
Cross Helmet
Linen Cuirass
Magic Ring

Level Jump8, Vertical Jump2
Poison, Spell Absorb, Life Drain, Zombie, Foxbird, Confusion Song, Sleep, Petrify, Dark Holy



Aldrammech
Female
Libra
66
63
Priest
Yin Yang Magic
Speed Save
Beastmaster
Waterwalking

Rainbow Staff

Cachusha
Chain Vest
Bracer

Raise, Reraise, Esuna
Blind, Poison, Spell Absorb, Doubt Faith, Zombie, Blind Rage



Laserman1000
Male
Scorpio
58
74
Priest
Elemental
Auto Potion
Defend
Waterwalking

Morning Star

Headgear
Linen Robe
Small Mantle

Cure, Cure 3, Cure 4, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna
Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball



Tougou
Female
Gemini
55
47
Geomancer
White Magic
Absorb Used MP
Halve MP
Teleport

Ice Brand
Bronze Shield
Flash Hat
Power Sleeve
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Cure 2, Regen, Shell 2, Esuna, Holy

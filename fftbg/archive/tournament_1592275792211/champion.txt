Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Kaidykat
Male
Pisces
66
51
Lancer
Item
Absorb Used MP
Secret Hunt
Move+3

Obelisk
Genji Shield
Barbuta
Genji Armor
Rubber Shoes

Level Jump8, Vertical Jump8
Potion, Hi-Potion, Hi-Ether, Soft, Phoenix Down



Nizaha
Male
Virgo
43
68
Mediator
Yin Yang Magic
Mana Shield
Maintenance
Jump+3

Glacier Gun

Flash Hat
Chameleon Robe
Feather Boots

Death Sentence, Negotiate, Refute
Spell Absorb, Zombie, Silence Song, Blind Rage, Sleep



Laserman1000
Male
Sagittarius
36
54
Knight
Summon Magic
Counter
Halve MP
Teleport 2

Materia Blade
Diamond Shield
Platinum Helmet
Black Robe
Vanish Mantle

Shield Break, Surging Sword
Moogle, Shiva, Titan, Bahamut, Odin



Daveb
Male
Libra
47
56
Ninja
Time Magic
Distribute
Equip Bow
Ignore Height

Bow Gun
Poison Bow
Green Beret
Adaman Vest
Spike Shoes

Shuriken, Hammer
Haste, Haste 2, Slow, Reflect, Quick, Stabilize Time

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



OneHundredFists
Male
Aries
49
72
Thief
Summon Magic
Mana Shield
Dual Wield
Jump+3

Hidden Knife
Diamond Sword
Flash Hat
Chain Vest
Diamond Armlet

Gil Taking, Steal Heart, Steal Accessory, Arm Aim
Moogle, Ifrit, Golem, Bahamut, Fairy



Powermhero
Male
Cancer
75
40
Time Mage
Draw Out
Catch
Magic Defense UP
Retreat

Musk Rod

Triangle Hat
Wizard Robe
Feather Mantle

Haste, Haste 2, Slow 2, Immobilize, Reflect, Quick
Koutetsu, Bizen Boat, Kiyomori, Muramasa



AltimaMantoid
Male
Leo
49
50
Mime

Caution
Monster Talk
Levitate



Feather Hat
Wizard Outfit
Spike Shoes

Mimic




KasugaiRoastedPeas
Female
Capricorn
70
54
Priest
Steal
Caution
Short Status
Retreat

Healing Staff

Black Hood
Silk Robe
Genji Gauntlet

Raise, Reraise, Regen, Protect 2, Esuna
Steal Heart, Leg Aim

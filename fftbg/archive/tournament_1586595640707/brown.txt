Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Jerboj
Male
Pisces
55
80
Oracle
Item
Regenerator
Throw Item
Teleport

Iron Fan

Feather Hat
Wizard Robe
Genji Gauntlet

Blind, Poison, Pray Faith, Doubt Faith, Zombie, Foxbird, Paralyze, Sleep, Petrify
Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



InOzWeTrust
Male
Gemini
74
71
Lancer
White Magic
Earplug
Long Status
Move+1

Holy Lance
Buckler
Circlet
Chain Mail
108 Gems

Level Jump5, Vertical Jump2
Cure 2, Cure 4, Raise, Raise 2, Reraise, Shell 2, Esuna



Chuckolator
Female
Taurus
67
42
Knight
Basic Skill
Auto Potion
Dual Wield
Teleport

Slasher
Ice Brand
Mythril Helmet
Black Robe
Jade Armlet

Head Break, Mind Break, Surging Sword
Accumulate, Dash, Heal, Tickle, Cheer Up, Fury



UmaiJam
Male
Libra
53
50
Chemist
Elemental
Abandon
Magic Defense UP
Retreat

Blaze Gun

Golden Hairpin
Power Sleeve
Jade Armlet

X-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Willjin
Female
Virgo
78
69
Mediator
Dance
Parry
Defense UP
Ignore Height

Blast Gun

Leather Hat
Black Robe
Defense Armlet

Preach, Solution, Insult, Mimic Daravon, Refute
Witch Hunt, Last Dance, Obsidian Blade, Void Storage



Michio Plays
Female
Gemini
45
74
Ninja
Talk Skill
Counter Tackle
Monster Talk
Swim

Flame Whip
Flail
Black Hood
Brigandine
Reflect Ring

Knife, Staff, Spear, Wand
Invitation, Persuade, Praise, Threaten, Refute, Rehabilitate



RubenFlonne
Female
Taurus
61
59
Mediator
Summon Magic
Parry
Magic Defense UP
Move+1

Stone Gun

Triangle Hat
Wizard Robe
Defense Ring

Praise, Negotiate, Mimic Daravon, Refute
Moogle, Shiva, Titan, Golem, Carbunkle, Fairy, Cyclops



Superdevon1
Male
Aries
42
45
Chemist
Jump
Mana Shield
Doublehand
Swim

Hydra Bag

Feather Hat
Brigandine
Magic Ring

Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Phoenix Down
Level Jump2, Vertical Jump8

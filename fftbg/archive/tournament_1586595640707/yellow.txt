Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WireLord
Female
Aquarius
59
62
Monk
White Magic
Counter
Magic Attack UP
Swim



Holy Miter
Wizard Outfit
Feather Boots

Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Cure 2, Cure 4, Raise, Raise 2, Shell, Esuna



SkyridgeZero
Monster
Aries
73
69
Holy Dragon










Part Ways
Male
Capricorn
60
70
Calculator
Yin Yang Magic
Auto Potion
Short Status
Move+3

Gokuu Rod

Black Hood
Light Robe
Feather Boots

Height, Prime Number, 4
Life Drain, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep



OneHundredFists
Female
Scorpio
52
78
Time Mage
Black Magic
Brave Up
Doublehand
Waterwalking

White Staff

Golden Hairpin
Silk Robe
Bracer

Haste 2, Slow 2, Immobilize, Quick, Demi, Demi 2, Stabilize Time, Meteor
Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice 3, Death

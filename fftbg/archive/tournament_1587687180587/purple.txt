Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Tougou
Male
Virgo
58
42
Knight
Charge
Counter
Equip Armor
Move-HP Up

Iron Sword
Bronze Shield
Holy Miter
Bronze Armor
Rubber Shoes

Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Surging Sword
Charge+10



Gorgewall
Male
Capricorn
69
59
Oracle
Talk Skill
MA Save
Magic Defense UP
Move+1

Papyrus Codex

Holy Miter
Brigandine
N-Kai Armlet

Blind, Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Persuade, Threaten, Solution, Negotiate, Mimic Daravon



KaLam1ty
Male
Gemini
68
77
Mediator
White Magic
Dragon Spirit
Equip Gun
Move+1

Blaze Gun

Thief Hat
Silk Robe
Small Mantle

Invitation, Persuade, Praise, Threaten, Preach, Negotiate, Refute
Cure 4, Raise, Shell, Wall, Esuna



L2 Sentinel
Male
Libra
58
64
Monk
Charge
Caution
Attack UP
Fly



Black Hood
Wizard Outfit
Chantage

Pummel, Earth Slash, Chakra, Revive
Charge+2, Charge+4

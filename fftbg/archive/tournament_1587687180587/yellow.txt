Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DeathTaxesAndAnime
Female
Aries
51
41
Samurai
Item
Damage Split
Dual Wield
Teleport

Javelin
Partisan
Platinum Helmet
Chameleon Robe
108 Gems

Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
X-Potion, Eye Drop, Echo Grass, Soft



Aldrammech
Male
Leo
40
49
Priest
Sing
Caution
Equip Armor
Ignore Height

White Staff

Twist Headband
Crystal Mail
108 Gems

Cure, Cure 3, Reraise, Regen, Protect 2, Shell 2, Wall
Angel Song, Hydra Pit



Lanshaft
Female
Aquarius
55
76
Wizard
Talk Skill
Meatbone Slash
Halve MP
Ignore Terrain

Cultist Dagger

Twist Headband
Wizard Robe
Cursed Ring

Fire 2, Bolt 3, Ice, Ice 2
Invitation, Persuade, Praise, Preach, Insult, Negotiate, Rehabilitate



Goust18
Male
Taurus
41
49
Summoner
Yin Yang Magic
Parry
Short Charge
Move+1

Poison Rod

Triangle Hat
Chameleon Robe
Feather Boots

Golem, Carbunkle, Bahamut, Odin, Leviathan, Fairy, Lich
Blind, Spell Absorb, Life Drain, Silence Song, Paralyze, Sleep, Dark Holy

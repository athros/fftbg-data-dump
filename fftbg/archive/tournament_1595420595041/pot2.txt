Final Bets: green - 6 bets for 3,288G (25.0%, x3.00); yellow - 13 bets for 9,876G (75.0%, x0.33)

green bets:
pyrelli: 1,150G (35.0%, 2,255G)
LDSkinny: 1,000G (30.4%, 36,120G)
Smokegiant: 500G (15.2%, 3,100G)
ruleof5: 276G (8.4%, 276G)
Belkra: 232G (7.1%, 232G)
getthemoneyz: 130G (4.0%, 1,335,530G)

yellow bets:
RaIshtar: 2,323G (23.5%, 2,323G)
NicoSavoy: 2,000G (20.3%, 274,201G)
AllInBot: 1,055G (10.7%, 1,055G)
BirbBrainsBot: 1,000G (10.1%, 186,207G)
TheChainNerd: 724G (7.3%, 724G)
twelfthrootoftwo: 620G (6.3%, 620G)
JumbocactuarX27: 500G (5.1%, 14,745G)
randgridr: 460G (4.7%, 2,425G)
DavenIII: 389G (3.9%, 389G)
pplvee1: 384G (3.9%, 384G)
datadrivenbot: 200G (2.0%, 31,890G)
Lydian_C: 121G (1.2%, 66,930G)
nhammen: 100G (1.0%, 5,932G)

Final Bets: green - 5 bets for 8,544G (38.4%, x1.60); champion - 14 bets for 13,683G (61.6%, x0.62)

green bets:
SkylerBunny: 4,117G (48.2%, 4,117G)
latebit: 2,000G (23.4%, 18,119G)
superdevon1: 1,243G (14.5%, 124,353G)
BirbBrainsBot: 1,000G (11.7%, 50,566G)
getthemoneyz: 184G (2.2%, 1,677,611G)

champion bets:
Sairentozon7: 8,000G (58.5%, 9,761G)
rNdOrchestra: 1,234G (9.0%, 7,709G)
JCBooBot: 1,000G (7.3%, 1,000G)
sinnyil2: 964G (7.0%, 964G)
resjudicata3: 734G (5.4%, 734G)
Laserman1000: 600G (4.4%, 4,100G)
datadrivenbot: 200G (1.5%, 63,785G)
JCBoorgo: 200G (1.5%, 1,769G)
Lazarus_DS: 200G (1.5%, 1,306G)
iBardic: 200G (1.5%, 7,264G)
gorgewall: 101G (0.7%, 5,460G)
AllInBot: 100G (0.7%, 100G)
Evewho: 100G (0.7%, 5,554G)
Drusiform: 50G (0.4%, 2,811G)

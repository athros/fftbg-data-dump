Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ZZ Yoshi
Female
Leo
75
60
Wizard
Draw Out
Catch
Short Charge
Move+2

Rod

Holy Miter
White Robe
Battle Boots

Fire, Fire 3, Ice, Ice 2, Ice 3, Ice 4, Empower, Flare
Asura, Koutetsu, Murasame, Kiyomori, Kikuichimoji



Phytik
Female
Scorpio
57
69
Dancer
Punch Art
Counter Magic
Secret Hunt
Fly

Persia

Golden Hairpin
Brigandine
Battle Boots

Wiznaibus, Nameless Dance, Obsidian Blade, Nether Demon
Pummel, Purification, Seal Evil



Sinnyil2
Male
Sagittarius
69
58
Ninja
Item
Blade Grasp
Attack UP
Jump+2

Morning Star
Sasuke Knife
Triangle Hat
Earth Clothes
Feather Boots

Shuriken, Dictionary
Potion, X-Potion, Elixir, Soft, Phoenix Down



Bioticism
Female
Virgo
49
60
Lancer
Punch Art
Parry
Attack UP
Teleport

Holy Lance
Platinum Shield
Platinum Helmet
Light Robe
Battle Boots

Level Jump5, Vertical Jump8
Spin Fist, Pummel, Chakra, Revive

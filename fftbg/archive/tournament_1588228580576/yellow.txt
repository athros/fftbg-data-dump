Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galkife
Male
Scorpio
55
67
Ninja
Elemental
Distribute
Magic Attack UP
Teleport

Kunai
Hidden Knife
Black Hood
Chain Vest
Angel Ring

Shuriken, Knife, Hammer
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Estan AD
Male
Virgo
47
78
Thief
Throw
Counter
Short Charge
Move-MP Up

Spell Edge

Twist Headband
Clothes
Germinas Boots

Steal Helmet, Steal Accessory
Shuriken, Hammer, Wand



NovaKnight21
Male
Leo
80
51
Bard
Basic Skill
Caution
Long Status
Waterbreathing

Fairy Harp

Flash Hat
Bronze Armor
Diamond Armlet

Angel Song, Life Song, Last Song
Accumulate, Heal, Tickle, Yell, Wish



HaplessOne
Female
Scorpio
44
68
Summoner
Talk Skill
Counter Magic
Magic Attack UP
Fly

Rod

Red Hood
Leather Outfit
Leather Mantle

Ifrit, Golem, Leviathan, Salamander
Praise, Threaten, Insult, Negotiate, Refute, Rehabilitate

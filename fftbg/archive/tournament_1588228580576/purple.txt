Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vorap
Monster
Virgo
55
61
Goblin










Red Celt
Male
Taurus
40
67
Oracle
Time Magic
Distribute
Defend
Fly

Ivory Rod

Black Hood
Earth Clothes
108 Gems

Blind, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Haste, Slow, Immobilize, Demi 2, Stabilize Time



Gorgewall
Female
Aries
41
71
Dancer
Steal
Caution
Equip Axe
Fly

Giant Axe

Holy Miter
Wizard Outfit
Defense Ring

Wiznaibus, Slow Dance, Last Dance, Dragon Pit
Steal Helmet, Steal Shield, Leg Aim



Firesheath
Female
Capricorn
54
65
Samurai
Punch Art
Sunken State
Equip Sword
Move+2

Chaos Blade

Circlet
Crystal Mail
Bracer

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Wave Fist, Secret Fist, Purification

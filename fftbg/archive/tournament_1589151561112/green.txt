Player: !Green
Team: Green Team
Palettes: Green/White



Awakeninfinity
Male
Leo
78
37
Lancer
Sing
HP Restore
Secret Hunt
Move-HP Up

Javelin
Escutcheon
Mythril Helmet
Diamond Armor
Spike Shoes

Level Jump8, Vertical Jump2
Battle Song, Space Storage



RoboticNomad
Female
Serpentarius
56
72
Priest
Throw
Critical Quick
Equip Bow
Teleport

Long Bow

Flash Hat
Wizard Robe
Leather Mantle

Cure, Cure 3, Cure 4, Raise 2, Regen, Protect, Shell 2, Wall, Esuna
Shuriken, Stick



Galkife
Female
Gemini
63
49
Thief
Elemental
PA Save
Short Status
Waterwalking

Kunai

Headgear
Adaman Vest
Feather Mantle

Steal Weapon, Steal Accessory, Leg Aim
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Lava Ball



Genericco
Monster
Gemini
49
75
Dragon








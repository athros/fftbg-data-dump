Player: !Green
Team: Green Team
Palettes: Green/White



TheRealRiggz
Male
Aries
48
62
Lancer
Elemental
Counter Magic
Dual Wield
Ignore Height

Spear
Obelisk
Iron Helmet
Carabini Mail
N-Kai Armlet

Level Jump2, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Lava Ball



Grandlanzer
Male
Scorpio
41
71
Bard
White Magic
Meatbone Slash
Equip Gun
Move-HP Up

Papyrus Codex

Black Hood
Earth Clothes
Feather Boots

Life Song, Cheer Song, Last Song
Cure 3, Cure 4, Esuna



Shalloween
Male
Capricorn
47
76
Mime

Critical Quick
Dual Wield
Levitate



Triangle Hat
Leather Outfit
Battle Boots

Mimic




Just Here2
Female
Libra
57
67
Priest
Battle Skill
HP Restore
Defend
Ignore Height

Healing Staff

Green Beret
Silk Robe
Feather Mantle

Cure 3, Raise, Protect 2, Wall, Esuna
Shield Break, Weapon Break, Speed Break, Justice Sword

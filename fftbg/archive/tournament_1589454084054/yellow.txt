Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Edgehead62888
Male
Capricorn
72
62
Priest
Item
MA Save
Equip Bow
Fly

Night Killer

Triangle Hat
Mythril Vest
Feather Boots

Cure 4, Raise, Regen, Protect, Shell 2, Esuna
Antidote, Maiden's Kiss, Remedy, Phoenix Down



Jeeboheebo
Female
Scorpio
73
42
Chemist
Throw
Abandon
Halve MP
Jump+2

Romanda Gun

Flash Hat
Wizard Outfit
Sprint Shoes

Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Shuriken, Knife



Roqqqpsi
Female
Capricorn
46
46
Ninja
Battle Skill
Arrow Guard
Short Status
Ignore Height

Sasuke Knife
Spell Edge
Feather Hat
Mythril Vest
Wizard Mantle

Staff
Shield Break, Magic Break



Tomion
Female
Serpentarius
52
66
Thief
Jump
Critical Quick
Equip Gun
Lava Walking

Fairy Harp

Black Hood
Black Costume
Jade Armlet

Steal Weapon
Level Jump2, Vertical Jump8

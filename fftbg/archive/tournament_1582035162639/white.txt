Player: !White
Team: White Team
Palettes: White/Blue



Lord Burrah
Female
Aquarius
48
78
Monk
Item
Dragon Spirit
Concentrate
Waterbreathing



Leather Hat
Power Sleeve
Elf Mantle

Pummel, Wave Fist, Earth Slash, Chakra, Seal Evil
Hi-Potion, Hi-Ether, Eye Drop, Holy Water, Remedy, Phoenix Down



EvilLego6
Monster
Sagittarius
65
50
Mindflayer










Off The Crossbar
Male
Scorpio
79
43
Archer
Summon Magic
Absorb Used MP
Maintenance
Fly

Long Bow

Flash Hat
Brigandine
Leather Mantle

Charge+1, Charge+2, Charge+4, Charge+10
Moogle, Titan, Golem, Odin, Silf, Fairy



MaouDono
Female
Aries
69
55
Mime

Counter
Equip Shield
Ignore Height


Ice Shield
Gold Helmet
Leather Outfit
Angel Ring

Mimic


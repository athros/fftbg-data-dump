Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SaintOmerville
Female
Pisces
61
77
Summoner
Item
Abandon
Equip Gun
Retreat

Battle Folio

Holy Miter
Judo Outfit
N-Kai Armlet

Shiva, Ramuh, Carbunkle, Salamander
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Sairentozon7
Male
Taurus
53
70
Mime

HP Restore
Martial Arts
Waterbreathing



Thief Hat
Clothes
Reflect Ring

Mimic




Heroebal
Male
Pisces
54
56
Archer
Item
Absorb Used MP
Throw Item
Lava Walking

Long Bow

Twist Headband
Wizard Outfit
Chantage

Charge+1, Charge+4, Charge+5
Potion, X-Potion, Ether, Antidote, Holy Water, Remedy



Baron Von Scrub
Female
Aquarius
40
56
Summoner
White Magic
Abandon
Dual Wield
Levitate

Dragon Rod
Faith Rod
Triangle Hat
Silk Robe
Germinas Boots

Moogle, Titan, Golem, Carbunkle, Bahamut, Odin, Salamander, Silf, Fairy
Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Shell, Shell 2, Wall, Esuna, Holy

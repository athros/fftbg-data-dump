Player: !Green
Team: Green Team
Palettes: Green/White



Fenaen
Monster
Libra
73
54
Minotaur










DHaveWord
Female
Aries
49
60
Monk
Charge
Meatbone Slash
Beastmaster
Retreat



Red Hood
Judo Outfit
Elf Mantle

Earth Slash
Charge+1, Charge+3, Charge+7, Charge+20



Catfashions
Male
Sagittarius
54
55
Mime

Distribute
Dual Wield
Jump+3



Feather Hat
Earth Clothes
Sprint Shoes

Mimic




Roqqqpsi
Female
Aries
53
72
Squire
Throw
Catch
Magic Defense UP
Swim

Sleep Sword
Hero Shield
Holy Miter
Earth Clothes
Defense Ring

Accumulate, Throw Stone, Heal, Cheer Up, Wish
Shuriken, Bomb, Spear

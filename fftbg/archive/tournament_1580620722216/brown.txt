Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rikrizen
Monster
Libra
39
74
Plague










RoikVS
Male
Capricorn
36
38
Knight
Punch Art
Speed Save
Beastmaster
Lava Walking

Ragnarok

Platinum Helmet
Wizard Robe
Dracula Mantle

Weapon Break, Mind Break, Stasis Sword
Earth Slash, Revive



Ominnous
Female
Taurus
67
49
Dancer
Jump
Counter Flood
Equip Sword
Move+3

Ryozan Silk

Feather Hat
Judo Outfit
Angel Ring

Wiznaibus, Slow Dance, Disillusion, Nether Demon, Dragon Pit
Level Jump5, Vertical Jump7



Cloudycube
Male
Gemini
48
48
Time Mage
Steal
HP Restore
Equip Bow
Ignore Height

Healing Staff

Headgear
Linen Robe
Feather Boots

Haste, Slow, Slow 2, Stop, Float, Reflect, Quick, Stabilize Time
Steal Heart, Arm Aim

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Victoriolue
Female
Taurus
69
65
Wizard
Draw Out
Faith Up
Defense UP
Move+1

Rod

Twist Headband
Mythril Vest
Feather Boots

Fire, Bolt, Bolt 2, Ice 3
Koutetsu, Kikuichimoji



Somegummybear
Male
Aries
47
71
Knight
Steal
Counter Magic
Attack UP
Move-MP Up

Coral Sword
Gold Shield
Bronze Helmet
Linen Cuirass
Dracula Mantle

Armor Break, Weapon Break
Gil Taking, Steal Helmet, Steal Accessory, Steal Status, Arm Aim



Feffle
Male
Taurus
77
57
Ninja
Draw Out
MP Restore
Maintenance
Move-MP Up

Flail
Mythril Knife
Headgear
Leather Vest
Feather Mantle

Bomb
Asura, Koutetsu, Bizen Boat, Murasame



Jinxzers
Male
Cancer
66
41
Oracle
Elemental
Catch
Equip Axe
Waterbreathing

Slasher

Headgear
Chameleon Robe
Reflect Ring

Blind, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Final Bets: white - 8 bets for 4,925G (55.6%, x0.80); black - 8 bets for 3,928G (44.4%, x1.25)

white bets:
SomthingMore: 2,000G (40.6%, 28,642G)
Aldrammech: 956G (19.4%, 956G)
DaveStrider55: 751G (15.2%, 751G)
ar_tactic: 350G (7.1%, 78,101G)
Tarheels218: 332G (6.7%, 332G)
MemoriesofFinal: 216G (4.4%, 216G)
datadrivenbot: 200G (4.1%, 62,029G)
Absalom_20: 120G (2.4%, 4,393G)

black bets:
BirbBrainsBot: 832G (21.2%, 48,396G)
getthemoneyz: 516G (13.1%, 1,750,033G)
MrPhuui: 500G (12.7%, 10,190G)
poorest_hobo: 500G (12.7%, 4,356G)
AllInBot: 452G (11.5%, 452G)
Sairentozon7: 428G (10.9%, 428G)
lord_merlin_11: 400G (10.2%, 2,500G)
gongonono: 300G (7.6%, 9,165G)

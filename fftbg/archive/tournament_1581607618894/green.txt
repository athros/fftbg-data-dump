Player: !Green
Team: Green Team
Palettes: Green/White



Jeeboheebo
Female
Capricorn
71
54
Chemist
Punch Art
Auto Potion
Equip Polearm
Lava Walking

Dragon Whisker

Holy Miter
Chain Vest
Defense Ring

Hi-Potion, Maiden's Kiss, Holy Water, Phoenix Down
Spin Fist, Wave Fist, Earth Slash, Purification, Revive



FlamingBlaze98
Female
Gemini
51
50
Time Mage
Draw Out
Earplug
Maintenance
Levitate

White Staff

Thief Hat
Adaman Vest
Red Shoes

Haste, Slow, Slow 2, Stop, Immobilize, Quick
Asura, Koutetsu, Bizen Boat, Muramasa



Galkife
Monster
Cancer
43
80
Steel Giant










Aviner42
Male
Scorpio
44
64
Calculator
Yin Yang Magic
Auto Potion
Equip Polearm
Jump+2

Battle Bamboo

Holy Miter
Adaman Vest
Germinas Boots

CT, Prime Number, 5
Blind, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify

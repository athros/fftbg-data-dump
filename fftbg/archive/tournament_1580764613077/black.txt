Player: !Black
Team: Black Team
Palettes: Black/Red



HSPsychoZA
Female
Aquarius
70
62
Oracle
Draw Out
MA Save
Equip Axe
Waterwalking

Rainbow Staff

Headgear
Brigandine
Defense Ring

Poison, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Dark Holy
Koutetsu, Kiyomori



Zeroroute
Female
Aquarius
63
69
Wizard
Basic Skill
Counter Flood
Equip Armor
Ignore Terrain

Air Knife

Genji Helmet
Secret Clothes
Reflect Ring

Fire 3, Bolt 3, Death, Flare
Dash, Heal, Tickle, Yell



Oreo Pizza
Female
Aries
71
63
Dancer
Item
MA Save
Defend
Waterbreathing

Ryozan Silk

Thief Hat
Clothes
Genji Gauntlet

Disillusion, Obsidian Blade, Dragon Pit
Potion, Hi-Potion, Hi-Ether, Echo Grass, Phoenix Down



Kl0kwurk
Female
Libra
64
43
Lancer
White Magic
Critical Quick
Magic Defense UP
Move+2

Obelisk
Genji Shield
Mythril Helmet
Genji Armor
Jade Armlet

Level Jump8, Vertical Jump8
Cure 3, Cure 4, Raise, Reraise, Shell, Esuna

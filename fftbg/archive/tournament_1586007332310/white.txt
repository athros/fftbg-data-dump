Player: !White
Team: White Team
Palettes: White/Blue



Rexamajinx
Female
Capricorn
71
59
Archer
Punch Art
Hamedo
Magic Defense UP
Jump+1

Romanda Gun
Bronze Shield
Cross Helmet
Secret Clothes
Spike Shoes

Charge+1, Charge+7, Charge+10
Wave Fist, Purification, Revive



Zagorsek
Female
Libra
79
56
Calculator
Black Magic
Arrow Guard
Beastmaster
Fly

Ice Rod

Black Hood
Wizard Outfit
108 Gems

CT, Height, Prime Number, 5, 4
Fire 3, Bolt 2, Bolt 4, Ice 2, Frog



SkeletorRS
Female
Aquarius
42
46
Thief
Time Magic
MP Restore
Equip Polearm
Jump+3

Holy Lance

Ribbon
Mystic Vest
Diamond Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Status, Arm Aim
Haste, Slow 2, Stop, Immobilize, Demi, Stabilize Time, Meteor



Lavian94
Male
Capricorn
59
47
Wizard
Punch Art
Abandon
Short Status
Waterwalking

Flame Rod

Thief Hat
Power Sleeve
Bracer

Ice 2, Death
Spin Fist

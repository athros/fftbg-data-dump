Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tuckerism
Female
Aquarius
65
44
Ninja
Time Magic
Arrow Guard
Beastmaster
Move+2

Cultist Dagger
Spell Edge
Red Hood
Wizard Outfit
Power Wrist

Knife, Hammer, Spear
Haste, Haste 2, Reflect, Quick, Demi, Stabilize Time



ALY327
Male
Scorpio
63
74
Ninja
Item
Counter Flood
Throw Item
Retreat

Air Knife
Cultist Dagger
Black Hood
Judo Outfit
Setiemson

Shuriken, Hammer, Axe, Stick, Wand
Hi-Potion, X-Potion, Ether, Hi-Ether, Holy Water, Phoenix Down



Rytor
Female
Libra
54
68
Ninja
Time Magic
Meatbone Slash
Sicken
Jump+3

Short Edge
Sasuke Knife
Thief Hat
Clothes
Feather Mantle

Shuriken, Staff, Wand
Haste, Slow, Stop, Immobilize, Reflect, Quick, Stabilize Time



Oreo Pizza
Male
Pisces
56
55
Ninja
Punch Art
Auto Potion
Magic Attack UP
Fly

Flail
Main Gauche
Black Hood
Earth Clothes
Genji Gauntlet

Knife
Spin Fist, Purification, Revive

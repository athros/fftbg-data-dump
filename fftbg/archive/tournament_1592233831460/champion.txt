Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Run With Stone GUNs
Male
Leo
51
59
Ninja
Item
Counter Flood
Equip Gun
Levitate

Battle Folio
Papyrus Codex
Headgear
Black Costume
Feather Boots

Shuriken
Potion, Hi-Potion, Hi-Ether, Antidote, Remedy, Phoenix Down



ShintaroNayaka
Male
Pisces
58
73
Samurai
Punch Art
Parry
Equip Polearm
Jump+3

Octagon Rod

Platinum Helmet
Mythril Armor
Genji Gauntlet

Asura, Heaven's Cloud, Kiyomori, Kikuichimoji
Wave Fist, Earth Slash, Purification, Chakra, Revive



Killth3kid
Male
Libra
71
44
Mime

Counter Magic
Monster Talk
Ignore Height



Leather Hat
Wizard Outfit
Dracula Mantle

Mimic




Mesmaster
Female
Pisces
46
67
Samurai
Elemental
MA Save
Beastmaster
Jump+3

Koutetsu Knife

Cross Helmet
Diamond Armor
Magic Gauntlet

Kikuichimoji
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

Player: !Green
Team: Green Team
Palettes: Green/White



Bloody Nips
Male
Aries
74
46
Time Mage
Punch Art
HP Restore
Dual Wield
Fly

Oak Staff
White Staff
Triangle Hat
Adaman Vest
Leather Mantle

Float, Demi, Demi 2
Pummel, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil



SarrgeQc
Female
Aries
54
51
Summoner
Battle Skill
Counter Magic
Martial Arts
Move-HP Up



Feather Hat
Light Robe
Angel Ring

Shiva, Ifrit, Carbunkle, Bahamut, Leviathan, Fairy
Mind Break, Stasis Sword



Nifboy
Male
Leo
53
72
Ninja
Black Magic
Sunken State
Equip Knife
Jump+1

Rod
Koga Knife
Green Beret
Leather Outfit
Battle Boots

Knife, Wand
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice



StealthModeLocke
Male
Pisces
44
75
Mediator
Punch Art
Speed Save
Dual Wield
Move-HP Up

Stone Gun
Glacier Gun
Triangle Hat
Chameleon Robe
Dracula Mantle

Invitation, Persuade, Praise, Preach, Solution, Negotiate, Refute, Rehabilitate
Wave Fist, Secret Fist, Purification, Revive

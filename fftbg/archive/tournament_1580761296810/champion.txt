Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Novapuppets
Male
Scorpio
77
65
Chemist
Charge
Counter Magic
Magic Attack UP
Levitate

Mythril Gun

Black Hood
Black Costume
Diamond Armlet

X-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down
Charge+3, Charge+7, Charge+10, Charge+20



Sinnyil2
Male
Gemini
78
75
Monk
Elemental
Damage Split
Beastmaster
Ignore Height



Flash Hat
Secret Clothes
Power Wrist

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Lowlf
Monster
Virgo
76
69
Tiamat










Kishi4
Male
Leo
41
65
Calculator
Limit
Earplug
Sicken
Lava Walking

Dragon Rod

Cachusha
Wizard Robe
Feather Boots

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

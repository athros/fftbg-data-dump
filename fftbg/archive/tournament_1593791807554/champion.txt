Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



L2 Sentinel
Male
Libra
49
72
Summoner
White Magic
Absorb Used MP
Short Charge
Retreat

Rod

Triangle Hat
Power Sleeve
Reflect Ring

Moogle, Ifrit, Titan, Golem, Carbunkle, Leviathan, Salamander
Cure 2, Raise, Wall, Esuna



Grininda
Male
Leo
70
45
Lancer
Item
Dragon Spirit
Dual Wield
Move+2

Ivory Rod
Obelisk
Diamond Helmet
Platinum Armor
Elf Mantle

Level Jump4, Vertical Jump8
Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down



Dexsana
Male
Aquarius
68
43
Geomancer
Talk Skill
Counter Flood
Attack UP
Move-HP Up

Giant Axe
Buckler
Headgear
Leather Outfit
Wizard Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Threaten, Preach, Solution, Insult, Negotiate, Refute



Thurman
Male
Taurus
76
73
Calculator
Bio
Damage Split
Attack UP
Waterbreathing

Bestiary
Flame Shield
Mythril Helmet
Light Robe
Small Mantle

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis

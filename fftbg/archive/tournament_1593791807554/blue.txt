Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



GrandmasterFrankerZ
Male
Libra
53
72
Samurai
Steal
Auto Potion
Equip Axe
Waterbreathing

Giant Axe

Mythril Helmet
Bronze Armor
Angel Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji
Steal Armor, Steal Accessory, Steal Status



Sandball
Female
Cancer
42
79
Summoner
White Magic
Speed Save
Magic Attack UP
Fly

Battle Folio

Triangle Hat
Silk Robe
Genji Gauntlet

Moogle, Golem, Leviathan, Silf
Cure, Cure 2, Cure 3, Cure 4, Raise 2, Regen, Shell



LDSkinny
Female
Aquarius
41
45
Oracle
Elemental
Catch
Short Charge
Ignore Terrain

Musk Rod

Holy Miter
Chameleon Robe
Rubber Shoes

Spell Absorb, Life Drain, Zombie, Blind Rage, Foxbird, Sleep, Petrify, Dark Holy
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Rabbitlogik
Female
Cancer
42
51
Dancer
Jump
PA Save
Magic Defense UP
Jump+2

Cashmere

Thief Hat
Mythril Vest
Cursed Ring

Wiznaibus, Disillusion, Nameless Dance, Last Dance
Level Jump3, Vertical Jump7

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Silentkaster
Monster
Gemini
64
69
Ghost










BoneMiser
Male
Scorpio
58
46
Bard
Yin Yang Magic
Parry
Dual Wield
Fly

Bloody Strings
Fairy Harp
Barette
Crystal Mail
Magic Ring

Battle Song, Nameless Song, Diamond Blade
Blind, Spell Absorb, Life Drain, Pray Faith, Dispel Magic, Paralyze, Petrify



Kronikle
Male
Taurus
45
44
Knight
Basic Skill
Abandon
Maintenance
Ignore Terrain

Slasher
Mythril Shield
Genji Helmet
Chain Mail
Sprint Shoes

Shield Break, Justice Sword, Dark Sword
Accumulate, Throw Stone, Heal, Yell, Cheer Up, Fury



Beowulfrulez
Female
Libra
42
42
Monk
Yin Yang Magic
Faith Save
Defense UP
Move+3



Feather Hat
Clothes
Battle Boots

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Sleep

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Loverish
Monster
Libra
54
41
Apanda










Kohlingen
Male
Leo
51
54
Wizard
Summon Magic
Counter
Equip Polearm
Jump+1

Cypress Rod

Flash Hat
Silk Robe
Dracula Mantle

Fire, Fire 2, Fire 4, Bolt 4, Ice, Frog, Flare
Moogle, Golem, Carbunkle, Odin, Lich



Zeroroute
Female
Cancer
48
66
Thief
Time Magic
Parry
Short Charge
Move+2

Hidden Knife

Cachusha
Power Sleeve
Genji Gauntlet

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Slow, Slow 2, Float, Demi, Demi 2, Stabilize Time, Meteor



Tougou
Male
Taurus
53
49
Thief
Throw
Caution
Equip Gun
Move-HP Up

Bestiary

Green Beret
Adaman Vest
Genji Gauntlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Arm Aim
Staff, Stick

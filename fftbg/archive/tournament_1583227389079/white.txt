Player: !White
Team: White Team
Palettes: White/Blue



Hasterious
Male
Aries
53
55
Ninja
Yin Yang Magic
Arrow Guard
Equip Shield
Ignore Height

Ninja Edge
Escutcheon
Triangle Hat
Judo Outfit
Genji Gauntlet

Sword
Spell Absorb, Life Drain, Pray Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Silphede1
Female
Taurus
56
64
Wizard
Yin Yang Magic
PA Save
Equip Knife
Waterwalking

Sasuke Knife

Cachusha
Light Robe
Small Mantle

Fire 2, Bolt, Ice 4, Empower, Flare
Blind, Poison, Life Drain, Silence Song, Paralyze, Sleep



TravisAU
Male
Gemini
63
62
Lancer
White Magic
Distribute
Secret Hunt
Swim

Musk Rod
Platinum Shield
Platinum Helmet
Crystal Mail
Battle Boots

Level Jump8, Vertical Jump6
Cure 4, Raise, Protect, Protect 2, Shell, Esuna, Holy



Brando84back
Female
Leo
52
44
Priest
Summon Magic
Critical Quick
Equip Knife
Levitate

Blind Knife

Thief Hat
White Robe
Red Shoes

Cure, Raise, Protect 2, Shell 2, Esuna
Moogle, Carbunkle, Lich, Cyclops

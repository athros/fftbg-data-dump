Final Bets: red - 15 bets for 7,118G (30.9%, x2.24); blue - 21 bets for 15,950G (69.1%, x0.45)

red bets:
YaBoy125: 1,058G (14.9%, 1,058G)
Aldrammech: 1,000G (14.0%, 12,292G)
DeathTaxesAndAnime: 588G (8.3%, 588G)
NovaKnight21: 560G (7.9%, 560G)
HaateXIII: 514G (7.2%, 514G)
Baron_von_Scrub: 512G (7.2%, 512G)
killth3kid: 500G (7.0%, 9,792G)
serperemagus: 500G (7.0%, 3,526G)
Evewho: 416G (5.8%, 416G)
electric_algus: 333G (4.7%, 6,134G)
DudeMonkey77: 333G (4.7%, 4,893G)
IanTheWoodchuck: 300G (4.2%, 2,023G)
skillomono: 300G (4.2%, 7,318G)
krasny1944: 104G (1.5%, 104G)
AllInBot: 100G (1.4%, 100G)

blue bets:
NicoSavoy: 2,000G (12.5%, 49,683G)
Mesmaster: 2,000G (12.5%, 74,190G)
HaplessOne: 1,500G (9.4%, 232,342G)
Zeroroute: 1,447G (9.1%, 3,100G)
Vex_Novisk: 1,000G (6.3%, 28,577G)
thefyeman: 1,000G (6.3%, 60,257G)
EnemyController: 1,000G (6.3%, 317,224G)
BirbBrainsBot: 1,000G (6.3%, 110,657G)
metagameface: 1,000G (6.3%, 6,894G)
genericco: 601G (3.8%, 601G)
CorpusCav: 600G (3.8%, 24,205G)
Jeeboheebo: 500G (3.1%, 3,724G)
twelfthrootoftwo: 500G (3.1%, 31,732G)
AUrato: 463G (2.9%, 11,578G)
Lali_Lulelo: 388G (2.4%, 388G)
neocarbuncle: 350G (2.2%, 1,404G)
gorgewall: 201G (1.3%, 9,925G)
OneHundredFists: 100G (0.6%, 2,438G)
Firesheath: 100G (0.6%, 1,651G)
CosmicTactician: 100G (0.6%, 17,497G)
datadrivenbot: 100G (0.6%, 22,810G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Maeveen
Male
Scorpio
48
58
Summoner
Steal
Abandon
Equip Sword
Move+2

Flame Rod

Twist Headband
Mystic Vest
Sprint Shoes

Moogle, Ramuh, Ifrit, Titan, Bahamut, Odin, Leviathan, Silf, Fairy, Lich
Steal Helmet, Steal Accessory



Porkbacon
Female
Sagittarius
39
79
Chemist
Jump
Counter Flood
Beastmaster
Move-HP Up

Dagger

Barette
Chain Vest
Feather Boots

Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Level Jump4, Vertical Jump8



Peeweemerman
Male
Serpentarius
45
47
Chemist
Basic Skill
Parry
Equip Armor
Ignore Terrain

Romanda Gun

Circlet
Bronze Armor
Reflect Ring

Potion, Hi-Potion, Maiden's Kiss, Holy Water, Phoenix Down
Heal, Yell



KoreanUsher
Male
Aries
69
80
Ninja
Talk Skill
Dragon Spirit
Monster Talk
Move+2

Flail
Flame Whip
Feather Hat
Mythril Vest
Rubber Shoes

Shuriken, Wand
Persuade, Solution, Negotiate

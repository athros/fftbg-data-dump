Player: !White
Team: White Team
Palettes: White/Blue



Galkife
Male
Taurus
41
76
Ninja
White Magic
Regenerator
Equip Bow
Swim

Long Bow

Golden Hairpin
Leather Outfit
Bracer

Shuriken, Staff, Dictionary
Cure, Raise, Wall, Esuna, Holy



Lanshaft
Female
Capricorn
73
78
Summoner
Elemental
Abandon
Equip Bow
Swim

Bow Gun

Golden Hairpin
Black Robe
Elf Mantle

Moogle, Ramuh, Carbunkle, Silf, Lich, Zodiac
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Evewho
Female
Aries
65
35
Chemist
Elemental
Caution
Defense UP
Jump+1

Main Gauche

Green Beret
Black Costume
Feather Mantle

Potion, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball



Unclebearboy
Female
Virgo
54
56
Summoner
Time Magic
Distribute
Doublehand
Move-HP Up

Thunder Rod

Red Hood
Chameleon Robe
Genji Gauntlet

Moogle, Ramuh, Golem, Bahamut, Leviathan, Silf
Haste, Stop, Float, Quick, Demi, Stabilize Time, Galaxy Stop

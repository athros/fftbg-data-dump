Final Bets: purple - 15 bets for 9,313G (38.5%, x1.60); brown - 19 bets for 14,870G (61.5%, x0.63)

purple bets:
UmaiJam: 1,369G (14.7%, 13,698G)
NovaKnight21: 1,083G (11.6%, 1,083G)
BirbBrainsBot: 1,000G (10.7%, 117,687G)
inzo_bettingbot: 1,000G (10.7%, 59,263G)
bad1dea: 1,000G (10.7%, 327,455G)
TheRobbieRobster: 836G (9.0%, 836G)
SkylerBunny: 822G (8.8%, 822G)
OmnibotGamma: 500G (5.4%, 3,467G)
Evewho: 500G (5.4%, 5,276G)
getthemoneyz: 340G (3.7%, 405,260G)
ColetteMSLP: 300G (3.2%, 5,612G)
hyvi_: 243G (2.6%, 243G)
upvla: 120G (1.3%, 1,257G)
Lythe_Caraker: 100G (1.1%, 45,756G)
BadBlanket: 100G (1.1%, 1,496G)

brown bets:
sinnyil2: 7,844G (52.8%, 15,688G)
Treapvort: 1,000G (6.7%, 19,574G)
Shalloween: 800G (5.4%, 48,088G)
Ant_ihistamine: 729G (4.9%, 729G)
Spirit_NB: 660G (4.4%, 660G)
BenYuPoker: 576G (3.9%, 576G)
jethrothrul: 504G (3.4%, 504G)
DustBirdEX: 500G (3.4%, 1,822G)
Lionhermit: 500G (3.4%, 4,034G)
SleepyBenzo: 322G (2.2%, 322G)
Reiniku: 236G (1.6%, 236G)
hkbadboy: 216G (1.5%, 216G)
ugoplatamia: 200G (1.3%, 21,432G)
alecttox: 200G (1.3%, 16,329G)
InOzWeTrust: 197G (1.3%, 197G)
RezzaDV: 136G (0.9%, 136G)
datadrivenbot: 100G (0.7%, 16,701G)
victoriolue: 100G (0.7%, 5,019G)
sixstop: 50G (0.3%, 1,513G)

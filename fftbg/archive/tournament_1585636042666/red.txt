Player: !Red
Team: Red Team
Palettes: Red/Brown



Oogthecaveman
Male
Scorpio
62
56
Priest
Steal
Earplug
Equip Axe
Teleport

Flame Whip

Twist Headband
Linen Robe
Cursed Ring

Raise, Reraise, Regen, Protect, Shell, Shell 2, Esuna
Gil Taking, Steal Shield, Steal Weapon



BenYuPoker
Male
Libra
47
78
Mime

Counter
Dual Wield
Jump+3



Feather Hat
Leather Outfit
Dracula Mantle

Mimic




Lionhermit
Female
Cancer
59
75
Mime

Regenerator
Short Status
Move-MP Up



Triangle Hat
Mystic Vest
N-Kai Armlet

Mimic




SeedSC
Female
Leo
78
76
Geomancer
Summon Magic
Counter Tackle
Short Charge
Jump+2

Rune Blade
Round Shield
Triangle Hat
Chameleon Robe
Jade Armlet

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Bahamut, Leviathan

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Stormpyre
Male
Aquarius
59
70
Thief
Yin Yang Magic
Arrow Guard
Maintenance
Waterbreathing

Air Knife

Green Beret
Leather Outfit
Jade Armlet

Steal Shield, Steal Accessory, Arm Aim
Blind, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Paralyze, Petrify



YourGMJack
Male
Aries
50
77
Thief
Yin Yang Magic
Catch
Equip Gun
Move+1

Blast Gun

Black Hood
Power Sleeve
Diamond Armlet

Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Leg Aim
Zombie, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy



Tinytittyfiend
Female
Cancer
42
68
Knight
Time Magic
Dragon Spirit
Doublehand
Jump+2

Battle Axe

Circlet
Carabini Mail
Rubber Shoes

Head Break, Stasis Sword, Surging Sword
Haste, Stop, Float, Reflect, Stabilize Time, Meteor



The Pengwin
Male
Sagittarius
54
63
Monk
Jump
Distribute
Attack UP
Fly



Headgear
Brigandine
Spike Shoes

Pummel, Purification, Chakra, Revive
Level Jump5, Vertical Jump2

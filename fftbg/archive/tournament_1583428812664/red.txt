Player: !Red
Team: Red Team
Palettes: Red/Brown



RughSontos
Male
Virgo
47
50
Bard
Item
Damage Split
Short Charge
Levitate

Fairy Harp

Triangle Hat
Carabini Mail
Defense Ring

Life Song, Battle Song, Diamond Blade, Space Storage, Sky Demon
Potion, Ether, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Eldente
Female
Cancer
54
52
Calculator
Limit
Mana Shield
Equip Knife
Ignore Height

Rod
Crystal Shield
Cross Helmet
Bronze Armor
Elf Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Goust18
Female
Taurus
56
76
Lancer
Time Magic
Arrow Guard
Doublehand
Fly

Obelisk

Gold Helmet
Diamond Armor
N-Kai Armlet

Level Jump8, Vertical Jump7
Haste, Slow, Stop, Quick, Stabilize Time, Meteor



Pizzapartytime
Male
Aries
69
65
Wizard
Item
Arrow Guard
Throw Item
Jump+1

Rod

Flash Hat
Linen Robe
Battle Boots

Fire 3, Bolt, Bolt 4, Frog
Ether, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down

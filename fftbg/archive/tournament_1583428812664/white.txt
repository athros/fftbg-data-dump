Player: !White
Team: White Team
Palettes: White/Blue



Rassyu
Female
Sagittarius
74
67
Archer
Throw
Counter
Short Status
Move-HP Up

Mythril Gun
Mythril Shield
Green Beret
Mythril Vest
Spike Shoes

Charge+1, Charge+2, Charge+3, Charge+7, Charge+20
Shuriken, Hammer



AM3P Batman
Male
Sagittarius
62
41
Monk
Battle Skill
Regenerator
Sicken
Waterbreathing



Headgear
Power Sleeve
Rubber Shoes

Purification, Chakra
Shield Break, Speed Break, Power Break, Mind Break, Dark Sword



Sauceatron
Female
Scorpio
61
41
Lancer
Black Magic
Caution
Long Status
Retreat

Javelin
Bronze Shield
Gold Helmet
Linen Cuirass
Genji Gauntlet

Level Jump8, Vertical Jump7
Bolt 3



Pteraranger
Female
Leo
57
71
Chemist
Throw
Catch
Halve MP
Jump+3

Blast Gun

Holy Miter
Leather Outfit
Red Shoes

Potion, Ether, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down
Axe

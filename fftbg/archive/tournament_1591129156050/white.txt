Player: !White
Team: White Team
Palettes: White/Blue



HaychDub
Male
Taurus
45
77
Geomancer
Black Magic
HP Restore
Beastmaster
Fly

Heaven's Cloud
Round Shield
Feather Hat
Clothes
Red Shoes

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Bolt 2, Bolt 3, Bolt 4, Ice 2, Frog



Nickyfive
Male
Gemini
71
71
Chemist
Yin Yang Magic
Speed Save
Defend
Swim

Romanda Gun

Headgear
Earth Clothes
Feather Boots

Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down
Poison, Spell Absorb, Silence Song, Blind Rage



CrownOfHorns
Female
Aquarius
73
53
Wizard
Time Magic
PA Save
Defense UP
Jump+2

Dragon Rod

Black Hood
Adaman Vest
Feather Boots

Fire 4, Bolt 3, Ice 3, Ice 4, Flare
Reflect, Quick



Wyonearth
Female
Sagittarius
67
58
Time Mage
Jump
Absorb Used MP
Concentrate
Jump+1

Oak Staff

Green Beret
Earth Clothes
Battle Boots

Haste 2, Float, Reflect, Demi
Level Jump2, Vertical Jump7

Final Bets: red - 23 bets for 10,354G (46.5%, x1.15); blue - 19 bets for 11,920G (53.5%, x0.87)

red bets:
LanseDM: 1,154G (11.1%, 1,154G)
Jeeboheebo: 1,000G (9.7%, 14,112G)
AlenaZarek: 1,000G (9.7%, 32,305G)
LordTomS: 834G (8.1%, 834G)
OneHundredFists: 763G (7.4%, 763G)
Laserman1000: 720G (7.0%, 30,720G)
Nickyfive: 596G (5.8%, 596G)
YaBoy125: 556G (5.4%, 556G)
Rislyeu: 500G (4.8%, 4,635G)
HaateXIII: 500G (4.8%, 5,475G)
DrPoopersMD: 500G (4.8%, 1,403G)
Cryptopsy70: 480G (4.6%, 3,333G)
EnemyController: 300G (2.9%, 479,553G)
Snoopiku: 250G (2.4%, 5,649G)
Flameatron: 200G (1.9%, 3,315G)
JoeykinsGaming: 200G (1.9%, 14,589G)
s_o_b_e_q: 200G (1.9%, 2,778G)
Firesheath: 101G (1.0%, 1,963G)
CorpusCav: 100G (1.0%, 14,471G)
nhammen: 100G (1.0%, 1,039G)
datadrivenbot: 100G (1.0%, 32,373G)
mrfripps: 100G (1.0%, 553G)
porkchop5158: 100G (1.0%, 482G)

blue bets:
Creggers: 2,766G (23.2%, 2,766G)
Shalloween: 1,449G (12.2%, 1,449G)
TheMM42: 1,000G (8.4%, 10,213G)
BirbBrainsBot: 1,000G (8.4%, 153,975G)
UmaiJam: 1,000G (8.4%, 15,117G)
Candina: 999G (8.4%, 38,523G)
itsadud: 920G (7.7%, 920G)
Digitalsocrates: 500G (4.2%, 5,118G)
wyonearth: 400G (3.4%, 3,760G)
getthemoneyz: 302G (2.5%, 799,066G)
cam_ATS: 224G (1.9%, 224G)
ColetteMSLP: 208G (1.7%, 208G)
Nohurty: 208G (1.7%, 208G)
gogofromtogo: 200G (1.7%, 4,788G)
Meta_Five: 200G (1.7%, 9,727G)
WitchHunterIX: 184G (1.5%, 184G)
Belkra: 160G (1.3%, 320G)
CosmicTactician: 100G (0.8%, 5,272G)
RuneS_77: 100G (0.8%, 7,972G)

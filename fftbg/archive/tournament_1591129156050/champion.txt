Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Eudes89
Female
Capricorn
54
68
Wizard
Summon Magic
MA Save
Concentrate
Waterbreathing

Flame Rod

Holy Miter
Judo Outfit
Cursed Ring

Fire, Bolt, Bolt 2, Ice 4, Frog, Flare
Moogle, Ramuh, Ifrit, Odin, Leviathan



Ross From Cali
Female
Gemini
78
46
Knight
Elemental
Meatbone Slash
Maintenance
Jump+2

Ancient Sword
Ice Shield
Leather Helmet
Linen Cuirass
Small Mantle

Magic Break, Night Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Lowlf
Monster
Aries
60
56
Steel Giant










Wyonearth
Female
Gemini
40
76
Lancer
Dance
MA Save
Attack UP
Teleport

Holy Lance
Escutcheon
Circlet
Chameleon Robe
Jade Armlet

Level Jump5, Vertical Jump8
Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon

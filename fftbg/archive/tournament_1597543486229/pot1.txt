Final Bets: red - 11 bets for 16,842G (70.8%, x0.41); blue - 12 bets for 6,943G (29.2%, x2.43)

red bets:
TasisSai: 9,915G (58.9%, 9,915G)
Firesheath: 3,912G (23.2%, 3,912G)
goth_Applebees_hostess: 1,000G (5.9%, 2,230G)
Forkmore: 520G (3.1%, 520G)
readdesert: 486G (2.9%, 486G)
Chuckolator: 384G (2.3%, 384G)
datadrivenbot: 200G (1.2%, 64,238G)
arctodus13: 124G (0.7%, 124G)
gorgewall: 101G (0.6%, 4,030G)
itsonlyspencer: 100G (0.6%, 1,267G)
ANFz: 100G (0.6%, 3,245G)

blue bets:
Thyrandaal: 2,345G (33.8%, 252,432G)
SkylerBunny: 1,000G (14.4%, 9,946G)
BirbBrainsBot: 1,000G (14.4%, 164,610G)
getthemoneyz: 914G (13.2%, 1,650,324G)
Shalloween: 600G (8.6%, 11,183G)
Tarheels218: 300G (4.3%, 5,186G)
3ngag3: 234G (3.4%, 1,838G)
AllInBot: 174G (2.5%, 174G)
CosmicTactician: 100G (1.4%, 22,600G)
itskage: 100G (1.4%, 2,789G)
douchetron: 100G (1.4%, 11,308G)
theBinklive: 76G (1.1%, 305G)

Player: !Green
Team: Green Team
Palettes: Green/White



Phytik
Male
Aries
66
65
Geomancer
Talk Skill
HP Restore
Monster Talk
Jump+2

Mythril Sword
Mythril Shield
Headgear
Mystic Vest
Angel Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Praise, Preach, Insult, Negotiate, Mimic Daravon, Rehabilitate



ZephyrTempest
Monster
Cancer
80
61
Steel Giant










Maakur
Male
Aquarius
61
44
Knight
Black Magic
Faith Up
Maintenance
Waterwalking

Battle Axe
Kaiser Plate
Circlet
Maximillian
Wizard Mantle

Head Break, Speed Break, Mind Break
Fire 2, Fire 4, Bolt, Bolt 4, Ice 3



HaplessOne
Male
Sagittarius
61
65
Ninja
White Magic
Regenerator
Concentrate
Move-HP Up

Short Edge
Assassin Dagger
Green Beret
Brigandine
Salty Rage

Shuriken, Bomb, Knife, Wand
Cure, Cure 4, Raise, Reraise, Shell 2, Wall, Esuna

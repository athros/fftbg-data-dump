Player: !Black
Team: Black Team
Palettes: Black/Red



NicoSavoy
Female
Aquarius
58
53
Calculator
Time Magic
Absorb Used MP
Halve MP
Retreat

Dragon Rod

Triangle Hat
Power Sleeve
Magic Gauntlet

CT, Height, Prime Number, 3
Stop, Reflect, Quick, Stabilize Time



ALY327
Female
Gemini
61
78
Geomancer
Draw Out
Distribute
Equip Sword
Waterbreathing

Coral Sword
Diamond Shield
Black Hood
Black Robe
Magic Ring

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Asura, Bizen Boat, Muramasa, Kikuichimoji



Syndony
Female
Sagittarius
44
60
Time Mage
Dance
Counter Magic
Concentrate
Jump+2

Gold Staff

Holy Miter
Robe of Lords
Bracer

Haste 2, Slow, Stop, Float, Reflect, Demi, Demi 2, Meteor
Polka Polka, Nameless Dance, Last Dance, Dragon Pit



Ambrosio
Male
Leo
62
59
Monk
Black Magic
Auto Potion
Attack UP
Lava Walking



Holy Miter
Chain Vest
Rubber Shoes

Pummel, Secret Fist, Purification, Revive
Fire 2, Bolt, Bolt 2, Bolt 3, Ice, Death, Flare

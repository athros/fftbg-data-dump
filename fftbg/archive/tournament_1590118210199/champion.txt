Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Deathmaker06
Male
Gemini
73
63
Lancer
Black Magic
Counter
Magic Defense UP
Move+1

Mythril Spear
Aegis Shield
Barbuta
Linen Cuirass
Magic Ring

Level Jump8, Vertical Jump6
Bolt 4



Kronikle
Female
Aries
55
40
Geomancer
Steal
PA Save
Magic Attack UP
Waterbreathing

Iron Sword
Ice Shield
Leather Hat
Adaman Vest
Bracer

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Steal Armor, Steal Accessory



Killth3kid
Male
Sagittarius
52
48
Samurai
Charge
Damage Split
Beastmaster
Teleport 2

Masamune

Iron Helmet
Chameleon Robe
Chantage

Murasame, Muramasa, Kikuichimoji
Charge+3



ItsZaes
Female
Sagittarius
71
76
Calculator
White Magic
Earplug
Equip Bow
Fly

Ice Bow

Headgear
Adaman Vest
Magic Gauntlet

Height, 5, 4, 3
Cure, Cure 3, Cure 4, Raise, Raise 2, Reraise, Shell 2, Wall, Esuna, Holy

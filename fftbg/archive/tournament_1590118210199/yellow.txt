Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ZZ Yoshi
Female
Scorpio
59
59
Ninja
Yin Yang Magic
Speed Save
Sicken
Waterwalking

Flail
Sasuke Knife
Leather Hat
Leather Outfit
Bracer

Shuriken, Knife, Spear
Poison, Doubt Faith, Confusion Song, Dispel Magic, Dark Holy



Porkchop5158
Male
Aquarius
82
63
Chemist
Black Magic
Caution
Long Status
Jump+2

Mythril Gun

Twist Headband
Power Sleeve
Reflect Ring

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Flare



Gooseyourself
Female
Aries
43
58
Dancer
White Magic
Dragon Spirit
Defense UP
Move-MP Up

Persia

Leather Hat
Light Robe
Feather Mantle

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Obsidian Blade, Nether Demon
Cure, Cure 2, Cure 3, Raise, Protect, Wall, Esuna



Baron Von Scrub
Female
Aries
42
56
Thief
Punch Art
Sunken State
Doublehand
Jump+2

Koga Knife

Headgear
Black Costume
Defense Ring

Gil Taking, Steal Armor
Pummel, Earth Slash, Secret Fist, Purification

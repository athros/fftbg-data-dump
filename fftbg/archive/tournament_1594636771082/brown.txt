Player: !Brown
Team: Brown Team
Palettes: Brown/Green



JethroThrul
Male
Aquarius
50
66
Bard
Talk Skill
Arrow Guard
Monster Talk
Levitate

Fairy Harp

Leather Hat
Adaman Vest
Spike Shoes

Angel Song, Battle Song, Magic Song, Last Song, Hydra Pit
Persuade, Preach, Solution, Refute



Digitalsocrates
Monster
Aquarius
44
50
Squidraken










Dem0nj0ns
Male
Aries
57
69
Lancer
Time Magic
Absorb Used MP
Doublehand
Jump+3

Spear

Cross Helmet
Leather Armor
Battle Boots

Level Jump8, Vertical Jump8
Slow, Stop, Immobilize, Reflect, Stabilize Time



Laila
Female
Scorpio
49
55
Thief
Throw
Meatbone Slash
Halve MP
Fly

Orichalcum

Holy Miter
Secret Clothes
Leather Mantle

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Steal Status
Shuriken, Bomb

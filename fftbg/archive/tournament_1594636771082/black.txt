Player: !Black
Team: Black Team
Palettes: Black/Red



Upvla
Male
Scorpio
46
45
Ninja
Summon Magic
Absorb Used MP
Equip Gun
Move+2

Stone Gun
Stone Gun
Holy Miter
Adaman Vest
Feather Mantle

Dictionary
Moogle, Shiva, Carbunkle, Fairy



Jaltos
Female
Taurus
75
59
Archer
Summon Magic
Parry
Short Status
Retreat

Ice Bow

Thief Hat
Power Sleeve
Germinas Boots

Charge+3
Moogle, Ramuh, Titan, Salamander, Silf



Turbn
Male
Scorpio
50
58
Mime

Counter Flood
Monster Talk
Swim



Holy Miter
Brigandine
Defense Ring

Mimic




Cassie
Female
Cancer
71
52
Mediator
Draw Out
Counter Tackle
Magic Defense UP
Move-MP Up

Mythril Gun

Holy Miter
Clothes
Small Mantle

Persuade, Praise, Threaten, Death Sentence, Mimic Daravon, Rehabilitate
Koutetsu, Murasame, Kiyomori

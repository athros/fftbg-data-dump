Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Namamu
Monster
Aquarius
70
60
Porky










CandyTamer
Female
Taurus
51
77
Samurai
Dance
PA Save
Magic Defense UP
Jump+3

Partisan

Crystal Helmet
Maximillian
Cursed Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji
Witch Hunt, Slow Dance, Nameless Dance



Galkife
Monster
Scorpio
65
70
Plague










Spartan Paladin
Male
Leo
49
53
Knight
Summon Magic
Speed Save
Equip Polearm
Jump+2

Obelisk
Hero Shield
Diamond Helmet
Black Robe
Defense Armlet

Armor Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Justice Sword, Surging Sword
Ifrit, Golem, Salamander

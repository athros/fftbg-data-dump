Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Moshyhero
Male
Leo
41
67
Bard
Draw Out
Arrow Guard
Martial Arts
Jump+3



Feather Hat
Mystic Vest
Angel Ring

Angel Song, Life Song, Cheer Song, Magic Song, Last Song, Hydra Pit
Asura, Murasame, Muramasa



ApplesauceBoss
Male
Aries
48
40
Thief
Charge
Damage Split
Defend
Jump+1

Sasuke Knife

Thief Hat
Mystic Vest
Feather Boots

Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Leg Aim
Charge+1, Charge+2, Charge+7, Charge+20



Goodnightrobo
Female
Capricorn
75
40
Squire
Charge
Counter Tackle
Equip Polearm
Levitate

Obelisk
Flame Shield
Leather Hat
Mythril Vest
Defense Ring

Fury, Wish
Charge+2, Charge+4, Charge+7



Digitalsocrates
Female
Aries
64
77
Mime

Meatbone Slash
Magic Defense UP
Waterbreathing



Golden Hairpin
Clothes
Diamond Armlet

Mimic


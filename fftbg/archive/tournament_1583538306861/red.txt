Player: !Red
Team: Red Team
Palettes: Red/Brown



Rocl
Male
Capricorn
39
72
Wizard
Item
Mana Shield
Throw Item
Move+2

Flame Rod

Holy Miter
Silk Robe
Battle Boots

Bolt 2, Bolt 4, Ice, Ice 2, Empower, Frog, Death
Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Mirapoix
Male
Cancer
56
64
Mime

Abandon
Secret Hunt
Move+3



Genji Helmet
Black Costume
Genji Gauntlet

Mimic




E7bbk
Female
Scorpio
58
61
Dancer
Throw
Counter Flood
Defense UP
Move-MP Up

Ryozan Silk

Black Hood
Black Robe
Leather Mantle

Slow Dance, Polka Polka, Disillusion, Void Storage
Hammer, Stick, Dictionary



KazJazz
Female
Gemini
48
50
Calculator
White Magic
MA Save
Secret Hunt
Retreat

Cypress Rod

Golden Hairpin
Brigandine
Small Mantle

CT, Height, Prime Number, 5
Cure 4, Raise, Raise 2, Reraise, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



ALY327
Female
Pisces
65
58
Dancer
Black Magic
Dragon Spirit
Equip Axe
Move-HP Up

Slasher

Leather Hat
Silk Robe
Defense Armlet

Witch Hunt
Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Empower



J2DaBibbles
Male
Aquarius
56
80
Archer
Yin Yang Magic
Caution
Dual Wield
Waterbreathing

Silver Bow

Circlet
Black Costume
Small Mantle

Charge+1, Charge+3, Charge+20
Poison, Pray Faith, Doubt Faith, Confusion Song, Petrify



Aerochris121
Female
Cancer
78
72
Geomancer
Summon Magic
Regenerator
Equip Axe
Lava Walking

Sage Staff
Ice Shield
Black Hood
Linen Robe
Spike Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Shiva, Ramuh, Titan, Carbunkle, Salamander



Marin1987
Female
Sagittarius
76
69
Thief
Item
Counter
Attack UP
Levitate

Sasuke Knife

Headgear
Black Costume
Reflect Ring

Steal Status, Arm Aim, Leg Aim
Potion, Ether, Echo Grass, Soft, Phoenix Down

Final Bets: blue - 11 bets for 13,147G (45.7%, x1.19); yellow - 19 bets for 15,623G (54.3%, x0.84)

blue bets:
JumbocactuarX27: 4,201G (32.0%, 8,402G)
DavenIII: 3,562G (27.1%, 7,125G)
eudes89: 1,036G (7.9%, 1,036G)
TheChainNerd: 1,000G (7.6%, 13,258G)
EnemyController: 1,000G (7.6%, 270,521G)
E_Ballard: 600G (4.6%, 600G)
reinoe: 600G (4.6%, 600G)
nifboy: 424G (3.2%, 424G)
FriendSquirrel: 360G (2.7%, 360G)
GladiatorLupe: 200G (1.5%, 5,518G)
MattMan119: 164G (1.2%, 164G)

yellow bets:
sinnyil2: 8,300G (53.1%, 16,601G)
HaplessOne: 1,111G (7.1%, 162,945G)
BirbBrainsBot: 1,000G (6.4%, 50,607G)
red__lancer: 1,000G (6.4%, 70,327G)
d4rr1n: 755G (4.8%, 755G)
CosmicTactician: 500G (3.2%, 22,515G)
ThePineappleSalesman: 456G (2.9%, 456G)
asherban: 385G (2.5%, 385G)
Jeeboheebo: 356G (2.3%, 356G)
DLJuggernaut: 350G (2.2%, 1,418G)
ShintaroNayaka: 344G (2.2%, 344G)
josephiroth_143: 216G (1.4%, 216G)
skillomono: 200G (1.3%, 1,454G)
getthemoneyz: 150G (1.0%, 687,232G)
maakur_: 100G (0.6%, 2,929G)
Estan_AD: 100G (0.6%, 3,358G)
datadrivenbot: 100G (0.6%, 23,567G)
Kyune: 100G (0.6%, 14,376G)
Evewho: 100G (0.6%, 26,078G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Maakur
Male
Leo
54
51
Ninja
Time Magic
Regenerator
Attack UP
Teleport

Hidden Knife
Morning Star
Golden Hairpin
Secret Clothes
Vanish Mantle

Shuriken, Ninja Sword, Spear
Stop, Immobilize, Quick, Demi, Demi 2



HaplessOne
Female
Capricorn
54
69
Oracle
Steal
MA Save
Doublehand
Move+2

Octagon Rod

Triangle Hat
Power Sleeve
Genji Gauntlet

Poison, Spell Absorb, Silence Song, Paralyze, Sleep, Petrify
Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Arm Aim



EnemyController
Male
Cancer
66
80
Monk
Yin Yang Magic
Arrow Guard
Equip Axe
Lava Walking

Slasher

Flash Hat
Earth Clothes
Vanish Mantle

Spin Fist, Secret Fist, Purification, Revive
Poison, Pray Faith, Doubt Faith, Zombie, Dispel Magic, Sleep



Volgrathemoose
Male
Sagittarius
38
45
Samurai
Talk Skill
Caution
Beastmaster
Lava Walking

Murasame

Circlet
Maximillian
Leather Mantle

Koutetsu, Bizen Boat
Negotiate, Refute

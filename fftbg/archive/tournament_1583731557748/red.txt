Player: !Red
Team: Red Team
Palettes: Red/Brown



Gelwain
Male
Aries
60
45
Mime

Catch
Maintenance
Move-HP Up



Triangle Hat
Brigandine
Red Shoes

Mimic




Zetchryn
Female
Cancer
53
72
Thief
Throw
Earplug
Short Status
Jump+2

Blind Knife

Headgear
Mythril Vest
Wizard Mantle

Steal Armor
Bomb



Draaaan
Male
Virgo
62
76
Calculator
White Magic
MP Restore
Equip Gun
Teleport

Battle Folio

Thief Hat
Adaman Vest
Wizard Mantle

Height, 5
Cure 4, Protect, Shell 2, Wall, Esuna, Holy



HaateXIII
Male
Gemini
58
52
Mediator
White Magic
MA Save
Defense UP
Move+3

Blind Knife

Feather Hat
Mystic Vest
Defense Armlet

Invitation, Threaten, Preach, Insult, Mimic Daravon, Refute, Rehabilitate
Cure 3, Cure 4, Raise, Shell 2, Wall, Esuna

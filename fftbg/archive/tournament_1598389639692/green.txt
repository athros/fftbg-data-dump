Player: !Green
Team: Green Team
Palettes: Green/White



ArchKnightX
Male
Leo
54
79
Lancer
Elemental
PA Save
Doublehand
Retreat

Javelin

Diamond Helmet
Bronze Armor
Leather Mantle

Level Jump8, Vertical Jump5
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Absalom 20
Monster
Aquarius
65
56
Ultima Demon










Powergems
Male
Aries
59
61
Lancer
Battle Skill
Abandon
Sicken
Jump+1

Gungnir
Crystal Shield
Platinum Helmet
Light Robe
Battle Boots

Level Jump5, Vertical Jump8
Head Break, Speed Break, Stasis Sword, Dark Sword



Mushufasa
Female
Capricorn
73
42
Ninja
Elemental
Catch
Equip Sword
Move-MP Up

Cultist Dagger
Long Sword
Triangle Hat
Clothes
Germinas Boots

Shuriken, Staff, Stick
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind

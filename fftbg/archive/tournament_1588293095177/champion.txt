Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



HaplessOne
Male
Libra
56
70
Knight
Draw Out
Auto Potion
Concentrate
Move+3

Ancient Sword
Platinum Shield
Iron Helmet
Bronze Armor
Chantage

Armor Break, Shield Break, Speed Break, Mind Break
Bizen Boat



Nifboy
Male
Scorpio
57
66
Bard
Item
Abandon
Dual Wield
Move+3

Bloody Strings
Ramia Harp
Flash Hat
Power Sleeve
Chantage

Angel Song, Life Song, Space Storage, Sky Demon
Hi-Potion, Phoenix Down



FriendSquirrel
Female
Sagittarius
50
65
Thief
White Magic
Counter Tackle
Equip Axe
Jump+2

Battle Axe

Feather Hat
Earth Clothes
Sprint Shoes

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Cure 3, Raise, Raise 2, Esuna, Holy



Dorman 777
Male
Sagittarius
59
45
Ninja
Item
Speed Save
Maintenance
Teleport

Iga Knife
Short Edge
Black Hood
Leather Outfit
Defense Armlet

Bomb
Hi-Potion, Antidote, Eye Drop, Soft, Phoenix Down

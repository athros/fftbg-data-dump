Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



RunicMagus
Female
Leo
68
55
Mediator
Yin Yang Magic
Sunken State
Dual Wield
Move+3

Stone Gun
Romanda Gun
Headgear
Chain Vest
Dracula Mantle

Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Blind, Life Drain, Confusion Song, Dispel Magic, Sleep



Ewan E
Female
Leo
68
55
Mediator
Yin Yang Magic
Sunken State
Dual Wield
Move+3

Stone Gun
Romanda Gun
Headgear
Chain Vest
Dracula Mantle

Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Blind, Life Drain, Confusion Song, Dispel Magic, Sleep



TrueRhyme
Female
Leo
68
55
Mediator
Yin Yang Magic
Sunken State
Dual Wield
Move+3

Stone Gun
Romanda Gun
Headgear
Chain Vest
Dracula Mantle

Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Blind, Life Drain, Confusion Song, Dispel Magic, Sleep



Arlum
Female
Leo
68
55
Mediator
Yin Yang Magic
Sunken State
Dual Wield
Move+3

Stone Gun
Romanda Gun
Headgear
Chain Vest
Dracula Mantle

Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Blind, Life Drain, Confusion Song, Dispel Magic, Sleep

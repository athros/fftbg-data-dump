Player: !Green
Team: Green Team
Palettes: Green/White



Proper Noun
Female
Sagittarius
40
67
Mediator
Time Magic
Arrow Guard
Defend
Move+1

Blast Gun

Holy Miter
Earth Clothes
Leather Mantle

Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Quick



RyuTsuno
Male
Cancer
56
80
Lancer
Draw Out
MA Save
Equip Gun
Jump+3

Stone Gun
Round Shield
Cross Helmet
Black Robe
Rubber Shoes

Level Jump2, Vertical Jump7
Koutetsu



Yasmosis
Female
Gemini
55
53
Summoner
Steal
Arrow Guard
Short Charge
Ignore Height

Poison Rod

Golden Hairpin
Silk Robe
Elf Mantle

Ramuh, Ifrit, Titan, Odin, Fairy
Steal Helmet, Steal Accessory



Kronikle
Male
Gemini
47
56
Calculator
Black Magic
Speed Save
Equip Bow
Levitate

Mythril Bow

Twist Headband
Black Costume
N-Kai Armlet

CT, Height, 5
Fire 2, Bolt 2, Empower, Flare

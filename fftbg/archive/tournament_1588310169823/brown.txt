Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Twelfthrootoftwo
Monster
Scorpio
69
62
Goblin










XalSmellsBad
Female
Sagittarius
76
72
Wizard
Dance
Earplug
Dual Wield
Retreat

Wizard Rod
Ice Rod
Leather Hat
Wizard Robe
Reflect Ring

Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3, Empower
Witch Hunt, Void Storage



HaplessOne
Female
Scorpio
36
51
Samurai
Time Magic
Damage Split
Equip Armor
Move+3

Kikuichimoji

Cross Helmet
Mythril Vest
Leather Mantle

Asura, Bizen Boat, Heaven's Cloud
Haste, Haste 2, Slow, Slow 2, Float, Reflect, Demi, Demi 2, Stabilize Time



Edgehead62888
Male
Sagittarius
59
78
Mediator
Summon Magic
Auto Potion
Defend
Move+2

Stone Gun

Green Beret
Wizard Robe
Feather Boots

Persuade, Threaten, Solution, Death Sentence, Refute
Ramuh, Golem, Carbunkle, Salamander

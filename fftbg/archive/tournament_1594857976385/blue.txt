Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Aldrammech
Male
Serpentarius
52
75
Summoner
Yin Yang Magic
Parry
Defend
Levitate

Thunder Rod

Green Beret
Chameleon Robe
Rubber Shoes

Moogle, Carbunkle, Salamander, Silf, Lich
Poison, Spell Absorb, Life Drain, Pray Faith, Silence Song, Blind Rage, Foxbird, Confusion Song



ValtonZenola
Monster
Gemini
47
50
Cockatrice










Othatmattkid
Female
Sagittarius
80
66
Knight
Yin Yang Magic
HP Restore
Dual Wield
Fly

Ragnarok
Giant Axe
Crystal Helmet
Diamond Armor
Sprint Shoes

Shield Break, Speed Break, Power Break
Blind, Silence Song, Foxbird, Dispel Magic, Paralyze



RyanTheRebs
Female
Scorpio
56
43
Knight
Talk Skill
Counter Magic
Monster Talk
Levitate

Defender
Gold Shield
Mythril Helmet
Reflect Mail
Spike Shoes

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword, Explosion Sword
Threaten, Preach, Solution, Insult, Mimic Daravon, Refute

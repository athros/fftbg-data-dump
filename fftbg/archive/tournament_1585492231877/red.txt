Player: !Red
Team: Red Team
Palettes: Red/Brown



Glory Seeker
Male
Leo
58
66
Wizard
Steal
Distribute
Equip Armor
Teleport

Wizard Rod

Mythril Helmet
Genji Armor
Genji Gauntlet

Fire, Fire 2, Fire 4, Bolt 4, Ice 2, Empower, Death
Gil Taking, Steal Shield, Steal Weapon, Leg Aim



WinnerBit
Female
Serpentarius
54
47
Summoner
Throw
Brave Up
Equip Armor
Ignore Height

Wizard Staff

Feather Hat
Mystic Vest
Magic Gauntlet

Moogle, Shiva, Ifrit, Golem, Carbunkle, Odin, Silf, Lich
Shuriken, Knife, Spear



Jhazor
Female
Pisces
45
58
Thief
Basic Skill
Faith Up
Attack UP
Swim

Ancient Sword

Flash Hat
Clothes
Sprint Shoes

Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Leg Aim
Scream



0utlier
Male
Libra
70
57
Thief
Jump
Critical Quick
Halve MP
Waterbreathing

Main Gauche

Black Hood
Earth Clothes
Angel Ring

Steal Heart
Level Jump5, Vertical Jump2

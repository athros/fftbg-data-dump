Player: !zChamp
Team: Champion Team
Palettes: Green/White



Leakimiko
Female
Aries
51
66
Summoner
White Magic
Auto Potion
Short Charge
Ignore Terrain

Dragon Rod

Headgear
Light Robe
Genji Gauntlet

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Bahamut, Salamander, Fairy, Lich
Cure 2, Raise, Reraise, Regen, Wall, Esuna



Zachara
Male
Cancer
47
48
Chemist
Jump
Faith Up
Doublehand
Ignore Terrain

Hydra Bag

Black Hood
Black Costume
Sprint Shoes

Potion, Hi-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down
Level Jump4, Vertical Jump5



Dynasti
Male
Sagittarius
44
49
Time Mage
Elemental
Catch
Secret Hunt
Move+3

Oak Staff

Headgear
Linen Robe
Wizard Mantle

Haste, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Nachtkrieger
Male
Virgo
59
80
Archer
Item
Arrow Guard
Doublehand
Move-HP Up

Ice Bow

Feather Hat
Power Sleeve
Genji Gauntlet

Charge+4, Charge+7, Charge+20
Potion, Hi-Potion, Hi-Ether, Antidote, Remedy, Phoenix Down

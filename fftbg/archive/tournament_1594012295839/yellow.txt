Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Moonliquor
Female
Libra
51
47
Mime

Counter
Secret Hunt
Jump+2



Feather Hat
Mystic Vest
Defense Armlet

Mimic




Omegasuspekt
Male
Pisces
46
52
Archer
Throw
Catch
Long Status
Retreat

Blaze Gun
Platinum Shield
Bronze Helmet
Chain Vest
Vanish Mantle

Charge+3, Charge+4, Charge+5, Charge+7
Shuriken, Bomb



SSwing
Female
Libra
55
45
Samurai
Summon Magic
Faith Save
Equip Shield
Move+2

Asura Knife
Hero Shield
Circlet
Diamond Armor
Battle Boots

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Moogle, Shiva, Ramuh, Leviathan, Salamander, Fairy, Cyclops



Grandlanzer
Female
Aquarius
55
67
Thief
Summon Magic
Counter Magic
Secret Hunt
Retreat

Mythril Knife

Leather Hat
Secret Clothes
Battle Boots

Gil Taking, Steal Helmet, Leg Aim
Titan, Zodiac

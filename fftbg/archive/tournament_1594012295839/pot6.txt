Final Bets: black - 8 bets for 7,191G (59.7%, x0.68); purple - 11 bets for 4,861G (40.3%, x1.48)

black bets:
evdoggity: 5,000G (69.5%, 108,100G)
cougboi: 500G (7.0%, 17,221G)
superdevon1: 384G (5.3%, 384G)
hiros13gts: 355G (4.9%, 355G)
JethroThrul: 336G (4.7%, 336G)
twelfthrootoftwo: 300G (4.2%, 8,866G)
ThreeMileIsland: 216G (3.0%, 216G)
moocaotao: 100G (1.4%, 3,938G)

purple bets:
ValtonZenola: 1,000G (20.6%, 19,784G)
BirbBrainsBot: 1,000G (20.6%, 70,693G)
genericco: 748G (15.4%, 748G)
electric_algus: 500G (10.3%, 40,908G)
ColetteMSLP: 400G (8.2%, 7,187G)
phrossi_v: 360G (7.4%, 4,958G)
getthemoneyz: 242G (5.0%, 1,199,609G)
Evewho: 200G (4.1%, 21,941G)
datadrivenbot: 200G (4.1%, 55,074G)
OrganicArtist: 111G (2.3%, 2,310G)
Ring_Wyrm: 100G (2.1%, 3,338G)

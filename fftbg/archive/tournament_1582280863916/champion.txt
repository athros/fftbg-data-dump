Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Clockthief
Male
Taurus
52
74
Knight
Draw Out
Counter Magic
Equip Knife
Move-HP Up

Kunai
Crystal Shield
Barbuta
Linen Cuirass
Leather Mantle

Power Break, Mind Break, Stasis Sword
Asura, Murasame, Muramasa, Kikuichimoji, Masamune



Eldente
Female
Pisces
72
51
Lancer
Summon Magic
Parry
Magic Attack UP
Teleport

Obelisk
Mythril Shield
Genji Helmet
Mythril Armor
Angel Ring

Level Jump8, Vertical Jump8
Moogle, Ramuh, Bahamut, Odin, Salamander, Fairy, Lich



Neoshono
Monster
Cancer
43
48
Red Chocobo










UmaiJam
Female
Capricorn
61
54
Ninja
Item
Mana Shield
Equip Gun
Move-MP Up

Stone Gun
Mythril Gun
Triangle Hat
Black Costume
Feather Mantle

Bomb
X-Potion, Antidote, Echo Grass, Remedy, Phoenix Down

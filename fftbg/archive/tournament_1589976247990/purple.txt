Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Metagameface
Female
Virgo
71
37
Mediator
Time Magic
Arrow Guard
Sicken
Move+1

Blaze Gun

Black Hood
Mythril Vest
Magic Gauntlet

Invitation, Praise, Preach, Insult, Negotiate, Mimic Daravon
Immobilize, Reflect, Stabilize Time



Error72
Male
Aries
63
65
Lancer
Battle Skill
Abandon
Martial Arts
Move+2


Bronze Shield
Iron Helmet
Bronze Armor
Angel Ring

Level Jump8, Vertical Jump8
Armor Break, Weapon Break, Speed Break, Mind Break, Justice Sword



GrandmasterFrankerZ
Female
Pisces
56
75
Chemist
White Magic
Arrow Guard
Equip Sword
Move+2

Nagrarock

Flash Hat
Leather Outfit
Bracer

Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Cure, Cure 2, Raise, Protect, Shell, Shell 2



Ar Tactic
Male
Serpentarius
58
48
Thief
Black Magic
Catch
Defense UP
Teleport

Assassin Dagger

Golden Hairpin
Adaman Vest
Reflect Ring

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Fire 2, Bolt 3, Bolt 4, Death, Flare

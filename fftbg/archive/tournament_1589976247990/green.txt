Player: !Green
Team: Green Team
Palettes: Green/White



PLifer
Female
Leo
77
47
Geomancer
Time Magic
Arrow Guard
Equip Gun
Fly

Battle Folio
Buckler
Feather Hat
Chameleon Robe
Small Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Immobilize, Float, Quick, Demi 2, Stabilize Time



LeoNightFury
Male
Leo
57
78
Lancer
Black Magic
PA Save
Magic Defense UP
Move+3

Cypress Rod
Buckler
Platinum Helmet
Platinum Armor
Jade Armlet

Level Jump5, Vertical Jump6
Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Ice, Ice 2



Boultson1337
Female
Leo
69
45
Oracle
Elemental
Critical Quick
Martial Arts
Move+1



Triangle Hat
Wizard Robe
Germinas Boots

Spell Absorb, Life Drain, Pray Faith, Blind Rage, Sleep, Petrify
Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



ALY327
Female
Scorpio
39
47
Time Mage
Steal
Counter
Dual Wield
Move+1

White Staff
Oak Staff
Golden Hairpin
Linen Robe
Jade Armlet

Haste, Haste 2, Slow 2, Stop, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Steal Heart, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim

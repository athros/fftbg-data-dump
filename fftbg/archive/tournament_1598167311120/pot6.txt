Final Bets: white - 8 bets for 6,079G (64.5%, x0.55); brown - 7 bets for 3,349G (35.5%, x1.82)

white bets:
NicoSavoy: 2,687G (44.2%, 5,375G)
Seaweed_B: 1,000G (16.5%, 48,959G)
corysteam: 1,000G (16.5%, 5,793G)
killth3kid: 792G (13.0%, 51,965G)
ar_tactic: 200G (3.3%, 81,333G)
datadrivenbot: 200G (3.3%, 64,476G)
AllInBot: 100G (1.6%, 100G)
Wooplestein: 100G (1.6%, 4,624G)

brown bets:
Sairentozon7: 1,000G (29.9%, 5,019G)
KasugaiRoastedPeas: 737G (22.0%, 737G)
BirbBrainsBot: 682G (20.4%, 154,640G)
resjudicata3: 384G (11.5%, 384G)
ProteinBiscuit: 200G (6.0%, 5,834G)
artysayres: 200G (6.0%, 5,775G)
getthemoneyz: 146G (4.4%, 1,726,990G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Miku Shikhu
Female
Sagittarius
80
71
Mediator
Throw
Earplug
Sicken
Retreat

Romanda Gun

Thief Hat
Wizard Robe
Magic Ring

Invitation, Praise, Preach, Death Sentence, Insult, Negotiate
Bomb, Dictionary



Killth3kid
Male
Virgo
80
74
Bard
Charge
Dragon Spirit
Doublehand
Ignore Terrain

Bloody Strings

Triangle Hat
Mythril Armor
Magic Gauntlet

Angel Song, Cheer Song, Battle Song, Magic Song, Space Storage
Charge+3, Charge+5



Gorgewall
Male
Gemini
72
54
Monk
Summon Magic
PA Save
Halve MP
Jump+3



Flash Hat
Black Costume
Small Mantle

Spin Fist, Purification, Chakra, Revive
Moogle, Shiva, Ramuh, Ifrit, Carbunkle



Chuckolator
Female
Sagittarius
51
65
Lancer
Black Magic
Counter
Magic Attack UP
Jump+3

Holy Lance
Mythril Shield
Iron Helmet
White Robe
N-Kai Armlet

Level Jump5, Vertical Jump8
Fire 3, Fire 4, Bolt 2, Ice 2, Ice 4

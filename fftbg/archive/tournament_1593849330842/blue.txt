Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



KyleWonToLiveForever
Male
Libra
49
65
Time Mage
Basic Skill
Counter
Equip Shield
Jump+1

Cypress Rod
Flame Shield
Flash Hat
Mythril Vest
Feather Boots

Haste, Slow 2, Stop, Immobilize, Stabilize Time
Accumulate, Dash, Fury, Scream



Hasterious
Female
Aquarius
47
62
Geomancer
Black Magic
MP Restore
Equip Polearm
Levitate

Mythril Spear
Aegis Shield
Black Hood
Linen Robe
108 Gems

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Fire 2, Fire 4, Bolt 2, Bolt 3, Ice, Ice 3, Empower, Death



IamEradicate
Female
Pisces
70
42
Monk
Basic Skill
Brave Save
Equip Polearm
Levitate

Javelin

Green Beret
Wizard Outfit
Dracula Mantle

Purification, Chakra, Revive
Heal



Twelfthrootoftwo
Male
Aquarius
50
65
Knight
White Magic
Mana Shield
Doublehand
Lava Walking

Long Sword

Circlet
Chain Mail
Defense Ring

Head Break, Power Break, Mind Break, Justice Sword, Dark Sword
Raise, Reraise, Protect, Protect 2, Esuna, Holy

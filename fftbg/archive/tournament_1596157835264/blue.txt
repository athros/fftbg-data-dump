Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HuffFlex
Male
Serpentarius
71
75
Knight
Time Magic
Critical Quick
Equip Knife
Jump+3

Iga Knife
Hero Shield
Crystal Helmet
Wizard Robe
Small Mantle

Armor Break, Shield Break, Speed Break, Surging Sword
Haste, Haste 2, Slow 2, Immobilize, Demi, Stabilize Time, Meteor



FriendSquirrel
Male
Libra
54
49
Calculator
Demon Skill
Dragon Spirit
Equip Armor
Levitate

Octagon Rod
Flame Shield
Leather Helmet
Adaman Vest
108 Gems

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Shalloween
Female
Cancer
59
56
Calculator
Time Magic
Auto Potion
Equip Polearm
Move-HP Up

Partisan

Triangle Hat
Chameleon Robe
Jade Armlet

CT, Height, Prime Number, 5, 4, 3
Haste 2, Float, Quick, Demi 2, Meteor



Slotheye17
Male
Gemini
61
80
Thief
Jump
Sunken State
Equip Bow
Swim

Night Killer

Flash Hat
Wizard Outfit
Feather Boots

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Leg Aim
Level Jump3, Vertical Jump8

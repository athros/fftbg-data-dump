Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Miku Shikhu
Male
Scorpio
40
40
Squire
Steal
Distribute
Dual Wield
Lava Walking

Poison Bow
Night Killer
Gold Helmet
Leather Armor
Small Mantle

Accumulate, Heal, Tickle, Yell, Scream
Gil Taking, Steal Helmet, Steal Shield



Grininda
Male
Serpentarius
48
71
Ninja
Jump
Distribute
Sicken
Jump+3

Mythril Knife
Short Edge
Red Hood
Black Costume
Red Shoes

Shuriken, Staff, Ninja Sword
Level Jump4, Vertical Jump5



Forkmore
Female
Capricorn
66
71
Thief
Yin Yang Magic
Counter
Equip Bow
Jump+3

Ultimus Bow

Twist Headband
Secret Clothes
Diamond Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Status, Arm Aim
Poison, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep



Baconbacon1207
Female
Gemini
65
52
Monk
Item
MA Save
Throw Item
Lava Walking



Green Beret
Power Sleeve
Genji Gauntlet

Chakra, Revive
Potion, Hi-Potion, X-Potion, Ether, Soft, Remedy

Player: !Green
Team: Green Team
Palettes: Green/White



GrandmasterFrankerZ
Male
Leo
47
68
Calculator
Dungeon Skill
Distribute
Equip Bow
Move+3

Silver Bow

Mythril Helmet
Wizard Robe
Salty Rage

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



VolgraTheMoose
Male
Libra
39
48
Geomancer
Black Magic
Mana Shield
Equip Knife
Waterbreathing

Blind Knife
Ice Shield
Golden Hairpin
White Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball
Fire 3, Bolt 2, Ice 2, Ice 3



LordTomS
Male
Leo
56
64
Bard
Jump
Auto Potion
Equip Shield
Move+2

Windslash Bow
Platinum Shield
Green Beret
Adaman Vest
Elf Mantle

Angel Song, Battle Song
Level Jump5, Vertical Jump7



Douchetron
Male
Gemini
45
54
Mime

Brave Save
Long Status
Waterwalking



Black Hood
Secret Clothes
Diamond Armlet

Mimic


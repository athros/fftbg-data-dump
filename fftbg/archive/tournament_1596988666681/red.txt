Player: !Red
Team: Red Team
Palettes: Red/Brown



Ruleof5
Female
Virgo
76
70
Archer
Yin Yang Magic
Abandon
Martial Arts
Waterbreathing


Escutcheon
Circlet
Black Costume
Bracer

Charge+1, Charge+2, Charge+3, Charge+10
Life Drain, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep



Kronikle
Female
Cancer
40
60
Calculator
Black Magic
Parry
Magic Attack UP
Ignore Height

Papyrus Codex

Holy Miter
White Robe
Defense Armlet

CT, Height, 5, 3
Fire 2, Fire 3, Fire 4, Bolt 3, Ice, Ice 3, Ice 4, Flare



Tarheels218
Female
Scorpio
70
63
Oracle
Elemental
MA Save
Magic Defense UP
Waterbreathing

Gokuu Rod

Golden Hairpin
Earth Clothes
Spike Shoes

Doubt Faith, Silence Song, Paralyze, Petrify
Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



Aldrammech
Female
Leo
43
70
Summoner
Battle Skill
Caution
Maintenance
Levitate

Dragon Rod

Flash Hat
Leather Outfit
Feather Mantle

Moogle, Ifrit, Golem, Odin, Salamander, Fairy, Lich
Armor Break, Magic Break, Speed Break, Justice Sword

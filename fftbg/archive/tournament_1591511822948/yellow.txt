Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



UmaiJam
Female
Virgo
79
69
Archer
White Magic
Counter Magic
Magic Defense UP
Retreat

Poison Bow
Aegis Shield
Barette
Earth Clothes
Power Wrist

Charge+1, Charge+4
Cure, Raise, Protect 2, Shell, Shell 2, Holy



Benticore
Male
Capricorn
50
76
Summoner
Throw
Catch
Equip Axe
Move+3

Mace of Zeus

Headgear
Light Robe
Battle Boots

Shiva, Ramuh, Ifrit, Carbunkle, Leviathan, Fairy
Knife, Wand, Dictionary



Thyrandaal
Male
Aries
43
42
Chemist
Battle Skill
Mana Shield
Equip Polearm
Jump+2

Cypress Rod

Twist Headband
Wizard Outfit
Sprint Shoes

Potion, Hi-Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break



CT 5 Holy
Female
Virgo
64
74
Dancer
White Magic
Parry
Equip Bow
Ignore Terrain

Silver Bow

Red Hood
Wizard Robe
Jade Armlet

Witch Hunt, Obsidian Blade
Cure, Cure 2, Raise, Raise 2, Protect, Esuna

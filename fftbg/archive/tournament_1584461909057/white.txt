Player: !White
Team: White Team
Palettes: White/Blue



Bad1dea
Female
Taurus
72
65
Oracle
Talk Skill
Speed Save
Maintenance
Move-HP Up

Battle Folio

Black Hood
Earth Clothes
Defense Armlet

Blind, Poison, Spell Absorb, Foxbird, Dispel Magic, Paralyze, Dark Holy
Persuade, Praise, Solution



AniZero
Male
Capricorn
74
46
Lancer
Steal
Hamedo
Doublehand
Move+2

Partisan

Crystal Helmet
Black Robe
108 Gems

Level Jump2, Vertical Jump3
Gil Taking, Steal Shield



Jadistk
Male
Aries
41
45
Thief
Battle Skill
Parry
Doublehand
Move-MP Up

Mage Masher

Black Hood
Adaman Vest
Wizard Mantle

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Stasis Sword, Dark Sword



Winterharte
Male
Aries
58
52
Bard
White Magic
Damage Split
Short Status
Teleport

Fairy Harp

Triangle Hat
Wizard Outfit
Defense Armlet

Life Song, Cheer Song, Space Storage, Sky Demon
Cure 2, Raise, Reraise, Protect, Shell, Shell 2, Esuna

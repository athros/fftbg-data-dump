Final Bets: white - 8 bets for 4,747G (52.9%, x0.89); black - 10 bets for 4,228G (47.1%, x1.12)

white bets:
nhammen: 2,000G (42.1%, 122,666G)
latebit: 1,000G (21.1%, 7,861G)
JumbocactuarX27: 664G (14.0%, 664G)
ShintaroNayaka: 440G (9.3%, 440G)
JonnyCue: 320G (6.7%, 320G)
datadrivenbot: 200G (4.2%, 63,944G)
Kellios11: 100G (2.1%, 1,556G)
BartTradingCompany: 23G (0.5%, 613G)

black bets:
BirbBrainsBot: 1,000G (23.7%, 60,599G)
Lord_Burrah: 1,000G (23.7%, 81,000G)
daveb_: 666G (15.8%, 11,955G)
getthemoneyz: 386G (9.1%, 1,761,689G)
AllInBot: 289G (6.8%, 289G)
Moshyhero: 250G (5.9%, 2,230G)
MemoriesofFinal: 236G (5.6%, 236G)
CosmicTactician: 200G (4.7%, 39,362G)
gorgewall: 101G (2.4%, 10,761G)
ko2q: 100G (2.4%, 5,564G)

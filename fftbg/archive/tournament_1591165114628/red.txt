Player: !Red
Team: Red Team
Palettes: Red/Brown



ALY327
Male
Virgo
53
47
Monk
Summon Magic
MP Restore
Equip Polearm
Waterbreathing

Spear

Black Hood
Mythril Vest
Jade Armlet

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Moogle, Ramuh, Ifrit, Golem, Bahamut



Phytik
Male
Taurus
79
80
Wizard
Item
Critical Quick
Dual Wield
Lava Walking

Mythril Knife
Main Gauche
Cachusha
Linen Robe
Power Wrist

Fire, Fire 3, Bolt, Bolt 2, Ice 3, Frog, Flare
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Kronikle
Female
Pisces
62
66
Ninja
White Magic
Counter Magic
Equip Gun
Jump+1

Bestiary
Bestiary
Green Beret
Black Costume
Power Wrist

Shuriken, Bomb, Knife
Cure, Cure 3, Cure 4, Raise, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna



NovaKnight21
Male
Cancer
73
58
Priest
Sing
Counter
Defend
Levitate

Flame Whip

Headgear
White Robe
Jade Armlet

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Protect, Shell, Shell 2, Wall, Esuna
Life Song, Battle Song

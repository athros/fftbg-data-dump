Player: !White
Team: White Team
Palettes: White/Blue



CorpusCav
Female
Aries
65
47
Summoner
Time Magic
Parry
Dual Wield
Ignore Height

Rod
Ice Rod
Black Hood
Judo Outfit
Diamond Armlet

Ramuh, Titan, Golem, Carbunkle, Leviathan, Salamander, Fairy, Lich, Cyclops
Slow 2, Stop, Immobilize, Stabilize Time



Gorgewall
Male
Cancer
50
59
Lancer
Steal
Blade Grasp
Defense UP
Waterwalking

Holy Lance
Ice Shield
Cross Helmet
Genji Armor
Rubber Shoes

Level Jump4, Vertical Jump3
Steal Helmet, Steal Armor, Steal Shield, Steal Status, Leg Aim



OneHundredFists
Female
Scorpio
46
43
Time Mage
Dance
Absorb Used MP
Equip Knife
Teleport

Mage Masher

Flash Hat
Judo Outfit
Elf Mantle

Slow 2, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time
Nameless Dance, Obsidian Blade, Nether Demon



YaBoy125
Female
Gemini
70
73
Ninja
Steal
Hamedo
Equip Gun
Waterbreathing

Bestiary
Battle Folio
Golden Hairpin
Adaman Vest
Sprint Shoes

Shuriken, Staff
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Leg Aim

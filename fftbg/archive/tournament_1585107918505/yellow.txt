Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Victoriolue
Male
Taurus
63
68
Squire
Black Magic
Catch
Dual Wield
Jump+1

Giant Axe
Giant Axe
Gold Helmet
Judo Outfit
Angel Ring

Throw Stone, Heal, Tickle, Cheer Up, Fury, Scream
Fire, Fire 3, Fire 4, Ice 3, Ice 4



Lionhermit
Female
Sagittarius
47
70
Samurai
Elemental
Brave Up
Equip Gun
Ignore Height

Romanda Gun

Barbuta
White Robe
Leather Mantle

Asura, Heaven's Cloud, Muramasa, Kikuichimoji
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Silentperogy
Female
Libra
45
40
Chemist
Throw
Absorb Used MP
Dual Wield
Move-HP Up

Hydra Bag
Air Knife
Black Hood
Power Sleeve
Diamond Armlet

Potion, Hi-Potion, Eye Drop, Echo Grass, Soft, Phoenix Down
Shuriken, Stick, Wand, Dictionary



RunicMagus
Female
Aquarius
42
80
Calculator
White Magic
Speed Save
Dual Wield
Levitate

Cypress Rod
Gokuu Rod
Green Beret
Power Sleeve
Rubber Shoes

CT, Prime Number, 4, 3
Cure 4, Raise, Protect, Protect 2, Shell, Esuna

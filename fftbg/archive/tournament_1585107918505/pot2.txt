Final Bets: green - 36 bets for 23,518G (59.9%, x0.67); yellow - 34 bets for 15,738G (40.1%, x1.49)

green bets:
Lydian_C: 4,240G (18.0%, 28,757G)
RurouniGeo: 3,000G (12.8%, 5,524G)
MantisFinch: 1,922G (8.2%, 1,922G)
SolarisFall: 1,329G (5.7%, 1,329G)
Lodrak: 1,316G (5.6%, 2,632G)
Shalloween: 1,000G (4.3%, 34,096G)
Lazerkick: 1,000G (4.3%, 9,931G)
Vulasuw: 1,000G (4.3%, 1,000G)
RyuTsuno: 876G (3.7%, 876G)
leakimiko: 759G (3.2%, 15,187G)
red__lancer: 600G (2.6%, 11,232G)
Zeroroute: 585G (2.5%, 585G)
Mr_G00SE: 579G (2.5%, 579G)
HaateXIII: 555G (2.4%, 5,779G)
Tougou: 500G (2.1%, 5,559G)
Oshimi12: 500G (2.1%, 2,404G)
NovaKnight21: 396G (1.7%, 396G)
SpaZmodeus: 333G (1.4%, 1,795G)
Humble_Fabio: 333G (1.4%, 940G)
fenaen: 300G (1.3%, 2,820G)
galkife: 250G (1.1%, 50,871G)
HighJayster: 224G (1.0%, 224G)
AllInBot: 200G (0.9%, 200G)
evdoggity: 200G (0.9%, 236G)
KasugaiRoastedPeas: 200G (0.9%, 29,121G)
ra1kia: 200G (0.9%, 1,073G)
Kyton786: 193G (0.8%, 386G)
ungabunga_bot: 128G (0.5%, 1,636G)
ApplesauceBoss: 100G (0.4%, 10,110G)
ANFz: 100G (0.4%, 23,334G)
anthrax85: 100G (0.4%, 100G)
Maeveen: 100G (0.4%, 5,883G)
denamda: 100G (0.4%, 18,127G)
datadrivenbot: 100G (0.4%, 14,070G)
chronoxtrigger: 100G (0.4%, 2,750G)
deesignal: 100G (0.4%, 3,709G)

yellow bets:
helpimabug: 1,565G (9.9%, 1,565G)
KupoKel: 1,500G (9.5%, 11,886G)
BirbBrainsBot: 1,000G (6.4%, 192,004G)
OmnibotGamma: 1,000G (6.4%, 11,471G)
bad1dea: 1,000G (6.4%, 181,128G)
SeedSC: 1,000G (6.4%, 80,211G)
jethrothrul: 921G (5.9%, 921G)
spit8866: 919G (5.8%, 919G)
sinnyil2: 800G (5.1%, 14,984G)
Lali_Lulelo: 524G (3.3%, 524G)
typicalfanboy: 500G (3.2%, 500G)
JustSuperish: 500G (3.2%, 3,894G)
gooseyourself: 500G (3.2%, 7,503G)
Davarian: 401G (2.5%, 401G)
mirapoix: 400G (2.5%, 400G)
RunicMagus: 400G (2.5%, 18,305G)
getthemoneyz: 324G (2.1%, 355,338G)
nomoment: 300G (1.9%, 300G)
ZephyrTempest: 246G (1.6%, 4,037G)
TrogdorTheMemeinator: 220G (1.4%, 220G)
ugoplatamia: 200G (1.3%, 7,295G)
ricky_ortease: 200G (1.3%, 204G)
rubenflonne: 200G (1.3%, 1,376G)
Billybones5150: 200G (1.3%, 2,457G)
sect_cor: 150G (1.0%, 21,033G)
most_shameful: 108G (0.7%, 108G)
Basmal: 100G (0.6%, 4,520G)
Bahamut64: 100G (0.6%, 7,510G)
victoriolue: 100G (0.6%, 2,579G)
silentperogy: 100G (0.6%, 6,855G)
CorpusCav: 100G (0.6%, 3,455G)
BenYuPoker: 100G (0.6%, 1,766G)
WinnerBit: 30G (0.2%, 550G)
andycyca: 30G (0.2%, 627G)

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Coralreeferz
Male
Taurus
42
47
Mediator
Sing
Counter Magic
Dual Wield
Fly

Stone Gun
Blast Gun
Holy Miter
Leather Outfit
Magic Ring

Persuade, Threaten, Mimic Daravon, Rehabilitate
Angel Song, Cheer Song, Last Song, Space Storage, Hydra Pit



ApplesauceBoss
Male
Sagittarius
69
76
Lancer
Black Magic
MA Save
Maintenance
Swim

Gungnir
Crystal Shield
Mythril Helmet
Genji Armor
Bracer

Level Jump8, Vertical Jump7
Fire 3, Bolt, Bolt 2, Bolt 4, Ice



LivingHitokiri
Female
Aquarius
66
42
Knight
Punch Art
Abandon
Beastmaster
Jump+1

Ragnarok
Buckler
Mythril Helmet
Chameleon Robe
Chantage

Weapon Break, Magic Break, Stasis Sword
Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive



Eudes89
Female
Scorpio
53
74
Time Mage
Summon Magic
Meatbone Slash
Short Charge
Waterwalking

Wizard Staff

Flash Hat
Brigandine
Leather Mantle

Haste, Haste 2, Slow, Quick, Stabilize Time, Meteor
Shiva, Ramuh, Ifrit, Golem, Leviathan, Salamander, Fairy

Player: !White
Team: White Team
Palettes: White/Blue



CrownOfHorns
Female
Gemini
73
67
Archer
Time Magic
Absorb Used MP
Short Charge
Fly

Gastrafitis
Mythril Shield
Green Beret
Brigandine
Feather Boots

Charge+1, Charge+2, Charge+7
Haste 2, Slow 2, Stabilize Time



AmaninAmide
Female
Taurus
46
78
Dancer
Elemental
Absorb Used MP
Short Charge
Move+3

Cashmere

Black Hood
White Robe
Sprint Shoes

Slow Dance, Polka Polka, Nameless Dance, Obsidian Blade
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Dymntd
Male
Aries
68
52
Chemist
Black Magic
Speed Save
Beastmaster
Jump+3

Assassin Dagger

Triangle Hat
Clothes
Germinas Boots

Potion, X-Potion, Ether, Echo Grass, Phoenix Down
Fire, Bolt, Bolt 2, Ice 3, Death, Flare



Xcessive30
Female
Cancer
66
78
Calculator
Time Magic
Parry
Equip Polearm
Retreat

Ivory Rod

Feather Hat
Brigandine
Battle Boots

CT, Height, 5, 4
Haste, Slow, Stop, Immobilize, Float, Demi 2

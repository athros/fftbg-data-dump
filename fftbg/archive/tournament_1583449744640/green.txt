Player: !Green
Team: Green Team
Palettes: Green/White



Pedrossj
Female
Libra
52
49
Geomancer
Throw
Counter
Equip Axe
Move-MP Up

Healing Staff
Flame Shield
Golden Hairpin
Wizard Robe
Rubber Shoes

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Shuriken, Ninja Sword



Nizaha
Female
Leo
44
58
Thief
Item
Sunken State
Maintenance
Waterwalking

Coral Sword

Red Hood
Wizard Outfit
108 Gems

Gil Taking, Steal Status
X-Potion, Hi-Ether, Eye Drop, Echo Grass, Soft



Lydian C
Female
Capricorn
68
70
Wizard
Draw Out
Damage Split
Short Charge
Fly

Rod

Feather Hat
Wizard Outfit
Diamond Armlet

Fire 2, Fire 4, Bolt, Ice 2, Ice 3, Ice 4, Flare
Koutetsu, Muramasa



Sonata Of Senpai
Male
Serpentarius
64
72
Archer
Yin Yang Magic
Counter Magic
Defend
Lava Walking

Gastrafitis
Escutcheon
Iron Helmet
Rubber Costume
Red Shoes

Charge+2, Charge+4, Charge+10
Pray Faith, Doubt Faith, Foxbird

Player: !White
Team: White Team
Palettes: White/Blue



TarosB4
Male
Taurus
72
78
Bard
Battle Skill
Absorb Used MP
Maintenance
Retreat

Bloody Strings

Leather Hat
Mythril Armor
Small Mantle

Angel Song, Diamond Blade, Hydra Pit
Magic Break, Stasis Sword



KitchTowel
Female
Sagittarius
80
58
Priest
Math Skill
HP Restore
Sicken
Move-MP Up

Healing Staff

Twist Headband
Black Robe
Germinas Boots

Cure 3, Raise, Raise 2, Protect, Protect 2, Shell 2, Wall, Esuna
CT, Prime Number, 5, 4



Kohlingen
Male
Sagittarius
78
54
Bard
Steal
Caution
Beastmaster
Teleport

Silver Bow

Headgear
Clothes
Leather Mantle

Angel Song, Hydra Pit
Steal Armor



MyFakeLife
Female
Sagittarius
69
49
Monk
Steal
PA Save
Defend
Waterbreathing

Star Bag

Black Hood
Adaman Vest
Defense Ring

Spin Fist, Secret Fist, Purification, Seal Evil
Gil Taking, Steal Shield, Steal Status, Arm Aim

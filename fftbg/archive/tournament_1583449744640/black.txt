Player: !Black
Team: Black Team
Palettes: Black/Red



Hales Bopp It
Male
Serpentarius
62
77
Lancer
Yin Yang Magic
Regenerator
Concentrate
Ignore Height

Octagon Rod
Round Shield
Barbuta
Light Robe
Sprint Shoes

Level Jump8, Vertical Jump8
Life Drain, Pray Faith, Zombie, Foxbird, Dispel Magic, Dark Holy



HippieDota
Female
Sagittarius
56
59
Summoner
Time Magic
Counter Flood
Magic Attack UP
Move+3

Wizard Rod

Headgear
Wizard Robe
Feather Mantle

Moogle, Shiva, Ifrit, Carbunkle
Haste, Stop, Immobilize, Reflect, Stabilize Time



JustDoomathon
Male
Aries
37
79
Calculator
Undeath Skill
Abandon
Magic Attack UP
Waterwalking

Ice Rod
Genji Shield
Mythril Helmet
Light Robe
Small Mantle

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Winterharte
Female
Pisces
47
77
Archer
Item
Catch
Dual Wield
Levitate

Bow Gun
Bow Gun
Bronze Helmet
Judo Outfit
Defense Ring

Charge+2, Charge+3, Charge+4, Charge+20
Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Holy Water, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



VolgraTheMoose
Male
Cancer
51
42
Lancer
Sing
Earplug
Attack UP
Levitate

Dragon Whisker
Gold Shield
Bronze Helmet
Plate Mail
Defense Ring

Level Jump5, Vertical Jump7
Life Song, Cheer Song, Magic Song, Last Song, Space Storage



WhiteTigress
Male
Scorpio
67
80
Archer
Item
MA Save
Doublehand
Teleport

Night Killer

Holy Miter
Adaman Vest
Small Mantle

Charge+3
Potion, Hi-Potion, X-Potion, Soft



Reddwind
Male
Pisces
67
60
Knight
Sing
Parry
Defense UP
Retreat

Excalibur
Crystal Shield
Diamond Helmet
Bronze Armor
Setiemson

Head Break, Magic Break, Speed Break
Angel Song, Life Song, Cheer Song



Tiffani
Female
Cancer
49
77
Oracle
Time Magic
Sunken State
Short Charge
Jump+2

Papyrus Codex

Red Hood
Chameleon Robe
108 Gems

Spell Absorb, Doubt Faith, Confusion Song, Dispel Magic, Sleep
Immobilize, Float, Demi 2

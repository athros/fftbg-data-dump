Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaplessOne
Female
Gemini
60
80
Oracle
Punch Art
Auto Potion
Dual Wield
Ignore Height

Gokuu Rod
Battle Bamboo
Flash Hat
Wizard Outfit
Bracer

Blind, Poison, Foxbird
Pummel, Wave Fist, Earth Slash, Purification



GrayGhostGaming
Female
Sagittarius
45
77
Squire
White Magic
Counter Flood
Doublehand
Move+2

Hydra Bag

Thief Hat
Earth Clothes
Defense Ring

Accumulate, Heal, Yell, Cheer Up, Scream
Cure 3, Raise, Raise 2, Protect, Esuna



Boojob
Male
Virgo
46
66
Knight
Yin Yang Magic
MA Save
Equip Sword
Move-HP Up

Kikuichimoji
Flame Shield
Circlet
Chameleon Robe
Rubber Shoes

Shield Break, Magic Break, Speed Break, Justice Sword, Explosion Sword
Spell Absorb, Life Drain, Silence Song, Confusion Song, Paralyze, Dark Holy



Rockmem21
Female
Gemini
71
68
Geomancer
Draw Out
MA Save
Martial Arts
Waterwalking


Genji Shield
Triangle Hat
White Robe
Cursed Ring

Pitfall, Hallowed Ground, Will-O-Wisp, Blizzard, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud

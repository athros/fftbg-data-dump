Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lord Burrah
Monster
Sagittarius
52
75
Reaper










Amadmet
Male
Serpentarius
48
70
Archer
Steal
Absorb Used MP
Long Status
Jump+1

Hunting Bow
Platinum Shield
Headgear
Adaman Vest
Battle Boots

Charge+2, Charge+3, Charge+4
Steal Weapon, Steal Accessory, Arm Aim



Go2sleepTV
Female
Libra
64
57
Summoner
Charge
Counter
Short Charge
Move+3

Poison Rod

Flash Hat
Chameleon Robe
Rubber Shoes

Moogle, Shiva, Ramuh, Carbunkle, Leviathan, Silf, Fairy, Lich
Charge+1, Charge+2, Charge+3



Yasmosis
Female
Leo
68
48
Chemist
Jump
Distribute
Equip Shield
Jump+1

Mythril Gun
Aegis Shield
Cachusha
Brigandine
Cursed Ring

Potion, Phoenix Down
Level Jump4, Vertical Jump8

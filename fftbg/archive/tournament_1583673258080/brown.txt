Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Female
Virgo
48
46
Samurai
Black Magic
MA Save
Equip Shield
Jump+3

Murasame
Mythril Shield
Bronze Helmet
Light Robe
Magic Gauntlet

Asura, Kikuichimoji
Fire 2, Ice, Ice 3, Frog



Mayfu
Female
Taurus
59
67
Geomancer
Draw Out
Counter Flood
Maintenance
Ignore Terrain

Muramasa
Genji Shield
Red Hood
Mythril Vest
Defense Ring

Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Heaven's Cloud



Trowaba
Male
Serpentarius
40
59
Ninja
Summon Magic
MP Restore
Equip Gun
Lava Walking

Bestiary
Bestiary
Black Hood
Judo Outfit
Genji Gauntlet

Bomb, Knife
Moogle, Shiva, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Lich, Cyclops



PlatinumPlume
Female
Virgo
70
69
Dancer
Item
Counter
Equip Polearm
Move+3

Octagon Rod

Golden Hairpin
White Robe
Cursed Ring

Slow Dance, Polka Polka, Nameless Dance
Potion, Hi-Potion, Ether, Eye Drop, Remedy, Phoenix Down

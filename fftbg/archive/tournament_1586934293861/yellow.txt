Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Realitydown
Female
Virgo
74
70
Priest
Charge
Counter
Equip Gun
Jump+3

Papyrus Codex

Holy Miter
Wizard Robe
Rubber Shoes

Raise 2, Reraise, Wall, Esuna
Charge+2, Charge+5



Lydian C
Female
Gemini
67
60
Ninja
Elemental
Counter
Maintenance
Jump+2

Flail
Morning Star
Green Beret
Earth Clothes
Red Shoes

Shuriken, Knife, Axe, Stick
Pitfall, Water Ball, Hallowed Ground, Local Quake, Sand Storm, Gusty Wind



Dymntd
Female
Leo
69
51
Knight
Dance
Counter Flood
Equip Sword
Waterwalking

Ragnarok
Escutcheon
Barbuta
Mythril Armor
Red Shoes

Head Break, Speed Break, Mind Break, Stasis Sword, Dark Sword
Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Dragon Pit



Sinnyil2
Male
Sagittarius
47
61
Lancer
White Magic
Meatbone Slash
Sicken
Teleport 2

Holy Lance
Mythril Shield
Gold Helmet
Platinum Armor
Jade Armlet

Level Jump2, Vertical Jump8
Shell 2, Wall, Esuna

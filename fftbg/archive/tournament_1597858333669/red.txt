Player: !Red
Team: Red Team
Palettes: Red/Brown



Forkmore
Female
Pisces
48
56
Time Mage
Math Skill
Parry
Short Charge
Waterbreathing

Oak Staff

Thief Hat
Judo Outfit
Magic Gauntlet

Haste, Stop, Immobilize, Demi, Stabilize Time, Meteor
CT, Height, Prime Number, 4



MagicBottle
Male
Libra
40
42
Archer
Summon Magic
Faith Save
Defense UP
Move+2

Night Killer
Hero Shield
Golden Hairpin
Power Sleeve
Elf Mantle

Charge+2, Charge+4, Charge+7, Charge+10, Charge+20
Moogle, Ifrit, Titan, Salamander



E Ballard
Male
Leo
48
75
Summoner
Yin Yang Magic
Caution
Defense UP
Swim

Healing Staff

Ribbon
Wizard Robe
Wizard Mantle

Moogle, Shiva, Ifrit, Bahamut, Odin, Lich
Blind, Life Drain, Zombie, Blind Rage, Sleep



SomthingMore
Female
Leo
74
53
Knight
Yin Yang Magic
Auto Potion
Equip Bow
Waterwalking

Perseus Bow
Escutcheon
Iron Helmet
Chameleon Robe
Red Shoes

Head Break, Armor Break, Shield Break, Magic Break, Speed Break
Poison, Spell Absorb, Life Drain, Foxbird, Dispel Magic

Player: !White
Team: White Team
Palettes: White/Blue



Tarheels218
Female
Taurus
76
40
Time Mage
Talk Skill
Parry
Secret Hunt
Jump+2

Healing Staff

Golden Hairpin
Adaman Vest
Leather Mantle

Haste 2, Float
Invitation, Persuade, Praise, Death Sentence, Insult, Rehabilitate



Mesmaster
Male
Capricorn
51
52
Knight
Yin Yang Magic
MA Save
Halve MP
Teleport

Platinum Sword
Mythril Shield
Bronze Helmet
Platinum Armor
Power Wrist

Armor Break, Magic Break, Power Break
Poison, Blind Rage



Powergems
Female
Taurus
41
51
Archer
Item
Damage Split
Equip Knife
Jump+2

Sasuke Knife
Escutcheon
Cachusha
Clothes
Small Mantle

Charge+1, Charge+2
Potion, Hi-Potion, X-Potion, Eye Drop, Phoenix Down



Poorest Hobo
Male
Pisces
64
41
Bard
White Magic
Mana Shield
Beastmaster
Move-MP Up

Bloody Strings

Feather Hat
Chain Mail
Leather Mantle

Magic Song, Sky Demon
Cure, Raise, Raise 2, Protect, Shell, Shell 2, Wall, Esuna

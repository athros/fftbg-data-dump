Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bad1dea
Monster
Taurus
54
66
Cockatrice










Upvla
Male
Aries
43
41
Lancer
Time Magic
Critical Quick
Dual Wield
Jump+2

Spear
Javelin
Iron Helmet
Genji Armor
Leather Mantle

Level Jump2, Vertical Jump5
Haste, Slow, Slow 2, Reflect



Jordache7K
Female
Leo
59
74
Archer
Basic Skill
Distribute
Long Status
Teleport

Yoichi Bow

Green Beret
Earth Clothes
Bracer

Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Accumulate, Dash, Throw Stone, Wish



Laserman1000
Male
Taurus
70
55
Knight
Throw
Parry
Equip Gun
Move-HP Up

Glacier Gun
Escutcheon
Barbuta
Maximillian
Reflect Ring

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Surging Sword
Shuriken, Staff, Wand

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BallSacPuncher
Female
Scorpio
54
74
Priest
Throw
Earplug
Defend
Levitate

Wizard Staff

Feather Hat
Judo Outfit
Defense Armlet

Cure 2, Cure 3, Raise, Raise 2, Protect, Wall, Esuna
Bomb, Staff



Maakur
Male
Leo
63
45
Lancer
Charge
HP Restore
Equip Armor
Retreat

Gokuu Rod
Genji Shield
Circlet
Leather Armor
Angel Ring

Level Jump3, Vertical Jump6
Charge+2



Basmal
Female
Cancer
61
53
Wizard
Draw Out
PA Save
Secret Hunt
Move+1

Dragon Rod

Black Hood
Brigandine
Defense Ring

Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower, Frog, Flare
Koutetsu, Murasame, Muramasa, Kikuichimoji



Arch8000
Female
Virgo
71
69
Chemist
Dance
Catch
Equip Axe
Swim

Flame Whip

Flash Hat
Wizard Outfit
Germinas Boots

Potion, Ether, Antidote, Soft, Phoenix Down
Witch Hunt, Polka Polka, Last Dance, Nether Demon

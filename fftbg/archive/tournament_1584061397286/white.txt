Player: !White
Team: White Team
Palettes: White/Blue



Mirapoix
Female
Virgo
59
46
Chemist
Elemental
Earplug
Long Status
Move-HP Up

Cute Bag

Golden Hairpin
Black Costume
Jade Armlet

Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Rocl
Male
Leo
42
69
Archer
Draw Out
Distribute
Martial Arts
Swim

Blast Gun
Mythril Shield
Golden Hairpin
Leather Outfit
Red Shoes

Charge+2, Charge+4, Charge+5, Charge+10
Muramasa, Kikuichimoji, Masamune



Sypheck
Female
Sagittarius
44
59
Dancer
Steal
Damage Split
Sicken
Jump+3

Persia

Twist Headband
Linen Robe
Vanish Mantle

Slow Dance, Last Dance, Nether Demon
Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory



I Nod My Head When I Lose
Female
Virgo
55
53
Priest
Draw Out
PA Save
Dual Wield
Move+3

Wizard Staff
Rainbow Staff
Twist Headband
White Robe
Reflect Ring

Cure, Cure 2, Shell, Wall, Esuna, Magic Barrier
Muramasa, Kikuichimoji

Player: !Black
Team: Black Team
Palettes: Black/Red



Genericllama
Male
Aries
71
61
Ninja
Battle Skill
Meatbone Slash
Attack UP
Ignore Terrain

Main Gauche
Sasuke Knife
Red Hood
Black Costume
Dracula Mantle

Shuriken, Dictionary
Shield Break, Speed Break, Power Break, Mind Break, Dark Sword, Surging Sword



Phi Sig
Male
Libra
78
39
Squire
Black Magic
Absorb Used MP
Equip Polearm
Move+1

Battle Axe

Triangle Hat
Adaman Vest
Germinas Boots

Accumulate, Throw Stone, Heal, Scream
Fire 2, Bolt, Ice, Ice 4



Ragnarok784
Female
Aquarius
55
77
Samurai
Dance
MP Restore
Equip Gun
Jump+2

Stone Gun

Crystal Helmet
Crystal Mail
Magic Ring

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Witch Hunt, Polka Polka, Obsidian Blade



B0nGj00k
Male
Scorpio
69
44
Lancer
Item
Sunken State
Short Charge
Jump+2

Ivory Rod
Genji Shield
Diamond Helmet
Crystal Mail
Diamond Armlet

Level Jump4, Vertical Jump8
Potion, X-Potion, Echo Grass, Holy Water, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



Aldrammech
Male
Serpentarius
58
53
Mime

Regenerator
Magic Defense UP
Jump+3



Flash Hat
Genji Armor
108 Gems

Mimic




DudeMonkey77
Monster
Libra
47
59
Iron Hawk










Kai Shee
Female
Taurus
50
54
Geomancer
Summon Magic
PA Save
Equip Polearm
Jump+2

Ivory Rod
Genji Shield
Black Hood
Leather Outfit
Vanish Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Golem, Bahamut, Odin, Salamander, Lich



DrAntiSocial
Male
Aquarius
69
79
Wizard
Basic Skill
Catch
Defend
Retreat

Flame Rod

Flash Hat
White Robe
Jade Armlet

Fire, Fire 2, Bolt, Ice, Ice 3, Death, Flare
Throw Stone

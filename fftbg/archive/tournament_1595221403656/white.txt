Player: !White
Team: White Team
Palettes: White/Blue



Galkife
Female
Capricorn
63
43
Calculator
Time Magic
Parry
Equip Knife
Swim

Dagger

Barette
Wizard Robe
Spike Shoes

Height, Prime Number, 4, 3
Haste 2, Slow, Slow 2, Stop, Immobilize, Quick



Sairentozon7
Female
Cancer
63
70
Monk
Dance
Catch
Long Status
Move+3



Flash Hat
Clothes
Feather Mantle

Pummel, Secret Fist, Purification, Revive
Witch Hunt, Wiznaibus, Nameless Dance, Void Storage



Benticore
Male
Sagittarius
59
68
Lancer
Yin Yang Magic
Counter Flood
Dual Wield
Ignore Terrain

Iron Fan
Obelisk
Mythril Helmet
Reflect Mail
N-Kai Armlet

Level Jump4, Vertical Jump7
Blind, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Sleep



Dtrain332
Female
Sagittarius
72
66
Geomancer
Jump
Counter
Concentrate
Ignore Height

Bizen Boat
Platinum Shield
Green Beret
Linen Robe
Reflect Ring

Water Ball, Hallowed Ground, Static Shock, Quicksand, Lava Ball
Level Jump3, Vertical Jump6

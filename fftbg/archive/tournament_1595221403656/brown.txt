Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Furrytomahawkk
Female
Cancer
61
76
Wizard
Talk Skill
Counter Tackle
Equip Gun
Move-HP Up

Romanda Gun

Holy Miter
Silk Robe
Setiemson

Fire 2, Fire 3, Bolt 2, Ice, Ice 2, Flare
Invitation, Persuade, Praise, Preach, Insult, Rehabilitate



Twelfthrootoftwo
Male
Aquarius
55
41
Knight
Time Magic
Critical Quick
Dual Wield
Teleport 2

Coral Sword
Slasher
Platinum Helmet
Reflect Mail
Feather Boots

Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword
Haste, Haste 2, Immobilize, Float, Reflect, Quick, Meteor



ColetteMSLP
Male
Cancer
73
44
Archer
Black Magic
Mana Shield
Dual Wield
Waterbreathing

Long Bow

Barbuta
Mystic Vest
Cherche

Charge+1, Charge+5
Fire, Fire 4, Bolt, Bolt 3, Flare



Roofiepops
Female
Gemini
59
49
Samurai
Jump
Caution
Equip Bow
Teleport 2

Hunting Bow

Bronze Helmet
Reflect Mail
Genji Gauntlet

Heaven's Cloud, Kiyomori
Level Jump5, Vertical Jump2

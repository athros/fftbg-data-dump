Player: !Red
Team: Red Team
Palettes: Red/Brown



ThePineappleSalesman
Male
Aquarius
64
57
Mediator
Steal
Arrow Guard
Beastmaster
Jump+2

Blaze Gun

Leather Hat
Leather Outfit
Rubber Shoes

Invitation, Praise, Preach, Solution, Death Sentence, Mimic Daravon
Gil Taking, Steal Weapon



Waterwatereverywhere
Female
Pisces
66
45
Thief
Jump
Parry
Short Charge
Move-MP Up

Mage Masher

Green Beret
Leather Outfit
Feather Mantle

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Leg Aim
Level Jump4, Vertical Jump7



Jeeboheebo
Male
Libra
52
57
Chemist
Draw Out
Damage Split
Equip Armor
Waterbreathing

Blast Gun

Bronze Helmet
Black Costume
Wizard Mantle

X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy
Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa



B0shii
Male
Libra
57
74
Archer
Punch Art
Parry
Doublehand
Jump+1

Ultimus Bow

Barette
Brigandine
108 Gems

Charge+1, Charge+7
Spin Fist, Purification, Revive

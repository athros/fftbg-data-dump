Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Chuckolator
Female
Libra
48
44
Thief
Summon Magic
MP Restore
Equip Knife
Move+1

Mage Masher

Leather Hat
Mythril Vest
N-Kai Armlet

Steal Heart, Steal Helmet, Steal Shield
Shiva, Titan, Carbunkle, Silf



ANFz
Female
Taurus
49
56
Mediator
Basic Skill
Arrow Guard
Equip Shield
Move+3

Blast Gun
Platinum Shield
Green Beret
Brigandine
Reflect Ring

Persuade, Solution, Death Sentence, Insult, Mimic Daravon, Rehabilitate
Throw Stone, Cheer Up, Scream



Plus Flora
Female
Aquarius
54
59
Lancer
Throw
Faith Save
Sicken
Move+2

Mythril Spear
Gold Shield
Mythril Helmet
White Robe
Feather Mantle

Level Jump8, Vertical Jump4
Sword



LordTomS
Male
Sagittarius
61
71
Archer
Yin Yang Magic
Counter Magic
Equip Bow
Move+2

Ice Bow

Headgear
Brigandine
N-Kai Armlet

Charge+2, Charge+3, Charge+7
Blind Rage, Dispel Magic, Sleep

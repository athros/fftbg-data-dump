Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Killth3kid
Female
Aries
80
62
Samurai
Battle Skill
Caution
Equip Shield
Move-MP Up

Koutetsu Knife
Bronze Shield
Leather Helmet
Plate Mail
Diamond Armlet

Heaven's Cloud
Head Break, Speed Break, Mind Break, Justice Sword



Lumberinjack
Female
Sagittarius
64
53
Wizard
Elemental
Catch
Halve MP
Move+1

Poison Rod

Holy Miter
Mythril Vest
Magic Ring

Fire 3, Ice, Ice 2, Empower, Death
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Laserman1000
Male
Capricorn
56
59
Geomancer
Item
Catch
Equip Sword
Move-MP Up

Defender

Green Beret
Wizard Outfit
Genji Gauntlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Potion, Hi-Ether, Eye Drop, Soft, Phoenix Down



ArchKnightX
Male
Sagittarius
57
51
Knight
Yin Yang Magic
Damage Split
Concentrate
Ignore Height

Defender
Flame Shield
Genji Helmet
White Robe
Setiemson

Power Break, Mind Break, Stasis Sword
Blind, Doubt Faith, Silence Song, Dispel Magic

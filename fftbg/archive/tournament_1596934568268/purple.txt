Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lord Burrah
Male
Gemini
42
58
Mime

Counter
Equip Armor
Move+2



Leather Hat
Leather Armor
Leather Mantle

Mimic




Hasterious
Male
Leo
43
52
Lancer
Basic Skill
Distribute
Martial Arts
Ignore Height


Mythril Shield
Iron Helmet
Leather Armor
Cursed Ring

Level Jump8, Vertical Jump4
Tickle, Yell, Fury, Wish



Drusiform
Male
Virgo
42
40
Bard
Elemental
Counter Magic
Dual Wield
Levitate

Windslash Bow

Thief Hat
Adaman Vest
Germinas Boots

Life Song, Cheer Song, Last Song, Diamond Blade, Sky Demon, Hydra Pit
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



Twisted Nutsatchel
Male
Cancer
41
40
Chemist
Elemental
Distribute
Magic Defense UP
Ignore Terrain

Hydra Bag

Golden Hairpin
Mythril Vest
Defense Armlet

X-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind

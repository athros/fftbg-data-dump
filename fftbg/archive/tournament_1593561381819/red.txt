Player: !Red
Team: Red Team
Palettes: Red/Brown



KyleWonToLiveForever
Male
Gemini
53
45
Ninja
Jump
Earplug
Beastmaster
Jump+2

Spell Edge
Blind Knife
Thief Hat
Earth Clothes
Feather Mantle

Shuriken
Level Jump8, Vertical Jump7



Maakur
Male
Taurus
76
58
Time Mage
Summon Magic
Counter
Dual Wield
Ignore Terrain

Healing Staff
Oak Staff
Green Beret
Chameleon Robe
Vanish Mantle

Haste 2, Slow 2, Immobilize, Reflect, Quick, Stabilize Time
Moogle, Carbunkle, Silf, Fairy



Biffcake01
Male
Taurus
55
43
Squire
Draw Out
Faith Save
Dual Wield
Jump+3

Flame Whip
Battle Axe
Bronze Helmet
Earth Clothes
Spike Shoes

Heal, Cheer Up, Wish
Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji



Nifboy
Male
Gemini
48
46
Samurai
Punch Art
Distribute
Long Status
Lava Walking

Obelisk

Cross Helmet
Gold Armor
Feather Mantle

Asura, Muramasa
Pummel, Earth Slash, Purification, Revive

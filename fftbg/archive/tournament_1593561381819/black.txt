Player: !Black
Team: Black Team
Palettes: Black/Red



Blastty
Female
Libra
75
48
Knight
Punch Art
Mana Shield
Defend
Jump+1

Coral Sword
Mythril Shield
Cross Helmet
Crystal Mail
Feather Mantle

Head Break, Weapon Break, Power Break, Surging Sword
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Seal Evil



Sans From Snowdin
Female
Scorpio
77
63
Priest
Draw Out
Counter Flood
Defend
Move+2

Morning Star

Red Hood
Linen Robe
Genji Gauntlet

Cure, Raise, Protect, Wall, Esuna
Bizen Boat, Heaven's Cloud



Zagorsek
Male
Gemini
45
63
Bard
White Magic
PA Save
Long Status
Move+1

Bloody Strings

Black Hood
Brigandine
Defense Ring

Cheer Song, Magic Song, Diamond Blade, Hydra Pit
Cure, Cure 2, Cure 4, Raise, Protect, Shell 2



DeathTaxesAndAnime
Female
Aries
43
81
Lancer
Item
Critical Quick
Throw Item
Move+2

Javelin
Ice Shield
Mythril Helmet
White Robe
Jade Armlet

Level Jump4, Vertical Jump7
Potion, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down

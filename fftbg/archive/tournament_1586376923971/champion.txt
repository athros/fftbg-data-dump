Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Daveb
Male
Cancer
73
54
Ninja
Talk Skill
Mana Shield
Equip Axe
Move+3

Scorpion Tail
Morning Star
Black Hood
Power Sleeve
Cherche

Shuriken, Knife, Staff, Ninja Sword, Dictionary
Persuade, Threaten, Solution, Death Sentence, Insult, Rehabilitate



Kronikle
Monster
Cancer
62
67
Wyvern










JustSuperish
Female
Taurus
54
58
Wizard
Yin Yang Magic
MP Restore
Defense UP
Jump+1

Panther Bag

Feather Hat
Light Robe
Feather Mantle

Bolt 3, Bolt 4, Ice, Ice 3, Death
Blind, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep



Nizaha
Male
Aries
75
58
Mediator
Draw Out
Faith Up
Dual Wield
Move+3

Mythril Gun
Mythril Gun
Flash Hat
Power Sleeve
Power Wrist

Invitation, Persuade, Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa

Player: !White
Team: White Team
Palettes: White/Blue



Skipsandwiches
Female
Gemini
66
52
Squire
Throw
Arrow Guard
Defense UP
Move+2

Star Bag
Hero Shield
Iron Helmet
Black Costume
Defense Armlet

Tickle, Fury
Shuriken



ALY327
Female
Pisces
59
67
Priest
Yin Yang Magic
Damage Split
Equip Shield
Ignore Terrain

Scorpion Tail
Venetian Shield
Red Hood
Wizard Outfit
Magic Gauntlet

Cure 2, Cure 4, Raise, Protect, Protect 2, Shell, Shell 2, Esuna
Blind, Poison, Spell Absorb, Confusion Song, Dispel Magic, Paralyze, Petrify



Willjin
Male
Sagittarius
78
48
Chemist
Talk Skill
Counter Magic
Magic Attack UP
Swim

Hydra Bag

Headgear
Black Costume
Feather Mantle

Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down
Invitation, Threaten, Solution, Insult, Mimic Daravon, Refute, Rehabilitate



Error72
Male
Aquarius
58
69
Monk
Summon Magic
Auto Potion
Halve MP
Jump+2



Leather Hat
Power Sleeve
Bracer

Spin Fist, Pummel, Wave Fist, Revive
Titan, Carbunkle, Leviathan, Lich

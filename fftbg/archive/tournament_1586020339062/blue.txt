Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Urieltheflameofgod
Monster
Leo
42
76
Sekhret










Volgrathemoose
Female
Gemini
40
53
Ninja
Steal
Regenerator
Magic Attack UP
Levitate

Scorpion Tail
Flail
Leather Hat
Brigandine
Bracer

Shuriken, Knife, Ninja Sword, Wand
Steal Weapon



Galkife
Male
Libra
39
63
Priest
Elemental
Catch
Short Status
Move+2

Morning Star

Black Hood
Light Robe
N-Kai Armlet

Cure, Cure 4, Raise, Reraise, Shell 2, Esuna
Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



ThePineappleSalesman
Male
Capricorn
71
80
Bard
Black Magic
Brave Up
Defend
Ignore Terrain

Mythril Bow

Red Hood
Clothes
Feather Boots

Life Song, Cheer Song, Battle Song, Magic Song, Nameless Song, Diamond Blade, Space Storage
Fire 4, Bolt 3, Ice, Empower

Player: !Green
Team: Green Team
Palettes: Green/White



Flowinprose
Female
Aries
79
58
Oracle
Steal
Arrow Guard
Defend
Ignore Height

Ivory Rod

Twist Headband
Clothes
Genji Gauntlet

Blind, Life Drain, Pray Faith, Blind Rage, Foxbird, Confusion Song
Steal Heart, Steal Accessory



ExecutedGiraffe
Male
Scorpio
39
51
Chemist
Time Magic
Damage Split
Equip Armor
Move-HP Up

Blaze Gun

Feather Hat
Chameleon Robe
Defense Armlet

Potion, Hi-Potion, X-Potion, Antidote, Remedy, Phoenix Down
Haste, Slow 2, Stop, Reflect



Grimmace45
Female
Capricorn
60
77
Squire
Yin Yang Magic
Distribute
Equip Sword
Move+2

Sleep Sword
Platinum Shield
Iron Helmet
Mystic Vest
Feather Boots

Dash, Heal, Yell, Scream
Pray Faith, Blind Rage, Foxbird, Paralyze



SSwing
Female
Gemini
49
53
Geomancer
Summon Magic
Mana Shield
Maintenance
Ignore Terrain

Slasher
Escutcheon
Red Hood
Earth Clothes
Wizard Mantle

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Fairy

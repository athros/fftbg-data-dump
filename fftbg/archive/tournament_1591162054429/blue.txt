Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



OneHundredFists
Male
Sagittarius
78
52
Knight
Elemental
Speed Save
Equip Sword
Ignore Height

Defender
Mythril Shield
Diamond Helmet
Bronze Armor
Jade Armlet

Head Break, Armor Break, Magic Break, Stasis Sword, Justice Sword
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Nhammen
Monster
Aquarius
74
55
Taiju










TinchoT
Male
Scorpio
60
45
Archer
Draw Out
Faith Save
Secret Hunt
Move+1

Night Killer
Platinum Shield
Red Hood
Adaman Vest
Rubber Shoes

Charge+3, Charge+4
Murasame, Kiyomori



DAC169
Female
Cancer
42
73
Priest
Throw
MA Save
Attack UP
Waterbreathing

Flame Whip

Holy Miter
Adaman Vest
Dracula Mantle

Cure, Raise, Reraise, Protect, Protect 2, Shell 2, Esuna, Holy
Shuriken, Stick, Wand

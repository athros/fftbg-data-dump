Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Female
Libra
69
45
Thief
Summon Magic
Mana Shield
Short Status
Retreat

Blood Sword

Leather Hat
Mystic Vest
Battle Boots

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Carbunkle



DeiNomine
Male
Libra
80
77
Thief
Time Magic
Parry
Equip Bow
Move+3

Hunting Bow

Holy Miter
Black Costume
Magic Gauntlet

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Status
Haste, Slow, Reflect, Demi 2, Stabilize Time, Meteor



Gorgewall
Male
Aquarius
50
71
Ninja
Item
Abandon
Long Status
Move+2

Hidden Knife
Dagger
Leather Hat
Leather Outfit
Dracula Mantle

Wand
Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Thyrandaal
Monster
Gemini
51
58
Taiju








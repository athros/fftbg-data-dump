Player: !zChamp
Team: Champion Team
Palettes: Green/White



Winterharte
Male
Virgo
62
74
Oracle
Math Skill
Counter
Dual Wield
Lava Walking

Iron Fan
Octagon Rod
Headgear
Chameleon Robe
Feather Mantle

Doubt Faith, Foxbird, Confusion Song, Petrify
CT, Height, 5, 4, 3



Numbersborne
Female
Aries
70
77
Wizard
Summon Magic
Parry
Equip Armor
Jump+1

Dragon Rod

Bronze Helmet
Bronze Armor
Sprint Shoes

Fire, Fire 2, Fire 3, Bolt, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Frog
Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Leviathan, Salamander



Kl0kwurk
Male
Leo
43
58
Archer
Item
Dragon Spirit
Equip Armor
Teleport

Lightning Bow

Barbuta
Diamond Armor
Jade Armlet

Charge+1, Charge+3
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Powergems
Female
Gemini
61
79
Samurai
Steal
Regenerator
Long Status
Move-HP Up

Murasame

Genji Helmet
Mythril Armor
Diamond Armlet

Asura, Bizen Boat, Chirijiraden
Steal Heart, Steal Helmet, Steal Status

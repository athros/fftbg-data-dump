Player: !Red
Team: Red Team
Palettes: Red/Brown



BlackFireUK
Female
Cancer
81
64
Wizard
Basic Skill
Distribute
Equip Bow
Move+3

Mythril Bow

Flash Hat
Chameleon Robe
Wizard Mantle

Bolt 2, Ice 2, Ice 3, Ice 4, Empower, Death
Accumulate, Dash, Wish, Scream



Alacor
Male
Libra
65
67
Knight
Basic Skill
Sunken State
Short Charge
Move-MP Up

Save the Queen

Platinum Helmet
Chain Mail
Defense Ring

Head Break, Armor Break, Shield Break, Weapon Break
Dash, Throw Stone, Heal, Cheer Up



Numbersborne
Female
Aquarius
52
65
Lancer
Item
Critical Quick
Magic Attack UP
Swim

Holy Lance
Escutcheon
Circlet
Diamond Armor
Bracer

Level Jump8, Vertical Jump8
Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



Shalloween
Male
Cancer
80
68
Squire
Charge
Parry
Martial Arts
Fly


Hero Shield
Crystal Helmet
Platinum Armor
Wizard Mantle

Accumulate, Dash, Heal, Cheer Up
Charge+2, Charge+5

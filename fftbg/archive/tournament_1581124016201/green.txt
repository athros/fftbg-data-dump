Player: !Green
Team: Green Team
Palettes: Green/White



Qexec
Male
Sagittarius
77
72
Ninja
Battle Skill
PA Save
Short Charge
Fly

Cultist Dagger
Cultist Dagger
Feather Hat
Judo Outfit
Dracula Mantle

Shuriken, Bomb, Sword, Dictionary
Weapon Break



Waterwatereverywhere
Female
Aries
43
73
Wizard
Draw Out
Counter Magic
Equip Knife
Jump+3

Koga Knife

Black Hood
Black Robe
Battle Boots

Fire, Fire 3, Bolt 3, Ice 3, Empower
Heaven's Cloud



Wizard 01
Female
Taurus
69
48
Mediator
Dance
Brave Up
Dual Wield
Move-HP Up

Battle Folio
Battle Folio
Leather Hat
Black Robe
Elf Mantle

Invitation, Persuade, Praise, Preach, Death Sentence, Mimic Daravon
Wiznaibus, Slow Dance, Polka Polka, Void Storage



Reginaldjtrotsfield
Female
Taurus
71
64
Ninja
Summon Magic
HP Restore
Equip Bow
Move+2

Hunting Bow
Hunting Bow
Twist Headband
Power Sleeve
Magic Ring

Shuriken, Ninja Sword
Moogle, Carbunkle, Leviathan, Salamander, Silf, Fairy

Player: !Red
Team: Red Team
Palettes: Red/Brown



Dynasti
Monster
Cancer
56
61
Byblos










Maakur
Male
Gemini
57
71
Calculator
Yin Yang Magic
Absorb Used MP
Defense UP
Swim

Gokuu Rod

Triangle Hat
Silk Robe
Setiemson

CT, Height, Prime Number, 4
Blind, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze



TempestuousT
Male
Scorpio
74
68
Monk
Summon Magic
Abandon
Equip Axe
Retreat

Giant Axe

Thief Hat
Power Sleeve
Red Shoes

Wave Fist, Secret Fist, Purification, Revive
Moogle, Ramuh, Titan, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Silf



Bilabrin
Female
Aries
78
51
Ninja
Yin Yang Magic
Abandon
Equip Armor
Teleport 2

Mythril Knife
Short Edge
Mythril Helmet
Bronze Armor
Elf Mantle

Wand
Poison, Zombie, Dispel Magic, Sleep

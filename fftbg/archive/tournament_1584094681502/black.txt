Player: !Black
Team: Black Team
Palettes: Black/Red



PlatinumPlume
Male
Leo
60
51
Time Mage
Elemental
Counter
Doublehand
Fly

White Staff

Triangle Hat
Wizard Outfit
Red Shoes

Haste 2, Stop, Quick, Demi, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard



ApplesauceBoss
Monster
Taurus
81
73
Holy Dragon










Powergems
Female
Leo
74
69
Chemist
Yin Yang Magic
Speed Save
Equip Shield
Move+1

Blast Gun
Mythril Shield
Leather Hat
Black Costume
Reflect Ring

Potion, X-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Remedy
Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify



Grininda
Female
Cancer
70
45
Knight
Jump
Faith Up
Concentrate
Move+3

Defender

Platinum Helmet
Diamond Armor
Magic Ring

Shield Break, Weapon Break, Justice Sword, Night Sword
Level Jump5, Vertical Jump7

Player: !White
Team: White Team
Palettes: White/Blue



Deathmaker06
Female
Aquarius
76
43
Monk
Draw Out
Critical Quick
Short Status
Ignore Height



Red Hood
Mystic Vest
Dracula Mantle

Pummel, Revive
Koutetsu, Bizen Boat, Murasame



Roofiepops
Female
Virgo
79
45
Time Mage
Dance
Counter Magic
Long Status
Fly

Ivory Rod

Triangle Hat
White Robe
Dracula Mantle

Slow, Demi 2, Stabilize Time, Meteor
Witch Hunt, Wiznaibus, Disillusion, Last Dance, Nether Demon, Dragon Pit



Redmage4evah
Female
Aquarius
52
73
Samurai
Throw
Critical Quick
Magic Attack UP
Teleport

Kikuichimoji

Genji Helmet
Linen Cuirass
Wizard Mantle

Asura, Murasame, Kiyomori
Shuriken



Actual JP
Monster
Capricorn
80
69
Porky








Player: !Green
Team: Green Team
Palettes: Green/White



Kyune
Male
Gemini
78
67
Mediator
Draw Out
Damage Split
Long Status
Move+3

Battle Folio

Twist Headband
Mystic Vest
Power Wrist

Threaten, Preach, Solution, Insult, Negotiate, Refute
Bizen Boat



Old Overholt
Male
Libra
46
48
Monk
Charge
Damage Split
Attack UP
Jump+2

Star Bag

Headgear
Secret Clothes
Magic Gauntlet

Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Charge+4, Charge+10



OperatorTheory
Monster
Leo
46
72
Coeurl










Gorgewall
Male
Cancer
72
79
Chemist
Basic Skill
MA Save
Magic Attack UP
Levitate

Main Gauche

Ribbon
Wizard Outfit
Small Mantle

Potion, Hi-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Heal, Yell

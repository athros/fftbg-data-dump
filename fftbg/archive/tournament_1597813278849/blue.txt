Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JonnyCue
Male
Libra
78
45
Wizard
White Magic
Counter Magic
Halve MP
Move-MP Up

Ice Rod

Red Hood
Silk Robe
Feather Mantle

Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Empower, Frog, Death
Cure 2, Raise, Reraise, Regen, Protect, Shell, Shell 2, Esuna



Chuckolator
Female
Cancer
57
52
Mediator
White Magic
Caution
Defense UP
Swim

Stone Gun

Triangle Hat
Black Robe
Angel Ring

Praise, Threaten, Preach, Solution, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Cure, Cure 3, Protect 2, Wall



Kronikle
Female
Libra
79
68
Geomancer
Throw
Speed Save
Magic Attack UP
Ignore Height

Long Sword
Crystal Shield
Headgear
Clothes
Magic Gauntlet

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shuriken, Staff, Stick



HaateXIII
Male
Virgo
47
48
Archer
Yin Yang Magic
Counter
Magic Defense UP
Move-MP Up

Silver Bow

Twist Headband
Chain Vest
Wizard Mantle

Charge+2, Charge+3, Charge+4, Charge+5, Charge+10, Charge+20
Blind, Zombie, Silence Song, Confusion Song, Dark Holy

Final Bets: black - 12 bets for 7,530G (26.1%, x2.84); brown - 14 bets for 21,364G (73.9%, x0.35)

black bets:
nekojin: 1,714G (22.8%, 3,362G)
superdevon1: 1,280G (17.0%, 128,034G)
silentkaster: 1,000G (13.3%, 4,000G)
balmung002: 1,000G (13.3%, 1,000G)
SanzoFunke: 1,000G (13.3%, 1,000G)
JonnyCue: 500G (6.6%, 6,366G)
lowlf: 336G (4.5%, 336G)
Drusiform: 200G (2.7%, 10,556G)
ko2q: 199G (2.6%, 4,207G)
gorgewall: 101G (1.3%, 3,863G)
AllInBot: 100G (1.3%, 100G)
BurlapChampion: 100G (1.3%, 1,250G)

brown bets:
DavenIII: 12,183G (57.0%, 81,225G)
Mesmaster: 2,608G (12.2%, 2,608G)
BirbBrainsBot: 1,000G (4.7%, 55,257G)
Sairentozon7: 1,000G (4.7%, 30,301G)
resjudicata3: 1,000G (4.7%, 8,983G)
ACAClA: 900G (4.2%, 1,000G)
SkylerBunny: 772G (3.6%, 772G)
DLJuggernaut: 729G (3.4%, 729G)
WhiteTigress: 300G (1.4%, 6,701G)
itsonlyspencer: 240G (1.1%, 4,960G)
rNdOrchestra: 200G (0.9%, 2,783G)
datadrivenbot: 200G (0.9%, 66,035G)
getthemoneyz: 132G (0.6%, 1,671,454G)
MinBetBot: 100G (0.5%, 6,848G)

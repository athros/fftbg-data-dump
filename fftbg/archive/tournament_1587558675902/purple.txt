Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mirapoix
Male
Libra
45
57
Time Mage
Yin Yang Magic
MA Save
Equip Polearm
Move+2

Iron Fan

Green Beret
Linen Robe
Sprint Shoes

Haste 2, Slow, Stop, Reflect, Stabilize Time
Poison, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Petrify



Kai Shee
Female
Aries
71
75
Samurai
Time Magic
Catch
Halve MP
Jump+2

Heaven's Cloud

Leather Helmet
Genji Armor
Feather Mantle

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Haste, Reflect, Stabilize Time



Upvla
Male
Pisces
76
60
Archer
Summon Magic
Parry
Dual Wield
Lava Walking

Romanda Gun
Mythril Gun
Gold Helmet
Chain Vest
Setiemson

Charge+1, Charge+2, Charge+5, Charge+7
Moogle, Titan, Bahamut, Silf



CassiePhoenix
Female
Aries
50
49
Knight
Basic Skill
Parry
Short Charge
Ignore Terrain

Chaos Blade
Aegis Shield
Bronze Helmet
Carabini Mail
Red Shoes

Head Break, Armor Break, Shield Break, Weapon Break, Mind Break, Dark Sword, Surging Sword
Cheer Up, Ultima

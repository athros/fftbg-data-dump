Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Kai Shee
Female
Taurus
76
70
Summoner
White Magic
HP Restore
Secret Hunt
Move-HP Up

Dragon Rod

Red Hood
Silk Robe
Dracula Mantle

Ramuh, Fairy, Lich
Cure, Raise 2, Regen, Esuna



JustSuperish
Male
Libra
50
77
Geomancer
Black Magic
Catch
Equip Polearm
Jump+2

Ivory Rod
Crystal Shield
Golden Hairpin
Linen Robe
108 Gems

Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire 2, Fire 4, Bolt 3, Ice, Ice 2, Death



Rechaun
Male
Aries
49
63
Ninja
Basic Skill
PA Save
Equip Axe
Move-HP Up

Spell Edge
Slasher
Green Beret
Mythril Vest
Red Shoes

Shuriken
Dash, Throw Stone, Heal, Tickle, Wish



ForagerCats
Female
Pisces
45
56
Oracle
Talk Skill
MA Save
Monster Talk
Waterbreathing

Papyrus Codex

Flash Hat
Mystic Vest
Red Shoes

Life Drain, Zombie, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify
Invitation, Praise, Preach, Solution, Mimic Daravon, Refute, Rehabilitate

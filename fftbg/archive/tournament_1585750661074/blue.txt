Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



StealthModeLocke
Female
Capricorn
67
74
Squire
Steal
Auto Potion
Equip Polearm
Teleport

Cashmere
Gold Shield
Thief Hat
Genji Armor
Leather Mantle

Accumulate, Heal, Wish
Steal Helmet, Steal Armor, Steal Weapon



RezzaDV
Female
Aries
49
52
Calculator
Yin Yang Magic
Mana Shield
Martial Arts
Jump+2



Holy Miter
Chain Vest
Elf Mantle

CT, Prime Number, 3
Spell Absorb, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Petrify



Roqqqpsi
Monster
Gemini
58
48
Behemoth










Vicemond
Female
Aquarius
65
73
Chemist
Charge
Counter
Attack UP
Move+3

Cute Bag

Golden Hairpin
Wizard Outfit
108 Gems

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop
Charge+1, Charge+3

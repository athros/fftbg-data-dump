Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RunicMagus
Monster
Cancer
66
51
Revenant










Red Celt
Female
Libra
78
69
Priest
Dance
Catch
Equip Shield
Move+3

Healing Staff
Ice Shield
Green Beret
Leather Outfit
Jade Armlet

Cure 4, Raise, Raise 2, Shell, Shell 2, Wall, Esuna
Disillusion, Nameless Dance



Domeil
Female
Libra
45
44
Wizard
Summon Magic
Absorb Used MP
Halve MP
Move-HP Up

Mythril Knife

Black Hood
Linen Robe
Leather Mantle

Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 2, Ice 4
Moogle, Ifrit, Odin, Silf, Lich



Bongomon7
Male
Aries
78
63
Thief
Battle Skill
HP Restore
Short Charge
Swim

Mage Masher

Headgear
Mythril Vest
Reflect Ring

Gil Taking, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Weapon Break, Power Break

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Skillomono
Female
Pisces
64
72
Oracle
Basic Skill
MA Save
Equip Axe
Waterbreathing

White Staff

Flash Hat
Linen Robe
Germinas Boots

Blind, Poison, Life Drain, Pray Faith, Silence Song, Confusion Song, Sleep, Petrify, Dark Holy
Accumulate, Heal, Tickle, Ultima



Codetravis
Male
Leo
41
77
Squire
Punch Art
Catch
Equip Armor
Move+2

Hydra Bag
Aegis Shield
Triangle Hat
Light Robe
Defense Armlet

Dash, Heal, Tickle
Secret Fist, Revive, Seal Evil



WhiteTigress
Female
Capricorn
57
48
Chemist
Punch Art
Speed Save
Dual Wield
Ignore Terrain

Romanda Gun
Mythril Gun
Golden Hairpin
Wizard Outfit
Leather Mantle

Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Pummel



Lawndough
Male
Sagittarius
56
73
Ninja
White Magic
Mana Shield
Magic Attack UP
Move+1

Assassin Dagger
Cultist Dagger
Flash Hat
Mythril Vest
Red Shoes

Staff, Ninja Sword
Raise, Regen, Esuna

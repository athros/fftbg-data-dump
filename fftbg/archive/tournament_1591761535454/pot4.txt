Final Bets: purple - 21 bets for 21,810G (71.9%, x0.39); brown - 18 bets for 8,542G (28.1%, x2.55)

purple bets:
OneHundredFists: 5,000G (22.9%, 29,155G)
Evewho: 3,762G (17.2%, 7,378G)
DeathTaxesAndAnime: 2,736G (12.5%, 5,366G)
Oobs56: 1,807G (8.3%, 3,615G)
Digitalsocrates: 1,260G (5.8%, 2,521G)
KasugaiRoastedPeas: 1,064G (4.9%, 1,064G)
HaychDub: 1,001G (4.6%, 19,408G)
SkylerBunny: 1,000G (4.6%, 106,196G)
DLJuggernaut: 968G (4.4%, 968G)
superdevon1: 795G (3.6%, 7,957G)
killth3kid: 500G (2.3%, 12,236G)
Nohurty: 376G (1.7%, 752G)
Sairentozon7: 320G (1.5%, 320G)
joewcarson: 300G (1.4%, 45,953G)
silentkaster: 300G (1.4%, 300G)
Lydian_C: 120G (0.6%, 17,516G)
gorgewall: 101G (0.5%, 15,071G)
AllInBot: 100G (0.5%, 100G)
ChronoG: 100G (0.5%, 4,891G)
datadrivenbot: 100G (0.5%, 38,383G)
Grandlanzer: 100G (0.5%, 10,113G)

brown bets:
roofiepops: 1,400G (16.4%, 7,065G)
Thyrandaal: 1,234G (14.4%, 96,880G)
Meta_Five: 1,004G (11.8%, 1,004G)
BirbBrainsBot: 1,000G (11.7%, 61,046G)
Mesmaster: 845G (9.9%, 845G)
Rislyeu: 576G (6.7%, 576G)
electric_algus: 500G (5.9%, 5,760G)
getthemoneyz: 400G (4.7%, 882,123G)
byrdturbo: 333G (3.9%, 15,100G)
CaptainGarlock: 200G (2.3%, 7,777G)
GladiatorLupe: 200G (2.3%, 1,131G)
JLinkletter: 200G (2.3%, 1,798G)
mrfripps: 200G (2.3%, 1,622G)
E_Ballard: 100G (1.2%, 3,794G)
CosmicTactician: 100G (1.2%, 26,139G)
Miku_Shikhu: 100G (1.2%, 19,967G)
moocaotao: 100G (1.2%, 1,738G)
cougboi: 50G (0.6%, 16,765G)

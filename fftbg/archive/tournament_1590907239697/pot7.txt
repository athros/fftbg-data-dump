Final Bets: blue - 10 bets for 5,048G (54.0%, x0.85); brown - 13 bets for 4,307G (46.0%, x1.17)

blue bets:
flowinprose: 1,000G (19.8%, 129,636G)
killth3kid: 1,000G (19.8%, 26,597G)
BirbBrainsBot: 1,000G (19.8%, 109,725G)
soren_of_tyto: 663G (13.1%, 663G)
CorpusCav: 500G (9.9%, 20,191G)
maleblackfiora: 300G (5.9%, 1,605G)
gorgewall: 201G (4.0%, 1,902G)
VynxYukida: 200G (4.0%, 711G)
Qaz12301: 100G (2.0%, 1,704G)
getthemoneyz: 84G (1.7%, 780,899G)

brown bets:
DustBirdEX: 789G (18.3%, 7,506G)
HASTERIOUS: 612G (14.2%, 12,250G)
superdevon1: 500G (11.6%, 500G)
cptjdang: 500G (11.6%, 1,877G)
gogofromtogo: 500G (11.6%, 4,175G)
RaIshtar: 272G (6.3%, 272G)
ASkyNightly: 200G (4.6%, 345G)
Valentine009: 200G (4.6%, 200G)
TheMM42: 200G (4.6%, 12,523G)
nok____: 186G (4.3%, 18,070G)
SoIidSmok3: 148G (3.4%, 148G)
waterwatereverywhere: 100G (2.3%, 7,828G)
datadrivenbot: 100G (2.3%, 29,486G)

Player: !Green
Team: Green Team
Palettes: Green/White



Maakur
Monster
Capricorn
75
62
Dryad










L2 Sentinel
Male
Leo
72
70
Lancer
Yin Yang Magic
Arrow Guard
Short Status
Teleport

Mythril Spear
Platinum Shield
Platinum Helmet
Carabini Mail
Jade Armlet

Level Jump8, Vertical Jump2
Blind, Life Drain, Zombie, Blind Rage, Dispel Magic



Roofiepops
Female
Capricorn
62
45
Priest
Throw
Counter
Magic Defense UP
Waterbreathing

Gold Staff

Black Hood
Black Robe
Salty Rage

Cure, Cure 3, Raise, Raise 2, Shell, Wall, Esuna
Bomb, Wand



MrDusa
Male
Capricorn
73
63
Chemist
Battle Skill
Mana Shield
Equip Sword
Jump+2

Broad Sword

Holy Miter
Leather Outfit
Cursed Ring

Potion, Hi-Potion, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down
Head Break, Magic Break, Mind Break, Stasis Sword, Justice Sword

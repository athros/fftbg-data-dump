Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Legitimized
Female
Virgo
71
48
Time Mage
Yin Yang Magic
Brave Up
Dual Wield
Retreat

Bestiary
Papyrus Codex
Feather Hat
Chameleon Robe
Spike Shoes

Slow, Slow 2, Demi
Blind, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy



Grashmu
Female
Leo
62
60
Calculator
White Magic
Brave Up
Beastmaster
Move-HP Up

Papyrus Codex

Triangle Hat
Linen Robe
Feather Boots

CT, Height, Prime Number, 4, 3
Cure 3, Cure 4, Raise, Reraise, Esuna, Holy



ZephyrTempest
Female
Scorpio
53
52
Knight
Time Magic
Counter Tackle
Halve MP
Move+1

Defender

Leather Helmet
Genji Armor
Jade Armlet

Head Break, Speed Break, Dark Sword, Surging Sword
Haste 2, Stop, Immobilize, Quick, Demi 2



Superdevon1
Male
Taurus
39
62
Bard
Steal
Dragon Spirit
Halve MP
Lava Walking

Ramia Harp

Barette
Leather Vest
Bracer

Angel Song, Battle Song, Magic Song, Diamond Blade, Space Storage, Hydra Pit
Steal Shield, Steal Status

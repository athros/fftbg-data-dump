Player: !Black
Team: Black Team
Palettes: Black/Red



UmbraKnights
Female
Libra
60
72
Knight
Draw Out
MA Save
Defense UP
Jump+3

Save the Queen
Bronze Shield
Iron Helmet
Gold Armor
Feather Boots

Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Dark Sword
Asura



Squatting Bear
Male
Capricorn
57
64
Mediator
Punch Art
Mana Shield
Magic Attack UP
Ignore Height

Glacier Gun

Flash Hat
Black Robe
Leather Mantle

Praise, Threaten, Preach, Solution, Insult, Refute
Spin Fist, Purification, Seal Evil



Jeeboheebo
Male
Cancer
64
76
Mime

Counter Tackle
Doublehand
Move+3



Green Beret
Power Sleeve
Sprint Shoes

Mimic




Error72
Female
Pisces
59
39
Ninja
Black Magic
Dragon Spirit
Equip Gun
Teleport

Ramia Harp
Bestiary
Golden Hairpin
Black Costume
Leather Mantle

Shuriken, Ninja Sword
Fire 2, Fire 4, Bolt 2, Ice 2

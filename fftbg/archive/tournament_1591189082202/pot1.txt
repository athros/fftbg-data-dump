Final Bets: red - 9 bets for 6,265G (41.8%, x1.39); blue - 17 bets for 8,723G (58.2%, x0.72)

red bets:
TheChainNerd: 1,500G (23.9%, 20,256G)
elelor: 1,092G (17.4%, 2,142G)
Nickyfive: 1,036G (16.5%, 1,036G)
twelfthrootoftwo: 1,000G (16.0%, 11,787G)
gr8keeper: 574G (9.2%, 574G)
ShintaroNayaka: 400G (6.4%, 7,460G)
coralreeferz: 350G (5.6%, 1,014G)
alterworlds: 193G (3.1%, 193G)
Lydian_C: 120G (1.9%, 4,265G)

blue bets:
DavenIII: 1,500G (17.2%, 5,884G)
serperemagus: 1,208G (13.8%, 1,208G)
Error72: 1,000G (11.5%, 12,305G)
BirbBrainsBot: 1,000G (11.5%, 179,759G)
StealthModeLocke: 583G (6.7%, 583G)
cam_ATS: 578G (6.6%, 578G)
Who_lio42: 500G (5.7%, 15,968G)
letdowncity: 500G (5.7%, 19,089G)
gogofromtogo: 465G (5.3%, 465G)
nifboy: 250G (2.9%, 3,644G)
Belkra: 228G (2.6%, 456G)
getthemoneyz: 216G (2.5%, 797,957G)
lifeguard_dan: 200G (2.3%, 3,580G)
Shinzutalos: 200G (2.3%, 200G)
Qaz12301: 100G (1.1%, 3,744G)
datadrivenbot: 100G (1.1%, 31,326G)
zepharoth89: 95G (1.1%, 1,902G)

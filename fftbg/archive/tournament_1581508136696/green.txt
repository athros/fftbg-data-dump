Player: !Green
Team: Green Team
Palettes: Green/White



PKayge
Male
Aquarius
43
42
Monk
Elemental
Arrow Guard
Attack UP
Swim



Ribbon
Chain Vest
Reflect Ring

Pummel, Earth Slash, Secret Fist, Purification, Revive
Pitfall, Hell Ivy, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Rechaun
Female
Gemini
53
65
Lancer
Item
Absorb Used MP
Throw Item
Fly

Obelisk
Crystal Shield
Platinum Helmet
Crystal Mail
Jade Armlet

Level Jump8, Vertical Jump6
Potion, Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Chococorne
Male
Cancer
51
73
Summoner
Sing
Dragon Spirit
Magic Attack UP
Jump+1

Wizard Staff

Leather Hat
Black Robe
Angel Ring

Carbunkle, Odin, Fairy, Cyclops
Angel Song, Battle Song, Nameless Song, Last Song, Hydra Pit



Quinnotetiquan
Female
Aries
53
45
Geomancer
Punch Art
Parry
Short Status
Jump+3

Slasher
Platinum Shield
Leather Hat
Black Robe
Bracer

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil

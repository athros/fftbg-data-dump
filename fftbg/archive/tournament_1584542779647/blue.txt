Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RjA0zcOQ96
Male
Gemini
77
57
Chemist
Jump
Arrow Guard
Defense UP
Jump+2

Star Bag

Flash Hat
Power Sleeve
Small Mantle

Potion, Hi-Potion, X-Potion, Eye Drop, Remedy, Phoenix Down
Level Jump5, Vertical Jump8



Leakimiko
Male
Libra
42
75
Thief
Jump
Distribute
Halve MP
Jump+1

Main Gauche

Headgear
Wizard Outfit
Magic Gauntlet

Steal Armor, Steal Weapon
Level Jump5, Vertical Jump5



Black Sheep 0213
Male
Libra
58
44
Priest
Draw Out
Earplug
Defend
Move-HP Up

Morning Star

Holy Miter
Robe of Lords
Power Wrist

Cure, Cure 3, Raise, Protect 2, Shell, Shell 2, Wall, Esuna
Muramasa



Lord Burrah
Female
Aquarius
41
78
Calculator
Limit
Counter Tackle
Maintenance
Lava Walking

Ice Rod
Aegis Shield
Twist Headband
Mystic Vest
Magic Gauntlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

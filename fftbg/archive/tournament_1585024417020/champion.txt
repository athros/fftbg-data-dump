Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Sairentozon7
Male
Virgo
74
74
Monk
Basic Skill
Brave Up
Maintenance
Jump+1



Leather Hat
Power Sleeve
Defense Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive
Accumulate, Throw Stone, Heal, Tickle, Yell, Scream



SkylerBunny
Male
Scorpio
63
63
Lancer
Summon Magic
Arrow Guard
Magic Attack UP
Levitate

Iron Fan
Diamond Shield
Mythril Helmet
Mythril Armor
Sprint Shoes

Level Jump8, Vertical Jump8
Moogle, Ifrit, Titan, Golem, Carbunkle, Salamander, Silf, Fairy



PatSouI
Female
Aquarius
64
76
Archer
White Magic
Faith Up
Halve MP
Ignore Height

Silver Bow

Holy Miter
Mythril Vest
Feather Boots

Charge+2, Charge+4
Raise, Protect 2, Wall



SeedSC
Monster
Sagittarius
73
79
Ultima Demon








Player: !Green
Team: Green Team
Palettes: Green/White



ZZ Yoshi
Male
Libra
66
79
Priest
Battle Skill
Sunken State
Defense UP
Move+2

Gold Staff

Triangle Hat
Adaman Vest
Red Shoes

Cure, Cure 2, Cure 4, Raise 2, Reraise, Shell, Shell 2, Wall, Esuna
Head Break, Armor Break, Shield Break, Speed Break, Stasis Sword, Night Sword



GrayGhostGaming
Male
Leo
50
59
Squire
Battle Skill
Sunken State
Monster Talk
Jump+2

Cross Bow
Ice Shield
Gold Helmet
Clothes
Magic Gauntlet

Accumulate, Throw Stone, Heal, Yell, Scream
Power Break, Justice Sword



Kyune
Female
Leo
41
52
Thief
Punch Art
Faith Up
Equip Gun
Swim

Papyrus Codex

Golden Hairpin
Clothes
Magic Ring

Steal Heart, Steal Armor, Steal Status, Leg Aim
Earth Slash, Secret Fist, Purification, Chakra, Revive



Thethorndog1
Male
Gemini
59
59
Summoner
Time Magic
Arrow Guard
Beastmaster
Retreat

Rod

Headgear
Wizard Robe
Small Mantle

Moogle, Shiva, Carbunkle, Odin, Leviathan, Cyclops
Haste, Haste 2, Slow 2, Stop, Demi, Stabilize Time

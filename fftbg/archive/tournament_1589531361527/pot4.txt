Final Bets: purple - 10 bets for 3,168G (37.7%, x1.65); brown - 12 bets for 5,235G (62.3%, x0.61)

purple bets:
ungabunga_bot: 1,000G (31.6%, 495,592G)
Orerries: 968G (30.6%, 4,188G)
CorpusCav: 300G (9.5%, 7,140G)
Aestheta: 200G (6.3%, 2,411G)
Lolprinze: 200G (6.3%, 790G)
ko2q: 100G (3.2%, 1,971G)
datadrivenbot: 100G (3.2%, 18,632G)
ZZ_Yoshi: 100G (3.2%, 29,946G)
Yasmosis: 100G (3.2%, 1,326G)
zzaanexx: 100G (3.2%, 2,426G)

brown bets:
Dymntd: 1,031G (19.7%, 1,031G)
BirbBrainsBot: 1,000G (19.1%, 65,835G)
Lionhermit: 1,000G (19.1%, 49,828G)
twelfthrootoftwo: 500G (9.6%, 6,823G)
ApplesauceBoss: 348G (6.6%, 348G)
gorgewall: 301G (5.7%, 3,437G)
thunderducker: 225G (4.3%, 2,199G)
thethorndog1: 224G (4.3%, 224G)
LonelyLammas: 200G (3.8%, 1,176G)
getthemoneyz: 182G (3.5%, 669,770G)
foofermoofer: 124G (2.4%, 124G)
otakutaylor: 100G (1.9%, 1,154G)

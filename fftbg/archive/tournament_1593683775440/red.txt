Player: !Red
Team: Red Team
Palettes: Red/Brown



Agemo027
Female
Gemini
51
44
Squire
Black Magic
Earplug
Concentrate
Ignore Terrain

Scorpion Tail
Hero Shield
Feather Hat
Linen Cuirass
Dracula Mantle

Dash
Fire, Frog



TeaTime29
Monster
Serpentarius
54
45
Gobbledeguck










SaltiestMage
Female
Scorpio
53
64
Wizard
Item
Counter
Equip Knife
Move+1

Kunai

Twist Headband
Black Robe
Wizard Mantle

Fire 2, Bolt 2, Bolt 3, Ice, Ice 2, Flare
Potion, X-Potion, Eye Drop, Soft, Remedy, Phoenix Down



Regine
Female
Serpentarius
46
55
Summoner
Talk Skill
Distribute
Halve MP
Jump+1

Rod

Flash Hat
Light Robe
Leather Mantle

Ramuh, Titan, Bahamut, Odin, Salamander, Fairy, Cyclops
Invitation, Threaten, Preach, Solution

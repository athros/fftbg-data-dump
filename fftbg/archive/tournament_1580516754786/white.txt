Player: !White
Team: White Team
Palettes: White/Blue



Artea
Female
Sagittarius
65
75
Chemist
Steal
Critical Quick
Dual Wield
Ignore Terrain

Blind Knife
Star Bag
Triangle Hat
Mythril Vest
Reflect Ring

Potion, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Gil Taking, Steal Weapon, Steal Status



Anethum
Male
Libra
57
59
Mime

Damage Split
Equip Armor
Retreat



Bronze Helmet
Mythril Armor
Bracer

Mimic




ArchKnightX
Female
Libra
59
52
Priest
Draw Out
Sunken State
Short Status
Ignore Terrain

Wizard Staff

Cachusha
Leather Outfit
Cherche

Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect, Shell, Shell 2, Esuna, Holy
Koutetsu, Heaven's Cloud, Muramasa



DynamoCG
Female
Gemini
47
51
Mime

Blade Grasp
Equip Armor
Move-MP Up



Holy Miter
Black Robe
Feather Mantle

Mimic


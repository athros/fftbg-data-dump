Final Bets: red - 13 bets for 9,573G (41.1%, x1.44); blue - 23 bets for 13,741G (58.9%, x0.70)

red bets:
Baron_von_Scrub: 4,670G (48.8%, 9,341G)
Mesmaster: 1,000G (10.4%, 98,817G)
DarrenDinosaurs: 891G (9.3%, 891G)
VolgraTheMoose: 800G (8.4%, 800G)
E_Ballard: 660G (6.9%, 660G)
killth3kid: 500G (5.2%, 11,933G)
Rislyeu: 336G (3.5%, 336G)
KasugaiRoastedPeas: 200G (2.1%, 1,523G)
rmelzer1986: 182G (1.9%, 182G)
porkchop5158: 124G (1.3%, 124G)
Firesheath: 100G (1.0%, 9,214G)
Lythe_Caraker: 100G (1.0%, 142,400G)
nhammen: 10G (0.1%, 1,476G)

blue bets:
dogsandcatsand: 2,250G (16.4%, 4,500G)
CosmicTactician: 2,000G (14.6%, 20,447G)
Nickyfive: 1,500G (10.9%, 29,292G)
fenaen: 1,200G (8.7%, 32,903G)
Dasutin23: 1,000G (7.3%, 32,871G)
BirbBrainsBot: 1,000G (7.3%, 43,524G)
DLJuggernaut: 873G (6.4%, 873G)
fluffskull: 673G (4.9%, 673G)
RunicMagus: 500G (3.6%, 54,811G)
Shalloween: 400G (2.9%, 77,938G)
getthemoneyz: 348G (2.5%, 1,069,042G)
sososteve: 300G (2.2%, 1,845G)
ColetteMSLP: 300G (2.2%, 2,975G)
Nizaha: 201G (1.5%, 9,909G)
LionelParks: 200G (1.5%, 6,261G)
extinctrational: 200G (1.5%, 662G)
Mermydon: 196G (1.4%, 196G)
moocaotao: 100G (0.7%, 4,383G)
placidphoenix: 100G (0.7%, 4,883G)
datadrivenbot: 100G (0.7%, 48,360G)
Digitalsocrates: 100G (0.7%, 2,698G)
nifboy: 100G (0.7%, 3,521G)
Ring_Wyrm: 100G (0.7%, 3,344G)

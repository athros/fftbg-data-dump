Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gorgewall
Male
Taurus
79
80
Monk
Time Magic
Auto Potion
Dual Wield
Retreat



Flash Hat
Mystic Vest
Battle Boots

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Haste, Quick, Demi 2



Kellios11
Male
Leo
79
68
Summoner
Sing
Parry
Maintenance
Ignore Height

Flame Rod

Black Hood
Linen Robe
Jade Armlet

Moogle, Shiva, Ifrit, Golem, Carbunkle, Bahamut, Odin, Fairy, Lich
Angel Song, Life Song, Sky Demon, Hydra Pit



ALY327
Female
Aquarius
39
78
Summoner
Talk Skill
Faith Save
Short Charge
Waterbreathing

Wizard Staff

Cachusha
Wizard Robe
108 Gems

Shiva, Ifrit, Carbunkle, Salamander, Silf
Praise, Preach, Solution, Death Sentence, Rehabilitate



ArchKnightX
Male
Capricorn
53
75
Monk
Steal
Earplug
Sicken
Levitate



Twist Headband
Mystic Vest
Magic Gauntlet

Wave Fist, Secret Fist, Purification
Steal Heart, Steal Armor, Steal Status, Leg Aim

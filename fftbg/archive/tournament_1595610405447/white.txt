Player: !White
Team: White Team
Palettes: White/Blue



Othatmattkid
Female
Capricorn
54
39
Knight
Elemental
Earplug
Doublehand
Lava Walking

Defender

Iron Helmet
Carabini Mail
Vanish Mantle

Weapon Break, Speed Break, Mind Break, Surging Sword
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



SomthingMore
Female
Aries
63
73
Samurai
Yin Yang Magic
Absorb Used MP
Beastmaster
Move-MP Up

Heaven's Cloud

Platinum Helmet
Diamond Armor
Reflect Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Pray Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep



Mpghappiness
Male
Libra
47
72
Priest
Draw Out
Critical Quick
Halve MP
Ignore Height

White Staff

Golden Hairpin
Black Robe
Power Wrist

Cure 2, Raise, Raise 2, Reraise, Regen, Protect, Wall
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



TheeBennison
Male
Scorpio
66
46
Priest
Basic Skill
Counter Flood
Dual Wield
Jump+2

Flail
Flame Whip
Holy Miter
Wizard Robe
Leather Mantle

Raise, Shell, Shell 2, Esuna, Holy
Accumulate, Heal, Tickle, Cheer Up

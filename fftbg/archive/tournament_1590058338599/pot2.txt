Final Bets: green - 7 bets for 4,221G (66.9%, x0.49); yellow - 7 bets for 2,084G (33.1%, x2.03)

green bets:
reinoe: 1,000G (23.7%, 15,051G)
UmaiJam: 1,000G (23.7%, 4,652G)
getthemoneyz: 900G (21.3%, 682,843G)
AUrato: 424G (10.0%, 10,621G)
gorgewall: 401G (9.5%, 12,876G)
Aneyus: 309G (7.3%, 30,932G)
BirbBrainsBot: 187G (4.4%, 32,425G)

yellow bets:
rechaun: 674G (32.3%, 674G)
d4rr1n: 566G (27.2%, 566G)
Digitalsocrates: 444G (21.3%, 444G)
Jeeboheebo: 100G (4.8%, 2,309G)
CosmicTactician: 100G (4.8%, 24,956G)
datadrivenbot: 100G (4.8%, 23,607G)
Evewho: 100G (4.8%, 7,537G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Cloudycube
Female
Leo
46
41
Mime

Sunken State
Martial Arts
Ignore Height


Genji Shield
Feather Hat
Mystic Vest
Defense Armlet

Mimic




Thyrandaal
Male
Gemini
65
39
Archer
White Magic
Mana Shield
Equip Knife
Move+3

Dagger
Mythril Shield
Flash Hat
Brigandine
Spike Shoes

Charge+5
Cure, Cure 3, Raise, Reraise, Regen, Esuna



Technominari
Female
Sagittarius
60
76
Priest
Yin Yang Magic
Counter
Equip Gun
Jump+1

Bestiary

Black Hood
Light Robe
Defense Armlet

Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Shell 2, Wall, Esuna
Blind, Spell Absorb, Life Drain, Confusion Song, Dispel Magic, Dark Holy



JCBoorgo
Male
Sagittarius
80
71
Bard
Charge
PA Save
Sicken
Waterwalking

Ramia Harp

Leather Hat
Plate Mail
Diamond Armlet

Battle Song, Nameless Song, Last Song
Charge+5, Charge+7, Charge+20

Player: !Green
Team: Green Team
Palettes: Green/White



ArchKnightX
Female
Taurus
75
50
Monk
Jump
Earplug
Attack UP
Move+1



Thief Hat
Rubber Costume
Defense Ring

Earth Slash, Purification
Level Jump4, Vertical Jump3



Sairentozon7
Male
Sagittarius
61
65
Chemist
Punch Art
Dragon Spirit
Equip Bow
Ignore Height

Poison Bow

Flash Hat
Black Costume
Dracula Mantle

Potion, Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down
Wave Fist, Purification, Revive, Seal Evil



HaateXIII
Male
Libra
76
64
Chemist
Charge
Meatbone Slash
Equip Shield
Jump+2

Cultist Dagger
Ice Shield
Golden Hairpin
Leather Outfit
Wizard Mantle

Hi-Potion, X-Potion, Hi-Ether, Soft, Phoenix Down
Charge+20



Loveyouallfriends
Male
Pisces
68
46
Wizard
Time Magic
Earplug
Long Status
Move-MP Up

Blind Knife

Black Hood
Wizard Outfit
Bracer

Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Ice 4
Float, Stabilize Time

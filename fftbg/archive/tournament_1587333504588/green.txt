Player: !Green
Team: Green Team
Palettes: Green/White



Lordminsc
Female
Capricorn
76
71
Priest
Math Skill
Hamedo
Beastmaster
Move-MP Up

Wizard Staff

Headgear
Chameleon Robe
Angel Ring

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Wall, Esuna
CT, Height, Prime Number, 4, 3



Nedryerson01
Male
Gemini
49
72
Lancer
Black Magic
Counter Tackle
Martial Arts
Move+3


Hero Shield
Leather Helmet
Chain Mail
Defense Ring

Level Jump8, Vertical Jump8
Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 3, Empower



Error72
Male
Libra
54
69
Geomancer
Talk Skill
Counter Tackle
Beastmaster
Move+3

Giant Axe
Crystal Shield
Headgear
Adaman Vest
Small Mantle

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Persuade, Preach, Solution, Mimic Daravon



BuffaloCrunch
Female
Serpentarius
41
40
Samurai
Basic Skill
HP Restore
Magic Attack UP
Jump+2

Heaven's Cloud

Iron Helmet
Carabini Mail
Power Wrist

Koutetsu, Bizen Boat
Heal

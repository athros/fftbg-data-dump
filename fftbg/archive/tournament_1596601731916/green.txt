Player: !Green
Team: Green Team
Palettes: Green/White



Dymntd
Female
Sagittarius
71
74
Oracle
Throw
Caution
Martial Arts
Move+3



Red Hood
Chameleon Robe
108 Gems

Spell Absorb, Life Drain, Confusion Song, Dispel Magic, Sleep, Petrify
Shuriken



ThreeMileIsland
Female
Cancer
77
50
Wizard
Elemental
Caution
Defense UP
Swim

Wizard Rod

Golden Hairpin
Clothes
Spike Shoes

Fire, Fire 3, Bolt, Bolt 3, Ice, Empower
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Nekojin
Male
Scorpio
79
51
Calculator
Byblos
Absorb Used MP
Magic Defense UP
Swim

Cute Bag
Mythril Shield
Cross Helmet
Gold Armor
Red Shoes

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Outer Monologue
Female
Scorpio
50
52
Dancer
Punch Art
HP Restore
Equip Armor
Teleport

Persia

Iron Helmet
Carabini Mail
Defense Armlet

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Last Dance
Secret Fist, Purification, Chakra, Revive

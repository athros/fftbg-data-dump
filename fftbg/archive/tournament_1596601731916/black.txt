Player: !Black
Team: Black Team
Palettes: Black/Red



Fattunaking
Male
Libra
72
50
Monk
Item
Auto Potion
Short Status
Waterbreathing



Red Hood
Wizard Outfit
108 Gems

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Antidote, Eye Drop, Phoenix Down



Artea
Male
Serpentarius
52
42
Oracle
Throw
Earplug
Maintenance
Teleport

Papyrus Codex

Holy Miter
Silk Robe
Cherche

Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Shuriken, Knife, Spear, Dictionary



Gelwain
Female
Sagittarius
68
59
Ninja
Dance
Parry
Defense UP
Levitate

Hydra Bag
Morning Star
Barette
Judo Outfit
Sprint Shoes

Shuriken, Bomb, Stick
Slow Dance, Last Dance, Nether Demon



The Pengwin
Monster
Taurus
76
78
Revenant








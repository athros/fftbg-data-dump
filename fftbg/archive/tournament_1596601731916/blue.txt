Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Latebit
Male
Libra
68
46
Summoner
Item
Caution
Throw Item
Move+1

Flame Rod

Flash Hat
Chain Vest
Bracer

Moogle
Potion, Hi-Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down



Wooplestein
Male
Taurus
48
45
Knight
Throw
Abandon
Equip Axe
Swim

Flail
Flame Shield
Barbuta
Gold Armor
Magic Gauntlet

Head Break, Shield Break, Magic Break, Power Break, Justice Sword, Dark Sword, Night Sword
Shuriken, Bomb, Wand



Armaniimam
Female
Leo
67
54
Monk
Dance
Damage Split
Equip Armor
Move+2



Platinum Helmet
Wizard Outfit
Genji Gauntlet

Revive
Witch Hunt, Last Dance, Obsidian Blade



Calajo
Female
Taurus
61
63
Priest
Charge
Earplug
Halve MP
Jump+2

Wizard Staff

Red Hood
Black Robe
Jade Armlet

Cure 2, Cure 3, Cure 4, Raise 2, Protect 2, Esuna
Charge+1, Charge+2, Charge+3, Charge+4

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



JackOnFire1
Male
Capricorn
70
76
Knight
White Magic
Counter
Defense UP
Waterbreathing

Slasher
Escutcheon
Gold Helmet
Carabini Mail
Leather Mantle

Shield Break, Weapon Break, Stasis Sword, Justice Sword
Cure 2, Cure 3, Cure 4, Raise, Holy, Magic Barrier



E7bbk
Male
Sagittarius
58
52
Ninja
Punch Art
HP Restore
Equip Armor
Move+2

Sasuke Knife
Kunai
Flash Hat
Crystal Mail
Cursed Ring

Shuriken, Sword, Dictionary
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive



Dynasti
Male
Pisces
65
65
Ninja
Steal
Hamedo
Magic Defense UP
Waterbreathing

Koga Knife
Ninja Edge
Thief Hat
Power Sleeve
Power Wrist

Bomb, Sword, Spear, Stick
Gil Taking, Steal Heart, Steal Armor, Steal Status, Leg Aim



Luminarii
Male
Gemini
74
66
Summoner
Draw Out
Parry
Short Charge
Ignore Height

Ice Rod

Golden Hairpin
White Robe
Diamond Armlet

Moogle, Ramuh, Titan, Golem, Odin, Salamander, Silf
Asura, Koutetsu, Murasame

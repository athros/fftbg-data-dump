Final Bets: green - 5 bets for 4,534G (51.9%, x0.93); champion - 7 bets for 4,200G (48.1%, x1.08)

green bets:
UmaiJam: 1,700G (37.5%, 79,456G)
cam_ATS: 1,234G (27.2%, 52,930G)
Draconis345: 1,000G (22.1%, 69,183G)
richardserious: 400G (8.8%, 7,701G)
datadrivenbot: 200G (4.4%, 56,356G)

champion bets:
Evewho: 1,611G (38.4%, 1,611G)
getthemoneyz: 786G (18.7%, 1,205,373G)
BirbBrainsBot: 769G (18.3%, 79,414G)
NIghtdew14: 500G (11.9%, 8,980G)
DustBirdEX: 234G (5.6%, 2,916G)
Nymissa: 200G (4.8%, 1,872G)
Error72: 100G (2.4%, 14,095G)

Final Bets: blue - 11 bets for 5,235G (40.5%, x1.47); black - 11 bets for 7,687G (59.5%, x0.68)

blue bets:
ColetteMSLP: 2,000G (38.2%, 46,541G)
mirapoix: 1,176G (22.5%, 1,176G)
waterwatereverywhere: 703G (13.4%, 703G)
Lanshaft: 340G (6.5%, 611G)
Cataphract116: 300G (5.7%, 33,660G)
Sn0bb: 200G (3.8%, 1,824G)
HeroponThrawn: 116G (2.2%, 7,716G)
AllInBot: 100G (1.9%, 100G)
FoeSquirrel: 100G (1.9%, 3,492G)
GSaucer: 100G (1.9%, 100G)
Treapvort: 100G (1.9%, 11,333G)

black bets:
Lydian_C: 1,500G (19.5%, 142,376G)
HaplessOne: 1,500G (19.5%, 111,054G)
ungabunga_bot: 1,000G (13.0%, 181,530G)
BirbBrainsBot: 1,000G (13.0%, 35,602G)
Slowbrofist: 637G (8.3%, 637G)
leakimiko: 543G (7.1%, 543G)
TheMurkGnome: 500G (6.5%, 17,864G)
getthemoneyz: 474G (6.2%, 524,214G)
roqqqpsi: 393G (5.1%, 1,787G)
mexskacin: 100G (1.3%, 5,952G)
DrAntiSocial: 40G (0.5%, 14,759G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Wurstwaesserchen
Female
Capricorn
65
67
Ninja
Steal
Abandon
Equip Armor
Retreat

Spell Edge
Kunai
Iron Helmet
Wizard Outfit
Chantage

Shuriken, Bomb
Steal Heart, Steal Shield



ALY327
Female
Scorpio
51
55
Mediator
Battle Skill
Sunken State
Equip Armor
Swim

Cute Bag

Leather Helmet
Secret Clothes
Bracer

Invitation, Persuade, Praise, Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute
Speed Break, Dark Sword



WireLord
Male
Aries
47
47
Ninja
Draw Out
Distribute
Secret Hunt
Move-HP Up

Flame Whip
Short Edge
Golden Hairpin
Mythril Vest
Germinas Boots

Shuriken, Bomb, Knife, Hammer, Wand, Dictionary
Murasame, Kiyomori, Muramasa



HaplessOne
Male
Sagittarius
79
45
Archer
Elemental
Meatbone Slash
Dual Wield
Levitate

Mythril Gun
Glacier Gun
Leather Hat
Clothes
Battle Boots

Charge+1, Charge+5, Charge+7
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball

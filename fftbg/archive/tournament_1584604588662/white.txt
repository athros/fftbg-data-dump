Player: !White
Team: White Team
Palettes: White/Blue



Scadooshlol
Male
Pisces
51
72
Summoner
Sing
Counter
Equip Axe
Lava Walking

Wizard Staff

Leather Hat
Light Robe
Spike Shoes

Moogle, Golem, Carbunkle, Salamander, Lich
Angel Song, Magic Song, Diamond Blade, Sky Demon



Norene
Female
Taurus
59
66
Wizard
Battle Skill
Caution
Sicken
Move+3

Blind Knife

Golden Hairpin
Brigandine
Elf Mantle

Fire 3, Bolt, Ice 3, Empower
Weapon Break, Stasis Sword, Night Sword



Lynkos
Male
Pisces
59
71
Ninja
Item
Parry
Concentrate
Teleport

Orichalcum
Orichalcum
Green Beret
Brigandine
N-Kai Armlet

Shuriken, Staff
Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Soft



Dody
Female
Virgo
67
69
Samurai
Black Magic
Brave Up
Dual Wield
Move-MP Up

Koutetsu Knife
Koutetsu Knife
Gold Helmet
Linen Cuirass
Wizard Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Fire 3, Bolt 3, Ice, Empower

Player: !White
Team: White Team
Palettes: White/Blue



Sinnyil2
Female
Scorpio
62
74
Time Mage
Draw Out
MP Restore
Short Status
Ignore Terrain

Battle Bamboo

Golden Hairpin
Linen Robe
Jade Armlet

Haste, Haste 2, Slow, Immobilize, Float, Quick
Koutetsu, Kiyomori, Muramasa, Kikuichimoji



Radical Psi
Monster
Taurus
57
74
Ahriman










Gelwain
Monster
Aries
69
54
Red Panther










Digitalsocrates
Male
Capricorn
77
74
Ninja
Steal
Counter Tackle
Equip Sword
Ignore Terrain

Mythril Sword
Rune Blade
Holy Miter
Power Sleeve
Small Mantle

Bomb, Knife, Sword, Spear
Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Arm Aim, Leg Aim

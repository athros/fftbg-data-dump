Player: !Red
Team: Red Team
Palettes: Red/Brown



Kyune
Male
Scorpio
80
54
Summoner
Jump
Brave Up
Beastmaster
Retreat

Flame Rod

Red Hood
Clothes
Sprint Shoes

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan, Silf, Fairy, Lich
Level Jump8, Vertical Jump8



Laserman1000
Male
Capricorn
55
44
Priest
Basic Skill
Brave Up
Short Charge
Waterbreathing

Scorpion Tail

Headgear
Clothes
Chantage

Cure 2, Cure 3, Raise, Raise 2, Reraise, Protect 2, Wall
Accumulate, Dash, Throw Stone, Heal



Lali Lulelo
Male
Pisces
74
51
Time Mage
Black Magic
Meatbone Slash
Martial Arts
Move+2

Rainbow Staff

Triangle Hat
Wizard Robe
Dracula Mantle

Haste, Stop, Reflect, Quick, Demi, Stabilize Time
Fire, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice, Empower, Frog



Smegma Sorcerer
Female
Leo
55
66
Wizard
Steal
Regenerator
Defend
Waterwalking

Mage Masher

Feather Hat
White Robe
Angel Ring

Fire 2, Bolt 2, Bolt 4, Ice, Ice 4, Empower
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Arm Aim

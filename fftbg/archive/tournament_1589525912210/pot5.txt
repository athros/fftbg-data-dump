Final Bets: red - 13 bets for 6,670G (47.8%, x1.09); yellow - 9 bets for 7,289G (52.2%, x0.92)

red bets:
ungabunga_bot: 1,000G (15.0%, 495,658G)
BirbBrainsBot: 1,000G (15.0%, 67,926G)
Mesmaster: 1,000G (15.0%, 33,914G)
Lolprinze: 991G (14.9%, 991G)
thethorndog1: 501G (7.5%, 501G)
reinoe: 500G (7.5%, 500G)
utopia_games: 500G (7.5%, 8,212G)
twelfthrootoftwo: 300G (4.5%, 6,488G)
getthemoneyz: 278G (4.2%, 669,951G)
otakutaylor: 200G (3.0%, 2,200G)
Digitalsocrates: 200G (3.0%, 1,047G)
Kyune: 100G (1.5%, 1,517G)
Conome: 100G (1.5%, 9,316G)

yellow bets:
metagameface: 3,000G (41.2%, 21,680G)
BuffaloCrunch: 1,056G (14.5%, 1,056G)
Lionhermit: 1,000G (13.7%, 51,013G)
ko2q: 967G (13.3%, 967G)
pandasforsale: 666G (9.1%, 12,412G)
Strifu: 300G (4.1%, 2,271G)
victoriolue: 100G (1.4%, 5,448G)
datadrivenbot: 100G (1.4%, 18,700G)
CapnChaos12: 100G (1.4%, 1,025G)

Final Bets: white - 7 bets for 2,385G (30.3%, x2.30); black - 11 bets for 5,491G (69.7%, x0.43)

white bets:
BirbBrainsBot: 1,000G (41.9%, 10,633G)
TheChainNerd: 524G (22.0%, 524G)
getthemoneyz: 260G (10.9%, 1,057,812G)
gorgewall: 201G (8.4%, 24,254G)
Evewho: 200G (8.4%, 7,533G)
nifboy: 100G (4.2%, 6,091G)
serperemagus: 100G (4.2%, 9,276G)

black bets:
dogsandcatsand: 1,074G (19.6%, 1,074G)
Draconis345: 1,000G (18.2%, 53,391G)
E_Ballard: 652G (11.9%, 652G)
DustBirdEX: 628G (11.4%, 628G)
rechaun: 528G (9.6%, 528G)
Zagorsek: 481G (8.8%, 481G)
JumbocactuarX27: 428G (7.8%, 428G)
CosmicTactician: 300G (5.5%, 21,412G)
demidusk: 200G (3.6%, 466G)
maakur_: 100G (1.8%, 77,269G)
datadrivenbot: 100G (1.8%, 49,081G)

Player: !Green
Team: Green Team
Palettes: Green/White



ANFz
Male
Aries
78
49
Oracle
White Magic
MP Restore
Long Status
Move-HP Up

Madlemgen

Leather Hat
Brigandine
Dracula Mantle

Pray Faith, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Petrify, Dark Holy
Cure 2, Raise



CapnChaos12
Male
Scorpio
80
77
Lancer
Throw
Catch
Maintenance
Move-MP Up

Dragon Whisker
Gold Shield
Gold Helmet
Light Robe
Feather Mantle

Level Jump8, Vertical Jump7
Bomb



TheRylander
Male
Aries
79
52
Priest
Draw Out
Absorb Used MP
Equip Sword
Jump+2

Heaven's Cloud

Holy Miter
Judo Outfit
Sprint Shoes

Cure, Cure 3, Raise, Regen, Protect, Shell, Shell 2, Esuna
Heaven's Cloud



B Bot
Female
Pisces
68
44
Calculator
Tentacle Skill
Brave Up
Concentrate
Move-HP Up

Papyrus Codex

Mythril Helmet
Mystic Vest
Rubber Shoes

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus

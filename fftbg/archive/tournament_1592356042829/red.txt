Player: !Red
Team: Red Team
Palettes: Red/Brown



HaychDub
Female
Virgo
46
67
Calculator
White Magic
Counter Flood
Short Charge
Jump+1

Dragon Rod

Feather Hat
Judo Outfit
Battle Boots

CT, Height, 3
Cure 4, Raise, Esuna



Cryptopsy70
Male
Taurus
68
53
Geomancer
Charge
Absorb Used MP
Equip Armor
Waterbreathing

Platinum Sword
Diamond Shield
Crystal Helmet
Earth Clothes
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+7, Charge+20



TheMM42
Female
Pisces
46
58
Ninja
Summon Magic
Distribute
Doublehand
Levitate

Kunai

Golden Hairpin
Leather Outfit
Germinas Boots

Shuriken, Bomb, Knife, Staff, Wand
Moogle, Shiva, Titan, Leviathan, Silf, Fairy



Dinin991
Female
Aries
41
48
Dancer
Basic Skill
Arrow Guard
Equip Polearm
Jump+2

Musk Rod

Holy Miter
Black Robe
Rubber Shoes

Witch Hunt, Last Dance
Throw Stone, Heal, Tickle, Fury, Wish

Player: !Red
Team: Red Team
Palettes: Red/Brown



WhiteTigress
Female
Pisces
51
58
Wizard
Throw
Brave Save
Beastmaster
Jump+1

Mage Masher

Red Hood
Adaman Vest
Reflect Ring

Fire, Fire 3, Fire 4, Bolt 3, Ice 4
Shuriken



NovaKnight21
Male
Gemini
47
53
Oracle
Basic Skill
Mana Shield
Martial Arts
Ignore Height



Red Hood
Light Robe
Defense Ring

Poison, Spell Absorb, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Dash, Throw Stone, Heal



DavenIII
Female
Pisces
69
44
Priest
Draw Out
Abandon
Defend
Levitate

Flame Whip

Golden Hairpin
Chameleon Robe
Magic Gauntlet

Cure, Cure 4, Raise, Regen, Protect 2, Shell, Shell 2, Esuna
Masamune



Skillomono
Male
Libra
56
57
Samurai
Time Magic
Critical Quick
Magic Defense UP
Move+2

Murasame

Bronze Helmet
Silk Robe
N-Kai Armlet

Heaven's Cloud, Kiyomori, Kikuichimoji, Chirijiraden
Haste 2, Float, Reflect, Quick, Demi 2, Galaxy Stop

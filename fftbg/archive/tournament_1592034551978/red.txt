Player: !Red
Team: Red Team
Palettes: Red/Brown



Reddwind
Male
Capricorn
78
54
Knight
Item
PA Save
Short Charge
Move+1

Long Sword
Diamond Shield
Crystal Helmet
Black Robe
Germinas Boots

Speed Break, Power Break, Surging Sword
Potion, Antidote, Maiden's Kiss, Soft, Phoenix Down



Zmoses
Male
Virgo
58
45
Monk
Throw
Counter Flood
Halve MP
Move-MP Up



Leather Hat
Leather Outfit
Diamond Armlet

Spin Fist, Pummel, Revive, Seal Evil
Shuriken, Knife



JRHiga
Monster
Capricorn
71
73
Serpentarius










ApplesauceBoss
Female
Leo
59
65
Mediator
Dance
Catch
Dual Wield
Move+3

Cute Bag
Hydra Bag
Green Beret
Power Sleeve
Germinas Boots

Invitation, Solution, Refute, Rehabilitate
Last Dance, Void Storage

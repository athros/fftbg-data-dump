Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Male
Capricorn
65
42
Bard
Summon Magic
Sunken State
Equip Armor
Move-MP Up

Fairy Harp

Diamond Helmet
Linen Cuirass
Feather Mantle

Cheer Song, Magic Song, Sky Demon, Hydra Pit
Moogle, Titan, Carbunkle, Salamander, Fairy



HaateXIII
Male
Gemini
59
46
Chemist
Yin Yang Magic
Auto Potion
Equip Axe
Swim

Healing Staff

Thief Hat
Wizard Outfit
Leather Mantle

Potion, X-Potion, Antidote, Eye Drop, Remedy, Phoenix Down
Blind, Life Drain, Pray Faith, Zombie, Silence Song, Foxbird



Bongomon7
Male
Sagittarius
46
73
Thief
Yin Yang Magic
Distribute
Dual Wield
Levitate

Main Gauche
Platinum Sword
Green Beret
Secret Clothes
Leather Mantle

Steal Armor, Steal Shield
Doubt Faith, Silence Song, Dispel Magic, Petrify



Spartan Paladin
Male
Pisces
80
44
Knight
Draw Out
Mana Shield
Halve MP
Ignore Height

Slasher
Genji Shield
Iron Helmet
Crystal Mail
Germinas Boots

Weapon Break, Magic Break, Power Break
Koutetsu, Heaven's Cloud

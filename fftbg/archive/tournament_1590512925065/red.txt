Player: !Red
Team: Red Team
Palettes: Red/Brown



Nifboy
Female
Capricorn
79
44
Lancer
Throw
MA Save
Equip Armor
Teleport

Octagon Rod
Ice Shield
Thief Hat
Secret Clothes
Feather Boots

Level Jump8, Vertical Jump4
Shuriken, Bomb



LDSkinny
Female
Aquarius
46
59
Oracle
Steal
Regenerator
Doublehand
Move+3

Battle Bamboo

Red Hood
Silk Robe
Germinas Boots

Blind, Poison, Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep
Steal Weapon, Steal Accessory, Steal Status



Kellios11
Male
Scorpio
68
47
Ninja
Summon Magic
Mana Shield
Equip Armor
Retreat

Scorpion Tail
Short Edge
Crystal Helmet
White Robe
N-Kai Armlet

Ninja Sword
Moogle, Ramuh, Titan, Leviathan, Silf



HaplessOne
Female
Aries
51
55
Summoner
Battle Skill
Sunken State
Short Status
Ignore Terrain

Ice Rod

Green Beret
White Robe
Salty Rage

Ramuh, Titan, Golem, Odin, Leviathan, Salamander, Silf, Fairy
Magic Break, Justice Sword, Surging Sword, Explosion Sword

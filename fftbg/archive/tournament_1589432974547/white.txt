Player: !White
Team: White Team
Palettes: White/Blue



ANFz
Male
Pisces
56
74
Squire
Jump
Meatbone Slash
Halve MP
Fly

Platinum Sword
Bronze Shield
Golden Hairpin
Leather Armor
Cursed Ring

Dash, Throw Stone, Heal, Fury, Scream
Level Jump8, Vertical Jump2



Mesmaster
Male
Virgo
76
75
Thief
Draw Out
Dragon Spirit
Defend
Move+2

Nagrarock

Triangle Hat
Rubber Costume
Leather Mantle

Gil Taking, Steal Helmet, Steal Weapon, Arm Aim, Leg Aim
Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji



Vorap
Female
Taurus
76
40
Wizard
Jump
Absorb Used MP
Concentrate
Jump+3

Flame Rod

Green Beret
Adaman Vest
Rubber Shoes

Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Ice 3, Empower
Level Jump8, Vertical Jump2



Leakimiko
Female
Leo
81
46
Calculator
Yin Yang Magic
Abandon
Equip Axe
Jump+3

White Staff

Red Hood
Power Sleeve
Germinas Boots

CT, 4, 3
Poison, Life Drain, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Dark Holy

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



JumbocactuarX27
Female
Virgo
67
71
Summoner
Throw
Mana Shield
Magic Attack UP
Lava Walking

Flame Rod

Red Hood
Robe of Lords
Magic Gauntlet

Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Fairy
Shuriken, Bomb, Knife



Evewho
Female
Aries
57
45
Geomancer
Draw Out
Earplug
Doublehand
Ignore Height

Murasame

Holy Miter
Wizard Robe
Leather Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa



Nifboy
Female
Taurus
57
65
Summoner
White Magic
HP Restore
Short Charge
Lava Walking

Ice Rod

Holy Miter
Silk Robe
Wizard Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle, Bahamut, Odin, Silf
Cure 3, Cure 4, Raise, Regen, Shell 2, Esuna



Smashy
Male
Sagittarius
65
60
Oracle
Draw Out
Speed Save
Magic Defense UP
Move+2

Musk Rod

Headgear
Robe of Lords
Small Mantle

Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep
Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji

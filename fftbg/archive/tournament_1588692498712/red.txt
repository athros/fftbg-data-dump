Player: !Red
Team: Red Team
Palettes: Red/Brown



Sinnyil2
Female
Pisces
77
59
Knight
White Magic
Sunken State
Defense UP
Jump+1

Mythril Sword
Gold Shield
Bronze Helmet
Linen Cuirass
Magic Gauntlet

Weapon Break, Speed Break, Justice Sword, Dark Sword
Cure 4, Raise, Protect, Shell 2, Wall, Esuna



Maakur
Male
Aries
65
70
Calculator
Time Magic
Dragon Spirit
Long Status
Move+2

Gokuu Rod

Leather Hat
White Robe
Setiemson

CT, Height, 5, 4, 3
Haste 2, Immobilize, Float, Reflect, Quick, Stabilize Time



Lawnboxer
Male
Cancer
76
74
Wizard
Item
Counter Flood
Secret Hunt
Move+3

Poison Rod

Thief Hat
Wizard Outfit
Germinas Boots

Fire, Fire 2, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 4, Empower
Hi-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water



CosmicTactician
Male
Pisces
63
75
Geomancer
Draw Out
Absorb Used MP
Sicken
Lava Walking

Coral Sword
Flame Shield
Twist Headband
Linen Robe
Setiemson

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Murasame

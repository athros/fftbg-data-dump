Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ZephyrTempest
Male
Capricorn
47
63
Geomancer
Throw
HP Restore
Martial Arts
Waterbreathing


Venetian Shield
Leather Hat
Chameleon Robe
Small Mantle

Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Shuriken, Sword, Dictionary



DavenIII
Male
Aquarius
46
58
Bard
Time Magic
Catch
Magic Attack UP
Move+2

Fairy Harp

Golden Hairpin
Plate Mail
Diamond Armlet

Battle Song, Space Storage, Hydra Pit
Haste, Slow, Reflect, Demi 2



Nosablake
Male
Aries
44
71
Archer
Item
Blade Grasp
Doublehand
Move-HP Up

Ice Bow

Headgear
Black Costume
Small Mantle

Charge+2, Charge+3, Charge+5, Charge+10
Potion, Antidote, Holy Water, Phoenix Down



J2DaBibbles
Female
Leo
78
54
Geomancer
Yin Yang Magic
Dragon Spirit
Equip Axe
Move-MP Up

Battle Axe
Round Shield
Headgear
Power Sleeve
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Poison, Pray Faith, Blind Rage, Confusion Song, Paralyze, Petrify, Dark Holy

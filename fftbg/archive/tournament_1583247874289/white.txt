Player: !White
Team: White Team
Palettes: White/Blue



MyFakeLife
Female
Serpentarius
43
45
Ninja
Black Magic
MA Save
Attack UP
Waterwalking

Air Knife
Flame Whip
Barette
Wizard Outfit
Chantage

Sword, Staff, Wand
Fire, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 4, Death



Seef2208
Male
Taurus
63
56
Calculator
White Magic
MA Save
Magic Defense UP
Retreat

Thunder Rod

Leather Hat
Wizard Robe
Red Shoes

CT, Height, 5, 3
Raise, Regen, Shell 2, Wall, Esuna, Holy



Hzor
Male
Aries
64
43
Geomancer
Jump
Dragon Spirit
Equip Polearm
Teleport

Spear
Buckler
Headgear
White Robe
Red Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Level Jump5, Vertical Jump8



Vapetrail
Male
Scorpio
59
47
Knight
Time Magic
Counter Flood
Halve MP
Waterbreathing

Battle Axe
Aegis Shield
Gold Helmet
Chain Mail
Wizard Mantle

Head Break, Armor Break, Mind Break, Stasis Sword, Dark Sword
Immobilize, Float, Demi 2

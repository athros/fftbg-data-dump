Player: !Red
Team: Red Team
Palettes: Red/Brown



Jethrothrul
Male
Gemini
61
45
Mediator
Charge
Faith Up
Dual Wield
Waterbreathing

Romanda Gun
Blast Gun
Feather Hat
Wizard Robe
Spike Shoes

Invitation, Threaten, Death Sentence, Negotiate, Mimic Daravon, Refute
Charge+1, Charge+5



ThePineappleSalesman
Female
Taurus
72
48
Wizard
Summon Magic
Counter Tackle
Magic Attack UP
Fly

Dragon Rod

Flash Hat
Black Costume
Reflect Ring

Fire, Fire 2, Fire 3, Bolt 2, Ice 3
Shiva, Carbunkle, Bahamut, Odin, Leviathan, Fairy



MantisFinch
Female
Cancer
75
69
Oracle
Item
Faith Up
Short Charge
Teleport

Iron Fan

Feather Hat
Mystic Vest
Reflect Ring

Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Potion, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



MyFakeLife
Female
Scorpio
67
44
Mime

Meatbone Slash
Monster Talk
Move-HP Up



Leather Helmet
Power Sleeve
Red Shoes

Mimic


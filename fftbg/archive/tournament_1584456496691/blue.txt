Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gabbagooluigi
Male
Sagittarius
43
66
Lancer
Basic Skill
Speed Save
Short Status
Move-MP Up

Mythril Spear
Mythril Shield
Platinum Helmet
Crystal Mail
Cursed Ring

Level Jump8, Vertical Jump8
Accumulate, Yell, Wish



Clippopo
Male
Aquarius
52
45
Monk
Elemental
Earplug
Equip Axe
Move+2

Giant Axe

Ribbon
Adaman Vest
Bracer

Purification, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Tapemeasure
Female
Scorpio
42
73
Calculator
Dragon Skill
Sunken State
Long Status
Retreat

Papyrus Codex

Headgear
Linen Cuirass
Angel Ring

Blue Magic
Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



Nizaha
Female
Aries
52
55
Samurai
Elemental
Auto Potion
Long Status
Teleport

Spear

Circlet
Genji Armor
Magic Gauntlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball

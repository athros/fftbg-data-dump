Player: !Green
Team: Green Team
Palettes: Green/White



Maakur
Male
Taurus
72
76
Squire
Black Magic
Absorb Used MP
Long Status
Jump+2

Mythril Sword
Crystal Shield
Holy Miter
Crystal Mail
Magic Gauntlet

Accumulate, Dash, Heal, Tickle, Fury
Fire, Fire 3, Bolt 3, Bolt 4, Ice



TheChainNerd
Male
Aries
58
73
Chemist
Time Magic
Absorb Used MP
Attack UP
Ignore Terrain

Cultist Dagger

Red Hood
Black Costume
Genji Gauntlet

Potion, X-Potion, Ether, Hi-Ether, Phoenix Down
Quick, Demi, Stabilize Time



Dexef
Male
Virgo
47
61
Lancer
Charge
Caution
Long Status
Move+2

Obelisk
Escutcheon
Diamond Helmet
Platinum Armor
Feather Mantle

Level Jump2, Vertical Jump8
Charge+1, Charge+10



Eldente
Female
Virgo
61
57
Calculator
Black Magic
Caution
Magic Attack UP
Move+2

Papyrus Codex

Twist Headband
Wizard Robe
Reflect Ring

CT, Height, Prime Number, 5, 4
Fire 4, Ice, Ice 3, Frog

Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Female
Scorpio
50
77
Oracle
Throw
Faith Save
Secret Hunt
Swim

Ivory Rod

Leather Hat
Earth Clothes
Vanish Mantle

Blind, Doubt Faith, Foxbird, Dispel Magic, Petrify
Staff



Phi Sig
Male
Capricorn
67
76
Thief
Jump
PA Save
Defense UP
Waterbreathing

Orichalcum

Golden Hairpin
Brigandine
Salty Rage

Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Leg Aim
Level Jump5, Vertical Jump6



Run With Stone GUNs
Monster
Virgo
70
59
Vampire










Grininda
Male
Cancer
67
44
Bard
Yin Yang Magic
Counter Tackle
Magic Attack UP
Jump+3

Ramia Harp

Leather Hat
Black Costume
Battle Boots

Angel Song, Life Song, Cheer Song
Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Paralyze, Sleep

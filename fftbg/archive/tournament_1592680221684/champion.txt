Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



VolgraTheMoose
Female
Leo
55
49
Wizard
Summon Magic
Damage Split
Equip Bow
Lava Walking

Silver Bow

Cachusha
Adaman Vest
Feather Mantle

Fire 3, Bolt, Bolt 2, Ice 3, Death
Ramuh, Bahamut, Leviathan, Lich



Shalloween
Male
Sagittarius
79
54
Samurai
Black Magic
Dragon Spirit
Halve MP
Waterbreathing

Bizen Boat

Circlet
Plate Mail
Spike Shoes

Muramasa, Kikuichimoji
Fire 2, Bolt, Bolt 2, Bolt 3, Frog



Bioticism
Male
Gemini
61
62
Bard
Item
Critical Quick
Throw Item
Jump+1

Yoichi Bow

Leather Hat
Diamond Armor
Angel Ring

Magic Song, Last Song
Potion, Hi-Potion, Ether, Hi-Ether, Remedy, Phoenix Down



Kaidykat
Monster
Capricorn
75
59
Archaic Demon








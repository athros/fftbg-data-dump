Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Leo
74
74
Priest
Punch Art
Regenerator
Short Charge
Move-MP Up

Rainbow Staff

Red Hood
Chameleon Robe
Angel Ring

Cure, Protect, Shell, Shell 2, Esuna
Wave Fist, Purification, Revive



Mirapoix
Female
Sagittarius
50
50
Ninja
Battle Skill
Mana Shield
Sicken
Swim

Spell Edge
Dagger
Leather Hat
Mythril Vest
Wizard Mantle

Bomb, Knife, Staff, Wand
Head Break, Armor Break, Magic Break, Speed Break, Surging Sword



SkylerBunny
Male
Libra
81
76
Squire
Yin Yang Magic
Meatbone Slash
Sicken
Jump+3

Platinum Sword
Gold Shield
Golden Hairpin
Mythril Armor
108 Gems

Dash, Tickle, Cheer Up
Blind, Confusion Song, Dispel Magic



RunicMagus
Female
Aquarius
46
80
Squire
Black Magic
Brave Up
Maintenance
Waterbreathing

Dagger
Aegis Shield
Headgear
Reflect Mail
Diamond Armlet

Accumulate, Heal, Yell, Cheer Up
Fire 3, Bolt, Ice 3, Frog

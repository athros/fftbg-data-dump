Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Pisces
42
60
Lancer
Battle Skill
Counter Tackle
Equip Knife
Waterwalking

Kunai
Flame Shield
Cross Helmet
Reflect Mail
Reflect Ring

Level Jump2, Vertical Jump6
Weapon Break, Speed Break, Mind Break, Surging Sword



Baconbacon1207
Female
Capricorn
75
50
Mediator
Basic Skill
HP Restore
Beastmaster
Ignore Terrain

Romanda Gun

Headgear
Chain Vest
Magic Ring

Invitation, Persuade, Praise, Preach, Insult, Negotiate, Rehabilitate
Accumulate, Tickle, Fury



Gelwain
Female
Scorpio
69
50
Wizard
Dance
Hamedo
Equip Axe
Fly

Rainbow Staff

Golden Hairpin
Earth Clothes
Magic Gauntlet

Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Death
Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Last Dance, Obsidian Blade, Void Storage



Meta Five
Female
Taurus
59
78
Archer
Basic Skill
Blade Grasp
Halve MP
Ignore Height

Blaze Gun
Mythril Shield
Holy Miter
Brigandine
Jade Armlet

Charge+1, Charge+4, Charge+20
Throw Stone, Yell, Cheer Up, Wish, Ultima

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Helpimabug
Female
Virgo
65
70
Monk
Black Magic
MP Restore
Short Status
Move-MP Up



Triangle Hat
Wizard Outfit
Feather Mantle

Purification, Revive
Bolt 4, Ice 2, Empower



PatSouI
Male
Cancer
63
43
Mediator
Item
Dragon Spirit
Dual Wield
Move+3

Papyrus Codex
Madlemgen
Feather Hat
Black Robe
Small Mantle

Invitation, Praise, Preach, Solution, Insult, Negotiate, Mimic Daravon
X-Potion, Holy Water, Phoenix Down



Anox Skell
Female
Taurus
64
47
Priest
Time Magic
MA Save
Beastmaster
Jump+2

Flail

Leather Hat
Linen Robe
Wizard Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell, Esuna, Holy
Haste 2, Slow, Quick, Stabilize Time, Meteor



Waterwatereverywhere
Male
Leo
44
69
Calculator
Time Magic
MA Save
Attack UP
Waterbreathing

Poison Rod

Feather Hat
Light Robe
Feather Mantle

CT, Height, Prime Number, 5, 4
Slow, Slow 2, Immobilize, Float, Stabilize Time, Meteor

Player: !Black
Team: Black Team
Palettes: Black/Red



Defaultlybrave
Male
Scorpio
67
55
Mediator
Time Magic
Speed Save
Equip Knife
Teleport

Blind Knife

Red Hood
Chain Vest
Small Mantle

Invitation, Persuade, Praise, Preach, Solution, Insult, Refute
Haste, Slow 2, Stop, Float, Quick, Demi, Meteor



Ruebyy
Male
Gemini
61
52
Calculator
Yin Yang Magic
Catch
Attack UP
Teleport

Dragon Rod

Holy Miter
Wizard Robe
Defense Ring

CT, Height, Prime Number, 3
Blind, Life Drain, Pray Faith, Silence Song, Confusion Song



CosmicTactician
Male
Capricorn
50
71
Bard
Yin Yang Magic
Absorb Used MP
Doublehand
Move-MP Up

Bloody Strings

Twist Headband
Linen Cuirass
Angel Ring

Battle Song, Sky Demon
Spell Absorb, Doubt Faith, Zombie, Silence Song, Foxbird, Petrify



Grininda
Male
Capricorn
66
58
Thief
Item
Caution
Throw Item
Ignore Height

Blind Knife

Flash Hat
Mythril Vest
Reflect Ring

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Potion, Soft, Holy Water, Remedy

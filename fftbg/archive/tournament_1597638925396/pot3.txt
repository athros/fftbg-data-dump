Final Bets: white - 12 bets for 22,373G (80.5%, x0.24); black - 7 bets for 5,430G (19.5%, x4.12)

white bets:
omegasuspekt: 11,090G (49.6%, 22,181G)
Firesheath: 4,449G (19.9%, 4,449G)
silentkaster: 1,500G (6.7%, 7,545G)
DLJuggernaut: 1,251G (5.6%, 2,453G)
nhammen: 1,000G (4.5%, 36,277G)
SkylerBunny: 736G (3.3%, 736G)
iBardic: 600G (2.7%, 4,601G)
AllInBot: 594G (2.7%, 594G)
cheapchristmaslights: 500G (2.2%, 2,222G)
brando84back: 300G (1.3%, 1,553G)
Lifebregin: 253G (1.1%, 1,453G)
ko2q: 100G (0.4%, 1,288G)

black bets:
lowlf: 1,464G (27.0%, 1,464G)
toka222: 1,200G (22.1%, 110,164G)
Laserman1000: 868G (16.0%, 868G)
BirbBrainsBot: 654G (12.0%, 167,450G)
reddwind_: 588G (10.8%, 20,947G)
SeniorBunk: 500G (9.2%, 7,420G)
getthemoneyz: 156G (2.9%, 1,655,144G)

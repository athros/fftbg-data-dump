Player: !Green
Team: Green Team
Palettes: Green/White



Grininda
Male
Taurus
76
71
Geomancer
Draw Out
Counter Flood
Equip Sword
Lava Walking

Defender

Black Hood
Chameleon Robe
Spike Shoes

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand
Koutetsu, Murasame, Kiyomori



Ar Tactic
Male
Libra
68
71
Summoner
Time Magic
Abandon
Equip Sword
Move-MP Up

Defender

Holy Miter
Mythril Vest
Bracer

Moogle, Ramuh, Bahamut, Odin, Leviathan, Salamander, Silf, Lich
Demi, Stabilize Time



Peeronid
Female
Pisces
57
61
Wizard
Math Skill
Damage Split
Equip Knife
Swim

Orichalcum

Green Beret
Wizard Robe
108 Gems

Fire 3, Bolt 3, Bolt 4, Ice 2, Ice 3
CT, 5, 3



ShintaroNayaka
Male
Cancer
66
50
Knight
Punch Art
Damage Split
Defense UP
Levitate

Broad Sword
Diamond Shield
Platinum Helmet
White Robe
Bracer

Head Break, Speed Break, Night Sword
Earth Slash, Purification, Revive, Seal Evil

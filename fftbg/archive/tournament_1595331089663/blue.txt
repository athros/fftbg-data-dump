Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DaveStrider55
Male
Cancer
67
57
Geomancer
Draw Out
Abandon
Equip Knife
Jump+1

Air Knife
Flame Shield
Golden Hairpin
White Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind
Murasame



Fluffskull
Male
Aries
36
52
Samurai
Throw
Meatbone Slash
Equip Sword
Levitate

Long Sword

Iron Helmet
Linen Robe
Diamond Armlet

Asura, Kiyomori, Kikuichimoji
Shuriken, Staff



Zetchryn
Female
Pisces
65
58
Dancer
Talk Skill
Hamedo
Equip Axe
Jump+3

Flame Whip

Holy Miter
Black Robe
Bracer

Wiznaibus, Polka Polka, Nameless Dance, Dragon Pit
Mimic Daravon, Rehabilitate



Dogsandcatsand
Female
Sagittarius
80
57
Squire
Punch Art
Damage Split
Attack UP
Teleport

Bow Gun
Platinum Shield
Triangle Hat
Crystal Mail
Rubber Shoes

Accumulate, Throw Stone, Heal, Fury, Scream
Pummel, Earth Slash, Purification, Chakra, Revive

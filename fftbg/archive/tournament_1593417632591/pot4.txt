Final Bets: purple - 11 bets for 4,957G (44.4%, x1.25); brown - 9 bets for 6,214G (55.6%, x0.80)

purple bets:
SkylerBunny: 1,400G (28.2%, 509,027G)
Chuckolator: 1,143G (23.1%, 120,864G)
serperemagus: 568G (11.5%, 568G)
Spuzzmocker: 428G (8.6%, 428G)
Lemonjohns: 414G (8.4%, 414G)
superdevon1: 332G (6.7%, 332G)
SuzakuReii: 200G (4.0%, 4,603G)
blorpy_: 172G (3.5%, 172G)
Ring_Wyrm: 100G (2.0%, 2,362G)
Armorgames1: 100G (2.0%, 1,317G)
datadrivenbot: 100G (2.0%, 47,742G)

brown bets:
UmaiJam: 1,200G (19.3%, 29,945G)
lowlf: 1,111G (17.9%, 93,308G)
BirbBrainsBot: 1,000G (16.1%, 117,673G)
escobro: 825G (13.3%, 825G)
prince_rogers_nelson_: 600G (9.7%, 600G)
getthemoneyz: 578G (9.3%, 1,106,564G)
Phytik: 352G (5.7%, 352G)
twelfthrootoftwo: 300G (4.8%, 17,767G)
placidphoenix: 248G (4.0%, 248G)

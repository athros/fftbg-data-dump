Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Mesmaster
Female
Capricorn
59
76
Wizard
White Magic
Speed Save
Short Charge
Move+2

Orichalcum

Leather Hat
Chain Vest
Dracula Mantle

Fire, Fire 3, Fire 4, Bolt 2, Ice, Ice 3, Frog, Death, Flare
Cure 2, Raise, Regen, Shell



ZZ Yoshi
Female
Aquarius
70
71
Summoner
Item
Counter Tackle
Short Charge
Waterwalking

Healing Staff

Black Hood
Clothes
Defense Ring

Shiva, Ramuh, Titan, Carbunkle, Odin, Leviathan, Salamander, Silf, Fairy, Lich
Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Twelfthrootoftwo
Female
Aquarius
60
78
Wizard
Throw
Counter Flood
Short Charge
Jump+1

Poison Rod

Twist Headband
Wizard Robe
Angel Ring

Fire, Fire 4, Bolt 4, Ice 2, Ice 4
Shuriken, Bomb, Staff



Sans From Snowdin
Male
Virgo
72
52
Mime

Auto Potion
Martial Arts
Waterbreathing



Triangle Hat
Mythril Vest
Reflect Ring

Mimic


Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Female
Leo
68
44
Priest
Jump
Earplug
Beastmaster
Ignore Terrain

Gold Staff

Leather Hat
Black Costume
Small Mantle

Cure, Raise, Reraise, Regen, Protect, Shell, Esuna
Level Jump8, Vertical Jump6



Enkikavlar
Female
Virgo
59
65
Knight
Item
Parry
Equip Sword
Waterwalking

Chaos Blade
Aegis Shield
Leather Helmet
Platinum Armor
Magic Ring

Armor Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Dark Sword
Potion, X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down



ChubbyBroCast
Male
Leo
72
57
Calculator
Black Magic
Counter Tackle
Short Status
Move+2

Bestiary

Triangle Hat
Power Sleeve
Feather Mantle

CT, Height, Prime Number, 5
Bolt 4, Ice 2



VolgraTheMoose
Female
Sagittarius
55
43
Wizard
Math Skill
MP Restore
Equip Armor
Fly

Poison Rod

Barbuta
Wizard Outfit
Small Mantle

Fire, Bolt, Bolt 2, Bolt 3
Height, Prime Number, 5, 4, 3

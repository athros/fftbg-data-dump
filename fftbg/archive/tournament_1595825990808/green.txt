Player: !Green
Team: Green Team
Palettes: Green/White



Ruleof5
Female
Aries
53
77
Ninja
Battle Skill
Distribute
Equip Bow
Fly

Windslash Bow

Barette
Adaman Vest
Feather Boots

Shuriken, Knife
Head Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Dark Sword



DAC169
Male
Virgo
56
63
Knight
Summon Magic
Sunken State
Short Charge
Move+1

Slasher
Genji Shield
Leather Helmet
Maximillian
Rubber Shoes

Head Break, Weapon Break
Moogle, Ramuh, Carbunkle, Silf



Reddwind
Male
Aquarius
74
55
Knight
Basic Skill
Catch
Defend
Move+2

Defender
Mythril Shield
Platinum Helmet
Chameleon Robe
Chantage

Armor Break, Shield Break, Weapon Break, Dark Sword
Heal



Hasterious
Male
Aries
44
44
Ninja
Yin Yang Magic
Sunken State
Martial Arts
Move+2



Thief Hat
Leather Outfit
Defense Ring

Shuriken
Blind, Poison, Pray Faith, Doubt Faith, Silence Song, Confusion Song, Petrify

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Hasterious
Male
Scorpio
55
76
Lancer
Throw
Parry
Equip Sword
Ignore Height

Save the Queen

Iron Helmet
Maximillian
Bracer

Level Jump3, Vertical Jump7
Shuriken, Knife, Spear, Stick



Thaetreis
Female
Virgo
49
64
Chemist
Time Magic
Counter Tackle
Magic Defense UP
Move-HP Up

Blast Gun

Twist Headband
Power Sleeve
Angel Ring

Potion, Hi-Potion, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Haste 2, Immobilize, Reflect, Quick, Stabilize Time



Laserman1000
Male
Sagittarius
80
43
Monk
Battle Skill
Hamedo
Equip Gun
Ignore Terrain

Battle Folio

Black Hood
Black Costume
Germinas Boots

Spin Fist, Wave Fist, Revive, Seal Evil
Head Break



Butterbelljedi
Female
Scorpio
40
55
Mime

Counter
Maintenance
Move+3



Genji Helmet
Bronze Armor
Spike Shoes

Mimic


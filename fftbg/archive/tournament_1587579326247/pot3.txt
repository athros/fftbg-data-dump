Final Bets: white - 18 bets for 13,549G (71.6%, x0.40); black - 15 bets for 5,370G (28.4%, x2.52)

white bets:
Zeroroute: 4,203G (31.0%, 7,813G)
toka222: 1,500G (11.1%, 4,848G)
HaateXIII: 1,500G (11.1%, 15,508G)
ungabunga_bot: 1,000G (7.4%, 211,816G)
BirbBrainsBot: 1,000G (7.4%, 135,673G)
JumbocactuarX27: 1,000G (7.4%, 10,673G)
DavenIII: 700G (5.2%, 3,648G)
getthemoneyz: 514G (3.8%, 554,488G)
Ethquan: 400G (3.0%, 6,843G)
EnemyController: 400G (3.0%, 75,859G)
Cryptopsy70: 372G (2.7%, 25,380G)
placeholdercats: 300G (2.2%, 345G)
portmanteur: 285G (2.1%, 2,853G)
AllInBot: 100G (0.7%, 100G)
Tougou: 100G (0.7%, 8,466G)
Sans_from_Snowdin: 100G (0.7%, 4,448G)
xYungAnarchyx: 50G (0.4%, 252G)
GingerDynomite: 25G (0.2%, 5,705G)

black bets:
kai_shee: 2,000G (37.2%, 140,290G)
Pie108: 1,000G (18.6%, 73,537G)
SerumD: 500G (9.3%, 2,092G)
Anomandaris__: 500G (9.3%, 8,527G)
PetitFoulard: 356G (6.6%, 356G)
old_overholt_: 200G (3.7%, 2,530G)
marin1987: 200G (3.7%, 2,258G)
Heroebal: 100G (1.9%, 5,727G)
IndecisiveNinja: 100G (1.9%, 89,701G)
trigger15: 100G (1.9%, 100G)
Firesheath: 100G (1.9%, 9,969G)
Sentwist: 100G (1.9%, 619G)
b0shii: 50G (0.9%, 5,363G)
DrAntiSocial: 50G (0.9%, 22,368G)
roqqqpsi: 14G (0.3%, 739G)

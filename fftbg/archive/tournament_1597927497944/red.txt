Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Female
Cancer
66
42
Geomancer
Yin Yang Magic
Abandon
Magic Attack UP
Fly

Diamond Sword
Genji Shield
Feather Hat
Light Robe
Red Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Blind, Pray Faith, Blind Rage, Confusion Song, Dispel Magic



Qaz12301
Female
Virgo
60
65
Priest
Time Magic
Counter
Equip Sword
Lava Walking

Koutetsu Knife

Twist Headband
Mystic Vest
Power Wrist

Cure 2, Shell 2, Magic Barrier
Haste, Slow, Reflect, Demi, Stabilize Time



LDSkinny
Female
Scorpio
59
60
Oracle
Charge
Earplug
Equip Shield
Levitate

Battle Folio
Kaiser Plate
Golden Hairpin
White Robe
Magic Ring

Blind, Poison, Pray Faith, Foxbird, Dispel Magic, Paralyze, Sleep
Charge+1



Ko2q
Female
Virgo
53
47
Oracle
White Magic
Hamedo
Short Status
Lava Walking

Gokuu Rod

Leather Hat
Chameleon Robe
Power Wrist

Life Drain, Blind Rage, Confusion Song, Dispel Magic
Cure, Cure 2, Cure 3, Raise, Reraise, Protect 2, Esuna

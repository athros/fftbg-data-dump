Player: !White
Team: White Team
Palettes: White/Blue



StealthModeLocke
Female
Capricorn
59
80
Ninja
Dance
Damage Split
Sicken
Move+1

Sasuke Knife
Scorpion Tail
Feather Hat
Leather Outfit
Sprint Shoes

Wand
Witch Hunt, Wiznaibus, Slow Dance, Void Storage, Dragon Pit



Bongomon7
Female
Taurus
48
56
Priest
Steal
Critical Quick
Beastmaster
Jump+3

Morning Star

Triangle Hat
Light Robe
Feather Mantle

Cure, Cure 2, Regen, Protect, Protect 2, Shell 2, Esuna
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Accessory



Lazarus DS
Female
Capricorn
65
47
Priest
Battle Skill
Regenerator
Equip Sword
Retreat

Asura Knife

Black Hood
Black Costume
Spike Shoes

Cure, Cure 2, Protect, Shell, Esuna
Head Break, Shield Break, Mind Break, Dark Sword



Ar Tactic
Male
Aries
80
57
Ninja
White Magic
Distribute
Short Charge
Jump+3

Orichalcum
Mythril Knife
Twist Headband
Adaman Vest
Sprint Shoes

Shuriken, Dictionary
Cure, Raise, Raise 2, Shell, Esuna

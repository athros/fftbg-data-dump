Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Male
Libra
77
51
Squire
Item
Regenerator
Throw Item
Lava Walking

Mage Masher
Platinum Shield
Platinum Helmet
Carabini Mail
Defense Ring

Accumulate, Dash, Yell
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Remedy



DudeMonkey77
Male
Libra
69
52
Oracle
Talk Skill
Faith Save
Short Status
Lava Walking

Ivory Rod

Feather Hat
Chameleon Robe
Feather Boots

Zombie, Silence Song, Foxbird, Confusion Song, Paralyze, Sleep, Dark Holy
Solution, Insult, Refute, Rehabilitate



TheMM42
Female
Cancer
43
65
Calculator
Yin Yang Magic
Counter Flood
Maintenance
Jump+3

Star Bag

Triangle Hat
Judo Outfit
Wizard Mantle

Height, Prime Number
Life Drain, Doubt Faith, Dispel Magic, Sleep, Petrify



EmperorBeef
Female
Aquarius
50
73
Lancer
Punch Art
Counter
Equip Armor
Move+3

Javelin
Bronze Shield
Red Hood
Clothes
Leather Mantle

Level Jump8, Vertical Jump3
Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil

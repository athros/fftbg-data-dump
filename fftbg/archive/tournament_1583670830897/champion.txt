Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Hi Pewpew
Male
Cancer
80
66
Geomancer
Steal
Auto Potion
Secret Hunt
Teleport

Masamune
Genji Shield
Flash Hat
Linen Robe
N-Kai Armlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Steal Weapon, Steal Status, Arm Aim



Ominnous
Female
Pisces
53
63
Summoner
Talk Skill
Auto Potion
Short Charge
Fly

Thunder Rod

Holy Miter
Linen Robe
Diamond Armlet

Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Salamander
Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Mimic Daravon, Refute



Kazanyr
Female
Leo
58
76
Thief
Draw Out
Regenerator
Attack UP
Jump+3

Broad Sword

Black Hood
Clothes
Elf Mantle

Steal Helmet, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Asura, Koutetsu, Kikuichimoji



Hzor
Male
Capricorn
54
68
Ninja
Basic Skill
Mana Shield
Equip Bow
Teleport

Bow Gun
Bow Gun
Holy Miter
Adaman Vest
Small Mantle

Shuriken, Axe, Spear, Wand
Accumulate, Throw Stone, Heal, Cheer Up

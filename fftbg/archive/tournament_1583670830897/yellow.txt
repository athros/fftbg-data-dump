Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Aldrammech
Male
Taurus
63
50
Oracle
Talk Skill
Parry
Monster Talk
Jump+1

Musk Rod

Cachusha
Judo Outfit
Small Mantle

Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic
Invitation, Praise, Threaten, Mimic Daravon, Refute, Rehabilitate



Treapvort
Male
Libra
49
70
Ninja
Draw Out
Auto Potion
Equip Bow
Waterbreathing

Poison Bow
Cross Bow
Feather Hat
Brigandine
Rubber Shoes

Shuriken, Knife, Ninja Sword, Stick, Dictionary
Asura, Koutetsu, Kiyomori



UmaiJam
Female
Libra
66
48
Dancer
Steal
MP Restore
Magic Defense UP
Retreat

Ryozan Silk

Golden Hairpin
Silk Robe
Power Wrist

Witch Hunt, Wiznaibus, Void Storage, Nether Demon
Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status



Z32o
Male
Pisces
75
76
Chemist
Throw
Mana Shield
Short Status
Waterbreathing

Cute Bag

Green Beret
Adaman Vest
Vanish Mantle

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Shuriken, Bomb, Knife, Hammer

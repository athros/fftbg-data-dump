Player: !Black
Team: Black Team
Palettes: Black/Red



Oogthecaveman
Female
Aquarius
69
62
Summoner
Charge
Damage Split
Maintenance
Retreat

Wizard Rod

Barette
White Robe
Magic Gauntlet

Moogle, Titan, Golem, Bahamut, Leviathan, Salamander, Fairy, Lich
Charge+1, Charge+3, Charge+5, Charge+7, Charge+10



Dynasti
Monster
Gemini
50
68
Red Chocobo










Goust18
Male
Aquarius
59
42
Ninja
Talk Skill
Sunken State
Equip Sword
Move-HP Up

Excalibur
Ancient Sword
Black Hood
Adaman Vest
Red Shoes

Shuriken, Bomb, Sword, Staff, Stick
Invitation, Preach, Refute



RongRongArts
Male
Gemini
67
59
Knight
Summon Magic
Dragon Spirit
Long Status
Jump+1

Sleep Sword
Crystal Shield
Circlet
Chameleon Robe
Sprint Shoes

Weapon Break, Magic Break, Justice Sword
Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander, Silf, Fairy

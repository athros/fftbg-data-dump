Player: !zChamp
Team: Champion Team
Palettes: Green/White



Firesheath
Female
Capricorn
67
38
Lancer
Talk Skill
Arrow Guard
Monster Talk
Levitate

Obelisk
Bronze Shield
Genji Helmet
Black Robe
Sprint Shoes

Level Jump4, Vertical Jump8
Solution, Death Sentence, Mimic Daravon, Refute



Heroebal
Female
Aquarius
67
76
Wizard
Time Magic
Sunken State
Equip Shield
Jump+1

Rod
Genji Shield
Headgear
Silk Robe
N-Kai Armlet

Fire 4, Bolt 3, Ice, Ice 4
Haste 2, Slow, Reflect, Quick



ANFz
Male
Virgo
60
64
Chemist
Yin Yang Magic
Faith Up
Equip Shield
Jump+2

Hydra Bag
Platinum Shield
Flash Hat
Adaman Vest
Diamond Armlet

Potion, Hi-Ether, Holy Water, Phoenix Down
Blind, Zombie, Sleep



Setdevildog
Male
Pisces
71
41
Ninja
Sing
Dragon Spirit
Equip Armor
Teleport

Mage Masher
Dagger
Flash Hat
White Robe
Defense Armlet

Axe
Sky Demon

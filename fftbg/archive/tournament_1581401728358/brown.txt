Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Neetneph
Monster
Virgo
56
75
Plague










Nachtkrieger
Male
Capricorn
48
78
Monk
Time Magic
Earplug
Magic Defense UP
Jump+1



Feather Hat
Chain Vest
Feather Mantle

Spin Fist, Pummel, Secret Fist, Purification, Revive
Haste 2, Slow, Quick, Demi, Demi 2



Jethrothrul
Female
Leo
61
43
Samurai
Punch Art
Critical Quick
Dual Wield
Jump+1

Spear
Javelin
Leather Helmet
Mythril Armor
Germinas Boots

Koutetsu, Bizen Boat, Murasame, Muramasa
Spin Fist, Pummel, Purification, Chakra, Revive



RadiantHex
Female
Capricorn
56
49
Time Mage
Summon Magic
Caution
Magic Attack UP
Ignore Terrain

Battle Folio

Leather Hat
White Robe
Angel Ring

Haste 2, Slow, Demi, Stabilize Time
Shiva, Ifrit, Carbunkle, Leviathan, Salamander

Final Bets: white - 8 bets for 2,824G (71.0%, x0.41); brown - 3 bets for 1,156G (29.0%, x2.44)

white bets:
roqqqpsi: 867G (30.7%, 2,550G)
gorgewall: 860G (30.5%, 860G)
PuzzleSecretary: 263G (9.3%, 263G)
BirbBrainsBot: 228G (8.1%, 118,246G)
InOzWeTrust: 200G (7.1%, 200G)
datadrivenbot: 200G (7.1%, 61,744G)
getthemoneyz: 106G (3.8%, 1,702,820G)
AllInBot: 100G (3.5%, 100G)

brown bets:
skipsandwiches: 688G (59.5%, 688G)
DouglasDragonThePoet: 268G (23.2%, 268G)
Lazarus_DS: 200G (17.3%, 435G)

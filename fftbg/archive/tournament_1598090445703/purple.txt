Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



E Ballard
Female
Aquarius
69
60
Thief
Charge
Parry
Magic Defense UP
Jump+2

Rune Blade

Leather Hat
Clothes
Salty Rage

Steal Heart, Steal Armor, Steal Accessory, Leg Aim
Charge+2, Charge+5, Charge+20



Ko2q
Male
Taurus
47
63
Samurai
Basic Skill
Caution
Beastmaster
Move+1

Murasame

Crystal Helmet
Black Robe
Rubber Shoes

Asura, Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji
Heal, Tickle, Yell, Cheer Up, Wish, Scream



WhiteTigress
Female
Cancer
49
70
Priest
Talk Skill
Distribute
Equip Shield
Levitate

Flail
Diamond Shield
Black Hood
Black Robe
Feather Mantle

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Preach, Death Sentence, Mimic Daravon, Refute



Archon
Male
Serpentarius
66
60
Priest
Battle Skill
Mana Shield
Halve MP
Jump+1

Oak Staff

Red Hood
Judo Outfit
Sprint Shoes

Cure 2, Raise, Raise 2, Regen, Shell, Esuna
Shield Break, Power Break, Dark Sword, Explosion Sword

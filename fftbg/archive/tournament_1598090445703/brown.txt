Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DAC169
Male
Taurus
38
67
Calculator
Bio
Meatbone Slash
Equip Gun
Move+1

Glacier Gun
Gold Shield
Mythril Helmet
Power Sleeve
Reflect Ring

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



TheMM42
Male
Gemini
76
54
Lancer
Battle Skill
Faith Save
Equip Polearm
Move+2

Spear
Ice Shield
Leather Helmet
Platinum Armor
Defense Armlet

Level Jump5, Vertical Jump4
Armor Break, Shield Break, Power Break



NovaKnight21
Female
Gemini
47
80
Chemist
Time Magic
Damage Split
Sicken
Jump+1

Blast Gun

Feather Hat
Judo Outfit
Sprint Shoes

Potion, Hi-Potion, Hi-Ether, Holy Water, Phoenix Down
Haste 2, Slow 2, Reflect



Reinoe
Female
Sagittarius
58
46
Oracle
Summon Magic
Absorb Used MP
Equip Shield
Waterwalking

Cypress Rod
Bronze Shield
Barette
Silk Robe
Spike Shoes

Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Dispel Magic
Moogle, Ramuh, Carbunkle, Odin, Salamander, Fairy

Final Bets: white - 8 bets for 6,273G (52.6%, x0.90); black - 3 bets for 5,656G (47.4%, x1.11)

white bets:
VolgraTheMoose: 2,563G (40.9%, 5,126G)
douchetron: 1,160G (18.5%, 2,321G)
UmaiJam: 1,000G (15.9%, 50,364G)
BirbBrainsBot: 1,000G (15.9%, 184,987G)
AllInBot: 200G (3.2%, 200G)
datadrivenbot: 200G (3.2%, 30,619G)
Error72: 100G (1.6%, 2,911G)
DouglasDragonThePoet: 50G (0.8%, 505G)

black bets:
Lydian_C: 4,200G (74.3%, 75,291G)
YaBoy125: 1,000G (17.7%, 1,000G)
randgridr: 456G (8.1%, 3,122G)

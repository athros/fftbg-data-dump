Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Thyrandaal
Male
Capricorn
44
64
Mime

Meatbone Slash
Magic Attack UP
Move-MP Up



Flash Hat
Genji Armor
Genji Gauntlet

Mimic




Kronikle
Male
Virgo
69
54
Monk
Elemental
Auto Potion
Attack UP
Teleport



Flash Hat
Power Sleeve
Reflect Ring

Earth Slash, Purification, Chakra, Revive
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Victoriolue
Male
Virgo
63
80
Archer
Yin Yang Magic
Sunken State
Doublehand
Move-MP Up

Long Bow

Platinum Helmet
Clothes
Magic Ring

Charge+3, Charge+10, Charge+20
Life Drain, Silence Song, Paralyze



Zenlion
Male
Aquarius
74
48
Priest
Summon Magic
Mana Shield
Magic Attack UP
Jump+3

Oak Staff

Leather Hat
Mythril Vest
Leather Mantle

Cure 2, Cure 3, Cure 4, Raise 2, Reraise, Protect 2, Shell 2, Esuna
Ramuh, Ifrit, Titan, Leviathan, Salamander, Silf

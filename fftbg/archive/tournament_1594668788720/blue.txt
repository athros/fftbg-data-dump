Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TimeJannies
Female
Libra
41
75
Priest
Black Magic
HP Restore
Martial Arts
Lava Walking



Golden Hairpin
Black Robe
Wizard Mantle

Cure, Cure 4, Raise, Reraise, Shell, Shell 2, Esuna, Holy
Fire 3, Fire 4, Bolt 4, Ice, Ice 3



Nifboy
Male
Gemini
38
57
Wizard
Math Skill
Hamedo
Equip Bow
Ignore Terrain

Ice Bow

Leather Hat
Chameleon Robe
Jade Armlet

Bolt 2, Bolt 3, Ice, Ice 2, Ice 4, Death, Flare
CT, Height, Prime Number, 5, 4



CorpusCav
Female
Libra
74
54
Dancer
Jump
Distribute
Concentrate
Ignore Terrain

Persia

Holy Miter
Black Costume
Elf Mantle

Witch Hunt, Wiznaibus, Disillusion, Last Dance, Obsidian Blade, Nether Demon
Level Jump8, Vertical Jump8



Grandlanzer
Male
Libra
44
56
Time Mage
Jump
Mana Shield
Equip Armor
Levitate

Gokuu Rod

Genji Helmet
Brigandine
Magic Gauntlet

Haste 2, Reflect, Stabilize Time, Meteor
Level Jump4, Vertical Jump8

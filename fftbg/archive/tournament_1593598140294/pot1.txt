Final Bets: red - 7 bets for 3,055G (34.4%, x1.90); blue - 10 bets for 5,814G (65.6%, x0.53)

red bets:
DavenIII: 1,000G (32.7%, 10,338G)
safira125: 686G (22.5%, 686G)
BlackFireUK: 504G (16.5%, 504G)
BoneMiser: 500G (16.4%, 500G)
getthemoneyz: 242G (7.9%, 1,132,334G)
Evewho: 100G (3.3%, 15,045G)
BirbBrainsBot: 23G (0.8%, 38,012G)

blue bets:
DamnThatShark: 1,471G (25.3%, 1,471G)
helpimabug: 1,258G (21.6%, 1,258G)
prince_rogers_nelson_: 1,000G (17.2%, 6,241G)
gorgewall: 696G (12.0%, 696G)
RaIshtar: 500G (8.6%, 10,806G)
Digitalsocrates: 489G (8.4%, 489G)
ApplesauceBoss: 100G (1.7%, 13,791G)
datadrivenbot: 100G (1.7%, 49,659G)
lastly: 100G (1.7%, 31,143G)
DuneMeta: 100G (1.7%, 5,120G)

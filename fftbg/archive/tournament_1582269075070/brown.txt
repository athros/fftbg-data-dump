Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ApplesauceBoss
Male
Cancer
56
64
Monk
Battle Skill
Hamedo
Equip Knife
Move+1

Cultist Dagger

Triangle Hat
Leather Outfit
Jade Armlet

Earth Slash, Purification, Seal Evil
Weapon Break, Magic Break, Speed Break, Justice Sword



TheFern33
Monster
Aries
56
58
Behemoth










Lydian C
Female
Aries
77
43
Geomancer
Draw Out
Catch
Equip Polearm
Jump+1

Musk Rod
Platinum Shield
Red Hood
Judo Outfit
Magic Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa



Imranimran
Female
Pisces
66
41
Dancer
Punch Art
Catch
Short Charge
Move-MP Up

Hydra Bag

Feather Hat
Mystic Vest
Magic Gauntlet

Polka Polka, Last Dance, Obsidian Blade, Nether Demon, Dragon Pit
Pummel, Earth Slash, Chakra, Revive

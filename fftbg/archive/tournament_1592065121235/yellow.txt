Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Milkman153
Male
Taurus
73
47
Oracle
Basic Skill
Caution
Dual Wield
Move-MP Up

Battle Folio
Bestiary
Feather Hat
Robe of Lords
Defense Armlet

Poison, Pray Faith, Zombie, Blind Rage, Confusion Song, Paralyze
Throw Stone, Heal



ALY327
Female
Aries
37
40
Ninja
Dance
Counter Tackle
Equip Shield
Ignore Height

Spell Edge
Platinum Shield
Thief Hat
Earth Clothes
Feather Boots

Shuriken, Hammer
Witch Hunt, Polka Polka, Last Dance, Obsidian Blade



Zbgs
Female
Aquarius
60
78
Time Mage
Jump
Counter
Doublehand
Jump+2

Rainbow Staff

Thief Hat
Wizard Robe
Magic Gauntlet

Slow, Immobilize, Quick, Demi
Level Jump2, Vertical Jump2



Fluffskull
Female
Gemini
76
42
Monk
Summon Magic
Counter Magic
Attack UP
Waterbreathing



Flash Hat
Leather Outfit
Diamond Armlet

Pummel, Earth Slash, Secret Fist, Purification, Seal Evil
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Salamander, Cyclops

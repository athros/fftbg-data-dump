Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Monster
Taurus
72
42
Blue Dragon










ThePineappleSalesman
Female
Pisces
67
57
Priest
Battle Skill
Counter
Long Status
Move+2

Gold Staff

Triangle Hat
Mythril Vest
Defense Ring

Cure, Cure 2, Cure 3, Reraise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Shield Break



E Ballard
Male
Gemini
48
49
Calculator
Time Magic
Mana Shield
Secret Hunt
Fly

Papyrus Codex

Triangle Hat
Black Costume
Reflect Ring

CT, Prime Number, 5, 4, 3
Haste, Slow, Slow 2, Stop, Demi 2



Digitalsocrates
Male
Capricorn
80
68
Archer
Basic Skill
Counter
Dual Wield
Teleport

Cross Bow
Bow Gun
Flash Hat
Clothes
Sprint Shoes

Charge+1, Charge+2, Charge+7, Charge+10, Charge+20
Accumulate, Heal, Yell

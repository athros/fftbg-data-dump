Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



L2 Sentinel
Male
Libra
49
78
Lancer
Yin Yang Magic
Speed Save
Doublehand
Lava Walking

Battle Bamboo

Grand Helmet
Leather Armor
Spike Shoes

Level Jump2, Vertical Jump8
Zombie, Silence Song, Dispel Magic, Paralyze



Findifesta
Male
Aquarius
42
52
Squire
Talk Skill
PA Save
Defend
Move+1

Morning Star
Mythril Shield
Feather Hat
Judo Outfit
Setiemson

Heal, Fury, Wish
Persuade, Insult, Mimic Daravon, Refute



Serperemagus
Female
Libra
54
60
Summoner
Elemental
Dragon Spirit
Attack UP
Ignore Height

Madlemgen

Flash Hat
Black Robe
Power Wrist

Moogle, Golem, Salamander, Lich
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



Alaquane
Female
Aries
68
64
Mime

Arrow Guard
Equip Armor
Ignore Terrain



Black Hood
Silk Robe
Small Mantle

Mimic


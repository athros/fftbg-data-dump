Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Vorap
Male
Sagittarius
64
61
Mediator
Steal
Hamedo
Defense UP
Levitate

Bestiary

Holy Miter
Power Sleeve
Defense Armlet

Persuade, Praise, Threaten, Insult, Rehabilitate
Gil Taking, Steal Accessory, Arm Aim



Benticore
Male
Aquarius
49
50
Squire
Charge
Absorb Used MP
Defense UP
Levitate

Bow Gun
Venetian Shield
Golden Hairpin
Clothes
Elf Mantle

Accumulate, Heal, Scream
Charge+2, Charge+3, Charge+7



Deathmaker06
Female
Aquarius
73
74
Thief
Item
HP Restore
Equip Sword
Jump+1

Ragnarok

Twist Headband
Mystic Vest
Feather Mantle

Steal Helmet, Steal Armor, Steal Shield
X-Potion, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down



Laserman1000
Male
Sagittarius
72
78
Calculator
White Magic
Dragon Spirit
Short Status
Move+1

Papyrus Codex

Holy Miter
Brigandine
N-Kai Armlet

CT, Height, Prime Number
Cure, Raise, Reraise, Protect, Protect 2, Shell, Wall, Esuna

Final Bets: green - 10 bets for 4,119G (28.5%, x2.51); yellow - 18 bets for 10,340G (71.5%, x0.40)

green bets:
sinnyil2: 1,200G (29.1%, 40,888G)
rocl: 1,000G (24.3%, 72,538G)
BirbBrainsBot: 428G (10.4%, 155,907G)
ungabunga_bot: 400G (9.7%, 143,891G)
UmbraKnights: 272G (6.6%, 272G)
gloomdog: 224G (5.4%, 224G)
ZephyrTempest: 213G (5.2%, 65,160G)
DeathTaxesAndAnime: 200G (4.9%, 1,650G)
ANFz: 100G (2.4%, 5,049G)
getthemoneyz: 82G (2.0%, 498,520G)

yellow bets:
HaplessOne: 2,000G (19.3%, 69,614G)
Gelwain: 1,777G (17.2%, 3,554G)
Lydian_C: 1,200G (11.6%, 76,508G)
sixstop: 750G (7.3%, 750G)
WireLord: 713G (6.9%, 713G)
mirapoix: 608G (5.9%, 608G)
aStatue: 500G (4.8%, 500G)
Wonser: 500G (4.8%, 1,209G)
Maeveen: 500G (4.8%, 3,305G)
NoNotBees: 444G (4.3%, 4,244G)
HASTERIOUS: 348G (3.4%, 348G)
Solid_Snake1982: 300G (2.9%, 843G)
Tithonus: 200G (1.9%, 11,473G)
AllInBot: 100G (1.0%, 100G)
ApplesauceBoss: 100G (1.0%, 4,420G)
Heroebal: 100G (1.0%, 4,923G)
datadrivenbot: 100G (1.0%, 100G)
Firesheath: 100G (1.0%, 1,032G)

Player: !Black
Team: Black Team
Palettes: Black/Red



DavenIII
Monster
Taurus
80
70
Draugr










Go2sleepTV
Male
Serpentarius
55
78
Archer
Summon Magic
Mana Shield
Defend
Move+1

Ultimus Bow

Feather Hat
Power Sleeve
Cursed Ring

Charge+5, Charge+7
Moogle, Shiva, Ifrit, Bahamut, Odin, Salamander, Silf, Lich, Zodiac



Treapvort
Female
Virgo
67
49
Oracle
Draw Out
Absorb Used MP
Concentrate
Move+2

Bestiary

Black Hood
Silk Robe
Germinas Boots

Blind, Poison, Zombie, Silence Song, Confusion Song
Murasame, Kiyomori



Maakur
Male
Aquarius
81
50
Oracle
White Magic
Mana Shield
Magic Attack UP
Fly

Ivory Rod

Feather Hat
Black Robe
Germinas Boots

Pray Faith, Silence Song, Foxbird, Dark Holy
Cure 2, Cure 4, Raise, Reraise, Regen, Shell 2, Wall, Esuna

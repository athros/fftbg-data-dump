Final Bets: green - 8 bets for 1,907G (9.4%, x9.61); yellow - 13 bets for 18,322G (90.6%, x0.10)

green bets:
getthemoneyz: 354G (18.6%, 1,013,098G)
placidphoenix: 315G (16.5%, 315G)
Mermydon: 300G (15.7%, 526G)
Ross_from_Cali: 288G (15.1%, 288G)
pancake11112: 200G (10.5%, 914G)
KasugaiRoastedPeas: 200G (10.5%, 10,013G)
Coolguye: 150G (7.9%, 2,681G)
Evewho: 100G (5.2%, 3,206G)

yellow bets:
reddwind_: 10,000G (54.6%, 25,733G)
Digitalsocrates: 3,000G (16.4%, 31,452G)
reinoe: 1,000G (5.5%, 10,336G)
Ring_Wyrm: 992G (5.4%, 992G)
Hasterious: 801G (4.4%, 80,197G)
BirbBrainsBot: 682G (3.7%, 50,749G)
TeaTime29: 500G (2.7%, 13,378G)
gorgewall: 401G (2.2%, 13,989G)
Lythe_Caraker: 250G (1.4%, 106,800G)
KingofTricksters: 216G (1.2%, 216G)
CT_5_Holy: 200G (1.1%, 1,995G)
josephiroth_143: 180G (1.0%, 180G)
datadrivenbot: 100G (0.5%, 44,783G)

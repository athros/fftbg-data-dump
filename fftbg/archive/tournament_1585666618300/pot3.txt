Final Bets: white - 24 bets for 14,965G (65.5%, x0.53); black - 16 bets for 7,882G (34.5%, x1.90)

white bets:
TheChainNerd: 2,726G (18.2%, 13,631G)
SleepyBenzo: 2,370G (15.8%, 2,370G)
NovaKnight21: 1,319G (8.8%, 2,639G)
kai_shee: 1,000G (6.7%, 50,208G)
Vampire_Killer: 1,000G (6.7%, 13,179G)
Ungaiii: 768G (5.1%, 4,768G)
JumbocactuarX27: 661G (4.4%, 661G)
Lanshaft: 600G (4.0%, 11,394G)
IndecisiveNinja: 521G (3.5%, 521G)
nomoment: 500G (3.3%, 22,732G)
HaplessOne: 500G (3.3%, 21,651G)
bad1dea: 400G (2.7%, 316,254G)
BirbBrainsBot: 394G (2.6%, 105,566G)
ForagerCats: 378G (2.5%, 378G)
Aeolus_000: 300G (2.0%, 3,699G)
ZephyrTempest: 298G (2.0%, 18,849G)
SomeCallMeGON: 244G (1.6%, 244G)
MeleeWizard: 236G (1.6%, 236G)
fenaen: 200G (1.3%, 2,987G)
sankvtw: 150G (1.0%, 2,539G)
SeedSC: 100G (0.7%, 99,361G)
rrazzoug: 100G (0.7%, 2,833G)
maakur_: 100G (0.7%, 2,128G)
datadrivenbot: 100G (0.7%, 15,816G)

black bets:
sinnyil2: 1,300G (16.5%, 27,054G)
Laserman1000: 1,181G (15.0%, 4,381G)
Nickyfive: 1,044G (13.2%, 2,089G)
inzo_bettingbot: 1,000G (12.7%, 42,145G)
Zeroroute: 716G (9.1%, 716G)
holyonline: 500G (6.3%, 3,454G)
Winterharte: 460G (5.8%, 460G)
ko2q: 300G (3.8%, 300G)
DLJuggernaut: 300G (3.8%, 8,115G)
Anethum: 264G (3.3%, 264G)
Baron_von_Scrub: 252G (3.2%, 1,455G)
ugoplatamia: 200G (2.5%, 20,036G)
AllInBot: 143G (1.8%, 143G)
getthemoneyz: 102G (1.3%, 400,797G)
astrozin11: 100G (1.3%, 6,169G)
Rikrizen: 20G (0.3%, 410G)

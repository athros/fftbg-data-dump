Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Marin1987
Male
Gemini
58
76
Calculator
Gore Skill
Caution
Short Status
Waterbreathing

Papyrus Codex

Black Hood
Judo Outfit
Cursed Ring

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



AUrato
Female
Taurus
77
72
Calculator
Dragon Skill
Mana Shield
Defend
Move-MP Up

Battle Folio
Ice Shield
Flash Hat
Chain Mail
Cursed Ring

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Arcblazer23
Female
Aquarius
74
54
Calculator
Catfish Skill
MP Restore
Equip Shield
Waterwalking

Papyrus Codex
Buckler
Red Hood
Chameleon Robe
Setiemson

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



Dellusionsx
Female
Capricorn
40
79
Calculator
Imp Skill
Absorb Used MP
Maintenance
Move+3

Musk Rod

Green Beret
Platinum Armor
Jade Armlet

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam

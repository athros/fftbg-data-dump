Player: !Green
Team: Green Team
Palettes: Green/White



Eudes89
Male
Leo
58
49
Geomancer
Yin Yang Magic
Dragon Spirit
Short Charge
Move-HP Up

Blood Sword
Flame Shield
Red Hood
Adaman Vest
Magic Gauntlet

Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Poison, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic



Miku Shikhu
Female
Libra
47
62
Thief
Black Magic
Earplug
Sicken
Jump+2

Rune Blade

Barette
Wizard Outfit
Feather Boots

Steal Armor, Arm Aim, Leg Aim
Fire, Fire 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower, Frog



Gooseyourself
Female
Pisces
65
39
Wizard
Battle Skill
Brave Save
Attack UP
Jump+1

Flame Rod

Red Hood
Silk Robe
Cursed Ring

Fire 3, Bolt, Bolt 2, Ice 4, Death
Magic Break, Speed Break, Power Break, Mind Break, Night Sword



Baron Von Scrub
Female
Libra
40
58
Calculator
Black Magic
Distribute
Defend
Waterwalking

Rod

Feather Hat
Linen Robe
Elf Mantle

Height, Prime Number, 5, 4, 3
Fire 2, Fire 4, Bolt 2, Ice

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ar Tactic
Male
Libra
65
60
Lancer
Basic Skill
Damage Split
Short Charge
Move+2

Battle Bamboo
Buckler
Iron Helmet
Wizard Robe
Red Shoes

Level Jump2, Vertical Jump5
Dash, Throw Stone, Wish



DavenIII
Male
Aquarius
63
65
Thief
Charge
Damage Split
Doublehand
Jump+2

Blood Sword

Leather Hat
Judo Outfit
Leather Mantle

Steal Accessory
Charge+1, Charge+3, Charge+20



Galkife
Male
Gemini
48
41
Mime

Distribute
Long Status
Lava Walking



Headgear
Chain Vest
Feather Boots

Mimic




BlackFireUK
Male
Aries
77
38
Knight
White Magic
Parry
Doublehand
Teleport

Platinum Sword

Gold Helmet
Diamond Armor
Magic Gauntlet

Head Break, Armor Break, Shield Break, Mind Break
Raise, Protect, Protect 2, Shell, Shell 2, Esuna

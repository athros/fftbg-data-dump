Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



StealthModeLocke
Female
Libra
74
47
Time Mage
Elemental
Counter
Beastmaster
Move+2

Rainbow Staff

Green Beret
Light Robe
Spike Shoes

Immobilize, Quick, Stabilize Time
Pitfall, Local Quake, Static Shock, Quicksand, Lava Ball



DamnThatShark
Female
Scorpio
47
63
Samurai
Talk Skill
Meatbone Slash
Dual Wield
Move+2

Murasame
Heaven's Cloud
Cross Helmet
Gold Armor
Feather Mantle

Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Death Sentence, Refute



GrayGhostGaming
Female
Leo
41
51
Geomancer
Summon Magic
Caution
Beastmaster
Retreat

Giant Axe
Gold Shield
Holy Miter
Silk Robe
Angel Ring

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Ifrit, Carbunkle, Odin, Leviathan, Salamander



EnemyController
Female
Gemini
75
71
Geomancer
Draw Out
Distribute
Short Charge
Move-MP Up

Battle Axe
Bronze Shield
Headgear
Black Robe
Defense Armlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Kikuichimoji

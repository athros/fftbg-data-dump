Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ShintaroNayaka
Monster
Cancer
48
66
Minotaur










Hasterious
Female
Libra
68
67
Priest
Dance
MA Save
Martial Arts
Move+1

Gold Staff

Headgear
Brigandine
Angel Ring

Cure 2, Cure 3, Raise, Raise 2, Regen, Protect, Shell, Wall, Holy, Magic Barrier
Disillusion



Lastly
Male
Serpentarius
38
69
Monk
Jump
Catch
Equip Axe
Teleport 2

Flame Whip

Red Hood
Power Sleeve
Feather Mantle

Wave Fist, Earth Slash, Secret Fist, Revive
Level Jump4, Vertical Jump6



Arleyne
Female
Sagittarius
42
63
Thief
Punch Art
Counter Flood
Maintenance
Lava Walking

Short Edge

Holy Miter
Mystic Vest
Jade Armlet

Steal Helmet, Steal Status
Earth Slash

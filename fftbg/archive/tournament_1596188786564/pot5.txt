Final Bets: blue - 11 bets for 11,231G (76.1%, x0.31); green - 6 bets for 3,533G (23.9%, x3.18)

blue bets:
Mesmaster: 3,000G (26.7%, 58,991G)
UmaiJam: 2,000G (17.8%, 42,950G)
gorgewall: 1,363G (12.1%, 1,363G)
douchetron: 1,099G (9.8%, 2,156G)
letdowncity: 1,000G (8.9%, 7,230G)
BirbBrainsBot: 1,000G (8.9%, 64,123G)
AllInBot: 533G (4.7%, 533G)
Lanshaft: 532G (4.7%, 30,157G)
ShintaroNayaka: 304G (2.7%, 304G)
CIairebear: 200G (1.8%, 3,079G)
datadrivenbot: 200G (1.8%, 45,201G)

green bets:
fluffskull: 1,333G (37.7%, 2,614G)
NIghtdew14: 1,000G (28.3%, 4,573G)
Lythe_Caraker: 500G (14.2%, 179,217G)
getthemoneyz: 400G (11.3%, 1,452,915G)
Klednar21: 200G (5.7%, 4,974G)
EmmaEnema: 100G (2.8%, 100G)

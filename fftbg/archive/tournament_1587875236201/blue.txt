Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



XalSmellsBad
Female
Capricorn
50
66
Squire
Summon Magic
HP Restore
Equip Knife
Move-MP Up

Thunder Rod
Bronze Shield
Headgear
Mystic Vest
Red Shoes

Heal, Wish
Moogle, Ramuh, Lich



Zagorsek
Male
Virgo
47
64
Monk
Battle Skill
Auto Potion
Equip Bow
Move+1

Windslash Bow

Flash Hat
Black Costume
Elf Mantle

Pummel, Wave Fist, Earth Slash, Purification, Revive
Head Break, Speed Break, Mind Break, Stasis Sword, Dark Sword



Powergems
Female
Sagittarius
65
74
Chemist
Draw Out
Sunken State
Dual Wield
Move+1

Cute Bag
Cute Bag
Flash Hat
Mythril Vest
Jade Armlet

Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Asura, Muramasa



RagequitSA
Male
Aries
76
46
Wizard
Battle Skill
MP Restore
Concentrate
Move+1

Thunder Rod

Black Hood
Chain Vest
Feather Boots

Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 2
Weapon Break, Power Break, Mind Break, Dark Sword

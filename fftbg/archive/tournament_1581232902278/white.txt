Player: !White
Team: White Team
Palettes: White/Blue



Quesobandito 713
Male
Virgo
75
78
Wizard
Sing
Parry
Defend
Teleport

Blind Knife

Feather Hat
Silk Robe
Salty Rage

Fire, Bolt, Bolt 4, Ice 2, Empower
Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade



Zachara
Monster
Gemini
69
57
Explosive










Evontno
Female
Sagittarius
47
70
Thief
Punch Art
Counter Flood
Defend
Teleport

Spell Edge

Flash Hat
Judo Outfit
Genji Gauntlet

Steal Heart, Steal Shield, Steal Weapon, Leg Aim
Wave Fist, Secret Fist, Purification, Revive



Mizucrux
Female
Sagittarius
56
78
Ninja
Talk Skill
PA Save
Equip Gun
Ignore Terrain

Papyrus Codex
Fairy Harp
Green Beret
Leather Outfit
Setiemson

Shuriken, Bomb, Knife, Ninja Sword
Praise, Threaten, Preach, Death Sentence, Mimic Daravon

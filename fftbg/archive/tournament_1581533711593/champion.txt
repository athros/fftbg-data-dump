Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Maha099
Male
Leo
68
48
Monk
Elemental
Caution
Dual Wield
Move+3



Red Hood
Earth Clothes
Cursed Ring

Purification, Revive, Seal Evil
Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



Sypheck
Male
Aquarius
70
50
Monk
Sing
Damage Split
Doublehand
Jump+3



Flash Hat
Earth Clothes
Bracer

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Life Song, Cheer Song



WhiteTigress
Male
Leo
57
52
Archer
Item
PA Save
Doublehand
Teleport

Poison Bow

Holy Miter
Clothes
Bracer

Charge+2, Charge+7
Ether, Hi-Ether, Soft, Holy Water, Remedy



Ruvelia BibeI
Female
Cancer
80
39
Dancer
Steal
Earplug
Dual Wield
Move+1

Ryozan Silk
Persia
Leather Hat
Black Costume
Bracer

Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Dragon Pit
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gelwain
Male
Taurus
61
62
Ninja
Draw Out
Sunken State
Defense UP
Move+2

Air Knife
Short Edge
Red Hood
Leather Outfit
Rubber Shoes

Bomb, Wand
Koutetsu, Heaven's Cloud



Reddwind
Male
Capricorn
67
50
Knight
Jump
Mana Shield
Attack UP
Jump+2

Long Sword
Ice Shield
Leather Helmet
Gold Armor
Reflect Ring

Head Break, Armor Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword, Night Sword
Level Jump4, Vertical Jump7



LAGBOT30000
Female
Gemini
61
76
Samurai
White Magic
Sunken State
Long Status
Move+1

Holy Lance

Bronze Helmet
Wizard Robe
Vanish Mantle

Asura, Heaven's Cloud, Kiyomori, Muramasa
Cure, Raise, Raise 2, Shell 2, Holy



BStarTV
Male
Virgo
43
55
Knight
Charge
Parry
Equip Bow
Ignore Terrain

Poison Bow
Mythril Shield
Circlet
Diamond Armor
Magic Gauntlet

Armor Break, Weapon Break, Speed Break, Power Break, Mind Break
Charge+7

Final Bets: red - 11 bets for 6,789G (55.4%, x0.81); blue - 8 bets for 5,467G (44.6%, x1.24)

red bets:
sinnyil2: 2,326G (34.3%, 4,652G)
VolgraTheMoose: 1,001G (14.7%, 67,816G)
Aldrammech: 700G (10.3%, 700G)
dogsandcatsand: 506G (7.5%, 506G)
maximumcrit: 500G (7.4%, 9,133G)
killth3kid: 500G (7.4%, 848G)
Snowfats: 500G (7.4%, 22,089G)
thunderducker: 333G (4.9%, 3,357G)
datadrivenbot: 200G (2.9%, 65,862G)
roofiepops: 123G (1.8%, 2,045G)
AllInBot: 100G (1.5%, 100G)

blue bets:
BirbBrainsBot: 1,000G (18.3%, 3,846G)
Mesmaster: 1,000G (18.3%, 16,503G)
HaateXIII: 812G (14.9%, 812G)
E_Ballard: 740G (13.5%, 50,868G)
Rytor: 735G (13.4%, 7,353G)
Thyrandaal: 500G (9.1%, 59,593G)
skipsandwiches: 368G (6.7%, 368G)
Rurk: 312G (5.7%, 399G)

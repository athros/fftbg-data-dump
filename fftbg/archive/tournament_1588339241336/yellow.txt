Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Daveb
Male
Virgo
70
64
Ninja
Jump
MP Restore
Long Status
Fly

Koga Knife
Scorpion Tail
Barette
Mythril Vest
Chantage

Shuriken, Bomb
Level Jump5, Vertical Jump5



J2DaBibbles
Female
Cancer
53
44
Ninja
Summon Magic
Parry
Equip Gun
Ignore Terrain

Stone Gun
Mythril Gun
Green Beret
Judo Outfit
Magic Gauntlet

Shuriken, Bomb, Dictionary
Moogle, Ramuh, Ifrit, Odin, Lich



Tmo50x
Male
Sagittarius
46
72
Ninja
Battle Skill
Distribute
Equip Armor
Jump+1

Ninja Edge
Flail
Grand Helmet
Diamond Armor
Jade Armlet

Shuriken, Axe
Armor Break, Shield Break, Weapon Break, Magic Break, Night Sword



DavenIII
Male
Virgo
61
42
Chemist
Black Magic
Distribute
Equip Sword
Lava Walking

Save the Queen

Red Hood
Leather Outfit
Germinas Boots

Potion, X-Potion, Hi-Ether, Antidote, Soft, Remedy
Fire 3, Bolt 2, Ice 2, Empower

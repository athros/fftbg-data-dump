Player: !White
Team: White Team
Palettes: White/Blue



Oldchris
Male
Sagittarius
54
59
Priest
Time Magic
HP Restore
Defend
Waterbreathing

Flame Whip

Flash Hat
Judo Outfit
Cursed Ring

Cure 2, Cure 4, Raise, Reraise, Shell, Esuna
Slow, Slow 2, Immobilize, Reflect, Demi, Meteor



Cryptopsy70
Male
Taurus
45
51
Mediator
Summon Magic
Counter
Defense UP
Move-HP Up

Glacier Gun

Cachusha
Linen Robe
Jade Armlet

Invitation, Persuade, Preach, Negotiate, Refute, Rehabilitate
Titan, Golem, Bahamut, Fairy, Lich



Laserman1000
Male
Scorpio
49
76
Mediator
Sing
Regenerator
Equip Gun
Fly

Bloody Strings

Twist Headband
Wizard Outfit
Elf Mantle

Praise, Threaten, Solution, Insult, Refute, Rehabilitate
Battle Song, Diamond Blade, Sky Demon



Latebit
Female
Capricorn
42
75
Time Mage
Steal
Absorb Used MP
Equip Sword
Jump+3

Save the Queen

Holy Miter
Black Robe
Bracer

Haste 2, Slow, Stop, Reflect, Demi 2, Stabilize Time
Steal Heart

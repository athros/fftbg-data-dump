Final Bets: green - 22 bets for 11,582G (53.6%, x0.86); yellow - 12 bets for 10,009G (46.4%, x1.16)

green bets:
pplvee1: 3,500G (30.2%, 23,350G)
coralreeferz: 1,000G (8.6%, 29,498G)
LDSkinny: 1,000G (8.6%, 38,020G)
BirbBrainsBot: 1,000G (8.6%, 158,714G)
skipsandwiches: 1,000G (8.6%, 7,589G)
BoneMiser: 554G (4.8%, 554G)
benticore: 500G (4.3%, 30,138G)
EvilJMcNasty: 500G (4.3%, 102,941G)
killth3kid: 468G (4.0%, 4,375G)
Cryptopsy70: 320G (2.8%, 320G)
CapnChaos12: 300G (2.6%, 8,506G)
AllInBot: 200G (1.7%, 200G)
Belkra: 200G (1.7%, 798G)
datadrivenbot: 200G (1.7%, 32,866G)
ohhinm: 167G (1.4%, 6,032G)
getthemoneyz: 148G (1.3%, 1,339,261G)
oldchris: 100G (0.9%, 5,479G)
holdenmagronik: 100G (0.9%, 385G)
triniculo: 100G (0.9%, 1,110G)
captainmilestw: 100G (0.9%, 1,900G)
possumm: 100G (0.9%, 2,619G)
DouglasDragonThePoet: 25G (0.2%, 244G)

yellow bets:
Lydian_C: 4,200G (42.0%, 77,487G)
TheChainNerd: 1,217G (12.2%, 1,217G)
Meta_Five: 1,000G (10.0%, 19,529G)
douchetron: 701G (7.0%, 1,376G)
Nizaha: 560G (5.6%, 9,270G)
JumbocactuarX27: 500G (5.0%, 500G)
letdowncity: 432G (4.3%, 432G)
dtrain332: 423G (4.2%, 423G)
Thyrandaal: 348G (3.5%, 348G)
dogsandcatsand: 308G (3.1%, 308G)
latebit: 208G (2.1%, 208G)
mattcorn7: 112G (1.1%, 112G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Anox Skell
Female
Leo
66
79
Time Mage
Summon Magic
Critical Quick
Short Status
Jump+2

Battle Folio

Headgear
Wizard Robe
Magic Gauntlet

Slow, Reflect, Demi, Demi 2, Stabilize Time
Carbunkle, Fairy



Lythe Caraker
Female
Taurus
41
59
Dancer
Jump
Counter
Equip Sword
Move+3

Diamond Sword

Headgear
Linen Robe
Spike Shoes

Disillusion, Dragon Pit
Level Jump4, Vertical Jump4



Laserman1000
Male
Taurus
45
59
Archer
Battle Skill
Counter
Doublehand
Move-HP Up

Long Bow

Triangle Hat
Chain Vest
Reflect Ring

Charge+3
Armor Break, Speed Break, Mind Break, Justice Sword, Night Sword



Mtueni
Female
Aquarius
48
74
Lancer
Dance
Parry
Equip Bow
Levitate

Gastrafitis
Gold Shield
Circlet
Platinum Armor
Defense Armlet

Level Jump8, Vertical Jump5
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Void Storage, Dragon Pit

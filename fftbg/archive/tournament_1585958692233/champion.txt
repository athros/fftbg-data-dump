Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Tithonus
Female
Scorpio
49
71
Ninja
Yin Yang Magic
Damage Split
Defend
Jump+3

Morning Star
Short Edge
Black Hood
Leather Outfit
Sprint Shoes

Bomb, Ninja Sword
Blind, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic



TheRobbieRobster
Female
Cancer
57
53
Lancer
Elemental
Distribute
Equip Armor
Jump+2

Spear
Platinum Shield
Flash Hat
Secret Clothes
Dracula Mantle

Level Jump8, Vertical Jump8
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Zagorsek
Female
Gemini
76
70
Summoner
Time Magic
Parry
Defense UP
Waterwalking

Oak Staff

Feather Hat
Rubber Costume
Leather Mantle

Moogle, Shiva, Ramuh, Ifrit, Golem, Odin, Leviathan, Silf
Slow, Float, Quick



Volgrathemoose
Female
Cancer
46
58
Summoner
Black Magic
Dragon Spirit
Short Charge
Move-MP Up

White Staff

Flash Hat
Linen Robe
Battle Boots

Ifrit, Golem, Carbunkle, Silf
Fire 3, Ice, Ice 4, Empower

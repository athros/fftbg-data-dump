Player: !White
Team: White Team
Palettes: White/Blue



Bryon W
Male
Scorpio
68
56
Knight
Basic Skill
Damage Split
Dual Wield
Waterbreathing

Defender
Giant Axe
Platinum Helmet
Diamond Armor
Defense Armlet

Head Break, Weapon Break, Power Break, Mind Break, Surging Sword
Accumulate, Heal, Tickle, Fury, Wish



Moshyhero
Male
Gemini
73
62
Geomancer
White Magic
Counter Tackle
Equip Armor
Waterwalking

Ancient Sword
Diamond Shield
Crystal Helmet
Chain Mail
Genji Gauntlet

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Cure 2, Cure 3, Cure 4, Raise 2, Protect, Protect 2, Shell, Esuna



Korrill1454
Female
Sagittarius
56
67
Knight
Basic Skill
Abandon
Attack UP
Move+2

Long Sword
Ice Shield
Leather Helmet
Black Robe
Germinas Boots

Shield Break, Weapon Break, Speed Break, Power Break, Surging Sword
Dash, Heal, Yell, Fury



Hasterious
Female
Aries
65
62
Chemist
Basic Skill
Counter Flood
Beastmaster
Move-HP Up

Star Bag

Twist Headband
Adaman Vest
Salty Rage

Potion, Hi-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Accumulate, Throw Stone, Heal, Tickle, Cheer Up

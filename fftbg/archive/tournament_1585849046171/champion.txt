Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Ko2q
Male
Sagittarius
64
55
Chemist
Basic Skill
Hamedo
Defend
Jump+2

Blaze Gun

Twist Headband
Power Sleeve
Defense Armlet

Potion, X-Potion, Eye Drop, Holy Water, Phoenix Down
Accumulate, Dash, Heal



Zachara
Female
Leo
37
65
Mediator
Summon Magic
Counter
Dual Wield
Move-MP Up

Stone Gun
Glacier Gun
Holy Miter
Silk Robe
Angel Ring

Persuade, Praise, Preach, Insult, Negotiate, Mimic Daravon
Moogle, Ifrit, Golem, Odin, Lich



ANFz
Female
Gemini
46
59
Chemist
Throw
Counter Tackle
Equip Gun
Teleport

Battle Folio

Thief Hat
Mythril Vest
Salty Rage

Potion, Hi-Potion, X-Potion, Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Bomb, Knife, Staff, Stick



Technominari
Female
Leo
59
51
Priest
Talk Skill
Meatbone Slash
Short Charge
Waterbreathing

Wizard Staff

Green Beret
Black Robe
Defense Armlet

Cure, Cure 2, Raise, Raise 2, Reraise, Protect, Wall, Esuna
Invitation, Threaten

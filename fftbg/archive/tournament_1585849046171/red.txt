Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Female
Virgo
64
60
Oracle
Item
Abandon
Equip Polearm
Jump+2

Partisan

Green Beret
Light Robe
Elf Mantle

Blind, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Sleep
Potion, X-Potion, Ether, Antidote, Eye Drop



Bryon W
Female
Capricorn
71
76
Archer
Dance
MA Save
Dual Wield
Move+2

Glacier Gun
Blast Gun
Iron Helmet
Mystic Vest
108 Gems

Charge+2, Charge+3, Charge+20
Slow Dance, Disillusion, Nether Demon



Chuckolator
Male
Gemini
47
49
Calculator
White Magic
Counter
Equip Sword
Levitate

Excalibur

Headgear
Chain Vest
Genji Gauntlet

CT, Height, 5, 4
Cure, Cure 2, Cure 4, Raise, Reraise, Protect 2, Shell, Esuna



ThePineappleSalesman
Male
Aries
66
53
Ninja
Black Magic
Abandon
Attack UP
Move-MP Up

Cultist Dagger
Blind Knife
Golden Hairpin
Power Sleeve
Small Mantle

Shuriken, Wand
Fire, Fire 3, Bolt 3, Ice, Ice 4, Frog, Death

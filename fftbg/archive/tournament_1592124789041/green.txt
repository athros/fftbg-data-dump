Player: !Green
Team: Green Team
Palettes: Green/White



Solomongrundy85
Male
Cancer
65
65
Knight
Jump
Earplug
Equip Knife
Waterwalking

Kunai
Mythril Shield
Iron Helmet
Linen Robe
Spike Shoes

Shield Break, Speed Break, Mind Break, Dark Sword
Level Jump8, Vertical Jump5



Alithesillybird
Female
Virgo
75
50
Calculator
Animal Skill
Damage Split
Equip Shield
Move-HP Up

Papyrus Codex
Venetian Shield
Bronze Helmet
Light Robe
Battle Boots

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



TeaTime29
Female
Libra
81
40
Geomancer
Item
Faith Save
Attack UP
Move+2

Heaven's Cloud
Aegis Shield
Thief Hat
Chameleon Robe
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down



HASTERIOUS
Male
Virgo
73
52
Bard
Punch Art
Caution
Short Status
Jump+2

Ramia Harp

Headgear
Mythril Armor
Genji Gauntlet

Angel Song, Nameless Song, Last Song, Hydra Pit
Wave Fist, Earth Slash, Secret Fist, Purification, Chakra

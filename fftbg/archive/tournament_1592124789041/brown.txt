Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ColetteMSLP
Female
Cancer
57
67
Thief
Draw Out
Abandon
Equip Knife
Waterwalking

Wizard Rod

Cachusha
Clothes
Germinas Boots

Gil Taking, Steal Armor
Asura, Bizen Boat, Heaven's Cloud, Kiyomori



CassiePhoenix
Female
Leo
55
63
Samurai
Punch Art
Auto Potion
Equip Bow
Ignore Terrain

Lightning Bow

Iron Helmet
Linen Robe
Genji Gauntlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Pummel, Wave Fist, Earth Slash, Secret Fist, Purification



Shannahan45
Female
Aquarius
50
58
Dancer
Time Magic
Caution
Defense UP
Move+1

Cashmere

Headgear
Linen Robe
Feather Mantle

Witch Hunt, Dragon Pit
Haste 2, Immobilize, Float



DamnThatShark
Male
Capricorn
38
49
Ninja
Charge
Auto Potion
Equip Bow
Teleport

Hunting Bow
Night Killer
Golden Hairpin
Rubber Costume
Defense Ring

Staff, Dictionary
Charge+2

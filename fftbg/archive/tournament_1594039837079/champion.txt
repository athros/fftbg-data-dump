Player: !zChamp
Team: Champion Team
Palettes: Green/White



Prince Rogers Nelson
Female
Virgo
74
59
Chemist
Charge
Speed Save
Defend
Move+2

Hydra Bag

Thief Hat
Mythril Vest
Angel Ring

Hi-Potion, Holy Water, Remedy, Phoenix Down
Charge+7, Charge+10



JethroThrul
Male
Cancer
64
45
Mime

Damage Split
Equip Shield
Waterwalking


Flame Shield
Triangle Hat
Clothes
Wizard Mantle

Mimic




RaIshtar
Female
Aries
72
37
Chemist
Yin Yang Magic
Brave Save
Magic Attack UP
Ignore Terrain

Blaze Gun

Ribbon
Clothes
Leather Mantle

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Life Drain, Confusion Song, Dispel Magic, Paralyze



CassiePhoenix
Female
Pisces
53
74
Archer
Summon Magic
Catch
Doublehand
Jump+3

Romanda Gun

Thief Hat
Clothes
Angel Ring

Charge+2, Charge+3, Charge+7
Ramuh, Golem, Carbunkle, Silf, Lich

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Laserman1000
Monster
Cancer
79
59
Holy Dragon










Latebit
Male
Libra
55
67
Calculator
Bird Skill
Dragon Spirit
Equip Bow
Teleport

Ice Bow

Crystal Helmet
Silk Robe
108 Gems

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



Omegasuspekt
Male
Capricorn
69
54
Lancer
Draw Out
Regenerator
Equip Gun
Jump+3

Romanda Gun
Kaiser Plate
Gold Helmet
Diamond Armor
Elf Mantle

Level Jump5, Vertical Jump6
Koutetsu, Bizen Boat, Kiyomori



Zenlion
Male
Leo
70
41
Time Mage
Draw Out
Auto Potion
Dual Wield
Jump+1

White Staff
Gold Staff
Green Beret
Light Robe
Rubber Shoes

Haste, Quick, Demi, Meteor
Asura, Koutetsu, Muramasa

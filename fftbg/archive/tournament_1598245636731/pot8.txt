Final Bets: red - 8 bets for 7,412G (47.3%, x1.12); champion - 9 bets for 8,266G (52.7%, x0.90)

red bets:
Geffro1908: 2,584G (34.9%, 8,616G)
evdoggity: 2,000G (27.0%, 236,120G)
BirbBrainsBot: 1,000G (13.5%, 190,781G)
SkylerBunny: 548G (7.4%, 548G)
silentperogy: 500G (6.7%, 41,620G)
lowlf: 380G (5.1%, 7,631G)
CT_Remix: 200G (2.7%, 531G)
datadrivenbot: 200G (2.7%, 67,887G)

champion bets:
EnemyController: 3,333G (40.3%, 1,559,660G)
iBardic: 2,209G (26.7%, 4,419G)
omegasuspekt: 975G (11.8%, 1,950G)
DLJuggernaut: 750G (9.1%, 10,677G)
latebit: 468G (5.7%, 468G)
RonaldoTheGypsy: 228G (2.8%, 228G)
getthemoneyz: 102G (1.2%, 1,732,455G)
gorgewall: 101G (1.2%, 21,273G)
AllInBot: 100G (1.2%, 100G)

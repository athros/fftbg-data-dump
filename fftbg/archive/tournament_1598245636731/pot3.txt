Final Bets: white - 7 bets for 6,217G (46.5%, x1.15); black - 12 bets for 7,155G (53.5%, x0.87)

white bets:
OneHundredFists: 1,676G (27.0%, 1,676G)
latebit: 1,290G (20.7%, 2,530G)
DLJuggernaut: 1,000G (16.1%, 10,030G)
BirbBrainsBot: 1,000G (16.1%, 189,969G)
Chihuahua_Charity: 614G (9.9%, 614G)
EnemyController: 555G (8.9%, 1,555,894G)
getthemoneyz: 82G (1.3%, 1,732,650G)

black bets:
Geffro1908: 4,066G (56.8%, 8,133G)
holdenmagronik: 651G (9.1%, 651G)
sinnyil2: 508G (7.1%, 1,016G)
silentperogy: 500G (7.0%, 41,038G)
RonaldoTheGypsy: 325G (4.5%, 325G)
Arcblazer23: 304G (4.2%, 304G)
iBardic: 200G (2.8%, 6,194G)
datadrivenbot: 200G (2.8%, 68,322G)
gorgewall: 101G (1.4%, 21,433G)
AllInBot: 100G (1.4%, 100G)
cloudycube: 100G (1.4%, 1,170G)
CT_Remix: 100G (1.4%, 519G)

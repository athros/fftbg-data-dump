Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Uncharreted
Female
Capricorn
74
80
Archer
Draw Out
Counter
Halve MP
Retreat

Night Killer
Bronze Shield
Headgear
Black Costume
Vanish Mantle

Charge+3
Koutetsu, Heaven's Cloud, Kikuichimoji



Gorgewall
Male
Capricorn
43
56
Knight
Jump
Catch
Martial Arts
Move-HP Up

Defender
Genji Shield
Mythril Helmet
Bronze Armor
Rubber Shoes

Armor Break, Speed Break, Justice Sword, Night Sword
Level Jump8, Vertical Jump2



Latebit
Female
Scorpio
64
69
Lancer
Yin Yang Magic
Faith Save
Defense UP
Jump+1

Gokuu Rod
Aegis Shield
Iron Helmet
Plate Mail
Leather Mantle

Level Jump5, Vertical Jump6
Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Sleep, Dark Holy



Ar Tactic
Male
Virgo
52
80
Ninja
Summon Magic
Faith Save
Defense UP
Levitate

Hidden Knife
Sasuke Knife
Triangle Hat
Leather Outfit
Diamond Armlet

Shuriken, Bomb, Knife, Hammer
Moogle, Carbunkle, Bahamut, Odin, Salamander, Fairy

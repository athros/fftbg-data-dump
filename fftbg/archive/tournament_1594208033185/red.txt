Player: !Red
Team: Red Team
Palettes: Red/Brown



ZCKaiser
Female
Libra
71
60
Dancer
Throw
Catch
Equip Gun
Ignore Terrain

Bloody Strings

Green Beret
Linen Robe
Genji Gauntlet

Witch Hunt, Polka Polka, Disillusion
Shuriken, Bomb, Staff



JethroThrul
Female
Leo
44
45
Mediator
Black Magic
Auto Potion
Equip Bow
Jump+3

Long Bow

Red Hood
Mystic Vest
Battle Boots

Persuade, Praise, Death Sentence, Insult, Negotiate, Rehabilitate
Bolt 3, Bolt 4, Empower



Randgridr
Monster
Taurus
57
65
Mindflayer










SephDarkheart
Male
Capricorn
75
45
Geomancer
White Magic
Dragon Spirit
Equip Armor
Waterbreathing

Battle Axe
Diamond Shield
Barbuta
Wizard Robe
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 2, Cure 4, Raise, Reraise, Shell, Wall

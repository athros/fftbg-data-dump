Player: !Green
Team: Green Team
Palettes: Green/White



Zenlion
Male
Capricorn
72
61
Oracle
Sing
Caution
Equip Armor
Move+1

Cypress Rod

Holy Miter
Reflect Mail
Rubber Shoes

Poison, Spell Absorb, Zombie, Silence Song, Blind Rage, Paralyze
Angel Song, Life Song, Magic Song, Hydra Pit



HASTERIOUS
Monster
Taurus
58
67
Tiamat










Mao420
Female
Taurus
79
71
Mediator
Punch Art
Brave Save
Equip Armor
Lava Walking

Papyrus Codex

Leather Helmet
Plate Mail
108 Gems

Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Wave Fist, Secret Fist, Revive



NicoSavoy
Male
Aquarius
48
78
Archer
Jump
Sunken State
Doublehand
Move-HP Up

Hunting Bow

Diamond Helmet
Judo Outfit
Feather Mantle

Charge+1, Charge+2, Charge+4, Charge+5, Charge+7, Charge+20
Level Jump8, Vertical Jump8

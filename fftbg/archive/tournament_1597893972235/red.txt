Player: !Red
Team: Red Team
Palettes: Red/Brown



Skillomono
Male
Aries
61
54
Geomancer
Summon Magic
Arrow Guard
Attack UP
Move-MP Up

Iron Sword
Mythril Shield
Green Beret
Leather Outfit
Defense Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Shiva, Bahamut, Lich



Fenaen
Female
Aries
56
76
Samurai
Talk Skill
Brave Save
Doublehand
Jump+1

Heaven's Cloud

Platinum Helmet
Wizard Robe
Dracula Mantle

Kiyomori, Muramasa, Kikuichimoji
Refute



Reinoe
Male
Aries
63
64
Knight
Draw Out
Blade Grasp
Defend
Waterbreathing

Platinum Sword
Hero Shield
Diamond Helmet
Crystal Mail
Power Wrist

Shield Break, Mind Break, Dark Sword
Asura, Koutetsu, Heaven's Cloud, Kiyomori



Aldrammech
Female
Leo
65
56
Lancer
Item
Counter Flood
Throw Item
Fly

Mythril Spear
Diamond Shield
Cross Helmet
Wizard Robe
Red Shoes

Level Jump5, Vertical Jump2
Potion, Hi-Potion, X-Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



BoneMiser
Male
Cancer
70
54
Samurai
Battle Skill
MP Restore
Doublehand
Jump+1

Chirijiraden

Grand Helmet
Crystal Mail
Germinas Boots

Asura, Heaven's Cloud, Kiyomori
Head Break, Magic Break, Night Sword



Randgridr
Female
Sagittarius
57
78
Lancer
Summon Magic
Counter Flood
Doublehand
Move-MP Up

Dragon Whisker

Leather Helmet
Plate Mail
Genji Gauntlet

Level Jump5, Vertical Jump8
Moogle, Ifrit, Titan, Carbunkle, Odin, Cyclops



Meta Five
Male
Cancer
42
81
Summoner
Punch Art
Counter
Concentrate
Fly

Dragon Rod

Holy Miter
Black Robe
Red Shoes

Moogle, Ifrit, Titan, Golem, Carbunkle, Bahamut, Fairy, Lich
Pummel, Secret Fist, Purification, Revive



Seaweed B
Female
Cancer
69
41
Mediator
Steal
Distribute
Dual Wield
Move+1

Papyrus Codex
Battle Folio
Flash Hat
Black Costume
108 Gems

Praise, Preach, Insult, Mimic Daravon
Gil Taking, Steal Heart, Arm Aim

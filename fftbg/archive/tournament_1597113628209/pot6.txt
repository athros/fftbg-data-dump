Final Bets: black - 8 bets for 4,395G (13.2%, x6.60); purple - 10 bets for 29,026G (86.8%, x0.15)

black bets:
SkylerBunny: 2,924G (66.5%, 5,735G)
ko2q: 404G (9.2%, 404G)
getthemoneyz: 366G (8.3%, 1,593,895G)
AllInBot: 200G (4.6%, 200G)
datadrivenbot: 200G (4.6%, 58,626G)
gorgewall: 101G (2.3%, 5,001G)
MinBetBot: 100G (2.3%, 23,160G)
xBizzy: 100G (2.3%, 1,265G)

purple bets:
Powermhero: 18,200G (62.7%, 18,200G)
TheDeeyo: 5,394G (18.6%, 10,577G)
Drusiform: 2,000G (6.9%, 19,314G)
ColetteMSLP: 1,000G (3.4%, 29,338G)
BirbBrainsBot: 1,000G (3.4%, 194,342G)
CosmicTactician: 500G (1.7%, 5,937G)
skillomono: 400G (1.4%, 3,735G)
zenlion: 228G (0.8%, 5,612G)
latebit: 204G (0.7%, 15,797G)
RunicMagus: 100G (0.3%, 5,344G)

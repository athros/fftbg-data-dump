Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nizaha
Male
Gemini
40
59
Chemist
Basic Skill
Hamedo
Magic Attack UP
Retreat

Stone Gun

Holy Miter
Leather Outfit
Small Mantle

Potion, Ether, Hi-Ether, Elixir, Eye Drop, Holy Water, Remedy, Phoenix Down
Tickle, Yell, Scream



StridentSong
Female
Gemini
80
79
Chemist
Jump
Sunken State
Concentrate
Waterwalking

Mythril Knife

Twist Headband
Earth Clothes
Power Wrist

Potion, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Level Jump8, Vertical Jump4



Snkey
Female
Leo
75
68
Dancer
White Magic
Critical Quick
Equip Knife
Jump+3

Ryozan Silk

Headgear
Chain Vest
Dracula Mantle

Slow Dance, Polka Polka, Nameless Dance, Last Dance, Obsidian Blade
Cure 3, Cure 4, Reraise, Protect, Protect 2, Shell, Shell 2, Esuna



DeadBart
Female
Libra
59
62
Knight
Punch Art
Counter
Doublehand
Levitate

Defender

Circlet
Carabini Mail
Vanish Mantle

Weapon Break, Power Break, Justice Sword
Earth Slash, Purification, Chakra

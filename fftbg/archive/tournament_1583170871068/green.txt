Player: !Green
Team: Green Team
Palettes: Green/White



Verxl
Male
Leo
53
78
Bard
Basic Skill
Counter Magic
Equip Shield
Retreat

Fairy Harp
Crystal Shield
Golden Hairpin
Gold Armor
Salty Rage

Battle Song, Last Song
Cheer Up, Wish



Kellios11
Male
Sagittarius
71
54
Wizard
Draw Out
Catch
Halve MP
Move+2

Assassin Dagger

Headgear
Chain Vest
Dracula Mantle

Fire, Fire 4, Bolt 3, Empower
Kiyomori, Kikuichimoji



Dropkickchannel
Male
Taurus
68
43
Wizard
Steal
Parry
Equip Armor
Retreat

Ice Rod

Thief Hat
Genji Armor
108 Gems

Fire, Fire 4, Ice 3, Ice 4
Steal Accessory



Coyowijuju
Female
Taurus
78
65
Samurai
Basic Skill
MA Save
Equip Armor
Retreat

Bizen Boat

Green Beret
Genji Armor
Magic Gauntlet

Asura, Murasame, Kiyomori
Heal, Yell

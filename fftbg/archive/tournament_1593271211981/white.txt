Player: !White
Team: White Team
Palettes: White/Blue



Lawnboxer
Male
Capricorn
62
61
Mime

Counter Tackle
Equip Shield
Fly


Escutcheon
Ribbon
Mystic Vest
Rubber Shoes

Mimic




Fenaen
Monster
Aries
45
76
Serpentarius










RampagingRobot
Male
Scorpio
52
47
Oracle
Basic Skill
Abandon
Equip Armor
Waterwalking

Bestiary

Triangle Hat
Carabini Mail
Feather Mantle

Life Drain, Silence Song, Blind Rage, Dispel Magic, Dark Holy
Heal, Tickle, Wish



Finewax
Male
Leo
48
77
Wizard
Summon Magic
HP Restore
Secret Hunt
Move+2

Air Knife

Black Hood
Light Robe
Reflect Ring

Fire, Fire 3, Bolt, Bolt 2, Bolt 4, Ice 3, Frog
Moogle, Ifrit, Golem, Fairy

Player: !Green
Team: Green Team
Palettes: Green/White



Meta Five
Monster
Aquarius
68
61
Cockatrice










Lastly
Male
Capricorn
77
60
Monk
Elemental
Counter
Defend
Jump+2



Golden Hairpin
Brigandine
Red Shoes

Pummel, Wave Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



O Heyno
Monster
Aquarius
76
75
Apanda










Baron Von Scrub
Female
Leo
62
72
Squire
Item
Abandon
Martial Arts
Retreat


Escutcheon
Red Hood
Reflect Mail
Feather Mantle

Accumulate, Heal, Tickle
Potion, X-Potion, Eye Drop, Holy Water, Remedy

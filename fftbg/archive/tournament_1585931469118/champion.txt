Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Bryon W
Male
Leo
57
65
Ninja
Steal
Meatbone Slash
Attack UP
Waterbreathing

Assassin Dagger
Short Edge
Twist Headband
Black Costume
Rubber Shoes

Shuriken, Dictionary
Steal Accessory, Arm Aim



R Raynos
Male
Serpentarius
68
65
Lancer
Charge
Caution
Defense UP
Move+1

Holy Lance
Venetian Shield
Diamond Helmet
Linen Cuirass
Cursed Ring

Level Jump5, Vertical Jump8
Charge+5, Charge+10



Herrshaun
Male
Leo
61
55
Priest
Black Magic
Dragon Spirit
Concentrate
Jump+2

White Staff

Triangle Hat
Mystic Vest
Feather Mantle

Cure 2, Raise, Shell 2, Esuna, Holy
Fire 2, Bolt 2, Bolt 3, Ice, Ice 4



NovaKnight21
Female
Sagittarius
67
77
Time Mage
Jump
Catch
Short Charge
Ignore Height

Rainbow Staff

Golden Hairpin
Power Sleeve
Defense Armlet

Haste, Slow, Slow 2, Stop, Quick, Demi
Level Jump2, Vertical Jump5

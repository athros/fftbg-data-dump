Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fozzington
Female
Scorpio
61
60
Chemist
Throw
Damage Split
Magic Attack UP
Levitate

Star Bag

Flash Hat
Adaman Vest
Genji Gauntlet

Potion, Hi-Ether, Eye Drop, Phoenix Down
Shuriken, Ninja Sword



Solomongrundy85
Female
Taurus
70
75
Thief
Summon Magic
Damage Split
Attack UP
Move+3

Nagrarock

Black Hood
Earth Clothes
Reflect Ring

Steal Helmet, Steal Shield, Arm Aim
Moogle, Fairy



Qexec
Female
Sagittarius
53
70
Priest
Item
Mana Shield
Beastmaster
Swim

White Staff

Leather Hat
Wizard Robe
Defense Armlet

Raise, Raise 2, Reraise, Esuna
Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down



Kuroda222
Male
Sagittarius
57
61
Archer
Throw
Catch
Doublehand
Jump+2

Night Killer

Leather Hat
Secret Clothes
Angel Ring

Charge+1, Charge+10
Shuriken, Bomb

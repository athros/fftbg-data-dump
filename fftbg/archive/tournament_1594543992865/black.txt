Player: !Black
Team: Black Team
Palettes: Black/Red



Choco Joe
Male
Leo
48
61
Ninja
Talk Skill
Sunken State
Equip Shield
Move+2

Sasuke Knife
Platinum Shield
Green Beret
Chain Vest
Magic Gauntlet

Bomb, Staff
Invitation, Praise, Threaten, Preach, Insult, Rehabilitate



Furrytomahawkk
Female
Taurus
72
80
Knight
Jump
Parry
Secret Hunt
Ignore Terrain

Ancient Sword
Buckler
Barbuta
White Robe
Small Mantle

Head Break, Armor Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword
Level Jump8, Vertical Jump8



Mysteriousdewd
Male
Aquarius
76
61
Bard
Throw
Counter Tackle
Equip Axe
Jump+2

Mace of Zeus

Feather Hat
Chain Vest
Power Wrist

Angel Song, Life Song, Battle Song, Last Song
Shuriken, Sword, Ninja Sword



Powermhero
Female
Cancer
83
79
Priest
Dance
Absorb Used MP
Secret Hunt
Retreat

Rainbow Staff

Flash Hat
Light Robe
Wizard Mantle

Cure 3, Cure 4, Raise, Raise 2, Regen, Shell, Esuna, Holy
Witch Hunt, Polka Polka, Void Storage, Nether Demon, Dragon Pit

Final Bets: purple - 7 bets for 6,459G (61.2%, x0.63); brown - 10 bets for 4,100G (38.8%, x1.58)

purple bets:
NicoSavoy: 2,000G (31.0%, 190,713G)
prince_rogers_nelson_: 1,575G (24.4%, 1,575G)
cam_ATS: 1,111G (17.2%, 47,912G)
BirbBrainsBot: 1,000G (15.5%, 76,394G)
byrdturbo: 333G (5.2%, 27,707G)
dantayystv: 240G (3.7%, 240G)
KasugaiRoastedPeas: 200G (3.1%, 19,727G)

brown bets:
reinoe: 1,000G (24.4%, 30,133G)
Smokegiant: 754G (18.4%, 754G)
TheMurkGnome: 748G (18.2%, 1,496G)
getthemoneyz: 528G (12.9%, 1,269,387G)
DamnThatShark: 304G (7.4%, 304G)
Mysteriousdewd: 228G (5.6%, 228G)
furrytomahawkk: 212G (5.2%, 212G)
datadrivenbot: 200G (4.9%, 62,048G)
Lydian_C: 121G (3.0%, 95,882G)
TheMM42: 5G (0.1%, 11,659G)

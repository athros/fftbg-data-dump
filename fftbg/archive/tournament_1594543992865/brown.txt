Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Male
Pisces
55
50
Mime

Counter
Equip Shield
Levitate


Round Shield
Green Beret
Judo Outfit
Spike Shoes

Mimic




Lowlf
Male
Cancer
67
61
Monk
Basic Skill
Sunken State
Dual Wield
Move+1



Twist Headband
Mystic Vest
Wizard Mantle

Spin Fist, Purification, Revive, Seal Evil
Throw Stone, Heal, Cheer Up, Fury



DamnThatShark
Male
Capricorn
57
73
Monk
Talk Skill
HP Restore
Beastmaster
Teleport



Golden Hairpin
Clothes
Genji Gauntlet

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Praise, Death Sentence, Negotiate, Refute



Songdeh
Female
Pisces
65
70
Oracle
Basic Skill
Hamedo
Magic Defense UP
Jump+2

Battle Bamboo

Headgear
Rubber Costume
Feather Mantle

Poison, Spell Absorb, Paralyze, Sleep, Petrify
Dash, Heal, Tickle, Fury

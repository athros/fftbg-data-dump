Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Kenaeisamazing
Male
Capricorn
61
64
Chemist
Time Magic
Counter Magic
Magic Defense UP
Levitate

Mythril Gun

Headgear
Chain Vest
Angel Ring

X-Potion, Ether, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Slow, Slow 2, Float, Reflect, Quick, Demi



Fftbchamp
Female
Libra
74
68
Dancer
Draw Out
Mana Shield
Equip Gun
Waterwalking

Fairy Harp

Green Beret
Adaman Vest
Angel Ring

Witch Hunt, Slow Dance, Last Dance
Asura, Bizen Boat, Heaven's Cloud, Kiyomori



Omegasuspekt
Male
Aries
56
68
Ninja
Steal
Counter Tackle
Equip Shield
Move+3

Assassin Dagger
Hero Shield
Golden Hairpin
Mystic Vest
Angel Ring

Bomb, Hammer
Steal Shield



BuffaloCrunch
Male
Sagittarius
56
59
Geomancer
Basic Skill
Counter Flood
Attack UP
Teleport

Murasame
Crystal Shield
Feather Hat
Black Robe
Wizard Mantle

Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Heal

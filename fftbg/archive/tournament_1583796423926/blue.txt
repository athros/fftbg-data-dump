Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Aldrammech
Male
Sagittarius
64
46
Mime

Caution
Equip Armor
Levitate



Black Hood
Genji Armor
N-Kai Armlet

Mimic




Bryon W
Male
Gemini
44
43
Summoner
Sing
Counter Tackle
Dual Wield
Lava Walking

Poison Rod
White Staff
Black Hood
Clothes
Defense Ring

Shiva, Ramuh, Golem, Carbunkle, Odin, Silf, Fairy
Diamond Blade, Space Storage



AniZero
Male
Pisces
77
79
Knight
Black Magic
Catch
Equip Polearm
Swim

Octagon Rod
Aegis Shield
Bronze Helmet
Leather Armor
108 Gems

Weapon Break, Justice Sword
Fire, Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 2, Empower



Buddychrist10
Male
Virgo
58
40
Summoner
Basic Skill
Critical Quick
Magic Attack UP
Waterbreathing

Thunder Rod

Red Hood
Linen Robe
Magic Ring

Golem, Carbunkle, Silf
Throw Stone, Heal, Yell

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Shalloween
Monster
Sagittarius
43
54
Blue Dragon










EmmaEnema
Female
Cancer
46
74
Time Mage
Steal
Auto Potion
Equip Sword
Retreat

Kiyomori

Twist Headband
Chameleon Robe
108 Gems

Haste, Slow, Slow 2, Stabilize Time
Steal Status



LionheartLuso
Male
Taurus
38
69
Squire
Sing
Parry
Concentrate
Ignore Height

Hunting Bow
Bronze Shield
Cross Helmet
Power Sleeve
N-Kai Armlet

Accumulate, Yell, Cheer Up, Ultima
Angel Song, Battle Song, Last Song, Diamond Blade, Hydra Pit



Lanshaft
Female
Pisces
52
59
Time Mage
Steal
Mana Shield
Dual Wield
Move+2

Gold Staff
Wizard Staff
Green Beret
Linen Robe
Sprint Shoes

Haste, Slow, Quick, Demi
Gil Taking, Steal Heart, Steal Helmet

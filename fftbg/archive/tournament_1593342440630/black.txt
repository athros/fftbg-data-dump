Player: !Black
Team: Black Team
Palettes: Black/Red



Ominnous
Female
Aquarius
56
47
Summoner
Battle Skill
Brave Save
Short Charge
Jump+1

Dragon Rod

Headgear
White Robe
Magic Gauntlet

Shiva, Ifrit, Golem, Leviathan, Silf, Fairy
Head Break, Weapon Break, Justice Sword, Night Sword



Just Here2
Male
Gemini
72
69
Mime

Faith Save
Attack UP
Waterbreathing



Barette
Wizard Outfit
Elf Mantle

Mimic




E Ballard
Male
Scorpio
46
69
Mediator
Black Magic
Counter
Equip Sword
Swim

Blood Sword

Feather Hat
Black Robe
Setiemson

Praise, Mimic Daravon, Refute
Fire, Death, Flare



Didi
Female
Gemini
49
69
Thief
Talk Skill
PA Save
Equip Axe
Swim

Morning Star

Leather Hat
Black Costume
Jade Armlet

Gil Taking, Steal Shield
Threaten, Preach, Solution, Refute

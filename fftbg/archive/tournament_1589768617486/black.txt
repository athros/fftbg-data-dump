Player: !Black
Team: Black Team
Palettes: Black/Red



Nekross92
Female
Scorpio
61
68
Chemist
Dance
Mana Shield
Defend
Move-MP Up

Mythril Knife

Green Beret
Chain Vest
Elf Mantle

Potion, Hi-Ether, Antidote, Echo Grass, Remedy, Phoenix Down
Witch Hunt, Slow Dance, Polka Polka, Disillusion, Last Dance



NIghtdew14
Female
Pisces
68
51
Monk
Basic Skill
Counter Tackle
Equip Armor
Ignore Terrain



Black Hood
Platinum Armor
Germinas Boots

Pummel, Secret Fist, Purification, Revive
Throw Stone, Tickle, Yell, Fury, Wish, Scream



Eudes89
Female
Taurus
57
72
Squire
Time Magic
Regenerator
Attack UP
Levitate

Slasher
Mythril Shield
Twist Headband
Leather Armor
N-Kai Armlet

Accumulate, Tickle, Yell, Wish
Haste 2, Slow, Slow 2, Immobilize, Reflect, Demi 2



Strifu
Male
Aquarius
65
61
Priest
Basic Skill
Mana Shield
Equip Sword
Jump+2

Kiyomori

Holy Miter
Linen Robe
Cherche

Cure 3, Raise, Raise 2, Shell, Shell 2, Wall, Esuna, Holy
Dash, Throw Stone, Scream

Player: !Green
Team: Green Team
Palettes: Green/White



Gorgewall
Monster
Leo
40
61
Bomb










Reinoe
Male
Serpentarius
54
44
Oracle
Item
Counter
Throw Item
Move-MP Up

Iron Fan

Feather Hat
Black Costume
Red Shoes

Blind, Life Drain, Confusion Song, Sleep
Elixir, Holy Water, Remedy, Phoenix Down



Gelwain
Female
Libra
70
80
Squire
Summon Magic
Arrow Guard
Doublehand
Move-HP Up

Coral Sword

Twist Headband
Power Sleeve
N-Kai Armlet

Accumulate, Yell, Wish
Moogle, Titan, Carbunkle, Bahamut, Odin, Silf



Holyonline
Male
Aquarius
67
64
Calculator
Time Magic
Brave Save
Equip Sword
Retreat

Kikuichimoji

Cachusha
Mystic Vest
Feather Boots

Height, Prime Number, 5, 4, 3
Haste, Haste 2, Slow 2, Reflect, Quick

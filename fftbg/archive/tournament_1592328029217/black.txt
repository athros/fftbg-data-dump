Player: !Black
Team: Black Team
Palettes: Black/Red



Joey1995456
Female
Capricorn
57
73
Knight
Yin Yang Magic
Parry
Short Charge
Move+3

Ragnarok
Genji Shield
Platinum Helmet
Reflect Mail
Reflect Ring

Armor Break, Power Break, Mind Break
Spell Absorb, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze, Dark Holy



Mosses76
Female
Pisces
52
60
Squire
Time Magic
Distribute
Equip Armor
Move+1

Dagger
Hero Shield
Cross Helmet
Linen Robe
Red Shoes

Dash, Throw Stone, Heal, Yell, Fury, Wish
Haste, Slow, Stop, Immobilize, Quick, Stabilize Time



Butterbelljedi
Female
Cancer
44
68
Mediator
Item
Counter Tackle
Equip Knife
Retreat

Spell Edge

Feather Hat
Silk Robe
Reflect Ring

Persuade, Preach, Insult, Mimic Daravon, Refute
Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



DLJuggernaut
Male
Aries
42
72
Bard
Item
Abandon
Throw Item
Jump+3

Fairy Harp

Golden Hairpin
Mythril Armor
Elf Mantle

Angel Song, Battle Song, Nameless Song, Last Song, Sky Demon, Hydra Pit
Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down

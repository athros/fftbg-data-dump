Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Shalloween
Female
Aries
58
56
Summoner
Battle Skill
Critical Quick
Short Charge
Levitate

Flame Rod

Red Hood
Silk Robe
Bracer

Moogle, Ifrit, Carbunkle, Odin, Salamander
Speed Break, Power Break, Mind Break



Coralreeferz
Male
Aquarius
80
43
Monk
Draw Out
Sunken State
Equip Bow
Jump+1

Ultimus Bow

Barette
Power Sleeve
Power Wrist

Pummel, Secret Fist, Purification, Revive, Seal Evil
Koutetsu, Muramasa, Kikuichimoji



PKayge
Male
Sagittarius
58
38
Ninja
Item
PA Save
Attack UP
Waterwalking

Flail
Flame Whip
Feather Hat
Earth Clothes
Wizard Mantle

Knife, Wand
Potion, X-Potion, Ether, Hi-Ether, Antidote, Remedy, Phoenix Down



Mirapoix
Male
Scorpio
44
41
Monk
Basic Skill
MA Save
Dual Wield
Teleport



Twist Headband
Chain Vest
Chantage

Pummel, Wave Fist, Purification, Chakra, Revive
Dash, Throw Stone, Heal, Tickle, Wish

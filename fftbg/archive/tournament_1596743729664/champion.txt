Player: !zChamp
Team: Champion Team
Palettes: White/Blue



PoroTact
Male
Libra
51
55
Thief
Draw Out
Regenerator
Halve MP
Move-MP Up

Mythril Sword

Black Hood
Mythril Vest
Angel Ring

Steal Heart, Steal Armor, Steal Shield
Koutetsu, Bizen Boat



Nhammen
Male
Libra
63
51
Knight
Steal
PA Save
Beastmaster
Jump+2

Defender
Crystal Shield
Mythril Helmet
Plate Mail
Sprint Shoes

Head Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Night Sword
Steal Armor



Evdoggity
Male
Gemini
46
46
Squire
Jump
Counter
Defense UP
Jump+1

Hunting Bow
Ice Shield
Golden Hairpin
Diamond Armor
Diamond Armlet

Dash, Throw Stone, Heal, Yell, Fury
Level Jump5, Vertical Jump7



Powergems
Female
Cancer
64
61
Monk
Summon Magic
Critical Quick
Attack UP
Waterwalking



Red Hood
Mystic Vest
Magic Gauntlet

Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Moogle, Titan, Carbunkle, Odin

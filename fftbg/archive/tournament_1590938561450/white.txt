Player: !White
Team: White Team
Palettes: White/Blue



Lawnboxer
Male
Leo
57
75
Lancer
Charge
Counter Tackle
Concentrate
Waterbreathing

Holy Lance
Diamond Shield
Genji Helmet
Mythril Armor
Angel Ring

Level Jump2, Vertical Jump2
Charge+1, Charge+3



Nifboy
Male
Cancer
73
77
Geomancer
Battle Skill
MA Save
Concentrate
Move+3

Bizen Boat
Genji Shield
Golden Hairpin
Wizard Outfit
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Shield Break, Magic Break, Power Break, Stasis Sword



Lionhermit
Female
Taurus
75
52
Time Mage
Charge
Dragon Spirit
Equip Shield
Move-HP Up

Rainbow Staff
Ice Shield
Barette
Black Costume
Elf Mantle

Haste 2, Slow, Float, Stabilize Time
Charge+1, Charge+2, Charge+3, Charge+4, Charge+20



Run With Stone GUNs
Male
Capricorn
70
78
Mediator
Summon Magic
Caution
Dual Wield
Swim

Battle Folio
Battle Folio
Leather Hat
Chain Vest
Diamond Armlet

Persuade, Rehabilitate
Moogle, Shiva, Carbunkle, Salamander, Silf, Fairy

Player: !White
Team: White Team
Palettes: White/Blue



Neerrm
Male
Aquarius
70
49
Oracle
Black Magic
Damage Split
Secret Hunt
Ignore Height

Iron Fan

Leather Hat
Rubber Costume
Wizard Mantle

Blind, Poison, Spell Absorb, Pray Faith, Dispel Magic, Sleep, Petrify
Fire, Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Empower



Reddwind
Male
Sagittarius
80
53
Knight
Black Magic
Counter Tackle
Concentrate
Jump+1

Defender
Ice Shield
Circlet
Linen Cuirass
Battle Boots

Head Break, Shield Break, Weapon Break, Speed Break, Stasis Sword
Fire, Fire 2, Fire 3, Ice, Ice 2, Empower



CITOtheMOSQUITO
Female
Leo
73
47
Chemist
Summon Magic
Distribute
Equip Polearm
Waterwalking

Obelisk

Red Hood
Brigandine
Small Mantle

Potion, Soft, Phoenix Down
Moogle, Golem, Carbunkle, Silf, Fairy



UmaiJam
Female
Aquarius
45
79
Geomancer
Battle Skill
Dragon Spirit
Long Status
Move+1

Kikuichimoji
Flame Shield
Flash Hat
Wizard Robe
Magic Gauntlet

Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Weapon Break, Magic Break, Power Break, Night Sword

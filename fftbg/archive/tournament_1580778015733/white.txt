Player: !White
Team: White Team
Palettes: White/Blue



Brondius
Female
Leo
53
58
Samurai
Throw
Critical Quick
Magic Attack UP
Retreat

Muramasa

Platinum Helmet
Chain Mail
Magic Ring

Koutetsu, Bizen Boat, Kikuichimoji
Shuriken, Hammer, Wand



Bryan792
Male
Taurus
65
82
Ninja
Basic Skill
Parry
Equip Gun
Waterwalking

Sasuke Knife
Flame Whip
Green Beret
Wizard Outfit
Small Mantle

Shuriken, Ninja Sword, Stick, Dictionary
Dash, Heal



Laserman1000
Male
Taurus
76
55
Oracle
Steal
MP Restore
Equip Gun
Jump+2

Glacier Gun

Black Hood
Leather Outfit
Jade Armlet

Poison, Zombie, Confusion Song
Gil Taking, Steal Armor, Steal Accessory, Arm Aim



Howplausible
Female
Pisces
42
41
Priest
Jump
Earplug
Beastmaster
Move+1

Rainbow Staff

Triangle Hat
Secret Clothes
Cursed Ring

Cure 3, Raise, Reraise, Wall, Esuna, Holy
Level Jump4, Vertical Jump5

Player: !Black
Team: Black Team
Palettes: Black/Red



Gelwain
Female
Aquarius
75
69
Monk
Talk Skill
Counter
Attack UP
Waterbreathing



Black Hood
Black Costume
Feather Boots

Secret Fist, Purification, Chakra, Seal Evil
Death Sentence, Insult, Refute



EnemyController
Monster
Cancer
47
44
Wild Boar










Lijarkh
Male
Cancer
76
77
Monk
Battle Skill
Counter Flood
Attack UP
Retreat



Red Hood
Mythril Vest
Wizard Mantle

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Armor Break, Power Break, Stasis Sword, Surging Sword



Roofiepops
Male
Aries
48
48
Ninja
Charge
Damage Split
Beastmaster
Retreat

Kunai
Ninja Edge
Thief Hat
Adaman Vest
Spike Shoes

Shuriken, Knife, Axe
Charge+1, Charge+2

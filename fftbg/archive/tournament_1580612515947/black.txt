Player: !Black
Team: Black Team
Palettes: Black/Red



JDogg2K4
Male
Taurus
45
42
Monk
Sing
Blade Grasp
Equip Knife
Jump+1

Flame Rod

Red Hood
Secret Clothes
Diamond Armlet

Wave Fist, Secret Fist, Purification, Revive
Cheer Song, Magic Song, Last Song



SamaelMerihem
Male
Libra
59
53
Archer
Punch Art
Earplug
Martial Arts
Jump+2

Perseus Bow

Leather Hat
Clothes
Rubber Shoes

Charge+1, Charge+2
Purification, Revive, Seal Evil



Omegasuspekt
Male
Pisces
58
49
Monk
Yin Yang Magic
PA Save
Attack UP
Lava Walking



Headgear
Adaman Vest
Magic Ring

Spin Fist, Purification, Revive, Seal Evil
Spell Absorb, Zombie, Foxbird, Paralyze, Sleep



Arrivan
Female
Gemini
71
61
Samurai
Summon Magic
Damage Split
Dual Wield
Move-MP Up

Spear
Gungnir
Iron Helmet
Carabini Mail
Germinas Boots

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji
Moogle, Shiva, Titan, Golem, Carbunkle, Bahamut, Salamander, Fairy, Lich

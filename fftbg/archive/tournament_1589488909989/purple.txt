Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaychDub
Male
Aquarius
52
43
Calculator
Black Magic
Brave Up
Equip Bow
Ignore Terrain

Poison Bow

Feather Hat
Wizard Outfit
Cherche

CT, Height, 5, 4, 3
Fire 3, Ice, Ice 4, Flare



DrAntiSocial
Male
Virgo
61
68
Knight
Elemental
Auto Potion
Equip Knife
Move-HP Up

Cultist Dagger
Aegis Shield
Crystal Helmet
White Robe
Battle Boots

Armor Break, Surging Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



KasugaiRoastedPeas
Female
Taurus
56
79
Archer
Elemental
Sunken State
Equip Armor
Lava Walking

Romanda Gun
Crystal Shield
Genji Helmet
Carabini Mail
Feather Mantle

Charge+2
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Zeroroute
Male
Libra
51
45
Wizard
Throw
Speed Save
Defend
Levitate

Cultist Dagger

Holy Miter
Linen Robe
Leather Mantle

Fire 2, Fire 4, Bolt 2, Bolt 3, Flare
Shuriken

Player: !Black
Team: Black Team
Palettes: Black/Red



DLJuggernaut
Monster
Aquarius
53
77
Black Goblin










Dymntd
Female
Scorpio
78
42
Time Mage
Draw Out
Mana Shield
Short Charge
Waterwalking

Octagon Rod

Flash Hat
Wizard Outfit
Genji Gauntlet

Haste, Reflect, Stabilize Time
Koutetsu, Bizen Boat, Kiyomori



ZombiFrancis
Female
Taurus
78
74
Geomancer
Jump
Regenerator
Halve MP
Retreat

Sleep Sword
Ice Shield
Flash Hat
Judo Outfit
Leather Mantle

Pitfall, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Level Jump3, Vertical Jump8



Oobs56
Female
Sagittarius
42
64
Wizard
Throw
Parry
Secret Hunt
Move+3

Ice Rod

Triangle Hat
Mythril Vest
Magic Ring

Bolt 4, Ice 3
Shuriken, Bomb, Knife

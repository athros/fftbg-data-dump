Player: !Green
Team: Green Team
Palettes: Green/White



Shalloween
Female
Pisces
67
67
Calculator
Black Magic
Damage Split
Short Charge
Retreat

Papyrus Codex

Headgear
Robe of Lords
Rubber Shoes

CT, Height, Prime Number, 5, 4, 3
Fire 3, Bolt 4, Frog



Galkife
Male
Scorpio
54
41
Samurai
Yin Yang Magic
Catch
Defense UP
Move+2

Heaven's Cloud

Leather Helmet
Chameleon Robe
Magic Gauntlet

Asura, Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Life Drain, Doubt Faith, Zombie, Silence Song, Paralyze



NovaKnight21
Female
Sagittarius
54
46
Dancer
Black Magic
Abandon
Defense UP
Levitate

Ryozan Silk

Flash Hat
Adaman Vest
Small Mantle

Disillusion, Obsidian Blade
Fire, Ice 3, Death



Bongomon7
Male
Pisces
43
45
Archer
Battle Skill
MA Save
Doublehand
Move+3

Poison Bow

Golden Hairpin
Black Costume
108 Gems

Charge+1, Charge+2, Charge+4
Head Break, Armor Break, Shield Break, Weapon Break, Mind Break, Stasis Sword, Justice Sword

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Prince Rogers Nelson
Monster
Leo
61
79
Bomb










Lythe Caraker
Male
Cancer
51
66
Mime

Dragon Spirit
Monster Talk
Teleport



Feather Hat
Brigandine
Power Wrist

Mimic




HaateXIII
Female
Capricorn
61
61
Mediator
Dance
Brave Save
Equip Armor
Teleport

Papyrus Codex

Cross Helmet
Bronze Armor
Battle Boots

Praise, Insult
Slow Dance, Disillusion, Nameless Dance, Last Dance, Obsidian Blade, Void Storage



Moonliquor
Female
Leo
62
67
Summoner
Yin Yang Magic
Mana Shield
Short Charge
Jump+2

Ice Rod

Headgear
Silk Robe
Vanish Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Odin
Life Drain, Zombie, Silence Song, Dispel Magic, Sleep

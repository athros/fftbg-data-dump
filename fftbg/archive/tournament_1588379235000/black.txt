Player: !Black
Team: Black Team
Palettes: Black/Red



DaveStrider55
Female
Libra
44
77
Geomancer
White Magic
Regenerator
Magic Defense UP
Move-MP Up

Giant Axe
Mythril Shield
Feather Hat
Silk Robe
Wizard Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind
Raise, Regen, Shell, Shell 2, Wall, Esuna, Holy



Hales Bopp It
Male
Cancer
57
43
Summoner
Sing
Auto Potion
Doublehand
Ignore Height

Wizard Staff

Headgear
Mystic Vest
Magic Gauntlet

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Salamander, Silf, Fairy, Cyclops
Angel Song, Cheer Song, Battle Song, Space Storage



Tougou
Male
Scorpio
40
48
Archer
Talk Skill
Counter
Doublehand
Fly

Hunting Bow

Golden Hairpin
Mythril Vest
Rubber Shoes

Charge+7, Charge+10
Death Sentence, Insult, Negotiate, Refute



Hamborn
Female
Cancer
68
74
Lancer
Steal
Meatbone Slash
Equip Bow
Move+1

Gastrafitis
Mythril Shield
Barbuta
Linen Cuirass
Small Mantle

Level Jump8, Vertical Jump6
Steal Armor, Steal Weapon, Steal Accessory, Leg Aim

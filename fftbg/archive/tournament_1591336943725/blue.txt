Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kaidykat
Female
Libra
66
67
Monk
Black Magic
Counter
Equip Sword
Move-HP Up

Long Sword

Twist Headband
Mythril Vest
Defense Armlet

Purification, Chakra
Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 3, Frog



Ring Wyrm
Female
Virgo
58
47
Mediator
Battle Skill
Auto Potion
Equip Knife
Move-MP Up

Rod

Headgear
Light Robe
Dracula Mantle

Persuade, Praise, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Head Break, Armor Break, Power Break



Sinnyil2
Male
Taurus
51
44
Knight
Black Magic
Abandon
Equip Polearm
Teleport 2

Cashmere
Escutcheon
Barbuta
Bronze Armor
Bracer

Power Break, Night Sword
Fire 2, Bolt 3, Ice, Death



Kalabain
Male
Aries
47
50
Knight
Elemental
Mana Shield
Secret Hunt
Lava Walking

Giant Axe
Flame Shield
Iron Helmet
Platinum Armor
Reflect Ring

Weapon Break, Speed Break
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

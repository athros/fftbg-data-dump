Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Vorap
Female
Aquarius
77
56
Monk
Time Magic
Parry
Maintenance
Move+2



Triangle Hat
Clothes
Magic Ring

Pummel, Secret Fist, Purification, Seal Evil
Stop, Stabilize Time



RaIshtar
Male
Libra
72
69
Mime

Meatbone Slash
Martial Arts
Lava Walking



Feather Hat
Earth Clothes
Red Shoes

Mimic




Moocaotao
Male
Aquarius
53
71
Lancer
Sing
Absorb Used MP
Defend
Move+1

Mythril Spear
Round Shield
Platinum Helmet
Diamond Armor
Sprint Shoes

Level Jump4, Vertical Jump5
Life Song, Magic Song



Nohurty
Male
Sagittarius
57
46
Chemist
Draw Out
Arrow Guard
Magic Attack UP
Ignore Terrain

Star Bag

Flash Hat
Brigandine
108 Gems

Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Kikuichimoji

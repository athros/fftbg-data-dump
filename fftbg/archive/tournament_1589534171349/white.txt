Player: !White
Team: White Team
Palettes: White/Blue



Yasmosis
Monster
Aquarius
52
50
Archaic Demon










Foofermoofer
Female
Gemini
73
45
Squire
Time Magic
Counter
Defense UP
Jump+1

Slasher
Crystal Shield
Green Beret
Crystal Mail
Spike Shoes

Dash, Throw Stone, Heal, Tickle, Cheer Up, Wish
Haste 2, Stop, Reflect, Demi, Demi 2, Stabilize Time, Meteor



Thethorndog1
Male
Libra
66
81
Bard
White Magic
HP Restore
Short Status
Move-MP Up

Ramia Harp

Feather Hat
Black Costume
Power Wrist

Life Song, Cheer Song, Nameless Song, Diamond Blade, Sky Demon
Cure 4, Raise, Shell, Shell 2, Holy



Lionhermit
Female
Taurus
50
80
Wizard
Draw Out
Counter Magic
Equip Knife
Lava Walking

Short Edge

Cachusha
Silk Robe
Defense Ring

Fire 3, Fire 4, Empower, Frog, Flare
Bizen Boat, Heaven's Cloud, Kiyomori

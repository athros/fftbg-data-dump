Player: !Black
Team: Black Team
Palettes: Black/Red



JohnDuggins
Female
Aquarius
59
58
Calculator
Black Magic
Absorb Used MP
Halve MP
Move+3

Papyrus Codex

Leather Hat
Mystic Vest
Red Shoes

CT, 4
Fire 4, Ice 2, Ice 3, Frog



TheChainNerd
Male
Aquarius
53
50
Wizard
Time Magic
Regenerator
Halve MP
Retreat

Poison Rod

Leather Hat
Silk Robe
Feather Mantle

Bolt, Ice, Ice 3, Ice 4, Empower, Death
Haste, Haste 2, Slow, Reflect, Demi, Stabilize Time



TrueRhyme
Monster
Leo
55
71
Cockatrice










Zagorsek
Female
Libra
70
51
Dancer
Summon Magic
Meatbone Slash
Magic Defense UP
Move-HP Up

Persia

Golden Hairpin
Chameleon Robe
Salty Rage

Slow Dance, Polka Polka, Disillusion, Last Dance, Void Storage
Moogle, Ramuh, Ifrit, Carbunkle, Odin, Leviathan, Silf

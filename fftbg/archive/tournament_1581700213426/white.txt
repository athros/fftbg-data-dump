Player: !White
Team: White Team
Palettes: White/Blue



WalkerNash
Female
Scorpio
62
57
Wizard
Yin Yang Magic
MA Save
Magic Attack UP
Jump+2

Mage Masher

Ribbon
Light Robe
Rubber Shoes

Fire 4, Ice, Ice 3
Blind, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Sleep



Tsuike
Female
Libra
60
50
Wizard
Draw Out
Counter
Short Charge
Move+1

Main Gauche

Green Beret
Black Robe
Small Mantle

Fire 2, Fire 4, Bolt 4, Ice, Ice 2, Ice 3
Murasame



ItotheCtotheE
Monster
Cancer
75
59
Red Chocobo










DrakeRowan
Male
Aquarius
41
73
Monk
Basic Skill
Speed Save
Equip Armor
Teleport



Crystal Helmet
Crystal Mail
Bracer

Revive, Seal Evil
Accumulate, Throw Stone, Heal, Tickle, Scream

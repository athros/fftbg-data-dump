Final Bets: green - 7 bets for 4,166G (51.3%, x0.95); yellow - 7 bets for 3,962G (48.7%, x1.05)

green bets:
CassiePhoenix: 1,497G (35.9%, 1,497G)
gorgewall: 1,360G (32.6%, 1,360G)
TheMM42: 500G (12.0%, 4,932G)
getthemoneyz: 306G (7.3%, 1,703,759G)
KasugaiRoastedPeas: 300G (7.2%, 3,327G)
BirbBrainsBot: 103G (2.5%, 117,663G)
AllInBot: 100G (2.4%, 100G)

yellow bets:
kaidykat: 1,814G (45.8%, 181,491G)
Lazarus_DS: 901G (22.7%, 901G)
SkylerBunny: 500G (12.6%, 3,658G)
CT_5_Holy: 300G (7.6%, 1,766G)
datadrivenbot: 200G (5.0%, 62,450G)
3ngag3: 147G (3.7%, 16,147G)
ko2q: 100G (2.5%, 3,047G)

Final Bets: white - 3 bets for 3,953G (55.4%, x0.80); black - 8 bets for 3,179G (44.6%, x1.24)

white bets:
gorgewall: 2,653G (67.1%, 2,653G)
BirbBrainsBot: 1,000G (25.3%, 117,761G)
TheMM42: 300G (7.6%, 5,408G)

black bets:
kaidykat: 1,796G (56.5%, 179,677G)
KasugaiRoastedPeas: 400G (12.6%, 3,612G)
datadrivenbot: 200G (6.3%, 62,250G)
CT_5_Holy: 200G (6.3%, 1,466G)
AllInBot: 195G (6.1%, 195G)
getthemoneyz: 180G (5.7%, 1,704,050G)
Lazarus_DS: 108G (3.4%, 108G)
ko2q: 100G (3.1%, 2,947G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Mr Kindaro
Female
Sagittarius
49
55
Oracle
Black Magic
Abandon
Long Status
Ignore Terrain

Battle Bamboo

Feather Hat
Judo Outfit
Rubber Shoes

Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Sleep
Fire 3, Bolt 4, Flare



Nifboy
Male
Sagittarius
60
48
Ninja
Basic Skill
Earplug
Secret Hunt
Waterbreathing

Kunai
Morning Star
Holy Miter
Wizard Outfit
Elf Mantle

Shuriken, Wand
Accumulate, Throw Stone, Heal, Cheer Up



Solomongrundy85
Female
Cancer
44
43
Dancer
Black Magic
Parry
Equip Bow
Move-MP Up

Poison Bow

Red Hood
Wizard Robe
Germinas Boots

Void Storage
Bolt, Bolt 4



Phi Sig
Male
Capricorn
56
50
Oracle
Talk Skill
MP Restore
Maintenance
Waterwalking

Musk Rod

Leather Hat
Chameleon Robe
Elf Mantle

Poison, Spell Absorb, Doubt Faith, Silence Song, Confusion Song, Sleep, Petrify
Persuade, Praise, Threaten, Solution, Death Sentence, Negotiate, Refute, Rehabilitate

Player: !Green
Team: Green Team
Palettes: Green/White



Killth3kid
Female
Aquarius
67
61
Chemist
Basic Skill
Catch
Secret Hunt
Levitate

Romanda Gun

Black Hood
Judo Outfit
Jade Armlet

Potion, X-Potion, Ether, Hi-Ether, Antidote, Holy Water, Phoenix Down
Throw Stone, Heal, Tickle, Cheer Up, Fury



SkylerBunny
Female
Aquarius
37
67
Archer
Punch Art
Counter Magic
Equip Polearm
Jump+3

Obelisk
Mythril Shield
Cachusha
Clothes
Reflect Ring

Charge+1, Charge+3, Charge+5, Charge+10
Secret Fist, Purification



HaychDub
Male
Leo
49
41
Squire
Item
MA Save
Magic Defense UP
Move-MP Up

Night Killer
Crystal Shield
Red Hood
Mystic Vest
N-Kai Armlet

Accumulate, Heal, Tickle, Yell
X-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft



ALY327
Male
Capricorn
63
64
Lancer
Summon Magic
Faith Save
Halve MP
Move-MP Up

Mythril Spear
Buckler
Mythril Helmet
Diamond Armor
Vanish Mantle

Level Jump2, Vertical Jump5
Carbunkle, Salamander, Silf, Fairy

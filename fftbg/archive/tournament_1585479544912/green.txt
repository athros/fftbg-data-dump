Player: !Green
Team: Green Team
Palettes: Green/White



ApplesauceBoss
Male
Virgo
67
68
Bard
Punch Art
Arrow Guard
Doublehand
Jump+2

Bloody Strings

Headgear
Carabini Mail
Rubber Shoes

Life Song, Diamond Blade, Space Storage
Pummel, Wave Fist, Revive



Volgrathemoose
Female
Scorpio
79
77
Lancer
Draw Out
MP Restore
Doublehand
Jump+3

Holy Lance

Bronze Helmet
Carabini Mail
Power Wrist

Level Jump3, Vertical Jump2
Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa



Go2sleepTV
Male
Sagittarius
49
78
Bard
Steal
Damage Split
Doublehand
Fly

Bloody Strings

Leather Hat
Mythril Vest
Setiemson

Angel Song, Nameless Song, Last Song, Space Storage
Steal Heart, Steal Helmet, Steal Armor, Leg Aim



Roqqqpsi
Male
Virgo
65
49
Mediator
Item
Dragon Spirit
Equip Shield
Levitate

Mythril Knife
Bronze Shield
Twist Headband
Chain Vest
Small Mantle

Invitation, Threaten, Mimic Daravon, Refute
Potion, Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down

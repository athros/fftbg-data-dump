Player: !Green
Team: Green Team
Palettes: Green/White



Gerynt
Female
Leo
58
73
Calculator
Imp Skill
Critical Quick
Maintenance
Fly

Papyrus Codex

Barbuta
Chain Mail
Defense Ring

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Beam, Look of Devil, Doom, Beam



SupahTree
Female
Taurus
45
80
Calculator
Bird Skill
Meatbone Slash
Equip Armor
Waterwalking

Thunder Rod

Twist Headband
Carabini Mail
Power Wrist

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



KiithsaTheGnome
Female
Aries
46
73
Calculator
Demon Skill
HP Restore
Equip Knife
Teleport

Ivory Rod

Black Hood
Chameleon Robe
108 Gems

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Ominnous
Male
Sagittarius
54
74
Calculator
Imp Skill
Meatbone Slash
Secret Hunt
Move-HP Up

Gokuu Rod

Golden Hairpin
Maximillian
Germinas Boots

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Beam, Look of Devil, Doom, Beam

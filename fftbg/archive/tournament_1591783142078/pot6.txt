Final Bets: white - 12 bets for 7,109G (59.0%, x0.69); brown - 8 bets for 4,932G (41.0%, x1.44)

white bets:
NovaKnight21: 2,859G (40.2%, 2,859G)
SkyridgeZero: 1,000G (14.1%, 28,700G)
helpimabug: 840G (11.8%, 840G)
HASTERIOUS: 569G (8.0%, 569G)
BlackFireUK: 500G (7.0%, 24,751G)
VolgraTheMoose: 500G (7.0%, 47,091G)
serperemagus: 240G (3.4%, 240G)
ar_tactic: 200G (2.8%, 62,668G)
gorgewall: 101G (1.4%, 11,406G)
AllInBot: 100G (1.4%, 100G)
lastly: 100G (1.4%, 16,460G)
datadrivenbot: 100G (1.4%, 36,915G)

brown bets:
Mesmaster: 2,000G (40.6%, 10,414G)
BirbBrainsBot: 1,000G (20.3%, 80,500G)
ShintaroNayaka: 500G (10.1%, 10,507G)
prince_rogers_nelson_: 432G (8.8%, 432G)
ASkyNightly: 400G (8.1%, 3,002G)
KyleWonToLiveForever: 300G (6.1%, 7,039G)
Evewho: 200G (4.1%, 17,843G)
GrayGhostGaming: 100G (2.0%, 10,284G)

Final Bets: red - 21 bets for 12,423G (44.6%, x1.24); blue - 6 bets for 15,421G (55.4%, x0.81)

red bets:
dogsandcatsand: 2,000G (16.1%, 5,706G)
RaIshtar: 1,500G (12.1%, 6,254G)
HaateXIII: 1,456G (11.7%, 1,456G)
BirbBrainsBot: 1,000G (8.0%, 193,519G)
DeathTaxesAndAnime: 839G (6.8%, 839G)
Laserman1000: 644G (5.2%, 644G)
Nizaha: 572G (4.6%, 9,197G)
DustBirdEX: 500G (4.0%, 500G)
killth3kid: 484G (3.9%, 3,215G)
randgridr: 464G (3.7%, 464G)
Miku_Shikhu: 431G (3.5%, 431G)
Digitalsocrates: 424G (3.4%, 1,197G)
DLJuggernaut: 350G (2.8%, 2,189G)
AllInBot: 345G (2.8%, 345G)
CapnChaos12: 300G (2.4%, 13,643G)
Treafa: 288G (2.3%, 993G)
Lord_Gwarth: 212G (1.7%, 212G)
Skincrawler: 200G (1.6%, 2,009G)
datadrivenbot: 200G (1.6%, 33,295G)
getthemoneyz: 114G (0.9%, 1,337,916G)
SephDarkheart: 100G (0.8%, 61,999G)

blue bets:
TheChainNerd: 6,430G (41.7%, 12,608G)
reinoe: 5,000G (32.4%, 58,906G)
E_Ballard: 2,987G (19.4%, 2,987G)
ColetteMSLP: 400G (2.6%, 7,652G)
RageImmortaI: 304G (2.0%, 304G)
OtherBrand: 300G (1.9%, 898G)

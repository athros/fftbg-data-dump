Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



LanseDM
Male
Aquarius
51
63
Ninja
Punch Art
Meatbone Slash
Long Status
Jump+2

Short Edge
Kunai
Feather Hat
Leather Outfit
Diamond Armlet

Shuriken, Bomb, Dictionary
Purification, Chakra, Revive



ZCKaiser
Monster
Sagittarius
38
64
Porky










Lord Burrah
Female
Cancer
42
47
Lancer
Elemental
Counter Flood
Equip Axe
Waterbreathing

Slasher
Round Shield
Cross Helmet
Chain Mail
Feather Mantle

Level Jump8, Vertical Jump6
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind



Reinoe
Female
Virgo
59
61
Priest
Black Magic
MP Restore
Equip Gun
Swim

Ramia Harp

Headgear
Wizard Outfit
Cursed Ring

Cure, Cure 3, Cure 4, Wall, Esuna
Fire 3, Bolt, Ice 3

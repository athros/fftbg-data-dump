Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Prince Rogers Nelson
Male
Aries
74
48
Ninja
Jump
Earplug
Equip Sword
Move-HP Up

Ragnarok
Kunai
Triangle Hat
Adaman Vest
Rubber Shoes

Knife
Level Jump4, Vertical Jump3



Choco Joe
Male
Libra
67
60
Ninja
Talk Skill
Counter Tackle
Monster Talk
Jump+3

Sasuke Knife
Main Gauche
Flash Hat
Judo Outfit
Bracer

Shuriken, Wand
Persuade, Preach, Solution, Negotiate, Mimic Daravon, Refute



Reddwind
Male
Gemini
80
59
Knight
Black Magic
Earplug
Equip Gun
Move-HP Up

Blaze Gun
Escutcheon
Platinum Helmet
Black Robe
Dracula Mantle

Shield Break, Weapon Break, Power Break, Night Sword
Fire, Fire 2, Fire 4, Bolt, Ice, Ice 2, Ice 3, Death



Ruebyy
Female
Libra
79
66
Wizard
Draw Out
Hamedo
Short Charge
Lava Walking

Dragon Rod

Feather Hat
Secret Clothes
Leather Mantle

Fire 3, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 3, Empower
Koutetsu, Kiyomori, Kikuichimoji

Player: !White
Team: White Team
Palettes: White/Blue



Otakutaylor
Female
Scorpio
65
76
Summoner
Dance
Meatbone Slash
Magic Attack UP
Swim

Ice Rod

Triangle Hat
Light Robe
Battle Boots

Moogle, Ifrit, Carbunkle, Bahamut
Witch Hunt, Slow Dance, Polka Polka, Void Storage, Nether Demon



Carledo
Male
Pisces
65
57
Bard
Throw
PA Save
Equip Axe
Retreat

Oak Staff

Holy Miter
Rubber Costume
Jade Armlet

Battle Song, Magic Song, Nameless Song, Last Song
Shuriken, Spear



StealthModeLocke
Female
Scorpio
74
72
Chemist
Talk Skill
Auto Potion
Halve MP
Jump+3

Main Gauche

Holy Miter
Black Costume
Magic Ring

Potion, Maiden's Kiss, Soft, Remedy, Phoenix Down
Invitation, Solution, Negotiate, Refute, Rehabilitate



RaIshtar
Male
Aries
54
68
Geomancer
Sing
Earplug
Magic Defense UP
Jump+3

Battle Axe
Crystal Shield
Golden Hairpin
Mystic Vest
Battle Boots

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball
Life Song, Battle Song

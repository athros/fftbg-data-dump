Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jeeboheebo
Monster
Serpentarius
44
77
Red Chocobo










CrownOfHorns
Monster
Capricorn
55
50
Draugr










Evewho
Female
Sagittarius
51
72
Samurai
Throw
Damage Split
Magic Attack UP
Move+3

Dragon Whisker

Barbuta
Light Robe
Small Mantle

Asura, Bizen Boat, Heaven's Cloud, Kiyomori
Shuriken, Staff, Dictionary



CorpusCav
Male
Leo
72
45
Squire
Sing
Hamedo
Equip Armor
Waterbreathing

Flail
Buckler
Circlet
Linen Robe
Diamond Armlet

Accumulate, Dash, Heal, Wish
Cheer Song, Nameless Song, Diamond Blade, Space Storage, Hydra Pit

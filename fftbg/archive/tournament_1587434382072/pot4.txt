Final Bets: purple - 17 bets for 27,376G (71.6%, x0.40); brown - 16 bets for 10,863G (28.4%, x2.52)

purple bets:
HaateXIII: 8,999G (32.9%, 8,999G)
GreatRedDragon: 5,224G (19.1%, 15,500G)
EnemyController: 2,391G (8.7%, 72,391G)
RyuTsuno: 1,500G (5.5%, 14,819G)
HaplessOne: 1,500G (5.5%, 119,455G)
R_Raynos: 1,316G (4.8%, 1,316G)
leakimiko: 1,312G (4.8%, 13,127G)
Fenrislfr: 1,159G (4.2%, 1,159G)
Aldrammech: 1,000G (3.7%, 20,269G)
ungabunga_bot: 1,000G (3.7%, 204,996G)
BirbBrainsBot: 1,000G (3.7%, 55,664G)
Laserman1000: 600G (2.2%, 25,100G)
AllInBot: 100G (0.4%, 100G)
ApplesauceBoss: 100G (0.4%, 9,517G)
DeathTaxesAndAnime: 100G (0.4%, 11,030G)
DrAntiSocial: 50G (0.2%, 17,311G)
GingerDynomite: 25G (0.1%, 6,417G)

brown bets:
mirapoix: 3,335G (30.7%, 3,335G)
marresil: 1,419G (13.1%, 1,419G)
Lydian_C: 1,200G (11.0%, 140,998G)
Dexsana: 1,015G (9.3%, 1,015G)
Drgnmstr366: 935G (8.6%, 935G)
SWATb1gdog: 550G (5.1%, 3,849G)
Aka_Gilly: 400G (3.7%, 26,991G)
elkydeluxe: 400G (3.7%, 18,852G)
getthemoneyz: 362G (3.3%, 524,580G)
Chuckolator: 297G (2.7%, 19,875G)
Irondynamics: 200G (1.8%, 2,156G)
Lavatis: 200G (1.8%, 2,625G)
Mandoragan: 200G (1.8%, 2,796G)
RunicMagus: 150G (1.4%, 34,689G)
gorgewall: 100G (0.9%, 3,079G)
alecttox: 100G (0.9%, 64,410G)

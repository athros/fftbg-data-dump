Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Drgnmstr366
Female
Scorpio
72
49
Summoner
Throw
Regenerator
Martial Arts
Move+2

White Staff

Golden Hairpin
Rubber Costume
Diamond Armlet

Moogle, Ramuh, Ifrit, Golem, Leviathan, Lich
Bomb, Knife



Elijahrockers
Female
Serpentarius
74
49
Priest
Dance
Absorb Used MP
Equip Polearm
Waterbreathing

Cypress Rod

Twist Headband
Black Robe
Salty Rage

Cure, Cure 3, Cure 4, Raise, Reraise, Protect 2, Shell 2, Esuna
Polka Polka, Last Dance



Madming25
Female
Gemini
52
70
Squire
White Magic
Faith Up
Equip Bow
Waterwalking

Panther Bag
Flame Shield
Triangle Hat
Wizard Outfit
Small Mantle

Dash, Throw Stone, Heal, Tickle, Yell, Wish, Scream
Cure, Cure 3, Raise, Raise 2, Reraise, Regen, Shell 2, Esuna, Magic Barrier



Evewho
Female
Capricorn
39
46
Archer
Battle Skill
Abandon
Equip Armor
Fly

Mythril Bow

Holy Miter
White Robe
Defense Armlet

Charge+3, Charge+4, Charge+10
Power Break, Justice Sword

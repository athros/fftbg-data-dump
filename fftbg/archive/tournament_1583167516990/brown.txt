Player: !Brown
Team: Brown Team
Palettes: Brown/Green



YaBoy125
Male
Aries
43
74
Archer
Item
PA Save
Beastmaster
Move-HP Up

Poison Bow
Flame Shield
Headgear
Wizard Outfit
Bracer

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Potion, X-Potion, Ether, Maiden's Kiss, Soft, Holy Water



Treapvort
Monster
Gemini
64
81
Sekhret










AM3P Batman
Female
Cancer
74
81
Summoner
Draw Out
Counter Tackle
Equip Bow
Levitate

Hunting Bow

Holy Miter
Chameleon Robe
Spike Shoes

Ifrit, Golem, Odin, Salamander, Silf, Fairy, Lich
Koutetsu, Heaven's Cloud, Kikuichimoji



Sinnyil2
Female
Aries
36
71
Priest
Dance
Distribute
Equip Polearm
Jump+1

Spear

Green Beret
White Robe
Setiemson

Raise, Raise 2, Regen, Protect, Shell, Esuna
Obsidian Blade, Void Storage

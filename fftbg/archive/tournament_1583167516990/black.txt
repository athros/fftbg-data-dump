Player: !Black
Team: Black Team
Palettes: Black/Red



OmnibotGamma
Monster
Taurus
65
40
Black Chocobo










EunosXX
Female
Aquarius
55
80
Monk
Charge
Counter Magic
Equip Knife
Fly

Rod

Flash Hat
Judo Outfit
Diamond Armlet

Wave Fist, Secret Fist, Revive
Charge+1



Ar Tactic
Female
Aries
54
52
Samurai
Throw
PA Save
Equip Armor
Retreat

Kiyomori

Mythril Helmet
Clothes
Bracer

Asura, Bizen Boat, Kiyomori, Muramasa
Shuriken, Bomb



AlysSariel
Female
Cancer
45
75
Samurai
Dance
Counter
Martial Arts
Swim

Javelin

Diamond Helmet
Reflect Mail
Reflect Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Masamune
Witch Hunt, Wiznaibus, Void Storage, Dragon Pit

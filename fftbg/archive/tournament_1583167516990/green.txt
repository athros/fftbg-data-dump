Player: !Green
Team: Green Team
Palettes: Green/White



Bryon W
Female
Gemini
55
38
Monk
Draw Out
Counter Magic
Doublehand
Jump+1



Golden Hairpin
Brigandine
Red Shoes

Wave Fist, Purification, Revive, Seal Evil
Bizen Boat, Murasame, Muramasa



Verxl
Female
Taurus
61
48
Summoner
Item
Parry
Equip Polearm
Lava Walking

Holy Lance

Golden Hairpin
Chameleon Robe
Red Shoes

Titan, Golem, Odin, Silf, Fairy
Potion, Ether, Hi-Ether, Elixir, Eye Drop, Phoenix Down



Thegapman
Male
Taurus
43
55
Priest
Time Magic
Dragon Spirit
Equip Bow
Fly

Lightning Bow

Green Beret
Linen Robe
Reflect Ring

Cure, Cure 3, Raise, Reraise, Regen, Shell 2, Esuna
Haste 2, Reflect, Demi 2, Stabilize Time



Lowlf
Male
Cancer
55
47
Mediator
Basic Skill
Hamedo
Magic Attack UP
Retreat

Papyrus Codex

Feather Hat
White Robe
N-Kai Armlet

Invitation, Praise, Preach, Death Sentence, Refute, Rehabilitate
Throw Stone, Heal, Yell

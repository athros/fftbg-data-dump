Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BaronHaynes
Monster
Libra
50
79
Squidraken










Mesmaster
Male
Virgo
62
41
Geomancer
White Magic
Auto Potion
Sicken
Retreat

Giant Axe
Genji Shield
Golden Hairpin
White Robe
Vanish Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Cure, Raise, Protect, Protect 2, Shell 2, Esuna



RaIshtar
Male
Sagittarius
66
41
Bard
Battle Skill
Faith Save
Secret Hunt
Move+2

Ice Bow

Black Hood
Black Costume
108 Gems

Angel Song, Life Song, Battle Song, Magic Song, Nameless Song, Last Song, Hydra Pit
Shield Break, Weapon Break, Magic Break, Speed Break



Amiture
Female
Cancer
77
39
Wizard
Dance
Regenerator
Halve MP
Swim

Assassin Dagger

Black Hood
Wizard Robe
Angel Ring

Fire, Bolt 2, Bolt 3, Ice 3, Ice 4
Wiznaibus, Disillusion

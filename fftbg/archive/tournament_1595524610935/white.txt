Player: !White
Team: White Team
Palettes: White/Blue



Lord Gwarth
Female
Sagittarius
60
41
Mime

Blade Grasp
Martial Arts
Teleport



Black Hood
Black Costume
Reflect Ring

Mimic




TasisSai
Female
Taurus
57
69
Dancer
Elemental
Auto Potion
Beastmaster
Jump+2

Ryozan Silk

Leather Hat
Chameleon Robe
Genji Gauntlet

Witch Hunt, Slow Dance, Polka Polka, Nameless Dance, Last Dance, Obsidian Blade, Dragon Pit
Hell Ivy, Quicksand, Sand Storm, Blizzard



SirAbcde
Monster
Taurus
46
67
Malboro










Mirapoix
Female
Cancer
77
67
Archer
Draw Out
Parry
Secret Hunt
Swim

Long Bow

Black Hood
Mystic Vest
Magic Gauntlet

Charge+1, Charge+4, Charge+7, Charge+20
Asura, Koutetsu, Heaven's Cloud

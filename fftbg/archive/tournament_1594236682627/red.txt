Player: !Red
Team: Red Team
Palettes: Red/Brown



Nsm013
Male
Cancer
65
62
Priest
Jump
Auto Potion
Long Status
Move-HP Up

Oak Staff

Twist Headband
Mythril Vest
Power Wrist

Cure 2, Cure 3, Raise 2, Protect, Shell, Shell 2, Esuna
Level Jump8, Vertical Jump5



DamnThatShark
Male
Sagittarius
67
75
Knight
Draw Out
Mana Shield
Beastmaster
Retreat

Defender
Ice Shield
Leather Helmet
Chain Mail
Rubber Shoes

Power Break, Justice Sword, Surging Sword
Kiyomori



Evewho
Female
Aries
51
48
Chemist
Yin Yang Magic
Absorb Used MP
Equip Sword
Jump+3

Chirijiraden

Leather Hat
Adaman Vest
Red Shoes

Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Blind, Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic



Lastly
Female
Taurus
73
56
Oracle
Dance
Counter
Maintenance
Swim

Musk Rod

Green Beret
Black Costume
Angel Ring

Blind, Poison, Spell Absorb, Doubt Faith, Dispel Magic, Paralyze, Petrify, Dark Holy
Witch Hunt, Polka Polka, Disillusion

Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Female
Sagittarius
65
74
Lancer
White Magic
MP Restore
Secret Hunt
Move+2

Partisan
Platinum Shield
Bronze Helmet
Chameleon Robe
Defense Armlet

Level Jump2, Vertical Jump7
Cure, Raise, Protect, Shell, Esuna, Holy



Mirapoix
Male
Gemini
73
46
Lancer
Elemental
Caution
Dual Wield
Move-MP Up

Mythril Spear
Mythril Spear
Genji Helmet
Gold Armor
Battle Boots

Level Jump8, Vertical Jump2
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



Go2sleepTV
Male
Libra
49
50
Archer
Battle Skill
Meatbone Slash
Doublehand
Move+2

Bow Gun

Thief Hat
Adaman Vest
Setiemson

Charge+7, Charge+10
Armor Break, Weapon Break, Night Sword



Friederike
Female
Taurus
42
57
Thief
Basic Skill
Dragon Spirit
Short Status
Jump+3

Dagger

Green Beret
Leather Outfit
Jade Armlet

Gil Taking, Steal Heart, Steal Armor, Steal Weapon
Accumulate, Dash, Tickle, Wish, Scream

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Arkreaver
Female
Taurus
75
69
Samurai
Elemental
Arrow Guard
Equip Bow
Swim

Snipe Bow

Circlet
Chain Mail
Magic Gauntlet

Kiyomori, Muramasa, Kikuichimoji
Pitfall, Hallowed Ground, Quicksand, Sand Storm



NIghtdew14
Female
Taurus
60
49
Dancer
Item
Arrow Guard
Dual Wield
Fly

Persia
Cashmere
Green Beret
Light Robe
Spike Shoes

Nameless Dance, Nether Demon
Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water



Grininda
Male
Gemini
52
64
Archer
Elemental
Mana Shield
Doublehand
Move-HP Up

Blaze Gun

Feather Hat
Mythril Vest
Battle Boots

Charge+1, Charge+4
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Mauricio
Male
Scorpio
61
55
Mediator
Throw
Caution
Equip Shield
Jump+3

Glacier Gun
Crystal Shield
Triangle Hat
Black Robe
Bracer

Invitation, Praise, Preach, Solution, Insult, Refute, Rehabilitate
Shuriken

Final Bets: black - 5 bets for 1,372G (15.6%, x5.40); brown - 11 bets for 7,414G (84.4%, x0.19)

black bets:
Seaweed_B: 500G (36.4%, 50,989G)
mistblade: 288G (21.0%, 288G)
Raixelol: 250G (18.2%, 16,574G)
PuzzleSecretary: 200G (14.6%, 387G)
getthemoneyz: 134G (9.8%, 1,756,395G)

brown bets:
sinnyil2: 2,308G (31.1%, 2,308G)
AllInBot: 1,016G (13.7%, 1,016G)
BirbBrainsBot: 1,000G (13.5%, 56,979G)
superdevon1: 851G (11.5%, 85,106G)
MemoriesofFinal: 597G (8.1%, 597G)
Absalom_20: 501G (6.8%, 501G)
thunderducker: 440G (5.9%, 440G)
Mushufasa_: 232G (3.1%, 232G)
datadrivenbot: 200G (2.7%, 64,758G)
Vultuous: 168G (2.3%, 168G)
gorgewall: 101G (1.4%, 14,541G)

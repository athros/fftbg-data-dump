Player: !Black
Team: Black Team
Palettes: Black/Red



ALY327
Female
Cancer
51
48
Archer
Yin Yang Magic
Counter
Defense UP
Ignore Height

Blaze Gun
Flame Shield
Black Hood
Mythril Vest
Spike Shoes

Charge+1, Charge+2, Charge+7
Spell Absorb, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep



DeathTaxesAndAnime
Female
Libra
77
73
Samurai
Item
Catch
Equip Knife
Lava Walking

Wizard Rod

Mythril Helmet
Bronze Armor
Angel Ring

Koutetsu, Bizen Boat
Potion, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down



Toka222
Female
Aries
62
73
Geomancer
Charge
Caution
Magic Attack UP
Waterbreathing

Giant Axe
Hero Shield
Holy Miter
Wizard Outfit
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp
Charge+1, Charge+10



FriendSquirrel
Male
Cancer
75
83
Wizard
Battle Skill
Counter Magic
Magic Defense UP
Levitate

Ice Rod

Red Hood
Linen Robe
Feather Boots

Fire 4, Bolt 2, Bolt 3, Death
Head Break, Armor Break, Shield Break, Weapon Break, Power Break, Mind Break

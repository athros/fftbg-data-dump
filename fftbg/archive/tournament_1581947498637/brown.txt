Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sparker9
Female
Virgo
56
65
Dancer
Elemental
Parry
Magic Attack UP
Swim

Persia

Barette
Wizard Outfit
108 Gems

Polka Polka, Disillusion, Obsidian Blade, Void Storage, Nether Demon
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



Krexenn
Male
Libra
74
47
Geomancer
Throw
MP Restore
Concentrate
Ignore Terrain

Kiyomori
Mythril Shield
Leather Hat
Chain Vest
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shuriken, Bomb



BigDLicious91
Female
Cancer
72
51
Geomancer
Time Magic
Parry
Long Status
Move+2

Murasame
Mythril Shield
Thief Hat
Power Sleeve
Leather Mantle

Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Slow 2, Quick, Demi 2, Stabilize Time



Zeroroute
Female
Aquarius
45
50
Knight
Elemental
MP Restore
Long Status
Move-MP Up

Ragnarok

Cross Helmet
Gold Armor
Reflect Ring

Shield Break, Power Break, Stasis Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Gusty Wind

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



O Heyno
Male
Leo
66
77
Archer
Steal
Counter Flood
Martial Arts
Retreat

Glacier Gun
Venetian Shield
Red Hood
Brigandine
N-Kai Armlet

Charge+2
Steal Helmet, Steal Shield, Steal Accessory



Grandlanzer
Female
Pisces
59
45
Wizard
Summon Magic
Sunken State
Concentrate
Retreat

Dragon Rod

Holy Miter
Brigandine
Genji Gauntlet

Fire, Fire 4, Bolt 2, Ice 3, Frog
Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Salamander, Silf, Lich



NovaKnight21
Male
Pisces
51
80
Knight
Punch Art
Caution
Magic Defense UP
Move+3

Defender
Aegis Shield
Circlet
Black Robe
Magic Ring

Weapon Break, Mind Break, Surging Sword, Explosion Sword
Purification, Chakra, Revive



Tougou
Female
Aquarius
60
41
Geomancer
Draw Out
Auto Potion
Equip Armor
Retreat

Giant Axe
Aegis Shield
Leather Helmet
Platinum Armor
Bracer

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Koutetsu, Muramasa

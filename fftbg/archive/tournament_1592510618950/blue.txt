Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Oobs56
Male
Taurus
40
60
Mediator
Jump
Sunken State
Equip Polearm
Fly

Javelin

Twist Headband
Light Robe
Battle Boots

Invitation, Persuade, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Level Jump8, Vertical Jump2



Run With Stone GUNs
Female
Capricorn
69
65
Calculator
Demon Skill
Speed Save
Dual Wield
Levitate

Rod
Dragon Rod
Golden Hairpin
Carabini Mail
N-Kai Armlet

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



TheMM42
Female
Aries
52
53
Thief
Talk Skill
MP Restore
Equip Sword
Move+2

Ragnarok

Black Hood
Earth Clothes
Reflect Ring

Steal Weapon, Steal Status, Leg Aim
Invitation, Praise, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Ferroniss
Female
Libra
73
65
Chemist
Battle Skill
Damage Split
Equip Gun
Teleport

Fairy Harp

Feather Hat
Wizard Outfit
Elf Mantle

Potion, X-Potion, Ether, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Surging Sword

Player: !Red
Team: Red Team
Palettes: Red/Brown



Theseawolf1
Female
Aries
56
65
Mediator
Black Magic
Faith Up
Equip Armor
Move+2

Papyrus Codex

Circlet
Bronze Armor
Genji Gauntlet

Persuade, Preach, Solution, Death Sentence, Refute
Fire, Fire 3, Bolt, Ice 4, Death



Aviner42
Male
Sagittarius
47
55
Wizard
Punch Art
Caution
Magic Attack UP
Retreat

Wizard Rod

Thief Hat
Linen Robe
Bracer

Fire, Bolt 2, Bolt 3, Bolt 4, Ice 2, Death
Purification, Chakra, Seal Evil



Fenixcrest
Male
Capricorn
60
54
Thief
Battle Skill
Faith Up
Equip Armor
Waterwalking

Diamond Sword

Mythril Helmet
Mythril Armor
Bracer

Steal Heart, Steal Shield
Weapon Break, Mind Break, Justice Sword, Night Sword



Faantatv
Male
Aries
62
46
Calculator
Undeath Skill
Counter Magic
Halve MP
Fly

Papyrus Codex
Ice Shield
Twist Headband
Silk Robe
Genji Gauntlet

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch

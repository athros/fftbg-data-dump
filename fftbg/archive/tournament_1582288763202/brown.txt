Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Epicureial
Female
Gemini
64
48
Mime

PA Save
Martial Arts
Move+1



Leather Hat
Secret Clothes
Rubber Shoes

Mimic




Upvla
Monster
Scorpio
63
37
Mindflayer










Lyner87
Male
Scorpio
62
77
Oracle
Black Magic
MP Restore
Magic Attack UP
Ignore Terrain

Ivory Rod

Thief Hat
Black Robe
Feather Mantle

Poison, Life Drain, Doubt Faith, Foxbird, Confusion Song, Paralyze, Petrify
Fire, Fire 4, Bolt, Ice



TheFalseTruth
Female
Aquarius
57
54
Knight
Talk Skill
Parry
Defense UP
Retreat

Battle Axe
Escutcheon
Mythril Helmet
Maximillian
Feather Boots

Head Break, Shield Break, Magic Break, Speed Break, Power Break, Surging Sword
Persuade, Praise, Solution, Refute, Rehabilitate

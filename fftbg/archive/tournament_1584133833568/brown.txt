Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MeleeWizard
Male
Serpentarius
55
48
Archer
Battle Skill
HP Restore
Equip Gun
Jump+1

Ramia Harp
Diamond Shield
Holy Miter
Mythril Vest
Bracer

Charge+1, Charge+3, Charge+20
Armor Break, Shield Break, Magic Break, Speed Break, Power Break



ManaBirb
Female
Cancer
65
76
Lancer
White Magic
Meatbone Slash
Secret Hunt
Waterwalking

Iron Fan
Ice Shield
Cross Helmet
Crystal Mail
Reflect Ring

Level Jump4, Vertical Jump7
Protect, Protect 2, Shell 2, Wall, Esuna



BAEN
Female
Capricorn
50
71
Lancer
Talk Skill
Caution
Monster Talk
Levitate

Javelin
Flame Shield
Iron Helmet
Light Robe
108 Gems

Level Jump8, Vertical Jump3
Persuade



Break3r
Female
Sagittarius
70
60
Dancer
White Magic
Abandon
Sicken
Ignore Terrain

Ryozan Silk

Flash Hat
Brigandine
Defense Ring

Wiznaibus, Slow Dance, Obsidian Blade, Nether Demon
Cure 3, Cure 4, Raise, Regen, Shell, Wall, Holy

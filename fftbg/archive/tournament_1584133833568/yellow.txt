Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Silentperogy
Female
Leo
79
68
Calculator
Yin Yang Magic
Counter Magic
Equip Armor
Retreat

Octagon Rod

Black Hood
Diamond Armor
Small Mantle

CT, Height, 5, 4
Blind, Poison, Pray Faith, Zombie, Blind Rage, Foxbird, Sleep



Rnark
Male
Aries
56
59
Priest
Battle Skill
Counter Magic
Attack UP
Jump+1

Wizard Staff

Golden Hairpin
Wizard Robe
Leather Mantle

Raise, Raise 2, Protect, Shell, Esuna, Holy
Head Break, Shield Break, Weapon Break, Speed Break, Justice Sword



Bryan792
Male
Cancer
40
78
Lancer
Charge
Arrow Guard
Dual Wield
Swim

Obelisk
Iron Fan
Cross Helmet
Light Robe
Dracula Mantle

Level Jump5, Vertical Jump8
Charge+1, Charge+4, Charge+5



LOKITHUS
Male
Leo
76
71
Summoner
Jump
Counter
Equip Bow
Ignore Terrain

Bow Gun

Triangle Hat
Chameleon Robe
Elf Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle, Bahamut, Salamander
Level Jump5, Vertical Jump7

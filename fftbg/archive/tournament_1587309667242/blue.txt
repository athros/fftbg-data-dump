Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Flowinprose
Female
Taurus
76
50
Dancer
Steal
Mana Shield
Equip Knife
Move-HP Up

Dagger

Triangle Hat
Clothes
Power Wrist

Slow Dance, Polka Polka, Disillusion, Nether Demon, Dragon Pit
Steal Heart, Steal Helmet, Steal Shield, Steal Accessory



ALY327
Male
Pisces
50
80
Summoner
Punch Art
MP Restore
Equip Gun
Ignore Height

Papyrus Codex

Triangle Hat
Chain Vest
Angel Ring

Ramuh, Golem, Leviathan, Salamander
Spin Fist, Purification, Chakra, Revive



Wurstwaesserchen
Female
Taurus
68
68
Wizard
Elemental
Caution
Attack UP
Swim

Blind Knife

Golden Hairpin
Secret Clothes
108 Gems

Fire 3, Bolt, Bolt 4, Ice 2, Ice 3, Ice 4, Empower, Death
Water Ball, Static Shock, Sand Storm



Lordminsc
Male
Capricorn
74
72
Bard
Elemental
Parry
Doublehand
Ignore Height

Fairy Harp

Green Beret
Chain Vest
Defense Ring

Angel Song, Last Song
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

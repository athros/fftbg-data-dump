Player: !White
Team: White Team
Palettes: White/Blue



Evewho
Female
Gemini
67
45
Chemist
Charge
Damage Split
Equip Axe
Jump+2

Battle Axe

Flash Hat
Mythril Vest
Genji Gauntlet

Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Charge+1, Charge+2



Nok
Monster
Aquarius
70
39
Minotaur










Reverie3
Male
Libra
44
69
Wizard
Punch Art
Abandon
Short Charge
Move-HP Up

Dagger

Thief Hat
Silk Robe
N-Kai Armlet

Fire 4, Bolt, Ice, Flare
Wave Fist, Purification



Bahamutlagooon
Male
Capricorn
45
80
Thief
Elemental
Mana Shield
Secret Hunt
Waterwalking

Mage Masher

Twist Headband
Power Sleeve
Spike Shoes

Gil Taking, Steal Shield, Steal Accessory, Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind

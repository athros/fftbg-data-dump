Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lobsterlobster
Male
Leo
77
56
Time Mage
Basic Skill
Counter
Equip Gun
Jump+2

Mythril Gun

Triangle Hat
Adaman Vest
Jade Armlet

Slow, Stop, Immobilize, Stabilize Time
Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Fury, Wish



Breakdown777
Female
Capricorn
71
43
Samurai
Charge
Sunken State
Magic Attack UP
Waterbreathing

Javelin

Bronze Helmet
Black Robe
Feather Mantle

Koutetsu, Kiyomori, Kikuichimoji
Charge+1, Charge+5



Itsdigitalbro
Male
Aquarius
65
54
Summoner
Elemental
Counter Magic
Equip Shield
Lava Walking

Dragon Rod
Mythril Shield
Leather Hat
Black Robe
Reflect Ring

Moogle, Ramuh, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Fairy
Pitfall, Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball



OneHundredFists
Male
Libra
50
48
Bard
Draw Out
Arrow Guard
Dual Wield
Teleport

Ramia Harp
Bloody Strings
Twist Headband
Platinum Armor
Wizard Mantle

Cheer Song, Nameless Song, Hydra Pit
Heaven's Cloud, Muramasa

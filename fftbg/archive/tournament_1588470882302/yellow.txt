Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



LDSkinny
Female
Virgo
49
79
Oracle
Elemental
Abandon
Defense UP
Move-HP Up

Ivory Rod

Green Beret
Secret Clothes
N-Kai Armlet

Spell Absorb, Pray Faith, Blind Rage, Dispel Magic
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Lava Ball



Baron Von Scrub
Female
Sagittarius
41
60
Mediator
Steal
Hamedo
Equip Sword
Fly

Iron Sword

Leather Hat
Earth Clothes
Wizard Mantle

Persuade, Preach, Insult, Negotiate, Mimic Daravon, Refute
Steal Accessory, Arm Aim



RyuTsuno
Female
Libra
69
78
Summoner
White Magic
Regenerator
Short Charge
Waterbreathing

Oak Staff

Green Beret
Wizard Outfit
Spike Shoes

Moogle, Ifrit, Golem, Carbunkle, Salamander, Fairy
Cure, Raise, Protect, Protect 2, Shell, Shell 2, Wall



HASTERIOUS
Female
Pisces
77
67
Monk
Yin Yang Magic
Faith Up
Beastmaster
Move+3



Thief Hat
Clothes
Small Mantle

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Blind, Poison, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify

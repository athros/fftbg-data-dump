Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Master Metroid
Male
Libra
73
69
Mime

Counter Tackle
Martial Arts
Move+3



Flash Hat
Mythril Armor
Sprint Shoes

Mimic




Mesmaster
Female
Pisces
71
76
Samurai
Talk Skill
Arrow Guard
Defense UP
Move+3

Murasame

Cross Helmet
White Robe
Sprint Shoes

Muramasa
Persuade, Threaten, Preach, Solution, Death Sentence, Insult, Refute, Rehabilitate



Squatting Bear
Male
Scorpio
45
46
Calculator
Mighty Sword
Counter
Short Charge
Waterbreathing

Bestiary

Feather Hat
Genji Armor
Battle Boots

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Fenixcrest
Male
Aquarius
61
63
Mediator
Steal
Mana Shield
Doublehand
Ignore Terrain

Orichalcum

Holy Miter
Light Robe
Genji Gauntlet

Threaten, Death Sentence, Insult, Refute
Gil Taking, Steal Helmet, Leg Aim

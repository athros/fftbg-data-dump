Final Bets: red - 11 bets for 10,915G (66.2%, x0.51); blue - 8 bets for 5,562G (33.8%, x1.96)

red bets:
KasugaiRoastedPeas: 3,405G (31.2%, 3,405G)
NicoSavoy: 2,000G (18.3%, 56,639G)
JCBooBot: 1,000G (9.2%, 51,645G)
BirbBrainsBot: 1,000G (9.2%, 89,193G)
getthemoneyz: 1,000G (9.2%, 1,760,242G)
Lifebregin: 519G (4.8%, 519G)
CT_5_Holy: 507G (4.6%, 10,140G)
Zenmaza: 500G (4.6%, 5,559G)
douchetron: 500G (4.6%, 3,533G)
iBardic: 324G (3.0%, 324G)
PuzzleSecretary: 160G (1.5%, 1,485G)

blue bets:
NovaKnight21: 2,040G (36.7%, 2,040G)
bruubarg: 1,320G (23.7%, 1,320G)
thunderducker: 815G (14.7%, 815G)
Chuckolator: 527G (9.5%, 14,068G)
Cryptopsy70: 340G (6.1%, 340G)
AllInBot: 220G (4.0%, 220G)
datadrivenbot: 200G (3.6%, 63,501G)
ko2q: 100G (1.8%, 7,055G)

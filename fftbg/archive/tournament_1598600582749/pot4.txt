Final Bets: purple - 9 bets for 5,869G (37.5%, x1.67); brown - 8 bets for 9,784G (62.5%, x0.60)

purple bets:
iBardic: 1,147G (19.5%, 1,147G)
bruubarg: 1,079G (18.4%, 1,079G)
JCBooBot: 1,000G (17.0%, 53,234G)
BirbBrainsBot: 863G (14.7%, 89,407G)
CT_5_Holy: 538G (9.2%, 10,763G)
fattunaking: 428G (7.3%, 428G)
getthemoneyz: 414G (7.1%, 1,760,829G)
Greggernaut: 300G (5.1%, 1,000G)
AllInBot: 100G (1.7%, 100G)

brown bets:
gorgewall: 4,924G (50.3%, 4,924G)
NicoSavoy: 2,000G (20.4%, 55,658G)
Zachara: 1,200G (12.3%, 135,700G)
NovaKnight21: 700G (7.2%, 700G)
amiture: 500G (5.1%, 22,456G)
datadrivenbot: 200G (2.0%, 63,176G)
PuzzleSecretary: 160G (1.6%, 1,520G)
ko2q: 100G (1.0%, 6,893G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Sinnyil2
Male
Scorpio
49
65
Ninja
Talk Skill
Earplug
Attack UP
Move+1

Main Gauche
Main Gauche
Green Beret
Adaman Vest
Angel Ring

Bomb
Persuade, Threaten, Negotiate, Refute



Jakeduhhsnake
Male
Taurus
78
64
Monk
Talk Skill
Critical Quick
Dual Wield
Waterbreathing



Green Beret
Mystic Vest
Feather Boots

Earth Slash, Purification, Chakra, Revive
Invitation, Praise, Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



Gooseyourself
Male
Leo
70
69
Summoner
Steal
Earplug
Beastmaster
Teleport

Rod

Cachusha
Linen Robe
Red Shoes

Moogle, Ramuh, Ifrit, Bahamut, Odin, Fairy
Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



Evdoggity
Male
Cancer
55
68
Samurai
Steal
Speed Save
Equip Polearm
Jump+3

Mythril Spear

Cross Helmet
Gold Armor
Bracer

Koutetsu, Muramasa
Steal Heart, Steal Armor, Steal Accessory

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Taypost
Female
Leo
79
71
Priest
Dance
Sunken State
Defense UP
Move+2

Wizard Staff

Red Hood
Linen Robe
Defense Armlet

Cure 2, Cure 4, Raise, Reraise, Esuna
Witch Hunt, Slow Dance, Disillusion



BenYuPoker
Male
Cancer
78
47
Bard
Black Magic
Sunken State
Dual Wield
Move+2

Bloody Strings
Bloody Strings
Black Hood
Chain Vest
Magic Gauntlet

Battle Song, Nameless Song
Fire 3, Bolt, Bolt 2, Bolt 4, Ice



Segomod
Male
Aquarius
71
51
Geomancer
Summon Magic
HP Restore
Magic Defense UP
Lava Walking

Broad Sword
Ice Shield
Red Hood
Wizard Robe
Small Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Golem, Carbunkle, Salamander, Fairy, Lich



Mirapoix
Male
Taurus
72
68
Monk
Throw
Counter Tackle
Beastmaster
Ignore Height



Feather Hat
Mystic Vest
Bracer

Spin Fist, Purification, Chakra, Revive
Shuriken, Spear

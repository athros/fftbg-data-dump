Final Bets: blue - 10 bets for 6,453G (59.4%, x0.68); yellow - 7 bets for 4,403G (40.6%, x1.47)

blue bets:
UmaiJam: 2,000G (31.0%, 42,522G)
douchetron: 1,853G (28.7%, 3,634G)
prince_rogers_nelson_: 668G (10.4%, 668G)
BaronHaynes: 574G (8.9%, 5,574G)
LAGBOT30000: 346G (5.4%, 6,920G)
gooblz: 300G (4.6%, 3,015G)
AllInBot: 200G (3.1%, 200G)
fluffskull: 200G (3.1%, 7,539G)
datadrivenbot: 200G (3.1%, 45,755G)
amhamor: 112G (1.7%, 112G)

yellow bets:
CT_5_Holy: 1,000G (22.7%, 4,624G)
BirbBrainsBot: 1,000G (22.7%, 36,154G)
BoneMiser: 807G (18.3%, 807G)
gorgewall: 800G (18.2%, 800G)
maximumcrit: 500G (11.4%, 6,003G)
getthemoneyz: 196G (4.5%, 1,443,751G)
letdowncity: 100G (2.3%, 3,328G)

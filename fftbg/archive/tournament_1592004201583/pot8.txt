Final Bets: blue - 20 bets for 14,534G (38.8%, x1.58); champion - 17 bets for 22,909G (61.2%, x0.63)

blue bets:
Korremar: 5,874G (40.4%, 5,874G)
sinnyil2: 1,795G (12.4%, 1,795G)
maakur_: 1,657G (11.4%, 1,657G)
Mesmaster: 1,000G (6.9%, 103,989G)
Thyrandaal: 645G (4.4%, 645G)
Cryptopsy70: 536G (3.7%, 11,128G)
fenaen: 500G (3.4%, 31,471G)
acid_flashback: 493G (3.4%, 9,869G)
benticore: 408G (2.8%, 408G)
gorgewall: 301G (2.1%, 19,893G)
Lythe_Caraker: 250G (1.7%, 116,058G)
Yiroep2: 250G (1.7%, 2,329G)
AllInBot: 235G (1.6%, 235G)
Alaquane: 104G (0.7%, 104G)
NightwolfXVI: 100G (0.7%, 100G)
Tougou: 100G (0.7%, 2,770G)
datadrivenbot: 100G (0.7%, 38,118G)
ko2q: 100G (0.7%, 3,843G)
silentkaster: 57G (0.4%, 457G)
rednecknazgul: 29G (0.2%, 729G)

champion bets:
OneHundredFists: 10,000G (43.7%, 28,984G)
reinoe: 3,000G (13.1%, 17,982G)
DLJuggernaut: 1,800G (7.9%, 12,002G)
kaelsun: 1,497G (6.5%, 1,497G)
BirbBrainsBot: 1,000G (4.4%, 161,906G)
mangowarfare: 1,000G (4.4%, 14,269G)
superdevon1: 973G (4.2%, 3,893G)
HASTERIOUS: 806G (3.5%, 16,124G)
killth3kid: 500G (2.2%, 28,087G)
twelfthrootoftwo: 500G (2.2%, 2,207G)
getthemoneyz: 454G (2.0%, 930,892G)
MagicBottle: 388G (1.7%, 388G)
byrdturbo: 333G (1.5%, 14,569G)
ColetteMSLP: 300G (1.3%, 456G)
Evewho: 200G (0.9%, 24,976G)
YaBoy125: 100G (0.4%, 1,395G)
Quadh0nk: 58G (0.3%, 1,102G)

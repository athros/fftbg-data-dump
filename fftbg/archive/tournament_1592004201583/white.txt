Player: !White
Team: White Team
Palettes: White/Blue



SSwing
Monster
Leo
47
48
Steel Giant










Just Here2
Male
Virgo
56
74
Bard
Steal
Caution
Sicken
Swim

Fairy Harp

Twist Headband
Crystal Mail
Small Mantle

Life Song, Cheer Song, Nameless Song, Sky Demon
Steal Shield, Steal Status



Firesheath
Male
Sagittarius
63
51
Archer
Time Magic
Faith Save
Concentrate
Ignore Height

Lightning Bow

Cachusha
Adaman Vest
Angel Ring

Charge+3, Charge+4, Charge+10
Haste 2, Immobilize, Reflect, Quick, Stabilize Time



OneHundredFists
Male
Pisces
62
42
Monk
Sing
Counter Flood
Equip Axe
Fly

Morning Star

Thief Hat
Mystic Vest
Chantage

Pummel, Purification, Revive
Sky Demon

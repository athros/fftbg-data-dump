Player: !zChamp
Team: Champion Team
Palettes: White/Blue



TheChainNerd
Female
Aquarius
70
72
Geomancer
Steal
Parry
Magic Defense UP
Retreat

Rune Blade
Flame Shield
Green Beret
Light Robe
Reflect Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind
Steal Helmet, Steal Shield, Steal Weapon, Leg Aim



Digitalsocrates
Male
Leo
58
49
Mime

Hamedo
Dual Wield
Lava Walking



Iron Helmet
Black Costume
Genji Gauntlet

Mimic




Mesmaster
Female
Aries
66
75
Wizard
Draw Out
Damage Split
Short Charge
Jump+2

Dragon Rod

Black Hood
White Robe
Dracula Mantle

Fire 2, Fire 3, Bolt, Bolt 3, Bolt 4
Koutetsu, Kikuichimoji



ForagerCats
Female
Pisces
50
72
Archer
Yin Yang Magic
Critical Quick
Equip Knife
Teleport 2

Main Gauche
Escutcheon
Leather Hat
Earth Clothes
Dracula Mantle

Charge+2, Charge+4, Charge+5, Charge+7, Charge+20
Poison, Life Drain, Pray Faith, Doubt Faith, Dispel Magic

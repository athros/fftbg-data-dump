Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Sinnyil2
Male
Taurus
60
59
Summoner
Draw Out
Counter
Secret Hunt
Move+3

Thunder Rod

Holy Miter
Black Robe
Diamond Armlet

Moogle, Shiva, Titan, Golem, Carbunkle, Salamander, Silf, Fairy
Murasame, Kiyomori, Muramasa, Kikuichimoji



Roofiepops
Male
Aquarius
51
43
Ninja
Steal
MA Save
Secret Hunt
Waterwalking

Flame Whip
Hidden Knife
Holy Miter
Secret Clothes
Bracer

Shuriken, Bomb, Knife, Staff, Stick
Steal Shield



Lanshaft
Female
Libra
57
48
Wizard
Time Magic
Counter Magic
Martial Arts
Swim



Flash Hat
Secret Clothes
Setiemson

Fire 3, Bolt, Bolt 4, Ice, Ice 4, Empower
Slow 2, Quick, Demi, Stabilize Time



Creggers
Female
Pisces
56
67
Summoner
Charge
Counter Magic
Short Charge
Move+2

Flame Rod

Triangle Hat
Wizard Robe
Reflect Ring

Moogle, Titan, Carbunkle, Odin, Leviathan, Salamander, Silf
Charge+1, Charge+2, Charge+4, Charge+7

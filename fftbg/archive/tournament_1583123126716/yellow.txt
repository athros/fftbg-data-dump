Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Rabbitlogik
Male
Cancer
52
51
Time Mage
White Magic
Critical Quick
Concentrate
Waterbreathing

Healing Staff

Flash Hat
Wizard Robe
Elf Mantle

Haste, Slow, Slow 2, Stop, Float, Quick, Demi, Stabilize Time
Cure, Raise, Raise 2, Shell, Wall, Esuna



JoeykinsGaming
Female
Scorpio
38
68
Calculator
Black Magic
Abandon
Halve MP
Retreat

Battle Bamboo

Flash Hat
Earth Clothes
Diamond Armlet

CT, Prime Number, 5, 4, 3
Fire 2, Fire 4, Bolt, Bolt 4, Ice 2, Ice 3



Waterwatereverywhere
Female
Pisces
41
76
Archer
Time Magic
MP Restore
Dual Wield
Ignore Height

Poison Bow
Cross Bow
Crystal Helmet
Brigandine
Rubber Shoes

Charge+2, Charge+3, Charge+7, Charge+10
Immobilize, Stabilize Time



Lyronmkii
Female
Taurus
47
67
Geomancer
Steal
Brave Up
Magic Attack UP
Move+3

Sleep Sword
Buckler
Black Hood
Rubber Costume
Dracula Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Steal Heart, Steal Weapon, Leg Aim

Player: !Green
Team: Green Team
Palettes: Green/White



VolgraTheMoose
Female
Taurus
68
49
Geomancer
Draw Out
Brave Save
Martial Arts
Waterwalking


Mythril Shield
Feather Hat
Clothes
Wizard Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori



CaptainGarlock
Monster
Pisces
76
67
Red Chocobo










Miku Shikhu
Male
Leo
62
57
Mediator
Black Magic
Distribute
Martial Arts
Teleport



Green Beret
Earth Clothes
Defense Armlet

Praise, Insult, Negotiate, Refute, Rehabilitate
Fire 3, Bolt, Bolt 3, Ice 3, Flare



NeoKevlar
Female
Libra
45
65
Mediator
Steal
Earplug
Dual Wield
Retreat

Stone Gun
Stone Gun
Triangle Hat
Mythril Vest
N-Kai Armlet

Invitation, Threaten, Refute
Steal Heart, Steal Armor, Steal Accessory, Steal Status

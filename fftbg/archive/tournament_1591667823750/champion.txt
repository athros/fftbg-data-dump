Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ALY327
Female
Cancer
43
70
Samurai
Dance
PA Save
Defense UP
Levitate

Kikuichimoji

Mythril Helmet
Black Robe
Leather Mantle

Murasame, Heaven's Cloud, Kiyomori
Slow Dance, Polka Polka, Nameless Dance, Last Dance



Evewho
Female
Pisces
66
69
Chemist
Summon Magic
Damage Split
Equip Sword
Waterwalking

Save the Queen

Triangle Hat
Mythril Vest
Bracer

Potion, Hi-Potion, Ether, Hi-Ether, Maiden's Kiss, Holy Water
Moogle, Titan, Golem, Carbunkle, Fairy



Kaidykat
Male
Leo
48
55
Thief
Item
Mana Shield
Equip Axe
Move-HP Up

Slasher

Red Hood
Mythril Vest
Germinas Boots

Steal Accessory, Leg Aim
Hi-Potion, X-Potion, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



K1ngofthechill
Female
Aries
65
65
Knight
Yin Yang Magic
Counter Magic
Halve MP
Move+1

Ragnarok
Flame Shield
Bronze Helmet
Crystal Mail
Spike Shoes

Weapon Break, Magic Break, Speed Break, Mind Break, Explosion Sword
Blind, Poison, Spell Absorb, Blind Rage, Confusion Song, Dispel Magic, Petrify

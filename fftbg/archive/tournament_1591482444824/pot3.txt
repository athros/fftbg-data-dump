Final Bets: white - 14 bets for 7,816G (46.4%, x1.16); black - 18 bets for 9,032G (53.6%, x0.87)

white bets:
pandasforsale: 1,427G (18.3%, 1,427G)
DeathTaxesAndAnime: 1,262G (16.1%, 2,476G)
Aldrammech: 1,000G (12.8%, 30,127G)
BirbBrainsBot: 649G (8.3%, 118,777G)
killth3kid: 500G (6.4%, 15,006G)
BlackFireUK: 500G (6.4%, 7,557G)
mpghappiness: 452G (5.8%, 452G)
HASTERIOUS: 444G (5.7%, 444G)
HaychDub: 401G (5.1%, 803G)
benticore: 376G (4.8%, 7,526G)
OneHundredFists: 360G (4.6%, 360G)
twelfthrootoftwo: 200G (2.6%, 591G)
Thyrandaal: 144G (1.8%, 144G)
gorgewall: 101G (1.3%, 1,518G)

black bets:
cam_ATS: 2,318G (25.7%, 2,318G)
Lydian_C: 1,200G (13.3%, 8,171G)
Riot_Jayway: 1,111G (12.3%, 211,405G)
nifboy: 1,000G (11.1%, 40,064G)
rockmem21: 750G (8.3%, 4,717G)
maakur_: 648G (7.2%, 648G)
GladiatorLupe: 473G (5.2%, 473G)
Celdia: 200G (2.2%, 200G)
Miku_Shikhu: 200G (2.2%, 1,535G)
regios91: 200G (2.2%, 2,182G)
Flameatron: 200G (2.2%, 1,089G)
dinin991: 120G (1.3%, 120G)
Geffro1908: 112G (1.2%, 112G)
Tougou: 100G (1.1%, 3,020G)
Evewho: 100G (1.1%, 12,288G)
ko2q: 100G (1.1%, 6,744G)
datadrivenbot: 100G (1.1%, 34,300G)
lastly: 100G (1.1%, 12,328G)

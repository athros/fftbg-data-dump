Player: !Red
Team: Red Team
Palettes: Red/Brown



Bahamutlagooon
Male
Pisces
61
61
Mediator
Black Magic
Damage Split
Equip Knife
Teleport

Kunai

Holy Miter
Wizard Robe
Sprint Shoes

Invitation, Praise, Threaten, Mimic Daravon, Refute
Fire, Fire 3, Ice 3, Frog



Breakdown777
Male
Libra
38
77
Chemist
Punch Art
Arrow Guard
Dual Wield
Retreat

Cultist Dagger
Assassin Dagger
Red Hood
Chain Vest
Small Mantle

Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft
Purification, Revive, Seal Evil



Arkreaver
Female
Scorpio
44
73
Monk
Basic Skill
Arrow Guard
Magic Attack UP
Swim



Cachusha
Earth Clothes
Bracer

Spin Fist, Secret Fist, Purification, Revive
Heal, Cheer Up



Cupholderr
Male
Taurus
76
50
Thief
Charge
Abandon
Short Charge
Jump+1

Air Knife

Headgear
Leather Outfit
Salty Rage

Steal Helmet, Steal Armor, Steal Accessory, Arm Aim, Leg Aim
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TheNGUYENNER
Female
Capricorn
63
55
Mime

Damage Split
Martial Arts
Fly



Leather Hat
Chain Vest
Magic Ring

Mimic




Jeeboheebo
Female
Libra
82
46
Time Mage
Draw Out
PA Save
Equip Sword
Fly

Kikuichimoji

Flash Hat
White Robe
Magic Gauntlet

Slow, Stabilize Time
Koutetsu, Muramasa



Foofermoofer
Female
Leo
62
67
Archer
Steal
Faith Up
Magic Defense UP
Levitate

Poison Bow
Round Shield
Headgear
Chain Vest
Setiemson

Charge+1, Charge+2, Charge+3, Charge+10
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Volgrathemoose
Female
Libra
41
72
Wizard
Math Skill
HP Restore
Attack UP
Ignore Terrain

Dragon Rod

Holy Miter
Black Robe
Feather Boots

Fire 2, Bolt 3, Bolt 4, Ice, Ice 4, Death
CT, Height, 3

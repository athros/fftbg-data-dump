Final Bets: black - 20 bets for 19,595G (61.1%, x0.64); brown - 25 bets for 12,478G (38.9%, x1.57)

black bets:
Wargles: 5,000G (25.5%, 13,400G)
Dothan89: 4,805G (24.5%, 9,422G)
DudeMonkey77: 3,411G (17.4%, 34,117G)
ThePineappleSalesman: 1,095G (5.6%, 1,095G)
BirbBrainsBot: 1,000G (5.1%, 129,246G)
bad1dea: 1,000G (5.1%, 350,704G)
baconbacon1207: 500G (2.6%, 15,377G)
getthemoneyz: 474G (2.4%, 403,261G)
Vampire_Killer: 370G (1.9%, 370G)
EnemyController: 314G (1.6%, 314G)
rrazzoug: 300G (1.5%, 1,982G)
Baron_von_Scrub: 296G (1.5%, 296G)
JimmyKetz: 250G (1.3%, 1,980G)
JIDkomu: 180G (0.9%, 180G)
Tithonus: 100G (0.5%, 9,146G)
AlenaZarek: 100G (0.5%, 20,061G)
twelfthrootoftwo: 100G (0.5%, 3,117G)
BadBlanket: 100G (0.5%, 3,317G)
ZephyrTempest: 100G (0.5%, 19,175G)
ApplesauceBoss: 100G (0.5%, 2,528G)

brown bets:
Lydian_C: 4,340G (34.8%, 50,560G)
sinnyil2: 1,200G (9.6%, 17,366G)
Jeeboheebo: 1,002G (8.0%, 8,041G)
reinoe: 1,000G (8.0%, 5,491G)
Axelrod: 1,000G (8.0%, 32,281G)
cnkakashi: 577G (4.6%, 577G)
HaateXIII: 500G (4.0%, 3,711G)
luminarii: 382G (3.1%, 13,302G)
DustBirdEX: 340G (2.7%, 340G)
Aeolus_000: 300G (2.4%, 6,183G)
DHaveWord: 200G (1.6%, 2,644G)
ugoplatamia: 200G (1.6%, 13,882G)
Evewho: 200G (1.6%, 5,374G)
AllInBot: 129G (1.0%, 129G)
joewcarson: 108G (0.9%, 108G)
Aislingeach: 100G (0.8%, 3,107G)
mirapoix: 100G (0.8%, 3,467G)
RunicMagus: 100G (0.8%, 25,493G)
lijarkh: 100G (0.8%, 3,309G)
maakur_: 100G (0.8%, 3,276G)
old_overholt_: 100G (0.8%, 1,268G)
Alc_Trinity: 100G (0.8%, 31,645G)
dem0gency: 100G (0.8%, 104G)
datadrivenbot: 100G (0.8%, 13,845G)
Tanookium: 100G (0.8%, 8,688G)

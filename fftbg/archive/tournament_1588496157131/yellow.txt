Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sinnyil2
Female
Capricorn
57
63
Geomancer
Draw Out
Absorb Used MP
Defend
Levitate

Giant Axe
Platinum Shield
Golden Hairpin
Silk Robe
Salty Rage

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Kiyomori



Midori Ribbon
Male
Virgo
70
51
Knight
Sing
Sunken State
Dual Wield
Move-HP Up

Platinum Sword
Sleep Sword
Circlet
Genji Armor
Feather Boots

Head Break, Magic Break, Stasis Sword
Angel Song, Life Song, Cheer Song, Magic Song, Last Song, Diamond Blade, Space Storage



Bioticism
Female
Taurus
48
69
Wizard
Time Magic
MA Save
Martial Arts
Lava Walking



Black Hood
Adaman Vest
Power Wrist

Fire, Fire 4, Bolt 3, Bolt 4
Haste 2, Slow, Reflect, Demi 2, Meteor



R Raynos
Female
Cancer
54
61
Wizard
Charge
Counter Flood
Halve MP
Move-MP Up

Dragon Rod

Golden Hairpin
Light Robe
Magic Gauntlet

Fire, Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 4, Frog
Charge+7, Charge+20

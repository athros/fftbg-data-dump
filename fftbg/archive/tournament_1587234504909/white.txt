Player: !White
Team: White Team
Palettes: White/Blue



KaLam1ty
Male
Cancer
71
52
Oracle
Summon Magic
Speed Save
Halve MP
Fly

Cypress Rod

Leather Hat
Black Costume
Elf Mantle

Blind, Poison, Life Drain, Pray Faith, Silence Song, Confusion Song, Dispel Magic
Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Fairy, Lich



BroHamma
Male
Virgo
79
64
Squire
Summon Magic
Counter Magic
Equip Axe
Move+2

Slasher
Platinum Shield
Flash Hat
Crystal Mail
Genji Gauntlet

Heal, Scream
Moogle, Carbunkle, Bahamut, Salamander



Maldoree
Monster
Libra
77
78
Red Dragon










EnemyController
Male
Gemini
56
59
Oracle
Draw Out
Abandon
Equip Knife
Swim

Hidden Knife

Headgear
Linen Robe
N-Kai Armlet

Poison, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Koutetsu

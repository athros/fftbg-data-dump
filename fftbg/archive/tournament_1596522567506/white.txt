Player: !White
Team: White Team
Palettes: White/Blue



Flacococo
Male
Scorpio
69
54
Monk
Elemental
Counter Magic
Dual Wield
Jump+2



Leather Hat
Clothes
Defense Ring

Earth Slash, Purification, Revive
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



DeathTaxesAndAnime
Female
Pisces
43
75
Mime

Counter Magic
Equip Armor
Fly



Mythril Helmet
Gold Armor
Bracer

Mimic




Drusiform
Male
Gemini
72
47
Archer
Time Magic
Hamedo
Attack UP
Swim

Ultimus Bow

Cachusha
Adaman Vest
Cursed Ring

Charge+20
Slow, Slow 2, Demi, Stabilize Time, Meteor



ALY327
Female
Aries
58
39
Mime

Speed Save
Martial Arts
Jump+2



Golden Hairpin
Wizard Robe
Genji Gauntlet

Mimic


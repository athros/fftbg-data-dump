Final Bets: black - 12 bets for 12,838G (65.0%, x0.54); purple - 7 bets for 6,902G (35.0%, x1.86)

black bets:
douchetron: 5,355G (41.7%, 10,500G)
randgridr: 2,438G (19.0%, 2,438G)
amiture: 2,000G (15.6%, 33,748G)
Calajo: 1,344G (10.5%, 4,481G)
gorgewall: 501G (3.9%, 5,269G)
sinnyil2: 450G (3.5%, 7,420G)
Evewho: 200G (1.6%, 7,928G)
datadrivenbot: 200G (1.6%, 47,668G)
DeathTaxesAndAnime: 100G (0.8%, 7,429G)
Firesheath: 100G (0.8%, 6,610G)
Wooplestein: 100G (0.8%, 8,563G)
Drusiform: 50G (0.4%, 1,171G)

purple bets:
AllInBot: 4,770G (69.1%, 4,770G)
Lydian_C: 1,200G (17.4%, 8,113G)
getthemoneyz: 246G (3.6%, 1,510,888G)
Aeriyah: 223G (3.2%, 7,223G)
BirbBrainsBot: 200G (2.9%, 200G)
Zachara: 137G (2.0%, 138,137G)
Treafa: 126G (1.8%, 2,474G)

Player: !White
Team: White Team
Palettes: White/Blue



Josephiroth 143
Monster
Aquarius
78
44
Dark Behemoth










Twelfthrootoftwo
Female
Capricorn
73
49
Mediator
Draw Out
Sunken State
Equip Armor
Jump+1

Battle Folio

Platinum Helmet
Linen Robe
Genji Gauntlet

Invitation, Threaten, Death Sentence, Insult
Bizen Boat, Murasame, Heaven's Cloud, Kiyomori



Letdowncity
Monster
Virgo
68
45
Great Malboro










773bird
Male
Scorpio
42
72
Geomancer
White Magic
Counter Flood
Attack UP
Move+2

Giant Axe
Round Shield
Leather Hat
Leather Outfit
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure, Raise, Raise 2, Shell, Wall, Esuna

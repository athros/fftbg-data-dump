Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Phi Sig
Female
Aquarius
61
81
Squire
Summon Magic
Counter
Short Status
Waterwalking

Nagrarock
Gold Shield
Leather Helmet
Rubber Costume
Diamond Armlet

Accumulate, Throw Stone, Heal, Fury, Scream
Ramuh, Bahamut, Odin, Salamander, Lich



DarrenDinosaurs
Female
Scorpio
53
63
Squire
White Magic
Brave Save
Short Status
Swim

Blind Knife
Diamond Shield
Bronze Helmet
Secret Clothes
Defense Armlet

Accumulate, Dash, Heal, Tickle, Yell, Cheer Up, Fury, Wish, Scream
Cure, Cure 2, Cure 4, Raise, Reraise, Regen, Protect 2, Shell, Shell 2, Magic Barrier



Superdevon1
Female
Virgo
80
43
Chemist
Dance
MP Restore
Equip Sword
Teleport

Koutetsu Knife

Black Hood
Mythril Vest
Elf Mantle

Potion, Hi-Potion, X-Potion, Maiden's Kiss, Phoenix Down
Slow Dance, Polka Polka, Void Storage



Killth3kid
Female
Sagittarius
40
43
Archer
Yin Yang Magic
Auto Potion
Attack UP
Move-HP Up

Blaze Gun
Venetian Shield
Green Beret
Adaman Vest
Cursed Ring

Charge+1, Charge+2, Charge+7, Charge+10
Blind, Poison, Spell Absorb, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy

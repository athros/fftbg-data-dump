Player: !Green
Team: Green Team
Palettes: Green/White



SSwing
Monster
Taurus
50
65
Steel Giant










Digitalsocrates
Female
Capricorn
77
44
Geomancer
Steal
Absorb Used MP
Equip Gun
Jump+3

Papyrus Codex
Mythril Shield
Green Beret
White Robe
Small Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Steal Heart, Steal Weapon, Steal Accessory, Steal Status



Gelwain
Male
Libra
74
72
Wizard
Item
Absorb Used MP
Defense UP
Levitate

Ice Rod

Twist Headband
Black Robe
108 Gems

Bolt 2, Bolt 4, Ice, Frog
Potion, Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water



Helpimabug
Female
Aries
78
68
Oracle
Throw
Faith Save
Short Status
Retreat

Bestiary

Holy Miter
Black Costume
Wizard Mantle

Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Dark Holy
Shuriken, Bomb, Knife

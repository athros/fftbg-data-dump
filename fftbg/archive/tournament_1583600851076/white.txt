Player: !White
Team: White Team
Palettes: White/Blue



Bad1dea
Male
Libra
71
52
Monk
Time Magic
Damage Split
Equip Gun
Ignore Height

Mythril Gun

Barette
Wizard Outfit
Salty Rage

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Haste, Slow 2, Reflect, Quick, Stabilize Time



Lastly
Female
Taurus
72
62
Squire
Punch Art
Caution
Dual Wield
Move+3

Air Knife
Battle Axe
Mythril Helmet
Mythril Vest
Diamond Armlet

Accumulate, Heal, Cheer Up
Wave Fist



ZeroHeat
Male
Scorpio
72
70
Oracle
Black Magic
Counter
Short Status
Ignore Terrain

Iron Fan

Black Hood
Light Robe
Angel Ring

Blind, Life Drain, Pray Faith
Fire 3, Bolt 2, Ice



Gabbagooluigi
Male
Scorpio
73
67
Geomancer
Charge
Regenerator
Dual Wield
Ignore Terrain

Iron Sword
Battle Axe
Twist Headband
Wizard Robe
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Charge+1, Charge+7

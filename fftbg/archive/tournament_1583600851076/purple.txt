Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaateXIII
Female
Libra
44
59
Lancer
Black Magic
Faith Up
Defend
Move-MP Up

Holy Lance
Venetian Shield
Mythril Helmet
Black Robe
Spike Shoes

Level Jump4, Vertical Jump7
Fire, Fire 3, Bolt 2, Bolt 3, Ice, Empower



Kamikaze Plague
Male
Capricorn
46
71
Time Mage
Punch Art
Absorb Used MP
Dual Wield
Waterwalking

Papyrus Codex
Bestiary
Green Beret
Judo Outfit
Power Wrist

Haste, Slow, Stop, Float, Quick, Stabilize Time
Secret Fist, Purification, Revive, Seal Evil



CrasusAkechi
Female
Scorpio
59
47
Summoner
Throw
Parry
Equip Gun
Jump+2

Battle Folio

Leather Hat
Light Robe
Red Shoes

Moogle, Ramuh, Ifrit, Titan, Bahamut, Leviathan
Shuriken, Bomb, Ninja Sword



WhiteTigress
Female
Taurus
55
40
Calculator
White Magic
Dragon Spirit
Defend
Waterbreathing

Bestiary

Feather Hat
Brigandine
Magic Gauntlet

CT, Prime Number, 5, 3
Raise, Protect, Protect 2, Esuna

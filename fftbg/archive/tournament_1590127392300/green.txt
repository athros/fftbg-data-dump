Player: !Green
Team: Green Team
Palettes: Green/White



Vorap
Female
Aquarius
50
67
Knight
Punch Art
Mana Shield
Maintenance
Levitate

Materia Blade
Gold Shield
Iron Helmet
Chain Mail
Magic Ring

Head Break, Armor Break, Magic Break, Power Break, Mind Break, Justice Sword
Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive



Genericco
Male
Taurus
47
49
Summoner
Steal
Distribute
Secret Hunt
Ignore Height

Madlemgen

Thief Hat
Wizard Robe
Jade Armlet

Ifrit, Golem, Bahamut, Silf
Gil Taking, Steal Accessory, Steal Status



Powermhero
Male
Serpentarius
77
71
Knight
Draw Out
Auto Potion
Short Status
Move+3

Long Sword
Crystal Shield
Crystal Helmet
White Robe
Genji Gauntlet

Shield Break, Weapon Break, Stasis Sword
Asura, Koutetsu, Murasame, Kiyomori



CapnChaos12
Male
Virgo
37
78
Geomancer
Battle Skill
Sunken State
Short Status
Jump+1

Battle Axe
Gold Shield
Holy Miter
Light Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Head Break, Magic Break, Speed Break, Power Break, Stasis Sword

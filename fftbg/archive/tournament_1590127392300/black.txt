Player: !Black
Team: Black Team
Palettes: Black/Red



Twelfthrootoftwo
Female
Cancer
47
55
Samurai
Black Magic
MA Save
Attack UP
Jump+3

Partisan

Circlet
Genji Armor
Genji Gauntlet

Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Fire, Fire 4, Bolt 2, Bolt 3, Ice 3, Frog



Rocl
Male
Cancer
72
53
Knight
Elemental
Absorb Used MP
Defense UP
Swim

Save the Queen
Flame Shield
Iron Helmet
White Robe
Germinas Boots

Shield Break, Power Break, Night Sword
Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Gusty Wind, Lava Ball



TheMM42
Male
Scorpio
51
55
Priest
Throw
Earplug
Defense UP
Jump+3

Morning Star

Golden Hairpin
Wizard Robe
Vanish Mantle

Cure 2, Cure 3, Protect, Esuna, Holy
Dictionary



OneHundredFists
Male
Aries
45
79
Archer
White Magic
Speed Save
Equip Gun
Move-HP Up

Madlemgen
Flame Shield
Twist Headband
Earth Clothes
Wizard Mantle

Charge+2, Charge+7, Charge+20
Cure 2, Cure 4, Reraise, Protect, Shell, Wall, Esuna

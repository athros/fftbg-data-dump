Final Bets: green - 15 bets for 11,853G (43.7%, x1.29); yellow - 6 bets for 15,276G (56.3%, x0.78)

green bets:
Shakarak: 2,000G (16.9%, 17,986G)
Phytik: 1,780G (15.0%, 19,068G)
ccordc: 1,747G (14.7%, 3,494G)
BirbBrainsBot: 1,000G (8.4%, 155,976G)
Mesmaster: 1,000G (8.4%, 54,483G)
TheChainNerd: 947G (8.0%, 947G)
HaplessOne: 777G (6.6%, 12,607G)
Lydian_C: 756G (6.4%, 756G)
aStatue: 600G (5.1%, 600G)
ko2q: 514G (4.3%, 514G)
FoxtrotNovemberCharlie: 284G (2.4%, 284G)
Zachara: 212G (1.8%, 44,012G)
datadrivenbot: 100G (0.8%, 11,595G)
Rislyeu: 100G (0.8%, 879G)
getthemoneyz: 36G (0.3%, 608,734G)

yellow bets:
HaateXIII: 12,574G (82.3%, 25,149G)
UmaiJam: 1,000G (6.5%, 15,375G)
megaman2202: 631G (4.1%, 631G)
ungabunga_bot: 621G (4.1%, 290,841G)
belife42: 250G (1.6%, 250G)
HorusTaurus: 200G (1.3%, 11,506G)

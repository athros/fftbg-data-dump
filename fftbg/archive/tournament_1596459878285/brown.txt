Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DavenIII
Male
Scorpio
65
77
Thief
Jump
Meatbone Slash
Dual Wield
Move+3

Cultist Dagger
Zorlin Shape
Flash Hat
Secret Clothes
Small Mantle

Gil Taking, Steal Shield
Level Jump8, Vertical Jump8



EmmaEnema
Male
Aquarius
45
73
Calculator
White Magic
Earplug
Secret Hunt
Fly

Papyrus Codex

Green Beret
Mystic Vest
Angel Ring

CT, 4, 3
Cure, Cure 3, Raise, Shell, Esuna



JumbocactuarX27
Male
Gemini
42
68
Archer
Draw Out
Counter Tackle
Equip Knife
Move-MP Up

Kunai
Platinum Shield
Green Beret
Leather Outfit
Bracer

Charge+1, Charge+2, Charge+4
Muramasa



VolgraTheMoose
Female
Aquarius
45
68
Summoner
Basic Skill
Counter Flood
Short Charge
Move-HP Up

Poison Rod

Headgear
Power Sleeve
Magic Gauntlet

Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Leviathan
Heal, Tickle, Cheer Up, Wish

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dem0nj0ns
Female
Aries
52
47
Chemist
Charge
HP Restore
Defend
Lava Walking

Cute Bag

Headgear
Mystic Vest
Magic Gauntlet

Potion, Maiden's Kiss, Soft, Phoenix Down
Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+10



Byrdturbo
Male
Scorpio
62
59
Wizard
Charge
Absorb Used MP
Defense UP
Lava Walking

Dragon Rod

Feather Hat
Judo Outfit
108 Gems

Fire 3, Bolt, Bolt 2, Ice 3, Frog, Death
Charge+2, Charge+5, Charge+20



Superdevon1
Male
Taurus
77
54
Oracle
Time Magic
Counter Magic
Concentrate
Move+1

Cypress Rod

Holy Miter
Mystic Vest
Sprint Shoes

Poison, Life Drain, Zombie, Foxbird, Dispel Magic
Haste 2, Stop, Quick



Lythe Caraker
Male
Gemini
60
46
Calculator
Black Magic
Parry
Doublehand
Move-HP Up

Bestiary

Leather Hat
Chameleon Robe
Magic Ring

CT, Height, Prime Number, 5, 4
Fire 2, Fire 3, Bolt 3, Empower, Frog

Player: !Red
Team: Red Team
Palettes: Red/Brown



Serperemagus
Female
Gemini
51
81
Chemist
Jump
Absorb Used MP
Equip Gun
Jump+3

Blast Gun

Golden Hairpin
Power Sleeve
Elf Mantle

Potion, Ether, Hi-Ether, Holy Water, Phoenix Down
Level Jump5, Vertical Jump8



Chuckolator
Male
Aries
60
56
Knight
Basic Skill
Absorb Used MP
Long Status
Retreat

Defender
Bronze Shield
Iron Helmet
Gold Armor
Battle Boots

Head Break, Shield Break, Speed Break, Justice Sword, Dark Sword
Accumulate, Dash, Throw Stone, Heal, Yell, Wish



Furaime
Male
Sagittarius
47
74
Chemist
Yin Yang Magic
Distribute
Dual Wield
Move+3

Main Gauche
Cute Bag
Triangle Hat
Mythril Vest
Battle Boots

Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Poison, Spell Absorb, Dispel Magic



Dantayystv
Male
Taurus
49
54
Bard
White Magic
Faith Save
Attack UP
Jump+3

Bloody Strings

Twist Headband
Platinum Armor
Bracer

Angel Song, Battle Song, Nameless Song, Last Song, Hydra Pit
Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Shell, Shell 2, Esuna, Holy

Final Bets: white - 7 bets for 7,317G (47.2%, x1.12); purple - 11 bets for 8,170G (52.8%, x0.90)

white bets:
Lydian_C: 4,200G (57.4%, 119,073G)
randgridr: 1,216G (16.6%, 1,216G)
TheMurkGnome: 999G (13.7%, 5,343G)
Lythe_Caraker: 250G (3.4%, 147,487G)
Lemonjohns: 244G (3.3%, 244G)
dem0nj0ns: 208G (2.8%, 208G)
datadrivenbot: 200G (2.7%, 63,083G)

purple bets:
Mesmaster: 2,000G (24.5%, 47,419G)
dantayystv: 1,265G (15.5%, 1,265G)
BirbBrainsBot: 1,000G (12.2%, 4,154G)
NicoSavoy: 1,000G (12.2%, 186,538G)
Evewho: 728G (8.9%, 728G)
PrancesWithWolves: 661G (8.1%, 661G)
Zetchryn: 424G (5.2%, 424G)
superdevon1: 420G (5.1%, 420G)
RoboticNomad: 300G (3.7%, 3,322G)
amiture: 280G (3.4%, 280G)
getthemoneyz: 92G (1.1%, 1,247,905G)

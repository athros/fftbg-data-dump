Player: !Red
Team: Red Team
Palettes: Red/Brown



Roqqqpsi
Male
Cancer
52
41
Thief
Draw Out
Blade Grasp
Martial Arts
Lava Walking

Coral Sword

Thief Hat
Clothes
Red Shoes

Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Leg Aim
Asura, Kiyomori, Muramasa, Masamune



Treapvort
Female
Cancer
78
42
Wizard
Math Skill
Mana Shield
Martial Arts
Swim

Wizard Rod

Feather Hat
White Robe
Defense Ring

Bolt 2, Ice 2, Ice 3, Ice 4, Death
CT, Height, Prime Number, 3



CassiePhoenix
Female
Capricorn
40
64
Time Mage
Battle Skill
Counter Flood
Equip Bow
Jump+2

Lightning Bow

Triangle Hat
Chameleon Robe
Diamond Armlet

Haste 2, Slow, Slow 2, Stop, Quick, Demi, Demi 2, Stabilize Time, Galaxy Stop
Shield Break



Rechaun
Female
Gemini
40
59
Mime

Absorb Used MP
Secret Hunt
Teleport



Leather Hat
Power Sleeve
Spike Shoes

Mimic


Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dakren
Male
Taurus
64
57
Samurai
White Magic
Counter Tackle
Short Charge
Ignore Terrain

Muramasa

Bronze Helmet
Crystal Mail
Jade Armlet

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Raise, Raise 2, Protect 2, Wall, Esuna



Camelchoir
Female
Aquarius
67
58
Mime

PA Save
Equip Shield
Move-MP Up


Diamond Shield
Headgear
Chain Vest
Diamond Armlet

Mimic




RuneS 77
Male
Capricorn
53
54
Lancer
Talk Skill
Sunken State
Equip Gun
Move+2

Spear
Crystal Shield
Platinum Helmet
Crystal Mail
Power Wrist

Level Jump3, Vertical Jump8
Persuade, Praise, Threaten, Mimic Daravon, Refute



MaouDono
Male
Libra
68
46
Calculator
Behemoth Skill
Dragon Spirit
Dual Wield
Levitate

Battle Folio
Papyrus Codex
Twist Headband
Leather Vest
Sprint Shoes

Blue Magic
Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest

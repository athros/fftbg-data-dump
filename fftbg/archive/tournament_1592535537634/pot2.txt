Final Bets: green - 17 bets for 8,059G (50.6%, x0.98); yellow - 11 bets for 7,875G (49.4%, x1.02)

green bets:
DLJuggernaut: 1,370G (17.0%, 1,370G)
HuffFlex: 1,000G (12.4%, 8,621G)
BirbBrainsBot: 1,000G (12.4%, 91,170G)
Oobs56: 897G (11.1%, 897G)
ANFz: 742G (9.2%, 742G)
Bilabrin: 500G (6.2%, 3,926G)
skillomono: 500G (6.2%, 1,578G)
Baron_von_Scrub: 436G (5.4%, 436G)
getthemoneyz: 394G (4.9%, 983,009G)
ColetteMSLP: 300G (3.7%, 2,613G)
GladiatorLupe: 200G (2.5%, 5,853G)
Evewho: 200G (2.5%, 4,864G)
cheesy_e: 200G (2.5%, 295G)
sir_uther115: 100G (1.2%, 1,158G)
mrfripps: 100G (1.2%, 4,538G)
JoeykinsX: 100G (1.2%, 20,156G)
lewdylew: 20G (0.2%, 215G)

yellow bets:
SkylerBunny: 2,000G (25.4%, 126,119G)
Thyrandaal: 2,000G (25.4%, 57,539G)
Digitalsocrates: 1,000G (12.7%, 63,443G)
DustBirdEX: 789G (10.0%, 2,820G)
prince_rogers_nelson_: 572G (7.3%, 572G)
KingofTricksters: 500G (6.3%, 682G)
CapnChaos12: 300G (3.8%, 15,961G)
LAGBOT30000: 300G (3.8%, 2,364G)
DuraiPapers: 264G (3.4%, 264G)
fluffskull: 100G (1.3%, 1,235G)
ericzubat: 50G (0.6%, 768G)

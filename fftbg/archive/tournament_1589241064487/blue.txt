Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dymntd
Male
Capricorn
67
50
Priest
Draw Out
Blade Grasp
Beastmaster
Lava Walking

Gold Staff

Feather Hat
Clothes
Small Mantle

Cure 4, Raise, Regen
Koutetsu, Bizen Boat, Muramasa, Chirijiraden



ExecutedGiraffe
Monster
Libra
49
64
Revenant










Kronikle
Female
Aries
72
44
Mediator
Time Magic
Parry
Dual Wield
Teleport

Romanda Gun
Blast Gun
Red Hood
Leather Outfit
Vanish Mantle

Invitation, Persuade, Praise, Preach, Death Sentence, Refute, Rehabilitate
Immobilize, Float, Demi 2, Stabilize Time



Meta Five
Female
Libra
80
71
Thief
Charge
Damage Split
Equip Armor
Ignore Height

Short Edge

Thief Hat
Chameleon Robe
Sprint Shoes

Steal Helmet, Steal Accessory, Arm Aim
Charge+1, Charge+2, Charge+20

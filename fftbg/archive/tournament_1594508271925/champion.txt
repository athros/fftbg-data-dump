Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



RageImmortaI
Male
Scorpio
44
66
Knight
Basic Skill
Damage Split
Magic Attack UP
Jump+2

Chaos Blade
Bronze Shield
Mythril Helmet
Reflect Mail
Small Mantle

Head Break, Weapon Break, Power Break, Stasis Sword, Dark Sword, Night Sword, Surging Sword
Accumulate, Dash, Throw Stone, Heal, Yell, Cheer Up



Peeronid
Female
Virgo
70
59
Geomancer
Black Magic
Meatbone Slash
Equip Polearm
Ignore Terrain

Octagon Rod
Escutcheon
Black Hood
Secret Clothes
Vanish Mantle

Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind
Bolt 2, Bolt 3, Ice 3



Breakdown777
Female
Leo
42
63
Lancer
Talk Skill
MP Restore
Monster Talk
Swim

Javelin
Bronze Shield
Platinum Helmet
Wizard Robe
Magic Gauntlet

Level Jump4, Vertical Jump3
Invitation, Praise, Threaten, Insult, Mimic Daravon, Rehabilitate



Just Here2
Female
Leo
56
48
Monk
Time Magic
HP Restore
Dual Wield
Move+2



Golden Hairpin
Power Sleeve
Genji Gauntlet

Spin Fist, Wave Fist, Purification
Haste, Haste 2, Slow, Stop, Stabilize Time, Meteor

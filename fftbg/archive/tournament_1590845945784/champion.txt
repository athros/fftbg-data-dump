Player: !zChamp
Team: Champion Team
Palettes: Black/Red



TheGuesty
Monster
Aries
49
74
Red Chocobo










Gogofromtogo
Female
Serpentarius
45
75
Mediator
Draw Out
Counter Magic
Defense UP
Lava Walking

Star Bag

Leather Hat
Leather Outfit
Feather Boots

Insult, Negotiate, Rehabilitate
Asura



UmaiJam
Female
Cancer
48
60
Time Mage
Item
Hamedo
Throw Item
Move-HP Up

Oak Staff

Thief Hat
Mystic Vest
Sprint Shoes

Haste, Haste 2, Slow, Slow 2, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Remedy, Phoenix Down



Brokenknight201
Female
Taurus
56
74
Knight
White Magic
Counter
Magic Attack UP
Move+2

Ancient Sword
Diamond Shield
Bronze Helmet
Chameleon Robe
Power Wrist

Armor Break, Mind Break, Stasis Sword
Cure, Cure 2, Cure 3, Cure 4, Raise 2, Protect, Esuna, Magic Barrier

Player: !Red
Team: Red Team
Palettes: Red/Brown



Nhammen
Female
Leo
42
76
Dancer
Battle Skill
Dragon Spirit
Equip Knife
Jump+2

Kunai

Triangle Hat
Brigandine
Spike Shoes

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Last Dance, Nether Demon, Dragon Pit
Weapon Break, Dark Sword



Vorap
Male
Pisces
76
55
Samurai
Black Magic
Sunken State
Doublehand
Jump+3

Kiyomori

Gold Helmet
Bronze Armor
Battle Boots

Heaven's Cloud
Fire 2, Fire 4, Bolt, Ice 2



NicoSavoy
Male
Gemini
72
67
Archer
Throw
Earplug
Doublehand
Move-MP Up

Perseus Bow

Holy Miter
Adaman Vest
Battle Boots

Charge+1, Charge+20
Shuriken, Bomb, Ninja Sword



Moshyhero
Male
Aries
51
64
Samurai
Elemental
Counter
Magic Attack UP
Waterbreathing

Muramasa

Grand Helmet
Silk Robe
Feather Boots

Asura, Heaven's Cloud
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

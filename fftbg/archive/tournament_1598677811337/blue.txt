Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DeathRtopper
Male
Capricorn
51
45
Samurai
Steal
PA Save
Defend
Teleport

Koutetsu Knife

Crystal Helmet
Bronze Armor
Dracula Mantle

Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Steal Heart, Steal Helmet, Steal Accessory



TasisSai
Monster
Scorpio
55
57
Steel Giant










Skillomono
Male
Aries
65
66
Geomancer
Draw Out
Auto Potion
Equip Polearm
Jump+2

Javelin
Platinum Shield
Twist Headband
Chain Vest
Battle Boots

Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Koutetsu, Murasame



WesSideVandal
Male
Scorpio
55
63
Geomancer
Steal
Damage Split
Defend
Move-MP Up

Battle Axe
Diamond Shield
Black Hood
Light Robe
Dracula Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Steal Heart, Steal Armor, Steal Shield, Steal Status

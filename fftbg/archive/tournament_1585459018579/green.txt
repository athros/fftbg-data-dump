Player: !Green
Team: Green Team
Palettes: Green/White



Boojob
Female
Capricorn
71
72
Dancer
Battle Skill
Damage Split
Short Charge
Move+3

Hydra Bag

Golden Hairpin
Rubber Costume
Magic Gauntlet

Witch Hunt, Nether Demon
Armor Break, Shield Break, Magic Break, Power Break, Stasis Sword, Justice Sword



HASTERIOUS
Female
Cancer
62
45
Summoner
Basic Skill
Earplug
Short Charge
Jump+2

Thunder Rod

Holy Miter
Power Sleeve
Rubber Shoes

Moogle, Carbunkle, Bahamut, Silf, Lich
Heal, Tickle, Yell, Cheer Up, Wish



Chuckolator
Female
Leo
51
56
Mediator
Battle Skill
Catch
Doublehand
Move+2

Hydra Bag

Red Hood
Silk Robe
Spike Shoes

Persuade, Praise, Negotiate
Weapon Break, Speed Break, Mind Break, Justice Sword



Baconbacon1207
Female
Aquarius
52
47
Calculator
Black Magic
MP Restore
Short Status
Levitate

Papyrus Codex

Green Beret
Light Robe
Spike Shoes

CT, 4, 3
Bolt, Bolt 4, Ice 2, Empower, Frog, Death

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Typicalfanboy
Female
Scorpio
77
58
Summoner
Item
Counter
Short Charge
Move+3

Dragon Rod

Green Beret
Power Sleeve
Wizard Mantle

Salamander, Cyclops
Potion, Hi-Potion, X-Potion, Ether, Antidote, Phoenix Down



Archpatissier
Male
Cancer
48
71
Ninja
Yin Yang Magic
Faith Up
Equip Polearm
Jump+1

Persia
Ryozan Silk
Headgear
Mythril Vest
Wizard Mantle

Shuriken, Bomb
Spell Absorb, Zombie, Dispel Magic, Paralyze



ArchaicHerald
Male
Taurus
78
62
Knight
Punch Art
Counter
Equip Gun
Waterbreathing

Blood Sword
Crystal Shield
Crystal Helmet
Bronze Armor
Defense Ring

Mind Break
Spin Fist, Pummel, Purification, Revive, Seal Evil



Oobs56
Male
Taurus
69
58
Chemist
Summon Magic
MA Save
Short Status
Move-HP Up

Cute Bag

Twist Headband
Earth Clothes
Feather Mantle

Ether, Hi-Ether, Eye Drop, Phoenix Down
Ramuh, Carbunkle, Silf, Fairy

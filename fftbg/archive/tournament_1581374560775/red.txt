Player: !Red
Team: Red Team
Palettes: Red/Brown



Nightyukiame
Female
Pisces
63
79
Summoner
Punch Art
Distribute
Secret Hunt
Move+3

Rainbow Staff

Triangle Hat
Wizard Robe
108 Gems

Moogle, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Lich
Spin Fist, Wave Fist, Secret Fist, Purification



Giantdeathlaser
Female
Sagittarius
48
67
Samurai
Yin Yang Magic
Critical Quick
Doublehand
Jump+3

Heaven's Cloud

Gold Helmet
Mythril Armor
Bracer

Asura, Kiyomori, Muramasa
Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Sleep



Iqunlimited
Female
Cancer
58
80
Archer
Summon Magic
MP Restore
Short Status
Waterwalking

Poison Bow
Diamond Shield
Cachusha
Leather Outfit
Angel Ring

Charge+4, Charge+5
Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Silf



Ser Capelli
Female
Libra
50
70
Samurai
Battle Skill
Earplug
Dual Wield
Ignore Height

Partisan
Gungnir
Diamond Helmet
Diamond Armor
Defense Ring

Koutetsu, Kikuichimoji
Power Break, Mind Break, Dark Sword

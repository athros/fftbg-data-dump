Final Bets: purple - 24 bets for 13,067G (37.8%, x1.64); brown - 18 bets for 21,466G (62.2%, x0.61)

purple bets:
HaateXIII: 2,166G (16.6%, 4,249G)
Jeeboheebo: 1,462G (11.2%, 1,462G)
Zeroroute: 1,345G (10.3%, 3,396G)
ColetteMSLP: 1,000G (7.7%, 30,435G)
getthemoneyz: 880G (6.7%, 613,561G)
RunicMagus: 850G (6.5%, 49,679G)
dorman_777: 736G (5.6%, 736G)
genericco: 518G (4.0%, 1,017G)
TheMM42: 500G (3.8%, 8,830G)
vorap: 500G (3.8%, 32,581G)
FoeSquirrel: 416G (3.2%, 832G)
Bahamut64: 400G (3.1%, 13,922G)
Sungazes: 350G (2.7%, 2,208G)
RyuTsuno: 324G (2.5%, 324G)
SarrgeQc: 300G (2.3%, 3,418G)
tsdZero: 250G (1.9%, 1,853G)
CoderCalamity: 220G (1.7%, 220G)
twelfthrootoftwo: 200G (1.5%, 2,128G)
carsenhk: 150G (1.1%, 11,333G)
Firesheath: 100G (0.8%, 12,998G)
nifboy: 100G (0.8%, 5,689G)
datadrivenbot: 100G (0.8%, 14,698G)
Bioticism: 100G (0.8%, 7,603G)
Realitydown: 100G (0.8%, 4,932G)

brown bets:
Arunic: 3,519G (16.4%, 3,519G)
OmnibotGamma: 2,875G (13.4%, 2,875G)
killth3kid: 2,000G (9.3%, 43,141G)
ApplesNP: 1,606G (7.5%, 1,606G)
jakoholic: 1,500G (7.0%, 6,021G)
sinnyil2: 1,500G (7.0%, 22,574G)
DeathTaxesAndAnime: 1,485G (6.9%, 2,912G)
TheChainNerd: 1,000G (4.7%, 12,484G)
BirbBrainsBot: 1,000G (4.7%, 10,098G)
volgrathemoose: 985G (4.6%, 1,971G)
Grimmace45: 983G (4.6%, 3,934G)
ungabunga_bot: 866G (4.0%, 335,645G)
fenaen: 737G (3.4%, 737G)
EnemyController: 500G (2.3%, 178,919G)
luminarii: 500G (2.3%, 10,599G)
saucy_wings: 200G (0.9%, 946G)
ZephyrTempest: 110G (0.5%, 2,216G)
maakur_: 100G (0.5%, 119,905G)

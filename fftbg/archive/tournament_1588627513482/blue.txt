Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ExecutedGiraffe
Male
Leo
77
73
Chemist
Sing
Counter Flood
Halve MP
Retreat

Blaze Gun

Holy Miter
Black Costume
Battle Boots

Potion, Hi-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy
Angel Song, Cheer Song, Battle Song, Nameless Song, Last Song, Space Storage, Sky Demon, Hydra Pit



Sungazes
Male
Scorpio
74
49
Time Mage
Steal
Dragon Spirit
Magic Attack UP
Levitate

Oak Staff

Barette
Light Robe
Germinas Boots

Slow, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Demi, Demi 2
Steal Heart, Steal Shield



0v3rr8d
Female
Taurus
71
79
Samurai
Talk Skill
Regenerator
Attack UP
Levitate

Partisan

Barbuta
Chameleon Robe
Genji Gauntlet

Murasame, Kiyomori, Kikuichimoji
Praise, Threaten, Preach, Mimic Daravon, Refute, Rehabilitate



Nizaha
Male
Sagittarius
77
52
Mime

Speed Save
Martial Arts
Jump+3



Green Beret
Clothes
Spike Shoes

Mimic


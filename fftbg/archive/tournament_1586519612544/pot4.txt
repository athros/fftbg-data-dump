Final Bets: purple - 12 bets for 10,443G (78.7%, x0.27); brown - 5 bets for 2,820G (21.3%, x3.70)

purple bets:
sinnyil2: 3,144G (30.1%, 3,144G)
TheChainNerd: 2,220G (21.3%, 2,220G)
BirbBrainsBot: 1,000G (9.6%, 47,073G)
Rastanar: 1,000G (9.6%, 10,703G)
datadrivenbot: 905G (8.7%, 25,123G)
jhazor: 869G (8.3%, 869G)
RubenFlonne: 388G (3.7%, 388G)
JumbocactuarX27: 312G (3.0%, 312G)
AllInBot: 305G (2.9%, 305G)
maakur_: 100G (1.0%, 3,832G)
Aneyus: 100G (1.0%, 9,771G)
HeroponThrawn: 100G (1.0%, 6,965G)

brown bets:
kai_shee: 1,001G (35.5%, 127,593G)
JustSuperish: 1,000G (35.5%, 4,644G)
TheMurkGnome: 707G (25.1%, 707G)
IndecisiveNinja: 100G (3.5%, 2,276G)
getthemoneyz: 12G (0.4%, 464,871G)

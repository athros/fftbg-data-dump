Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Wurstwaesserchen
Female
Taurus
60
39
Samurai
Time Magic
Mana Shield
Equip Gun
Ignore Terrain

Battle Folio

Crystal Helmet
Gold Armor
Wizard Mantle

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Haste, Haste 2, Slow 2, Stop, Immobilize, Float, Demi, Demi 2, Stabilize Time, Meteor



Dynasti
Male
Libra
42
54
Ninja
Punch Art
Mana Shield
Maintenance
Move+1

Hidden Knife
Short Edge
Feather Hat
Mythril Vest
Jade Armlet

Shuriken
Purification, Seal Evil



Volgrathemoose
Male
Gemini
55
42
Archer
Black Magic
Absorb Used MP
Doublehand
Fly

Night Killer

Green Beret
Earth Clothes
Bracer

Charge+1, Charge+4, Charge+7
Fire, Bolt, Bolt 2, Bolt 3, Ice, Empower



Upvla
Male
Scorpio
79
67
Archer
Basic Skill
Damage Split
Doublehand
Waterbreathing

Poison Bow

Triangle Hat
Clothes
Sprint Shoes

Charge+7
Accumulate, Throw Stone, Tickle

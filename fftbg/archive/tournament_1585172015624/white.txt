Player: !White
Team: White Team
Palettes: White/Blue



Bad1dea
Male
Leo
43
72
Priest
Summon Magic
Dragon Spirit
Sicken
Ignore Height

Oak Staff

Black Hood
Chain Vest
Reflect Ring

Cure 3, Raise, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna, Holy
Ramuh, Ifrit, Carbunkle, Silf, Cyclops



HaateXIII
Monster
Leo
54
70
Great Malboro










Ricky Ortease
Male
Leo
49
69
Bard
Item
Abandon
Equip Armor
Jump+2

Ramia Harp

Barbuta
Wizard Outfit
Battle Boots

Battle Song, Nameless Song, Diamond Blade
Maiden's Kiss, Remedy, Phoenix Down



AnthraX85
Male
Capricorn
75
47
Ninja
Summon Magic
HP Restore
Halve MP
Jump+3

Flame Whip
Morning Star
Red Hood
Earth Clothes
Diamond Armlet

Bomb, Knife, Spear, Dictionary
Golem, Carbunkle, Bahamut, Odin, Leviathan, Cyclops

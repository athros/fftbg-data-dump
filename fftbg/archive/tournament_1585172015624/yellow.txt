Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Male
Virgo
72
48
Bard
Item
Counter Magic
Martial Arts
Jump+2



Feather Hat
Secret Clothes
Magic Ring

Angel Song, Life Song, Battle Song, Nameless Song, Sky Demon
Ether, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down



Grasnikk
Male
Gemini
63
60
Samurai
Basic Skill
Brave Up
Attack UP
Teleport

Heaven's Cloud

Barbuta
White Robe
Magic Gauntlet

Koutetsu
Heal, Tickle



CapnChaos12
Female
Leo
46
57
Archer
Item
Counter Magic
Defense UP
Waterbreathing

Night Killer
Crystal Shield
Leather Helmet
Brigandine
Genji Gauntlet

Charge+20
Potion, X-Potion, Ether, Remedy



Jordache7K
Female
Aries
40
46
Thief
Elemental
Sunken State
Concentrate
Move+3

Mage Masher

Headgear
Black Costume
Battle Boots

Steal Helmet, Steal Armor, Steal Status
Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

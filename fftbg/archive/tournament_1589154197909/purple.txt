Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheJonTerp
Female
Aries
55
48
Time Mage
Black Magic
Counter
Defense UP
Swim

Oak Staff

Leather Hat
Clothes
Jade Armlet

Haste, Slow, Float, Stabilize Time
Bolt 3, Ice, Empower



FriendSquirrel
Male
Capricorn
63
54
Squire
Time Magic
Arrow Guard
Dual Wield
Retreat

Night Killer
Poison Bow
Mythril Helmet
Power Sleeve
Bracer

Dash, Heal
Haste, Haste 2, Slow, Slow 2, Reflect, Stabilize Time, Galaxy Stop



GrandmasterFrankerZ
Male
Gemini
56
47
Thief
Item
Faith Up
Defense UP
Teleport

Ninja Edge

Feather Hat
Mystic Vest
Bracer

Steal Heart, Steal Accessory, Steal Status
Potion, X-Potion, Ether, Antidote, Echo Grass, Holy Water, Phoenix Down



ColetteMSLP
Male
Leo
64
52
Thief
Battle Skill
Arrow Guard
Dual Wield
Ignore Terrain

Blind Knife
Mage Masher
Feather Hat
Adaman Vest
Magic Ring

Steal Heart, Steal Status, Leg Aim
Head Break, Explosion Sword

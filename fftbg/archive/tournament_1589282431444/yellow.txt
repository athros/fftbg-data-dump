Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Roqqqpsi
Female
Scorpio
54
79
Chemist
Time Magic
Damage Split
Magic Attack UP
Lava Walking

Hydra Bag

Black Hood
Wizard Outfit
Spike Shoes

Potion, X-Potion, Antidote, Eye Drop, Remedy, Phoenix Down
Haste, Slow, Slow 2, Demi



Gooseyourself
Male
Aries
80
62
Chemist
Throw
Mana Shield
Attack UP
Fly

Panther Bag

Black Hood
Chain Vest
Germinas Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down
Shuriken, Staff



Witchlight
Female
Taurus
42
64
Dancer
Basic Skill
Critical Quick
Equip Polearm
Move-MP Up

Cypress Rod

Triangle Hat
Brigandine
Bracer

Slow Dance, Last Dance, Void Storage, Nether Demon
Heal, Cheer Up



CassiePhoenix
Female
Pisces
72
71
Wizard
Draw Out
Abandon
Defense UP
Jump+3

Poison Rod

Leather Hat
Adaman Vest
Cherche

Fire 3, Bolt 2, Bolt 3, Ice 4, Empower
Murasame, Kikuichimoji

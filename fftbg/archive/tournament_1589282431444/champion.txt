Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Otakutaylor
Male
Taurus
59
53
Knight
Item
Absorb Used MP
Sicken
Teleport

Blood Sword
Bronze Shield
Crystal Helmet
Chain Mail
Dracula Mantle

Weapon Break, Magic Break, Power Break, Surging Sword
Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Holy Water, Remedy



Dexsana
Monster
Aquarius
66
48
Holy Dragon










PotionDweller
Male
Sagittarius
50
40
Archer
Sing
Damage Split
Short Charge
Ignore Height

Stone Gun
Diamond Shield
Triangle Hat
Secret Clothes
Diamond Armlet

Charge+5, Charge+10
Angel Song, Life Song, Cheer Song, Battle Song, Space Storage



Leakimiko
Female
Sagittarius
79
62
Wizard
Yin Yang Magic
Mana Shield
Short Charge
Jump+2

Main Gauche

Twist Headband
Black Costume
Defense Ring

Fire, Fire 2, Fire 4, Bolt 2, Bolt 3, Ice 4, Empower, Frog, Death, Flare
Poison, Life Drain, Pray Faith, Dispel Magic, Paralyze, Petrify

Player: !Red
Team: Red Team
Palettes: Red/Brown



Nekojin
Female
Leo
51
65
Monk
Dance
Parry
Concentrate
Move-MP Up



Feather Hat
Earth Clothes
Feather Boots

Earth Slash, Chakra, Seal Evil
Wiznaibus, Slow Dance, Void Storage



TeaTime29
Female
Taurus
44
76
Knight
Throw
Caution
Martial Arts
Teleport


Ice Shield
Circlet
Linen Robe
Spike Shoes

Shield Break, Weapon Break, Power Break, Mind Break, Surging Sword
Shuriken, Bomb, Staff



Evdoggity
Male
Capricorn
47
70
Oracle
Steal
Distribute
Long Status
Waterbreathing

Battle Folio

Leather Hat
White Robe
Defense Ring

Poison, Silence Song, Blind Rage, Foxbird, Paralyze, Sleep
Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim



AniZero
Male
Aquarius
54
76
Squire
Steal
Speed Save
Equip Gun
Retreat

Papyrus Codex
Kaiser Plate
Platinum Helmet
Mystic Vest
Defense Armlet

Accumulate, Dash, Heal, Tickle, Yell, Cheer Up, Wish, Scream
Steal Helmet, Steal Weapon, Steal Status

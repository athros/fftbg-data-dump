Final Bets: purple - 8 bets for 4,949G (60.3%, x0.66); brown - 7 bets for 3,260G (39.7%, x1.52)

purple bets:
resjudicata3: 3,000G (60.6%, 6,069G)
superdevon1: 748G (15.1%, 74,871G)
3ngag3: 500G (10.1%, 500G)
datadrivenbot: 200G (4.0%, 62,835G)
TheDali23: 200G (4.0%, 2,002G)
gorgewall: 101G (2.0%, 5,075G)
ko2q: 100G (2.0%, 1,093G)
windsah: 100G (2.0%, 3,496G)

brown bets:
BirbBrainsBot: 1,000G (30.7%, 76,353G)
getthemoneyz: 1,000G (30.7%, 1,619,595G)
amiture: 500G (15.3%, 3,515G)
Chuckolator: 360G (11.0%, 360G)
AllInBot: 250G (7.7%, 250G)
MinBetBot: 100G (3.1%, 4,980G)
Drusiform: 50G (1.5%, 5,114G)

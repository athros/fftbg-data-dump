Player: !Green
Team: Green Team
Palettes: Green/White



AM3P Batman
Male
Capricorn
48
66
Geomancer
Jump
Counter
Halve MP
Levitate

Giant Axe
Aegis Shield
Headgear
Wizard Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Level Jump2, Vertical Jump7



Basmal
Male
Sagittarius
40
60
Ninja
Punch Art
Meatbone Slash
Equip Knife
Retreat

Kunai
Ice Rod
Flash Hat
Earth Clothes
Jade Armlet

Shuriken, Sword
Spin Fist, Pummel



Hales Bopp It
Female
Aquarius
57
56
Time Mage
Summon Magic
Damage Split
Equip Sword
Move-MP Up

Ice Brand

Flash Hat
Chain Vest
Wizard Mantle

Haste, Slow, Slow 2, Stop, Immobilize, Reflect, Stabilize Time
Moogle, Golem, Bahamut, Silf, Fairy, Cyclops



Maakur
Female
Libra
58
53
Time Mage
Battle Skill
Arrow Guard
Maintenance
Move-HP Up

Star Bag

Leather Hat
Wizard Robe
Battle Boots

Haste, Haste 2, Slow, Immobilize, Float, Reflect, Demi 2, Meteor, Galaxy Stop
Head Break, Weapon Break, Magic Break, Justice Sword, Surging Sword, Explosion Sword

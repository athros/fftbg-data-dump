Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KasugaiRoastedPeas
Male
Leo
50
69
Knight
Summon Magic
Parry
Concentrate
Ignore Height

Coral Sword
Venetian Shield
Barbuta
Chameleon Robe
Magic Ring

Head Break, Magic Break, Speed Break, Justice Sword, Surging Sword
Titan, Golem, Bahamut, Fairy



DavenIII
Female
Libra
39
53
Calculator
Wildfire Skill
Arrow Guard
Beastmaster
Move+1

Bestiary
Genji Shield
Gold Helmet
Carabini Mail
Elf Mantle

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit



Jeeboheebo
Male
Taurus
61
52
Calculator
Time Magic
Counter
Concentrate
Swim

Bestiary

Leather Hat
Linen Robe
Defense Ring

CT, Height, 5, 4
Stop, Float, Stabilize Time



Zachara
Male
Aries
78
60
Monk
Draw Out
Sunken State
Secret Hunt
Teleport



Golden Hairpin
Power Sleeve
Rubber Shoes

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Koutetsu, Murasame, Heaven's Cloud

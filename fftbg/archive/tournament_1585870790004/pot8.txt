Final Bets: purple - 17 bets for 11,390G (28.3%, x2.54); champion - 34 bets for 28,901G (71.7%, x0.39)

purple bets:
DudeMonkey77: 5,225G (45.9%, 10,450G)
sinnyil2: 1,299G (11.4%, 55,964G)
Shakarak: 1,000G (8.8%, 3,323G)
kingchadking: 905G (7.9%, 905G)
Laserman1000: 597G (5.2%, 9,697G)
Bryon_W: 500G (4.4%, 5,115G)
LuckyLuckLuc2: 333G (2.9%, 15,767G)
robespyah: 232G (2.0%, 232G)
AllInBot: 219G (1.9%, 219G)
TheMurkGnome: 204G (1.8%, 204G)
KasugaiRoastedPeas: 200G (1.8%, 24,064G)
Adam1949: 140G (1.2%, 140G)
NytFantom: 128G (1.1%, 128G)
LeepingJJ: 108G (0.9%, 2,380G)
maakur_: 100G (0.9%, 1,959G)
SeedSC: 100G (0.9%, 134,801G)
CorpusCav: 100G (0.9%, 6,264G)

champion bets:
Conome: 6,570G (22.7%, 6,570G)
ugoplatamia: 4,092G (14.2%, 20,461G)
urieltheflameofgod: 3,500G (12.1%, 6,847G)
Lodrak: 1,659G (5.7%, 1,659G)
HaplessOne: 1,500G (5.2%, 33,309G)
BirbBrainsBot: 1,000G (3.5%, 148,636G)
bad1dea: 1,000G (3.5%, 365,653G)
I_nod_my_head_when_I_lose: 1,000G (3.5%, 11,697G)
MoonSlayerRS: 800G (2.8%, 1,680G)
old_overholt_: 725G (2.5%, 725G)
soapydragonfly: 719G (2.5%, 719G)
Lanshaft: 600G (2.1%, 2,363G)
SkylerBunny: 600G (2.1%, 600G)
volgrathemoose: 501G (1.7%, 13,776G)
Zeroroute: 500G (1.7%, 500G)
Vampire_Killer: 500G (1.7%, 2,294G)
Virilikus: 416G (1.4%, 416G)
WireLord: 376G (1.3%, 376G)
ZephyrTempest: 368G (1.3%, 19,961G)
Jordache7K: 333G (1.2%, 12,589G)
Maeveen: 300G (1.0%, 300G)
Cataphract116: 300G (1.0%, 2,331G)
SkeletorRS: 281G (1.0%, 281G)
DeathTaxesAndAnime: 200G (0.7%, 943G)
chronoxtrigger: 200G (0.7%, 4,835G)
JIDkomu: 144G (0.5%, 144G)
Andrain: 116G (0.4%, 116G)
ApplesauceBoss: 100G (0.3%, 1,998G)
Tithonus: 100G (0.3%, 6,475G)
lijarkh: 100G (0.3%, 6,587G)
Mechapope1: 100G (0.3%, 3,191G)
datadrivenbot: 100G (0.3%, 15,879G)
BadBlanket: 100G (0.3%, 5,163G)
ko2q: 1G (0.0%, 1,671G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grandlanzer
Female
Pisces
64
52
Archer
Talk Skill
Abandon
Beastmaster
Waterbreathing

Silver Bow

Black Hood
Brigandine
Rubber Shoes

Charge+1, Charge+3, Charge+5
Invitation, Threaten



Lokenwow
Male
Virgo
73
52
Squire
Item
Speed Save
Halve MP
Move+2

Bow Gun
Diamond Shield
Black Hood
Diamond Armor
Wizard Mantle

Accumulate, Dash, Tickle, Cheer Up
Eye Drop, Soft, Remedy, Phoenix Down



Brokenknight201
Male
Serpentarius
71
71
Time Mage
Draw Out
Abandon
Long Status
Move+3

Rainbow Staff

Black Hood
Silk Robe
Bracer

Haste, Slow, Slow 2, Float, Reflect, Quick, Demi 2, Stabilize Time
Asura, Kiyomori



Codetravis
Male
Virgo
64
76
Ninja
Talk Skill
Sunken State
Equip Sword
Lava Walking

Defender
Ninja Edge
Green Beret
Brigandine
Sprint Shoes

Axe
Invitation, Persuade

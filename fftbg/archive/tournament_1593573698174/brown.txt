Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ALY327
Female
Sagittarius
61
80
Chemist
Yin Yang Magic
Meatbone Slash
Equip Polearm
Move+1

Gokuu Rod

Leather Hat
Power Sleeve
Angel Ring

Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Blind, Zombie, Silence Song, Dispel Magic, Paralyze



Just Here2
Female
Sagittarius
73
42
Mime

Damage Split
Dual Wield
Move+1



Twist Headband
Black Robe
Power Wrist

Mimic




Ring Wyrm
Female
Libra
57
74
Oracle
Punch Art
Dragon Spirit
Short Charge
Move-HP Up

Iron Fan

Flash Hat
White Robe
Diamond Armlet

Blind, Poison, Doubt Faith, Blind Rage, Dispel Magic
Spin Fist, Pummel, Purification, Chakra



Snowfats
Female
Cancer
75
48
Samurai
Black Magic
Abandon
Equip Polearm
Move+1

Obelisk

Platinum Helmet
White Robe
Germinas Boots

Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji
Fire 4, Bolt, Bolt 3, Bolt 4

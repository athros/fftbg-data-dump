Final Bets: red - 8 bets for 6,973G (32.4%, x2.09); brown - 17 bets for 14,558G (67.6%, x0.48)

red bets:
YaBoy125: 3,957G (56.7%, 4,997G)
killth3kid: 1,512G (21.7%, 15,124G)
Forkmore: 1,000G (14.3%, 44,158G)
Smokegiant: 104G (1.5%, 104G)
maakur_: 100G (1.4%, 77,051G)
Alanaire: 100G (1.4%, 22,228G)
CosmicTactician: 100G (1.4%, 32,070G)
Loyalgaming237: 100G (1.4%, 1,037G)

brown bets:
SkylerBunny: 3,500G (24.0%, 486,503G)
sinnyil2: 2,732G (18.8%, 5,464G)
UmaiJam: 1,800G (12.4%, 84,150G)
Chuckolator: 1,565G (10.8%, 89,237G)
DeathTaxesAndAnime: 1,304G (9.0%, 1,304G)
BirbBrainsBot: 1,000G (6.9%, 6,555G)
Cryptopsy70: 616G (4.2%, 14,524G)
evontno: 520G (3.6%, 520G)
getthemoneyz: 400G (2.7%, 1,115,248G)
Snowfats: 250G (1.7%, 5,327G)
silentkaster: 200G (1.4%, 8,024G)
alekzanndrr: 200G (1.4%, 200G)
Lydian_C: 121G (0.8%, 174,128G)
fluffskull: 100G (0.7%, 749G)
RasenRendan: 100G (0.7%, 126G)
datadrivenbot: 100G (0.7%, 48,685G)
AetosofValla: 50G (0.3%, 1,826G)

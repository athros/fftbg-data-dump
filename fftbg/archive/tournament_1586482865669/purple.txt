Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sairentozon7
Male
Taurus
51
69
Samurai
Punch Art
Arrow Guard
Long Status
Fly

Bizen Boat

Mythril Helmet
Black Robe
Genji Gauntlet

Asura, Koutetsu, Murasame, Heaven's Cloud
Secret Fist, Revive



Nizaha
Female
Scorpio
58
80
Ninja
Basic Skill
Damage Split
Beastmaster
Move+3

Morning Star
Assassin Dagger
Holy Miter
Earth Clothes
Rubber Shoes

Bomb, Stick
Accumulate, Heal, Wish



Escorian
Male
Pisces
62
55
Monk
Throw
Brave Up
Equip Armor
Ignore Terrain



Iron Helmet
Silk Robe
Wizard Mantle

Spin Fist, Secret Fist, Purification, Chakra, Seal Evil
Shuriken, Dictionary



Roofiepops
Male
Gemini
41
67
Lancer
Yin Yang Magic
Mana Shield
Martial Arts
Move-MP Up


Gold Shield
Platinum Helmet
Carabini Mail
Genji Gauntlet

Level Jump2, Vertical Jump7
Pray Faith, Silence Song

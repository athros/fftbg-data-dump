Final Bets: red - 15 bets for 13,390G (38.5%, x1.59); blue - 30 bets for 21,355G (61.5%, x0.63)

red bets:
Jeeboheebo: 3,927G (29.3%, 3,927G)
Lanshaft: 3,088G (23.1%, 3,088G)
DeathTaxesAndAnime: 1,663G (12.4%, 3,262G)
Zeroroute: 1,006G (7.5%, 1,006G)
OmnibotGamma: 826G (6.2%, 826G)
SkylerBunny: 620G (4.6%, 620G)
AllInBot: 612G (4.6%, 612G)
Theseawolf1: 500G (3.7%, 6,946G)
HaateXIII: 324G (2.4%, 324G)
DrAntiSocial: 200G (1.5%, 3,585G)
V3rdeni: 200G (1.5%, 5,307G)
BlinkFS: 124G (0.9%, 124G)
Tougou: 100G (0.7%, 6,984G)
Aneyus: 100G (0.7%, 10,681G)
RunicMagus: 100G (0.7%, 2,484G)

blue bets:
Nizaha: 3,366G (15.8%, 6,733G)
coledot_: 2,175G (10.2%, 7,253G)
NoNotBees: 2,102G (9.8%, 2,102G)
leakimiko: 1,544G (7.2%, 30,893G)
Lord_Burrah: 1,200G (5.6%, 95,965G)
WireLord: 1,000G (4.7%, 30,800G)
ungabunga_bot: 1,000G (4.7%, 84,585G)
BirbBrainsBot: 1,000G (4.7%, 21,692G)
getthemoneyz: 1,000G (4.7%, 460,422G)
Tyerenex: 1,000G (4.7%, 8,088G)
Laserman1000: 781G (3.7%, 14,981G)
Faplo: 728G (3.4%, 728G)
TheMurkGnome: 500G (2.3%, 6,224G)
reinoe: 500G (2.3%, 17,409G)
sneakyness: 400G (1.9%, 400G)
luminarii: 383G (1.8%, 13,517G)
datadrivenbot: 381G (1.8%, 24,401G)
Meta_Five: 300G (1.4%, 6,450G)
Spartan_Paladin: 300G (1.4%, 23,509G)
Evewho: 296G (1.4%, 296G)
proper__noun__: 261G (1.2%, 261G)
moonliquor: 211G (1.0%, 423G)
ANFz: 200G (0.9%, 1,879G)
Aka_Gilly: 200G (0.9%, 9,766G)
Lythe_Caraker: 100G (0.5%, 48,599G)
BuffaloCrunch: 100G (0.5%, 13,701G)
wollise89: 100G (0.5%, 773G)
ko2q: 100G (0.5%, 5,066G)
Ross_from_Cali: 77G (0.4%, 926G)
b0shii: 50G (0.2%, 1,296G)

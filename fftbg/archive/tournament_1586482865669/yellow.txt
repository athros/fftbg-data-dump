Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Proper Noun
Male
Taurus
44
75
Thief
Time Magic
Brave Up
Attack UP
Ignore Terrain

Mythril Knife

Green Beret
Power Sleeve
Bracer

Gil Taking, Steal Weapon
Haste, Stabilize Time



Mtueni
Female
Taurus
69
64
Summoner
Steal
HP Restore
Short Charge
Fly

Dragon Rod

Golden Hairpin
Robe of Lords
Small Mantle

Moogle, Ramuh, Golem, Salamander
Gil Taking, Steal Shield, Arm Aim



TheKillerNacho
Female
Scorpio
41
59
Chemist
Elemental
Distribute
Equip Shield
Ignore Height

Stone Gun
Aegis Shield
Red Hood
Brigandine
Cursed Ring

Antidote, Soft, Phoenix Down
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard



Evewho
Female
Aries
68
52
Ninja
Yin Yang Magic
Damage Split
Equip Knife
Jump+3

Cultist Dagger
Kunai
Flash Hat
Judo Outfit
Small Mantle

Shuriken, Knife, Hammer
Blind, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze

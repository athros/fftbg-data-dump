Final Bets: white - 14 bets for 28,962G (85.0%, x0.18); black - 12 bets for 5,110G (15.0%, x5.67)

white bets:
R_Raynos: 17,743G (61.3%, 17,743G)
reinoe: 6,028G (20.8%, 6,028G)
lijarkh: 1,000G (3.5%, 19,132G)
Lionhermit: 1,000G (3.5%, 14,909G)
itsdigitalbro: 750G (2.6%, 5,148G)
DeathTaxesAndAnime: 748G (2.6%, 748G)
amiture: 500G (1.7%, 5,340G)
Eyepoor_: 300G (1.0%, 5,493G)
midori_ribbon: 276G (1.0%, 276G)
electric_glass: 200G (0.7%, 2,498G)
ungabunga_bot: 117G (0.4%, 359,405G)
datadrivenbot: 100G (0.3%, 12,456G)
cougboi: 100G (0.3%, 16,024G)
Arkreaver: 100G (0.3%, 12,622G)

black bets:
BirbBrainsBot: 1,000G (19.6%, 62,259G)
ko2q: 733G (14.3%, 733G)
sinnyil2: 600G (11.7%, 2,830G)
superdevon1: 500G (9.8%, 4,723G)
RyuTsuno: 500G (9.8%, 3,124G)
z32o: 500G (9.8%, 2,244G)
Chuckolator: 329G (6.4%, 5,269G)
getthemoneyz: 262G (5.1%, 629,482G)
Estan_AD: 250G (4.9%, 1,114G)
twelfthrootoftwo: 200G (3.9%, 2,500G)
Practice_Pad: 136G (2.7%, 136G)
TimeJannies: 100G (2.0%, 2,315G)

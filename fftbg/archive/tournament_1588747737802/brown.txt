Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheMM42
Male
Gemini
56
70
Thief
Talk Skill
Speed Save
Martial Arts
Swim

Ninja Edge

Black Hood
Wizard Outfit
Bracer

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Praise, Threaten, Refute, Rehabilitate



DeathTaxesAndAnime
Female
Libra
72
54
Samurai
White Magic
Caution
Equip Axe
Teleport

Slasher

Cross Helmet
Diamond Armor
Germinas Boots

Murasame, Muramasa
Cure, Regen, Protect, Shell, Esuna



Evontno
Male
Gemini
49
53
Wizard
Throw
Counter Tackle
Secret Hunt
Jump+3

Ice Rod

Ribbon
Robe of Lords
Rubber Shoes

Fire, Fire 2, Fire 3, Bolt, Bolt 4, Ice 2, Death
Sword, Staff



ApplesauceBoss
Male
Pisces
80
48
Monk
Jump
MA Save
Magic Attack UP
Jump+3



Black Hood
Judo Outfit
Bracer

Secret Fist, Purification, Chakra, Seal Evil
Level Jump4, Vertical Jump8

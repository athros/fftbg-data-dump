Player: !Red
Team: Red Team
Palettes: Red/Brown



Aldrammech
Male
Virgo
80
77
Calculator
White Magic
Counter Magic
Martial Arts
Ignore Height

Papyrus Codex

Golden Hairpin
Silk Robe
Spike Shoes

CT, Prime Number, 4
Cure, Cure 2, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna



Nedryerson01
Female
Aries
47
80
Summoner
Battle Skill
Damage Split
Beastmaster
Ignore Terrain

Rainbow Staff

Leather Hat
Chameleon Robe
Defense Ring

Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Silf, Fairy, Lich, Cyclops
Head Break, Weapon Break, Speed Break, Night Sword



DHaveWord
Monster
Gemini
42
71
Tiamat










Fenaen
Male
Pisces
79
51
Bard
Yin Yang Magic
Counter Tackle
Maintenance
Jump+2

Windslash Bow

Headgear
Black Costume
108 Gems

Angel Song, Life Song, Battle Song, Magic Song, Last Song
Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Petrify

Player: !Black
Team: Black Team
Palettes: Black/Red



Mexskacin
Male
Pisces
80
58
Calculator
Yin Yang Magic
Damage Split
Equip Knife
Levitate

Cultist Dagger

Twist Headband
Wizard Robe
Diamond Armlet

CT, Prime Number, 4
Life Drain, Pray Faith, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep



ZephyrTempest
Female
Aquarius
59
75
Dancer
Talk Skill
Brave Up
Equip Gun
Ignore Terrain

Glacier Gun

Twist Headband
Chameleon Robe
Magic Gauntlet

Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Dragon Pit
Preach, Solution, Rehabilitate



JonnyCue
Female
Virgo
46
68
Geomancer
Yin Yang Magic
MA Save
Defend
Move-MP Up

Muramasa
Ice Shield
Red Hood
Wizard Robe
Defense Armlet

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Blind, Poison, Zombie, Petrify



Commandlistfgc
Female
Taurus
57
65
Squire
Time Magic
HP Restore
Martial Arts
Move-HP Up


Buckler
Red Hood
Carabini Mail
Battle Boots

Accumulate, Throw Stone, Heal, Fury, Wish, Ultima
Haste, Stop, Immobilize, Demi 2, Stabilize Time

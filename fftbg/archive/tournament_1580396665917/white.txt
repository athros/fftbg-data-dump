Player: !White
Team: White Team
Palettes: White/Blue



Worker11
Female
Leo
40
54
Calculator
Time Magic
Distribute
Defend
Jump+3

Papyrus Codex

Holy Miter
Black Robe
Defense Armlet

CT, Prime Number, 5, 4, 3
Haste, Slow 2, Stabilize Time



E Ballard
Male
Aries
44
57
Time Mage
Throw
Faith Up
Long Status
Fly

Rainbow Staff

Thief Hat
Power Sleeve
Elf Mantle

Haste 2, Stop, Demi, Demi 2, Stabilize Time
Shuriken, Bomb



HughJeffner
Male
Scorpio
52
58
Chemist
Sing
Auto Potion
Maintenance
Ignore Terrain

Hydra Bag

Feather Hat
Power Sleeve
Leather Mantle

Potion, X-Potion, Remedy
Life Song, Cheer Song, Nameless Song



Wizard 01
Male
Scorpio
42
44
Priest
Battle Skill
Brave Up
Magic Defense UP
Move+2

Healing Staff

Headgear
Chameleon Robe
Leather Mantle

Cure 2, Raise, Raise 2, Regen, Shell, Esuna, Holy
Head Break, Magic Break, Speed Break, Power Break

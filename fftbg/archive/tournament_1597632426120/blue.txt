Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Douchetron
Female
Capricorn
74
73
Ninja
Basic Skill
Counter
Equip Bow
Move-HP Up

Windslash Bow

Leather Hat
Black Costume
Defense Armlet

Shuriken, Hammer
Dash, Heal, Cheer Up, Wish



Reinoe
Male
Cancer
60
43
Geomancer
Charge
Meatbone Slash
Equip Gun
Swim

Papyrus Codex
Gold Shield
Golden Hairpin
Linen Robe
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Charge+5, Charge+7



Drusiform
Monster
Pisces
45
78
Red Chocobo










Eweaselbeth
Female
Libra
49
62
Thief
Time Magic
Catch
Equip Armor
Jump+1

Cultist Dagger

Circlet
White Robe
Defense Armlet

Steal Heart, Steal Helmet, Steal Status, Arm Aim
Haste, Demi, Demi 2, Stabilize Time

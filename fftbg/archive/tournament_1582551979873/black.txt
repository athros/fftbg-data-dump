Player: !Black
Team: Black Team
Palettes: Black/Red



Trowaba
Female
Pisces
74
50
Calculator
Byblos
Hamedo
Defense UP
Ignore Terrain

Ice Rod

Cross Helmet
Clothes
Feather Boots

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Mirapoix
Male
Taurus
50
78
Monk
Black Magic
Critical Quick
Secret Hunt
Teleport



Golden Hairpin
Earth Clothes
Leather Mantle

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Ice 2, Ice 4



Upvla
Male
Leo
72
51
Lancer
Talk Skill
Distribute
Dual Wield
Fly

Partisan
Spear
Mythril Helmet
Leather Armor
Leather Mantle

Level Jump8, Vertical Jump2
Praise, Threaten, Preach, Solution, Mimic Daravon, Refute, Rehabilitate



BoneMiser
Male
Sagittarius
56
66
Wizard
Steal
Speed Save
Maintenance
Ignore Terrain

Wizard Rod

Red Hood
Linen Robe
108 Gems

Fire 2, Bolt, Bolt 2, Ice 2, Ice 3
Steal Armor, Steal Shield, Steal Status

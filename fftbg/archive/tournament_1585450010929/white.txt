Player: !White
Team: White Team
Palettes: White/Blue



ZZ Yoshi
Female
Taurus
64
62
Wizard
Draw Out
Caution
Equip Shield
Move+3

Thunder Rod
Buckler
Black Hood
Mythril Vest
Dracula Mantle

Bolt, Bolt 2, Bolt 4, Ice 2
Kiyomori, Muramasa



Jordache7K
Male
Capricorn
46
70
Bard
Summon Magic
Damage Split
Martial Arts
Fly



Feather Hat
Crystal Mail
Small Mantle

Angel Song, Cheer Song, Battle Song, Last Song, Space Storage, Sky Demon, Hydra Pit
Moogle, Shiva, Ifrit, Bahamut, Silf



Mechapope1
Monster
Gemini
70
45
Red Chocobo










Mayormcfunbags
Female
Capricorn
69
66
Chemist
Charge
Brave Up
Equip Bow
Ignore Terrain

Bow Gun

Flash Hat
Earth Clothes
N-Kai Armlet

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down
Charge+4, Charge+5

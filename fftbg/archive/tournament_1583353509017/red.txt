Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Gemini
62
63
Lancer
Elemental
Distribute
Halve MP
Jump+3

Mythril Spear
Crystal Shield
Genji Helmet
Chameleon Robe
Power Wrist

Level Jump8, Vertical Jump5
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



LuckyLuckLuc2
Female
Aquarius
47
44
Wizard
Yin Yang Magic
Regenerator
Martial Arts
Retreat



Holy Miter
Light Robe
Rubber Shoes

Fire 2, Fire 3, Fire 4, Bolt 3, Empower
Life Drain, Pray Faith, Zombie, Silence Song, Sleep, Petrify, Dark Holy



IkariAllen
Male
Pisces
75
49
Priest
Math Skill
Parry
Martial Arts
Swim



Black Hood
Light Robe
Leather Mantle

Cure, Cure 4, Raise, Reraise, Regen, Wall, Esuna
Height, 4, 3



Diablos24
Female
Gemini
79
75
Knight
White Magic
Caution
Martial Arts
Move+2


Diamond Shield
Diamond Helmet
Bronze Armor
Wizard Mantle

Head Break, Weapon Break, Mind Break, Stasis Sword
Raise 2, Reraise, Protect, Shell, Esuna

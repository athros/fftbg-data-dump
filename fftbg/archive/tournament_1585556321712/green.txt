Player: !Green
Team: Green Team
Palettes: Green/White



HEROScioN
Female
Capricorn
74
68
Monk
Draw Out
Catch
Concentrate
Move+1



Twist Headband
Earth Clothes
Battle Boots

Secret Fist, Purification, Revive
Murasame, Heaven's Cloud



SpaZmodeus
Female
Leo
53
51
Geomancer
Dance
Sunken State
Maintenance
Fly

Muramasa
Platinum Shield
Headgear
Light Robe
Cursed Ring

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Disillusion, Last Dance, Nether Demon



Pah J
Female
Pisces
54
75
Samurai
Throw
PA Save
Maintenance
Move+2

Kiyomori

Crystal Helmet
Platinum Armor
Reflect Ring

Murasame, Kiyomori, Kikuichimoji
Shuriken, Knife, Staff, Axe



Tithonus
Female
Scorpio
45
59
Squire
Draw Out
MP Restore
Maintenance
Move+2

Ice Brand
Ice Shield
Gold Helmet
Wizard Outfit
Diamond Armlet

Accumulate, Heal, Fury, Wish
Asura, Murasame, Kiyomori, Muramasa

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



IMMASHAARK
Monster
Virgo
71
62
Skeleton










Zetchryn
Female
Taurus
40
46
Mediator
Battle Skill
Hamedo
Short Status
Jump+1

Bestiary

Flash Hat
Black Robe
Reflect Ring

Invitation, Praise, Preach, Solution, Refute
Head Break, Armor Break, Shield Break, Mind Break



Basmal
Female
Pisces
46
41
Archer
Draw Out
Parry
Dual Wield
Jump+2

Mythril Bow

Twist Headband
Power Sleeve
Feather Mantle

Charge+1, Charge+4
Asura, Murasame



Reinoe
Male
Sagittarius
69
44
Geomancer
Basic Skill
Meatbone Slash
Halve MP
Swim

Hydra Bag
Crystal Shield
Flash Hat
Earth Clothes
Battle Boots

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Lava Ball
Heal, Yell, Fury

Player: !White
Team: White Team
Palettes: White/Blue



Hyvi
Male
Scorpio
42
67
Bard
Steal
Faith Up
Dual Wield
Waterbreathing

Fairy Harp
Fairy Harp
Holy Miter
Chain Mail
Battle Boots

Angel Song, Battle Song, Magic Song, Last Song, Sky Demon
Steal Helmet, Steal Armor, Arm Aim



Clar3d
Monster
Sagittarius
77
40
Plague










PlatinumPlume
Male
Aquarius
58
58
Thief
White Magic
Caution
Magic Attack UP
Ignore Terrain

Rune Blade

Black Hood
Wizard Outfit
Small Mantle

Steal Heart, Steal Armor, Arm Aim
Raise 2, Shell, Shell 2, Esuna



Zagorsek
Male
Leo
42
45
Chemist
Jump
Sunken State
Short Charge
Move-MP Up

Cute Bag

Red Hood
Brigandine
N-Kai Armlet

Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Phoenix Down
Level Jump8, Vertical Jump5

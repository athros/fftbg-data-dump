Player: !Red
Team: Red Team
Palettes: Red/Brown



DudeMonkey77
Male
Cancer
72
68
Chemist
Draw Out
Counter
Equip Knife
Waterbreathing

Short Edge

Feather Hat
Adaman Vest
108 Gems

Potion, X-Potion, Maiden's Kiss, Remedy
Bizen Boat



QuestMoreLand
Male
Gemini
66
54
Calculator
Robosnake Skill
Meatbone Slash
Equip Knife
Move+3

Blind Knife
Gold Shield
Platinum Helmet
Black Robe
Bracer

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Mesmaster
Female
Aquarius
62
68
Samurai
Dance
Sunken State
Long Status
Waterwalking

Bizen Boat

Leather Helmet
White Robe
Genji Gauntlet

Heaven's Cloud, Muramasa
Disillusion, Obsidian Blade, Dragon Pit



Flowinprose
Female
Leo
63
46
Geomancer
Basic Skill
Absorb Used MP
Short Charge
Move+1

Battle Axe
Kaiser Plate
Flash Hat
Black Robe
Setiemson

Pitfall, Water Ball, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Accumulate, Heal, Tickle, Yell

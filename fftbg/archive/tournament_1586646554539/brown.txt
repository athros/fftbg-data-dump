Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Edgehead62888
Female
Aries
59
59
Thief
Talk Skill
Counter Magic
Defend
Move+1

Zorlin Shape

Triangle Hat
Earth Clothes
Elf Mantle

Steal Heart, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Threaten, Preach, Solution, Insult, Mimic Daravon, Refute



Sinnyil2
Female
Capricorn
42
51
Dancer
Draw Out
Counter Flood
Short Status
Move-HP Up

Ryozan Silk

Black Hood
Wizard Outfit
Defense Ring

Witch Hunt, Polka Polka, Nether Demon, Dragon Pit
Bizen Boat, Kiyomori, Muramasa



Omegasuspekt
Male
Libra
76
44
Oracle
Time Magic
Catch
Concentrate
Fly

Battle Bamboo

Headgear
Chain Vest
Red Shoes

Spell Absorb, Pray Faith, Dispel Magic, Paralyze, Sleep, Dark Holy
Slow 2, Demi, Demi 2



DeathTaxesAndAnime
Female
Sagittarius
76
64
Samurai
Charge
Caution
Equip Shield
Waterbreathing

Asura Knife
Hero Shield
Iron Helmet
Diamond Armor
Vanish Mantle

Asura, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Charge+4, Charge+20

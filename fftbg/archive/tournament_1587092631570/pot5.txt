Final Bets: red - 16 bets for 8,727G (50.2%, x0.99); green - 15 bets for 8,654G (49.8%, x1.01)

red bets:
Nickyfive: 1,988G (22.8%, 7,955G)
Nizaha: 1,200G (13.8%, 2,401G)
NovaKnight21: 1,052G (12.1%, 1,052G)
Zbgs: 1,000G (11.5%, 49,260G)
BirbBrainsBot: 789G (9.0%, 186,298G)
DaMadPanda182: 581G (6.7%, 2,581G)
Slowbrofist: 356G (4.1%, 356G)
SpookyCookieMonster: 346G (4.0%, 346G)
proper__noun__: 300G (3.4%, 2,204G)
ZephyrTempest: 246G (2.8%, 75,240G)
itzover9001: 216G (2.5%, 216G)
datadrivenbot: 203G (2.3%, 236G)
carsenhk: 150G (1.7%, 489G)
ApplesauceBoss: 100G (1.1%, 3,202G)
electric_glass: 100G (1.1%, 2,110G)
Tithonus: 100G (1.1%, 4,564G)

green bets:
Shalloween: 1,500G (17.3%, 31,887G)
alrightbye: 1,250G (14.4%, 8,824G)
sinnyil2: 1,200G (13.9%, 51,523G)
fenaen: 1,001G (11.6%, 12,965G)
Virilikus: 1,000G (11.6%, 53,790G)
HASTERIOUS: 560G (6.5%, 560G)
getthemoneyz: 530G (6.1%, 499,923G)
ungabunga_bot: 519G (6.0%, 149,781G)
V3rdeni: 300G (3.5%, 1,995G)
GrandmasterFrankerZ: 212G (2.4%, 212G)
ko2q: 200G (2.3%, 8,569G)
ThePuss: 132G (1.5%, 132G)
Jerboj: 100G (1.2%, 40,457G)
Conome: 100G (1.2%, 5,326G)
GingerDynomite: 50G (0.6%, 1,090G)

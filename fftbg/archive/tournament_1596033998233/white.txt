Player: !White
Team: White Team
Palettes: White/Blue



TasisSai
Male
Gemini
72
46
Bard
Elemental
Counter Tackle
Equip Shield
Jump+2

Ramia Harp
Crystal Shield
Cachusha
Diamond Armor
Magic Ring

Cheer Song, Battle Song, Diamond Blade, Hydra Pit
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Grininda
Female
Sagittarius
57
68
Summoner
Dance
MA Save
Maintenance
Move+3

Wizard Rod

Thief Hat
Brigandine
Feather Mantle

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Leviathan, Fairy, Lich
Witch Hunt, Polka Polka, Disillusion



SephDarkheart
Male
Virgo
39
75
Samurai
Basic Skill
Faith Save
Equip Knife
Teleport

Ice Rod

Platinum Helmet
Chain Mail
Magic Ring

Koutetsu, Heaven's Cloud
Dash, Tickle, Yell, Fury, Wish



UmaiJam
Male
Gemini
67
52
Monk
Black Magic
Brave Save
Dual Wield
Swim



Flash Hat
Mythril Vest
Reflect Ring

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Fire 3, Ice 2, Ice 3, Frog

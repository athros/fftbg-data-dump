Player: !Red
Team: Red Team
Palettes: Red/Brown



Thejoden
Female
Virgo
45
46
Mediator
Jump
Caution
Equip Bow
Lava Walking

Windslash Bow

Headgear
Chain Vest
Sprint Shoes

Persuade, Preach, Rehabilitate
Level Jump2, Vertical Jump2



UmaiJam
Male
Aries
52
80
Lancer
Item
Auto Potion
Concentrate
Ignore Height

Holy Lance
Bronze Shield
Leather Helmet
Bronze Armor
Leather Mantle

Level Jump8, Vertical Jump2
Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down



Beastmages
Monster
Leo
72
46
Squidraken










Bad1dea
Female
Pisces
64
59
Geomancer
Item
Meatbone Slash
Secret Hunt
Teleport

Ancient Sword
Hero Shield
Feather Hat
Black Costume
Leather Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, Hi-Ether, Remedy, Phoenix Down

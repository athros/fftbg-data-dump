Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Go2sleepTV
Male
Cancer
45
63
Lancer
White Magic
Mana Shield
Secret Hunt
Levitate

Mythril Spear
Ice Shield
Mythril Helmet
Crystal Mail
Leather Mantle

Level Jump5, Vertical Jump8
Cure 2, Raise, Wall, Esuna



Reverie3
Male
Taurus
52
48
Calculator
Black Magic
Critical Quick
Equip Polearm
Move+1

Cashmere

Green Beret
Robe of Lords
Magic Gauntlet

CT, Prime Number, 5, 3
Fire 3, Bolt 3



Rubenflonne
Female
Aries
64
80
Mediator
Summon Magic
Regenerator
Sicken
Jump+2

Mythril Gun

Holy Miter
Wizard Outfit
Salty Rage

Praise, Threaten, Solution, Insult, Negotiate, Mimic Daravon, Refute
Ramuh, Golem, Bahamut, Leviathan, Lich, Cyclops



Clockthief
Male
Gemini
52
44
Calculator
Undeath Skill
PA Save
Equip Gun
Waterwalking

Stone Gun
Gold Shield
Black Hood
Earth Clothes
Cursed Ring

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch

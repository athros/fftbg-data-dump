Player: !White
Team: White Team
Palettes: White/Blue



Arcblazer23
Female
Virgo
74
62
Dancer
Punch Art
Counter
Concentrate
Teleport 2

Panther Bag

Holy Miter
Wizard Robe
Defense Ring

Polka Polka, Disillusion, Void Storage, Dragon Pit
Purification, Chakra, Revive



Gorgewall
Female
Scorpio
78
66
Monk
Talk Skill
Hamedo
Equip Knife
Move-HP Up

Air Knife

Barette
Adaman Vest
Vanish Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Chakra, Revive
Invitation, Persuade, Praise, Preach, Insult, Mimic Daravon



KasugaiRoastedPeas
Female
Gemini
80
40
Ninja
Punch Art
Parry
Equip Armor
Move+1

Ninja Edge
Hidden Knife
Crystal Helmet
Mythril Armor
Red Shoes

Shuriken, Bomb, Knife
Spin Fist, Secret Fist, Purification, Revive



ValtonZenola
Male
Aries
62
65
Samurai
Summon Magic
Brave Save
Maintenance
Move-HP Up

Murasame

Diamond Helmet
Platinum Armor
Feather Boots

Muramasa, Kikuichimoji
Moogle, Shiva, Carbunkle, Fairy

Player: !Black
Team: Black Team
Palettes: Black/Red



Kyune
Female
Serpentarius
59
69
Knight
Item
Counter
Throw Item
Move-HP Up

Ancient Sword
Ice Shield
Crystal Helmet
Reflect Mail
Elf Mantle

Armor Break, Weapon Break, Magic Break
Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



CorpusCav
Female
Gemini
68
44
Dancer
Draw Out
Faith Save
Equip Sword
Teleport

Chirijiraden

Leather Hat
Chameleon Robe
Elf Mantle

Wiznaibus, Disillusion, Void Storage, Nether Demon
Bizen Boat, Muramasa



KasugaiRoastedPeas
Monster
Libra
73
73
Tiamat










Gr8keeper
Female
Virgo
77
59
Ninja
Summon Magic
MP Restore
Equip Gun
Swim

Stone Gun
Blaze Gun
Feather Hat
Black Costume
Elf Mantle

Bomb, Knife, Staff
Moogle, Shiva, Carbunkle, Odin

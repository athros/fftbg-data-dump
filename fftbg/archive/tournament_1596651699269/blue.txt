Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lube Squid
Female
Aquarius
48
72
Monk
Draw Out
Counter Flood
Equip Shield
Move+3


Ice Shield
Holy Miter
Black Costume
Diamond Armlet

Secret Fist, Purification, Chakra, Revive
Murasame, Kiyomori



Gunz232323
Male
Leo
58
59
Chemist
Black Magic
Critical Quick
Sicken
Move+2

Blaze Gun

Golden Hairpin
Chain Vest
Elf Mantle

Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Fire, Fire 3, Bolt, Bolt 2, Ice 4, Frog, Death



Kellios11
Male
Capricorn
76
49
Monk
Elemental
Earplug
Maintenance
Waterwalking



Golden Hairpin
Judo Outfit
Magic Gauntlet

Spin Fist, Pummel, Wave Fist, Purification, Revive
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Lava Ball



LDSkinny
Female
Capricorn
46
69
Oracle
Item
Counter
Throw Item
Move+3

Cypress Rod

Headgear
Chameleon Robe
Elf Mantle

Blind, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Potion, Ether, Echo Grass, Soft, Remedy, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ArchKnightX
Female
Gemini
74
65
Summoner
Item
Speed Save
Throw Item
Levitate

Rod

Green Beret
Light Robe
Red Shoes

Shiva, Titan, Bahamut, Odin
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Call Me Sean
Male
Virgo
50
55
Priest
Battle Skill
Brave Save
Attack UP
Jump+2

Oak Staff

Flash Hat
Earth Clothes
Feather Boots

Cure 2, Raise, Esuna, Holy
Head Break, Shield Break, Speed Break, Mind Break, Surging Sword



LordTomS
Female
Leo
63
80
Knight
Jump
Catch
Martial Arts
Jump+3


Mythril Shield
Bronze Helmet
Platinum Armor
Reflect Ring

Power Break, Mind Break
Level Jump5, Vertical Jump3



Galkife
Male
Libra
63
79
Oracle
Jump
Earplug
Equip Bow
Waterwalking

Bow Gun

Triangle Hat
Chameleon Robe
Angel Ring

Poison, Pray Faith, Zombie, Foxbird, Paralyze
Level Jump4, Vertical Jump4

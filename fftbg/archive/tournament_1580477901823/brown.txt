Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dreygyron
Female
Capricorn
52
42
Time Mage
Item
Critical Quick
Equip Armor
Lava Walking

Battle Folio

Headgear
Silk Robe
Red Shoes

Haste, Slow, Quick, Demi, Stabilize Time
Potion, X-Potion, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down



Tristar49
Male
Libra
74
43
Time Mage
Jump
Parry
Maintenance
Waterbreathing

Bestiary

Thief Hat
Wizard Robe
Jade Armlet

Slow 2, Stop, Immobilize, Float, Demi 2, Stabilize Time, Meteor
Level Jump3, Vertical Jump6



Shs
Male
Taurus
60
80
Chemist
Basic Skill
MA Save
Beastmaster
Waterwalking

Panther Bag

Golden Hairpin
Wizard Outfit
N-Kai Armlet

Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Remedy
Accumulate, Throw Stone, Heal



Artea
Female
Taurus
43
54
Time Mage
Black Magic
Absorb Used MP
Halve MP
Levitate

Oak Staff

Thief Hat
Clothes
Power Wrist

Haste, Haste 2, Immobilize, Reflect, Quick, Demi 2, Stabilize Time
Fire, Fire 4, Bolt 2, Ice 3, Ice 4

Final Bets: red - 22 bets for 11,759G (34.6%, x1.89); blue - 28 bets for 22,224G (65.4%, x0.53)

red bets:
Gelwain: 2,396G (20.4%, 23,964G)
CCRUNNER149UWP: 1,111G (9.4%, 16,366G)
BirbBrainsBot: 1,000G (8.5%, 2,856G)
bad1dea: 1,000G (8.5%, 233,712G)
lunaticruin: 847G (7.2%, 847G)
UmaiJam: 842G (7.2%, 8,424G)
Axelrod: 731G (6.2%, 14,637G)
HaplessOne: 500G (4.3%, 45,663G)
onetrickwolf: 400G (3.4%, 8,266G)
pandasforsale: 333G (2.8%, 7,532G)
CapnChaos12: 300G (2.6%, 2,605G)
ZephyrTempest: 297G (2.5%, 3,487G)
Aurilliux: 280G (2.4%, 280G)
orlandu_lv: 250G (2.1%, 1,791G)
JIDkomu: 232G (2.0%, 232G)
RurouniGeo: 220G (1.9%, 220G)
Aeolus_000: 200G (1.7%, 228G)
ensiron: 200G (1.7%, 8,533G)
robespyah: 200G (1.7%, 1,081G)
CorpusCav: 200G (1.7%, 5,986G)
getthemoneyz: 120G (1.0%, 397,057G)
Nizaha: 100G (0.9%, 8,285G)

blue bets:
OmnibotGamma: 8,981G (40.4%, 17,962G)
maakur_: 1,299G (5.8%, 2,598G)
BadBlanket: 1,128G (5.1%, 1,128G)
sinnyil2: 1,028G (4.6%, 1,028G)
kai_shee: 1,000G (4.5%, 35,211G)
Theseawolf1: 1,000G (4.5%, 4,802G)
Drakel: 1,000G (4.5%, 4,526G)
Vampire_Killer: 1,000G (4.5%, 16,440G)
leakimiko: 879G (4.0%, 29,303G)
DeathTaxesAndAnime: 804G (3.6%, 804G)
Bubbafun: 699G (3.1%, 699G)
lijarkh: 500G (2.2%, 4,172G)
fenaen: 400G (1.8%, 4,640G)
TheGuesty: 350G (1.6%, 6,233G)
dodgeroll: 331G (1.5%, 331G)
DHaveWord: 316G (1.4%, 316G)
MrFlabyo: 300G (1.3%, 19,927G)
inzo24: 297G (1.3%, 297G)
JophesMannhoh: 200G (0.9%, 4,971G)
Jordache7K: 111G (0.5%, 11,495G)
shiokenstar: 100G (0.4%, 1,474G)
fr3akaz01d19: 100G (0.4%, 357G)
datadrivenbot: 100G (0.4%, 16,155G)
Oshimi12: 100G (0.4%, 1,183G)
volgrathemoose: 100G (0.4%, 1,081G)
Strifu: 50G (0.2%, 434G)
denamda: 50G (0.2%, 18,443G)
daveb_: 1G (0.0%, 1,549G)

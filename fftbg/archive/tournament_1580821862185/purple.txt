Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



PrisstineOnscreen
Female
Capricorn
55
50
Knight
Talk Skill
Auto Potion
Attack UP
Move-MP Up

Blood Sword
Escutcheon
Circlet
Light Robe
Red Shoes

Head Break, Shield Break, Weapon Break, Magic Break, Speed Break
Persuade, Death Sentence, Negotiate, Mimic Daravon



Bigshowens
Male
Cancer
72
58
Oracle
Talk Skill
Meatbone Slash
Maintenance
Teleport

Battle Folio

Red Hood
Light Robe
Feather Mantle

Poison, Spell Absorb, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep
Persuade, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Megaman2202
Female
Libra
70
45
Time Mage
Black Magic
Meatbone Slash
Equip Sword
Teleport

Ragnarok

Black Hood
Wizard Robe
Sprint Shoes

Haste, Slow, Float, Quick, Stabilize Time
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 3



Weegz
Female
Libra
60
52
Geomancer
Steal
Counter Tackle
Sicken
Ignore Terrain

Coral Sword
Mythril Shield
Black Hood
Clothes
Feather Boots

Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Gil Taking, Steal Heart, Steal Armor, Steal Accessory

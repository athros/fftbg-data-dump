Player: !Black
Team: Black Team
Palettes: Black/Red



Shellback27
Female
Leo
81
71
Summoner
Talk Skill
Counter Tackle
Concentrate
Jump+1

White Staff

Flash Hat
Silk Robe
Spike Shoes

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Salamander, Silf
Persuade, Threaten, Solution, Death Sentence, Insult, Refute, Rehabilitate



TheBlobZ
Male
Virgo
77
55
Archer
White Magic
MA Save
Doublehand
Move+3

Poison Bow

Thief Hat
Chain Vest
Small Mantle

Charge+1, Charge+20
Regen, Protect 2, Shell 2, Esuna



Dimmitree
Male
Leo
69
64
Summoner
Basic Skill
Catch
Attack UP
Retreat

Flame Rod

Flash Hat
Adaman Vest
Jade Armlet

Moogle, Golem, Carbunkle, Odin, Lich, Cyclops
Heal, Tickle, Cheer Up, Wish, Scream



Zeroroute
Male
Aries
57
57
Oracle
Black Magic
PA Save
Equip Shield
Move+1

Octagon Rod
Ice Shield
Triangle Hat
Adaman Vest
108 Gems

Blind, Poison, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Petrify
Fire, Fire 3, Bolt 3, Ice 2, Frog, Death

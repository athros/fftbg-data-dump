Player: !White
Team: White Team
Palettes: White/Blue



CassiePhoenix
Female
Taurus
76
74
Lancer
Summon Magic
Damage Split
Attack UP
Waterwalking

Hydra Bag
Aegis Shield
Gold Helmet
Wizard Robe
Setiemson

Level Jump5, Vertical Jump3
Moogle, Shiva, Ifrit, Carbunkle, Salamander



NovaKnight21
Female
Leo
77
39
Monk
Jump
Faith Save
Dual Wield
Jump+3



Flash Hat
Clothes
108 Gems

Pummel, Earth Slash, Purification
Level Jump8, Vertical Jump4



Go2sleepTV
Female
Leo
77
56
Lancer
Dance
Parry
Halve MP
Waterbreathing

Gungnir
Kaiser Plate
Barbuta
Black Robe
Cursed Ring

Level Jump5, Vertical Jump7
Polka Polka, Disillusion, Nether Demon



Ruleof5
Female
Cancer
61
48
Archer
Dance
Counter Flood
Dual Wield
Move+3

Night Killer
Gastrafitis
Triangle Hat
Black Costume
Dracula Mantle

Charge+2, Charge+5
Polka Polka, Void Storage, Dragon Pit

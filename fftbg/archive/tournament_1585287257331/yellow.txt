Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



L2 Sentinel
Male
Gemini
64
45
Lancer
Talk Skill
Earplug
Magic Attack UP
Swim

Javelin
Crystal Shield
Iron Helmet
Black Robe
108 Gems

Level Jump3, Vertical Jump8
Invitation, Persuade, Threaten, Solution, Mimic Daravon, Rehabilitate



R Raynos
Female
Aries
67
63
Oracle
Dance
Counter Magic
Long Status
Move+1

Battle Bamboo

Green Beret
Black Robe
Elf Mantle

Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Petrify
Polka Polka, Obsidian Blade, Void Storage, Nether Demon



YaBoy125
Female
Scorpio
42
52
Priest
Talk Skill
Meatbone Slash
Short Charge
Waterwalking

White Staff

Holy Miter
Secret Clothes
Wizard Mantle

Cure 2, Cure 3, Cure 4, Raise, Shell 2, Esuna, Holy
Persuade, Praise, Threaten, Preach, Solution, Insult, Rehabilitate



Dreadnxught
Female
Capricorn
46
71
Knight
Throw
Critical Quick
Doublehand
Teleport 2

Coral Sword

Crystal Helmet
Platinum Armor
Defense Ring

Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Night Sword
Shuriken, Bomb, Axe

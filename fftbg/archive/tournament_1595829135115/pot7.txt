Final Bets: yellow - 9 bets for 8,057G (53.0%, x0.89); white - 7 bets for 7,141G (47.0%, x1.13)

yellow bets:
NicoSavoy: 3,540G (43.9%, 354,086G)
mrpickins: 1,000G (12.4%, 5,969G)
getthemoneyz: 1,000G (12.4%, 1,402,632G)
randgridr: 1,000G (12.4%, 3,472G)
JLinkletter: 516G (6.4%, 516G)
Helwithe: 500G (6.2%, 1,753G)
gorgewall: 201G (2.5%, 6,472G)
datadrivenbot: 200G (2.5%, 46,738G)
poGpopE: 100G (1.2%, 13,454G)

white bets:
Smokegiant: 4,129G (57.8%, 4,129G)
AllInBot: 1,042G (14.6%, 1,042G)
BirbBrainsBot: 1,000G (14.0%, 121,654G)
CT_5_Holy: 500G (7.0%, 2,161G)
douchetron: 336G (4.7%, 336G)
Sans_from_Snowdin: 100G (1.4%, 23,576G)
dtrain332: 34G (0.5%, 3,465G)

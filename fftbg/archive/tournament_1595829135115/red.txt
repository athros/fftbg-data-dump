Player: !Red
Team: Red Team
Palettes: Red/Brown



Blorpy
Male
Libra
47
67
Priest
Jump
Catch
Attack UP
Jump+3

Rainbow Staff

Holy Miter
Silk Robe
Leather Mantle

Cure 2, Raise, Raise 2, Shell, Shell 2, Esuna, Holy
Level Jump5, Vertical Jump7



Lemonjohns
Male
Gemini
41
63
Ninja
Item
Catch
Martial Arts
Move+3



Cachusha
Rubber Costume
Rubber Shoes

Shuriken, Bomb, Knife, Staff
Potion, Antidote, Soft, Phoenix Down



TimeJannies
Male
Pisces
58
80
Knight
Black Magic
Meatbone Slash
Magic Attack UP
Fly

Save the Queen
Flame Shield
Leather Helmet
Platinum Armor
Elf Mantle

Head Break, Magic Break, Speed Break, Power Break
Fire 2, Fire 4, Ice, Ice 4, Empower, Frog, Death, Flare



Lythe Caraker
Male
Taurus
71
73
Knight
Summon Magic
Auto Potion
Magic Defense UP
Jump+2

Save the Queen
Diamond Shield
Leather Helmet
Crystal Mail
Dracula Mantle

Stasis Sword
Moogle, Ifrit, Bahamut, Leviathan, Lich

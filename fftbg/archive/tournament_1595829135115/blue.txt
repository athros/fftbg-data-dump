Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gorgewall
Female
Pisces
54
67
Mediator
White Magic
Sunken State
Beastmaster
Teleport

Papyrus Codex

Golden Hairpin
Power Sleeve
Bracer

Threaten, Death Sentence, Mimic Daravon, Refute
Cure, Raise, Protect, Protect 2, Shell 2, Wall, Esuna



SkylerBunny
Female
Aquarius
62
60
Chemist
Summon Magic
Critical Quick
Equip Gun
Fly

Bestiary

Black Hood
Judo Outfit
Power Wrist

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down
Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Fairy



Coralreeferz
Male
Scorpio
51
50
Bard
Draw Out
PA Save
Long Status
Jump+1

Ramia Harp

Flash Hat
Platinum Armor
N-Kai Armlet

Angel Song, Battle Song, Diamond Blade, Space Storage, Sky Demon
Asura, Kikuichimoji



Kronikle
Male
Aries
43
69
Archer
Yin Yang Magic
MP Restore
Doublehand
Retreat

Ultimus Bow

Holy Miter
Mythril Vest
Dracula Mantle

Charge+1, Charge+2, Charge+5
Poison, Life Drain, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Sleep

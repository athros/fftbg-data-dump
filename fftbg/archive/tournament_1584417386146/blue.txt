Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ZephyrFirethorn
Male
Pisces
41
58
Bard
Jump
Counter
Equip Gun
Move-HP Up

Blast Gun

Twist Headband
Brigandine
Rubber Shoes

Angel Song, Battle Song, Nameless Song, Last Song, Sky Demon
Level Jump5, Vertical Jump7



Silentperogy
Female
Scorpio
63
78
Ninja
Time Magic
Parry
Long Status
Waterbreathing

Morning Star
Flail
Triangle Hat
Judo Outfit
Elf Mantle

Shuriken, Bomb, Wand
Haste, Haste 2, Stop, Float, Stabilize Time



Zetchryn
Male
Aries
66
41
Ninja
Jump
Dragon Spirit
Equip Polearm
Ignore Height

Cypress Rod
Iron Fan
Green Beret
Brigandine
Genji Gauntlet

Shuriken
Level Jump8, Vertical Jump7



SolarisFall
Male
Scorpio
41
71
Geomancer
Yin Yang Magic
Arrow Guard
Equip Polearm
Jump+3

Mythril Spear
Flame Shield
Twist Headband
Wizard Outfit
Sprint Shoes

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Poison, Foxbird, Confusion Song, Dark Holy

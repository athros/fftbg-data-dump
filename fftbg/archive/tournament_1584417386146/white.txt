Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Female
Cancer
47
63
Mediator
Elemental
Counter Tackle
Equip Knife
Waterbreathing

Dragon Rod

Holy Miter
Mystic Vest
Defense Ring

Invitation, Death Sentence, Insult, Negotiate, Refute
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



Evdoggity
Male
Sagittarius
65
68
Summoner
Elemental
Arrow Guard
Magic Defense UP
Move+2

Wizard Rod

Flash Hat
Black Costume
Spike Shoes

Moogle, Ramuh, Carbunkle, Odin, Leviathan, Salamander, Lich, Cyclops
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



ANFz
Female
Aries
39
72
Thief
Yin Yang Magic
Critical Quick
Doublehand
Retreat

Ice Brand

Feather Hat
Mystic Vest
Jade Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic



BuffaloCrunch
Male
Aries
68
73
Wizard
Summon Magic
Counter Magic
Equip Knife
Move+3

Main Gauche

Green Beret
Black Robe
Diamond Armlet

Fire, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 3, Flare
Shiva, Titan, Golem, Carbunkle, Leviathan, Salamander, Silf, Fairy

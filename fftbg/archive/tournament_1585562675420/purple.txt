Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



PatSouI
Male
Virgo
72
50
Samurai
White Magic
Parry
Beastmaster
Waterbreathing

Kiyomori

Leather Helmet
Reflect Mail
Genji Gauntlet

Koutetsu, Muramasa
Cure, Cure 4, Raise, Raise 2, Shell 2, Wall, Esuna



Treapvort
Female
Aquarius
54
72
Geomancer
Jump
Distribute
Short Status
Teleport

Giant Axe
Ice Shield
Feather Hat
Black Robe
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind
Level Jump2, Vertical Jump6



Ko2q
Female
Capricorn
58
42
Dancer
Basic Skill
HP Restore
Attack UP
Waterwalking

Cashmere

Headgear
Wizard Robe
Diamond Armlet

Wiznaibus, Nameless Dance, Obsidian Blade
Throw Stone, Yell, Fury, Ultima



DaveStrider55
Female
Pisces
42
77
Mediator
Basic Skill
Damage Split
Dual Wield
Jump+2

Stone Gun
Blaze Gun
Holy Miter
Black Robe
Wizard Mantle

Persuade, Praise, Insult, Negotiate, Rehabilitate
Accumulate, Heal, Yell, Cheer Up

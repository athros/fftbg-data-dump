Player: !Red
Team: Red Team
Palettes: Red/Brown



Cataphract116
Female
Virgo
67
51
Calculator
Black Magic
Catch
Equip Knife
Waterwalking

Spell Edge

Black Hood
Wizard Robe
Battle Boots

CT, Height, Prime Number, 5, 4, 3
Fire, Fire 3, Bolt, Bolt 2, Ice



RonaldoTheGypsy
Monster
Capricorn
75
49
Porky










Oodlesonoodles11
Male
Pisces
80
76
Mediator
White Magic
Caution
Martial Arts
Ignore Height



Flash Hat
Wizard Robe
Bracer

Persuade, Threaten, Preach, Solution, Negotiate, Mimic Daravon, Refute
Cure, Regen, Protect 2, Wall, Esuna



Shakarak
Female
Capricorn
74
40
Time Mage
Talk Skill
Caution
Equip Shield
Jump+1

Battle Folio
Ice Shield
Leather Hat
Leather Outfit
Bracer

Haste, Stop, Reflect, Demi, Stabilize Time
Praise, Solution, Mimic Daravon, Rehabilitate

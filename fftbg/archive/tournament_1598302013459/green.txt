Player: !Green
Team: Green Team
Palettes: Green/White



Soren Of Tyto
Male
Sagittarius
37
66
Monk
Item
Parry
Throw Item
Jump+2



Leather Hat
Clothes
Battle Boots

Secret Fist, Purification, Revive, Seal Evil
Hi-Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down



Arumz
Male
Cancer
71
81
Samurai
Sing
PA Save
Beastmaster
Fly

Spear

Circlet
Light Robe
Defense Ring

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa
Battle Song, Nameless Song, Last Song



DavenIII
Male
Sagittarius
78
66
Monk
Charge
Critical Quick
Dual Wield
Teleport



Green Beret
Judo Outfit
Cursed Ring

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Charge+3



Dogsandcatsand
Monster
Aquarius
68
45
Ghoul








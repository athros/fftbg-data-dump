Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Gorgewall
Female
Aries
37
79
Wizard
Basic Skill
HP Restore
Equip Gun
Fly

Battle Folio

Flash Hat
Wizard Outfit
Reflect Ring

Fire, Bolt, Bolt 2
Dash, Wish, Ultima



Sairentozon7
Female
Aries
71
48
Time Mage
Charge
Earplug
Equip Knife
Move+1

Thunder Rod

Twist Headband
Secret Clothes
Salty Rage

Haste, Slow 2, Immobilize, Float, Quick, Stabilize Time
Charge+1, Charge+10



L2 Sentinel
Male
Leo
73
45
Lancer
Charge
Counter
Doublehand
Ignore Height

Dragon Whisker

Iron Helmet
Leather Armor
Angel Ring

Level Jump8, Vertical Jump2
Charge+4, Charge+5



Polygamic
Female
Virgo
57
39
Archer
Black Magic
HP Restore
Magic Defense UP
Waterbreathing

Night Killer
Diamond Shield
Green Beret
Judo Outfit
Sprint Shoes

Charge+1, Charge+7, Charge+20
Fire 3, Ice 4, Frog, Death

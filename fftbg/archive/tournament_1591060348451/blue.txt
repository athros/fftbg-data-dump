Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CosmicTactician
Male
Taurus
50
57
Wizard
Time Magic
Counter
Short Charge
Retreat

Wizard Rod

Red Hood
Light Robe
Sprint Shoes

Fire 2, Fire 3, Bolt, Bolt 3, Ice, Ice 2, Ice 3
Haste, Haste 2



DudeMonkey77
Female
Virgo
63
77
Chemist
Charge
Counter
Martial Arts
Move+2

Star Bag

Leather Hat
Chain Vest
Diamond Armlet

Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Charge+1, Charge+3, Charge+4



Rislyeu
Female
Cancer
69
59
Mime

Damage Split
Attack UP
Move+2



Holy Miter
Maximillian
Feather Mantle

Mimic




Polygamic
Male
Leo
72
62
Wizard
Steal
Caution
Equip Armor
Jump+1

Ice Rod

Circlet
Gold Armor
Dracula Mantle

Fire 4, Bolt 2, Flare
Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory

Player: !Red
Team: Red Team
Palettes: Red/Brown



The Pengwin
Female
Capricorn
65
35
Time Mage
Jump
Counter Magic
Short Status
Lava Walking

Battle Bamboo

Cachusha
Wizard Robe
Genji Gauntlet

Haste, Haste 2, Slow 2, Float, Quick, Demi
Level Jump8, Vertical Jump4



Kyune
Female
Virgo
42
71
Calculator
Undeath Skill
Parry
Secret Hunt
Lava Walking

Battle Folio

Iron Helmet
Robe of Lords
Magic Gauntlet

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



TinchoT
Female
Aquarius
43
68
Squire
Dance
Counter Tackle
Doublehand
Teleport

Assassin Dagger

Barbuta
Mythril Armor
Feather Mantle

Accumulate, Dash, Throw Stone, Heal, Tickle
Slow Dance, Polka Polka, Last Dance, Dragon Pit



Oreo Pizza
Male
Sagittarius
39
72
Lancer
Throw
Regenerator
Doublehand
Teleport

Spear

Diamond Helmet
Carabini Mail
Feather Mantle

Level Jump8, Vertical Jump6
Bomb, Knife, Spear

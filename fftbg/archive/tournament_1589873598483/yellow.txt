Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



JLinkletter
Male
Virgo
51
68
Knight
White Magic
Faith Save
Maintenance
Move-HP Up

Save the Queen
Buckler
Crystal Helmet
Wizard Robe
Feather Mantle

Armor Break, Stasis Sword, Dark Sword
Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect 2, Esuna, Holy, Magic Barrier



Phytik
Female
Aries
80
58
Lancer
White Magic
Counter Tackle
Martial Arts
Levitate


Round Shield
Gold Helmet
Silk Robe
Defense Armlet

Level Jump3, Vertical Jump7
Cure 4, Raise, Raise 2, Regen, Protect, Shell, Esuna



MalakiGenesys
Male
Aquarius
54
74
Time Mage
White Magic
MP Restore
Equip Shield
Move+2

Wizard Staff
Bronze Shield
Holy Miter
Mythril Vest
Small Mantle

Slow, Slow 2, Stop, Stabilize Time
Cure, Cure 2, Protect, Protect 2, Shell 2, Wall, Esuna, Holy



Communis
Male
Taurus
54
69
Archer
Battle Skill
Brave Save
Concentrate
Fly

Bow Gun
Bronze Shield
Holy Miter
Chain Vest
Elf Mantle

Charge+1, Charge+2, Charge+4, Charge+20
Head Break, Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Justice Sword, Night Sword

Final Bets: white - 13 bets for 4,796G (42.3%, x1.37); black - 11 bets for 6,548G (57.7%, x0.73)

white bets:
SkylerBunny: 1,500G (31.3%, 59,912G)
douchetron: 726G (15.1%, 1,425G)
Laserman1000: 672G (14.0%, 672G)
superdevon1: 546G (11.4%, 18,207G)
holdenmagronik: 224G (4.7%, 1,500G)
MLWebz0r: 212G (4.4%, 212G)
Treafa: 200G (4.2%, 1,877G)
datadrivenbot: 200G (4.2%, 41,605G)
mrpickins: 200G (4.2%, 3,403G)
slotheye17: 104G (2.2%, 104G)
timthehoodie: 100G (2.1%, 1,667G)
Firesheath: 100G (2.1%, 11,143G)
getthemoneyz: 12G (0.3%, 1,377,971G)

black bets:
reinoe: 2,000G (30.5%, 125,318G)
sinnyil2: 1,604G (24.5%, 1,604G)
HaateXIII: 868G (13.3%, 868G)
SUGRboi: 500G (7.6%, 8,583G)
BirbBrainsBot: 452G (6.9%, 55,731G)
josephiroth_143: 312G (4.8%, 312G)
gorgewall: 201G (3.1%, 14,340G)
AllInBot: 200G (3.1%, 200G)
Hirameki85: 200G (3.1%, 2,400G)
readdesert: 111G (1.7%, 2,237G)
Twisted_Nutsatchel: 100G (1.5%, 13,226G)

Final Bets: white - 6 bets for 4,760G (50.2%, x0.99); black - 8 bets for 4,718G (49.8%, x1.01)

white bets:
BoneMiser: 1,332G (28.0%, 1,332G)
douchetron: 1,330G (27.9%, 2,608G)
ColetteMSLP: 1,000G (21.0%, 33,554G)
prince_rogers_nelson_: 672G (14.1%, 672G)
getthemoneyz: 326G (6.8%, 1,444,304G)
Wooplestein: 100G (2.1%, 1,685G)

black bets:
AllInBot: 1,114G (23.6%, 1,114G)
BirbBrainsBot: 1,000G (21.2%, 35,091G)
NovaKnight21: 800G (17.0%, 800G)
CassiePhoenix: 764G (16.2%, 764G)
KyleWonToLiveForever: 416G (8.8%, 416G)
Chambs12: 224G (4.7%, 1,958G)
fluffskull: 200G (4.2%, 5,264G)
datadrivenbot: 200G (4.2%, 45,986G)

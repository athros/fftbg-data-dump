Player: !White
Team: White Team
Palettes: White/Blue



Mirapoix
Monster
Capricorn
61
53
Taiju










Go2sleepTV
Female
Gemini
58
58
Calculator
White Magic
Auto Potion
Equip Sword
Waterbreathing

Bizen Boat

Leather Hat
Black Costume
Germinas Boots

Height, Prime Number, 4, 3
Cure, Cure 2, Cure 4, Reraise, Regen, Wall, Esuna



Wooplestein
Female
Cancer
40
43
Summoner
Battle Skill
PA Save
Halve MP
Ignore Terrain

Bestiary

Barette
Light Robe
Angel Ring

Moogle, Ramuh, Titan, Carbunkle, Salamander, Fairy
Weapon Break, Magic Break, Speed Break, Justice Sword, Surging Sword, Explosion Sword



Jobina
Female
Leo
67
71
Knight
Throw
Brave Save
Halve MP
Jump+1

Defender
Platinum Shield
Platinum Helmet
Plate Mail
Leather Mantle

Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword
Bomb

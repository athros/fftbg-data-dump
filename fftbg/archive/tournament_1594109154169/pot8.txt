Final Bets: purple - 4 bets for 3,458G (28.8%, x2.48); champion - 12 bets for 8,559G (71.2%, x0.40)

purple bets:
electric_algus: 1,500G (43.4%, 38,018G)
byrdturbo: 1,111G (32.1%, 17,198G)
YaBoy125: 647G (18.7%, 6,471G)
hiros13gts: 200G (5.8%, 2,029G)

champion bets:
Evewho: 3,701G (43.2%, 7,258G)
BirbBrainsBot: 875G (10.2%, 122,043G)
VolgraTheMoose: 818G (9.6%, 818G)
getthemoneyz: 764G (8.9%, 1,217,863G)
rhsgangster: 676G (7.9%, 676G)
reinoe: 500G (5.8%, 38,897G)
superdevon1: 325G (3.8%, 6,514G)
enkikavlar: 316G (3.7%, 316G)
datadrivenbot: 200G (2.3%, 56,436G)
CT_5_Holy: 184G (2.1%, 3,695G)
DeathTaxesAndAnime: 100G (1.2%, 2,089G)
Chompie: 100G (1.2%, 11,109G)

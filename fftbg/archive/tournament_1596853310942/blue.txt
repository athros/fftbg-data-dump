Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grininda
Male
Scorpio
58
61
Lancer
Steal
Counter
Magic Attack UP
Move+3

Javelin
Platinum Shield
Mythril Helmet
Light Robe
Reflect Ring

Level Jump8, Vertical Jump8
Gil Taking, Steal Status



DustBirdEX
Monster
Pisces
58
52
Taiju










ALY327
Female
Capricorn
57
73
Ninja
Battle Skill
Parry
Martial Arts
Retreat



Red Hood
Adaman Vest
Rubber Shoes

Shuriken, Bomb, Staff
Armor Break, Power Break, Mind Break, Justice Sword, Surging Sword



Thyrandaal
Male
Taurus
76
47
Archer
Item
Catch
Attack UP
Move-HP Up

Mythril Gun
Buckler
Ribbon
Rubber Costume
Spike Shoes

Charge+1, Charge+2, Charge+3, Charge+10, Charge+20
Potion, Hi-Potion, X-Potion, Eye Drop, Phoenix Down

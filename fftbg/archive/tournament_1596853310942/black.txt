Player: !Black
Team: Black Team
Palettes: Black/Red



BoneMiser
Male
Cancer
65
72
Bard
Battle Skill
Counter Magic
Magic Defense UP
Jump+3

Fairy Harp

Holy Miter
Chain Vest
Feather Mantle

Life Song, Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword



Kellios11
Male
Libra
43
77
Priest
Time Magic
Distribute
Magic Defense UP
Teleport

White Staff

Thief Hat
Chain Vest
Magic Gauntlet

Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Esuna
Slow, Stop, Float, Demi, Stabilize Time



Nhammen
Female
Capricorn
75
41
Geomancer
Basic Skill
Parry
Secret Hunt
Move+3

Coral Sword
Escutcheon
Twist Headband
Mystic Vest
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Accumulate, Dash, Heal, Tickle, Wish



Gorgewall
Female
Capricorn
65
65
Ninja
Black Magic
Earplug
Equip Knife
Ignore Terrain

Spell Edge
Thunder Rod
Triangle Hat
Black Costume
108 Gems

Bomb, Sword
Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 3, Ice 4, Frog

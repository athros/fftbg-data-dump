Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Roseblood
Monster
Scorpio
72
80
Archaic Demon










OperatorTheory
Female
Sagittarius
65
80
Chemist
Yin Yang Magic
MA Save
Equip Polearm
Ignore Terrain

Obelisk

Golden Hairpin
Wizard Outfit
Jade Armlet

Hi-Potion, Ether, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down
Doubt Faith, Zombie, Dispel Magic, Paralyze, Dark Holy



Arcblazer23
Male
Aries
60
72
Oracle
Jump
HP Restore
Magic Defense UP
Move+2

Battle Folio

Leather Hat
Linen Robe
Elf Mantle

Spell Absorb, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dark Holy
Level Jump8, Vertical Jump7



DLJuggernaut
Male
Aquarius
46
77
Priest
Summon Magic
Meatbone Slash
Equip Gun
Waterbreathing

Battle Folio

Black Hood
Adaman Vest
Genji Gauntlet

Cure, Raise, Shell, Esuna, Holy
Moogle, Titan, Fairy, Lich

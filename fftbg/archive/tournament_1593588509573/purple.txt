Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ring Wyrm
Female
Aries
71
51
Monk
Dance
Regenerator
Equip Knife
Waterwalking

Spell Edge

Headgear
Leather Outfit
Power Wrist

Purification, Revive
Wiznaibus, Nameless Dance



Digitalsocrates
Female
Sagittarius
67
50
Mime

Counter Flood
Martial Arts
Jump+2



Black Hood
Chain Vest
108 Gems

Mimic




Genericco
Female
Leo
80
66
Geomancer
Draw Out
PA Save
Equip Armor
Move-MP Up

Ancient Sword
Gold Shield
Diamond Helmet
Judo Outfit
Feather Boots

Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bizen Boat



OpHendoslice
Male
Sagittarius
52
75
Knight
Basic Skill
Hamedo
Equip Gun
Move-HP Up

Fairy Harp
Platinum Shield
Cross Helmet
Gold Armor
Cursed Ring

Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Night Sword, Surging Sword
Heal, Fury

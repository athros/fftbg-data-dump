Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CCRUNNER149UWP
Male
Sagittarius
57
49
Geomancer
Punch Art
Damage Split
Magic Attack UP
Move+3

Blood Sword
Round Shield
Headgear
White Robe
Angel Ring

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Purification



Breakdown777
Female
Libra
80
36
Chemist
Talk Skill
MA Save
Equip Knife
Teleport

Short Edge

Thief Hat
Leather Outfit
Wizard Mantle

Potion, X-Potion, Echo Grass, Holy Water, Remedy, Phoenix Down
Invitation, Threaten, Solution, Death Sentence, Rehabilitate



Vorap
Male
Serpentarius
45
63
Oracle
Steal
Faith Save
Equip Bow
Levitate

Mythril Bow

Cachusha
Black Robe
Reflect Ring

Spell Absorb, Dispel Magic, Sleep, Petrify, Dark Holy
Steal Heart, Steal Armor, Steal Shield



XalSmellsBad
Male
Cancer
74
69
Wizard
Charge
Counter Flood
Martial Arts
Move-MP Up

Rod

Triangle Hat
Clothes
Battle Boots

Fire, Fire 2, Fire 3, Bolt 2, Ice 4, Empower
Charge+1, Charge+4, Charge+5, Charge+7, Charge+10

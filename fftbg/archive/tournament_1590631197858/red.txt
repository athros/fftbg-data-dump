Player: !Red
Team: Red Team
Palettes: Red/Brown



LDSkinny
Female
Scorpio
48
43
Oracle
Draw Out
Caution
Short Charge
Swim

Octagon Rod

Leather Hat
Light Robe
Vanish Mantle

Poison, Life Drain, Zombie, Silence Song, Confusion Song, Dispel Magic
Asura, Koutetsu, Bizen Boat, Muramasa, Kikuichimoji



Virilikus
Female
Gemini
69
44
Geomancer
Draw Out
Mana Shield
Short Status
Move+1

Battle Axe
Bronze Shield
Black Hood
Silk Robe
Magic Gauntlet

Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard
Kikuichimoji



ValtonZenola
Male
Leo
73
63
Wizard
Summon Magic
Regenerator
Equip Bow
Levitate

Ice Bow

Green Beret
Black Robe
Angel Ring

Fire, Bolt 3, Bolt 4, Ice, Ice 4, Death, Flare
Moogle, Golem, Leviathan, Salamander



Gorgewall
Female
Capricorn
65
63
Lancer
Steal
HP Restore
Equip Sword
Levitate

Chaos Blade

Cross Helmet
Linen Cuirass
Small Mantle

Level Jump5, Vertical Jump6
Gil Taking, Steal Helmet, Steal Status

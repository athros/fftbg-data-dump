Player: !Green
Team: Green Team
Palettes: Green/White



Brokenknight201
Female
Cancer
63
66
Priest
Basic Skill
Counter Tackle
Short Status
Jump+3

Flail

Ribbon
Light Robe
Power Wrist

Cure, Cure 2, Raise, Wall, Esuna
Accumulate, Heal, Cheer Up



Baconbacon1207
Male
Libra
74
55
Knight
Basic Skill
Sunken State
Equip Armor
Teleport

Mythril Sword
Platinum Shield
Leather Hat
Judo Outfit
Battle Boots

Shield Break, Magic Break, Speed Break, Power Break, Mind Break
Dash, Yell, Wish



Error72
Male
Libra
75
54
Knight
Yin Yang Magic
Faith Save
Equip Bow
Jump+1

Night Killer
Crystal Shield
Crystal Helmet
Reflect Mail
Cherche

Head Break, Weapon Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword, Night Sword, Surging Sword
Blind, Pray Faith, Zombie, Foxbird, Dispel Magic, Sleep



Peeronid
Female
Aries
50
52
Priest
Draw Out
Faith Save
Equip Armor
Move+1

Scorpion Tail

Cross Helmet
Light Robe
Red Shoes

Cure, Cure 3, Raise, Shell 2, Esuna
Asura, Bizen Boat, Kiyomori

Player: !Green
Team: Green Team
Palettes: Green/White



Lowlf
Female
Cancer
58
58
Samurai
Yin Yang Magic
Regenerator
Equip Shield
Lava Walking

Bizen Boat
Ice Shield
Genji Helmet
Linen Robe
Red Shoes

Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Life Drain, Zombie, Foxbird, Dispel Magic, Sleep



GrayGhostGaming
Monster
Aquarius
67
76
Floating Eye










MadnessR
Male
Leo
69
70
Mediator
Summon Magic
Counter
Doublehand
Jump+1

Mythril Gun

Twist Headband
Chameleon Robe
Setiemson

Persuade, Preach, Negotiate, Rehabilitate
Moogle, Ramuh, Golem, Carbunkle, Leviathan



Avin Chaos
Female
Serpentarius
39
79
Priest
Battle Skill
Dragon Spirit
Sicken
Jump+1

Healing Staff

Headgear
Black Costume
Magic Ring

Cure, Raise, Raise 2, Protect, Shell 2, Esuna
Head Break, Armor Break, Magic Break, Dark Sword, Night Sword

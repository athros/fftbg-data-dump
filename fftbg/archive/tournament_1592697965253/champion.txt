Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Neo Exodus
Male
Leo
63
55
Thief
White Magic
Catch
Beastmaster
Jump+1

Mythril Sword

Leather Hat
Earth Clothes
Spike Shoes

Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Arm Aim
Raise, Reraise, Regen, Shell, Wall, Esuna



Sinnyil2
Male
Capricorn
73
64
Knight
Summon Magic
Meatbone Slash
Sicken
Teleport 2

Chaos Blade
Ice Shield
Cross Helmet
Crystal Mail
Magic Ring

Head Break, Magic Break, Speed Break, Night Sword
Moogle, Titan, Golem, Carbunkle, Leviathan, Silf



Hasterious
Monster
Virgo
51
65
Holy Dragon










Quadh0nk
Male
Capricorn
77
50
Bard
Talk Skill
Earplug
Maintenance
Lava Walking

Fairy Harp

Leather Hat
Secret Clothes
Bracer

Cheer Song, Magic Song, Nameless Song, Sky Demon
Persuade, Preach, Death Sentence, Refute

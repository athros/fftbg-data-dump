Player: !Red
Team: Red Team
Palettes: Red/Brown



Reinoe
Male
Taurus
76
70
Monk
Item
Distribute
Equip Bow
Teleport

Hunting Bow

Headgear
Brigandine
Angel Ring

Spin Fist, Secret Fist, Purification, Chakra, Revive
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Umdstreaming
Female
Aquarius
73
44
Oracle
Charge
Mana Shield
Beastmaster
Jump+2

Gokuu Rod

Green Beret
Linen Robe
Reflect Ring

Blind, Spell Absorb, Blind Rage
Charge+2, Charge+10, Charge+20



Kolonel Panic
Monster
Virgo
70
78
Minotaur










Arch8000
Female
Capricorn
78
80
Chemist
Jump
Auto Potion
Equip Axe
Waterbreathing

Giant Axe

Headgear
Judo Outfit
Magic Gauntlet

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Level Jump2, Vertical Jump8

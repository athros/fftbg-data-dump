Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mirapoix
Male
Cancer
62
43
Mime

Auto Potion
Secret Hunt
Jump+1



Flash Hat
Chain Mail
Magic Gauntlet

Mimic




Shakarak
Male
Virgo
62
49
Squire
White Magic
Earplug
Short Status
Jump+2

Coral Sword

Golden Hairpin
Rubber Costume
Dracula Mantle

Accumulate, Dash, Heal, Yell
Raise 2, Protect, Protect 2, Shell, Esuna, Magic Barrier



SeedSC
Female
Leo
45
75
Calculator
Time Magic
Counter
Magic Attack UP
Move+3

Wizard Rod

Triangle Hat
Power Sleeve
Germinas Boots

CT, Height, 3
Haste, Haste 2, Immobilize, Float, Demi



DaveStrider55
Male
Aries
54
68
Time Mage
Punch Art
Regenerator
Long Status
Move+2

White Staff

Green Beret
Robe of Lords
Vanish Mantle

Haste, Slow 2, Stop, Immobilize, Float, Reflect, Demi 2, Stabilize Time
Earth Slash, Purification, Revive

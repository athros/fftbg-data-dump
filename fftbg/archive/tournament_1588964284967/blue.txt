Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ZephyrTempest
Male
Aries
52
55
Mediator
Yin Yang Magic
Parry
Attack UP
Retreat

Papyrus Codex

Twist Headband
Black Robe
Power Wrist

Threaten, Death Sentence, Insult, Mimic Daravon
Poison, Pray Faith, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze, Dark Holy



Roofiepops
Male
Pisces
44
41
Chemist
Yin Yang Magic
Abandon
Equip Polearm
Waterwalking

Javelin

Golden Hairpin
Mystic Vest
Genji Gauntlet

Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Phoenix Down
Poison, Spell Absorb, Blind Rage, Dispel Magic



Gikokiko
Female
Pisces
80
74
Calculator
Nether Skill
Arrow Guard
Equip Bow
Waterbreathing

Poison Bow

Black Hood
Judo Outfit
Bracer

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



DeathTaxesAndAnime
Female
Sagittarius
57
76
Samurai
Basic Skill
Faith Up
Equip Armor
Jump+2

Chirijiraden

Golden Hairpin
Robe of Lords
Sprint Shoes

Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Throw Stone, Heal, Tickle, Wish, Ultima

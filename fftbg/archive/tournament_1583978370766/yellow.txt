Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Witchhunterix
Female
Pisces
46
55
Mime

Counter Flood
Defend
Fly



Holy Miter
Chain Mail
Jade Armlet

Mimic




HaychDub
Male
Sagittarius
45
47
Squire
Item
Critical Quick
Defend
Ignore Terrain

Rune Blade
Platinum Shield
Holy Miter
Diamond Armor
Genji Gauntlet

Accumulate, Dash, Tickle, Scream
Hi-Potion, X-Potion, Antidote, Echo Grass, Holy Water, Phoenix Down



Galkife
Male
Scorpio
51
64
Oracle
Steal
HP Restore
Magic Attack UP
Move+2

Battle Folio

Black Hood
Silk Robe
Sprint Shoes

Poison, Life Drain, Pray Faith, Foxbird, Confusion Song, Petrify
Gil Taking, Steal Accessory



VirulenceXT
Male
Libra
43
75
Chemist
Draw Out
Counter
Defense UP
Waterwalking

Dagger

Feather Hat
Brigandine
Wizard Mantle

Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Kikuichimoji, Chirijiraden

Player: !Green
Team: Green Team
Palettes: Green/White



DavenIII
Male
Taurus
66
59
Ninja
Summon Magic
Parry
Magic Attack UP
Fly

Zorlin Shape
Orichalcum
Twist Headband
Black Costume
Cursed Ring

Bomb, Spear, Wand
Moogle, Leviathan, Salamander, Fairy, Lich, Cyclops



DudeMonkey77
Female
Virgo
49
41
Knight
Charge
Dragon Spirit
Magic Defense UP
Waterwalking

Mythril Sword
Mythril Shield
Bronze Helmet
Plate Mail
Cursed Ring

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break
Charge+4, Charge+5



Kohlingen
Female
Virgo
61
65
Summoner
Charge
Earplug
Equip Axe
Lava Walking

Scorpion Tail

Golden Hairpin
Black Robe
Red Shoes

Moogle, Titan, Golem, Leviathan, Fairy, Lich
Charge+1, Charge+2, Charge+5



Mesmaster
Female
Leo
49
52
Wizard
Draw Out
Caution
Defend
Jump+1

Rod

Twist Headband
Mythril Vest
Bracer

Fire, Fire 4, Bolt 3, Bolt 4, Ice
Muramasa

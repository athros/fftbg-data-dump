Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Deathmaker06
Female
Gemini
73
47
Geomancer
Draw Out
HP Restore
Equip Bow
Jump+3

Hunting Bow
Escutcheon
Black Hood
Chain Vest
Elf Mantle

Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Koutetsu, Kiyomori



MrDiggs49
Female
Capricorn
72
63
Squire
Talk Skill
Parry
Equip Bow
Swim

Hunting Bow
Aegis Shield
Leather Hat
Mystic Vest
Magic Gauntlet

Throw Stone, Heal, Wish
Praise, Negotiate, Refute



RughSontos
Male
Aquarius
45
54
Geomancer
Punch Art
Meatbone Slash
Equip Polearm
Levitate

Mythril Spear
Genji Shield
Red Hood
Judo Outfit
Genji Gauntlet

Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Purification, Chakra, Revive



Heroebal
Female
Taurus
49
60
Calculator
Black Magic
Arrow Guard
Defense UP
Jump+3

Wizard Rod

Feather Hat
Chain Vest
Dracula Mantle

Height, 4
Fire, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 3

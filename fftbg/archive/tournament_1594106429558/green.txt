Player: !Green
Team: Green Team
Palettes: Green/White



L2 Sentinel
Male
Cancer
62
79
Monk
Charge
Distribute
Dual Wield
Move+3



Twist Headband
Mythril Vest
Reflect Ring

Spin Fist, Pummel, Earth Slash, Purification, Revive, Seal Evil
Charge+7, Charge+10



Reinoe
Male
Capricorn
50
70
Mediator
Sing
Damage Split
Equip Sword
Jump+2

Defender

Leather Hat
Silk Robe
Jade Armlet

Invitation, Threaten, Death Sentence, Mimic Daravon, Refute
Angel Song, Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade, Sky Demon



Enkikavlar
Female
Aries
67
44
Knight
Punch Art
Critical Quick
Defend
Move+2

Slasher
Buckler
Bronze Helmet
White Robe
Feather Mantle

Armor Break, Power Break, Mind Break
Earth Slash, Purification, Revive, Seal Evil



Deathmaker06
Female
Aries
80
57
Thief
Elemental
Catch
Magic Attack UP
Fly

Mythril Knife

Leather Hat
Mystic Vest
Battle Boots

Gil Taking, Steal Shield, Steal Weapon
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

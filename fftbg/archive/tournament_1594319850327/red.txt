Player: !Red
Team: Red Team
Palettes: Red/Brown



Blastinus
Female
Gemini
77
55
Knight
Throw
Hamedo
Long Status
Fly

Sleep Sword
Venetian Shield
Bronze Helmet
Bronze Armor
Defense Ring

Head Break, Armor Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Night Sword
Shuriken, Bomb, Wand, Dictionary



Laserman1000
Monster
Libra
75
53
Hydra










Vultuous
Male
Taurus
77
55
Priest
Yin Yang Magic
Absorb Used MP
Magic Defense UP
Teleport

White Staff

Golden Hairpin
Silk Robe
Feather Boots

Cure, Cure 3, Raise, Raise 2, Reraise, Protect, Shell, Shell 2, Esuna, Holy
Poison, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Dark Holy



E Ballard
Male
Virgo
74
44
Knight
Draw Out
Critical Quick
Equip Bow
Lava Walking

Hunting Bow
Aegis Shield
Iron Helmet
Carabini Mail
Dracula Mantle

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword
Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji

Player: !White
Team: White Team
Palettes: White/Blue



KingofTricksters
Male
Aries
64
75
Geomancer
Battle Skill
HP Restore
Magic Attack UP
Move+2

Masamune
Platinum Shield
Headgear
Black Robe
Magic Ring

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break



Lowlf
Male
Scorpio
43
62
Samurai
Sing
Sunken State
Equip Polearm
Jump+1

Mythril Spear

Crystal Helmet
Carabini Mail
Red Shoes

Koutetsu, Bizen Boat, Heaven's Cloud
Cheer Song, Sky Demon



ArchKnightX
Female
Sagittarius
63
80
Archer
Draw Out
Counter
Equip Axe
Ignore Terrain

Gold Staff
Gold Shield
Twist Headband
Judo Outfit
108 Gems

Charge+2, Charge+7
Kiyomori



Laserman1000
Male
Aquarius
52
70
Chemist
Basic Skill
MA Save
Magic Defense UP
Move+1

Orichalcum

Leather Hat
Mystic Vest
Small Mantle

Potion, Hi-Potion, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Throw Stone, Heal, Cheer Up

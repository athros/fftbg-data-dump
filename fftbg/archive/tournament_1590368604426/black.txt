Player: !Black
Team: Black Team
Palettes: Black/Red



Theseawolf1
Male
Virgo
35
62
Lancer
Draw Out
HP Restore
Sicken
Move-HP Up

Mythril Spear
Mythril Shield
Cross Helmet
Silk Robe
Defense Armlet

Level Jump2, Vertical Jump5
Koutetsu, Heaven's Cloud



Mtueni
Female
Cancer
44
60
Mime

Counter
Equip Armor
Retreat



Mythril Helmet
White Robe
Magic Ring

Mimic




Lawnboxer
Female
Capricorn
53
37
Summoner
Throw
Speed Save
Concentrate
Retreat

Flame Rod

Headgear
Silk Robe
Feather Boots

Shiva, Ifrit, Titan, Carbunkle, Odin, Silf, Fairy
Shuriken, Staff, Wand



ExecutedGiraffe
Female
Virgo
40
52
Squire
White Magic
Earplug
Attack UP
Fly

Blind Knife
Flame Shield
Green Beret
Crystal Mail
Elf Mantle

Accumulate
Raise, Regen, Protect 2, Shell 2, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Helpimabug
Male
Cancer
39
67
Time Mage
Steal
HP Restore
Attack UP
Waterbreathing

White Staff

Thief Hat
Wizard Outfit
Small Mantle

Haste, Slow, Float, Demi, Demi 2, Stabilize Time
Steal Heart, Steal Armor



ZergTwitch
Female
Aquarius
79
68
Archer
Basic Skill
Regenerator
Secret Hunt
Lava Walking

Hunting Bow
Escutcheon
Golden Hairpin
Wizard Outfit
Wizard Mantle

Charge+7, Charge+10, Charge+20
Heal, Cheer Up, Fury



Tildy
Female
Cancer
59
74
Dancer
Steal
Speed Save
Equip Shield
Jump+2

Ryozan Silk
Platinum Shield
Flash Hat
Black Robe
Defense Ring

Wiznaibus, Polka Polka, Nameless Dance, Last Dance, Nether Demon
Steal Shield, Steal Accessory, Steal Status



Thaddus
Male
Leo
79
76
Bard
Elemental
Faith Up
Dual Wield
Move+3

Bloody Strings
Fairy Harp
Red Hood
Bronze Armor
Spike Shoes

Last Song, Diamond Blade
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Minina Bella
Male
Taurus
75
45
Monk
Jump
Counter Magic
Defend
Move+3



Holy Miter
Clothes
Spike Shoes

Spin Fist, Pummel, Chakra
Level Jump5, Vertical Jump7



Egrazor
Male
Serpentarius
61
38
Chemist
Talk Skill
Distribute
Equip Sword
Jump+1

Orichalcum

Thief Hat
Black Costume
Germinas Boots

X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate



Upvla
Female
Libra
55
48
Archer
Jump
Damage Split
Doublehand
Fly

Poison Bow

Leather Hat
Rubber Costume
Bracer

Charge+1, Charge+7
Level Jump5, Vertical Jump7



Catfashions
Female
Taurus
78
47
Squire
Talk Skill
Counter Tackle
Equip Knife
Levitate

Koga Knife
Genji Shield
Diamond Helmet
Judo Outfit
Power Wrist

Heal, Yell
Praise, Refute

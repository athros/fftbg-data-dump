Player: !White
Team: White Team
Palettes: White/Blue



GameCorps
Female
Gemini
43
67
Dancer
Yin Yang Magic
Auto Potion
Beastmaster
Jump+2

Persia

Leather Hat
Wizard Outfit
Angel Ring

Witch Hunt, Polka Polka, Disillusion, Nameless Dance
Life Drain, Zombie, Foxbird



Jaing Skirata
Male
Leo
70
50
Archer
Elemental
Faith Up
Doublehand
Move+3

Hunting Bow

Golden Hairpin
Mystic Vest
Defense Ring

Charge+1, Charge+3, Charge+10, Charge+20
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Lodrak
Female
Taurus
74
66
Samurai
Throw
Abandon
Dual Wield
Move+1

Koutetsu Knife
Murasame
Diamond Helmet
Robe of Lords
Magic Gauntlet

Asura, Koutetsu
Shuriken, Sword



Zeroroute
Male
Serpentarius
54
67
Monk
Talk Skill
Regenerator
Throw Item
Ignore Terrain



Green Beret
Black Costume
Feather Mantle

Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Persuade, Threaten, Preach, Negotiate, Mimic Daravon, Refute

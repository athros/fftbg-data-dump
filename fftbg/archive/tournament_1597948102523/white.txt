Player: !White
Team: White Team
Palettes: White/Blue



Drusiform
Male
Leo
62
63
Lancer
Sing
Mana Shield
Doublehand
Waterbreathing

Holy Lance

Genji Helmet
Gold Armor
Bracer

Level Jump8, Vertical Jump8
Cheer Song, Magic Song, Nameless Song, Space Storage



CapnChaos12
Female
Gemini
46
77
Wizard
Draw Out
Speed Save
Attack UP
Move-MP Up

Ice Rod

Holy Miter
Mystic Vest
Magic Gauntlet

Fire, Fire 3, Bolt 2, Ice 2
Bizen Boat, Heaven's Cloud



Nifboy
Female
Virgo
56
69
Dancer
Elemental
Caution
Equip Sword
Swim

Kikuichimoji

Triangle Hat
Brigandine
Cursed Ring

Nameless Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Roofiepops
Female
Libra
60
59
Knight
Draw Out
Damage Split
Equip Polearm
Move+1

Spear
Hero Shield
Circlet
Linen Cuirass
Reflect Ring

Head Break, Armor Break, Shield Break, Weapon Break, Power Break, Justice Sword
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Chirijiraden

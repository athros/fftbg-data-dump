Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evdoggity
Female
Scorpio
78
48
Mediator
Battle Skill
PA Save
Equip Gun
Jump+3

Fairy Harp

Barette
White Robe
Power Wrist

Threaten, Preach, Refute, Rehabilitate
Weapon Break, Justice Sword, Dark Sword, Night Sword



Bruubarg
Female
Leo
72
48
Lancer
White Magic
Arrow Guard
Sicken
Levitate

Iron Fan
Crystal Shield
Crystal Helmet
Black Robe
Power Wrist

Level Jump3, Vertical Jump7
Cure 2, Cure 3, Cure 4, Raise, Reraise, Shell 2, Holy



JCBoorgo
Male
Serpentarius
79
79
Oracle
Throw
Absorb Used MP
Halve MP
Retreat

Octagon Rod

Feather Hat
Silk Robe
Jade Armlet

Blind, Poison, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Shuriken, Bomb



Error72
Male
Scorpio
57
74
Lancer
Throw
Counter Flood
Magic Attack UP
Move-MP Up

Javelin
Gold Shield
Bronze Helmet
Gold Armor
Battle Boots

Level Jump8, Vertical Jump3
Shuriken, Bomb, Ninja Sword

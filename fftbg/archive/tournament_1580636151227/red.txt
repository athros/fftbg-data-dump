Player: !Red
Team: Red Team
Palettes: Red/Brown



Shakarak
Male
Taurus
66
70
Thief
Talk Skill
Counter Magic
Equip Armor
Waterbreathing

Cultist Dagger

Headgear
Power Sleeve
Small Mantle

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Invitation, Persuade, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



Dallas7174
Female
Sagittarius
39
44
Geomancer
Basic Skill
Abandon
Secret Hunt
Ignore Terrain

Giant Axe
Crystal Shield
Holy Miter
Wizard Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Accumulate, Dash, Heal, Yell



WeepinbeII
Monster
Leo
50
69
Holy Dragon










YaBoy125
Male
Capricorn
37
37
Ninja
Item
Distribute
Equip Gun
Move-MP Up

Stone Gun
Mythril Gun
Twist Headband
Leather Vest
Battle Boots

Shuriken, Bomb
Potion, Ether, Hi-Ether, Remedy, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



Pathogen7
Female
Taurus
70
69
Priest
Punch Art
Mana Shield
Secret Hunt
Waterbreathing

Oak Staff

Holy Miter
Chameleon Robe
Spike Shoes

Cure 2, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell, Wall, Esuna
Secret Fist, Purification, Chakra, Revive



Snkey
Monster
Sagittarius
64
70
Blue Dragon










Ominnous
Male
Scorpio
44
68
Monk
Item
Counter Flood
Long Status
Teleport



Leather Hat
Adaman Vest
Bracer

Purification
Potion, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Lydian C
Female
Scorpio
70
62
Geomancer
Battle Skill
Counter
Equip Bow
Ignore Height

Long Bow

Twist Headband
Clothes
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Armor Break, Shield Break, Magic Break, Mind Break, Surging Sword

Player: !Green
Team: Green Team
Palettes: Green/White



Orangejuliuscaesar
Male
Virgo
45
40
Squire
Jump
Faith Up
Long Status
Move+1

Cross Bow
Round Shield
Gold Helmet
Mystic Vest
Sprint Shoes

Accumulate, Heal
Level Jump5, Vertical Jump8



MagicBottle
Female
Taurus
73
67
Mediator
Elemental
Critical Quick
Concentrate
Teleport

Madlemgen

Twist Headband
White Robe
Red Shoes

Persuade, Preach, Insult, Negotiate, Refute, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



LDSkinny
Male
Leo
77
51
Samurai
Black Magic
Parry
Short Charge
Lava Walking

Murasame

Leather Helmet
Genji Armor
Dracula Mantle

Bizen Boat, Kiyomori
Fire, Bolt, Bolt 2, Bolt 3, Bolt 4, Frog



Sotehr
Male
Capricorn
78
81
Monk
Item
Speed Save
Maintenance
Move+2



Flash Hat
Black Costume
Reflect Ring

Spin Fist, Earth Slash, Purification
Potion, Hi-Potion, Ether, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



D4rr1n
Male
Aquarius
49
69
Calculator
Yin Yang Magic
Counter
Martial Arts
Jump+2



Headgear
Black Costume
Angel Ring

CT, Height, Prime Number, 5, 3
Life Drain, Zombie, Dispel Magic, Sleep



Sinnyil2
Male
Gemini
51
55
Lancer
Time Magic
HP Restore
Doublehand
Ignore Terrain

Obelisk

Gold Helmet
Chameleon Robe
Vanish Mantle

Level Jump8, Vertical Jump4
Haste, Haste 2, Slow 2, Reflect



Arcadiues
Male
Sagittarius
75
39
Thief
Battle Skill
Counter Flood
Dual Wield
Swim

Coral Sword
Broad Sword
Flash Hat
Clothes
Rubber Shoes

Gil Taking, Steal Heart, Steal Status, Arm Aim
Weapon Break, Magic Break, Power Break, Mind Break, Surging Sword



Ilos
Male
Libra
71
47
Monk
Charge
Earplug
Secret Hunt
Ignore Terrain



Leather Hat
Power Sleeve
Power Wrist

Earth Slash, Secret Fist, Purification, Chakra, Revive
Charge+1, Charge+4, Charge+10

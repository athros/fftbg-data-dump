Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



WoooBlaaa
Female
Sagittarius
78
64
Geomancer
Battle Skill
Absorb Used MP
Equip Armor
Ignore Height

Battle Axe
Escutcheon
Bronze Helmet
Earth Clothes
Power Wrist

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Head Break, Power Break, Mind Break, Night Sword



Aldrammech
Female
Taurus
60
67
Time Mage
Punch Art
Mana Shield
Dual Wield
Move+1

Papyrus Codex
Papyrus Codex
Triangle Hat
Judo Outfit
Leather Mantle

Slow, Immobilize, Demi, Stabilize Time, Galaxy Stop
Purification, Revive



Ko2q
Female
Gemini
69
64
Thief
Item
Mana Shield
Equip Bow
Levitate

Lightning Bow

Thief Hat
Brigandine
Red Shoes

Steal Status, Arm Aim
Potion, Eye Drop, Phoenix Down



ACSpree
Male
Scorpio
60
78
Calculator
Yin Yang Magic
PA Save
Concentrate
Jump+3

Gokuu Rod

Flash Hat
Black Costume
Wizard Mantle

CT, Height, Prime Number, 5, 4
Blind, Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep

Final Bets: blue - 38 bets for 84,912G (64.8%, x0.54); yellow - 24 bets for 46,053G (35.2%, x1.84)

blue bets:
onetrickwolf: 33,829G (39.8%, 33,829G)
JonnyCue: 20,003G (23.6%, 20,003G)
catfashions: 8,969G (10.6%, 17,938G)
feDoran: 2,000G (2.4%, 27,368G)
cgMcWhiskers: 2,000G (2.4%, 4,246G)
Mesmaster: 2,000G (2.4%, 32,479G)
fenaen: 1,500G (1.8%, 59,846G)
lobsterlobster: 1,386G (1.6%, 1,386G)
Aldrammech: 1,000G (1.2%, 88,904G)
Draconis345: 1,000G (1.2%, 80,479G)
ungabunga_bot: 1,000G (1.2%, 278,492G)
BirbBrainsBot: 1,000G (1.2%, 81,010G)
vorap: 1,000G (1.2%, 13,798G)
WoooBlaaa: 942G (1.1%, 942G)
getthemoneyz: 872G (1.0%, 602,153G)
ZephyrTempest: 686G (0.8%, 4,574G)
YaBoy125: 594G (0.7%, 594G)
floopinthepig: 500G (0.6%, 1,543G)
killth3kid: 500G (0.6%, 41,619G)
HorusTaurus: 500G (0.6%, 8,424G)
ANFz: 500G (0.6%, 12,608G)
Lavatis: 430G (0.5%, 4,958G)
PetitFoulard: 390G (0.5%, 390G)
Pink_LuigiX: 305G (0.4%, 305G)
Avin_Chaos: 300G (0.4%, 15,550G)
trigger15: 250G (0.3%, 886G)
skuldar: 233G (0.3%, 233G)
oldmanbody: 204G (0.2%, 204G)
richardserious: 200G (0.2%, 466G)
AllInBot: 182G (0.2%, 182G)
DrAntiSocial: 100G (0.1%, 4,123G)
Evewho: 100G (0.1%, 5,039G)
nifboy: 100G (0.1%, 1,569G)
maakur_: 100G (0.1%, 110,392G)
datadrivenbot: 100G (0.1%, 8,859G)
FoeSquirrel: 100G (0.1%, 2,548G)
GingerDynomite: 25G (0.0%, 6,797G)
daveb_: 12G (0.0%, 2,153G)

yellow bets:
LDSkinny: 19,748G (42.9%, 19,748G)
DeathTaxesAndAnime: 6,350G (13.8%, 12,451G)
lowlf: 5,001G (10.9%, 53,199G)
Nizaha: 4,303G (9.3%, 8,607G)
Cataphract116: 2,000G (4.3%, 22,879G)
Chrysaors: 1,717G (3.7%, 42,945G)
DavenIII: 1,325G (2.9%, 2,650G)
reinoe: 1,000G (2.2%, 13,698G)
RyuTsuno: 600G (1.3%, 600G)
CapnChaos12: 500G (1.1%, 46,277G)
helpimabug: 500G (1.1%, 3,433G)
sinnyil2: 500G (1.1%, 2,528G)
ACSpree: 450G (1.0%, 1,089G)
Anethum: 440G (1.0%, 440G)
elkydeluxe: 300G (0.7%, 22,108G)
Egil72: 292G (0.6%, 292G)
SquidgyXom: 217G (0.5%, 4,345G)
run_with_STONE_GUNS: 200G (0.4%, 6,061G)
Yontipon: 130G (0.3%, 130G)
lastly: 100G (0.2%, 729G)
CosmicTactician: 100G (0.2%, 3,317G)
funkyjelloman: 100G (0.2%, 479G)
rawb2: 100G (0.2%, 146G)
brokenknight201: 80G (0.2%, 100G)

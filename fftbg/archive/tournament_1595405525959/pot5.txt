Final Bets: red - 12 bets for 5,874G (75.3%, x0.33); green - 4 bets for 1,922G (24.7%, x3.06)

red bets:
skies_walker: 1,242G (21.1%, 1,242G)
UmaiJam: 1,000G (17.0%, 59,093G)
Lythe_Caraker: 1,000G (17.0%, 153,283G)
electric_algus: 500G (8.5%, 33,800G)
randgridr: 456G (7.8%, 2,887G)
superdevon1: 382G (6.5%, 3,824G)
byrdturbo: 333G (5.7%, 32,020G)
amiture: 308G (5.2%, 308G)
AllInBot: 200G (3.4%, 200G)
datadrivenbot: 200G (3.4%, 30,849G)
getthemoneyz: 132G (2.2%, 1,338,612G)
Lydian_C: 121G (2.1%, 76,296G)

green bets:
Capitanufa: 1,000G (52.0%, 1,789G)
douchetron: 394G (20.5%, 394G)
Arcblazer23: 325G (16.9%, 325G)
BirbBrainsBot: 203G (10.6%, 177,629G)

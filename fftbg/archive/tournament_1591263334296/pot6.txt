Final Bets: white - 6 bets for 4,536G (56.0%, x0.79); brown - 8 bets for 3,570G (44.0%, x1.27)

white bets:
NovaKnight21: 3,000G (66.1%, 12,411G)
Sairentozon7: 728G (16.0%, 728G)
ar_tactic: 300G (6.6%, 58,430G)
BlackFireUK: 276G (6.1%, 276G)
KyleWonToLiveForever: 132G (2.9%, 132G)
datadrivenbot: 100G (2.2%, 33,284G)

brown bets:
ColetteMSLP: 1,295G (36.3%, 1,295G)
BirbBrainsBot: 1,000G (28.0%, 9,742G)
volgrathemoose: 500G (14.0%, 7,298G)
getthemoneyz: 204G (5.7%, 809,037G)
gogofromtogo: 200G (5.6%, 4,673G)
Kalabain: 136G (3.8%, 136G)
Zachara: 135G (3.8%, 99,935G)
Ring_Wyrm: 100G (2.8%, 2,724G)

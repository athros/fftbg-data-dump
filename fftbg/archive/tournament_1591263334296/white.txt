Player: !White
Team: White Team
Palettes: White/Blue



KyleWonToLiveForever
Male
Aquarius
57
78
Mediator
Battle Skill
Regenerator
Short Charge
Teleport

Battle Folio

Leather Hat
Wizard Robe
Rubber Shoes

Persuade, Threaten, Preach, Solution, Insult, Mimic Daravon
Head Break, Armor Break, Speed Break, Mind Break, Dark Sword, Surging Sword



NIghtdew14
Female
Sagittarius
61
56
Dancer
Item
Mana Shield
Dual Wield
Ignore Height

Cute Bag
Cute Bag
Cachusha
Chain Vest
Reflect Ring

Slow Dance, Disillusion, Nameless Dance, Nether Demon, Dragon Pit
Hi-Potion, Echo Grass, Maiden's Kiss



BlackFireUK
Male
Leo
74
68
Priest
Black Magic
Abandon
Equip Knife
Fly

Spell Edge

Headgear
Black Robe
Sprint Shoes

Cure 2, Cure 3, Raise, Protect, Protect 2, Shell, Shell 2
Bolt, Ice, Ice 2, Ice 4



Red Celt
Female
Gemini
50
65
Chemist
Throw
Earplug
Equip Gun
Ignore Terrain

Romanda Gun

Black Hood
Clothes
Feather Boots

Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Shuriken

Player: !Green
Team: Green Team
Palettes: Green/White



Lyonslegacy
Male
Virgo
81
80
Calculator
Mighty Sword
Counter
Beastmaster
Levitate

Gokuu Rod

Feather Hat
Genji Armor
Small Mantle

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Lastly
Female
Virgo
47
48
Chemist
Elemental
Arrow Guard
Equip Armor
Jump+1

Cultist Dagger

Gold Helmet
Robe of Lords
Feather Mantle

Potion, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Sinnyil2
Male
Leo
64
60
Archer
Elemental
Parry
Equip Polearm
Waterbreathing

Obelisk
Platinum Shield
Grand Helmet
Clothes
Setiemson

Charge+1, Charge+2, Charge+7, Charge+10
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Galkife
Male
Aquarius
60
54
Bard
Throw
MP Restore
Doublehand
Ignore Height

Ramia Harp

Feather Hat
Platinum Armor
Diamond Armlet

Cheer Song, Last Song
Bomb, Spear, Dictionary

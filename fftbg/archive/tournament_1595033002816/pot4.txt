Final Bets: purple - 14 bets for 4,769G (13.0%, x6.69); brown - 19 bets for 31,903G (87.0%, x0.15)

purple bets:
Oreo_Pizza: 1,000G (21.0%, 20,358G)
Thyrandaal: 600G (12.6%, 600G)
TheMurkGnome: 449G (9.4%, 9,445G)
Zetchryn: 432G (9.1%, 432G)
ColetteMSLP: 300G (6.3%, 5,699G)
fluffskull: 300G (6.3%, 3,952G)
getthemoneyz: 270G (5.7%, 1,309,741G)
ZicaX: 257G (5.4%, 2,862G)
Treafa: 248G (5.2%, 3,235G)
Othatmattkid: 212G (4.4%, 212G)
gorgewall: 201G (4.2%, 9,129G)
AllInBot: 200G (4.2%, 200G)
datadrivenbot: 200G (4.2%, 31,368G)
Miku_Shikhu: 100G (2.1%, 18,457G)

brown bets:
NicoSavoy: 10,000G (31.3%, 215,064G)
prince_rogers_nelson_: 6,200G (19.4%, 6,200G)
evontno: 3,000G (9.4%, 33,713G)
Evewho: 2,000G (6.3%, 5,977G)
HaateXIII: 1,806G (5.7%, 1,806G)
NIghtdew14: 1,567G (4.9%, 1,567G)
ANFz: 1,000G (3.1%, 47,779G)
BirbBrainsBot: 1,000G (3.1%, 42,985G)
dogsandcatsand: 1,000G (3.1%, 20,031G)
dem0nj0ns: 1,000G (3.1%, 8,618G)
Rislyeu: 1,000G (3.1%, 8,739G)
Mesmaster: 616G (1.9%, 616G)
serperemagus: 407G (1.3%, 407G)
randgridr: 388G (1.2%, 388G)
VolgraTheMoose: 301G (0.9%, 17,363G)
dtrain332: 248G (0.8%, 248G)
lyonslegacy: 150G (0.5%, 1,822G)
Royal_Ruckus: 120G (0.4%, 120G)
TasisSai: 100G (0.3%, 2,223G)

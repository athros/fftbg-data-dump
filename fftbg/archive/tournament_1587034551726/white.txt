Player: !White
Team: White Team
Palettes: White/Blue



Goust18
Monster
Libra
73
58
Swine










Volgrathemoose
Female
Sagittarius
76
77
Summoner
Battle Skill
Auto Potion
Short Charge
Fly

Thunder Rod

Twist Headband
Light Robe
Feather Boots

Moogle, Bahamut, Odin, Salamander, Fairy
Armor Break, Weapon Break, Speed Break, Power Break, Mind Break



Aneyus
Male
Aquarius
68
81
Bard
Jump
MP Restore
Equip Gun
Fly

Battle Folio

Golden Hairpin
Chain Mail
Feather Boots

Cheer Song, Battle Song, Nameless Song, Last Song, Diamond Blade, Space Storage
Level Jump2, Vertical Jump4



CassiePhoenix
Female
Gemini
48
62
Monk
Black Magic
Absorb Used MP
Dual Wield
Teleport



Triangle Hat
Brigandine
Genji Gauntlet

Pummel, Earth Slash, Chakra, Revive
Fire 3, Bolt 2, Bolt 4, Ice, Ice 3, Empower, Frog

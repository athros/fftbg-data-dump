Player: !Red
Team: Red Team
Palettes: Red/Brown



Daveb
Male
Taurus
54
73
Ninja
Jump
Dragon Spirit
Short Charge
Move-MP Up

Koga Knife
Flame Whip
Red Hood
Clothes
Dracula Mantle

Shuriken, Knife, Dictionary
Level Jump5, Vertical Jump8



HASTERIOUS
Female
Capricorn
49
60
Calculator
Black Magic
Sunken State
Short Status
Move+3

Ivory Rod

Twist Headband
Leather Outfit
Magic Gauntlet

CT, 5, 4, 3
Fire 4, Ice, Ice 2, Ice 4



Mtueni
Female
Leo
62
43
Mediator
White Magic
Damage Split
Magic Defense UP
Waterbreathing

Mythril Gun

Golden Hairpin
Chain Vest
Reflect Ring

Praise, Preach, Insult, Mimic Daravon, Rehabilitate
Cure 2, Cure 4, Raise, Shell 2, Esuna



HaplessOne
Male
Aquarius
53
76
Archer
Talk Skill
Meatbone Slash
Doublehand
Ignore Terrain

Windslash Bow

Circlet
Earth Clothes
Red Shoes

Charge+1, Charge+2, Charge+3, Charge+7, Charge+10
Praise, Solution, Insult, Mimic Daravon, Refute

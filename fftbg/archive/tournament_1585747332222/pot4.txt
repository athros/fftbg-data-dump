Final Bets: purple - 25 bets for 29,394G (71.7%, x0.39); brown - 20 bets for 11,608G (28.3%, x2.53)

purple bets:
LuckyLuckLuc2: 13,171G (44.8%, 13,171G)
Lodrak: 3,448G (11.7%, 6,896G)
rocl: 2,792G (9.5%, 5,584G)
Zeroroute: 1,806G (6.1%, 1,806G)
sinnyil2: 1,200G (4.1%, 9,996G)
NovaKnight21: 948G (3.2%, 948G)
jethrothrul: 703G (2.4%, 703G)
ig_ats: 657G (2.2%, 657G)
Ungaiii: 500G (1.7%, 3,000G)
Shalloween: 500G (1.7%, 49,037G)
volgrathemoose: 420G (1.4%, 1,920G)
Laserman1000: 400G (1.4%, 9,100G)
Bryon_W: 328G (1.1%, 328G)
LDSkinny: 320G (1.1%, 320G)
UmaiJam: 311G (1.1%, 3,110G)
Treapvort: 300G (1.0%, 300G)
holyonline: 300G (1.0%, 7,222G)
powergems: 200G (0.7%, 3,849G)
ugoplatamia: 200G (0.7%, 32,307G)
mikeyoffbeat: 200G (0.7%, 2,010G)
tmo50x: 200G (0.7%, 1,726G)
rjA0zcOQ96: 146G (0.5%, 2,754G)
RezzaDV: 144G (0.5%, 144G)
ko2q: 100G (0.3%, 2,901G)
datadrivenbot: 100G (0.3%, 14,550G)

brown bets:
ShintaroNayaka: 2,882G (24.8%, 2,882G)
TheChainNerd: 1,000G (8.6%, 31,394G)
thefyeman: 1,000G (8.6%, 16,100G)
BirbBrainsBot: 1,000G (8.6%, 136,019G)
getthemoneyz: 1,000G (8.6%, 402,521G)
bad1dea: 1,000G (8.6%, 352,106G)
fenixcrest: 786G (6.8%, 15,722G)
YaBoy125: 568G (4.9%, 568G)
RongRongArts: 512G (4.4%, 512G)
AdaephonD: 500G (4.3%, 7,830G)
SolarisFall: 459G (4.0%, 459G)
SomeCallMeGON: 200G (1.7%, 1,546G)
AllInBot: 100G (0.9%, 100G)
Slowbrofist: 100G (0.9%, 100G)
JumbocactuarX27: 100G (0.9%, 3,879G)
maakur_: 100G (0.9%, 3,073G)
RunicMagus: 100G (0.9%, 22,772G)
OttoRaynar: 100G (0.9%, 1,120G)
ZephyrTempest: 100G (0.9%, 23,521G)
daveb_: 1G (0.0%, 548G)

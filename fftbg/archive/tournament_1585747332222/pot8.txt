Final Bets: white - 15 bets for 7,798G (19.6%, x4.11); champion - 26 bets for 32,073G (80.4%, x0.24)

white bets:
ShintaroNayaka: 1,808G (23.2%, 1,808G)
sinnyil2: 1,200G (15.4%, 13,399G)
thefyeman: 1,000G (12.8%, 16,046G)
ins4n1ty87: 900G (11.5%, 6,180G)
Cataphract116: 572G (7.3%, 572G)
volgrathemoose: 501G (6.4%, 2,072G)
rubenflonne: 500G (6.4%, 7,481G)
EnemyController: 391G (5.0%, 391G)
rechaun: 300G (3.8%, 300G)
SomeCallMeGON: 200G (2.6%, 2,882G)
richardserious: 104G (1.3%, 104G)
AllInBot: 100G (1.3%, 100G)
holyonline: 100G (1.3%, 8,229G)
LDSkinny: 100G (1.3%, 1,609G)
roqqqpsi: 22G (0.3%, 1,124G)

champion bets:
Lord_Burrah: 8,547G (26.6%, 85,475G)
ugoplatamia: 6,496G (20.3%, 32,480G)
LuckyLuckLuc2: 5,555G (17.3%, 13,652G)
powergems: 1,814G (5.7%, 3,628G)
Treapvort: 1,244G (3.9%, 1,244G)
BirbBrainsBot: 1,000G (3.1%, 136,026G)
bad1dea: 1,000G (3.1%, 352,071G)
red__lancer: 1,000G (3.1%, 16,628G)
Lodrak: 732G (2.3%, 732G)
jethrothrul: 524G (1.6%, 524G)
RongRongArts: 512G (1.6%, 512G)
RunicMagus: 500G (1.6%, 22,624G)
Zeroroute: 500G (1.6%, 500G)
Shalloween: 400G (1.2%, 49,923G)
Slowbrofist: 396G (1.2%, 396G)
OmnibotGamma: 328G (1.0%, 328G)
SolarisFall: 308G (1.0%, 308G)
Aeolus_000: 300G (0.9%, 4,723G)
ForagerCats: 272G (0.8%, 272G)
RezzaDV: 144G (0.4%, 144G)
ko2q: 100G (0.3%, 3,384G)
DeathTaxesAndAnime: 100G (0.3%, 2,752G)
JumbocactuarX27: 100G (0.3%, 3,827G)
datadrivenbot: 100G (0.3%, 14,437G)
ZephyrTempest: 100G (0.3%, 23,517G)
daveb_: 1G (0.0%, 548G)

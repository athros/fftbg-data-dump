Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SSwing
Male
Aries
54
67
Oracle
Charge
Counter Flood
Equip Polearm
Swim

Cypress Rod

Green Beret
Linen Robe
Rubber Shoes

Poison, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Sleep
Charge+3, Charge+7, Charge+10



Estan AD
Female
Libra
60
66
Calculator
White Magic
PA Save
Long Status
Ignore Terrain

Iron Fan

Headgear
Light Robe
Magic Ring

CT, 5, 3
Cure 2, Raise, Raise 2, Esuna



Mannequ1n
Male
Libra
55
52
Lancer
Talk Skill
Regenerator
Doublehand
Ignore Height

Holy Lance

Crystal Helmet
Gold Armor
Feather Mantle

Level Jump8, Vertical Jump5
Invitation, Persuade, Preach, Mimic Daravon, Refute, Rehabilitate



Kyune
Male
Pisces
66
67
Knight
Sing
Counter Tackle
Defense UP
Move-MP Up

Platinum Sword
Genji Shield
Cross Helmet
Wizard Robe
Spike Shoes

Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Explosion Sword
Battle Song, Hydra Pit

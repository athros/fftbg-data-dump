Player: !White
Team: White Team
Palettes: White/Blue



NovaKnight21
Female
Leo
54
55
Samurai
Steal
Critical Quick
Equip Bow
Teleport

Hunting Bow

Barbuta
Mythril Armor
Wizard Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Steal Weapon, Arm Aim



Randgridr
Female
Scorpio
47
80
Mime

Counter
Doublehand
Fly



Green Beret
Wizard Outfit
Feather Mantle

Mimic




Victoriolue
Male
Capricorn
69
62
Calculator
Black Magic
Sunken State
Defense UP
Move-MP Up

Dragon Rod

Headgear
Linen Robe
Red Shoes

CT, Height, 5, 4
Fire 4, Bolt 2, Bolt 3, Ice 4



Calajo
Male
Libra
66
61
Ninja
Summon Magic
Faith Save
Short Charge
Jump+1

Assassin Dagger
Dagger
Black Hood
Black Costume
N-Kai Armlet

Shuriken
Moogle, Shiva, Ramuh, Titan, Carbunkle, Bahamut, Fairy, Lich, Cyclops

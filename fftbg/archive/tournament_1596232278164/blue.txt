Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RNdOrchestra
Female
Leo
47
70
Time Mage
Elemental
Parry
Equip Knife
Move+1

Ninja Edge

Feather Hat
White Robe
Magic Ring

Slow 2, Stop, Reflect, Quick, Stabilize Time
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Grandlanzer
Female
Libra
64
58
Mime

Distribute
Doublehand
Jump+3



Twist Headband
Earth Clothes
Red Shoes

Mimic




GrayGhostGaming
Female
Scorpio
66
77
Priest
Jump
Mana Shield
Equip Axe
Move+1

Giant Axe

Holy Miter
White Robe
Power Wrist

Shell, Wall, Esuna, Holy
Level Jump2, Vertical Jump5



Actual JP
Male
Aquarius
72
47
Knight
Time Magic
MP Restore
Short Charge
Move+1

Mythril Sword
Aegis Shield
Grand Helmet
Carabini Mail
Reflect Ring

Head Break, Armor Break, Shield Break, Justice Sword, Surging Sword
Haste, Slow, Float, Quick, Demi

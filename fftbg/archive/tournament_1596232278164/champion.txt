Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Miku Shikhu
Monster
Libra
79
76
Taiju










LDSkinny
Female
Capricorn
54
55
Oracle
Summon Magic
Caution
Dual Wield
Jump+1

Battle Bamboo
Iron Fan
Twist Headband
Black Robe
Vanish Mantle

Life Drain, Silence Song, Dispel Magic, Sleep, Petrify
Moogle, Shiva, Ramuh, Salamander, Silf



ArlanKels
Female
Aries
52
77
Knight
Item
Speed Save
Maintenance
Lava Walking

Iron Sword
Crystal Shield
Diamond Helmet
Crystal Mail
Defense Armlet

Head Break, Power Break, Mind Break, Dark Sword
Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Moonliquor
Male
Aquarius
64
44
Monk
Item
Counter
Equip Shield
Ignore Height


Venetian Shield
Golden Hairpin
Power Sleeve
Small Mantle

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

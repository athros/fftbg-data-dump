Player: !Green
Team: Green Team
Palettes: Green/White



Twelfthrootoftwo
Male
Capricorn
46
62
Ninja
White Magic
HP Restore
Concentrate
Lava Walking

Morning Star
Koga Knife
Red Hood
Power Sleeve
Defense Armlet

Bomb, Sword
Cure 2, Cure 3, Cure 4, Reraise, Protect 2, Shell, Shell 2, Esuna



ThePineappleSalesman
Male
Sagittarius
81
59
Calculator
Dragon Skill
Speed Save
Defend
Move-MP Up

Bestiary

Iron Helmet
Chain Vest
Diamond Armlet

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Valentine009
Male
Gemini
51
79
Knight
Draw Out
Damage Split
Equip Knife
Move+2

Wizard Rod
Platinum Shield
Circlet
Linen Cuirass
Red Shoes

Weapon Break, Magic Break, Surging Sword
Bizen Boat



HASTERIOUS
Male
Capricorn
53
76
Thief
Punch Art
Earplug
Concentrate
Swim

Mage Masher

Golden Hairpin
Mythril Vest
Germinas Boots

Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Spin Fist, Pummel, Purification, Revive

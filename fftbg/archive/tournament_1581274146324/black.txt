Player: !Black
Team: Black Team
Palettes: Black/Red



Silverthundr
Female
Taurus
44
56
Priest
Summon Magic
Blade Grasp
Equip Armor
Retreat

Oak Staff

Mythril Helmet
Crystal Mail
Cursed Ring

Cure 3, Raise, Reraise, Protect, Shell, Wall, Esuna
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Leviathan



Willjin
Male
Leo
58
63
Ninja
Elemental
Hamedo
Equip Polearm
Move-MP Up

Holy Lance
Partisan
Green Beret
Power Sleeve
Elf Mantle

Shuriken
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand



Moskey71
Male
Aquarius
49
43
Chemist
Summon Magic
Arrow Guard
Magic Defense UP
Swim

Hydra Bag

Golden Hairpin
Power Sleeve
Angel Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Moogle, Shiva, Titan, Bahamut, Odin, Salamander, Fairy



DavenIII
Male
Aries
44
42
Ninja
Elemental
Caution
Equip Armor
Lava Walking

Morning Star
Flail
Circlet
Light Robe
Angel Ring

Shuriken, Knife, Hammer, Dictionary
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

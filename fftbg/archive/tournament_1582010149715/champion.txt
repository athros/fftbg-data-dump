Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Drummerforsale
Monster
Gemini
46
54
Dark Behemoth










Insurmountablebluff
Male
Capricorn
45
68
Knight
Sing
Absorb Used MP
Equip Knife
Teleport

Air Knife
Mythril Shield
Gold Helmet
Carabini Mail
N-Kai Armlet

Shield Break, Power Break, Mind Break, Night Sword
Battle Song, Magic Song, Nameless Song, Space Storage, Sky Demon



Biske13
Male
Leo
45
80
Mime

Counter Magic
Attack UP
Move+3



Green Beret
Brigandine
Dracula Mantle

Mimic




WireLord
Male
Sagittarius
60
61
Calculator
Limit
Absorb Used MP
Long Status
Ignore Height

Dragon Rod

Mythril Helmet
Bronze Armor
Battle Boots

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

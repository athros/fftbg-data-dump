Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Justalnant
Male
Capricorn
70
75
Monk
Sing
Earplug
Attack UP
Jump+1



Black Hood
Earth Clothes
Feather Mantle

Chakra, Revive, Seal Evil
Diamond Blade



Alexmozg
Male
Aquarius
62
74
Squire
Punch Art
Parry
Defense UP
Fly

Rune Blade

Red Hood
Power Sleeve
Diamond Armlet

Accumulate, Dash, Yell, Cheer Up
Pummel, Purification



Linkn
Female
Scorpio
69
53
Knight
White Magic
Sunken State
Equip Armor
Jump+2

Ragnarok

Genji Helmet
Linen Robe
Defense Ring

Weapon Break, Speed Break, Surging Sword
Cure 2, Cure 3, Raise, Reraise, Shell, Shell 2, Esuna



Morenothings
Male
Sagittarius
45
68
Time Mage
Basic Skill
Absorb Used MP
Short Status
Move+3

Oak Staff

Thief Hat
Silk Robe
Diamond Armlet

Haste, Slow, Slow 2, Stop, Immobilize, Float, Demi, Demi 2, Stabilize Time, Meteor
Accumulate, Dash, Heal, Yell, Wish, Scream

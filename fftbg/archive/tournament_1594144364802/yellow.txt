Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Smokegiant
Female
Aries
62
63
Dancer
Basic Skill
Counter
Short Charge
Waterbreathing

Persia

Red Hood
Clothes
Chantage

Witch Hunt, Polka Polka, Void Storage
Accumulate, Dash, Heal, Yell



Just Here2
Monster
Leo
61
56
Blue Dragon










Sparker9
Male
Taurus
59
44
Bard
Jump
Sunken State
Defense UP
Jump+3

Bloody Strings

Holy Miter
Gold Armor
Battle Boots

Angel Song
Level Jump5, Vertical Jump6



TheChainNerd
Male
Capricorn
62
47
Monk
Jump
Speed Save
Short Charge
Teleport 2



Thief Hat
Power Sleeve
Reflect Ring

Purification, Chakra, Revive, Seal Evil
Level Jump2, Vertical Jump6

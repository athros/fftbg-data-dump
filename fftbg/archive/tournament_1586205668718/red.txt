Player: !Red
Team: Red Team
Palettes: Red/Brown



Gikokiko
Male
Serpentarius
65
45
Mediator
Punch Art
Mana Shield
Halve MP
Ignore Height

Assassin Dagger

Twist Headband
Judo Outfit
Leather Mantle

Threaten, Insult, Negotiate, Mimic Daravon, Refute
Earth Slash, Purification, Revive



Hyvi
Male
Aquarius
78
75
Ninja
Draw Out
MA Save
Concentrate
Move+1

Air Knife
Scorpion Tail
Feather Hat
Black Costume
Small Mantle

Shuriken, Wand, Dictionary
Murasame, Muramasa



Action Otter
Male
Pisces
71
72
Ninja
Talk Skill
Mana Shield
Equip Polearm
Waterbreathing

Whale Whisker
Cypress Rod
Holy Miter
Earth Clothes
Wizard Mantle

Bomb
Persuade, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon



ANFz
Female
Taurus
60
62
Time Mage
Battle Skill
Counter Tackle
Short Charge
Lava Walking

Oak Staff

Golden Hairpin
White Robe
Small Mantle

Haste, Slow, Immobilize, Float, Demi 2, Stabilize Time
Armor Break, Power Break, Justice Sword, Dark Sword, Night Sword

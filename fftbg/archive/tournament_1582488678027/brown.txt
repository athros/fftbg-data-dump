Player: !Brown
Team: Brown Team
Palettes: Brown/Green



JonnRPG
Monster
Sagittarius
80
74
Cockatrice










KoreanUsher
Female
Leo
68
76
Knight
Punch Art
Arrow Guard
Short Charge
Waterbreathing

Broad Sword
Aegis Shield
Leather Helmet
Robe of Lords
Diamond Armlet

Weapon Break, Speed Break, Power Break, Stasis Sword
Wave Fist, Purification



Secret Character
Female
Leo
68
69
Summoner
Punch Art
Mana Shield
Defend
Move+1

Dragon Rod

Flash Hat
Black Robe
108 Gems

Moogle, Ramuh, Ifrit, Leviathan, Lich, Cyclops
Pummel, Secret Fist, Purification, Revive, Seal Evil



Deciimos
Male
Leo
75
39
Ninja
Battle Skill
Parry
Equip Gun
Move+2

Battle Folio
Battle Folio
Barette
Mystic Vest
Power Wrist

Bomb
Head Break, Armor Break, Speed Break, Power Break, Mind Break

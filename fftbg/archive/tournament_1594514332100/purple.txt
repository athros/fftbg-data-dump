Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Letdowncity
Female
Virgo
59
73
Archer
Battle Skill
Counter Flood
Magic Defense UP
Retreat

Silver Bow

Holy Miter
Wizard Outfit
Spike Shoes

Charge+1, Charge+2, Charge+7
Armor Break, Speed Break, Power Break, Stasis Sword



ArchKnightX
Male
Pisces
47
47
Time Mage
Black Magic
Sunken State
Defend
Lava Walking

Oak Staff

Headgear
Silk Robe
Reflect Ring

Haste, Immobilize, Float, Quick
Fire, Fire 2, Bolt, Bolt 4, Ice, Ice 2, Ice 4, Flare



Omegasuspekt
Male
Aries
75
80
Ninja
White Magic
Catch
Defend
Move+1

Scorpion Tail
Flail
Flash Hat
Secret Clothes
Magic Ring

Shuriken, Bomb
Cure 4, Raise 2, Reraise, Regen, Esuna



Megakim
Male
Sagittarius
43
76
Summoner
Elemental
MA Save
Beastmaster
Swim

Dragon Rod

Black Hood
Chameleon Robe
Wizard Mantle

Moogle, Carbunkle, Fairy
Pitfall, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

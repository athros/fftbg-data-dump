Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DustBirdEX
Female
Virgo
55
52
Squire
Jump
Counter
Magic Attack UP
Jump+2

Morning Star
Gold Shield
Cross Helmet
Power Sleeve
Leather Mantle

Throw Stone, Heal, Yell, Cheer Up
Level Jump8, Vertical Jump4



Narcius
Male
Cancer
50
60
Squire
Talk Skill
MA Save
Equip Sword
Retreat

Defender

Black Hood
Judo Outfit
108 Gems

Throw Stone, Cheer Up
Invitation, Persuade, Praise, Solution, Death Sentence, Negotiate, Rehabilitate



Lowlf
Monster
Gemini
61
75
Gobbledeguck










VolgraTheMoose
Male
Libra
77
53
Geomancer
Charge
Counter Magic
Defense UP
Teleport

Heaven's Cloud
Bronze Shield
Headgear
Linen Robe
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+1, Charge+4, Charge+5, Charge+7

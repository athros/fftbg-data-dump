Player: !Green
Team: Green Team
Palettes: Green/White



MachFighterG
Female
Cancer
76
58
Time Mage
Talk Skill
Critical Quick
Magic Defense UP
Swim

Battle Folio

Golden Hairpin
Light Robe
Spike Shoes

Haste, Haste 2, Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time
Solution, Refute



Treapvort
Male
Capricorn
68
64
Squire
Elemental
Caution
Equip Armor
Waterwalking

Gastrafitis

Diamond Helmet
Silk Robe
Defense Ring

Dash, Heal, Tickle
Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm



Boyosif
Female
Gemini
49
39
Lancer
Summon Magic
Auto Potion
Equip Knife
Jump+3

Main Gauche
Genji Shield
Diamond Helmet
Genji Armor
Salty Rage

Level Jump2, Vertical Jump2
Moogle, Ramuh, Golem, Salamander, Fairy, Lich



Zachara
Female
Libra
75
65
Ninja
Talk Skill
Mana Shield
Equip Shield
Ignore Height

Flail
Bronze Shield
Leather Hat
Black Costume
Defense Armlet

Shuriken, Hammer, Staff
Threaten

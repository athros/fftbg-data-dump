Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



HysteriaDays
Male
Pisces
71
43
Ninja
Summon Magic
Dragon Spirit
Equip Gun
Jump+2

Blaze Gun
Blaze Gun
Golden Hairpin
Mystic Vest
Magic Ring

Shuriken
Moogle, Shiva, Ifrit, Carbunkle



LanseDM
Female
Pisces
44
76
Geomancer
Draw Out
Auto Potion
Equip Armor
Ignore Height

Sleep Sword
Diamond Shield
Iron Helmet
Linen Cuirass
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind
Murasame, Kikuichimoji



MrUbiq
Female
Aries
43
77
Wizard
Jump
Auto Potion
Short Charge
Move-HP Up

Thunder Rod

Thief Hat
Linen Robe
Germinas Boots

Fire 3, Fire 4, Bolt, Ice 4, Frog, Death, Flare
Level Jump8, Vertical Jump8



NoxeGS
Male
Gemini
43
43
Summoner
Elemental
Mana Shield
Halve MP
Fly

Flame Rod

Red Hood
Clothes
Feather Boots

Moogle, Shiva, Ramuh, Golem, Bahamut, Odin, Leviathan, Fairy
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind

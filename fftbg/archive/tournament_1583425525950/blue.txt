Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JonnyCue
Female
Cancer
58
75
Knight
Item
Abandon
Equip Polearm
Fly

Spear
Mythril Shield
Barbuta
Chameleon Robe
Defense Ring

Shield Break, Weapon Break, Magic Break, Stasis Sword, Justice Sword, Dark Sword
Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



PlatinumPlume
Female
Libra
74
46
Calculator
Yin Yang Magic
Counter Magic
Beastmaster
Lava Walking

Gokuu Rod

Cachusha
Linen Robe
Germinas Boots

Height, 5, 3
Poison, Paralyze



Antifalockhart
Monster
Leo
69
75
Pisco Demon










Lanshaft
Male
Leo
59
60
Time Mage
Steal
Faith Up
Sicken
Ignore Terrain

Gold Staff

Thief Hat
Chameleon Robe
108 Gems

Haste, Slow, Slow 2, Immobilize, Demi, Stabilize Time
Steal Heart

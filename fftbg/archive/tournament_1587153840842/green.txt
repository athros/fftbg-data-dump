Player: !Green
Team: Green Team
Palettes: Green/White



Nifboy
Monster
Aries
80
59
Minotaur










Galkife
Female
Sagittarius
69
50
Wizard
Throw
Auto Potion
Defend
Ignore Terrain

Cultist Dagger

Headgear
Silk Robe
Magic Gauntlet

Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower
Bomb



Spartan Paladin
Male
Leo
56
72
Knight
Item
Counter
Secret Hunt
Jump+1

Giant Axe
Platinum Shield
Bronze Helmet
Linen Cuirass
Small Mantle

Weapon Break, Magic Break, Speed Break, Mind Break
Potion, Ether, Antidote, Phoenix Down



Tougou
Male
Gemini
64
57
Archer
Summon Magic
Faith Up
Dual Wield
Move+2

Mythril Gun
Blaze Gun
Barbuta
Black Costume
Diamond Armlet

Charge+4, Charge+5
Moogle, Ifrit, Titan, Salamander

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Dynasti
Female
Aries
76
65
Squire
Item
Brave Up
Throw Item
Move-HP Up

Dagger
Mythril Shield
Green Beret
Mythril Vest
Spike Shoes

Heal, Tickle, Cheer Up
Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Hyvi
Female
Libra
73
71
Archer
Item
Counter Flood
Doublehand
Move-MP Up

Mythril Gun

Diamond Helmet
Judo Outfit
Feather Boots

Charge+5, Charge+10, Charge+20
Potion, X-Potion, Antidote, Echo Grass, Remedy, Phoenix Down



Roqqqpsi
Female
Gemini
69
50
Chemist
Yin Yang Magic
Counter Magic
Equip Sword
Waterwalking

Ragnarok

Headgear
Mystic Vest
Defense Armlet

Potion, Hi-Potion, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Poison, Pray Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze, Petrify, Dark Holy



Jordache7K
Female
Aries
44
65
Summoner
Yin Yang Magic
Auto Potion
Long Status
Levitate

Poison Rod

Red Hood
Clothes
Jade Armlet

Moogle, Shiva, Ramuh, Golem, Carbunkle, Leviathan, Silf, Fairy
Poison, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze, Petrify

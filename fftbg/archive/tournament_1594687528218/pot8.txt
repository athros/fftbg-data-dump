Final Bets: brown - 21 bets for 30,251G (66.0%, x0.51); champion - 16 bets for 15,560G (34.0%, x1.94)

brown bets:
Evewho: 10,350G (34.2%, 20,296G)
Lydian_C: 5,600G (18.5%, 97,204G)
Rislyeu: 2,918G (9.6%, 5,836G)
DamnThatShark: 2,250G (7.4%, 2,250G)
GRRR12: 1,066G (3.5%, 1,066G)
SephDarkheart: 1,000G (3.3%, 23,658G)
dogsandcatsand: 908G (3.0%, 2,444G)
Spartan_Paladin: 800G (2.6%, 44,870G)
killth3kid: 700G (2.3%, 104,403G)
Chanar_ejin: 700G (2.3%, 2,811G)
BoneMiser: 600G (2.0%, 600G)
Mushufasa_: 600G (2.0%, 3,935G)
BlueAbs: 533G (1.8%, 533G)
XshieselX: 513G (1.7%, 513G)
NIghtdew14: 460G (1.5%, 460G)
Xoomwaffle: 407G (1.3%, 407G)
Lemonjohns: 300G (1.0%, 3,838G)
datadrivenbot: 200G (0.7%, 57,826G)
silentkaster: 134G (0.4%, 18,834G)
skipsandwiches: 112G (0.4%, 112G)
Kellios11: 100G (0.3%, 31,004G)

champion bets:
EnemyController: 4,000G (25.7%, 1,424,019G)
DeathTaxesAndAnime: 2,050G (13.2%, 4,021G)
ValtonZenola: 1,879G (12.1%, 1,879G)
TasisSai: 1,181G (7.6%, 1,181G)
twelfthrootoftwo: 1,004G (6.5%, 1,004G)
BirbBrainsBot: 1,000G (6.4%, 131,222G)
getthemoneyz: 1,000G (6.4%, 1,276,791G)
NicoSavoy: 1,000G (6.4%, 182,439G)
Laserman1000: 761G (4.9%, 31,261G)
vorap: 500G (3.2%, 126,248G)
Zbgs: 448G (2.9%, 448G)
turbn: 236G (1.5%, 236G)
gorgewall: 201G (1.3%, 5,956G)
fluffskull: 100G (0.6%, 14,373G)
nhammen: 100G (0.6%, 9,626G)
ANFz: 100G (0.6%, 42,137G)

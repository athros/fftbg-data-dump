Player: !Red
Team: Red Team
Palettes: Red/Brown



Just Here2
Male
Sagittarius
61
55
Lancer
Sing
Brave Save
Equip Sword
Ignore Height

Iron Sword
Diamond Shield
Bronze Helmet
Leather Armor
Red Shoes

Level Jump4, Vertical Jump8
Angel Song, Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade, Space Storage, Sky Demon



Lazarus DS
Monster
Cancer
70
63
Bomb










Gorgewall
Male
Scorpio
56
79
Squire
Summon Magic
Critical Quick
Equip Polearm
Jump+2

Gokuu Rod
Flame Shield
Feather Hat
Bronze Armor
Chantage

Heal, Yell, Cheer Up, Wish, Scream
Moogle, Shiva, Ramuh, Salamander, Fairy, Lich, Cyclops



Skipsandwiches
Female
Libra
75
50
Thief
Charge
Meatbone Slash
Short Charge
Levitate

Diamond Sword

Green Beret
Adaman Vest
Salty Rage

Steal Shield, Steal Weapon, Steal Status, Arm Aim
Charge+1, Charge+4, Charge+7, Charge+10

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



NovaKnight21
Female
Taurus
74
72
Calculator
White Magic
Distribute
Equip Sword
Waterbreathing

Materia Blade

Red Hood
Black Costume
Defense Ring

CT, Height, Prime Number, 5, 3
Raise, Raise 2



AdaephonD
Male
Taurus
44
53
Ninja
White Magic
Catch
Magic Attack UP
Waterbreathing

Star Bag
Hidden Knife
Triangle Hat
Chain Vest
Sprint Shoes

Shuriken, Bomb, Hammer
Cure 3, Cure 4, Raise, Raise 2, Shell, Wall



Ko2q
Female
Serpentarius
52
50
Chemist
White Magic
Auto Potion
Equip Shield
Ignore Terrain

Hydra Bag
Aegis Shield
Feather Hat
Wizard Outfit
Power Wrist

Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Cure, Cure 3, Protect, Protect 2, Esuna, Holy



Gage
Male
Leo
46
49
Wizard
Draw Out
Sunken State
Defense UP
Lava Walking

Panther Bag

Headgear
Linen Robe
108 Gems

Fire, Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4
Asura, Bizen Boat, Kikuichimoji

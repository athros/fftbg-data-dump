Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lythe Caraker
Male
Taurus
67
78
Mediator
Sing
MA Save
Short Charge
Waterwalking

Battle Folio

Black Hood
Chameleon Robe
Leather Mantle

Invitation, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Hydra Pit



Lunchboxtac
Male
Leo
51
66
Archer
Battle Skill
Counter Magic
Equip Polearm
Retreat

Holy Lance
Flame Shield
Thief Hat
Adaman Vest
Defense Armlet

Charge+2, Charge+3, Charge+5, Charge+7, Charge+10
Weapon Break, Speed Break, Power Break, Mind Break



Mirapoix
Monster
Aries
66
59
Gobbledeguck










RunicMagus
Female
Taurus
49
47
Time Mage
Elemental
Counter Magic
Equip Bow
Levitate

Poison Bow

Feather Hat
Chameleon Robe
Bracer

Slow, Immobilize, Quick, Demi, Demi 2, Stabilize Time
Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball

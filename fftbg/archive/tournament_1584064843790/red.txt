Player: !Red
Team: Red Team
Palettes: Red/Brown



Kingchadking
Male
Serpentarius
66
39
Monk
Charge
Abandon
Defend
Move+3



Black Hood
Power Sleeve
Genji Gauntlet

Spin Fist, Revive, Seal Evil
Charge+1, Charge+2, Charge+5, Charge+7



Black Sheep 0213
Female
Scorpio
50
54
Dancer
Black Magic
Absorb Used MP
Equip Knife
Lava Walking

Assassin Dagger

Twist Headband
Chain Vest
Rubber Shoes

Witch Hunt, Nameless Dance, Last Dance, Obsidian Blade, Nether Demon
Fire 4, Bolt 3, Bolt 4, Ice, Ice 3, Empower



Deathmaker06
Female
Capricorn
54
44
Knight
Draw Out
Brave Up
Equip Armor
Move-HP Up

Defender

Cachusha
Diamond Armor
Jade Armlet

Head Break, Shield Break, Night Sword
Asura, Koutetsu, Bizen Boat



HaateXIII
Female
Aquarius
78
45
Oracle
Throw
Catch
Attack UP
Teleport

Cypress Rod

Headgear
Brigandine
Germinas Boots

Poison, Spell Absorb, Zombie, Silence Song, Blind Rage, Sleep
Axe

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lijarkh
Male
Libra
38
51
Monk
Talk Skill
Auto Potion
Short Status
Waterbreathing



Flash Hat
Wizard Outfit
Germinas Boots

Spin Fist, Chakra
Persuade, Preach, Solution, Insult



Slowbrofist
Male
Aries
50
69
Knight
Summon Magic
Dragon Spirit
Doublehand
Jump+2

Platinum Sword

Circlet
Light Robe
Bracer

Head Break, Magic Break
Moogle, Shiva, Ramuh, Fairy, Cyclops



Zagorsek
Male
Aquarius
52
44
Samurai
Summon Magic
Sunken State
Martial Arts
Move+3



Genji Helmet
Light Robe
Cursed Ring

Koutetsu, Murasame, Kikuichimoji
Ramuh, Titan, Salamander, Lich



Godkone
Female
Sagittarius
70
62
Priest
Draw Out
PA Save
Martial Arts
Waterwalking



Feather Hat
White Robe
Reflect Ring

Cure, Cure 2, Cure 3, Raise, Reraise, Protect, Protect 2, Shell 2, Esuna
Koutetsu, Heaven's Cloud

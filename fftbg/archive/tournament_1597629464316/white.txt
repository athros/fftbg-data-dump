Player: !White
Team: White Team
Palettes: White/Blue



Drusiform
Male
Sagittarius
44
57
Samurai
Black Magic
Critical Quick
Magic Defense UP
Retreat

Heaven's Cloud

Leather Helmet
Light Robe
Feather Mantle

Asura, Bizen Boat
Ice, Ice 3



VolgraTheMoose
Male
Taurus
57
47
Thief
Charge
Hamedo
Dual Wield
Teleport

Main Gauche
Ninja Edge
Ribbon
Leather Outfit
Defense Ring

Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Arm Aim
Charge+5, Charge+10



Ko2q
Male
Scorpio
51
62
Thief
Basic Skill
Parry
Secret Hunt
Move+3

Hidden Knife

Flash Hat
Clothes
Wizard Mantle

Steal Shield, Steal Weapon
Dash, Heal, Yell, Cheer Up



Daveb
Male
Libra
80
77
Ninja
Basic Skill
Parry
Equip Armor
Move+2

Air Knife
Air Knife
Mythril Helmet
Silk Robe
Angel Ring

Dictionary
Dash, Throw Stone, Heal, Yell

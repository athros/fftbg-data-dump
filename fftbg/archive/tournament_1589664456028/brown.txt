Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sairentozon7
Monster
Aries
51
79
Archaic Demon










DoublePrizes
Female
Capricorn
80
65
Chemist
Dance
Regenerator
Equip Knife
Ignore Height

Main Gauche

Feather Hat
Power Sleeve
Angel Ring

Hi-Potion, Echo Grass, Soft, Phoenix Down
Witch Hunt, Wiznaibus, Nameless Dance, Last Dance



Sinnyil2
Female
Aries
44
72
Squire
Item
Meatbone Slash
Magic Attack UP
Move-MP Up

Giant Axe
Genji Shield
Mythril Helmet
Crystal Mail
Germinas Boots

Tickle, Cheer Up, Fury, Wish, Scream
Ether, Soft, Remedy, Phoenix Down



Kohlingen
Female
Aries
45
63
Monk
Dance
Sunken State
Equip Shield
Move-HP Up

Panther Bag
Crystal Shield
Golden Hairpin
Adaman Vest
Magic Ring

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Void Storage

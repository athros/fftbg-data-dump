Player: !Black
Team: Black Team
Palettes: Black/Red



IndecisiveNinja
Male
Cancer
44
51
Lancer
Time Magic
HP Restore
Equip Knife
Fly

Rod
Mythril Shield
Barbuta
Carabini Mail
Leather Mantle

Level Jump5, Vertical Jump8
Immobilize, Reflect, Stabilize Time



Chokestomp
Male
Virgo
52
65
Thief
Throw
Caution
Magic Defense UP
Jump+3

Ninja Edge

Headgear
Rubber Costume
Diamond Armlet

Steal Heart, Steal Status, Leg Aim
Shuriken



Lowlf
Monster
Cancer
56
75
Reaper










Nickyfive
Female
Capricorn
80
64
Calculator
Robot Skill
Parry
Equip Armor
Fly

Dragon Rod

Cross Helmet
Crystal Mail
Small Mantle

Blue Magic
Destroy, Compress, Dispose, Repair

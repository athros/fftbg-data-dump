Player: !Red
Team: Red Team
Palettes: Red/Brown



Basmal
Male
Capricorn
78
50
Geomancer
Item
Faith Up
Magic Attack UP
Swim

Battle Axe
Genji Shield
Green Beret
Black Robe
Germinas Boots

Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball
Potion, Ether, Antidote, Holy Water, Remedy, Phoenix Down



Sairentozon7
Male
Pisces
74
54
Lancer
Elemental
Counter Tackle
Equip Bow
Move-MP Up

Cross Bow
Ice Shield
Iron Helmet
Gold Armor
Leather Mantle

Level Jump4, Vertical Jump6
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



BaronHaynes
Male
Leo
79
78
Oracle
Time Magic
Speed Save
Secret Hunt
Move+2

Battle Bamboo

Golden Hairpin
Chain Vest
Magic Gauntlet

Blind, Spell Absorb, Life Drain, Zombie, Foxbird, Confusion Song, Paralyze
Haste, Slow 2, Immobilize, Reflect, Quick, Demi 2, Stabilize Time



Gogovachi
Female
Taurus
66
73
Samurai
Elemental
Hamedo
Equip Knife
Teleport

Spell Edge

Diamond Helmet
Gold Armor
Dracula Mantle

Muramasa, Kikuichimoji
Pitfall, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Gusty Wind, Lava Ball

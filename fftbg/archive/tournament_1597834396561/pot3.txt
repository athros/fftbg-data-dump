Final Bets: white - 7 bets for 2,775G (44.8%, x1.23); black - 7 bets for 3,424G (55.2%, x0.81)

white bets:
Sairentozon7: 1,000G (36.0%, 25,969G)
iBardic: 678G (24.4%, 678G)
WhiteTigress: 300G (10.8%, 5,849G)
AllInBot: 296G (10.7%, 296G)
datadrivenbot: 200G (7.2%, 64,509G)
ar_tactic: 200G (7.2%, 79,504G)
gorgewall: 101G (3.6%, 6,441G)

black bets:
getthemoneyz: 1,000G (29.2%, 1,669,660G)
BirbBrainsBot: 1,000G (29.2%, 52,111G)
Willjin: 1,000G (29.2%, 24,227G)
DouglasDragonThePoet: 200G (5.8%, 804G)
Lydian_C: 123G (3.6%, 3,823G)
MinBetBot: 100G (2.9%, 6,684G)
SephDarkheart: 1G (0.0%, 112,849G)

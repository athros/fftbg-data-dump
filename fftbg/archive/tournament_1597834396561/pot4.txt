Final Bets: purple - 7 bets for 4,243G (59.1%, x0.69); brown - 8 bets for 2,938G (40.9%, x1.44)

purple bets:
BirbBrainsBot: 1,000G (23.6%, 51,111G)
Sairentozon7: 1,000G (23.6%, 27,203G)
Songdeh: 735G (17.3%, 735G)
SephDarkheart: 600G (14.1%, 112,848G)
iBardic: 515G (12.1%, 515G)
getthemoneyz: 292G (6.9%, 1,668,660G)
gorgewall: 101G (2.4%, 6,566G)

brown bets:
Willjin: 1,000G (34.0%, 23,227G)
AllInBot: 661G (22.5%, 661G)
DouglasDragonThePoet: 604G (20.6%, 604G)
datadrivenbot: 200G (6.8%, 64,756G)
VolgraTheMoose: 150G (5.1%, 2,728G)
Lydian_C: 123G (4.2%, 3,700G)
MinBetBot: 100G (3.4%, 6,584G)
ko2q: 100G (3.4%, 4,671G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



SolarisFall
Male
Capricorn
66
64
Chemist
Sing
Sunken State
Equip Shield
Jump+1

Star Bag
Crystal Shield
Golden Hairpin
Brigandine
Magic Ring

Potion, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Last Song, Space Storage



LDSkinny
Female
Scorpio
70
63
Priest
Charge
Parry
Short Charge
Jump+1

Scorpion Tail

Green Beret
Earth Clothes
Germinas Boots

Cure 4, Raise, Protect, Shell, Shell 2, Esuna, Holy
Charge+1, Charge+3, Charge+10



Coralreeferz
Male
Pisces
54
46
Monk
Draw Out
Counter
Sicken
Move-HP Up



Feather Hat
Power Sleeve
Germinas Boots

Purification, Revive
Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa, Chirijiraden



Friendsquirrel
Female
Aquarius
44
70
Archer
Yin Yang Magic
Mana Shield
Equip Bow
Levitate

Night Killer
Crystal Shield
Flash Hat
Leather Outfit
Magic Gauntlet

Charge+2, Charge+3, Charge+4, Charge+5, Charge+10
Blind, Poison, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic

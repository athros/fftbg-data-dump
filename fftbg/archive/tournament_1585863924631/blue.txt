Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



IndecisiveNinja
Female
Taurus
52
69
Priest
Item
Distribute
Equip Shield
Jump+3

Healing Staff
Kaiser Plate
Triangle Hat
Silk Robe
Diamond Armlet

Cure 3, Raise, Raise 2, Esuna
X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



DeathTaxesAndAnime
Female
Libra
79
79
Priest
Charge
Absorb Used MP
Concentrate
Move-HP Up

Morning Star

Black Hood
Robe of Lords
Cursed Ring

Raise 2, Regen, Wall, Esuna
Charge+1, Charge+3, Charge+5



Slowbrofist
Female
Sagittarius
60
68
Priest
Black Magic
Sunken State
Equip Bow
Waterwalking

Long Bow

Flash Hat
Robe of Lords
Feather Mantle

Cure, Cure 3, Raise, Protect 2, Shell, Shell 2, Wall, Esuna
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Empower



Fenaen
Female
Cancer
67
71
Chemist
Charge
Auto Potion
Equip Armor
Ignore Terrain

Zorlin Shape

Genji Helmet
Gold Armor
Power Wrist

Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Charge+1, Charge+2, Charge+7

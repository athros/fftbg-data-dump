Player: !White
Team: White Team
Palettes: White/Blue



Ko2q
Male
Gemini
80
68
Geomancer
Summon Magic
Faith Up
Equip Axe
Jump+2

Battle Axe
Round Shield
Green Beret
Light Robe
108 Gems

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander, Silf, Fairy, Lich



ValtonZenola
Male
Aries
56
74
Geomancer
Jump
Catch
Dual Wield
Fly

Slasher
Muramasa
Holy Miter
Silk Robe
Dracula Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Level Jump5, Vertical Jump8



Bad1dea
Male
Capricorn
77
56
Time Mage
Draw Out
Regenerator
Short Charge
Move-HP Up

Oak Staff

Barette
Wizard Robe
Bracer

Haste, Haste 2, Float, Reflect, Quick
Koutetsu, Bizen Boat



Sociallubricant1
Male
Leo
71
49
Mime

Critical Quick
Doublehand
Move+1



Headgear
Chain Vest
Magic Ring

Mimic


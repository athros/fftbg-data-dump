Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Blorpy
Male
Aries
49
73
Mime

Absorb Used MP
Monster Talk
Move+3



Golden Hairpin
Clothes
N-Kai Armlet

Mimic




WhiteTigress
Male
Virgo
70
69
Oracle
Draw Out
MA Save
Dual Wield
Ignore Terrain

Cypress Rod
Ivory Rod
Thief Hat
White Robe
Defense Ring

Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Dark Holy
Koutetsu, Muramasa



ALY327
Male
Leo
74
43
Geomancer
Sing
Counter
Short Charge
Fly

Giant Axe
Buckler
Holy Miter
Silk Robe
Battle Boots

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Life Song, Cheer Song, Battle Song, Last Song, Hydra Pit



Chuckolator
Male
Pisces
76
57
Mediator
Steal
Counter
Dual Wield
Jump+3

Romanda Gun
Glacier Gun
Flash Hat
Leather Outfit
108 Gems

Threaten, Solution, Refute
Gil Taking, Steal Armor, Steal Accessory, Steal Status

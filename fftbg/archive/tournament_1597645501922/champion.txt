Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Victoriolue
Female
Virgo
48
75
Calculator
Black Magic
HP Restore
Equip Axe
Move+3

Healing Staff

Black Hood
Chameleon Robe
Feather Boots

CT, Height, 4, 3
Fire, Fire 2, Bolt, Ice, Ice 4



Redmage4evah
Male
Capricorn
42
44
Priest
Throw
Faith Save
Attack UP
Retreat

Rainbow Staff

Flash Hat
Linen Robe
Reflect Ring

Cure, Protect, Shell 2, Esuna
Axe



Evdoggity
Female
Scorpio
64
53
Chemist
White Magic
Abandon
Concentrate
Move+1

Air Knife

Black Hood
Mythril Vest
108 Gems

X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down
Raise, Protect 2, Shell 2, Esuna



Resjudicata3
Female
Pisces
61
54
Mime

Abandon
Dual Wield
Move+3



Green Beret
Secret Clothes
Feather Mantle

Mimic


Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Female
Aries
55
47
Oracle
Punch Art
Abandon
Martial Arts
Waterwalking

Battle Bamboo

Thief Hat
Adaman Vest
N-Kai Armlet

Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze
Wave Fist, Purification, Chakra, Revive



Lastly
Female
Capricorn
67
72
Geomancer
Talk Skill
Distribute
Defense UP
Jump+2

Muramasa
Diamond Shield
Twist Headband
Wizard Robe
Diamond Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Praise, Insult, Negotiate, Refute



Eudes89
Female
Capricorn
67
73
Wizard
Summon Magic
Auto Potion
Short Status
Swim

Wizard Rod

Leather Hat
Linen Robe
Magic Gauntlet

Fire 2, Bolt 3, Ice, Ice 3, Frog, Death, Flare
Moogle, Shiva, Titan, Carbunkle, Odin, Leviathan, Fairy



Just Here2
Male
Sagittarius
72
76
Summoner
Item
Auto Potion
Dual Wield
Levitate

Dragon Rod
Rainbow Staff
Red Hood
Black Robe
Magic Ring

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Leviathan
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down

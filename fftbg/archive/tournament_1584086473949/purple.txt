Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nickelbank
Female
Virgo
44
61
Samurai
White Magic
Counter Magic
Sicken
Move+2

Murasame

Mythril Helmet
Linen Cuirass
Spike Shoes

Koutetsu, Muramasa
Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect 2, Esuna



RRazza
Female
Virgo
37
39
Ninja
Yin Yang Magic
Auto Potion
Equip Bow
Fly

Hunting Bow
Night Killer
Leather Hat
Brigandine
Spike Shoes

Dictionary
Spell Absorb, Life Drain, Pray Faith, Zombie, Foxbird, Paralyze, Dark Holy



Bryan792
Male
Pisces
80
47
Lancer
Charge
Regenerator
Dual Wield
Move+1

Obelisk
Holy Lance
Crystal Helmet
Genji Armor
N-Kai Armlet

Level Jump2, Vertical Jump4
Charge+2, Charge+3, Charge+4, Charge+5, Charge+7



Orangejuliuscaesar
Female
Libra
79
54
Knight
Item
Critical Quick
Magic Attack UP
Move-HP Up

Defender

Iron Helmet
Mythril Armor
Angel Ring

Shield Break, Speed Break, Power Break, Stasis Sword
Potion, Hi-Potion, Ether, Hi-Ether, Antidote

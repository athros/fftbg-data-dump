Player: !White
Team: White Team
Palettes: White/Blue



NovaKnight21
Male
Scorpio
59
70
Geomancer
Draw Out
Hamedo
Defend
Lava Walking

Giant Axe
Ice Shield
Feather Hat
Mythril Vest
Dracula Mantle

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Murasame, Heaven's Cloud, Muramasa



Meta Five
Male
Pisces
57
37
Calculator
Black Magic
Mana Shield
Beastmaster
Fly

Cute Bag

Red Hood
Mythril Vest
Magic Gauntlet

CT, Prime Number, 4
Fire, Fire 3, Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Death, Flare



BadBlanket
Male
Scorpio
55
39
Wizard
Basic Skill
Mana Shield
Magic Defense UP
Move+1

Rod

Feather Hat
Mystic Vest
Leather Mantle

Fire, Fire 2, Fire 3, Bolt, Flare
Cheer Up, Wish



Lythe Caraker
Female
Scorpio
55
80
Summoner
Steal
PA Save
Magic Attack UP
Jump+2

Rod

Feather Hat
Mystic Vest
Elf Mantle

Ifrit, Carbunkle, Odin, Silf, Fairy, Lich
Steal Heart, Steal Armor, Steal Shield, Steal Weapon

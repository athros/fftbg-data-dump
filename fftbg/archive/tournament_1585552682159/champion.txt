Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



KMCHurricane
Female
Libra
58
38
Lancer
Punch Art
Abandon
Long Status
Move+3

Dragon Whisker
Buckler
Genji Helmet
Chain Mail
Elf Mantle

Level Jump8, Vertical Jump8
Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil



Chuckolator
Monster
Taurus
58
65
Bull Demon










Zetchryn
Male
Scorpio
73
44
Lancer
Battle Skill
Blade Grasp
Equip Sword
Retreat

Ice Brand
Diamond Shield
Mythril Helmet
Linen Robe
Elf Mantle

Level Jump8, Vertical Jump2
Shield Break, Magic Break, Power Break, Mind Break, Justice Sword



ThanatosXRagnarok
Female
Cancer
69
66
Archer
Time Magic
Counter
Short Charge
Jump+2

Windslash Bow

Red Hood
Clothes
Diamond Armlet

Charge+1, Charge+3, Charge+5, Charge+7
Haste, Slow 2, Stop, Immobilize, Float, Demi 2, Stabilize Time

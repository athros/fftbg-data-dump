Player: !Green
Team: Green Team
Palettes: Green/White



Hydroshade
Female
Taurus
74
52
Mediator
Yin Yang Magic
Brave Up
Dual Wield
Swim

Romanda Gun
Glacier Gun
Holy Miter
Black Costume
Leather Mantle

Invitation, Persuade, Threaten, Death Sentence, Refute
Spell Absorb, Life Drain, Pray Faith, Blind Rage, Dispel Magic, Sleep



DustBirdEX
Female
Libra
71
72
Samurai
Black Magic
Dragon Spirit
Dual Wield
Jump+1

Bizen Boat
Muramasa
Diamond Helmet
Chameleon Robe
Power Wrist

Asura, Murasame, Heaven's Cloud
Bolt 2, Bolt 3, Ice 3



Forsakenundergroundkings
Male
Scorpio
63
49
Mediator
Jump
Abandon
Magic Attack UP
Fly

Battle Folio

Red Hood
Mystic Vest
Rubber Shoes

Invitation, Negotiate, Refute
Level Jump8, Vertical Jump8



Lionhermit
Male
Gemini
58
49
Thief
Jump
Regenerator
Dual Wield
Jump+1

Spell Edge
Koga Knife
Headgear
Judo Outfit
Red Shoes

Steal Heart, Steal Helmet, Steal Shield, Arm Aim
Level Jump5, Vertical Jump8

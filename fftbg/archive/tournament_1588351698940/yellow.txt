Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Vampyreinabox
Female
Scorpio
37
69
Wizard
Charge
Parry
Beastmaster
Jump+2

Wizard Rod

Thief Hat
Mystic Vest
Battle Boots

Bolt, Bolt 3, Bolt 4, Ice
Charge+4, Charge+5, Charge+7, Charge+10



SaintOmerville
Female
Taurus
64
75
Monk
Summon Magic
MA Save
Equip Knife
Move+3

Dragon Rod

Red Hood
Clothes
Reflect Ring

Purification, Chakra, Seal Evil
Shiva, Ifrit, Titan, Golem, Carbunkle, Odin



Mesmaster
Male
Capricorn
74
45
Mime

Brave Up
Martial Arts
Waterwalking



Twist Headband
Chain Vest
108 Gems

Mimic




Vampire Killer
Male
Virgo
42
52
Oracle
Black Magic
Counter Tackle
Magic Attack UP
Waterwalking

Papyrus Codex

Holy Miter
Chameleon Robe
Feather Boots

Blind, Life Drain, Pray Faith, Paralyze, Sleep
Fire 3, Bolt 2, Bolt 4, Ice, Ice 3, Empower, Frog

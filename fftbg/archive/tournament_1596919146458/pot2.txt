Final Bets: green - 15 bets for 27,190G (75.0%, x0.33); yellow - 17 bets for 9,071G (25.0%, x3.00)

green bets:
nekojin: 16,311G (60.0%, 31,983G)
SkylerBunny: 3,000G (11.0%, 292,091G)
reinoe: 1,000G (3.7%, 142,907G)
Aldrammech: 1,000G (3.7%, 8,827G)
NovaKnight21: 1,000G (3.7%, 3,553G)
ko2q: 1,000G (3.7%, 41,254G)
superdevon1: 758G (2.8%, 37,930G)
gorgewall: 719G (2.6%, 719G)
LAGBOT30000: 715G (2.6%, 3,575G)
pplvee1: 566G (2.1%, 566G)
TasisSai: 500G (1.8%, 17,122G)
Amorial: 216G (0.8%, 216G)
vorap: 204G (0.8%, 204G)
ANFz: 101G (0.4%, 46,169G)
MinBetBot: 100G (0.4%, 17,168G)

yellow bets:
DeathTaxesAndAnime: 2,470G (27.2%, 4,844G)
BirbBrainsBot: 1,000G (11.0%, 96,839G)
getthemoneyz: 1,000G (11.0%, 1,557,489G)
douchetron: 741G (8.2%, 741G)
Laserman1000: 708G (7.8%, 708G)
Drusiform: 557G (6.1%, 557G)
foxfaeez: 489G (5.4%, 489G)
Lemonjohns: 420G (4.6%, 6,987G)
DustBirdEX: 300G (3.3%, 3,597G)
resjudicata3: 280G (3.1%, 280G)
Pbjellytimez: 278G (3.1%, 278G)
Ayntlerz: 228G (2.5%, 228G)
AllInBot: 200G (2.2%, 200G)
Skjodo: 100G (1.1%, 100G)
loveyouallfriends: 100G (1.1%, 43,248G)
northkoreancleetus: 100G (1.1%, 100G)
deegelcr: 100G (1.1%, 115G)

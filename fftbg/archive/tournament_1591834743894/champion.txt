Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Dogsandcatsand
Female
Sagittarius
40
51
Samurai
Time Magic
Faith Save
Equip Armor
Waterbreathing

Holy Lance

Golden Hairpin
Leather Outfit
Spike Shoes

Murasame, Kiyomori, Muramasa, Kikuichimoji
Haste, Slow, Stop, Reflect, Demi 2



Porkchop5158
Female
Capricorn
41
79
Wizard
Battle Skill
HP Restore
Equip Bow
Fly

Hunting Bow

Flash Hat
Judo Outfit
Germinas Boots

Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 2
Head Break, Magic Break, Mind Break, Justice Sword



E Ballard
Female
Scorpio
78
73
Calculator
White Magic
Mana Shield
Equip Knife
Waterwalking

Spell Edge

Triangle Hat
Light Robe
Spike Shoes

CT, Prime Number, 5, 3
Cure, Raise, Reraise, Regen, Shell, Wall, Esuna, Holy



Kaelsun
Male
Cancer
62
71
Lancer
Battle Skill
Auto Potion
Maintenance
Move+2

Spear
Escutcheon
Mythril Helmet
Genji Armor
Magic Ring

Level Jump8, Vertical Jump8
Magic Break

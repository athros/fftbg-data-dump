Player: !Green
Team: Green Team
Palettes: Green/White



NoNotBees
Female
Sagittarius
44
52
Chemist
Throw
Distribute
Equip Knife
Jump+3

Wizard Rod

Green Beret
Wizard Outfit
Defense Armlet

Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Shuriken, Knife, Wand



HashtagRob
Monster
Gemini
62
58
Floating Eye










Zepi Zza
Male
Capricorn
70
60
Archer
Summon Magic
Parry
Equip Sword
Jump+3

Ancient Sword
Ice Shield
Triangle Hat
Mystic Vest
Leather Mantle

Charge+1, Charge+3, Charge+5, Charge+7
Moogle, Ramuh, Golem, Carbunkle, Odin, Leviathan, Lich



Gooseyourself
Male
Gemini
49
71
Oracle
White Magic
Arrow Guard
Dual Wield
Jump+2

Musk Rod
Gokuu Rod
Green Beret
Black Costume
Power Wrist

Poison, Spell Absorb, Silence Song, Dispel Magic, Paralyze
Cure, Cure 2, Raise, Protect, Protect 2, Shell 2, Esuna

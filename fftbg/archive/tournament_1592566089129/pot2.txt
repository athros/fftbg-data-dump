Final Bets: green - 8 bets for 11,326G (54.8%, x0.82); yellow - 9 bets for 9,327G (45.2%, x1.21)

green bets:
actual_JP: 5,956G (52.6%, 5,956G)
Jeeboheebo: 2,000G (17.7%, 4,089G)
BlackFireUK: 1,000G (8.8%, 39,814G)
Zachara: 768G (6.8%, 109,768G)
powergems: 600G (5.3%, 5,338G)
Nizaha: 501G (4.4%, 16,122G)
ColetteMSLP: 300G (2.6%, 2,119G)
gorgewall: 201G (1.8%, 18,414G)

yellow bets:
bruubarg: 6,709G (71.9%, 6,709G)
BirbBrainsBot: 1,000G (10.7%, 95,716G)
getthemoneyz: 530G (5.7%, 987,139G)
pplvee1: 284G (3.0%, 284G)
DuraiPapers: 250G (2.7%, 2,062G)
Magicandy: 200G (2.1%, 2,924G)
AltimaMantoid: 154G (1.7%, 954G)
IphoneDarkness: 100G (1.1%, 3,253G)
E_Ballard: 100G (1.1%, 8,342G)

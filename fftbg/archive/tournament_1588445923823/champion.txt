Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Arkreaver
Male
Serpentarius
75
77
Monk
Battle Skill
Catch
Sicken
Teleport 2



Feather Hat
Earth Clothes
Bracer

Spin Fist, Wave Fist, Earth Slash, Chakra, Seal Evil
Weapon Break, Magic Break, Speed Break, Justice Sword



Jaritras
Male
Scorpio
62
78
Monk
Talk Skill
Mana Shield
Defend
Retreat



Headgear
Judo Outfit
Germinas Boots

Spin Fist, Earth Slash, Purification, Chakra, Revive
Persuade, Preach, Insult, Refute



Ar Tactic
Monster
Virgo
50
61
Red Chocobo










Mudrockk
Female
Serpentarius
57
45
Squire
White Magic
Counter
Short Charge
Retreat

Mythril Sword
Bronze Shield
Thief Hat
Black Costume
Magic Gauntlet

Throw Stone, Heal, Yell, Cheer Up, Wish
Raise 2, Esuna

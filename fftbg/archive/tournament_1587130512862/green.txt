Player: !Green
Team: Green Team
Palettes: Green/White



ColetteMSLP
Monster
Leo
72
69
Gobbledeguck










ALY327
Female
Libra
54
53
Chemist
Dance
HP Restore
Martial Arts
Waterbreathing



Red Hood
Mythril Vest
Elf Mantle

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Soft, Phoenix Down
Polka Polka, Last Dance



Joewcarson
Female
Sagittarius
57
80
Ninja
Punch Art
Counter Flood
Equip Gun
Jump+1

Battle Folio
Battle Folio
Thief Hat
Adaman Vest
Elf Mantle

Staff, Dictionary
Pummel, Purification



YaBoy125
Male
Sagittarius
53
66
Knight
Talk Skill
Speed Save
Short Charge
Ignore Height

Slasher
Escutcheon
Genji Helmet
Gold Armor
Angel Ring

Weapon Break, Magic Break, Speed Break, Night Sword
Invitation, Insult, Negotiate

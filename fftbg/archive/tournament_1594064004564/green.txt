Player: !Green
Team: Green Team
Palettes: Green/White



Quadh0nk
Male
Capricorn
57
66
Samurai
Summon Magic
Earplug
Defense UP
Lava Walking

Murasame

Iron Helmet
Linen Robe
Sprint Shoes

Asura, Koutetsu, Bizen Boat, Muramasa
Moogle, Shiva, Titan, Golem, Carbunkle, Silf



Redmage4evah
Male
Scorpio
80
46
Bard
Black Magic
Distribute
Concentrate
Levitate

Fairy Harp

Golden Hairpin
Leather Armor
Jade Armlet

Battle Song, Hydra Pit
Fire 4, Bolt, Bolt 2, Ice 3, Empower, Frog



FriendSquirrel
Female
Scorpio
68
61
Dancer
Steal
Speed Save
Beastmaster
Swim

Cashmere

Leather Hat
Wizard Robe
Power Wrist

Wiznaibus, Disillusion, Nameless Dance, Obsidian Blade
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Status



Fenaen
Female
Leo
69
59
Mediator
Elemental
Mana Shield
Equip Axe
Retreat

Morning Star

Headgear
Adaman Vest
Power Wrist

Persuade, Threaten, Solution, Refute, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

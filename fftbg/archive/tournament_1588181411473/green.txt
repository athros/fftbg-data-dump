Player: !Green
Team: Green Team
Palettes: Green/White



Maakur
Male
Gemini
56
79
Lancer
Battle Skill
Distribute
Equip Axe
Jump+2

Slasher
Gold Shield
Mythril Helmet
Diamond Armor
Sprint Shoes

Level Jump8, Vertical Jump6
Head Break, Power Break, Stasis Sword



Kohlingen
Female
Aries
76
45
Monk
White Magic
MP Restore
Magic Defense UP
Jump+2



Twist Headband
Secret Clothes
Feather Mantle

Wave Fist, Earth Slash, Purification, Revive
Cure 3, Raise, Raise 2, Reraise, Protect, Wall, Esuna, Holy



HaateXIII
Monster
Gemini
73
42
Black Chocobo










Zeroroute
Female
Aries
64
56
Time Mage
Yin Yang Magic
Sunken State
Short Status
Lava Walking

Gold Staff

Flash Hat
White Robe
Jade Armlet

Haste, Demi, Stabilize Time
Silence Song, Blind Rage, Dispel Magic

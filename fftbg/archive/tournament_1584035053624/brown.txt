Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kl0kwurk
Male
Virgo
70
49
Squire
Jump
Absorb Used MP
Dual Wield
Levitate

Zorlin Shape
Battle Axe
Black Hood
Power Sleeve
Wizard Mantle

Throw Stone, Heal, Wish, Scream
Level Jump4, Vertical Jump2



Hzor
Male
Scorpio
77
63
Ninja
Steal
Parry
Equip Armor
Jump+1

Ninja Edge
Hidden Knife
Flash Hat
Linen Cuirass
Elf Mantle

Shuriken, Bomb, Sword
Steal Armor, Steal Status, Leg Aim



Lonfyre
Male
Virgo
73
74
Calculator
Yin Yang Magic
Damage Split
Long Status
Move-HP Up

Ice Rod

Barette
Adaman Vest
Germinas Boots

CT, Height, 5, 4, 3
Blind, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Petrify, Dark Holy



Nizaha
Male
Leo
75
76
Lancer
Time Magic
Sunken State
Secret Hunt
Teleport

Spear
Platinum Shield
Iron Helmet
Crystal Mail
Salty Rage

Level Jump2, Vertical Jump7
Haste 2, Slow, Slow 2, Stop, Float, Stabilize Time

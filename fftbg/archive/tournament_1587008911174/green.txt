Player: !Green
Team: Green Team
Palettes: Green/White



Rinhander
Female
Capricorn
37
75
Mime

Dragon Spirit
Short Status
Move-MP Up



Red Hood
White Robe
Elf Mantle

Mimic




NWOW 44
Female
Libra
64
46
Time Mage
Yin Yang Magic
Counter
Martial Arts
Move+2

Madlemgen

Flash Hat
Chain Vest
Leather Mantle

Slow, Stop, Float, Quick, Demi 2
Blind, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic



DejaPoo21
Female
Aries
81
64
Summoner
Yin Yang Magic
Brave Up
Secret Hunt
Move+3

Wizard Staff

Red Hood
Silk Robe
Feather Mantle

Moogle, Shiva, Ifrit, Golem, Carbunkle, Silf, Fairy
Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic, Petrify



HaateXIII
Male
Aries
69
54
Priest
Steal
Brave Up
Equip Bow
Waterwalking

Night Killer

Triangle Hat
Light Robe
Genji Gauntlet

Cure, Cure 2, Raise, Protect 2, Shell, Wall, Esuna
Steal Armor, Steal Shield, Steal Status

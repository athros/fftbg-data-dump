Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesauceBoss
Female
Scorpio
46
58
Chemist
Time Magic
Auto Potion
Dual Wield
Ignore Terrain

Cute Bag
Star Bag
Feather Hat
Earth Clothes
Angel Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass
Haste, Haste 2, Stop, Immobilize, Quick, Demi, Demi 2



Alecttox
Female
Sagittarius
60
62
Lancer
Basic Skill
Arrow Guard
Equip Bow
Move-HP Up

Long Bow

Diamond Helmet
Chain Mail
Feather Boots

Level Jump8, Vertical Jump8
Accumulate, Heal



Lythe Caraker
Female
Virgo
78
63
Summoner
Throw
MA Save
Equip Axe
Move+2

Flail

Holy Miter
Earth Clothes
Diamond Armlet

Moogle, Ifrit, Golem, Carbunkle, Bahamut, Salamander, Silf, Fairy, Zodiac
Shuriken, Bomb, Stick



Maeveen
Male
Gemini
61
77
Knight
Yin Yang Magic
Catch
Defense UP
Move-MP Up

Ice Brand
Bronze Shield
Gold Helmet
Leather Armor
Bracer

Head Break, Power Break, Mind Break, Justice Sword
Blind, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep

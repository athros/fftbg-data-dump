Player: !Red
Team: Red Team
Palettes: Red/Brown



ALY327
Male
Libra
59
54
Ninja
Battle Skill
Regenerator
Equip Sword
Ignore Height

Ancient Sword
Flail
Green Beret
Judo Outfit
Angel Ring

Bomb, Wand
Head Break, Weapon Break, Mind Break



ArashiKurobara
Male
Pisces
69
58
Squire
Yin Yang Magic
Regenerator
Attack UP
Waterbreathing

Broad Sword
Mythril Shield
Leather Hat
Chain Vest
Germinas Boots

Accumulate, Dash, Throw Stone, Heal, Cheer Up, Fury
Spell Absorb, Doubt Faith, Blind Rage, Confusion Song, Dark Holy



Nifboy
Male
Capricorn
44
65
Ninja
Time Magic
Regenerator
Concentrate
Jump+3

Flail
Flail
Barette
Leather Outfit
Defense Armlet

Shuriken, Bomb, Staff, Stick
Haste, Haste 2, Slow 2, Immobilize, Float, Reflect



DeathTaxesAndAnime
Female
Virgo
50
45
Mime

Dragon Spirit
Equip Shield
Jump+3


Buckler
Flash Hat
Rubber Costume
Power Wrist

Mimic


Player: !Black
Team: Black Team
Palettes: Black/Red



Grininda
Male
Scorpio
54
56
Monk
Item
Counter Tackle
Concentrate
Jump+1



Headgear
Brigandine
Bracer

Spin Fist, Secret Fist, Purification
Potion, Hi-Ether, Antidote, Phoenix Down



DavenIII
Male
Virgo
68
62
Thief
Basic Skill
Mana Shield
Equip Knife
Levitate

Wizard Rod

Leather Hat
Clothes
108 Gems

Steal Shield
Wish



Nhammen
Female
Cancer
49
43
Dancer
Charge
Dragon Spirit
Halve MP
Levitate

Hydra Bag

Golden Hairpin
Silk Robe
Bracer

Wiznaibus, Disillusion, Nameless Dance, Obsidian Blade, Void Storage
Charge+1, Charge+3



SomthingMore
Female
Gemini
52
69
Mediator
Draw Out
Regenerator
Dual Wield
Lava Walking

Romanda Gun
Glacier Gun
Leather Hat
Wizard Robe
N-Kai Armlet

Invitation, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Murasame, Kiyomori

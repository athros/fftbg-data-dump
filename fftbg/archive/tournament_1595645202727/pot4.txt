Final Bets: purple - 14 bets for 9,493G (51.3%, x0.95); brown - 13 bets for 9,021G (48.7%, x1.05)

purple bets:
evontno: 4,000G (42.1%, 40,197G)
DustBirdEX: 1,516G (16.0%, 1,516G)
BirbBrainsBot: 1,000G (10.5%, 50,303G)
Rislyeu: 1,000G (10.5%, 14,545G)
killth3kid: 508G (5.4%, 687G)
getthemoneyz: 294G (3.1%, 1,377,591G)
resjudicata3: 208G (2.2%, 208G)
gorgewall: 201G (2.1%, 15,993G)
datadrivenbot: 200G (2.1%, 40,641G)
ribbiks: 200G (2.1%, 3,519G)
LordMaxoss: 100G (1.1%, 4,172G)
mrpickins: 100G (1.1%, 3,341G)
nhammen: 93G (1.0%, 9,378G)
slotheye17: 73G (0.8%, 147G)

brown bets:
SkylerBunny: 2,000G (22.2%, 66,281G)
Zagorsek: 1,727G (19.1%, 3,455G)
douchetron: 1,401G (15.5%, 2,749G)
thehotrefrigerator: 1,121G (12.4%, 1,121G)
nifboy: 1,000G (11.1%, 31,112G)
galkife: 500G (5.5%, 6,197G)
Hirameki85: 220G (2.4%, 220G)
poGpopE: 212G (2.4%, 212G)
AllInBot: 200G (2.2%, 200G)
Rytor: 200G (2.2%, 23,940G)
scottydeluxe: 198G (2.2%, 198G)
ArlanKels: 142G (1.6%, 2,842G)
WillFitzgerald: 100G (1.1%, 7,882G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SuzakuReii
Female
Virgo
58
73
Ninja
Basic Skill
Dragon Spirit
Equip Gun
Move+1

Battle Folio
Papyrus Codex
Feather Hat
Clothes
Defense Armlet

Knife
Dash, Heal, Tickle



Error72
Male
Cancer
56
72
Mime

Regenerator
Equip Shield
Ignore Terrain


Flame Shield
Twist Headband
Mythril Vest
Dracula Mantle

Mimic




Mudrockk
Female
Aquarius
57
56
Dancer
Draw Out
Catch
Equip Axe
Ignore Terrain

Giant Axe

Feather Hat
Brigandine
Feather Mantle

Witch Hunt, Slow Dance, Polka Polka, Last Dance
Bizen Boat, Kikuichimoji



Lanshaft
Male
Sagittarius
71
77
Mediator
White Magic
Counter Flood
Defense UP
Move-HP Up

Romanda Gun

Twist Headband
Leather Outfit
Jade Armlet

Preach, Solution, Negotiate, Rehabilitate
Cure, Protect, Shell 2

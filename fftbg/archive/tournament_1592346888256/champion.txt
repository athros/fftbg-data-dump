Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Evewho
Female
Virgo
77
48
Samurai
Item
Absorb Used MP
Throw Item
Waterwalking

Holy Lance

Diamond Helmet
Mythril Armor
Wizard Mantle

Bizen Boat, Heaven's Cloud, Kikuichimoji
Potion, Hi-Potion, Hi-Ether, Holy Water, Remedy



Placidphoenix
Male
Taurus
64
72
Summoner
White Magic
Damage Split
Beastmaster
Move-MP Up

Flame Rod

Green Beret
Brigandine
Germinas Boots

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Cyclops
Cure, Cure 3, Cure 4, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna



Sairentozon7
Male
Gemini
60
79
Oracle
Draw Out
Auto Potion
Equip Polearm
Waterbreathing

Holy Lance

Red Hood
Power Sleeve
Feather Mantle

Spell Absorb, Life Drain, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Murasame



Kronikle
Monster
Aquarius
54
64
Blue Dragon








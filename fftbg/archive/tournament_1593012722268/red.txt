Player: !Red
Team: Red Team
Palettes: Red/Brown



E Ballard
Female
Serpentarius
51
64
Lancer
Time Magic
Caution
Equip Axe
Ignore Height

Morning Star
Round Shield
Mythril Helmet
Linen Cuirass
108 Gems

Level Jump2, Vertical Jump8
Haste, Haste 2, Slow 2, Stop, Reflect, Stabilize Time



Nickyfive
Male
Sagittarius
52
80
Monk
Charge
Parry
Defense UP
Ignore Height



Triangle Hat
Clothes
Elf Mantle

Spin Fist, Pummel, Purification, Chakra
Charge+2, Charge+4, Charge+7



Fenaen
Female
Pisces
52
45
Geomancer
Summon Magic
MP Restore
Equip Axe
Lava Walking

Morning Star
Platinum Shield
Black Hood
Chain Vest
Magic Gauntlet

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Moogle, Carbunkle, Cyclops



Actual JP
Female
Aquarius
54
59
Ninja
Black Magic
Sunken State
Halve MP
Move+2

Kunai
Ninja Edge
Black Hood
Adaman Vest
Power Wrist

Ninja Sword, Axe
Bolt, Bolt 4, Ice 2

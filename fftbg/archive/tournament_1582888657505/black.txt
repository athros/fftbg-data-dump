Player: !Black
Team: Black Team
Palettes: Black/Red



HysteriaDays
Male
Capricorn
47
50
Archer
Throw
Caution
Attack UP
Waterbreathing

Bow Gun
Round Shield
Genji Helmet
Mythril Vest
Battle Boots

Charge+1, Charge+3, Charge+7
Shuriken



Typicalfanboy
Female
Aquarius
47
52
Summoner
Punch Art
MP Restore
Short Charge
Levitate

Flame Rod

Thief Hat
Robe of Lords
Genji Gauntlet

Moogle, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Fairy
Wave Fist, Chakra, Revive



Pathogen7
Male
Capricorn
40
57
Oracle
Talk Skill
Mana Shield
Defend
Swim

Battle Bamboo

Headgear
Mythril Vest
Wizard Mantle

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Sleep
Invitation, Insult, Refute



LanseDM
Female
Capricorn
76
57
Chemist
Draw Out
Distribute
Short Status
Move+2

Hydra Bag

Flash Hat
Brigandine
N-Kai Armlet

Potion, Hi-Potion, Eye Drop, Phoenix Down
Asura

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WitchHunterIX
Male
Libra
80
74
Oracle
Elemental
Counter
Magic Defense UP
Swim

Iron Fan

Feather Hat
Light Robe
Elf Mantle

Pray Faith, Foxbird, Dispel Magic, Sleep
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



HASTERIOUS
Male
Gemini
81
56
Lancer
Yin Yang Magic
Distribute
Short Status
Retreat

Octagon Rod
Ice Shield
Leather Helmet
Plate Mail
Angel Ring

Level Jump2, Vertical Jump8
Blind, Poison, Spell Absorb, Pray Faith, Foxbird, Dispel Magic, Sleep, Petrify, Dark Holy



Rilgon
Male
Gemini
80
64
Summoner
Time Magic
Blade Grasp
Beastmaster
Move+3

Oak Staff

Red Hood
Linen Robe
Cherche

Carbunkle, Leviathan
Haste 2, Slow, Stop, Float, Reflect, Stabilize Time



Killth3kid
Male
Scorpio
74
58
Ninja
Draw Out
Brave Save
Long Status
Move+2

Dagger
Mage Masher
Leather Hat
Mystic Vest
Angel Ring

Shuriken, Bomb
Murasame

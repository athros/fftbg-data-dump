Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kaelsun
Female
Virgo
47
50
Chemist
Basic Skill
Dragon Spirit
Dual Wield
Move+1

Hydra Bag
Panther Bag
Golden Hairpin
Wizard Outfit
Magic Gauntlet

Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Accumulate, Heal, Yell, Cheer Up



HaateXIII
Male
Scorpio
61
52
Archer
Basic Skill
Caution
Equip Gun
Jump+3

Ramia Harp
Mythril Shield
Golden Hairpin
Adaman Vest
Bracer

Charge+4, Charge+5, Charge+7
Accumulate, Dash, Heal, Scream



Volgrathemoose
Female
Gemini
71
48
Squire
Draw Out
HP Restore
Equip Polearm
Fly

Holy Lance
Diamond Shield
Black Hood
Black Costume
Dracula Mantle

Dash, Throw Stone, Heal, Tickle
Koutetsu



The Shep
Female
Aries
78
44
Mediator
White Magic
MP Restore
Long Status
Fly

Romanda Gun

Holy Miter
White Robe
Vanish Mantle

Persuade, Preach, Solution, Insult, Negotiate, Refute, Rehabilitate
Cure, Raise, Raise 2, Regen, Shell 2, Wall, Esuna

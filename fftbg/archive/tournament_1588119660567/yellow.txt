Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WrathfulRemy
Monster
Capricorn
45
79
Trent










Anethum
Male
Aries
67
61
Summoner
Item
Counter Magic
Short Charge
Fly

Oak Staff

Twist Headband
Chameleon Robe
108 Gems

Moogle, Ramuh, Titan, Carbunkle, Salamander
Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Fenaen
Male
Pisces
63
40
Knight
Basic Skill
Sunken State
Concentrate
Waterwalking

Broad Sword
Buckler
Iron Helmet
White Robe
Defense Ring

Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword
Dash, Heal, Tickle, Wish



Twelfthrootoftwo
Male
Gemini
70
42
Calculator
Yin Yang Magic
Auto Potion
Equip Sword
Jump+1

Sleep Sword

Thief Hat
Brigandine
Battle Boots

CT, Prime Number, 5, 4, 3
Poison, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Paralyze, Sleep, Petrify, Dark Holy

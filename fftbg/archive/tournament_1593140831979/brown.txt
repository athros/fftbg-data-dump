Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Monster
Libra
40
53
Ghost










Sairentozon7
Male
Scorpio
55
64
Archer
Time Magic
PA Save
Dual Wield
Swim

Lightning Bow

Twist Headband
Brigandine
Power Wrist

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Haste, Slow 2, Immobilize, Reflect, Quick, Demi, Meteor, Galaxy Stop



OpHendoslice
Male
Taurus
58
62
Ninja
Basic Skill
Arrow Guard
Equip Sword
Move-MP Up

Coral Sword
Ancient Sword
Triangle Hat
Clothes
Diamond Armlet

Shuriken, Bomb, Hammer, Wand
Throw Stone, Heal, Yell, Fury, Wish



Ring Wyrm
Female
Virgo
53
56
Geomancer
Talk Skill
Counter
Monster Talk
Levitate

Battle Axe
Escutcheon
Green Beret
Linen Robe
Vanish Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard
Death Sentence, Refute, Rehabilitate

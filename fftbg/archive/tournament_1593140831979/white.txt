Player: !White
Team: White Team
Palettes: White/Blue



DamnThatShark
Male
Cancer
80
70
Squire
Steal
Auto Potion
Equip Armor
Ignore Terrain

Giant Axe
Genji Shield
Platinum Helmet
Wizard Robe
Red Shoes

Heal, Tickle, Yell
Steal Shield, Leg Aim



Reddwind
Male
Cancer
67
51
Knight
Sing
Sunken State
Attack UP
Teleport

Blood Sword
Bronze Shield
Barbuta
White Robe
N-Kai Armlet

Armor Break, Shield Break, Speed Break, Justice Sword, Dark Sword, Explosion Sword
Cheer Song



Moshyhero
Female
Aquarius
63
58
Squire
Elemental
Counter Flood
Equip Bow
Jump+1

Poison Bow
Mythril Shield
Green Beret
Adaman Vest
Feather Mantle

Accumulate, Throw Stone, Heal, Yell, Cheer Up, Wish, Scream
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Gelwain
Female
Capricorn
48
44
Knight
Talk Skill
Parry
Concentrate
Waterbreathing

Giant Axe
Bronze Shield
Iron Helmet
Platinum Armor
Rubber Shoes

Head Break, Power Break, Night Sword
Persuade, Praise, Preach, Solution, Insult, Refute, Rehabilitate

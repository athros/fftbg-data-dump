Final Bets: white - 7 bets for 6,502G (65.4%, x0.53); purple - 9 bets for 3,433G (34.6%, x1.89)

white bets:
roqqqpsi: 2,640G (40.6%, 24,006G)
E_Ballard: 1,565G (24.1%, 1,565G)
VolgraTheMoose: 1,001G (15.4%, 11,894G)
Breakdown777: 889G (13.7%, 889G)
gorgewall: 201G (3.1%, 25,494G)
getthemoneyz: 106G (1.6%, 1,077,072G)
CosmicTactician: 100G (1.5%, 21,777G)

purple bets:
Moshyhero: 749G (21.8%, 749G)
Evewho: 564G (16.4%, 564G)
NIghtdew14: 500G (14.6%, 3,345G)
Chronolynx42: 500G (14.6%, 45,027G)
twelfthrootoftwo: 300G (8.7%, 8,624G)
BirbBrainsBot: 260G (7.6%, 66,988G)
alterworlds: 248G (7.2%, 248G)
cactus_tony_: 212G (6.2%, 212G)
datadrivenbot: 100G (2.9%, 46,635G)

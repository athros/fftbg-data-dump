Player: !White
Team: White Team
Palettes: White/Blue



Lowlf
Male
Sagittarius
60
62
Monk
Charge
Sunken State
Equip Sword
Teleport

Chirijiraden

Holy Miter
Clothes
Diamond Armlet

Purification, Chakra, Revive
Charge+2, Charge+3, Charge+5, Charge+7, Charge+10



Fluffywormhole
Monster
Aquarius
59
81
Squidraken










Dogsandcatsand
Female
Taurus
63
60
Wizard
Item
Damage Split
Long Status
Swim

Blind Knife

Headgear
White Robe
Rubber Shoes

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Ice 3, Empower, Frog
Potion, X-Potion, Hi-Ether, Echo Grass, Phoenix Down



Firesheath
Male
Scorpio
61
43
Ninja
Steal
HP Restore
Equip Sword
Move+1

Kiyomori
Sasuke Knife
Twist Headband
Chain Vest
Jade Armlet

Bomb
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Status

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mpghappiness
Male
Capricorn
70
71
Bard
White Magic
Earplug
Equip Axe
Move+3

Wizard Staff

Red Hood
Brigandine
Magic Gauntlet

Cheer Song, Space Storage, Hydra Pit
Raise, Reraise, Protect 2, Holy



NovaKnight21
Female
Aries
65
77
Dancer
Jump
Regenerator
Equip Gun
Retreat

Blaze Gun

Red Hood
Silk Robe
Defense Ring

Witch Hunt, Polka Polka
Level Jump3, Vertical Jump7



Mirapoix
Female
Cancer
64
57
Mediator
Basic Skill
Catch
Equip Knife
Jump+1

Hidden Knife

Green Beret
Power Sleeve
Jade Armlet

Invitation, Praise, Preach, Insult, Mimic Daravon, Refute, Rehabilitate
Heal, Yell, Fury



TasisSai
Female
Pisces
59
52
Time Mage
Punch Art
Abandon
Equip Bow
Retreat

Snipe Bow

Flash Hat
Robe of Lords
Sprint Shoes

Haste 2, Slow, Immobilize, Demi, Demi 2
Pummel, Secret Fist, Purification, Revive, Seal Evil

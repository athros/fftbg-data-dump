Player: !Green
Team: Green Team
Palettes: Green/White



Nifboy
Female
Sagittarius
55
60
Thief
Basic Skill
Counter
Martial Arts
Move+1

Cultist Dagger

Holy Miter
Mythril Vest
Angel Ring

Gil Taking, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Heal, Tickle, Cheer Up, Fury



ZombiFrancis
Male
Aquarius
41
80
Ninja
White Magic
Parry
Short Charge
Move-HP Up

Sasuke Knife
Dagger
Headgear
Secret Clothes
Rubber Shoes

Bomb, Stick
Cure, Cure 3, Raise, Regen, Protect 2, Shell 2, Wall, Esuna, Holy



DustBirdEX
Female
Capricorn
61
66
Thief
Jump
Counter
Equip Gun
Move+1

Glacier Gun

Golden Hairpin
Black Costume
Defense Armlet

Steal Heart, Steal Armor, Steal Shield
Level Jump3, Vertical Jump8



Gunz232323
Male
Taurus
52
51
Oracle
Charge
Abandon
Defend
Fly

Gokuu Rod

Triangle Hat
White Robe
Red Shoes

Blind, Life Drain, Pray Faith, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy
Charge+1

Player: !Red
Team: Red Team
Palettes: Red/Brown



Happyfundude
Female
Leo
56
50
Summoner
Draw Out
Counter Magic
Magic Attack UP
Teleport

Sage Staff

Red Hood
Wizard Outfit
Jade Armlet

Moogle, Golem, Carbunkle, Leviathan, Salamander
Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa



Evewho
Female
Taurus
69
53
Priest
Draw Out
Caution
Halve MP
Move+3

Oak Staff

Twist Headband
White Robe
Spike Shoes

Cure 2, Shell, Shell 2, Esuna, Magic Barrier
Asura, Koutetsu, Bizen Boat, Murasame



Lionhermit
Male
Aquarius
69
77
Knight
Talk Skill
Abandon
Monster Talk
Jump+2

Defender
Round Shield
Mythril Helmet
Plate Mail
Reflect Ring

Head Break, Armor Break, Magic Break, Power Break, Mind Break, Surging Sword
Invitation, Threaten, Negotiate, Refute



Arch8000
Female
Aquarius
64
76
Knight
Dance
HP Restore
Doublehand
Teleport

Long Sword

Platinum Helmet
Platinum Armor
Rubber Shoes

Head Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Surging Sword
Wiznaibus, Disillusion, Nameless Dance, Void Storage

Player: !White
Team: White Team
Palettes: White/Blue



Artysayres
Male
Taurus
79
47
Thief
Item
MP Restore
Short Charge
Jump+3

Cute Bag

Leather Helmet
Judo Outfit
108 Gems

Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Potion, X-Potion, Ether, Echo Grass, Remedy, Phoenix Down



RughSontos
Female
Virgo
54
61
Oracle
Elemental
MA Save
Defense UP
Lava Walking

Papyrus Codex

Leather Hat
Leather Outfit
Rubber Shoes

Blind, Pray Faith, Doubt Faith, Foxbird, Confusion Song, Paralyze
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball



Raventwynn
Female
Aries
55
64
Knight
Jump
Counter
Dual Wield
Move-MP Up

Slasher
Platinum Sword
Barbuta
Robe of Lords
Battle Boots

Head Break, Shield Break, Weapon Break, Mind Break, Justice Sword, Night Sword, Surging Sword
Level Jump3, Vertical Jump4



Sixstop
Female
Leo
53
47
Oracle
Jump
Arrow Guard
Halve MP
Waterwalking

Bestiary

Holy Miter
Judo Outfit
Bracer

Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Paralyze
Level Jump2, Vertical Jump8

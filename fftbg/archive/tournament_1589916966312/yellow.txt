Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RjA0zcOQ96
Female
Taurus
45
60
Time Mage
Throw
Caution
Long Status
Jump+2

White Staff

Green Beret
Silk Robe
108 Gems

Haste, Slow, Quick, Demi, Stabilize Time
Shuriken



Vorap
Female
Gemini
44
42
Geomancer
Charge
Critical Quick
Equip Knife
Move-MP Up

Hidden Knife
Venetian Shield
Twist Headband
Silk Robe
Cursed Ring

Pitfall, Static Shock, Quicksand, Lava Ball
Charge+1



DudeMonkey77
Female
Aquarius
52
42
Squire
Jump
Critical Quick
Concentrate
Ignore Terrain

Morning Star
Platinum Shield
Headgear
Maximillian
Power Wrist

Heal, Tickle, Scream
Level Jump5, Vertical Jump4



Brokenknight201
Male
Cancer
65
70
Knight
Item
Counter Flood
Throw Item
Levitate

Save the Queen
Round Shield
Grand Helmet
Diamond Armor
Small Mantle

Shield Break, Magic Break, Power Break, Justice Sword
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft

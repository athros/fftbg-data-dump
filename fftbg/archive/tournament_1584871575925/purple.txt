Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ranmilia
Male
Aries
50
60
Priest
Draw Out
Counter
Concentrate
Jump+1

Morning Star

Golden Hairpin
Black Robe
Defense Ring

Cure 2, Raise, Protect, Esuna, Holy
Koutetsu, Murasame, Kiyomori, Kikuichimoji



HaateXIII
Male
Virgo
57
67
Mime

Counter Flood
Long Status
Waterbreathing



Twist Headband
Mystic Vest
Bracer

Mimic




BenYuPoker
Female
Capricorn
56
62
Thief
Item
Abandon
Defend
Jump+2

Main Gauche

Feather Hat
Chain Vest
Cursed Ring

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Soft



Tomion
Female
Gemini
61
45
Mediator
White Magic
Counter Magic
Defense UP
Jump+1

Romanda Gun

Cachusha
Light Robe
Dracula Mantle

Persuade, Solution, Death Sentence, Insult, Negotiate, Refute
Cure 2, Raise, Reraise, Protect 2, Shell 2, Wall, Esuna, Holy

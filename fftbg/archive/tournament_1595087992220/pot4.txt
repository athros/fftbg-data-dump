Final Bets: purple - 11 bets for 6,894G (66.4%, x0.51); brown - 7 bets for 3,495G (33.6%, x1.97)

purple bets:
reinoe: 2,000G (29.0%, 81,056G)
BirbBrainsBot: 1,000G (14.5%, 65,774G)
Xoomwaffle: 874G (12.7%, 874G)
roqqqpsi: 653G (9.5%, 2,970G)
VicViper09: 500G (7.3%, 1,371G)
randgridr: 500G (7.3%, 4,139G)
getthemoneyz: 410G (5.9%, 1,320,156G)
Thyrandaal: 407G (5.9%, 407G)
Treafa: 250G (3.6%, 490G)
datadrivenbot: 200G (2.9%, 32,472G)
Miku_Shikhu: 100G (1.5%, 16,569G)

brown bets:
AllInBot: 1,710G (48.9%, 1,710G)
DeathTaxesAndAnime: 724G (20.7%, 724G)
rockmem21: 400G (11.4%, 8,176G)
ProjectLG: 300G (8.6%, 4,787G)
dinin991: 156G (4.5%, 156G)
VolgraTheMoose: 105G (3.0%, 18,914G)
JiroDoge: 100G (2.9%, 1,383G)

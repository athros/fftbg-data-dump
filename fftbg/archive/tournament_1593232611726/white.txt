Player: !White
Team: White Team
Palettes: White/Blue



Rikrizen
Female
Cancer
51
50
Samurai
Steal
Absorb Used MP
Equip Polearm
Ignore Terrain

Iron Fan

Barbuta
Gold Armor
Cursed Ring

Asura, Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Steal Weapon, Steal Accessory, Steal Status, Arm Aim



Grandlanzer
Monster
Gemini
43
71
Bomb










Cactus Tony
Male
Leo
40
54
Squire
Talk Skill
Counter
Beastmaster
Waterbreathing

Cultist Dagger
Flame Shield
Thief Hat
Bronze Armor
Defense Ring

Accumulate, Cheer Up
Preach, Rehabilitate



Galkife
Female
Sagittarius
46
78
Samurai
Talk Skill
Catch
Short Charge
Move+2

Obelisk

Crystal Helmet
Black Robe
Sprint Shoes

Asura, Koutetsu, Murasame, Masamune
Persuade, Threaten, Preach, Mimic Daravon, Refute, Rehabilitate

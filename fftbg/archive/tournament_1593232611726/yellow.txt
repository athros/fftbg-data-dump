Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



OpHendoslice
Male
Aquarius
46
44
Ninja
Elemental
Counter Flood
Short Status
Jump+1

Flail
Flame Whip
Leather Hat
Mythril Vest
Small Mantle

Staff
Pitfall, Water Ball, Quicksand, Gusty Wind, Lava Ball



Baron Von Scrub
Male
Sagittarius
63
74
Thief
Sing
Earplug
Equip Sword
Ignore Height

Ancient Sword

Headgear
Power Sleeve
Spike Shoes

Gil Taking, Steal Shield, Steal Weapon, Steal Status
Battle Song, Magic Song, Diamond Blade, Space Storage, Hydra Pit



UmaiJam
Male
Leo
45
74
Archer
Throw
Auto Potion
Doublehand
Move+3

Mythril Bow

Twist Headband
Brigandine
Red Shoes

Charge+4, Charge+5
Shuriken



GatsbysGhost
Male
Scorpio
67
71
Archer
Basic Skill
Absorb Used MP
Equip Bow
Jump+3

Panther Bag
Escutcheon
Triangle Hat
Judo Outfit
Elf Mantle

Charge+4, Charge+5
Throw Stone, Tickle, Wish

Player: !White
Team: White Team
Palettes: White/Blue



HysteriaDays
Male
Gemini
53
53
Knight
Steal
Damage Split
Long Status
Waterbreathing

Broad Sword
Bronze Shield
Mythril Helmet
Chain Mail
Defense Armlet

Head Break, Magic Break, Speed Break, Power Break, Surging Sword
Steal Heart, Steal Accessory



Ar Tactic
Male
Libra
73
71
Chemist
Elemental
Speed Save
Martial Arts
Jump+1



Thief Hat
Wizard Outfit
Elf Mantle

Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



XCodes
Monster
Gemini
56
54
Sekhret










Tithonus
Male
Pisces
40
43
Knight
Elemental
Auto Potion
Long Status
Move-MP Up

Defender

Genji Helmet
Crystal Mail
Magic Gauntlet

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball

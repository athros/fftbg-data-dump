Player: !White
Team: White Team
Palettes: White/Blue



Ko2q
Female
Sagittarius
72
77
Samurai
Steal
MA Save
Defense UP
Waterwalking

Kiyomori

Barbuta
Wizard Robe
Sprint Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Accessory



0v3rr8d
Male
Aries
68
77
Lancer
Draw Out
Parry
Doublehand
Jump+3

Spear

Mythril Helmet
Chain Mail
Defense Armlet

Level Jump4, Vertical Jump7
Asura, Koutetsu



CapnSauce420
Male
Leo
45
47
Ninja
Jump
Counter Magic
Secret Hunt
Move-HP Up

Hidden Knife
Hidden Knife
Triangle Hat
Brigandine
Germinas Boots

Bomb, Hammer, Wand
Level Jump2, Vertical Jump8



Flox Silverbow
Male
Scorpio
66
49
Thief
Battle Skill
Earplug
Defend
Jump+3

Blind Knife

Black Hood
Judo Outfit
Bracer

Steal Heart, Steal Shield, Steal Accessory
Head Break, Magic Break, Explosion Sword

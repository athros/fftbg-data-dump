Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Rogueain
Female
Pisces
60
69
Mediator
Dance
Distribute
Equip Shield
Move+2

Papyrus Codex
Round Shield
Green Beret
Silk Robe
Feather Mantle

Invitation, Praise, Threaten, Solution, Insult, Mimic Daravon, Refute
Obsidian Blade



Elelor
Female
Virgo
78
65
Chemist
Jump
Counter Flood
Long Status
Move+3

Panther Bag

Black Hood
Mythril Vest
Jade Armlet

X-Potion, Hi-Ether, Antidote, Echo Grass, Soft
Level Jump8, Vertical Jump2



Hamborn
Female
Pisces
66
56
Squire
Summon Magic
Catch
Concentrate
Move-MP Up

Night Killer
Platinum Shield
Iron Helmet
Genji Armor
N-Kai Armlet

Accumulate, Throw Stone, Heal, Tickle, Fury
Moogle, Golem, Leviathan, Silf, Fairy



Arch8000
Female
Taurus
69
57
Calculator
Limit
Counter Flood
Secret Hunt
Jump+2

Cypress Rod

Cross Helmet
Mythril Vest
Diamond Armlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

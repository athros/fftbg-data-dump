Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Krinku
Female
Serpentarius
64
58
Geomancer
Jump
Auto Potion
Equip Sword
Waterbreathing

Blood Sword
Aegis Shield
Golden Hairpin
Clothes
Small Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm
Level Jump8, Vertical Jump4



Evdoggity
Male
Gemini
50
50
Monk
Elemental
Speed Save
Attack UP
Jump+2



Triangle Hat
Leather Outfit
Dracula Mantle

Chakra, Revive
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp



Drusiform
Female
Scorpio
47
72
Thief
Talk Skill
Sunken State
Equip Knife
Jump+3

Kunai

Golden Hairpin
Adaman Vest
Elf Mantle

Gil Taking, Steal Armor, Steal Shield, Steal Status, Arm Aim, Leg Aim
Death Sentence, Insult, Mimic Daravon



Letdowncity
Female
Taurus
70
45
Calculator
White Magic
Arrow Guard
Dual Wield
Lava Walking

Battle Folio
Bestiary
Triangle Hat
Chameleon Robe
Sprint Shoes

CT, 5
Raise, Reraise, Regen, Shell 2

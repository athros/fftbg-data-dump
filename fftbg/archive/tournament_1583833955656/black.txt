Player: !Black
Team: Black Team
Palettes: Black/Red



RezzaDV
Female
Sagittarius
40
70
Dancer
Basic Skill
Dragon Spirit
Equip Polearm
Teleport

Mythril Spear

Red Hood
Chain Vest
Diamond Armlet

Wiznaibus, Polka Polka, Disillusion, Obsidian Blade, Void Storage
Heal, Yell, Cheer Up, Wish, Ultima



MalakoFox
Male
Libra
59
63
Squire
Time Magic
Parry
Equip Shield
Lava Walking

Battle Axe
Kaiser Plate
Bronze Helmet
Mythril Vest
Red Shoes

Dash, Heal, Tickle
Haste, Float, Stabilize Time



Ar Tactic
Female
Aquarius
76
56
Thief
Elemental
Counter
Equip Gun
Move-MP Up

Blast Gun

Holy Miter
Chain Vest
Cursed Ring

Gil Taking, Steal Armor, Steal Weapon, Steal Status, Arm Aim
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Nachtkrieger
Male
Taurus
51
50
Monk
Talk Skill
MA Save
Dual Wield
Waterbreathing



Red Hood
Wizard Outfit
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive
Invitation, Praise, Preach, Death Sentence, Mimic Daravon

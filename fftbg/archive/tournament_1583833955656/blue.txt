Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaplessOne
Male
Cancer
60
73
Thief
Battle Skill
Meatbone Slash
Sicken
Waterwalking

Rune Blade

Barette
Brigandine
Chantage

Gil Taking, Steal Heart, Steal Status, Leg Aim
Head Break, Surging Sword



Dexef
Male
Aquarius
78
60
Mediator
Charge
Sunken State
Equip Polearm
Waterwalking

Octagon Rod

Golden Hairpin
White Robe
Reflect Ring

Praise, Death Sentence, Refute
Charge+3, Charge+5, Charge+7



WhiteDog420
Female
Cancer
48
52
Knight
Draw Out
Arrow Guard
Defend
Swim

Ragnarok

Gold Helmet
Carabini Mail
Magic Gauntlet

Armor Break, Shield Break, Weapon Break
Koutetsu, Kiyomori, Muramasa



I Nod My Head When I Lose
Male
Capricorn
75
67
Lancer
Punch Art
Catch
Equip Sword
Move-MP Up

Koutetsu Knife
Gold Shield
Iron Helmet
Reflect Mail
Angel Ring

Level Jump2, Vertical Jump5
Spin Fist, Purification, Seal Evil

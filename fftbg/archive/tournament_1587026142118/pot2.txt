Final Bets: green - 4 bets for 5,288G (44.9%, x1.22); yellow - 13 bets for 6,477G (55.1%, x0.82)

green bets:
Lydian_C: 4,200G (79.4%, 60,127G)
Jerboj: 500G (9.5%, 43,239G)
waterwatereverywhere: 356G (6.7%, 356G)
getthemoneyz: 232G (4.4%, 500,909G)

yellow bets:
AzuriteReaction: 1,000G (15.4%, 8,781G)
RionDoesThings: 954G (14.7%, 954G)
Phytik: 859G (13.3%, 8,019G)
BirbBrainsBot: 760G (11.7%, 187,884G)
Jeeboheebo: 687G (10.6%, 687G)
datadrivenbot: 493G (7.6%, 543G)
PotionDweller: 456G (7.0%, 456G)
lijarkh: 404G (6.2%, 404G)
HASTERIOUS: 364G (5.6%, 364G)
Maeveen: 200G (3.1%, 5,912G)
gorgewall: 100G (1.5%, 2,311G)
Treapvort: 100G (1.5%, 3,317G)
WireLord: 100G (1.5%, 5,685G)

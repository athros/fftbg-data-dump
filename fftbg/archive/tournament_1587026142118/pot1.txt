Final Bets: red - 9 bets for 5,753G (54.8%, x0.82); blue - 9 bets for 4,743G (45.2%, x1.21)

red bets:
sinnyil2: 1,200G (20.9%, 26,204G)
HASTERIOUS: 1,014G (17.6%, 1,014G)
BirbBrainsBot: 1,000G (17.4%, 188,884G)
waterwatereverywhere: 835G (14.5%, 835G)
Jerboj: 500G (8.7%, 44,739G)
lijarkh: 404G (7.0%, 404G)
Lydian_C: 400G (7.0%, 60,527G)
thevilmen: 300G (5.2%, 3,531G)
Evewho: 100G (1.7%, 5,498G)

blue bets:
Treapvort: 1,499G (31.6%, 1,499G)
WireLord: 1,000G (21.1%, 4,472G)
Phytik: 840G (17.7%, 8,000G)
RionDoesThings: 431G (9.1%, 431G)
getthemoneyz: 278G (5.9%, 500,572G)
DeathTaxesAndAnime: 250G (5.3%, 2,187G)
datadrivenbot: 245G (5.2%, 246G)
lastly: 100G (2.1%, 12,215G)
Maeveen: 100G (2.1%, 5,791G)

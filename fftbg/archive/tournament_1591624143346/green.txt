Player: !Green
Team: Green Team
Palettes: Green/White



Sinnyil2
Female
Virgo
59
52
Geomancer
Item
Dragon Spirit
Throw Item
Jump+2

Slasher
Diamond Shield
Feather Hat
Adaman Vest
Small Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Lava Ball
Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



NovaKnight21
Male
Capricorn
59
56
Mediator
Black Magic
PA Save
Doublehand
Lava Walking

Blast Gun

Flash Hat
Linen Robe
Wizard Mantle

Persuade, Threaten, Insult, Negotiate, Refute
Fire 2, Fire 3, Fire 4, Bolt 2, Ice 2, Ice 3



Rocl
Male
Virgo
60
71
Priest
Battle Skill
Catch
Equip Axe
Jump+1

Healing Staff

Triangle Hat
Earth Clothes
Bracer

Cure, Cure 2, Raise, Raise 2, Reraise, Shell, Wall, Esuna
Weapon Break, Magic Break, Power Break, Stasis Sword



Prince Rogers Nelson
Female
Taurus
53
57
Wizard
Talk Skill
Arrow Guard
Maintenance
Fly

Flame Rod

Green Beret
Chain Vest
Reflect Ring

Fire, Fire 3, Bolt, Ice 4, Frog
Invitation, Persuade, Praise, Solution, Insult

Player: !Red
Team: Red Team
Palettes: Red/Brown



ShintaroNayaka
Monster
Aries
63
61
Grenade










ALY327
Female
Gemini
43
68
Time Mage
Yin Yang Magic
Counter
Halve MP
Ignore Terrain

Battle Bamboo

Triangle Hat
Black Costume
Cursed Ring

Haste, Haste 2, Slow, Slow 2, Immobilize, Stabilize Time, Meteor
Silence Song, Blind Rage, Dispel Magic, Sleep



Mrfripps
Female
Libra
67
43
Knight
Talk Skill
Parry
Magic Attack UP
Waterwalking

Broad Sword
Buckler
Bronze Helmet
Linen Robe
Diamond Armlet

Weapon Break, Magic Break, Speed Break, Power Break
Praise, Solution, Death Sentence, Insult, Negotiate, Refute



Bongomon7
Male
Pisces
44
67
Mediator
Basic Skill
MP Restore
Halve MP
Fly

Papyrus Codex

Leather Hat
Mythril Vest
Feather Boots

Praise, Rehabilitate
Accumulate, Dash, Throw Stone, Heal, Wish, Scream

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DLJuggernaut
Female
Libra
58
76
Squire
White Magic
Counter Flood
Equip Axe
Move+2

White Staff
Mythril Shield
Twist Headband
Carabini Mail
Power Wrist

Dash, Throw Stone, Wish, Scream
Cure 2, Raise, Raise 2, Protect, Esuna



Powergems
Male
Leo
73
51
Bard
Elemental
Absorb Used MP
Secret Hunt
Lava Walking

Silver Bow

Red Hood
Bronze Armor
Elf Mantle

Nameless Song
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



UmaiJam
Male
Virgo
45
47
Lancer
Summon Magic
Counter
Maintenance
Fly

Javelin
Mythril Shield
Bronze Helmet
White Robe
Genji Gauntlet

Level Jump2, Vertical Jump6
Moogle, Ifrit, Carbunkle, Bahamut, Salamander, Silf



Run With Stone GUNs
Male
Virgo
56
71
Bard
Battle Skill
Counter
Doublehand
Swim

Bloody Strings

Holy Miter
Crystal Mail
Power Wrist

Last Song, Diamond Blade, Hydra Pit
Weapon Break, Magic Break

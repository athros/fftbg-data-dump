Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



HaychDub
Male
Sagittarius
69
66
Priest
Draw Out
Counter Magic
Dual Wield
Move-MP Up

Scorpion Tail
Flail
Triangle Hat
Robe of Lords
Power Wrist

Cure, Raise, Protect, Protect 2, Shell, Shell 2, Esuna, Holy
Bizen Boat, Murasame, Heaven's Cloud



SkylerBunny
Male
Libra
53
47
Thief
Throw
Speed Save
Sicken
Fly

Mage Masher

Golden Hairpin
Brigandine
Defense Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Shield
Shuriken, Bomb, Axe



Gooseyourself
Male
Pisces
49
52
Geomancer
Draw Out
Meatbone Slash
Martial Arts
Jump+3

Battle Axe
Ice Shield
Twist Headband
Judo Outfit
Bracer

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Murasame, Kikuichimoji



Flameatron
Female
Scorpio
62
66
Oracle
Talk Skill
MA Save
Magic Attack UP
Move+3

Ivory Rod

Leather Hat
Linen Robe
Defense Armlet

Poison, Life Drain, Pray Faith, Confusion Song, Dispel Magic, Sleep
Invitation, Praise, Preach, Death Sentence, Mimic Daravon, Refute

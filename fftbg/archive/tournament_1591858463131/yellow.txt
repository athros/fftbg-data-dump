Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HaateXIII
Female
Aquarius
39
45
Priest
Draw Out
Parry
Short Charge
Retreat

Oak Staff

Twist Headband
Black Robe
Elf Mantle

Cure 3, Reraise, Protect, Shell, Wall, Esuna, Holy
Heaven's Cloud, Kiyomori



Richardserious
Male
Pisces
64
74
Calculator
Undeath Skill
HP Restore
Equip Sword
Move+1

Ragnarok

Feather Hat
Wizard Robe
Magic Gauntlet

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



HRak050
Male
Leo
62
56
Summoner
Sing
Auto Potion
Equip Polearm
Move+2

Battle Bamboo

Headgear
Black Robe
Magic Ring

Moogle, Ramuh, Titan, Golem, Odin, Silf, Lich
Life Song, Battle Song, Nameless Song



Vorap
Male
Libra
40
79
Knight
Time Magic
Counter
Equip Axe
Move-MP Up

Giant Axe
Flame Shield
Bronze Helmet
Crystal Mail
Wizard Mantle

Weapon Break, Speed Break
Haste, Immobilize, Float

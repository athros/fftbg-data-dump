Player: !Black
Team: Black Team
Palettes: Black/Red



Mesmaster
Monster
Leo
44
68
Byblos










GladiatorLupe
Monster
Serpentarius
50
71
Red Chocobo










Kalabain
Female
Sagittarius
49
60
Geomancer
Steal
Auto Potion
Beastmaster
Levitate

Sleep Sword
Round Shield
Triangle Hat
Linen Robe
Magic Gauntlet

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Gil Taking, Steal Armor, Steal Shield, Steal Weapon



ApplesauceBoss
Female
Scorpio
77
48
Ninja
Steal
Parry
Equip Gun
Swim

Battle Folio
Battle Folio
Golden Hairpin
Secret Clothes
Feather Mantle

Bomb
Steal Heart, Steal Weapon, Steal Accessory

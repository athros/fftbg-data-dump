Player: !Black
Team: Black Team
Palettes: Black/Red



Arcblazer23
Male
Cancer
80
66
Mime

Regenerator
Equip Armor
Move+2



Holy Miter
White Robe
Defense Armlet

Mimic




CosmicTactician
Female
Aries
41
42
Squire
White Magic
Absorb Used MP
Sicken
Move-MP Up

Assassin Dagger
Flame Shield
Flash Hat
Judo Outfit
Salty Rage

Accumulate, Yell, Cheer Up, Fury
Cure 2, Cure 4, Raise, Raise 2, Regen, Protect, Esuna



Vultuous
Male
Aquarius
81
46
Squire
Yin Yang Magic
MP Restore
Equip Sword
Fly

Kikuichimoji
Round Shield
Bronze Helmet
Mythril Vest
Jade Armlet

Dash, Throw Stone, Heal, Tickle
Life Drain, Doubt Faith, Dispel Magic, Dark Holy



Zagorsek
Male
Aquarius
46
45
Ninja
Draw Out
MP Restore
Defend
Ignore Terrain

Koga Knife
Flame Whip
Green Beret
Earth Clothes
Wizard Mantle

Shuriken, Wand
Koutetsu, Bizen Boat, Murasame

Player: !Green
Team: Green Team
Palettes: Green/White



Upvla
Male
Scorpio
51
72
Wizard
Sing
Hamedo
Short Charge
Ignore Height

Mythril Knife

Headgear
Silk Robe
Spike Shoes

Bolt, Ice, Ice 3, Empower, Flare
Angel Song, Life Song, Cheer Song



Twelfthrootoftwo
Male
Virgo
45
51
Mediator
Punch Art
Auto Potion
Magic Attack UP
Move+3

Glacier Gun

Twist Headband
Earth Clothes
Wizard Mantle

Invitation, Threaten, Solution, Negotiate, Mimic Daravon, Refute
Earth Slash, Secret Fist, Revive, Seal Evil



Porkbacon
Male
Serpentarius
75
82
Ninja
Battle Skill
Hamedo
Short Charge
Jump+2

Morning Star
Flame Whip
Green Beret
Clothes
Red Shoes

Shuriken, Bomb, Ninja Sword
Shield Break, Magic Break, Speed Break, Power Break



Lydian C
Female
Scorpio
44
80
Geomancer
Battle Skill
Arrow Guard
Short Charge
Swim

Giant Axe
Buckler
Twist Headband
Mystic Vest
Germinas Boots

Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Blizzard, Lava Ball
Weapon Break, Night Sword

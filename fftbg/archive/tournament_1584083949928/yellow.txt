Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Deathmaker06
Female
Virgo
74
69
Chemist
Battle Skill
Dragon Spirit
Attack UP
Lava Walking

Cute Bag

Feather Hat
Black Costume
Spike Shoes

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Head Break, Shield Break, Magic Break, Power Break, Justice Sword



Dynasti
Male
Sagittarius
54
75
Ninja
Draw Out
Blade Grasp
Equip Knife
Lava Walking

Hidden Knife
Air Knife
Triangle Hat
Mystic Vest
Leather Mantle

Staff
Asura, Koutetsu, Masamune



A Mo22
Female
Virgo
61
45
Knight
Summon Magic
Counter Magic
Concentrate
Swim

Slasher
Crystal Shield
Leather Helmet
Carabini Mail
Magic Ring

Head Break, Armor Break, Power Break, Stasis Sword
Moogle, Ifrit



HysteriaDays
Male
Libra
72
41
Squire
White Magic
Earplug
Short Charge
Move+1

Mythril Sword
Buckler
Red Hood
Black Costume
Rubber Shoes

Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Fury, Wish
Cure 2, Raise 2, Reraise, Regen, Esuna, Holy

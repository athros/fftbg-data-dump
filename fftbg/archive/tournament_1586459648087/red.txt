Player: !Red
Team: Red Team
Palettes: Red/Brown



Soapydragonfly
Female
Virgo
80
75
Calculator
Time Magic
Faith Up
Maintenance
Lava Walking

Wizard Rod

Holy Miter
Black Robe
Elf Mantle

CT, Prime Number, 5, 4, 3
Haste, Slow, Slow 2, Reflect, Quick, Stabilize Time, Meteor



ApplesauceBoss
Male
Taurus
51
79
Mediator
Yin Yang Magic
Counter Flood
Dual Wield
Jump+2

Stone Gun
Blast Gun
Black Hood
Black Robe
Feather Boots

Invitation, Threaten, Preach, Negotiate, Mimic Daravon, Refute
Poison, Spell Absorb, Pray Faith, Doubt Faith, Confusion Song



Oogthecaveman
Female
Leo
46
57
Monk
Jump
Arrow Guard
Maintenance
Ignore Terrain



Twist Headband
Chain Vest
Elf Mantle

Purification, Chakra, Revive
Level Jump4, Vertical Jump7



Stalskegg
Male
Scorpio
75
80
Bard
Steal
Counter
Equip Shield
Move-HP Up

Bloody Strings
Diamond Shield
Twist Headband
Bronze Armor
Sprint Shoes

Angel Song, Diamond Blade, Sky Demon, Hydra Pit
Steal Armor, Steal Weapon

Player: !Black
Team: Black Team
Palettes: Black/Red



Narcius
Male
Libra
70
68
Knight
Throw
Counter Magic
Defend
Ignore Terrain

Sleep Sword
Diamond Shield
Platinum Helmet
Gold Armor
Red Shoes

Armor Break, Power Break, Mind Break, Justice Sword
Shuriken, Staff, Axe, Dictionary



NovaKnight21
Male
Virgo
76
61
Thief
Summon Magic
Parry
Defense UP
Teleport

Blind Knife

Ribbon
Power Sleeve
Salty Rage

Steal Heart, Steal Helmet, Steal Armor, Steal Status, Arm Aim, Leg Aim
Moogle, Ramuh, Golem, Carbunkle, Odin, Silf



Skjodo
Female
Scorpio
80
65
Archer
Black Magic
Counter
Secret Hunt
Ignore Terrain

Blaze Gun
Round Shield
Leather Hat
Brigandine
Magic Ring

Charge+1, Charge+3, Charge+7, Charge+20
Fire 2, Fire 3, Bolt, Bolt 4, Ice 4, Frog



TheBinklive
Female
Leo
77
38
Samurai
Talk Skill
Counter
Monster Talk
Move+3

Kiyomori

Iron Helmet
Black Robe
Elf Mantle

Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate

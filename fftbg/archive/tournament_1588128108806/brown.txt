Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lawnboxer
Female
Leo
69
61
Priest
Elemental
Counter Magic
Martial Arts
Lava Walking



Triangle Hat
Rubber Costume
Red Shoes

Cure, Cure 2, Cure 3, Raise, Protect, Protect 2, Wall, Esuna
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Spartan Paladin
Male
Capricorn
76
67
Knight
Draw Out
Counter Flood
Magic Attack UP
Waterwalking

Ice Brand
Diamond Shield
Iron Helmet
Linen Robe
Small Mantle

Shield Break, Weapon Break, Speed Break, Mind Break
Bizen Boat



Fenixcrest
Female
Serpentarius
39
70
Calculator
Byblos
Damage Split
Short Charge
Lava Walking

Bestiary
Gold Shield
Bronze Helmet
Wizard Outfit
Diamond Armlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Arch8000
Female
Aries
57
51
Knight
Time Magic
Catch
Equip Gun
Jump+3

Papyrus Codex
Mythril Shield
Circlet
White Robe
Elf Mantle

Head Break, Shield Break, Magic Break, Dark Sword
Haste, Slow, Slow 2, Float, Demi, Demi 2, Stabilize Time, Galaxy Stop

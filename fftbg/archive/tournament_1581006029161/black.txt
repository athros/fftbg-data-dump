Player: !Black
Team: Black Team
Palettes: Black/Red



BenYuPoker
Male
Aries
73
53
Ninja
Basic Skill
Counter Tackle
Equip Knife
Ignore Terrain

Main Gauche
Spell Edge
Twist Headband
Judo Outfit
Power Wrist

Bomb
Accumulate, Dash, Yell



AniZero
Female
Aries
49
82
Geomancer
Draw Out
Damage Split
Attack UP
Jump+2

Slasher
Diamond Shield
Red Hood
Power Sleeve
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Heaven's Cloud



Lodrak
Male
Gemini
68
59
Bard
White Magic
Critical Quick
Doublehand
Move+3

Ramia Harp

Thief Hat
Clothes
Rubber Shoes

Angel Song, Battle Song, Magic Song, Nameless Song, Diamond Blade, Hydra Pit
Cure 2, Cure 3, Raise, Reraise, Wall, Esuna, Magic Barrier



Ququroon
Male
Leo
45
76
Thief
Battle Skill
Critical Quick
Equip Shield
Jump+1

Ice Brand
Aegis Shield
Leather Hat
Black Costume
Red Shoes

Gil Taking, Steal Weapon, Steal Accessory, Steal Status
Speed Break, Mind Break, Stasis Sword

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



JimmyKetz
Male
Gemini
41
76
Priest
Yin Yang Magic
MA Save
Equip Knife
Levitate

Ice Rod

Feather Hat
Mythril Vest
Elf Mantle

Raise, Protect, Protect 2, Shell 2, Wall, Esuna
Doubt Faith



Pandasforsale
Male
Taurus
63
58
Calculator
Lucavi Skill
Speed Save
Defense UP
Levitate

Cypress Rod

Holy Miter
Bronze Armor
Power Wrist

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Rico Flex
Female
Serpentarius
55
74
Lancer
Black Magic
Auto Potion
Doublehand
Swim

Partisan

Genji Helmet
Linen Cuirass
Defense Armlet

Level Jump5, Vertical Jump3
Fire, Fire 2, Fire 3, Ice 2, Ice 3, Empower, Flare



Rubenflonne
Female
Capricorn
66
57
Calculator
Imp Skill
Counter Magic
Beastmaster
Jump+3

Cypress Rod

Leather Hat
Linen Cuirass
108 Gems

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam

Player: !Green
Team: Green Team
Palettes: Green/White



DeathTaxesAndAnime
Female
Pisces
81
78
Samurai
Elemental
Abandon
Magic Defense UP
Move-HP Up

Murasame

Barbuta
Plate Mail
Dracula Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Aldrammech
Male
Aquarius
62
58
Ninja
White Magic
Earplug
Defend
Ignore Terrain

Morning Star
Flame Whip
Headgear
Wizard Outfit
Genji Gauntlet

Shuriken, Knife, Wand
Cure 2, Regen, Shell, Esuna



CgMcWhiskers
Female
Cancer
62
66
Knight
Yin Yang Magic
Caution
Magic Attack UP
Jump+1

Ragnarok
Aegis Shield
Crystal Helmet
Chameleon Robe
Bracer

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword
Blind, Poison, Life Drain, Blind Rage, Dispel Magic



GreatRedDragon
Female
Pisces
79
49
Dancer
Black Magic
Distribute
Equip Bow
Levitate

Hunting Bow

Flash Hat
Clothes
Rubber Shoes

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Obsidian Blade, Void Storage, Nether Demon
Fire 3, Fire 4, Bolt, Ice 2, Ice 3, Ice 4, Empower

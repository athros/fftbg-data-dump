Player: !Red
Team: Red Team
Palettes: Red/Brown



SkylerBunny
Female
Aquarius
45
67
Priest
Dance
Sunken State
Defense UP
Teleport

Sage Staff

Triangle Hat
Mythril Vest
Diamond Armlet

Cure 2, Cure 3, Protect 2, Shell 2, Wall, Esuna
Wiznaibus, Disillusion, Obsidian Blade, Nether Demon



TheMurkGnome
Monster
Leo
55
40
Dragon










Hasterious
Monster
Libra
70
66
Black Chocobo










Nhammen
Male
Cancer
58
46
Knight
Yin Yang Magic
Counter Magic
Secret Hunt
Jump+2

Battle Axe
Escutcheon
Circlet
Linen Robe
Red Shoes

Head Break, Armor Break, Shield Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Blind, Life Drain, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep

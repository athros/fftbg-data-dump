Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Shalloween
Female
Leo
44
56
Priest
Draw Out
Parry
Long Status
Move-HP Up

Flail

Golden Hairpin
Chain Vest
Genji Gauntlet

Cure 3, Cure 4, Raise, Raise 2, Shell, Wall, Esuna
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



PoroTact
Female
Leo
69
69
Samurai
Black Magic
Counter
Equip Axe
Move+1

Flail

Circlet
Chameleon Robe
Angel Ring

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji, Masamune
Fire, Bolt 2, Ice, Empower, Death



Omegasuspekt
Male
Capricorn
59
77
Mediator
White Magic
Sunken State
Short Status
Waterwalking

Papyrus Codex

Flash Hat
Light Robe
Germinas Boots

Persuade, Threaten, Death Sentence, Insult, Rehabilitate
Cure 2, Cure 3, Raise, Regen, Esuna



Coralreeferz
Female
Cancer
58
76
Samurai
Punch Art
Critical Quick
Martial Arts
Fly

Asura Knife

Crystal Helmet
Linen Robe
Defense Armlet

Asura, Koutetsu, Heaven's Cloud, Kikuichimoji
Spin Fist, Wave Fist, Purification, Chakra, Revive

Player: !Black
Team: Black Team
Palettes: Black/Red



Genoguy
Male
Pisces
73
47
Lancer
Talk Skill
Parry
Equip Knife
Move-HP Up

Spell Edge
Mythril Shield
Platinum Helmet
Plate Mail
Angel Ring

Level Jump5, Vertical Jump8
Invitation, Refute



Jordache7K
Monster
Libra
70
65
Steel Giant










AlenaZarek
Male
Leo
69
59
Geomancer
Summon Magic
Arrow Guard
Equip Bow
Ignore Terrain

Yoichi Bow

Black Hood
Light Robe
Bracer

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Shiva, Ifrit, Leviathan, Salamander, Cyclops



Alacor
Male
Gemini
68
41
Knight
White Magic
Sunken State
Short Charge
Jump+3

Sleep Sword
Flame Shield
Gold Helmet
Gold Armor
Feather Mantle

Head Break, Shield Break, Speed Break, Surging Sword
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Protect, Protect 2, Wall, Esuna

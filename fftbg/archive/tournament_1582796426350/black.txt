Player: !Black
Team: Black Team
Palettes: Black/Red



SeedSC
Female
Aquarius
48
42
Geomancer
Summon Magic
Brave Up
Equip Armor
Teleport

Broad Sword
Kaiser Plate
Circlet
Leather Armor
Angel Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock
Moogle, Titan, Carbunkle, Bahamut, Salamander, Fairy, Zodiac



Basmal
Male
Taurus
71
73
Mime

Counter
Defend
Move+2



Thief Hat
Judo Outfit
Dracula Mantle

Mimic




AlysSariel
Male
Cancer
54
55
Mime

Critical Quick
Equip Shield
Waterbreathing


Aegis Shield
Twist Headband
Black Robe
Germinas Boots

Mimic




HysteriaDays
Male
Pisces
67
55
Summoner
Steal
Parry
Beastmaster
Levitate

Ice Rod

Triangle Hat
Earth Clothes
108 Gems

Moogle, Golem, Bahamut
Gil Taking, Steal Accessory, Arm Aim

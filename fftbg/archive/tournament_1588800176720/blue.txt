Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DonCardenio
Monster
Aquarius
73
54
Pisco Demon










Sairentozon7
Female
Cancer
49
72
Calculator
Byblos
Parry
Equip Sword
Teleport

Murasame
Buckler
Headgear
Light Robe
Chantage

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



HorusTaurus
Male
Libra
44
47
Knight
Throw
Hamedo
Dual Wield
Jump+2

Platinum Sword
Ice Brand
Diamond Helmet
Bronze Armor
Defense Armlet

Head Break, Shield Break, Weapon Break, Stasis Sword, Justice Sword
Shuriken, Wand



Byrdturbo
Male
Cancer
42
47
Bard
Talk Skill
Meatbone Slash
Equip Shield
Move-HP Up

Ramia Harp
Mythril Shield
Thief Hat
Bronze Armor
Defense Armlet

Angel Song, Magic Song, Space Storage, Sky Demon
Persuade, Praise, Solution

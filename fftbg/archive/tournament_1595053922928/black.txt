Player: !Black
Team: Black Team
Palettes: Black/Red



Lydian C
Female
Taurus
45
75
Geomancer
Draw Out
Counter Magic
Attack UP
Retreat

Coral Sword
Flame Shield
Ribbon
Earth Clothes
Small Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Asura, Bizen Boat, Murasame



StealthModeLocke
Monster
Aquarius
68
47
Revenant










HaateXIII
Male
Libra
69
80
Lancer
Throw
Meatbone Slash
Concentrate
Teleport

Mythril Spear
Flame Shield
Diamond Helmet
Plate Mail
Spike Shoes

Level Jump5, Vertical Jump5
Bomb, Knife



Legitimized
Male
Taurus
56
46
Bard
Jump
Auto Potion
Defense UP
Levitate

Bloody Strings

Triangle Hat
Mythril Armor
Jade Armlet

Life Song, Magic Song, Hydra Pit
Level Jump5, Vertical Jump8

Player: !White
Team: White Team
Palettes: White/Blue



Mtueni
Male
Aquarius
61
71
Priest
Steal
Critical Quick
Secret Hunt
Fly

Flame Whip

Headgear
Linen Robe
Angel Ring

Cure 3, Raise, Raise 2, Reraise, Protect, Shell 2, Wall, Esuna
Gil Taking



RageImmortaI
Female
Aquarius
51
57
Ninja
Basic Skill
Counter Flood
Attack UP
Move+3

Morning Star
Ninja Edge
Holy Miter
Earth Clothes
N-Kai Armlet

Shuriken, Bomb, Staff, Dictionary
Heal, Wish



GarinL
Female
Libra
72
74
Priest
Elemental
Auto Potion
Magic Attack UP
Move+1

Rainbow Staff

Holy Miter
Silk Robe
Bracer

Cure 2, Cure 3, Raise, Raise 2, Protect 2, Wall, Esuna
Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Dothan89
Female
Taurus
44
53
Knight
Basic Skill
Speed Save
Defense UP
Ignore Terrain

Defender

Platinum Helmet
Gold Armor
Reflect Ring

Head Break, Magic Break, Mind Break, Stasis Sword
Accumulate, Dash, Tickle

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nizaha
Female
Gemini
78
48
Geomancer
Draw Out
Sunken State
Equip Armor
Move+3

Slasher
Escutcheon
Headgear
Diamond Armor
Setiemson

Pitfall, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud



Gormkt30
Male
Aries
39
44
Mime

MP Restore
Doublehand
Swim



Flash Hat
Clothes
Sprint Shoes

Mimic




Artea
Male
Scorpio
51
51
Mime

Faith Up
Attack UP
Move-HP Up



Black Hood
Chain Vest
Jade Armlet

Mimic




Wonderkush
Female
Taurus
61
64
Priest
Basic Skill
PA Save
Long Status
Fly

Gold Staff

Holy Miter
Power Sleeve
Chantage

Cure, Cure 3, Protect 2, Shell, Shell 2, Esuna
Accumulate, Dash, Heal, Cheer Up

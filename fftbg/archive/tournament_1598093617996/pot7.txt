Final Bets: green - 6 bets for 2,267G (22.7%, x3.41); brown - 7 bets for 7,721G (77.3%, x0.29)

green bets:
gorgewall: 1,278G (56.4%, 1,278G)
Lazarus_DS: 361G (15.9%, 361G)
Panushenko: 220G (9.7%, 220G)
datadrivenbot: 200G (8.8%, 61,357G)
FalseLobster: 108G (4.8%, 108G)
ko2q: 100G (4.4%, 3,195G)

brown bets:
E_Ballard: 5,329G (69.0%, 10,658G)
BirbBrainsBot: 622G (8.1%, 121,503G)
getthemoneyz: 574G (7.4%, 1,702,775G)
NIghtdew14: 376G (4.9%, 376G)
skipsandwiches: 356G (4.6%, 356G)
AllInBot: 254G (3.3%, 254G)
roqqqpsi: 210G (2.7%, 958G)

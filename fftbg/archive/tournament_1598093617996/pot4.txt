Final Bets: purple - 6 bets for 2,489G (21.2%, x3.72); brown - 9 bets for 9,247G (78.8%, x0.27)

purple bets:
gorgewall: 864G (34.7%, 864G)
DouglasDragonThePoet: 532G (21.4%, 1,064G)
roqqqpsi: 517G (20.8%, 562G)
NIghtdew14: 376G (15.1%, 376G)
AllInBot: 100G (4.0%, 100G)
ko2q: 100G (4.0%, 3,171G)

brown bets:
E_Ballard: 4,978G (53.8%, 4,978G)
Mudrockk: 1,000G (10.8%, 4,532G)
BirbBrainsBot: 1,000G (10.8%, 123,234G)
getthemoneyz: 1,000G (10.8%, 1,703,455G)
VolgraTheMoose: 501G (5.4%, 33,213G)
josephiroth_143: 348G (3.8%, 348G)
datadrivenbot: 200G (2.2%, 61,063G)
Lazarus_DS: 112G (1.2%, 112G)
FalseLobster: 108G (1.2%, 108G)

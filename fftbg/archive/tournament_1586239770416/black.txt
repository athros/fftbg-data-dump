Player: !Black
Team: Black Team
Palettes: Black/Red



Nocturnal Perspective
Female
Pisces
54
53
Time Mage
White Magic
Earplug
Halve MP
Ignore Height

Bestiary

Flash Hat
Wizard Robe
Magic Gauntlet

Haste 2, Quick, Stabilize Time, Meteor
Cure 3, Raise, Raise 2, Protect 2, Wall, Esuna



Triyaden
Female
Aries
57
76
Lancer
Yin Yang Magic
Brave Up
Halve MP
Swim

Partisan
Crystal Shield
Barbuta
Linen Cuirass
108 Gems

Level Jump4, Vertical Jump4
Life Drain, Doubt Faith



KaLam1ty
Female
Virgo
55
53
Priest
Steal
MP Restore
Defense UP
Move+3

White Staff

Golden Hairpin
Mystic Vest
Reflect Ring

Cure, Cure 2, Raise, Regen, Protect 2, Shell, Shell 2, Esuna
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



Reinoe
Male
Sagittarius
76
72
Summoner
Charge
Abandon
Defend
Teleport

Dragon Rod

Thief Hat
Black Robe
Defense Ring

Moogle, Shiva, Ifrit, Titan, Carbunkle, Fairy, Lich
Charge+1, Charge+3, Charge+4, Charge+10

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ZephyrTempest
Female
Capricorn
43
57
Oracle
Steal
Meatbone Slash
Short Status
Ignore Height

Battle Bamboo

Triangle Hat
Silk Robe
Jade Armlet

Spell Absorb, Life Drain, Pray Faith, Foxbird, Sleep, Petrify, Dark Holy
Gil Taking, Steal Heart, Steal Helmet, Steal Shield



Dstnmarsh
Female
Cancer
60
42
Wizard
Battle Skill
Earplug
Beastmaster
Jump+1

Ice Rod

Triangle Hat
Light Robe
Feather Mantle

Fire, Fire 2, Ice 3, Ice 4, Empower, Flare
Head Break, Weapon Break, Magic Break, Speed Break, Surging Sword



Tougou
Male
Sagittarius
46
57
Archer
Time Magic
Meatbone Slash
Doublehand
Jump+1

Ice Bow

Headgear
Leather Outfit
Jade Armlet

Charge+1, Charge+4
Haste, Stop, Float, Demi 2, Stabilize Time



LOKITHUS
Female
Leo
49
54
Lancer
Elemental
Earplug
Sicken
Lava Walking

Javelin
Aegis Shield
Barbuta
Plate Mail
Elf Mantle

Level Jump3, Vertical Jump7
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

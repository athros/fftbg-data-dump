Player: !zChamp
Team: Champion Team
Palettes: Green/White



PotionDweller
Monster
Capricorn
47
40
Ultima Demon










Shakarak
Female
Virgo
59
43
Mediator
Summon Magic
Parry
Dual Wield
Waterbreathing

Bestiary
Battle Folio
Thief Hat
Adaman Vest
Leather Mantle

Invitation, Persuade, Threaten, Solution, Insult, Rehabilitate
Moogle, Ramuh, Leviathan, Fairy, Lich



Vivithegr8
Female
Cancer
49
57
Wizard
Draw Out
Faith Up
Equip Knife
Retreat

Blind Knife

Feather Hat
Adaman Vest
Rubber Shoes

Fire 4, Bolt, Bolt 4, Ice 2, Ice 3, Empower
Asura, Bizen Boat, Murasame, Kikuichimoji, Masamune



Mrasin
Monster
Aries
46
49
Steel Giant








Final Bets: white - 7 bets for 3,221G (50.3%, x0.99); black - 5 bets for 3,185G (49.7%, x1.01)

white bets:
Creggers: 1,735G (53.9%, 1,735G)
nifboy: 628G (19.5%, 628G)
FalseLobster: 300G (9.3%, 850G)
datadrivenbot: 200G (6.2%, 61,929G)
3ngag3: 157G (4.9%, 1,457G)
gorgewall: 101G (3.1%, 3,017G)
Lazarus_DS: 100G (3.1%, 100G)

black bets:
CT_5_Holy: 1,666G (52.3%, 1,666G)
BirbBrainsBot: 953G (29.9%, 49,160G)
getthemoneyz: 358G (11.2%, 1,681,423G)
nebukin: 108G (3.4%, 108G)
AllInBot: 100G (3.1%, 100G)

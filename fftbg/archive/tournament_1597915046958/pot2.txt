Final Bets: green - 4 bets for 1,267G (39.6%, x1.53); yellow - 7 bets for 1,936G (60.4%, x0.65)

green bets:
CT_5_Holy: 659G (52.0%, 659G)
reddwind_: 308G (24.3%, 346G)
datadrivenbot: 200G (15.8%, 61,623G)
3ngag3: 100G (7.9%, 1,300G)

yellow bets:
BirbBrainsBot: 811G (41.9%, 49,971G)
Creggers: 500G (25.8%, 2,235G)
getthemoneyz: 174G (9.0%, 1,681,597G)
FalseLobster: 150G (7.7%, 1,000G)
gorgewall: 101G (5.2%, 3,118G)
AllInBot: 100G (5.2%, 100G)
Lazarus_DS: 100G (5.2%, 156G)

Player: !Green
Team: Green Team
Palettes: Green/White



Draaaan
Male
Virgo
57
48
Summoner
Battle Skill
Counter
Equip Sword
Move+2

Asura Knife

Triangle Hat
Silk Robe
Small Mantle

Golem, Carbunkle, Bahamut, Silf, Lich, Cyclops
Head Break, Speed Break, Justice Sword



Fozzington
Monster
Virgo
80
56
Dragon










Ar Tactic
Male
Leo
63
65
Mediator
Summon Magic
Parry
Halve MP
Move-MP Up

Battle Folio

Flash Hat
Black Robe
Bracer

Praise, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Moogle, Carbunkle, Odin



Riot Jayway
Male
Leo
59
68
Ninja
Elemental
Earplug
Short Status
Move+2

Hidden Knife
Short Edge
Holy Miter
Clothes
Battle Boots

Shuriken
Pitfall, Water Ball, Hell Ivy, Local Quake, Quicksand, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



StealthModeLocke
Male
Capricorn
69
45
Oracle
Draw Out
Earplug
Maintenance
Waterwalking

Iron Fan

Barette
Adaman Vest
Chantage

Poison, Spell Absorb, Doubt Faith, Silence Song
Asura, Bizen Boat, Muramasa



Ferroniss
Female
Aquarius
53
63
Time Mage
Draw Out
Catch
Secret Hunt
Ignore Height

Wizard Staff

Black Hood
Black Robe
Battle Boots

Slow 2, Demi, Stabilize Time
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Kellios11
Female
Serpentarius
75
65
Mediator
Time Magic
Sunken State
Dual Wield
Fly

Mythril Gun
Blaze Gun
Thief Hat
Light Robe
Reflect Ring

Solution, Insult, Negotiate, Refute, Rehabilitate
Haste, Slow, Slow 2, Quick, Demi 2, Stabilize Time



ExecutedGiraffe
Female
Taurus
51
52
Geomancer
Dance
HP Restore
Defend
Ignore Terrain

Slasher
Aegis Shield
Green Beret
Earth Clothes
Chantage

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind
Wiznaibus, Nameless Dance, Last Dance, Dragon Pit

Final Bets: red - 19 bets for 12,628G (49.3%, x1.03); champion - 22 bets for 12,965G (50.7%, x0.97)

red bets:
Lydian_C: 2,376G (18.8%, 4,660G)
Firesheath: 1,588G (12.6%, 1,588G)
Lord_Burrah: 1,500G (11.9%, 96,000G)
leakimiko: 1,500G (11.9%, 57,967G)
BirbBrainsBot: 1,000G (7.9%, 146,355G)
roofiepops: 600G (4.8%, 48,839G)
DarrenDinosaurs: 500G (4.0%, 5,242G)
Drusic: 500G (4.0%, 5,560G)
HaateXIII: 500G (4.0%, 2,482G)
alecttox: 455G (3.6%, 455G)
tronfonne: 417G (3.3%, 417G)
Baron_von_Scrub: 353G (2.8%, 9,218G)
gorgewall: 301G (2.4%, 7,964G)
Meta_Five: 300G (2.4%, 16,403G)
nekross92: 238G (1.9%, 477G)
Aerochris121: 200G (1.6%, 1,464G)
E_Ballard: 100G (0.8%, 7,510G)
OneHundredFists: 100G (0.8%, 3,143G)
rjA0zcOQ96: 100G (0.8%, 4,361G)

champion bets:
ExecutedGiraffe: 3,000G (23.1%, 14,555G)
powergems: 1,655G (12.8%, 1,655G)
cam_ATS: 1,423G (11.0%, 1,423G)
RunicMagus: 1,200G (9.3%, 37,117G)
ungabunga_bot: 1,000G (7.7%, 529,812G)
HaplessOne: 666G (5.1%, 11,729G)
killth3kid: 600G (4.6%, 3,611G)
GrandmasterFrankerZ: 558G (4.3%, 558G)
twelfthrootoftwo: 500G (3.9%, 18,895G)
skillomono: 400G (3.1%, 4,026G)
Lionhermit: 308G (2.4%, 308G)
Mtueni: 300G (2.3%, 9,581G)
Setdevildog: 216G (1.7%, 216G)
Lawndough: 200G (1.5%, 3,906G)
getthemoneyz: 184G (1.4%, 681,097G)
ScurvyMitch: 155G (1.2%, 1,155G)
rikimaruhitori: 100G (0.8%, 1,128G)
Tougou: 100G (0.8%, 5,170G)
DeathTaxesAndAnime: 100G (0.8%, 3,378G)
Soozaphone: 100G (0.8%, 100G)
datadrivenbot: 100G (0.8%, 18,686G)
asherban: 100G (0.8%, 1,194G)

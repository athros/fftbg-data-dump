Final Bets: purple - 8 bets for 3,799G (56.1%, x0.78); brown - 9 bets for 2,975G (43.9%, x1.28)

purple bets:
BirbBrainsBot: 1,000G (26.3%, 155,159G)
sinnyil2: 796G (21.0%, 796G)
Estan_AD: 728G (19.2%, 1,456G)
otakutaylor: 400G (10.5%, 3,613G)
CorpusCav: 400G (10.5%, 400G)
tronfonne: 228G (6.0%, 228G)
itsZaes: 147G (3.9%, 147G)
datadrivenbot: 100G (2.6%, 18,552G)

brown bets:
OrganicArtist: 666G (22.4%, 3,295G)
DaveStrider55: 544G (18.3%, 544G)
Phytik: 446G (15.0%, 446G)
Evewho: 356G (12.0%, 356G)
Digitalsocrates: 228G (7.7%, 228G)
gorgewall: 201G (6.8%, 11,280G)
AmaninAmide: 200G (6.7%, 4,659G)
ungabunga_bot: 186G (6.3%, 535,350G)
getthemoneyz: 148G (5.0%, 686,031G)

Player: !Green
Team: Green Team
Palettes: Green/White



Bioticism
Female
Virgo
78
78
Monk
Throw
Regenerator
Equip Axe
Jump+2

Battle Axe

Red Hood
Judo Outfit
Sprint Shoes

Earth Slash, Purification, Chakra
Stick



Triskiut
Female
Gemini
50
70
Chemist
Steal
Counter
Short Charge
Teleport

Panther Bag

Triangle Hat
Clothes
Spike Shoes

Echo Grass, Phoenix Down
Steal Heart, Steal Armor, Steal Weapon



KasugaiRoastedPeas
Male
Cancer
49
64
Lancer
Draw Out
Critical Quick
Long Status
Jump+3

Spear
Round Shield
Mythril Helmet
Gold Armor
Feather Boots

Level Jump4, Vertical Jump8
Asura, Kikuichimoji



Mistblade
Female
Cancer
65
68
Ninja
Elemental
Critical Quick
Defend
Swim

Hidden Knife
Spell Edge
Thief Hat
Leather Outfit
Spike Shoes

Shuriken, Knife, Staff
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

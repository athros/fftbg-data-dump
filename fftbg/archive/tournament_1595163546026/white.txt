Player: !White
Team: White Team
Palettes: White/Blue



Snoopiku
Male
Aries
59
70
Samurai
Jump
Meatbone Slash
Equip Axe
Jump+2

Flail

Gold Helmet
Carabini Mail
Feather Boots

Koutetsu, Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Level Jump8, Vertical Jump7



SomthingMore
Male
Virgo
63
72
Samurai
Punch Art
Faith Save
Doublehand
Move+1

Javelin

Grand Helmet
Genji Armor
Reflect Ring

Murasame, Kiyomori, Muramasa, Kikuichimoji
Purification, Revive



Yepyeyeye
Male
Taurus
43
76
Lancer
Summon Magic
Counter Magic
Equip Axe
Waterbreathing

Gold Staff
Aegis Shield
Barbuta
Wizard Robe
Angel Ring

Level Jump2, Vertical Jump8
Moogle, Ifrit, Titan, Leviathan, Fairy



Ruleof5
Female
Pisces
64
45
Ninja
Elemental
Counter Tackle
Equip Bow
Waterbreathing

Poison Bow
Bow Gun
Green Beret
Brigandine
Dracula Mantle

Wand
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard

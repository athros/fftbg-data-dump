Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ohhinm
Female
Scorpio
54
77
Samurai
Black Magic
Absorb Used MP
Short Charge
Levitate

Heaven's Cloud

Platinum Helmet
Platinum Armor
Genji Gauntlet

Kiyomori, Muramasa, Kikuichimoji
Fire 2, Fire 4, Ice 3, Empower, Death



FriendSquirrel
Female
Libra
51
48
Oracle
Black Magic
Absorb Used MP
Doublehand
Jump+1

Battle Bamboo

Green Beret
Chain Vest
Battle Boots

Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze
Fire, Fire 2, Bolt 3, Bolt 4, Ice



Randgridr
Female
Scorpio
75
59
Calculator
Black Magic
MP Restore
Magic Attack UP
Jump+1

Panther Bag

Leather Hat
Power Sleeve
Defense Armlet

CT, Height, Prime Number, 5, 3
Fire 2



Nhammen
Male
Cancer
73
47
Knight
Basic Skill
MP Restore
Magic Defense UP
Move+3

Save the Queen
Crystal Shield
Diamond Helmet
Wizard Robe
108 Gems

Shield Break, Dark Sword, Night Sword
Heal

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Lemonjohns
Monster
Scorpio
48
75
Tiamat










ForagerCats
Female
Libra
72
72
Geomancer
Summon Magic
Regenerator
Equip Armor
Move+1

Long Sword
Escutcheon
Cross Helmet
Gold Armor
Feather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard
Moogle, Shiva, Ifrit, Silf



Ruleof5
Female
Taurus
72
69
Samurai
Punch Art
Brave Save
Magic Attack UP
Fly

Obelisk

Crystal Helmet
Genji Armor
Leather Mantle

Kiyomori, Kikuichimoji, Masamune
Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil



E Ballard
Male
Capricorn
54
73
Bard
White Magic
Counter Tackle
Maintenance
Ignore Height

Bloody Strings

Golden Hairpin
Mystic Vest
Battle Boots

Angel Song
Cure, Raise, Reraise, Regen, Protect 2, Holy

Final Bets: purple - 10 bets for 4,939G (25.1%, x2.99); brown - 9 bets for 14,744G (74.9%, x0.33)

purple bets:
sinnyil2: 1,140G (23.1%, 2,280G)
twelfthrootoftwo: 1,127G (22.8%, 1,127G)
superdevon1: 890G (18.0%, 29,677G)
DeathTaxesAndAnime: 784G (15.9%, 784G)
resjudicata3: 212G (4.3%, 212G)
NeonTarget: 212G (4.3%, 212G)
datadrivenbot: 200G (4.0%, 41,217G)
getthemoneyz: 174G (3.5%, 1,383,649G)
Firesheath: 100G (2.0%, 11,161G)
poGpopE: 100G (2.0%, 2,816G)

brown bets:
evontno: 8,000G (54.3%, 35,094G)
renkei_fukai: 1,838G (12.5%, 1,838G)
UmaiJam: 1,500G (10.2%, 56,493G)
BirbBrainsBot: 1,000G (6.8%, 61,168G)
douchetron: 854G (5.8%, 1,676G)
Laserman1000: 628G (4.3%, 3,528G)
josephiroth_143: 500G (3.4%, 4,339G)
Hirameki85: 224G (1.5%, 224G)
AllInBot: 200G (1.4%, 200G)

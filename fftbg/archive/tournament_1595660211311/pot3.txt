Final Bets: white - 12 bets for 7,538G (72.8%, x0.37); black - 7 bets for 2,817G (27.2%, x2.68)

white bets:
mirapoix: 1,343G (17.8%, 1,343G)
twelfthrootoftwo: 1,171G (15.5%, 2,298G)
HaateXIII: 872G (11.6%, 872G)
Hirameki85: 794G (10.5%, 794G)
DeathTaxesAndAnime: 788G (10.5%, 788G)
NeonTarget: 752G (10.0%, 752G)
BirbBrainsBot: 636G (8.4%, 61,804G)
josephiroth_143: 500G (6.6%, 4,839G)
resjudicata3: 212G (2.8%, 212G)
AllInBot: 200G (2.7%, 200G)
datadrivenbot: 200G (2.7%, 41,417G)
AugnosMusic: 70G (0.9%, 235G)

black bets:
poGpopE: 766G (27.2%, 766G)
superdevon1: 563G (20.0%, 28,170G)
renkei_fukai: 500G (17.7%, 500G)
douchetron: 456G (16.2%, 456G)
getthemoneyz: 232G (8.2%, 1,383,028G)
dtrain332: 200G (7.1%, 2,713G)
Firesheath: 100G (3.5%, 10,893G)

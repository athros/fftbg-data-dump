Final Bets: yellow - 7 bets for 5,101G (14.4%, x5.93); champion - 12 bets for 30,257G (85.6%, x0.17)

yellow bets:
UmaiJam: 1,500G (29.4%, 56,576G)
BirbBrainsBot: 1,000G (19.6%, 59,779G)
mirapoix: 794G (15.6%, 794G)
Laserman1000: 683G (13.4%, 3,283G)
superdevon1: 637G (12.5%, 31,891G)
AllInBot: 277G (5.4%, 277G)
getthemoneyz: 210G (4.1%, 1,383,604G)

champion bets:
prince_rogers_nelson_: 13,340G (44.1%, 26,680G)
evontno: 10,000G (33.1%, 30,188G)
josephiroth_143: 3,005G (9.9%, 3,005G)
poGpopE: 1,000G (3.3%, 3,115G)
grininda: 1,000G (3.3%, 5,325G)
douchetron: 653G (2.2%, 653G)
NeonTarget: 283G (0.9%, 283G)
Hirameki85: 224G (0.7%, 224G)
resjudicata3: 212G (0.7%, 212G)
gorgewall: 201G (0.7%, 12,370G)
datadrivenbot: 200G (0.7%, 41,758G)
renkei_fukai: 139G (0.5%, 139G)

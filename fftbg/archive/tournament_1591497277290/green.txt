Player: !Green
Team: Green Team
Palettes: Green/White



Blastty
Female
Aquarius
49
51
Wizard
Draw Out
Counter Tackle
Equip Armor
Jump+1

Blind Knife

Gold Helmet
Plate Mail
Magic Ring

Fire 4, Bolt 2, Bolt 4, Ice 3, Ice 4, Empower
Asura, Bizen Boat, Murasame



Mpghappiness
Female
Leo
61
42
Summoner
Draw Out
Arrow Guard
Equip Sword
Move+2

Broad Sword

Headgear
Power Sleeve
Feather Boots

Ramuh, Leviathan, Salamander, Fairy, Zodiac
Bizen Boat, Muramasa, Masamune



Roofiepops
Male
Virgo
48
51
Lancer
Throw
Hamedo
Short Charge
Ignore Height

Mythril Spear
Flame Shield
Grand Helmet
Leather Armor
Rubber Shoes

Level Jump8, Vertical Jump8
Shuriken, Spear



StealthModeLocke
Male
Aries
68
77
Mime

Arrow Guard
Long Status
Jump+2



Leather Hat
Power Sleeve
Setiemson

Mimic


Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Helpimabug
Male
Serpentarius
60
52
Bard
Charge
Arrow Guard
Long Status
Ignore Height

Ramia Harp

Holy Miter
Brigandine
Feather Boots

Life Song, Cheer Song, Battle Song, Last Song, Sky Demon
Charge+2



Ko2q
Male
Scorpio
46
58
Knight
White Magic
Mana Shield
Equip Gun
Move+3

Papyrus Codex
Diamond Shield
Bronze Helmet
Silk Robe
Magic Gauntlet

Head Break, Weapon Break, Power Break, Mind Break, Night Sword
Cure 2, Raise, Reraise, Shell 2, Wall, Esuna, Holy



Laserman1000
Male
Pisces
48
80
Samurai
Punch Art
Absorb Used MP
Dual Wield
Ignore Terrain

Muramasa
Heaven's Cloud
Gold Helmet
Genji Armor
Small Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Pummel, Secret Fist, Purification, Chakra, Revive, Seal Evil



Reinoe
Female
Leo
64
65
Dancer
Item
MP Restore
Throw Item
Move+2

Hydra Bag

Barette
Clothes
Feather Boots

Witch Hunt, Polka Polka
Potion, Holy Water

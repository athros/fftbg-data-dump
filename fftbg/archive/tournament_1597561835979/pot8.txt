Final Bets: white - 11 bets for 3,924G (19.7%, x4.07); champion - 7 bets for 15,954G (80.3%, x0.25)

white bets:
BirbBrainsBot: 1,000G (25.5%, 171,682G)
TheMurkGnome: 800G (20.4%, 7,264G)
blorpy_: 714G (18.2%, 17,870G)
ryjelsum: 300G (7.6%, 2,692G)
getthemoneyz: 210G (5.4%, 1,657,552G)
X0ras: 200G (5.1%, 1,147G)
datadrivenbot: 200G (5.1%, 64,278G)
DesertWooder: 200G (5.1%, 2,315G)
Drusiform: 100G (2.5%, 7,487G)
CT_5_Holy: 100G (2.5%, 4,162G)
McGooferson: 100G (2.5%, 250G)

champion bets:
reinoe: 15,000G (94.0%, 226,518G)
lowlf: 304G (1.9%, 1,958G)
AllInBot: 241G (1.5%, 241G)
thephunbaba: 108G (0.7%, 108G)
gorgewall: 101G (0.6%, 8,157G)
ko2q: 100G (0.6%, 4,223G)
Sans_from_Snowdin: 100G (0.6%, 33,033G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Just Here2
Female
Pisces
46
55
Priest
Charge
Damage Split
Secret Hunt
Move-HP Up

Healing Staff

Headgear
Mystic Vest
Setiemson

Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect, Shell, Shell 2, Esuna, Holy
Charge+4, Charge+5, Charge+7



Hamborn
Male
Leo
42
47
Mediator
Sing
Earplug
Magic Defense UP
Waterbreathing

Romanda Gun

Feather Hat
Chameleon Robe
Defense Ring

Invitation, Persuade, Praise, Preach
Angel Song, Life Song, Cheer Song, Nameless Song



CosmicTactician
Male
Sagittarius
59
44
Calculator
White Magic
MP Restore
Equip Armor
Jump+2

Iron Fan

Twist Headband
Mythril Armor
Magic Ring

Height, Prime Number, 4, 3
Cure, Cure 2, Cure 3, Reraise, Regen, Shell, Wall



Moonliquor
Male
Gemini
58
54
Bard
Item
Sunken State
Throw Item
Move-MP Up

Mythril Bow

Headgear
Leather Armor
Diamond Armlet

Angel Song, Battle Song, Nameless Song, Diamond Blade, Space Storage, Sky Demon
Potion, Ether, Echo Grass, Remedy

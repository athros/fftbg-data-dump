Player: !White
Team: White Team
Palettes: White/Blue



SeniorBunk
Male
Cancer
49
72
Lancer
Battle Skill
Meatbone Slash
Doublehand
Ignore Height

Gungnir

Platinum Helmet
Linen Cuirass
Defense Ring

Level Jump8, Vertical Jump5
Head Break, Speed Break, Power Break, Mind Break, Justice Sword



Killth3kid
Female
Cancer
58
40
Squire
Battle Skill
Earplug
Dual Wield
Waterwalking

Morning Star
Mythril Knife
Triangle Hat
Linen Cuirass
Rubber Shoes

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Fury
Shield Break, Stasis Sword, Justice Sword, Surging Sword



Lowlf
Male
Aries
48
55
Lancer
Sing
Distribute
Equip Knife
Levitate

Kunai
Mythril Shield
Mythril Helmet
Plate Mail
Dracula Mantle

Level Jump8, Vertical Jump2
Life Song, Cheer Song, Battle Song, Nameless Song



Chuckolator
Male
Capricorn
81
50
Geomancer
Jump
Counter Magic
Equip Bow
Waterwalking

Night Killer
Round Shield
Headgear
Silk Robe
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Level Jump4, Vertical Jump8

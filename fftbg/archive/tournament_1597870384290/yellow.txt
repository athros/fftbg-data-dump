Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Latebit
Female
Gemini
59
79
Monk
Steal
MP Restore
Equip Polearm
Jump+2

Octagon Rod

Red Hood
Power Sleeve
Feather Boots

Purification, Chakra, Revive
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim



E Ballard
Male
Virgo
65
76
Summoner
Basic Skill
Counter Magic
Magic Attack UP
Lava Walking

Papyrus Codex

Red Hood
Wizard Robe
Angel Ring

Moogle, Ramuh, Ifrit, Golem, Bahamut, Salamander, Silf
Accumulate, Heal, Tickle, Yell



Panushenko
Male
Leo
60
58
Calculator
Beast Skill
Absorb Used MP
Dual Wield
Swim

Musk Rod
Battle Bamboo
Bronze Helmet
Chain Mail
Cursed Ring

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest



Daveb
Female
Aries
52
74
Ninja
Charge
Meatbone Slash
Equip Knife
Fly

Dagger
Orichalcum
Green Beret
Brigandine
Cherche

Staff, Ninja Sword, Stick
Charge+3, Charge+4, Charge+7

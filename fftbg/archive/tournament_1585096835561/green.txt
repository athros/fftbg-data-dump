Player: !Green
Team: Green Team
Palettes: Green/White



HaateXIII
Male
Virgo
60
48
Calculator
Lucavi Skill
Regenerator
Defense UP
Waterbreathing

Bestiary
Gold Shield
Black Hood
Genji Armor
Red Shoes

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



ApplesauceBoss
Male
Scorpio
67
52
Time Mage
Charge
Parry
Sicken
Teleport

Gold Staff

Feather Hat
Chameleon Robe
Wizard Mantle

Haste 2, Slow, Stop, Immobilize, Quick, Stabilize Time, Meteor
Charge+5



Jethrothrul
Female
Pisces
40
78
Priest
Elemental
Parry
Defend
Teleport

Flame Whip

Twist Headband
Linen Robe
Battle Boots

Cure 4, Raise, Protect 2, Shell 2, Esuna
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball



Chuckolator
Female
Scorpio
80
79
Time Mage
Charge
Sunken State
Short Charge
Jump+2

White Staff

Headgear
Wizard Outfit
Reflect Ring

Haste, Float, Quick, Demi 2, Meteor
Charge+1, Charge+3

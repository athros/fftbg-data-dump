Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mizucrux
Female
Sagittarius
57
50
Time Mage
Basic Skill
Faith Up
Equip Shield
Waterbreathing

White Staff
Ice Shield
Green Beret
Silk Robe
Leather Mantle

Haste 2, Slow 2, Stop, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time
Accumulate, Wish



Kronikle
Female
Leo
60
65
Summoner
Time Magic
MA Save
Short Charge
Teleport

Wizard Rod

Red Hood
Mythril Vest
Germinas Boots

Moogle, Shiva, Titan, Carbunkle, Fairy
Haste, Slow 2, Stop, Immobilize, Stabilize Time



SkylerBunny
Male
Sagittarius
70
46
Geomancer
Draw Out
Abandon
Magic Defense UP
Ignore Terrain

Giant Axe
Round Shield
Black Hood
Leather Outfit
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Kiyomori



LOKITHUS
Female
Virgo
76
65
Summoner
Time Magic
Critical Quick
Short Charge
Waterbreathing

Rod

Feather Hat
Earth Clothes
Battle Boots

Ramuh, Titan, Golem, Carbunkle, Salamander, Fairy
Reflect, Demi, Demi 2, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Run With STONE GUNS
Monster
Leo
65
66
Byblos










Dymntd
Male
Libra
71
51
Geomancer
Item
Damage Split
Long Status
Jump+1

Battle Axe
Platinum Shield
Ribbon
Clothes
Leather Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Potion, Eye Drop



Volgrathemoose
Female
Libra
78
46
Wizard
Math Skill
PA Save
Equip Shield
Move+1

Orichalcum
Crystal Shield
Leather Hat
Secret Clothes
Wizard Mantle

Fire, Bolt, Bolt 3, Ice, Ice 3, Ice 4, Frog
CT, 5, 4



Hales Bopp It
Female
Capricorn
76
66
Ninja
Talk Skill
Counter
Equip Bow
Move+1

Hunting Bow
Bow Gun
Flash Hat
Judo Outfit
Wizard Mantle

Shuriken, Spear
Persuade, Praise, Preach, Death Sentence, Insult, Mimic Daravon, Refute

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Male
Aries
70
55
Time Mage
Sing
Caution
Dual Wield
Jump+1

Healing Staff
Wizard Staff
Headgear
Chameleon Robe
N-Kai Armlet

Slow, Immobilize, Reflect, Demi, Stabilize Time
Cheer Song, Battle Song, Last Song



Vorap
Female
Cancer
53
48
Squire
Jump
Sunken State
Short Charge
Move-MP Up

Platinum Sword
Round Shield
Headgear
Secret Clothes
Spike Shoes

Dash, Scream
Level Jump4, Vertical Jump6



UmaiJam
Female
Aquarius
64
48
Summoner
Battle Skill
Counter
Short Charge
Jump+1

Battle Folio

Thief Hat
White Robe
Dracula Mantle

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Bahamut, Leviathan, Silf, Lich
Shield Break, Weapon Break, Speed Break, Power Break



Serperemagus
Male
Virgo
47
78
Wizard
Time Magic
MA Save
Maintenance
Jump+1

Ice Rod

Twist Headband
Light Robe
N-Kai Armlet

Fire, Fire 2, Fire 3, Bolt 3, Ice 2, Ice 4
Haste, Haste 2, Slow 2, Stop, Reflect, Demi

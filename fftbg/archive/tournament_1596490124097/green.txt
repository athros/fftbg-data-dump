Player: !Green
Team: Green Team
Palettes: Green/White



Miku Shikhu
Female
Taurus
56
46
Wizard
Time Magic
Abandon
Short Charge
Jump+2

Thunder Rod

Red Hood
Adaman Vest
108 Gems

Ice, Ice 3, Ice 4
Haste, Slow, Reflect



Daveb
Male
Gemini
41
59
Ninja
Steal
Auto Potion
Secret Hunt
Teleport

Air Knife
Sasuke Knife
Red Hood
Adaman Vest
Defense Armlet

Bomb, Staff
Gil Taking, Leg Aim



Lokenwow
Female
Gemini
43
80
Samurai
Yin Yang Magic
Absorb Used MP
Doublehand
Swim

Kiyomori

Genji Helmet
Gold Armor
Defense Ring

Koutetsu, Murasame, Kiyomori, Kikuichimoji, Chirijiraden
Blind, Poison, Zombie, Foxbird, Dispel Magic



DesertWooder
Female
Pisces
64
44
Lancer
Yin Yang Magic
Regenerator
Magic Defense UP
Ignore Height

Obelisk
Gold Shield
Barbuta
Wizard Robe
Feather Mantle

Level Jump5, Vertical Jump7
Poison, Life Drain, Pray Faith, Zombie

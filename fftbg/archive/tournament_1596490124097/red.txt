Player: !Red
Team: Red Team
Palettes: Red/Brown



Butterbelljedi
Monster
Capricorn
41
49
Minotaur










Killth3kid
Male
Serpentarius
68
41
Knight
Elemental
Earplug
Short Charge
Ignore Height

Battle Axe
Ice Shield
Circlet
Chain Mail
Leather Mantle

Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Kronikle
Female
Pisces
66
63
Calculator
Black Magic
Absorb Used MP
Equip Axe
Jump+3

Wizard Staff

Green Beret
Leather Outfit
Power Wrist

Height, Prime Number
Bolt, Bolt 4, Ice, Ice 4, Empower, Death



Phrossi V
Female
Serpentarius
79
47
Archer
Yin Yang Magic
Meatbone Slash
Equip Knife
Jump+2

Ninja Edge
Crystal Shield
Thief Hat
Black Costume
Wizard Mantle

Charge+2, Charge+4
Pray Faith, Zombie, Foxbird, Dispel Magic

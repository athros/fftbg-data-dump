Final Bets: purple - 11 bets for 9,252G (46.5%, x1.15); brown - 8 bets for 10,630G (53.5%, x0.87)

purple bets:
JumbocactuarX27: 2,099G (22.7%, 2,099G)
Mesmaster: 2,000G (21.6%, 98,351G)
pplvee1: 1,750G (18.9%, 14,504G)
BirbBrainsBot: 1,000G (10.8%, 12,169G)
Error72: 1,000G (10.8%, 18,706G)
TheChainNerd: 524G (5.7%, 524G)
getthemoneyz: 278G (3.0%, 1,058,503G)
gorgewall: 201G (2.2%, 25,537G)
Evewho: 200G (2.2%, 7,064G)
RunicMagus: 100G (1.1%, 58,207G)
datadrivenbot: 100G (1.1%, 49,130G)

brown bets:
sparker9: 3,000G (28.2%, 16,960G)
E_Ballard: 2,157G (20.3%, 2,157G)
Nickyfive: 2,000G (18.8%, 25,647G)
Baron_von_Scrub: 1,111G (10.5%, 14,969G)
DustBirdEX: 971G (9.1%, 971G)
alterworlds: 575G (5.4%, 575G)
nifboy: 500G (4.7%, 5,439G)
Zagorsek: 316G (3.0%, 316G)

Final Bets: green - 10 bets for 7,367G (83.3%, x0.20); black - 6 bets for 1,481G (16.7%, x4.97)

green bets:
pplvee1: 3,627G (49.2%, 14,511G)
Nickyfive: 2,000G (27.1%, 23,651G)
TheChainNerd: 524G (7.1%, 524G)
Zagorsek: 316G (4.3%, 316G)
RunicMagus: 200G (2.7%, 58,167G)
CosmicTactician: 200G (2.7%, 21,414G)
extinctrational: 200G (2.7%, 1,063G)
maakur_: 100G (1.4%, 77,503G)
nifboy: 100G (1.4%, 4,839G)
datadrivenbot: 100G (1.4%, 49,167G)

black bets:
BirbBrainsBot: 336G (22.7%, 15,240G)
sparker9: 300G (20.3%, 12,460G)
Chronolynx42: 300G (20.3%, 49,567G)
benticore: 250G (16.9%, 2,102G)
getthemoneyz: 194G (13.1%, 1,059,902G)
gorgewall: 101G (6.8%, 25,808G)

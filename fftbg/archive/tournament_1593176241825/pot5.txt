Final Bets: red - 8 bets for 2,260G (18.3%, x4.45); green - 10 bets for 10,062G (81.7%, x0.22)

red bets:
E_Ballard: 652G (28.8%, 652G)
DustBirdEX: 412G (18.2%, 412G)
gorgewall: 301G (13.3%, 25,768G)
benticore: 250G (11.1%, 2,352G)
alterworlds: 244G (10.8%, 244G)
Rytor: 200G (8.8%, 7,605G)
Baron_von_Scrub: 101G (4.5%, 13,858G)
nifboy: 100G (4.4%, 4,939G)

green bets:
JumbocactuarX27: 4,511G (44.8%, 4,511G)
pplvee1: 2,477G (24.6%, 16,515G)
TheChainNerd: 1,126G (11.2%, 1,126G)
BirbBrainsBot: 1,000G (9.9%, 13,318G)
Chronolynx42: 300G (3.0%, 48,652G)
RunicMagus: 200G (2.0%, 58,322G)
Evewho: 200G (2.0%, 7,294G)
datadrivenbot: 100G (1.0%, 49,245G)
maakur_: 100G (1.0%, 77,311G)
getthemoneyz: 48G (0.5%, 1,058,822G)

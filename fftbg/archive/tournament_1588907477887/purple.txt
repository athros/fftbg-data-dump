Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



0v3rr8d
Female
Taurus
47
76
Mime

Caution
Martial Arts
Jump+1



Flash Hat
Gold Armor
N-Kai Armlet

Mimic




HorusTaurus
Female
Sagittarius
51
77
Ninja
Talk Skill
Caution
Beastmaster
Ignore Terrain

Dagger
Mage Masher
Feather Hat
Mystic Vest
Small Mantle

Bomb
Invitation, Threaten, Solution, Negotiate, Mimic Daravon, Refute



Heroebal
Male
Aries
66
77
Monk
Yin Yang Magic
Mana Shield
Beastmaster
Ignore Terrain



Black Hood
Judo Outfit
Jade Armlet

Purification, Revive
Blind, Pray Faith, Doubt Faith, Silence Song, Foxbird, Confusion Song



Galkife
Male
Aries
61
60
Bard
Yin Yang Magic
Caution
Secret Hunt
Ignore Terrain

Long Bow

Twist Headband
Judo Outfit
Angel Ring

Angel Song, Battle Song, Diamond Blade
Blind, Confusion Song, Dispel Magic, Paralyze, Sleep

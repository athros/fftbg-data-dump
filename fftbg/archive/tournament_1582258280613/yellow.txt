Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dragoon ZERO
Male
Serpentarius
63
71
Priest
Jump
Earplug
Equip Gun
Teleport

Papyrus Codex

Green Beret
Chameleon Robe
Sprint Shoes

Cure 3, Cure 4, Raise 2, Reraise, Regen, Protect 2, Shell, Shell 2, Esuna, Holy
Level Jump4, Vertical Jump3



Windsah
Male
Pisces
70
61
Bard
Charge
Mana Shield
Maintenance
Fly

Bloody Strings

Red Hood
Linen Cuirass
Rubber Shoes

Magic Song, Nameless Song, Diamond Blade
Charge+1, Charge+5, Charge+7, Charge+20



Gabbagooluigi
Male
Cancer
44
41
Samurai
Elemental
Damage Split
Magic Attack UP
Retreat

Javelin

Platinum Helmet
Light Robe
Defense Ring

Asura, Koutetsu, Bizen Boat, Kiyomori, Chirijiraden
Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Leakimiko
Male
Scorpio
50
38
Lancer
Summon Magic
Mana Shield
Doublehand
Lava Walking

Obelisk

Platinum Helmet
White Robe
Angel Ring

Level Jump4, Vertical Jump4
Ramuh, Ifrit, Golem, Bahamut, Odin, Fairy

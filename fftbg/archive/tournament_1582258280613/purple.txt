Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



MoonSlayerRS
Male
Aquarius
77
77
Samurai
Elemental
Auto Potion
Short Charge
Move+2

Bizen Boat

Barbuta
Genji Armor
Salty Rage

Bizen Boat, Heaven's Cloud, Muramasa
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Thefyeman
Female
Cancer
51
44
Dancer
Draw Out
Speed Save
Magic Attack UP
Move-HP Up

Blind Knife

Ribbon
Leather Outfit
Defense Armlet

Wiznaibus, Polka Polka, Nameless Dance, Nether Demon
Koutetsu, Bizen Boat, Kikuichimoji



VySaika
Female
Virgo
58
53
Monk
Battle Skill
Counter Flood
Long Status
Lava Walking



Green Beret
Clothes
Dracula Mantle

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Armor Break, Magic Break, Speed Break, Power Break



SkitchTheWolf
Male
Virgo
44
58
Mediator
White Magic
Brave Up
Magic Attack UP
Ignore Height

Mythril Gun

Feather Hat
Brigandine
Salty Rage

Praise, Insult
Cure 3, Raise, Raise 2, Shell 2, Esuna

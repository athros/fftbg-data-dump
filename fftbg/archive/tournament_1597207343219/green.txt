Player: !Green
Team: Green Team
Palettes: Green/White



Shalloween
Male
Libra
79
61
Lancer
Punch Art
Absorb Used MP
Concentrate
Waterbreathing

Spear
Kaiser Plate
Platinum Helmet
Diamond Armor
Feather Boots

Level Jump8, Vertical Jump7
Spin Fist, Pummel, Earth Slash, Purification



Phi Sig
Male
Leo
71
60
Squire
White Magic
Earplug
Doublehand
Jump+3

Dagger

Headgear
Mythril Armor
Leather Mantle

Accumulate, Yell
Cure 4, Raise, Raise 2, Shell



Nizaha
Male
Sagittarius
73
49
Thief
Throw
Parry
Doublehand
Retreat

Mage Masher

Thief Hat
Mystic Vest
Cursed Ring

Steal Heart, Steal Helmet, Steal Accessory, Steal Status, Arm Aim
Shuriken, Spear, Wand, Dictionary



Kysune
Female
Gemini
51
69
Dancer
Summon Magic
HP Restore
Attack UP
Swim

Ryozan Silk

Golden Hairpin
Mythril Vest
Feather Mantle

Polka Polka, Nether Demon, Dragon Pit
Ifrit, Titan, Carbunkle, Silf, Lich

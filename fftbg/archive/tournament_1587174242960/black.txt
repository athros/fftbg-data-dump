Player: !Black
Team: Black Team
Palettes: Black/Red



Urieltheflameofgod
Female
Leo
59
72
Wizard
Summon Magic
Counter Flood
Short Charge
Lava Walking

Flame Rod

Red Hood
Earth Clothes
Diamond Armlet

Fire, Fire 2, Bolt, Bolt 2, Ice 4, Flare
Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Salamander, Fairy, Cyclops



Fluffywormhole
Female
Gemini
73
69
Priest
Throw
Absorb Used MP
Maintenance
Move+2

Oak Staff

Holy Miter
Wizard Robe
Battle Boots

Cure, Reraise, Protect, Protect 2, Shell
Shuriken, Bomb, Spear



Zeroroute
Monster
Cancer
76
62
Ghoul










CapnChaos12
Male
Scorpio
65
63
Archer
Talk Skill
Counter Magic
Doublehand
Jump+3

Blaze Gun

Triangle Hat
Mystic Vest
Elf Mantle

Charge+1, Charge+3, Charge+4, Charge+20
Persuade, Death Sentence, Refute

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jeeboheebo
Female
Cancer
73
50
Summoner
Elemental
Arrow Guard
Short Charge
Move+2

Poison Rod

Green Beret
Leather Outfit
Leather Mantle

Moogle, Shiva, Ramuh, Golem, Carbunkle, Leviathan, Salamander
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Fluffywormhole
Male
Aquarius
58
72
Priest
Charge
Counter Flood
Defense UP
Move+1

Flail

Leather Hat
Chameleon Robe
Reflect Ring

Cure, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell
Charge+5



Aldrammech
Female
Virgo
65
81
Oracle
Time Magic
Counter Flood
Defense UP
Jump+3

Bestiary

Triangle Hat
Black Robe
Defense Ring

Blind, Poison, Spell Absorb, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep
Haste, Stop, Float, Stabilize Time



XFallenDevilx
Male
Taurus
63
40
Samurai
Time Magic
Abandon
Martial Arts
Waterwalking



Barbuta
Carabini Mail
Leather Mantle

Koutetsu, Bizen Boat, Kikuichimoji
Haste, Demi, Demi 2, Stabilize Time

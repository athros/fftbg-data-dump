Final Bets: white - 31 bets for 47,580G (78.1%, x0.28); black - 17 bets for 13,309G (21.9%, x3.58)

white bets:
Evewho: 9,008G (18.9%, 18,017G)
Sairentozon7: 4,754G (10.0%, 4,754G)
reinoe: 4,532G (9.5%, 4,532G)
Lydian_C: 3,400G (7.1%, 14,587G)
Mesmaster: 3,000G (6.3%, 65,754G)
Theseawolf1: 2,000G (4.2%, 9,065G)
Spartan_Paladin: 2,000G (4.2%, 52,058G)
SquidgyXom: 1,935G (4.1%, 9,675G)
WireLord: 1,896G (4.0%, 3,793G)
SkylerBunny: 1,600G (3.4%, 57,624G)
Shalloween: 1,588G (3.3%, 1,588G)
rico_flex: 1,500G (3.2%, 32,110G)
gorgewall: 1,014G (2.1%, 1,014G)
Aldrammech: 1,000G (2.1%, 3,726G)
Lord_Burrah: 1,000G (2.1%, 105,577G)
Lionhermit: 1,000G (2.1%, 23,448G)
midori_ribbon: 768G (1.6%, 768G)
HaplessOne: 666G (1.4%, 48,466G)
getthemoneyz: 588G (1.2%, 610,451G)
SarrgeQc: 501G (1.1%, 2,115G)
Kronikle: 500G (1.1%, 4,974G)
LazarusAjin: 500G (1.1%, 20,995G)
Zeroroute: 500G (1.1%, 500G)
Setdevildog: 500G (1.1%, 1,882G)
Breakdown777: 500G (1.1%, 17,004G)
helpimabug: 497G (1.0%, 994G)
RunicMagus: 400G (0.8%, 50,458G)
reshiramdude16: 133G (0.3%, 133G)
ANFz: 100G (0.2%, 33,181G)
datadrivenbot: 100G (0.2%, 14,511G)
brokenknight201: 100G (0.2%, 201G)

black bets:
killth3kid: 2,000G (15.0%, 38,151G)
superdevon1: 1,536G (11.5%, 1,536G)
RestIessNight: 1,124G (8.4%, 1,124G)
KasugaiRoastedPeas: 1,100G (8.3%, 1,100G)
HaychDub: 1,010G (7.6%, 5,435G)
ungabunga_bot: 1,000G (7.5%, 336,599G)
moonliquor: 1,000G (7.5%, 8,168G)
BirbBrainsBot: 1,000G (7.5%, 8,121G)
OmnibotGamma: 960G (7.2%, 960G)
CoderCalamity: 724G (5.4%, 724G)
itsdigitalbro: 724G (5.4%, 724G)
TheFALLofLindsay: 500G (3.8%, 9,172G)
Chuckolator: 231G (1.7%, 6,932G)
Firesheath: 100G (0.8%, 13,644G)
kaidykat: 100G (0.8%, 1,738G)
maakur_: 100G (0.8%, 129,441G)
thalessenador: 100G (0.8%, 100G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Nickelbank
Female
Capricorn
53
68
Squire
Charge
Caution
Maintenance
Jump+2

Poison Bow
Crystal Shield
Feather Hat
Leather Outfit
Germinas Boots

Accumulate, Heal, Cheer Up
Charge+3, Charge+5, Charge+7, Charge+10



Grininda
Male
Aries
49
53
Archer
Elemental
Regenerator
Doublehand
Move+3

Long Bow

Platinum Helmet
Brigandine
Magic Ring

Charge+4, Charge+10
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



TeaTime29
Female
Capricorn
46
75
Monk
Battle Skill
Sunken State
Halve MP
Teleport



Red Hood
Earth Clothes
Wizard Mantle

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Head Break, Weapon Break, Justice Sword, Night Sword



ShintaroNayaka
Female
Libra
40
77
Calculator
Yin Yang Magic
Meatbone Slash
Equip Axe
Move+3

Morning Star

Golden Hairpin
Silk Robe
Genji Gauntlet

CT, Prime Number, 3
Blind, Spell Absorb, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Digitalsocrates
Female
Pisces
79
60
Samurai
Steal
Meatbone Slash
Magic Attack UP
Teleport 2

Koutetsu Knife

Mythril Helmet
Gold Armor
Battle Boots

Koutetsu, Murasame, Muramasa
Steal Shield



P0jothen00b
Male
Taurus
75
37
Archer
Jump
Distribute
Magic Attack UP
Jump+3

Bow Gun
Gold Shield
Triangle Hat
Power Sleeve
Defense Armlet

Charge+1, Charge+2, Charge+5, Charge+20
Level Jump5, Vertical Jump8



Linkn
Male
Leo
63
72
Geomancer
Summon Magic
Distribute
Defend
Move+1

Giant Axe
Genji Shield
Red Hood
Chain Vest
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard
Moogle, Carbunkle, Salamander, Cyclops



Kixandra
Female
Virgo
52
64
Summoner
Dance
Meatbone Slash
Dual Wield
Jump+1

Poison Rod
Wizard Rod
Green Beret
Chain Vest
Wizard Mantle

Moogle, Ramuh, Ifrit, Golem, Carbunkle
Wiznaibus, Disillusion, Void Storage

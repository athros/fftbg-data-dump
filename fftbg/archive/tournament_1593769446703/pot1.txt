Final Bets: red - 10 bets for 6,705G (65.4%, x0.53); blue - 6 bets for 3,554G (34.6%, x1.89)

red bets:
Mesmaster: 2,000G (29.8%, 47,658G)
ShintaroNayaka: 1,511G (22.5%, 1,511G)
BirbBrainsBot: 1,000G (14.9%, 165,202G)
fluffskull: 500G (7.5%, 2,325G)
killth3kid: 444G (6.6%, 115,895G)
ColetteMSLP: 300G (4.5%, 1,888G)
prince_rogers_nelson_: 300G (4.5%, 300G)
Lythe_Caraker: 250G (3.7%, 143,294G)
datadrivenbot: 200G (3.0%, 52,766G)
readdesert: 200G (3.0%, 1,144G)

blue bets:
Jeeboheebo: 1,000G (28.1%, 12,681G)
getthemoneyz: 1,000G (28.1%, 1,170,235G)
Evewho: 1,000G (28.1%, 8,094G)
DustBirdEX: 234G (6.6%, 1,750G)
Digitalsocrates: 200G (5.6%, 1,476G)
P0jothen00b: 120G (3.4%, 120G)

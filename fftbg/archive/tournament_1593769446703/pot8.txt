Final Bets: white - 9 bets for 9,791G (80.0%, x0.25); champion - 8 bets for 2,452G (20.0%, x3.99)

white bets:
YaBoy125: 3,160G (32.3%, 6,320G)
MrFlabyo: 3,000G (30.6%, 362,916G)
Lanshaft: 1,267G (12.9%, 1,267G)
Lythe_Caraker: 1,000G (10.2%, 146,114G)
Dexsana: 464G (4.7%, 464G)
Magicandy: 300G (3.1%, 25,297G)
ColetteMSLP: 300G (3.1%, 2,509G)
datadrivenbot: 200G (2.0%, 52,682G)
Quadh0nk: 100G (1.0%, 1,307G)

champion bets:
BirbBrainsBot: 1,000G (40.8%, 165,281G)
Tugboat1: 400G (16.3%, 6,554G)
prince_rogers_nelson_: 300G (12.2%, 300G)
DustBirdEX: 234G (9.5%, 3,225G)
getthemoneyz: 214G (8.7%, 1,172,064G)
shannahan45: 104G (4.2%, 104G)
Ring_Wyrm: 100G (4.1%, 5,509G)
Kixandra: 100G (4.1%, 100G)

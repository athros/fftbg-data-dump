Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Twelfthrootoftwo
Male
Gemini
62
67
Archer
Sing
MA Save
Dual Wield
Lava Walking

Lightning Bow

Gold Helmet
Brigandine
Diamond Armlet

Charge+2, Charge+10, Charge+20
Cheer Song, Battle Song, Space Storage



Djorama
Female
Aquarius
67
81
Archer
Punch Art
Counter Magic
Attack UP
Ignore Terrain

Snipe Bow
Escutcheon
Crystal Helmet
Chain Vest
Small Mantle

Charge+2, Charge+5, Charge+10
Wave Fist, Earth Slash, Secret Fist, Purification



Mesmaster
Male
Sagittarius
50
38
Knight
Throw
MP Restore
Short Status
Move-HP Up

Slasher
Round Shield
Mythril Helmet
Robe of Lords
Sprint Shoes

Power Break, Justice Sword, Dark Sword
Shuriken, Bomb



Jeeboheebo
Male
Pisces
54
43
Monk
Elemental
Counter
Dual Wield
Fly



Golden Hairpin
Leather Outfit
N-Kai Armlet

Purification, Revive
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Lava Ball

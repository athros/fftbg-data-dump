Player: !Green
Team: Green Team
Palettes: Green/White



JumbocactuarX27
Female
Scorpio
61
53
Squire
Black Magic
Regenerator
Doublehand
Lava Walking

Coral Sword

Circlet
Reflect Mail
Bracer

Accumulate, Heal, Tickle, Yell
Bolt 3, Ice 2, Ice 4, Empower



Baron Von Scrub
Male
Aries
51
54
Time Mage
Elemental
MP Restore
Attack UP
Ignore Terrain

Bestiary

Feather Hat
Judo Outfit
Angel Ring

Haste, Slow 2, Stabilize Time
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



ThePanda1016
Monster
Libra
71
39
Malboro










Phi Sig
Male
Capricorn
51
44
Chemist
Draw Out
MA Save
Equip Axe
Swim

Flail

Triangle Hat
Rubber Costume
Sprint Shoes

Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Holy Water, Phoenix Down
Koutetsu

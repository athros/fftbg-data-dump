Player: !Black
Team: Black Team
Palettes: Black/Red



Zeroroute
Monster
Virgo
76
77
Black Chocobo










DHaveWord
Monster
Libra
52
44
Holy Dragon










Bryan792
Male
Capricorn
72
66
Archer
Elemental
Sunken State
Doublehand
Lava Walking

Stone Gun

Barbuta
Chain Vest
Bracer

Charge+2, Charge+3, Charge+4
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Lunaeriss
Female
Capricorn
41
39
Squire
Charge
Arrow Guard
Short Status
Jump+2

Flail
Flame Shield
Genji Helmet
Gold Armor
Small Mantle

Accumulate, Heal, Tickle, Yell, Fury
Charge+1, Charge+4, Charge+5, Charge+7, Charge+10

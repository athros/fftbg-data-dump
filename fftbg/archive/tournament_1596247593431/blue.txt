Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dem0nj0ns
Female
Capricorn
51
75
Oracle
White Magic
Counter Flood
Maintenance
Ignore Terrain

Battle Bamboo

Holy Miter
Clothes
Small Mantle

Blind, Poison, Life Drain, Silence Song, Dispel Magic
Raise, Protect, Wall, Esuna, Holy



Ribbiks
Female
Gemini
49
78
Archer
Steal
Abandon
Equip Knife
Move+3

Short Edge
Genji Shield
Holy Miter
Wizard Outfit
Magic Gauntlet

Charge+7, Charge+10
Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Leg Aim



CosmicTactician
Female
Cancer
61
57
Oracle
Elemental
HP Restore
Doublehand
Ignore Terrain

Battle Bamboo

Black Hood
Black Robe
Feather Mantle

Blind, Spell Absorb, Pray Faith, Zombie, Confusion Song, Dispel Magic, Sleep
Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Resjudicata3
Male
Leo
61
52
Time Mage
Basic Skill
Abandon
Attack UP
Ignore Height

Healing Staff

Red Hood
Mystic Vest
Power Wrist

Haste, Slow 2, Reflect, Stabilize Time
Heal, Yell, Scream, Ultima

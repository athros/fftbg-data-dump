Player: !Red
Team: Red Team
Palettes: Red/Brown



DavenIII
Female
Leo
65
61
Oracle
Time Magic
Parry
Equip Axe
Move+3

Giant Axe

Golden Hairpin
Chameleon Robe
108 Gems

Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic
Slow, Float, Quick



Chuckolator
Male
Cancer
54
70
Archer
Summon Magic
Absorb Used MP
Long Status
Move+1

Cross Bow
Platinum Shield
Thief Hat
Mythril Vest
Cursed Ring

Charge+3, Charge+7
Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Salamander, Fairy



ThePineappleSalesman
Monster
Scorpio
47
77
Ghoul










Ra1kia
Male
Gemini
53
49
Priest
Basic Skill
Absorb Used MP
Equip Gun
Ignore Height

Ramia Harp

Holy Miter
Chameleon Robe
Magic Ring

Cure, Cure 3, Raise, Raise 2, Reraise, Esuna
Throw Stone, Heal, Yell, Cheer Up

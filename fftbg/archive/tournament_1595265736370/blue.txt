Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JumbocactuarX27
Male
Scorpio
66
40
Knight
Draw Out
Sunken State
Equip Armor
Teleport 2

Defender
Gold Shield
Thief Hat
Mystic Vest
Magic Gauntlet

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword
Koutetsu



Error72
Female
Serpentarius
77
41
Geomancer
Draw Out
Speed Save
Equip Sword
Move-MP Up

Murasame
Aegis Shield
Thief Hat
Black Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Heaven's Cloud, Kiyomori



Xoomwaffle
Male
Cancer
52
73
Wizard
Draw Out
Critical Quick
Defend
Move+2

Assassin Dagger

Holy Miter
Wizard Outfit
N-Kai Armlet

Fire, Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice
Asura, Koutetsu, Murasame, Heaven's Cloud



Chuckolator
Female
Gemini
65
69
Geomancer
Steal
Caution
Secret Hunt
Move+2

Rune Blade
Gold Shield
Ribbon
Chain Vest
Magic Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Armor, Steal Status

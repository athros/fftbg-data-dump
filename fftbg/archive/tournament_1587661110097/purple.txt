Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



MirrorTwin
Male
Gemini
58
75
Calculator
Yin Yang Magic
Caution
Martial Arts
Ignore Terrain



Flash Hat
Chain Vest
Bracer

CT, Height, 3
Blind, Poison, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep



StealthModeLocke
Female
Scorpio
72
41
Squire
Item
Caution
Halve MP
Swim

Flame Whip
Buckler
Feather Hat
Leather Outfit
Angel Ring

Dash, Tickle, Yell, Cheer Up, Wish
Potion, Hi-Ether, Phoenix Down



Rogueain
Male
Aquarius
57
55
Lancer
Yin Yang Magic
Auto Potion
Equip Axe
Jump+1

Rainbow Staff
Buckler
Iron Helmet
Crystal Mail
Feather Mantle

Level Jump5, Vertical Jump7
Blind, Spell Absorb, Pray Faith



ImXT
Female
Pisces
71
77
Chemist
Elemental
Counter
Attack UP
Teleport

Hydra Bag

Twist Headband
Chain Vest
Genji Gauntlet

Potion, X-Potion, Antidote, Eye Drop, Remedy, Phoenix Down
Water Ball, Hell Ivy, Local Quake, Blizzard, Lava Ball

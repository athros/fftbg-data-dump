Player: !zChamp
Team: Champion Team
Palettes: Green/White



Fenaen
Female
Pisces
51
69
Priest
Battle Skill
Counter Magic
Equip Knife
Swim

Dragon Rod

Flash Hat
Judo Outfit
Magic Gauntlet

Cure 2, Cure 3, Raise, Reraise, Regen, Shell, Shell 2, Holy
Head Break, Shield Break, Weapon Break, Speed Break, Mind Break, Dark Sword



Roofiepops
Female
Virgo
73
62
Ninja
Yin Yang Magic
Mana Shield
Magic Defense UP
Teleport

Flail
Spell Edge
Holy Miter
Chain Vest
N-Kai Armlet

Shuriken, Bomb, Sword, Staff, Stick, Wand, Dictionary
Poison, Life Drain, Pray Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic



GreatRedDragon
Male
Capricorn
77
57
Chemist
Sing
Speed Save
Defend
Teleport

Panther Bag

Leather Hat
Mystic Vest
Genji Gauntlet

Potion, X-Potion, Ether, Antidote, Echo Grass, Soft, Phoenix Down
Life Song, Cheer Song, Hydra Pit



LDSkinny
Female
Virgo
52
74
Summoner
Black Magic
Sunken State
Short Charge
Move+2

Thunder Rod

Black Hood
Wizard Robe
Rubber Shoes

Moogle, Ifrit, Titan, Carbunkle
Fire 4, Bolt, Bolt 2, Bolt 4, Ice 3, Ice 4, Empower

Player: !Black
Team: Black Team
Palettes: Black/Red



JonnyCue
Male
Cancer
56
57
Monk
Talk Skill
Counter Tackle
Concentrate
Retreat



Cachusha
Black Costume
Leather Mantle

Pummel, Wave Fist, Secret Fist, Chakra, Revive
Invitation, Praise, Insult



RagequitSA
Monster
Virgo
80
45
Grenade










Galkife
Male
Sagittarius
68
42
Bard
Talk Skill
Regenerator
Equip Knife
Move+1

Poison Rod

Red Hood
Gold Armor
Defense Ring

Angel Song, Cheer Song, Battle Song, Sky Demon
Invitation, Death Sentence, Refute



HippieDota
Male
Taurus
56
73
Wizard
Basic Skill
Distribute
Short Charge
Move+2

Main Gauche

Twist Headband
Light Robe
108 Gems

Bolt 3, Empower, Death
Dash, Heal, Tickle

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Wickslee
Male
Cancer
66
46
Monk
Black Magic
Mana Shield
Defense UP
Jump+2



Green Beret
Leather Outfit
Bracer

Pummel, Revive
Fire, Fire 2, Ice 3, Ice 4



Boojob
Male
Libra
79
69
Ninja
Sing
Earplug
Attack UP
Move-HP Up

Short Edge
Hidden Knife
Holy Miter
Wizard Outfit
Reflect Ring

Knife, Wand
Hydra Pit



Jigglefluffenstuff
Male
Aries
44
78
Archer
Draw Out
Regenerator
Equip Armor
Jump+1

Night Killer
Round Shield
Thief Hat
Carabini Mail
Angel Ring

Charge+1, Charge+4, Charge+5, Charge+7
Asura



Shalloween
Male
Libra
48
46
Calculator
White Magic
Dragon Spirit
Concentrate
Ignore Height

Octagon Rod

Triangle Hat
Black Robe
Power Wrist

CT, Prime Number, 4
Cure 2, Raise, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



Krasny1944
Female
Aquarius
77
57
Time Mage
Summon Magic
MA Save
Attack UP
Teleport

Rainbow Staff

Red Hood
Wizard Robe
Germinas Boots

Haste, Slow, Demi, Demi 2
Ramuh, Ifrit, Odin, Leviathan, Silf



DLJuggernaut
Male
Virgo
50
73
Geomancer
White Magic
Blade Grasp
Equip Axe
Ignore Height

Rainbow Staff
Buckler
Golden Hairpin
Leather Outfit
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Raise, Protect, Protect 2, Shell 2, Wall, Esuna



Baconbacon1207
Female
Sagittarius
62
54
Dancer
Black Magic
Brave Save
Equip Gun
Jump+3

Mythril Gun

Headgear
Earth Clothes
Elf Mantle

Wiznaibus, Polka Polka, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit
Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 2, Ice 4, Empower, Frog, Death



Kyune
Male
Virgo
51
50
Samurai
Item
HP Restore
Sicken
Waterbreathing

Kiyomori

Circlet
Mythril Armor
Leather Mantle

Koutetsu, Bizen Boat, Murasame
Potion, X-Potion, Maiden's Kiss, Holy Water, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



RoachCarnival
Male
Gemini
62
47
Lancer
Sing
Damage Split
Equip Polearm
Move-MP Up

Javelin
Venetian Shield
Gold Helmet
Black Robe
Defense Ring

Level Jump2, Vertical Jump5
Angel Song, Life Song, Battle Song, Magic Song, Nameless Song



Narcius
Female
Scorpio
54
62
Thief
Black Magic
Distribute
Equip Gun
Teleport

Battle Folio

Leather Hat
Chain Vest
Magic Ring

Steal Heart, Steal Armor, Steal Weapon, Steal Status
Fire 4, Bolt, Bolt 3, Ice 2, Empower, Flare



Buddychrist10
Male
Taurus
71
49
Priest
Elemental
Parry
Attack UP
Move-HP Up

Healing Staff

Holy Miter
Mythril Vest
Battle Boots

Cure, Raise 2, Reraise, Protect, Shell, Shell 2, Wall, Esuna, Magic Barrier
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



IcePattern
Male
Cancer
44
64
Bard
Throw
Counter Tackle
Equip Axe
Waterwalking

White Staff

Red Hood
Earth Clothes
Feather Boots

Life Song, Cheer Song, Magic Song, Last Song, Hydra Pit
Knife, Hammer

Final Bets: white - 11 bets for 6,702G (58.9%, x0.70); black - 8 bets for 4,686G (41.1%, x1.43)

white bets:
Lawndough: 1,692G (25.2%, 1,692G)
reinoe: 1,000G (14.9%, 9,083G)
Mesmaster: 1,000G (14.9%, 79,710G)
getthemoneyz: 712G (10.6%, 687,566G)
lowlf: 710G (10.6%, 710G)
HASTERIOUS: 691G (10.3%, 13,826G)
vorap: 300G (4.5%, 81,021G)
Digitalsocrates: 260G (3.9%, 260G)
serperemagus: 137G (2.0%, 3,249G)
nifboy: 100G (1.5%, 5,113G)
datadrivenbot: 100G (1.5%, 22,725G)

black bets:
CorpusCav: 2,000G (42.7%, 12,835G)
BirbBrainsBot: 1,000G (21.3%, 75,794G)
Zachara: 986G (21.0%, 85,986G)
Nerf_Vergil: 200G (4.3%, 388G)
solomongrundy85: 200G (4.3%, 2,332G)
GnielKnows: 150G (3.2%, 5,328G)
mardokttv: 100G (2.1%, 1,037G)
AmaninAmide: 50G (1.1%, 2,674G)

Player: !Green
Team: Green Team
Palettes: Green/White



Arch8000
Female
Leo
79
37
Lancer
Black Magic
Arrow Guard
Concentrate
Move+1

Dragon Whisker
Diamond Shield
Grand Helmet
Linen Cuirass
Salty Rage

Level Jump8, Vertical Jump4
Fire, Fire 2, Bolt, Empower



LDSkinny
Female
Aries
63
56
Summoner
Draw Out
Dragon Spirit
Short Charge
Move+2

Poison Rod

Feather Hat
Black Robe
Defense Ring

Moogle, Titan, Carbunkle, Bahamut, Odin, Salamander, Fairy, Cyclops
Koutetsu, Kiyomori, Kikuichimoji



HaychDub
Female
Scorpio
69
54
Summoner
Yin Yang Magic
Counter
Short Charge
Jump+3

Rainbow Staff

Twist Headband
Black Costume
Angel Ring

Moogle, Carbunkle, Salamander, Silf, Fairy, Cyclops
Blind Rage, Dispel Magic, Paralyze



Boojob
Female
Gemini
62
46
Summoner
Yin Yang Magic
Regenerator
Short Charge
Ignore Terrain

Wizard Rod

Feather Hat
Silk Robe
108 Gems

Ifrit, Titan, Golem, Carbunkle, Bahamut, Odin, Silf, Lich
Blind, Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Petrify, Dark Holy

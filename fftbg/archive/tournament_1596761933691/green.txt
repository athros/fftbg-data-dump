Player: !Green
Team: Green Team
Palettes: Green/White



Coralreeferz
Male
Cancer
51
74
Mediator
Jump
Meatbone Slash
Dual Wield
Lava Walking

Blast Gun
Glacier Gun
Black Hood
Brigandine
Magic Gauntlet

Invitation, Persuade, Death Sentence, Insult, Refute, Rehabilitate
Level Jump2, Vertical Jump2



Pplvee1
Female
Leo
67
71
Chemist
Steal
Earplug
Equip Gun
Jump+3

Stone Gun

Thief Hat
Chain Vest
Diamond Armlet

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Steal Heart, Steal Shield, Steal Accessory, Leg Aim



RunicMagus
Male
Pisces
54
69
Thief
Jump
Absorb Used MP
Equip Axe
Move-HP Up

Slasher

Green Beret
Earth Clothes
Defense Ring

Steal Heart, Steal Armor, Steal Status, Arm Aim, Leg Aim
Level Jump8, Vertical Jump8



StealthModeLocke
Female
Serpentarius
48
70
Summoner
Punch Art
Dragon Spirit
Defense UP
Jump+1

Dragon Rod

Flash Hat
Linen Robe
Genji Gauntlet

Moogle, Titan, Golem, Leviathan, Fairy, Lich
Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Seal Evil

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nickyfive
Female
Scorpio
57
76
Summoner
Elemental
Abandon
Short Charge
Jump+2

Dragon Rod

Golden Hairpin
Wizard Outfit
Sprint Shoes

Ramuh, Titan, Golem, Carbunkle, Silf, Lich
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Dantetouhou
Monster
Aries
74
51
Dark Behemoth










Oogthecaveman
Female
Cancer
68
72
Monk
Steal
Counter
Short Status
Move+3



Twist Headband
Power Sleeve
Spike Shoes

Pummel, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil
Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Daveb
Male
Pisces
57
55
Ninja
Battle Skill
Sunken State
Equip Axe
Jump+3

Flail
Flail
Twist Headband
Wizard Outfit
Chantage

Shuriken, Wand
Head Break, Speed Break, Mind Break

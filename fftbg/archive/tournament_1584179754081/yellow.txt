Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Powergems
Male
Sagittarius
58
67
Bard
Draw Out
Parry
Equip Sword
Swim

Defender

Golden Hairpin
Gold Armor
108 Gems

Life Song, Cheer Song, Hydra Pit
Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



WhiteDog420
Male
Serpentarius
63
47
Samurai
Item
MP Restore
Equip Shield
Move+3

Heaven's Cloud
Bronze Shield
Leather Helmet
Chain Mail
Genji Gauntlet

Koutetsu, Murasame, Kikuichimoji
Potion, Maiden's Kiss, Holy Water, Phoenix Down



Egil72
Female
Libra
62
73
Summoner
Black Magic
Arrow Guard
Martial Arts
Waterwalking



Flash Hat
Black Costume
Leather Mantle

Moogle, Ramuh, Titan, Carbunkle, Lich
Fire, Fire 3, Fire 4, Bolt 2, Ice 3, Ice 4, Frog



Darthchi71
Male
Virgo
74
78
Calculator
Time Magic
Counter Magic
Attack UP
Jump+3

Bestiary

Red Hood
Black Robe
Genji Gauntlet

CT, Height, Prime Number, 4, 3
Haste, Slow 2, Stop, Float, Reflect, Demi 2, Stabilize Time, Meteor

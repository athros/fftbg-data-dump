Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Club Sodar
Male
Aries
57
75
Thief
Jump
Speed Save
Defend
Waterwalking

Ancient Sword

Feather Hat
Leather Outfit
Setiemson

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Level Jump8, Vertical Jump3



Cheezybuda
Female
Virgo
40
47
Oracle
Black Magic
Parry
Dual Wield
Move-HP Up

Octagon Rod
Iron Fan
Ribbon
Wizard Outfit
Reflect Ring

Blind, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze, Sleep
Fire 4, Bolt, Bolt 4, Ice



Grininda
Female
Libra
56
65
Dancer
Jump
Auto Potion
Secret Hunt
Jump+3

Ryozan Silk

Headgear
Mystic Vest
Dracula Mantle

Polka Polka, Last Dance, Void Storage, Nether Demon
Level Jump2, Vertical Jump8



IcePattern
Male
Gemini
55
62
Wizard
Jump
Distribute
Equip Knife
Fly

Mage Masher

Leather Hat
White Robe
Feather Boots

Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Flare
Level Jump5, Vertical Jump7

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sinnyil2
Female
Capricorn
80
81
Oracle
Draw Out
Absorb Used MP
Magic Attack UP
Ignore Height

Gokuu Rod

Green Beret
Chain Vest
Angel Ring

Blind, Poison, Foxbird, Confusion Song, Paralyze
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Lord Burrah
Male
Aries
53
46
Priest
Yin Yang Magic
Brave Up
Attack UP
Waterwalking

Healing Staff

Red Hood
Linen Robe
Germinas Boots

Cure 3, Cure 4, Protect, Shell, Wall, Esuna
Blind, Pray Faith, Doubt Faith, Dispel Magic, Sleep



Baconbacon1207
Male
Pisces
79
77
Bard
Steal
HP Restore
Concentrate
Move+3

Ultimus Bow

Green Beret
Gold Armor
Bracer

Life Song, Battle Song, Last Song, Hydra Pit
Steal Helmet, Steal Weapon, Leg Aim



Powergems
Male
Pisces
74
58
Knight
Elemental
Meatbone Slash
Attack UP
Retreat

Iron Sword
Bronze Shield
Mythril Helmet
Wizard Robe
Feather Boots

Head Break, Speed Break
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

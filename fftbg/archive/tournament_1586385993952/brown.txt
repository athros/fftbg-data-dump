Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rolodex
Male
Pisces
63
64
Samurai
Punch Art
Meatbone Slash
Equip Axe
Move+2

Giant Axe

Gold Helmet
White Robe
108 Gems

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Spin Fist, Pummel, Earth Slash, Purification



BlinkFS
Female
Virgo
42
72
Samurai
Punch Art
Critical Quick
Equip Armor
Swim

Obelisk

Black Hood
Power Sleeve
Diamond Armlet

Asura, Koutetsu, Kiyomori
Wave Fist, Secret Fist, Revive, Seal Evil



Dynasti
Male
Cancer
45
78
Ninja
Time Magic
MA Save
Equip Knife
Ignore Terrain

Rod
Ninja Edge
Thief Hat
Leather Outfit
Power Wrist

Shuriken, Bomb, Knife, Spear
Haste, Reflect, Demi 2, Stabilize Time



Baron Von Scrub
Male
Gemini
42
80
Calculator
Beast Skill
Earplug
Secret Hunt
Ignore Terrain

Iron Fan

Bronze Helmet
Wizard Robe
Defense Ring

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest

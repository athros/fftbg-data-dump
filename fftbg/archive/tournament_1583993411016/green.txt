Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Male
Scorpio
80
61
Squire
Item
Dragon Spirit
Magic Defense UP
Jump+2

Slasher
Platinum Shield
Feather Hat
Carabini Mail
Rubber Shoes

Throw Stone, Heal, Fury, Wish, Scream
Hi-Potion, X-Potion, Ether, Antidote, Holy Water, Phoenix Down



BAEN
Female
Capricorn
75
59
Mediator
Elemental
MA Save
Equip Armor
Retreat

Romanda Gun

Gold Helmet
Chameleon Robe
Bracer

Invitation, Persuade, Praise, Threaten, Preach, Solution, Insult, Negotiate, Rehabilitate
Static Shock, Will-O-Wisp, Blizzard, Lava Ball



AdmiralPikachu
Female
Scorpio
54
69
Priest
Draw Out
Arrow Guard
Maintenance
Lava Walking

Morning Star

Golden Hairpin
Silk Robe
Defense Ring

Cure 3, Raise, Raise 2, Shell, Esuna
Koutetsu



RRazza
Female
Sagittarius
59
53
Archer
Basic Skill
PA Save
Defense UP
Jump+2

Poison Bow
Escutcheon
Red Hood
Chain Vest
Wizard Mantle

Charge+1, Charge+2, Charge+7, Charge+20
Accumulate, Heal, Tickle, Cheer Up, Fury

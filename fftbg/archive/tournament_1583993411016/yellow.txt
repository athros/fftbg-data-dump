Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Warwizard23
Female
Sagittarius
64
67
Time Mage
White Magic
Counter Flood
Doublehand
Ignore Terrain

Wizard Staff

Red Hood
Chameleon Robe
Magic Gauntlet

Haste 2, Stop, Quick, Demi, Demi 2, Stabilize Time
Cure 3, Raise, Regen, Shell, Shell 2, Esuna



Grininda
Male
Scorpio
57
62
Samurai
Elemental
Damage Split
Magic Attack UP
Move+3

Asura Knife

Diamond Helmet
Gold Armor
Leather Mantle

Asura, Heaven's Cloud, Kikuichimoji
Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard



HaplessOne
Male
Leo
80
36
Archer
Item
Counter Magic
Doublehand
Jump+3

Gastrafitis

Diamond Helmet
Mythril Vest
Wizard Mantle

Charge+1, Charge+2, Charge+3, Charge+7
Potion, Hi-Potion, Ether, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down



Hzor
Male
Aquarius
75
57
Ninja
Charge
Caution
Attack UP
Jump+3

Cultist Dagger
Short Edge
Red Hood
Wizard Outfit
Feather Mantle

Shuriken, Bomb, Staff, Dictionary
Charge+1, Charge+3, Charge+4, Charge+5

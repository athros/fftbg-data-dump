Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lythe Caraker
Female
Virgo
70
72
Lancer
Draw Out
Parry
Equip Polearm
Jump+3

Octagon Rod
Bronze Shield
Diamond Helmet
Chameleon Robe
Spike Shoes

Level Jump5, Vertical Jump6
Koutetsu, Murasame, Muramasa, Masamune



WireLord
Female
Sagittarius
51
71
Mediator
Battle Skill
Earplug
Dual Wield
Move-MP Up

Cultist Dagger
Zorlin Shape
Leather Hat
Leather Outfit
Small Mantle

Invitation, Threaten, Mimic Daravon, Rehabilitate
Magic Break, Speed Break, Power Break, Night Sword



Nickelbank
Male
Leo
67
74
Bard
Draw Out
Speed Save
Dual Wield
Jump+3

Bloody Strings
Ramia Harp
Golden Hairpin
Clothes
Salty Rage

Battle Song, Magic Song, Hydra Pit
Asura, Koutetsu, Heaven's Cloud, Muramasa



Orangejuliuscaesar
Female
Capricorn
53
73
Geomancer
Punch Art
PA Save
Equip Polearm
Ignore Height

Mythril Spear
Crystal Shield
Golden Hairpin
White Robe
Wizard Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Pummel, Earth Slash, Purification, Chakra, Revive

Player: !White
Team: White Team
Palettes: White/Blue



Luv Thy Neighbor
Male
Virgo
68
59
Wizard
Throw
Dragon Spirit
Concentrate
Waterwalking

Ice Rod

Leather Hat
Black Costume
Dracula Mantle

Bolt 3, Ice 4, Empower, Frog, Flare
Shuriken, Bomb, Knife



Cheidiabros
Male
Pisces
71
43
Calculator
White Magic
Mana Shield
Equip Armor
Waterwalking

Bestiary

Genji Helmet
Linen Cuirass
Genji Gauntlet

CT, Height, Prime Number, 5, 4
Cure 3, Cure 4, Reraise, Regen, Protect 2, Wall



Scadooshlol
Male
Gemini
48
42
Ninja
Steal
Mana Shield
Attack UP
Waterwalking

Short Edge
Spell Edge
Flash Hat
Power Sleeve
Red Shoes

Knife, Ninja Sword, Dictionary
Steal Weapon, Steal Accessory, Steal Status



Eltejongrande
Male
Scorpio
51
77
Bard
Elemental
Arrow Guard
Magic Defense UP
Teleport

Ramia Harp

Flash Hat
Platinum Armor
108 Gems

Angel Song, Battle Song, Last Song
Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

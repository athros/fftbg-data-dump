Player: !Black
Team: Black Team
Palettes: Black/Red



Safira125
Monster
Leo
71
58
Steel Giant










Sairentozon7
Female
Sagittarius
46
73
Wizard
Steal
PA Save
Beastmaster
Ignore Height

Mythril Knife

Triangle Hat
Linen Robe
Defense Armlet

Fire 3, Fire 4, Bolt, Bolt 3, Ice
Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim



Error72
Monster
Scorpio
46
66
Trent










Thyrandaal
Female
Sagittarius
71
51
Mediator
Time Magic
Absorb Used MP
Dual Wield
Retreat

Papyrus Codex
Battle Folio
Green Beret
Clothes
Sprint Shoes

Persuade, Praise, Threaten, Solution, Death Sentence, Refute, Rehabilitate
Haste, Slow 2, Stop, Quick, Stabilize Time

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sinnyil2
Female
Libra
71
69
Wizard
Time Magic
Brave Up
Short Charge
Ignore Height

Poison Rod

Headgear
Power Sleeve
Germinas Boots

Bolt 3, Ice 2, Ice 3, Empower
Slow 2, Reflect, Stabilize Time



Mesmaster
Female
Virgo
69
67
Samurai
Punch Art
Damage Split
Equip Bow
Move+2

Ultimus Bow

Leather Helmet
Light Robe
Dracula Mantle

Koutetsu, Bizen Boat, Kiyomori, Muramasa
Secret Fist, Purification, Chakra, Revive



Byrdturbo
Male
Pisces
59
59
Oracle
Steal
Parry
Equip Knife
Levitate

Dragon Rod

Triangle Hat
Wizard Robe
Battle Boots

Poison, Dispel Magic, Paralyze, Petrify
Steal Heart, Steal Helmet, Steal Accessory, Steal Status



Ruvelia BibeI
Male
Virgo
62
67
Calculator
Mighty Sword
Distribute
Equip Axe
Jump+2

Oak Staff
Escutcheon
Feather Hat
Judo Outfit
Defense Ring

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite

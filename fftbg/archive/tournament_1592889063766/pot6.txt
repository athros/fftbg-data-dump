Final Bets: black - 11 bets for 6,161G (33.1%, x2.02); brown - 10 bets for 12,440G (66.9%, x0.50)

black bets:
solomongrundy85: 1,989G (32.3%, 3,978G)
SkylerBunny: 1,200G (19.5%, 574,784G)
BirbBrainsBot: 1,000G (16.2%, 77,242G)
Laserman1000: 600G (9.7%, 600G)
E_Ballard: 588G (9.5%, 588G)
Meanderandreturn: 300G (4.9%, 1,347G)
getthemoneyz: 174G (2.8%, 1,010,579G)
twelfthrootoftwo: 100G (1.6%, 3,266G)
TheNiDDleR180: 100G (1.6%, 100G)
delcake: 100G (1.6%, 878G)
moonliquor: 10G (0.2%, 7,403G)

brown bets:
prince_rogers_nelson_: 4,962G (39.9%, 4,962G)
BlackfyreRoy: 2,224G (17.9%, 2,224G)
helpimabug: 1,318G (10.6%, 2,690G)
soren_of_tyto: 1,200G (9.6%, 1,200G)
NicoSavoy: 970G (7.8%, 970G)
Sairentozon7: 710G (5.7%, 710G)
Chuckolator: 553G (4.4%, 41,299G)
silentkaster: 203G (1.6%, 10,603G)
ruleof5: 200G (1.6%, 33,553G)
datadrivenbot: 100G (0.8%, 48,503G)

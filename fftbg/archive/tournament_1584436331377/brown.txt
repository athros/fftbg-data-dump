Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Pandasforsale
Female
Libra
74
65
Samurai
Item
Earplug
Throw Item
Ignore Height

Bizen Boat

Gold Helmet
Genji Armor
Cursed Ring

Murasame
Potion, Hi-Potion, Echo Grass, Holy Water, Remedy, Phoenix Down



RongRongArts
Male
Sagittarius
75
60
Chemist
Sing
Abandon
Equip Gun
Move+1

Mythril Gun

Black Hood
Mythril Vest
Magic Gauntlet

X-Potion, Ether, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Nameless Song, Last Song, Space Storage, Hydra Pit



Most Shameful
Male
Pisces
57
69
Ninja
Summon Magic
Sunken State
Martial Arts
Retreat



Holy Miter
Chain Vest
Leather Mantle

Shuriken, Bomb
Moogle, Ifrit, Carbunkle, Leviathan



MalakoFox
Female
Sagittarius
63
65
Squire
Charge
Dragon Spirit
Maintenance
Lava Walking

Coral Sword
Buckler
Diamond Helmet
Carabini Mail
Feather Mantle

Accumulate, Heal, Tickle, Yell, Cheer Up, Fury
Charge+4, Charge+7

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Ranmilia
Male
Scorpio
72
61
Summoner
Jump
Parry
Equip Sword
Move+2

Asura Knife

Holy Miter
Black Robe
Germinas Boots

Moogle, Ramuh, Titan, Carbunkle, Salamander, Fairy, Cyclops
Level Jump4, Vertical Jump6



Lynx1x1
Male
Scorpio
57
51
Knight
Draw Out
Critical Quick
Equip Shield
Move-HP Up

Defender
Platinum Shield
Leather Helmet
Crystal Mail
Genji Gauntlet

Head Break, Weapon Break, Magic Break, Stasis Sword, Night Sword
Heaven's Cloud, Kikuichimoji



Dragoduval
Female
Sagittarius
58
68
Summoner
Punch Art
Blade Grasp
Doublehand
Lava Walking

Thunder Rod

Black Hood
Silk Robe
Red Shoes

Moogle, Ramuh, Ifrit, Titan, Salamander
Wave Fist, Earth Slash, Purification, Seal Evil



Masta Glenn
Male
Sagittarius
42
70
Geomancer
Charge
Parry
Equip Axe
Jump+2

Morning Star
Round Shield
Leather Hat
Wizard Outfit
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp
Charge+2, Charge+4, Charge+7

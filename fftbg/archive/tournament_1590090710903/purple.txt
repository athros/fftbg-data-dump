Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Asherban
Female
Cancer
72
48
Ninja
Summon Magic
Distribute
Equip Gun
Lava Walking

Papyrus Codex
Bestiary
Feather Hat
Mystic Vest
Reflect Ring

Shuriken, Hammer
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Odin, Salamander, Silf, Lich



Candina
Female
Virgo
56
63
Priest
Talk Skill
Counter
Martial Arts
Jump+2



Thief Hat
Linen Robe
Leather Mantle

Raise, Raise 2, Reraise, Regen, Protect, Shell 2, Wall, Esuna
Preach, Negotiate, Refute



TheFALLofLindsay
Monster
Leo
72
59
Apanda










Drusic
Female
Scorpio
64
73
Lancer
Punch Art
Brave Save
Defend
Waterbreathing

Partisan
Diamond Shield
Barbuta
Bronze Armor
Vanish Mantle

Level Jump4, Vertical Jump4
Spin Fist, Pummel, Purification

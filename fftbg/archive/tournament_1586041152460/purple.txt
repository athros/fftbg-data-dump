Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Volgrathemoose
Female
Cancer
66
58
Summoner
White Magic
PA Save
Short Charge
Move-MP Up

Oak Staff

Golden Hairpin
Black Robe
Diamond Armlet

Ifrit, Titan, Golem, Leviathan, Fairy
Cure, Raise, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna



ThePineappleSalesman
Monster
Taurus
39
51
Iron Hawk










Galkife
Male
Sagittarius
68
58
Bard
Steal
Abandon
Equip Polearm
Ignore Height

Partisan

Feather Hat
Adaman Vest
Small Mantle

Angel Song, Cheer Song, Battle Song, Sky Demon
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory



Tithonus
Male
Capricorn
71
43
Lancer
Charge
Distribute
Defense UP
Lava Walking

Gokuu Rod
Gold Shield
Grand Helmet
Chain Mail
Power Wrist

Level Jump8, Vertical Jump8
Charge+1, Charge+3, Charge+4, Charge+5

Player: !White
Team: White Team
Palettes: White/Blue



Heroebal
Female
Sagittarius
63
72
Chemist
Steal
Parry
Equip Sword
Retreat

Kiyomori

Leather Hat
Brigandine
Rubber Shoes

Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



NovaKnight21
Male
Sagittarius
41
67
Summoner
Elemental
Abandon
Equip Shield
Move+1

Ice Rod
Gold Shield
Holy Miter
Wizard Outfit
Feather Boots

Moogle, Ramuh, Titan, Carbunkle, Salamander, Cyclops
Water Ball, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Rastanar
Male
Leo
70
64
Monk
Sing
Caution
Short Charge
Ignore Terrain



Flash Hat
Earth Clothes
Rubber Shoes

Spin Fist, Secret Fist, Chakra, Revive, Seal Evil
Angel Song, Cheer Song, Magic Song, Hydra Pit



KonzeraLive
Male
Sagittarius
69
53
Wizard
Throw
Dragon Spirit
Halve MP
Lava Walking

Rod

Leather Hat
Black Costume
Germinas Boots

Fire 2, Bolt, Bolt 2, Ice 4, Death
Bomb, Axe

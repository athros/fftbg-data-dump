Player: !Black
Team: Black Team
Palettes: Black/Red



Lifeguard Dan
Female
Aquarius
46
67
Summoner
Dance
Auto Potion
Magic Attack UP
Jump+1

Thunder Rod

Green Beret
Mythril Vest
Elf Mantle

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf
Polka Polka, Disillusion



Rolodex
Female
Scorpio
48
66
Dancer
Item
MP Restore
Throw Item
Move+3

Hydra Bag

Thief Hat
Mythril Vest
Genji Gauntlet

Slow Dance, Disillusion, Nether Demon
Ether, Eye Drop, Soft



EnemyController
Male
Pisces
37
75
Monk
Steal
Damage Split
Attack UP
Move+2



Feather Hat
Brigandine
Genji Gauntlet

Spin Fist, Pummel, Earth Slash, Purification, Revive
Gil Taking, Steal Heart, Steal Status



Dickawesome
Female
Sagittarius
73
54
Calculator
Limit
Absorb Used MP
Equip Polearm
Lava Walking

Ryozan Silk

Crystal Helmet
Mythril Vest
Bracer

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

Player: !Black
Team: Black Team
Palettes: Black/Red



Galkife
Female
Virgo
44
60
Samurai
Punch Art
HP Restore
Equip Sword
Swim

Sleep Sword

Barbuta
Diamond Armor
Bracer

Heaven's Cloud, Kiyomori
Secret Fist, Purification, Revive



ApplesauceBoss
Male
Sagittarius
45
52
Calculator
Beast Skill
Damage Split
Magic Defense UP
Jump+2

Iron Fan
Aegis Shield
Black Hood
Clothes
Angel Ring

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



DaveStrider55
Female
Virgo
46
75
Calculator
Yin Yang Magic
Damage Split
Concentrate
Ignore Height

Bestiary

Green Beret
Adaman Vest
Cursed Ring

CT, 4, 3
Blind, Sleep, Dark Holy



Nofreewill
Female
Virgo
42
73
Summoner
Jump
Damage Split
Equip Gun
Swim

Papyrus Codex

Triangle Hat
Robe of Lords
Dracula Mantle

Moogle, Ramuh, Golem, Carbunkle, Silf, Cyclops
Level Jump2, Vertical Jump2

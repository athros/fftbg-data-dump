Player: !Green
Team: Green Team
Palettes: Green/White



Maldoree
Monster
Virgo
67
51
Blue Dragon










JustSuperish
Monster
Aquarius
41
43
Red Panther










FattyBearsteak
Male
Gemini
58
79
Wizard
Item
Distribute
Equip Armor
Waterwalking

Ice Rod

Holy Miter
Leather Armor
Reflect Ring

Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice 2, Flare
Potion, Hi-Ether, Eye Drop, Echo Grass, Soft, Phoenix Down



HaateXIII
Female
Virgo
48
47
Squire
Steal
Damage Split
Concentrate
Levitate

Coral Sword
Bronze Shield
Cross Helmet
Earth Clothes
Dracula Mantle

Accumulate, Throw Stone, Heal, Fury, Wish, Scream
Steal Heart, Steal Accessory, Steal Status, Arm Aim

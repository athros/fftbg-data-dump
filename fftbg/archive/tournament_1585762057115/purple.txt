Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesauceBoss
Male
Leo
59
50
Squire
Summon Magic
MP Restore
Long Status
Move+1

Air Knife
Platinum Shield
Triangle Hat
Linen Cuirass
Cherche

Accumulate, Dash, Heal, Yell
Ifrit, Titan, Carbunkle, Bahamut, Leviathan



Jethrothrul
Monster
Scorpio
53
64
Skeleton










Theseawolf1
Female
Aries
72
42
Lancer
Battle Skill
Counter Flood
Secret Hunt
Swim

Whale Whisker
Mythril Shield
Mythril Helmet
Light Robe
Dracula Mantle

Level Jump5, Vertical Jump8
Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Justice Sword, Night Sword



Dreadnxught
Male
Leo
69
45
Chemist
Throw
Absorb Used MP
Doublehand
Levitate

Dagger

Holy Miter
Mythril Vest
Sprint Shoes

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Soft, Phoenix Down
Knife

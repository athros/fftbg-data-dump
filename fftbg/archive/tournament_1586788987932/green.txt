Player: !Green
Team: Green Team
Palettes: Green/White



Baron Von Scrub
Male
Cancer
58
58
Thief
Time Magic
Damage Split
Dual Wield
Ignore Terrain

Mage Masher
Blood Sword
Headgear
Earth Clothes
Battle Boots

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory
Haste, Immobilize, Reflect, Stabilize Time



MattMan119
Male
Gemini
71
53
Ninja
Talk Skill
Counter Tackle
Equip Gun
Ignore Terrain

Ramia Harp
Bloody Strings
Headgear
Chain Vest
N-Kai Armlet

Knife
Praise, Mimic Daravon, Rehabilitate



Maakur
Male
Capricorn
68
69
Oracle
Basic Skill
Brave Up
Beastmaster
Fly

Ivory Rod

Golden Hairpin
Black Robe
Red Shoes

Poison, Spell Absorb, Foxbird, Paralyze, Sleep
Heal, Tickle



DaveStrider55
Male
Sagittarius
62
55
Thief
Sing
Counter Flood
Maintenance
Waterbreathing

Broad Sword

Headgear
Wizard Outfit
Rubber Shoes

Steal Helmet, Steal Status
Angel Song, Life Song, Cheer Song, Magic Song

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Stukerk
Monster
Aries
58
45
Sekhret










Kohlingen
Male
Leo
56
49
Bard
Yin Yang Magic
Counter Tackle
Doublehand
Waterbreathing

Ramia Harp

Red Hood
Mystic Vest
Dracula Mantle

Cheer Song, Magic Song, Nameless Song, Space Storage
Pray Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy



Maakur
Female
Sagittarius
46
60
Geomancer
Battle Skill
PA Save
Doublehand
Move+2

Giant Axe

Twist Headband
Mystic Vest
108 Gems

Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Power Break, Dark Sword



UmaiJam
Female
Gemini
72
77
Archer
Black Magic
Counter Flood
Doublehand
Move-MP Up

Cross Bow

Leather Hat
Adaman Vest
Magic Ring

Charge+4, Charge+7
Fire 2, Fire 3, Ice, Ice 3, Ice 4, Empower, Flare

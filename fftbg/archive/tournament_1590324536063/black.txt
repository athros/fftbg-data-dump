Player: !Black
Team: Black Team
Palettes: Black/Red



Error72
Female
Gemini
49
76
Dancer
Summon Magic
Regenerator
Sicken
Fly

Hydra Bag

Feather Hat
Wizard Outfit
108 Gems

Polka Polka, Obsidian Blade, Dragon Pit
Moogle, Shiva, Golem, Fairy



Bruubarg
Monster
Capricorn
47
57
Ochu










Smashy
Female
Serpentarius
69
69
Summoner
Punch Art
Meatbone Slash
Equip Sword
Move-HP Up

Broad Sword

Headgear
Wizard Robe
Dracula Mantle

Ramuh, Titan, Carbunkle, Bahamut, Odin, Silf, Fairy
Pummel, Earth Slash, Revive



Roqqqpsi
Female
Virgo
50
43
Calculator
Black Magic
Earplug
Equip Shield
Jump+2

Battle Folio
Crystal Shield
Flash Hat
Leather Outfit
Red Shoes

CT, Height, Prime Number, 5, 4
Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Ice 4, Flare

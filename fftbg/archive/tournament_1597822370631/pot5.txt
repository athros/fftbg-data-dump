Final Bets: red - 5 bets for 7,439G (72.9%, x0.37); yellow - 7 bets for 2,769G (27.1%, x2.69)

red bets:
AllInBot: 6,526G (87.7%, 6,526G)
fluffskull: 504G (6.8%, 504G)
ruebyy: 200G (2.7%, 1,312G)
Drusiform: 109G (1.5%, 9,573G)
ko2q: 100G (1.3%, 2,261G)

yellow bets:
BirbBrainsBot: 1,000G (36.1%, 50,999G)
Sairentozon7: 1,000G (36.1%, 25,575G)
getthemoneyz: 260G (9.4%, 1,669,984G)
datadrivenbot: 200G (7.2%, 65,040G)
TheSyffy: 108G (3.9%, 108G)
gorgewall: 101G (3.6%, 3,538G)
MinBetBot: 100G (3.6%, 6,895G)

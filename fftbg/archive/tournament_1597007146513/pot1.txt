Final Bets: red - 25 bets for 10,911G (51.1%, x0.96); blue - 8 bets for 10,424G (48.9%, x1.05)

red bets:
Aldrammech: 1,000G (9.2%, 5,952G)
BirbBrainsBot: 1,000G (9.2%, 140,930G)
nhammen: 1,000G (9.2%, 230,028G)
Zbgs: 1,000G (9.2%, 6,376G)
itskage: 847G (7.8%, 8,476G)
HaateXIII: 692G (6.3%, 692G)
killth3kid: 660G (6.0%, 12,641G)
dogsandcatsand: 512G (4.7%, 512G)
TheDeeyo: 500G (4.6%, 4,070G)
sinnyil2: 500G (4.6%, 8,444G)
Amorial: 400G (3.7%, 4,218G)
silverChangeling: 400G (3.7%, 2,358G)
DesertWooder: 300G (2.7%, 1,093G)
ruleof5: 300G (2.7%, 25,390G)
byrdturbo: 255G (2.3%, 24,560G)
Galeronis: 236G (2.2%, 236G)
getthemoneyz: 208G (1.9%, 1,563,473G)
patdotrick: 200G (1.8%, 442G)
datadrivenbot: 200G (1.8%, 53,500G)
latebit: 184G (1.7%, 1,983G)
Vahn_Blade: 116G (1.1%, 116G)
gorgewall: 101G (0.9%, 2,787G)
MinBetBot: 100G (0.9%, 18,519G)
Firesheath: 100G (0.9%, 24,356G)
Drusiform: 100G (0.9%, 1,038G)

blue bets:
DeathTaxesAndAnime: 7,734G (74.2%, 15,166G)
Lord_Gwarth: 753G (7.2%, 753G)
Lanshaft: 568G (5.4%, 9,537G)
Phi_Sig: 555G (5.3%, 144,040G)
nobbletv: 300G (2.9%, 5,225G)
AllInBot: 200G (1.9%, 200G)
flacococo: 164G (1.6%, 164G)
ar_tactic: 150G (1.4%, 82,515G)

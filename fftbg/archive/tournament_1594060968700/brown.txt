Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HaychDub
Male
Aquarius
74
64
Geomancer
Punch Art
Hamedo
Equip Knife
Levitate

Rod
Round Shield
Holy Miter
Adaman Vest
Angel Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Lava Ball
Wave Fist, Earth Slash, Purification, Revive



Zeroroute
Female
Aquarius
48
51
Samurai
Elemental
Mana Shield
Defense UP
Lava Walking

Murasame

Platinum Helmet
Carabini Mail
Elf Mantle

Koutetsu, Heaven's Cloud
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



Hp199297
Female
Aries
47
58
Thief
Time Magic
PA Save
Beastmaster
Levitate

Hidden Knife

Twist Headband
Leather Outfit
Germinas Boots

Steal Helmet, Steal Shield, Steal Accessory, Leg Aim
Slow 2, Immobilize, Float, Quick, Demi, Stabilize Time



ChaoreLance
Male
Scorpio
73
72
Squire
Charge
Blade Grasp
Magic Attack UP
Ignore Terrain

Flail
Diamond Shield
Twist Headband
Adaman Vest
Feather Boots

Dash, Throw Stone, Heal, Tickle, Yell, Wish
Charge+1, Charge+7

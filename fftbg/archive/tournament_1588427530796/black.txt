Player: !Black
Team: Black Team
Palettes: Black/Red



Imagine Pink
Female
Virgo
78
47
Dancer
Basic Skill
Arrow Guard
Magic Defense UP
Move+2

Hydra Bag

Twist Headband
Leather Outfit
Small Mantle

Witch Hunt, Wiznaibus, Disillusion, Nether Demon
Accumulate, Dash, Heal, Tickle, Yell, Fury



CassiePhoenix
Female
Taurus
55
42
Monk
Throw
Critical Quick
Sicken
Fly



Thief Hat
Adaman Vest
Wizard Mantle

Pummel, Purification, Revive, Seal Evil
Shuriken, Sword, Dictionary



Maakur
Female
Serpentarius
55
68
Dancer
Steal
Critical Quick
Equip Armor
Jump+3

Main Gauche

Crystal Helmet
Leather Armor
Rubber Shoes

Witch Hunt, Wiznaibus, Polka Polka, Nether Demon
Steal Accessory



RjA0zcOQ96
Male
Libra
65
57
Bard
Time Magic
Earplug
Dual Wield
Move+2

Fairy Harp
Bloody Strings
Feather Hat
Wizard Outfit
Sprint Shoes

Angel Song, Battle Song, Sky Demon
Haste, Slow 2, Immobilize, Float, Demi 2, Stabilize Time

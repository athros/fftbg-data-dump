Final Bets: black - 6 bets for 3,353G (19.7%, x4.07); purple - 11 bets for 13,646G (80.3%, x0.25)

black bets:
BirbBrainsBot: 1,000G (29.8%, 172,285G)
Lionhermit: 1,000G (29.8%, 37,402G)
arch8000: 566G (16.9%, 1,132G)
SarrgeQc: 501G (14.9%, 1,332G)
getthemoneyz: 186G (5.5%, 615,198G)
CassiePhoenix: 100G (3.0%, 6,885G)

purple bets:
HaplessOne: 3,961G (29.0%, 19,807G)
gooseyourself: 3,752G (27.5%, 3,752G)
Shakarak: 2,000G (14.7%, 14,688G)
volgrathemoose: 1,001G (7.3%, 2,784G)
leakimiko: 936G (6.9%, 6,243G)
ungabunga_bot: 634G (4.6%, 314,184G)
Rislyeu: 562G (4.1%, 562G)
fenaen: 500G (3.7%, 11,237G)
Langrisser: 100G (0.7%, 6,604G)
maakur_: 100G (0.7%, 117,935G)
datadrivenbot: 100G (0.7%, 11,705G)

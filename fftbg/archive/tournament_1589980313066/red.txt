Player: !Red
Team: Red Team
Palettes: Red/Brown



Powergems
Male
Leo
44
64
Squire
Elemental
Counter Tackle
Concentrate
Teleport

Giant Axe
Diamond Shield
Crystal Helmet
Leather Armor
Rubber Shoes

Dash, Throw Stone, Heal, Cheer Up, Fury, Wish
Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Galkife
Monster
Cancer
61
68
Red Chocobo










Dellusionsx
Male
Virgo
40
53
Monk
Jump
HP Restore
Secret Hunt
Jump+2



Red Hood
Adaman Vest
Bracer

Secret Fist, Purification, Chakra, Revive
Level Jump5, Vertical Jump8



Cloud92684
Male
Gemini
51
42
Monk
Black Magic
Sunken State
Equip Axe
Move+2

Healing Staff

Feather Hat
Chain Vest
Elf Mantle

Revive
Fire, Fire 4, Bolt 3, Bolt 4, Frog

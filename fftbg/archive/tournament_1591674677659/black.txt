Player: !Black
Team: Black Team
Palettes: Black/Red



Harbinger605
Female
Pisces
75
61
Calculator
Gore Skill
Distribute
Equip Armor
Ignore Height

Battle Folio
Buckler
Leather Hat
Judo Outfit
Cursed Ring

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Richardserious
Male
Virgo
80
46
Archer
Sing
Counter Magic
Equip Bow
Move+1

Ultimus Bow

Triangle Hat
Black Costume
Battle Boots

Charge+2, Charge+20
Cheer Song, Battle Song, Magic Song, Last Song



L2 Sentinel
Male
Virgo
42
53
Lancer
Elemental
Mana Shield
Equip Gun
Fly

Bestiary
Ice Shield
Iron Helmet
Wizard Robe
108 Gems

Level Jump8, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Powergems
Female
Libra
62
70
Thief
Dance
Critical Quick
Equip Bow
Fly

Lightning Bow

Flash Hat
Earth Clothes
108 Gems

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Disillusion, Obsidian Blade

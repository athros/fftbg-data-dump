Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Toka222
Female
Capricorn
59
75
Ninja
Item
Counter Magic
Equip Gun
Swim

Bestiary
Bestiary
Headgear
Leather Outfit
Sprint Shoes

Shuriken, Bomb
Potion, X-Potion, Echo Grass, Holy Water, Remedy, Phoenix Down



Roqqqpsi
Female
Pisces
78
50
Ninja
Charge
Mana Shield
Concentrate
Fly

Flame Whip
Mythril Knife
Feather Hat
Clothes
Vanish Mantle

Shuriken
Charge+2



Luminarii
Male
Libra
38
39
Knight
Steal
Meatbone Slash
Short Charge
Ignore Terrain

Battle Axe
Escutcheon
Grand Helmet
Light Robe
Defense Ring

Shield Break, Justice Sword
Steal Heart, Steal Helmet, Steal Armor, Steal Status



Baron Von Scrub
Female
Capricorn
53
69
Mediator
Throw
Critical Quick
Doublehand
Waterbreathing

Mage Masher

Golden Hairpin
Silk Robe
Angel Ring

Praise, Preach, Solution, Death Sentence, Insult, Refute, Rehabilitate
Shuriken

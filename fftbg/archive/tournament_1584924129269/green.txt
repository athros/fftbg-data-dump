Player: !Green
Team: Green Team
Palettes: Green/White



MantisFinch
Monster
Taurus
70
57
Grenade










Davarian
Female
Cancer
38
49
Monk
Time Magic
Faith Up
Equip Polearm
Waterbreathing

Musk Rod

Green Beret
Black Costume
Red Shoes

Secret Fist, Purification, Chakra, Seal Evil
Haste 2, Slow, Immobilize, Reflect, Demi 2, Stabilize Time



Ant Ihistamine
Female
Pisces
73
73
Geomancer
Punch Art
Sunken State
Equip Armor
Ignore Height

Battle Axe
Buckler
Leather Helmet
Reflect Mail
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Gusty Wind
Wave Fist, Earth Slash, Purification, Revive



HASTERIOUS
Female
Leo
67
60
Wizard
Draw Out
Earplug
Short Charge
Lava Walking

Thunder Rod

Triangle Hat
Wizard Robe
Leather Mantle

Fire 3, Fire 4, Bolt, Ice, Ice 2, Empower
Asura, Bizen Boat, Heaven's Cloud

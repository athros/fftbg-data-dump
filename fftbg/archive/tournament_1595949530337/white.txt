Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Female
Cancer
79
62
Oracle
Item
Counter Magic
Attack UP
Waterwalking

Cypress Rod

Red Hood
Silk Robe
108 Gems

Blind, Poison, Pray Faith, Foxbird, Confusion Song, Paralyze, Petrify
Potion, X-Potion, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Phi Sig
Female
Taurus
43
53
Time Mage
White Magic
Counter Flood
Short Charge
Lava Walking

Musk Rod

Thief Hat
Linen Robe
Genji Gauntlet

Haste 2, Reflect
Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Magic Barrier



Kronikle
Female
Serpentarius
50
46
Thief
Elemental
Counter Tackle
Equip Gun
Ignore Height

Bestiary

Red Hood
Brigandine
Cursed Ring

Gil Taking, Steal Weapon, Steal Status, Arm Aim
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball



Benticore
Monster
Leo
71
37
Great Malboro








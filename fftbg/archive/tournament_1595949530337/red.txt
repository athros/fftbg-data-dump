Player: !Red
Team: Red Team
Palettes: Red/Brown



Hasterious
Male
Aries
69
54
Mime

Counter
Equip Shield
Fly


Hero Shield
Green Beret
Black Costume
Leather Mantle

Mimic




KasugaiRoastedPeas
Female
Taurus
76
75
Mediator
Time Magic
Hamedo
Equip Shield
Move-HP Up

Papyrus Codex
Buckler
Ribbon
Judo Outfit
Dracula Mantle

Invitation, Persuade, Threaten, Preach, Refute, Rehabilitate
Haste, Slow, Stop, Immobilize, Quick, Demi 2, Stabilize Time



Lord Gwarth
Male
Aries
75
62
Geomancer
Charge
Counter Flood
Equip Sword
Jump+1

Save the Queen

Flash Hat
Mythril Vest
Elf Mantle

Hell Ivy, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+1



E Ballard
Male
Virgo
79
59
Lancer
Sing
Sunken State
Equip Polearm
Retreat

Holy Lance
Round Shield
Gold Helmet
Bronze Armor
Feather Mantle

Level Jump5, Vertical Jump7
Angel Song, Life Song, Magic Song, Last Song, Sky Demon, Hydra Pit

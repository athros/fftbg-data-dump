Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Yiroep2
Female
Pisces
54
79
Lancer
White Magic
Counter
Equip Knife
Swim

Rod
Round Shield
Gold Helmet
Bronze Armor
Feather Mantle

Level Jump2, Vertical Jump7
Cure 3, Raise 2, Shell 2, Magic Barrier



CosmicTactician
Male
Capricorn
51
60
Knight
Basic Skill
Parry
Doublehand
Move+1

Battle Axe

Mythril Helmet
White Robe
Magic Gauntlet

Speed Break, Power Break, Mind Break, Justice Sword, Night Sword
Accumulate, Tickle, Yell, Cheer Up, Wish, Scream



SQUiDSQUARKLIN
Male
Sagittarius
57
78
Archer
Time Magic
Brave Save
Doublehand
Levitate

Hunting Bow

Flash Hat
Earth Clothes
108 Gems

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Slow 2, Immobilize, Quick, Stabilize Time



Maakur
Female
Scorpio
75
52
Summoner
Dance
Speed Save
Defend
Jump+1

Poison Rod

Leather Hat
Wizard Robe
Germinas Boots

Moogle, Ramuh, Ifrit, Titan, Golem, Odin, Leviathan, Lich
Wiznaibus, Slow Dance, Polka Polka

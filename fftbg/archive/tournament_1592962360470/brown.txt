Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Cryptopsy70
Male
Leo
59
63
Bard
Battle Skill
PA Save
Defend
Fly

Fairy Harp

Flash Hat
Power Sleeve
Diamond Armlet

Nameless Song, Sky Demon
Head Break, Armor Break, Magic Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Surging Sword, Explosion Sword



Kellios11
Male
Capricorn
58
72
Chemist
Jump
Counter Flood
Equip Bow
Teleport

Mythril Bow

Barette
Black Costume
Defense Ring

Potion, Antidote, Echo Grass, Soft, Phoenix Down
Level Jump4, Vertical Jump8



LDSkinny
Female
Leo
54
55
Oracle
Jump
HP Restore
Attack UP
Jump+3

Papyrus Codex

Barette
Black Robe
Feather Boots

Spell Absorb, Life Drain, Doubt Faith, Zombie, Dispel Magic, Sleep
Level Jump2, Vertical Jump8



Galkife
Male
Leo
79
71
Squire
Time Magic
Hamedo
Equip Polearm
Waterwalking

Gokuu Rod
Venetian Shield
Triangle Hat
Gold Armor
Spike Shoes

Accumulate, Throw Stone, Heal, Yell, Fury
Slow, Slow 2, Demi, Demi 2, Meteor

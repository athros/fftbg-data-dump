Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Magicandy
Female
Taurus
58
73
Geomancer
Draw Out
Damage Split
Dual Wield
Teleport

Mythril Sword
Bizen Boat
Leather Hat
Chain Vest
Reflect Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bizen Boat, Murasame, Heaven's Cloud, Kiyomori



RageImmortaI
Male
Leo
50
80
Chemist
Draw Out
Counter Flood
Equip Sword
Move+3

Long Sword

Green Beret
Leather Outfit
Dracula Mantle

Potion, X-Potion, Ether, Eye Drop, Soft, Remedy, Phoenix Down
Heaven's Cloud



Nekojin
Male
Virgo
69
65
Lancer
Talk Skill
Counter Flood
Dual Wield
Move-HP Up

Dragon Whisker
Battle Bamboo
Cross Helmet
Crystal Mail
Magic Gauntlet

Level Jump8, Vertical Jump6
Persuade, Threaten, Solution, Death Sentence, Refute, Rehabilitate



OpHendoslice
Male
Aries
42
48
Knight
Elemental
Critical Quick
Dual Wield
Move+3

Mythril Sword
Diamond Sword
Cross Helmet
Platinum Armor
Defense Armlet

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm

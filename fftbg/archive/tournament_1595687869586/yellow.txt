Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mudrockk
Female
Virgo
60
65
Geomancer
Yin Yang Magic
Earplug
Equip Sword
Levitate

Long Sword
Ice Shield
Triangle Hat
Black Robe
Elf Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep



Lydian C
Female
Capricorn
40
62
Time Mage
Draw Out
Counter
Doublehand
Fly

Oak Staff

Black Hood
Clothes
Battle Boots

Haste 2, Slow 2, Immobilize, Demi
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



SQUiDSQUARKLIN
Female
Leo
52
54
Priest
Draw Out
Sunken State
Dual Wield
Jump+3

Morning Star
Flame Whip
Twist Headband
Wizard Robe
108 Gems

Cure 2, Shell, Shell 2, Esuna
Heaven's Cloud, Muramasa



Joewcarson
Male
Pisces
41
45
Lancer
Basic Skill
PA Save
Equip Axe
Ignore Terrain

Rainbow Staff
Diamond Shield
Genji Helmet
Mythril Armor
Magic Ring

Level Jump4, Vertical Jump8
Heal, Yell, Fury

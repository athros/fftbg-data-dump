Player: !Green
Team: Green Team
Palettes: Green/White



CapnChaos12
Male
Aquarius
73
41
Monk
Throw
Abandon
Defend
Jump+3



Feather Hat
Power Sleeve
Reflect Ring

Pummel, Earth Slash, Secret Fist, Chakra, Revive
Shuriken, Bomb, Axe



Aeonicvector
Monster
Aries
59
77
Revenant










Gelwain
Monster
Gemini
58
39
Skeleton










CorpusCav
Female
Taurus
42
67
Geomancer
Yin Yang Magic
PA Save
Equip Knife
Move+3

Air Knife
Ice Shield
Twist Headband
Black Costume
N-Kai Armlet

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Blind, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dark Holy

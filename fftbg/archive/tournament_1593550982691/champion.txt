Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Shalloween
Male
Aquarius
49
61
Mime

Damage Split
Dual Wield
Jump+1



Twist Headband
Mythril Vest
N-Kai Armlet

Mimic




Lastly
Female
Cancer
62
50
Summoner
Dance
MA Save
Equip Armor
Teleport

Dragon Rod

Gold Helmet
Linen Cuirass
Reflect Ring

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Fairy
Witch Hunt, Slow Dance, Polka Polka



GrayGhostGaming
Male
Libra
48
72
Priest
Jump
Parry
Equip Knife
Levitate

Poison Rod

Triangle Hat
Wizard Robe
Battle Boots

Cure, Cure 2, Cure 3, Raise, Protect, Protect 2, Shell, Shell 2
Level Jump4, Vertical Jump6



Rurk
Male
Cancer
79
65
Calculator
Black Magic
Faith Save
Equip Armor
Teleport

Gokuu Rod

Barbuta
Mythril Armor
Reflect Ring

CT, Height, 5, 4, 3
Bolt 2, Ice 3

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Virilikus
Male
Libra
72
61
Mime

Distribute
Martial Arts
Jump+2



Headgear
Wizard Outfit
Reflect Ring

Mimic




RoamingUragaan
Female
Cancer
39
75
Summoner
Item
Abandon
Defend
Move-HP Up

Wizard Staff

Triangle Hat
Light Robe
Vanish Mantle

Moogle, Shiva, Titan, Carbunkle, Odin
Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



NovaKnight21
Monster
Taurus
60
61
Ghost










Kenaeisamazing
Female
Scorpio
52
49
Priest
Basic Skill
Caution
Defense UP
Move+1

Flame Whip

Feather Hat
Linen Robe
Setiemson

Cure 3, Raise, Raise 2, Regen, Wall, Esuna
Dash, Throw Stone, Heal, Yell, Cheer Up, Scream

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WhiteTigress
Male
Scorpio
69
60
Geomancer
Battle Skill
Faith Up
Concentrate
Ignore Terrain

Asura Knife
Ice Shield
Ribbon
Clothes
Magic Gauntlet

Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind
Armor Break, Shield Break, Magic Break



Nomoment
Male
Scorpio
58
74
Squire
Charge
Meatbone Slash
Equip Knife
Ignore Height

Mythril Knife

Red Hood
Linen Cuirass
Spike Shoes

Dash, Yell
Charge+4



Mirapoix
Female
Taurus
47
59
Archer
Item
PA Save
Defend
Swim

Gastrafitis
Buckler
Thief Hat
Brigandine
Small Mantle

Charge+2, Charge+3, Charge+4
Potion, Antidote



Timidpimp11
Male
Virgo
55
50
Archer
Item
HP Restore
Dual Wield
Swim

Mythril Gun
Glacier Gun
Thief Hat
Adaman Vest
Defense Ring

Charge+1, Charge+3, Charge+4, Charge+5
Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down

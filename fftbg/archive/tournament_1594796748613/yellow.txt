Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Evewho
Male
Aries
72
67
Priest
Talk Skill
Earplug
Magic Defense UP
Move-MP Up

White Staff

Flash Hat
Clothes
Elf Mantle

Cure 3, Raise 2, Protect 2, Shell, Esuna
Persuade, Threaten, Preach, Negotiate, Mimic Daravon, Refute



Geffro1908
Male
Cancer
68
50
Mime

Arrow Guard
Martial Arts
Teleport



Black Hood
Wizard Outfit
Dracula Mantle

Mimic




UmaiJam
Male
Virgo
79
79
Monk
Summon Magic
Auto Potion
Dual Wield
Levitate



Red Hood
Earth Clothes
Rubber Shoes

Wave Fist, Purification, Chakra, Revive
Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Odin, Leviathan, Silf



DamnThatShark
Female
Leo
56
65
Calculator
Black Magic
Counter
Equip Knife
Move-MP Up

Rod

Holy Miter
Judo Outfit
Magic Gauntlet

CT, 5, 4, 3
Fire, Bolt 3, Bolt 4, Ice, Frog, Death

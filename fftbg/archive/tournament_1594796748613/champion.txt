Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Thyrandaal
Male
Virgo
62
61
Archer
Draw Out
Distribute
Attack UP
Fly

Blaze Gun
Crystal Shield
Genji Helmet
Brigandine
Germinas Boots

Charge+1, Charge+4, Charge+10
Kikuichimoji



Peeronid
Female
Scorpio
44
49
Archer
Time Magic
PA Save
Doublehand
Move-HP Up

Ultimus Bow

Diamond Helmet
Secret Clothes
Reflect Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+20
Stop, Immobilize, Quick, Stabilize Time, Meteor



Roqqqpsi
Female
Sagittarius
68
68
Dancer
Summon Magic
Faith Save
Dual Wield
Retreat

Cultist Dagger
Zorlin Shape
Headgear
Black Costume
Elf Mantle

Wiznaibus, Slow Dance, Disillusion
Titan, Carbunkle, Fairy



Creggers
Female
Virgo
65
64
Summoner
White Magic
Meatbone Slash
Short Charge
Move+2

Wizard Rod

Thief Hat
Robe of Lords
Wizard Mantle

Moogle, Ifrit, Carbunkle, Leviathan, Fairy, Cyclops
Cure 3, Raise, Regen, Wall, Esuna

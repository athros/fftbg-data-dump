Player: !Green
Team: Green Team
Palettes: Green/White



Laserman1000
Male
Virgo
60
52
Chemist
Time Magic
Mana Shield
Beastmaster
Jump+3

Orichalcum

Triangle Hat
Clothes
Reflect Ring

Potion, Hi-Potion, Echo Grass, Holy Water, Phoenix Down
Haste, Haste 2, Stop, Immobilize, Stabilize Time



KasugaiRoastedPeas
Male
Capricorn
55
45
Calculator
Time Magic
Faith Save
Secret Hunt
Retreat

Rod

Leather Hat
White Robe
Power Wrist

CT, Height, Prime Number, 5, 4
Haste, Haste 2, Slow, Demi 2, Stabilize Time, Meteor



TeaTime29
Monster
Taurus
38
51
Great Malboro










Creggers
Female
Gemini
74
46
Time Mage
Item
Counter Magic
Halve MP
Ignore Terrain

Healing Staff

Holy Miter
Light Robe
Bracer

Haste, Immobilize, Demi 2, Stabilize Time, Meteor
Potion, Hi-Potion, Ether, Echo Grass, Holy Water

Player: !White
Team: White Team
Palettes: White/Blue



AlysSariel
Male
Aries
41
66
Geomancer
Throw
Faith Up
Equip Gun
Jump+3

Battle Folio
Bronze Shield
Leather Hat
Mythril Vest
Reflect Ring

Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Shuriken, Sword, Ninja Sword



Tsarman
Monster
Capricorn
45
61
Serpentarius










Clippopo
Female
Pisces
75
64
Calculator
White Magic
Counter Flood
Halve MP
Move-MP Up

Bestiary

Green Beret
Clothes
Magic Ring

Height, Prime Number, 4, 3
Cure 2, Cure 3, Raise, Reraise, Protect 2, Shell, Shell 2, Esuna, Holy



Ar Tactic
Female
Taurus
73
59
Time Mage
Elemental
MA Save
Magic Attack UP
Waterwalking

Oak Staff

Triangle Hat
Silk Robe
Diamond Armlet

Haste, Haste 2, Immobilize, Demi 2, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand

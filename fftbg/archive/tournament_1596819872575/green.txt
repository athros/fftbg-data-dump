Player: !Green
Team: Green Team
Palettes: Green/White



Just Here2
Male
Aries
80
51
Chemist
Black Magic
Mana Shield
Equip Bow
Swim

Poison Bow

Red Hood
Rubber Costume
Sprint Shoes

Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Fire, Bolt 3, Bolt 4, Ice 3, Ice 4



ArashiKurobara
Female
Aquarius
51
71
Calculator
Limit
Earplug
Secret Hunt
Fly

Gokuu Rod

Mythril Helmet
Diamond Armor
Genji Gauntlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Shalloween
Male
Aquarius
52
68
Bard
Punch Art
Counter Magic
Attack UP
Move-HP Up

Bloody Strings

Ribbon
Adaman Vest
Magic Ring

Angel Song, Diamond Blade, Sky Demon
Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil



JumbocactuarX27
Female
Gemini
49
65
Dancer
Time Magic
Catch
Doublehand
Move+2

Ryozan Silk

Twist Headband
Wizard Robe
Chantage

Wiznaibus, Slow Dance, Polka Polka, Obsidian Blade, Void Storage, Dragon Pit
Haste, Reflect, Demi, Demi 2

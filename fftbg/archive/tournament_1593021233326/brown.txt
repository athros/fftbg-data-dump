Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Helpimabug
Female
Aries
53
45
Knight
Basic Skill
MP Restore
Beastmaster
Ignore Height

Ragnarok
Genji Shield
Barbuta
Genji Armor
Angel Ring

Head Break, Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Stasis Sword
Dash, Heal, Tickle, Cheer Up, Wish



DeathTaxesAndAnime
Female
Leo
41
59
Lancer
Item
Parry
Martial Arts
Fly


Aegis Shield
Leather Helmet
Crystal Mail
Reflect Ring

Level Jump4, Vertical Jump6
Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft



Oreo Pizza
Male
Cancer
48
44
Knight
White Magic
Counter Magic
Magic Defense UP
Swim

Save the Queen
Bronze Shield
Diamond Helmet
Reflect Mail
Sprint Shoes

Head Break, Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword
Cure 3, Reraise, Protect, Protect 2, Shell, Shell 2



Brokenknight201
Female
Leo
66
80
Calculator
Labyrinth Skill
Brave Save
Equip Knife
Move-MP Up

Blind Knife

Triangle Hat
Chain Mail
Leather Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power

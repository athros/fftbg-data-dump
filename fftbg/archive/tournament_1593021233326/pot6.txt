Final Bets: white - 8 bets for 2,735G (26.9%, x2.72); purple - 16 bets for 7,437G (73.1%, x0.37)

white bets:
dogsandcatsand: 555G (20.3%, 15,336G)
killth3kid: 500G (18.3%, 6,708G)
DuraiPapers: 500G (18.3%, 20,372G)
Shalloween: 400G (14.6%, 44,945G)
RunicMagus: 300G (11.0%, 44,771G)
sir_uther115: 300G (11.0%, 3,578G)
goodnightrobo: 152G (5.6%, 152G)
getthemoneyz: 28G (1.0%, 1,024,687G)

purple bets:
Rilgon: 1,825G (24.5%, 7,300G)
E_Ballard: 838G (11.3%, 838G)
helpimabug: 750G (10.1%, 4,752G)
BirbBrainsBot: 641G (8.6%, 150,271G)
Laserman1000: 600G (8.1%, 3,204G)
NotBalrog: 500G (6.7%, 4,665G)
evontno: 500G (6.7%, 4,275G)
fluffskull: 300G (4.0%, 300G)
Chronolynx42: 300G (4.0%, 50,040G)
CapnChaos12: 300G (4.0%, 3,705G)
lewdylew: 283G (3.8%, 283G)
Rytor: 200G (2.7%, 4,142G)
thewondertrickster: 100G (1.3%, 100G)
CosmicTactician: 100G (1.3%, 15,454G)
flacococo: 100G (1.3%, 1,793G)
datadrivenbot: 100G (1.3%, 47,685G)

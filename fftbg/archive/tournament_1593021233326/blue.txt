Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Laserman1000
Male
Scorpio
77
80
Ninja
Summon Magic
Abandon
Equip Knife
Fly

Cultist Dagger
Mage Masher
Triangle Hat
Mythril Vest
Leather Mantle

Shuriken, Bomb, Stick
Moogle, Shiva, Ramuh, Golem, Odin, Leviathan, Silf, Fairy, Cyclops



BlackFireUK
Female
Aries
46
57
Knight
Dance
Damage Split
Long Status
Lava Walking

Ice Brand
Platinum Shield
Genji Helmet
Silk Robe
Defense Ring

Shield Break, Magic Break, Speed Break, Power Break, Mind Break
Wiznaibus, Polka Polka, Disillusion, Void Storage



Roofiepops
Male
Gemini
71
56
Lancer
Talk Skill
Mana Shield
Defense UP
Move+2

Obelisk
Venetian Shield
Gold Helmet
Black Robe
Reflect Ring

Level Jump8, Vertical Jump5
Persuade, Death Sentence, Refute, Rehabilitate



DLJuggernaut
Male
Gemini
58
57
Bard
Basic Skill
Hamedo
Equip Axe
Ignore Height

Morning Star

Black Hood
Mythril Armor
Wizard Mantle

Angel Song, Life Song, Cheer Song, Magic Song
Throw Stone, Heal, Wish

Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Female
Taurus
60
43
Dancer
Item
Counter Tackle
Equip Polearm
Move+3

Partisan

Green Beret
Clothes
108 Gems

Wiznaibus, Disillusion, Void Storage, Dragon Pit
Potion, X-Potion, Eye Drop, Phoenix Down



Gorgewall
Female
Taurus
54
52
Thief
Battle Skill
Faith Save
Magic Defense UP
Teleport

Dagger

Black Hood
Brigandine
Genji Gauntlet

Steal Armor, Steal Status, Arm Aim
Shield Break, Magic Break, Mind Break, Surging Sword



Goodnightrobo
Monster
Taurus
70
48
Archaic Demon










D4rr1n
Male
Capricorn
70
37
Lancer
Yin Yang Magic
Meatbone Slash
Dual Wield
Ignore Height

Cypress Rod
Mythril Spear
Genji Helmet
Reflect Mail
Spike Shoes

Level Jump3, Vertical Jump8
Poison, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Confusion Song

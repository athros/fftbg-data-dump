Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WalkerNash
Female
Sagittarius
53
80
Calculator
White Magic
MA Save
Magic Attack UP
Retreat

Battle Folio

Black Hood
White Robe
Germinas Boots

Height, Prime Number, 4
Cure 3, Raise, Raise 2, Protect, Protect 2, Shell 2



Omegasuspekt
Male
Capricorn
51
55
Ninja
Item
Blade Grasp
Equip Shield
Jump+1

Blind Knife
Platinum Shield
Ribbon
Leather Outfit
Germinas Boots

Shuriken, Dictionary
Potion, Hi-Ether, Elixir, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down



LanseDM
Male
Pisces
57
44
Ninja
Punch Art
Damage Split
Attack UP
Move-HP Up

Iga Knife
Mage Masher
Leather Hat
Mythril Vest
Rubber Shoes

Shuriken, Bomb
Wave Fist, Secret Fist, Purification, Chakra, Seal Evil



Nomoment
Female
Gemini
62
43
Wizard
Elemental
MA Save
Equip Knife
Move-MP Up

Zorlin Shape

Ribbon
Rubber Costume
Rubber Shoes

Fire 3, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Ice 4, Frog
Local Quake, Will-O-Wisp, Sand Storm, Lava Ball

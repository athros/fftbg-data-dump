Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Cloud92684
Male
Leo
81
78
Priest
Talk Skill
Counter Magic
Defense UP
Teleport

White Staff

Red Hood
Black Robe
Defense Ring

Cure 4, Raise, Raise 2, Protect 2, Esuna, Holy
Persuade, Preach, Rehabilitate



Galkife
Female
Leo
70
68
Priest
Time Magic
Catch
Attack UP
Ignore Terrain

Flail

Leather Hat
Judo Outfit
Sprint Shoes

Cure, Cure 2, Raise, Raise 2, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna
Haste, Haste 2, Stop, Immobilize, Demi



ShintaroNayaka
Male
Libra
69
56
Calculator
White Magic
Catch
Equip Knife
Jump+3

Orichalcum

Headgear
Silk Robe
Leather Mantle

Height, 5, 3
Raise, Raise 2, Shell, Shell 2, Wall



Pandasforsale
Male
Sagittarius
79
50
Geomancer
Punch Art
Parry
Equip Sword
Levitate

Chirijiraden
Flame Shield
Triangle Hat
Silk Robe
Defense Armlet

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm
Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



RunicMagus
Monster
Sagittarius
76
77
Wyvern










Athros13
Male
Taurus
47
71
Chemist
Punch Art
Distribute
Magic Defense UP
Waterbreathing

Star Bag

Green Beret
Adaman Vest
Defense Ring

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down
Spin Fist, Wave Fist, Purification, Revive



Nickyfive
Male
Pisces
43
69
Ninja
Charge
Speed Save
Defense UP
Teleport

Main Gauche
Flame Whip
Golden Hairpin
Leather Outfit
Cursed Ring

Shuriken, Bomb, Axe
Charge+1, Charge+5



Powergems
Male
Scorpio
52
47
Priest
Punch Art
Caution
Defend
Move+3

Flail

Golden Hairpin
Silk Robe
Germinas Boots

Cure, Cure 3, Raise, Raise 2, Wall, Holy
Spin Fist, Purification, Chakra, Revive

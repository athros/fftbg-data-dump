Final Bets: blue - 18 bets for 13,090G (46.5%, x1.15); champion - 26 bets for 15,087G (53.5%, x0.87)

blue bets:
killth3kid: 2,000G (15.3%, 44,115G)
Lali_Lulelo: 1,951G (14.9%, 1,951G)
ZZ_Yoshi: 1,514G (11.6%, 1,514G)
metagameface: 1,220G (9.3%, 1,220G)
BirbBrainsBot: 1,000G (7.6%, 168,640G)
Estan_AD: 1,000G (7.6%, 11,598G)
rico_flex: 1,000G (7.6%, 38,609G)
roofiepops: 568G (4.3%, 568G)
DeathTaxesAndAnime: 504G (3.9%, 504G)
moonliquor: 500G (3.8%, 23,175G)
Mtueni: 400G (3.1%, 233,325G)
thalessenador: 396G (3.0%, 396G)
HaateXIII: 268G (2.0%, 1,392G)
Miyokari: 250G (1.9%, 5,272G)
mannequ1n: 240G (1.8%, 240G)
gorgewall: 201G (1.5%, 8,871G)
tmo50x: 50G (0.4%, 8,275G)
getthemoneyz: 28G (0.2%, 657,868G)

champion bets:
Baron_von_Scrub: 2,552G (16.9%, 16,825G)
Grimmace45: 2,229G (14.8%, 22,292G)
theNGUYENNER: 1,380G (9.1%, 1,380G)
Lionhermit: 1,000G (6.6%, 51,279G)
ungabunga_bot: 742G (4.9%, 423,959G)
cupholderr: 720G (4.8%, 720G)
HaplessOne: 696G (4.6%, 3,482G)
Evewho: 637G (4.2%, 637G)
mikesk1: 500G (3.3%, 2,552G)
CorpusCav: 500G (3.3%, 3,500G)
catfashions: 500G (3.3%, 10,241G)
Laserman1000: 440G (2.9%, 440G)
vorap: 400G (2.7%, 34,059G)
sir_sugarboogerz: 400G (2.7%, 5,526G)
jwchapm: 300G (2.0%, 3,181G)
Primeval_33: 300G (2.0%, 6,077G)
Jaritras: 280G (1.9%, 280G)
NicoSavoy: 265G (1.8%, 265G)
Dexsana: 250G (1.7%, 2,294G)
Kyune: 250G (1.7%, 1,200G)
dogsandcatsand: 236G (1.6%, 236G)
VeryVert: 156G (1.0%, 156G)
Waracy: 104G (0.7%, 104G)
CosmicTactician: 100G (0.7%, 12,391G)
datadrivenbot: 100G (0.7%, 12,782G)
DrAntiSocial: 50G (0.3%, 13,646G)

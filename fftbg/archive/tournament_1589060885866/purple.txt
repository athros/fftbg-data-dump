Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Boojob
Female
Cancer
46
41
Wizard
Yin Yang Magic
Auto Potion
Equip Polearm
Ignore Height

Octagon Rod

Thief Hat
Silk Robe
Rubber Shoes

Fire 4, Ice 3, Ice 4, Flare
Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep



TheNGUYENNER
Female
Capricorn
56
67
Dancer
Charge
Catch
Concentrate
Jump+2

Cashmere

Red Hood
Chameleon Robe
Feather Mantle

Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Last Dance, Obsidian Blade
Charge+3, Charge+5



CosmicTactician
Male
Taurus
42
63
Samurai
Charge
PA Save
Martial Arts
Move+3



Crystal Helmet
Genji Armor
Leather Mantle

Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Charge+4, Charge+7, Charge+10



Mannequ1n
Female
Aries
69
69
Priest
Battle Skill
Meatbone Slash
Halve MP
Move+2

Flail

Black Hood
White Robe
Wizard Mantle

Cure, Cure 3, Raise, Raise 2, Esuna
Shield Break, Magic Break, Dark Sword

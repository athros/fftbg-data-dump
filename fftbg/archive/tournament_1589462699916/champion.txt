Player: !zChamp
Team: Champion Team
Palettes: Green/White



Anomandaris
Female
Cancer
38
75
Samurai
Throw
Counter
Magic Attack UP
Fly

Star Bag

Genji Helmet
Carabini Mail
Vanish Mantle

Koutetsu, Heaven's Cloud, Kikuichimoji
Shuriken



Lokenwow
Female
Virgo
75
48
Oracle
Time Magic
Hamedo
Dual Wield
Move+2

Octagon Rod
Ivory Rod
Triangle Hat
Chameleon Robe
N-Kai Armlet

Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic, Dark Holy
Haste, Slow, Slow 2, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time



CassiePhoenix
Female
Libra
45
58
Oracle
Draw Out
Counter Magic
Magic Defense UP
Swim

Papyrus Codex

Feather Hat
Brigandine
Power Wrist

Blind, Poison, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Masamune



Twelfthrootoftwo
Male
Taurus
60
64
Geomancer
Yin Yang Magic
Dragon Spirit
Equip Sword
Waterbreathing

Rune Blade
Kaiser Plate
Feather Hat
Wizard Robe
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind
Life Drain, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Dark Holy

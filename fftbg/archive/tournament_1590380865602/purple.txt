Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gooseyourself
Female
Leo
71
43
Time Mage
Dance
Counter
Equip Shield
Move-MP Up

Healing Staff
Genji Shield
Headgear
Wizard Robe
Sprint Shoes

Haste 2, Slow, Slow 2, Stop, Float, Reflect
Witch Hunt, Slow Dance, Disillusion



FriendSquirrel
Female
Pisces
70
60
Chemist
Punch Art
MP Restore
Concentrate
Waterbreathing

Blaze Gun

Holy Miter
Adaman Vest
Rubber Shoes

Potion, Hi-Potion, X-Potion, Hi-Ether, Holy Water, Remedy, Phoenix Down
Spin Fist, Earth Slash, Purification, Revive



Vorap
Male
Gemini
66
67
Calculator
Yin Yang Magic
Counter
Doublehand
Jump+2

Cypress Rod

Red Hood
Light Robe
Defense Ring

CT, 5, 4, 3
Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep



DeathTaxesAndAnime
Female
Sagittarius
66
76
Geomancer
Black Magic
Damage Split
Short Charge
Jump+2

Kikuichimoji
Crystal Shield
Golden Hairpin
Clothes
Defense Armlet

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Fire, Fire 3, Fire 4, Bolt 2, Ice 4, Death

Player: !Red
Team: Red Team
Palettes: Red/Brown



Genericco
Female
Virgo
60
58
Squire
Jump
Parry
Equip Knife
Levitate

Flame Rod
Platinum Shield
Genji Helmet
Clothes
Cursed Ring

Wish
Level Jump2, Vertical Jump8



Redrhinofever
Male
Capricorn
66
77
Samurai
Charge
Absorb Used MP
Concentrate
Move-MP Up

Chirijiraden

Iron Helmet
Plate Mail
Leather Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Charge+3, Charge+10



Lifeguard Dan
Female
Aquarius
69
65
Geomancer
Battle Skill
Arrow Guard
Martial Arts
Jump+3


Round Shield
Flash Hat
Chain Vest
Battle Boots

Pitfall, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball
Magic Break



Moocaotao
Female
Scorpio
58
70
Samurai
Summon Magic
Abandon
Long Status
Jump+2

Mythril Spear

Iron Helmet
Black Robe
Diamond Armlet

Murasame, Kiyomori, Muramasa
Moogle, Shiva, Ramuh, Golem, Carbunkle, Leviathan, Fairy, Zodiac

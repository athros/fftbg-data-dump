Player: !Red
Team: Red Team
Palettes: Red/Brown



AM3P Batman
Male
Taurus
72
56
Bard
Punch Art
Caution
Sicken
Jump+3

Panther Bag

Golden Hairpin
Leather Armor
Power Wrist

Life Song, Battle Song, Hydra Pit
Purification, Chakra, Seal Evil



Jethrothrul
Male
Libra
71
75
Chemist
Punch Art
Distribute
Magic Attack UP
Move+3

Mythril Knife

Ribbon
Judo Outfit
Power Wrist

Antidote, Echo Grass, Soft, Remedy, Phoenix Down
Purification, Seal Evil



Lowlf
Male
Capricorn
42
80
Ninja
White Magic
PA Save
Equip Sword
Move+1

Long Sword
Coral Sword
Twist Headband
Clothes
Wizard Mantle

Shuriken
Cure, Cure 3, Raise, Protect 2, Shell, Shell 2, Esuna, Holy



Leakimiko
Female
Gemini
46
56
Summoner
White Magic
Absorb Used MP
Short Charge
Swim

Wizard Staff

Twist Headband
Silk Robe
Vanish Mantle

Shiva, Ifrit, Titan, Golem, Bahamut, Silf, Fairy, Cyclops
Cure 4, Raise, Raise 2, Regen, Protect, Shell 2, Wall, Esuna

Final Bets: white - 7 bets for 6,813G (61.9%, x0.62); black - 7 bets for 4,202G (38.1%, x1.62)

white bets:
UmaiJam: 3,000G (44.0%, 42,327G)
SomthingMore: 1,400G (20.5%, 50,192G)
reinoe: 1,000G (14.7%, 68,998G)
NovaKnight21: 536G (7.9%, 536G)
ShintaroNayaka: 476G (7.0%, 476G)
gorgewall: 201G (3.0%, 5,228G)
datadrivenbot: 200G (2.9%, 47,403G)

black bets:
maximumcrit: 1,000G (23.8%, 8,370G)
BirbBrainsBot: 1,000G (23.8%, 11,465G)
AllInBot: 776G (18.5%, 776G)
EmmaEnema: 500G (11.9%, 6,508G)
BaronHaynes: 500G (11.9%, 8,935G)
DesertWooder: 250G (5.9%, 2,115G)
getthemoneyz: 176G (4.2%, 1,516,832G)

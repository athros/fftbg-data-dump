Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DeadGirlzREasy
Male
Leo
59
54
Thief
Draw Out
Arrow Guard
Maintenance
Jump+3

Hidden Knife

Black Hood
Mythril Vest
Salty Rage

Steal Heart, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Asura, Muramasa, Kikuichimoji



Fluffskull
Monster
Sagittarius
46
68
Apanda










CorpusCav
Male
Libra
42
59
Oracle
Sing
Arrow Guard
Equip Sword
Fly

Ancient Sword

Leather Hat
Black Costume
Vanish Mantle

Blind, Poison, Silence Song, Confusion Song, Dispel Magic
Angel Song, Cheer Song, Hydra Pit



Lythe Caraker
Male
Virgo
64
73
Bard
Yin Yang Magic
Parry
Equip Polearm
Move+2

Obelisk

Black Hood
Adaman Vest
Diamond Armlet

Last Song
Spell Absorb, Doubt Faith, Blind Rage, Confusion Song, Paralyze, Dark Holy

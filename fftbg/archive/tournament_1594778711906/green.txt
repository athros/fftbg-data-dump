Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Female
Pisces
60
40
Oracle
Basic Skill
Damage Split
Dual Wield
Waterwalking

Battle Folio
Bestiary
Flash Hat
Wizard Robe
Jade Armlet

Life Drain, Silence Song, Dispel Magic
Dash, Throw Stone, Wish, Scream



DaKoopa49
Male
Capricorn
72
77
Monk
Charge
Counter
Secret Hunt
Move+1



Leather Hat
Chain Vest
Feather Mantle

Spin Fist, Purification, Chakra, Revive, Seal Evil
Charge+5, Charge+7, Charge+10, Charge+20



ALY327
Female
Leo
46
68
Mediator
Jump
Distribute
Equip Knife
Move+2

Orichalcum

Black Hood
Clothes
Wizard Mantle

Preach, Insult, Refute, Rehabilitate
Level Jump2, Vertical Jump7



Helpimabug
Male
Scorpio
74
67
Lancer
Draw Out
Counter Magic
Defend
Teleport

Partisan
Escutcheon
Barbuta
Plate Mail
Defense Armlet

Level Jump8, Vertical Jump3
Murasame

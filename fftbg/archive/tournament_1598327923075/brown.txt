Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Evdoggity
Male
Taurus
51
39
Archer
Steal
Meatbone Slash
Doublehand
Jump+1

Mythril Bow

Triangle Hat
Chain Vest
Defense Ring

Charge+2, Charge+4, Charge+5, Charge+7
Gil Taking, Steal Shield, Steal Weapon, Leg Aim



CorpusCav
Female
Taurus
76
79
Summoner
Dance
Abandon
Equip Shield
Fly

Wizard Rod
Flame Shield
Green Beret
Brigandine
Wizard Mantle

Ramuh, Golem, Odin, Salamander
Wiznaibus, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Nether Demon



Zenlion
Female
Capricorn
41
62
Dancer
Yin Yang Magic
Sunken State
Sicken
Move+3

Cashmere

Golden Hairpin
Brigandine
Defense Armlet

Witch Hunt, Wiznaibus, Slow Dance, Disillusion
Blind, Poison, Spell Absorb, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep



Thunderducker
Male
Capricorn
47
68
Ninja
Item
Hamedo
Equip Sword
Move+2

Defender
Asura Knife
Holy Miter
Adaman Vest
Germinas Boots

Wand
Potion, Hi-Ether, Eye Drop, Soft, Phoenix Down

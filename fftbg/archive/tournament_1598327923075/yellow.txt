Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Foamsoldier
Male
Virgo
38
73
Archer
Punch Art
Auto Potion
Equip Bow
Levitate

Snipe Bow
Kaiser Plate
Feather Hat
Mystic Vest
Battle Boots

Charge+2, Charge+3, Charge+10, Charge+20
Spin Fist, Pummel, Earth Slash, Revive



MDMAyylmao
Male
Taurus
66
54
Lancer
Sing
Arrow Guard
Halve MP
Swim

Spear
Flame Shield
Barbuta
Carabini Mail
Angel Ring

Level Jump2, Vertical Jump2
Angel Song, Space Storage



Gorgewall
Male
Sagittarius
72
51
Lancer
Draw Out
Parry
Doublehand
Move+3

Mythril Spear

Mythril Helmet
Carabini Mail
Magic Gauntlet

Level Jump5, Vertical Jump5
Asura, Kikuichimoji



IBardic
Male
Cancer
53
68
Geomancer
Yin Yang Magic
Faith Save
Long Status
Fly

Battle Axe
Mythril Shield
Leather Hat
Silk Robe
Reflect Ring

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Paralyze, Sleep, Petrify

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Leakimiko
Male
Taurus
53
54
Monk
Item
Critical Quick
Equip Sword
Ignore Height

Bizen Boat

Leather Hat
Chain Vest
Jade Armlet

Purification, Chakra
Ether, Hi-Ether, Eye Drop, Soft



Mirapoix
Female
Sagittarius
65
48
Wizard
Math Skill
Regenerator
Halve MP
Lava Walking

Wizard Rod

Cachusha
Chameleon Robe
Cursed Ring

Fire, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4, Death
CT, 5, 4



ThanatosXRagnarok
Female
Serpentarius
81
63
Monk
Black Magic
MP Restore
Attack UP
Move+3



Green Beret
Mythril Vest
Defense Armlet

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive, Seal Evil
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 2, Empower



Winterharte
Male
Pisces
58
74
Mime

Speed Save
Equip Armor
Lava Walking



Genji Helmet
Carabini Mail
Vanish Mantle

Mimic


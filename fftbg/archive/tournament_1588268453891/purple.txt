Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DudeMonkey77
Female
Sagittarius
54
82
Archer
Yin Yang Magic
Arrow Guard
Martial Arts
Lava Walking

Blaze Gun
Bronze Shield
Circlet
Leather Outfit
Bracer

Charge+2, Charge+7, Charge+10
Blind, Spell Absorb, Doubt Faith, Sleep



Oldmanbody
Female
Aquarius
44
77
Oracle
Elemental
Earplug
Defense UP
Move-MP Up

Musk Rod

Golden Hairpin
Wizard Outfit
Small Mantle

Blind, Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Kohlingen
Female
Taurus
77
66
Wizard
Time Magic
MA Save
Doublehand
Ignore Height

Assassin Dagger

Golden Hairpin
Wizard Robe
Angel Ring

Fire 2, Fire 3, Fire 4, Bolt 2, Ice 2
Haste, Haste 2, Immobilize, Reflect, Demi, Demi 2



ApplesauceBoss
Male
Aries
40
74
Archer
Item
PA Save
Doublehand
Move-HP Up

Perseus Bow

Golden Hairpin
Earth Clothes
Jade Armlet

Charge+2, Charge+4, Charge+7, Charge+20
Potion, X-Potion, Echo Grass

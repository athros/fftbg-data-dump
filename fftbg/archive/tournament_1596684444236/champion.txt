Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Douchetron
Monster
Capricorn
75
45
Minotaur










Vorap
Male
Cancer
53
53
Thief
Jump
Abandon
Attack UP
Move-HP Up

Spell Edge

Triangle Hat
Clothes
Power Wrist

Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Level Jump8, Vertical Jump4



Redmage4evah
Male
Sagittarius
76
74
Geomancer
Throw
Brave Save
Attack UP
Ignore Terrain

Asura Knife
Ice Shield
Flash Hat
Wizard Robe
Genji Gauntlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bomb, Axe



Seaweed B
Male
Aquarius
49
49
Calculator
Yin Yang Magic
Sunken State
Attack UP
Swim

Bestiary

Black Hood
Silk Robe
Battle Boots

CT, Height, Prime Number, 5, 4, 3
Zombie, Foxbird, Dispel Magic, Petrify, Dark Holy

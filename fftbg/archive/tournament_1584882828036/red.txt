Player: !Red
Team: Red Team
Palettes: Red/Brown



Bongomon7
Male
Leo
75
70
Thief
Black Magic
Parry
Magic Defense UP
Move-HP Up

Air Knife

Headgear
Secret Clothes
Angel Ring

Steal Heart, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Empower, Flare



Gooseyourself
Male
Gemini
57
80
Monk
Jump
Counter
Halve MP
Fly



Feather Hat
Leather Outfit
Red Shoes

Spin Fist, Wave Fist, Purification, Revive
Level Jump2, Vertical Jump7



MoonSlayerRS
Female
Serpentarius
50
57
Priest
Battle Skill
Arrow Guard
Secret Hunt
Move+2

Flail

Headgear
Light Robe
Magic Ring

Cure 2, Cure 3, Raise, Regen, Protect, Protect 2, Shell 2, Esuna, Holy
Head Break, Power Break, Surging Sword



DaveStrider55
Male
Leo
72
43
Calculator
Yin Yang Magic
Auto Potion
Equip Shield
Jump+1

Cypress Rod
Gold Shield
Black Hood
Mythril Vest
Defense Ring

CT, Height, Prime Number, 3
Blind, Spell Absorb, Pray Faith, Doubt Faith, Foxbird, Sleep, Dark Holy

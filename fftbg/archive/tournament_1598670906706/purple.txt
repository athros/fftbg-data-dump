Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mushufasa
Male
Serpentarius
70
63
Bard
Elemental
Counter
Dual Wield
Move+3

Bloody Strings
Fairy Harp
Black Hood
Wizard Outfit
Dracula Mantle

Angel Song, Cheer Song, Last Song, Diamond Blade, Sky Demon
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Ruleof5
Female
Pisces
67
72
Archer
Draw Out
Absorb Used MP
Doublehand
Move-MP Up

Ice Bow

Circlet
Judo Outfit
Red Shoes

Charge+1, Charge+4, Charge+7, Charge+10
Kiyomori, Kikuichimoji, Chirijiraden



Leonidusx
Male
Leo
61
70
Knight
Basic Skill
Absorb Used MP
Magic Attack UP
Levitate

Chaos Blade
Escutcheon
Mythril Helmet
Diamond Armor
108 Gems

Shield Break, Mind Break
Dash, Heal, Wish



Runeseeker22
Male
Gemini
60
47
Summoner
Throw
Abandon
Doublehand
Retreat

White Staff

Triangle Hat
Light Robe
Magic Ring

Moogle, Shiva, Carbunkle, Bahamut
Shuriken, Dictionary

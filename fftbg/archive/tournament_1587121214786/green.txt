Player: !Green
Team: Green Team
Palettes: Green/White



CassiePhoenix
Female
Libra
45
61
Time Mage
Basic Skill
Parry
Martial Arts
Jump+2

Wizard Staff

Headgear
Linen Robe
Angel Ring

Haste, Float, Reflect, Demi, Stabilize Time
Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Fury



Lali Lulelo
Female
Scorpio
69
73
Oracle
Black Magic
Caution
Magic Defense UP
Jump+3

Gokuu Rod

Red Hood
Black Costume
Diamond Armlet

Blind, Spell Absorb, Pray Faith, Confusion Song, Paralyze, Dark Holy
Fire, Fire 3, Bolt 2, Ice 3, Empower



HASTERIOUS
Female
Aries
59
77
Chemist
Black Magic
Counter Flood
Attack UP
Retreat

Blast Gun

Feather Hat
Wizard Outfit
Defense Ring

Ether, Hi-Ether, Soft, Phoenix Down
Fire 2, Fire 3, Bolt, Bolt 4, Frog



GrandmasterFrankerZ
Female
Aries
54
38
Oracle
Basic Skill
PA Save
Dual Wield
Teleport

Bestiary
Bestiary
Triangle Hat
Judo Outfit
Bracer

Blind, Poison, Spell Absorb, Zombie, Silence Song, Blind Rage, Dispel Magic
Throw Stone, Tickle, Cheer Up, Wish

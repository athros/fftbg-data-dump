Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Killth3kid
Male
Pisces
55
46
Oracle
Talk Skill
Faith Save
Equip Armor
Move-MP Up

Iron Fan

Gold Helmet
Brigandine
Jade Armlet

Blind, Poison, Spell Absorb, Pray Faith, Silence Song, Blind Rage, Confusion Song, Petrify
Invitation, Praise, Solution, Insult, Negotiate, Mimic Daravon



Spartan Paladin
Male
Scorpio
62
75
Knight
Talk Skill
Counter Flood
Equip Bow
Move-MP Up

Cross Bow
Diamond Shield
Leather Helmet
Crystal Mail
Bracer

Armor Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Invitation, Persuade, Threaten, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate



GatsbysGhost
Male
Sagittarius
50
72
Mediator
Punch Art
Arrow Guard
Concentrate
Move+3

Mythril Gun

Holy Miter
Adaman Vest
Power Wrist

Threaten, Death Sentence, Mimic Daravon
Wave Fist, Earth Slash, Chakra, Revive



UmaiJam
Female
Aries
58
52
Priest
Elemental
Dragon Spirit
Equip Shield
Jump+3

Healing Staff
Bronze Shield
Thief Hat
Mythril Vest
Battle Boots

Cure, Raise, Protect, Protect 2, Shell, Wall, Esuna, Holy
Pitfall, Hell Ivy, Static Shock, Quicksand, Sand Storm

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Pie108
Female
Aries
62
56
Dancer
Item
Earplug
Throw Item
Ignore Terrain

Hydra Bag

Holy Miter
Black Robe
Battle Boots

Wiznaibus, Polka Polka, Nameless Dance, Dragon Pit
Potion, Ether, Echo Grass, Maiden's Kiss, Phoenix Down



Alrightbye
Female
Leo
57
54
Dancer
Summon Magic
Sunken State
Secret Hunt
Move+1

Persia

Golden Hairpin
White Robe
Elf Mantle

Slow Dance, Disillusion, Nameless Dance
Moogle, Shiva, Ifrit, Silf, Lich, Cyclops



HaplessOne
Female
Taurus
80
46
Oracle
Black Magic
MP Restore
Dual Wield
Move+1

Musk Rod
Iron Fan
Feather Hat
Chameleon Robe
Power Wrist

Zombie, Foxbird, Confusion Song
Fire 2, Ice 2



InOzWeTrust
Female
Capricorn
71
72
Squire
Yin Yang Magic
Absorb Used MP
Long Status
Move-HP Up

Battle Axe
Bronze Shield
Golden Hairpin
Judo Outfit
Spike Shoes

Accumulate, Dash, Heal, Yell, Fury, Scream
Poison, Life Drain, Dispel Magic

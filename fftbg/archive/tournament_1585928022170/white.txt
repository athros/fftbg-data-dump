Player: !White
Team: White Team
Palettes: White/Blue



Slowbrofist
Male
Gemini
42
72
Monk
Draw Out
Parry
Short Charge
Jump+1



Feather Hat
Brigandine
Bracer

Purification, Revive, Seal Evil
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



SeedSC
Female
Virgo
74
76
Oracle
Punch Art
Dragon Spirit
Equip Armor
Levitate

Bestiary

Iron Helmet
Chain Mail
Dracula Mantle

Blind, Poison, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze, Sleep
Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil



Smiggenator
Male
Cancer
57
61
Priest
Jump
Counter Flood
Equip Shield
Waterbreathing

Flail
Escutcheon
Feather Hat
Silk Robe
Diamond Armlet

Raise, Protect, Protect 2, Shell 2, Wall, Esuna
Level Jump3, Vertical Jump3



Jethrothrul
Male
Sagittarius
79
79
Ninja
White Magic
Absorb Used MP
Equip Gun
Swim

Bestiary
Bloody Strings
Feather Hat
Judo Outfit
Defense Ring

Staff
Cure 3, Raise, Protect 2, Shell

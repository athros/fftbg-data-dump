Player: !Red
Team: Red Team
Palettes: Red/Brown



Zmoses
Female
Aquarius
82
79
Dancer
Summon Magic
Caution
Attack UP
Ignore Height

Ryozan Silk

Holy Miter
Power Sleeve
Feather Mantle

Witch Hunt, Slow Dance, Disillusion, Last Dance, Obsidian Blade
Moogle, Shiva, Bahamut, Odin, Silf



Dogsandcatsand
Male
Gemini
63
58
Summoner
Jump
Parry
Short Charge
Move+2

Battle Folio

Feather Hat
Light Robe
Power Wrist

Moogle, Golem, Carbunkle, Leviathan, Salamander
Level Jump8, Vertical Jump8



Tacobowl8
Female
Leo
50
56
Mime

Earplug
Dual Wield
Move-MP Up



Flash Hat
Judo Outfit
Cursed Ring

Mimic




Firesheath
Male
Aquarius
59
79
Wizard
Punch Art
Counter Flood
Doublehand
Lava Walking

Dagger

Leather Hat
Clothes
N-Kai Armlet

Fire 2, Bolt 3, Ice 2, Frog
Pummel, Earth Slash, Secret Fist, Purification, Revive

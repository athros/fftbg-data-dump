Final Bets: white - 9 bets for 10,166G (53.9%, x0.86); black - 9 bets for 8,703G (46.1%, x1.17)

white bets:
TheChainNerd: 4,918G (48.4%, 29,918G)
killth3kid: 1,000G (9.8%, 107,914G)
reddwind_: 1,000G (9.8%, 47,658G)
dogsandcatsand: 949G (9.3%, 4,748G)
run_with_stone_GUNs: 662G (6.5%, 13,247G)
ShintaroNayaka: 632G (6.2%, 632G)
EnemyController: 555G (5.5%, 1,381,173G)
Ixionnyu: 250G (2.5%, 2,108G)
datadrivenbot: 200G (2.0%, 54,775G)

black bets:
neerrm: 4,000G (46.0%, 15,655G)
fenaen: 1,200G (13.8%, 25,746G)
BirbBrainsBot: 1,000G (11.5%, 124,977G)
Nickyfive: 985G (11.3%, 985G)
maakur_: 520G (6.0%, 520G)
getthemoneyz: 510G (5.9%, 1,218,014G)
ArashiKurobara: 288G (3.3%, 9,583G)
nhammen: 100G (1.1%, 12,104G)
lastly: 100G (1.1%, 36,394G)

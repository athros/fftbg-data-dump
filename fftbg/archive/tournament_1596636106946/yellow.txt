Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dogsandcatsand
Female
Libra
71
63
Wizard
Summon Magic
Earplug
Equip Polearm
Swim

Gokuu Rod

Thief Hat
Earth Clothes
Reflect Ring

Ice 3, Death
Moogle, Carbunkle, Silf



ShintaroNayaka
Monster
Gemini
65
68
Iron Hawk










NIghtdew14
Female
Libra
61
79
Dancer
Yin Yang Magic
Sunken State
Equip Armor
Move+2

Ryozan Silk

Leather Helmet
Genji Armor
Magic Ring

Last Dance, Dragon Pit
Spell Absorb, Confusion Song, Dispel Magic, Petrify



Actual JP
Female
Scorpio
71
49
Monk
Talk Skill
Counter Magic
Equip Polearm
Ignore Height

Musk Rod

Holy Miter
Power Sleeve
108 Gems

Pummel, Wave Fist, Purification
Invitation, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute

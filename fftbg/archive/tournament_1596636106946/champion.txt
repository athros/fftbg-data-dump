Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Ar Tactic
Male
Capricorn
66
79
Monk
Time Magic
Dragon Spirit
Magic Defense UP
Move+3



Golden Hairpin
Earth Clothes
Battle Boots

Pummel, Purification, Chakra, Seal Evil
Haste 2, Stop, Immobilize, Float, Reflect, Quick, Demi, Stabilize Time



Grininda
Male
Virgo
76
76
Archer
Black Magic
Earplug
Doublehand
Move+2

Silver Bow

Golden Hairpin
Judo Outfit
Jade Armlet

Charge+1, Charge+3, Charge+5, Charge+10
Fire 4, Bolt, Ice 4, Frog



Maximumcrit
Monster
Taurus
76
44
King Behemoth










DesertWooder
Male
Gemini
51
79
Lancer
Summon Magic
Brave Save
Magic Defense UP
Lava Walking

Holy Lance
Platinum Shield
Barbuta
Carabini Mail
Feather Mantle

Level Jump3, Vertical Jump4
Ifrit, Titan, Carbunkle, Leviathan, Salamander, Silf

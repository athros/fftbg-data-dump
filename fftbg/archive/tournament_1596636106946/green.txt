Player: !Green
Team: Green Team
Palettes: Green/White



Miku Shikhu
Male
Gemini
72
46
Oracle
Time Magic
Mana Shield
Magic Defense UP
Move-MP Up

Ivory Rod

Headgear
Light Robe
Angel Ring

Spell Absorb, Pray Faith, Doubt Faith, Blind Rage, Petrify
Stop, Immobilize, Reflect, Demi, Stabilize Time



Finewax
Male
Cancer
61
62
Bard
Time Magic
Absorb Used MP
Equip Armor
Move+2

Ramia Harp

Mythril Helmet
Wizard Outfit
108 Gems

Diamond Blade, Sky Demon
Haste 2, Slow 2, Float, Quick, Demi 2, Stabilize Time



LDSkinny
Female
Capricorn
70
61
Oracle
Basic Skill
Catch
Long Status
Move+2

Octagon Rod

Golden Hairpin
Wizard Robe
Magic Ring

Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Paralyze
Accumulate, Dash, Tickle, Scream



Grininda
Male
Gemini
72
44
Knight
Sing
Auto Potion
Equip Bow
Move+1

Hunting Bow
Hero Shield
Gold Helmet
Silk Robe
Battle Boots

Speed Break, Stasis Sword, Justice Sword
Angel Song, Cheer Song, Last Song, Sky Demon, Hydra Pit

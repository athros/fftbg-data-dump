Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thejoden
Female
Capricorn
64
69
Priest
Punch Art
Counter Flood
Martial Arts
Move+2



Leather Hat
Wizard Robe
Germinas Boots

Cure 2, Cure 4, Raise, Raise 2, Regen, Shell, Shell 2, Esuna
Wave Fist, Purification



RurouniGeo
Male
Aries
55
70
Knight
White Magic
Counter
Equip Sword
Ignore Terrain

Diamond Sword
Buckler
Barbuta
Linen Robe
Magic Gauntlet

Armor Break, Shield Break, Magic Break, Power Break, Justice Sword
Cure, Cure 2, Raise, Protect, Protect 2, Esuna



Maeveen
Male
Scorpio
57
67
Monk
Jump
Brave Up
Attack UP
Move-HP Up



Flash Hat
Secret Clothes
Leather Mantle

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Level Jump2, Vertical Jump7



Moonliquor
Male
Leo
69
67
Thief
Time Magic
Mana Shield
Attack UP
Ignore Terrain

Dagger

Flash Hat
Earth Clothes
Battle Boots

Gil Taking, Steal Armor, Steal Weapon, Steal Accessory, Steal Status
Haste, Slow 2, Stop, Stabilize Time

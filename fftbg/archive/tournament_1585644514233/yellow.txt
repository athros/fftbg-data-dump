Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lydian C
Female
Leo
80
39
Dancer
Item
Faith Up
Martial Arts
Levitate



Thief Hat
Chain Vest
Small Mantle

Slow Dance, Polka Polka
Potion, X-Potion, Eye Drop, Echo Grass, Remedy



PatSouI
Male
Virgo
47
77
Summoner
Throw
Counter
Equip Bow
Waterwalking

Hunting Bow

Headgear
Black Costume
Power Wrist

Shiva, Ramuh, Ifrit, Bahamut, Salamander, Lich
Shuriken, Bomb



SleepyBenzo
Male
Libra
62
80
Wizard
Summon Magic
Critical Quick
Equip Gun
Swim

Bestiary

Red Hood
White Robe
Genji Gauntlet

Fire 3, Bolt, Ice 3, Ice 4
Moogle, Ifrit, Golem, Leviathan, Salamander



Sairentozon7
Male
Pisces
75
56
Squire
Throw
Earplug
Equip Gun
Move+2

Romanda Gun
Flame Shield
Leather Hat
Leather Outfit
Small Mantle

Dash, Throw Stone, Heal, Yell, Wish
Shuriken

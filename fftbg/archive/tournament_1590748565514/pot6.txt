Final Bets: black - 8 bets for 30,461G (79.7%, x0.25); brown - 11 bets for 7,750G (20.3%, x3.93)

black bets:
LivingHitokiri: 21,776G (71.5%, 21,776G)
rechaun: 4,027G (13.2%, 8,054G)
TheChainNerd: 2,000G (6.6%, 72,596G)
megaman2202: 1,665G (5.5%, 6,663G)
Jeeboheebo: 492G (1.6%, 492G)
gorgewall: 201G (0.7%, 15,678G)
Saldarin: 200G (0.7%, 5,416G)
datadrivenbot: 100G (0.3%, 28,284G)

brown bets:
Mesmaster: 2,000G (25.8%, 92,492G)
NovaKnight21: 1,340G (17.3%, 1,340G)
BirbBrainsBot: 1,000G (12.9%, 65,101G)
NicoSavoy: 1,000G (12.9%, 18,528G)
sinnyil2: 912G (11.8%, 912G)
bruubarg: 786G (10.1%, 786G)
Lydian_C: 390G (5.0%, 3,998G)
AllInBot: 100G (1.3%, 100G)
CosmicTactician: 100G (1.3%, 4,464G)
getthemoneyz: 72G (0.9%, 759,577G)
emperor_knight: 50G (0.6%, 374G)

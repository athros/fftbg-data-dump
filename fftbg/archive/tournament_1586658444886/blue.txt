Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaateXIII
Female
Libra
77
62
Knight
Talk Skill
Mana Shield
Equip Sword
Teleport

Ice Brand
Crystal Shield
Leather Helmet
Linen Cuirass
Salty Rage

Head Break, Shield Break, Mind Break, Stasis Sword, Surging Sword
Invitation, Persuade, Death Sentence, Refute



Rocl
Female
Aquarius
75
50
Oracle
White Magic
Auto Potion
Defend
Waterbreathing

Octagon Rod

Thief Hat
Judo Outfit
Cherche

Spell Absorb, Doubt Faith, Zombie, Foxbird, Paralyze, Sleep, Dark Holy
Regen, Protect 2



Dennarai
Female
Libra
66
72
Time Mage
Dance
Counter Tackle
Magic Defense UP
Swim

Wizard Staff

Barette
Linen Robe
Defense Armlet

Haste, Haste 2, Slow, Stop, Immobilize, Demi 2
Witch Hunt, Wiznaibus, Disillusion, Dragon Pit



Hamborn
Female
Aries
65
42
Knight
Basic Skill
Earplug
Martial Arts
Teleport

Coral Sword
Crystal Shield
Diamond Helmet
Diamond Armor
Defense Armlet

Armor Break, Magic Break, Justice Sword, Dark Sword, Night Sword, Surging Sword
Yell, Cheer Up, Wish

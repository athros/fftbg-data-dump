Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Chuckolator
Male
Sagittarius
50
44
Priest
Battle Skill
HP Restore
Magic Defense UP
Jump+2

Flame Whip

Triangle Hat
Mystic Vest
Chantage

Cure, Raise, Protect 2, Shell, Esuna
Shield Break, Speed Break, Mind Break, Stasis Sword



Gyarz
Female
Libra
68
60
Ninja
Item
Dragon Spirit
Halve MP
Move+3

Morning Star
Morning Star
Black Hood
Leather Outfit
Power Wrist

Bomb, Wand
Potion, Hi-Potion, Hi-Ether, Remedy, Phoenix Down



Red Lancer
Male
Taurus
73
75
Geomancer
Black Magic
Parry
Defense UP
Waterbreathing

Slasher
Ice Shield
Barette
Clothes
Dracula Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Frog, Flare



Melbraega
Female
Aquarius
50
52
Geomancer
White Magic
Mana Shield
Concentrate
Jump+2

Chirijiraden
Flame Shield
Headgear
Power Sleeve
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure, Cure 2, Cure 4, Raise, Wall, Esuna

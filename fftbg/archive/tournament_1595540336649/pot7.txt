Final Bets: yellow - 11 bets for 4,200G (16.4%, x5.11); brown - 18 bets for 21,474G (83.6%, x0.20)

yellow bets:
dogsandcatsand: 1,222G (29.1%, 19,217G)
Miku_Shikhu: 737G (17.5%, 737G)
killth3kid: 492G (11.7%, 9,202G)
lyonslegacy: 399G (9.5%, 21,537G)
rockmem21: 319G (7.6%, 319G)
Treafa: 280G (6.7%, 1,179G)
gorgewall: 201G (4.8%, 17,211G)
BirbBrainsBot: 200G (4.8%, 200G)
ribbiks: 200G (4.8%, 1,511G)
Vultuous: 100G (2.4%, 765G)
nhammen: 50G (1.2%, 2,264G)

brown bets:
BoneMiser: 11,779G (54.9%, 23,559G)
reinoe: 2,000G (9.3%, 92,589G)
twelfthrootoftwo: 1,129G (5.3%, 1,129G)
ColetteMSLP: 1,000G (4.7%, 20,514G)
upvla: 946G (4.4%, 11,036G)
DustBirdEX: 889G (4.1%, 889G)
Nizaha: 576G (2.7%, 17,951G)
RaIshtar: 524G (2.4%, 524G)
Lanshaft: 496G (2.3%, 23,804G)
randgridr: 476G (2.2%, 9,912G)
ShintaroNayaka: 400G (1.9%, 3,087G)
SephDarkheart: 364G (1.7%, 364G)
flacococo: 295G (1.4%, 295G)
thehotrefrigerator: 204G (0.9%, 204G)
AllInBot: 200G (0.9%, 200G)
Thyrandaal: 100G (0.5%, 66,491G)
DouglasDragonThePoet: 50G (0.2%, 3,527G)
getthemoneyz: 46G (0.2%, 1,354,415G)

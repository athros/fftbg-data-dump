Final Bets: yellow - 8 bets for 2,121G (20.9%, x3.79); purple - 13 bets for 8,029G (79.1%, x0.26)

yellow bets:
Ixionnyu: 867G (40.9%, 1,700G)
BirbBrainsBot: 635G (29.9%, 127,927G)
VolgraTheMoose: 251G (11.8%, 6,988G)
DustBirdEX: 234G (11.0%, 3,569G)
Thyrandaal: 100G (4.7%, 8,591G)
hiros13gts: 20G (0.9%, 3,002G)
raenrond: 10G (0.5%, 1,085G)
getthemoneyz: 4G (0.2%, 1,216,475G)

purple bets:
NIghtdew14: 1,500G (18.7%, 13,855G)
Error72: 1,320G (16.4%, 2,641G)
DeathTaxesAndAnime: 1,017G (12.7%, 2,817G)
run_with_stone_GUNs: 712G (8.9%, 14,240G)
dogsandcatsand: 666G (8.3%, 5,177G)
TheChainNerd: 547G (6.8%, 15,547G)
JumbocactuarX27: 500G (6.2%, 11,045G)
DavenIII: 500G (6.2%, 1,620G)
xLilAsia: 383G (4.8%, 767G)
ShintaroNayaka: 300G (3.7%, 2,246G)
ArashiKurobara: 284G (3.5%, 6,004G)
datadrivenbot: 200G (2.5%, 55,371G)
nhammen: 100G (1.2%, 12,151G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Dogsandcatsand
Female
Cancer
76
70
Wizard
Dance
Brave Save
Defend
Jump+3

Flame Rod

Golden Hairpin
Linen Robe
Magic Gauntlet

Bolt, Death
Wiznaibus



CassiePhoenix
Monster
Cancer
52
55
Chocobo










Just Here2
Male
Cancer
70
60
Squire
Steal
Regenerator
Dual Wield
Teleport

Diamond Sword
Long Sword
Black Hood
Chain Vest
Elf Mantle

Accumulate, Dash, Heal, Tickle, Cheer Up, Ultima
Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory



Ruebyy
Male
Taurus
73
56
Knight
Summon Magic
MA Save
Beastmaster
Move+3

Mythril Sword
Aegis Shield
Circlet
Robe of Lords
Defense Ring

Weapon Break, Magic Break, Power Break, Justice Sword, Surging Sword
Moogle, Ramuh, Golem, Carbunkle, Salamander

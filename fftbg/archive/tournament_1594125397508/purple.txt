Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Powergems
Male
Libra
71
68
Geomancer
Time Magic
Auto Potion
Concentrate
Teleport 2

Battle Axe
Platinum Shield
Holy Miter
Light Robe
N-Kai Armlet

Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Haste, Haste 2, Slow 2, Stop, Immobilize, Float, Stabilize Time



NIghtdew14
Female
Cancer
46
75
Dancer
Talk Skill
Auto Potion
Monster Talk
Levitate

Hydra Bag

Feather Hat
Black Costume
Reflect Ring

Witch Hunt, Disillusion, Obsidian Blade
Invitation, Persuade, Threaten, Solution, Death Sentence, Refute, Rehabilitate



Hiros13gts
Male
Capricorn
79
79
Knight
White Magic
Critical Quick
Secret Hunt
Ignore Terrain

Long Sword
Kaiser Plate
Iron Helmet
White Robe
108 Gems

Weapon Break, Surging Sword
Cure 2, Cure 3, Raise 2, Reraise, Regen, Wall



Belkra
Male
Gemini
58
72
Oracle
White Magic
Parry
Halve MP
Move-HP Up

Octagon Rod

Leather Hat
Brigandine
Cursed Ring

Blind, Pray Faith, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Cure 2, Cure 4, Regen, Esuna

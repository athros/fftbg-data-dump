Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



LordTomS
Male
Aquarius
74
48
Monk
Jump
Auto Potion
Magic Defense UP
Move+2



Black Hood
Leather Outfit
Defense Armlet

Pummel, Secret Fist, Revive, Seal Evil
Level Jump8, Vertical Jump8



Gorgewall
Monster
Taurus
44
64
Archaic Demon










JonnyCue
Male
Aries
52
70
Archer
Black Magic
Speed Save
Equip Armor
Retreat

Night Killer
Bronze Shield
Platinum Helmet
Mythril Armor
Feather Mantle

Charge+1, Charge+4, Charge+20
Bolt 3, Ice 4, Frog



Twelfthrootoftwo
Male
Leo
50
72
Knight
Punch Art
Earplug
Equip Armor
Swim

Rune Blade
Genji Shield
Feather Hat
Plate Mail
Elf Mantle

Head Break, Armor Break, Shield Break, Speed Break, Mind Break, Justice Sword, Explosion Sword
Wave Fist, Earth Slash, Purification, Chakra, Revive

Player: !Black
Team: Black Team
Palettes: Black/Red



HaateXIII
Male
Cancer
75
74
Archer
Sing
Brave Save
Defend
Swim

Poison Bow
Flame Shield
Diamond Helmet
Black Costume
Bracer

Charge+1, Charge+2
Cheer Song, Magic Song, Diamond Blade



MemoriesofFinal
Male
Taurus
64
67
Summoner
Time Magic
Critical Quick
Martial Arts
Swim

Madlemgen

Holy Miter
Linen Robe
Power Wrist

Shiva, Titan, Carbunkle, Silf
Haste, Slow, Slow 2, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time



Holandrix
Female
Gemini
79
43
Wizard
Throw
Parry
Defense UP
Move-MP Up

Rod

Holy Miter
Wizard Robe
Battle Boots

Fire, Fire 3, Bolt 4, Ice 3, Ice 4
Shuriken, Bomb, Wand, Dictionary



Seaweed B
Female
Libra
44
56
Mediator
Throw
PA Save
Dual Wield
Move+2

Glacier Gun
Glacier Gun
Ribbon
Silk Robe
Genji Gauntlet

Invitation, Persuade, Threaten, Preach, Solution, Refute
Bomb, Knife, Hammer, Spear

Player: !Red
Team: Red Team
Palettes: Red/Brown



Thyrandaal
Male
Aries
47
72
Mime

Counter Tackle
Equip Shield
Swim


Ice Shield
Red Hood
Brigandine
Genji Gauntlet

Mimic




Randgridr
Female
Capricorn
76
64
Mime

Counter Tackle
Dual Wield
Move+1



Headgear
Silk Robe
Defense Ring

Mimic




Phi Sig
Male
Aries
79
76
Knight
Basic Skill
Arrow Guard
Halve MP
Move+3

Blood Sword
Kaiser Plate
Circlet
Mythril Armor
Cursed Ring

Head Break, Shield Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Dark Sword
Dash, Throw Stone, Heal



Gorgewall
Female
Taurus
69
68
Summoner
Yin Yang Magic
Counter Magic
Sicken
Move+2

White Staff

Black Hood
Earth Clothes
108 Gems

Golem, Salamander
Poison, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy

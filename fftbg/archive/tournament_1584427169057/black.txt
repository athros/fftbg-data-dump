Player: !Black
Team: Black Team
Palettes: Black/Red



Tylershoelaces
Male
Taurus
80
70
Time Mage
Sing
Faith Up
Maintenance
Move-HP Up

Gold Staff

Green Beret
Chain Vest
Bracer

Slow, Slow 2, Float, Reflect, Demi 2, Stabilize Time
Magic Song, Last Song



Lyronmkii
Male
Aries
67
47
Monk
White Magic
Arrow Guard
Doublehand
Move+2



Cachusha
Adaman Vest
Angel Ring

Pummel, Wave Fist, Purification, Revive
Cure 4, Raise, Regen, Shell 2



Ranmilia
Female
Virgo
72
57
Lancer
Talk Skill
PA Save
Attack UP
Retreat

Partisan
Platinum Shield
Bronze Helmet
Crystal Mail
Elf Mantle

Level Jump4, Vertical Jump2
Praise, Preach, Solution



Maeveen
Male
Leo
40
43
Monk
Item
Damage Split
Dual Wield
Move+3



Ribbon
Wizard Outfit
Small Mantle

Pummel, Secret Fist, Purification, Chakra
Potion, X-Potion, Holy Water, Remedy, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Female
Gemini
49
64
Summoner
Steal
HP Restore
Short Charge
Waterbreathing

Healing Staff

Black Hood
Linen Robe
Magic Ring

Moogle, Shiva, Golem, Odin, Salamander, Lich, Cyclops
Gil Taking, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



Firesheath
Male
Taurus
74
80
Samurai
Time Magic
Counter Flood
Equip Bow
Move+2

Ultimus Bow

Platinum Helmet
Linen Cuirass
Diamond Armlet

Heaven's Cloud
Haste 2, Slow, Float, Reflect, Demi



AstroSwank
Female
Sagittarius
78
43
Priest
Dance
Counter
Attack UP
Move-HP Up

Rainbow Staff

Twist Headband
Chameleon Robe
Genji Gauntlet

Cure, Cure 2, Raise, Shell, Esuna, Holy
Last Dance



Zetchryn
Female
Pisces
61
46
Summoner
Basic Skill
Brave Up
Magic Attack UP
Retreat

Ice Rod

Headgear
Power Sleeve
Red Shoes

Moogle, Carbunkle, Bahamut, Salamander, Silf, Lich
Throw Stone, Heal, Tickle, Scream

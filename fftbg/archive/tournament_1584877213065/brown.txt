Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Male
Scorpio
64
76
Mime

Caution
Doublehand
Ignore Height



Green Beret
Judo Outfit
Bracer

Mimic




M0nk3yNutz
Female
Libra
50
77
Calculator
Black Magic
Parry
Equip Armor
Teleport

Thunder Rod

Bronze Helmet
Black Costume
Spike Shoes

CT, Height, 3
Fire 3, Fire 4, Bolt, Bolt 2, Ice 3, Ice 4



HASTERIOUS
Female
Aquarius
56
70
Wizard
Dance
Arrow Guard
Short Charge
Jump+3

Thunder Rod

Feather Hat
Light Robe
108 Gems

Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Ice 4
Wiznaibus, Polka Polka, Disillusion, Void Storage



Treapvort
Female
Leo
47
80
Geomancer
Draw Out
Counter Magic
Attack UP
Retreat

Battle Axe
Gold Shield
Twist Headband
Power Sleeve
Leather Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Murasame, Kiyomori

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Gooseyourself
Male
Gemini
52
49
Lancer
Talk Skill
MA Save
Short Charge
Move+1

Dragon Whisker
Aegis Shield
Genji Helmet
Reflect Mail
Chantage

Level Jump5, Vertical Jump7
Invitation, Threaten, Rehabilitate



XYungAnarchyx
Female
Capricorn
46
70
Geomancer
Draw Out
Counter Tackle
Equip Axe
Ignore Height

White Staff
Buckler
Leather Hat
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Koutetsu, Bizen Boat, Kiyomori, Masamune



DudeMonkey77
Male
Aries
55
47
Archer
Summon Magic
Dragon Spirit
Magic Attack UP
Jump+3

Hunting Bow
Platinum Shield
Black Hood
Wizard Outfit
Battle Boots

Charge+4, Charge+5, Charge+20
Moogle, Ramuh, Carbunkle, Lich



Fenaen
Male
Capricorn
70
50
Lancer
Battle Skill
Damage Split
Magic Defense UP
Move-MP Up

Dragon Whisker
Platinum Shield
Platinum Helmet
Diamond Armor
Genji Gauntlet

Level Jump5, Vertical Jump4
Shield Break, Weapon Break, Night Sword, Surging Sword

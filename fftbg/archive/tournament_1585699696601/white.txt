Player: !White
Team: White Team
Palettes: White/Blue



Lord Burrah
Female
Cancer
72
53
Knight
Yin Yang Magic
Faith Up
Equip Bow
Jump+3

Ultimus Bow
Buckler
Leather Helmet
Wizard Robe
Magic Ring

Armor Break, Speed Break, Mind Break, Justice Sword, Night Sword
Blind, Poison, Spell Absorb, Zombie, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify



EnemyController
Female
Leo
59
48
Oracle
Charge
Catch
Halve MP
Levitate

Battle Folio

Thief Hat
Power Sleeve
Leather Mantle

Poison, Life Drain, Pray Faith, Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Charge+4, Charge+5, Charge+7



Tithonus
Female
Sagittarius
54
67
Mediator
Item
Damage Split
Dual Wield
Teleport

Bestiary
Battle Folio
Green Beret
Leather Outfit
Power Wrist

Persuade, Mimic Daravon, Refute, Rehabilitate
Potion, Ether, Maiden's Kiss, Phoenix Down



Baconbacon1207
Monster
Leo
43
55
Serpentarius








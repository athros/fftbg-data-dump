Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



LadyKoto0
Female
Leo
50
51
Monk
Item
Distribute
Throw Item
Move+2



Barette
Chain Vest
Leather Mantle

Secret Fist, Purification, Seal Evil
Hi-Potion, Ether, Eye Drop, Echo Grass, Phoenix Down



I Nod My Head When I Lose
Male
Libra
58
63
Monk
Yin Yang Magic
HP Restore
Dual Wield
Move-MP Up



Black Hood
Wizard Outfit
Small Mantle

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Poison, Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep, Petrify



Kay0ss
Male
Capricorn
50
60
Archer
Talk Skill
Absorb Used MP
Secret Hunt
Move+3

Long Bow

Red Hood
Chain Vest
Angel Ring

Charge+1, Charge+3, Charge+10
Persuade, Praise, Solution, Negotiate, Refute



Pah J
Male
Virgo
75
76
Geomancer
Basic Skill
Blade Grasp
Sicken
Waterwalking

Slasher
Crystal Shield
Red Hood
Chameleon Robe
Spike Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Accumulate, Heal, Tickle, Cheer Up

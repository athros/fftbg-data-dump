Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Clippopo
Male
Sagittarius
46
54
Samurai
Battle Skill
Hamedo
Equip Sword
Ignore Terrain

Platinum Sword

Cross Helmet
Crystal Mail
Setiemson

Asura, Murasame, Kiyomori, Kikuichimoji
Shield Break, Weapon Break, Power Break, Mind Break, Dark Sword



Sinnyil2
Female
Taurus
64
80
Chemist
Draw Out
Mana Shield
Equip Axe
Levitate

Morning Star

Flash Hat
Judo Outfit
Cursed Ring

Potion, X-Potion, Echo Grass, Soft, Phoenix Down
Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji



SomthingMore
Female
Aquarius
61
72
Monk
Draw Out
Counter Magic
Sicken
Lava Walking



Headgear
Wizard Outfit
Salty Rage

Pummel, Purification, Revive
Bizen Boat, Murasame, Muramasa



King Slime
Male
Scorpio
67
58
Mime

Counter
Martial Arts
Lava Walking



Golden Hairpin
Earth Clothes
Power Wrist

Mimic


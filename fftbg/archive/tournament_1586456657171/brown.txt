Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Spartan Paladin
Male
Aries
68
79
Knight
Elemental
MA Save
Defend
Ignore Terrain

Battle Axe
Aegis Shield
Gold Helmet
Light Robe
Setiemson

Armor Break, Magic Break, Mind Break, Justice Sword, Night Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Maakur
Female
Libra
76
65
Thief
Dance
Critical Quick
Equip Knife
Move-HP Up

Dagger

Green Beret
Wizard Outfit
Dracula Mantle

Gil Taking, Steal Heart, Steal Armor
Witch Hunt, Dragon Pit



Sinnyil2
Male
Sagittarius
78
51
Knight
Talk Skill
Counter
Monster Talk
Waterbreathing

Save the Queen
Buckler
Circlet
Leather Armor
Power Wrist

Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Explosion Sword
Invitation, Persuade, Death Sentence, Insult



MantisFinch
Female
Sagittarius
77
70
Ninja
Talk Skill
Counter
Equip Sword
Fly

Morning Star
Defender
Twist Headband
Brigandine
Wizard Mantle

Bomb
Praise, Negotiate, Rehabilitate

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Faplo
Male
Gemini
75
75
Geomancer
Battle Skill
Regenerator
Magic Attack UP
Move+2

Battle Axe
Bronze Shield
Flash Hat
White Robe
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Armor Break, Shield Break, Magic Break, Mind Break



Hyvi
Male
Virgo
45
65
Ninja
Steal
Counter
Long Status
Jump+3

Morning Star
Flail
Flash Hat
Secret Clothes
Reflect Ring

Shuriken, Knife, Hammer
Gil Taking, Steal Helmet, Steal Armor



LDSkinny
Male
Scorpio
74
44
Knight
Sing
Caution
Equip Armor
Levitate

Broad Sword
Platinum Shield
Red Hood
Power Sleeve
Setiemson

Head Break, Armor Break, Shield Break, Speed Break, Power Break
Cheer Song, Last Song, Hydra Pit



Skarthe
Male
Scorpio
78
69
Thief
Throw
Arrow Guard
Beastmaster
Move+1

Mage Masher

Twist Headband
Earth Clothes
Feather Mantle

Arm Aim
Bomb, Sword, Hammer

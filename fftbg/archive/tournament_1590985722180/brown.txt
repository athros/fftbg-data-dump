Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BreederLancet
Female
Gemini
75
54
Knight
Item
Faith Save
Short Status
Jump+3

Ancient Sword
Flame Shield
Gold Helmet
Silk Robe
Red Shoes

Head Break, Shield Break, Speed Break, Power Break, Surging Sword
Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



Powerpinch
Female
Pisces
55
70
Archer
White Magic
MP Restore
Equip Axe
Move-HP Up

Morning Star
Genji Shield
Barette
Leather Outfit
Magic Gauntlet

Charge+1, Charge+3, Charge+5
Raise, Protect 2, Shell 2



TinchoT
Female
Scorpio
51
67
Summoner
White Magic
Regenerator
Short Charge
Jump+2

Wizard Rod

Holy Miter
Leather Outfit
N-Kai Armlet

Moogle, Shiva, Golem, Carbunkle, Salamander, Cyclops
Cure 2, Raise, Regen, Protect, Esuna, Holy



Candina
Male
Gemini
60
73
Ninja
Jump
Damage Split
Equip Knife
Waterwalking

Assassin Dagger
Dragon Rod
Headgear
Chain Vest
Bracer

Shuriken, Bomb, Staff
Level Jump4, Vertical Jump2

Player: !Red
Team: Red Team
Palettes: Red/Brown



Kellios11
Monster
Cancer
78
80
Ultima Demon










0utlier
Female
Capricorn
47
59
Wizard
Item
Absorb Used MP
Throw Item
Waterwalking

Assassin Dagger

Flash Hat
Mystic Vest
Defense Armlet

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Frog, Flare
Hi-Ether, Eye Drop, Holy Water, Phoenix Down



Conome
Male
Cancer
58
70
Samurai
Item
Catch
Throw Item
Retreat

Bizen Boat

Bronze Helmet
Crystal Mail
Spike Shoes

Koutetsu, Kiyomori, Kikuichimoji
Potion, Ether, Eye Drop, Phoenix Down



TycerNova
Male
Taurus
76
67
Oracle
Charge
HP Restore
Doublehand
Waterbreathing

Gokuu Rod

Thief Hat
Black Costume
Genji Gauntlet

Poison, Spell Absorb, Dispel Magic, Paralyze, Petrify, Dark Holy
Charge+1, Charge+2, Charge+4, Charge+5

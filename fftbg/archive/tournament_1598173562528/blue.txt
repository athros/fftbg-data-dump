Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Female
Libra
62
75
Monk
Elemental
Counter
Equip Bow
Fly

Long Bow

Flash Hat
Power Sleeve
Power Wrist

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball



HASTERIOUS
Monster
Leo
56
55
Dark Behemoth










LordTomS
Male
Leo
42
67
Geomancer
Jump
Counter Flood
Equip Knife
Ignore Height

Rod
Ice Shield
Thief Hat
Black Robe
Rubber Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball
Level Jump8, Vertical Jump7



DeathTaxesAndAnime
Female
Pisces
45
75
Mediator
Throw
MP Restore
Dual Wield
Lava Walking

Mythril Gun
Glacier Gun
Red Hood
White Robe
Wizard Mantle

Preach, Death Sentence, Mimic Daravon
Shuriken, Bomb, Knife

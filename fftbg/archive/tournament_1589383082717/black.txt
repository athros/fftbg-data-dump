Player: !Black
Team: Black Team
Palettes: Black/Red



Leakimiko
Male
Taurus
47
80
Thief
Black Magic
Damage Split
Equip Shield
Move+2

Broad Sword
Crystal Shield
Thief Hat
Rubber Costume
Defense Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Status
Fire 2, Bolt, Bolt 4, Ice 3, Death, Flare



Wizblizz
Female
Libra
45
65
Knight
Elemental
Speed Save
Magic Defense UP
Teleport

Iron Sword
Gold Shield
Gold Helmet
Linen Cuirass
Spike Shoes

Shield Break, Weapon Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



ALY327
Male
Taurus
80
56
Ninja
Steal
HP Restore
Short Status
Jump+2

Orichalcum
Morning Star
Leather Hat
Mystic Vest
Germinas Boots

Shuriken, Bomb, Knife, Wand
Steal Shield, Steal Accessory, Arm Aim



JumbocactuarX27
Female
Cancer
51
57
Thief
Elemental
Faith Up
Beastmaster
Fly

Koga Knife

Holy Miter
Power Sleeve
Cursed Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

Player: !White
Team: White Team
Palettes: White/Blue



Lawnboxer
Female
Gemini
69
71
Priest
Elemental
MP Restore
Short Charge
Move+1

Flail

Flash Hat
Light Robe
N-Kai Armlet

Raise, Shell, Shell 2, Esuna, Holy
Hallowed Ground, Static Shock, Blizzard, Gusty Wind



Mesmaster
Female
Aries
74
62
Priest
Elemental
PA Save
Long Status
Levitate

Scorpion Tail

Flash Hat
Black Robe
Cursed Ring

Cure, Cure 2, Cure 3, Cure 4, Raise, Protect 2, Shell, Wall
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Mpghappiness
Female
Virgo
57
64
Chemist
Dance
Damage Split
Attack UP
Waterbreathing

Star Bag

Feather Hat
Wizard Outfit
Feather Boots

Potion, Hi-Ether, Echo Grass, Soft, Holy Water
Wiznaibus, Slow Dance, Disillusion, Nameless Dance



Cogniv0r3
Female
Gemini
53
64
Thief
Punch Art
Brave Save
Secret Hunt
Move-MP Up

Air Knife

Twist Headband
Earth Clothes
Defense Ring

Gil Taking, Steal Helmet, Steal Armor, Arm Aim
Spin Fist, Earth Slash, Purification, Chakra, Revive

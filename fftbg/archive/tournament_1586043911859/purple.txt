Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Bryan792
Male
Taurus
57
49
Archer
Basic Skill
Counter Magic
Doublehand
Levitate

Glacier Gun

Flash Hat
Wizard Outfit
Wizard Mantle

Charge+2, Charge+5, Charge+7
Throw Stone, Tickle, Yell



B0shii
Male
Aquarius
58
53
Calculator
Beast Skill
Catch
Maintenance
Move-HP Up

Bestiary

Barbuta
Plate Mail
Cherche

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



Aneyus
Female
Libra
79
73
Monk
Yin Yang Magic
Earplug
Short Status
Ignore Terrain



Leather Hat
Leather Outfit
Diamond Armlet

Pummel, Earth Slash, Secret Fist, Purification
Blind, Spell Absorb, Life Drain, Silence Song, Foxbird, Paralyze



TheKillerNacho
Male
Gemini
48
76
Bard
Throw
Sunken State
Equip Bow
Fly

Bow Gun

Triangle Hat
Judo Outfit
Magic Ring

Nameless Song
Shuriken

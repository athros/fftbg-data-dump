Final Bets: white - 8 bets for 1,784G (48.9%, x1.04); black - 4 bets for 1,863G (51.1%, x0.96)

white bets:
gorgewall: 501G (28.1%, 21,691G)
BirbBrainsBot: 365G (20.5%, 9,961G)
DAC169: 336G (18.8%, 336G)
alithesillybird: 200G (11.2%, 3,687G)
Powermhero: 102G (5.7%, 23,932G)
Koorndog: 100G (5.6%, 100G)
datadrivenbot: 100G (5.6%, 38,962G)
getthemoneyz: 80G (4.5%, 950,774G)

black bets:
prince_rogers_nelson_: 1,063G (57.1%, 1,063G)
reinoe: 500G (26.8%, 15,658G)
twelfthrootoftwo: 200G (10.7%, 3,080G)
AllInBot: 100G (5.4%, 100G)

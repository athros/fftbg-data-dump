Player: !White
Team: White Team
Palettes: White/Blue



Jigglefluffenstuff
Monster
Sagittarius
44
73
Dryad










HaateXIII
Female
Sagittarius
73
48
Priest
Draw Out
Critical Quick
Equip Bow
Levitate

Hunting Bow

Green Beret
Silk Robe
N-Kai Armlet

Cure, Cure 3, Raise, Raise 2, Regen, Protect, Shell, Shell 2, Esuna
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Jordache7K
Female
Cancer
62
77
Lancer
Item
Counter Flood
Concentrate
Waterwalking

Spear
Gold Shield
Crystal Helmet
Wizard Robe
N-Kai Armlet

Level Jump8, Vertical Jump6
Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Tevoul
Male
Aries
72
59
Knight
Elemental
Critical Quick
Magic Attack UP
Levitate

Ragnarok

Iron Helmet
Wizard Robe
Elf Mantle

Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball

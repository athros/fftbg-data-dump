Final Bets: yellow - 10 bets for 5,667G (39.2%, x1.55); white - 9 bets for 8,803G (60.8%, x0.64)

yellow bets:
Digitalsocrates: 2,000G (35.3%, 61,361G)
Lydian_C: 1,200G (21.2%, 25,038G)
BStarTV: 1,000G (17.6%, 9,306G)
opHendoslice: 500G (8.8%, 1,959G)
gorgewall: 301G (5.3%, 15,468G)
Lythe_Caraker: 250G (4.4%, 109,687G)
AltimaMantoid: 112G (2.0%, 812G)
ThisGuyLovesMath: 104G (1.8%, 104G)
slickdang: 100G (1.8%, 516G)
Avin_Chaos: 100G (1.8%, 17,843G)

white bets:
Realitydown: 4,187G (47.6%, 4,187G)
BlackFireUK: 1,000G (11.4%, 32,207G)
BirbBrainsBot: 1,000G (11.4%, 92,365G)
NicoSavoy: 1,000G (11.4%, 13,424G)
prince_rogers_nelson_: 580G (6.6%, 580G)
CassiePhoenix: 580G (6.6%, 580G)
TeaTime29: 300G (3.4%, 13,403G)
Firesheath: 100G (1.1%, 11,819G)
getthemoneyz: 56G (0.6%, 985,293G)

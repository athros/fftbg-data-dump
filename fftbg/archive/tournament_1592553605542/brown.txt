Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ko2q
Male
Aquarius
70
49
Knight
Time Magic
Dragon Spirit
Magic Defense UP
Waterwalking

Save the Queen
Mythril Shield
Gold Helmet
Linen Robe
Magic Gauntlet

Armor Break, Shield Break, Magic Break, Speed Break, Stasis Sword, Surging Sword
Haste, Stop, Quick, Stabilize Time



Firesheath
Female
Cancer
58
56
Knight
Throw
Caution
Magic Defense UP
Waterwalking

Battle Axe
Flame Shield
Platinum Helmet
Maximillian
Diamond Armlet

Weapon Break, Speed Break, Justice Sword, Night Sword
Bomb



Victoriolue
Male
Virgo
75
44
Lancer
Yin Yang Magic
Arrow Guard
Doublehand
Teleport

Octagon Rod

Genji Helmet
Silk Robe
Small Mantle

Level Jump8, Vertical Jump4
Spell Absorb, Pray Faith, Blind Rage



Sairentozon7
Female
Virgo
63
58
Thief
Black Magic
Brave Save
Equip Gun
Jump+3

Blaze Gun

Leather Hat
Adaman Vest
108 Gems

Steal Heart, Steal Helmet, Steal Accessory
Bolt 2, Death

Player: !Red
Team: Red Team
Palettes: Red/Brown



ColetteMSLP
Female
Libra
44
80
Mediator
Throw
Caution
Concentrate
Waterbreathing

Romanda Gun

Flash Hat
Mystic Vest
Germinas Boots

Invitation, Praise, Preach, Solution, Mimic Daravon, Rehabilitate
Bomb, Staff



Fluffskull
Male
Scorpio
77
67
Oracle
White Magic
Sunken State
Doublehand
Waterwalking

Gokuu Rod

Feather Hat
Black Robe
Battle Boots

Poison, Doubt Faith, Zombie, Silence Song, Dispel Magic, Sleep
Cure 2, Cure 4, Raise, Reraise, Protect, Wall, Esuna



Evewho
Female
Sagittarius
80
44
Chemist
Battle Skill
Speed Save
Attack UP
Move+3

Panther Bag

Feather Hat
Earth Clothes
Defense Armlet

Hi-Potion, Antidote, Soft, Phoenix Down
Armor Break, Shield Break, Justice Sword



Digitalsocrates
Male
Libra
77
41
Ninja
Basic Skill
Faith Save
Equip Sword
Fly

Dagger
Bizen Boat
Flash Hat
Clothes
Elf Mantle

Shuriken, Bomb, Wand, Dictionary
Dash

Player: !White
Team: White Team
Palettes: White/Blue



ALY327
Female
Leo
46
66
Ninja
Black Magic
Parry
Equip Axe
Move+3

Morning Star
Sage Staff
Holy Miter
Earth Clothes
Defense Armlet

Shuriken, Bomb
Fire 3, Ice



HaateXIII
Male
Cancer
63
58
Lancer
Battle Skill
Blade Grasp
Equip Sword
Move-HP Up

Ice Brand
Genji Shield
Crystal Helmet
Black Robe
Defense Ring

Level Jump8, Vertical Jump2
Head Break, Power Break, Dark Sword



AniZero
Male
Cancer
69
53
Ninja
Draw Out
Critical Quick
Equip Polearm
Move-MP Up

Cypress Rod
Spear
Headgear
Leather Outfit
Power Wrist

Shuriken, Knife
Asura, Murasame, Heaven's Cloud, Kikuichimoji



Edgehead62888
Female
Gemini
69
73
Oracle
Steal
Regenerator
Short Status
Jump+3

Octagon Rod

Green Beret
Chameleon Robe
Red Shoes

Blind, Pray Faith, Blind Rage, Dispel Magic
Steal Heart, Steal Armor

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheChainNerd
Female
Gemini
68
75
Oracle
Charge
Auto Potion
Doublehand
Ignore Height

Ivory Rod

Leather Hat
Wizard Outfit
Bracer

Blind, Spell Absorb, Pray Faith, Dispel Magic, Petrify
Charge+3



Mudrockk
Male
Capricorn
66
44
Squire
Charge
Counter
Equip Gun
Jump+1

Papyrus Codex
Flame Shield
Feather Hat
Wizard Outfit
Dracula Mantle

Heal, Fury, Wish, Scream
Charge+1, Charge+2



Miku Shikhu
Female
Libra
70
40
Chemist
Steal
Counter
Equip Shield
Move+2

Main Gauche
Gold Shield
Holy Miter
Wizard Outfit
Sprint Shoes

Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Holy Water, Phoenix Down
Steal Heart, Steal Shield



DudeMonkey77
Female
Sagittarius
54
78
Lancer
White Magic
PA Save
Long Status
Swim

Partisan
Genji Shield
Circlet
Reflect Mail
Feather Boots

Level Jump5, Vertical Jump6
Cure, Cure 3, Raise, Raise 2, Reraise, Wall, Esuna

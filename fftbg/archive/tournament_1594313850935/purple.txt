Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ArashiKurobara
Monster
Sagittarius
60
41
Hydra










Kellios11
Male
Cancer
70
57
Archer
Punch Art
Counter
Long Status
Move-HP Up

Snipe Bow
Hero Shield
Gold Helmet
Earth Clothes
Bracer

Charge+10, Charge+20
Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil



Dantayystv
Male
Gemini
71
73
Archer
Sing
Arrow Guard
Secret Hunt
Fly

Night Killer
Gold Shield
Gold Helmet
Power Sleeve
Magic Gauntlet

Charge+1, Charge+3, Charge+7, Charge+10
Magic Song



SQUiDSQUARKLIN
Male
Aquarius
56
36
Geomancer
Draw Out
Catch
Secret Hunt
Jump+1

Bizen Boat
Genji Shield
Headgear
Mystic Vest
Feather Boots

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Asura, Heaven's Cloud

Player: !White
Team: White Team
Palettes: White/Blue



Ominnous
Male
Capricorn
57
81
Ninja
White Magic
Regenerator
Equip Sword
Waterwalking

Kunai
Sasuke Knife
Green Beret
Judo Outfit
Bracer

Bomb, Knife, Hammer, Spear, Wand
Cure 2, Cure 4, Raise, Protect, Protect 2, Shell 2



Jigglefluffenstuff
Male
Aquarius
52
68
Archer
Sing
Absorb Used MP
Beastmaster
Move+1

Bow Gun
Bronze Shield
Flash Hat
Wizard Outfit
Feather Boots

Charge+1, Charge+2, Charge+7, Charge+10
Magic Song, Nameless Song, Hydra Pit



Sypheck
Male
Gemini
72
64
Knight
Steal
Distribute
Doublehand
Swim

Defender

Iron Helmet
Leather Armor
Genji Gauntlet

Head Break, Armor Break, Magic Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Steal Shield, Steal Weapon, Steal Accessory, Arm Aim



Blacklistclancy
Male
Gemini
43
79
Knight
Elemental
HP Restore
Short Status
Levitate

Battle Axe
Round Shield
Cross Helmet
Gold Armor
Cursed Ring

Head Break, Armor Break, Speed Break, Night Sword
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind

Player: !Black
Team: Black Team
Palettes: Black/Red



BandsWithLegends
Male
Sagittarius
57
57
Calculator
Demon Skill
MA Save
Magic Defense UP
Move-HP Up

Iron Fan

Cross Helmet
Leather Armor
Dracula Mantle

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



BenYuPoker
Male
Aries
75
40
Monk
White Magic
Caution
Dual Wield
Swim



Red Hood
Black Costume
Dracula Mantle

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Cure 2, Raise, Raise 2, Reraise, Protect 2, Shell



Krowbird
Female
Aquarius
51
47
Thief
Time Magic
Speed Save
Equip Sword
Waterbreathing

Cultist Dagger

Platinum Helmet
Mystic Vest
Cursed Ring

Gil Taking, Steal Helmet, Steal Weapon, Leg Aim
Haste 2, Stop, Stabilize Time



Cloudycube
Female
Virgo
45
53
Monk
Basic Skill
MP Restore
Maintenance
Lava Walking



Black Hood
Clothes
Defense Ring

Spin Fist, Purification, Chakra, Revive, Seal Evil
Dash, Heal

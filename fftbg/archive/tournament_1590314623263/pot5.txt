Final Bets: red - 4 bets for 1,914G (30.2%, x2.31); yellow - 12 bets for 4,422G (69.8%, x0.43)

red bets:
CorpusCav: 800G (41.8%, 14,971G)
Zachara: 482G (25.2%, 90,982G)
superdevon1: 432G (22.6%, 4,324G)
d4rr1n: 200G (10.4%, 2,750G)

yellow bets:
Mesmaster: 2,000G (45.2%, 61,287G)
cloud92684: 500G (11.3%, 2,673G)
rocl: 461G (10.4%, 4,618G)
GnielKnows: 331G (7.5%, 331G)
otakutaylor: 300G (6.8%, 5,666G)
josephiroth_143: 228G (5.2%, 228G)
serperemagus: 200G (4.5%, 984G)
AllInBot: 100G (2.3%, 100G)
Mr_Greed029: 100G (2.3%, 770G)
datadrivenbot: 100G (2.3%, 21,970G)
getthemoneyz: 70G (1.6%, 708,612G)
BirbBrainsBot: 32G (0.7%, 143,557G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Richardserious
Female
Aries
49
45
Thief
Jump
Counter Magic
Long Status
Teleport

Main Gauche

Black Hood
Judo Outfit
Genji Gauntlet

Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Leg Aim
Level Jump8, Vertical Jump7



Lali Lulelo
Female
Aquarius
78
75
Ninja
Steal
Speed Save
Doublehand
Fly

Kunai

Ribbon
Chain Vest
Defense Ring

Hammer, Stick
Gil Taking, Steal Heart, Steal Armor, Steal Status



KasugaiRoastedPeas
Female
Cancer
44
79
Summoner
Charge
MP Restore
Magic Attack UP
Swim

Poison Rod

Leather Hat
Mythril Vest
Salty Rage

Moogle, Ramuh, Ifrit, Titan, Leviathan, Fairy
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10, Charge+20



LDSkinny
Female
Scorpio
56
62
Oracle
Black Magic
Counter
Short Status
Jump+1

Papyrus Codex

Red Hood
Adaman Vest
108 Gems

Blind, Life Drain, Pray Faith, Zombie, Silence Song, Dispel Magic, Petrify
Fire 4, Bolt 3

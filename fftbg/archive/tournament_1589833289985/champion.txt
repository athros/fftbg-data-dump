Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ApplesauceBoss
Female
Leo
73
51
Time Mage
Punch Art
Absorb Used MP
Equip Shield
Jump+1

Wizard Staff
Escutcheon
Feather Hat
Linen Robe
Defense Ring

Slow, Slow 2, Immobilize, Quick, Demi, Stabilize Time
Spin Fist, Earth Slash, Purification, Chakra, Revive



Tsuike
Female
Gemini
69
74
Oracle
Black Magic
Counter Magic
Short Charge
Move-HP Up

Ivory Rod

Green Beret
Power Sleeve
Defense Ring

Blind, Poison, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze
Fire 2, Bolt 3, Ice, Ice 2, Ice 3



Vorap
Male
Sagittarius
56
62
Mime

Meatbone Slash
Martial Arts
Jump+2



Golden Hairpin
Black Costume
Germinas Boots

Mimic




UmaiJam
Female
Leo
74
41
Summoner
Elemental
Counter Magic
Short Charge
Fly

Mace of Zeus

Leather Hat
Judo Outfit
Feather Mantle

Shiva, Ifrit, Golem, Odin
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

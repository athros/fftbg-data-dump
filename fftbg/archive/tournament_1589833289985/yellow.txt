Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Tougou
Male
Aquarius
81
58
Ninja
Item
Dragon Spirit
Halve MP
Teleport 2

Hidden Knife
Short Edge
Flash Hat
Adaman Vest
Defense Armlet

Bomb, Wand
Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down



Dogsandcatsand
Female
Sagittarius
80
70
Calculator
Black Magic
Abandon
Attack UP
Move-MP Up

Bestiary

Triangle Hat
Clothes
Dracula Mantle

CT, Prime Number, 4, 3
Fire 2, Bolt 4, Ice, Ice 2, Frog, Death



Evewho
Female
Taurus
64
68
Knight
Steal
Damage Split
Magic Attack UP
Move+3

Defender
Escutcheon
Genji Helmet
Chain Mail
Jade Armlet

Head Break, Weapon Break, Speed Break, Mind Break
Steal Helmet, Steal Weapon, Steal Status



Kohlingen
Female
Leo
42
62
Thief
Charge
Counter Flood
Short Charge
Teleport 2

Air Knife

Triangle Hat
Mythril Vest
Sprint Shoes

Steal Armor, Steal Weapon, Arm Aim
Charge+2, Charge+4, Charge+5

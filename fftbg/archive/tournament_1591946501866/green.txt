Player: !Green
Team: Green Team
Palettes: Green/White



Electric Algus
Female
Serpentarius
41
59
Dancer
Steal
Counter
Equip Gun
Move-MP Up

Bestiary

Flash Hat
White Robe
N-Kai Armlet

Witch Hunt, Polka Polka, Disillusion, Nameless Dance
Steal Armor



Vorap
Female
Taurus
74
77
Monk
Dance
Damage Split
Equip Polearm
Levitate

Javelin

Headgear
Earth Clothes
Bracer

Pummel, Purification, Chakra, Revive
Wiznaibus, Slow Dance, Disillusion, Obsidian Blade



SSwing
Female
Aquarius
47
75
Wizard
Charge
Dragon Spirit
Short Charge
Move-MP Up

Thunder Rod

Golden Hairpin
Black Robe
N-Kai Armlet

Fire 2, Bolt, Ice, Frog
Charge+5, Charge+7



Yiroep2
Male
Aries
49
64
Time Mage
White Magic
Parry
Equip Bow
Ignore Height

Poison Bow

Headgear
Adaman Vest
Feather Boots

Haste, Slow 2, Stop, Immobilize
Cure, Cure 2, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Esuna

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheJonTerp
Male
Virgo
48
39
Oracle
Item
MP Restore
Equip Bow
Waterbreathing

Ultimus Bow

Feather Hat
Linen Robe
Elf Mantle

Life Drain, Pray Faith, Doubt Faith, Silence Song, Dark Holy
Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



Twelfthrootoftwo
Male
Cancer
67
66
Wizard
Sing
Regenerator
Equip Axe
Waterwalking

Gold Staff

Flash Hat
Silk Robe
108 Gems

Fire, Bolt, Bolt 3, Ice, Ice 4, Death
Life Song, Last Song



Cupholderr
Female
Virgo
54
58
Dancer
Jump
Counter
Short Charge
Ignore Height

Hydra Bag

Green Beret
Leather Outfit
Bracer

Witch Hunt, Polka Polka, Void Storage, Dragon Pit
Level Jump2, Vertical Jump6



Run With Stone GUNs
Male
Aries
65
71
Calculator
Bio
HP Restore
Secret Hunt
Lava Walking

Papyrus Codex
Bronze Shield
Genji Helmet
Chameleon Robe
Dracula Mantle

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis

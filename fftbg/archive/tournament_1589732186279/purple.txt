Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lawnboxer
Female
Gemini
57
80
Lancer
Punch Art
Brave Save
Doublehand
Teleport

Holy Lance

Crystal Helmet
Bronze Armor
108 Gems

Level Jump4, Vertical Jump3
Spin Fist, Secret Fist, Purification, Chakra, Seal Evil



HaplessOne
Male
Aquarius
74
37
Knight
Throw
HP Restore
Concentrate
Jump+3

Ragnarok
Mythril Shield
Circlet
Genji Armor
Spike Shoes

Head Break, Justice Sword, Night Sword, Explosion Sword
Wand



Sinnyil2
Female
Cancer
58
61
Ninja
Talk Skill
HP Restore
Equip Gun
Waterbreathing

Papyrus Codex
Bestiary
Triangle Hat
Mystic Vest
Small Mantle

Shuriken, Wand
Invitation, Praise, Solution, Death Sentence, Mimic Daravon, Refute



Leakimiko
Female
Taurus
47
62
Ninja
Basic Skill
Brave Save
Equip Gun
Move+2

Papyrus Codex
Papyrus Codex
Flash Hat
Power Sleeve
Elf Mantle

Shuriken
Yell, Fury, Scream

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Silverhand7
Female
Scorpio
76
52
Wizard
Basic Skill
Faith Save
Equip Knife
Waterbreathing

Dragon Rod

Flash Hat
Wizard Robe
Defense Armlet

Fire 3, Bolt, Bolt 3, Bolt 4, Ice 3, Ice 4, Frog
Accumulate, Throw Stone, Wish



Zachara
Monster
Scorpio
64
69
Ultima Demon










NovaKnight21
Female
Leo
66
48
Time Mage
Item
Sunken State
Dual Wield
Waterbreathing

Gokuu Rod
Cypress Rod
Cachusha
Chain Vest
Angel Ring

Haste, Haste 2, Slow, Stop, Immobilize, Float, Quick, Demi 2, Meteor
Potion, Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



Nifboy
Female
Scorpio
66
76
Time Mage
Steal
Counter Magic
Halve MP
Ignore Terrain

Healing Staff

Green Beret
White Robe
Bracer

Haste, Stop, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Steal Heart, Steal Shield, Steal Weapon, Steal Status, Leg Aim

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ko2q
Female
Taurus
48
53
Samurai
Item
Counter Magic
Equip Axe
Move-MP Up

Battle Axe

Gold Helmet
Carabini Mail
Red Shoes

Asura, Murasame, Muramasa
Hi-Potion, Hi-Ether, Soft, Remedy, Phoenix Down



Lokenwow
Male
Aries
45
65
Bard
Battle Skill
Parry
Equip Shield
Swim

Bloody Strings
Aegis Shield
Red Hood
Linen Cuirass
Cherche

Life Song, Magic Song, Diamond Blade
Shield Break, Weapon Break, Magic Break, Mind Break, Surging Sword



Powergems
Female
Aries
71
77
Samurai
White Magic
Dragon Spirit
Maintenance
Lava Walking

Dragon Whisker

Diamond Helmet
Reflect Mail
Setiemson

Asura, Murasame, Heaven's Cloud
Cure, Cure 4, Reraise, Esuna



NIghtdew14
Female
Leo
45
80
Dancer
Elemental
Counter Magic
Dual Wield
Jump+3

Ryozan Silk
Cashmere
Leather Hat
Brigandine
Diamond Armlet

Wiznaibus
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Smashy
Female
Capricorn
68
72
Wizard
Steal
Auto Potion
Equip Polearm
Waterwalking

Gokuu Rod

Flash Hat
Silk Robe
Bracer

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Empower
Gil Taking, Steal Accessory, Steal Status



NicoSavoy
Male
Pisces
78
74
Ninja
Jump
Critical Quick
Long Status
Waterbreathing

Flail
Hidden Knife
Twist Headband
Clothes
Angel Ring

Shuriken, Knife
Level Jump8, Vertical Jump6



Seaweed B
Male
Pisces
67
60
Knight
White Magic
Abandon
Attack UP
Move+3

Save the Queen
Round Shield
Crystal Helmet
Chain Mail
Small Mantle

Armor Break, Stasis Sword
Cure, Cure 3, Cure 4, Raise, Reraise, Protect 2, Wall, Holy



DeathRtopper
Female
Leo
54
68
Ninja
Time Magic
Caution
Defense UP
Ignore Height

Ninja Edge
Cultist Dagger
Ribbon
Clothes
Power Wrist

Bomb, Wand
Haste, Slow, Float, Quick, Demi 2, Stabilize Time, Meteor, Galaxy Stop

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galeronis
Female
Aquarius
51
66
Time Mage
Elemental
Counter
Beastmaster
Ignore Terrain

Oak Staff

Leather Hat
Earth Clothes
Spike Shoes

Slow, Stop, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball



StealthModeLocke
Male
Taurus
77
45
Squire
Battle Skill
Abandon
Equip Sword
Move-HP Up

Defender

Red Hood
Platinum Armor
Rubber Shoes

Dash, Heal, Cheer Up, Wish
Weapon Break, Power Break, Mind Break, Night Sword



JumbocactuarX27
Female
Sagittarius
75
59
Ninja
Dance
MA Save
Attack UP
Ignore Height

Spell Edge
Kunai
Triangle Hat
Mystic Vest
Wizard Mantle

Shuriken, Knife, Staff, Wand
Wiznaibus, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Void Storage, Dragon Pit



Letdowncity
Male
Aries
73
79
Mime

Speed Save
Attack UP
Ignore Terrain



Genji Helmet
Earth Clothes
Diamond Armlet

Mimic


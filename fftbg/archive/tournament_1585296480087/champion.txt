Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Basmal
Female
Virgo
74
77
Wizard
Draw Out
Critical Quick
Short Charge
Jump+3

Thunder Rod

Feather Hat
Chameleon Robe
Genji Gauntlet

Fire, Fire 3, Bolt 2, Bolt 4, Ice 3, Empower, Death, Flare
Koutetsu, Heaven's Cloud, Muramasa



Victoriolue
Male
Gemini
49
60
Thief
Throw
Mana Shield
Concentrate
Jump+3

Dagger

Flash Hat
Mystic Vest
Defense Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Status, Arm Aim
Hammer, Axe



SeedSC
Male
Taurus
63
57
Archer
Summon Magic
Dragon Spirit
Dual Wield
Fly

Bow Gun
Cross Bow
Flash Hat
Brigandine
Salty Rage

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Moogle, Odin, Silf, Fairy, Cyclops



Ominnous
Female
Scorpio
44
51
Summoner
Time Magic
Catch
Short Charge
Teleport

Healing Staff

Triangle Hat
Clothes
Leather Mantle

Golem, Carbunkle, Bahamut, Leviathan, Salamander
Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Demi

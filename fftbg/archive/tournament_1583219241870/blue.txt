Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Tanookium
Male
Aries
47
51
Archer
Time Magic
Critical Quick
Equip Bow
Move-MP Up

Lightning Bow

Flash Hat
Judo Outfit
Magic Ring

Charge+1, Charge+20
Haste, Immobilize, Reflect, Stabilize Time, Meteor



DarrenDinosaurs
Female
Virgo
37
66
Thief
Summon Magic
Brave Up
Magic Attack UP
Move+1

Kunai

Black Hood
Mythril Vest
Angel Ring

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Arm Aim
Moogle, Fairy



Prqt
Male
Taurus
74
73
Archer
Elemental
HP Restore
Short Status
Waterwalking

Gastrafitis
Genji Shield
Headgear
Chain Vest
108 Gems

Charge+3, Charge+4
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Chrysaors
Male
Taurus
54
57
Lancer
Sing
PA Save
Equip Gun
Move-HP Up

Bestiary
Ice Shield
Cross Helmet
Chain Mail
Wizard Mantle

Level Jump4, Vertical Jump7
Life Song, Battle Song, Magic Song, Nameless Song, Diamond Blade, Sky Demon

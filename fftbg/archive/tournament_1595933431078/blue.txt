Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ForagerCats
Male
Sagittarius
46
54
Archer
Battle Skill
Mana Shield
Equip Shield
Move+1

Hunting Bow
Diamond Shield
Twist Headband
Brigandine
Jade Armlet

Charge+1, Charge+2, Charge+3, Charge+5
Head Break, Armor Break, Weapon Break, Power Break



Josephiroth 143
Male
Scorpio
74
55
Mediator
Black Magic
Hamedo
Magic Attack UP
Levitate

Papyrus Codex

Flash Hat
Mystic Vest
Leather Mantle

Invitation, Praise, Threaten, Preach, Death Sentence, Insult
Fire, Fire 2, Fire 4, Bolt, Bolt 3, Empower



Prince Rogers Nelson
Female
Aquarius
75
45
Geomancer
Punch Art
Counter
Martial Arts
Retreat

Battle Axe
Buckler
Holy Miter
Brigandine
Leather Mantle

Pitfall, Water Ball, Hallowed Ground, Sand Storm, Gusty Wind, Lava Ball
Secret Fist, Purification, Revive



ALY327
Female
Cancer
61
59
Dancer
Talk Skill
Arrow Guard
Short Status
Move-MP Up

Panther Bag

Headgear
Wizard Robe
Dracula Mantle

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Obsidian Blade
Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Rehabilitate

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



DunkasaurusRekt
Female
Capricorn
46
62
Geomancer
Draw Out
Distribute
Doublehand
Fly

Broad Sword

Black Hood
Linen Robe
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Koutetsu, Murasame, Muramasa



Morgan Webbs Thong
Male
Cancer
49
44
Geomancer
Summon Magic
Parry
Halve MP
Move+2

Giant Axe
Ice Shield
Green Beret
Silk Robe
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Odin, Leviathan



Mtueni
Male
Taurus
58
75
Archer
Draw Out
Arrow Guard
Sicken
Waterwalking

Blast Gun
Genji Shield
Leather Hat
Mythril Vest
Dracula Mantle

Charge+2, Charge+7, Charge+10, Charge+20
Heaven's Cloud, Kiyomori



Phi Sig
Female
Libra
63
46
Priest
Jump
Blade Grasp
Equip Gun
Ignore Height

Blaze Gun

Feather Hat
Judo Outfit
Spike Shoes

Cure, Cure 3, Cure 4, Raise, Regen, Protect 2, Wall, Esuna
Level Jump2, Vertical Jump6

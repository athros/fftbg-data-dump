Player: !Black
Team: Black Team
Palettes: Black/Red



ShintaroNayaka
Female
Gemini
56
64
Time Mage
Yin Yang Magic
Counter Magic
Equip Sword
Swim

Rainbow Staff

Cachusha
Wizard Robe
Cursed Ring

Haste, Slow, Reflect, Stabilize Time, Meteor
Poison, Dispel Magic



Anethum
Female
Scorpio
70
77
Oracle
Throw
Blade Grasp
Doublehand
Ignore Height

Ivory Rod

Red Hood
Power Sleeve
Angel Ring

Blind, Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Petrify
Shuriken, Knife



WhiteTigress
Female
Taurus
65
61
Priest
Battle Skill
Mana Shield
Equip Gun
Waterbreathing

Wizard Staff

Cachusha
Black Costume
Feather Boots

Cure, Cure 3, Reraise, Wall, Esuna
Shield Break, Magic Break, Speed Break, Night Sword



Nizaha
Male
Sagittarius
58
53
Mediator
Punch Art
MP Restore
Dual Wield
Jump+2

Romanda Gun
Stone Gun
Red Hood
Power Sleeve
Diamond Armlet

Persuade, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate
Pummel, Wave Fist, Purification, Revive

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



IroncladWarbear
Male
Aquarius
59
53
Geomancer
Basic Skill
Distribute
Attack UP
Move+2

Ice Brand
Ice Shield
Twist Headband
Light Robe
Sprint Shoes

Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Accumulate, Heal, Yell, Cheer Up



Undyingshdw
Male
Taurus
43
70
Wizard
Time Magic
Auto Potion
Maintenance
Move-HP Up

Poison Rod

Red Hood
White Robe
Elf Mantle

Fire 4, Bolt, Bolt 2, Bolt 4, Empower, Frog
Slow, Immobilize, Demi, Stabilize Time



Yayifications
Female
Gemini
77
46
Mediator
Summon Magic
Distribute
Dual Wield
Move+1

Stone Gun
Romanda Gun
Headgear
Brigandine
Leather Mantle

Invitation, Threaten, Insult, Mimic Daravon, Refute
Moogle, Leviathan



LadyTeacup
Female
Aquarius
77
61
Wizard
Draw Out
Parry
Attack UP
Jump+3

Rod

Headgear
Black Robe
Rubber Shoes

Fire 3, Fire 4, Bolt, Bolt 3, Empower, Frog, Flare
Heaven's Cloud

Player: !Red
Team: Red Team
Palettes: Red/Brown



TravisBlack
Monster
Cancer
66
48
Gobbledeguck










Zen Burning
Female
Aries
40
71
Summoner
Dance
Meatbone Slash
Long Status
Jump+2

White Staff

Red Hood
Black Costume
Rubber Shoes

Moogle, Ramuh, Golem, Bahamut, Leviathan, Fairy
Witch Hunt, Void Storage, Dragon Pit



Nizaha
Male
Sagittarius
79
40
Thief
Time Magic
Counter
Maintenance
Retreat

Sasuke Knife

Ribbon
Brigandine
Red Shoes

Steal Helmet, Steal Armor, Steal Weapon
Haste, Immobilize, Demi 2, Meteor



DarrenDinosaurs
Female
Capricorn
80
49
Ninja
Battle Skill
PA Save
Equip Gun
Swim

Blast Gun
Mythril Gun
Holy Miter
Secret Clothes
Battle Boots

Shuriken, Bomb, Hammer, Stick
Justice Sword

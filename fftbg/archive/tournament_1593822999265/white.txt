Player: !White
Team: White Team
Palettes: White/Blue



Nifboy
Female
Aquarius
67
76
Archer
Talk Skill
Damage Split
Sicken
Retreat

Mythril Gun
Bronze Shield
Feather Hat
Mythril Vest
Reflect Ring

Charge+1, Charge+3, Charge+5, Charge+10, Charge+20
Persuade, Praise, Solution, Insult



Laserman1000
Male
Scorpio
49
55
Oracle
Throw
MP Restore
Short Charge
Move+1

Ivory Rod

Holy Miter
Black Robe
Diamond Armlet

Spell Absorb, Foxbird, Confusion Song, Dispel Magic
Shuriken, Spear



Redmage4evah
Male
Virgo
61
51
Knight
Item
Sunken State
Throw Item
Jump+1

Slasher
Venetian Shield
Cross Helmet
Linen Robe
Defense Ring

Armor Break, Magic Break, Night Sword
Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down



Yiroep2
Female
Capricorn
52
40
Squire
Draw Out
Hamedo
Beastmaster
Retreat

Diamond Sword
Bronze Shield
Red Hood
Genji Armor
Power Wrist

Accumulate, Dash, Throw Stone, Tickle, Cheer Up, Scream
Asura, Koutetsu

Player: !Green
Team: Green Team
Palettes: Green/White



Duckshut
Male
Scorpio
54
57
Time Mage
Basic Skill
Speed Save
Attack UP
Swim

Gold Staff

Black Hood
Light Robe
Elf Mantle

Haste, Slow, Slow 2, Float, Reflect, Demi 2, Stabilize Time
Accumulate, Heal, Wish



SpekkoFromLL
Female
Pisces
59
39
Dancer
Talk Skill
Damage Split
Long Status
Swim

Persia

Black Hood
Black Costume
Battle Boots

Disillusion
Praise, Solution, Insult, Refute



Riot Jayway
Male
Gemini
71
59
Calculator
White Magic
PA Save
Equip Polearm
Jump+2

Holy Lance

Green Beret
Wizard Outfit
Red Shoes

CT, Height, Prime Number, 4, 3
Cure, Raise, Protect 2, Shell



Shiznicle
Female
Taurus
63
78
Mime

MA Save
Equip Armor
Move+2



Cross Helmet
Chain Mail
Red Shoes

Mimic


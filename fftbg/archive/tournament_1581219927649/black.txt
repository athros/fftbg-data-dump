Player: !Black
Team: Black Team
Palettes: Black/Red



Aranthell
Monster
Capricorn
78
47
Ghoul










Aelmarkin
Male
Sagittarius
64
50
Monk
Charge
Critical Quick
Magic Defense UP
Waterwalking



Cachusha
Brigandine
Diamond Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification
Charge+1, Charge+4, Charge+7



Ruvelia BibeI
Female
Cancer
73
72
Thief
Yin Yang Magic
Absorb Used MP
Secret Hunt
Move+2

Main Gauche

Red Hood
Black Costume
Germinas Boots

Steal Weapon, Arm Aim
Life Drain, Doubt Faith, Blind Rage



Mountable
Male
Virgo
41
66
Mediator
Throw
Counter Magic
Martial Arts
Ignore Terrain



Green Beret
Secret Clothes
Wizard Mantle

Invitation, Persuade, Mimic Daravon
Bomb

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Tougou
Male
Scorpio
40
77
Archer
White Magic
Parry
Dual Wield
Jump+1

Glacier Gun
Blaze Gun
Leather Hat
Leather Vest
Power Wrist

Charge+1, Charge+3, Charge+4, Charge+10
Cure 3, Raise, Protect, Protect 2, Shell 2



Jethrothrul
Female
Scorpio
60
78
Knight
Summon Magic
Counter Magic
Dual Wield
Ignore Height

Defender
Coral Sword
Crystal Helmet
Robe of Lords
Feather Boots

Armor Break, Speed Break, Night Sword
Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Silf



Silverbreeze
Male
Gemini
52
44
Monk
Time Magic
Damage Split
Beastmaster
Move+2



Flash Hat
Judo Outfit
Elf Mantle

Pummel, Earth Slash, Secret Fist, Purification, Revive
Haste, Haste 2, Slow, Reflect, Demi 2, Stabilize Time



Mirapoix
Female
Libra
61
49
Geomancer
Battle Skill
Counter Tackle
Short Charge
Move-MP Up

Asura Knife
Platinum Shield
Thief Hat
Robe of Lords
Angel Ring

Water Ball, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Armor Break, Shield Break, Magic Break, Night Sword

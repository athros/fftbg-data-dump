Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zagorsek
Female
Aries
66
56
Time Mage
White Magic
Counter Magic
Equip Knife
Retreat

Mythril Knife

Leather Hat
Robe of Lords
Magic Ring

Slow 2, Float, Reflect, Quick, Stabilize Time
Cure 2, Raise, Raise 2, Protect



Brokenknight201
Female
Pisces
69
58
Mediator
Punch Art
Mana Shield
Dual Wield
Jump+1

Papyrus Codex
Bestiary
Green Beret
Mystic Vest
Magic Ring

Death Sentence, Insult, Refute
Earth Slash, Secret Fist, Purification, Chakra



Thyrandaal
Male
Pisces
50
39
Bard
Black Magic
Auto Potion
Sicken
Jump+3

Ramia Harp

Red Hood
Mythril Vest
Feather Boots

Cheer Song, Magic Song, Diamond Blade
Fire, Ice 2, Ice 3



Defaultlybrave
Female
Capricorn
69
69
Ninja
Elemental
Counter
Defend
Teleport

Flame Whip
Ninja Edge
Flash Hat
Brigandine
Magic Gauntlet

Bomb, Wand
Local Quake, Will-O-Wisp, Quicksand, Blizzard

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Grimmace45
Male
Sagittarius
66
76
Chemist
Jump
Parry
Equip Axe
Waterbreathing

Giant Axe

Green Beret
Judo Outfit
Feather Boots

Potion, Hi-Potion, X-Potion, Antidote, Soft, Phoenix Down
Level Jump2, Vertical Jump8



Tougou
Female
Capricorn
68
41
Geomancer
Punch Art
Dragon Spirit
Equip Gun
Swim

Bestiary
Venetian Shield
Triangle Hat
Black Robe
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Pummel, Secret Fist, Purification, Revive



Ultimainferno
Male
Aries
47
37
Samurai
Battle Skill
Counter
Beastmaster
Ignore Height

Muramasa

Bronze Helmet
Leather Armor
Wizard Mantle

Bizen Boat, Murasame, Kiyomori
Armor Break, Shield Break, Power Break, Justice Sword



Smugzug
Male
Cancer
79
45
Summoner
White Magic
Dragon Spirit
Equip Axe
Move+3

Mace of Zeus

Ribbon
Silk Robe
Leather Mantle

Moogle, Titan, Golem, Silf
Cure 2, Cure 4, Regen, Protect 2, Wall, Esuna

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



The Pengwin
Male
Scorpio
46
80
Archer
Yin Yang Magic
Counter
Concentrate
Jump+2

Ultimus Bow

Flash Hat
Mystic Vest
Feather Boots

Charge+1, Charge+3, Charge+5, Charge+10
Blind, Poison, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



KingofTricksters
Male
Capricorn
51
42
Squire
Talk Skill
Counter
Beastmaster
Retreat

Giant Axe
Genji Shield
Barbuta
Gold Armor
Magic Ring

Throw Stone, Cheer Up, Fury
Negotiate, Mimic Daravon, Refute



B0shii
Male
Aries
57
80
Oracle
Sing
Counter
Magic Defense UP
Jump+3

Musk Rod

Twist Headband
White Robe
Feather Boots

Doubt Faith, Silence Song, Confusion Song
Cheer Song, Magic Song



DudeMonkey77
Male
Gemini
70
69
Mediator
Summon Magic
Critical Quick
Doublehand
Move-MP Up

Blind Knife

Feather Hat
White Robe
Feather Boots

Threaten, Insult, Refute, Rehabilitate
Moogle, Ramuh, Titan, Leviathan, Cyclops

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Kai Shee
Female
Capricorn
58
74
Monk
Steal
MA Save
Equip Sword
Swim

Heaven's Cloud

Red Hood
Adaman Vest
Small Mantle

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Steal Heart, Steal Armor, Steal Status



Mirapoix
Male
Taurus
76
63
Mediator
Item
Dragon Spirit
Short Charge
Move-MP Up

Mythril Gun

Golden Hairpin
Secret Clothes
Bracer

Persuade, Threaten, Solution, Insult, Negotiate, Mimic Daravon, Rehabilitate
X-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down



Mayfu
Monster
Sagittarius
60
63
Goblin










Clar3d
Male
Virgo
75
47
Squire
Elemental
Counter
Doublehand
Waterbreathing

Air Knife

Headgear
Reflect Mail
108 Gems

Accumulate, Throw Stone, Heal, Yell
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

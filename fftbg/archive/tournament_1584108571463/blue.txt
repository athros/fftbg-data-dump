Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Jeeboheebo
Female
Pisces
56
77
Summoner
Throw
Parry
Short Charge
Move-HP Up

Wizard Rod

Flash Hat
Mythril Vest
Defense Ring

Moogle, Ramuh, Titan, Carbunkle, Fairy
Shuriken, Ninja Sword



Jigglefluffenstuff
Female
Capricorn
57
64
Summoner
Steal
Caution
Magic Defense UP
Move+2

Wizard Staff

Twist Headband
White Robe
Wizard Mantle

Moogle, Carbunkle, Leviathan
Steal Shield, Steal Status, Arm Aim, Leg Aim



SeedSC
Female
Pisces
80
81
Mediator
Yin Yang Magic
Parry
Long Status
Move+2

Blaze Gun

Barette
Silk Robe
Small Mantle

Persuade, Solution, Negotiate, Refute
Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify



TheKillerNacho
Male
Taurus
58
46
Bard
Basic Skill
Dragon Spirit
Short Charge
Swim

Fairy Harp

Leather Hat
Brigandine
Angel Ring

Life Song, Cheer Song, Magic Song, Nameless Song, Space Storage
Throw Stone, Heal, Cheer Up, Fury, Wish, Ultima

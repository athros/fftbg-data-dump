Player: !Black
Team: Black Team
Palettes: Black/Red



Happybobby1623
Male
Sagittarius
61
47
Bard
Steal
HP Restore
Equip Axe
Move+1

Rainbow Staff

Triangle Hat
Brigandine
Genji Gauntlet

Life Song, Battle Song, Magic Song, Space Storage, Sky Demon, Hydra Pit
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Steal Status



TheBlobZ
Female
Aries
73
62
Ninja
White Magic
HP Restore
Equip Gun
Waterwalking

Papyrus Codex
Papyrus Codex
Green Beret
Earth Clothes
Reflect Ring

Bomb
Cure 2, Cure 3, Raise, Protect, Esuna



KupoKel
Female
Scorpio
52
58
Thief
Summon Magic
Hamedo
Maintenance
Jump+2

Mage Masher

Black Hood
Mystic Vest
Small Mantle

Gil Taking, Steal Heart, Steal Accessory, Steal Status, Arm Aim
Moogle, Shiva, Carbunkle, Silf, Fairy



Maeveen
Male
Sagittarius
38
52
Monk
Draw Out
Counter
Equip Sword
Move+3

Ragnarok

Ribbon
Judo Outfit
Setiemson

Pummel, Wave Fist, Earth Slash, Purification, Revive
Koutetsu

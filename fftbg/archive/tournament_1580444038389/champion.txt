Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Nyandroid Ai
Female
Scorpio
80
74
Ninja
Punch Art
Catch
Equip Armor
Waterbreathing

Assassin Dagger
Sasuke Knife
Genji Helmet
Platinum Armor
Defense Ring

Shuriken, Bomb
Purification, Revive



Rurk
Male
Scorpio
57
64
Knight
White Magic
Critical Quick
Equip Sword
Fly

Blood Sword
Aegis Shield
Cross Helmet
Genji Armor
Rubber Shoes

Armor Break, Weapon Break, Speed Break, Mind Break, Dark Sword
Cure 3, Raise, Reraise, Regen, Protect, Shell, Esuna



DaveStrider55
Female
Cancer
63
70
Geomancer
Throw
Caution
Maintenance
Ignore Height

Slasher
Platinum Shield
Ribbon
Mythril Vest
Bracer

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball
Shuriken, Bomb, Knife



Play Puyopuyo
Male
Libra
49
51
Mime

Damage Split
Dual Wield
Fly



Flash Hat
Judo Outfit
Cursed Ring

Mimic


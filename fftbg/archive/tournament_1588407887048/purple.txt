Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ColetteMSLP
Monster
Aquarius
44
74
Holy Dragon










Daveb
Male
Taurus
78
50
Ninja
Item
Counter Flood
Equip Gun
Waterwalking

Bestiary
Papyrus Codex
Triangle Hat
Wizard Outfit
Wizard Mantle

Wand
Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Remedy, Phoenix Down



MalakoFox
Female
Gemini
58
58
Priest
Punch Art
Mana Shield
Doublehand
Levitate

Morning Star

Green Beret
Chameleon Robe
Bracer

Raise 2, Protect 2, Shell 2, Wall, Esuna
Pummel, Wave Fist



Shalthen
Male
Serpentarius
78
51
Mediator
Punch Art
Counter
Maintenance
Levitate

Mythril Knife

Black Hood
Wizard Robe
Spike Shoes

Solution, Death Sentence, Insult, Negotiate, Rehabilitate
Spin Fist, Pummel, Wave Fist, Purification, Revive

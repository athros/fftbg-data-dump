Player: !White
Team: White Team
Palettes: White/Blue



CrashCat
Male
Cancer
68
53
Geomancer
Charge
Critical Quick
Equip Gun
Teleport

Glacier Gun
Flame Shield
Golden Hairpin
Wizard Outfit
Defense Armlet

Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm
Charge+1, Charge+5



Realitydown
Female
Taurus
70
63
Summoner
Steal
Brave Up
Equip Shield
Jump+1

Faith Rod
Genji Shield
Headgear
White Robe
Small Mantle

Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Silf, Fairy, Lich
Gil Taking, Steal Helmet, Steal Shield



RubenFlonne
Male
Cancer
46
43
Monk
White Magic
Damage Split
Dual Wield
Move-HP Up



Golden Hairpin
Earth Clothes
Genji Gauntlet

Pummel, Earth Slash, Purification
Cure 3, Raise, Regen, Esuna



Red Celt
Female
Cancer
61
51
Dancer
Yin Yang Magic
HP Restore
Equip Shield
Ignore Terrain

Mage Masher
Buckler
Ribbon
Black Robe
Red Shoes

Witch Hunt, Slow Dance, Dragon Pit
Blind, Zombie, Silence Song, Blind Rage, Dispel Magic

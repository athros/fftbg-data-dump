Final Bets: white - 7 bets for 4,709G (54.0%, x0.85); brown - 5 bets for 4,011G (46.0%, x1.17)

white bets:
TheGuesty: 2,625G (55.7%, 67,625G)
DeathTaxesAndAnime: 672G (14.3%, 672G)
roqqqpsi: 654G (13.9%, 654G)
getthemoneyz: 308G (6.5%, 994,923G)
Evewho: 200G (4.2%, 13,641G)
jojo2k: 150G (3.2%, 572G)
rednecknazgul: 100G (2.1%, 2,037G)

brown bets:
VolgraTheMoose: 2,001G (49.9%, 24,879G)
BirbBrainsBot: 1,000G (24.9%, 137,908G)
CassiePhoenix: 610G (15.2%, 610G)
twelfthrootoftwo: 300G (7.5%, 1,781G)
E_Ballard: 100G (2.5%, 10,761G)

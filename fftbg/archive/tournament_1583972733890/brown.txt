Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Snkey
Male
Aquarius
53
43
Geomancer
Throw
Mana Shield
Equip Bow
Waterbreathing

Night Killer
Platinum Shield
Green Beret
Judo Outfit
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Shuriken, Bomb



Kl0kwurk
Female
Capricorn
64
46
Wizard
Time Magic
Dragon Spirit
Equip Polearm
Jump+3

Partisan

Thief Hat
Chameleon Robe
Sprint Shoes

Fire, Fire 2, Fire 4, Ice 3, Frog, Flare
Slow, Stop, Immobilize, Quick, Meteor



MyFakeLife
Male
Libra
81
52
Archer
White Magic
Parry
Doublehand
Move-HP Up

Bow Gun

Green Beret
Wizard Outfit
Bracer

Charge+1, Charge+3, Charge+5, Charge+10
Cure 2, Cure 3, Raise, Regen, Esuna, Holy



Black Sheep 0213
Male
Libra
46
70
Bard
Battle Skill
Critical Quick
Doublehand
Fly

Lightning Bow

Feather Hat
Bronze Armor
Elf Mantle

Angel Song, Cheer Song, Nameless Song, Last Song, Hydra Pit
Head Break, Weapon Break, Speed Break, Night Sword

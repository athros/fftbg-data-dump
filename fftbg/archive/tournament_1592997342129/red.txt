Player: !Red
Team: Red Team
Palettes: Red/Brown



Just Here2
Female
Cancer
67
43
Mediator
Jump
Meatbone Slash
Equip Polearm
Fly

Gokuu Rod

Golden Hairpin
Power Sleeve
Defense Ring

Praise, Threaten, Death Sentence, Refute
Level Jump8, Vertical Jump8



Grininda
Female
Scorpio
77
79
Geomancer
Draw Out
Auto Potion
Equip Bow
Move+1

Ice Bow

Flash Hat
Wizard Robe
Small Mantle

Pitfall, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bizen Boat, Murasame, Muramasa



NovaKnight21
Male
Scorpio
39
52
Time Mage
Elemental
Counter
Magic Defense UP
Waterbreathing

Wizard Staff

Black Hood
Black Robe
108 Gems

Slow 2, Stop, Float, Reflect, Demi 2, Galaxy Stop
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Shinzutalos
Male
Leo
42
72
Chemist
Draw Out
Auto Potion
Dual Wield
Jump+3

Panther Bag
Panther Bag
Thief Hat
Chain Vest
Jade Armlet

Potion, Hi-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Bizen Boat, Kiyomori

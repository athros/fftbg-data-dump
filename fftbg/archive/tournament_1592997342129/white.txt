Player: !White
Team: White Team
Palettes: White/Blue



Error72
Female
Aries
67
64
Squire
Elemental
Parry
Beastmaster
Waterwalking

Broad Sword
Mythril Shield
Iron Helmet
Crystal Mail
Elf Mantle

Accumulate, Heal, Cheer Up
Pitfall, Hell Ivy, Local Quake, Static Shock, Blizzard, Lava Ball



DeathTaxesAndAnime
Female
Aquarius
73
61
Lancer
Basic Skill
Sunken State
Equip Sword
Levitate

Asura Knife
Gold Shield
Gold Helmet
Crystal Mail
Red Shoes

Level Jump8, Vertical Jump2
Accumulate, Dash, Throw Stone, Yell, Fury



TeaTime29
Male
Scorpio
74
62
Archer
Elemental
Brave Save
Equip Bow
Ignore Terrain

Silver Bow

Golden Hairpin
Brigandine
Jade Armlet

Charge+1, Charge+3, Charge+4
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Lava Ball



Karol
Female
Leo
76
61
Squire
Draw Out
Catch
Sicken
Jump+3

Morning Star
Genji Shield
Crystal Helmet
Mystic Vest
Magic Gauntlet

Accumulate, Dash, Tickle
Asura, Koutetsu, Muramasa, Kikuichimoji

Final Bets: white - 13 bets for 7,466G (62.0%, x0.61); brown - 12 bets for 4,573G (38.0%, x1.63)

white bets:
Mesmaster: 3,000G (40.2%, 106,767G)
BirbBrainsBot: 1,000G (13.4%, 57,819G)
DusterW: 608G (8.1%, 608G)
DuraiPapers: 600G (8.0%, 1,640G)
OneHundredFists: 508G (6.8%, 508G)
Azelgrim: 500G (6.7%, 4,318G)
killth3kid: 450G (6.0%, 6,734G)
CorpusCav: 300G (4.0%, 1,195G)
AllInBot: 100G (1.3%, 100G)
fluffskull: 100G (1.3%, 1,917G)
Lythe_Caraker: 100G (1.3%, 109,960G)
datadrivenbot: 100G (1.3%, 44,553G)
Firesheath: 100G (1.3%, 14,677G)

brown bets:
BlackFireUK: 1,000G (21.9%, 25,111G)
prince_rogers_nelson_: 1,000G (21.9%, 6,891G)
CrownOfHorns: 504G (11.0%, 504G)
nifboy: 440G (9.6%, 440G)
DeadGirlzREasy: 315G (6.9%, 315G)
TeaTime29: 300G (6.6%, 14,961G)
TheChainNerd: 250G (5.5%, 3,662G)
autnagrag: 222G (4.9%, 2,061G)
ZCKaiser: 200G (4.4%, 1,881G)
oops25: 184G (4.0%, 1,684G)
CosmicTactician: 100G (2.2%, 8,723G)
getthemoneyz: 58G (1.3%, 966,176G)

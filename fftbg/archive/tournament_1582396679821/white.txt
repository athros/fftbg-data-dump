Player: !White
Team: White Team
Palettes: White/Blue



RampantMind
Monster
Virgo
54
40
Dryad










Sugdenunderscore
Monster
Leo
62
52
Apanda










Ayntlerz
Male
Capricorn
80
49
Squire
Jump
Arrow Guard
Long Status
Move+2

Blind Knife

Leather Hat
Mythril Armor
Elf Mantle

Dash, Throw Stone, Heal, Tickle, Yell
Level Jump4, Vertical Jump2



Neoshono
Female
Taurus
76
59
Geomancer
White Magic
Caution
Secret Hunt
Fly

Battle Axe
Round Shield
Red Hood
Light Robe
108 Gems

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Regen

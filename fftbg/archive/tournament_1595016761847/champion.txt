Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Zeroroute
Female
Virgo
72
57
Ninja
White Magic
MP Restore
Long Status
Teleport 2

Dagger
Assassin Dagger
Golden Hairpin
Earth Clothes
Elf Mantle

Shuriken, Knife
Cure 2, Cure 3, Raise, Shell 2, Esuna, Holy



Error72
Male
Aquarius
55
59
Knight
Jump
Faith Save
Secret Hunt
Move+2

Slasher
Escutcheon
Circlet
Bronze Armor
Feather Mantle

Head Break, Weapon Break, Magic Break, Stasis Sword, Surging Sword
Level Jump8, Vertical Jump5



Reddwind
Male
Gemini
40
78
Knight
Steal
Earplug
Dual Wield
Jump+1

Save the Queen
Chaos Blade
Crystal Helmet
Leather Armor
Sprint Shoes

Shield Break, Magic Break, Power Break, Justice Sword, Night Sword, Surging Sword
Gil Taking, Steal Heart, Steal Weapon, Arm Aim, Leg Aim



Killth3kid
Female
Cancer
72
79
Summoner
Draw Out
PA Save
Secret Hunt
Teleport

Gold Staff

Red Hood
Light Robe
Magic Ring

Moogle, Ifrit, Golem, Bahamut, Leviathan
Bizen Boat, Muramasa, Kikuichimoji, Chirijiraden

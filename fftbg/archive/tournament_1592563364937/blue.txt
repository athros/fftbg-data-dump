Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bruubarg
Female
Gemini
61
45
Ninja
Item
MA Save
Short Charge
Move+2

Mythril Knife
Dagger
Holy Miter
Leather Outfit
Rubber Shoes

Shuriken, Bomb, Knife
Hi-Ether, Eye Drop, Remedy, Phoenix Down



ForagerCats
Female
Aquarius
72
54
Archer
Summon Magic
Critical Quick
Equip Gun
Waterbreathing

Fairy Harp
Gold Shield
Red Hood
Mystic Vest
Power Wrist

Charge+1, Charge+3, Charge+4
Ramuh, Titan, Golem, Bahamut, Salamander



DuraiPapers
Male
Virgo
60
49
Wizard
Punch Art
HP Restore
Short Charge
Swim

Cultist Dagger

Green Beret
Linen Robe
Small Mantle

Bolt 2, Ice, Ice 2, Ice 3, Empower, Frog, Flare
Purification, Revive



Avin Chaos
Female
Libra
69
65
Monk
Time Magic
PA Save
Magic Defense UP
Retreat



Golden Hairpin
Black Costume
Germinas Boots

Spin Fist, Purification, Revive, Seal Evil
Stop, Immobilize, Float, Quick, Demi 2

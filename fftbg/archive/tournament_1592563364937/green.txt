Player: !Green
Team: Green Team
Palettes: Green/White



BlackFireUK
Female
Leo
68
54
Samurai
Battle Skill
Dragon Spirit
Magic Defense UP
Levitate

Heaven's Cloud

Crystal Helmet
Bronze Armor
Battle Boots

Asura, Muramasa
Head Break, Armor Break, Mind Break, Dark Sword, Night Sword



GrayGhostGaming
Female
Sagittarius
72
49
Archer
Battle Skill
Sunken State
Secret Hunt
Move+2

Ultimus Bow

Genji Helmet
Chain Vest
Angel Ring

Charge+2
Explosion Sword



YaBoy125
Male
Sagittarius
64
67
Ninja
Charge
MA Save
Equip Sword
Jump+3

Flail
Bizen Boat
Leather Hat
Black Costume
Defense Ring

Shuriken, Sword, Stick
Charge+2, Charge+3, Charge+10, Charge+20



AltimaMantoid
Female
Cancer
55
40
Thief
White Magic
Earplug
Maintenance
Retreat

Mage Masher

Cachusha
Chain Vest
Genji Gauntlet

Steal Weapon, Steal Status, Leg Aim
Cure, Cure 2, Cure 3, Cure 4, Raise, Protect, Esuna, Holy

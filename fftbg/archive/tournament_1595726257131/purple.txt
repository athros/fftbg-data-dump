Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Butterbelljedi
Monster
Serpentarius
70
64
Ultima Demon










Lowlf
Male
Leo
60
69
Monk
Summon Magic
HP Restore
Equip Gun
Waterbreathing

Mythril Gun

Twist Headband
Judo Outfit
Bracer

Purification, Chakra, Revive
Moogle, Titan, Carbunkle, Silf, Lich, Cyclops



Fenaen
Male
Capricorn
69
60
Oracle
White Magic
Mana Shield
Martial Arts
Move+2

Ivory Rod

Holy Miter
Wizard Outfit
Bracer

Life Drain, Pray Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Dark Holy
Cure, Cure 3, Raise, Shell 2, Esuna



Redmage4evah
Male
Pisces
49
63
Priest
Steal
Meatbone Slash
Dual Wield
Waterwalking

Rainbow Staff
Morning Star
Holy Miter
Linen Robe
Elf Mantle

Cure 2, Raise, Raise 2, Protect, Protect 2, Shell, Esuna
Gil Taking, Steal Helmet, Leg Aim

Player: !Red
Team: Red Team
Palettes: Red/Brown



Reddwind
Male
Gemini
76
49
Knight
Elemental
Parry
Martial Arts
Lava Walking


Kaiser Plate
Barbuta
Mythril Armor
Vanish Mantle

Shield Break, Speed Break, Power Break, Mind Break, Surging Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind



DAC169
Female
Aries
54
43
Monk
Talk Skill
HP Restore
Maintenance
Lava Walking



Headgear
Secret Clothes
Feather Boots

Spin Fist, Wave Fist, Secret Fist, Revive
Persuade, Preach, Solution, Mimic Daravon, Refute, Rehabilitate



Error72
Female
Virgo
71
66
Monk
Talk Skill
Blade Grasp
Monster Talk
Teleport



Thief Hat
Clothes
Germinas Boots

Secret Fist, Purification, Seal Evil
Invitation, Praise, Solution, Death Sentence, Mimic Daravon, Refute



Aldrammech
Female
Pisces
72
75
Chemist
Draw Out
Brave Save
Equip Shield
Move+3

Panther Bag
Round Shield
Barette
Adaman Vest
Sprint Shoes

Potion, Maiden's Kiss, Soft, Remedy
Heaven's Cloud, Muramasa

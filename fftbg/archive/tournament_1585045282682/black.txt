Player: !Black
Team: Black Team
Palettes: Black/Red



Djorama
Male
Leo
58
67
Bard
Steal
Counter
Dual Wield
Jump+2

Ice Bow

Black Hood
Black Costume
Reflect Ring

Angel Song, Battle Song, Magic Song
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



PlatinumPlume
Monster
Virgo
45
43
Hydra










Theregularirregular
Female
Libra
65
68
Dancer
Elemental
Abandon
Dual Wield
Move-MP Up

Persia
Cashmere
Leather Hat
White Robe
Red Shoes

Disillusion, Nameless Dance, Obsidian Blade
Pitfall, Water Ball, Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball



WinnerBit
Male
Virgo
59
60
Squire
Steal
Sunken State
Short Charge
Teleport

Mythril Knife
Bronze Shield
Leather Hat
Adaman Vest
Setiemson

Dash, Tickle, Yell
Steal Heart, Steal Armor, Steal Status, Leg Aim

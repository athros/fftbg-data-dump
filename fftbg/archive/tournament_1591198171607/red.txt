Player: !Red
Team: Red Team
Palettes: Red/Brown



Zepharoth89
Male
Libra
45
56
Mime

Counter Magic
Defend
Jump+2



Holy Miter
Linen Robe
Spike Shoes

Mimic




Lydian C
Female
Cancer
47
64
Ninja
Battle Skill
Arrow Guard
Equip Gun
Levitate

Bloody Strings
Battle Folio
Thief Hat
Judo Outfit
Red Shoes

Shuriken, Dictionary
Mind Break, Night Sword



TheMM42
Monster
Taurus
60
60
Dryad










Nickyfive
Female
Sagittarius
73
51
Priest
Talk Skill
Counter Flood
Defense UP
Waterwalking

Morning Star

Ribbon
Silk Robe
Jade Armlet

Cure 2, Raise, Raise 2, Shell, Shell 2, Esuna
Preach, Solution, Insult, Refute, Rehabilitate

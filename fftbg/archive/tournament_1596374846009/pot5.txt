Final Bets: blue - 7 bets for 24,684G (51.4%, x0.94); green - 11 bets for 23,293G (48.6%, x1.06)

blue bets:
douchetron: 21,272G (86.2%, 41,711G)
skipsandwiches: 1,256G (5.1%, 1,256G)
E_Ballard: 772G (3.1%, 772G)
Aldrammech: 564G (2.3%, 564G)
WillFitzgerald: 420G (1.7%, 8,674G)
AllInBot: 200G (0.8%, 200G)
datadrivenbot: 200G (0.8%, 50,189G)

green bets:
NIghtdew14: 10,694G (45.9%, 21,388G)
UmaiJam: 3,000G (12.9%, 54,522G)
Zbgs: 2,001G (8.6%, 8,564G)
reinoe: 2,000G (8.6%, 57,920G)
Thyrandaal: 2,000G (8.6%, 51,733G)
sinnyil2: 1,292G (5.5%, 1,292G)
BirbBrainsBot: 1,000G (4.3%, 135,197G)
Lord_Gwarth: 500G (2.1%, 1,240G)
getthemoneyz: 306G (1.3%, 1,475,213G)
maximumcrit: 300G (1.3%, 11,528G)
LordTomS: 200G (0.9%, 6,016G)

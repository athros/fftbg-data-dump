Player: !Brown
Team: Brown Team
Palettes: Brown/Green



WillFitzgerald
Male
Taurus
74
64
Priest
Time Magic
Catch
Equip Armor
Waterbreathing

Oak Staff

Leather Helmet
Linen Cuirass
Reflect Ring

Cure, Cure 4, Raise, Raise 2, Reraise, Shell, Shell 2
Haste, Slow 2, Reflect, Demi 2, Meteor



Upvla
Female
Aquarius
40
66
Geomancer
White Magic
Critical Quick
Magic Defense UP
Levitate

Mythril Sword
Kaiser Plate
Headgear
Power Sleeve
Magic Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 3, Raise, Protect, Protect 2, Wall, Esuna, Holy



Gelwain
Male
Pisces
64
63
Time Mage
Elemental
MA Save
Defense UP
Fly

White Staff

Black Hood
Chameleon Robe
Magic Ring

Haste, Slow, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Galkife
Female
Gemini
59
56
Mediator
Time Magic
MP Restore
Equip Axe
Waterwalking

White Staff

Leather Hat
Linen Robe
Genji Gauntlet

Invitation, Threaten, Insult, Mimic Daravon
Haste, Slow 2, Demi 2, Stabilize Time

Player: !Green
Team: Green Team
Palettes: Green/White



Actual JP
Male
Virgo
50
51
Knight
Steal
Faith Save
Equip Sword
Swim

Defender
Flame Shield
Circlet
Genji Armor
Red Shoes

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword, Dark Sword
Gil Taking, Steal Helmet



Sinnyil2
Male
Leo
56
42
Oracle
Throw
PA Save
Equip Shield
Jump+1

Battle Bamboo
Ice Shield
Twist Headband
Wizard Robe
Magic Ring

Pray Faith, Zombie, Blind Rage, Dispel Magic
Shuriken, Sword, Stick, Dictionary



Aldrammech
Female
Gemini
78
59
Squire
Steal
PA Save
Beastmaster
Fly

Flame Whip
Diamond Shield
Iron Helmet
Chain Vest
Cursed Ring

Throw Stone, Tickle, Yell
Gil Taking, Steal Heart, Steal Accessory



Reinoe
Male
Serpentarius
75
51
Ninja
Jump
Parry
Equip Sword
Waterbreathing

Masamune
Flail
Leather Hat
Chain Vest
Sprint Shoes

Shuriken, Bomb
Level Jump8, Vertical Jump6

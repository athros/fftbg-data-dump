Final Bets: white - 8 bets for 19,326G (80.3%, x0.25); brown - 8 bets for 4,738G (19.7%, x4.08)

white bets:
douchetron: 10,423G (53.9%, 20,439G)
pplvee1: 4,182G (21.6%, 4,182G)
UmaiJam: 3,000G (15.5%, 57,701G)
Aldrammech: 564G (2.9%, 564G)
WillFitzgerald: 420G (2.2%, 8,254G)
Lord_Gwarth: 337G (1.7%, 1,770G)
AllInBot: 200G (1.0%, 200G)
datadrivenbot: 200G (1.0%, 49,989G)

brown bets:
BirbBrainsBot: 1,000G (21.1%, 136,257G)
NIghtdew14: 1,000G (21.1%, 32,721G)
E_Ballard: 772G (16.3%, 772G)
maximumcrit: 500G (10.6%, 11,846G)
reinoe: 500G (10.6%, 60,039G)
Thyrandaal: 500G (10.6%, 53,852G)
Vaxaldan: 248G (5.2%, 248G)
getthemoneyz: 218G (4.6%, 1,475,537G)

Final Bets: purple - 13 bets for 13,978G (29.8%, x2.36); brown - 6 bets for 32,967G (70.2%, x0.42)

purple bets:
NIghtdew14: 5,000G (35.8%, 26,388G)
UmaiJam: 3,000G (21.5%, 57,522G)
E_Ballard: 1,542G (11.0%, 1,542G)
BirbBrainsBot: 1,000G (7.2%, 136,197G)
Zbgs: 1,000G (7.2%, 9,564G)
Aldrammech: 564G (4.0%, 564G)
Forkmore: 500G (3.6%, 1,712G)
maximumcrit: 300G (2.1%, 11,828G)
getthemoneyz: 236G (1.7%, 1,475,449G)
silentkaster: 236G (1.7%, 20,863G)
AllInBot: 200G (1.4%, 200G)
datadrivenbot: 200G (1.4%, 50,389G)
nhammen: 200G (1.4%, 30,196G)

brown bets:
douchetron: 17,490G (53.1%, 34,295G)
reinoe: 10,000G (30.3%, 53,680G)
pplvee1: 2,937G (8.9%, 2,937G)
upvla: 1,308G (4.0%, 1,308G)
skipsandwiches: 882G (2.7%, 882G)
Lord_Gwarth: 350G (1.1%, 1,092G)

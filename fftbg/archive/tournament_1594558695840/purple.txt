Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Randgridr
Male
Gemini
61
57
Geomancer
Time Magic
Earplug
Equip Bow
Move-MP Up

Hunting Bow
Mythril Shield
Barette
Leather Outfit
Angel Ring

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Haste, Stabilize Time



SomthingMore
Female
Gemini
56
77
Calculator
White Magic
Counter
Concentrate
Waterbreathing

Bestiary

Flash Hat
Adaman Vest
Angel Ring

CT, Height, Prime Number, 3
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Esuna



ALY327
Male
Aquarius
75
58
Priest
Talk Skill
Sunken State
Sicken
Jump+1

Healing Staff

Black Hood
Judo Outfit
Small Mantle

Cure, Protect 2, Shell, Esuna
Praise, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



Erich
Male
Scorpio
54
77
Mime

Counter
Equip Shield
Move+2


Genji Shield
Holy Miter
Mythril Vest
Bracer

Mimic


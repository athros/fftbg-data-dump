Player: !Red
Team: Red Team
Palettes: Red/Brown



DeathTaxesAndAnime
Female
Aquarius
61
49
Lancer
Item
Counter
Attack UP
Retreat

Partisan
Flame Shield
Circlet
Diamond Armor
Feather Mantle

Level Jump2, Vertical Jump2
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



Prince Rogers Nelson
Monster
Sagittarius
79
55
King Behemoth










Upvla
Male
Gemini
43
61
Ninja
Summon Magic
Regenerator
Equip Gun
Move-HP Up

Ramia Harp
Bestiary
Ribbon
Adaman Vest
Feather Boots

Wand
Ramuh, Bahamut, Odin, Silf



Just Here2
Male
Pisces
49
79
Bard
White Magic
Absorb Used MP
Martial Arts
Move+3



Twist Headband
Linen Cuirass
Sprint Shoes

Cheer Song, Magic Song
Cure 3, Cure 4, Raise, Reraise, Protect, Protect 2, Esuna, Magic Barrier

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sairentozon7
Male
Capricorn
66
70
Knight
Elemental
Parry
Equip Knife
Waterwalking

Wizard Rod
Mythril Shield
Mythril Helmet
Mythril Armor
Angel Ring

Head Break, Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword
Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Soren Of Tyto
Female
Capricorn
43
73
Wizard
Basic Skill
Critical Quick
Dual Wield
Move+2

Dragon Rod
Faith Rod
Holy Miter
Earth Clothes
Magic Gauntlet

Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice, Ice 2, Flare
Dash, Heal, Yell, Cheer Up, Wish



NovaKnight21
Female
Gemini
79
40
Mediator
Yin Yang Magic
Meatbone Slash
Martial Arts
Move+2



Twist Headband
Black Robe
Diamond Armlet

Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Pray Faith, Zombie, Dispel Magic, Paralyze, Sleep



Lowlf
Male
Virgo
58
45
Monk
Draw Out
Critical Quick
Equip Armor
Lava Walking



Circlet
Black Robe
108 Gems

Pummel, Earth Slash, Secret Fist, Purification, Revive
Asura, Kiyomori, Masamune

Final Bets: red - 10 bets for 10,473G (60.6%, x0.65); blue - 9 bets for 6,822G (39.4%, x1.54)

red bets:
Adam1949: 6,415G (61.3%, 64,152G)
killth3kid: 1,445G (13.8%, 1,445G)
SkylerBunny: 1,200G (11.5%, 28,452G)
Seaweed_B: 500G (4.8%, 42,346G)
MemoriesofFinal: 212G (2.0%, 212G)
DeathTaxesAndAnime: 200G (1.9%, 1,876G)
datadrivenbot: 200G (1.9%, 62,899G)
gorgewall: 101G (1.0%, 18,146G)
AllInBot: 100G (1.0%, 100G)
AbandonedHall: 100G (1.0%, 1,273G)

blue bets:
HaateXIII: 1,754G (25.7%, 1,754G)
thunderducker: 1,281G (18.8%, 1,281G)
sinnyil2: 1,032G (15.1%, 1,032G)
BirbBrainsBot: 1,000G (14.7%, 44,761G)
Laserman1000: 537G (7.9%, 21,237G)
getthemoneyz: 498G (7.3%, 1,748,064G)
chronicdeath: 300G (4.4%, 1,926G)
holandrix: 220G (3.2%, 220G)
Exaltedone87: 200G (2.9%, 1,000G)

Final Bets: yellow - 18 bets for 13,616G (45.5%, x1.20); white - 26 bets for 16,334G (54.5%, x0.83)

yellow bets:
Shalloween: 5,489G (40.3%, 10,763G)
happyfundude: 2,000G (14.7%, 12,038G)
Eyepoor_: 1,587G (11.7%, 1,587G)
Aldrammech: 1,000G (7.3%, 22,313G)
twelfthrootoftwo: 760G (5.6%, 760G)
hydroshade: 500G (3.7%, 1,440G)
Baron_von_Scrub: 332G (2.4%, 332G)
Kronikle: 300G (2.2%, 2,718G)
evdoggity: 300G (2.2%, 4,083G)
ZephyrTempest: 282G (2.1%, 5,640G)
Bloody_Nips: 250G (1.8%, 829G)
KasugaiRoastedPeas: 200G (1.5%, 4,918G)
RestIessNight: 116G (0.9%, 116G)
nifboy: 100G (0.7%, 4,848G)
mpghappiness: 100G (0.7%, 1,377G)
Boultson1337: 100G (0.7%, 100G)
Evewho: 100G (0.7%, 10,013G)
OneHundredFists: 100G (0.7%, 1,261G)

white bets:
HaateXIII: 2,382G (14.6%, 4,764G)
evontno: 2,001G (12.3%, 13,469G)
volgrathemoose: 1,453G (8.9%, 2,907G)
jakeduhhsnake: 1,000G (6.1%, 76,243G)
ungabunga_bot: 1,000G (6.1%, 329,486G)
arch8000: 1,000G (6.1%, 7,396G)
reinoe: 846G (5.2%, 846G)
genericco: 807G (4.9%, 807G)
Laserman1000: 806G (4.9%, 89,706G)
NovaKnight21: 708G (4.3%, 708G)
ShintaroNayaka: 551G (3.4%, 2,139G)
MrDiggs49: 510G (3.1%, 510G)
getthemoneyz: 502G (3.1%, 610,200G)
WrathfulRemy: 500G (3.1%, 2,207G)
Breakdown777: 500G (3.1%, 19,326G)
mirapoix: 468G (2.9%, 468G)
rico_flex: 250G (1.5%, 31,615G)
kaidykat: 200G (1.2%, 1,860G)
BirbBrainsBot: 200G (1.2%, 200G)
maakur_: 100G (0.6%, 119,741G)
frozentomato: 100G (0.6%, 1,310G)
Langrisser: 100G (0.6%, 5,899G)
Kooks_R_Us: 100G (0.6%, 1,206G)
datadrivenbot: 100G (0.6%, 12,395G)
Squatting_Bear: 100G (0.6%, 1,657G)
DrAntiSocial: 50G (0.3%, 8,384G)

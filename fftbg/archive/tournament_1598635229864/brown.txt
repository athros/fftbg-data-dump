Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Error72
Male
Sagittarius
43
57
Wizard
Item
PA Save
Dual Wield
Move+3

Assassin Dagger
Faith Rod
Golden Hairpin
Mythril Vest
Germinas Boots

Fire 2, Fire 3, Ice 2
Potion, Antidote, Phoenix Down



RiffRoff
Monster
Capricorn
39
67
Revenant










Just Here2
Female
Leo
43
78
Priest
Yin Yang Magic
Counter Magic
Short Charge
Waterbreathing

Scorpion Tail

Red Hood
Leather Outfit
Germinas Boots

Cure 4, Raise, Esuna, Magic Barrier
Blind, Poison, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Shalloween
Female
Virgo
58
71
Dancer
Throw
Auto Potion
Defend
Lava Walking

Cashmere

Leather Hat
White Robe
Wizard Mantle

Witch Hunt, Slow Dance, Disillusion, Last Dance, Dragon Pit
Bomb, Stick

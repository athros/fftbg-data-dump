Player: !Black
Team: Black Team
Palettes: Black/Red



ColetteMSLP
Female
Gemini
49
51
Oracle
Jump
Catch
Equip Axe
Swim

Battle Axe

Black Hood
Silk Robe
N-Kai Armlet

Poison, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep
Level Jump2, Vertical Jump2



Hyral Delita
Female
Virgo
48
53
Calculator
Yin Yang Magic
PA Save
Concentrate
Move+1

Gokuu Rod

Golden Hairpin
Wizard Robe
Small Mantle

CT, 5, 4
Poison, Life Drain, Confusion Song, Dispel Magic, Petrify



Takuwan0405
Male
Pisces
62
67
Time Mage
Jump
MP Restore
Equip Shield
Jump+2

Oak Staff
Mythril Shield
Holy Miter
Wizard Robe
Leather Mantle

Haste, Haste 2, Slow, Immobilize, Demi 2, Stabilize Time, Meteor
Level Jump5, Vertical Jump8



Hasterious
Male
Scorpio
42
67
Geomancer
Battle Skill
Auto Potion
Beastmaster
Retreat

Ice Brand
Gold Shield
Headgear
Mystic Vest
Cursed Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Head Break, Armor Break

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Upvla
Monster
Aries
40
55
Tiamat










Rogueain
Male
Taurus
73
75
Samurai
Charge
PA Save
Attack UP
Swim

Murasame

Diamond Helmet
Chain Mail
Feather Boots

Asura, Murasame
Charge+1, Charge+7



Pandasforsale
Female
Pisces
51
48
Archer
Item
Parry
Throw Item
Levitate

Romanda Gun
Flame Shield
Golden Hairpin
Judo Outfit
Rubber Shoes

Charge+1, Charge+4, Charge+5, Charge+20
Potion, Hi-Potion, X-Potion, Maiden's Kiss, Holy Water, Phoenix Down



Imranimran
Male
Taurus
46
74
Mediator
Charge
Counter Magic
Equip Shield
Waterbreathing

Dagger
Aegis Shield
Triangle Hat
Linen Robe
Leather Mantle

Invitation, Persuade, Death Sentence, Insult, Mimic Daravon
Charge+7, Charge+10

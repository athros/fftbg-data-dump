Player: !Black
Team: Black Team
Palettes: Black/Red



CassiePhoenix
Female
Capricorn
53
60
Chemist
Summon Magic
Blade Grasp
Maintenance
Jump+3

Hydra Bag

Holy Miter
Wizard Outfit
Dracula Mantle

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down
Moogle, Shiva, Ramuh, Golem, Bahamut, Salamander, Cyclops



BoneMiser
Male
Aquarius
77
75
Chemist
Sing
Absorb Used MP
Equip Gun
Teleport

Mythril Gun

Red Hood
Mystic Vest
Feather Boots

Potion, Hi-Potion, Soft, Holy Water, Remedy, Phoenix Down
Life Song, Nameless Song, Last Song, Hydra Pit



ALY327
Male
Virgo
42
79
Calculator
Time Magic
Counter
Halve MP
Swim

Battle Folio

Holy Miter
Black Robe
Cursed Ring

CT, Height, Prime Number, 5, 4
Slow, Stop, Float, Demi, Meteor



VanciousRex
Female
Libra
41
62
Monk
Item
Brave Save
Defense UP
Swim



Twist Headband
Chain Vest
Cursed Ring

Wave Fist, Purification, Revive
Potion, X-Potion, Ether, Eye Drop, Echo Grass, Soft, Phoenix Down

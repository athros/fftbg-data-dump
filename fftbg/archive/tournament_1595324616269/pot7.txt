Final Bets: blue - 5 bets for 1,976G (16.0%, x5.26); white - 12 bets for 10,388G (84.0%, x0.19)

blue bets:
VolgraTheMoose: 1,001G (50.7%, 16,324G)
randgridr: 444G (22.5%, 444G)
getthemoneyz: 210G (10.6%, 1,330,075G)
AllInBot: 200G (10.1%, 200G)
Lydian_C: 121G (6.1%, 100,816G)

white bets:
Ruvelia_BibeI: 3,017G (29.0%, 5,917G)
twelfthrootoftwo: 1,465G (14.1%, 1,465G)
ruleof5: 1,000G (9.6%, 13,920G)
Zetchryn: 1,000G (9.6%, 3,952G)
BirbBrainsBot: 1,000G (9.6%, 142,529G)
gorgewall: 676G (6.5%, 676G)
Evewho: 500G (4.8%, 7,775G)
0v3rr8d: 500G (4.8%, 23,160G)
dem0nj0ns: 500G (4.8%, 2,156G)
NIghtdew14: 500G (4.8%, 14,227G)
datadrivenbot: 200G (1.9%, 32,204G)
7Cerulean7: 30G (0.3%, 8,005G)

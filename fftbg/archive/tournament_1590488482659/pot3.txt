Final Bets: white - 10 bets for 1,918G (46.6%, x1.14); black - 6 bets for 2,196G (53.4%, x0.87)

white bets:
CorpusCav: 500G (26.1%, 12,481G)
otakutaylor: 300G (15.6%, 2,923G)
GnielKnows: 252G (13.1%, 252G)
Levi_System: 200G (10.4%, 757G)
AllInBot: 166G (8.7%, 166G)
Jeeboheebo: 100G (5.2%, 2,605G)
rednecknazgul: 100G (5.2%, 1,641G)
CrownOfHorns: 100G (5.2%, 1,347G)
datadrivenbot: 100G (5.2%, 20,167G)
letdowncity: 100G (5.2%, 2,656G)

black bets:
BirbBrainsBot: 1,000G (45.5%, 171,580G)
LivingHitokiri: 500G (22.8%, 15,198G)
Digitalsocrates: 200G (9.1%, 2,657G)
thenextlantern: 200G (9.1%, 1,046G)
getthemoneyz: 196G (8.9%, 729,436G)
Evewho: 100G (4.6%, 5,503G)

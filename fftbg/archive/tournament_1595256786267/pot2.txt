Final Bets: green - 18 bets for 29,247G (61.6%, x0.62); yellow - 12 bets for 18,194G (38.4%, x1.61)

green bets:
RaIshtar: 10,000G (34.2%, 77,185G)
coralreeferz: 6,255G (21.4%, 6,255G)
Zetchryn: 2,000G (6.8%, 7,086G)
nhammen: 2,000G (6.8%, 11,357G)
Zeroroute: 1,572G (5.4%, 3,025G)
DeathTaxesAndAnime: 1,439G (4.9%, 1,439G)
holdenmagronik: 1,000G (3.4%, 1,000G)
prince_rogers_nelson_: 1,000G (3.4%, 2,506G)
JumbocactuarX27: 1,000G (3.4%, 42,093G)
killth3kid: 845G (2.9%, 845G)
VolgraTheMoose: 500G (1.7%, 12,487G)
randgridr: 436G (1.5%, 9,442G)
MattMan119: 300G (1.0%, 5,117G)
CapnChaos12: 300G (1.0%, 7,205G)
AllInBot: 200G (0.7%, 200G)
datadrivenbot: 200G (0.7%, 33,412G)
Miku_Shikhu: 100G (0.3%, 15,356G)
DouglasDragonThePoet: 100G (0.3%, 861G)

yellow bets:
helpimabug: 13,653G (75.0%, 27,306G)
nifboy: 1,000G (5.5%, 6,305G)
BirbBrainsBot: 1,000G (5.5%, 139,437G)
mirapoix: 572G (3.1%, 572G)
Xoomwaffle: 526G (2.9%, 526G)
Legitimized: 468G (2.6%, 468G)
getthemoneyz: 254G (1.4%, 1,331,379G)
Scurg: 200G (1.1%, 200G)
peeronid: 200G (1.1%, 11,197G)
Lydian_C: 121G (0.7%, 75,957G)
Bongomon7: 100G (0.5%, 10,372G)
ohhinm: 100G (0.5%, 4,841G)

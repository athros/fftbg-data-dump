Final Bets: brown - 13 bets for 15,774G (21.8%, x3.58); champion - 22 bets for 56,461G (78.2%, x0.28)

brown bets:
SephDarkheart: 5,550G (35.2%, 55,503G)
helpimabug: 4,247G (26.9%, 8,494G)
lowlf: 1,111G (7.0%, 106,496G)
dogsandcatsand: 1,100G (7.0%, 1,100G)
killth3kid: 960G (6.1%, 960G)
prince_rogers_nelson_: 700G (4.4%, 7,206G)
ShintaroNayaka: 500G (3.2%, 10,464G)
Scurg: 456G (2.9%, 456G)
baconbacon1207: 300G (1.9%, 14,258G)
silentkaster: 300G (1.9%, 12,336G)
brokenknight201: 300G (1.9%, 1,974G)
holdenmagronik: 200G (1.3%, 2,165G)
DouglasDragonThePoet: 50G (0.3%, 1,573G)

champion bets:
RaIshtar: 20,000G (35.4%, 87,791G)
Lydian_C: 12,000G (21.3%, 76,161G)
red__lancer: 5,000G (8.9%, 110,035G)
Error72: 2,394G (4.2%, 4,696G)
Zetchryn: 2,000G (3.5%, 9,144G)
ar_tactic: 2,000G (3.5%, 80,430G)
nhammen: 2,000G (3.5%, 12,796G)
Thyrandaal: 2,000G (3.5%, 9,510G)
Lord_Burrah: 1,500G (2.7%, 80,500G)
Bongomon7: 1,000G (1.8%, 10,086G)
randgridr: 1,000G (1.8%, 10,184G)
BirbBrainsBot: 1,000G (1.8%, 136,992G)
JumbocactuarX27: 1,000G (1.8%, 47,368G)
Zeroroute: 600G (1.1%, 600G)
AllInBot: 581G (1.0%, 581G)
furrytomahawkk: 500G (0.9%, 1,779G)
MattMan119: 500G (0.9%, 5,776G)
getthemoneyz: 440G (0.8%, 1,330,540G)
CapnChaos12: 300G (0.5%, 6,835G)
tsukiyo2830: 230G (0.4%, 230G)
SUGRboi: 216G (0.4%, 216G)
datadrivenbot: 200G (0.4%, 33,528G)

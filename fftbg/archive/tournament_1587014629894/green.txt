Player: !Green
Team: Green Team
Palettes: Green/White



GrandmasterFrankerZ
Female
Aries
56
76
Geomancer
Draw Out
Damage Split
Doublehand
Fly

Bizen Boat

Leather Hat
Adaman Vest
Wizard Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



Leakimiko
Female
Cancer
47
78
Wizard
Basic Skill
Dragon Spirit
Short Charge
Teleport

Poison Rod

Leather Hat
Silk Robe
Defense Ring

Fire 3, Bolt 3, Bolt 4, Ice 2
Throw Stone, Heal, Cheer Up, Scream



DrAntiSocial
Female
Leo
59
60
Chemist
Dance
Counter
Equip Gun
Jump+3

Mythril Gun

Triangle Hat
Clothes
Setiemson

Potion, Hi-Potion, Echo Grass, Soft, Phoenix Down
Witch Hunt, Wiznaibus, Obsidian Blade



ZephyrTempest
Male
Serpentarius
73
53
Mime

Regenerator
Equip Shield
Move+3


Hero Shield
Leather Hat
Adaman Vest
Red Shoes

Mimic


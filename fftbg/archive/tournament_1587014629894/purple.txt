Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



WireLord
Male
Gemini
78
64
Bard
Battle Skill
Critical Quick
Sicken
Waterbreathing

Lightning Bow

Holy Miter
Power Sleeve
Jade Armlet

Angel Song, Life Song, Cheer Song, Diamond Blade, Sky Demon, Hydra Pit
Shield Break, Weapon Break, Power Break, Stasis Sword, Justice Sword, Night Sword



Baron Von Scrub
Male
Capricorn
75
44
Thief
Punch Art
Counter Magic
Attack UP
Jump+3

Ancient Sword

Red Hood
Power Sleeve
Rubber Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Earth Slash, Purification, Seal Evil



ZZ Yoshi
Monster
Taurus
81
42
Explosive










Shalloween
Male
Virgo
58
79
Samurai
Item
Critical Quick
Throw Item
Teleport

Masamune

Circlet
White Robe
Diamond Armlet

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Potion, X-Potion, Antidote, Soft, Phoenix Down

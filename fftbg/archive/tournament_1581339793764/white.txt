Player: !White
Team: White Team
Palettes: White/Blue



Sershia
Female
Scorpio
59
55
Samurai
Time Magic
Speed Save
Beastmaster
Jump+2

Murasame

Mythril Helmet
Gold Armor
Feather Mantle

Asura, Bizen Boat, Kiyomori, Muramasa
Slow, Stop, Immobilize, Float, Reflect, Demi, Stabilize Time



Revvy
Male
Aquarius
72
71
Calculator
Beast Skill
MA Save
Equip Shield
Fly

Battle Folio
Gold Shield
Holy Miter
Judo Outfit
Small Mantle

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



Error72
Female
Leo
73
70
Calculator
Bio
Caution
Equip Armor
Teleport

Rod

Barbuta
Earth Clothes
Bracer

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Coyowijuju
Female
Aries
75
61
Ninja
Draw Out
HP Restore
Equip Polearm
Retreat

Flail
Flail
Barette
Power Sleeve
Red Shoes

Shuriken, Bomb, Axe, Dictionary
Asura, Murasame, Kiyomori, Muramasa

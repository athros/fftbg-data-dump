Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Jahshine
Male
Aquarius
48
80
Squire
Yin Yang Magic
Faith Up
Long Status
Levitate

Iron Sword
Bronze Shield
Feather Hat
Black Costume
Diamond Armlet

Accumulate, Heal, Tickle, Yell, Fury, Wish
Poison, Spell Absorb, Life Drain, Doubt Faith, Sleep



Skullshatterer
Female
Cancer
78
57
Lancer
Yin Yang Magic
Dragon Spirit
Equip Gun
Teleport

Battle Folio
Buckler
Crystal Helmet
Reflect Mail
Feather Boots

Level Jump2, Vertical Jump6
Spell Absorb, Zombie, Foxbird, Confusion Song, Dispel Magic, Dark Holy



LanseDM
Male
Aquarius
56
53
Mime

Counter Magic
Equip Armor
Ignore Terrain



Genji Helmet
Crystal Mail
Elf Mantle

Mimic




Twelfthrootoftwo
Female
Virgo
74
46
Thief
Black Magic
Counter
Equip Axe
Fly

Flame Whip

Feather Hat
Chain Vest
Cherche

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Empower, Frog, Death

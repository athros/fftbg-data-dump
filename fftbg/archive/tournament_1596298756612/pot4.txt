Final Bets: purple - 10 bets for 13,188G (65.8%, x0.52); brown - 9 bets for 6,864G (34.2%, x1.92)

purple bets:
Aldrammech: 10,000G (75.8%, 36,454G)
Zeroroute: 600G (4.5%, 600G)
Shalloween: 500G (3.8%, 15,707G)
Thyrandaal: 500G (3.8%, 133,283G)
dogsandcatsand: 420G (3.2%, 13,217G)
Chambs12: 368G (2.8%, 368G)
maximumcrit: 300G (2.3%, 10,402G)
datadrivenbot: 200G (1.5%, 44,428G)
TasisSai: 200G (1.5%, 13,805G)
HonestlyNobu: 100G (0.8%, 1,000G)

brown bets:
DeathTaxesAndAnime: 2,922G (42.6%, 2,922G)
VolgraTheMoose: 884G (12.9%, 884G)
getthemoneyz: 872G (12.7%, 1,461,608G)
AlenaZarek: 550G (8.0%, 48,153G)
nekojin: 500G (7.3%, 11,469G)
BirbBrainsBot: 416G (6.1%, 121,548G)
Smashy: 300G (4.4%, 9,582G)
Othatmattkid: 220G (3.2%, 220G)
AllInBot: 200G (2.9%, 200G)

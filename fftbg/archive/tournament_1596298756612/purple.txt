Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Wooplestein
Male
Taurus
62
61
Mediator
Black Magic
Hamedo
Defend
Waterwalking

Romanda Gun

Red Hood
Mystic Vest
Bracer

Invitation, Praise, Solution, Refute, Rehabilitate
Fire 2, Bolt 3, Ice 4, Empower



SensualADC
Female
Gemini
71
69
Monk
Yin Yang Magic
Counter
Concentrate
Lava Walking



Leather Hat
Leather Outfit
Defense Ring

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Blind, Poison, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep



Zachara
Male
Aries
80
52
Mediator
Battle Skill
Counter Tackle
Dual Wield
Swim

Mythril Gun
Glacier Gun
Headgear
Silk Robe
Power Wrist

Invitation, Praise, Death Sentence, Insult, Negotiate, Mimic Daravon
Shield Break, Magic Break, Speed Break, Justice Sword, Dark Sword



Silentkaster
Monster
Taurus
48
61
Explosive








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thyrandaal
Male
Scorpio
69
64
Monk
Battle Skill
HP Restore
Equip Gun
Waterbreathing

Romanda Gun

Red Hood
Power Sleeve
Setiemson

Pummel, Wave Fist, Purification, Revive
Armor Break, Weapon Break, Stasis Sword, Justice Sword, Dark Sword



Sinnyil2
Male
Taurus
76
66
Time Mage
Sing
Damage Split
Concentrate
Waterbreathing

White Staff

Thief Hat
Earth Clothes
Battle Boots

Haste, Slow, Slow 2, Reflect, Stabilize Time
Angel Song, Life Song



Grandlanzer
Male
Cancer
58
46
Thief
Summon Magic
MP Restore
Martial Arts
Lava Walking



Barette
Earth Clothes
Red Shoes

Steal Helmet, Steal Shield, Steal Weapon, Leg Aim
Ifrit, Carbunkle, Bahamut, Odin, Salamander, Silf



BakuretsuLa
Female
Sagittarius
60
78
Archer
Basic Skill
Dragon Spirit
Defense UP
Move-HP Up

Lightning Bow

Golden Hairpin
Mythril Vest
Angel Ring

Charge+1, Charge+4, Charge+5, Charge+20
Tickle, Fury, Wish

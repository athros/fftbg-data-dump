Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



PoroTact
Male
Scorpio
72
64
Wizard
Basic Skill
Hamedo
Maintenance
Move+1

Cultist Dagger

Green Beret
Adaman Vest
Diamond Armlet

Fire, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower, Death
Throw Stone, Heal, Yell, Wish, Scream



ArashiKurobara
Male
Aries
50
48
Mediator
Item
MA Save
Equip Knife
Swim

Hidden Knife

Leather Hat
Silk Robe
Magic Ring

Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
X-Potion, Ether, Antidote



CosmicTactician
Female
Leo
56
60
Mediator
White Magic
Speed Save
Dual Wield
Jump+2

Stone Gun
Glacier Gun
Green Beret
White Robe
Feather Mantle

Praise, Negotiate
Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Wall, Esuna



Just Here2
Female
Sagittarius
57
43
Mediator
Item
Caution
Dual Wield
Waterbreathing

Mythril Gun
Romanda Gun
Red Hood
Black Costume
Rubber Shoes

Solution, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Potion, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



Laserman1000
Male
Taurus
41
76
Time Mage
Sing
Faith Up
Short Charge
Move+1

Gokuu Rod

Golden Hairpin
Chameleon Robe
Vanish Mantle

Haste 2, Slow 2, Float, Demi, Galaxy Stop
Angel Song, Battle Song, Nameless Song, Sky Demon



Lionhermit
Female
Leo
50
75
Summoner
Dance
HP Restore
Equip Polearm
Teleport

Octagon Rod

Black Hood
Wizard Robe
Jade Armlet

Moogle, Carbunkle, Salamander, Fairy, Lich
Polka Polka, Nameless Dance



ALY327
Female
Libra
57
59
Time Mage
Steal
Abandon
Attack UP
Move-MP Up

Oak Staff

Flash Hat
Chameleon Robe
Magic Gauntlet

Haste, Haste 2, Slow 2, Stop, Stabilize Time
Steal Helmet, Steal Shield



TimeJannies
Male
Taurus
64
58
Thief
Sing
Absorb Used MP
Doublehand
Move+3

Cultist Dagger

Green Beret
Brigandine
Wizard Mantle

Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Angel Song, Life Song, Cheer Song, Magic Song, Last Song, Hydra Pit

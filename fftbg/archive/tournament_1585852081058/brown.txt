Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rrazzoug
Male
Leo
46
77
Lancer
Basic Skill
Damage Split
Equip Sword
Waterwalking

Save the Queen

Gold Helmet
Plate Mail
Feather Mantle

Level Jump5, Vertical Jump5
Heal



Aeolus 000
Male
Aquarius
65
39
Bard
Time Magic
Counter Magic
Defense UP
Levitate

Fairy Harp

Leather Hat
Judo Outfit
108 Gems

Life Song, Battle Song, Magic Song, Space Storage, Sky Demon
Haste, Slow, Reflect, Demi, Stabilize Time



HaplessOne
Female
Pisces
58
74
Oracle
Basic Skill
Arrow Guard
Doublehand
Lava Walking

Musk Rod

Feather Hat
Black Robe
Power Wrist

Spell Absorb, Silence Song, Confusion Song, Dispel Magic, Paralyze
Accumulate, Heal, Yell, Wish



Goust18
Female
Scorpio
57
74
Monk
Battle Skill
Regenerator
Magic Attack UP
Ignore Height



Flash Hat
Power Sleeve
Feather Boots

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Head Break, Armor Break, Speed Break, Mind Break, Justice Sword, Night Sword

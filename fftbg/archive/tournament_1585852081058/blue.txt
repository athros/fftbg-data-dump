Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Hales Bopp It
Female
Libra
76
55
Knight
Punch Art
MP Restore
Martial Arts
Jump+3

Ragnarok
Buckler
Gold Helmet
Leather Armor
Sprint Shoes

Head Break, Shield Break, Power Break, Justice Sword, Dark Sword, Night Sword, Surging Sword
Secret Fist, Purification, Revive, Seal Evil



KaLam1ty
Male
Aquarius
45
67
Squire
Sing
Counter Flood
Attack UP
Jump+1

Bow Gun
Buckler
Leather Hat
Brigandine
Red Shoes

Accumulate, Dash, Throw Stone, Heal, Tickle
Angel Song, Battle Song, Last Song, Sky Demon



Lythe Caraker
Female
Gemini
60
73
Ninja
White Magic
Dragon Spirit
Secret Hunt
Fly

Assassin Dagger
Sasuke Knife
Leather Hat
Mystic Vest
Jade Armlet

Shuriken, Axe
Cure 2, Raise, Protect 2, Shell 2, Esuna, Holy



Numbersborne
Male
Leo
57
47
Mime

Damage Split
Equip Armor
Jump+3



Green Beret
Black Robe
Cursed Ring

Mimic


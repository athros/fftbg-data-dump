Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



DarrenDinosaurs
Male
Capricorn
55
67
Squire
Talk Skill
Auto Potion
Concentrate
Swim

Zorlin Shape
Round Shield
Barbuta
Chain Vest
Power Wrist

Accumulate, Dash, Throw Stone, Heal, Fury, Scream
Threaten, Preach, Death Sentence, Refute



E Ballard
Male
Capricorn
62
70
Wizard
Yin Yang Magic
Earplug
Defense UP
Jump+2

Dragon Rod

Holy Miter
Mythril Vest
Germinas Boots

Bolt 2, Bolt 3, Ice, Empower, Death
Spell Absorb, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Gunz232323
Male
Sagittarius
49
64
Chemist
Jump
Auto Potion
Attack UP
Ignore Height

Blind Knife

Golden Hairpin
Chain Vest
N-Kai Armlet

Potion, Hi-Potion, Soft, Holy Water, Phoenix Down
Level Jump5, Vertical Jump8



HASTERIOUS
Male
Leo
48
42
Lancer
Item
Regenerator
Secret Hunt
Move-MP Up

Holy Lance
Platinum Shield
Genji Helmet
Plate Mail
Genji Gauntlet

Level Jump8, Vertical Jump7
Potion, Hi-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down

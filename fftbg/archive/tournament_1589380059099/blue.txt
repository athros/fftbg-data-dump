Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaplessOne
Male
Aries
71
69
Ninja
Draw Out
Counter
Beastmaster
Jump+3

Spell Edge
Hidden Knife
Twist Headband
Earth Clothes
Wizard Mantle

Bomb, Staff
Bizen Boat, Murasame, Kiyomori



Ko2q
Male
Leo
66
61
Knight
Time Magic
Speed Save
Defend
Jump+2

Save the Queen
Genji Shield
Leather Helmet
Mythril Armor
Power Wrist

Armor Break, Speed Break, Surging Sword
Haste, Haste 2, Reflect



Kellios11
Monster
Libra
62
49
Ochu










Snaccubus
Male
Scorpio
78
42
Summoner
Black Magic
Counter Tackle
Defense UP
Ignore Terrain

Ice Rod

Feather Hat
Brigandine
Feather Boots

Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Salamander, Fairy, Lich
Fire, Fire 2, Empower

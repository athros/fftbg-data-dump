Final Bets: purple - 19 bets for 9,994G (49.9%, x1.01); brown - 8 bets for 10,053G (50.1%, x0.99)

purple bets:
midnight_blues3: 2,099G (21.0%, 13,995G)
BirbBrainsBot: 1,000G (10.0%, 2,607G)
E_Ballard: 1,000G (10.0%, 2,427G)
Lord_Burrah: 1,000G (10.0%, 76,573G)
getthemoneyz: 940G (9.4%, 1,245,547G)
Nizaha: 536G (5.4%, 4,425G)
MagicBottle: 520G (5.2%, 12,425G)
ColetteMSLP: 496G (5.0%, 496G)
roofiepops: 444G (4.4%, 7,007G)
run_with_stone_GUNs: 308G (3.1%, 10,091G)
CapnChaos12: 300G (3.0%, 7,782G)
Zagorsek: 292G (2.9%, 292G)
shineeyo: 257G (2.6%, 257G)
gorgewall: 201G (2.0%, 9,227G)
datadrivenbot: 200G (2.0%, 62,935G)
VolgraTheMoose: 101G (1.0%, 5,250G)
turbn: 100G (1.0%, 1,856G)
dantayystv: 100G (1.0%, 535G)
CosmicTactician: 100G (1.0%, 44,567G)

brown bets:
Evewho: 2,379G (23.7%, 4,665G)
ShintaroNayaka: 2,186G (21.7%, 2,186G)
DeathTaxesAndAnime: 1,413G (14.1%, 2,772G)
SkylerBunny: 1,014G (10.1%, 1,014G)
Theseawolf1: 1,000G (9.9%, 25,997G)
twelfthrootoftwo: 961G (9.6%, 1,886G)
BoneMiser: 600G (6.0%, 600G)
Shalloween: 500G (5.0%, 55,801G)

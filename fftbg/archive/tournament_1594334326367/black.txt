Player: !Black
Team: Black Team
Palettes: Black/Red



Evewho
Female
Taurus
76
41
Knight
Item
Parry
Concentrate
Swim

Slasher
Kaiser Plate
Mythril Helmet
Mythril Armor
Vanish Mantle

Armor Break, Shield Break, Magic Break, Justice Sword, Surging Sword
Potion, Echo Grass, Phoenix Down



D4rr1n
Female
Capricorn
64
53
Geomancer
Draw Out
Critical Quick
Magic Defense UP
Fly

Broad Sword
Kaiser Plate
Feather Hat
Silk Robe
Battle Boots

Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Asura, Koutetsu, Heaven's Cloud, Kiyomori



Hasterious
Male
Sagittarius
41
65
Ninja
Yin Yang Magic
Counter Tackle
Equip Knife
Retreat

Wizard Rod
Main Gauche
Twist Headband
Judo Outfit
Genji Gauntlet

Bomb, Sword
Blind, Poison, Life Drain, Silence Song, Foxbird, Dispel Magic, Sleep



CosmicTactician
Male
Capricorn
57
63
Squire
Item
Hamedo
Throw Item
Move+2

Diamond Sword
Escutcheon
Mythril Helmet
Platinum Armor
Defense Ring

Tickle, Wish, Scream
Hi-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Remedy

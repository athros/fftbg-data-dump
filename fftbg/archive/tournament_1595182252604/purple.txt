Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Scurg
Female
Sagittarius
66
56
Summoner
Black Magic
Earplug
Beastmaster
Move+1

Oak Staff

Flash Hat
Wizard Robe
Dracula Mantle

Shiva, Ifrit, Titan, Carbunkle, Salamander, Fairy
Fire, Bolt 2, Ice 2



Reddwind
Male
Scorpio
52
76
Knight
Talk Skill
Mana Shield
Monster Talk
Waterbreathing

Nagrarock
Gold Shield
Platinum Helmet
White Robe
Cherche

Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword, Surging Sword
Persuade, Death Sentence, Negotiate, Refute, Rehabilitate



ArchKnightX
Female
Capricorn
62
70
Archer
Summon Magic
Counter
Equip Polearm
Lava Walking

Musk Rod
Flame Shield
Holy Miter
Judo Outfit
Small Mantle

Charge+3, Charge+4, Charge+5
Moogle, Shiva, Ramuh, Ifrit, Salamander, Lich



Zetchryn
Female
Pisces
53
67
Summoner
Punch Art
Earplug
Magic Attack UP
Lava Walking

Thunder Rod

Leather Hat
Silk Robe
Jade Armlet

Moogle, Shiva, Ifrit, Carbunkle, Odin, Leviathan, Salamander, Silf, Fairy
Pummel, Purification

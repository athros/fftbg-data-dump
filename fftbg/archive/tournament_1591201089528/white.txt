Player: !White
Team: White Team
Palettes: White/Blue



ShintaroNayaka
Female
Scorpio
50
40
Time Mage
Elemental
Dragon Spirit
Equip Armor
Move-MP Up

Sage Staff

Black Hood
Bronze Armor
Small Mantle

Haste, Slow, Slow 2, Immobilize, Demi 2, Stabilize Time
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



SQUiDSQUARKLIN
Male
Taurus
46
52
Geomancer
Battle Skill
Damage Split
Beastmaster
Levitate

Asura Knife
Escutcheon
Flash Hat
Black Costume
Angel Ring

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Night Sword, Surging Sword



Angelomortis
Male
Virgo
80
51
Mediator
Elemental
Caution
Long Status
Levitate

Blaze Gun

Holy Miter
Light Robe
Bracer

Preach, Negotiate, Mimic Daravon, Refute
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



ExecutedGiraffe
Female
Taurus
50
45
Chemist
Jump
Arrow Guard
Defend
Swim

Blast Gun

Cachusha
Mythril Vest
Germinas Boots

Potion, X-Potion, Ether, Hi-Ether, Soft, Remedy, Phoenix Down
Level Jump5, Vertical Jump7

Player: !Red
Team: Red Team
Palettes: Red/Brown



NIghtdew14
Female
Cancer
63
57
Dancer
Battle Skill
Dragon Spirit
Secret Hunt
Move-MP Up

Persia

Black Hood
Judo Outfit
Magic Ring

Witch Hunt, Polka Polka, Last Dance
Mind Break, Dark Sword



Upvla
Male
Capricorn
58
55
Monk
Item
MA Save
Attack UP
Move-HP Up



Feather Hat
Earth Clothes
Small Mantle

Pummel, Wave Fist, Secret Fist, Purification
Potion, Hi-Potion, Ether, Antidote, Eye Drop, Phoenix Down



ALY327
Male
Scorpio
71
77
Ninja
Elemental
Auto Potion
Concentrate
Move-HP Up

Koga Knife
Orichalcum
Black Hood
Mythril Vest
Reflect Ring

Shuriken, Bomb, Knife
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind



Vin
Female
Pisces
53
73
Samurai
Black Magic
Speed Save
Short Status
Jump+1

Murasame

Mythril Helmet
Diamond Armor
Magic Gauntlet

Asura, Kikuichimoji
Fire 2, Fire 3, Ice 2

Final Bets: white - 6 bets for 5,507G (54.3%, x0.84); black - 8 bets for 4,642G (45.7%, x1.19)

white bets:
helpimabug: 2,950G (53.6%, 5,901G)
prince_rogers_nelson_: 1,509G (27.4%, 1,509G)
RaIshtar: 528G (9.6%, 528G)
ruleof5: 300G (5.4%, 11,074G)
datadrivenbot: 200G (3.6%, 38,802G)
DouglasDragonThePoet: 20G (0.4%, 2,478G)

black bets:
NIghtdew14: 1,500G (32.3%, 45,918G)
BirbBrainsBot: 1,000G (21.5%, 45,540G)
ForagerCats: 500G (10.8%, 4,271G)
randgridr: 488G (10.5%, 488G)
getthemoneyz: 486G (10.5%, 1,371,346G)
AllInBot: 260G (5.6%, 260G)
Jadusable2: 236G (5.1%, 236G)
douchetron: 172G (3.7%, 172G)

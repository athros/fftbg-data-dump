Player: !Black
Team: Black Team
Palettes: Black/Red



Prince Rogers Nelson
Female
Pisces
55
45
Archer
Steal
Counter
Martial Arts
Move+2


Mythril Shield
Leather Hat
Judo Outfit
Battle Boots

Charge+2, Charge+10
Steal Helmet, Steal Shield, Steal Accessory, Steal Status



Neerrm
Male
Sagittarius
72
49
Bard
Draw Out
Speed Save
Martial Arts
Waterwalking



Golden Hairpin
Mythril Vest
Defense Ring

Angel Song, Battle Song, Nameless Song, Diamond Blade, Sky Demon
Heaven's Cloud, Kiyomori, Muramasa



NovaKnight21
Male
Leo
61
47
Squire
Item
Absorb Used MP
Throw Item
Waterwalking

Ice Brand
Crystal Shield
Genji Helmet
Earth Clothes
Jade Armlet

Throw Stone, Heal, Tickle, Yell, Fury
Potion, Hi-Potion, Hi-Ether, Antidote, Remedy, Phoenix Down



Randgridr
Male
Pisces
66
60
Mediator
Sing
Auto Potion
Secret Hunt
Move+3

Papyrus Codex

Green Beret
Adaman Vest
Diamond Armlet

Death Sentence, Negotiate, Mimic Daravon, Refute
Battle Song, Hydra Pit

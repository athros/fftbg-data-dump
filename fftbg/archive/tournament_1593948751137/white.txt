Player: !White
Team: White Team
Palettes: White/Blue



Lastly
Female
Cancer
65
55
Calculator
White Magic
HP Restore
Defense UP
Fly

Ivory Rod

Holy Miter
Black Robe
Feather Boots

CT, Height, Prime Number, 4
Cure 3, Regen, Shell 2, Esuna



NIghtdew14
Female
Aquarius
51
79
Dancer
Punch Art
Mana Shield
Equip Gun
Jump+1

Bloody Strings

Triangle Hat
Brigandine
Power Wrist

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Nether Demon
Spin Fist, Secret Fist, Purification



VolgraTheMoose
Male
Aquarius
79
53
Chemist
Sing
Earplug
Equip Armor
Lava Walking

Blind Knife

Genji Helmet
Chain Mail
N-Kai Armlet

Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Life Song, Cheer Song



Reddwind
Male
Sagittarius
55
79
Knight
White Magic
Parry
Dual Wield
Move+3

Ragnarok
Defender
Circlet
Platinum Armor
Elf Mantle

Shield Break, Magic Break, Stasis Sword, Justice Sword
Cure, Cure 2, Cure 3, Raise, Reraise, Protect, Shell, Wall, Esuna

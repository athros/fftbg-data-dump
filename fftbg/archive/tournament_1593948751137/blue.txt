Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Actual JP
Female
Scorpio
69
73
Wizard
Throw
PA Save
Equip Sword
Ignore Terrain

Sleep Sword

Feather Hat
Judo Outfit
Power Wrist

Fire 4, Bolt, Bolt 2, Ice, Ice 2, Death
Shuriken, Bomb, Stick



YaBoy125
Male
Aquarius
76
78
Knight
Black Magic
MP Restore
Equip Armor
Waterwalking

Slasher
Escutcheon
Holy Miter
Reflect Mail
Sprint Shoes

Head Break, Shield Break, Weapon Break, Mind Break, Stasis Sword, Justice Sword, Night Sword
Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Frog, Flare



SephDarkheart
Male
Cancer
47
73
Archer
Item
Counter
Halve MP
Levitate

Silver Bow

Flash Hat
Rubber Costume
Sprint Shoes

Charge+5, Charge+7
Potion, Echo Grass, Soft, Phoenix Down



Serperemagus
Male
Sagittarius
58
49
Mime

Dragon Spirit
Attack UP
Teleport



Feather Hat
Secret Clothes
Reflect Ring

Mimic


Player: !White
Team: White Team
Palettes: White/Blue



Ferroniss
Female
Sagittarius
58
55
Mediator
Charge
Damage Split
Equip Polearm
Lava Walking

Iron Fan

Green Beret
Black Robe
Spike Shoes

Persuade, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Charge+1, Charge+7



Bruubarg
Female
Aries
69
70
Calculator
Black Magic
Distribute
Magic Attack UP
Jump+3

Cypress Rod

Triangle Hat
Light Robe
Spike Shoes

Height, Prime Number, 4
Fire, Bolt 3, Bolt 4, Ice



Benticore
Monster
Virgo
58
72
Wyvern










LeepingJJ
Male
Pisces
73
48
Knight
Draw Out
Counter Flood
Equip Gun
Waterwalking

Papyrus Codex
Buckler
Genji Helmet
Crystal Mail
Battle Boots

Weapon Break, Speed Break, Mind Break, Stasis Sword
Koutetsu, Heaven's Cloud

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Skillomono
Male
Sagittarius
44
71
Geomancer
Draw Out
Damage Split
Beastmaster
Lava Walking

Slasher
Flame Shield
Red Hood
Mythril Vest
Sprint Shoes

Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



Eudes89
Female
Capricorn
67
78
Oracle
Battle Skill
Counter Tackle
Equip Axe
Lava Walking

Wizard Staff

Flash Hat
Chain Vest
Feather Boots

Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze
Shield Break, Magic Break, Speed Break, Dark Sword, Surging Sword



Lowlf
Male
Sagittarius
42
79
Lancer
Black Magic
Counter Magic
Secret Hunt
Waterbreathing

Javelin
Diamond Shield
Mythril Helmet
Black Robe
Magic Gauntlet

Level Jump5, Vertical Jump3
Fire 2, Ice, Ice 4



Lawnboxer
Female
Virgo
58
48
Mediator
Black Magic
Parry
Beastmaster
Swim

Papyrus Codex

Twist Headband
Light Robe
Defense Armlet

Persuade, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Fire 2, Bolt, Bolt 3, Death

Player: !Green
Team: Green Team
Palettes: Green/White



Maldoree
Female
Sagittarius
77
43
Mediator
Black Magic
Catch
Equip Gun
Teleport 2

Fairy Harp

Red Hood
Wizard Robe
Genji Gauntlet

Invitation, Persuade, Threaten, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Fire, Fire 2, Bolt 3, Ice 2, Death



MiseryKoss
Monster
Cancer
66
73
Steel Giant










Slickdang
Male
Capricorn
69
73
Thief
Item
Absorb Used MP
Throw Item
Fly

Hidden Knife

Golden Hairpin
Earth Clothes
Feather Boots

Steal Helmet, Steal Shield, Steal Weapon
Potion, Ether, Hi-Ether, Eye Drop, Phoenix Down



Nickyfive
Female
Capricorn
68
65
Summoner
Talk Skill
Earplug
Defend
Jump+2

Ice Rod

Twist Headband
Black Robe
Feather Boots

Moogle, Shiva, Titan, Golem, Bahamut, Fairy
Preach, Refute

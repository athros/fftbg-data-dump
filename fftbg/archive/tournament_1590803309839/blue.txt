Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TheGuesty
Male
Capricorn
47
74
Priest
Draw Out
Dragon Spirit
Equip Knife
Move+3

Thunder Rod

Black Hood
Chameleon Robe
Feather Boots

Raise, Reraise, Regen, Protect 2, Shell 2, Esuna
Koutetsu, Kikuichimoji



Dirtpigishbrown
Female
Pisces
54
80
Chemist
Time Magic
Auto Potion
Sicken
Jump+2

Mythril Gun

Red Hood
Judo Outfit
Dracula Mantle

Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down
Haste, Float, Demi 2



H00z0r
Male
Taurus
49
63
Summoner
White Magic
Earplug
Defense UP
Waterwalking

Healing Staff

Leather Hat
Brigandine
Battle Boots

Ramuh, Bahamut, Odin, Salamander, Silf
Cure 2, Regen, Protect, Shell 2



Moonliquor
Female
Virgo
76
60
Thief
Charge
Counter
Equip Knife
Move-MP Up

Wizard Rod

Headgear
Secret Clothes
Germinas Boots

Steal Heart, Steal Weapon, Steal Status, Leg Aim
Charge+3, Charge+7, Charge+10

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Meta Five
Female
Aries
61
47
Samurai
Time Magic
MP Restore
Halve MP
Move-HP Up

Muramasa

Mythril Helmet
Wizard Robe
Magic Gauntlet

Asura, Koutetsu, Kiyomori, Muramasa, Kikuichimoji, Chirijiraden
Float, Demi 2



J2DaBibbles
Male
Capricorn
75
70
Monk
Jump
Parry
Equip Shield
Teleport


Diamond Shield
Twist Headband
Brigandine
Small Mantle

Wave Fist, Chakra, Revive
Level Jump2, Vertical Jump7



Hydroshade
Female
Aquarius
48
72
Archer
Item
Hamedo
Equip Polearm
Retreat

Cashmere
Gold Shield
Triangle Hat
Mythril Vest
Cursed Ring

Charge+3, Charge+4, Charge+7, Charge+20
Potion, Ether, Eye Drop, Remedy, Phoenix Down



Coralreeferz
Female
Libra
55
51
Geomancer
Draw Out
Counter Tackle
Short Status
Move-MP Up

Giant Axe
Genji Shield
Golden Hairpin
Wizard Robe
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Lava Ball
Asura, Kiyomori

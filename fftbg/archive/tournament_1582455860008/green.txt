Player: !Green
Team: Green Team
Palettes: Green/White



Saiko707
Monster
Gemini
52
61
Serpentarius










Z32o
Female
Gemini
64
70
Geomancer
Item
Damage Split
Secret Hunt
Jump+2

Broad Sword
Platinum Shield
Headgear
Adaman Vest
Bracer

Pitfall, Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Hi-Potion, Eye Drop, Echo Grass, Holy Water, Phoenix Down



Alaylle
Female
Aquarius
46
37
Oracle
Draw Out
Sunken State
Maintenance
Waterbreathing

Whale Whisker

Golden Hairpin
Linen Robe
Reflect Ring

Life Drain, Pray Faith, Silence Song, Dispel Magic
Asura, Heaven's Cloud, Kiyomori, Muramasa



SkyridgeZero
Male
Leo
48
39
Lancer
Throw
Regenerator
Doublehand
Move+2

Holy Lance

Mythril Helmet
Wizard Robe
Small Mantle

Level Jump4, Vertical Jump2
Bomb, Ninja Sword

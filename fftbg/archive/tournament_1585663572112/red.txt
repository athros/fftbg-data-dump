Player: !Red
Team: Red Team
Palettes: Red/Brown



Holyonline
Male
Pisces
52
63
Samurai
Summon Magic
Distribute
Short Status
Jump+3

Murasame

Platinum Helmet
Crystal Mail
Defense Armlet

Asura, Bizen Boat, Kiyomori
Moogle, Ifrit, Carbunkle, Odin, Salamander, Fairy, Lich



Draconis345
Female
Aquarius
66
68
Summoner
Time Magic
Dragon Spirit
Dual Wield
Retreat

Rod
Rod
Twist Headband
White Robe
Spike Shoes

Moogle, Titan, Carbunkle, Odin, Silf, Lich
Slow 2, Stop, Immobilize, Reflect, Quick



IcePattern
Male
Aquarius
47
52
Monk
White Magic
Distribute
Attack UP
Ignore Height



Triangle Hat
Wizard Outfit
Defense Armlet

Pummel, Earth Slash, Purification
Cure 2, Raise, Protect, Protect 2, Shell, Wall, Esuna



Numbersborne
Female
Pisces
61
47
Knight
Time Magic
Earplug
Defend
Ignore Terrain

Battle Axe
Genji Shield
Cross Helmet
Silk Robe
Leather Mantle

Head Break, Magic Break, Mind Break, Justice Sword, Night Sword
Slow 2, Stop, Stabilize Time

Player: !Black
Team: Black Team
Palettes: Black/Red



ThanatosXRagnarok
Female
Capricorn
79
71
Dancer
White Magic
PA Save
Equip Polearm
Waterbreathing

Cypress Rod

Black Hood
Black Costume
Rubber Shoes

Witch Hunt, Slow Dance, Nameless Dance, Last Dance
Cure 3, Cure 4, Raise, Raise 2, Reraise, Shell, Shell 2, Esuna



Izlude
Male
Gemini
75
61
Mime

Distribute
Magic Attack UP
Lava Walking



Triangle Hat
Judo Outfit
Bracer

Mimic




Frayda
Female
Pisces
72
50
Ninja
Basic Skill
Faith Up
Concentrate
Waterbreathing

Sasuke Knife
Flame Whip
Black Hood
Judo Outfit
Magic Gauntlet

Shuriken, Knife
Accumulate, Dash, Throw Stone, Heal, Yell, Cheer Up, Fury, Wish, Scream



Flossy
Female
Taurus
51
49
Dancer
Battle Skill
Arrow Guard
Attack UP
Ignore Terrain

Ryozan Silk

Headgear
Judo Outfit
Feather Boots

Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon, Dragon Pit
Shield Break, Magic Break, Speed Break, Power Break

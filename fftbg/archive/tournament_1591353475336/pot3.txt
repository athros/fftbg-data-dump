Final Bets: white - 5 bets for 5,550G (67.8%, x0.48); black - 8 bets for 2,640G (32.2%, x2.10)

white bets:
NovaKnight21: 3,452G (62.2%, 3,452G)
thaetreis: 1,498G (27.0%, 1,498G)
joewcarson: 300G (5.4%, 16,813G)
serperemagus: 200G (3.6%, 850G)
datadrivenbot: 100G (1.8%, 32,994G)

black bets:
BirbBrainsBot: 1,000G (37.9%, 72,646G)
CassiePhoenix: 376G (14.2%, 376G)
Nelrith: 356G (13.5%, 1,781G)
gogofromtogo: 340G (12.9%, 340G)
DeathTaxesAndAnime: 200G (7.6%, 27,299G)
SQUiDSQUARKLIN: 200G (7.6%, 5,689G)
outer_monologue: 100G (3.8%, 759G)
getthemoneyz: 68G (2.6%, 822,113G)

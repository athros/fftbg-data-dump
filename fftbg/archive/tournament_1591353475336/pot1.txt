Final Bets: red - 8 bets for 3,331G (33.4%, x1.99); blue - 10 bets for 6,635G (66.6%, x0.50)

red bets:
BirbBrainsBot: 1,000G (30.0%, 69,967G)
NovaKnight21: 684G (20.5%, 684G)
just_here2: 500G (15.0%, 5,870G)
thaetreis: 455G (13.7%, 455G)
Nelrith: 392G (11.8%, 785G)
Evewho: 100G (3.0%, 4,401G)
outer_monologue: 100G (3.0%, 491G)
fluffskull: 100G (3.0%, 727G)

blue bets:
gorgewall: 2,106G (31.7%, 2,106G)
rechaun: 1,223G (18.4%, 1,223G)
gogofromtogo: 1,070G (16.1%, 1,070G)
CassiePhoenix: 848G (12.8%, 848G)
Sairentozon7: 552G (8.3%, 552G)
twelfthrootoftwo: 300G (4.5%, 2,847G)
DeathTaxesAndAnime: 200G (3.0%, 27,362G)
KyleWonToLiveForever: 136G (2.0%, 136G)
ApplesauceBoss: 100G (1.5%, 1,737G)
datadrivenbot: 100G (1.5%, 33,025G)

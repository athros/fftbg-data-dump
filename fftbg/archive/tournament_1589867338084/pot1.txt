Final Bets: red - 13 bets for 7,692G (41.6%, x1.40); blue - 19 bets for 10,790G (58.4%, x0.71)

red bets:
BuffaloCrunch: 1,382G (18.0%, 1,382G)
Kalpho: 1,337G (17.4%, 2,674G)
ungabunga_bot: 1,000G (13.0%, 506,352G)
BirbBrainsBot: 1,000G (13.0%, 173,008G)
Lolprinze: 696G (9.0%, 696G)
sinnyil2: 607G (7.9%, 1,214G)
killth3kid: 600G (7.8%, 9,690G)
MalakiGenesys: 409G (5.3%, 409G)
skillomono: 300G (3.9%, 2,329G)
itsZaes: 111G (1.4%, 732G)
Firesheath: 100G (1.3%, 304G)
getthemoneyz: 100G (1.3%, 685,093G)
CT_5_Holy: 50G (0.7%, 1,162G)

blue bets:
Evewho: 1,747G (16.2%, 1,747G)
leakimiko: 1,111G (10.3%, 39,796G)
HaateXIII: 1,068G (9.9%, 1,068G)
GladiatorLupe: 1,000G (9.3%, 8,342G)
EnemyController: 1,000G (9.3%, 276,918G)
nifboy: 645G (6.0%, 645G)
Laserman1000: 600G (5.6%, 6,300G)
mannequ1n: 588G (5.4%, 2,353G)
waterwatereverywhere: 500G (4.6%, 3,679G)
vorap: 400G (3.7%, 59,419G)
DLJuggernaut: 350G (3.2%, 2,554G)
CorpusCav: 345G (3.2%, 3,745G)
CapnChaos12: 300G (2.8%, 1,634G)
Estan_AD: 300G (2.8%, 300G)
neocarbuncle: 265G (2.5%, 265G)
gorgewall: 201G (1.9%, 13,770G)
Communis: 150G (1.4%, 2,223G)
JLinkletter: 120G (1.1%, 120G)
datadrivenbot: 100G (0.9%, 18,686G)

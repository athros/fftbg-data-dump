Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lolprinze
Monster
Cancer
75
68
Red Panther










Galkife
Male
Leo
80
68
Priest
Basic Skill
Counter
Equip Shield
Lava Walking

Oak Staff
Round Shield
Headgear
Wizard Robe
Genji Gauntlet

Cure 4, Raise, Raise 2, Reraise, Protect, Esuna
Dash, Heal, Tickle, Yell, Fury



Strifu
Female
Virgo
43
74
Squire
Black Magic
Damage Split
Short Charge
Waterwalking

Bow Gun
Mythril Shield
Green Beret
Brigandine
108 Gems

Heal, Yell, Cheer Up, Wish
Frog



CT 5 Holy
Male
Gemini
45
68
Ninja
Sing
Caution
Maintenance
Fly

Assassin Dagger
Air Knife
Triangle Hat
Mystic Vest
Dracula Mantle

Shuriken, Spear, Dictionary
Life Song, Space Storage, Sky Demon

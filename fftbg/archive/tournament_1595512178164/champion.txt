Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



VolgraTheMoose
Male
Aquarius
46
66
Lancer
Talk Skill
MA Save
Secret Hunt
Jump+3

Spear
Bronze Shield
Genji Helmet
Platinum Armor
Dracula Mantle

Level Jump3, Vertical Jump8
Invitation, Persuade, Death Sentence, Refute, Rehabilitate



Nsm013
Male
Taurus
56
52
Summoner
Jump
Abandon
Defense UP
Ignore Height

Rainbow Staff

Green Beret
Robe of Lords
Feather Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Salamander, Lich
Level Jump3, Vertical Jump7



Cryptopsy70
Female
Scorpio
46
77
Samurai
Steal
Absorb Used MP
Concentrate
Move+2

Kiyomori

Gold Helmet
Gold Armor
Magic Ring

Asura, Koutetsu, Murasame
Steal Armor, Steal Accessory, Steal Status



Reddwind
Female
Cancer
40
63
Geomancer
Basic Skill
Counter Flood
Magic Defense UP
Move+1

Materia Blade
Diamond Shield
Barette
Judo Outfit
Germinas Boots

Hallowed Ground, Local Quake, Sand Storm, Blizzard, Lava Ball
Heal, Tickle, Yell

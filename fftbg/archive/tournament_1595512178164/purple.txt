Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SirAbcde
Male
Libra
46
63
Calculator
Byblos
MP Restore
Dual Wield
Waterbreathing

Thunder Rod
Poison Rod
Leather Hat
Linen Cuirass
Genji Gauntlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Grininda
Male
Virgo
63
75
Calculator
Bio
Counter Magic
Defend
Waterwalking

Ivory Rod

Headgear
Gold Armor
Small Mantle

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Roofiepops
Female
Aries
51
52
Calculator
Bio
MA Save
Short Charge
Jump+1

Rod
Ice Shield
Circlet
Light Robe
Power Wrist

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Evewho
Female
Libra
79
80
Calculator
Demon Skill
Distribute
Attack UP
Ignore Terrain

Cypress Rod

Platinum Helmet
White Robe
Dracula Mantle

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2

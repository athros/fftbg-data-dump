Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Female
Scorpio
54
72
Time Mage
Punch Art
Caution
Short Charge
Jump+3

White Staff

Headgear
Wizard Robe
Reflect Ring

Haste 2, Slow, Slow 2, Float, Reflect, Quick, Stabilize Time
Wave Fist, Earth Slash, Chakra



Chizad623
Male
Taurus
61
67
Geomancer
Talk Skill
Parry
Equip Gun
Lava Walking

Papyrus Codex
Mythril Shield
Black Hood
Chameleon Robe
Defense Ring

Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Insult, Negotiate, Mimic Daravon, Refute



Maximumcrit
Female
Cancer
47
74
Geomancer
Punch Art
Meatbone Slash
Sicken
Move+2

Ice Brand
Hero Shield
Holy Miter
Black Robe
108 Gems

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Revive



Ssfsx17
Female
Aquarius
51
49
Time Mage
Charge
Meatbone Slash
Equip Armor
Waterbreathing

Battle Bamboo

Leather Helmet
Crystal Mail
Battle Boots

Haste 2, Slow, Float, Quick, Meteor
Charge+1, Charge+2, Charge+7, Charge+10

Player: !Green
Team: Green Team
Palettes: Green/White



NicoSavoy
Female
Cancer
80
77
Mediator
Battle Skill
Auto Potion
Magic Attack UP
Move+3

Bestiary

Triangle Hat
Light Robe
Red Shoes

Preach, Solution, Death Sentence, Insult, Refute
Armor Break, Shield Break, Weapon Break, Magic Break, Night Sword



Holdenmagronik
Monster
Leo
64
54
Tiamat










TheseJeans
Male
Leo
76
70
Geomancer
White Magic
Parry
Attack UP
Ignore Height

Long Sword
Escutcheon
Headgear
Light Robe
Angel Ring

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Cure, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Wall, Esuna, Holy



Wooplestein
Male
Sagittarius
69
42
Samurai
Jump
Faith Save
Defend
Jump+3

Holy Lance

Diamond Helmet
Wizard Robe
Bracer

Koutetsu, Murasame, Heaven's Cloud
Level Jump8, Vertical Jump8

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Grininda
Male
Gemini
76
67
Archer
Elemental
HP Restore
Doublehand
Lava Walking

Long Bow

Feather Hat
Wizard Outfit
Diamond Armlet

Charge+2, Charge+3, Charge+7, Charge+20
Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



UmaiJam
Female
Aries
75
63
Summoner
Dance
Faith Save
Short Charge
Move+2

Wizard Rod

Red Hood
Wizard Robe
Red Shoes

Ramuh, Carbunkle, Silf
Polka Polka, Disillusion, Nameless Dance, Obsidian Blade



Coralreeferz
Female
Capricorn
41
75
Geomancer
Draw Out
Parry
Defense UP
Waterbreathing

Battle Axe
Flame Shield
Feather Hat
Brigandine
Magic Gauntlet

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Koutetsu, Kiyomori, Muramasa, Masamune



Farand
Female
Scorpio
67
64
Geomancer
Steal
Counter Tackle
Equip Axe
Jump+2

Gold Staff
Buckler
Red Hood
Linen Robe
Elf Mantle

Water Ball, Hallowed Ground, Local Quake, Sand Storm, Lava Ball
Gil Taking, Steal Shield, Steal Accessory, Arm Aim, Leg Aim

Player: !Red
Team: Red Team
Palettes: Red/Brown



Snkey
Male
Aries
57
79
Samurai
Summon Magic
Absorb Used MP
Short Charge
Waterbreathing

Bizen Boat

Diamond Helmet
Diamond Armor
Power Wrist

Asura, Bizen Boat
Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Odin, Fairy



WhiteDog420
Monster
Capricorn
45
44
Dragon










ANFz
Male
Aries
50
75
Geomancer
Sing
Earplug
Beastmaster
Ignore Terrain

Hydra Bag
Platinum Shield
Golden Hairpin
Linen Robe
Genji Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Battle Song, Diamond Blade



Shalloween
Female
Libra
65
66
Summoner
Steal
Speed Save
Short Charge
Jump+1

Healing Staff

Green Beret
Silk Robe
Spike Shoes

Ramuh, Ifrit, Golem, Carbunkle, Salamander, Silf
Steal Armor, Steal Shield, Arm Aim, Leg Aim

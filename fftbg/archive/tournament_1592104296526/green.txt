Player: !Green
Team: Green Team
Palettes: Green/White



Electric Algus
Female
Virgo
64
61
Squire
Black Magic
Abandon
Maintenance
Waterwalking

Battle Axe
Genji Shield
Circlet
Chain Mail
Small Mantle

Dash, Tickle, Yell, Wish, Scream
Fire 4, Ice, Ice 2, Ice 3, Ice 4, Empower, Death



Eudes89
Female
Aries
74
80
Time Mage
Yin Yang Magic
Meatbone Slash
Short Charge
Ignore Terrain

Mace of Zeus

Twist Headband
White Robe
Defense Ring

Slow, Slow 2, Stop
Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy



Kaelsun
Monster
Taurus
47
62
Behemoth










Lawnboxer
Female
Sagittarius
48
62
Priest
Dance
Damage Split
Equip Knife
Swim

Assassin Dagger

Headgear
Wizard Robe
Rubber Shoes

Cure 2, Raise, Regen, Protect, Protect 2, Wall, Esuna, Holy
Wiznaibus, Slow Dance, Polka Polka, Dragon Pit

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



OneHundredFists
Male
Pisces
65
51
Ninja
Item
Dragon Spirit
Attack UP
Waterwalking

Air Knife
Main Gauche
Black Hood
Leather Outfit
Spike Shoes

Knife
Potion, Antidote, Remedy, Phoenix Down



NightwolfXVI
Male
Leo
55
74
Ninja
Charge
Arrow Guard
Equip Shield
Jump+3

Spell Edge
Mythril Shield
Green Beret
Mythril Vest
Angel Ring

Shuriken, Staff
Charge+1, Charge+7



Deathmaker06
Male
Cancer
54
79
Lancer
Punch Art
Critical Quick
Equip Polearm
Levitate

Cashmere
Aegis Shield
Iron Helmet
Crystal Mail
Genji Gauntlet

Level Jump5, Vertical Jump8
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive



Davarian
Female
Pisces
61
57
Summoner
Time Magic
Blade Grasp
Equip Sword
Swim

Diamond Sword

Black Hood
Linen Robe
Defense Ring

Moogle, Titan, Carbunkle, Leviathan, Fairy, Lich
Slow, Slow 2, Stop, Reflect, Demi, Stabilize Time

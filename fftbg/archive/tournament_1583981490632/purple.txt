Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ranmilia
Male
Libra
77
75
Knight
Item
Damage Split
Equip Shield
Swim

Save the Queen
Bronze Shield
Bronze Helmet
Gold Armor
Rubber Shoes

Head Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Surging Sword
Potion, X-Potion, Hi-Ether, Antidote, Soft



Mustafio Belluccini
Male
Sagittarius
58
44
Mime

Absorb Used MP
Defend
Move+2



Flash Hat
Black Costume
Diamond Armlet

Mimic




Kellios11
Male
Scorpio
47
43
Archer
Sing
Parry
Concentrate
Teleport

Stone Gun
Hero Shield
Holy Miter
Brigandine
Defense Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+10, Charge+20
Life Song, Nameless Song, Hydra Pit



MantisFinch
Female
Aries
72
71
Time Mage
Jump
Parry
Sicken
Move-MP Up

Gold Staff

Triangle Hat
Wizard Outfit
Vanish Mantle

Haste, Float, Demi, Galaxy Stop
Level Jump5, Vertical Jump6

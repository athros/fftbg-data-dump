Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Smegma Sorcerer
Male
Leo
64
56
Ninja
Sing
HP Restore
Maintenance
Ignore Terrain

Hidden Knife
Spell Edge
Holy Miter
Brigandine
Genji Gauntlet

Shuriken
Magic Song, Nameless Song, Last Song



HaplessOne
Female
Aries
68
66
Thief
Punch Art
Counter Magic
Halve MP
Move+1

Dagger

Headgear
Mystic Vest
Feather Mantle

Arm Aim
Spin Fist, Purification, Revive



Tithonus
Female
Taurus
73
60
Mime

Hamedo
Equip Armor
Move-HP Up



Platinum Helmet
Platinum Armor
108 Gems

Mimic




KSops
Female
Taurus
79
61
Priest
Yin Yang Magic
Faith Up
Equip Polearm
Swim

Musk Rod

Holy Miter
Silk Robe
Angel Ring

Cure, Cure 2, Raise, Raise 2, Reraise, Regen, Protect 2, Shell, Shell 2, Esuna
Poison, Spell Absorb, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Paralyze

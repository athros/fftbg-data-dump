Final Bets: green - 9 bets for 2,840G (54.6%, x0.83); yellow - 7 bets for 2,366G (45.4%, x1.20)

green bets:
R_Raynos: 640G (22.5%, 640G)
megaman2202: 500G (17.6%, 2,630G)
reinoe: 500G (17.6%, 8,404G)
Rislyeu: 400G (14.1%, 10,103G)
Cataphract116: 300G (10.6%, 19,527G)
L2_Sentinel: 200G (7.0%, 30,492G)
nifboy: 100G (3.5%, 7,560G)
datadrivenbot: 100G (3.5%, 13,054G)
ungabunga_bot: 100G (3.5%, 339,360G)

yellow bets:
BirbBrainsBot: 1,000G (42.3%, 38,489G)
getthemoneyz: 528G (22.3%, 621,251G)
NovaKnight21: 400G (16.9%, 2,586G)
sukotsuto: 200G (8.5%, 1,417G)
Practice_Pad: 128G (5.4%, 128G)
bahamutlagooon: 60G (2.5%, 171G)
DrAntiSocial: 50G (2.1%, 8,512G)

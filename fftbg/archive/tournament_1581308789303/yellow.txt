Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Neetneph
Male
Libra
70
47
Mime

Counter Flood
Martial Arts
Move+2



Genji Helmet
Chain Vest
Leather Mantle

Mimic




Scotobot
Monster
Aquarius
74
55
Great Malboro










SneakyFingerz
Male
Gemini
46
63
Oracle
Time Magic
Counter
Martial Arts
Ignore Terrain



Flash Hat
Wizard Robe
Leather Mantle

Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep
Haste, Stop, Reflect, Quick, Stabilize Time, Meteor



WireLord
Female
Taurus
67
63
Priest
Summon Magic
Mana Shield
Short Charge
Move+1

Flame Whip

Thief Hat
Silk Robe
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Reraise, Wall
Moogle, Ifrit, Titan, Leviathan, Lich, Cyclops

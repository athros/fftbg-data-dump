Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dtgalganski
Male
Leo
62
47
Archer
Sing
Parry
Equip Armor
Move+1

Ultimus Bow

Mythril Helmet
Reflect Mail
Magic Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+10, Charge+20
Life Song, Battle Song, Nameless Song, Diamond Blade, Space Storage



ImXT
Monster
Aquarius
54
64
Red Dragon










MillionGhosts
Male
Gemini
77
51
Knight
Black Magic
Mana Shield
Equip Armor
Move-MP Up

Diamond Sword
Kaiser Plate
Twist Headband
Chain Vest
Chantage

Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword
Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 2, Ice 4



CaptainAdmiralSPATULA
Male
Taurus
68
72
Squire
Steal
Meatbone Slash
Equip Polearm
Jump+3

Blind Knife

Golden Hairpin
Linen Cuirass
Genji Gauntlet

Accumulate, Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish
Steal Shield

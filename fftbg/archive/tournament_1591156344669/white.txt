Player: !White
Team: White Team
Palettes: White/Blue



SkylerBunny
Female
Aries
46
65
Lancer
Summon Magic
Counter Magic
Defend
Jump+3

Mythril Spear
Diamond Shield
Platinum Helmet
Chain Mail
Jade Armlet

Level Jump8, Vertical Jump8
Moogle, Ramuh, Silf, Fairy



EnemyController
Female
Leo
60
74
Mime

Speed Save
Equip Armor
Move-MP Up



Flash Hat
Light Robe
Germinas Boots

Mimic




GladiatorLupe
Female
Pisces
80
72
Monk
Summon Magic
Earplug
Attack UP
Jump+2



Golden Hairpin
Wizard Outfit
Sprint Shoes

Purification, Chakra, Revive
Moogle, Titan, Carbunkle, Leviathan, Silf



Setdevildog
Female
Leo
79
52
Oracle
White Magic
Auto Potion
Attack UP
Retreat

Battle Bamboo

Green Beret
Light Robe
Genji Gauntlet

Blind, Poison, Foxbird, Dispel Magic, Paralyze
Cure, Cure 2, Cure 3, Cure 4, Protect 2, Shell, Shell 2, Wall, Esuna, Holy

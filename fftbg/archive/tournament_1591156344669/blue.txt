Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lythe Caraker
Female
Virgo
73
45
Geomancer
Black Magic
MP Restore
Long Status
Retreat

Rune Blade
Flame Shield
Leather Hat
Robe of Lords
Power Wrist

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Fire 4, Bolt 3, Ice, Flare



Error72
Male
Gemini
55
39
Archer
Draw Out
Regenerator
Doublehand
Waterbreathing

Yoichi Bow

Leather Hat
Earth Clothes
Battle Boots

Charge+1
Asura, Koutetsu, Murasame



Aldrammech
Female
Aquarius
44
53
Summoner
Punch Art
HP Restore
Martial Arts
Lava Walking



Thief Hat
Silk Robe
Battle Boots

Moogle, Ramuh, Salamander, Fairy
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive



Skillomono
Male
Gemini
51
44
Archer
Elemental
Dragon Spirit
Magic Defense UP
Ignore Terrain

Romanda Gun
Hero Shield
Cachusha
Brigandine
Battle Boots

Charge+4, Charge+7, Charge+10
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind

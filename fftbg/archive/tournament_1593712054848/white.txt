Player: !White
Team: White Team
Palettes: White/Blue



Lythe Caraker
Female
Scorpio
74
72
Chemist
Steal
MA Save
Magic Attack UP
Move-HP Up

Blast Gun

Flash Hat
Clothes
Rubber Shoes

Potion, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down
Gil Taking, Steal Heart



Actual JP
Female
Aquarius
78
76
Dancer
Summon Magic
MA Save
Beastmaster
Teleport 2

Persia

Green Beret
Mythril Vest
Elf Mantle

Witch Hunt, Polka Polka, Disillusion, Obsidian Blade, Void Storage, Dragon Pit
Moogle, Ramuh, Ifrit, Carbunkle, Leviathan



O Heyno
Female
Cancer
49
38
Chemist
Steal
PA Save
Sicken
Waterbreathing

Cultist Dagger

Black Hood
Wizard Outfit
Elf Mantle

Potion, Hi-Potion, Ether, Echo Grass, Remedy, Phoenix Down
Steal Heart, Steal Status



EnemyController
Female
Gemini
46
44
Ninja
Yin Yang Magic
Mana Shield
Equip Sword
Move+1

Diamond Sword
Broad Sword
Golden Hairpin
Wizard Outfit
Feather Mantle

Shuriken, Knife
Spell Absorb, Silence Song, Blind Rage, Foxbird, Dispel Magic

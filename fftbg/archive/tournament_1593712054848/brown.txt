Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ZCKaiser
Female
Leo
56
50
Oracle
Basic Skill
Caution
Concentrate
Move+1

Octagon Rod

Triangle Hat
Chain Vest
Dracula Mantle

Life Drain, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze
Accumulate, Heal



Rytor
Female
Virgo
43
45
Dancer
Basic Skill
Meatbone Slash
Equip Knife
Waterwalking

Dragon Rod

Leather Hat
Adaman Vest
Jade Armlet

Slow Dance, Polka Polka, Disillusion, Last Dance, Dragon Pit
Dash, Heal, Fury



PoroTact
Female
Scorpio
59
50
Squire
Summon Magic
Parry
Equip Sword
Levitate

Long Sword
Diamond Shield
Holy Miter
Clothes
Bracer

Accumulate, Cheer Up, Fury
Moogle, Bahamut, Odin



NotBalrog
Male
Leo
76
61
Wizard
Punch Art
Counter Tackle
Equip Shield
Waterwalking

Blind Knife
Gold Shield
Twist Headband
Clothes
Angel Ring

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Frog
Spin Fist, Secret Fist, Purification, Chakra, Revive

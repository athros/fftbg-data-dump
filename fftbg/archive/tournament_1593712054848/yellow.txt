Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Pplvee1
Female
Pisces
55
70
Lancer
Yin Yang Magic
Brave Save
Defend
Jump+2

Octagon Rod
Escutcheon
Barbuta
Linen Cuirass
Germinas Boots

Level Jump2, Vertical Jump8
Life Drain, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify



Lastly
Female
Libra
74
75
Calculator
Yin Yang Magic
Dragon Spirit
Short Status
Fly

Battle Folio

Red Hood
Light Robe
Vanish Mantle

CT, Prime Number, 4, 3
Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy



Safira125
Male
Taurus
38
79
Oracle
Sing
Abandon
Equip Armor
Move-MP Up

Bestiary

Leather Hat
Chain Mail
Chantage

Blind, Spell Absorb, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Sleep
Cheer Song, Battle Song, Nameless Song



ArashiKurobara
Male
Aquarius
79
54
Ninja
Talk Skill
Counter
Equip Armor
Move+3

Sasuke Knife
Koga Knife
Black Hood
Diamond Armor
Power Wrist

Shuriken, Sword
Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon

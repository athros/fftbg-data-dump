Player: !White
Team: White Team
Palettes: White/Blue



Baron Von Scrub
Female
Aries
45
69
Thief
Summon Magic
PA Save
Attack UP
Levitate

Diamond Sword

Barette
Wizard Outfit
Diamond Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Status
Moogle, Shiva, Ramuh, Ifrit, Salamander



Banjooie
Male
Taurus
68
56
Thief
Basic Skill
Auto Potion
Secret Hunt
Teleport

Blind Knife

Holy Miter
Brigandine
N-Kai Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Accessory
Tickle, Yell



Ar Tactic
Male
Scorpio
61
43
Time Mage
Yin Yang Magic
Sunken State
Defense UP
Move+2

Healing Staff

Headgear
Chameleon Robe
Magic Ring

Haste, Slow, Slow 2, Stop, Float, Quick, Demi 2
Poison, Pray Faith, Blind Rage, Dispel Magic, Sleep



Bruubarg
Female
Scorpio
73
58
Monk
Yin Yang Magic
Counter Flood
Magic Attack UP
Waterbreathing



Red Hood
Clothes
Feather Mantle

Pummel, Secret Fist, Purification, Revive
Blind, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy

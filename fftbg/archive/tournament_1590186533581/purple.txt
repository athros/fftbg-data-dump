Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Apeljoose
Female
Sagittarius
80
79
Mediator
Elemental
Caution
Martial Arts
Jump+3

Cute Bag

Green Beret
Chain Vest
Germinas Boots

Praise, Threaten, Refute
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Dymntd
Female
Leo
74
60
Lancer
Draw Out
Counter Flood
Short Charge
Jump+3

Obelisk
Mythril Shield
Bronze Helmet
White Robe
Genji Gauntlet

Level Jump8, Vertical Jump8
Muramasa



Seawalker777
Female
Scorpio
57
62
Samurai
Dance
PA Save
Doublehand
Waterwalking

Bizen Boat

Cross Helmet
Carabini Mail
Battle Boots

Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Last Dance, Void Storage, Dragon Pit



ExecutedGiraffe
Female
Taurus
43
82
Squire
Charge
Mana Shield
Equip Bow
Move-MP Up

Ultimus Bow

Headgear
Chain Mail
Leather Mantle

Dash, Heal, Yell, Cheer Up
Charge+5, Charge+7

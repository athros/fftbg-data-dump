Final Bets: black - 15 bets for 10,188G (42.3%, x1.37); champion - 25 bets for 13,910G (57.7%, x0.73)

black bets:
Vex_Novisk: 3,878G (38.1%, 7,756G)
Dymntd: 1,308G (12.8%, 1,308G)
BirbBrainsBot: 1,000G (9.8%, 123,121G)
aznwanderor: 807G (7.9%, 807G)
Aldrammech: 600G (5.9%, 600G)
HASTERIOUS: 571G (5.6%, 11,421G)
animefan112233: 500G (4.9%, 1,450G)
luminarii: 450G (4.4%, 19,796G)
CapnChaos12: 300G (2.9%, 3,658G)
d4rr1n: 224G (2.2%, 224G)
KasugaiRoastedPeas: 200G (2.0%, 615G)
FoeSquirrel: 100G (1.0%, 920G)
Apeljoose: 100G (1.0%, 487G)
E_Ballard: 100G (1.0%, 1,342G)
letdowncity: 50G (0.5%, 2,017G)

champion bets:
Grimmace45: 3,148G (22.6%, 20,990G)
Zbgs: 1,500G (10.8%, 48,942G)
Mesmaster: 1,000G (7.2%, 80,233G)
EnemyController: 1,000G (7.2%, 318,188G)
jakeduhhsnake: 974G (7.0%, 974G)
Baron_von_Scrub: 771G (5.5%, 771G)
getthemoneyz: 604G (4.3%, 700,218G)
bruubarg: 500G (3.6%, 3,000G)
HaateXIII: 500G (3.6%, 2,487G)
DLJuggernaut: 500G (3.6%, 1,645G)
Laserman1000: 500G (3.6%, 5,800G)
reinoe: 500G (3.6%, 16,522G)
Evewho: 412G (3.0%, 412G)
Lali_Lulelo: 380G (2.7%, 380G)
Seawalker777: 351G (2.5%, 351G)
slash_kamei: 243G (1.7%, 243G)
O_Heyno: 200G (1.4%, 2,067G)
earthbeard: 200G (1.4%, 200G)
serperemagus: 116G (0.8%, 116G)
MADTheta: 100G (0.7%, 780G)
datadrivenbot: 100G (0.7%, 22,134G)
Rintastic: 100G (0.7%, 2,331G)
FriendSquirrel: 100G (0.7%, 1,308G)
RunicMagus: 100G (0.7%, 32,994G)
roofiepops: 11G (0.1%, 9,360G)

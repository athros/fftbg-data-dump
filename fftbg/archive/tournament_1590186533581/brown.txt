Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Roofiepops
Female
Virgo
67
70
Chemist
Yin Yang Magic
MP Restore
Magic Defense UP
Retreat

Hydra Bag

Thief Hat
Wizard Outfit
Cursed Ring

Potion, Hi-Potion, Hi-Ether, Echo Grass, Phoenix Down
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Petrify



Aldrammech
Male
Gemini
43
68
Ninja
Yin Yang Magic
Dragon Spirit
Secret Hunt
Move+3

Scorpion Tail
Ninja Edge
Headgear
Mythril Vest
Power Wrist

Shuriken, Bomb
Poison, Pray Faith, Blind Rage, Paralyze, Sleep



ThePineappleSalesman
Female
Cancer
80
74
Calculator
Black Magic
Faith Save
Magic Attack UP
Teleport

Thunder Rod

Feather Hat
Linen Robe
Genji Gauntlet

CT, Height, 5
Bolt, Bolt 2, Bolt 3, Ice, Ice 3



MagicBottle
Male
Cancer
64
69
Mime

Dragon Spirit
Equip Shield
Ignore Height


Round Shield
Holy Miter
Mythril Vest
Magic Gauntlet

Mimic


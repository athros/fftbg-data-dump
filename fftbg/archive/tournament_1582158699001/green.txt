Player: !Green
Team: Green Team
Palettes: Green/White



Helpimabug
Female
Sagittarius
44
40
Ninja
White Magic
Damage Split
Attack UP
Teleport

Sasuke Knife
Flame Whip
Red Hood
Power Sleeve
Reflect Ring

Bomb, Stick
Cure 2, Cure 3, Raise 2, Regen, Shell 2



Anachronity
Female
Gemini
49
72
Samurai
Dance
Regenerator
Maintenance
Move-HP Up

Kiyomori

Platinum Helmet
Gold Armor
Magic Gauntlet

Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Disillusion, Obsidian Blade, Dragon Pit



Mithical9
Male
Scorpio
45
72
Wizard
Steal
Absorb Used MP
Magic Attack UP
Ignore Terrain

Rod

Flash Hat
Mystic Vest
Defense Ring

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Empower
Steal Heart, Steal Helmet, Steal Shield



GoAwayImBaitin
Male
Pisces
62
74
Geomancer
Basic Skill
Sunken State
Defense UP
Teleport

Slasher
Platinum Shield
Holy Miter
Light Robe
Defense Ring

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Heal, Tickle

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Xerisse
Monster
Cancer
53
72
Pisco Demon










Saithis1
Male
Libra
54
67
Wizard
Basic Skill
MA Save
Equip Polearm
Teleport

Musk Rod

Barette
Chameleon Robe
Defense Armlet

Fire, Fire 2, Bolt 4, Ice 3, Ice 4, Frog
Dash, Throw Stone, Heal, Cheer Up, Scream, Ultima



Diyahressem
Female
Leo
74
45
Summoner
Yin Yang Magic
Counter Tackle
Defense UP
Move+2

Thunder Rod

Thief Hat
Power Sleeve
108 Gems

Moogle, Ramuh, Carbunkle, Bahamut, Odin, Salamander
Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic



VulcanPanderp
Male
Aries
51
45
Chemist
Summon Magic
Regenerator
Concentrate
Lava Walking

Cute Bag

Red Hood
Judo Outfit
Elf Mantle

Potion, Hi-Potion, X-Potion, Eye Drop, Soft, Phoenix Down
Moogle, Shiva, Ifrit, Bahamut, Salamander, Silf

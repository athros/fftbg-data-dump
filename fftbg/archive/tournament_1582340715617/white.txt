Player: !White
Team: White Team
Palettes: White/Blue



Andrain
Female
Capricorn
70
56
Geomancer
Item
Arrow Guard
Equip Armor
Fly

Slasher
Flame Shield
Diamond Helmet
Reflect Mail
Germinas Boots

Pitfall, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard
Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy



Dragoon ZERO
Male
Taurus
48
75
Ninja
Jump
MA Save
Equip Gun
Move+2

Bestiary
Battle Folio
Thief Hat
Clothes
Defense Ring

Shuriken, Bomb, Wand
Level Jump4, Vertical Jump8



DeliciousSub
Male
Gemini
64
73
Knight
Summon Magic
Dragon Spirit
Dual Wield
Jump+2

Defender
Mythril Sword
Leather Helmet
Leather Armor
Defense Armlet

Head Break, Shield Break, Magic Break, Power Break, Dark Sword
Moogle, Ramuh, Golem, Bahamut, Leviathan, Salamander, Fairy, Lich



Nomoment
Male
Virgo
56
48
Lancer
Punch Art
Faith Up
Defense UP
Move+1

Dragon Whisker
Venetian Shield
Bronze Helmet
Bronze Armor
Bracer

Level Jump5, Vertical Jump8
Earth Slash, Secret Fist, Purification, Revive, Seal Evil

Final Bets: green - 11 bets for 3,081G (30.8%, x2.24); yellow - 12 bets for 6,916G (69.2%, x0.45)

green bets:
TasisSai: 1,000G (32.5%, 4,582G)
onewouldfall: 500G (16.2%, 2,804G)
Lord_Gwarth: 388G (12.6%, 388G)
CapnChaos12: 300G (9.7%, 2,797G)
Raixelol: 250G (8.1%, 13,196G)
Lydian_C: 123G (4.0%, 13,575G)
Absalom_20: 120G (3.9%, 6,934G)
AllInBot: 100G (3.2%, 100G)
ravingsockmonkey: 100G (3.2%, 4,726G)
iieunueii: 100G (3.2%, 700G)
Tougou: 100G (3.2%, 6,250G)

yellow bets:
DavenIII: 2,500G (36.1%, 63,535G)
BirbBrainsBot: 1,000G (14.5%, 33,073G)
Thyrandaal: 1,000G (14.5%, 49,692G)
Laserman1000: 600G (8.7%, 24,000G)
dogsandcatsand: 424G (6.1%, 424G)
thunderducker: 312G (4.5%, 312G)
maximumcrit: 300G (4.3%, 5,075G)
LASERJESUS1337: 300G (4.3%, 300G)
datadrivenbot: 200G (2.9%, 62,556G)
acid_flashback: 132G (1.9%, 132G)
captainmilestw: 100G (1.4%, 845G)
getthemoneyz: 48G (0.7%, 1,736,344G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CassiePhoenix
Female
Virgo
62
79
Monk
Elemental
Catch
Dual Wield
Move+2



Feather Hat
Earth Clothes
Bracer

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Lava Ball



AND4H
Male
Gemini
44
51
Thief
Charge
Earplug
Halve MP
Move-MP Up

Orichalcum

Red Hood
Wizard Outfit
Elf Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Steal Status
Charge+2



Nekojin
Monster
Scorpio
41
56
Mindflayer










JumbocactuarX27
Male
Aries
65
75
Geomancer
Summon Magic
Sunken State
Equip Axe
Teleport

Slasher
Gold Shield
Thief Hat
Mythril Vest
Salty Rage

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Ifrit, Titan, Golem, Odin, Salamander, Silf, Lich, Cyclops

Player: !White
Team: White Team
Palettes: White/Blue



Prince Rogers Nelson
Male
Aries
72
74
Mediator
Throw
Mana Shield
Halve MP
Move+2

Mythril Gun

Leather Hat
Black Robe
Dracula Mantle

Praise, Insult, Mimic Daravon, Refute, Rehabilitate
Axe



StealthModeLocke
Monster
Capricorn
69
46
Malboro










Jaltos
Female
Scorpio
49
56
Mediator
Time Magic
Parry
Magic Attack UP
Retreat

Bestiary

Headgear
Mythril Vest
Feather Mantle

Invitation, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute
Slow 2, Demi



Just Here2
Female
Capricorn
61
74
Thief
Black Magic
Abandon
Short Charge
Teleport

Sleep Sword

Green Beret
Judo Outfit
Diamond Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Empower

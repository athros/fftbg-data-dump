Final Bets: red - 17 bets for 7,421G (47.5%, x1.11); brown - 15 bets for 8,217G (52.5%, x0.90)

red bets:
pplvee1: 1,850G (24.9%, 5,378G)
AllInBot: 1,253G (16.9%, 1,253G)
AND4H: 500G (6.7%, 7,294G)
cho_pin: 500G (6.7%, 2,471G)
galkife: 500G (6.7%, 78,680G)
furrytomahawkk: 500G (6.7%, 2,122G)
bahamutlagooon: 423G (5.7%, 8,466G)
gorgewall: 301G (4.1%, 9,720G)
DeathTaxesAndAnime: 250G (3.4%, 3,409G)
skipsandwiches: 232G (3.1%, 232G)
UnderOneLight: 200G (2.7%, 1,777G)
datadrivenbot: 200G (2.7%, 32,772G)
Jaku618: 200G (2.7%, 5,228G)
oldchris: 200G (2.7%, 1,870G)
DouglasDragonThePoet: 112G (1.5%, 112G)
Firesheath: 100G (1.3%, 12,213G)
Miku_Shikhu: 100G (1.3%, 16,886G)

brown bets:
BirbBrainsBot: 1,000G (12.2%, 25,376G)
Meta_Five: 1,000G (12.2%, 18,941G)
JumbocactuarX27: 1,000G (12.2%, 49,059G)
Zeroroute: 921G (11.2%, 921G)
HaateXIII: 772G (9.4%, 772G)
cowlander: 660G (8.0%, 6,608G)
BoneMiser: 558G (6.8%, 558G)
Nizaha: 552G (6.7%, 8,611G)
Xoomwaffle: 500G (6.1%, 2,738G)
getthemoneyz: 430G (5.2%, 1,303,263G)
RyanTheRebs: 300G (3.7%, 5,863G)
BlueAbs: 224G (2.7%, 224G)
nhammen: 100G (1.2%, 14,500G)
TasisSai: 100G (1.2%, 6,318G)
boosted420: 100G (1.2%, 112G)

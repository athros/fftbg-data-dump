Player: !White
Team: White Team
Palettes: White/Blue



Firesheath
Male
Aries
60
58
Monk
White Magic
Caution
Equip Sword
Swim

Defender

Leather Hat
Mystic Vest
Jade Armlet

Purification, Revive
Cure, Raise 2, Wall, Esuna



Cowlander
Male
Sagittarius
61
77
Summoner
Basic Skill
Faith Save
Long Status
Ignore Height

Battle Folio

Leather Hat
Black Robe
Defense Ring

Moogle, Carbunkle, Silf
Accumulate, Dash, Throw Stone, Heal, Tickle, Cheer Up, Fury, Wish, Ultima



Upvla
Female
Scorpio
59
64
Wizard
Item
Auto Potion
Throw Item
Move+3

Air Knife

Holy Miter
Wizard Outfit
Magic Gauntlet

Bolt 2, Death
Hi-Potion, Ether, Hi-Ether, Soft, Holy Water, Phoenix Down



Zbgs
Male
Aquarius
42
42
Calculator
Time Magic
Distribute
Equip Armor
Ignore Height

Papyrus Codex

Platinum Helmet
Plate Mail
Feather Mantle

CT, Height, 5, 4
Haste, Haste 2, Slow, Reflect

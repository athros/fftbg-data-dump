Player: !Green
Team: Green Team
Palettes: Green/White



Elijahrockers
Monster
Sagittarius
45
61
Blue Dragon










Lifeguard Dan
Male
Cancer
62
69
Thief
Jump
Dragon Spirit
Secret Hunt
Move+1

Ancient Sword

Triangle Hat
Adaman Vest
Bracer

Steal Helmet, Steal Armor, Steal Shield, Arm Aim
Level Jump5, Vertical Jump7



Coralreeferz
Female
Aquarius
72
75
Geomancer
Draw Out
Auto Potion
Equip Armor
Fly

Long Sword
Bronze Shield
Green Beret
Plate Mail
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Murasame



Anox Skell
Female
Leo
41
63
Priest
Talk Skill
Damage Split
Dual Wield
Jump+2

Wizard Staff
White Staff
Flash Hat
Wizard Robe
Feather Mantle

Raise, Raise 2, Reraise, Protect 2, Shell 2, Wall, Holy
Persuade, Refute

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TimeJannies
Male
Pisces
79
63
Lancer
Yin Yang Magic
Auto Potion
Secret Hunt
Teleport

Javelin
Aegis Shield
Platinum Helmet
Silk Robe
Defense Armlet

Level Jump8, Vertical Jump3
Pray Faith, Silence Song, Foxbird, Dispel Magic, Dark Holy



MattMan119
Male
Virgo
70
54
Lancer
Elemental
Counter Tackle
Sicken
Ignore Height

Spear
Venetian Shield
Genji Helmet
Carabini Mail
Feather Mantle

Level Jump5, Vertical Jump8
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Itzover9001
Male
Aries
59
51
Bard
Battle Skill
Faith Up
Equip Knife
Move+2

Ninja Edge

Black Hood
Power Sleeve
Power Wrist

Battle Song, Diamond Blade, Sky Demon
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Night Sword, Explosion Sword



HaateXIII
Female
Gemini
80
45
Geomancer
Charge
Hamedo
Equip Armor
Waterbreathing

Ancient Sword
Flame Shield
Circlet
Light Robe
Vanish Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+5, Charge+10

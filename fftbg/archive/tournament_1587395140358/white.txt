Player: !White
Team: White Team
Palettes: White/Blue



Lanshaft
Male
Capricorn
76
46
Calculator
Black Magic
Caution
Sicken
Levitate

Papyrus Codex

Flash Hat
Brigandine
Magic Ring

CT, Height, 3
Fire, Fire 3, Bolt, Ice 3, Death



SaintOmerville
Male
Aquarius
73
79
Time Mage
Summon Magic
MA Save
Sicken
Retreat

Healing Staff

Red Hood
White Robe
Spike Shoes

Haste 2, Slow 2, Stop, Stabilize Time
Moogle, Golem, Carbunkle, Odin, Silf, Lich, Cyclops



UmaiJam
Female
Pisces
52
74
Summoner
Basic Skill
MA Save
Short Charge
Waterbreathing

Ice Rod

Red Hood
Secret Clothes
Defense Armlet

Ifrit, Titan, Carbunkle, Odin, Fairy, Lich
Accumulate, Dash, Throw Stone, Cheer Up, Fury, Wish, Scream, Ultima



Powergems
Male
Gemini
54
60
Knight
Time Magic
Counter
Short Charge
Move+3

Save the Queen
Round Shield
Barbuta
Leather Armor
Elf Mantle

Armor Break, Weapon Break, Magic Break, Night Sword
Haste, Slow 2, Demi, Stabilize Time

Player: !Black
Team: Black Team
Palettes: Black/Red



StealthModeLocke
Female
Sagittarius
80
54
Squire
Jump
Counter
Short Status
Levitate

Slasher
Round Shield
Cross Helmet
Judo Outfit
Chantage

Accumulate, Dash, Throw Stone, Heal, Cheer Up, Fury
Level Jump8, Vertical Jump5



TheChainNerd
Male
Libra
62
65
Wizard
Basic Skill
Distribute
Halve MP
Ignore Terrain

Faith Rod

Golden Hairpin
Clothes
Red Shoes

Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Empower, Flare
Heal, Cheer Up, Fury



DLJuggernaut
Female
Serpentarius
74
37
Summoner
Jump
Abandon
Doublehand
Fly

Flame Rod

Leather Hat
White Robe
Defense Armlet

Moogle, Shiva, Ifrit, Titan, Carbunkle, Fairy, Lich
Level Jump8, Vertical Jump8



Catfashions
Female
Gemini
50
63
Squire
Black Magic
Sunken State
Doublehand
Move-HP Up

Iron Sword

Holy Miter
Crystal Mail
Spike Shoes

Dash, Throw Stone, Heal, Yell, Scream
Fire 2, Ice 4, Flare

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zagorsek
Male
Sagittarius
49
59
Bard
Item
Mana Shield
Doublehand
Jump+1

Ultimus Bow

Thief Hat
Bronze Armor
Wizard Mantle

Angel Song, Diamond Blade, Hydra Pit
Potion, Hi-Potion, Hi-Ether, Eye Drop, Soft, Holy Water, Phoenix Down



ManaBirb
Male
Taurus
41
59
Priest
Basic Skill
Counter
Equip Bow
Teleport

Night Killer

Headgear
Light Robe
108 Gems

Cure 3, Raise, Reraise, Protect, Protect 2, Shell 2, Esuna
Dash, Throw Stone, Heal



Vivithegr8
Male
Gemini
78
47
Summoner
White Magic
Parry
Short Charge
Move-HP Up

Thunder Rod

Thief Hat
Wizard Robe
Magic Gauntlet

Shiva, Ifrit, Titan, Bahamut, Odin, Leviathan, Salamander, Silf, Lich
Raise, Protect 2, Shell, Wall, Esuna



KasugaiRoastedPeas
Female
Scorpio
58
49
Summoner
Charge
Counter Flood
Halve MP
Swim

Flame Rod

Holy Miter
Brigandine
Chantage

Moogle, Ifrit, Carbunkle, Odin, Lich
Charge+2, Charge+3, Charge+7

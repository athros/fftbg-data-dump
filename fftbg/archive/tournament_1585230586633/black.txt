Player: !Black
Team: Black Team
Palettes: Black/Red



Treapvort
Female
Leo
52
46
Geomancer
Draw Out
Caution
Equip Sword
Fly

Ragnarok

Feather Hat
Silk Robe
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji



Zbgs
Male
Taurus
58
51
Lancer
Item
Auto Potion
Attack UP
Move-HP Up

Cypress Rod
Bronze Shield
Crystal Helmet
Chain Mail
Salty Rage

Level Jump5, Vertical Jump3
Potion, Hi-Potion, Maiden's Kiss, Remedy, Phoenix Down



Draconis345
Female
Leo
50
73
Lancer
White Magic
Critical Quick
Sicken
Levitate

Mythril Spear
Genji Shield
Crystal Helmet
Bronze Armor
N-Kai Armlet

Level Jump8, Vertical Jump8
Raise, Raise 2, Reraise, Protect, Wall



Z32o
Female
Scorpio
80
66
Priest
Throw
Caution
Equip Polearm
Waterbreathing

Mythril Spear

Leather Hat
Clothes
Red Shoes

Cure, Cure 3, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell, Shell 2
Shuriken, Bomb, Sword, Staff, Axe, Dictionary

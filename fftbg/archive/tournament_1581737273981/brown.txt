Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tanookium
Monster
Aquarius
78
43
Plague










Terra 32
Male
Capricorn
45
40
Summoner
Charge
Counter
Equip Bow
Retreat

Silver Bow

Black Hood
Leather Outfit
Red Shoes

Moogle, Ramuh, Odin, Fairy
Charge+1



ClutchCasters
Male
Gemini
76
74
Knight
Basic Skill
PA Save
Defense UP
Move+3

Defender

Mythril Helmet
Leather Armor
Battle Boots

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break
Accumulate, Heal, Tickle, Cheer Up, Scream



UmbraKnights
Male
Libra
59
64
Monk
Elemental
Earplug
Dual Wield
Move+3



Leather Hat
Chain Vest
Leather Mantle

Purification, Revive, Seal Evil
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball

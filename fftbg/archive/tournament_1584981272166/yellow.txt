Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zerostarhx
Female
Leo
76
62
Dancer
Throw
Brave Up
Sicken
Move+2

Persia

Leather Hat
Black Robe
Jade Armlet

Wiznaibus, Disillusion, Nameless Dance, Dragon Pit
Shuriken



Winterharte
Male
Capricorn
66
73
Geomancer
Steal
Parry
Equip Bow
Move-HP Up

Lightning Bow

Thief Hat
Wizard Robe
Magic Gauntlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory



Coaks388
Male
Libra
45
52
Archer
Battle Skill
Counter Magic
Defense UP
Teleport

Windslash Bow

Holy Miter
Leather Outfit
Spike Shoes

Charge+1, Charge+2, Charge+7, Charge+10
Armor Break, Weapon Break, Power Break, Mind Break, Dark Sword, Night Sword



Oogthecaveman
Male
Gemini
49
70
Geomancer
Black Magic
Critical Quick
Sicken
Move+1

Asura Knife
Mythril Shield
Feather Hat
Wizard Robe
Defense Ring

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind
Fire 3, Empower

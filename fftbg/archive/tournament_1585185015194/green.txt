Player: !Green
Team: Green Team
Palettes: Green/White



Oshimi12
Monster
Aries
64
65
Ochu










ApplesauceBoss
Male
Gemini
39
49
Mediator
Elemental
Counter Tackle
Equip Polearm
Move+3

Cypress Rod

Thief Hat
Robe of Lords
Jade Armlet

Invitation, Persuade, Praise, Threaten, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard



Gelwain
Monster
Aries
66
50
Ahriman










SolarisFall
Male
Sagittarius
55
52
Priest
Black Magic
Counter Flood
Beastmaster
Waterbreathing

Morning Star

Black Hood
Chameleon Robe
Angel Ring

Cure, Cure 2, Wall, Esuna
Fire, Bolt, Ice, Death

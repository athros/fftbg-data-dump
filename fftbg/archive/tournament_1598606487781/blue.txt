Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lokenwow
Male
Aquarius
64
53
Mediator
Elemental
Critical Quick
Short Charge
Move+3

Bestiary

Flash Hat
Robe of Lords
Red Shoes

Invitation, Persuade, Praise, Death Sentence, Negotiate, Refute
Pitfall, Water Ball, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball



Vorap
Female
Libra
46
66
Lancer
Time Magic
Faith Save
Doublehand
Swim

Javelin

Cross Helmet
Leather Armor
Spike Shoes

Level Jump4, Vertical Jump7
Haste, Haste 2, Slow, Stop, Immobilize, Stabilize Time



CT 5 Holy
Male
Aquarius
41
48
Ninja
Draw Out
MA Save
Short Charge
Jump+2

Kunai
Blind Knife
Black Hood
Chain Vest
Bracer

Shuriken, Staff, Axe
Murasame, Muramasa



Saunders
Male
Aquarius
71
47
Knight
Sing
PA Save
Equip Knife
Fly

Air Knife
Round Shield
Cross Helmet
Silk Robe
Battle Boots

Head Break, Shield Break, Magic Break, Speed Break, Power Break, Stasis Sword
Angel Song, Magic Song, Space Storage, Hydra Pit

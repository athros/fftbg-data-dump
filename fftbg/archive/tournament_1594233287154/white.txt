Player: !White
Team: White Team
Palettes: White/Blue



ZCKaiser
Monster
Sagittarius
50
65
Reaper










DudeMonkey77
Monster
Gemini
42
68
Archaic Demon










HaateXIII
Female
Libra
41
73
Lancer
Black Magic
Mana Shield
Equip Knife
Lava Walking

Air Knife
Gold Shield
Circlet
Chameleon Robe
Reflect Ring

Level Jump5, Vertical Jump5
Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Ice 2, Ice 4



Fluffywormhole
Female
Aquarius
68
62
Knight
Basic Skill
Damage Split
Equip Bow
Waterwalking

Long Bow
Escutcheon
Genji Helmet
Silk Robe
Power Wrist

Shield Break, Magic Break, Power Break, Justice Sword, Explosion Sword
Throw Stone, Heal, Tickle

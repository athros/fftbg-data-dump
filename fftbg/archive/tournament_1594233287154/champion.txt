Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Prince Rogers Nelson
Female
Aquarius
67
51
Wizard
Math Skill
Distribute
Short Charge
Jump+2

Poison Rod

Thief Hat
Silk Robe
Dracula Mantle

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3
CT, Height, Prime Number, 5



Dogsandcatsand
Male
Pisces
48
63
Monk
Elemental
Meatbone Slash
Equip Armor
Move-MP Up



Leather Helmet
Light Robe
Bracer

Spin Fist, Earth Slash, Chakra, Revive, Seal Evil
Water Ball, Hallowed Ground, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



CosmicTactician
Female
Gemini
78
80
Lancer
Item
Dragon Spirit
Attack UP
Ignore Height

Partisan
Escutcheon
Platinum Helmet
Black Robe
N-Kai Armlet

Level Jump4, Vertical Jump8
Potion, Remedy, Phoenix Down



Roofiepops
Male
Virgo
72
50
Monk
Yin Yang Magic
Sunken State
Equip Knife
Retreat

Assassin Dagger

Black Hood
Mystic Vest
Cursed Ring

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Blind, Pray Faith, Doubt Faith, Confusion Song, Dispel Magic

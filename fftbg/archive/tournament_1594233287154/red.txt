Player: !Red
Team: Red Team
Palettes: Red/Brown



ArashiKurobara
Female
Aquarius
67
55
Dancer
Item
Speed Save
Doublehand
Ignore Terrain

Ryozan Silk

Leather Hat
White Robe
Feather Boots

Wiznaibus, Disillusion, Last Dance, Nether Demon, Dragon Pit
Potion, X-Potion, Ether, Eye Drop, Remedy, Phoenix Down



Gelwain
Female
Aries
52
50
Lancer
Steal
Catch
Short Status
Move+1

Gokuu Rod
Mythril Shield
Cross Helmet
Leather Armor
Bracer

Level Jump8, Vertical Jump4
Steal Shield, Steal Weapon, Steal Accessory, Steal Status



Nekojin
Female
Aries
63
58
Lancer
Steal
Abandon
Attack UP
Levitate

Holy Lance
Platinum Shield
Barbuta
Gold Armor
Power Wrist

Level Jump8, Vertical Jump3
Steal Armor, Steal Accessory, Steal Status, Arm Aim



Actual JP
Male
Aries
41
73
Chemist
Basic Skill
Absorb Used MP
Secret Hunt
Ignore Terrain

Orichalcum

Green Beret
Chain Vest
Sprint Shoes

Potion, Ether, Hi-Ether, Antidote, Holy Water, Phoenix Down
Dash, Heal, Yell, Fury, Scream

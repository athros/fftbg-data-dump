Player: !Green
Team: Green Team
Palettes: Green/White



Fenaen
Female
Sagittarius
61
64
Chemist
Dance
Blade Grasp
Magic Defense UP
Jump+1

Rune Blade

Flash Hat
Chain Vest
Angel Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down
Witch Hunt, Wiznaibus, Disillusion, Obsidian Blade, Nether Demon



Evontno
Female
Taurus
47
59
Samurai
Jump
Dragon Spirit
Beastmaster
Waterwalking

Mythril Spear

Gold Helmet
Leather Armor
Red Shoes

Asura, Koutetsu, Heaven's Cloud, Kikuichimoji
Level Jump8, Vertical Jump7



Nifboy
Female
Virgo
45
67
Mime

Distribute
Martial Arts
Ignore Terrain



Bronze Helmet
Wizard Outfit
Reflect Ring

Mimic




Laserman1000
Male
Aquarius
37
77
Samurai
Jump
Parry
Short Charge
Waterbreathing

Bizen Boat

Cross Helmet
Gold Armor
Leather Mantle

Bizen Boat, Kiyomori, Muramasa
Level Jump8, Vertical Jump7

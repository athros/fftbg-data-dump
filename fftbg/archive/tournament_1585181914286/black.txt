Player: !Black
Team: Black Team
Palettes: Black/Red



The Pengwin
Monster
Capricorn
46
43
Pisco Demon










MoonSlayerRS
Male
Taurus
60
49
Oracle
Charge
Counter
Equip Axe
Retreat

Rainbow Staff

Leather Hat
Chameleon Robe
Defense Ring

Blind, Life Drain, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep, Petrify
Charge+5, Charge+7



DudeMonkey77
Monster
Cancer
63
45
Black Goblin










CorpusCav
Female
Virgo
80
51
Priest
Summon Magic
Mana Shield
Dual Wield
Move+2

Healing Staff
White Staff
Leather Hat
Black Robe
Battle Boots

Cure 2, Cure 4, Raise 2, Reraise, Regen, Shell 2, Wall
Moogle, Shiva, Golem, Carbunkle, Odin, Leviathan, Silf

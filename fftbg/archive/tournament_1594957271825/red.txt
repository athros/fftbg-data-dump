Player: !Red
Team: Red Team
Palettes: Red/Brown



DAC169
Female
Capricorn
47
80
Summoner
Battle Skill
Caution
Short Charge
Retreat

Thunder Rod

Leather Hat
Silk Robe
Dracula Mantle

Moogle, Shiva, Ramuh, Ifrit
Weapon Break, Magic Break, Power Break



Snowfats
Female
Leo
47
42
Knight
White Magic
Catch
Equip Polearm
Fly

Javelin
Genji Shield
Genji Helmet
Linen Cuirass
Genji Gauntlet

Head Break, Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword
Cure 4, Raise, Protect 2, Wall, Esuna



DeathTaxesAndAnime
Female
Sagittarius
81
51
Mime

Counter
Equip Shield
Waterwalking


Mythril Shield
Mythril Helmet
Earth Clothes
Wizard Mantle

Mimic




Darnitkevin
Female
Cancer
58
66
Lancer
Black Magic
Damage Split
Equip Sword
Jump+1

Bizen Boat
Ice Shield
Platinum Helmet
Wizard Robe
Defense Armlet

Level Jump8, Vertical Jump8
Fire, Fire 3, Bolt, Ice 2

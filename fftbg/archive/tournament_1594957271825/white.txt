Player: !White
Team: White Team
Palettes: White/Blue



SkylerBunny
Female
Pisces
80
53
Time Mage
Steal
HP Restore
Long Status
Move+3

Rainbow Staff

Black Hood
Judo Outfit
Magic Ring

Haste, Slow 2, Stop, Immobilize, Reflect, Quick, Demi 2, Stabilize Time
Steal Heart, Steal Weapon, Steal Status, Leg Aim



Othatmattkid
Male
Aries
44
56
Geomancer
Draw Out
Damage Split
Magic Defense UP
Fly

Slasher
Genji Shield
Headgear
Wizard Robe
Dracula Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Koutetsu, Muramasa, Kikuichimoji



Electric Algus
Female
Sagittarius
72
49
Chemist
White Magic
Critical Quick
Long Status
Move-MP Up

Star Bag

Green Beret
Adaman Vest
Defense Ring

Potion, Hi-Potion, X-Potion, Ether, Antidote, Phoenix Down
Cure 2, Cure 3, Cure 4, Protect 2, Shell, Wall, Esuna, Holy



Vorap
Female
Leo
43
46
Monk
Steal
Auto Potion
Attack UP
Fly



Holy Miter
Leather Outfit
Magic Ring

Spin Fist, Pummel, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil
Arm Aim

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



LuckyLuckLuc2
Male
Sagittarius
55
65
Samurai
Talk Skill
Faith Up
Equip Bow
Waterbreathing

Cross Bow

Barbuta
Maximillian
Cursed Ring

Murasame
Invitation, Preach, Insult, Refute



HASTERIOUS
Female
Pisces
72
80
Wizard
Item
Counter
Short Charge
Waterwalking

Ice Rod

Triangle Hat
Black Robe
Cursed Ring

Fire, Fire 3, Fire 4, Bolt 4, Ice 2, Ice 3, Ice 4
Potion, X-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



Dantetouhou
Monster
Leo
74
49
Dryad










Bad1dea
Female
Libra
71
58
Squire
Draw Out
Critical Quick
Defend
Jump+1

Iron Sword
Mythril Shield
Black Hood
Clothes
Magic Gauntlet

Dash, Heal, Fury, Wish
Murasame, Muramasa

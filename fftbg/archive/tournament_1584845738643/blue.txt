Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Fenaen
Female
Aries
56
39
Monk
Black Magic
Auto Potion
Secret Hunt
Jump+1



Golden Hairpin
Mystic Vest
Diamond Armlet

Spin Fist, Wave Fist, Secret Fist, Chakra, Revive
Fire, Fire 4, Bolt, Bolt 3, Bolt 4, Flare



Xbtpxsauce
Female
Aries
64
71
Priest
Yin Yang Magic
Regenerator
Long Status
Move+2

Flame Whip

Red Hood
Brigandine
108 Gems

Cure 4, Shell, Wall, Esuna
Blind, Pray Faith, Dispel Magic, Sleep



Drutaru
Male
Aquarius
52
57
Bard
Black Magic
Critical Quick
Long Status
Jump+1

Bloody Strings

Flash Hat
Genji Armor
Germinas Boots

Angel Song, Life Song, Cheer Song, Battle Song, Diamond Blade
Fire 2, Bolt, Ice 4, Flare



Powergems
Male
Sagittarius
63
41
Lancer
Basic Skill
Distribute
Magic Defense UP
Move+3

Javelin
Gold Shield
Bronze Helmet
Reflect Mail
Jade Armlet

Level Jump5, Vertical Jump4
Throw Stone, Heal, Tickle, Yell

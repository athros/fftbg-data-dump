Player: !Black
Team: Black Team
Palettes: Black/Red



DustBirdEX
Female
Sagittarius
73
52
Thief
White Magic
Meatbone Slash
Maintenance
Waterwalking

Cultist Dagger

Black Hood
Chain Vest
Germinas Boots

Steal Armor, Steal Accessory, Arm Aim
Cure, Reraise, Protect, Shell 2, Wall, Esuna



LDSkinny
Female
Taurus
40
67
Oracle
Steal
Earplug
Equip Bow
Retreat

Perseus Bow

Twist Headband
Linen Robe
Vanish Mantle

Spell Absorb, Life Drain, Foxbird, Confusion Song, Dispel Magic, Petrify
Steal Helmet, Steal Accessory



Scuba Steve3
Male
Pisces
47
58
Archer
Yin Yang Magic
Damage Split
Equip Knife
Jump+2

Sasuke Knife
Round Shield
Black Hood
Clothes
Diamond Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+10
Life Drain, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze



MonchoStrife
Female
Virgo
73
41
Priest
Basic Skill
Earplug
Sicken
Ignore Terrain

Oak Staff

Thief Hat
Silk Robe
N-Kai Armlet

Cure, Raise, Raise 2, Reraise, Shell 2, Esuna, Magic Barrier
Dash, Throw Stone, Heal, Yell, Wish

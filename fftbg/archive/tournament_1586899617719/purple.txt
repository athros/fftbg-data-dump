Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



WoooBlaaa
Female
Aries
63
81
Mediator
Summon Magic
Hamedo
Concentrate
Levitate

Stone Gun

Ribbon
Secret Clothes
Dracula Mantle

Praise, Mimic Daravon, Rehabilitate
Ramuh, Leviathan



AerodyneX
Male
Scorpio
57
61
Priest
Punch Art
Caution
Equip Axe
Lava Walking

Flail

Holy Miter
Linen Robe
Bracer

Cure 2, Cure 3, Reraise, Regen, Esuna
Spin Fist, Purification



Andrain
Female
Capricorn
43
45
Samurai
Punch Art
Counter Tackle
Equip Knife
Jump+1

Rod

Crystal Helmet
Bronze Armor
Jade Armlet

Asura, Koutetsu, Muramasa, Kikuichimoji
Pummel, Wave Fist, Purification, Seal Evil



Ko2q
Male
Libra
64
62
Knight
Throw
Abandon
Defense UP
Swim

Mythril Sword
Gold Shield
Bronze Helmet
Gold Armor
Rubber Shoes

Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword, Surging Sword
Shuriken, Bomb, Knife, Hammer, Dictionary

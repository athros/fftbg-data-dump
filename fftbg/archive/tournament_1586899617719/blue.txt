Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Roqqqpsi
Female
Sagittarius
79
70
Mime

Counter Magic
Doublehand
Lava Walking



Green Beret
Light Robe
Jade Armlet

Mimic




Mirapoix
Male
Gemini
46
71
Knight
Yin Yang Magic
Counter Magic
Beastmaster
Move+2

Slasher
Escutcheon
Iron Helmet
Gold Armor
Elf Mantle

Shield Break, Justice Sword, Dark Sword
Spell Absorb, Life Drain, Zombie, Confusion Song, Dispel Magic, Paralyze



Zbgs
Male
Taurus
45
77
Summoner
Basic Skill
PA Save
Equip Bow
Swim

Long Bow

Golden Hairpin
Mystic Vest
Chantage

Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Salamander, Fairy
Heal, Yell, Cheer Up



HippieDota
Female
Pisces
75
75
Oracle
Math Skill
Faith Up
Halve MP
Move+1

Battle Folio

Black Hood
Robe of Lords
Rubber Shoes

Life Drain, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze
CT, 5, 4

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Evewho
Female
Pisces
41
71
Knight
Yin Yang Magic
Abandon
Halve MP
Swim

Chaos Blade
Gold Shield
Mythril Helmet
Plate Mail
Leather Mantle

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break
Life Drain, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic



ANFz
Male
Sagittarius
44
55
Wizard
Basic Skill
MP Restore
Equip Polearm
Retreat

Gokuu Rod

Headgear
Silk Robe
108 Gems

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice 3, Empower, Death
Accumulate, Throw Stone, Heal, Yell, Cheer Up, Fury



Shakarak
Female
Libra
77
53
Samurai
White Magic
Parry
Defend
Move+1

Kikuichimoji

Gold Helmet
Genji Armor
Genji Gauntlet

Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori, Kikuichimoji, Chirijiraden
Cure 3, Raise, Regen, Shell, Wall



Catfashions
Male
Capricorn
55
66
Ninja
Steal
Parry
Attack UP
Teleport

Zorlin Shape
Spell Edge
Holy Miter
Black Costume
Vanish Mantle

Bomb, Knife, Axe, Dictionary
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status

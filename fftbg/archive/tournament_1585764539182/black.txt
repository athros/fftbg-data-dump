Player: !Black
Team: Black Team
Palettes: Black/Red



TheChainNerd
Female
Aries
68
61
Ninja
Time Magic
Distribute
Attack UP
Swim

Short Edge
Flail
Golden Hairpin
Judo Outfit
Magic Ring

Bomb
Stop, Quick, Demi 2, Stabilize Time



Zagorsek
Female
Pisces
40
64
Monk
Charge
Counter Flood
Magic Attack UP
Jump+3



Golden Hairpin
Black Costume
Sprint Shoes

Pummel, Earth Slash, Seal Evil
Charge+1, Charge+2, Charge+3



Hyvi
Male
Leo
81
46
Bard
Talk Skill
Critical Quick
Doublehand
Move+3

Hydra Bag

Cachusha
Bronze Armor
Germinas Boots

Life Song, Cheer Song, Nameless Song, Space Storage, Sky Demon
Invitation, Preach, Negotiate, Refute



Toka222
Male
Capricorn
45
54
Archer
White Magic
Counter
Doublehand
Fly

Mythril Bow

Feather Hat
Adaman Vest
Jade Armlet

Charge+5
Cure 3, Regen, Esuna

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheNGUYENNER
Female
Cancer
59
44
Chemist
Elemental
Parry
Defend
Ignore Height

Mythril Gun

Holy Miter
Secret Clothes
Rubber Shoes

Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



NicoSavoy
Monster
Taurus
52
76
Serpentarius










MantisFinch
Male
Aquarius
68
41
Oracle
Draw Out
Auto Potion
Sicken
Move-HP Up

Madlemgen

Red Hood
Light Robe
Feather Mantle

Blind, Poison, Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic
Koutetsu, Kiyomori, Muramasa



Sukotsuto
Female
Leo
65
61
Ninja
Dance
Sunken State
Martial Arts
Ignore Height



Golden Hairpin
Judo Outfit
Cursed Ring

Shuriken, Bomb, Knife
Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon

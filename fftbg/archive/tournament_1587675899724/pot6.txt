Final Bets: white - 18 bets for 12,063G (50.1%, x0.99); brown - 21 bets for 11,992G (49.9%, x1.01)

white bets:
itzover9001: 2,000G (16.6%, 38,081G)
MantisFinch: 1,500G (12.4%, 15,791G)
fenaen: 1,500G (12.4%, 54,106G)
Lordminsc: 1,205G (10.0%, 51,898G)
Shalloween: 1,000G (8.3%, 40,953G)
BirbBrainsBot: 1,000G (8.3%, 147,332G)
mirapoix: 664G (5.5%, 664G)
ShintaroNayaka: 600G (5.0%, 23,027G)
MrDiggs49: 500G (4.1%, 13,215G)
feDoran: 500G (4.1%, 7,727G)
EnemyController: 400G (3.3%, 83,621G)
xYungAnarchyx: 333G (2.8%, 333G)
FoeSquirrel: 310G (2.6%, 620G)
ewan_e: 200G (1.7%, 2,595G)
ZephyrTempest: 101G (0.8%, 180,865G)
MMagnetMan: 100G (0.8%, 100G)
duskps: 100G (0.8%, 8,669G)
DrAntiSocial: 50G (0.4%, 23,781G)

brown bets:
volgrathemoose: 2,001G (16.7%, 10,746G)
JonnyCue: 2,000G (16.7%, 40,010G)
Baron_von_Scrub: 1,111G (9.3%, 92,618G)
braumbles: 1,000G (8.3%, 88,931G)
ungabunga_bot: 1,000G (8.3%, 215,378G)
TheGuesty: 863G (7.2%, 24,863G)
RunicMagus: 600G (5.0%, 32,172G)
Zeroroute: 600G (5.0%, 600G)
XalSmellsBad: 500G (4.2%, 11,966G)
HaateXIII: 500G (4.2%, 35,974G)
Evewho: 388G (3.2%, 388G)
nifboy: 289G (2.4%, 7,246G)
lifeguard_dan: 280G (2.3%, 280G)
summmmmmmmmm: 180G (1.5%, 180G)
AllInBot: 100G (0.8%, 100G)
WoooBlaaa: 100G (0.8%, 14,694G)
avathesunqueen: 100G (0.8%, 100G)
datadrivenbot: 100G (0.8%, 1,997G)
ko2q: 100G (0.8%, 3,954G)
Oobs56: 100G (0.8%, 5,061G)
getthemoneyz: 80G (0.7%, 566,374G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



KaLam1ty
Female
Gemini
64
52
Oracle
Steal
Faith Up
Equip Bow
Swim

Hydra Bag

Leather Hat
Light Robe
Cursed Ring

Life Drain, Pray Faith, Foxbird, Dispel Magic, Paralyze, Sleep
Steal Heart, Steal Armor, Steal Weapon



Bad1dea
Female
Sagittarius
52
63
Summoner
Throw
Auto Potion
Secret Hunt
Teleport

White Staff

Flash Hat
Chameleon Robe
Small Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Odin, Silf, Lich, Zodiac
Sword, Hammer



L2 Sentinel
Male
Aries
45
61
Oracle
White Magic
Speed Save
Short Status
Jump+3

Cypress Rod

Headgear
Wizard Outfit
Rubber Shoes

Pray Faith, Doubt Faith, Foxbird, Confusion Song, Paralyze, Petrify
Cure 2, Raise 2, Shell 2, Esuna



Powergems
Female
Leo
49
66
Monk
Time Magic
Counter Magic
Equip Armor
Ignore Height



Diamond Helmet
Reflect Mail
Defense Armlet

Wave Fist, Earth Slash, Secret Fist, Chakra, Revive
Haste, Reflect, Quick, Demi

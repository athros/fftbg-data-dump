Player: !Green
Team: Green Team
Palettes: Green/White



CassiePhoenix
Female
Libra
61
79
Geomancer
Punch Art
Mana Shield
Dual Wield
Waterbreathing

Slasher
Asura Knife
Green Beret
Judo Outfit
Diamond Armlet

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Purification, Chakra, Revive



Prince Rogers Nelson
Male
Gemini
55
46
Mediator
Item
PA Save
Dual Wield
Move+3

Mythril Gun
Blaze Gun
Twist Headband
Silk Robe
Battle Boots

Invitation, Persuade, Praise, Preach, Negotiate, Refute, Rehabilitate
Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Autnagrag
Monster
Serpentarius
40
58
Plague










CosmicTactician
Female
Capricorn
42
74
Ninja
Yin Yang Magic
Speed Save
Equip Gun
Move+1

Bestiary
Papyrus Codex
Green Beret
Adaman Vest
Elf Mantle

Shuriken, Bomb
Blind, Poison, Life Drain, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy

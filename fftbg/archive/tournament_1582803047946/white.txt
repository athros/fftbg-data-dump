Player: !White
Team: White Team
Palettes: White/Blue



Ominnous
Male
Virgo
63
60
Lancer
Battle Skill
Parry
Magic Defense UP
Move-HP Up

Cypress Rod
Round Shield
Barbuta
Mythril Armor
Magic Ring

Level Jump8, Vertical Jump8
Head Break, Armor Break, Weapon Break, Power Break, Mind Break



TheKillerNacho
Female
Leo
51
63
Priest
Dance
Abandon
Equip Gun
Fly

Bestiary

Black Hood
Mythril Vest
Magic Ring

Cure 2, Raise, Esuna, Holy
Obsidian Blade, Dragon Pit



Activetimebattle
Female
Libra
43
45
Priest
Yin Yang Magic
Counter Magic
Equip Shield
Move+3

Oak Staff
Crystal Shield
Green Beret
Rubber Costume
Defense Ring

Cure, Raise, Raise 2, Regen, Protect 2, Esuna
Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep



Taypost
Male
Libra
40
78
Archer
Yin Yang Magic
Regenerator
Long Status
Fly

Ultimus Bow

Golden Hairpin
Rubber Costume
Feather Mantle

Charge+1, Charge+4, Charge+5
Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep

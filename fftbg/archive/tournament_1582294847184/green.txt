Player: !Green
Team: Green Team
Palettes: Green/White



Z32o
Male
Aries
50
55
Bard
Basic Skill
Regenerator
Equip Knife
Ignore Height

Flame Rod

Feather Hat
Mythril Vest
Spike Shoes

Angel Song, Life Song, Cheer Song
Heal, Tickle, Wish



MalakoFox
Monster
Virgo
62
52
Minotaur










MrUbiq
Female
Pisces
74
55
Dancer
Throw
Brave Up
Dual Wield
Ignore Terrain

Ryozan Silk
Cashmere
Thief Hat
Silk Robe
Leather Mantle

Witch Hunt, Obsidian Blade
Bomb



E7bbk
Male
Virgo
55
53
Archer
Draw Out
Meatbone Slash
Doublehand
Move+3

Bow Gun

Headgear
Mystic Vest
Wizard Mantle

Charge+1, Charge+5, Charge+10
Koutetsu, Heaven's Cloud, Muramasa

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Snkey
Female
Virgo
63
64
Samurai
Item
Counter
Equip Polearm
Move-HP Up

Battle Bamboo

Bronze Helmet
Gold Armor
Sprint Shoes

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji, Masamune
Potion, Hi-Potion, X-Potion, Holy Water, Phoenix Down



Stoneforge
Female
Sagittarius
55
38
Knight
Charge
Caution
Defend
Jump+3

Mythril Sword
Buckler
Crystal Helmet
Linen Cuirass
Dracula Mantle

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword
Charge+2, Charge+3, Charge+5, Charge+10



Adenis222
Female
Aries
78
58
Lancer
Charge
Auto Potion
Equip Sword
Jump+3

Rune Blade
Crystal Shield
Bronze Helmet
Black Robe
Diamond Armlet

Level Jump4, Vertical Jump6
Charge+4, Charge+5, Charge+20



Lodrak
Male
Pisces
61
48
Thief
Jump
Abandon
Dual Wield
Move+3

Orichalcum
Coral Sword
Leather Hat
Brigandine
Small Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Level Jump8, Vertical Jump7

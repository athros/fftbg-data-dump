Player: !White
Team: White Team
Palettes: White/Blue



DudeMonkey77
Female
Taurus
49
47
Geomancer
Draw Out
Counter
Halve MP
Jump+3

Kiyomori
Bronze Shield
Holy Miter
Linen Robe
Power Wrist

Water Ball, Hell Ivy, Quicksand, Sand Storm, Lava Ball
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



Jennero
Male
Sagittarius
51
50
Oracle
Battle Skill
Auto Potion
Equip Gun
Jump+1

Ramia Harp

Red Hood
Earth Clothes
Genji Gauntlet

Dispel Magic, Paralyze
Shield Break, Magic Break, Mind Break



Catfashions
Male
Gemini
39
51
Mime

Arrow Guard
Dual Wield
Waterwalking



Green Beret
Judo Outfit
Power Wrist

Mimic




ANFz
Male
Scorpio
63
41
Squire
Elemental
Counter
Halve MP
Levitate

Zorlin Shape
Escutcheon
Leather Hat
Earth Clothes
Magic Gauntlet

Accumulate, Throw Stone, Heal, Tickle, Yell, Fury
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gorgewall
Female
Aries
61
51
Knight
White Magic
Regenerator
Short Charge
Jump+1

Battle Axe
Venetian Shield
Barbuta
Wizard Robe
Feather Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Surging Sword
Cure, Cure 2, Cure 4, Raise, Protect, Shell 2, Esuna, Holy



LLight
Female
Taurus
52
78
Thief
Dance
Auto Potion
Sicken
Waterwalking

Mage Masher

Twist Headband
Adaman Vest
Spike Shoes

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Status
Witch Hunt, Wiznaibus, Polka Polka, Last Dance, Dragon Pit



Nifboy
Female
Gemini
49
79
Squire
Elemental
Mana Shield
Doublehand
Jump+3

Bow Gun

Triangle Hat
Clothes
Germinas Boots

Accumulate, Dash, Heal, Yell, Wish
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Gusty Wind, Lava Ball



Thethorndog1
Male
Pisces
44
65
Calculator
Black Magic
Counter Tackle
Equip Polearm
Move-HP Up

Iron Fan

Flash Hat
Chameleon Robe
Small Mantle

CT, 5, 4, 3
Fire, Fire 2, Fire 4, Bolt, Ice, Ice 2, Ice 3

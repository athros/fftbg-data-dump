Player: !White
Team: White Team
Palettes: White/Blue



E Ballard
Male
Gemini
74
50
Knight
White Magic
HP Restore
Short Charge
Jump+2

Defender
Flame Shield
Genji Helmet
Chameleon Robe
Wizard Mantle

Shield Break, Justice Sword, Dark Sword
Cure 3, Raise, Protect 2, Shell, Esuna



Reddwind
Male
Gemini
78
73
Knight
Steal
Sunken State
Short Status
Levitate

Ragnarok
Buckler
Diamond Helmet
Bronze Armor
Angel Ring

Shield Break, Speed Break, Dark Sword
Gil Taking, Steal Shield, Steal Weapon, Steal Accessory



Miku Shikhu
Male
Cancer
79
75
Oracle
Draw Out
PA Save
Martial Arts
Move-HP Up



Cachusha
Wizard Robe
Germinas Boots

Spell Absorb, Pray Faith, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dark Holy
Asura, Bizen Boat, Heaven's Cloud



Laserman1000
Male
Cancer
64
65
Chemist
Talk Skill
Counter Tackle
Martial Arts
Lava Walking

Main Gauche

Holy Miter
Brigandine
Bracer

Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Persuade, Threaten, Negotiate, Mimic Daravon

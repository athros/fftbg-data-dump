Player: !Red
Team: Red Team
Palettes: Red/Brown



Digitalsocrates
Male
Aries
50
55
Lancer
Black Magic
Parry
Defense UP
Jump+3

Obelisk
Round Shield
Platinum Helmet
Linen Cuirass
Red Shoes

Level Jump5, Vertical Jump5
Fire 2, Bolt 4, Ice 4, Empower, Frog, Flare



Bioticism
Female
Cancer
65
79
Monk
Dance
Distribute
Short Status
Fly



Green Beret
Brigandine
Red Shoes

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Witch Hunt, Slow Dance, Last Dance, Dragon Pit



Solomongrundy85
Female
Pisces
51
55
Samurai
Time Magic
Counter Tackle
Magic Attack UP
Move-HP Up

Heaven's Cloud

Diamond Helmet
Diamond Armor
Small Mantle

Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Stop, Quick, Demi, Stabilize Time



Graehme
Male
Virgo
46
52
Lancer
Battle Skill
HP Restore
Equip Gun
Move-MP Up

Ramia Harp
Crystal Shield
Bronze Helmet
Mythril Armor
Defense Armlet

Level Jump8, Vertical Jump8
Weapon Break, Magic Break

Final Bets: white - 12 bets for 6,018G (71.7%, x0.39); purple - 6 bets for 2,375G (28.3%, x2.53)

white bets:
run_with_stone_GUNs: 2,000G (33.2%, 29,638G)
serperemagus: 1,000G (16.6%, 28,212G)
prince_rogers_nelson_: 600G (10.0%, 600G)
ShintaroNayaka: 592G (9.8%, 592G)
twelfthrootoftwo: 500G (8.3%, 19,609G)
gorgewall: 400G (6.6%, 400G)
BirbBrainsBot: 326G (5.4%, 36,620G)
Bryon_W: 200G (3.3%, 2,780G)
E_Ballard: 100G (1.7%, 1,894G)
nhammen: 100G (1.7%, 8,421G)
datadrivenbot: 100G (1.7%, 50,900G)
CosmicTactician: 100G (1.7%, 58,530G)

purple bets:
helpimabug: 786G (33.1%, 1,573G)
NIghtdew14: 650G (27.4%, 14,161G)
TheChainNerd: 361G (15.2%, 3,610G)
ArashiKurobara: 240G (10.1%, 240G)
getthemoneyz: 238G (10.0%, 1,134,985G)
brokenknight201: 100G (4.2%, 800G)

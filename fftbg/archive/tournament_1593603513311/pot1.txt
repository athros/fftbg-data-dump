Final Bets: red - 6 bets for 4,517G (55.7%, x0.80); blue - 6 bets for 3,599G (44.3%, x1.26)

red bets:
amiture: 2,000G (44.3%, 17,939G)
prince_rogers_nelson_: 1,717G (38.0%, 1,717G)
twelfthrootoftwo: 300G (6.6%, 19,993G)
skillomono: 300G (6.6%, 26,841G)
E_Ballard: 100G (2.2%, 2,094G)
datadrivenbot: 100G (2.2%, 50,990G)

blue bets:
ForagerCats: 1,000G (27.8%, 15,183G)
BirbBrainsBot: 1,000G (27.8%, 33,432G)
getthemoneyz: 604G (16.8%, 1,133,049G)
helpimabug: 495G (13.8%, 495G)
gorgewall: 400G (11.1%, 400G)
CosmicTactician: 100G (2.8%, 58,932G)

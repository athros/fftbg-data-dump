Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Fluffywormhole
Male
Aquarius
72
57
Lancer
Yin Yang Magic
Damage Split
Equip Armor
Swim

Obelisk
Flame Shield
Platinum Helmet
Wizard Outfit
Sprint Shoes

Level Jump4, Vertical Jump8
Poison, Spell Absorb, Pray Faith, Zombie



FriendSquirrel
Female
Taurus
67
77
Squire
Throw
PA Save
Doublehand
Retreat

Gastrafitis

Grand Helmet
Chain Mail
Magic Ring

Accumulate, Dash, Heal, Yell, Cheer Up, Fury, Ultima
Shuriken, Staff



Kyune
Female
Capricorn
45
50
Geomancer
Time Magic
Dragon Spirit
Long Status
Teleport

Nagrarock
Buckler
Headgear
Chameleon Robe
Reflect Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Haste, Slow, Slow 2, Stop, Quick, Demi, Stabilize Time, Meteor



HaateXIII
Female
Virgo
52
65
Thief
Jump
Auto Potion
Sicken
Move-MP Up

Dagger

Triangle Hat
Adaman Vest
Magic Gauntlet

Steal Armor, Steal Accessory, Arm Aim
Level Jump2, Vertical Jump3

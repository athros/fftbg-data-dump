Final Bets: red - 9 bets for 3,985G (38.3%, x1.61); blue - 10 bets for 6,420G (61.7%, x0.62)

red bets:
UmaiJam: 1,500G (37.6%, 51,266G)
BirbBrainsBot: 1,000G (25.1%, 170,913G)
getthemoneyz: 406G (10.2%, 1,231,395G)
CT_5_Holy: 375G (9.4%, 375G)
PrancesWithWolves: 204G (5.1%, 204G)
amiture: 200G (5.0%, 2,382G)
Ring_Wyrm: 100G (2.5%, 10,390G)
pLifer: 100G (2.5%, 559G)
randgridr: 100G (2.5%, 1,150G)

blue bets:
twelfthrootoftwo: 1,248G (19.4%, 2,449G)
DeathTaxesAndAnime: 1,243G (19.4%, 1,243G)
Evewho: 1,000G (15.6%, 5,043G)
serperemagus: 1,000G (15.6%, 16,287G)
NicoSavoy: 1,000G (15.6%, 158,928G)
dem0nj0ns: 297G (4.6%, 297G)
Lemonjohns: 232G (3.6%, 232G)
datadrivenbot: 200G (3.1%, 63,327G)
fluffskull: 100G (1.6%, 4,574G)
furrytomahawkk: 100G (1.6%, 3,528G)

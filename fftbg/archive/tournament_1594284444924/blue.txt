Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Quadh0nk
Monster
Taurus
42
65
Behemoth










Dem0nj0ns
Male
Aquarius
79
55
Chemist
Jump
Auto Potion
Concentrate
Jump+2

Mythril Gun

Triangle Hat
Mythril Vest
Diamond Armlet

Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Level Jump3, Vertical Jump8



NicoSavoy
Female
Virgo
70
62
Calculator
Yin Yang Magic
MA Save
Equip Sword
Ignore Height

Ragnarok

Leather Hat
Power Sleeve
Diamond Armlet

Height, Prime Number, 5
Poison, Life Drain, Confusion Song, Dispel Magic, Sleep



Twelfthrootoftwo
Female
Taurus
52
66
Samurai
Yin Yang Magic
Brave Save
Equip Shield
Move+3

Muramasa
Flame Shield
Genji Helmet
Crystal Mail
Magic Gauntlet

Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Poison, Life Drain, Dispel Magic

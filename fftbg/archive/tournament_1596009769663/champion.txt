Player: !zChamp
Team: Champion Team
Palettes: Green/White



Mirapoix
Male
Capricorn
56
59
Archer
Yin Yang Magic
MP Restore
Doublehand
Waterbreathing

Snipe Bow

Genji Helmet
Earth Clothes
Germinas Boots

Charge+1, Charge+4, Charge+5, Charge+7
Poison, Spell Absorb, Pray Faith, Confusion Song, Dispel Magic



TasisSai
Female
Scorpio
79
49
Geomancer
Throw
Critical Quick
Long Status
Move+1

Slasher
Buckler
Holy Miter
Secret Clothes
Sprint Shoes

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bomb



Error72
Male
Leo
69
48
Monk
Steal
Parry
Attack UP
Jump+1



Triangle Hat
Clothes
Reflect Ring

Spin Fist, Wave Fist, Earth Slash
Steal Heart, Leg Aim



Grasnikk
Male
Taurus
45
40
Ninja
Item
Speed Save
Throw Item
Move-HP Up

Morning Star
Kunai
Leather Hat
Secret Clothes
Cursed Ring

Shuriken, Knife, Staff
Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down

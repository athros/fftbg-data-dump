Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Pplvee1
Female
Leo
56
70
Priest
Charge
Caution
Equip Knife
Move+3

Kunai

Green Beret
Chameleon Robe
Dracula Mantle

Cure, Cure 4, Raise, Raise 2, Reraise, Regen, Shell 2, Wall, Esuna, Holy
Charge+1, Charge+3, Charge+5, Charge+10



TeaTime29
Female
Capricorn
78
78
Chemist
Throw
Arrow Guard
Equip Axe
Waterwalking

White Staff

Green Beret
Black Costume
Power Wrist

Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Shuriken



Quadh0nk
Female
Aquarius
52
44
Knight
Time Magic
Meatbone Slash
Martial Arts
Jump+2


Diamond Shield
Platinum Helmet
Linen Robe
Battle Boots

Magic Break, Mind Break, Stasis Sword
Haste, Slow, Slow 2, Stop, Float, Reflect, Demi, Demi 2



BlackFireUK
Male
Aquarius
49
57
Ninja
White Magic
Distribute
Equip Gun
Levitate

Bestiary
Battle Folio
Flash Hat
Mythril Vest
Wizard Mantle

Shuriken, Dictionary
Cure, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Wall, Esuna, Holy

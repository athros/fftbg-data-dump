Player: !Green
Team: Green Team
Palettes: Green/White



TheChainNerd
Female
Cancer
51
78
Dancer
Elemental
Arrow Guard
Long Status
Ignore Height

Cashmere

Flash Hat
Clothes
Jade Armlet

Witch Hunt, Polka Polka, Disillusion, Void Storage
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Gusty Wind



Valvalis
Male
Capricorn
53
59
Wizard
Elemental
Caution
Short Charge
Lava Walking

Wizard Rod

Green Beret
Wizard Outfit
Power Wrist

Fire 3, Bolt 4, Ice, Ice 3, Ice 4, Frog, Flare
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard



Maakur
Monster
Aquarius
53
75
Pisco Demon










0utlier
Female
Libra
42
74
Squire
Charge
Absorb Used MP
Magic Attack UP
Jump+2

Broad Sword
Crystal Shield
Leather Helmet
Wizard Outfit
Feather Mantle

Accumulate, Dash, Heal, Tickle, Yell, Wish, Scream
Charge+1, Charge+2, Charge+3, Charge+4, Charge+10

Player: !Green
Team: Green Team
Palettes: Green/White



KasugaiRoastedPeas
Male
Taurus
50
45
Mediator
Battle Skill
Counter Tackle
Equip Knife
Jump+2

Dragon Rod

Twist Headband
Black Costume
Reflect Ring

Persuade, Praise, Solution, Death Sentence, Insult, Mimic Daravon, Rehabilitate
Armor Break, Weapon Break, Dark Sword



TheChainNerd
Male
Cancer
54
45
Summoner
Black Magic
MA Save
Equip Sword
Waterbreathing

Defender

Green Beret
Wizard Robe
Diamond Armlet

Moogle, Titan, Carbunkle
Fire 3, Bolt 3, Ice 2, Ice 3, Ice 4, Death



HaateXIII
Male
Pisces
43
72
Chemist
Steal
Brave Up
Beastmaster
Ignore Height

Cute Bag

Twist Headband
Leather Outfit
Germinas Boots

Potion, Echo Grass, Soft
Steal Helmet, Steal Armor, Steal Shield, Leg Aim



Vicemond
Female
Virgo
57
59
Samurai
Throw
Parry
Equip Gun
Move+2

Bestiary

Mythril Helmet
Diamond Armor
Genji Gauntlet

Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa
Shuriken, Bomb

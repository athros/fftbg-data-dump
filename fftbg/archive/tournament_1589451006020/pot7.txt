Final Bets: green - 11 bets for 3,514G (27.0%, x2.71); white - 13 bets for 9,519G (73.0%, x0.37)

green bets:
sukotsuto: 1,000G (28.5%, 13,821G)
rocl: 1,000G (28.5%, 58,357G)
roqqqpsi: 256G (7.3%, 1,166G)
BlackfyreRoy: 208G (5.9%, 208G)
ar_tactic: 200G (5.7%, 38,826G)
SuicideKing_XIX: 200G (5.7%, 774G)
KasugaiRoastedPeas: 200G (5.7%, 5,172G)
7Cerulean7: 150G (4.3%, 15,986G)
Jeeboheebo: 100G (2.8%, 4,166G)
Lawndough: 100G (2.8%, 3,000G)
datadrivenbot: 100G (2.8%, 16,403G)

white bets:
volgrathemoose: 1,500G (15.8%, 5,789G)
Arcblazer23: 1,205G (12.7%, 1,205G)
Shakarak: 1,200G (12.6%, 32,873G)
BirbBrainsBot: 1,000G (10.5%, 58,001G)
reinoe: 1,000G (10.5%, 30,872G)
edgehead62888: 1,000G (10.5%, 6,866G)
Dexsana: 670G (7.0%, 670G)
BoneMiser: 596G (6.3%, 596G)
getthemoneyz: 412G (4.3%, 676,922G)
Strifu: 400G (4.2%, 1,925G)
ShintaroNayaka: 300G (3.2%, 3,062G)
otakutaylor: 136G (1.4%, 136G)
Evewho: 100G (1.1%, 16,579G)

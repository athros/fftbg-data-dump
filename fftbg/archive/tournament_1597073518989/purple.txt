Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheDeeyo
Male
Leo
59
39
Priest
Time Magic
Mana Shield
Short Charge
Lava Walking

Wizard Staff

Headgear
Chain Vest
Battle Boots

Cure, Cure 2, Cure 3, Raise, Reraise, Protect 2, Esuna
Haste, Immobilize, Quick



Fenaen
Female
Libra
49
68
Summoner
Charge
Caution
Short Charge
Move+3

Gold Staff

Flash Hat
Wizard Robe
Feather Mantle

Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Silf, Lich
Charge+1, Charge+3, Charge+4, Charge+5



NovaKnight21
Monster
Virgo
46
75
Revenant










Just Here2
Female
Virgo
57
46
Thief
Elemental
Regenerator
Maintenance
Waterwalking

Iron Sword

Leather Hat
Secret Clothes
Power Wrist

Steal Shield, Steal Weapon
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

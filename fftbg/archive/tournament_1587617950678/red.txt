Player: !Red
Team: Red Team
Palettes: Red/Brown



Superdevon1
Female
Aquarius
70
78
Time Mage
Draw Out
Parry
Martial Arts
Jump+2



Twist Headband
Silk Robe
Rubber Shoes

Haste, Haste 2, Immobilize, Quick, Stabilize Time
Muramasa, Kikuichimoji



Galkife
Male
Capricorn
40
59
Archer
Jump
Dragon Spirit
Sicken
Move+2

Silver Bow

Leather Hat
Power Sleeve
Defense Armlet

Charge+3
Level Jump5, Vertical Jump8



CapnChaos12
Male
Libra
67
52
Ninja
Punch Art
Damage Split
Equip Shield
Waterwalking

Dagger
Ice Shield
Golden Hairpin
Mythril Vest
Small Mantle

Shuriken, Dictionary
Pummel, Wave Fist, Purification, Revive



Wesablo
Male
Libra
81
79
Priest
Item
Counter Flood
Throw Item
Swim

Flame Whip

Black Hood
White Robe
Angel Ring

Cure, Cure 3, Cure 4, Raise, Reraise, Regen, Protect 2, Shell, Shell 2, Holy
Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water

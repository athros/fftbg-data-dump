Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Female
Aquarius
48
44
Thief
Battle Skill
Earplug
Concentrate
Waterwalking

Mythril Sword

Headgear
Leather Outfit
Feather Mantle

Gil Taking, Steal Weapon, Arm Aim, Leg Aim
Power Break



Butterbelljedi
Male
Libra
75
49
Monk
Time Magic
Auto Potion
Equip Sword
Swim

Rune Blade

Red Hood
Chain Vest
Power Wrist

Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil
Stop, Stabilize Time, Meteor



Wonser
Female
Taurus
80
53
Lancer
Charge
Damage Split
Defense UP
Jump+1

Obelisk
Crystal Shield
Diamond Helmet
Chameleon Robe
N-Kai Armlet

Level Jump2, Vertical Jump7
Charge+4



TakKerna
Female
Scorpio
56
61
Monk
Jump
Arrow Guard
Attack UP
Jump+2



Green Beret
Chain Vest
Jade Armlet

Pummel, Wave Fist, Secret Fist, Purification, Seal Evil
Level Jump8, Vertical Jump8

Player: !White
Team: White Team
Palettes: White/Blue



ZCKaiser
Female
Gemini
80
54
Oracle
Draw Out
Damage Split
Martial Arts
Ignore Height



Flash Hat
White Robe
Elf Mantle

Poison, Spell Absorb, Pray Faith, Doubt Faith, Foxbird, Dispel Magic
Murasame, Heaven's Cloud, Kiyomori



Gelwain
Female
Aries
54
75
Oracle
Black Magic
HP Restore
Equip Shield
Jump+2

Gokuu Rod
Bronze Shield
Feather Hat
Light Robe
Leather Mantle

Poison, Spell Absorb, Life Drain, Pray Faith, Zombie, Blind Rage, Confusion Song, Paralyze, Sleep, Dark Holy
Fire 2, Fire 4, Bolt 4, Death



PuzzleSecretary
Female
Cancer
67
58
Priest
Throw
Distribute
Magic Defense UP
Move+2

Healing Staff

Triangle Hat
Wizard Robe
Elf Mantle

Cure 2, Raise, Reraise, Regen, Shell, Wall, Esuna
Shuriken, Bomb, Ninja Sword



Runeseeker22
Male
Sagittarius
79
68
Thief
Sing
Faith Save
Defense UP
Waterwalking

Main Gauche

Triangle Hat
Chain Vest
Feather Mantle

Steal Heart, Steal Helmet, Steal Shield, Leg Aim
Angel Song, Life Song, Cheer Song, Magic Song, Nameless Song, Last Song, Hydra Pit

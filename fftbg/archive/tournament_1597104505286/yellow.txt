Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Aldrammech
Monster
Taurus
59
81
Juravis










Laserman1000
Male
Capricorn
65
65
Oracle
Battle Skill
Parry
Equip Armor
Waterwalking

Battle Bamboo

Iron Helmet
Chain Mail
Leather Mantle

Pray Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy
Head Break, Weapon Break, Magic Break, Surging Sword



Killth3kid
Male
Gemini
73
40
Archer
Black Magic
Auto Potion
Martial Arts
Jump+1


Crystal Shield
Bronze Helmet
Power Sleeve
Defense Armlet

Charge+1, Charge+3, Charge+20
Fire 3, Ice 2, Empower, Flare



Sinnyil2
Male
Cancer
71
59
Chemist
Jump
Counter Tackle
Equip Polearm
Levitate

Cashmere

Headgear
Mythril Vest
Leather Mantle

Potion, Holy Water, Phoenix Down
Level Jump2, Vertical Jump2

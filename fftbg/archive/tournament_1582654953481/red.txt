Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Scorpio
57
65
Archer
Time Magic
Parry
Martial Arts
Move-MP Up


Bronze Shield
Headgear
Mystic Vest
Feather Boots

Charge+1, Charge+5, Charge+7
Haste 2, Slow 2, Stop, Demi, Stabilize Time



Silgryn
Male
Scorpio
49
39
Mediator
Jump
Sunken State
Equip Polearm
Jump+3

Battle Bamboo

Headgear
Linen Robe
Magic Gauntlet

Invitation, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Rehabilitate
Level Jump5, Vertical Jump8



WhiteTigress
Female
Scorpio
64
58
Calculator
Yin Yang Magic
Counter Tackle
Defense UP
Fly

Battle Folio

Twist Headband
White Robe
Genji Gauntlet

CT, Height, Prime Number, 4
Blind, Spell Absorb, Pray Faith, Zombie, Blind Rage



Lowlf
Female
Sagittarius
74
76
Priest
Black Magic
Counter Tackle
Concentrate
Jump+2

Flail

Feather Hat
Black Robe
Dracula Mantle

Cure, Raise, Reraise, Regen, Protect, Shell 2, Esuna, Holy
Fire 2, Fire 3, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower

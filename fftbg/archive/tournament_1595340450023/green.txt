Player: !Green
Team: Green Team
Palettes: Green/White



Fenaen
Female
Virgo
49
80
Mediator
Elemental
Catch
Magic Defense UP
Ignore Height

Romanda Gun

Black Hood
Silk Robe
Small Mantle

Preach, Refute
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Zeroroute
Female
Leo
45
48
Squire
Punch Art
Arrow Guard
Long Status
Jump+1

Giant Axe
Platinum Shield
Golden Hairpin
Mythril Vest
Defense Armlet

Accumulate, Dash, Throw Stone, Heal, Fury, Wish, Scream
Wave Fist, Secret Fist, Purification



Mirapoix
Male
Virgo
53
43
Chemist
Battle Skill
Damage Split
Defense UP
Jump+3

Assassin Dagger

Leather Hat
Wizard Outfit
Vanish Mantle

Potion, X-Potion, Hi-Ether, Echo Grass
Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Night Sword



Error72
Female
Scorpio
67
53
Mime

Damage Split
Defense UP
Move-HP Up



Green Beret
Chain Vest
Chantage

Mimic


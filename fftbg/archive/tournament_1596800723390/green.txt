Player: !Green
Team: Green Team
Palettes: Green/White



Mesmaster
Male
Gemini
72
58
Squire
Sing
Damage Split
Beastmaster
Fly

Battle Axe
Platinum Shield
Thief Hat
Chain Mail
Small Mantle

Accumulate, Heal, Yell, Wish
Life Song, Battle Song, Nameless Song, Diamond Blade, Space Storage



Nok
Female
Capricorn
53
79
Geomancer
Summon Magic
Abandon
Short Status
Teleport

Giant Axe
Bronze Shield
Headgear
Adaman Vest
Cursed Ring

Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Moogle, Ramuh, Carbunkle, Leviathan, Fairy, Zodiac



Lokenwow
Monster
Cancer
54
39
Red Chocobo










Grininda
Male
Pisces
61
62
Knight
Yin Yang Magic
Faith Save
Concentrate
Teleport

Rune Blade
Platinum Shield
Iron Helmet
Leather Armor
Genji Gauntlet

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Night Sword
Doubt Faith, Blind Rage, Sleep

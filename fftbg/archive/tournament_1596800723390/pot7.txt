Final Bets: green - 8 bets for 3,786G (41.7%, x1.40); brown - 10 bets for 5,299G (58.3%, x0.71)

green bets:
powergems: 1,088G (28.7%, 1,088G)
Mesmaster: 668G (17.6%, 668G)
E_Ballard: 536G (14.2%, 536G)
Scurg: 500G (13.2%, 18,580G)
Error72: 424G (11.2%, 424G)
Galeronis: 220G (5.8%, 220G)
AllInBot: 200G (5.3%, 200G)
Aestheta: 150G (4.0%, 1,083G)

brown bets:
KevvTwo: 1,269G (23.9%, 1,269G)
UmaiJam: 1,100G (20.8%, 13,770G)
BirbBrainsBot: 997G (18.8%, 32,261G)
DesertWooder: 600G (11.3%, 2,125G)
pplvee1: 464G (8.8%, 1,083G)
Zeera98: 300G (5.7%, 3,365G)
gorgewall: 201G (3.8%, 3,176G)
Pbjellytimez: 200G (3.8%, 3,322G)
getthemoneyz: 118G (2.2%, 1,554,650G)
srdonko2098: 50G (0.9%, 2,759G)

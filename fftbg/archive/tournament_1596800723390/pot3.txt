Final Bets: white - 9 bets for 3,315G (39.6%, x1.53); black - 7 bets for 5,061G (60.4%, x0.66)

white bets:
SephDarkheart: 1,043G (31.5%, 1,043G)
getthemoneyz: 666G (20.1%, 1,551,375G)
VolgraTheMoose: 401G (12.1%, 2,609G)
Zeera98: 300G (9.0%, 4,091G)
DesertWooder: 300G (9.0%, 427G)
AllInBot: 254G (7.7%, 254G)
gorgewall: 201G (6.1%, 2,228G)
Scurg: 100G (3.0%, 14,986G)
srdonko2098: 50G (1.5%, 2,851G)

black bets:
Mesmaster: 1,502G (29.7%, 1,502G)
ColetteMSLP: 1,000G (19.8%, 29,445G)
BirbBrainsBot: 1,000G (19.8%, 34,627G)
Error72: 695G (13.7%, 695G)
E_Ballard: 536G (10.6%, 720G)
Aestheta: 228G (4.5%, 228G)
Pbjellytimez: 100G (2.0%, 2,000G)

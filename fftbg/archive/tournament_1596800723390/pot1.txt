Final Bets: red - 10 bets for 5,697G (58.5%, x0.71); blue - 9 bets for 4,038G (41.5%, x1.41)

red bets:
UmaiJam: 1,100G (19.3%, 14,891G)
BirbBrainsBot: 1,000G (17.6%, 33,646G)
defaultlybrave: 1,000G (17.6%, 8,240G)
Error72: 780G (13.7%, 780G)
Mesmaster: 691G (12.1%, 691G)
SephDarkheart: 480G (8.4%, 480G)
getthemoneyz: 346G (6.1%, 1,550,858G)
ArlanKels: 100G (1.8%, 4,885G)
srdonko2098: 100G (1.8%, 2,766G)
gargola2498: 100G (1.8%, 4,613G)

blue bets:
ColetteMSLP: 1,000G (24.8%, 31,445G)
powergems: 1,000G (24.8%, 2,088G)
E_Ballard: 536G (13.3%, 1,256G)
VolgraTheMoose: 501G (12.4%, 3,110G)
DesertWooder: 300G (7.4%, 1,027G)
gorgewall: 201G (5.0%, 2,630G)
AllInBot: 200G (5.0%, 200G)
Pbjellytimez: 200G (5.0%, 2,200G)
Zeera98: 100G (2.5%, 4,137G)

Final Bets: brown - 8 bets for 7,426G (52.0%, x0.92); champion - 12 bets for 6,860G (48.0%, x1.08)

brown bets:
Zeera98: 3,579G (48.2%, 3,579G)
SephDarkheart: 1,293G (17.4%, 1,293G)
Aestheta: 933G (12.6%, 933G)
DesertWooder: 500G (6.7%, 2,554G)
gargola2498: 500G (6.7%, 4,591G)
Galeronis: 220G (3.0%, 220G)
gorgewall: 201G (2.7%, 3,324G)
AllInBot: 200G (2.7%, 200G)

champion bets:
UmaiJam: 1,100G (16.0%, 14,556G)
JumbocactuarX27: 1,000G (14.6%, 43,517G)
BirbBrainsBot: 1,000G (14.6%, 32,973G)
grininda: 1,000G (14.6%, 6,891G)
Mesmaster: 668G (9.7%, 668G)
E_Ballard: 536G (7.8%, 536G)
powergems: 428G (6.2%, 428G)
Forkmore: 416G (6.1%, 1,690G)
getthemoneyz: 348G (5.1%, 1,554,734G)
latebit: 164G (2.4%, 164G)
srdonko2098: 100G (1.5%, 2,795G)
Pbjellytimez: 100G (1.5%, 3,465G)

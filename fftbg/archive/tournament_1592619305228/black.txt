Player: !Black
Team: Black Team
Palettes: Black/Red



Reinoe
Female
Serpentarius
80
63
Knight
Talk Skill
HP Restore
Dual Wield
Move-HP Up

Broad Sword
Rune Blade
Genji Helmet
Carabini Mail
Wizard Mantle

Head Break, Weapon Break, Power Break, Mind Break, Dark Sword, Night Sword, Surging Sword
Invitation, Persuade, Threaten, Negotiate, Rehabilitate



Lythe Caraker
Male
Leo
70
77
Samurai
Sing
Distribute
Equip Gun
Jump+1

Mythril Gun

Crystal Helmet
Plate Mail
Elf Mantle

Heaven's Cloud, Kiyomori
Last Song



Nizaha
Male
Pisces
70
74
Squire
Black Magic
Earplug
Short Status
Levitate

Battle Axe
Genji Shield
Bronze Helmet
Chain Vest
Elf Mantle

Accumulate, Dash, Throw Stone, Heal, Tickle
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Frog, Flare



Mesmaster
Male
Capricorn
49
54
Lancer
Charge
Arrow Guard
Doublehand
Waterbreathing

Obelisk

Iron Helmet
Crystal Mail
Defense Armlet

Level Jump2, Vertical Jump3
Charge+1, Charge+4

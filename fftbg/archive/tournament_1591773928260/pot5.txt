Final Bets: red - 8 bets for 3,535G (38.6%, x1.59); green - 11 bets for 5,623G (61.4%, x0.63)

red bets:
Evewho: 1,460G (41.3%, 2,864G)
BirbBrainsBot: 663G (18.8%, 71,381G)
prince_rogers_nelson_: 500G (14.1%, 4,710G)
Sairentozon7: 320G (9.1%, 320G)
ColetteMSLP: 300G (8.5%, 2,237G)
astrozin11: 150G (4.2%, 2,200G)
SincereName: 100G (2.8%, 1,029G)
getthemoneyz: 42G (1.2%, 888,857G)

green bets:
Mesmaster: 2,000G (35.6%, 4,632G)
Lydian_C: 1,200G (21.3%, 15,413G)
Kalabain: 800G (14.2%, 30,265G)
CrownOfHorns: 717G (12.8%, 717G)
ChronoG: 244G (4.3%, 244G)
almontys: 196G (3.5%, 196G)
AllInBot: 100G (1.8%, 100G)
DeathTaxesAndAnime: 100G (1.8%, 15,522G)
datadrivenbot: 100G (1.8%, 37,322G)
reinoe: 100G (1.8%, 22,149G)
VidMonkey: 66G (1.2%, 166G)

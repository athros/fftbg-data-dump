Player: !Red
Team: Red Team
Palettes: Red/Brown



JonnyCue
Male
Libra
60
80
Thief
Jump
Counter
Equip Armor
Ignore Height

Dagger

Genji Helmet
Plate Mail
Genji Gauntlet

Steal Heart, Arm Aim, Leg Aim
Level Jump3, Vertical Jump2



Booty Scope
Male
Libra
54
76
Calculator
Yin Yang Magic
MA Save
Short Status
Levitate

Dragon Rod

Green Beret
Secret Clothes
Leather Mantle

Height, Prime Number, 5, 3
Poison, Pray Faith, Doubt Faith, Confusion Song, Petrify



Technominari
Female
Gemini
77
78
Priest
Draw Out
Counter Magic
Short Charge
Swim

Healing Staff

Feather Hat
Power Sleeve
Defense Ring

Cure, Raise, Reraise, Regen, Protect, Shell 2, Esuna
Koutetsu, Bizen Boat, Heaven's Cloud



RagequitSA
Male
Aries
76
55
Monk
Elemental
Regenerator
Equip Gun
Ignore Terrain

Ramia Harp

Red Hood
Wizard Outfit
Feather Boots

Secret Fist, Purification, Revive, Seal Evil
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball

Player: !Black
Team: Black Team
Palettes: Black/Red



Captainmilestw
Female
Libra
66
40
Calculator
Time Magic
Parry
Concentrate
Move+2

Papyrus Codex

Golden Hairpin
Silk Robe
Defense Armlet

CT, Prime Number, 5, 4, 3
Haste 2, Slow 2, Quick, Demi



PuzzleSecretary
Male
Taurus
60
54
Wizard
Sing
Auto Potion
Short Status
Teleport

Ice Rod

Holy Miter
White Robe
Defense Armlet

Bolt 2, Empower, Flare
Angel Song, Cheer Song, Battle Song



DavenIII
Male
Gemini
51
41
Lancer
Throw
Distribute
Short Status
Waterbreathing

Ivory Rod
Diamond Shield
Diamond Helmet
Carabini Mail
Rubber Shoes

Level Jump2, Vertical Jump4
Shuriken, Dictionary



Chuckolator
Female
Taurus
66
45
Squire
Draw Out
Counter
Martial Arts
Move-HP Up


Crystal Shield
Holy Miter
Mystic Vest
Sprint Shoes

Dash, Throw Stone, Heal, Tickle, Wish
Asura, Kikuichimoji

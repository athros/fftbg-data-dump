Player: !Red
Team: Red Team
Palettes: Red/Brown



ForagerCats
Female
Virgo
72
51
Time Mage
Throw
Critical Quick
Equip Gun
Waterwalking

Mythril Gun

Black Hood
Black Robe
Small Mantle

Haste, Slow, Reflect, Quick, Demi 2
Staff



The Adversary
Monster
Taurus
78
57
Taiju










Maximumcrit
Female
Aries
63
46
Calculator
Limit
Hamedo
Equip Gun
Ignore Height

Mythril Gun

Mythril Helmet
Diamond Armor
Wizard Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



EmmaEnema
Female
Sagittarius
56
54
Oracle
Charge
Critical Quick
Martial Arts
Jump+1



Green Beret
Wizard Outfit
Feather Mantle

Poison, Life Drain, Pray Faith, Blind Rage, Dispel Magic, Paralyze
Charge+1, Charge+10

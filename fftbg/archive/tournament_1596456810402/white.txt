Player: !White
Team: White Team
Palettes: White/Blue



ALY327
Male
Sagittarius
53
81
Mime

MA Save
Equip Armor
Move+1



Circlet
Linen Robe
Dracula Mantle

Mimic




NIghtdew14
Male
Leo
70
52
Lancer
Talk Skill
Arrow Guard
Beastmaster
Retreat

Holy Lance
Genji Shield
Platinum Helmet
Black Robe
Sprint Shoes

Level Jump4, Vertical Jump8
Invitation, Persuade, Threaten, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



Nok
Female
Scorpio
39
73
Time Mage
Steal
Absorb Used MP
Maintenance
Move-HP Up

Mace of Zeus

Headgear
Brigandine
Red Shoes

Slow 2, Float, Reflect, Quick, Demi, Stabilize Time, Meteor
Gil Taking, Steal Heart, Steal Status, Arm Aim



LDSkinny
Female
Capricorn
51
68
Oracle
Battle Skill
Auto Potion
Beastmaster
Waterwalking

Bestiary

Feather Hat
White Robe
Dracula Mantle

Blind, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Head Break, Shield Break, Power Break, Dark Sword, Surging Sword

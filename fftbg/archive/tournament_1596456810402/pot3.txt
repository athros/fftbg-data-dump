Final Bets: white - 9 bets for 21,849G (73.8%, x0.35); black - 13 bets for 7,756G (26.2%, x2.82)

white bets:
VolgraTheMoose: 16,425G (75.2%, 32,851G)
maximumcrit: 1,527G (7.0%, 1,527G)
LDSkinny: 1,000G (4.6%, 7,827G)
LordTomS: 1,000G (4.6%, 6,391G)
E_Ballard: 876G (4.0%, 876G)
rednecknazgul: 641G (2.9%, 641G)
datadrivenbot: 200G (0.9%, 48,605G)
EmmaEnema: 100G (0.5%, 2,199G)
xBizzy: 80G (0.4%, 803G)

black bets:
UmaiJam: 3,000G (38.7%, 44,518G)
BirbBrainsBot: 1,000G (12.9%, 180,973G)
randgridr: 1,000G (12.9%, 14,610G)
getthemoneyz: 538G (6.9%, 1,494,401G)
ruleof5: 500G (6.4%, 5,564G)
AllInBot: 438G (5.6%, 438G)
Error72: 404G (5.2%, 404G)
latebit: 220G (2.8%, 220G)
CIairebear: 200G (2.6%, 10,995G)
DavenIII: 196G (2.5%, 364G)
RunicMagus: 100G (1.3%, 1,466G)
DesertWooder: 100G (1.3%, 800G)
ArlanKels: 60G (0.8%, 1,506G)

Player: !Green
Team: Green Team
Palettes: Green/White



Sun Pixie
Monster
Virgo
72
74
Dragon










Lydian C
Female
Leo
55
52
Lancer
Punch Art
Damage Split
Dual Wield
Move-HP Up

Javelin
Octagon Rod
Platinum Helmet
Linen Cuirass
Magic Ring

Level Jump3, Vertical Jump8
Spin Fist, Pummel, Wave Fist, Revive



AdaephonD
Female
Libra
79
45
Geomancer
Battle Skill
Regenerator
Equip Sword
Swim

Muramasa
Ice Shield
Flash Hat
Light Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Gusty Wind
Head Break, Shield Break, Magic Break, Speed Break, Power Break, Stasis Sword



Oogthecaveman
Male
Taurus
52
44
Lancer
Elemental
Parry
Doublehand
Waterbreathing

Spear

Cross Helmet
Leather Armor
Elf Mantle

Level Jump5, Vertical Jump2
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

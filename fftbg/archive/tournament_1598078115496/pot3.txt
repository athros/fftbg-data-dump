Final Bets: white - 12 bets for 13,856G (78.7%, x0.27); black - 7 bets for 3,744G (21.3%, x3.70)

white bets:
neerrm: 7,777G (56.1%, 40,749G)
NicoSavoy: 1,674G (12.1%, 33,486G)
gorgewall: 1,322G (9.5%, 1,322G)
JCBooBot: 1,000G (7.2%, 18,567G)
latebit: 661G (4.8%, 661G)
DeathTaxesAndAnime: 420G (3.0%, 2,668G)
rNdOrchestra: 248G (1.8%, 979G)
datadrivenbot: 200G (1.4%, 61,702G)
ProteinBiscuit: 200G (1.4%, 3,967G)
AllInBot: 154G (1.1%, 154G)
cloudycube: 100G (0.7%, 790G)
jjhull3: 100G (0.7%, 788G)

black bets:
BirbBrainsBot: 1,000G (26.7%, 120,646G)
Sairentozon7: 1,000G (26.7%, 27,565G)
getthemoneyz: 502G (13.4%, 1,704,554G)
CT_5_Holy: 376G (10.0%, 3,764G)
lowlf: 368G (9.8%, 3,587G)
PuzzleSecretary: 250G (6.7%, 416G)
Humble_Fabio: 248G (6.6%, 248G)

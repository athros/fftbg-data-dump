Player: !Green
Team: Green Team
Palettes: Green/White



Hyvi
Male
Gemini
40
50
Oracle
Sing
PA Save
Attack UP
Levitate

Gokuu Rod

Holy Miter
Chameleon Robe
Germinas Boots

Life Drain, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Life Song, Cheer Song, Nameless Song, Sky Demon, Hydra Pit



Leakimiko
Female
Aries
70
56
Oracle
Time Magic
MA Save
Equip Armor
Waterbreathing

Battle Bamboo

Diamond Helmet
Linen Robe
N-Kai Armlet

Life Drain, Doubt Faith, Zombie, Dispel Magic, Sleep, Petrify, Dark Holy
Haste 2, Slow, Slow 2, Quick, Demi 2, Stabilize Time, Meteor



Pah J
Female
Pisces
49
73
Time Mage
Black Magic
Arrow Guard
Attack UP
Jump+3

Bestiary

Green Beret
White Robe
Germinas Boots

Haste 2, Slow, Stop, Immobilize, Demi, Stabilize Time
Fire, Fire 3, Bolt, Bolt 2, Ice, Ice 4, Empower, Frog



RoamingUragaan
Female
Virgo
68
79
Monk
Jump
Parry
Doublehand
Jump+1



Triangle Hat
Black Costume
108 Gems

Secret Fist, Chakra
Level Jump5, Vertical Jump2

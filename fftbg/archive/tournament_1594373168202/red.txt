Player: !Red
Team: Red Team
Palettes: Red/Brown



Randgridr
Male
Taurus
45
73
Chemist
White Magic
Regenerator
Equip Polearm
Move+1

Javelin

Red Hood
Leather Outfit
Defense Armlet

X-Potion, Holy Water, Remedy, Phoenix Down
Raise, Reraise, Holy



Evewho
Female
Libra
72
61
Archer
Battle Skill
Parry
Attack UP
Move+3

Mythril Gun
Ice Shield
Leather Hat
Black Costume
Wizard Mantle

Charge+3, Charge+4
Magic Break, Power Break



TeaTime29
Female
Taurus
73
42
Squire
Jump
HP Restore
Equip Polearm
Waterwalking

Obelisk
Gold Shield
Holy Miter
Mythril Armor
N-Kai Armlet

Throw Stone, Heal, Yell
Level Jump4, Vertical Jump4



Superdevon1
Male
Virgo
57
80
Squire
Summon Magic
MP Restore
Maintenance
Ignore Height

Iron Sword
Diamond Shield
Thief Hat
Judo Outfit
Chantage

Throw Stone, Heal, Yell, Cheer Up, Fury, Scream
Moogle, Shiva, Odin, Leviathan, Fairy, Lich

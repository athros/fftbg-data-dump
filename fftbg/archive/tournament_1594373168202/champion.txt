Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lemonjohns
Female
Virgo
53
67
Priest
Battle Skill
Auto Potion
Defense UP
Move-HP Up

Scorpion Tail

Twist Headband
White Robe
Setiemson

Cure, Cure 4, Raise, Protect, Wall, Esuna, Holy, Magic Barrier
Power Break, Dark Sword



Zetchryn
Male
Gemini
63
77
Thief
Draw Out
Arrow Guard
Long Status
Lava Walking

Assassin Dagger

Feather Hat
Wizard Outfit
Diamond Armlet

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Arm Aim
Koutetsu, Heaven's Cloud, Masamune



ApplesauceBoss
Female
Taurus
52
46
Ninja
Yin Yang Magic
Counter Tackle
Equip Gun
Levitate

Blaze Gun
Blaze Gun
Thief Hat
Brigandine
Sprint Shoes

Shuriken, Bomb, Knife, Dictionary
Zombie, Blind Rage, Dispel Magic, Sleep, Petrify, Dark Holy



CT 5 Holy
Female
Aries
56
58
Lancer
Draw Out
Regenerator
Doublehand
Move+3

Holy Lance

Diamond Helmet
Wizard Robe
Wizard Mantle

Level Jump2, Vertical Jump8
Koutetsu, Kiyomori, Kikuichimoji

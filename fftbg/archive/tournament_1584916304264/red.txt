Player: !Red
Team: Red Team
Palettes: Red/Brown



Ar Tactic
Female
Serpentarius
72
71
Thief
Punch Art
Meatbone Slash
Long Status
Jump+2

Air Knife

Holy Miter
Power Sleeve
Wizard Mantle

Steal Weapon, Steal Status, Arm Aim
Secret Fist, Purification, Seal Evil



GreatRedDragon
Female
Taurus
70
52
Knight
Talk Skill
Counter Flood
Beastmaster
Move-MP Up

Defender

Leather Helmet
Carabini Mail
Angel Ring

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break
Invitation, Persuade, Threaten, Solution, Insult, Refute



PleXmito
Male
Gemini
46
66
Monk
Elemental
Catch
Halve MP
Teleport



Flash Hat
Power Sleeve
Bracer

Pummel, Wave Fist, Purification, Revive, Seal Evil
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



TrogdorTheMemeinator
Female
Virgo
76
60
Geomancer
Jump
Counter Flood
Magic Attack UP
Move+1

Giant Axe
Crystal Shield
Leather Hat
White Robe
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump8

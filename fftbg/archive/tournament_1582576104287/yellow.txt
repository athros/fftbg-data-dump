Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BigDLicious91
Male
Scorpio
47
73
Wizard
Basic Skill
Counter Flood
Equip Bow
Teleport

Ultimus Bow

Green Beret
Wizard Outfit
Sprint Shoes

Fire 3, Bolt, Bolt 2, Ice 2
Dash, Heal, Yell, Scream



Red  Lancer
Male
Capricorn
76
60
Summoner
Throw
Parry
Equip Knife
Lava Walking

Thunder Rod

Red Hood
Linen Robe
Elf Mantle

Ramuh, Odin, Leviathan, Silf, Fairy, Lich
Shuriken, Dictionary



RedGrinGrumbl3
Male
Aquarius
80
79
Monk
Charge
Sunken State
Attack UP
Move+2



Thief Hat
Clothes
Battle Boots

Spin Fist, Wave Fist, Chakra, Revive
Charge+5, Charge+7



MrUbiq
Female
Cancer
72
38
Calculator
White Magic
Earplug
Halve MP
Waterwalking

Papyrus Codex

Green Beret
Chameleon Robe
Diamond Armlet

CT, 5, 3
Raise, Raise 2, Protect, Shell, Wall, Esuna, Holy

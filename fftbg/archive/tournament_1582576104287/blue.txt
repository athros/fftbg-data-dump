Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ominnous
Male
Libra
56
79
Priest
Yin Yang Magic
Arrow Guard
Short Charge
Waterbreathing

White Staff

Flash Hat
Chameleon Robe
Red Shoes

Cure 2, Cure 3, Cure 4, Raise 2, Esuna
Pray Faith, Foxbird, Dispel Magic, Paralyze



MrFlabyo
Female
Libra
73
51
Samurai
Jump
Counter Magic
Short Status
Move+1

Murasame

Crystal Helmet
Linen Robe
Defense Armlet

Asura, Koutetsu, Bizen Boat
Level Jump2, Vertical Jump6



RampantMind
Female
Capricorn
71
53
Summoner
Time Magic
Counter Tackle
Halve MP
Jump+3

Wizard Rod

Cachusha
Linen Robe
Spike Shoes

Moogle, Ramuh, Golem, Bahamut, Odin, Salamander
Haste, Haste 2, Slow, Reflect, Quick, Demi



Sociallubricant1
Female
Serpentarius
46
74
Time Mage
Basic Skill
Counter Magic
Martial Arts
Lava Walking



Green Beret
Secret Clothes
Feather Boots

Haste, Slow, Float, Reflect, Demi
Accumulate, Dash, Yell, Cheer Up

Player: !Red
Team: Red Team
Palettes: Red/Brown



Dothan89
Male
Taurus
42
65
Time Mage
Black Magic
Damage Split
Equip Bow
Retreat

Mythril Bow

Green Beret
White Robe
Red Shoes

Haste 2, Slow, Demi 2, Stabilize Time, Meteor
Fire, Fire 2, Bolt 2, Ice 4



Ant Ihistamine
Monster
Pisces
63
45
Reaper










Denamda
Female
Aquarius
48
68
Wizard
Yin Yang Magic
Counter Tackle
Equip Gun
Retreat

Papyrus Codex

Twist Headband
Leather Outfit
Magic Ring

Fire 3, Empower, Frog
Blind Rage, Foxbird, Dispel Magic



Bryon W
Male
Virgo
59
60
Archer
White Magic
Speed Save
Short Charge
Lava Walking

Mythril Bow

Red Hood
Adaman Vest
Power Wrist

Charge+7, Charge+10
Cure, Cure 3, Cure 4, Raise, Raise 2, Shell, Shell 2, Esuna

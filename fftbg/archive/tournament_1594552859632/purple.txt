Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SeniorBunk
Female
Aquarius
65
81
Monk
Talk Skill
HP Restore
Dual Wield
Waterbreathing



Leather Hat
Earth Clothes
Genji Gauntlet

Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Persuade, Praise, Threaten, Death Sentence, Negotiate, Refute



Bruubarg
Female
Cancer
65
74
Thief
Punch Art
Catch
Equip Armor
Retreat

Rune Blade

Diamond Helmet
Bronze Armor
Wizard Mantle

Steal Helmet, Steal Armor, Steal Weapon
Earth Slash, Chakra



Quadh0nk
Male
Virgo
73
71
Mime

Counter
Short Status
Move+1



Golden Hairpin
Judo Outfit
Germinas Boots

Mimic




Iamos
Male
Scorpio
57
75
Calculator
Yin Yang Magic
Caution
Equip Sword
Move-HP Up

Murasame

Flash Hat
Leather Outfit
Diamond Armlet

Height, Prime Number, 3
Blind, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Sleep, Petrify

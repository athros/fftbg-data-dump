Player: !Red
Team: Red Team
Palettes: Red/Brown



SephDarkheart
Female
Taurus
67
46
Geomancer
Steal
Regenerator
Doublehand
Ignore Terrain

Long Sword

Leather Hat
Silk Robe
Cursed Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Gil Taking, Steal Heart, Steal Helmet, Steal Weapon



Actual JP
Male
Virgo
57
48
Chemist
Charge
Mana Shield
Maintenance
Swim

Romanda Gun

Black Hood
Judo Outfit
Small Mantle

Potion, Antidote, Eye Drop, Echo Grass
Charge+1, Charge+4, Charge+7



E Ballard
Male
Gemini
52
69
Knight
Draw Out
Counter Flood
Magic Defense UP
Waterwalking

Sleep Sword
Escutcheon
Genji Helmet
Linen Robe
N-Kai Armlet

Speed Break, Mind Break
Heaven's Cloud



Wurstwaesserchen
Female
Gemini
63
62
Summoner
Yin Yang Magic
Sunken State
Short Charge
Levitate

Poison Rod

Leather Hat
Brigandine
Small Mantle

Shiva, Ramuh, Titan, Golem, Carbunkle, Bahamut, Silf
Blind, Life Drain, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic

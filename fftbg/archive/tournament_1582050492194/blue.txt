Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Galkife
Male
Leo
51
59
Mime

MP Restore
Equip Shield
Levitate


Aegis Shield
Holy Miter
Chain Vest
Leather Mantle

Mimic




HaplessOne
Female
Gemini
77
43
Wizard
Elemental
Counter Magic
Magic Attack UP
Swim

Faith Rod

Thief Hat
White Robe
N-Kai Armlet

Fire, Fire 3, Ice 3, Empower, Frog
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Winterharte
Monster
Taurus
81
64
Red Dragon










MrUbiq
Male
Leo
74
53
Samurai
Throw
PA Save
Martial Arts
Move+3

Holy Lance

Circlet
Chain Mail
Leather Mantle

Muramasa, Kikuichimoji
Hammer, Staff

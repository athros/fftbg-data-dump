Player: !Red
Team: Red Team
Palettes: Red/Brown



Laserman1000
Male
Taurus
52
77
Archer
Sing
Regenerator
Long Status
Waterbreathing

Blaze Gun
Kaiser Plate
Iron Helmet
Black Costume
Leather Mantle

Charge+3, Charge+7, Charge+10, Charge+20
Life Song, Hydra Pit



Chaerie
Female
Leo
58
70
Geomancer
Jump
Critical Quick
Equip Axe
Levitate

Flame Whip
Crystal Shield
Black Hood
Wizard Robe
Power Wrist

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Level Jump5, Vertical Jump4



Rabbitlogik
Male
Gemini
73
54
Bard
Summon Magic
MP Restore
Equip Gun
Waterwalking

Romanda Gun

Flash Hat
Adaman Vest
Magic Ring

Life Song, Battle Song, Sky Demon
Moogle, Ramuh, Titan, Carbunkle, Bahamut, Odin, Fairy, Lich



Lowlf
Male
Gemini
61
66
Monk
Summon Magic
Sunken State
Dual Wield
Swim



Green Beret
Earth Clothes
Sprint Shoes

Spin Fist, Purification, Revive, Seal Evil
Shiva, Ramuh, Carbunkle, Leviathan, Salamander, Fairy, Cyclops

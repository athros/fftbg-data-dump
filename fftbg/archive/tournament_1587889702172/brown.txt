Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ASimpleSpaceBee
Male
Gemini
45
72
Samurai
Sing
Counter Tackle
Secret Hunt
Jump+2

Bizen Boat

Mythril Helmet
Leather Armor
Diamond Armlet

Asura, Bizen Boat, Kiyomori, Muramasa
Cheer Song, Battle Song, Space Storage



EnemyController
Female
Capricorn
72
63
Chemist
Jump
Absorb Used MP
Long Status
Jump+1

Panther Bag

Green Beret
Chain Vest
Genji Gauntlet

Elixir, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy
Level Jump8, Vertical Jump7



Notazzie
Male
Sagittarius
66
43
Squire
Summon Magic
Dragon Spirit
Attack UP
Fly

Gastrafitis
Platinum Shield
Flash Hat
Chain Mail
Spike Shoes

Accumulate, Dash, Heal, Yell, Wish, Scream
Shiva, Ifrit, Carbunkle



Alrightbye
Male
Scorpio
65
70
Calculator
Black Magic
Auto Potion
Beastmaster
Teleport 2

Papyrus Codex

Black Hood
Linen Robe
Defense Armlet

CT, Prime Number, 4
Fire, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Empower, Frog

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



MrMarkPolo
Female
Aries
54
42
Thief
Draw Out
Dragon Spirit
Magic Defense UP
Move-HP Up

Hidden Knife

Twist Headband
Brigandine
Power Wrist

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Leg Aim
Asura, Kiyomori, Kikuichimoji



HaateXIII
Female
Gemini
65
78
Geomancer
Summon Magic
PA Save
Defend
Waterwalking

Slasher
Crystal Shield
Golden Hairpin
Black Costume
Red Shoes

Pitfall, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Ramuh, Carbunkle



Geffro1908
Male
Virgo
55
79
Summoner
Time Magic
Hamedo
Sicken
Teleport

Panther Bag

Flash Hat
Earth Clothes
Reflect Ring

Moogle, Ramuh, Ifrit, Titan, Salamander, Silf, Fairy, Lich
Haste 2, Slow, Quick, Demi 2, Stabilize Time, Meteor



Joewcarson
Female
Pisces
69
54
Calculator
Time Magic
Counter Magic
Maintenance
Fly

Battle Folio

Thief Hat
Judo Outfit
Leather Mantle

CT, Height, Prime Number, 5
Haste, Haste 2, Demi, Demi 2, Stabilize Time, Meteor

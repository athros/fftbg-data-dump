Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Goust18
Female
Sagittarius
41
48
Lancer
Yin Yang Magic
HP Restore
Doublehand
Teleport

Cypress Rod

Diamond Helmet
Gold Armor
Defense Armlet

Level Jump8, Vertical Jump6
Poison, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy



Zebobz
Female
Libra
74
41
Priest
Draw Out
Counter Flood
Defense UP
Move+2

Oak Staff

Black Hood
Silk Robe
Rubber Shoes

Cure, Raise, Reraise, Regen, Esuna
Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



K1ngofthechill
Male
Virgo
68
63
Wizard
Talk Skill
Speed Save
Equip Bow
Ignore Terrain

Poison Bow

Green Beret
Mystic Vest
Spike Shoes

Fire 2, Fire 3, Bolt, Ice, Empower
Persuade, Praise, Refute



Ps36wii
Male
Sagittarius
54
50
Time Mage
Charge
Regenerator
Equip Polearm
Teleport

Javelin

Golden Hairpin
Earth Clothes
Rubber Shoes

Haste 2, Float, Quick, Demi 2, Stabilize Time, Galaxy Stop
Charge+2, Charge+4

Player: !Black
Team: Black Team
Palettes: Black/Red



Actual JP
Male
Capricorn
75
68
Wizard
White Magic
MA Save
Equip Shield
Waterbreathing

Assassin Dagger
Aegis Shield
Red Hood
Silk Robe
Bracer

Fire, Fire 3, Fire 4, Bolt, Bolt 3, Ice, Ice 2, Ice 3, Death
Cure, Cure 2, Raise, Protect, Esuna



HaateXIII
Female
Cancer
67
62
Samurai
Charge
Parry
Dual Wield
Ignore Height

Asura Knife
Kikuichimoji
Cross Helmet
Chain Mail
Wizard Mantle

Asura, Bizen Boat, Murasame, Kiyomori, Muramasa
Charge+1, Charge+2, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20



DesertWooder
Female
Pisces
78
73
Samurai
Charge
Catch
Sicken
Move-MP Up

Bizen Boat

Diamond Helmet
Black Robe
Red Shoes

Asura, Murasame, Kiyomori
Charge+5



Lowlf
Female
Libra
79
77
Oracle
White Magic
Parry
Doublehand
Ignore Height

Whale Whisker

Feather Hat
Adaman Vest
Battle Boots

Spell Absorb, Pray Faith, Foxbird, Confusion Song, Dispel Magic
Cure 4, Raise, Protect 2, Wall, Esuna, Holy

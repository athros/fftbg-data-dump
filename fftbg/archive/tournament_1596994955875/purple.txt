Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zephyrik
Female
Leo
43
64
Oracle
Black Magic
Earplug
Long Status
Jump+1

Bestiary

Headgear
Chameleon Robe
N-Kai Armlet

Blind, Poison, Spell Absorb, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify
Fire 3, Bolt 2, Bolt 4, Ice, Ice 3



Firesheath
Female
Capricorn
63
64
Mediator
Black Magic
Distribute
Equip Knife
Waterbreathing

Cultist Dagger

Triangle Hat
Silk Robe
Red Shoes

Invitation, Persuade, Preach, Mimic Daravon, Refute
Fire 2, Fire 3, Bolt, Bolt 3, Ice, Ice 3, Frog, Flare



SkylerBunny
Female
Leo
56
73
Geomancer
Basic Skill
Mana Shield
Equip Bow
Levitate

Ice Bow

Headgear
Black Costume
Cursed Ring

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Heal, Tickle, Cheer Up, Fury, Scream



Chuckolator
Female
Libra
56
53
Mediator
Throw
Absorb Used MP
Equip Gun
Waterwalking

Madlemgen

Golden Hairpin
Linen Robe
Spike Shoes

Invitation, Persuade, Threaten, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Shuriken, Staff

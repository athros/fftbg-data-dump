Player: !Green
Team: Green Team
Palettes: Green/White



Neetneph
Female
Capricorn
39
48
Squire
Throw
Parry
Equip Armor
Retreat

Blood Sword
Bronze Shield
Golden Hairpin
Gold Armor
Germinas Boots

Accumulate, Heal, Yell, Wish
Knife



Insurmountablebluff
Monster
Sagittarius
51
55
Ultima Demon










Beastmages
Male
Aries
44
64
Lancer
Draw Out
Brave Up
Attack UP
Jump+3

Musk Rod
Aegis Shield
Platinum Helmet
Diamond Armor
Jade Armlet

Level Jump5, Vertical Jump7
Koutetsu, Heaven's Cloud, Kikuichimoji



EvilLego6
Male
Scorpio
53
52
Priest
Draw Out
Critical Quick
Magic Attack UP
Teleport

Wizard Staff

Leather Hat
Black Robe
Red Shoes

Cure, Raise, Raise 2, Regen, Protect 2, Shell 2, Wall, Esuna, Holy
Koutetsu, Bizen Boat, Murasame

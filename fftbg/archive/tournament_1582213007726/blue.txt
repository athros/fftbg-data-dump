Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



FrostWalrusMan
Male
Taurus
66
68
Calculator
Time Magic
Brave Up
Martial Arts
Move+1



Twist Headband
Black Robe
Power Wrist

Height, Prime Number, 4, 3
Slow 2, Float, Reflect, Demi, Stabilize Time



MattmanX311
Male
Cancer
53
59
Ninja
Jump
MP Restore
Defend
Teleport

Dagger
Morning Star
Black Hood
Mystic Vest
Diamond Armlet

Shuriken, Bomb, Stick, Dictionary
Level Jump2, Vertical Jump2



Lanshaft
Female
Leo
68
44
Chemist
Draw Out
HP Restore
Halve MP
Jump+2

Hydra Bag

Holy Miter
Mythril Vest
Feather Boots

Potion, Hi-Ether, Phoenix Down
Bizen Boat, Heaven's Cloud



Gamesage53
Male
Aries
61
57
Monk
Item
PA Save
Throw Item
Move+1



Headgear
Mythril Vest
Diamond Armlet

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Seal Evil
Potion, Hi-Potion, Holy Water, Phoenix Down

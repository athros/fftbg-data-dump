Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Twelfthrootoftwo
Female
Pisces
80
78
Wizard
Draw Out
Parry
Short Charge
Teleport

Ice Rod

Triangle Hat
Mythril Vest
Defense Armlet

Fire, Fire 2, Fire 3, Bolt 2, Bolt 4, Empower, Death, Flare
Koutetsu, Bizen Boat, Heaven's Cloud, Chirijiraden



UmaiJam
Female
Cancer
41
53
Dancer
Charge
Earplug
Secret Hunt
Ignore Height

Persia

Twist Headband
Adaman Vest
Rubber Shoes

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Obsidian Blade, Void Storage
Charge+2, Charge+5



Mesmaster
Female
Gemini
49
44
Calculator
Time Magic
Sunken State
Dual Wield
Ignore Height

Bestiary
Madlemgen
Green Beret
Power Sleeve
Magic Gauntlet

CT, Height, 4, 3
Haste, Stop, Quick, Demi



SeniorBunk
Female
Leo
50
79
Ninja
Summon Magic
Speed Save
Sicken
Move-MP Up

Mage Masher
Assassin Dagger
Green Beret
Mystic Vest
Jade Armlet

Knife, Staff
Moogle, Golem, Carbunkle

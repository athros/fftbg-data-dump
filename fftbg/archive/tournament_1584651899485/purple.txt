Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Reiker
Female
Capricorn
54
61
Priest
Dance
Speed Save
Magic Defense UP
Ignore Height

Morning Star

Golden Hairpin
Linen Robe
Spike Shoes

Cure 4, Raise 2, Reraise, Protect 2, Wall, Esuna
Polka Polka, Last Dance, Obsidian Blade, Nether Demon



Friendsquirrel
Male
Aquarius
53
64
Chemist
Punch Art
Parry
Maintenance
Jump+1

Cute Bag

Golden Hairpin
Power Sleeve
Power Wrist

Potion, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Secret Fist, Purification, Revive



UmaiJam
Female
Aries
49
53
Knight
Draw Out
Regenerator
Attack UP
Fly

Defender

Cross Helmet
Plate Mail
108 Gems

Head Break, Shield Break, Magic Break
Heaven's Cloud, Kikuichimoji



Kronikle
Male
Leo
76
52
Archer
Time Magic
Regenerator
Short Charge
Jump+2

Bow Gun
Kaiser Plate
Black Hood
Chain Vest
Spike Shoes

Charge+1, Charge+10
Slow, Reflect, Quick

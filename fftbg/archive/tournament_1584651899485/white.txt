Player: !White
Team: White Team
Palettes: White/Blue



L2 Sentinel
Male
Aries
68
44
Archer
Summon Magic
Earplug
Equip Bow
Move+2

Lightning Bow

Cross Helmet
Judo Outfit
N-Kai Armlet

Charge+1, Charge+7
Ramuh, Titan, Fairy



Thefyeman
Male
Aries
49
71
Lancer
Draw Out
Mana Shield
Short Charge
Jump+3

Holy Lance
Genji Shield
Iron Helmet
Genji Armor
Feather Mantle

Level Jump3, Vertical Jump7
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



BAEN
Monster
Libra
54
76
Bomb










Humble Fabio
Female
Serpentarius
63
57
Samurai
Summon Magic
Parry
Equip Knife
Levitate

Spell Edge

Crystal Helmet
Genji Armor
Rubber Shoes

Asura, Murasame, Kikuichimoji
Moogle, Ifrit, Titan, Odin, Leviathan, Salamander, Silf

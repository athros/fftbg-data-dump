Player: !Black
Team: Black Team
Palettes: Black/Red



Randgridr
Female
Pisces
68
76
Thief
Elemental
Speed Save
Equip Armor
Move-MP Up

Cultist Dagger

Circlet
Chain Mail
Small Mantle

Gil Taking, Steal Shield, Steal Accessory
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



KasugaiRoastedPeas
Female
Scorpio
70
56
Monk
Charge
Dragon Spirit
Short Status
Move-MP Up



Triangle Hat
Wizard Outfit
Vanish Mantle

Wave Fist, Purification, Revive, Seal Evil
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10, Charge+20



Ruleof5
Female
Libra
43
51
Archer
Throw
Speed Save
Long Status
Move+3

Bow Gun
Kaiser Plate
Green Beret
Secret Clothes
Bracer

Charge+1, Charge+2, Charge+3, Charge+5
Shuriken, Stick



Man
Male
Libra
80
45
Ninja
Black Magic
Critical Quick
Magic Attack UP
Levitate

Ninja Edge
Assassin Dagger
Golden Hairpin
Mythril Vest
Reflect Ring

Shuriken
Fire 3, Fire 4, Bolt 2, Ice, Ice 2, Ice 4

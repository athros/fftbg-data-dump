Final Bets: red - 16 bets for 7,856G (56.8%, x0.76); brown - 8 bets for 5,976G (43.2%, x1.31)

red bets:
killth3kid: 1,291G (16.4%, 129,115G)
DudeMonkey77: 1,204G (15.3%, 1,204G)
casteia: 1,152G (14.7%, 2,304G)
Thyrandaal: 1,000G (12.7%, 15,844G)
NightwolfXVI: 736G (9.4%, 736G)
BirbBrainsBot: 729G (9.3%, 134,000G)
ShintaroNayaka: 604G (7.7%, 604G)
SaltiestMage: 212G (2.7%, 212G)
gorgewall: 201G (2.6%, 2,347G)
ZCKaiser: 166G (2.1%, 1,665G)
getthemoneyz: 156G (2.0%, 1,161,949G)
CosmicTactician: 100G (1.3%, 51,504G)
DuneMeta: 100G (1.3%, 4,697G)
datadrivenbot: 100G (1.3%, 51,463G)
E_Ballard: 100G (1.3%, 4,552G)
SephDarkheart: 5G (0.1%, 1,014G)

brown bets:
Nickyfive: 2,000G (33.5%, 11,178G)
nifboy: 1,088G (18.2%, 1,088G)
fenixcrest: 1,000G (16.7%, 16,578G)
TheChainNerd: 788G (13.2%, 4,788G)
ArashiKurobara: 500G (8.4%, 18,594G)
Shalloween: 400G (6.7%, 64,514G)
nhammen: 100G (1.7%, 10,454G)
nsm013: 100G (1.7%, 10,208G)

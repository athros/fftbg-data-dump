Final Bets: black - 10 bets for 5,292G (45.8%, x1.19); brown - 9 bets for 6,275G (54.2%, x0.84)

black bets:
Firesheath: 2,000G (37.8%, 16,850G)
BirbBrainsBot: 1,000G (18.9%, 86,222G)
prince_rogers_nelson_: 600G (11.3%, 600G)
superdevon1: 496G (9.4%, 496G)
PrancesWithWolves: 300G (5.7%, 314G)
roqqqpsi: 226G (4.3%, 22,681G)
gorgewall: 201G (3.8%, 27,821G)
Tithonus: 200G (3.8%, 1,281G)
Armorgames1: 157G (3.0%, 157G)
getthemoneyz: 112G (2.1%, 1,091,502G)

brown bets:
Arcblazer23: 2,929G (46.7%, 5,858G)
lowlf: 1,212G (19.3%, 102,035G)
Who_lio42: 600G (9.6%, 14,206G)
Klednar21: 334G (5.3%, 334G)
LAGBOT30000: 300G (4.8%, 4,362G)
twelfthrootoftwo: 300G (4.8%, 12,885G)
Neo_Exodus: 300G (4.8%, 4,431G)
Evewho: 200G (3.2%, 8,438G)
datadrivenbot: 100G (1.6%, 46,361G)

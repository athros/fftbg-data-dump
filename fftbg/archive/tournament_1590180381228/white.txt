Player: !White
Team: White Team
Palettes: White/Blue



Itszaane
Male
Scorpio
70
61
Ninja
Draw Out
Mana Shield
Equip Sword
Ignore Height

Bizen Boat
Iga Knife
Flash Hat
Judo Outfit
Feather Boots

Shuriken, Bomb, Wand
Murasame, Muramasa



Old Overholt
Male
Sagittarius
42
56
Ninja
Draw Out
Earplug
Equip Sword
Ignore Height

Defender
Flame Whip
Green Beret
Mystic Vest
N-Kai Armlet

Shuriken, Staff, Spear
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Killth3kid
Male
Libra
48
45
Thief
Battle Skill
Abandon
Equip Polearm
Ignore Height

Ryozan Silk

Twist Headband
Chain Vest
Rubber Shoes

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Head Break, Armor Break, Weapon Break, Power Break, Justice Sword



StealthModeLocke
Female
Taurus
80
77
Archer
Draw Out
Caution
Attack UP
Lava Walking

Windslash Bow

Triangle Hat
Judo Outfit
Germinas Boots

Charge+2, Charge+4, Charge+5, Charge+7
Asura, Koutetsu, Muramasa, Kikuichimoji

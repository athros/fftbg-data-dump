Player: !Red
Team: Red Team
Palettes: Red/Brown



Lanouh
Female
Pisces
53
82
Time Mage
Elemental
Catch
Equip Bow
Jump+2

Silver Bow

Headgear
White Robe
Red Shoes

Haste, Haste 2, Slow, Stop, Immobilize, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



GnielKnows
Male
Aries
65
50
Geomancer
Punch Art
Sunken State
Attack UP
Waterwalking

Battle Axe
Platinum Shield
Flash Hat
Wizard Robe
Spike Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Lava Ball
Chakra



Evewho
Female
Taurus
57
79
Mediator
Charge
Counter Magic
Long Status
Lava Walking

Romanda Gun

Headgear
Brigandine
Reflect Ring

Invitation, Praise, Preach, Insult, Refute
Charge+2, Charge+4, Charge+5



CorpusCav
Female
Virgo
65
71
Summoner
Yin Yang Magic
Counter Flood
Magic Defense UP
Lava Walking

Bestiary

Black Hood
Linen Robe
Defense Armlet

Moogle, Titan, Carbunkle, Leviathan, Salamander, Silf, Lich
Spell Absorb, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep

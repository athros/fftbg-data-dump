Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Phi Sig
Male
Capricorn
59
47
Mime

Counter
Sicken
Fly



Golden Hairpin
Power Sleeve
Magic Gauntlet

Mimic




Byrdturbo
Male
Scorpio
75
35
Calculator
Yin Yang Magic
Damage Split
Secret Hunt
Levitate

Gokuu Rod

Feather Hat
Mystic Vest
Feather Mantle

CT, Height, 5, 4, 3
Blind, Life Drain, Zombie, Foxbird, Confusion Song, Dispel Magic, Petrify



Aldrammech
Female
Libra
60
78
Archer
Punch Art
Faith Save
Doublehand
Jump+2

Ultimus Bow

Holy Miter
Leather Outfit
Power Wrist

Charge+1, Charge+2, Charge+3, Charge+4, Charge+10
Spin Fist, Wave Fist, Purification, Chakra, Seal Evil



Nifboy
Female
Capricorn
56
63
Priest
Jump
Mana Shield
Beastmaster
Retreat

Mace of Zeus

Triangle Hat
Chameleon Robe
Salty Rage

Cure, Cure 2, Reraise, Protect, Esuna
Level Jump5, Vertical Jump5

Final Bets: purple - 8 bets for 2,620G (29.6%, x2.38); brown - 9 bets for 6,243G (70.4%, x0.42)

purple bets:
resjudicata3: 1,000G (38.2%, 5,306G)
lowlf: 372G (14.2%, 1,810G)
MagicBottle: 324G (12.4%, 691G)
idioSyncr4zy: 273G (10.4%, 5,474G)
getthemoneyz: 222G (8.5%, 1,717,171G)
BirbBrainsBot: 206G (7.9%, 156,065G)
Thyrandaal: 123G (4.7%, 32,907G)
absalom_20: 100G (3.8%, 100G)

brown bets:
sinnyil2: 1,641G (26.3%, 3,283G)
maximumcrit: 1,480G (23.7%, 1,480G)
reinoe: 1,000G (16.0%, 27,088G)
corysteam: 751G (12.0%, 751G)
Forkmore: 453G (7.3%, 889G)
AllInBot: 368G (5.9%, 368G)
PuzzleSecretary: 250G (4.0%, 465G)
datadrivenbot: 200G (3.2%, 62,302G)
17RJ: 100G (1.6%, 838G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Snkey
Male
Aries
56
59
Knight
Time Magic
Auto Potion
Secret Hunt
Move+3

Rune Blade
Hero Shield
Barbuta
Chain Mail
Feather Mantle

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword, Explosion Sword
Haste, Haste 2, Stop, Demi 2, Stabilize Time



Realitydown
Male
Aquarius
48
73
Mime

PA Save
Dual Wield
Levitate



Headgear
Wizard Outfit
Setiemson

Mimic




GreatRedDragon
Male
Leo
70
65
Mime

MA Save
Long Status
Jump+1



Headgear
Mythril Vest
Wizard Mantle

Mimic




E Of Pi
Male
Aries
78
64
Samurai
Black Magic
Parry
Doublehand
Waterwalking

Heaven's Cloud

Diamond Helmet
Gold Armor
Germinas Boots

Koutetsu, Murasame, Heaven's Cloud
Fire, Fire 4, Bolt 2, Ice 3, Death

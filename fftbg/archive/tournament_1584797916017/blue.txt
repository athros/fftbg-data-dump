Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Z32o
Female
Scorpio
79
43
Thief
Time Magic
Catch
Equip Bow
Teleport

Bow Gun

Black Hood
Mystic Vest
Leather Mantle

Steal Shield
Haste, Slow, Meteor



Ranmilia
Male
Leo
69
74
Archer
Battle Skill
Parry
Magic Attack UP
Jump+2

Long Bow

Thief Hat
Chain Vest
N-Kai Armlet

Charge+2, Charge+5, Charge+10, Charge+20
Head Break, Magic Break, Speed Break, Mind Break, Dark Sword



Maeveen
Male
Taurus
54
50
Squire
Item
Distribute
Defense UP
Levitate

Poison Bow
Crystal Shield
Black Hood
Linen Cuirass
Defense Ring

Dash, Throw Stone, Fury
Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Phoenix Down



Ninjasteepler
Female
Cancer
58
70
Ninja
Jump
Meatbone Slash
Equip Gun
Ignore Terrain

Bestiary
Fairy Harp
Ribbon
Wizard Outfit
Spike Shoes

Bomb, Knife, Spear
Level Jump2, Vertical Jump8

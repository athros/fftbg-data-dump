Player: !White
Team: White Team
Palettes: White/Blue



Lanshaft
Monster
Scorpio
74
62
Ahriman










HughJeffner
Male
Cancer
40
69
Mediator
Throw
MP Restore
Dual Wield
Move+3

Stone Gun
Glacier Gun
Green Beret
Brigandine
Genji Gauntlet

Invitation, Threaten, Solution, Mimic Daravon, Refute
Ninja Sword



Coyowijuju
Male
Pisces
51
59
Thief
Charge
Arrow Guard
Equip Gun
Move+2

Mythril Gun

Leather Helmet
Mystic Vest
Defense Armlet

Steal Status, Arm Aim
Charge+2, Charge+3, Charge+7, Charge+10, Charge+20



Ministeroftaste
Female
Gemini
53
59
Wizard
Math Skill
Arrow Guard
Defense UP
Ignore Height

Ice Rod

Golden Hairpin
Secret Clothes
Defense Armlet

Bolt, Ice 2, Death
Height, Prime Number, 4, 3

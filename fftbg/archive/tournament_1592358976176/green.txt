Player: !Green
Team: Green Team
Palettes: Green/White



Thyrandaal
Male
Aries
74
52
Knight
Black Magic
Parry
Long Status
Waterwalking

Giant Axe
Bronze Shield
Bronze Helmet
Carabini Mail
108 Gems

Head Break, Speed Break, Mind Break
Fire 4, Ice 3, Ice 4



EnemyController
Male
Scorpio
71
49
Calculator
Byblos
Distribute
Equip Sword
Lava Walking

Ice Brand
Buckler
Headgear
Crystal Mail
108 Gems

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Galkife
Female
Cancer
53
58
Dancer
Item
Arrow Guard
Magic Defense UP
Move-MP Up

Persia

Black Hood
Judo Outfit
Diamond Armlet

Witch Hunt, Polka Polka, Nameless Dance, Obsidian Blade, Void Storage
Potion, Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



Aldrammech
Female
Taurus
50
63
Mediator
Steal
Parry
Equip Armor
Move+1

Romanda Gun

Barbuta
Carabini Mail
Sprint Shoes

Invitation, Persuade, Praise, Insult, Negotiate, Refute, Rehabilitate
Steal Heart, Steal Helmet, Steal Armor

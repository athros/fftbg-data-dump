Player: !Green
Team: Green Team
Palettes: Green/White



ZephyrTempest
Female
Aries
79
63
Archer
Dance
Meatbone Slash
Concentrate
Move-MP Up

Silver Bow

Bronze Helmet
Chain Vest
Elf Mantle

Charge+1, Charge+3, Charge+5, Charge+7
Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Dragon Pit



BenYuPoker
Male
Virgo
70
66
Archer
White Magic
Brave Up
Doublehand
Waterwalking

Glacier Gun

Flash Hat
Secret Clothes
Magic Gauntlet

Charge+7
Cure 3, Cure 4, Raise, Regen, Esuna



UmaiJam
Male
Capricorn
57
44
Bard
Steal
Absorb Used MP
Equip Bow
Move+2

Lightning Bow

Black Hood
Genji Armor
Chantage

Life Song, Battle Song, Nameless Song, Last Song, Space Storage
Steal Shield, Leg Aim



Galkife
Female
Capricorn
74
70
Dancer
White Magic
Arrow Guard
Long Status
Levitate

Panther Bag

Leather Hat
Chain Vest
Magic Ring

Wiznaibus, Polka Polka, Disillusion, Last Dance
Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Esuna

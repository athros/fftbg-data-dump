Final Bets: green - 8 bets for 2,500G (23.9%, x3.19); yellow - 7 bets for 7,974G (76.1%, x0.31)

green bets:
Xoomwaffle: 500G (20.0%, 2,507G)
Artea_: 500G (20.0%, 7,502G)
NIghtdew14: 500G (20.0%, 13,908G)
prince_rogers_nelson_: 444G (17.8%, 444G)
AllInBot: 334G (13.4%, 334G)
Lydian_C: 121G (4.8%, 63,555G)
CosmicTactician: 100G (4.0%, 45,613G)
SephDarkheart: 1G (0.0%, 53,466G)

yellow bets:
dantayystv: 5,000G (62.7%, 23,084G)
BirbBrainsBot: 1,000G (12.5%, 27,991G)
Chuckolator: 848G (10.6%, 40,767G)
TheMurkGnome: 500G (6.3%, 3,492G)
getthemoneyz: 276G (3.5%, 1,302,604G)
datadrivenbot: 200G (2.5%, 31,150G)
DouglasDragonThePoet: 150G (1.9%, 1,482G)

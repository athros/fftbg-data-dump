Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ShintaroNayaka
Male
Leo
79
62
Samurai
Throw
Dragon Spirit
Beastmaster
Teleport

Obelisk

Bronze Helmet
Leather Armor
Spike Shoes

Asura, Heaven's Cloud, Muramasa
Shuriken, Bomb



Mirapoix
Male
Virgo
53
55
Samurai
Summon Magic
Meatbone Slash
Defend
Jump+2

Muramasa

Barbuta
Linen Cuirass
108 Gems

Bizen Boat, Murasame, Kiyomori, Kikuichimoji, Chirijiraden
Moogle, Titan, Carbunkle, Silf



Dogsandcatsand
Monster
Aquarius
54
55
Behemoth










NovaKnight21
Female
Capricorn
66
68
Dancer
Draw Out
Distribute
Equip Gun
Ignore Terrain

Glacier Gun

Flash Hat
Robe of Lords
Rubber Shoes

Witch Hunt, Slow Dance, Polka Polka, Disillusion, Last Dance, Nether Demon
Asura, Murasame

Final Bets: blue - 11 bets for 11,090G (69.9%, x0.43); black - 6 bets for 4,779G (30.1%, x2.32)

blue bets:
Lydian_C: 4,200G (37.9%, 65,853G)
bruubarg: 2,500G (22.5%, 37,630G)
TheChainNerd: 1,500G (13.5%, 61,611G)
JumbocactuarX27: 1,000G (9.0%, 41,538G)
BirbBrainsBot: 1,000G (9.0%, 24,587G)
AllInBot: 200G (1.8%, 200G)
Rytor: 200G (1.8%, 2,991G)
datadrivenbot: 200G (1.8%, 32,152G)
DouglasDragonThePoet: 150G (1.4%, 1,848G)
CosmicTactician: 100G (0.9%, 44,713G)
getthemoneyz: 40G (0.4%, 1,301,728G)

black bets:
NIghtdew14: 1,978G (41.4%, 13,193G)
Chuckolator: 1,016G (21.3%, 36,058G)
ShintaroNayaka: 800G (16.7%, 9,210G)
randgridr: 380G (8.0%, 380G)
TheMurkGnome: 357G (7.5%, 4,349G)
Xoomwaffle: 248G (5.2%, 7,078G)

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Maakur
Male
Cancer
37
59
Thief
Sing
Parry
Magic Attack UP
Teleport

Dagger

Golden Hairpin
Power Sleeve
Feather Mantle

Steal Armor, Steal Shield, Steal Weapon, Leg Aim
Last Song, Hydra Pit



JonnyCue
Male
Capricorn
77
77
Wizard
Yin Yang Magic
Critical Quick
Equip Shield
Teleport

Faith Rod
Gold Shield
Holy Miter
Linen Robe
Feather Mantle

Fire, Fire 2, Bolt, Bolt 3, Ice 4, Flare
Blind, Silence Song, Dispel Magic, Sleep, Petrify, Dark Holy



Thyrandaal
Female
Cancer
77
63
Priest
Talk Skill
PA Save
Short Charge
Lava Walking

Gold Staff

Golden Hairpin
Chameleon Robe
Genji Gauntlet

Cure 2, Cure 3, Cure 4, Raise, Protect 2, Esuna, Holy
Preach, Insult, Negotiate



SkylerBunny
Monster
Pisces
57
76
Cockatrice








Player: !White
Team: White Team
Palettes: White/Blue



Alc Trinity
Female
Libra
64
55
Geomancer
Jump
Brave Up
Defense UP
Move+3

Ancient Sword
Ice Shield
Cachusha
Chameleon Robe
Rubber Shoes

Pitfall, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump2



Hamedont
Female
Aries
58
43
Monk
Jump
Caution
Maintenance
Retreat



Leather Hat
Earth Clothes
Magic Ring

Wave Fist, Purification, Chakra
Level Jump5, Vertical Jump8



Eltee78641
Female
Aquarius
52
51
Chemist
Yin Yang Magic
Catch
Equip Knife
Ignore Height

Cultist Dagger

Twist Headband
Wizard Outfit
Feather Boots

Potion, X-Potion, Hi-Ether, Echo Grass, Holy Water, Phoenix Down
Poison, Spell Absorb, Pray Faith, Doubt Faith, Foxbird, Confusion Song, Paralyze



Sairentozon7
Male
Leo
76
52
Lancer
Elemental
Parry
Equip Armor
Levitate

Holy Lance
Crystal Shield
Barbuta
Bronze Armor
Rubber Shoes

Level Jump3, Vertical Jump7
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

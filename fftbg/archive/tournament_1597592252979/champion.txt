Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ALY327
Male
Taurus
39
62
Knight
Charge
Arrow Guard
Attack UP
Waterbreathing

Materia Blade
Bronze Shield
Diamond Helmet
Carabini Mail
Feather Boots

Armor Break, Weapon Break, Night Sword, Explosion Sword
Charge+1, Charge+2, Charge+7, Charge+20



Error72
Male
Aries
60
46
Knight
Jump
Catch
Magic Defense UP
Retreat

Slasher
Round Shield
Mythril Helmet
Platinum Armor
108 Gems

Head Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword
Level Jump4, Vertical Jump6



Mesmaster
Female
Leo
46
45
Priest
Time Magic
Counter
Equip Armor
Teleport

Scorpion Tail

Gold Helmet
Chameleon Robe
Wizard Mantle

Cure 2, Cure 3, Raise 2, Protect, Esuna
Haste, Slow 2, Stop, Float, Demi 2



J2DaBibbles
Female
Taurus
66
49
Mediator
Elemental
Parry
Dual Wield
Move+3

Mythril Gun
Blast Gun
Cachusha
Rubber Costume
Sprint Shoes

Persuade, Threaten, Negotiate, Mimic Daravon, Refute
Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

Player: !White
Team: White Team
Palettes: White/Blue



Silentkaster
Male
Gemini
47
59
Archer
White Magic
Abandon
Concentrate
Retreat

Hunting Bow
Round Shield
Red Hood
Power Sleeve
Defense Armlet

Charge+1
Cure 2, Cure 3, Cure 4, Raise, Protect, Wall



BoneMiser
Male
Libra
71
44
Bard
Time Magic
Faith Save
Equip Axe
Lava Walking

Flail

Leather Hat
Gold Armor
Elf Mantle

Battle Song, Nameless Song
Haste 2, Slow, Stop, Stabilize Time



UnparalleledDev
Female
Cancer
57
71
Ninja
Battle Skill
Meatbone Slash
Sicken
Jump+1

Koga Knife
Mythril Knife
Red Hood
Adaman Vest
Reflect Ring

Shuriken, Staff, Spear
Armor Break, Weapon Break, Mind Break



Ar Tactic
Male
Libra
61
46
Knight
Summon Magic
Damage Split
Maintenance
Fly

Save the Queen
Genji Shield
Iron Helmet
Chameleon Robe
N-Kai Armlet

Weapon Break, Magic Break, Power Break, Stasis Sword, Night Sword
Moogle, Ramuh, Salamander, Fairy

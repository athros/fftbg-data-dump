Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Tarheels218
Female
Cancer
44
75
Archer
Time Magic
Distribute
Dual Wield
Jump+3

Stone Gun
Glacier Gun
Flash Hat
Adaman Vest
Leather Mantle

Charge+2, Charge+3, Charge+5
Haste, Haste 2, Reflect



Rockmem21
Female
Leo
78
71
Wizard
Item
Caution
Throw Item
Lava Walking

Poison Rod

Thief Hat
Black Costume
Defense Armlet

Fire, Fire 3, Bolt 2, Bolt 3, Bolt 4, Empower
Ether, Eye Drop, Maiden's Kiss



Nekojin
Male
Libra
54
61
Mime

Blade Grasp
Dual Wield
Jump+1



Holy Miter
Chain Vest
Spike Shoes

Mimic




TheUnforgivenRage
Female
Cancer
60
69
Archer
Item
Counter Tackle
Defend
Waterwalking

Glacier Gun
Buckler
Green Beret
Adaman Vest
Leather Mantle

Charge+1, Charge+2, Charge+7, Charge+10, Charge+20
Potion, Antidote, Eye Drop, Soft, Remedy, Phoenix Down

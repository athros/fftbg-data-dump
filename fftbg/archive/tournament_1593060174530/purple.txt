Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



OneHundredFists
Monster
Cancer
51
67
Dragon










Evewho
Female
Capricorn
72
43
Knight
Draw Out
HP Restore
Equip Knife
Jump+3

Short Edge
Genji Shield
Cross Helmet
White Robe
Feather Mantle

Head Break, Armor Break, Shield Break, Power Break
Bizen Boat, Heaven's Cloud



Mesmaster
Male
Aquarius
52
45
Squire
Jump
Hamedo
Dual Wield
Ignore Terrain

Bow Gun
Hunting Bow
Bronze Helmet
Mythril Vest
Rubber Shoes

Accumulate, Dash, Heal, Tickle, Fury
Level Jump8, Vertical Jump7



Sairentozon7
Male
Virgo
71
74
Chemist
Basic Skill
Sunken State
Equip Knife
Jump+2

Thunder Rod

Flash Hat
Brigandine
Defense Armlet

Potion, Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Accumulate, Dash, Throw Stone, Heal, Tickle, Cheer Up, Fury

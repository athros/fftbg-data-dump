Player: !Green
Team: Green Team
Palettes: Green/White



Lawndough
Female
Aquarius
43
66
Summoner
Basic Skill
MP Restore
Attack UP
Move+2

Ice Rod

Headgear
Brigandine
Battle Boots

Moogle, Ramuh, Carbunkle, Odin, Leviathan, Salamander, Lich
Tickle, Yell



Saldarin
Male
Libra
78
51
Ninja
Punch Art
Earplug
Concentrate
Waterwalking

Flail
Flail
Flash Hat
Black Costume
Germinas Boots

Bomb
Wave Fist, Purification, Chakra, Revive



Poulol
Male
Sagittarius
77
42
Monk
Charge
Sunken State
Equip Gun
Move-MP Up

Madlemgen

Black Hood
Power Sleeve
Germinas Boots

Wave Fist, Purification
Charge+1, Charge+7



Digitalsocrates
Female
Taurus
51
40
Wizard
Dance
Dragon Spirit
Short Charge
Waterbreathing

Poison Rod

Feather Hat
White Robe
Magic Gauntlet

Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Frog, Flare
Wiznaibus, Obsidian Blade, Void Storage

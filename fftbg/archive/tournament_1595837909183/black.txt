Player: !Black
Team: Black Team
Palettes: Black/Red



Ar Tactic
Male
Taurus
69
49
Oracle
Talk Skill
Sunken State
Sicken
Move+3

Iron Fan

Leather Hat
Silk Robe
Germinas Boots

Life Drain, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify
Persuade, Praise, Threaten, Preach, Solution, Mimic Daravon



Lowlf
Female
Scorpio
75
47
Time Mage
Black Magic
Catch
Doublehand
Fly

Wizard Staff

Twist Headband
Adaman Vest
Red Shoes

Haste, Slow, Slow 2, Stop, Quick, Meteor
Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 3



Superdevon1
Male
Sagittarius
64
61
Bard
White Magic
Dragon Spirit
Magic Attack UP
Ignore Height

Fairy Harp

Thief Hat
Clothes
Magic Ring

Life Song, Nameless Song, Sky Demon
Cure, Raise, Reraise, Wall, Esuna



Blorpy
Male
Virgo
59
53
Oracle
Basic Skill
Abandon
Secret Hunt
Waterwalking

Gokuu Rod

Barette
Rubber Costume
Genji Gauntlet

Silence Song, Foxbird, Confusion Song, Dispel Magic
Heal, Cheer Up, Wish

Player: !White
Team: White Team
Palettes: White/Blue



LivingHitokiri
Male
Libra
65
47
Lancer
Draw Out
Abandon
Doublehand
Move+1

Spear

Diamond Helmet
Gold Armor
Leather Mantle

Level Jump8, Vertical Jump6
Kikuichimoji



Jaritras
Female
Sagittarius
58
68
Ninja
Black Magic
Distribute
Equip Shield
Move+1

Air Knife
Round Shield
Green Beret
Power Sleeve
Feather Boots

Knife
Fire, Fire 2, Fire 3, Bolt 2, Bolt 3



Pandasforsale
Male
Aries
69
58
Knight
Steal
Damage Split
Equip Bow
Ignore Height

Hunting Bow
Platinum Shield
Grand Helmet
Bronze Armor
Magic Gauntlet

Mind Break, Justice Sword
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status



Laserman1000
Male
Virgo
43
69
Mime

Caution
Dual Wield
Fly



Twist Headband
Wizard Outfit
Vanish Mantle

Mimic


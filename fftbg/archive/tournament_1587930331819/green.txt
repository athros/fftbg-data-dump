Player: !Green
Team: Green Team
Palettes: Green/White



Baron Von Scrub
Male
Gemini
57
70
Mime

Speed Save
Martial Arts
Move-HP Up



Flash Hat
Judo Outfit
Elf Mantle

Mimic




DrAntiSocial
Male
Virgo
69
49
Ninja
Punch Art
HP Restore
Equip Sword
Move+1

Blood Sword
Coral Sword
Thief Hat
Judo Outfit
Defense Armlet

Shuriken, Bomb, Knife
Spin Fist, Earth Slash, Secret Fist, Purification, Revive



Urieltheflameofgod
Male
Virgo
55
74
Priest
Elemental
Hamedo
Defense UP
Jump+2

Rainbow Staff

Red Hood
Clothes
Jade Armlet

Raise, Raise 2, Reraise, Shell, Esuna
Water Ball, Hell Ivy, Will-O-Wisp, Blizzard, Gusty Wind



Squatting Bear
Male
Gemini
42
60
Lancer
White Magic
Hamedo
Equip Sword
Move+1

Koutetsu Knife
Diamond Shield
Gold Helmet
Mythril Armor
N-Kai Armlet

Level Jump8, Vertical Jump8
Cure 4, Raise, Esuna

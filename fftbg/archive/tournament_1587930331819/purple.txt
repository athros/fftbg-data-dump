Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Technominari
Female
Aquarius
58
79
Priest
Throw
MP Restore
Martial Arts
Ignore Terrain



Triangle Hat
Linen Robe
Vanish Mantle

Cure 2, Cure 4, Raise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Shuriken



Nedryerson01
Female
Capricorn
77
47
Priest
Yin Yang Magic
Faith Up
Magic Attack UP
Move+3

Flail

Green Beret
Silk Robe
Red Shoes

Cure 4, Raise 2, Shell, Esuna
Silence Song, Foxbird, Dispel Magic, Petrify



SolarisFall
Female
Sagittarius
72
79
Monk
Draw Out
Parry
Doublehand
Move+3



Black Hood
Adaman Vest
Bracer

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Asura, Murasame, Muramasa, Kikuichimoji



Museofdoom
Male
Gemini
65
65
Samurai
Black Magic
Meatbone Slash
Defend
Waterbreathing

Kiyomori

Mythril Helmet
Maximillian
Defense Ring

Asura, Koutetsu, Bizen Boat, Murasame
Fire, Fire 4, Ice, Ice 2, Empower

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mesmaster
Female
Aquarius
41
49
Samurai
Jump
Mana Shield
Short Charge
Teleport

Chirijiraden

Iron Helmet
Bronze Armor
Magic Gauntlet

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Level Jump8, Vertical Jump6



Helpimabug
Monster
Aquarius
64
79
Revenant










Phi Sig
Female
Virgo
70
52
Mime

Distribute
Equip Armor
Move+2



Leather Hat
White Robe
Genji Gauntlet

Mimic




Douchetron
Female
Leo
44
58
Oracle
Basic Skill
Catch
Martial Arts
Move+1



Red Hood
Chain Vest
Feather Boots

Life Drain, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Accumulate, Heal, Yell, Cheer Up

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lowlf
Male
Libra
48
47
Monk
Battle Skill
Arrow Guard
Equip Polearm
Fly

Iron Fan

Black Hood
Mystic Vest
Jade Armlet

Earth Slash, Purification, Revive
Armor Break, Magic Break, Power Break, Stasis Sword, Explosion Sword



Firesheath
Male
Virgo
55
38
Ninja
Talk Skill
Blade Grasp
Secret Hunt
Waterwalking

Sasuke Knife
Iga Knife
Leather Hat
Judo Outfit
Spike Shoes

Staff
Invitation, Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate



Treafa
Female
Capricorn
75
69
Squire
Punch Art
Absorb Used MP
Beastmaster
Ignore Height

Slasher
Crystal Shield
Barbuta
Adaman Vest
Battle Boots

Tickle, Yell, Wish
Wave Fist



Braveboyblue
Male
Aries
50
71
Squire
Jump
Faith Save
Equip Axe
Move+2

Battle Axe
Buckler
Red Hood
Power Sleeve
Spike Shoes

Accumulate, Throw Stone, Tickle, Wish
Level Jump8, Vertical Jump8

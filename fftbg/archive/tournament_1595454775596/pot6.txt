Final Bets: white - 12 bets for 9,884G (55.7%, x0.79); purple - 6 bets for 7,849G (44.3%, x1.26)

white bets:
AllInBot: 2,244G (22.7%, 2,244G)
randgridr: 1,660G (16.8%, 3,320G)
HaateXIII: 1,357G (13.7%, 1,357G)
twelfthrootoftwo: 997G (10.1%, 997G)
Laserman1000: 644G (6.5%, 644G)
Nizaha: 572G (5.8%, 9,769G)
DaKoopa49: 563G (5.7%, 563G)
getthemoneyz: 532G (5.4%, 1,338,936G)
Miku_Shikhu: 495G (5.0%, 495G)
killth3kid: 484G (4.9%, 4,183G)
datadrivenbot: 200G (2.0%, 33,895G)
douchetron: 136G (1.4%, 136G)

purple bets:
TheChainNerd: 2,980G (38.0%, 5,844G)
DarrenDinosaurs: 2,869G (36.6%, 2,869G)
BirbBrainsBot: 1,000G (12.7%, 192,535G)
benticore: 500G (6.4%, 28,041G)
ColetteMSLP: 400G (5.1%, 6,269G)
Firesheath: 100G (1.3%, 7,978G)

Player: !Green
Team: Green Team
Palettes: Green/White



CapnChaos12
Male
Leo
60
39
Knight
Throw
Hamedo
Short Status
Teleport 2

Blood Sword
Gold Shield
Diamond Helmet
Silk Robe
Angel Ring

Armor Break, Shield Break, Weapon Break, Mind Break
Shuriken, Bomb



Galkife
Male
Aries
64
49
Samurai
Punch Art
Dragon Spirit
Doublehand
Ignore Height

Koutetsu Knife

Mythril Helmet
Gold Armor
Reflect Ring

Asura, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Purification, Chakra



VolgraTheMoose
Female
Virgo
59
71
Priest
Talk Skill
Counter Flood
Monster Talk
Teleport

Healing Staff

Feather Hat
Leather Outfit
Small Mantle

Raise, Reraise, Shell
Praise, Death Sentence, Insult, Mimic Daravon, Refute



Vultuous
Female
Pisces
43
56
Lancer
Yin Yang Magic
Brave Save
Equip Knife
Move-HP Up

Assassin Dagger
Ice Shield
Diamond Helmet
Linen Cuirass
Magic Gauntlet

Level Jump5, Vertical Jump8
Poison, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep

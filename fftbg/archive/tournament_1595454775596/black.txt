Player: !Black
Team: Black Team
Palettes: Black/Red



Evontno
Male
Leo
70
72
Geomancer
White Magic
Auto Potion
Defend
Jump+1

Koutetsu Knife
Crystal Shield
Flash Hat
Mystic Vest
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Raise, Protect, Shell



Error72
Female
Aquarius
55
52
Geomancer
Draw Out
Faith Save
Maintenance
Jump+1

Kikuichimoji
Gold Shield
Green Beret
Brigandine
Feather Boots

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Murasame



ALY327
Male
Cancer
69
52
Samurai
Black Magic
HP Restore
Equip Axe
Jump+3

Morning Star

Cross Helmet
Silk Robe
Sprint Shoes

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Fire, Fire 2, Fire 4, Bolt 3, Ice 4



ACSpree
Male
Sagittarius
53
54
Lancer
Sing
Meatbone Slash
Equip Knife
Lava Walking

Mythril Knife
Mythril Shield
Cross Helmet
White Robe
Battle Boots

Level Jump5, Vertical Jump2
Life Song, Magic Song, Nameless Song, Sky Demon, Hydra Pit

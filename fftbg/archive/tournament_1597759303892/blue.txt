Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Galkife
Female
Aries
69
69
Geomancer
Draw Out
Counter Magic
Equip Gun
Move-HP Up

Romanda Gun
Kaiser Plate
Headgear
Light Robe
Rubber Shoes

Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bizen Boat, Murasame, Kiyomori



JumbocactuarX27
Female
Aquarius
58
74
Wizard
Punch Art
Arrow Guard
Magic Attack UP
Jump+3

Flame Rod

Flash Hat
Brigandine
Wizard Mantle

Fire, Bolt, Ice 2, Death, Flare
Purification, Revive, Seal Evil



Joewcarson
Male
Libra
45
42
Knight
Punch Art
Counter
Halve MP
Waterwalking

Giant Axe
Mythril Shield
Diamond Helmet
Black Robe
Elf Mantle

Shield Break, Justice Sword
Pummel, Earth Slash, Purification, Chakra, Revive



NovaKnight21
Male
Cancer
56
78
Mediator
Battle Skill
Dragon Spirit
Dual Wield
Lava Walking

Bestiary
Papyrus Codex
Golden Hairpin
Wizard Robe
Defense Ring

Invitation, Threaten, Death Sentence, Negotiate, Refute, Rehabilitate
Magic Break

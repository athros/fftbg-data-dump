Player: !Red
Team: Red Team
Palettes: Red/Brown



Miku Shikhu
Male
Sagittarius
69
40
Monk
Jump
Auto Potion
Attack UP
Fly



Feather Hat
Leather Outfit
Small Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive
Level Jump8, Vertical Jump3



Forkmore
Male
Libra
68
63
Mediator
Summon Magic
HP Restore
Magic Attack UP
Jump+1

Blaze Gun

Red Hood
Adaman Vest
Diamond Armlet

Invitation, Praise, Preach, Death Sentence, Refute, Rehabilitate
Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan



CapnChaos12
Male
Libra
60
58
Mime

Caution
Martial Arts
Jump+1



Golden Hairpin
Linen Robe
Power Wrist

Mimic




Butterbelljedi
Male
Aquarius
74
55
Chemist
White Magic
Counter Tackle
Equip Polearm
Move+3

Octagon Rod

Holy Miter
Mystic Vest
108 Gems

Potion, X-Potion, Ether, Eye Drop, Soft, Remedy, Phoenix Down
Raise, Raise 2, Reraise, Shell, Wall, Esuna, Magic Barrier

Player: !Black
Team: Black Team
Palettes: Black/Red



Wickslee
Monster
Cancer
60
57
Red Chocobo










Eryzion
Female
Sagittarius
41
50
Ninja
Steal
Distribute
Defend
Ignore Height

Assassin Dagger
Mythril Knife
Thief Hat
Wizard Outfit
Elf Mantle

Shuriken, Bomb, Axe
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim, Leg Aim



JoeBlowForever
Male
Cancer
68
60
Calculator
Yin Yang Magic
Distribute
Defense UP
Move+2

Musk Rod

Thief Hat
Wizard Outfit
Cursed Ring

CT, Height, Prime Number, 5, 4
Spell Absorb, Zombie, Confusion Song, Dispel Magic, Sleep



Madarius777
Male
Aquarius
80
47
Ninja
Steal
Sunken State
Equip Gun
Move+3

Romanda Gun
Mythril Gun
Feather Hat
Power Sleeve
Sprint Shoes

Knife
Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim

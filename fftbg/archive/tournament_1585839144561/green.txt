Player: !Green
Team: Green Team
Palettes: Green/White



SeedSC
Female
Leo
72
69
Priest
Basic Skill
Absorb Used MP
Magic Defense UP
Waterwalking

Flail

Triangle Hat
Robe of Lords
Small Mantle

Cure, Cure 2, Raise, Raise 2, Protect, Esuna
Accumulate, Dash, Heal, Wish



Pwlloth
Female
Gemini
48
64
Mediator
Battle Skill
HP Restore
Equip Axe
Fly

Battle Axe

Thief Hat
Secret Clothes
Bracer

Persuade, Praise, Preach, Solution, Insult, Mimic Daravon
Mind Break, Night Sword, Surging Sword



Roqqqpsi
Male
Libra
79
53
Squire
Jump
Damage Split
Halve MP
Jump+1

Sleep Sword
Buckler
Thief Hat
Mythril Vest
Setiemson

Accumulate, Heal, Tickle, Wish
Level Jump5, Vertical Jump7



Club Sodar
Monster
Sagittarius
75
48
Great Malboro








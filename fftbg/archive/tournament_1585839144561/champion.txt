Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



StealthModeLocke
Male
Capricorn
52
68
Samurai
Summon Magic
Parry
Equip Gun
Waterbreathing

Blast Gun

Leather Helmet
Platinum Armor
Germinas Boots

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Moogle, Ramuh, Titan, Golem, Salamander, Fairy



Daveb
Female
Virgo
61
69
Ninja
Steal
Catch
Long Status
Move+1

Dagger
Dagger
Flash Hat
Mythril Vest
Bracer

Shuriken, Spear
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Arm Aim, Leg Aim



Coralreeferz
Male
Taurus
76
56
Squire
Draw Out
HP Restore
Concentrate
Jump+2

Flail
Gold Shield
Golden Hairpin
Earth Clothes
Dracula Mantle

Accumulate, Heal, Wish
Asura, Heaven's Cloud, Kiyomori



Lyner87
Male
Aquarius
71
76
Lancer
Throw
Earplug
Maintenance
Waterwalking

Mythril Spear
Diamond Shield
Iron Helmet
Carabini Mail
Dracula Mantle

Level Jump8, Vertical Jump7
Sword

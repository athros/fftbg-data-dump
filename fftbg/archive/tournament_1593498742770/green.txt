Player: !Green
Team: Green Team
Palettes: Green/White



Primeval 33
Male
Capricorn
70
49
Knight
Throw
Caution
Beastmaster
Swim

Slasher
Mythril Shield
Crystal Helmet
Mythril Armor
N-Kai Armlet

Weapon Break, Speed Break, Stasis Sword
Shuriken, Bomb



Oops25
Female
Libra
53
62
Calculator
Yin Yang Magic
Parry
Maintenance
Move+3

Musk Rod

Twist Headband
Wizard Outfit
Genji Gauntlet

CT, 5, 3
Blind, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy



Lemonjohns
Male
Gemini
74
61
Geomancer
Throw
Catch
Halve MP
Move-MP Up

Rune Blade
Mythril Shield
Headgear
White Robe
Defense Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Shuriken, Sword, Staff, Dictionary



Byrdturbo
Male
Pisces
61
54
Bard
Jump
Faith Save
Equip Shield
Retreat

Fairy Harp
Venetian Shield
Leather Hat
Black Costume
Battle Boots

Angel Song, Life Song, Cheer Song, Battle Song, Nameless Song, Last Song, Space Storage, Sky Demon, Hydra Pit
Level Jump5, Vertical Jump8

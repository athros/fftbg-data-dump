Player: !White
Team: White Team
Palettes: White/Blue



Superdevon1
Female
Capricorn
80
75
Chemist
Elemental
Caution
Defense UP
Move-HP Up

Panther Bag

Holy Miter
Chain Vest
Feather Mantle

Potion, Eye Drop, Phoenix Down
Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



KasugaiRoastedPeas
Male
Libra
48
39
Calculator
Black Magic
Counter Tackle
Equip Axe
Ignore Terrain

Morning Star

Green Beret
Judo Outfit
Feather Boots

CT, Prime Number, 5
Fire 3, Bolt 3, Ice 2, Ice 3, Empower



Nhammen
Male
Leo
69
67
Bard
Black Magic
Distribute
Doublehand
Retreat

Fairy Harp

Headgear
Power Sleeve
Defense Armlet

Life Song, Last Song, Diamond Blade, Sky Demon, Hydra Pit
Fire, Fire 3, Bolt



NovaKnight21
Female
Libra
58
52
Samurai
Talk Skill
Hamedo
Equip Axe
Jump+3

Flame Whip

Barbuta
Light Robe
Magic Gauntlet

Koutetsu, Muramasa
Threaten, Death Sentence, Insult, Refute

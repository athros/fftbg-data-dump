Player: !Red
Team: Red Team
Palettes: Red/Brown



TeaTime29
Male
Scorpio
68
49
Archer
Draw Out
Damage Split
Attack UP
Fly

Poison Bow
Flame Shield
Feather Hat
Mystic Vest
Cherche

Charge+2, Charge+3, Charge+10
Asura, Koutetsu, Bizen Boat



Jinglejangler
Male
Gemini
63
45
Calculator
White Magic
Faith Save
Equip Knife
Fly

Sasuke Knife

Green Beret
Silk Robe
Magic Gauntlet

Height, 5, 4
Cure, Cure 3, Raise, Reraise, Regen, Protect 2, Wall, Esuna



DuraiPapers
Female
Cancer
76
58
Calculator
Lucavi Skill
Parry
Long Status
Move+2

Battle Folio
Flame Shield
Gold Helmet
White Robe
Leather Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Solomongrundy85
Male
Taurus
43
39
Thief
Jump
Arrow Guard
Sicken
Move-MP Up

Star Bag

Flash Hat
Leather Outfit
Magic Ring

Steal Armor, Steal Accessory, Leg Aim
Level Jump8, Vertical Jump2

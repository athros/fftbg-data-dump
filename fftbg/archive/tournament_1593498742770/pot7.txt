Final Bets: red - 7 bets for 9,259G (54.9%, x0.82); purple - 10 bets for 7,612G (45.1%, x1.22)

red bets:
DuraiPapers: 3,753G (40.5%, 3,753G)
Jinglejangler: 2,714G (29.3%, 5,429G)
bruubarg: 1,324G (14.3%, 1,324G)
alekzanndrr: 664G (7.2%, 664G)
serperemagus: 500G (5.4%, 2,034G)
CITOtheMOSQUITO: 204G (2.2%, 204G)
datadrivenbot: 100G (1.1%, 46,454G)

purple bets:
Tyerenex: 1,234G (16.2%, 23,525G)
prince_rogers_nelson_: 1,000G (13.1%, 8,298G)
Evewho: 1,000G (13.1%, 7,266G)
BirbBrainsBot: 1,000G (13.1%, 162,682G)
Arcblazer23: 1,000G (13.1%, 6,781G)
getthemoneyz: 590G (7.8%, 1,115,845G)
superdevon1: 588G (7.7%, 588G)
BoneMiser: 500G (6.6%, 500G)
escobro: 400G (5.3%, 5,642G)
ColetteMSLP: 300G (3.9%, 2,475G)

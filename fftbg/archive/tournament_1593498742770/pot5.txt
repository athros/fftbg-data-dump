Final Bets: red - 8 bets for 3,287G (46.9%, x1.13); green - 5 bets for 3,722G (53.1%, x0.88)

red bets:
Jinglejangler: 1,543G (46.9%, 3,087G)
prince_rogers_nelson_: 500G (15.2%, 8,232G)
superdevon1: 340G (10.3%, 340G)
escobro: 300G (9.1%, 4,826G)
CITOtheMOSQUITO: 204G (6.2%, 204G)
alekzanndrr: 200G (6.1%, 438G)
Ring_Wyrm: 100G (3.0%, 2,123G)
datadrivenbot: 100G (3.0%, 46,222G)

green bets:
Evewho: 1,000G (26.9%, 7,071G)
BirbBrainsBot: 1,000G (26.9%, 163,868G)
Tyerenex: 1,000G (26.9%, 24,525G)
getthemoneyz: 522G (14.0%, 1,116,789G)
KasugaiRoastedPeas: 200G (5.4%, 17,090G)

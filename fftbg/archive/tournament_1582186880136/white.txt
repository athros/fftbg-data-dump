Player: !White
Team: White Team
Palettes: White/Blue



Solomongrundy85
Male
Aries
39
57
Ninja
White Magic
Counter Flood
Attack UP
Move-MP Up

Orichalcum
Dagger
Cachusha
Brigandine
108 Gems

Shuriken, Knife
Cure, Cure 2, Cure 3, Raise, Shell 2, Esuna, Holy



GoAwayImBaitin
Female
Leo
46
75
Dancer
Punch Art
Earplug
Equip Armor
Waterbreathing

Ryozan Silk

Bronze Helmet
White Robe
Cursed Ring

Wiznaibus, Disillusion, Nameless Dance
Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil



WireLord
Female
Pisces
75
76
Thief
Talk Skill
Counter Tackle
Monster Talk
Move-HP Up

Mage Masher

Red Hood
Mythril Vest
Vanish Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Invitation, Preach, Death Sentence



Hzor
Male
Aries
61
72
Knight
White Magic
Catch
Doublehand
Ignore Terrain

Iron Sword

Diamond Helmet
Wizard Robe
Red Shoes

Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Night Sword
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Esuna, Holy

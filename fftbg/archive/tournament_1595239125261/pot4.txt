Final Bets: purple - 13 bets for 40,480G (98.8%, x0.01); brown - 3 bets for 500G (1.2%, x80.96)

purple bets:
Sairentozon7: 20,907G (51.6%, 20,907G)
upvla: 10,000G (24.7%, 19,765G)
Lythe_Caraker: 2,500G (6.2%, 152,492G)
ForagerCats: 2,000G (4.9%, 9,263G)
randgridr: 1,000G (2.5%, 8,074G)
BirbBrainsBot: 1,000G (2.5%, 137,758G)
CT_5_Holy: 983G (2.4%, 983G)
ColetteMSLP: 536G (1.3%, 536G)
Lanshaft: 492G (1.2%, 23,641G)
superdevon1: 353G (0.9%, 3,538G)
iBardic: 257G (0.6%, 257G)
pLifer: 252G (0.6%, 252G)
datadrivenbot: 200G (0.5%, 33,720G)

brown bets:
FFTBattleground: 208G (41.6%, 500G)
AllInBot: 200G (40.0%, 200G)
getthemoneyz: 92G (18.4%, 1,329,237G)

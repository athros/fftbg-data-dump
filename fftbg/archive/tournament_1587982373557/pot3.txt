Final Bets: white - 7 bets for 4,481G (49.9%, x1.00); black - 10 bets for 4,493G (50.1%, x1.00)

white bets:
waterwatereverywhere: 1,199G (26.8%, 1,199G)
BirbBrainsBot: 748G (16.7%, 57,399G)
CT_5_Holy: 728G (16.2%, 728G)
ColetteMSLP: 600G (13.4%, 24,298G)
Reductions: 596G (13.3%, 1,192G)
volgrathemoose: 364G (8.1%, 364G)
getthemoneyz: 246G (5.5%, 600,148G)

black bets:
ko2q: 1,072G (23.9%, 1,072G)
Mesmaster: 1,000G (22.3%, 33,859G)
R_Raynos: 856G (19.1%, 856G)
LordSDB: 500G (11.1%, 1,746G)
rechaun: 444G (9.9%, 444G)
LivingHitokiri: 200G (4.5%, 3,569G)
ungabunga_bot: 164G (3.7%, 264,667G)
AlurenFTW: 100G (2.2%, 1,462G)
datadrivenbot: 100G (2.2%, 3,740G)
roqqqpsi: 57G (1.3%, 2,883G)

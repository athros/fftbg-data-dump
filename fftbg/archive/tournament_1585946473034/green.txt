Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Male
Scorpio
49
78
Priest
Yin Yang Magic
Auto Potion
Doublehand
Swim

Wizard Staff

Holy Miter
Wizard Outfit
Leather Mantle

Cure, Cure 2, Cure 3, Regen, Protect, Protect 2, Shell, Wall, Esuna, Holy
Blind, Silence Song, Dispel Magic, Petrify, Dark Holy



Urieltheflameofgod
Male
Virgo
47
37
Lancer
Yin Yang Magic
Earplug
Beastmaster
Teleport

Ivory Rod
Platinum Shield
Bronze Helmet
Genji Armor
Magic Gauntlet

Level Jump4, Vertical Jump6
Blind, Spell Absorb, Zombie, Foxbird, Dispel Magic



Fenaen
Female
Virgo
43
78
Calculator
Yin Yang Magic
MP Restore
Magic Defense UP
Ignore Height

Papyrus Codex

Flash Hat
White Robe
Dracula Mantle

CT, Height, Prime Number, 5, 4
Poison, Life Drain, Silence Song, Dispel Magic, Paralyze, Dark Holy



Nizaha
Male
Leo
75
63
Ninja
Talk Skill
Absorb Used MP
Monster Talk
Move+3

Hidden Knife
Kunai
Flash Hat
Wizard Outfit
Chantage

Shuriken, Bomb
Death Sentence, Mimic Daravon, Rehabilitate

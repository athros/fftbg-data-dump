Player: !Red
Team: Red Team
Palettes: Red/Brown



TheChainNerd
Male
Libra
75
63
Bard
Talk Skill
Brave Up
Monster Talk
Move-HP Up

Fairy Harp

Triangle Hat
Mythril Vest
Small Mantle

Magic Song, Diamond Blade, Sky Demon
Invitation, Praise, Preach, Death Sentence, Negotiate, Mimic Daravon



B0shii
Male
Capricorn
65
56
Calculator
Yin Yang Magic
Faith Up
Magic Defense UP
Lava Walking

Rod

Feather Hat
Wizard Outfit
Angel Ring

CT, 4, 3
Blind, Poison, Blind Rage



Upvla
Female
Pisces
44
62
Geomancer
Yin Yang Magic
Damage Split
Equip Knife
Swim

Air Knife
Round Shield
Barette
Black Robe
Chantage

Water Ball, Hell Ivy, Local Quake, Static Shock, Blizzard
Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Foxbird, Petrify



Conome
Female
Cancer
57
71
Squire
Throw
MP Restore
Magic Defense UP
Waterbreathing

Poison Bow
Buckler
Feather Hat
Secret Clothes
Angel Ring

Accumulate, Dash, Throw Stone, Heal, Fury
Shuriken, Knife, Staff, Wand

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Jonusfatson
Male
Scorpio
44
72
Priest
Battle Skill
Catch
Long Status
Ignore Terrain

Scorpion Tail

Golden Hairpin
Silk Robe
Genji Gauntlet

Cure, Cure 3, Cure 4, Raise, Protect, Protect 2, Esuna
Armor Break, Magic Break, Stasis Sword, Justice Sword



Cheesetacular
Male
Leo
77
63
Knight
Jump
Auto Potion
Dual Wield
Move+2

Defender
Broad Sword
Bronze Helmet
Crystal Mail
Diamond Armlet

Armor Break, Magic Break, Justice Sword
Level Jump3, Vertical Jump2



Kronikle
Male
Capricorn
46
43
Lancer
Yin Yang Magic
Dragon Spirit
Beastmaster
Levitate

Gokuu Rod
Buckler
Cross Helmet
Gold Armor
Leather Mantle

Level Jump5, Vertical Jump2
Poison, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Dispel Magic, Petrify



Shalloween
Male
Virgo
71
79
Monk
Time Magic
Critical Quick
Dual Wield
Waterwalking



Black Hood
Black Costume
Dracula Mantle

Spin Fist, Purification, Revive
Haste 2, Slow, Immobilize, Reflect, Demi, Stabilize Time

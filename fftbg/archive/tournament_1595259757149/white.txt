Player: !White
Team: White Team
Palettes: White/Blue



Sairentozon7
Female
Pisces
52
45
Knight
Black Magic
Abandon
Concentrate
Ignore Terrain

Ragnarok
Flame Shield
Gold Helmet
Leather Armor
N-Kai Armlet

Dark Sword
Fire 2, Fire 3, Bolt 3, Ice, Ice 2, Ice 4, Empower, Frog, Death, Flare



Randgridr
Male
Gemini
56
39
Ninja
Punch Art
Absorb Used MP
Magic Attack UP
Move-HP Up

Kunai
Sasuke Knife
Twist Headband
Earth Clothes
Elf Mantle

Shuriken, Staff
Spin Fist, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Mattcorn7
Female
Virgo
47
58
Archer
Elemental
Parry
Sicken
Move-MP Up

Ice Bow

Red Hood
Secret Clothes
Elf Mantle

Charge+1, Charge+2, Charge+4, Charge+5, Charge+7
Pitfall, Water Ball, Hallowed Ground, Quicksand, Gusty Wind, Lava Ball



Cryptopsy70
Female
Pisces
40
76
Ninja
Charge
MA Save
Magic Defense UP
Lava Walking

Kunai
Mythril Knife
Green Beret
Clothes
Germinas Boots

Shuriken, Bomb, Knife, Axe
Charge+1, Charge+2, Charge+4, Charge+7

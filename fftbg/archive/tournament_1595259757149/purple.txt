Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



RaIshtar
Female
Serpentarius
72
67
Geomancer
Basic Skill
Auto Potion
Magic Attack UP
Move+3

Bizen Boat
Diamond Shield
Barette
Adaman Vest
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Accumulate, Heal, Yell, Fury



Actual JP
Female
Gemini
80
45
Archer
Steal
Meatbone Slash
Doublehand
Fly

Glacier Gun

Triangle Hat
Adaman Vest
Jade Armlet

Charge+1, Charge+3, Charge+10, Charge+20
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Accessory



SUGRboi
Male
Libra
66
48
Archer
Summon Magic
Regenerator
Dual Wield
Jump+2

Long Bow

Twist Headband
Wizard Outfit
Small Mantle

Charge+1, Charge+10
Ramuh, Titan, Salamander



Zetchryn
Male
Pisces
67
56
Monk
Draw Out
Catch
Defense UP
Ignore Height



Triangle Hat
Leather Outfit
Angel Ring

Pummel, Earth Slash, Purification, Chakra
Koutetsu, Murasame, Kiyomori

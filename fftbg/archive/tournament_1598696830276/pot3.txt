Final Bets: white - 8 bets for 5,350G (55.9%, x0.79); black - 7 bets for 4,222G (44.1%, x1.27)

white bets:
Mesmaster: 2,000G (37.4%, 77,623G)
Aldrammech: 897G (16.8%, 897G)
E_Ballard: 792G (14.8%, 63,514G)
Panushenko: 765G (14.3%, 765G)
VolgraTheMoose: 501G (9.4%, 21,831G)
datadrivenbot: 200G (3.7%, 58,249G)
AllInBot: 100G (1.9%, 100G)
roqqqpsi: 95G (1.8%, 1,056G)

black bets:
MemoriesofFinal: 1,045G (24.8%, 1,045G)
BirbBrainsBot: 1,000G (23.7%, 128,794G)
3ngag3: 800G (18.9%, 11,800G)
ForagerCats: 500G (11.8%, 2,513G)
skipsandwiches: 482G (11.4%, 482G)
getthemoneyz: 294G (7.0%, 1,775,254G)
gorgewall: 101G (2.4%, 10,471G)

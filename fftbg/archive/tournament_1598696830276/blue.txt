Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Skipsandwiches
Male
Aquarius
58
77
Monk
Time Magic
Distribute
Beastmaster
Waterbreathing



Triangle Hat
Adaman Vest
Spike Shoes

Spin Fist, Pummel, Secret Fist, Purification, Seal Evil
Haste, Stop, Immobilize, Float, Reflect, Demi, Galaxy Stop



MemoriesofFinal
Female
Taurus
44
66
Knight
Jump
Counter Flood
Equip Polearm
Retreat

Spear
Buckler
Diamond Helmet
Wizard Robe
Jade Armlet

Head Break, Weapon Break, Power Break, Mind Break, Justice Sword
Level Jump5, Vertical Jump8



Jeeboheebo
Male
Serpentarius
43
67
Bard
Throw
Dragon Spirit
Doublehand
Jump+2

Ramia Harp

Headgear
Plate Mail
Magic Gauntlet

Angel Song, Life Song, Nameless Song, Hydra Pit
Shuriken, Stick, Wand



Smashy
Female
Sagittarius
68
46
Samurai
Time Magic
Speed Save
Equip Gun
Jump+1

Mythril Gun

Platinum Helmet
Wizard Robe
Reflect Ring

Asura, Murasame, Heaven's Cloud
Reflect, Quick, Demi 2, Stabilize Time, Meteor

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Powergems
Female
Aquarius
50
55
Thief
Throw
Auto Potion
Sicken
Fly

Blind Knife

Ribbon
Rubber Costume
Angel Ring

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Knife



CosmicTactician
Female
Libra
56
69
Archer
Steal
Hamedo
Magic Defense UP
Ignore Height

Poison Bow
Venetian Shield
Green Beret
Judo Outfit
Leather Mantle

Charge+1, Charge+7
Gil Taking, Steal Accessory, Steal Status, Arm Aim



Bongomon7
Female
Cancer
66
42
Time Mage
Item
Distribute
Doublehand
Teleport

Wizard Staff

Golden Hairpin
Silk Robe
Power Wrist

Haste, Slow, Immobilize, Float, Quick, Stabilize Time
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Toka222
Female
Taurus
64
78
Priest
Summon Magic
Counter
Secret Hunt
Lava Walking

Oak Staff

Flash Hat
Black Costume
Cherche

Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Shell, Esuna
Ifrit, Titan, Carbunkle, Salamander, Silf

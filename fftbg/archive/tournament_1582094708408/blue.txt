Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Denamda
Male
Sagittarius
61
58
Archer
Punch Art
Catch
Equip Shield
Jump+1

Night Killer
Platinum Shield
Flash Hat
Mystic Vest
Feather Boots

Charge+1, Charge+3, Charge+4, Charge+5
Spin Fist, Secret Fist, Purification



Invaderzimbo77
Male
Libra
75
63
Bard
Basic Skill
MP Restore
Equip Axe
Retreat

Wizard Staff

Holy Miter
Gold Armor
Leather Mantle

Battle Song, Last Song, Diamond Blade
Throw Stone, Tickle, Yell, Wish



Rolodex
Male
Aquarius
59
76
Samurai
Time Magic
Counter Tackle
Equip Polearm
Ignore Height

Murasame

Leather Helmet
Linen Robe
Bracer

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Stop, Immobilize, Reflect, Demi 2



LanseDM
Female
Scorpio
35
64
Samurai
White Magic
Counter
Concentrate
Jump+3

Javelin

Leather Helmet
Wizard Robe
108 Gems

Koutetsu, Murasame, Heaven's Cloud
Cure 3, Raise, Raise 2, Protect 2, Shell, Shell 2, Esuna, Holy

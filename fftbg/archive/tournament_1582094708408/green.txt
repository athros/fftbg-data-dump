Player: !Green
Team: Green Team
Palettes: Green/White



ExpectedResults
Male
Libra
80
54
Monk
Time Magic
Speed Save
Secret Hunt
Move+1



Triangle Hat
Clothes
Small Mantle

Purification, Revive
Slow, Slow 2, Reflect, Quick, Demi



Revvy
Male
Scorpio
69
49
Monk
Yin Yang Magic
PA Save
Magic Attack UP
Move+2



Feather Hat
Wizard Outfit
Dracula Mantle

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Pray Faith, Zombie



Lazlodamus
Female
Cancer
59
52
Dancer
Steal
Parry
Defense UP
Waterwalking

Star Bag

Golden Hairpin
Power Sleeve
Diamond Armlet

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon
Steal Weapon, Arm Aim



Dynasti
Male
Virgo
67
49
Knight
Summon Magic
Meatbone Slash
Doublehand
Fly

Defender

Diamond Helmet
Linen Robe
Diamond Armlet

Head Break, Magic Break, Dark Sword, Night Sword
Shiva, Ramuh, Ifrit, Leviathan, Fairy, Lich

Player: !Green
Team: Green Team
Palettes: Green/White



Nekojin
Female
Gemini
66
75
Dancer
Jump
MA Save
Equip Shield
Move+1

Persia
Mythril Shield
Green Beret
Leather Outfit
Leather Mantle

Nameless Dance, Last Dance, Obsidian Blade, Void Storage
Level Jump8, Vertical Jump7



EnemyController
Female
Sagittarius
43
62
Geomancer
Draw Out
Caution
Equip Knife
Waterwalking

Short Edge
Diamond Shield
Thief Hat
Light Robe
Sprint Shoes

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Asura, Bizen Boat, Murasame, Masamune



GrayGhostGaming
Female
Virgo
73
69
Priest
Charge
Speed Save
Equip Armor
Move+2

White Staff

Bronze Helmet
Linen Robe
Genji Gauntlet

Cure, Cure 3, Raise, Raise 2, Reraise, Shell 2, Wall, Holy
Charge+1, Charge+7, Charge+20



Killth3kid
Female
Virgo
79
59
Mime

Regenerator
Equip Shield
Ignore Terrain


Flame Shield
Twist Headband
Earth Clothes
N-Kai Armlet

Mimic


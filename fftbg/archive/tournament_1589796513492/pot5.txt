Final Bets: red - 7 bets for 1,751G (19.3%, x4.19); green - 9 bets for 7,332G (80.7%, x0.24)

red bets:
thaetreis: 650G (37.1%, 3,600G)
Who_lio42: 500G (28.6%, 11,470G)
gorgewall: 201G (11.5%, 12,734G)
YaBoy125: 100G (5.7%, 9,395G)
lastly: 100G (5.7%, 13,618G)
Lawndough: 100G (5.7%, 3,134G)
datadrivenbot: 100G (5.7%, 19,648G)

green bets:
HorusTaurus: 2,544G (34.7%, 2,544G)
Mesmaster: 1,000G (13.6%, 79,373G)
ungabunga_bot: 1,000G (13.6%, 527,474G)
BirbBrainsBot: 1,000G (13.6%, 154,303G)
leakimiko: 1,000G (13.6%, 36,146G)
AmaninAmide: 300G (4.1%, 8,586G)
getthemoneyz: 288G (3.9%, 684,725G)
MrUbiq: 100G (1.4%, 3,475G)
Evewho: 100G (1.4%, 990G)

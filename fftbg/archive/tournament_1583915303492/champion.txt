Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



KasugaiRoastedPeas
Female
Capricorn
45
54
Mime

Absorb Used MP
Attack UP
Move-MP Up



Black Hood
Rubber Costume
Defense Armlet

Mimic




Sneakyness
Monster
Gemini
40
44
Dark Behemoth










Alaylle
Female
Libra
79
57
Mediator
Draw Out
Brave Up
Equip Gun
Swim

Blast Gun

Twist Headband
Light Robe
Jade Armlet

Invitation, Threaten, Preach, Mimic Daravon, Rehabilitate
Asura, Koutetsu, Murasame, Kiyomori



LanseDM
Female
Sagittarius
60
41
Geomancer
Draw Out
Critical Quick
Equip Sword
Waterwalking

Ragnarok

Leather Hat
Silk Robe
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Heaven's Cloud

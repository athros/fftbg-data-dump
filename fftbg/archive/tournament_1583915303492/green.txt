Player: !Green
Team: Green Team
Palettes: Green/White



Bpc2163
Female
Taurus
42
71
Wizard
Steal
Caution
Magic Attack UP
Move-MP Up

Wizard Rod

Golden Hairpin
White Robe
Spike Shoes

Fire, Bolt 2, Bolt 4, Ice, Ice 2
Steal Shield



Upvla
Male
Leo
66
50
Monk
Jump
Counter
Sicken
Waterwalking



Headgear
Black Costume
Feather Mantle

Spin Fist, Secret Fist, Purification, Seal Evil
Level Jump4, Vertical Jump7



Grininda
Male
Virgo
78
61
Knight
Charge
Caution
Defense UP
Move+1

Defender

Platinum Helmet
Crystal Mail
Defense Ring

Head Break, Mind Break, Stasis Sword, Night Sword, Explosion Sword
Charge+2, Charge+3, Charge+5



ZergTwitch
Male
Capricorn
69
53
Lancer
Talk Skill
HP Restore
Equip Gun
Lava Walking

Mythril Gun
Aegis Shield
Leather Helmet
Silk Robe
Bracer

Level Jump5, Vertical Jump7
Threaten, Rehabilitate

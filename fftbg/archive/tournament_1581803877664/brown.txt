Player: !Brown
Team: Brown Team
Palettes: Brown/Green



E Ballard
Male
Virgo
63
74
Bard
Battle Skill
Faith Up
Equip Bow
Waterbreathing

Bow Gun

Thief Hat
Mythril Armor
Magic Gauntlet

Life Song, Cheer Song, Battle Song, Space Storage
Speed Break, Mind Break, Dark Sword



AzuriteReaction
Monster
Leo
65
62
Grenade










The Pengwin
Male
Leo
45
45
Monk
Elemental
Earplug
Attack UP
Move+1



Thief Hat
Power Sleeve
Bracer

Wave Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Mirapoix
Female
Sagittarius
78
48
Squire
Time Magic
Regenerator
Short Status
Lava Walking

Materia Blade

Triangle Hat
Earth Clothes
Cherche

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up
Slow, Reflect, Stabilize Time

Final Bets: white - 20 bets for 9,023G (23.8%, x3.20); champion - 22 bets for 28,900G (76.2%, x0.31)

white bets:
Mesmaster: 2,000G (22.2%, 45,829G)
BirbBrainsBot: 1,000G (11.1%, 150,320G)
Dymntd: 1,000G (11.1%, 3,166G)
Ross_from_Cali: 777G (8.6%, 11,435G)
Mirrorclaw: 500G (5.5%, 3,062G)
Jerboj: 500G (5.5%, 61,971G)
DustBirdEX: 456G (5.1%, 3,891G)
electric_glass: 333G (3.7%, 6,045G)
LAGBOT30000: 322G (3.6%, 32,258G)
Estan_AD: 316G (3.5%, 316G)
ApplesNP: 304G (3.4%, 304G)
gorgewall: 300G (3.3%, 19,734G)
KasugaiRoastedPeas: 200G (2.2%, 6,064G)
twelfthrootoftwo: 200G (2.2%, 3,851G)
luminarii: 200G (2.2%, 19,649G)
ko2q: 199G (2.2%, 2,697G)
getthemoneyz: 116G (1.3%, 610,956G)
kuterian: 100G (1.1%, 1,366G)
datadrivenbot: 100G (1.1%, 10,981G)
alecttox: 100G (1.1%, 8,448G)

champion bets:
reinoe: 10,192G (35.3%, 20,384G)
sinnyil2: 4,114G (14.2%, 7,968G)
HaplessOne: 3,003G (10.4%, 12,013G)
Billybones5150: 2,000G (6.9%, 27,935G)
sect_cor: 2,000G (6.9%, 24,428G)
DeathTaxesAndAnime: 1,031G (3.6%, 2,060G)
jhaller2: 1,000G (3.5%, 4,143G)
Rislyeu: 887G (3.1%, 887G)
ColetteMSLP: 800G (2.8%, 44,432G)
ZZ_Yoshi: 777G (2.7%, 71,126G)
Laserman1000: 582G (2.0%, 74,482G)
vorap: 500G (1.7%, 28,773G)
Bioticism: 500G (1.7%, 1,697G)
itzover9001: 328G (1.1%, 328G)
moonliquor: 326G (1.1%, 326G)
squiregonnacary: 200G (0.7%, 1,536G)
RestIessNight: 129G (0.4%, 129G)
ungabunga_bot: 127G (0.4%, 291,055G)
nepSmug: 104G (0.4%, 104G)
ApplesauceBoss: 100G (0.3%, 4,877G)
Firesheath: 100G (0.3%, 12,713G)
ANFz: 100G (0.3%, 23,774G)

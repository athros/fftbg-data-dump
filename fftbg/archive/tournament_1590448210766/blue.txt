Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Theseawolf1
Male
Sagittarius
56
64
Lancer
Sing
Mana Shield
Defend
Ignore Height

Spear
Gold Shield
Mythril Helmet
Wizard Robe
Jade Armlet

Level Jump4, Vertical Jump4
Life Song, Cheer Song, Battle Song, Magic Song, Last Song, Diamond Blade



Milkman153
Female
Cancer
48
79
Mime

MP Restore
Magic Attack UP
Waterbreathing



Red Hood
Wizard Robe
Reflect Ring

Mimic




RaIshtar
Male
Sagittarius
67
77
Geomancer
Draw Out
Faith Save
Equip Axe
Move-MP Up

Flail
Ice Shield
Leather Hat
Chameleon Robe
Sprint Shoes

Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Heaven's Cloud



CandyTamer
Male
Gemini
66
56
Thief
Draw Out
Counter Flood
Short Status
Waterwalking

Air Knife

Leather Hat
Clothes
Dracula Mantle

Steal Heart, Steal Helmet
Bizen Boat, Heaven's Cloud, Kikuichimoji

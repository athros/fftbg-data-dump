Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lydian C
Female
Serpentarius
50
50
Calculator
Yin Yang Magic
Catch
Magic Attack UP
Swim

Poison Rod

Headgear
Robe of Lords
Jade Armlet

CT, Prime Number, 5, 4, 3
Poison, Foxbird, Dark Holy



Ar Tactic
Female
Taurus
78
53
Ninja
Black Magic
Auto Potion
Equip Axe
Ignore Terrain

Mythril Knife
Healing Staff
Headgear
Clothes
Rubber Shoes

Knife, Stick
Fire 4, Bolt, Bolt 2, Bolt 3, Death



Ko2q
Female
Leo
43
54
Time Mage
Basic Skill
Catch
Halve MP
Move+2

Madlemgen

Headgear
Clothes
N-Kai Armlet

Haste, Meteor
Accumulate, Dash, Heal, Yell, Cheer Up



Rislyeu
Male
Sagittarius
63
53
Ninja
Basic Skill
Counter Tackle
Equip Axe
Move+3

Battle Axe
Short Edge
Black Hood
Leather Outfit
N-Kai Armlet

Shuriken, Staff
Dash, Heal, Tickle, Yell, Cheer Up

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nizaha
Male
Virgo
73
60
Thief
Time Magic
PA Save
Defend
Teleport 2

Iron Sword

Red Hood
Wizard Outfit
Chantage

Steal Heart, Arm Aim, Leg Aim
Haste 2, Slow, Quick, Demi, Demi 2, Meteor, Galaxy Stop



Dinin991
Female
Leo
49
62
Ninja
Dance
Counter
Equip Gun
Jump+2

Madlemgen
Papyrus Codex
Twist Headband
Mythril Vest
Diamond Armlet

Dictionary
Polka Polka, Disillusion, Dragon Pit



Lanshaft
Male
Leo
45
47
Knight
White Magic
Earplug
Equip Bow
Waterwalking

Ice Bow
Genji Shield
Diamond Helmet
Mythril Armor
Power Wrist

Head Break, Shield Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword
Cure 2, Raise, Reraise, Wall, Esuna, Holy



CosmicTactician
Female
Aries
68
58
Samurai
Charge
Catch
Dual Wield
Waterbreathing

Chirijiraden
Asura Knife
Iron Helmet
Platinum Armor
108 Gems

Kiyomori, Masamune
Charge+1, Charge+7

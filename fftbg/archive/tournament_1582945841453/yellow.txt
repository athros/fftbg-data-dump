Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lythe Caraker
Female
Capricorn
62
61
Dancer
Steal
MP Restore
Equip Gun
Swim

Glacier Gun

Golden Hairpin
Silk Robe
Chantage

Witch Hunt, Polka Polka, Last Dance, Void Storage, Nether Demon
Gil Taking, Steal Heart, Steal Armor



DudeOMG
Monster
Aries
80
61
Draugr










MrUbiq
Female
Aquarius
71
68
Dancer
Basic Skill
Brave Up
Equip Bow
Teleport

Long Bow

Green Beret
Power Sleeve
Magic Gauntlet

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Dragon Pit
Throw Stone, Heal, Ultima



Waterwatereverywhere
Male
Libra
61
60
Thief
Time Magic
Regenerator
Short Charge
Jump+1

Long Sword

Thief Hat
Wizard Outfit
Feather Boots

Steal Heart, Steal Shield, Leg Aim
Haste 2, Immobilize, Reflect, Quick, Demi 2, Stabilize Time

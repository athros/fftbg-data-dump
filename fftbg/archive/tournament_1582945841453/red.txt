Player: !Red
Team: Red Team
Palettes: Red/Brown



Inderf
Male
Capricorn
66
40
Samurai
Steal
Hamedo
Beastmaster
Move+1

Mythril Spear

Mythril Helmet
Crystal Mail
Genji Gauntlet

Koutetsu, Muramasa
Steal Armor, Steal Weapon, Steal Accessory



Hasterious
Monster
Aries
66
73
Explosive










MagicBottle
Female
Gemini
66
46
Samurai
Punch Art
Parry
Equip Armor
Retreat

Holy Lance

Leather Hat
Light Robe
Genji Gauntlet

Bizen Boat, Muramasa
Earth Slash, Secret Fist, Purification



OttoRaynar
Female
Gemini
45
44
Oracle
Elemental
Counter
Magic Defense UP
Retreat

Cypress Rod

Green Beret
Black Robe
Wizard Mantle

Poison, Life Drain, Silence Song, Confusion Song, Dispel Magic, Sleep
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind

Player: !Green
Team: Green Team
Palettes: Green/White



Harbinger605
Male
Capricorn
64
47
Mediator
White Magic
HP Restore
Equip Gun
Ignore Terrain

Ramia Harp

Twist Headband
Silk Robe
Leather Mantle

Invitation, Persuade, Threaten, Preach, Solution, Negotiate, Refute
Cure, Raise, Raise 2, Wall, Esuna



Holybobomb
Female
Virgo
63
46
Mime

Brave Up
Defense UP
Retreat



Thief Hat
Clothes
Defense Ring

Mimic




DeathTaxesAndAnime
Female
Scorpio
66
77
Priest
Basic Skill
Hamedo
Equip Gun
Waterbreathing

Papyrus Codex

Flash Hat
Chain Vest
Small Mantle

Raise, Protect 2, Shell, Esuna
Accumulate, Throw Stone, Heal, Tickle, Yell, Wish, Ultima



Phi Sig
Female
Cancer
63
72
Priest
Charge
Parry
Equip Shield
Ignore Terrain

Gold Staff
Aegis Shield
Leather Hat
Chameleon Robe
108 Gems

Cure 3, Raise, Reraise, Protect, Protect 2, Shell 2, Wall, Esuna, Holy
Charge+2, Charge+5

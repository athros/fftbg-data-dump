Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nhammen
Female
Aries
48
55
Dancer
Punch Art
Dragon Spirit
Defend
Move+2

Hydra Bag

Golden Hairpin
White Robe
Battle Boots

Witch Hunt, Polka Polka, Last Dance, Nether Demon
Wave Fist, Secret Fist, Revive



MrFlabyo
Male
Capricorn
79
50
Time Mage
Draw Out
Meatbone Slash
Dual Wield
Move+2

Musk Rod
Iron Fan
Twist Headband
Silk Robe
Feather Boots

Slow 2, Immobilize, Reflect, Stabilize Time
Koutetsu, Murasame, Heaven's Cloud



StealthModeLocke
Female
Virgo
73
56
Monk
Draw Out
Blade Grasp
Equip Polearm
Move+3

Mythril Spear

Twist Headband
Mythril Vest
Jade Armlet

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra
Bizen Boat, Murasame, Heaven's Cloud



VolgraTheMoose
Male
Libra
44
57
Ninja
Elemental
Mana Shield
Attack UP
Move-MP Up

Mage Masher
Mythril Knife
Feather Hat
Leather Outfit
N-Kai Armlet

Shuriken, Knife
Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

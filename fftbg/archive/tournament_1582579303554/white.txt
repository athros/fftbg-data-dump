Player: !White
Team: White Team
Palettes: White/Blue



Kennymillennium
Female
Leo
80
73
Summoner
Punch Art
Distribute
Magic Attack UP
Move+3

Wizard Rod

Flash Hat
White Robe
Feather Boots

Moogle, Ifrit, Leviathan, Fairy
Spin Fist, Pummel



Silgryn
Female
Cancer
73
64
Archer
Talk Skill
Absorb Used MP
Equip Shield
Jump+2

Yoichi Bow
Bronze Shield
Green Beret
Clothes
Angel Ring

Charge+1
Persuade, Negotiate, Mimic Daravon, Refute



Blacklistclancy
Female
Aquarius
66
55
Ninja
Elemental
Caution
Equip Gun
Move+1

Madlemgen
Madlemgen
Thief Hat
Judo Outfit
Magic Gauntlet

Shuriken, Staff
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Sand Storm, Blizzard, Lava Ball



ForagerCats
Male
Taurus
68
65
Geomancer
Yin Yang Magic
Blade Grasp
Magic Defense UP
Fly

Battle Axe
Diamond Shield
Red Hood
Black Robe
Angel Ring

Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard
Spell Absorb, Life Drain, Zombie, Confusion Song, Dispel Magic, Sleep

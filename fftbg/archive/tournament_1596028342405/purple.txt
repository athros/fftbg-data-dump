Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



E Ballard
Male
Leo
59
49
Ninja
Battle Skill
Regenerator
Equip Sword
Waterwalking

Iron Sword
Morning Star
Black Hood
Secret Clothes
Red Shoes

Bomb, Knife
Head Break, Magic Break, Speed Break, Mind Break, Justice Sword, Surging Sword, Explosion Sword



CosmicTactician
Female
Cancer
76
51
Summoner
Draw Out
Catch
Short Charge
Retreat

Poison Rod

Headgear
Chameleon Robe
Feather Mantle

Moogle, Carbunkle, Leviathan
Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori



Grininda
Male
Pisces
63
52
Archer
Yin Yang Magic
Speed Save
Magic Attack UP
Move+2

Snipe Bow
Buckler
Leather Helmet
Adaman Vest
Jade Armlet

Charge+1, Charge+3, Charge+10, Charge+20
Blind, Poison, Blind Rage, Foxbird, Dispel Magic, Petrify



J2DaBibbles
Female
Cancer
53
53
Time Mage
Item
Catch
Throw Item
Teleport

Ivory Rod

Black Hood
Wizard Outfit
Power Wrist

Haste, Haste 2, Float, Quick, Demi, Demi 2
Potion, Hi-Potion, Hi-Ether, Holy Water, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



Klednar21
Male
Cancer
71
58
Bard
Draw Out
Counter Tackle
Equip Bow
Fly

Night Killer

Black Hood
Gold Armor
Battle Boots

Angel Song, Battle Song, Magic Song, Nameless Song
Asura, Murasame, Heaven's Cloud



Zagorsek
Monster
Capricorn
73
76
Wild Boar










SQUiDSQUARKLIN
Female
Scorpio
71
54
Oracle
Summon Magic
MP Restore
Equip Armor
Jump+2

Iron Fan

Cross Helmet
Leather Armor
Red Shoes

Life Drain, Pray Faith, Doubt Faith, Zombie, Foxbird, Sleep
Moogle, Golem, Leviathan, Silf



Sinnyil2
Female
Taurus
59
66
Summoner
Time Magic
MP Restore
Beastmaster
Levitate

Papyrus Codex

Red Hood
Wizard Outfit
Diamond Armlet

Moogle, Ifrit, Titan, Leviathan, Salamander
Haste, Stop, Reflect, Demi, Stabilize Time

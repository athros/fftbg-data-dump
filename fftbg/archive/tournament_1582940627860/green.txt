Player: !Green
Team: Green Team
Palettes: Green/White



Alaylle
Female
Scorpio
74
59
Oracle
Draw Out
Counter
Long Status
Move+2

Gokuu Rod

Holy Miter
Judo Outfit
Reflect Ring

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Petrify
Koutetsu



RagequitSA
Male
Pisces
61
69
Ninja
Steal
MP Restore
Martial Arts
Lava Walking

Short Edge
Short Edge
Flash Hat
Mystic Vest
Elf Mantle

Knife, Ninja Sword
Steal Armor, Steal Shield, Steal Weapon



BuffaloCrunch
Female
Virgo
45
46
Summoner
Black Magic
HP Restore
Dual Wield
Fly

White Staff
Ice Rod
Golden Hairpin
Rubber Costume
Magic Ring

Moogle, Shiva, Titan, Odin, Leviathan, Silf, Cyclops
Fire 2, Ice, Empower, Flare



Furorcelt
Male
Gemini
50
80
Thief
Throw
PA Save
Doublehand
Move-HP Up

Spell Edge

Red Hood
Judo Outfit
Diamond Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Shuriken, Hammer

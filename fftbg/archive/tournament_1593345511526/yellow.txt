Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



GladiatorLupe
Male
Serpentarius
53
45
Ninja
Yin Yang Magic
Critical Quick
Equip Gun
Move-MP Up

Mythril Gun
Blast Gun
Green Beret
Mystic Vest
Salty Rage

Shuriken, Bomb, Knife, Ninja Sword, Wand
Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep



EizanTayama
Female
Libra
58
48
Dancer
Black Magic
Counter Tackle
Equip Axe
Move+1

Slasher

Red Hood
Chameleon Robe
Spike Shoes

Wiznaibus, Obsidian Blade, Void Storage
Fire 2, Bolt, Bolt 2, Bolt 3, Ice 2, Ice 4, Flare



Ominnous
Male
Aries
66
68
Knight
Jump
Mana Shield
Short Charge
Ignore Height

Long Sword
Crystal Shield
Crystal Helmet
Genji Armor
Spike Shoes

Armor Break, Shield Break, Mind Break
Level Jump2, Vertical Jump8



DavenIII
Male
Aquarius
79
60
Thief
Elemental
Distribute
Equip Gun
Levitate

Mythril Gun

Triangle Hat
Mythril Vest
Dracula Mantle

Steal Heart, Steal Accessory, Steal Status, Leg Aim
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

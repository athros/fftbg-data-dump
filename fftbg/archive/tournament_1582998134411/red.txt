Player: !Red
Team: Red Team
Palettes: Red/Brown



Tougou
Male
Aries
51
71
Bard
Draw Out
MP Restore
Doublehand
Swim

Ice Bow

Headgear
Black Costume
Angel Ring

Angel Song, Battle Song
Asura, Murasame, Heaven's Cloud



SkylerBunny
Female
Aquarius
50
79
Oracle
Punch Art
Arrow Guard
Equip Polearm
Waterbreathing

Ivory Rod

Golden Hairpin
Silk Robe
Leather Mantle

Blind, Poison, Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Dark Holy
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Tithonus
Female
Leo
68
61
Oracle
Item
Critical Quick
Equip Bow
Jump+3

Mythril Bow

Golden Hairpin
Chameleon Robe
Elf Mantle

Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic
Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Phoenix Down



Aldrammech
Female
Virgo
54
72
Priest
Battle Skill
Catch
Equip Shield
Ignore Terrain

Gold Staff
Bronze Shield
Triangle Hat
Wizard Robe
Leather Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Wall, Esuna, Holy
Armor Break

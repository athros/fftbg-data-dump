Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ko2q
Female
Scorpio
58
52
Oracle
Charge
PA Save
Beastmaster
Levitate

Octagon Rod

Triangle Hat
Chameleon Robe
Diamond Armlet

Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Charge+7, Charge+20



Roqqqpsi
Female
Taurus
61
78
Mime

MP Restore
Short Status
Ignore Terrain



Golden Hairpin
Gold Armor
Wizard Mantle

Mimic




Ominnous
Male
Virgo
60
54
Lancer
Battle Skill
MA Save
Magic Defense UP
Move+1

Mythril Spear
Buckler
Bronze Helmet
Black Robe
Battle Boots

Level Jump8, Vertical Jump6
Shield Break, Weapon Break, Power Break, Surging Sword



SpaZmodeus
Female
Pisces
43
50
Archer
Steal
Critical Quick
Short Status
Swim

Night Killer
Escutcheon
Green Beret
Judo Outfit
Reflect Ring

Charge+1, Charge+20
Steal Weapon

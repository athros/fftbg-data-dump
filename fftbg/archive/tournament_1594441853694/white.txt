Player: !White
Team: White Team
Palettes: White/Blue



Thyrandaal
Female
Scorpio
46
80
Time Mage
Draw Out
Counter Tackle
Equip Axe
Move-HP Up

Giant Axe

Headgear
Light Robe
Defense Ring

Haste, Slow 2, Stop, Immobilize, Reflect, Stabilize Time
Kiyomori



Evontno
Monster
Aquarius
60
62
Holy Dragon










Mudrockk
Female
Sagittarius
64
68
Wizard
Dance
Dragon Spirit
Equip Shield
Retreat

Main Gauche
Genji Shield
Leather Hat
Leather Outfit
Magic Gauntlet

Fire 3, Bolt, Bolt 4, Ice 2, Empower
Witch Hunt, Wiznaibus, Slow Dance, Dragon Pit



Maddrave09
Male
Virgo
76
58
Knight
Item
Counter
Defense UP
Swim

Giant Axe
Platinum Shield
Gold Helmet
Gold Armor
Defense Armlet

Shield Break, Weapon Break, Speed Break, Justice Sword, Dark Sword
Hi-Potion, Ether, Hi-Ether, Soft, Remedy, Phoenix Down

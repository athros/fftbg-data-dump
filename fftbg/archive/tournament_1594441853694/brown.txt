Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DustBirdEX
Female
Libra
43
53
Mediator
Throw
Damage Split
Equip Gun
Move+2

Blast Gun

Red Hood
Light Robe
Dracula Mantle

Invitation, Praise, Threaten, Preach, Death Sentence, Mimic Daravon, Refute
Shuriken, Bomb



ArchKnightX
Monster
Sagittarius
51
80
Porky










DeathTaxesAndAnime
Female
Leo
62
59
Lancer
Black Magic
Dragon Spirit
Defend
Waterbreathing

Obelisk
Flame Shield
Circlet
Light Robe
Cursed Ring

Level Jump8, Vertical Jump3
Fire 4, Bolt 4, Ice 4



Letdowncity
Male
Aries
51
71
Squire
Summon Magic
Caution
Equip Armor
Lava Walking

Gastrafitis
Crystal Shield
Diamond Helmet
Light Robe
108 Gems

Dash, Heal, Yell, Cheer Up, Wish, Ultima
Moogle, Shiva, Golem, Carbunkle, Fairy

Player: !Black
Team: Black Team
Palettes: Black/Red



Reinoe
Female
Leo
78
78
Summoner
Item
Auto Potion
Beastmaster
Move+3

Oak Staff

Leather Hat
Wizard Outfit
Diamond Armlet

Moogle, Ifrit, Golem, Bahamut, Salamander, Fairy, Lich
Potion, Hi-Potion, Maiden's Kiss, Holy Water, Phoenix Down



Aldrammech
Male
Libra
61
53
Time Mage
Sing
Sunken State
Attack UP
Swim

Bestiary

Twist Headband
Chain Vest
Germinas Boots

Haste, Haste 2, Immobilize, Quick, Demi 2, Stabilize Time
Nameless Song, Last Song



XalSmellsBad
Female
Pisces
70
76
Mediator
Steal
MP Restore
Short Charge
Waterbreathing

Cultist Dagger

Twist Headband
Power Sleeve
Chantage

Praise, Preach, Insult, Mimic Daravon, Rehabilitate
Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Leg Aim



EnemyController
Male
Cancer
57
59
Mime

Damage Split
Magic Defense UP
Retreat



Golden Hairpin
Brigandine
Germinas Boots

Mimic


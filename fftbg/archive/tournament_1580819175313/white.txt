Player: !White
Team: White Team
Palettes: White/Blue



Belivalan
Male
Sagittarius
58
58
Ninja
Item
Counter
Equip Gun
Lava Walking

Short Edge
Cultist Dagger
Black Hood
Clothes
Elf Mantle

Shuriken, Staff, Stick, Dictionary
Potion, Hi-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down



Adenis222
Male
Scorpio
71
45
Lancer
Black Magic
Abandon
Attack UP
Move+2

Mythril Spear
Ice Shield
Iron Helmet
Chain Mail
Genji Gauntlet

Level Jump3, Vertical Jump5
Fire 4, Bolt 3, Ice, Ice 2



MohawkBigNOS
Male
Aries
72
76
Calculator
Yin Yang Magic
Damage Split
Equip Gun
Levitate

Papyrus Codex

Flash Hat
Black Robe
108 Gems

CT, Height, Prime Number
Blind, Pray Faith, Doubt Faith, Foxbird, Dispel Magic, Paralyze



Fozzington
Male
Scorpio
59
45
Archer
Yin Yang Magic
MP Restore
Magic Defense UP
Fly

Mythril Bow

Green Beret
Clothes
Red Shoes

Charge+1, Charge+10
Doubt Faith, Silence Song, Blind Rage, Dispel Magic

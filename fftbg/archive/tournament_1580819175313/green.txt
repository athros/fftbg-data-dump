Player: !Green
Team: Green Team
Palettes: Green/White



Hyvi
Female
Pisces
49
71
Archer
Throw
PA Save
Equip Gun
Teleport

Cross Bow
Flame Shield
Flash Hat
Brigandine
Defense Armlet

Charge+2, Charge+3, Charge+10
Sword, Axe, Dictionary



Quinnotetiquan
Male
Taurus
80
46
Time Mage
Summon Magic
MP Restore
Beastmaster
Levitate

Gold Staff

Red Hood
Light Robe
Reflect Ring

Haste, Immobilize, Demi, Stabilize Time, Meteor
Moogle, Ramuh



Neohart
Male
Taurus
55
52
Samurai
White Magic
HP Restore
Attack UP
Ignore Height

Partisan

Platinum Helmet
Linen Cuirass
Power Wrist

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Raise, Raise 2, Reraise, Protect 2, Shell, Esuna



Time  Mage
Female
Gemini
53
66
Lancer
Draw Out
Counter
Equip Polearm
Jump+3

Mythril Spear
Buckler
Leather Helmet
Linen Robe
Battle Boots

Level Jump4, Vertical Jump5
Kikuichimoji

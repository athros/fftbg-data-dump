Player: !Green
Team: Green Team
Palettes: Green/White



ScarletSpiderBen
Female
Pisces
47
44
Ninja
Talk Skill
Mana Shield
Attack UP
Waterbreathing

Scorpion Tail
Blind Knife
Flash Hat
Mystic Vest
Battle Boots

Bomb, Knife, Staff
Invitation, Persuade, Threaten, Solution, Death Sentence, Rehabilitate



Sairentozon7
Male
Virgo
65
48
Archer
Yin Yang Magic
Caution
Short Charge
Waterbreathing

Poison Bow
Buckler
Mythril Helmet
Leather Outfit
Rubber Shoes

Charge+1, Charge+2, Charge+20
Spell Absorb, Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep, Dark Holy



Nocturnal Perspective
Female
Leo
73
73
Squire
Black Magic
Parry
Halve MP
Ignore Height

Poison Bow
Platinum Shield
Headgear
Mythril Vest
Magic Gauntlet

Dash, Heal, Yell, Fury
Fire, Fire 2, Fire 3, Ice 2, Ice 3, Empower, Death



Upvla
Male
Scorpio
61
52
Ninja
Sing
Sunken State
Doublehand
Levitate

Iga Knife

Headgear
Black Costume
Rubber Shoes

Shuriken, Stick
Life Song, Nameless Song, Diamond Blade

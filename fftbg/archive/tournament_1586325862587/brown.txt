Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NoNotBees
Female
Leo
75
57
Archer
Item
Speed Save
Short Charge
Swim

Hunting Bow
Escutcheon
Headgear
Earth Clothes
Magic Ring

Charge+1, Charge+3, Charge+7
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down



SeedSC
Female
Capricorn
67
59
Priest
Steal
Arrow Guard
Equip Sword
Ignore Height

Kiyomori

Black Hood
Secret Clothes
Cherche

Cure, Cure 2, Cure 4, Raise, Reraise, Regen, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Steal Heart, Steal Shield



Gripheenix
Male
Virgo
43
77
Monk
Talk Skill
Catch
Equip Armor
Teleport



Cross Helmet
Earth Clothes
Feather Mantle

Purification, Chakra, Revive
Praise, Refute, Rehabilitate



Chuckolator
Female
Virgo
55
48
Monk
Talk Skill
Critical Quick
Monster Talk
Waterbreathing



Holy Miter
Chain Vest
Magic Gauntlet

Spin Fist, Wave Fist, Chakra, Seal Evil
Preach, Death Sentence, Insult, Negotiate, Refute

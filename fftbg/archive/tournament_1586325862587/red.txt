Player: !Red
Team: Red Team
Palettes: Red/Brown



L2 Sentinel
Male
Taurus
49
52
Lancer
Punch Art
Speed Save
Attack UP
Retreat

Javelin
Hero Shield
Iron Helmet
Diamond Armor
Elf Mantle

Level Jump3, Vertical Jump6
Pummel, Wave Fist, Earth Slash, Revive



Lijarkh
Female
Scorpio
69
36
Time Mage
Steal
Counter Tackle
Beastmaster
Waterbreathing

Rainbow Staff

Green Beret
Light Robe
Cherche

Slow 2, Stop, Reflect, Demi 2, Stabilize Time
Steal Helmet, Steal Accessory



Bryan792
Female
Libra
65
53
Geomancer
Draw Out
Counter Magic
Halve MP
Fly

Platinum Sword
Flame Shield
Green Beret
Mystic Vest
Power Wrist

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Kiyomori, Masamune



Gooseyourself
Female
Aquarius
55
62
Calculator
Time Magic
Brave Up
Defend
Waterwalking

Musk Rod

Flash Hat
Judo Outfit
Spike Shoes

CT, Prime Number, 5
Haste, Slow, Slow 2, Immobilize, Float, Quick, Demi 2, Stabilize Time

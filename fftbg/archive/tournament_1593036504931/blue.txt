Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Choco Joe
Male
Virgo
68
50
Bard
Elemental
Damage Split
Martial Arts
Jump+2

Fairy Harp

Thief Hat
Chain Mail
Jade Armlet

Life Song, Nameless Song, Hydra Pit
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



Joewcarson
Female
Pisces
47
55
Summoner
Talk Skill
Auto Potion
Martial Arts
Retreat

Rainbow Staff

Twist Headband
Wizard Robe
Leather Mantle

Moogle, Ifrit, Titan, Carbunkle, Bahamut, Odin, Leviathan, Lich, Cyclops
Praise, Threaten, Insult, Refute



Daveb
Female
Sagittarius
38
49
Ninja
Talk Skill
Auto Potion
Short Status
Move+2

Kunai
Spell Edge
Twist Headband
Mystic Vest
Feather Mantle

Shuriken
Invitation, Preach, Solution, Death Sentence, Insult, Negotiate, Rehabilitate



Lewdylew
Male
Taurus
79
78
Bard
White Magic
Brave Save
Defend
Waterbreathing

Bloody Strings

Feather Hat
Adaman Vest
Germinas Boots

Life Song, Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Cure, Raise, Regen, Protect 2, Esuna

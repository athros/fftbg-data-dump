Final Bets: purple - 12 bets for 9,764G (31.7%, x2.15); brown - 15 bets for 20,989G (68.3%, x0.47)

purple bets:
E_Ballard: 2,720G (27.9%, 2,720G)
gamesage53: 2,307G (23.6%, 2,307G)
thewondertrickster: 1,045G (10.7%, 2,091G)
BirbBrainsBot: 1,000G (10.2%, 143,247G)
Cryptopsy70: 592G (6.1%, 25,811G)
neerrm: 433G (4.4%, 433G)
MagicBottle: 428G (4.4%, 428G)
Aldrammech: 364G (3.7%, 364G)
getthemoneyz: 316G (3.2%, 1,022,366G)
ColetteMSLP: 300G (3.1%, 3,024G)
lewdylew: 159G (1.6%, 159G)
Firesheath: 100G (1.0%, 3,308G)

brown bets:
OneHundredFists: 7,416G (35.3%, 14,543G)
evontno: 5,290G (25.2%, 5,290G)
Mesmaster: 3,000G (14.3%, 205,869G)
VolgraTheMoose: 2,073G (9.9%, 4,146G)
eudes89: 1,236G (5.9%, 1,236G)
DustBirdEX: 345G (1.6%, 3,005G)
ZCKaiser: 317G (1.5%, 635G)
twelfthrootoftwo: 300G (1.4%, 2,793G)
letdowncity: 200G (1.0%, 7,713G)
Magicandy: 200G (1.0%, 3,971G)
BlackfyreRoy: 200G (1.0%, 17,800G)
almontys: 112G (0.5%, 112G)
CosmicTactician: 100G (0.5%, 17,619G)
datadrivenbot: 100G (0.5%, 48,851G)
ericzubat: 100G (0.5%, 3,771G)

Player: !Green
Team: Green Team
Palettes: Green/White



Midori Ribbon
Female
Scorpio
47
49
Samurai
Battle Skill
Absorb Used MP
Equip Axe
Ignore Terrain

White Staff

Barbuta
Black Robe
Small Mantle

Asura
Head Break, Armor Break, Magic Break, Power Break, Mind Break, Night Sword



Chuckolator
Female
Aquarius
62
62
Samurai
Yin Yang Magic
Distribute
Magic Defense UP
Retreat

Javelin

Genji Helmet
Chain Mail
Genji Gauntlet

Asura, Koutetsu, Heaven's Cloud
Doubt Faith, Blind Rage, Dispel Magic, Sleep



Realitydown
Male
Pisces
72
78
Wizard
Time Magic
Regenerator
Doublehand
Move+3

Wizard Rod

Twist Headband
Black Robe
Reflect Ring

Fire, Fire 4, Bolt, Bolt 2, Death
Haste, Slow 2, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time



Pink LuigiX
Male
Sagittarius
63
59
Time Mage
Summon Magic
Counter Magic
Equip Gun
Waterbreathing

Blaze Gun

Holy Miter
Wizard Robe
Feather Boots

Stop, Demi, Demi 2, Meteor
Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan

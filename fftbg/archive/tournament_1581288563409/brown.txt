Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Evergreenentity
Male
Libra
49
71
Priest
Draw Out
PA Save
Equip Axe
Fly

Flail

Red Hood
Adaman Vest
Elf Mantle

Cure 3, Raise, Regen, Protect 2, Esuna, Holy
Koutetsu



Netokon
Male
Libra
44
47
Mediator
Throw
Distribute
Magic Defense UP
Move+3

Glacier Gun

Red Hood
Silk Robe
Salty Rage

Persuade, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Shuriken



DrakeRowan
Male
Capricorn
58
60
Monk
Item
Auto Potion
Magic Defense UP
Levitate



Holy Miter
Black Costume
Genji Gauntlet

Pummel, Wave Fist, Purification, Chakra, Revive
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Phoenix Down



Shripe
Male
Aquarius
51
61
Ninja
Sing
Catch
Equip Axe
Move+2

Morning Star
Ninja Edge
Flash Hat
Mythril Vest
N-Kai Armlet

Shuriken, Bomb, Staff
Angel Song, Magic Song, Sky Demon

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeniorBunk
Female
Virgo
67
63
Archer
Battle Skill
Critical Quick
Equip Sword
Jump+2

Defender

Flash Hat
Brigandine
Defense Armlet

Charge+5, Charge+7, Charge+10
Head Break, Armor Break, Weapon Break, Speed Break, Mind Break, Stasis Sword, Night Sword, Surging Sword



ShadowedFlames
Female
Scorpio
76
76
Archer
Throw
HP Restore
Defense UP
Waterbreathing

Hunting Bow
Round Shield
Red Hood
Rubber Costume
108 Gems

Charge+1, Charge+2, Charge+3, Charge+10, Charge+20
Knife, Hammer



Zeando
Female
Aquarius
48
46
Time Mage
Steal
Absorb Used MP
Concentrate
Fly

White Staff

Feather Hat
Wizard Outfit
Magic Ring

Haste, Slow 2, Stop, Immobilize, Float, Quick, Stabilize Time
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim



Miku Shikhu
Female
Aquarius
49
75
Ninja
Battle Skill
PA Save
Equip Bow
Move-HP Up

Poison Bow
Poison Bow
Leather Hat
Mythril Vest
Elf Mantle

Shuriken, Bomb, Ninja Sword, Wand, Dictionary
Shield Break, Speed Break, Explosion Sword

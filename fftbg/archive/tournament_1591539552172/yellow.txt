Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Fenixcrest
Female
Gemini
36
62
Knight
Throw
Damage Split
Equip Sword
Move+1

Ancient Sword
Crystal Shield
Diamond Helmet
Chameleon Robe
Jade Armlet

Shield Break, Magic Break, Stasis Sword, Surging Sword
Shuriken, Ninja Sword, Axe



BlackFireUK
Female
Scorpio
47
64
Wizard
Battle Skill
Counter
Short Charge
Waterwalking

Orichalcum

Black Hood
Linen Robe
Defense Ring

Fire, Fire 4, Bolt, Bolt 4, Ice 2
Head Break, Armor Break, Speed Break, Mind Break, Justice Sword, Surging Sword



Mesmaster
Monster
Capricorn
53
73
Red Dragon










Just Here2
Female
Scorpio
71
75
Chemist
Yin Yang Magic
HP Restore
Equip Armor
Waterbreathing

Stone Gun

Golden Hairpin
Carabini Mail
Feather Boots

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Doubt Faith, Silence Song, Dispel Magic, Paralyze

Player: !White
Team: White Team
Palettes: White/Blue



Thyrandaal
Male
Aries
41
66
Archer
Jump
Counter
Sicken
Move+3

Night Killer
Mythril Shield
Black Hood
Adaman Vest
Wizard Mantle

Charge+1, Charge+2, Charge+3, Charge+5
Level Jump3, Vertical Jump7



Powergems
Male
Capricorn
49
64
Ninja
Battle Skill
Earplug
Equip Sword
Retreat

Ragnarok
Bizen Boat
Twist Headband
Mystic Vest
Elf Mantle

Knife, Staff
Armor Break, Shield Break, Speed Break, Power Break, Mind Break, Justice Sword, Surging Sword



GrandmasterFrankerZ
Male
Cancer
70
70
Summoner
Talk Skill
Absorb Used MP
Monster Talk
Move+2

Oak Staff

Leather Hat
Linen Robe
Bracer

Moogle, Ifrit, Carbunkle, Bahamut, Leviathan, Silf
Invitation, Praise, Threaten, Preach, Insult, Negotiate, Refute, Rehabilitate



VolgraTheMoose
Monster
Libra
82
77
Red Dragon








Final Bets: yellow - 11 bets for 6,829G (65.2%, x0.53); champion - 7 bets for 3,644G (34.8%, x1.87)

yellow bets:
DavenIII: 2,500G (36.6%, 106,890G)
roqqqpsi: 796G (11.7%, 6,127G)
Lydian_C: 760G (11.1%, 2,893G)
Tecc2: 500G (7.3%, 6,110G)
maximumcrit: 500G (7.3%, 5,371G)
Valdimar_5467: 500G (7.3%, 10,085G)
Draconis345: 500G (7.3%, 19,683G)
Lord_Gwarth: 337G (4.9%, 2,448G)
datadrivenbot: 200G (2.9%, 64,766G)
Lazarus_DS: 136G (2.0%, 136G)
CosmicTactician: 100G (1.5%, 35,866G)

champion bets:
VolgraTheMoose: 1,501G (41.2%, 33,941G)
BirbBrainsBot: 1,000G (27.4%, 120,873G)
NIghtdew14: 581G (15.9%, 581G)
nhammen: 200G (5.5%, 129,360G)
getthemoneyz: 162G (4.4%, 1,702,830G)
AllInBot: 100G (2.7%, 100G)
neerrm: 100G (2.7%, 44,402G)

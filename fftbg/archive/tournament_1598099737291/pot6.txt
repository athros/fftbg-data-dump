Final Bets: white - 7 bets for 3,424G (25.1%, x2.98); purple - 6 bets for 10,194G (74.9%, x0.34)

white bets:
neerrm: 1,001G (29.2%, 42,423G)
Tecc2: 1,000G (29.2%, 4,383G)
Nizaha: 764G (22.3%, 14,605G)
CosmicTactician: 200G (5.8%, 34,717G)
datadrivenbot: 200G (5.8%, 64,062G)
Lazarus_DS: 160G (4.7%, 160G)
roqqqpsi: 99G (2.9%, 4,960G)

purple bets:
reinoe: 5,000G (49.0%, 70,812G)
DavenIII: 3,500G (34.3%, 106,668G)
BirbBrainsBot: 1,000G (9.8%, 121,329G)
NIghtdew14: 376G (3.7%, 376G)
getthemoneyz: 218G (2.1%, 1,702,703G)
AllInBot: 100G (1.0%, 100G)

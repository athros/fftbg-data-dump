Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



PlatinumPlume
Male
Sagittarius
45
71
Knight
Throw
Auto Potion
Attack UP
Jump+3

Blood Sword
Bronze Shield
Cross Helmet
Bronze Armor
Bracer

Shield Break, Weapon Break, Speed Break, Power Break, Mind Break
Shuriken, Bomb



Malus55
Monster
Leo
59
60
Malboro










MalakoFox
Male
Libra
79
66
Chemist
Throw
Auto Potion
Equip Knife
Levitate

Wizard Rod

Headgear
Mystic Vest
Magic Gauntlet

Potion, X-Potion, Ether, Eye Drop, Holy Water
Shuriken, Axe, Wand



Howplausible
Male
Aquarius
56
53
Bard
Summon Magic
MP Restore
Long Status
Retreat

Ramia Harp

Flash Hat
Mythril Vest
Elf Mantle

Battle Song, Magic Song, Nameless Song, Diamond Blade, Sky Demon, Hydra Pit
Salamander

Player: !Red
Team: Red Team
Palettes: Red/Brown



Maakur
Female
Serpentarius
55
51
Oracle
Battle Skill
Hamedo
Defend
Levitate

Cute Bag

Red Hood
Leather Outfit
Dracula Mantle

Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Head Break, Speed Break, Justice Sword



Galkife
Male
Capricorn
43
54
Lancer
Black Magic
Regenerator
Maintenance
Ignore Terrain

Partisan
Crystal Shield
Leather Helmet
White Robe
Leather Mantle

Level Jump5, Vertical Jump6
Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Death



Kintaru Oe
Monster
Capricorn
48
44
Floating Eye










WunUP
Male
Aries
73
64
Archer
Punch Art
MP Restore
Magic Attack UP
Ignore Terrain

Lightning Bow

Black Hood
Adaman Vest
Dracula Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Pummel, Earth Slash, Chakra

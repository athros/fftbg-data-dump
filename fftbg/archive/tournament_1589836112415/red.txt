Player: !Red
Team: Red Team
Palettes: Red/Brown



Eudes89
Male
Capricorn
55
72
Knight
Punch Art
Caution
Magic Attack UP
Ignore Height

Defender
Round Shield
Leather Helmet
White Robe
Leather Mantle

Armor Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Dark Sword
Secret Fist



Aidanlucky7
Female
Capricorn
69
74
Mime

MA Save
Martial Arts
Levitate



Gold Helmet
Judo Outfit
Setiemson

Mimic




Sinnyil2
Female
Scorpio
57
70
Wizard
Draw Out
Sunken State
Dual Wield
Move+1

Ice Rod
Flame Rod
Green Beret
Black Costume
Rubber Shoes

Fire, Fire 3, Bolt
Asura, Koutetsu, Bizen Boat, Heaven's Cloud



ParadiseNinja
Male
Aquarius
72
54
Lancer
Basic Skill
Sunken State
Secret Hunt
Ignore Terrain

Spear
Platinum Shield
Crystal Helmet
Linen Cuirass
Jade Armlet

Level Jump4, Vertical Jump2
Accumulate, Dash, Heal, Fury

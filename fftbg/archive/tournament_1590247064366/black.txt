Player: !Black
Team: Black Team
Palettes: Black/Red



Powergems
Male
Virgo
70
76
Ninja
Item
Critical Quick
Equip Sword
Move-MP Up

Chaos Blade
Blood Sword
Feather Hat
Leather Outfit
Feather Mantle

Shuriken
Potion, X-Potion, Antidote, Eye Drop, Echo Grass



Mudrockk
Female
Scorpio
71
44
Time Mage
Throw
Regenerator
Equip Bow
Move-MP Up

Lightning Bow

Triangle Hat
Black Robe
Red Shoes

Haste, Haste 2, Slow, Reflect, Quick, Demi
Shuriken, Staff



Lawndough
Female
Aquarius
79
55
Squire
Draw Out
Catch
Defense UP
Jump+1

Slasher
Diamond Shield
Iron Helmet
Chain Vest
Defense Armlet

Accumulate, Dash, Throw Stone, Heal, Yell, Wish
Asura, Koutetsu, Murasame



ALY327
Female
Pisces
76
44
Mediator
Time Magic
Counter Tackle
Maintenance
Levitate

Blaze Gun

Red Hood
Silk Robe
Germinas Boots

Invitation, Praise, Threaten, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Demi, Demi 2

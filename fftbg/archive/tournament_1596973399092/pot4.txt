Final Bets: purple - 7 bets for 2,444G (33.2%, x2.01); brown - 8 bets for 4,924G (66.8%, x0.50)

purple bets:
BirbBrainsBot: 1,000G (40.9%, 138,437G)
getthemoneyz: 480G (19.6%, 1,560,766G)
gargola2498: 300G (12.3%, 2,731G)
DesertWooder: 264G (10.8%, 264G)
AllInBot: 200G (8.2%, 200G)
srdonko2098: 100G (4.1%, 1,758G)
onehitslay: 100G (4.1%, 2,279G)

brown bets:
ko2q: 2,204G (44.8%, 4,408G)
randgridr: 1,295G (26.3%, 1,295G)
dogsandcatsand: 500G (10.2%, 36,480G)
roqqqpsi: 324G (6.6%, 324G)
gorgewall: 201G (4.1%, 3,307G)
datadrivenbot: 200G (4.1%, 50,933G)
MinBetBot: 100G (2.0%, 17,020G)
StrawbreeRed: 100G (2.0%, 104G)

Player: !Green
Team: Green Team
Palettes: Green/White



HaterDave
Female
Sagittarius
73
67
Lancer
Charge
Caution
Maintenance
Jump+1

Spear
Diamond Shield
Bronze Helmet
Plate Mail
Jade Armlet

Level Jump2, Vertical Jump2
Charge+4, Charge+7, Charge+10



Part Ways
Male
Capricorn
68
67
Monk
Black Magic
Regenerator
Equip Gun
Jump+2

Stone Gun

Green Beret
Wizard Outfit
Defense Ring

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive
Fire, Fire 3, Bolt 2, Ice, Death



TheBlobZ
Male
Virgo
76
52
Ninja
Black Magic
Counter
Equip Gun
Teleport

Flail
Hidden Knife
Flash Hat
Earth Clothes
Bracer

Spear, Dictionary
Fire 3, Bolt 3, Ice, Ice 2, Empower, Frog



Kingbananabear
Female
Leo
56
58
Ninja
White Magic
MA Save
Sicken
Waterwalking

Flail
Morning Star
Thief Hat
Leather Outfit
Red Shoes

Shuriken
Raise, Regen, Protect 2, Wall

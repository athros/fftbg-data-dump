Player: !Green
Team: Green Team
Palettes: Green/White



Azreyam
Male
Capricorn
48
77
Oracle
Charge
Distribute
Secret Hunt
Fly

Battle Bamboo

Black Hood
Light Robe
Spike Shoes

Blind Rage, Dispel Magic, Paralyze, Petrify
Charge+1, Charge+2, Charge+4, Charge+10



MrDusa
Male
Cancer
75
60
Ninja
White Magic
Regenerator
Equip Axe
Waterbreathing

Battle Axe
Battle Axe
Holy Miter
Earth Clothes
Red Shoes

Shuriken
Cure 4, Raise, Regen, Shell, Esuna, Holy



Denamda
Female
Leo
57
66
Samurai
Time Magic
Caution
Magic Attack UP
Ignore Height

Murasame

Barbuta
Crystal Mail
Spike Shoes

Bizen Boat, Muramasa
Haste, Slow, Slow 2, Stop, Stabilize Time



Peluducer
Male
Virgo
76
74
Chemist
Steal
PA Save
Defend
Levitate

Romanda Gun

Feather Hat
Mythril Vest
Battle Boots

Potion, Hi-Potion, Ether, Phoenix Down
Gil Taking, Steal Status, Leg Aim

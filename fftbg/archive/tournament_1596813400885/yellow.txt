Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ShintaroNayaka
Female
Cancer
75
51
Summoner
Item
Critical Quick
Throw Item
Move+1

Dragon Rod

Golden Hairpin
Adaman Vest
Red Shoes

Moogle, Ifrit, Titan, Carbunkle, Salamander, Cyclops
Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



HaateXIII
Female
Virgo
47
49
Dancer
Elemental
Hamedo
Halve MP
Waterbreathing

Ryozan Silk

Feather Hat
Light Robe
Diamond Armlet

Wiznaibus, Polka Polka, Disillusion, Last Dance, Nether Demon
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



JDogg2K4
Monster
Sagittarius
55
47
Skeleton










Roofiepops
Female
Virgo
71
55
Time Mage
Basic Skill
Hamedo
Equip Polearm
Ignore Terrain

Javelin

Headgear
Earth Clothes
Salty Rage

Haste 2, Slow, Immobilize, Quick, Demi, Demi 2, Stabilize Time
Throw Stone, Tickle, Cheer Up, Fury

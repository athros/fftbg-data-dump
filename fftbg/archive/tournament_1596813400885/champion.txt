Player: !zChamp
Team: Champion Team
Palettes: Green/White



Maakur
Male
Aquarius
43
64
Samurai
Time Magic
Hamedo
Defend
Swim

Javelin

Barbuta
Light Robe
Diamond Armlet

Kikuichimoji
Haste, Stop, Quick, Demi, Stabilize Time



ArashiKurobara
Female
Capricorn
73
53
Dancer
White Magic
Dragon Spirit
Defend
Lava Walking

Ryozan Silk

Green Beret
Clothes
108 Gems

Polka Polka, Last Dance, Nether Demon
Cure, Cure 2, Cure 3, Protect, Wall, Esuna



Pandasforsale
Male
Aries
48
73
Chemist
Yin Yang Magic
Counter Magic
Halve MP
Ignore Terrain

Glacier Gun

Holy Miter
Earth Clothes
Wizard Mantle

X-Potion, Ether, Hi-Ether, Eye Drop, Soft, Remedy, Phoenix Down
Pray Faith, Doubt Faith, Paralyze, Dark Holy



Ruebyy
Female
Cancer
53
71
Samurai
Jump
Brave Save
Equip Sword
Waterwalking

Asura Knife

Genji Helmet
Plate Mail
Jade Armlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kikuichimoji
Level Jump2, Vertical Jump8

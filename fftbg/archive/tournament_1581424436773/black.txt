Player: !Black
Team: Black Team
Palettes: Black/Red



Sinnyil2
Female
Serpentarius
76
69
Oracle
Draw Out
Earplug
Halve MP
Jump+2

Gokuu Rod

Golden Hairpin
Power Sleeve
Red Shoes

Poison, Life Drain, Doubt Faith, Zombie, Foxbird, Confusion Song, Paralyze, Dark Holy
Asura, Koutetsu, Muramasa, Masamune



Sairentozon7
Male
Gemini
62
49
Priest
Draw Out
Abandon
Martial Arts
Swim



Green Beret
Silk Robe
108 Gems

Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Murasame, Kiyomori



Jaguary14
Female
Pisces
70
68
Archer
Item
MP Restore
Throw Item
Move+3

Night Killer
Crystal Shield
Twist Headband
Clothes
Genji Gauntlet

Charge+1, Charge+4, Charge+5, Charge+7, Charge+10
Potion, Eye Drop, Holy Water, Remedy, Phoenix Down



SpraycanHitman
Female
Gemini
45
66
Mime

Meatbone Slash
Equip Shield
Move+3


Aegis Shield
Leather Helmet
Mythril Vest
Magic Gauntlet

Mimic


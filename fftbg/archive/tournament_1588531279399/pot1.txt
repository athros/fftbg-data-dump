Final Bets: red - 20 bets for 7,239G (36.7%, x1.73); blue - 11 bets for 12,506G (63.3%, x0.58)

red bets:
ElectricLogan: 1,856G (25.6%, 3,813G)
Aldrammech: 1,000G (13.8%, 25,940G)
DustBirdEX: 543G (7.5%, 1,855G)
Breakdown777: 500G (6.9%, 20,126G)
Eyepoor_: 500G (6.9%, 1,434G)
gooseyourself: 436G (6.0%, 436G)
joewcarson: 300G (4.1%, 5,891G)
SQUiDSQUARKLIN: 300G (4.1%, 1,518G)
Dymntd: 276G (3.8%, 276G)
Lyner87: 250G (3.5%, 64,511G)
FNCardascia_: 212G (2.9%, 212G)
MeotorMang: 200G (2.8%, 9,031G)
gggamezzz: 144G (2.0%, 289G)
byrdturbo: 111G (1.5%, 11,538G)
ANFz: 111G (1.5%, 30,994G)
maakur_: 100G (1.4%, 119,008G)
Bloody_Nips: 100G (1.4%, 239G)
LorianFaust: 100G (1.4%, 2,589G)
frozentomato: 100G (1.4%, 994G)
datadrivenbot: 100G (1.4%, 12,697G)

blue bets:
Lordminsc: 2,538G (20.3%, 5,076G)
Pie108: 2,000G (16.0%, 164,420G)
TheUnforgivenRage: 1,727G (13.8%, 1,727G)
Zeroroute: 1,561G (12.5%, 3,255G)
DeathTaxesAndAnime: 1,046G (8.4%, 1,046G)
catfashions: 1,000G (8.0%, 7,117G)
Laserman1000: 852G (6.8%, 90,252G)
leakimiko: 800G (6.4%, 1,757G)
karmakamikaze: 462G (3.7%, 462G)
KasugaiRoastedPeas: 320G (2.6%, 5,167G)
Billybones5150: 200G (1.6%, 23,512G)

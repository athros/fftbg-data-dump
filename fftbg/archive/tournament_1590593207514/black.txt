Player: !Black
Team: Black Team
Palettes: Black/Red



Candina
Monster
Leo
51
75
Tiamat










Cloud92684
Male
Virgo
56
69
Priest
Jump
HP Restore
Halve MP
Retreat

White Staff

Golden Hairpin
Black Robe
Dracula Mantle

Cure 2, Cure 3, Reraise, Protect, Shell 2
Level Jump2, Vertical Jump2



Fenaen
Male
Sagittarius
67
53
Knight
Yin Yang Magic
Counter Flood
Doublehand
Jump+2

Giant Axe

Genji Helmet
Platinum Armor
Bracer

Head Break, Shield Break, Stasis Sword
Spell Absorb, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Killth3kid
Male
Leo
60
60
Archer
Elemental
Parry
Sicken
Move+3

Stone Gun
Platinum Shield
Red Hood
Clothes
Red Shoes

Charge+1, Charge+2, Charge+3, Charge+4
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CosmicTactician
Female
Taurus
70
71
Samurai
Summon Magic
Caution
Defense UP
Lava Walking

Bizen Boat

Gold Helmet
Chain Mail
Germinas Boots

Muramasa, Kikuichimoji
Shiva, Titan, Carbunkle, Odin, Leviathan, Salamander



Daveb
Male
Sagittarius
43
58
Ninja
Time Magic
Abandon
Equip Gun
Move-HP Up

Papyrus Codex
Bloody Strings
Triangle Hat
Clothes
Red Shoes

Shuriken, Hammer, Axe
Slow 2, Stop, Immobilize, Quick, Stabilize Time



NovaKnight21
Male
Cancer
66
50
Summoner
Item
MA Save
Short Charge
Jump+1

Papyrus Codex

Holy Miter
Light Robe
Power Wrist

Moogle, Ifrit, Titan, Leviathan, Salamander, Cyclops
Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down



AlenaZarek
Male
Sagittarius
55
70
Wizard
Battle Skill
Regenerator
Long Status
Levitate

Faith Rod

Flash Hat
Wizard Outfit
Feather Boots

Bolt 2, Bolt 4, Ice 2, Ice 4, Empower
Magic Break, Justice Sword

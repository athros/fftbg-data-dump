Final Bets: purple - 9 bets for 5,655G (35.6%, x1.81); brown - 9 bets for 10,217G (64.4%, x0.55)

purple bets:
argentbast: 1,500G (26.5%, 23,531G)
BirbBrainsBot: 1,000G (17.7%, 123,794G)
Mesmaster: 1,000G (17.7%, 142,128G)
Vaxaldan: 998G (17.6%, 998G)
DustBirdEX: 456G (8.1%, 22,312G)
HASTERIOUS: 441G (7.8%, 8,835G)
DAC169: 100G (1.8%, 4,031G)
getthemoneyz: 80G (1.4%, 782,850G)
itsadud: 80G (1.4%, 616G)

brown bets:
gogofromtogo: 2,926G (28.6%, 2,926G)
CorpusCav: 2,896G (28.3%, 43,523G)
gorgewall: 1,432G (14.0%, 1,432G)
foofermoofer: 1,190G (11.6%, 2,380G)
Shalloween: 940G (9.2%, 940G)
gr8keeper: 632G (6.2%, 632G)
ApplesauceBoss: 100G (1.0%, 5,812G)
datadrivenbot: 100G (1.0%, 30,631G)
bahamutlagooon: 1G (0.0%, 12,509G)

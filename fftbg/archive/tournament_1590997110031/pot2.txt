Final Bets: green - 11 bets for 7,088G (72.3%, x0.38); yellow - 6 bets for 2,721G (27.7%, x2.60)

green bets:
Mesmaster: 2,000G (28.2%, 143,360G)
BirbBrainsBot: 1,000G (14.1%, 124,410G)
Zachara: 1,000G (14.1%, 89,000G)
Shalloween: 789G (11.1%, 789G)
gorgewall: 667G (9.4%, 667G)
DustBirdEX: 456G (6.4%, 19,579G)
HASTERIOUS: 408G (5.8%, 8,178G)
Vaxaldan: 357G (5.0%, 357G)
foofermoofer: 195G (2.8%, 195G)
itsadud: 116G (1.6%, 116G)
datadrivenbot: 100G (1.4%, 29,997G)

yellow bets:
argentbast: 1,000G (36.8%, 25,031G)
Valentine009: 1,000G (36.8%, 6,971G)
gogofromtogo: 329G (12.1%, 329G)
getthemoneyz: 192G (7.1%, 783,802G)
ApplesauceBoss: 100G (3.7%, 5,412G)
CorpusCav: 100G (3.7%, 48,123G)

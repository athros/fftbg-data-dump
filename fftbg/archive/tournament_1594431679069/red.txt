Player: !Red
Team: Red Team
Palettes: Red/Brown



Upvla
Female
Capricorn
53
70
Summoner
Throw
Counter Flood
Short Charge
Jump+1

Battle Folio

Green Beret
Wizard Robe
Elf Mantle

Moogle, Shiva, Ifrit, Titan, Carbunkle, Leviathan, Silf
Staff, Axe



ThreeMileIsland
Female
Leo
75
55
Wizard
Draw Out
Critical Quick
Concentrate
Fly

Thunder Rod

Feather Hat
Black Robe
Spike Shoes

Bolt, Bolt 3, Ice, Ice 2, Ice 3, Frog
Asura



Rislyeu
Male
Taurus
51
44
Mime

Brave Save
Martial Arts
Retreat



Flash Hat
Black Costume
Bracer

Mimic




TheMurkGnome
Male
Sagittarius
46
68
Chemist
Basic Skill
Critical Quick
Sicken
Move-MP Up

Cute Bag

Thief Hat
Wizard Outfit
Germinas Boots

Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Tickle, Wish

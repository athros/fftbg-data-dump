Player: !Green
Team: Green Team
Palettes: Green/White



Miku Shikhu
Female
Capricorn
55
76
Calculator
Byblos
Regenerator
Equip Knife
Ignore Height

Short Edge
Ice Shield
Holy Miter
Bronze Armor
Diamond Armlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Jeras01
Male
Sagittarius
70
77
Time Mage
Jump
Critical Quick
Attack UP
Move+1

Gokuu Rod

Ribbon
Wizard Robe
Vanish Mantle

Haste, Stop, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time
Level Jump8, Vertical Jump7



Upvla
Male
Libra
48
44
Monk
Yin Yang Magic
Regenerator
Equip Bow
Jump+1

Hunting Bow

Twist Headband
Chain Vest
Bracer

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Poison, Doubt Faith, Blind Rage, Foxbird, Paralyze, Sleep, Dark Holy



OtherBrand
Female
Capricorn
51
38
Monk
Throw
Brave Save
Dual Wield
Ignore Terrain



Flash Hat
Mystic Vest
Feather Boots

Spin Fist, Earth Slash, Purification
Shuriken, Bomb, Stick, Dictionary

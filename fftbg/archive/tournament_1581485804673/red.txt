Player: !Red
Team: Red Team
Palettes: Red/Brown



Lodrak
Female
Taurus
50
45
Wizard
Yin Yang Magic
Meatbone Slash
Concentrate
Ignore Terrain

Mage Masher

Thief Hat
Wizard Outfit
Cherche

Fire 3, Bolt, Ice, Ice 2, Ice 3, Flare
Poison, Doubt Faith, Silence Song, Dispel Magic



Doogles4221
Female
Capricorn
47
57
Priest
Punch Art
Faith Up
Magic Attack UP
Move+3

Wizard Staff

Triangle Hat
Adaman Vest
Angel Ring

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Shell, Esuna, Holy
Secret Fist, Purification, Chakra, Revive



Mantisfinch
Male
Aries
58
68
Thief
Elemental
Dragon Spirit
Doublehand
Ignore Terrain

Blind Knife

Iron Helmet
Mythril Vest
Rubber Shoes

Steal Accessory, Steal Status
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



LordPaimon
Female
Aquarius
79
66
Ninja
Basic Skill
Auto Potion
Martial Arts
Move-MP Up



Headgear
Wizard Outfit
Battle Boots

Shuriken, Staff
Yell, Scream

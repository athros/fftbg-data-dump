Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Josephiroth 143
Female
Sagittarius
57
55
Chemist
Time Magic
Arrow Guard
Halve MP
Move+3

Glacier Gun

Feather Hat
Mystic Vest
Rubber Shoes

Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down
Haste, Immobilize, Float, Quick, Demi, Stabilize Time



D4rr1n
Male
Gemini
71
78
Chemist
Battle Skill
Auto Potion
Equip Knife
Move-MP Up

Poison Rod

Green Beret
Black Costume
Defense Armlet

Potion, Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Shield Break, Magic Break, Mind Break, Dark Sword



Gorgewall
Female
Aries
57
72
Summoner
Elemental
Dragon Spirit
Equip Polearm
Retreat

Dragon Whisker

Green Beret
Wizard Robe
Magic Ring

Moogle, Ramuh, Titan, Golem
Pitfall, Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Vorap
Female
Scorpio
81
63
Monk
Basic Skill
Critical Quick
Equip Shield
Jump+2


Diamond Shield
Twist Headband
Adaman Vest
Red Shoes

Wave Fist, Secret Fist, Revive, Seal Evil
Throw Stone, Heal, Wish

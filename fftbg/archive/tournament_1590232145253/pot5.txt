Final Bets: blue - 13 bets for 7,131G (72.5%, x0.38); yellow - 9 bets for 2,703G (27.5%, x2.64)

blue bets:
J2DaBibbles: 2,000G (28.0%, 21,061G)
BirbBrainsBot: 1,000G (14.0%, 126,931G)
ForagerCats: 1,000G (14.0%, 13,072G)
Lydian_C: 903G (12.7%, 903G)
josephiroth_143: 740G (10.4%, 740G)
otakutaylor: 488G (6.8%, 488G)
Kyune: 250G (3.5%, 19,199G)
d4rr1n: 200G (2.8%, 2,926G)
AllInBot: 150G (2.1%, 150G)
aurosaerondo: 100G (1.4%, 983G)
datadrivenbot: 100G (1.4%, 22,406G)
E_Ballard: 100G (1.4%, 6,981G)
Evewho: 100G (1.4%, 10,979G)

yellow bets:
roqqqpsi: 1,071G (39.6%, 9,741G)
YaBoy125: 424G (15.7%, 424G)
ShintaroNayaka: 300G (11.1%, 2,887G)
Digitalsocrates: 276G (10.2%, 276G)
Sakaejpk: 200G (7.4%, 2,224G)
getthemoneyz: 132G (4.9%, 700,155G)
CosmicTactician: 100G (3.7%, 19,158G)
littlebilly3: 100G (3.7%, 100G)
madman_clinton: 100G (3.7%, 1,106G)

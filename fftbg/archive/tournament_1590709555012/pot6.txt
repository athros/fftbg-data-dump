Final Bets: white - 11 bets for 8,563G (42.0%, x1.38); purple - 19 bets for 11,822G (58.0%, x0.72)

white bets:
metagameface: 3,859G (45.1%, 3,859G)
TheChainNerd: 2,000G (23.4%, 50,000G)
Aldrammech: 600G (7.0%, 600G)
lowlf: 592G (6.9%, 592G)
killth3kid: 500G (5.8%, 26,420G)
cloud92684: 300G (3.5%, 11,856G)
nifboy: 250G (2.9%, 11,748G)
AllInBot: 154G (1.8%, 154G)
DAC169: 108G (1.3%, 108G)
mrfripps: 100G (1.2%, 100G)
maakur_: 100G (1.2%, 8,348G)

purple bets:
rico_flex: 3,000G (25.4%, 25,345G)
Virilikus: 3,000G (25.4%, 184,078G)
EnemyController: 1,000G (8.5%, 439,386G)
BirbBrainsBot: 1,000G (8.5%, 63,027G)
volgrathemoose: 456G (3.9%, 456G)
luminarii: 450G (3.8%, 20,560G)
KonzeraLive: 400G (3.4%, 10,007G)
coralreeferz: 380G (3.2%, 380G)
kaelsun: 364G (3.1%, 364G)
Breakdown777: 300G (2.5%, 3,229G)
gr8keeper: 250G (2.1%, 3,514G)
TinchoT: 232G (2.0%, 232G)
Monopool: 200G (1.7%, 3,397G)
Flameatron: 200G (1.7%, 1,371G)
getthemoneyz: 190G (1.6%, 760,685G)
CorpusCav: 100G (0.8%, 7,269G)
ravingsockmonkey: 100G (0.8%, 496G)
CosmicTactician: 100G (0.8%, 1,514G)
datadrivenbot: 100G (0.8%, 28,707G)

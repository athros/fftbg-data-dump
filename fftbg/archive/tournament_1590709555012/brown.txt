Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Poulol
Male
Gemini
76
59
Archer
Sing
Parry
Equip Bow
Move+1

Ultimus Bow

Headgear
Chain Vest
Red Shoes

Charge+3, Charge+4
Life Song, Magic Song



Killth3kid
Female
Scorpio
75
72
Geomancer
Draw Out
MA Save
Martial Arts
Retreat

Battle Axe
Aegis Shield
Thief Hat
Black Robe
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind
Bizen Boat, Heaven's Cloud, Kikuichimoji



B0shii
Monster
Aquarius
67
47
Grenade










LeepingJJ
Male
Scorpio
69
43
Summoner
Yin Yang Magic
Dragon Spirit
Equip Axe
Waterbreathing

Gold Staff

Holy Miter
Chameleon Robe
N-Kai Armlet

Moogle, Carbunkle, Leviathan, Fairy
Blind, Life Drain, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy

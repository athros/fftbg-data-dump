Player: !White
Team: White Team
Palettes: White/Blue



Gogofromtogo
Female
Aquarius
60
47
Mime

Counter Tackle
Magic Attack UP
Fly



Holy Miter
Mythril Vest
Feather Boots

Mimic




CosmicTactician
Female
Aquarius
56
58
Mime

Distribute
Martial Arts
Fly



Red Hood
Clothes
Germinas Boots

Mimic




Zetchryn
Female
Scorpio
52
51
Time Mage
Black Magic
HP Restore
Equip Bow
Waterbreathing

Bow Gun

Triangle Hat
Robe of Lords
Diamond Armlet

Meteor
Fire 2, Fire 4, Bolt, Bolt 2, Death



Elelor
Male
Serpentarius
73
73
Samurai
Sing
Brave Save
Equip Shield
Jump+3

Asura Knife
Platinum Shield
Diamond Helmet
Gold Armor
Rubber Shoes

Asura, Kiyomori, Kikuichimoji
Battle Song, Last Song

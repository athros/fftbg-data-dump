Player: !Red
Team: Red Team
Palettes: Red/Brown



Docmitchphd
Male
Capricorn
68
58
Mime

Distribute
Dual Wield
Levitate



Ribbon
Brigandine
Sprint Shoes

Mimic




Baron Von Scrub
Female
Virgo
38
51
Mime

Catch
Equip Shield
Waterwalking


Mythril Shield
Black Hood
Judo Outfit
Leather Mantle

Mimic




ALY327
Female
Taurus
46
62
Summoner
Black Magic
Absorb Used MP
Concentrate
Ignore Height

Dragon Rod

Holy Miter
Secret Clothes
Spike Shoes

Moogle, Ramuh, Titan, Carbunkle, Silf
Fire 2, Bolt 2, Ice, Ice 2, Ice 3



StealthModeLocke
Female
Sagittarius
75
69
Dancer
Time Magic
Distribute
Defense UP
Ignore Height

Cashmere

Thief Hat
Linen Robe
Magic Ring

Wiznaibus, Last Dance
Meteor, Galaxy Stop

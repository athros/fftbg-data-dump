Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CosmicTactician
Female
Scorpio
62
40
Summoner
Battle Skill
Mana Shield
Halve MP
Lava Walking

Papyrus Codex

Headgear
Linen Robe
Power Wrist

Shiva, Ifrit, Carbunkle, Silf, Lich
Shield Break, Speed Break, Power Break



Killth3kid
Male
Virgo
55
77
Samurai
Elemental
Counter Tackle
Martial Arts
Move-MP Up

Murasame

Crystal Helmet
Linen Cuirass
Wizard Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



StealthModeLocke
Male
Virgo
50
56
Wizard
Elemental
Sunken State
Equip Sword
Teleport

Defender

Headgear
White Robe
N-Kai Armlet

Fire, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Empower, Frog, Death
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Benticore
Female
Leo
45
77
Mediator
Dance
Parry
Long Status
Jump+3

Mythril Gun

Feather Hat
Black Robe
Small Mantle

Preach, Insult, Mimic Daravon, Refute
Slow Dance, Nameless Dance

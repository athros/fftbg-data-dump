Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Grandlanzer
Female
Capricorn
49
53
Archer
White Magic
PA Save
Equip Shield
Jump+2

Yoichi Bow
Venetian Shield
Gold Helmet
Adaman Vest
Power Wrist

Charge+2, Charge+3, Charge+4, Charge+20
Cure, Cure 2, Raise, Shell 2, Esuna, Holy



LDSkinny
Female
Sagittarius
41
67
Oracle
Item
HP Restore
Dual Wield
Swim

Battle Folio
Papyrus Codex
Headgear
Chameleon Robe
Elf Mantle

Blind, Poison, Life Drain, Doubt Faith, Zombie, Foxbird, Confusion Song, Sleep
Hi-Potion, Ether, Hi-Ether, Antidote, Remedy, Phoenix Down



TeaTime29
Monster
Aries
53
64
Tiamat










HaychDub
Female
Scorpio
81
57
Geomancer
Item
MA Save
Magic Attack UP
Fly

Blood Sword
Round Shield
Golden Hairpin
Black Costume
Reflect Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, Antidote, Holy Water, Phoenix Down

Final Bets: black - 20 bets for 11,287G (47.2%, x1.12); champion - 16 bets for 12,636G (52.8%, x0.89)

black bets:
VolgraTheMoose: 2,001G (17.7%, 45,554G)
reinoe: 2,000G (17.7%, 15,028G)
HASTERIOUS: 1,202G (10.6%, 40,092G)
Zetchryn: 1,000G (8.9%, 5,526G)
Meta_Five: 800G (7.1%, 34,408G)
argentbast: 696G (6.2%, 696G)
nifboy: 500G (4.4%, 5,041G)
Error72: 500G (4.4%, 15,441G)
skillomono: 500G (4.4%, 1,784G)
MagicBottle: 412G (3.7%, 412G)
Grandlanzer: 300G (2.7%, 7,213G)
Spuzzmocker: 260G (2.3%, 260G)
Nizaha: 201G (1.8%, 11,233G)
KasugaiRoastedPeas: 200G (1.8%, 20,103G)
GatsbysGhost: 200G (1.8%, 1,685G)
kilrazin: 154G (1.4%, 154G)
killth3kid: 133G (1.2%, 1,332G)
Evewho: 100G (0.9%, 7,234G)
Firesheath: 100G (0.9%, 11,660G)
getthemoneyz: 28G (0.2%, 986,754G)

champion bets:
DavenIII: 3,500G (27.7%, 24,988G)
OneHundredFists: 1,842G (14.6%, 3,613G)
toka222: 1,650G (13.1%, 48,548G)
EnemyController: 1,111G (8.8%, 616,075G)
BirbBrainsBot: 1,000G (7.9%, 95,102G)
red__lancer: 1,000G (7.9%, 90,422G)
Kronikle: 500G (4.0%, 16,770G)
HaychDub: 500G (4.0%, 33,204G)
evdoggity: 400G (3.2%, 14,925G)
neerrm: 276G (2.2%, 276G)
sososteve: 250G (2.0%, 2,556G)
roqqqpsi: 179G (1.4%, 1,635G)
ericzubat: 128G (1.0%, 128G)
Tougou: 100G (0.8%, 2,691G)
CosmicTactician: 100G (0.8%, 7,040G)
Nohurty: 100G (0.8%, 3,512G)

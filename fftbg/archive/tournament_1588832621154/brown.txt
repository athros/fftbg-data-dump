Player: !Brown
Team: Brown Team
Palettes: Brown/Green



GnielKnows
Female
Leo
63
77
Mime

Dragon Spirit
Martial Arts
Move-HP Up



Green Beret
Mystic Vest
Sprint Shoes

Mimic




CorpusCav
Female
Virgo
53
61
Thief
Battle Skill
Catch
Defense UP
Move+3

Blood Sword

Leather Hat
Leather Outfit
Salty Rage

Steal Heart, Steal Accessory, Arm Aim
Armor Break, Surging Sword



Rengald
Male
Capricorn
65
48
Chemist
Talk Skill
Catch
Monster Talk
Move+2

Cute Bag

Black Hood
Brigandine
Feather Boots

Potion, Hi-Potion, Hi-Ether, Eye Drop, Holy Water, Phoenix Down
Praise, Insult, Negotiate



LAGBOT30000
Male
Aries
73
47
Archer
Black Magic
Brave Up
Doublehand
Jump+1

Cross Bow

Black Hood
Judo Outfit
Magic Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+20
Fire 3, Ice 3, Ice 4, Empower

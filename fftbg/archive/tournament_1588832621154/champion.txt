Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Powergems
Male
Libra
80
79
Calculator
White Magic
Blade Grasp
Maintenance
Jump+3

Madlemgen

Feather Hat
Judo Outfit
N-Kai Armlet

CT, Height, 5, 4
Cure, Cure 2, Cure 3, Raise, Raise 2, Wall, Holy



Meta Five
Male
Aquarius
68
77
Monk
Time Magic
Counter Flood
Maintenance
Jump+3



Twist Headband
Wizard Outfit
Genji Gauntlet

Earth Slash, Revive
Haste, Slow, Quick, Stabilize Time



WireLord
Female
Leo
75
53
Geomancer
Basic Skill
Counter Flood
Attack UP
Waterbreathing

Ancient Sword
Aegis Shield
Green Beret
Mystic Vest
N-Kai Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind
Accumulate, Tickle



GiggleStik
Female
Aries
56
47
Archer
Dance
Mana Shield
Long Status
Jump+3

Stone Gun
Aegis Shield
Platinum Helmet
Black Costume
Red Shoes

Charge+7, Charge+20
Wiznaibus, Disillusion, Nameless Dance, Obsidian Blade

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Joey1995456
Female
Cancer
70
46
Calculator
Black Magic
Distribute
Sicken
Waterwalking

Battle Folio

Twist Headband
Light Robe
Germinas Boots

CT, Height, Prime Number, 5
Bolt, Bolt 3, Ice, Empower, Frog, Death



Eudes89
Male
Sagittarius
50
77
Samurai
Steal
Counter Flood
Maintenance
Move+1

Kikuichimoji

Iron Helmet
Silk Robe
Sprint Shoes

Asura, Bizen Boat, Heaven's Cloud, Muramasa, Masamune
Steal Heart, Steal Helmet, Steal Shield, Steal Accessory



Wonser
Female
Gemini
49
70
Priest
Dance
Damage Split
Equip Axe
Waterwalking

Slasher

Flash Hat
Wizard Robe
Angel Ring

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell 2, Holy
Slow Dance, Polka Polka, Disillusion, Void Storage, Nether Demon, Dragon Pit



Proper Noun
Female
Aquarius
79
71
Ninja
Charge
Absorb Used MP
Equip Gun
Retreat

Romanda Gun
Stone Gun
Headgear
Mystic Vest
Red Shoes

Shuriken, Bomb
Charge+2, Charge+3, Charge+4, Charge+5

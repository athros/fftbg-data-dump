Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nekross92
Female
Libra
42
78
Chemist
Time Magic
Absorb Used MP
Magic Defense UP
Fly

Cute Bag

Flash Hat
Secret Clothes
Defense Armlet

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Haste, Haste 2, Immobilize, Float, Demi



Eudes89
Male
Gemini
41
65
Calculator
Yin Yang Magic
Distribute
Halve MP
Teleport

Bestiary

Golden Hairpin
Mythril Vest
Spike Shoes

CT, Height, 5, 4
Blind, Poison, Spell Absorb, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic



OneHundredFists
Male
Gemini
49
48
Wizard
Item
Absorb Used MP
Equip Shield
Move+2

Flame Rod
Round Shield
Flash Hat
Chain Vest
Feather Mantle

Fire 3, Bolt
Potion, Hi-Potion, X-Potion, Hi-Ether, Soft, Phoenix Down



Zzaanexx
Female
Aquarius
71
68
Knight
Dance
MP Restore
Sicken
Move+1

Broad Sword
Genji Shield
Gold Helmet
Platinum Armor
Small Mantle

Armor Break, Magic Break, Speed Break, Justice Sword, Dark Sword
Slow Dance, Polka Polka, Obsidian Blade, Nether Demon, Dragon Pit

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Vultuous
Female
Virgo
47
80
Chemist
Punch Art
Meatbone Slash
Maintenance
Waterbreathing

Blaze Gun

Black Hood
Wizard Outfit
Magic Gauntlet

Eye Drop, Soft, Phoenix Down
Wave Fist, Secret Fist, Purification, Chakra, Seal Evil



Lythe Caraker
Male
Virgo
60
70
Ninja
Draw Out
Absorb Used MP
Short Charge
Ignore Height

Short Edge
Koga Knife
Leather Hat
Judo Outfit
Red Shoes

Bomb
Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji



Nifboy
Female
Pisces
69
73
Dancer
Throw
Abandon
Maintenance
Jump+3

Cashmere

Holy Miter
Silk Robe
Battle Boots

Disillusion, Obsidian Blade, Void Storage
Shuriken, Stick



TasisSai
Female
Virgo
67
44
Squire
Steal
Abandon
Equip Axe
Ignore Terrain

White Staff
Ice Shield
Genji Helmet
Wizard Outfit
Cursed Ring

Accumulate, Throw Stone, Wish, Scream
Steal Weapon, Steal Status

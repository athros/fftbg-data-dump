Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Ratchety
Monster
Pisces
80
40
Red Chocobo










Fenaen
Male
Libra
53
45
Monk
Steal
HP Restore
Defense UP
Move+2



Headgear
Brigandine
Battle Boots

Purification, Chakra, Revive
Gil Taking, Steal Heart, Steal Weapon



Ominnous
Female
Capricorn
71
77
Samurai
Summon Magic
Dragon Spirit
Equip Polearm
Move+3

Gokuu Rod

Circlet
Silk Robe
Reflect Ring

Koutetsu, Murasame, Kiyomori
Moogle, Ramuh, Golem, Silf



Firesheath
Female
Cancer
47
77
Oracle
Elemental
Parry
Equip Shield
Move+3

Battle Bamboo
Platinum Shield
Black Hood
Chain Vest
Angel Ring

Blind, Silence Song, Paralyze, Sleep, Petrify
Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

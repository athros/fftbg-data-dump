Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rubenflonne
Monster
Aquarius
46
67
Red Chocobo










ZephyrTempest
Female
Libra
52
67
Ninja
Elemental
Absorb Used MP
Equip Bow
Teleport

Windslash Bow

Golden Hairpin
Adaman Vest
Feather Boots

Shuriken, Bomb
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Sypheck
Female
Virgo
42
44
Oracle
Punch Art
Catch
Dual Wield
Move-MP Up

Bestiary
Papyrus Codex
Black Hood
Leather Outfit
Small Mantle

Zombie, Silence Song, Dispel Magic, Paralyze
Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil



Zbgs
Female
Sagittarius
71
59
Dancer
Throw
Meatbone Slash
Equip Sword
Move-MP Up

Defender

Feather Hat
Black Costume
N-Kai Armlet

Disillusion, Void Storage, Nether Demon
Shuriken

Player: !White
Team: White Team
Palettes: White/Blue



Lydian C
Female
Cancer
43
62
Time Mage
Throw
PA Save
Short Charge
Levitate

White Staff

Black Hood
Earth Clothes
Genji Gauntlet

Haste, Slow, Slow 2, Quick, Stabilize Time
Shuriken, Bomb



Ar Tactic
Female
Cancer
78
46
Summoner
Draw Out
Distribute
Defense UP
Jump+3

Thunder Rod

Flash Hat
Light Robe
N-Kai Armlet

Moogle, Shiva, Ramuh, Ifrit, Titan, Bahamut, Leviathan, Fairy, Cyclops
Muramasa, Chirijiraden



Sukotsuto
Male
Taurus
52
64
Calculator
Time Magic
PA Save
Equip Knife
Jump+1

Kunai

Twist Headband
Silk Robe
Small Mantle

CT, Height, 4, 3
Haste, Haste 2, Slow, Float, Demi, Demi 2, Meteor



Gertruda
Female
Leo
72
73
Dancer
Charge
Critical Quick
Equip Polearm
Jump+3

Dragon Whisker

Twist Headband
Black Costume
Defense Ring

Slow Dance, Last Dance, Obsidian Blade, Dragon Pit
Charge+1, Charge+3, Charge+4, Charge+5

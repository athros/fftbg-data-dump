Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Reddwind
Male
Cancer
47
53
Knight
Basic Skill
Faith Save
Equip Axe
Move+3

Giant Axe
Buckler
Cross Helmet
Plate Mail
108 Gems

Head Break, Shield Break, Speed Break, Power Break, Night Sword
Heal, Fury



Prince Rogers Nelson
Male
Capricorn
50
78
Geomancer
Yin Yang Magic
Damage Split
Sicken
Ignore Height

Battle Axe
Ice Shield
Barette
Chameleon Robe
Magic Gauntlet

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Blind, Poison, Spell Absorb, Pray Faith, Blind Rage, Dispel Magic, Petrify



Mysteriousdewd
Monster
Taurus
44
79
Explosive










UmaiJam
Male
Virgo
43
49
Archer
Yin Yang Magic
Regenerator
Doublehand
Fly

Night Killer

Green Beret
Brigandine
Small Mantle

Charge+2, Charge+4, Charge+20
Spell Absorb, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify

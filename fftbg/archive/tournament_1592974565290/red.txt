Player: !Red
Team: Red Team
Palettes: Red/Brown



DuraiPapers
Male
Capricorn
79
72
Squire
Steal
Absorb Used MP
Doublehand
Retreat

Mythril Sword

Leather Hat
Chain Mail
Reflect Ring

Accumulate, Heal, Yell
Steal Heart



Hasterious
Female
Aquarius
44
61
Geomancer
Jump
Regenerator
Doublehand
Move+1

Star Bag

Golden Hairpin
Black Robe
Leather Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Level Jump8, Vertical Jump2



Ring Wyrm
Female
Sagittarius
59
75
Thief
Black Magic
Sunken State
Equip Shield
Ignore Height

Short Edge
Escutcheon
Leather Hat
Black Costume
Small Mantle

Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Fire, Fire 3, Bolt, Bolt 2, Empower



OpHendoslice
Male
Aquarius
68
62
Knight
Elemental
Damage Split
Dual Wield
Waterwalking

Broad Sword
Blood Sword
Leather Helmet
Gold Armor
Rubber Shoes

Head Break, Armor Break, Magic Break, Speed Break, Mind Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind

Player: !Green
Team: Green Team
Palettes: Green/White



Shalloween
Male
Serpentarius
46
67
Monk
Jump
Mana Shield
Attack UP
Waterwalking



Red Hood
Judo Outfit
Reflect Ring

Pummel, Purification
Level Jump4, Vertical Jump5



Nekojin
Male
Virgo
51
78
Knight
Punch Art
Critical Quick
Martial Arts
Retreat

Slasher
Genji Shield
Platinum Helmet
Genji Armor
Angel Ring

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Surging Sword
Spin Fist, Pummel, Revive



Flameatron
Female
Aquarius
44
68
Mediator
Steal
PA Save
Equip Axe
Jump+3

Slasher

Feather Hat
Black Robe
Salty Rage

Threaten, Solution, Death Sentence, Insult, Negotiate, Refute
Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status



0v3rr8d
Male
Taurus
76
67
Lancer
Basic Skill
Speed Save
Halve MP
Retreat

Dragon Whisker
Platinum Shield
Diamond Helmet
Linen Robe
108 Gems

Level Jump8, Vertical Jump2
Accumulate, Throw Stone, Heal

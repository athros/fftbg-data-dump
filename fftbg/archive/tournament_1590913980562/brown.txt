Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HASTERIOUS
Female
Taurus
73
68
Wizard
Punch Art
HP Restore
Short Charge
Waterbreathing

Mage Masher

Flash Hat
Black Robe
Cursed Ring

Bolt, Frog
Spin Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil



Solomongrundy85
Male
Cancer
61
48
Geomancer
Yin Yang Magic
Earplug
Magic Attack UP
Move+2

Battle Axe
Ice Shield
Twist Headband
Silk Robe
Genji Gauntlet

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Blind, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy



The Pengwin
Female
Libra
67
52
Mime

PA Save
Monster Talk
Move+3


Crystal Shield
Golden Hairpin
Earth Clothes
Defense Armlet

Mimic




DeathTaxesAndAnime
Female
Taurus
67
58
Geomancer
Summon Magic
Damage Split
Defense UP
Swim

Blood Sword
Genji Shield
Leather Hat
Linen Robe
Diamond Armlet

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Moogle, Leviathan, Silf, Fairy, Lich, Cyclops

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Serperemagus
Female
Leo
44
80
Mime

MA Save
Equip Armor
Move-MP Up



Barbuta
Linen Robe
Angel Ring

Mimic




Maleblackfiora
Male
Cancer
44
71
Priest
Basic Skill
Distribute
Equip Armor
Ignore Height

Healing Staff

Flash Hat
Mythril Armor
Bracer

Cure, Raise, Reraise, Wall, Esuna
Dash, Throw Stone, Heal, Tickle, Wish



Sukotsuto
Male
Leo
59
62
Mediator
Elemental
Hamedo
Beastmaster
Move+2

Mythril Gun

Twist Headband
Brigandine
Wizard Mantle

Persuade, Preach, Solution, Death Sentence, Insult, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



J2DaBibbles
Male
Serpentarius
43
80
Chemist
Throw
Sunken State
Equip Shield
Teleport

Cultist Dagger
Flame Shield
Black Hood
Black Costume
Wizard Mantle

Potion, Hi-Potion, Ether, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Shuriken, Knife, Axe

Player: !Black
Team: Black Team
Palettes: Black/Red



Fenixcrest
Female
Leo
60
77
Samurai
Battle Skill
Catch
Dual Wield
Move-HP Up

Kiyomori
Muramasa
Circlet
Diamond Armor
Genji Gauntlet

Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Shield Break, Weapon Break, Power Break, Mind Break, Stasis Sword, Dark Sword



Holyonline
Female
Aquarius
73
70
Lancer
Dance
Parry
Maintenance
Retreat

Gungnir
Flame Shield
Crystal Helmet
White Robe
Spike Shoes

Level Jump4, Vertical Jump8
Obsidian Blade, Dragon Pit



Zagorsek
Male
Libra
66
53
Summoner
Item
Parry
Equip Sword
Move+3

Kiyomori

Black Hood
White Robe
Red Shoes

Moogle, Ifrit, Fairy, Lich
Potion, Ether, Hi-Ether, Antidote, Holy Water, Phoenix Down



Ominnous
Male
Cancer
42
63
Lancer
White Magic
Critical Quick
Long Status
Teleport

Holy Lance
Ice Shield
Crystal Helmet
Silk Robe
Small Mantle

Level Jump5, Vertical Jump6
Cure 2, Raise, Raise 2, Protect 2, Shell, Shell 2, Wall, Esuna

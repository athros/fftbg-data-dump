Player: !Red
Team: Red Team
Palettes: Red/Brown



Kohlingen
Male
Aries
44
73
Bard
Charge
Meatbone Slash
Equip Gun
Retreat

Romanda Gun

Leather Hat
Reflect Mail
Magic Gauntlet

Angel Song, Nameless Song, Last Song, Space Storage
Charge+10



Heyifoundawhistle
Monster
Capricorn
65
57
Steel Giant










MoxOb
Female
Aries
80
70
Ninja
Black Magic
Brave Up
Sicken
Teleport

Orichalcum
Air Knife
Flash Hat
Brigandine
Genji Gauntlet

Shuriken, Bomb, Knife, Wand
Bolt 2, Ice 2, Ice 4



Lord Burrah
Female
Aries
68
74
Knight
Steal
Dragon Spirit
Defend
Move+3

Slasher
Platinum Shield
Crystal Helmet
Carabini Mail
Reflect Ring

Head Break, Mind Break, Justice Sword
Gil Taking, Steal Heart, Steal Status, Arm Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



Gabbagooluigi
Female
Sagittarius
52
70
Samurai
Basic Skill
Sunken State
Short Charge
Teleport

Murasame

Barbuta
Leather Armor
Leather Mantle

Asura, Bizen Boat, Murasame, Kikuichimoji
Accumulate, Heal, Cheer Up, Wish



Heybeetlejuice
Male
Taurus
63
79
Oracle
Throw
MA Save
Equip Shield
Move+2

Ivory Rod
Crystal Shield
Feather Hat
Leather Outfit
Battle Boots

Poison, Pray Faith, Blind Rage, Dispel Magic, Paralyze, Sleep
Shuriken, Bomb, Ninja Sword



Lowlf
Female
Scorpio
38
52
Wizard
Basic Skill
Caution
Martial Arts
Jump+3



Thief Hat
Linen Robe
Elf Mantle

Bolt, Bolt 3, Ice 2, Frog
Heal, Tickle, Cheer Up, Wish



Lyronmkii
Female
Gemini
50
44
Knight
Item
PA Save
Sicken
Ignore Terrain

Save the Queen

Leather Helmet
Linen Robe
Salty Rage

Shield Break, Stasis Sword, Night Sword, Surging Sword
Hi-Potion, Hi-Ether, Maiden's Kiss, Soft

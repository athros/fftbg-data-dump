Player: !White
Team: White Team
Palettes: White/Blue



WzzrdBlzrd
Female
Capricorn
67
77
Geomancer
Charge
PA Save
Maintenance
Teleport

Giant Axe
Mythril Shield
Feather Hat
White Robe
Feather Mantle

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Charge+3, Charge+5, Charge+7



RoachCarnival
Male
Libra
44
77
Lancer
Throw
Faith Up
Doublehand
Ignore Height

Partisan

Bronze Helmet
Gold Armor
Germinas Boots

Level Jump2, Vertical Jump6
Bomb, Staff, Ninja Sword, Wand



TheManInPlaid
Male
Leo
59
43
Archer
Elemental
PA Save
Attack UP
Jump+1

Poison Bow
Diamond Shield
Grand Helmet
Mystic Vest
108 Gems

Charge+1, Charge+4, Charge+5, Charge+7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind



BigDLicious91
Female
Capricorn
79
47
Chemist
Talk Skill
Speed Save
Equip Gun
Ignore Height

Papyrus Codex

Twist Headband
Brigandine
Dracula Mantle

Hi-Potion, Echo Grass, Soft, Remedy, Phoenix Down
Invitation, Praise

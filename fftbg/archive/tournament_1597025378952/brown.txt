Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kronikle
Male
Leo
54
46
Archer
Steal
Counter Magic
Doublehand
Levitate

Bow Gun

Green Beret
Brigandine
Dracula Mantle

Charge+1, Charge+7
Gil Taking, Steal Heart, Steal Helmet, Steal Weapon



XBizzy
Female
Cancer
50
46
Archer
Basic Skill
MA Save
Martial Arts
Move-MP Up

Poison Bow
Buckler
Diamond Helmet
Adaman Vest
Power Wrist

Charge+2, Charge+4
Throw Stone, Heal, Cheer Up



ALY327
Male
Leo
49
62
Samurai
White Magic
MA Save
Attack UP
Move+3

Heaven's Cloud

Iron Helmet
Carabini Mail
Defense Ring

Heaven's Cloud, Kikuichimoji
Cure, Raise, Raise 2, Esuna, Holy



Nhammen
Female
Aquarius
78
73
Geomancer
Black Magic
PA Save
Short Charge
Move+3

Broad Sword
Platinum Shield
Holy Miter
Wizard Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire, Fire 2, Ice, Ice 2, Ice 4, Death

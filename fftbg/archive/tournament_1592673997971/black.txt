Player: !Black
Team: Black Team
Palettes: Black/Red



Drusic
Male
Capricorn
64
39
Thief
Draw Out
Arrow Guard
Long Status
Move+2

Rune Blade

Green Beret
Earth Clothes
Germinas Boots

Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



GladiatorLupe
Female
Leo
78
47
Calculator
Yin Yang Magic
Parry
Magic Attack UP
Jump+1

Flame Rod

Golden Hairpin
Linen Robe
Angel Ring

CT, Height, Prime Number, 5, 4
Life Drain, Confusion Song, Dispel Magic, Petrify



Lawnboxer
Male
Leo
74
45
Squire
White Magic
Abandon
Defend
Move+1

Star Bag
Diamond Shield
Holy Miter
Diamond Armor
Leather Mantle

Accumulate, Heal, Tickle
Cure, Cure 2, Cure 3, Cure 4, Reraise, Esuna, Holy



Skillomono
Male
Aquarius
57
53
Geomancer
Battle Skill
Sunken State
Halve MP
Move-HP Up

Broad Sword
Mythril Shield
Barette
Rubber Costume
Red Shoes

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Mind Break, Justice Sword

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galvior
Male
Taurus
42
47
Monk
Summon Magic
HP Restore
Equip Shield
Move+2


Bronze Shield
Golden Hairpin
Power Sleeve
108 Gems

Spin Fist, Pummel, Secret Fist, Purification, Revive, Seal Evil
Moogle, Ramuh, Titan, Golem, Carbunkle, Salamander, Silf, Fairy, Cyclops



TheOneRanger
Female
Virgo
53
77
Priest
Draw Out
Brave Up
Short Charge
Jump+1

Gold Staff

Headgear
Black Robe
Small Mantle

Cure, Cure 3, Raise, Reraise, Shell 2, Wall, Esuna
Asura



Keaur
Male
Libra
51
62
Mime

Counter
Magic Attack UP
Move+2



Thief Hat
Mythril Vest
Bracer

Mimic




Slatios
Female
Gemini
48
48
Wizard
Battle Skill
Sunken State
Equip Armor
Move-MP Up

Wizard Rod

Black Hood
Gold Armor
Dracula Mantle

Fire, Fire 4, Ice 4, Flare
Magic Break, Justice Sword, Dark Sword

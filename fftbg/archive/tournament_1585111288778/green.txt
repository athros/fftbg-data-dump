Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Capricorn
72
45
Dancer
Steal
Earplug
Maintenance
Teleport

Panther Bag

Green Beret
Wizard Robe
Wizard Mantle

Disillusion
Steal Heart, Steal Accessory, Arm Aim



Zeroroute
Male
Aquarius
60
75
Monk
Battle Skill
Speed Save
Defense UP
Jump+2



Green Beret
Black Costume
Feather Boots

Spin Fist, Purification, Chakra, Revive
Shield Break, Speed Break, Mind Break, Explosion Sword



Gelwain
Female
Sagittarius
65
72
Summoner
Time Magic
Faith Up
Short Charge
Move+1

Ice Rod

Triangle Hat
Black Robe
Defense Ring

Ramuh, Ifrit, Golem, Leviathan, Lich, Zodiac
Haste 2, Slow, Immobilize, Reflect, Demi 2, Stabilize Time



Valvalis
Male
Sagittarius
44
78
Time Mage
Charge
Speed Save
Magic Attack UP
Move-MP Up

Gold Staff

Barette
Judo Outfit
Leather Mantle

Slow, Slow 2, Immobilize, Quick, Stabilize Time
Charge+7

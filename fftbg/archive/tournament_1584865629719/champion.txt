Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



BenYuPoker
Male
Aquarius
48
62
Mediator
Item
Auto Potion
Dual Wield
Move-HP Up

Madlemgen
Battle Folio
Feather Hat
Silk Robe
Bracer

Invitation, Praise, Threaten, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Potion, Hi-Potion, Antidote, Eye Drop, Phoenix Down



AoiTsukiYuri
Monster
Aries
71
67
Steel Giant










Grininda
Female
Libra
62
62
Summoner
Dance
Brave Up
Equip Gun
Jump+3

Madlemgen

Headgear
Silk Robe
Leather Mantle

Moogle, Titan, Bahamut, Silf, Fairy
Slow Dance, Disillusion, Dragon Pit



Numbersborne
Male
Taurus
58
80
Knight
Sing
HP Restore
Equip Polearm
Teleport

Gokuu Rod
Kaiser Plate
Crystal Helmet
Robe of Lords
Chantage

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword
Angel Song, Diamond Blade

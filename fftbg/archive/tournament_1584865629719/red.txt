Player: !Red
Team: Red Team
Palettes: Red/Brown



I Nod My Head When I Lose
Monster
Capricorn
63
49
Ghoul










PatSouI
Male
Libra
63
72
Ninja
Yin Yang Magic
Absorb Used MP
Magic Attack UP
Waterbreathing

Mage Masher
Short Edge
Headgear
Mystic Vest
Cursed Ring

Shuriken, Bomb, Staff
Blind, Poison, Silence Song



Chuckolator
Female
Gemini
47
60
Oracle
Battle Skill
Brave Up
Secret Hunt
Retreat

Battle Folio

Black Hood
White Robe
Leather Mantle

Poison, Life Drain, Pray Faith, Blind Rage, Dispel Magic
Shield Break, Dark Sword



Chronoxtrigger
Female
Sagittarius
64
79
Wizard
Dance
Dragon Spirit
Equip Polearm
Move-HP Up

Spear

Leather Hat
Black Costume
Sprint Shoes

Fire 2, Fire 4, Bolt 4, Ice, Ice 2, Ice 4, Empower, Frog
Witch Hunt, Wiznaibus, Nameless Dance, Last Dance, Void Storage, Nether Demon

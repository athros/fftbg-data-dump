Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ThanatosXRagnarok
Female
Taurus
76
50
Wizard
Steal
Caution
Magic Defense UP
Move+1

Wizard Rod

Flash Hat
Chain Vest
Power Wrist

Bolt 2, Bolt 3, Ice, Death
Steal Helmet, Steal Weapon, Arm Aim



Astrozin11
Female
Aries
44
73
Wizard
Elemental
Counter
Short Charge
Waterwalking

Ice Rod

Golden Hairpin
Power Sleeve
Spike Shoes

Fire, Fire 2, Bolt 2, Bolt 4, Ice, Empower
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



WireLord
Male
Libra
60
51
Bard
Summon Magic
HP Restore
Doublehand
Lava Walking

Fairy Harp

Holy Miter
Judo Outfit
Magic Gauntlet

Angel Song, Life Song, Cheer Song, Space Storage, Sky Demon
Moogle, Shiva, Ramuh, Titan, Carbunkle, Odin, Salamander, Lich



ApplesauceBoss
Male
Pisces
69
66
Monk
White Magic
HP Restore
Attack UP
Jump+3



Black Hood
Black Costume
Angel Ring

Spin Fist, Secret Fist, Revive
Raise, Protect, Protect 2, Wall, Esuna, Holy

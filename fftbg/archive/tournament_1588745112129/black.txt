Player: !Black
Team: Black Team
Palettes: Black/Red



Gelwain
Female
Gemini
43
81
Chemist
Steal
Meatbone Slash
Attack UP
Jump+1

Hydra Bag

Twist Headband
Brigandine
Spike Shoes

Potion, Eye Drop, Soft, Phoenix Down
Steal Heart, Arm Aim



Galkife
Female
Virgo
40
69
Priest
Throw
Earplug
Equip Bow
Move-HP Up

Poison Bow

Twist Headband
Brigandine
Power Wrist

Cure 2, Cure 4, Raise, Reraise, Regen, Esuna
Shuriken, Bomb, Staff, Wand



Sairentozon7
Female
Sagittarius
57
77
Calculator
Limit
Absorb Used MP
Doublehand
Waterbreathing

Flame Rod

Thief Hat
Wizard Outfit
Angel Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



PatSouI
Male
Scorpio
43
41
Ninja
Time Magic
HP Restore
Equip Bow
Swim

Bow Gun
Night Killer
Red Hood
Earth Clothes
Genji Gauntlet

Shuriken, Bomb
Haste, Haste 2, Slow, Slow 2, Reflect, Demi

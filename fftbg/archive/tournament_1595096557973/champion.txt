Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Run With Stone GUNs
Male
Serpentarius
45
72
Samurai
Battle Skill
Sunken State
Defense UP
Move+3

Asura Knife

Genji Helmet
Diamond Armor
Genji Gauntlet

Asura, Murasame, Heaven's Cloud
Armor Break, Shield Break, Surging Sword



Butterbelljedi
Male
Scorpio
63
75
Ninja
Elemental
PA Save
Magic Defense UP
Waterwalking

Flail
Ninja Edge
Green Beret
Leather Outfit
Dracula Mantle

Shuriken, Bomb, Ninja Sword, Spear
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Xoomwaffle
Female
Scorpio
41
51
Wizard
Summon Magic
Caution
Secret Hunt
Move-MP Up

Poison Rod

Holy Miter
Black Robe
Wizard Mantle

Fire, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Death, Flare
Moogle, Titan, Golem, Carbunkle, Leviathan, Silf



Rockmem21
Male
Aries
60
65
Knight
Talk Skill
Hamedo
Magic Defense UP
Move+3

Chaos Blade
Diamond Shield
Genji Helmet
White Robe
Battle Boots

Armor Break, Shield Break, Night Sword
Praise, Solution

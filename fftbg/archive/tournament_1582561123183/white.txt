Player: !White
Team: White Team
Palettes: White/Blue



Matthewmuscles
Monster
Sagittarius
49
62
Ochu










Tougou
Female
Virgo
39
66
Oracle
Black Magic
Distribute
Short Status
Ignore Terrain

Madlemgen

Thief Hat
Chain Vest
Power Wrist

Pray Faith, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze
Fire 3, Bolt 2, Bolt 3, Ice 4, Frog



Lowlf
Female
Leo
43
51
Calculator
White Magic
Abandon
Secret Hunt
Move+3

Bestiary

Thief Hat
Power Sleeve
Angel Ring

CT, Height, Prime Number, 5, 4
Cure, Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Shell, Wall, Esuna



Momijibot
Male
Aries
79
63
Chemist
Battle Skill
Parry
Defend
Move+3

Blind Knife

Green Beret
Brigandine
Wizard Mantle

Potion, X-Potion, Ether, Antidote, Phoenix Down
Armor Break, Shield Break, Magic Break, Speed Break, Justice Sword

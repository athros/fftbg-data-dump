Player: !Red
Team: Red Team
Palettes: Red/Brown



JumbocactuarX27
Male
Taurus
68
80
Bard
Battle Skill
Speed Save
Short Charge
Move-MP Up

Bloody Strings

Twist Headband
Bronze Armor
Dracula Mantle

Cheer Song, Magic Song, Space Storage, Sky Demon
Shield Break, Magic Break



Ultraballer2000
Female
Leo
66
49
Squire
Elemental
Parry
Equip Bow
Move+1

Panther Bag
Bronze Shield
Circlet
Mystic Vest
Bracer

Dash, Throw Stone, Tickle, Cheer Up, Fury, Scream
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



ThePineappleSalesman
Male
Capricorn
58
68
Knight
Time Magic
Arrow Guard
Equip Gun
Ignore Terrain

Glacier Gun
Diamond Shield
Iron Helmet
Genji Armor
Diamond Armlet

Head Break, Weapon Break, Speed Break, Mind Break, Justice Sword
Haste 2, Slow 2, Demi 2, Stabilize Time



E7bbk
Male
Virgo
70
43
Squire
Punch Art
Counter
Attack UP
Move-MP Up

Poison Bow
Gold Shield
Crystal Helmet
Mythril Vest
Feather Mantle

Heal, Tickle, Fury, Wish
Earth Slash, Purification, Chakra

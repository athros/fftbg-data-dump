Player: !White
Team: White Team
Palettes: White/Blue



KasugaiRoastedPeas
Male
Scorpio
53
68
Time Mage
Battle Skill
Regenerator
Defense UP
Fly

Sage Staff

Twist Headband
Wizard Outfit
Power Wrist

Haste 2, Slow, Slow 2, Stop, Float, Quick
Armor Break, Shield Break, Night Sword



ALY327
Male
Pisces
66
44
Oracle
Battle Skill
Damage Split
Dual Wield
Fly

Gokuu Rod
Battle Bamboo
Thief Hat
Earth Clothes
Bracer

Blind, Silence Song, Dispel Magic, Sleep
Power Break, Mind Break



Genericco
Male
Virgo
43
60
Ninja
Steal
Counter Flood
Long Status
Retreat

Kunai
Short Edge
Cachusha
Mythril Vest
Small Mantle

Shuriken, Sword
Steal Helmet, Steal Weapon



Kyune
Female
Taurus
66
47
Oracle
Item
Dragon Spirit
Long Status
Move+2

Cypress Rod

Thief Hat
Wizard Robe
Wizard Mantle

Blind, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down

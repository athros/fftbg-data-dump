Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SkylerBunny
Female
Capricorn
38
56
Dancer
Summon Magic
Arrow Guard
Maintenance
Retreat

Orichalcum

Leather Hat
Brigandine
Germinas Boots

Witch Hunt, Slow Dance, Disillusion, Obsidian Blade
Shiva, Titan, Odin, Leviathan, Salamander, Silf, Fairy



Lumberinjack
Male
Aries
78
67
Archer
Jump
Parry
Dual Wield
Ignore Terrain

Poison Bow
Poison Bow
Flash Hat
Mythril Vest
N-Kai Armlet

Charge+20
Level Jump3, Vertical Jump3



Evdoggity
Male
Virgo
76
53
Ninja
Summon Magic
Faith Save
Equip Shield
Jump+3

Sasuke Knife
Aegis Shield
Thief Hat
Leather Outfit
Defense Armlet

Knife, Ninja Sword
Moogle, Shiva, Ifrit, Carbunkle, Leviathan



Pbjellytimez
Male
Libra
61
65
Time Mage
Talk Skill
Damage Split
Equip Axe
Move+3

Gold Staff

Triangle Hat
Leather Outfit
N-Kai Armlet

Haste, Haste 2, Stop, Immobilize, Reflect, Quick, Demi 2, Stabilize Time
Praise, Solution, Negotiate, Mimic Daravon

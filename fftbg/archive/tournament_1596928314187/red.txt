Player: !Red
Team: Red Team
Palettes: Red/Brown



Kronikle
Male
Aries
39
60
Lancer
Talk Skill
Dragon Spirit
Doublehand
Jump+2

Octagon Rod

Gold Helmet
Crystal Mail
Power Wrist

Level Jump8, Vertical Jump6
Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Refute, Rehabilitate



ANFz
Male
Capricorn
54
56
Bard
Time Magic
Parry
Doublehand
Move-HP Up

Ramia Harp

Red Hood
Earth Clothes
Spike Shoes

Life Song, Cheer Song, Space Storage, Sky Demon
Slow 2, Stop, Reflect, Quick, Stabilize Time



Letdowncity
Male
Aries
59
65
Geomancer
Basic Skill
Auto Potion
Concentrate
Jump+2

Slasher
Ice Shield
Holy Miter
Light Robe
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Throw Stone, Heal



Lemonjohns
Female
Scorpio
59
71
Priest
Draw Out
Abandon
Dual Wield
Move-MP Up

Wizard Staff
Oak Staff
Twist Headband
Black Robe
Vanish Mantle

Cure 3, Raise, Raise 2, Regen, Protect, Protect 2, Esuna, Holy
Asura, Murasame

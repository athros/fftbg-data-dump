Player: !zChamp
Team: Champion Team
Palettes: Green/White



Kaelsun
Male
Gemini
44
56
Knight
Elemental
MA Save
Magic Defense UP
Jump+1

Defender
Bronze Shield
Circlet
Black Robe
N-Kai Armlet

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword
Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



OneHundredFists
Female
Capricorn
45
69
Priest
Black Magic
Regenerator
Short Charge
Fly

Wizard Staff

Holy Miter
Linen Robe
Magic Ring

Cure, Cure 3, Raise, Raise 2, Protect 2, Shell, Wall, Esuna
Bolt, Ice, Ice 2, Empower, Frog, Flare



B0shii
Male
Serpentarius
66
75
Knight
Punch Art
Caution
Sicken
Move-MP Up

Chaos Blade
Flame Shield
Leather Helmet
Maximillian
Bracer

Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Night Sword, Explosion Sword
Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil



DAC169
Monster
Sagittarius
75
45
Draugr








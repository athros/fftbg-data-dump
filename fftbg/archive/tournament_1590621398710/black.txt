Player: !Black
Team: Black Team
Palettes: Black/Red



Aidanlucky7
Male
Capricorn
42
77
Lancer
Basic Skill
Brave Save
Magic Defense UP
Jump+3

Musk Rod
Diamond Shield
Bronze Helmet
Light Robe
Magic Gauntlet

Level Jump5, Vertical Jump7
Tickle, Yell, Cheer Up, Wish



CapnChaos12
Male
Virgo
46
73
Monk
Talk Skill
Mana Shield
Long Status
Ignore Terrain



Headgear
Adaman Vest
Battle Boots

Wave Fist, Purification, Revive
Praise, Solution, Insult, Rehabilitate



Mokura3rd
Female
Sagittarius
67
48
Knight
White Magic
Counter
Martial Arts
Waterbreathing


Gold Shield
Iron Helmet
Leather Armor
Sprint Shoes

Armor Break, Shield Break, Power Break
Cure, Cure 2, Raise, Regen, Protect 2, Shell, Esuna



Old Overholt
Female
Pisces
47
68
Geomancer
Basic Skill
Counter Flood
Long Status
Move-MP Up

Muramasa
Flame Shield
Red Hood
Brigandine
Genji Gauntlet

Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Accumulate, Heal, Cheer Up

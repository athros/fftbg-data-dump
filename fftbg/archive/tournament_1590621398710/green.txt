Player: !Green
Team: Green Team
Palettes: Green/White



Ferroniss
Female
Serpentarius
64
64
Squire
Item
Regenerator
Throw Item
Swim

Ancient Sword
Gold Shield
Barbuta
Power Sleeve
N-Kai Armlet

Throw Stone, Heal, Tickle, Fury
Potion, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down



Coralreeferz
Female
Taurus
66
46
Thief
Charge
Counter Flood
Dual Wield
Levitate

Hidden Knife
Kunai
Golden Hairpin
Black Costume
Battle Boots

Steal Heart, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Charge+10



Run With Stone GUNs
Male
Pisces
60
80
Knight
Draw Out
MA Save
Long Status
Move+2

Save the Queen
Round Shield
Diamond Helmet
Linen Cuirass
Diamond Armlet

Armor Break, Shield Break, Justice Sword
Asura, Koutetsu, Bizen Boat, Muramasa



Brokenknight201
Female
Scorpio
71
46
Wizard
Throw
MA Save
Dual Wield
Fly

Flame Rod
Faith Rod
Feather Hat
Black Robe
Angel Ring

Fire 2, Bolt 3, Ice 3, Ice 4, Empower, Frog
Shuriken

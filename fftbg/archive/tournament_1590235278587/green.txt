Player: !Green
Team: Green Team
Palettes: Green/White



Otakutaylor
Male
Sagittarius
59
56
Oracle
Elemental
Parry
Equip Shield
Swim

Papyrus Codex
Mythril Shield
Flash Hat
Wizard Robe
Defense Ring

Life Drain, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep
Water Ball, Local Quake, Static Shock, Will-O-Wisp, Lava Ball



Josephiroth 143
Monster
Sagittarius
72
64
Archaic Demon










Lastly
Male
Aquarius
50
64
Samurai
Basic Skill
Regenerator
Short Charge
Waterwalking

Kikuichimoji

Crystal Helmet
Chain Mail
Elf Mantle

Murasame, Kikuichimoji
Throw Stone, Heal, Tickle, Fury, Ultima



ThePineappleSalesman
Male
Virgo
44
81
Monk
Elemental
Regenerator
Sicken
Move+3



Black Hood
Leather Outfit
Rubber Shoes

Wave Fist, Purification, Chakra, Revive
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

Player: !Red
Team: Red Team
Palettes: Red/Brown



KasugaiRoastedPeas
Female
Scorpio
41
63
Thief
Draw Out
Brave Save
Dual Wield
Jump+1

Mage Masher
Dagger
Headgear
Power Sleeve
Small Mantle

Gil Taking, Steal Helmet, Steal Shield
Koutetsu, Kiyomori



Digitalsocrates
Monster
Leo
69
48
Juravis










ShintaroNayaka
Female
Gemini
72
68
Squire
Summon Magic
Hamedo
Equip Armor
Move+1

Platinum Sword
Escutcheon
Bronze Helmet
White Robe
Diamond Armlet

Dash, Throw Stone, Cheer Up, Fury
Moogle, Ramuh, Golem, Salamander, Silf



Gooseyourself
Male
Aries
55
77
Chemist
Sing
Faith Save
Martial Arts
Jump+1



Holy Miter
Chain Vest
Reflect Ring

Potion, X-Potion, Holy Water
Angel Song, Space Storage, Sky Demon

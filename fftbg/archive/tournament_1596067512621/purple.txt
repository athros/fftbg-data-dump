Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LordTomS
Male
Libra
45
51
Mime

Sunken State
Defend
Swim



Green Beret
Adaman Vest
Cursed Ring

Mimic




Galkife
Female
Leo
49
41
Squire
Time Magic
Absorb Used MP
Beastmaster
Swim

Battle Axe
Genji Shield
Flash Hat
Crystal Mail
N-Kai Armlet

Dash, Fury
Haste, Stop, Immobilize, Reflect



Lokenwow
Male
Cancer
43
59
Mediator
Item
MA Save
Throw Item
Waterwalking

Papyrus Codex

Feather Hat
Black Costume
Reflect Ring

Threaten, Death Sentence, Insult, Mimic Daravon
Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Holy Water, Remedy, Phoenix Down



Endiverge
Female
Gemini
62
60
Geomancer
Talk Skill
Counter Tackle
Equip Armor
Move+1

Panther Bag
Flame Shield
Genji Helmet
Diamond Armor
Dracula Mantle

Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Persuade, Solution

Player: !Black
Team: Black Team
Palettes: Black/Red



JDogg2K4
Male
Gemini
58
67
Samurai
Charge
Counter
Magic Attack UP
Move+3

Heaven's Cloud

Mythril Helmet
Gold Armor
Leather Mantle

Bizen Boat, Kiyomori, Muramasa
Charge+1, Charge+2, Charge+5, Charge+20



Rilgon
Female
Sagittarius
49
43
Knight
Basic Skill
Meatbone Slash
Equip Polearm
Teleport 2

Spear
Venetian Shield
Mythril Helmet
Crystal Mail
N-Kai Armlet

Armor Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword
Accumulate, Dash, Heal, Fury



E Ballard
Male
Cancer
69
80
Geomancer
Throw
Faith Save
Defense UP
Ignore Terrain

Murasame
Crystal Shield
Triangle Hat
Leather Outfit
Magic Ring

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind
Shuriken, Staff



J2DaBibbles
Female
Aquarius
59
41
Time Mage
Charge
Caution
Short Charge
Teleport

Octagon Rod

Red Hood
Chameleon Robe
Reflect Ring

Slow 2, Stop, Demi 2, Meteor
Charge+1

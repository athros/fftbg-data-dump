Player: !White
Team: White Team
Palettes: White/Blue



DLJuggernaut
Female
Scorpio
77
48
Time Mage
Basic Skill
Speed Save
Defend
Move+3

Mace of Zeus

Cachusha
Power Sleeve
Diamond Armlet

Haste, Haste 2, Slow, Slow 2, Stop, Reflect
Accumulate, Heal, Yell, Wish



Nickyfive
Female
Serpentarius
80
64
Squire
Yin Yang Magic
MA Save
Halve MP
Waterbreathing

Ancient Sword
Round Shield
Mythril Helmet
Wizard Outfit
Salty Rage

Accumulate, Dash, Throw Stone, Heal, Yell, Fury, Wish
Blind Rage, Confusion Song



Daveb
Male
Gemini
57
55
Ninja
Yin Yang Magic
Speed Save
Equip Gun
Move-HP Up

Ramia Harp
Papyrus Codex
Leather Hat
Adaman Vest
Red Shoes

Shuriken, Bomb, Knife, Staff, Ninja Sword
Blind, Life Drain, Dispel Magic, Paralyze



Reverie3
Female
Scorpio
75
44
Ninja
Jump
Catch
Concentrate
Ignore Height

Sasuke Knife
Hydra Bag
Feather Hat
Secret Clothes
Rubber Shoes

Bomb, Spear
Level Jump5, Vertical Jump5

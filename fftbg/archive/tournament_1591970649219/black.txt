Player: !Black
Team: Black Team
Palettes: Black/Red



Laserman1000
Male
Cancer
56
63
Calculator
Yin Yang Magic
Mana Shield
Magic Defense UP
Fly

Bestiary

Green Beret
Mythril Vest
Defense Armlet

CT, Height, 4
Pray Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze



ExecutedGiraffe
Monster
Aquarius
46
45
Serpentarius










Mesmaster
Female
Taurus
45
62
Priest
Punch Art
Damage Split
Equip Sword
Move+2

Heaven's Cloud

Feather Hat
Clothes
Elf Mantle

Cure, Cure 2, Raise 2, Regen, Protect
Wave Fist, Purification, Chakra, Revive



Oobs56
Female
Aquarius
40
59
Thief
Talk Skill
Dragon Spirit
Equip Sword
Waterwalking

Rune Blade

Leather Hat
Adaman Vest
N-Kai Armlet

Steal Weapon, Steal Status, Arm Aim
Invitation, Threaten, Preach, Insult, Refute

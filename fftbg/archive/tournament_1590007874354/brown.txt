Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Alanaire
Monster
Aquarius
48
73
Behemoth










DudeMonkey77
Male
Libra
35
68
Mediator
Jump
Auto Potion
Equip Gun
Waterbreathing

Romanda Gun

Holy Miter
Linen Robe
Sprint Shoes

Preach, Solution, Refute
Level Jump5, Vertical Jump8



HaateXIII
Female
Scorpio
73
60
Ninja
Punch Art
Counter Flood
Equip Shield
Levitate

Hidden Knife
Diamond Shield
Barette
Clothes
Magic Gauntlet

Knife
Pummel, Earth Slash, Purification, Revive



Ar Tactic
Male
Aries
56
55
Bard
Charge
Counter
Short Status
Jump+2

Windslash Bow

Feather Hat
Brigandine
Cursed Ring

Nameless Song, Space Storage, Sky Demon
Charge+2, Charge+4, Charge+7, Charge+10

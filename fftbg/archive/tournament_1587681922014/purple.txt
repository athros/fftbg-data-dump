Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gelwain
Male
Cancer
54
56
Wizard
Draw Out
HP Restore
Equip Polearm
Waterbreathing

Obelisk

Black Hood
Power Sleeve
Defense Armlet

Fire, Fire 2, Fire 3, Ice 2, Ice 3, Frog
Asura, Muramasa



Tougou
Male
Taurus
41
57
Lancer
Steal
Earplug
Sicken
Ignore Height

Holy Lance
Mythril Shield
Cross Helmet
Crystal Mail
Cursed Ring

Level Jump8, Vertical Jump7
Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



OmnibotGamma
Monster
Capricorn
77
45
Ochu










Mastershango
Male
Scorpio
72
45
Ninja
Sing
Earplug
Equip Bow
Jump+1

Night Killer
Bow Gun
Triangle Hat
Secret Clothes
Magic Gauntlet

Shuriken, Bomb
Life Song, Cheer Song, Battle Song, Magic Song, Last Song, Hydra Pit

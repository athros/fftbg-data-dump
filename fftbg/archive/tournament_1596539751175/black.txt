Player: !Black
Team: Black Team
Palettes: Black/Red



ColetteMSLP
Monster
Virgo
48
73
Dragon










Randgridr
Female
Libra
72
54
Dancer
White Magic
Meatbone Slash
Magic Attack UP
Move+1

Ryozan Silk

Holy Miter
Silk Robe
Angel Ring

Witch Hunt, Polka Polka, Dragon Pit
Cure 3, Raise 2, Regen, Esuna, Holy, Magic Barrier



Roqqqpsi
Female
Sagittarius
76
46
Monk
Charge
Faith Save
Attack UP
Ignore Terrain



Thief Hat
Chain Vest
Magic Ring

Wave Fist, Purification, Revive
Charge+1, Charge+2, Charge+3, Charge+7, Charge+10



Error72
Male
Virgo
76
40
Knight
Draw Out
Abandon
Halve MP
Lava Walking

Defender
Flame Shield
Leather Helmet
Plate Mail
Battle Boots

Head Break, Shield Break, Speed Break, Mind Break, Night Sword, Surging Sword
Asura, Muramasa

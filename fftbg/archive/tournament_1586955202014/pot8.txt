Final Bets: yellow - 11 bets for 8,315G (45.7%, x1.19); champion - 17 bets for 9,890G (54.3%, x0.84)

yellow bets:
Lanshaft: 3,107G (37.4%, 6,214G)
CCRUNNER149UWP: 1,312G (15.8%, 1,312G)
ShintaroNayaka: 1,000G (12.0%, 17,694G)
DavenIII: 1,000G (12.0%, 16,481G)
Laserman1000: 865G (10.4%, 4,765G)
LDSkinny: 356G (4.3%, 356G)
Cataphract116: 200G (2.4%, 5,518G)
Kmatt128: 175G (2.1%, 175G)
AllInBot: 100G (1.2%, 100G)
JumbocactuarX27: 100G (1.2%, 3,535G)
datadrivenbot: 100G (1.2%, 100G)

champion bets:
Draconis345: 1,856G (18.8%, 1,856G)
HaplessOne: 1,500G (15.2%, 62,326G)
TheChainNerd: 1,000G (10.1%, 20,106G)
ungabunga_bot: 1,000G (10.1%, 143,537G)
BirbBrainsBot: 1,000G (10.1%, 175,051G)
superdevon1: 600G (6.1%, 600G)
mexskacin: 520G (5.3%, 520G)
friendsquirrel: 425G (4.3%, 7,849G)
waterwatereverywhere: 352G (3.6%, 352G)
powergems: 300G (3.0%, 8,802G)
Elson_Vi: 300G (3.0%, 1,300G)
ZephyrTempest: 251G (2.5%, 66,593G)
GingerDynomite: 250G (2.5%, 5,661G)
catfashions: 200G (2.0%, 3,007G)
HeroponThrawn: 132G (1.3%, 132G)
NewbGaming: 104G (1.1%, 104G)
RunicMagus: 100G (1.0%, 2,303G)

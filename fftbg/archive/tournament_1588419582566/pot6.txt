Final Bets: black - 7 bets for 4,446G (57.5%, x0.74); purple - 9 bets for 3,281G (42.5%, x1.36)

black bets:
ungabunga_bot: 1,000G (22.5%, 313,719G)
BirbBrainsBot: 1,000G (22.5%, 173,243G)
getthemoneyz: 1,000G (22.5%, 615,511G)
volgrathemoose: 756G (17.0%, 1,512G)
ko2q: 340G (7.6%, 340G)
twelfthrootoftwo: 300G (6.7%, 3,967G)
letdowncity: 50G (1.1%, 790G)

purple bets:
HaplessOne: 999G (30.4%, 20,598G)
TheGuesty: 500G (15.2%, 30,541G)
ewan_e: 500G (15.2%, 9,059G)
Rislyeu: 400G (12.2%, 2,257G)
Flox_silverbow: 332G (10.1%, 332G)
ccordc: 300G (9.1%, 3,550G)
Arkreaver: 100G (3.0%, 10,168G)
datadrivenbot: 100G (3.0%, 11,468G)
emperor_knight: 50G (1.5%, 590G)

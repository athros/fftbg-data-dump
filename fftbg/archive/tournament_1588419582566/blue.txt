Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Goust18
Female
Libra
71
64
Archer
Draw Out
MP Restore
Doublehand
Ignore Height

Cross Bow

Twist Headband
Secret Clothes
Reflect Ring

Charge+4, Charge+7
Bizen Boat



Zachara
Male
Libra
41
79
Samurai
Punch Art
Distribute
Equip Polearm
Levitate

Ivory Rod

Leather Helmet
Crystal Mail
Power Wrist

Koutetsu, Kikuichimoji
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Leakimiko
Male
Libra
47
48
Bard
Black Magic
MA Save
Doublehand
Retreat

Ultimus Bow

Triangle Hat
Mythril Vest
Jade Armlet

Life Song, Battle Song, Sky Demon
Fire 2, Bolt, Bolt 3, Ice, Ice 2, Ice 3, Ice 4



Twelfthrootoftwo
Female
Libra
79
67
Oracle
Talk Skill
Parry
Equip Axe
Jump+2

Morning Star

Leather Hat
Chameleon Robe
Red Shoes

Poison, Spell Absorb, Pray Faith, Zombie, Sleep, Petrify
Persuade, Praise, Solution, Insult, Refute

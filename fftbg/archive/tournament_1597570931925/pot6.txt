Final Bets: white - 9 bets for 10,301G (60.2%, x0.66); purple - 10 bets for 6,805G (39.8%, x1.51)

white bets:
WhattayaBrian: 2,754G (26.7%, 2,754G)
Tarheels218: 2,751G (26.7%, 2,751G)
Mesmaster: 1,696G (16.5%, 1,696G)
amiture: 1,000G (9.7%, 12,084G)
ZephyrBurst83: 1,000G (9.7%, 8,473G)
Lydian_C: 420G (4.1%, 1,332G)
WhiteTigress: 360G (3.5%, 360G)
datadrivenbot: 200G (1.9%, 65,573G)
Mister_Quof: 120G (1.2%, 120G)

purple bets:
E_Ballard: 2,031G (29.8%, 2,031G)
BirbBrainsBot: 1,000G (14.7%, 172,737G)
Creggers: 1,000G (14.7%, 23,430G)
Lanshaft: 596G (8.8%, 6,361G)
getthemoneyz: 544G (8.0%, 1,656,996G)
Lythe_Caraker: 500G (7.3%, 156,922G)
TheMurkGnome: 400G (5.9%, 4,868G)
AllInBot: 330G (4.8%, 330G)
lowlf: 304G (4.5%, 2,319G)
ko2q: 100G (1.5%, 2,202G)

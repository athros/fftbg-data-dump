Player: !Black
Team: Black Team
Palettes: Black/Red



HASTERIOUS
Female
Libra
72
70
Monk
Draw Out
Earplug
Equip Sword
Teleport

Diamond Sword

Green Beret
Wizard Outfit
Rubber Shoes

Earth Slash, Revive
Asura, Koutetsu, Murasame, Kiyomori



TheMurkGnome
Female
Aries
52
61
Oracle
Steal
Counter Magic
Beastmaster
Jump+2

Battle Bamboo

Feather Hat
Wizard Robe
Elf Mantle

Life Drain, Zombie, Foxbird, Paralyze, Dark Holy
Steal Armor, Steal Weapon



McGooferson
Female
Pisces
48
42
Wizard
White Magic
Counter
Dual Wield
Lava Walking

Thunder Rod
Dragon Rod
Red Hood
Wizard Robe
Leather Mantle

Fire, Fire 2, Fire 3, Bolt, Bolt 4, Frog
Cure, Cure 4, Raise, Regen



3ngag3
Female
Leo
43
35
Geomancer
Item
Critical Quick
Equip Bow
Jump+1

Hunting Bow
Platinum Shield
Barette
White Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Potion, Hi-Ether, Soft, Remedy, Phoenix Down

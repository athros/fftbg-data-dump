Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grininda
Male
Libra
65
75
Archer
Steal
Faith Up
Doublehand
Move-HP Up

Silver Bow

Feather Hat
Secret Clothes
Magic Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Gil Taking



PatSouI
Male
Sagittarius
76
75
Summoner
Steal
Critical Quick
Concentrate
Jump+1

Flame Rod

Green Beret
Earth Clothes
Reflect Ring

Shiva, Golem, Odin, Salamander, Lich, Cyclops
Steal Heart, Steal Helmet, Steal Shield, Leg Aim



Leakimiko
Male
Leo
56
65
Monk
Throw
MP Restore
Dual Wield
Swim



Red Hood
Judo Outfit
Battle Boots

Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Shuriken, Sword



Lydian C
Female
Sagittarius
67
49
Mime

Mana Shield
Equip Armor
Move+1



Circlet
Gold Armor
Dracula Mantle

Mimic


Final Bets: white - 6 bets for 6,529G (44.5%, x1.25); purple - 7 bets for 8,140G (55.5%, x0.80)

white bets:
Evewho: 2,000G (30.6%, 8,284G)
roqqqpsi: 1,970G (30.2%, 17,911G)
prince_rogers_nelson_: 1,443G (22.1%, 1,443G)
VolgraTheMoose: 516G (7.9%, 516G)
Digitalsocrates: 500G (7.7%, 1,755G)
datadrivenbot: 100G (1.5%, 46,319G)

purple bets:
Dexsana: 5,867G (72.1%, 5,867G)
BirbBrainsBot: 1,000G (12.3%, 88,712G)
E_Ballard: 692G (8.5%, 692G)
gorgewall: 201G (2.5%, 27,742G)
getthemoneyz: 148G (1.8%, 1,098,548G)
Klednar21: 132G (1.6%, 132G)
Neo_Exodus: 100G (1.2%, 4,307G)

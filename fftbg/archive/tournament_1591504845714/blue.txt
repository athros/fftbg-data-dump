Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Evewho
Female
Gemini
67
43
Knight
Throw
Abandon
Equip Armor
Ignore Height

Defender
Platinum Shield
Green Beret
Mythril Armor
Feather Boots

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword
Shuriken



Nohurty
Male
Scorpio
59
64
Geomancer
Yin Yang Magic
Absorb Used MP
Defense UP
Move+2

Slasher
Escutcheon
Golden Hairpin
Clothes
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Poison, Spell Absorb, Doubt Faith, Blind Rage, Confusion Song, Sleep



Grandlanzer
Female
Pisces
74
63
Squire
White Magic
Mana Shield
Long Status
Levitate

Flame Whip
Mythril Shield
Twist Headband
Black Costume
Feather Boots

Accumulate, Throw Stone, Heal, Yell, Cheer Up, Wish, Scream
Cure, Cure 2, Raise, Reraise, Esuna



LAGBOT30000
Male
Cancer
45
54
Oracle
Black Magic
Mana Shield
Equip Gun
Levitate

Bestiary

Barette
Black Costume
Feather Boots

Spell Absorb, Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Sleep, Dark Holy
Fire, Ice, Death

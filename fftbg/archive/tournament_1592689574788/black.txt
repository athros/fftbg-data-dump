Player: !Black
Team: Black Team
Palettes: Black/Red



Quadh0nk
Male
Sagittarius
68
54
Ninja
Summon Magic
Abandon
Equip Gun
Ignore Terrain

Papyrus Codex
Battle Folio
Golden Hairpin
Leather Outfit
Feather Mantle

Shuriken, Bomb
Moogle, Shiva, Ifrit, Golem, Carbunkle, Odin



Lowlf
Male
Libra
67
54
Time Mage
Draw Out
Mana Shield
Magic Attack UP
Move-MP Up

Battle Bamboo

Black Hood
Wizard Robe
Sprint Shoes

Haste, Slow, Immobilize, Quick
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



ApplesauceBoss
Male
Aquarius
44
72
Monk
Battle Skill
Counter Flood
Attack UP
Fly



Twist Headband
Brigandine
Angel Ring

Spin Fist, Wave Fist, Secret Fist
Armor Break, Magic Break, Mind Break, Dark Sword



Holyonline
Female
Capricorn
62
61
Time Mage
Talk Skill
Critical Quick
Defend
Lava Walking

Wizard Staff

Flash Hat
Wizard Robe
Spike Shoes

Haste 2, Slow 2, Stop, Immobilize, Stabilize Time
Persuade, Threaten, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate

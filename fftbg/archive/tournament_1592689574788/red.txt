Player: !Red
Team: Red Team
Palettes: Red/Brown



NovaKnight21
Male
Cancer
69
64
Time Mage
Sing
HP Restore
Equip Knife
Ignore Terrain

Main Gauche

Headgear
Adaman Vest
N-Kai Armlet

Haste, Slow 2, Immobilize, Float, Reflect, Quick, Stabilize Time, Meteor
Angel Song, Cheer Song, Magic Song



Oreo Pizza
Male
Leo
70
72
Archer
Battle Skill
Damage Split
Beastmaster
Teleport 2

Bow Gun
Genji Shield
Circlet
Brigandine
Wizard Mantle

Charge+7
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break



Neocarbuncle
Male
Gemini
59
76
Knight
Punch Art
Counter Tackle
Magic Defense UP
Teleport

Defender
Ice Shield
Diamond Helmet
Chain Mail
Sprint Shoes

Shield Break, Mind Break, Justice Sword
Wave Fist, Purification, Chakra, Revive



IphoneDarkness
Female
Capricorn
52
65
Geomancer
Draw Out
Abandon
Equip Armor
Waterwalking

Giant Axe
Crystal Shield
Leather Helmet
Chain Mail
Bracer

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Bizen Boat

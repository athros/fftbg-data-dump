Player: !Black
Team: Black Team
Palettes: Black/Red



Ar Tactic
Female
Pisces
78
71
Summoner
Throw
Counter Tackle
Doublehand
Waterwalking

Thunder Rod

Thief Hat
Adaman Vest
Defense Ring

Moogle, Shiva, Titan, Carbunkle, Fairy, Lich, Cyclops
Knife, Sword



Draconis345
Female
Taurus
65
62
Wizard
Steal
Critical Quick
Equip Shield
Retreat

Blind Knife
Mythril Shield
Holy Miter
Clothes
Spike Shoes

Fire 3, Bolt 2, Bolt 3, Ice 3
Steal Armor, Steal Weapon



Lowlf
Female
Aries
77
74
Ninja
White Magic
Speed Save
Martial Arts
Jump+3



Twist Headband
Wizard Outfit
N-Kai Armlet

Shuriken, Knife, Sword, Wand
Cure, Raise, Protect 2, Shell, Esuna, Holy



Smokegiant
Female
Sagittarius
48
56
Oracle
Talk Skill
Caution
Equip Shield
Levitate

Battle Folio
Buckler
Holy Miter
White Robe
Vanish Mantle

Poison, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Sleep
Invitation, Persuade, Preach, Mimic Daravon, Refute

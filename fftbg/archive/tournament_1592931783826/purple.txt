Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SkylerBunny
Female
Aquarius
81
51
Squire
Throw
Counter Magic
Defense UP
Lava Walking

Iron Sword
Genji Shield
Crystal Helmet
Chain Mail
N-Kai Armlet

Accumulate, Throw Stone, Heal, Yell, Fury
Shuriken, Knife



Chuckolator
Female
Aquarius
55
61
Chemist
Time Magic
Counter Tackle
Equip Armor
Move+1

Star Bag

Barbuta
Crystal Mail
Defense Ring

X-Potion, Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Reflect, Demi 2, Galaxy Stop



Choco Joe
Male
Aries
73
50
Summoner
Yin Yang Magic
MP Restore
Martial Arts
Fly



Triangle Hat
Chain Vest
Magic Ring

Moogle, Bahamut, Fairy
Blind, Pray Faith, Blind Rage, Confusion Song, Petrify, Dark Holy



UmaiJam
Female
Libra
73
78
Wizard
Dance
Absorb Used MP
Short Status
Move-HP Up

Thunder Rod

Feather Hat
Chameleon Robe
Jade Armlet

Fire, Fire 3, Fire 4, Bolt 4, Ice 2, Ice 3, Ice 4, Death, Flare
Void Storage

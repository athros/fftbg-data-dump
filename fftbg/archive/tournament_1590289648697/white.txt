Player: !White
Team: White Team
Palettes: White/Blue



Lord Burrah
Male
Aries
42
59
Monk
Black Magic
Catch
Equip Armor
Jump+1



Mythril Helmet
Bronze Armor
Leather Mantle

Secret Fist, Purification, Chakra, Revive
Fire 2, Bolt 2, Ice, Ice 3, Ice 4, Death



LASERJESUS1337
Monster
Libra
48
57
Pisco Demon










Soren Of Tyto
Male
Virgo
68
42
Geomancer
Item
MA Save
Throw Item
Move-HP Up

Kiyomori
Genji Shield
Black Hood
Chameleon Robe
Jade Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



Error72
Male
Taurus
50
49
Ninja
Steal
Speed Save
Concentrate
Jump+1

Orichalcum
Short Edge
Triangle Hat
Black Costume
Feather Boots

Bomb, Sword
Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Arm Aim, Leg Aim

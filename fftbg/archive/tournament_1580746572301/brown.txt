Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gabbagooluigi
Female
Sagittarius
48
49
Chemist
Battle Skill
Speed Save
Dual Wield
Jump+2

Cute Bag
Cute Bag
Golden Hairpin
Judo Outfit
Feather Boots

Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Dark Sword



TheBlobZ
Male
Leo
70
73
Archer
Battle Skill
Caution
Beastmaster
Lava Walking

Mythril Gun
Diamond Shield
Red Hood
Leather Outfit
Bracer

Charge+2, Charge+4, Charge+5
Head Break, Power Break, Stasis Sword



BurtReynoldz
Male
Leo
71
80
Summoner
Punch Art
Counter Tackle
Attack UP
Waterbreathing

Wizard Rod

Holy Miter
Black Robe
Power Wrist

Moogle, Ifrit, Leviathan, Salamander, Silf
Pummel, Secret Fist, Purification, Revive



Looseone
Female
Virgo
55
60
Wizard
Elemental
Faith Up
Equip Armor
Move-MP Up

Dragon Rod

Genji Helmet
Leather Outfit
Setiemson

Fire 3, Bolt 2, Ice, Ice 3, Empower, Flare
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

Player: !Green
Team: Green Team
Palettes: Green/White



BlueAbs
Male
Leo
69
79
Chemist
Charge
Caution
Martial Arts
Levitate

Blind Knife

Red Hood
Rubber Costume
Red Shoes

Potion, Hi-Potion, Eye Drop, Remedy, Phoenix Down
Charge+3, Charge+4, Charge+5, Charge+7



Xoomwaffle
Male
Libra
62
40
Squire
Elemental
Arrow Guard
Maintenance
Move+3

Platinum Sword
Flame Shield
Diamond Helmet
Platinum Armor
Reflect Ring

Heal, Yell, Cheer Up
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Furrytomahawkk
Male
Aries
77
74
Ninja
Item
Parry
Long Status
Move+3

Mythril Knife
Flail
Holy Miter
Leather Outfit
Defense Ring

Shuriken
Potion, Hi-Potion, Ether, Holy Water, Phoenix Down



LDSkinny
Female
Libra
56
64
Oracle
White Magic
Dragon Spirit
Equip Bow
Jump+3

Hunting Bow

Headgear
Chameleon Robe
Magic Gauntlet

Pray Faith, Zombie, Confusion Song, Paralyze, Petrify, Dark Holy
Cure, Cure 4, Raise, Raise 2, Reraise, Shell 2, Esuna, Holy

Player: !White
Team: White Team
Palettes: White/Blue



Cryptopsy70
Female
Virgo
45
68
Thief
Yin Yang Magic
Auto Potion
Magic Defense UP
Jump+2

Hidden Knife

Triangle Hat
Leather Vest
Defense Armlet

Steal Heart, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Life Drain, Silence Song, Blind Rage, Foxbird, Sleep, Petrify, Dark Holy



SamaelMerihem
Female
Leo
73
54
Summoner
Battle Skill
Mana Shield
Short Charge
Move-MP Up

Wizard Rod

Black Hood
Brigandine
Small Mantle

Bahamut, Odin, Leviathan, Silf, Lich, Cyclops
Power Break



Shalloween
Monster
Virgo
77
47
Red Chocobo










Ikuyoru
Female
Libra
61
56
Chemist
Dance
PA Save
Dual Wield
Lava Walking

Star Bag
Hydra Bag
Green Beret
Leather Vest
Rubber Shoes

Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Disillusion, Nameless Dance, Obsidian Blade

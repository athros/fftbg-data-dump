Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LDSkinny
Female
Gemini
75
77
Oracle
Time Magic
Auto Potion
Magic Attack UP
Jump+1

Papyrus Codex

Black Hood
Black Robe
Spike Shoes

Poison, Blind Rage, Confusion Song, Sleep, Petrify
Haste, Stop, Reflect, Quick, Stabilize Time



PetitFoulard
Female
Aquarius
56
76
Wizard
Jump
Speed Save
Maintenance
Lava Walking

Rod

Headgear
Leather Outfit
Spike Shoes

Fire, Fire 2, Bolt 3, Bolt 4, Ice 2, Empower
Level Jump8, Vertical Jump2



Nedryerson01
Male
Libra
68
55
Archer
Item
MA Save
Equip Axe
Jump+2

Battle Axe
Buckler
Leather Hat
Clothes
Magic Gauntlet

Charge+3, Charge+4, Charge+10
Potion, Antidote, Remedy, Phoenix Down



Baron Von Scrub
Male
Pisces
61
69
Mime

Dragon Spirit
Equip Shield
Fly


Flame Shield
Holy Miter
Adaman Vest
Feather Mantle

Mimic


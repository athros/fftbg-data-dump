Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



D4rr1n
Male
Aquarius
71
54
Chemist
Talk Skill
Distribute
Equip Armor
Move+2

Hydra Bag

Gold Helmet
Platinum Armor
Battle Boots

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Invitation, Praise, Threaten, Solution, Refute



Lawndough
Female
Serpentarius
50
59
Knight
Elemental
Faith Save
Beastmaster
Move+3

Excalibur
Bronze Shield
Diamond Helmet
Reflect Mail
Bracer

Head Break, Shield Break, Weapon Break, Stasis Sword, Dark Sword, Night Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Mardokttv
Monster
Taurus
77
58
Skeleton










Serperemagus
Male
Leo
43
70
Bard
Throw
Parry
Martial Arts
Lava Walking



Flash Hat
Wizard Outfit
108 Gems

Life Song, Cheer Song, Battle Song, Sky Demon
Shuriken, Bomb, Knife, Stick

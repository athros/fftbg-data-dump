Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Shoosler
Female
Cancer
70
61
Oracle
Jump
Counter Flood
Equip Knife
Jump+2

Wizard Rod

Feather Hat
Brigandine
Diamond Armlet

Blind, Spell Absorb, Zombie, Confusion Song, Dispel Magic, Sleep
Level Jump5, Vertical Jump6



HASTERIOUS
Female
Pisces
76
57
Samurai
Jump
Speed Save
Dual Wield
Teleport

Gungnir
Partisan
Diamond Helmet
Carabini Mail
108 Gems

Asura, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Level Jump2, Vertical Jump5



Bryan792
Male
Sagittarius
60
65
Ninja
Elemental
Critical Quick
Equip Gun
Levitate

Battle Folio
Papyrus Codex
Cachusha
Adaman Vest
Germinas Boots

Shuriken, Bomb, Knife
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Lava Ball



Mario
Male
Gemini
64
64
Squire
Yin Yang Magic
Auto Potion
Equip Gun
Levitate

Stone Gun
Escutcheon
Red Hood
Wizard Outfit
Feather Boots

Dash, Throw Stone, Heal, Cheer Up, Wish
Life Drain, Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Paralyze, Dark Holy

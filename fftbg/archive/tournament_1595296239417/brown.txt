Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Choco Joe
Male
Cancer
41
52
Archer
Summon Magic
HP Restore
Doublehand
Move-MP Up

Blaze Gun

Barbuta
Judo Outfit
Reflect Ring

Charge+2, Charge+3, Charge+4, Charge+20
Moogle, Ifrit, Leviathan, Salamander, Cyclops



Just Here2
Male
Cancer
41
56
Knight
Sing
Damage Split
Attack UP
Move+2

Save the Queen
Escutcheon
Mythril Helmet
Maximillian
Elf Mantle

Head Break, Armor Break, Stasis Sword, Justice Sword
Angel Song, Battle Song, Sky Demon, Hydra Pit



Grandlanzer
Male
Scorpio
52
44
Bard
Basic Skill
Counter Flood
Equip Sword
Teleport

Sleep Sword

Barette
Gold Armor
Spike Shoes

Cheer Song, Hydra Pit
Accumulate, Throw Stone, Heal, Yell



Legionx13
Male
Gemini
66
74
Knight
Time Magic
Counter Magic
Dual Wield
Waterbreathing

Iron Sword
Blood Sword
Cross Helmet
Carabini Mail
Small Mantle

Head Break, Weapon Break, Magic Break, Speed Break
Slow, Slow 2, Reflect, Quick, Demi 2, Stabilize Time

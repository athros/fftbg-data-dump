Final Bets: purple - 10 bets for 8,349G (64.5%, x0.55); brown - 8 bets for 4,591G (35.5%, x1.82)

purple bets:
CorpusCav: 4,513G (54.1%, 9,026G)
Draconis345: 1,000G (12.0%, 65,158G)
HASTERIOUS: 692G (8.3%, 13,840G)
Belivalan: 500G (6.0%, 2,641G)
killth3kid: 500G (6.0%, 17,088G)
josephiroth_143: 400G (4.8%, 1,485G)
GnielKnows: 244G (2.9%, 244G)
KasugaiRoastedPeas: 200G (2.4%, 1,087G)
otakutaylor: 200G (2.4%, 4,410G)
datadrivenbot: 100G (1.2%, 21,701G)

brown bets:
volgrathemoose: 2,498G (54.4%, 4,996G)
BirbBrainsBot: 1,000G (21.8%, 146,504G)
getthemoneyz: 416G (9.1%, 708,254G)
rocl: 227G (4.9%, 4,546G)
cloud92684: 200G (4.4%, 1,669G)
AllInBot: 100G (2.2%, 100G)
E_Ballard: 100G (2.2%, 6,976G)
DrAntiSocial: 50G (1.1%, 18,761G)

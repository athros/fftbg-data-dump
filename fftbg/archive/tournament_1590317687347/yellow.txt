Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DrAntiSocial
Male
Virgo
51
79
Knight
Punch Art
Auto Potion
Equip Polearm
Waterbreathing

Holy Lance
Kaiser Plate
Leather Helmet
Wizard Robe
Battle Boots

Magic Break, Speed Break
Pummel, Purification



HASTERIOUS
Male
Capricorn
70
77
Calculator
White Magic
Arrow Guard
Equip Armor
Ignore Terrain

Bestiary

Genji Helmet
Chain Mail
Power Wrist

Height, 3
Cure 3, Raise, Raise 2, Regen, Esuna, Holy



Reverie3
Male
Scorpio
40
41
Geomancer
Time Magic
Mana Shield
Sicken
Jump+2

Sleep Sword
Venetian Shield
Black Hood
White Robe
Magic Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Lava Ball
Haste, Slow, Demi 2, Stabilize Time



Solomongrundy85
Female
Aquarius
63
69
Mime

Auto Potion
Long Status
Fly



Red Hood
Leather Outfit
Genji Gauntlet

Mimic


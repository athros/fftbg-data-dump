Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Luminarii
Female
Sagittarius
49
69
Knight
Elemental
Damage Split
Long Status
Fly

Defender
Bronze Shield
Cross Helmet
Plate Mail
Cursed Ring

Head Break, Armor Break, Speed Break, Mind Break, Justice Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Nekross92
Male
Taurus
72
48
Calculator
Limit
Meatbone Slash
Long Status
Jump+1

Battle Bamboo

Leather Hat
Mythril Armor
N-Kai Armlet

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



HaateXIII
Female
Aries
65
59
Wizard
Draw Out
Damage Split
Doublehand
Teleport

Thunder Rod

Flash Hat
Wizard Robe
Jade Armlet

Fire 3, Bolt 2, Bolt 4, Ice 2, Ice 4
Asura, Koutetsu, Muramasa



ChrisWado
Male
Aquarius
48
79
Archer
Summon Magic
MA Save
Long Status
Ignore Terrain

Hunting Bow
Platinum Shield
Black Hood
Leather Outfit
Sprint Shoes

Charge+1, Charge+5, Charge+10
Ramuh, Silf

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Davarian
Male
Leo
66
70
Samurai
Summon Magic
HP Restore
Doublehand
Waterwalking

Kiyomori

Bronze Helmet
Reflect Mail
Magic Ring

Bizen Boat, Murasame, Kikuichimoji
Ramuh, Ifrit, Carbunkle, Bahamut, Leviathan, Silf, Fairy, Lich



The Pengwin
Female
Aries
70
71
Geomancer
Summon Magic
Arrow Guard
Attack UP
Move+3

Battle Axe
Round Shield
Black Hood
Light Robe
Diamond Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Titan, Golem, Carbunkle, Odin, Salamander, Cyclops



Ko2q
Male
Taurus
69
62
Knight
Basic Skill
Earplug
Long Status
Jump+1

Iron Sword
Ice Shield
Genji Helmet
Diamond Armor
Defense Armlet

Speed Break, Power Break, Mind Break, Dark Sword
Accumulate, Dash, Throw Stone, Heal, Tickle, Fury, Wish



Smashy
Female
Scorpio
63
45
Squire
Yin Yang Magic
Abandon
Defend
Move+1

Morning Star
Hero Shield
Cachusha
Bronze Armor
Small Mantle

Accumulate, Throw Stone, Heal, Fury
Life Drain, Doubt Faith, Silence Song, Confusion Song, Paralyze, Sleep, Petrify

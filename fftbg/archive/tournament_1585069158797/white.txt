Player: !White
Team: White Team
Palettes: White/Blue



RjA0zcOQ96
Male
Taurus
67
73
Bard
Elemental
Earplug
Equip Armor
Move-HP Up

Ramia Harp

Leather Helmet
Diamond Armor
Jade Armlet

Life Song, Cheer Song, Battle Song, Magic Song, Last Song, Diamond Blade, Hydra Pit
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind



Typicalfanboy
Male
Taurus
36
53
Mediator
Steal
Auto Potion
Equip Sword
Waterwalking

Mythril Sword

Twist Headband
Linen Robe
Dracula Mantle

Praise, Threaten, Solution, Death Sentence, Negotiate, Refute, Rehabilitate
Steal Shield, Steal Weapon



L2 Sentinel
Male
Aquarius
68
75
Monk
Time Magic
MP Restore
Dual Wield
Move+2



Golden Hairpin
Rubber Costume
Battle Boots

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Reflect, Stabilize Time, Meteor



LDSkinny
Female
Capricorn
45
46
Monk
White Magic
Sunken State
Dual Wield
Retreat



Triangle Hat
Rubber Costume
Spike Shoes

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Cure 2, Cure 3, Raise, Protect, Shell 2

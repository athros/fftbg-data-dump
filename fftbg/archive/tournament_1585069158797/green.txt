Player: !Green
Team: Green Team
Palettes: Green/White



Ra1kia
Female
Scorpio
68
76
Oracle
Steal
Damage Split
Maintenance
Move+1

Papyrus Codex

Barette
Linen Robe
Feather Boots

Blind, Spell Absorb, Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic
Steal Heart, Steal Armor, Steal Weapon, Leg Aim



Pie108
Male
Pisces
72
75
Thief
Jump
Brave Up
Martial Arts
Waterwalking

Ninja Edge

Cachusha
Judo Outfit
Defense Ring

Gil Taking, Steal Armor, Leg Aim
Level Jump4, Vertical Jump8



Glastonberry48
Male
Serpentarius
63
80
Oracle
Draw Out
Sunken State
Equip Armor
Jump+3

Cypress Rod

Mythril Helmet
Carabini Mail
Cursed Ring

Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Sleep
Koutetsu, Muramasa, Kikuichimoji



Nickyfive
Female
Taurus
49
76
Samurai
Elemental
Counter Magic
Equip Axe
Teleport

Rainbow Staff

Diamond Helmet
Bronze Armor
Leather Mantle

Asura, Heaven's Cloud, Muramasa
Pitfall, Water Ball, Local Quake, Quicksand, Blizzard, Gusty Wind

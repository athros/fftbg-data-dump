Player: !White
Team: White Team
Palettes: White/Blue



Diablos24
Male
Scorpio
42
75
Archer
Elemental
Brave Up
Doublehand
Waterwalking

Ultimus Bow

Green Beret
Judo Outfit
Magic Ring

Charge+3, Charge+5
Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind



GoodGuyRoger
Female
Capricorn
48
58
Chemist
Talk Skill
Caution
Monster Talk
Fly

Hydra Bag

Golden Hairpin
Chain Vest
Jade Armlet

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Threaten, Preach, Death Sentence, Insult, Refute



Kami 39
Male
Gemini
76
48
Squire
Talk Skill
Counter Tackle
Monster Talk
Jump+2

Morning Star

Feather Hat
Leather Armor
Magic Gauntlet

Accumulate, Dash, Heal, Yell, Scream
Praise, Preach, Death Sentence, Negotiate, Refute, Rehabilitate



P0jothen00b
Male
Sagittarius
63
47
Mediator
Yin Yang Magic
Counter Magic
Equip Knife
Waterbreathing

Thunder Rod

Golden Hairpin
Leather Outfit
Jade Armlet

Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Blind, Spell Absorb, Pray Faith, Doubt Faith, Blind Rage, Foxbird

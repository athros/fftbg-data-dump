Player: !Green
Team: Green Team
Palettes: Green/White



MoxOb
Male
Taurus
53
46
Archer
Battle Skill
Catch
Secret Hunt
Move+3

Bow Gun
Diamond Shield
Cross Helmet
Adaman Vest
Bracer

Charge+4, Charge+5
Head Break, Armor Break, Shield Break, Magic Break



Trowaba
Male
Capricorn
45
78
Archer
Punch Art
Counter Flood
Attack UP
Move-MP Up

Romanda Gun
Round Shield
Mythril Helmet
Earth Clothes
Angel Ring

Charge+2, Charge+7
Spin Fist, Purification, Seal Evil



RoachCarnival
Male
Gemini
42
63
Archer
Elemental
Regenerator
Doublehand
Jump+1

Night Killer

Leather Hat
Wizard Outfit
Vanish Mantle

Charge+2, Charge+3, Charge+7, Charge+10, Charge+20
Pitfall, Water Ball, Sand Storm, Blizzard



Dexsana
Male
Sagittarius
74
63
Samurai
Elemental
Caution
Magic Defense UP
Ignore Terrain

Kikuichimoji

Iron Helmet
Leather Armor
Power Wrist

Asura, Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball

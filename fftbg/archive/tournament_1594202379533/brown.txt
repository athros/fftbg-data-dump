Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ShintaroNayaka
Male
Aries
54
60
Bard
Punch Art
Auto Potion
Equip Sword
Waterbreathing

Bizen Boat

Feather Hat
Carabini Mail
Leather Mantle

Last Song, Space Storage
Pummel, Secret Fist



Evewho
Female
Taurus
55
77
Chemist
Punch Art
Auto Potion
Defense UP
Waterwalking

Romanda Gun

Feather Hat
Judo Outfit
Rubber Shoes

Hi-Potion, Ether, Echo Grass, Holy Water, Remedy, Phoenix Down
Pummel, Earth Slash, Purification, Chakra, Seal Evil



Eba
Female
Sagittarius
55
46
Thief
Basic Skill
Counter Magic
Attack UP
Move+1

Dagger

Headgear
Black Costume
Battle Boots

Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Accumulate, Dash, Throw Stone, Yell



Lenora
Female
Capricorn
55
55
Dancer
Summon Magic
Arrow Guard
Martial Arts
Jump+3



Leather Hat
White Robe
Diamond Armlet

Witch Hunt, Wiznaibus, Nether Demon
Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Fairy

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



LordTomS
Female
Aries
64
51
Summoner
Battle Skill
Dragon Spirit
Maintenance
Move+3

Battle Folio

Flash Hat
Black Robe
Dracula Mantle

Moogle, Titan, Golem, Silf
Shield Break, Speed Break, Dark Sword



DavenIII
Male
Virgo
55
80
Monk
Draw Out
Counter
Short Charge
Retreat



Flash Hat
Black Costume
Jade Armlet

Pummel, Wave Fist, Purification, Seal Evil
Kiyomori, Muramasa



MDMAyylmao
Female
Capricorn
62
41
Samurai
White Magic
Regenerator
Doublehand
Waterbreathing

Obelisk

Gold Helmet
Crystal Mail
Small Mantle

Asura, Muramasa
Cure 2, Regen, Protect, Shell 2, Wall, Esuna, Holy



Phi Sig
Female
Aries
52
59
Samurai
Battle Skill
Distribute
Equip Polearm
Move-HP Up

Musk Rod

Leather Helmet
Diamond Armor
108 Gems

Bizen Boat, Heaven's Cloud
Head Break, Armor Break, Weapon Break, Mind Break, Dark Sword

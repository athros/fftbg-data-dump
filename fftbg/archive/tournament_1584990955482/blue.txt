Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rnark
Female
Scorpio
71
55
Oracle
Dance
Damage Split
Secret Hunt
Levitate

Battle Bamboo

Green Beret
Silk Robe
Vanish Mantle

Spell Absorb, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic
Wiznaibus, Slow Dance, Disillusion



Coralreeferz
Female
Virgo
58
70
Geomancer
Charge
HP Restore
Martial Arts
Move+2


Ice Shield
Headgear
Light Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind
Charge+1, Charge+3, Charge+4, Charge+20



Skarthe
Male
Cancer
49
67
Geomancer
Draw Out
Parry
Defense UP
Teleport

Battle Axe
Bronze Shield
Headgear
Silk Robe
Battle Boots

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Murasame



Shakarak
Female
Libra
41
42
Samurai
Basic Skill
Auto Potion
Concentrate
Fly

Koutetsu Knife

Diamond Helmet
Silk Robe
Defense Ring

Koutetsu, Kikuichimoji
Heal, Wish

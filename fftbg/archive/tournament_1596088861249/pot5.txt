Final Bets: red - 6 bets for 3,750G (37.1%, x1.70); yellow - 10 bets for 6,365G (62.9%, x0.59)

red bets:
amiture: 2,000G (53.3%, 35,170G)
BirbBrainsBot: 1,000G (26.7%, 27,094G)
amhamor: 350G (9.3%, 350G)
AllInBot: 200G (5.3%, 200G)
mirapoix: 100G (2.7%, 1,392G)
CT_5_Holy: 100G (2.7%, 5,000G)

yellow bets:
douchetron: 2,385G (37.5%, 4,677G)
SkylerBunny: 2,000G (31.4%, 109,560G)
gorgewall: 788G (12.4%, 788G)
Arcblazer23: 284G (4.5%, 284G)
holdenmagronik: 244G (3.8%, 704G)
datadrivenbot: 200G (3.1%, 46,801G)
twelfthrootoftwo: 200G (3.1%, 2,225G)
maximumcrit: 104G (1.6%, 104G)
BaronHaynes: 100G (1.6%, 7,607G)
getthemoneyz: 60G (0.9%, 1,441,181G)

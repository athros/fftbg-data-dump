Player: !Red
Team: Red Team
Palettes: Red/Brown



Lijarkh
Male
Sagittarius
76
54
Monk
Talk Skill
Speed Save
Dual Wield
Ignore Terrain



Green Beret
Earth Clothes
Small Mantle

Spin Fist, Earth Slash, Secret Fist
Invitation, Persuade, Praise, Threaten, Preach, Solution, Rehabilitate



Estan AD
Male
Taurus
75
54
Thief
Throw
Counter Tackle
Equip Armor
Ignore Terrain

Rune Blade

Leather Helmet
Chameleon Robe
Germinas Boots

Gil Taking, Steal Weapon, Steal Status
Knife



Rislyeu
Male
Scorpio
71
80
Mime

Brave Up
Monster Talk
Move+3



Flash Hat
Black Costume
Battle Boots

Mimic




Gelwain
Female
Pisces
72
67
Archer
Draw Out
Counter Magic
Doublehand
Teleport

Mythril Bow

Triangle Hat
Secret Clothes
Angel Ring

Charge+2, Charge+3, Charge+4
Murasame

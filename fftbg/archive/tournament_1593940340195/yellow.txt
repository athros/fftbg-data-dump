Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Safira125
Female
Taurus
74
73
Knight
Time Magic
Caution
Defend
Swim

Defender
Round Shield
Diamond Helmet
Reflect Mail
Magic Gauntlet

Head Break
Haste 2, Reflect, Demi, Demi 2, Stabilize Time



ZZ Yoshi
Female
Pisces
71
71
Summoner
Talk Skill
Critical Quick
Short Charge
Waterwalking

Papyrus Codex

Golden Hairpin
Chameleon Robe
Battle Boots

Moogle, Ramuh, Golem, Carbunkle, Cyclops
Invitation, Persuade, Praise, Threaten, Death Sentence, Refute, Rehabilitate



SomthingMore
Male
Gemini
62
57
Mediator
White Magic
Counter Tackle
Dual Wield
Jump+2

Madlemgen
Battle Folio
Twist Headband
Linen Robe
Angel Ring

Invitation, Persuade, Solution, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Cure, Raise, Raise 2, Regen



Genericco
Male
Cancer
48
42
Oracle
Item
Mana Shield
Magic Attack UP
Fly

Iron Fan

Feather Hat
Mystic Vest
Setiemson

Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Paralyze
Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down

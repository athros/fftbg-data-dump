Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ikamuii
Male
Scorpio
59
60
Samurai
Punch Art
Counter
Martial Arts
Waterbreathing



Genji Helmet
Leather Armor
Angel Ring

Asura, Koutetsu, Heaven's Cloud
Pummel, Secret Fist, Purification



DudeOMG
Female
Capricorn
68
60
Knight
Black Magic
Parry
Magic Attack UP
Move+3

Defender

Leather Helmet
Gold Armor
Rubber Shoes

Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Justice Sword, Dark Sword
Fire 4, Bolt, Bolt 3, Ice, Ice 2, Empower



Chao Garden
Female
Capricorn
59
49
Oracle
Item
Caution
Throw Item
Move+2

Iron Fan

Red Hood
Leather Vest
Cursed Ring

Blind, Poison, Pray Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep, Petrify, Dark Holy
Potion, Antidote, Echo Grass, Holy Water, Phoenix Down



Flowinprose
Female
Virgo
41
58
Chemist
Dance
Faith Up
Magic Attack UP
Move+3

Cute Bag

Triangle Hat
Earth Clothes
Defense Armlet

Potion, X-Potion, Eye Drop, Soft, Phoenix Down
Obsidian Blade, Dragon Pit

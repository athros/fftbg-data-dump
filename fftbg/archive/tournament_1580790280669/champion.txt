Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Weegz
Female
Aquarius
39
41
Chemist
Elemental
Faith Up
Equip Axe
Move+3

Oak Staff

Green Beret
Earth Clothes
Cursed Ring

Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Shs
Monster
Libra
76
50
Dark Behemoth










Phi Sig
Female
Pisces
71
65
Summoner
Battle Skill
PA Save
Short Charge
Fly

Wizard Rod

Twist Headband
Light Robe
Feather Mantle

Moogle, Ramuh, Carbunkle, Odin
Head Break, Armor Break, Power Break, Mind Break



Concles
Male
Capricorn
60
62
Lancer
Sing
Meatbone Slash
Martial Arts
Move-MP Up


Round Shield
Genji Helmet
Bronze Armor
Angel Ring

Level Jump4, Vertical Jump8
Nameless Song, Sky Demon

Player: !White
Team: White Team
Palettes: White/Blue



Firesheath
Male
Pisces
62
44
Monk
Draw Out
Speed Save
Equip Gun
Fly

Blast Gun

Twist Headband
Judo Outfit
Reflect Ring

Pummel, Wave Fist, Secret Fist, Purification, Seal Evil
Asura



Pplvee1
Female
Capricorn
60
67
Knight
Summon Magic
Counter
Long Status
Levitate

Defender
Ice Shield
Iron Helmet
Gold Armor
Bracer

Shield Break, Power Break, Mind Break, Dark Sword, Night Sword
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Fairy



Galkife
Male
Capricorn
49
47
Squire
White Magic
Critical Quick
Sicken
Fly

Night Killer
Escutcheon
Black Hood
Chain Vest
Defense Armlet

Accumulate, Tickle, Cheer Up, Scream
Cure, Cure 4, Shell 2, Esuna, Holy



Bruubarg
Female
Capricorn
47
51
Archer
Basic Skill
Caution
Short Charge
Waterwalking

Poison Bow
Platinum Shield
Leather Hat
Judo Outfit
Leather Mantle

Charge+2, Charge+3, Charge+5, Charge+7
Throw Stone, Heal, Cheer Up

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nosablake
Male
Aquarius
66
43
Lancer
Time Magic
Brave Up
Doublehand
Move-MP Up

Javelin

Leather Helmet
Chameleon Robe
Jade Armlet

Level Jump8, Vertical Jump3
Slow 2, Immobilize, Demi, Demi 2, Stabilize Time, Galaxy Stop



CaptainAdmiralSPATULA
Male
Pisces
54
65
Lancer
Sing
HP Restore
Doublehand
Waterbreathing

Cypress Rod

Crystal Helmet
Platinum Armor
Red Shoes

Level Jump2, Vertical Jump8
Cheer Song, Nameless Song, Sky Demon



Matthewmuscles
Female
Libra
77
42
Archer
Yin Yang Magic
Meatbone Slash
Maintenance
Jump+2

Blaze Gun
Mythril Shield
Headgear
Leather Outfit
Leather Mantle

Charge+1
Spell Absorb, Life Drain, Dispel Magic, Paralyze, Sleep



Genoguy
Female
Cancer
72
69
Lancer
Time Magic
PA Save
Secret Hunt
Waterwalking

Obelisk
Buckler
Leather Helmet
Mythril Armor
Angel Ring

Level Jump5, Vertical Jump8
Haste 2, Slow, Slow 2, Demi, Stabilize Time

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jordache7K
Female
Libra
68
44
Priest
Elemental
Counter Flood
Equip Armor
Waterbreathing

Scorpion Tail

Leather Hat
Leather Armor
Salty Rage

Cure, Cure 2, Protect 2, Shell, Shell 2, Esuna
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Terra 32
Female
Taurus
46
63
Knight
Steal
Counter Tackle
Defense UP
Levitate

Ragnarok

Bronze Helmet
Light Robe
Defense Armlet

Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Surging Sword
Gil Taking, Steal Helmet, Steal Accessory, Leg Aim



Ominnous
Female
Capricorn
58
74
Mediator
Time Magic
Parry
Secret Hunt
Move+3

Mythril Gun

Triangle Hat
Adaman Vest
Jade Armlet

Threaten, Preach, Solution, Death Sentence, Refute
Haste, Reflect, Quick, Stabilize Time



ZergTwitch
Monster
Sagittarius
68
72
Vampire








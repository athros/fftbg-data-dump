Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Coralreeferz
Male
Serpentarius
78
75
Ninja
Yin Yang Magic
Catch
Doublehand
Move+2

Short Edge

Feather Hat
Brigandine
Reflect Ring

Ninja Sword
Life Drain, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify



Victoriolue
Female
Aquarius
73
45
Wizard
Battle Skill
Counter
Halve MP
Ignore Terrain

Dragon Rod

Leather Hat
Robe of Lords
Elf Mantle

Fire 3, Fire 4, Ice 2, Empower
Armor Break, Shield Break, Weapon Break, Speed Break, Surging Sword



NicoSavoy
Female
Leo
73
53
Mime

Auto Potion
Equip Shield
Jump+2


Ice Shield
Barbuta
Mystic Vest
Rubber Shoes

Mimic




Fluffywormhole
Male
Pisces
80
79
Mediator
Time Magic
Critical Quick
Attack UP
Move+3

Mythril Gun

Red Hood
Silk Robe
Rubber Shoes

Persuade, Praise, Threaten, Preach, Refute
Haste, Float, Demi 2, Meteor

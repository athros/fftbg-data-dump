Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ThePineappleSalesman
Female
Scorpio
52
78
Archer
Black Magic
Caution
Defend
Waterwalking

Glacier Gun
Buckler
Triangle Hat
Leather Outfit
Defense Armlet

Charge+1
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice, Ice 4



Deathmaker06
Monster
Serpentarius
47
72
Red Dragon










Megakim
Female
Taurus
70
80
Samurai
Dance
Distribute
Equip Polearm
Waterbreathing

Octagon Rod

Barbuta
Crystal Mail
Defense Armlet

Heaven's Cloud
Wiznaibus, Nameless Dance



Breakdown777
Female
Aquarius
67
44
Wizard
Draw Out
Distribute
Magic Attack UP
Retreat

Rod

Red Hood
Light Robe
Rubber Shoes

Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 3
Koutetsu, Bizen Boat, Heaven's Cloud

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mirapoix
Male
Gemini
57
66
Time Mage
Throw
Damage Split
Equip Sword
Jump+1

Defender

Red Hood
Linen Robe
Sprint Shoes

Haste 2, Demi, Demi 2, Stabilize Time
Shuriken, Bomb, Axe



Randgridr
Female
Sagittarius
44
62
Summoner
Black Magic
Caution
Halve MP
Move-MP Up

Battle Folio

Thief Hat
Judo Outfit
Feather Boots

Moogle, Ifrit, Golem, Carbunkle, Bahamut
Fire 2, Fire 3, Bolt 3, Bolt 4, Flare



DeathTaxesAndAnime
Female
Pisces
77
56
Samurai
Yin Yang Magic
Parry
Beastmaster
Teleport 2

Obelisk

Crystal Helmet
Silk Robe
Battle Boots

Koutetsu, Muramasa
Blind, Poison, Life Drain, Pray Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep



StealthModeLocke
Male
Sagittarius
57
37
Archer
Draw Out
HP Restore
Equip Sword
Teleport

Muramasa
Hero Shield
Thief Hat
Power Sleeve
Germinas Boots

Charge+2
Koutetsu, Murasame, Muramasa

Player: !White
Team: White Team
Palettes: White/Blue



Sect Cor
Male
Serpentarius
57
79
Monk
Elemental
Mana Shield
Defense UP
Jump+2



Golden Hairpin
Chain Vest
108 Gems

Spin Fist, Wave Fist, Secret Fist, Purification
Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



NepSmug
Male
Scorpio
66
60
Time Mage
Charge
Speed Save
Dual Wield
Retreat

Rainbow Staff
Oak Staff
Cachusha
Black Robe
Leather Mantle

Haste, Stop, Float, Demi, Demi 2, Stabilize Time
Charge+1, Charge+3, Charge+7



Teensydino
Male
Virgo
66
66
Thief
Sing
Abandon
Magic Attack UP
Waterwalking

Cultist Dagger

Circlet
Chain Vest
Rubber Shoes

Gil Taking, Steal Helmet, Steal Armor, Steal Status, Arm Aim
Angel Song, Space Storage, Sky Demon, Hydra Pit



Deathmaker06
Female
Sagittarius
65
60
Calculator
Black Magic
Caution
Short Charge
Ignore Height

Flame Rod

Red Hood
Black Costume
Leather Mantle

CT, Prime Number, 5, 4, 3
Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Frog, Death

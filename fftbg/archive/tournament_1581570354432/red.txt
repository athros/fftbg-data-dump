Player: !Red
Team: Red Team
Palettes: Red/Brown



The Fryman
Monster
Capricorn
77
68
Dragon










Ezpaguety
Male
Virgo
66
59
Mediator
Summon Magic
Faith Up
Martial Arts
Jump+3

Assassin Dagger

Leather Hat
Light Robe
Defense Armlet

Persuade, Praise, Threaten, Preach, Insult, Mimic Daravon, Refute
Ramuh, Carbunkle, Leviathan



Deciimos
Female
Pisces
67
82
Wizard
Basic Skill
Damage Split
Magic Attack UP
Jump+3

Mage Masher

Barette
Robe of Lords
Reflect Ring

Fire, Fire 2, Bolt, Bolt 2, Ice 2, Ice 3, Ice 4, Empower
Dash, Heal, Tickle, Wish, Scream



Lonfyre
Male
Capricorn
64
56
Samurai
Talk Skill
Regenerator
Short Status
Ignore Height

Koutetsu Knife

Platinum Helmet
Chameleon Robe
Wizard Mantle

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa
Praise, Threaten, Solution, Rehabilitate

Player: !Black
Team: Black Team
Palettes: Black/Red



Meta Five
Male
Leo
65
76
Mediator
Draw Out
Faith Save
Sicken
Move+2

Stone Gun

Golden Hairpin
Black Costume
Battle Boots

Invitation, Death Sentence, Insult, Refute, Rehabilitate
Asura, Muramasa, Chirijiraden



J2DaBibbles
Female
Virgo
69
67
Mediator
Draw Out
Auto Potion
Defense UP
Move+2

Romanda Gun

Headgear
Adaman Vest
Power Wrist

Persuade, Refute, Rehabilitate
Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji



Killth3kid
Male
Aries
53
63
Oracle
Basic Skill
Absorb Used MP
Equip Gun
Jump+3

Blast Gun

Flash Hat
Light Robe
Red Shoes

Poison, Pray Faith, Silence Song, Dark Holy
Accumulate, Throw Stone, Cheer Up



Redmage4evah
Male
Scorpio
73
45
Wizard
Punch Art
Meatbone Slash
Sicken
Swim

Rod

Triangle Hat
Adaman Vest
Magic Gauntlet

Fire 2, Bolt, Bolt 4, Ice 3, Empower, Death, Flare
Earth Slash, Secret Fist, Purification, Seal Evil

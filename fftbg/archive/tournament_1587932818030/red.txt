Player: !Red
Team: Red Team
Palettes: Red/Brown



YaBoy125
Female
Gemini
65
74
Summoner
Punch Art
Mana Shield
Concentrate
Move+2

Wizard Staff

Thief Hat
White Robe
Germinas Boots

Moogle, Shiva, Ifrit, Odin, Salamander, Cyclops
Purification, Revive



UmaiJam
Male
Taurus
77
64
Monk
Yin Yang Magic
Distribute
Dual Wield
Move-MP Up



Golden Hairpin
Wizard Outfit
Power Wrist

Purification, Revive
Poison, Spell Absorb, Foxbird, Dispel Magic



Realitydown
Male
Capricorn
55
45
Squire
Talk Skill
Dragon Spirit
Maintenance
Move-MP Up

Battle Axe
Mythril Shield
Green Beret
Gold Armor
Magic Gauntlet

Heal, Yell
Invitation, Persuade, Threaten, Preach, Solution, Mimic Daravon, Refute, Rehabilitate



MantisFinch
Female
Sagittarius
64
69
Squire
Jump
Distribute
Dual Wield
Move-MP Up

Blood Sword
Giant Axe
Black Hood
Platinum Armor
Battle Boots

Accumulate, Tickle, Scream
Level Jump4, Vertical Jump6

Player: !Green
Team: Green Team
Palettes: Green/White



Sir Uther115
Male
Libra
43
48
Knight
Throw
Caution
Dual Wield
Waterbreathing

Blood Sword
Coral Sword
Cross Helmet
Bronze Armor
Jade Armlet

Head Break, Armor Break, Speed Break, Power Break, Dark Sword
Axe



Laserman1000
Male
Taurus
72
73
Thief
Draw Out
Faith Save
Sicken
Jump+1

Air Knife

Black Hood
Chain Vest
Reflect Ring

Gil Taking, Steal Shield, Leg Aim
Asura, Heaven's Cloud



Ko2q
Male
Serpentarius
62
40
Mediator
Charge
Brave Save
Magic Attack UP
Lava Walking

Glacier Gun

Feather Hat
Mythril Vest
Small Mantle

Invitation, Persuade, Threaten, Insult, Refute, Rehabilitate
Charge+1, Charge+2, Charge+5, Charge+7



Letdowncity
Female
Gemini
58
68
Monk
Steal
Counter Magic
Magic Defense UP
Fly



Thief Hat
Chain Vest
Elf Mantle

Pummel, Purification, Chakra, Seal Evil
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Steal Status

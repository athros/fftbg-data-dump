Player: !Red
Team: Red Team
Palettes: Red/Brown



Lyonslegacy
Female
Cancer
62
69
Archer
Time Magic
MP Restore
Defense UP
Move+3

Mythril Bow

Diamond Helmet
Earth Clothes
Genji Gauntlet

Charge+1, Charge+4, Charge+5
Haste, Slow, Float, Reflect, Quick, Stabilize Time



E Ballard
Male
Leo
56
45
Mime

Counter Magic
Defense UP
Jump+2



Holy Miter
Mystic Vest
Small Mantle

Mimic




RaIshtar
Female
Scorpio
72
70
Dancer
Elemental
Arrow Guard
Equip Shield
Levitate

Panther Bag
Round Shield
Golden Hairpin
Light Robe
108 Gems

Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Last Dance, Obsidian Blade
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Modoh
Female
Sagittarius
56
50
Calculator
Yin Yang Magic
Brave Save
Short Status
Ignore Height

Papyrus Codex

Green Beret
White Robe
Magic Gauntlet

Height, 5, 4, 3
Poison, Zombie, Silence Song, Paralyze, Sleep

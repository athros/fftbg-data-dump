Player: !Red
Team: Red Team
Palettes: Red/Brown



Lordminsc
Male
Cancer
42
49
Bard
Draw Out
Counter
Doublehand
Waterbreathing

Ramia Harp

Flash Hat
Gold Armor
N-Kai Armlet

Cheer Song, Battle Song, Nameless Song, Diamond Blade
Kiyomori, Kikuichimoji



Gelwain
Female
Taurus
56
60
Time Mage
Draw Out
Sunken State
Short Charge
Jump+1

Oak Staff

Holy Miter
Brigandine
Dracula Mantle

Haste, Float, Quick, Stabilize Time
Muramasa



Fluffywormhole
Male
Taurus
55
65
Geomancer
Time Magic
Sunken State
Magic Attack UP
Ignore Terrain

Slasher
Crystal Shield
Red Hood
Silk Robe
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Slow, Float, Stabilize Time



GreatRedDragon
Male
Gemini
62
54
Summoner
Black Magic
Auto Potion
Beastmaster
Swim

Wizard Staff

Leather Hat
Chameleon Robe
Elf Mantle

Moogle, Shiva, Carbunkle, Silf, Lich
Fire, Bolt 2, Bolt 4, Ice 4, Frog, Death

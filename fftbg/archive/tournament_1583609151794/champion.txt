Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Faantatv
Female
Pisces
66
70
Priest
Talk Skill
Distribute
Monster Talk
Move+3

Wizard Staff

Feather Hat
Chameleon Robe
N-Kai Armlet

Cure, Cure 3, Cure 4, Regen, Wall, Esuna, Holy
Threaten, Death Sentence, Insult, Refute



Omegasuspekt
Male
Taurus
74
74
Ninja
Elemental
HP Restore
Equip Armor
Jump+2

Morning Star
Sasuke Knife
Genji Helmet
Silk Robe
Diamond Armlet

Bomb, Dictionary
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Leakimiko
Female
Aries
79
38
Chemist
Steal
MA Save
Equip Gun
Move-HP Up

Mythril Gun

Black Hood
Judo Outfit
Battle Boots

Ether, Eye Drop, Maiden's Kiss, Phoenix Down
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Arm Aim



Krymzon19
Female
Capricorn
45
64
Mediator
Punch Art
Dragon Spirit
Dual Wield
Ignore Height

Blaze Gun
Blaze Gun
Thief Hat
Robe of Lords
Feather Mantle

Invitation, Threaten, Negotiate, Refute, Rehabilitate
Spin Fist, Wave Fist, Purification, Revive

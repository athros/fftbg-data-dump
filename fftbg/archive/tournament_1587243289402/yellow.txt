Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dictatorhowells
Female
Cancer
66
55
Geomancer
Summon Magic
Absorb Used MP
Equip Sword
Teleport

Kikuichimoji
Genji Shield
Leather Hat
White Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Moogle, Ifrit, Carbunkle, Odin, Leviathan, Silf, Lich



TheChainNerd
Female
Aries
68
49
Time Mage
Punch Art
MA Save
Dual Wield
Waterwalking

Rainbow Staff
Cute Bag
Feather Hat
Light Robe
Cursed Ring

Haste, Slow, Stop, Demi 2, Stabilize Time, Meteor
Spin Fist, Wave Fist, Earth Slash, Chakra



Elkydeluxe
Male
Sagittarius
40
66
Summoner
Time Magic
Catch
Sicken
Jump+2

Thunder Rod

Golden Hairpin
Chain Vest
Jade Armlet

Moogle, Ramuh
Haste, Haste 2, Slow 2, Stop, Float, Reflect, Quick, Stabilize Time



Gelwain
Female
Cancer
75
48
Wizard
Yin Yang Magic
Sunken State
Short Status
Waterbreathing

Air Knife

Leather Hat
Linen Robe
Magic Gauntlet

Fire 2, Fire 4, Bolt, Ice 3, Empower
Poison, Foxbird

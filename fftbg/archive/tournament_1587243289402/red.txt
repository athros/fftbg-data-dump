Player: !Red
Team: Red Team
Palettes: Red/Brown



Hales Bopp It
Male
Sagittarius
45
61
Summoner
Talk Skill
Hamedo
Short Status
Jump+3

Thunder Rod

Golden Hairpin
White Robe
Small Mantle

Moogle, Ifrit, Carbunkle, Odin, Leviathan, Silf, Fairy, Lich
Invitation, Persuade, Threaten, Solution, Refute



KonzeraLive
Male
Capricorn
80
80
Archer
Steal
Hamedo
Sicken
Waterbreathing

Bow Gun
Diamond Shield
Black Hood
Leather Outfit
Sprint Shoes

Charge+1, Charge+5, Charge+7
Steal Status



Silentperogy
Male
Leo
55
75
Squire
Charge
Arrow Guard
Maintenance
Jump+1

Giant Axe
Ice Shield
Ribbon
Rubber Costume
Chantage

Dash, Heal, Tickle
Charge+1, Charge+3



CT 5 Holy
Male
Cancer
65
51
Archer
Jump
Meatbone Slash
Magic Defense UP
Move+1

Night Killer
Round Shield
Feather Hat
Wizard Outfit
Feather Mantle

Charge+2, Charge+5, Charge+10
Level Jump8, Vertical Jump8

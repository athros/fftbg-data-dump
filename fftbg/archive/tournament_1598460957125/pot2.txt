Final Bets: green - 16 bets for 15,049G (76.0%, x0.32); yellow - 9 bets for 4,744G (24.0%, x3.17)

green bets:
DavenIII: 5,000G (33.2%, 19,333G)
Zeroroute: 2,793G (18.6%, 8,465G)
Lydian_C: 1,200G (8.0%, 16,339G)
BirbBrainsBot: 1,000G (6.6%, 50,798G)
Forkmore: 777G (5.2%, 2,466G)
Nizaha: 768G (5.1%, 25,026G)
getthemoneyz: 746G (5.0%, 1,748,476G)
dogsandcatsand: 666G (4.4%, 666G)
killth3kid: 524G (3.5%, 2,687G)
thunderducker: 500G (3.3%, 1,585G)
Lord_Gwarth: 337G (2.2%, 1,052G)
datadrivenbot: 200G (1.3%, 61,785G)
WesSideVandal: 188G (1.2%, 188G)
Absalom_20: 150G (1.0%, 3,939G)
ko2q: 100G (0.7%, 10,470G)
Casiodorus: 100G (0.7%, 132G)

yellow bets:
DustBirdEX: 1,665G (35.1%, 1,665G)
toka222: 1,000G (21.1%, 273,127G)
Kellios11: 625G (13.2%, 625G)
SephDarkheart: 600G (12.6%, 194,306G)
CapnChaos12: 300G (6.3%, 2,414G)
old_overholt_: 200G (4.2%, 5,701G)
AllInBot: 154G (3.2%, 154G)
IncredibleInept: 100G (2.1%, 798G)
brenogarwin: 100G (2.1%, 1,056G)

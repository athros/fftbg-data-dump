Player: !White
Team: White Team
Palettes: White/Blue



Thaetreis
Female
Libra
62
46
Summoner
Battle Skill
Brave Up
Short Charge
Lava Walking

Flame Rod

Golden Hairpin
Silk Robe
Power Wrist

Moogle, Shiva, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Fairy, Lich
Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Surging Sword



Rnark
Male
Aquarius
44
81
Monk
Charge
Meatbone Slash
Concentrate
Move-HP Up



Ribbon
Power Sleeve
Battle Boots

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Charge+7



JustSuperish
Female
Taurus
55
48
Ninja
Item
Counter
Maintenance
Move+1

Spell Edge
Air Knife
Golden Hairpin
Clothes
Elf Mantle

Shuriken, Bomb, Knife, Spear
Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down



L2 Sentinel
Female
Virgo
74
69
Knight
Steal
Catch
Equip Bow
Lava Walking

Mythril Bow

Bronze Helmet
White Robe
Power Wrist

Head Break, Shield Break, Weapon Break, Stasis Sword, Surging Sword
Steal Helmet, Steal Armor, Steal Shield, Steal Accessory

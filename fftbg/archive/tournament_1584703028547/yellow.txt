Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ApplesauceBoss
Female
Cancer
62
76
Wizard
Battle Skill
Distribute
Magic Attack UP
Ignore Terrain

Thunder Rod

Headgear
Mythril Vest
Angel Ring

Ice, Ice 2, Death
Weapon Break, Magic Break, Power Break



PlatinumPlume
Male
Capricorn
59
76
Squire
Punch Art
Counter Flood
Attack UP
Fly

Cross Bow
Aegis Shield
Feather Hat
Genji Armor
Red Shoes

Accumulate, Dash, Throw Stone, Heal, Yell, Fury, Wish
Earth Slash, Secret Fist, Purification, Chakra, Revive



Red Celt
Male
Virgo
65
69
Time Mage
Throw
Critical Quick
Short Charge
Ignore Terrain

Wizard Staff

Red Hood
Light Robe
Small Mantle

Haste, Haste 2, Immobilize, Quick, Stabilize Time
Shuriken



Aneyus
Monster
Virgo
66
56
Red Panther








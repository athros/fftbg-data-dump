Player: !Green
Team: Green Team
Palettes: Green/White



Numbersborne
Female
Scorpio
61
50
Samurai
Elemental
Counter Magic
Maintenance
Move+2

Asura Knife

Gold Helmet
Gold Armor
Defense Armlet

Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Aldrammech
Male
Sagittarius
51
48
Time Mage
Sing
Distribute
Short Status
Swim

Wizard Staff

Red Hood
White Robe
108 Gems

Haste, Haste 2, Slow, Float, Quick, Demi 2, Stabilize Time
Life Song, Battle Song, Magic Song, Diamond Blade, Space Storage



Denamda
Female
Scorpio
56
56
Monk
White Magic
Parry
Equip Axe
Move+1

Gold Staff

Leather Hat
Leather Outfit
Magic Gauntlet

Pummel, Wave Fist, Purification, Revive
Raise, Raise 2, Regen



WillFitzgerald
Monster
Taurus
65
42
Hydra








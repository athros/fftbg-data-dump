Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CCRUNNER149UWP
Monster
Gemini
56
72
Black Goblin










Lanshaft
Male
Scorpio
74
69
Bard
Battle Skill
Brave Up
Short Charge
Retreat

Fairy Harp

Ribbon
Leather Outfit
Elf Mantle

Life Song, Battle Song, Magic Song, Nameless Song, Diamond Blade
Weapon Break, Speed Break, Mind Break, Stasis Sword, Surging Sword



Daveb
Male
Libra
66
65
Ninja
Yin Yang Magic
Sunken State
Equip Gun
Move+1

Ramia Harp
Bestiary
Leather Hat
Leather Outfit
Defense Armlet

Shuriken, Dictionary
Spell Absorb, Silence Song, Blind Rage, Confusion Song



Lyner87
Female
Capricorn
53
65
Priest
Steal
Sunken State
Short Charge
Jump+3

Flail

Twist Headband
Power Sleeve
Genji Gauntlet

Cure 4, Raise, Raise 2, Protect 2, Esuna
Gil Taking, Arm Aim

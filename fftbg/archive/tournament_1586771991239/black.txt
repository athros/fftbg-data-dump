Player: !Black
Team: Black Team
Palettes: Black/Red



Maeveen
Male
Libra
60
69
Thief
Throw
Caution
Equip Axe
Move-HP Up

Morning Star

Green Beret
Earth Clothes
Red Shoes

Gil Taking, Steal Armor, Leg Aim
Shuriken, Bomb, Stick



Upvla
Male
Leo
68
55
Archer
Time Magic
Speed Save
Doublehand
Levitate

Ice Bow

Leather Helmet
Adaman Vest
Setiemson

Charge+4, Charge+7, Charge+20
Haste 2, Demi, Stabilize Time



KitchTowel
Male
Virgo
55
50
Geomancer
Summon Magic
MA Save
Equip Armor
Teleport

Kiyomori
Escutcheon
Bronze Helmet
Adaman Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Carbunkle, Bahamut



Red Celt
Female
Libra
73
72
Lancer
Basic Skill
Regenerator
Magic Attack UP
Lava Walking

Obelisk
Bronze Shield
Platinum Helmet
Reflect Mail
Magic Ring

Level Jump8, Vertical Jump3
Heal, Yell, Cheer Up, Wish

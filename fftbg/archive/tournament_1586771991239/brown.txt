Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeedSC
Male
Scorpio
47
77
Knight
Charge
HP Restore
Equip Axe
Teleport

Slasher
Escutcheon
Cross Helmet
Leather Armor
Diamond Armlet

Head Break, Armor Break, Magic Break, Power Break, Mind Break, Stasis Sword
Charge+1



Kojigawatz
Male
Aries
59
76
Samurai
Talk Skill
Absorb Used MP
Defend
Jump+3

Partisan

Mythril Helmet
Diamond Armor
Rubber Shoes

Muramasa
Praise, Death Sentence, Insult, Negotiate, Refute, Rehabilitate



BlaqkElectric
Female
Virgo
60
78
Samurai
Steal
Sunken State
Magic Defense UP
Teleport

Bizen Boat

Cross Helmet
Genji Armor
Germinas Boots

Koutetsu
Steal Heart, Steal Helmet, Leg Aim



Lali Lulelo
Female
Scorpio
79
55
Summoner
Draw Out
Brave Up
Short Charge
Ignore Terrain

Thunder Rod

Flash Hat
Black Costume
N-Kai Armlet

Moogle, Ramuh, Titan, Golem, Fairy, Lich, Cyclops
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud

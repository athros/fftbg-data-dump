Player: !White
Team: White Team
Palettes: White/Blue



LDSkinny
Female
Aries
47
50
Oracle
Item
Meatbone Slash
Short Charge
Retreat

Octagon Rod

Golden Hairpin
Power Sleeve
Germinas Boots

Blind, Confusion Song, Dispel Magic, Sleep, Petrify
Potion, Hi-Potion, Hi-Ether, Phoenix Down



Actual JP
Female
Scorpio
69
69
Calculator
Nether Skill
Faith Save
Maintenance
Retreat

Battle Bamboo

Twist Headband
Light Robe
Reflect Ring

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



NIghtdew14
Female
Aquarius
61
60
Dancer
Yin Yang Magic
Caution
Equip Bow
Waterwalking

Mythril Bow

Flash Hat
Linen Robe
Leather Mantle

Witch Hunt, Slow Dance, Nameless Dance, Nether Demon, Dragon Pit
Spell Absorb, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze



Twelfthrootoftwo
Male
Serpentarius
79
72
Ninja
Basic Skill
Faith Save
Magic Attack UP
Move+3

Morning Star
Morning Star
Thief Hat
Mystic Vest
Reflect Ring

Shuriken, Stick
Throw Stone, Heal, Tickle, Cheer Up

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Nizaha
Female
Capricorn
72
46
Summoner
Yin Yang Magic
Caution
Short Charge
Retreat

Healing Staff

Holy Miter
White Robe
Reflect Ring

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Silf, Lich
Poison, Spell Absorb, Doubt Faith, Dispel Magic



Trowaba
Male
Sagittarius
58
54
Archer
Item
MP Restore
Equip Shield
Ignore Height

Ultimus Bow
Escutcheon
Headgear
Secret Clothes
Cherche

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Potion, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Lordminsc
Male
Gemini
79
46
Ninja
Charge
Speed Save
Magic Attack UP
Move+3

Morning Star
Air Knife
Red Hood
Mythril Vest
Vanish Mantle

Spear
Charge+1, Charge+2, Charge+3, Charge+4, Charge+5



PleXmito
Male
Pisces
70
60
Ninja
Summon Magic
Counter Flood
Equip Gun
Move-HP Up

Romanda Gun
Stone Gun
Leather Hat
Leather Outfit
Power Wrist

Staff
Moogle, Bahamut, Fairy, Lich

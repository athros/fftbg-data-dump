Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Midori Ribbon
Female
Taurus
60
72
Samurai
Item
Dragon Spirit
Defend
Move+1

Spear

Iron Helmet
Diamond Armor
Red Shoes

Asura, Koutetsu, Bizen Boat, Muramasa
Hi-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



ZephyrTempest
Male
Libra
67
68
Mediator
Battle Skill
Parry
Secret Hunt
Levitate

Mythril Gun

Flash Hat
Judo Outfit
Feather Mantle

Praise, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Armor Break, Shield Break



Omegasuspekt
Female
Sagittarius
62
78
Lancer
Yin Yang Magic
Faith Up
Defend
Jump+3

Holy Lance
Ice Shield
Iron Helmet
Silk Robe
Power Wrist

Level Jump2, Vertical Jump5
Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep



DrAntiSocial
Male
Cancer
66
42
Summoner
Jump
Sunken State
Short Charge
Jump+1

Cute Bag

Flash Hat
Adaman Vest
Battle Boots

Golem, Bahamut, Odin, Leviathan, Salamander, Silf, Fairy
Level Jump2, Vertical Jump2

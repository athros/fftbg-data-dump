Player: !Black
Team: Black Team
Palettes: Black/Red



Ayeayex3
Male
Pisces
49
68
Knight
Sing
Damage Split
Short Status
Move+1

Ancient Sword
Aegis Shield
Iron Helmet
Maximillian
Diamond Armlet

Power Break, Mind Break, Stasis Sword, Surging Sword
Life Song, Cheer Song, Last Song



Neerrm
Male
Pisces
62
41
Ninja
Summon Magic
Speed Save
Equip Polearm
Jump+1

Ryozan Silk
Obelisk
Headgear
Adaman Vest
Germinas Boots

Bomb
Moogle, Carbunkle, Odin, Leviathan, Salamander



Maakur
Male
Libra
76
60
Bard
Elemental
Abandon
Equip Polearm
Levitate

Mythril Spear

Leather Hat
Chain Mail
Feather Boots

Life Song, Nameless Song, Space Storage, Hydra Pit
Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Grininda
Female
Aquarius
72
69
Samurai
Time Magic
Counter Magic
Halve MP
Jump+3

Heaven's Cloud

Barbuta
Crystal Mail
N-Kai Armlet

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji, Masamune
Immobilize, Float, Reflect, Demi, Meteor

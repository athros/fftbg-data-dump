Player: !White
Team: White Team
Palettes: White/Blue



TheKillerNacho
Male
Cancer
57
47
Geomancer
Punch Art
Mana Shield
Equip Armor
Teleport

Giant Axe
Buckler
Leather Helmet
Reflect Mail
Feather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Pummel, Earth Slash, Purification, Revive



Jethrothrul
Female
Capricorn
58
59
Summoner
Charge
Counter Magic
Equip Gun
Move+3

Fairy Harp

Barette
Chameleon Robe
Genji Gauntlet

Moogle, Carbunkle, Fairy
Charge+1



Treapvort
Male
Leo
69
74
Bard
Charge
Catch
Doublehand
Ignore Terrain

Bloody Strings

Black Hood
Chain Vest
Leather Mantle

Life Song, Battle Song, Magic Song
Charge+20



Grininda
Female
Libra
73
57
Lancer
Punch Art
Caution
Magic Attack UP
Jump+1

Mythril Spear
Genji Shield
Cross Helmet
Carabini Mail
Leather Mantle

Level Jump2, Vertical Jump4
Pummel, Purification, Chakra, Revive

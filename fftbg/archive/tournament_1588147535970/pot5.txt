Final Bets: blue - 17 bets for 13,701G (66.9%, x0.49); green - 13 bets for 6,779G (33.1%, x2.02)

blue bets:
Phytik: 5,000G (36.5%, 15,276G)
pandasforsale: 1,111G (8.1%, 34,932G)
HaplessOne: 1,111G (8.1%, 9,023G)
Chrysaors: 1,068G (7.8%, 35,615G)
HorusTaurus: 1,000G (7.3%, 7,702G)
DeathTaxesAndAnime: 907G (6.6%, 1,780G)
Lydian_C: 596G (4.4%, 596G)
crimsonhellkite666: 500G (3.6%, 8,806G)
vorap: 500G (3.6%, 22,011G)
getthemoneyz: 484G (3.5%, 604,577G)
leakimiko: 436G (3.2%, 436G)
PotionDweller: 316G (2.3%, 316G)
cgMcWhiskers: 232G (1.7%, 232G)
ungabunga_bot: 140G (1.0%, 286,601G)
Arkreaver: 100G (0.7%, 14,740G)
StryderSeven: 100G (0.7%, 2,088G)
datadrivenbot: 100G (0.7%, 10,566G)

green bets:
R_Raynos: 1,061G (15.7%, 1,061G)
BirbBrainsBot: 1,000G (14.8%, 103,398G)
Mesmaster: 1,000G (14.8%, 14,526G)
ColetteMSLP: 700G (10.3%, 38,652G)
superdevon1: 600G (8.9%, 22,206G)
sinnyil2: 600G (8.9%, 4,806G)
Dymntd: 500G (7.4%, 17,385G)
AllInBot: 386G (5.7%, 386G)
midori_ribbon: 232G (3.4%, 232G)
Scotty297: 200G (3.0%, 2,469G)
KasugaiRoastedPeas: 200G (3.0%, 27,854G)
LivingHitokiri: 200G (3.0%, 11,954G)
Anethum: 100G (1.5%, 1,048G)

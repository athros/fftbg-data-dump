Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ANFz
Male
Libra
48
79
Geomancer
Throw
Abandon
Equip Gun
Jump+3

Stone Gun
Mythril Shield
Leather Hat
Mystic Vest
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Hammer, Stick



RunicMagus
Male
Libra
58
75
Chemist
Battle Skill
Brave Up
Secret Hunt
Move+3

Star Bag

Flash Hat
Power Sleeve
Chantage

Potion, X-Potion, Phoenix Down
Armor Break, Shield Break, Speed Break, Mind Break, Justice Sword, Surging Sword, Explosion Sword



Alacor
Male
Serpentarius
59
54
Knight
White Magic
Catch
Equip Sword
Move+1

Save the Queen

Gold Helmet
Linen Cuirass
Dracula Mantle

Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword, Night Sword
Cure 2, Cure 3, Cure 4, Raise, Regen, Shell, Esuna



AdmiralPikachu
Female
Leo
53
51
Geomancer
Summon Magic
Abandon
Magic Attack UP
Jump+3

Slasher
Gold Shield
Headgear
Judo Outfit
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball
Moogle, Shiva, Ramuh, Golem, Carbunkle, Bahamut

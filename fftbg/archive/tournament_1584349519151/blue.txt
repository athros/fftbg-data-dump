Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RongRongArts
Male
Capricorn
58
73
Chemist
Punch Art
Catch
Equip Polearm
Teleport

Cypress Rod

Black Hood
Leather Outfit
Angel Ring

Hi-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass
Wave Fist, Purification, Revive



RyuTsuno
Female
Cancer
69
67
Thief
White Magic
Hamedo
Equip Knife
Levitate

Zorlin Shape

Red Hood
Power Sleeve
Wizard Mantle

Steal Heart, Steal Helmet, Steal Armor, Arm Aim, Leg Aim
Cure, Raise, Raise 2, Protect



Chode Ular
Monster
Taurus
66
59
Hydra










L2 Sentinel
Male
Aquarius
73
78
Summoner
Jump
Counter Tackle
Beastmaster
Fly

Ice Rod

Holy Miter
Chameleon Robe
Magic Ring

Moogle, Carbunkle, Fairy
Level Jump5, Vertical Jump5

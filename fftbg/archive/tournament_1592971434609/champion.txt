Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Evewho
Female
Virgo
66
47
Samurai
Battle Skill
Critical Quick
Defense UP
Teleport 2

Obelisk

Diamond Helmet
Linen Cuirass
Feather Boots

Koutetsu, Heaven's Cloud, Kikuichimoji
Head Break, Armor Break, Shield Break, Power Break



Magicandy
Female
Scorpio
72
77
Geomancer
Talk Skill
PA Save
Magic Defense UP
Jump+2

Ancient Sword
Crystal Shield
Green Beret
Clothes
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Praise, Threaten, Preach, Mimic Daravon



OpHendoslice
Male
Aries
58
49
Samurai
Basic Skill
PA Save
Magic Attack UP
Fly

Kiyomori

Diamond Helmet
Black Robe
Magic Ring

Koutetsu, Bizen Boat, Murasame, Muramasa, Kikuichimoji
Accumulate, Dash, Throw Stone, Tickle, Yell, Wish, Scream



Ring Wyrm
Male
Gemini
69
47
Bard
Punch Art
Distribute
Concentrate
Lava Walking

Mythril Bow

Triangle Hat
Platinum Armor
Leather Mantle

Cheer Song
Spin Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil

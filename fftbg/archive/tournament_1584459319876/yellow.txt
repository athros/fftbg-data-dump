Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RoachCarnival
Female
Scorpio
50
55
Ninja
Time Magic
Abandon
Equip Gun
Move+1

Papyrus Codex
Bestiary
Red Hood
Secret Clothes
Defense Ring

Knife
Reflect, Quick, Meteor



Gabbagooluigi
Male
Scorpio
61
46
Chemist
White Magic
Parry
Halve MP
Move+1

Star Bag

Flash Hat
Earth Clothes
Cherche

Potion, X-Potion, Ether, Holy Water, Remedy, Phoenix Down
Regen



Wizblizz
Female
Leo
49
53
Samurai
Yin Yang Magic
Mana Shield
Attack UP
Waterwalking

Asura Knife

Cross Helmet
Robe of Lords
Leather Mantle

Koutetsu, Bizen Boat
Poison, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify



Jeeboheebo
Male
Libra
82
53
Samurai
Elemental
Counter Flood
Dual Wield
Levitate

Heaven's Cloud
Asura Knife
Leather Helmet
Plate Mail
Elf Mantle

Asura, Koutetsu, Muramasa
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

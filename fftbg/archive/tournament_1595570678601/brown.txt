Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Joewcarson
Male
Cancer
64
50
Lancer
Summon Magic
Speed Save
Long Status
Waterwalking

Holy Lance
Platinum Shield
Iron Helmet
Crystal Mail
Red Shoes

Level Jump8, Vertical Jump8
Titan, Golem, Leviathan, Salamander



SQUiDSQUARKLIN
Male
Cancer
66
71
Chemist
Black Magic
Auto Potion
Dual Wield
Retreat

Glacier Gun
Blast Gun
Holy Miter
Brigandine
Germinas Boots

Potion, Hi-Potion, Soft, Phoenix Down
Fire, Fire 4, Bolt 3, Bolt 4, Ice 3, Empower, Flare



HaateXIII
Female
Gemini
61
51
Oracle
Black Magic
Counter Magic
Maintenance
Waterwalking

Octagon Rod

Flash Hat
Chain Vest
Diamond Armlet

Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy
Fire, Fire 3, Bolt 2, Ice 2, Empower, Frog



Amiture
Female
Taurus
49
55
Summoner
Punch Art
Counter Flood
Short Charge
Jump+2

Ice Rod

Golden Hairpin
Clothes
Elf Mantle

Moogle, Shiva, Ifrit, Titan, Carbunkle, Fairy
Wave Fist, Purification, Revive

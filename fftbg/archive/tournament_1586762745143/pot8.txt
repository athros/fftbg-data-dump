Final Bets: blue - 14 bets for 12,815G (48.2%, x1.08); champion - 14 bets for 13,793G (51.8%, x0.93)

blue bets:
dotwarner17: 4,012G (31.3%, 4,012G)
HaplessOne: 1,500G (11.7%, 41,063G)
creedplaysgames: 1,013G (7.9%, 2,117G)
ungabunga_bot: 1,000G (7.8%, 118,413G)
BirbBrainsBot: 1,000G (7.8%, 70,811G)
rocl: 1,000G (7.8%, 68,214G)
Draconis345: 778G (6.1%, 778G)
Nocturnal_Perspective: 599G (4.7%, 11,475G)
deenglow: 500G (3.9%, 29,123G)
waterwatereverywhere: 500G (3.9%, 21,395G)
mangowarfare: 349G (2.7%, 349G)
Chuckolator: 264G (2.1%, 12,705G)
sixstop: 200G (1.6%, 2,924G)
Aneyus: 100G (0.8%, 20,498G)

champion bets:
leakimiko: 4,090G (29.7%, 16,360G)
UmaiJam: 2,498G (18.1%, 2,498G)
JackOnFire1: 2,349G (17.0%, 2,349G)
NytFantom: 1,000G (7.3%, 2,159G)
Conome: 1,000G (7.3%, 4,738G)
datadrivenbot: 606G (4.4%, 25,083G)
YaBoy125: 577G (4.2%, 5,777G)
WireLord: 496G (3.6%, 496G)
DeathTaxesAndAnime: 300G (2.2%, 4,803G)
fallendj77: 248G (1.8%, 248G)
Wesablo: 229G (1.7%, 229G)
Arkreaver: 200G (1.5%, 2,599G)
AllInBot: 100G (0.7%, 100G)
Maeveen: 100G (0.7%, 4,821G)

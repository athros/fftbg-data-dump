Player: !Black
Team: Black Team
Palettes: Black/Red



Nocturnal Perspective
Male
Gemini
55
70
Monk
Steal
Abandon
Defend
Move-MP Up



Leather Hat
Wizard Outfit
Spike Shoes

Wave Fist, Purification, Chakra
Steal Shield, Steal Accessory



LOKITHUS
Female
Taurus
46
79
Ninja
Dance
Brave Up
Magic Attack UP
Waterwalking

Air Knife
Ninja Edge
Headgear
Chain Vest
Defense Armlet

Bomb, Hammer, Staff
Witch Hunt, Polka Polka, Nameless Dance, Last Dance



CapnChaos12
Male
Gemini
47
70
Ninja
Time Magic
Catch
Equip Knife
Waterbreathing

Spell Edge
Flame Rod
Feather Hat
Clothes
Small Mantle

Shuriken, Hammer, Wand
Immobilize, Quick, Stabilize Time, Meteor



Go2sleepTV
Female
Leo
50
69
Squire
Throw
Faith Up
Defend
Move+1

Nagrarock
Genji Shield
Cross Helmet
Leather Outfit
Genji Gauntlet

Dash, Heal
Shuriken, Knife

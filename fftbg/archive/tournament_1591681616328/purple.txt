Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



L2 Sentinel
Male
Scorpio
73
74
Monk
Jump
Parry
Equip Gun
Lava Walking

Ramia Harp

Triangle Hat
Chain Vest
Defense Ring

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Level Jump4, Vertical Jump7



BuffaloCrunch
Female
Taurus
52
71
Chemist
Throw
Damage Split
Doublehand
Waterbreathing

Panther Bag

Feather Hat
Adaman Vest
Defense Armlet

X-Potion, Antidote, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Shuriken, Staff, Wand



Metagameface
Female
Taurus
62
53
Lancer
Battle Skill
Parry
Sicken
Jump+3

Mythril Spear
Gold Shield
Cross Helmet
Black Robe
Jade Armlet

Level Jump8, Vertical Jump7
Magic Break, Speed Break, Mind Break, Justice Sword, Dark Sword, Surging Sword



DeathTaxesAndAnime
Female
Gemini
74
66
Geomancer
Draw Out
Earplug
Equip Axe
Waterbreathing

Flail
Mythril Shield
Feather Hat
Light Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball
Muramasa

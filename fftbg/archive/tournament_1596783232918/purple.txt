Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ko2q
Female
Pisces
74
77
Summoner
Basic Skill
Absorb Used MP
Maintenance
Ignore Terrain

Ice Rod

Flash Hat
Wizard Robe
Elf Mantle

Moogle, Carbunkle, Bahamut, Cyclops
Accumulate, Heal, Fury, Wish, Scream



Seaweed B
Female
Capricorn
55
40
Ninja
Jump
Sunken State
Equip Gun
Move+1

Bestiary
Battle Folio
Headgear
Power Sleeve
Magic Gauntlet

Shuriken, Bomb, Knife, Spear
Level Jump8, Vertical Jump2



Sairentozon7
Male
Leo
49
55
Thief
Item
Caution
Equip Shield
Ignore Height

Hidden Knife
Buckler
Holy Miter
Brigandine
Wizard Mantle

Gil Taking, Steal Heart, Steal Shield, Steal Status, Arm Aim
Potion, Ether, Hi-Ether, Eye Drop, Soft, Remedy



Outer Monologue
Male
Sagittarius
47
62
Summoner
Draw Out
Distribute
Secret Hunt
Levitate

Battle Folio

Golden Hairpin
Silk Robe
Red Shoes

Moogle, Shiva, Ifrit, Golem, Carbunkle, Silf
Asura

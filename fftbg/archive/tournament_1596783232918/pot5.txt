Final Bets: blue - 8 bets for 6,416G (64.3%, x0.56); green - 8 bets for 3,566G (35.7%, x1.80)

blue bets:
NicoSavoy: 2,271G (35.4%, 113,585G)
BirbBrainsBot: 1,000G (15.6%, 20,750G)
josephiroth_143: 1,000G (15.6%, 3,056G)
DeathTaxesAndAnime: 948G (14.8%, 948G)
ko2q: 604G (9.4%, 604G)
AllInBot: 292G (4.6%, 292G)
gorgewall: 201G (3.1%, 1,955G)
Firesheath: 100G (1.6%, 24,222G)

green bets:
superdevon1: 875G (24.5%, 43,778G)
getthemoneyz: 684G (19.2%, 1,544,443G)
douchetron: 600G (16.8%, 600G)
DHaveWord: 456G (12.8%, 3,047G)
DesertWooder: 400G (11.2%, 3,196G)
outer_monologue: 240G (6.7%, 240G)
datadrivenbot: 200G (5.6%, 49,350G)
PantherIscariot: 111G (3.1%, 2,549G)

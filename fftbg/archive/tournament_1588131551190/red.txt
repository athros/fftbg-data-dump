Player: !Red
Team: Red Team
Palettes: Red/Brown



Alacor
Male
Sagittarius
43
58
Knight
Sing
Earplug
Short Charge
Ignore Height

Long Sword
Aegis Shield
Barbuta
Leather Armor
Wizard Mantle

Armor Break, Weapon Break, Magic Break, Stasis Sword, Justice Sword
Cheer Song, Magic Song, Nameless Song, Space Storage



GrayGhostGaming
Male
Libra
52
79
Geomancer
Punch Art
Abandon
Equip Axe
Teleport 2

Flail
Genji Shield
Triangle Hat
White Robe
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard, Lava Ball
Spin Fist, Secret Fist, Purification, Chakra, Revive



0v3rr8d
Female
Libra
56
56
Ninja
Dance
Abandon
Maintenance
Jump+3

Sasuke Knife
Hidden Knife
Feather Hat
Rubber Costume
Feather Boots

Shuriken, Sword
Polka Polka, Disillusion, Nameless Dance, Obsidian Blade



BuffaloCrunch
Female
Taurus
74
73
Squire
Dance
Counter
Defend
Move+3

Platinum Sword
Hero Shield
Barbuta
Gold Armor
Dracula Mantle

Accumulate, Throw Stone, Heal, Yell, Cheer Up, Fury, Wish, Scream
Obsidian Blade, Nether Demon

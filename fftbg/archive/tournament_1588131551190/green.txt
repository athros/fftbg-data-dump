Player: !Green
Team: Green Team
Palettes: Green/White



Draconis345
Male
Libra
79
64
Mime

Counter Flood
Dual Wield
Levitate



Green Beret
Secret Clothes
Defense Ring

Mimic




Realitydown
Female
Taurus
58
60
Ninja
Jump
Counter Magic
Defend
Move-MP Up

Morning Star
Hidden Knife
Red Hood
Brigandine
Feather Boots

Shuriken, Bomb, Knife
Level Jump8, Vertical Jump8



Jaritras
Male
Cancer
66
70
Ninja
Item
HP Restore
Halve MP
Jump+1

Ninja Edge
Kunai
Golden Hairpin
Adaman Vest
Magic Gauntlet

Shuriken, Bomb, Spear
Potion, Ether, Antidote, Echo Grass, Phoenix Down



Gorgewall
Female
Aquarius
76
60
Oracle
Talk Skill
Absorb Used MP
Halve MP
Ignore Height

Bestiary

Twist Headband
Black Robe
Magic Gauntlet

Spell Absorb, Blind Rage, Dispel Magic
Invitation, Death Sentence, Negotiate, Refute

Final Bets: black - 13 bets for 11,764G (85.8%, x0.17); brown - 5 bets for 1,952G (14.2%, x6.03)

black bets:
furrytomahawkk: 2,539G (21.6%, 2,539G)
Mesmaster: 2,000G (17.0%, 29,240G)
NicoSavoy: 2,000G (17.0%, 162,596G)
UmaiJam: 1,500G (12.8%, 53,317G)
BirbBrainsBot: 1,000G (8.5%, 167,208G)
twelfthrootoftwo: 803G (6.8%, 1,575G)
randgridr: 786G (6.7%, 786G)
escobro: 300G (2.6%, 6,955G)
Lemonjohns: 232G (2.0%, 232G)
dem0nj0ns: 204G (1.7%, 204G)
datadrivenbot: 200G (1.7%, 63,694G)
SaltiestMage: 100G (0.9%, 2,603G)
DeathTaxesAndAnime: 100G (0.9%, 2,326G)

brown bets:
extinctrational: 800G (41.0%, 800G)
amiture: 500G (25.6%, 1,494G)
opHendoslice: 338G (17.3%, 338G)
OrgasmicToast: 228G (11.7%, 228G)
getthemoneyz: 86G (4.4%, 1,229,425G)

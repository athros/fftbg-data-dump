Final Bets: red - 13 bets for 6,724G (59.5%, x0.68); blue - 8 bets for 4,573G (40.5%, x1.47)

red bets:
Mesmaster: 2,000G (29.7%, 11,139G)
roofiepops: 1,015G (15.1%, 1,015G)
gorgewall: 900G (13.4%, 900G)
Aldrammech: 697G (10.4%, 7,692G)
Smashy: 452G (6.7%, 9,116G)
skipsandwiches: 368G (5.5%, 368G)
Rurk: 312G (4.6%, 499G)
Strifu: 280G (4.2%, 280G)
datadrivenbot: 200G (3.0%, 65,905G)
WillFitzgerald: 200G (3.0%, 3,343G)
AllInBot: 100G (1.5%, 100G)
ravingsockmonkey: 100G (1.5%, 490G)
Lemonjohns: 100G (1.5%, 3,845G)

blue bets:
DeathTaxesAndAnime: 1,477G (32.3%, 1,477G)
BirbBrainsBot: 1,000G (21.9%, 4,159G)
DustBirdEX: 697G (15.2%, 697G)
Phi_Sig: 555G (12.1%, 124,622G)
Sairentozon7: 416G (9.1%, 416G)
JonnyCue: 300G (6.6%, 300G)
Absalom_20: 100G (2.2%, 441G)
getthemoneyz: 28G (0.6%, 1,733,665G)

Player: !Green
Team: Green Team
Palettes: Green/White



ACSpree
Male
Serpentarius
66
79
Wizard
Elemental
Absorb Used MP
Long Status
Move-MP Up

Thunder Rod

Golden Hairpin
Wizard Robe
Spike Shoes

Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 4
Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Pwlloth
Female
Pisces
69
61
Chemist
Black Magic
HP Restore
Equip Gun
Jump+2

Papyrus Codex

Feather Hat
Black Costume
Feather Boots

Potion, Hi-Potion, Ether, Antidote, Holy Water
Bolt 3, Bolt 4, Ice 3



DustBirdEX
Female
Libra
59
63
Geomancer
Summon Magic
Damage Split
Defend
Jump+2

Coral Sword
Genji Shield
Red Hood
Chain Vest
Defense Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Moogle, Golem, Carbunkle, Cyclops



Upvla
Female
Capricorn
80
57
Geomancer
Draw Out
PA Save
Martial Arts
Waterwalking


Ice Shield
Golden Hairpin
Chameleon Robe
Magic Gauntlet

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Asura, Bizen Boat, Murasame

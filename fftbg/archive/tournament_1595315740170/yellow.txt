Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Firesheath
Female
Pisces
79
51
Mediator
Charge
Abandon
Magic Attack UP
Ignore Terrain

Battle Folio

Black Hood
Adaman Vest
Magic Ring

Invitation, Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon
Charge+7, Charge+20



ThanatosXRagnarok
Female
Libra
57
63
Priest
Steal
Blade Grasp
Equip Gun
Move-MP Up

Battle Folio

Holy Miter
Chameleon Robe
Diamond Armlet

Cure, Regen, Protect, Shell, Shell 2, Wall, Esuna
Steal Shield



Porkchop5158
Male
Leo
67
80
Squire
Steal
Mana Shield
Beastmaster
Jump+2

Dagger
Ice Shield
Circlet
Linen Cuirass
Power Wrist

Accumulate, Cheer Up
Gil Taking, Steal Weapon, Arm Aim, Leg Aim



DAC169
Female
Aquarius
76
51
Knight
Summon Magic
Regenerator
Martial Arts
Jump+3

Broad Sword
Mythril Shield
Crystal Helmet
Crystal Mail
Magic Ring

Armor Break, Weapon Break, Speed Break, Power Break, Justice Sword, Night Sword, Surging Sword
Moogle, Ramuh, Titan, Carbunkle, Bahamut, Silf

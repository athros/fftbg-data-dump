Player: !Red
Team: Red Team
Palettes: Red/Brown



Calajo
Male
Taurus
46
74
Samurai
Punch Art
Parry
Secret Hunt
Move+1

Koutetsu Knife

Genji Helmet
Linen Robe
Small Mantle

Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil



Vultuous
Male
Gemini
70
68
Oracle
Battle Skill
Parry
Attack UP
Retreat

Gokuu Rod

Headgear
Mythril Vest
Power Wrist

Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Dispel Magic
Weapon Break, Magic Break, Speed Break, Mind Break, Dark Sword, Surging Sword



Arcblazer23
Female
Gemini
57
75
Samurai
Charge
Parry
Beastmaster
Move-HP Up

Bizen Boat

Iron Helmet
Crystal Mail
Feather Mantle

Murasame, Muramasa
Charge+3, Charge+4, Charge+5



Powermhero
Male
Aquarius
58
37
Monk
Throw
Dragon Spirit
Sicken
Move+3



Leather Hat
Earth Clothes
Dracula Mantle

Wave Fist, Secret Fist, Purification, Revive
Shuriken, Bomb, Staff

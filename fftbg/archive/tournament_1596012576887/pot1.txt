Final Bets: red - 10 bets for 4,349G (39.5%, x1.53); blue - 7 bets for 6,656G (60.5%, x0.65)

red bets:
BirbBrainsBot: 1,000G (23.0%, 5,521G)
ColetteMSLP: 1,000G (23.0%, 34,714G)
Calajo: 897G (20.6%, 897G)
superdevon1: 631G (14.5%, 31,581G)
gorgewall: 201G (4.6%, 5,804G)
datadrivenbot: 200G (4.6%, 45,620G)
Chambs12: 120G (2.8%, 1,552G)
Vultuous: 100G (2.3%, 876G)
Powermhero: 100G (2.3%, 31,710G)
TheseJeans: 100G (2.3%, 2,811G)

blue bets:
randgridr: 3,359G (50.5%, 6,718G)
Vaxaldan: 1,485G (22.3%, 1,485G)
getthemoneyz: 710G (10.7%, 1,429,187G)
Wooplestein: 414G (6.2%, 414G)
douchetron: 388G (5.8%, 388G)
AllInBot: 200G (3.0%, 200G)
twelfthrootoftwo: 100G (1.5%, 854G)

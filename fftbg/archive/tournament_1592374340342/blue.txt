Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Soren Of Tyto
Male
Libra
67
44
Squire
Sing
PA Save
Equip Armor
Fly

Scorpion Tail
Mythril Shield
Holy Miter
Black Robe
Defense Ring

Fury, Scream
Life Song, Magic Song, Diamond Blade, Sky Demon



Lythe Caraker
Male
Cancer
71
51
Archer
Punch Art
Counter
Doublehand
Jump+3

Long Bow

Cross Helmet
Earth Clothes
Spike Shoes

Charge+1, Charge+5, Charge+7
Purification, Chakra, Revive



Grininda
Female
Scorpio
76
73
Ninja
Jump
Regenerator
Secret Hunt
Waterbreathing

Spell Edge
Iga Knife
Twist Headband
Mythril Vest
Feather Boots

Knife, Staff
Level Jump8, Vertical Jump4



Cryptopsy70
Female
Scorpio
41
64
Knight
Time Magic
PA Save
Equip Gun
Jump+2

Fairy Harp
Bronze Shield
Bronze Helmet
Gold Armor
Defense Armlet

Head Break, Weapon Break, Speed Break, Stasis Sword, Night Sword
Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time

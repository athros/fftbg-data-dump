Player: !zChamp
Team: Champion Team
Palettes: Green/White



Accophox
Male
Aquarius
45
57
Wizard
Throw
Catch
Equip Armor
Move-MP Up

Mage Masher

Grand Helmet
Diamond Armor
Jade Armlet

Fire, Fire 4, Bolt 4, Ice, Ice 3, Ice 4, Empower
Bomb



Firesheath
Male
Sagittarius
59
73
Lancer
Steal
Meatbone Slash
Magic Attack UP
Teleport

Gungnir
Ice Shield
Barbuta
Plate Mail
Battle Boots

Level Jump2, Vertical Jump8
Steal Heart, Steal Helmet, Steal Armor, Steal Weapon



Jaritras
Male
Taurus
57
48
Knight
Draw Out
Speed Save
Martial Arts
Move+2

Battle Axe
Crystal Shield
Crystal Helmet
Black Robe
Sprint Shoes

Armor Break, Magic Break, Power Break, Stasis Sword, Surging Sword, Explosion Sword
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



Cougboi
Female
Leo
59
62
Geomancer
Draw Out
PA Save
Equip Gun
Move+3

Romanda Gun
Flame Shield
Headgear
Light Robe
Defense Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud

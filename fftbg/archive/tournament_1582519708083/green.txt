Player: !Green
Team: Green Team
Palettes: Green/White



Hasterious
Female
Taurus
78
69
Samurai
Charge
Critical Quick
Magic Attack UP
Ignore Terrain

Gungnir

Cross Helmet
White Robe
Angel Ring

Koutetsu, Bizen Boat, Kikuichimoji
Charge+3, Charge+4



Regular Obbi
Male
Capricorn
41
60
Ninja
Item
Mana Shield
Equip Gun
Waterbreathing

Bestiary
Bloody Strings
Headgear
Adaman Vest
Magic Gauntlet

Shuriken, Bomb, Wand
Hi-Potion, Elixir, Echo Grass



Eficent
Monster
Capricorn
43
75
Wild Boar










VulgarFlamingo
Male
Gemini
47
75
Calculator
Bird Skill
MA Save
Dual Wield
Waterwalking

Wizard Rod
Rod
Platinum Helmet
Platinum Armor
Feather Boots

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak

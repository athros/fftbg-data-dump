Final Bets: red - 16 bets for 7,410G (42.7%, x1.34); yellow - 16 bets for 9,933G (57.3%, x0.75)

red bets:
Zeroroute: 2,150G (29.0%, 2,150G)
ApplesauceBoss: 966G (13.0%, 966G)
ANFz: 832G (11.2%, 832G)
itsZaes: 658G (8.9%, 658G)
E_Ballard: 600G (8.1%, 600G)
Saldarin: 500G (6.7%, 2,472G)
CorpusCav: 392G (5.3%, 392G)
vorap: 300G (4.0%, 52,517G)
passermine: 200G (2.7%, 2,155G)
AUrato: 200G (2.7%, 3,311G)
Tetsuwan_Melty: 112G (1.5%, 112G)
Tougou: 100G (1.3%, 5,071G)
silverelmdor: 100G (1.3%, 639G)
Avin_Chaos: 100G (1.3%, 12,440G)
datadrivenbot: 100G (1.3%, 18,974G)
FlohIristhrae: 100G (1.3%, 1,115G)

yellow bets:
Mesmaster: 2,000G (20.1%, 51,763G)
Rislyeu: 1,823G (18.4%, 1,823G)
Draconis345: 1,000G (10.1%, 75,840G)
ungabunga_bot: 1,000G (10.1%, 534,457G)
BirbBrainsBot: 1,000G (10.1%, 141,081G)
DeathTaxesAndAnime: 532G (5.4%, 532G)
EnemyController: 500G (5.0%, 243,170G)
HaateXIII: 500G (5.0%, 5,062G)
evdoggity: 288G (2.9%, 288G)
nekojin: 250G (2.5%, 250G)
cam_ATS: 208G (2.1%, 208G)
tronfonne: 200G (2.0%, 1,385G)
Moshyhero: 200G (2.0%, 745G)
benticore: 200G (2.0%, 4,521G)
getthemoneyz: 132G (1.3%, 678,721G)
Jampck: 100G (1.0%, 1,119G)

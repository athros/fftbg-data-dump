Player: !Black
Team: Black Team
Palettes: Black/Red



Bryan792
Female
Sagittarius
57
67
Chemist
Yin Yang Magic
Counter Magic
Equip Bow
Waterwalking

Bow Gun

Black Hood
Leather Outfit
Defense Ring

X-Potion, Echo Grass, Remedy, Phoenix Down
Blind, Spell Absorb, Doubt Faith, Zombie, Silence Song, Sleep, Petrify



Mtueni
Male
Gemini
71
49
Knight
Time Magic
Counter
Maintenance
Move-MP Up

Rune Blade
Crystal Shield
Crystal Helmet
Chain Mail
Jade Armlet

Head Break, Armor Break, Weapon Break, Mind Break, Stasis Sword, Night Sword
Haste, Slow 2



UmaiJam
Female
Capricorn
53
44
Calculator
Black Magic
MA Save
Short Status
Levitate

Bestiary

Headgear
Light Robe
Genji Gauntlet

CT, Height, 4, 3
Fire 2, Fire 3, Bolt 4, Ice, Frog



Lydian C
Female
Pisces
59
63
Geomancer
Draw Out
Sunken State
Equip Knife
Jump+1

Wizard Rod
Flame Shield
Holy Miter
Earth Clothes
Power Wrist

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Koutetsu, Murasame, Heaven's Cloud, Muramasa

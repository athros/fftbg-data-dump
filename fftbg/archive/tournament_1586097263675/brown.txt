Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeedSC
Male
Pisces
46
52
Geomancer
Summon Magic
Parry
Beastmaster
Waterbreathing

Slasher
Genji Shield
Feather Hat
Wizard Robe
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Shiva, Ramuh, Ifrit, Titan, Golem, Fairy



Bad1dea
Female
Taurus
66
64
Time Mage
Battle Skill
Counter Tackle
Defend
Ignore Terrain

Bestiary

Black Hood
Chameleon Robe
Cherche

Haste, Slow, Slow 2, Quick, Demi, Demi 2, Stabilize Time
Mind Break, Justice Sword



BenYuPoker
Female
Cancer
65
62
Samurai
Item
MA Save
Attack UP
Swim

Spear

Circlet
Linen Cuirass
Genji Gauntlet

Murasame, Muramasa
Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down



Shakarak
Male
Libra
60
40
Archer
Punch Art
Absorb Used MP
Dual Wield
Retreat

Mythril Gun
Glacier Gun
Genji Helmet
Mystic Vest
Dracula Mantle

Charge+5, Charge+7
Spin Fist, Wave Fist, Earth Slash, Purification, Revive

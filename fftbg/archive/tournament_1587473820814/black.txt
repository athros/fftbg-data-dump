Player: !Black
Team: Black Team
Palettes: Black/Red



Baron Von Scrub
Female
Gemini
55
50
Mediator
Draw Out
Counter
Equip Knife
Jump+3

Kunai

Barette
Mystic Vest
Jade Armlet

Persuade, Threaten, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon
Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa



Nedryerson01
Female
Taurus
42
74
Oracle
Elemental
MA Save
Magic Attack UP
Waterwalking

Papyrus Codex

Holy Miter
Light Robe
Magic Gauntlet

Poison, Life Drain, Zombie, Silence Song, Dark Holy
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Elelor
Female
Sagittarius
46
57
Knight
Steal
Mana Shield
Halve MP
Jump+3

Iron Sword
Hero Shield
Mythril Helmet
Mythril Armor
N-Kai Armlet

Shield Break, Weapon Break, Mind Break, Surging Sword
Steal Heart



Loveyouallfriends
Female
Virgo
74
43
Thief
Battle Skill
Catch
Equip Shield
Move+1

Kunai
Crystal Shield
Golden Hairpin
Judo Outfit
Magic Ring

Gil Taking, Steal Heart, Steal Shield, Steal Accessory
Shield Break, Power Break, Justice Sword, Dark Sword

Final Bets: green - 18 bets for 13,459G (58.6%, x0.71); yellow - 16 bets for 9,500G (41.4%, x1.42)

green bets:
Zeroroute: 2,257G (16.8%, 3,606G)
fenixcrest: 2,181G (16.2%, 4,363G)
AllInBot: 1,313G (9.8%, 1,313G)
Dexef: 1,256G (9.3%, 1,256G)
TheChainNerd: 1,190G (8.8%, 2,380G)
lifeguard_dan: 909G (6.8%, 909G)
Zachara: 700G (5.2%, 70,700G)
ungabunga_bot: 615G (4.6%, 207,524G)
Laserman1000: 591G (4.4%, 23,991G)
nifboy: 579G (4.3%, 11,585G)
aStatue: 500G (3.7%, 500G)
ShintaroNayaka: 400G (3.0%, 19,396G)
Justice0320: 280G (2.1%, 2,800G)
CassiePhoenix: 268G (2.0%, 268G)
PetitFoulard: 220G (1.6%, 220G)
perce90: 125G (0.9%, 125G)
tmo50x: 50G (0.4%, 4,856G)
GingerDynomite: 25G (0.2%, 6,689G)

yellow bets:
upvla: 1,234G (13.0%, 37,741G)
Baron_von_Scrub: 1,111G (11.7%, 102,844G)
ColetteMSLP: 1,000G (10.5%, 31,879G)
Draconis345: 1,000G (10.5%, 85,256G)
BirbBrainsBot: 1,000G (10.5%, 90,931G)
getthemoneyz: 984G (10.4%, 546,284G)
loveyouallfriends: 900G (9.5%, 36,924G)
DavenIII: 700G (7.4%, 5,536G)
JumbocactuarX27: 500G (5.3%, 11,583G)
Cataphract116: 250G (2.6%, 33,081G)
Andrain: 220G (2.3%, 220G)
nedryerson01: 200G (2.1%, 4,652G)
ZephyrTempest: 101G (1.1%, 165,845G)
bahumat989: 100G (1.1%, 3,463G)
twelfthrootoftwo: 100G (1.1%, 5,243G)
datadrivenbot: 100G (1.1%, 705G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ShintaroNayaka
Female
Virgo
80
75
Monk
Elemental
Mana Shield
Maintenance
Move+1



Leather Hat
Earth Clothes
Salty Rage

Spin Fist, Earth Slash, Chakra, Revive
Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Aneyus
Male
Sagittarius
65
60
Thief
Time Magic
Parry
Equip Gun
Move-MP Up

Papyrus Codex

Holy Miter
Black Costume
Leather Mantle

Steal Helmet, Steal Armor, Steal Shield, Steal Status, Arm Aim
Haste, Stop, Float



Andrain
Female
Aries
45
74
Mediator
Steal
Counter
Attack UP
Levitate

Madlemgen

Holy Miter
Light Robe
Angel Ring

Praise, Threaten, Death Sentence, Negotiate, Refute
Steal Helmet, Steal Shield, Steal Weapon



OttoRaynar
Male
Sagittarius
59
68
Samurai
Steal
Counter Tackle
Magic Defense UP
Ignore Terrain

Asura Knife

Barbuta
Bronze Armor
Germinas Boots

Bizen Boat
Arm Aim

Player: !Red
Team: Red Team
Palettes: Red/Brown



PremaritalHandHolding
Male
Cancer
58
48
Time Mage
Summon Magic
Meatbone Slash
Magic Defense UP
Move-HP Up

Oak Staff

Black Hood
Judo Outfit
Angel Ring

Slow, Demi, Demi 2, Stabilize Time
Ifrit, Cyclops



ArlanKels
Female
Gemini
46
43
Oracle
Throw
Counter Tackle
Magic Defense UP
Move-MP Up

Cypress Rod

Red Hood
Light Robe
Germinas Boots

Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Petrify
Shuriken, Dictionary



Josephiroth 143
Male
Aquarius
77
55
Wizard
Jump
Absorb Used MP
Doublehand
Lava Walking

Ice Rod

Holy Miter
Wizard Robe
Genji Gauntlet

Fire, Fire 2, Bolt 2, Bolt 3, Ice 3, Ice 4
Level Jump8, Vertical Jump6



Cho Pin
Male
Gemini
43
78
Wizard
Steal
Speed Save
Equip Polearm
Levitate

Gokuu Rod

Feather Hat
Linen Robe
N-Kai Armlet

Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice, Ice 3, Flare
Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status

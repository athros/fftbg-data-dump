Final Bets: blue - 12 bets for 6,903G (71.4%, x0.40); white - 6 bets for 2,771G (28.6%, x2.49)

blue bets:
SephDarkheart: 1,594G (23.1%, 1,594G)
BirbBrainsBot: 1,000G (14.5%, 114,461G)
Thyrandaal: 1,000G (14.5%, 238,406G)
josephiroth_143: 799G (11.6%, 799G)
sinnyil2: 619G (9.0%, 1,239G)
killth3kid: 520G (7.5%, 5,614G)
brokenknight201: 500G (7.2%, 3,703G)
getthemoneyz: 270G (3.9%, 1,405,726G)
datadrivenbot: 200G (2.9%, 41,157G)
RedRiderAyame: 200G (2.9%, 11,427G)
douchetron: 101G (1.5%, 2,723G)
DouglasDragonThePoet: 100G (1.4%, 1,863G)

white bets:
AllInBot: 1,166G (42.1%, 1,166G)
reinoe: 1,000G (36.1%, 122,766G)
roqqqpsi: 205G (7.4%, 1,373G)
Netmonmatt: 200G (7.2%, 3,726G)
LordMaxoss: 100G (3.6%, 5,641G)
dinin991: 100G (3.6%, 884G)

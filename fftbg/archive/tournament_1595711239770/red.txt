Player: !Red
Team: Red Team
Palettes: Red/Brown



Miku Shikhu
Male
Sagittarius
54
79
Mime

Counter
Long Status
Ignore Terrain



Triangle Hat
Black Costume
Magic Gauntlet

Mimic




Grininda
Male
Virgo
67
50
Squire
Steal
Meatbone Slash
Defense UP
Move+3

Morning Star
Flame Shield
Circlet
Power Sleeve
Cursed Ring

Throw Stone, Heal, Yell
Gil Taking, Steal Armor, Steal Weapon, Arm Aim



Flacococo
Monster
Virgo
67
48
Explosive










Reinoe
Female
Libra
50
49
Squire
Elemental
Caution
Equip Knife
Move+2

Kunai
Buckler
Leather Hat
Crystal Mail
Rubber Shoes

Accumulate, Dash, Throw Stone, Tickle
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

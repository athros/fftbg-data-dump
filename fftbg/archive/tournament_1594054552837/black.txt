Player: !Black
Team: Black Team
Palettes: Black/Red



Run With Stone GUNs
Female
Taurus
65
51
Oracle
Summon Magic
Speed Save
Halve MP
Jump+1

Octagon Rod

Triangle Hat
White Robe
Cursed Ring

Blind, Dispel Magic, Sleep
Shiva, Carbunkle, Salamander



NotBalrog
Monster
Gemini
68
56
Reaper










Actual JP
Male
Leo
61
76
Geomancer
Draw Out
MP Restore
Defend
Ignore Terrain

Platinum Sword
Platinum Shield
Triangle Hat
Judo Outfit
Defense Armlet

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Asura, Bizen Boat, Heaven's Cloud



Lydian C
Female
Taurus
74
40
Samurai
Yin Yang Magic
Counter Magic
Secret Hunt
Move+3

Mythril Spear

Barbuta
Bronze Armor
Jade Armlet

Koutetsu
Blind, Spell Absorb, Life Drain, Pray Faith, Foxbird, Confusion Song, Paralyze, Sleep, Petrify, Dark Holy

Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Female
Aquarius
58
49
Priest
Draw Out
Absorb Used MP
Halve MP
Ignore Height

Sage Staff

Triangle Hat
Silk Robe
Defense Armlet

Cure, Cure 3, Raise, Protect, Protect 2, Wall, Esuna
Koutetsu, Kikuichimoji



Holdenmagronik
Male
Leo
47
77
Bard
Charge
Caution
Maintenance
Move+1

Mythril Bow

Triangle Hat
Plate Mail
Rubber Shoes

Angel Song, Cheer Song, Magic Song, Diamond Blade, Space Storage, Hydra Pit
Charge+4, Charge+10, Charge+20



ScottyDeluxe
Male
Capricorn
58
72
Oracle
Time Magic
Abandon
Short Status
Jump+3

Cypress Rod

Holy Miter
Chameleon Robe
Power Wrist

Blind, Doubt Faith, Zombie, Foxbird, Dispel Magic, Sleep, Petrify
Haste, Haste 2, Slow, Slow 2, Float, Reflect



Reddwind
Male
Capricorn
54
45
Knight
Summon Magic
Dragon Spirit
Attack UP
Waterbreathing

Slasher
Gold Shield
Platinum Helmet
Diamond Armor
Diamond Armlet

Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword
Moogle, Shiva, Cyclops

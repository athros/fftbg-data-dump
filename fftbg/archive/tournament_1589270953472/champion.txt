Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Vorap
Female
Virgo
41
58
Lancer
Punch Art
Counter Tackle
Defend
Levitate

Gungnir
Buckler
Circlet
Crystal Mail
Defense Ring

Level Jump8, Vertical Jump6
Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil



TimeJannies
Male
Aquarius
62
79
Thief
Punch Art
Regenerator
Equip Axe
Ignore Height

Battle Axe

Red Hood
Secret Clothes
Reflect Ring

Steal Armor, Steal Shield, Steal Status
Wave Fist, Purification, Chakra, Revive



OneHundredFists
Female
Aries
76
75
Mediator
Basic Skill
Absorb Used MP
Dual Wield
Ignore Height

Romanda Gun
Stone Gun
Headgear
Light Robe
Defense Armlet

Persuade, Praise, Death Sentence, Refute, Rehabilitate
Dash, Throw Stone, Heal, Cheer Up



Victoriolue
Female
Taurus
46
62
Summoner
Talk Skill
Mana Shield
Short Charge
Move-HP Up

Dragon Rod

Headgear
Light Robe
Vanish Mantle

Moogle, Ramuh, Golem, Carbunkle, Bahamut, Odin, Leviathan, Lich, Cyclops
Invitation, Persuade, Negotiate, Refute, Rehabilitate

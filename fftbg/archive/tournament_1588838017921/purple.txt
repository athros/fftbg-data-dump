Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaplessOne
Female
Aquarius
54
67
Wizard
Draw Out
Counter Tackle
Magic Attack UP
Fly

Dragon Rod

Feather Hat
Black Costume
Defense Ring

Fire 4, Bolt 3, Ice 2
Koutetsu, Heaven's Cloud



UmaiJam
Male
Scorpio
50
52
Archer
Yin Yang Magic
Counter Flood
Doublehand
Jump+1

Glacier Gun

Flash Hat
Wizard Outfit
Magic Ring

Charge+1, Charge+4, Charge+10
Blind, Poison, Doubt Faith, Blind Rage, Dark Holy



Twelfthrootoftwo
Female
Scorpio
66
40
Calculator
White Magic
Damage Split
Magic Defense UP
Move+2

Poison Rod

Golden Hairpin
Light Robe
Setiemson

CT, Height, Prime Number, 5, 4, 3
Cure 3, Raise, Shell 2, Wall, Esuna



Solomongrundy85
Male
Gemini
68
75
Ninja
Draw Out
Counter Magic
Attack UP
Fly

Short Edge
Blind Knife
Triangle Hat
Leather Outfit
Jade Armlet

Knife, Sword
Asura, Heaven's Cloud

Player: !Black
Team: Black Team
Palettes: Black/Red



Bryon W
Male
Leo
41
39
Ninja
Steal
Counter Flood
Attack UP
Retreat

Short Edge
Spell Edge
Thief Hat
Black Costume
Spike Shoes

Shuriken, Knife
Steal Helmet, Steal Armor, Steal Accessory, Leg Aim



Liquidbob
Male
Gemini
62
41
Mediator
Sing
Faith Up
Maintenance
Waterbreathing

Main Gauche

Green Beret
Earth Clothes
Leather Mantle

Invitation, Threaten, Solution, Mimic Daravon, Refute, Rehabilitate
Life Song, Cheer Song, Magic Song, Last Song, Sky Demon



Da Aa Ve
Female
Gemini
70
57
Calculator
Black Magic
Regenerator
Equip Gun
Move+3

Fairy Harp

Thief Hat
Adaman Vest
Cursed Ring

Height, Prime Number, 4, 3
Fire, Fire 4, Bolt 3, Ice 2, Ice 3, Ice 4, Death, Flare



Krexenn
Male
Aries
46
75
Knight
Draw Out
Absorb Used MP
Long Status
Lava Walking

Ragnarok

Platinum Helmet
Chain Mail
Sprint Shoes

Head Break, Shield Break, Magic Break, Speed Break, Power Break, Dark Sword
Koutetsu, Murasame, Heaven's Cloud, Muramasa

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Yayifications
Female
Leo
44
70
Geomancer
Draw Out
Regenerator
Attack UP
Retreat

Mythril Sword
Diamond Shield
Flash Hat
Wizard Robe
Bracer

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Bizen Boat, Kikuichimoji



Kahlindra
Female
Gemini
41
74
Dancer
Talk Skill
Meatbone Slash
Equip Bow
Move+2

Poison Bow

Flash Hat
Clothes
Power Wrist

Witch Hunt, Last Dance, Nether Demon
Invitation, Solution, Negotiate, Mimic Daravon



MoonSlayerRS
Monster
Sagittarius
63
43
Black Chocobo










WhiteTigress
Male
Aries
74
61
Lancer
Time Magic
Damage Split
Doublehand
Move+3

Mythril Spear

Iron Helmet
Chain Mail
Dracula Mantle

Level Jump8, Vertical Jump5
Haste, Immobilize, Reflect, Demi 2, Stabilize Time, Galaxy Stop

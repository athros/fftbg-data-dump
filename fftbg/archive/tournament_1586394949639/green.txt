Player: !Green
Team: Green Team
Palettes: Green/White



Theseawolf1
Female
Aquarius
56
57
Time Mage
Draw Out
Faith Up
Doublehand
Move+2

Papyrus Codex

Feather Hat
Silk Robe
Genji Gauntlet

Haste, Slow, Slow 2, Stop, Demi 2, Stabilize Time
Bizen Boat, Murasame, Kiyomori, Muramasa



ApplesauceBoss
Female
Leo
48
64
Dancer
Time Magic
Parry
Dual Wield
Move+1

Ryozan Silk
Cashmere
Red Hood
Mythril Vest
Diamond Armlet

Polka Polka, Void Storage
Haste, Haste 2, Slow, Immobilize, Reflect, Demi 2



Jeeboheebo
Male
Virgo
58
70
Bard
Battle Skill
Damage Split
Dual Wield
Lava Walking

Fairy Harp
Fairy Harp
Holy Miter
Platinum Armor
Small Mantle

Cheer Song, Battle Song, Magic Song, Last Song, Sky Demon
Shield Break, Power Break, Justice Sword, Explosion Sword



Baron Von Scrub
Male
Scorpio
50
45
Monk
Talk Skill
Brave Up
Equip Sword
Move+2

Kiyomori

Headgear
Black Costume
Dracula Mantle

Spin Fist, Wave Fist, Purification, Revive
Persuade, Praise, Insult, Refute

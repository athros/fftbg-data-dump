Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ko2q
Male
Scorpio
72
48
Knight
Summon Magic
Absorb Used MP
Defense UP
Waterbreathing

Platinum Sword
Platinum Shield
Mythril Helmet
Crystal Mail
Feather Boots

Weapon Break, Stasis Sword, Explosion Sword
Titan, Carbunkle, Silf



Blain Cooper
Female
Capricorn
70
39
Wizard
Summon Magic
Counter
Short Charge
Move+2

Rod

Headgear
Chameleon Robe
Rubber Shoes

Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower
Carbunkle, Leviathan



Kai Shee
Male
Libra
37
55
Monk
Draw Out
Hamedo
Long Status
Teleport



Flash Hat
Power Sleeve
Feather Mantle

Spin Fist, Purification, Chakra, Revive
Asura, Koutetsu, Kiyomori, Muramasa



Waterwatereverywhere
Female
Sagittarius
54
56
Time Mage
Black Magic
Faith Up
Martial Arts
Swim



Black Hood
Light Robe
Magic Gauntlet

Slow, Slow 2, Immobilize, Reflect, Quick
Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Death

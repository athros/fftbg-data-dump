Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ALY327
Female
Libra
55
44
Chemist
Punch Art
Catch
Concentrate
Waterwalking

Mythril Gun

Black Hood
Power Sleeve
Magic Gauntlet

Potion, X-Potion, Antidote, Phoenix Down
Wave Fist, Earth Slash, Secret Fist, Purification



Bruubarg
Male
Libra
61
80
Calculator
White Magic
Distribute
Magic Defense UP
Move+3

Iron Fan

Golden Hairpin
Judo Outfit
Genji Gauntlet

CT, Height, Prime Number, 4, 3
Cure 2, Raise, Reraise, Protect, Protect 2, Esuna



JethroThrul
Male
Capricorn
44
45
Archer
Steal
Critical Quick
Defense UP
Move-MP Up

Ultimus Bow

Green Beret
Adaman Vest
Power Wrist

Charge+5, Charge+7
Steal Armor, Steal Accessory, Leg Aim



D4rr1n
Male
Leo
50
68
Samurai
Punch Art
Damage Split
Doublehand
Jump+2

Javelin

Crystal Helmet
Light Robe
Spike Shoes

Heaven's Cloud, Kiyomori, Muramasa
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive

Player: !White
Team: White Team
Palettes: White/Blue



Prince Rogers Nelson
Monster
Aquarius
40
74
Serpentarius










Bulleta
Female
Cancer
64
52
Mime

Distribute
Long Status
Move-HP Up



Green Beret
Judo Outfit
Dracula Mantle

Mimic




Nekojin
Female
Gemini
71
76
Squire
Steal
Arrow Guard
Magic Attack UP
Swim

Cross Bow
Escutcheon
Diamond Helmet
Brigandine
Reflect Ring

Heal, Cheer Up, Fury, Wish, Scream
Steal Heart, Steal Weapon, Leg Aim



Victoriolue
Female
Pisces
59
46
Samurai
Charge
Counter Magic
Maintenance
Fly

Asura Knife

Crystal Helmet
Platinum Armor
Magic Ring

Koutetsu, Kikuichimoji
Charge+1, Charge+3, Charge+5, Charge+20

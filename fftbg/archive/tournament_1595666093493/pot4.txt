Final Bets: purple - 5 bets for 4,822G (49.0%, x1.04); brown - 12 bets for 5,012G (51.0%, x0.96)

purple bets:
NovaKnight21: 2,000G (41.5%, 8,798G)
prince_rogers_nelson_: 1,054G (21.9%, 1,054G)
getthemoneyz: 902G (18.7%, 1,386,245G)
pandasforsale: 666G (13.8%, 15,438G)
poGpopE: 200G (4.1%, 4,598G)

brown bets:
UmaiJam: 1,700G (33.9%, 70,304G)
douchetron: 843G (16.8%, 1,654G)
superdevon1: 723G (14.4%, 36,199G)
DeathTaxesAndAnime: 400G (8.0%, 2,524G)
Hirameki85: 265G (5.3%, 2,765G)
resjudicata3: 212G (4.2%, 212G)
AllInBot: 200G (4.0%, 200G)
dtrain332: 200G (4.0%, 3,202G)
datadrivenbot: 200G (4.0%, 40,985G)
renkei_fukai: 100G (2.0%, 100G)
AugnosMusic: 100G (2.0%, 100G)
BirbBrainsBot: 69G (1.4%, 66,049G)

Final Bets: purple - 9 bets for 8,874G (76.9%, x0.30); brown - 5 bets for 2,662G (23.1%, x3.33)

purple bets:
Mesmaster: 2,063G (23.2%, 2,063G)
bruubarg: 2,008G (22.6%, 2,008G)
ko2q: 1,373G (15.5%, 1,373G)
gorgewall: 1,261G (14.2%, 1,261G)
superdevon1: 713G (8.0%, 71,357G)
reddwind_: 580G (6.5%, 18,796G)
Lanshaft: 576G (6.5%, 14,766G)
datadrivenbot: 200G (2.3%, 58,953G)
MinBetBot: 100G (1.1%, 21,333G)

brown bets:
BirbBrainsBot: 1,000G (37.6%, 49,953G)
Sairentozon7: 1,000G (37.6%, 94,526G)
getthemoneyz: 296G (11.1%, 1,615,144G)
AllInBot: 266G (10.0%, 266G)
peeronid: 100G (3.8%, 9,556G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jathalord
Female
Pisces
69
68
Monk
Draw Out
Distribute
Defense UP
Ignore Height



Triangle Hat
Brigandine
Cherche

Wave Fist, Secret Fist, Purification, Chakra
Bizen Boat, Heaven's Cloud, Kiyomori



Dynasti
Male
Aries
77
41
Monk
Battle Skill
Caution
Dual Wield
Jump+3



Black Hood
Power Sleeve
Power Wrist

Spin Fist, Pummel, Wave Fist, Purification, Seal Evil
Power Break, Mind Break, Stasis Sword, Surging Sword



Coyowijuju
Female
Pisces
68
60
Chemist
Steal
Parry
Short Charge
Ignore Height

Hydra Bag

Golden Hairpin
Earth Clothes
108 Gems

Potion, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Steal Helmet, Steal Armor, Steal Shield, Steal Status, Arm Aim



Hyvi
Male
Cancer
58
65
Bard
Talk Skill
Parry
Dual Wield
Swim

Fairy Harp
Ramia Harp
Black Hood
Clothes
Bracer

Angel Song, Cheer Song, Battle Song, Hydra Pit
Invitation, Persuade, Preach, Negotiate

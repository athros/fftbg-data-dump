Player: !Red
Team: Red Team
Palettes: Red/Brown



Brokenknight201
Female
Aries
41
72
Knight
Black Magic
Meatbone Slash
Dual Wield
Move+3

Blood Sword
Rune Blade
Genji Helmet
Genji Armor
Germinas Boots

Armor Break, Shield Break, Magic Break, Mind Break, Justice Sword
Fire 3, Ice 2, Ice 3, Ice 4, Empower, Frog



VolgraTheMoose
Female
Pisces
44
56
Samurai
Talk Skill
Arrow Guard
Equip Armor
Teleport 2

Obelisk

Red Hood
Mythril Vest
Bracer

Asura, Kikuichimoji
Preach, Death Sentence, Refute, Rehabilitate



CapnChaos12
Male
Capricorn
52
79
Chemist
Time Magic
Dragon Spirit
Equip Gun
Ignore Height

Fairy Harp

Black Hood
Brigandine
Dracula Mantle

X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down
Haste, Slow 2, Float



E Ballard
Male
Libra
74
78
Mime

Counter
Dual Wield
Jump+2



Mythril Helmet
Adaman Vest
Feather Boots

Mimic


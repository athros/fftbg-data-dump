Player: !Black
Team: Black Team
Palettes: Black/Red



YaBoy125
Male
Scorpio
71
74
Knight
Summon Magic
MA Save
Equip Sword
Move+2

Sleep Sword
Kaiser Plate
Gold Helmet
Crystal Mail
Bracer

Armor Break, Shield Break, Weapon Break, Speed Break, Mind Break
Moogle, Shiva, Ramuh, Ifrit, Titan, Odin, Leviathan, Silf, Fairy, Lich



Mpghappiness
Male
Pisces
79
49
Lancer
Charge
MA Save
Doublehand
Fly

Partisan

Circlet
White Robe
Cursed Ring

Level Jump8, Vertical Jump6
Charge+2, Charge+7



VolgraTheMoose
Male
Sagittarius
59
39
Monk
Basic Skill
Critical Quick
Halve MP
Move+3



Headgear
Wizard Outfit
Elf Mantle

Wave Fist, Secret Fist, Chakra, Revive, Seal Evil
Accumulate, Heal, Scream



Gooseyourself
Male
Virgo
47
50
Archer
Talk Skill
Parry
Doublehand
Ignore Height

Mythril Bow

Green Beret
Mystic Vest
Wizard Mantle

Charge+3, Charge+5, Charge+7
Praise, Death Sentence, Insult, Negotiate, Refute

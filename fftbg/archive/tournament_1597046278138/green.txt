Player: !Green
Team: Green Team
Palettes: Green/White



Nok
Male
Leo
60
64
Wizard
Time Magic
PA Save
Defend
Fly

Air Knife

Flash Hat
Robe of Lords
Reflect Ring

Fire, Fire 2, Fire 3, Ice, Empower, Flare
Haste 2, Slow, Reflect, Quick, Demi, Stabilize Time, Meteor



Mike The Mike
Monster
Cancer
48
38
Skeleton










Strumisgod
Male
Scorpio
53
78
Priest
Steal
Abandon
Equip Sword
Swim

Platinum Sword

Feather Hat
Linen Robe
Small Mantle

Cure, Cure 4, Raise 2, Protect, Esuna
Steal Helmet, Steal Armor, Steal Shield, Steal Weapon



Alastair
Male
Virgo
54
62
Mime

PA Save
Dual Wield
Fly



Green Beret
Adaman Vest
Elf Mantle

Mimic


Player: !Black
Team: Black Team
Palettes: Black/Red



PotionDweller
Female
Virgo
76
57
Ninja
Draw Out
HP Restore
Equip Bow
Teleport

Ultimus Bow

Triangle Hat
Adaman Vest
Reflect Ring

Bomb
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



Midori Ribbon
Female
Libra
57
74
Lancer
Steal
Auto Potion
Martial Arts
Move-HP Up


Hero Shield
Gold Helmet
Crystal Mail
Magic Gauntlet

Level Jump3, Vertical Jump8
Steal Helmet, Steal Weapon, Steal Status



R Raynos
Male
Libra
55
54
Ninja
Sing
Speed Save
Halve MP
Move+1

Short Edge
Mythril Knife
Feather Hat
Earth Clothes
Genji Gauntlet

Shuriken, Knife
Cheer Song, Battle Song, Magic Song, Nameless Song, Last Song



Kyune
Female
Gemini
68
54
Oracle
White Magic
HP Restore
Dual Wield
Waterbreathing

Ivory Rod
Iron Fan
Headgear
Chain Vest
Feather Mantle

Poison, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Shell, Shell 2, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



NovaKnight21
Female
Gemini
69
73
Samurai
Jump
Brave Up
Equip Polearm
Jump+1

Gokuu Rod

Cross Helmet
Linen Cuirass
108 Gems

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Level Jump3, Vertical Jump8



Lythe Caraker
Monster
Scorpio
58
47
Dryad










CassiePhoenix
Male
Cancer
58
80
Knight
Elemental
Catch
Secret Hunt
Move+1

Long Sword
Ice Shield
Mythril Helmet
Silk Robe
Defense Armlet

Shield Break, Mind Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Sairentozon7
Female
Taurus
79
78
Thief
Black Magic
Counter Flood
Short Status
Ignore Height

Spell Edge

Black Hood
Secret Clothes
Rubber Shoes

Gil Taking, Steal Heart, Steal Weapon, Arm Aim, Leg Aim
Fire 2, Fire 3, Bolt 2, Ice 3, Flare

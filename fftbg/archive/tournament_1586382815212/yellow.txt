Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lanshaft
Female
Aries
72
73
Archer
Dance
Sunken State
Defend
Move-MP Up

Hunting Bow
Aegis Shield
Feather Hat
Wizard Outfit
Genji Gauntlet

Charge+1, Charge+2, Charge+7, Charge+10
Witch Hunt, Wiznaibus, Slow Dance, Dragon Pit



Red Lancer
Male
Libra
69
79
Samurai
Steal
Speed Save
Equip Knife
Move+3

Faith Rod

Bronze Helmet
Leather Armor
Feather Mantle

Murasame, Kiyomori
Steal Helmet, Steal Accessory, Steal Status, Arm Aim



Escorian
Female
Pisces
57
65
Geomancer
White Magic
Counter Tackle
Dual Wield
Move+3

Giant Axe
Materia Blade
Headgear
Power Sleeve
Germinas Boots

Water Ball, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 4, Raise 2, Regen, Wall, Esuna, Magic Barrier



IndecisiveNinja
Female
Aquarius
38
56
Samurai
Battle Skill
Critical Quick
Concentrate
Jump+2

Koutetsu Knife

Iron Helmet
Reflect Mail
Setiemson

Koutetsu, Heaven's Cloud, Muramasa
Magic Break, Speed Break, Power Break, Justice Sword, Night Sword

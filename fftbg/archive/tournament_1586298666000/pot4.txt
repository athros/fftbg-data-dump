Final Bets: purple - 21 bets for 19,892G (65.9%, x0.52); brown - 26 bets for 10,313G (34.1%, x1.93)

purple bets:
DeathTaxesAndAnime: 3,621G (18.2%, 7,100G)
Bryon_W: 3,000G (15.1%, 28,021G)
DudeMonkey77: 2,338G (11.8%, 2,338G)
BenYuPoker: 2,168G (10.9%, 2,168G)
Aldrammech: 1,237G (6.2%, 12,372G)
Jeeboheebo: 1,002G (5.0%, 3,601G)
JackOnFire1: 1,000G (5.0%, 1,000G)
I_nod_my_head_when_I_lose: 1,000G (5.0%, 18,280G)
jethrothrul: 850G (4.3%, 850G)
Maeveen: 814G (4.1%, 1,629G)
upvla: 632G (3.2%, 29,351G)
Laserman1000: 500G (2.5%, 11,500G)
MoonSlayerRS: 408G (2.1%, 408G)
Cryptopsy70: 356G (1.8%, 7,002G)
Draconis345: 332G (1.7%, 332G)
HeroponThrawn: 229G (1.2%, 229G)
AllInBot: 100G (0.5%, 100G)
carchan131: 100G (0.5%, 6,370G)
maakur_: 100G (0.5%, 3,643G)
CapnChaos12: 100G (0.5%, 3,879G)
ko2q: 5G (0.0%, 1,955G)

brown bets:
Scotobot: 1,000G (9.7%, 5,643G)
SeedSC: 1,000G (9.7%, 128,576G)
HaplessOne: 1,000G (9.7%, 111,316G)
Nizaha: 600G (5.8%, 600G)
Lanshaft: 600G (5.8%, 8,773G)
HASTERIOUS: 532G (5.2%, 532G)
Pie108: 500G (4.8%, 10,843G)
martymcfly2021: 439G (4.3%, 439G)
BirbBrainsBot: 439G (4.3%, 268,304G)
DustBirdEX: 400G (3.9%, 4,306G)
Baron_von_Scrub: 353G (3.4%, 7,241G)
OmnibotGamma: 352G (3.4%, 352G)
TheManInPlaid: 336G (3.3%, 336G)
Shalloween: 332G (3.2%, 332G)
luminarii: 306G (3.0%, 12,290G)
getthemoneyz: 284G (2.8%, 444,250G)
moonliquor: 283G (2.7%, 283G)
Slowbrofist: 272G (2.6%, 272G)
ungabunga_bot: 240G (2.3%, 67,913G)
MaouDono: 228G (2.2%, 228G)
ZephyrTempest: 203G (2.0%, 24,791G)
Aka_Gilly: 200G (1.9%, 10,686G)
Evewho: 200G (1.9%, 5,723G)
Lodrak: 100G (1.0%, 2,480G)
datadrivenbot: 100G (1.0%, 25,047G)
DrAntiSocial: 14G (0.1%, 1,238G)

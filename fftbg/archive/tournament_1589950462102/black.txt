Player: !Black
Team: Black Team
Palettes: Black/Red



Kaidykat
Monster
Leo
67
42
Ghoul










TheJonTerp
Male
Aquarius
53
78
Ninja
Talk Skill
HP Restore
Equip Shield
Jump+1

Spell Edge
Diamond Shield
Flash Hat
Mythril Vest
N-Kai Armlet

Shuriken, Knife
Persuade, Solution, Insult, Mimic Daravon, Refute



Pandasforsale
Male
Virgo
57
79
Samurai
Elemental
Faith Save
Concentrate
Jump+3

Heaven's Cloud

Crystal Helmet
Leather Armor
Defense Armlet

Heaven's Cloud, Muramasa, Kikuichimoji
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Dymntd
Female
Capricorn
81
78
Samurai
Time Magic
Abandon
Magic Attack UP
Jump+1

Murasame

Bronze Helmet
Mythril Armor
Genji Gauntlet

Bizen Boat, Heaven's Cloud, Kikuichimoji
Demi 2, Stabilize Time

Player: !White
Team: White Team
Palettes: White/Blue



ZeracheilGames
Female
Gemini
65
51
Monk
Summon Magic
Caution
Secret Hunt
Waterwalking



Flash Hat
Judo Outfit
Defense Ring

Pummel, Wave Fist, Revive, Seal Evil
Moogle, Ramuh, Ifrit, Titan, Golem, Odin, Salamander, Lich, Cyclops



Gabbagooluigi
Female
Libra
76
61
Thief
Draw Out
Auto Potion
Concentrate
Teleport

Orichalcum

Twist Headband
Chain Vest
Feather Boots

Steal Helmet, Steal Armor, Leg Aim
Asura, Koutetsu



ShinoGuy
Male
Libra
56
69
Samurai
Summon Magic
Catch
Magic Attack UP
Jump+1

Asura Knife

Gold Helmet
Wizard Robe
Angel Ring

Murasame, Kiyomori, Muramasa
Moogle, Titan, Odin, Salamander



Haaaaaank
Female
Virgo
64
68
Chemist
Charge
Counter Magic
Equip Armor
Retreat

Panther Bag

Feather Hat
Light Robe
Dracula Mantle

Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down
Charge+5

Player: !Green
Team: Green Team
Palettes: Green/White



Nizaha
Female
Serpentarius
37
62
Ninja
Jump
Counter
Halve MP
Move+1

Air Knife
Orichalcum
Headgear
Earth Clothes
Reflect Ring

Staff, Dictionary
Level Jump8, Vertical Jump8



Oreo Pizza
Male
Gemini
57
68
Archer
Sing
Speed Save
Short Status
Move+2

Romanda Gun
Genji Shield
Red Hood
Mythril Vest
108 Gems

Charge+5, Charge+10, Charge+20
Battle Song, Magic Song, Nameless Song, Last Song



Galkife
Male
Virgo
81
75
Samurai
Basic Skill
Counter Magic
Defend
Teleport

Kikuichimoji

Platinum Helmet
Crystal Mail
Defense Ring

Heaven's Cloud, Muramasa
Heal, Tickle, Yell, Cheer Up, Wish



Chef Devbot
Male
Aries
77
43
Priest
Jump
Regenerator
Short Charge
Waterwalking

White Staff

Red Hood
Black Costume
Spike Shoes

Cure, Cure 2, Raise, Protect, Esuna, Holy
Level Jump8, Vertical Jump8

Player: !White
Team: White Team
Palettes: White/Blue



Mesmaster
Female
Libra
76
54
Samurai
Talk Skill
Sunken State
Defend
Teleport

Obelisk

Gold Helmet
Chameleon Robe
Chantage

Asura, Koutetsu, Bizen Boat, Muramasa
Invitation, Preach, Solution, Death Sentence, Insult, Mimic Daravon



Lowlf
Female
Taurus
69
45
Thief
Throw
Counter Flood
Equip Axe
Waterbreathing

White Staff

Black Hood
Black Costume
Dracula Mantle

Gil Taking, Steal Armor, Steal Accessory
Knife



DAC169
Female
Capricorn
73
42
Dancer
Talk Skill
Counter
Short Charge
Swim

Persia

Thief Hat
Adaman Vest
Power Wrist

Wiznaibus, Slow Dance, Disillusion, Nether Demon, Dragon Pit
Threaten, Preach, Negotiate, Mimic Daravon, Rehabilitate



Flacococo
Male
Libra
56
62
Archer
Talk Skill
Counter
Halve MP
Ignore Height

Silver Bow

Green Beret
Mystic Vest
Diamond Armlet

Charge+1, Charge+2
Invitation, Persuade, Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute

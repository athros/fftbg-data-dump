Player: !Brown
Team: Brown Team
Palettes: Brown/Green



J2DaBibbles
Male
Cancer
60
58
Mediator
Draw Out
Critical Quick
Halve MP
Ignore Height

Blast Gun

Triangle Hat
Wizard Robe
Feather Mantle

Invitation, Threaten, Negotiate, Mimic Daravon, Refute, Rehabilitate
Bizen Boat, Murasame, Heaven's Cloud



Laserman1000
Male
Capricorn
73
53
Calculator
Limit
Caution
Magic Defense UP
Jump+1

Papyrus Codex
Ice Shield
Cross Helmet
Adaman Vest
Magic Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



RunicMagus
Female
Capricorn
55
80
Mime

Damage Split
Martial Arts
Lava Walking



Feather Hat
Light Robe
Wizard Mantle

Mimic




Caster Daily
Male
Scorpio
61
63
Summoner
Black Magic
Damage Split
Magic Defense UP
Ignore Terrain

White Staff

Headgear
Earth Clothes
Germinas Boots

Moogle, Golem, Carbunkle, Bahamut, Odin
Fire, Bolt, Ice, Ice 3, Frog

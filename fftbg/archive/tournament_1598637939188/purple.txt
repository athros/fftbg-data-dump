Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



MemoriesofFinal
Male
Gemini
45
74
Wizard
Talk Skill
Parry
Equip Polearm
Teleport

Battle Bamboo

Headgear
Linen Robe
N-Kai Armlet

Fire, Fire 2, Bolt 3, Bolt 4, Empower, Flare
Praise, Insult, Negotiate, Mimic Daravon



Laserman1000
Male
Aries
52
48
Ninja
White Magic
Faith Save
Sicken
Move+2

Scorpion Tail
Kunai
Thief Hat
Wizard Outfit
Battle Boots

Shuriken, Knife
Regen, Protect, Shell 2, Wall, Esuna



Vorap
Male
Scorpio
48
64
Summoner
Jump
Catch
Long Status
Waterbreathing

Wizard Rod

Holy Miter
Brigandine
Sprint Shoes

Ifrit, Titan, Carbunkle, Fairy
Level Jump3, Vertical Jump3



DLJuggernaut
Male
Scorpio
53
74
Bard
Time Magic
Dragon Spirit
Sicken
Move+2

Fairy Harp

Holy Miter
Power Sleeve
Elf Mantle

Angel Song, Cheer Song, Battle Song, Magic Song
Haste, Slow, Float, Quick, Demi 2

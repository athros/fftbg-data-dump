Player: !zChamp
Team: Champion Team
Palettes: White/Blue



ZCKaiser
Male
Leo
68
39
Ninja
Yin Yang Magic
Faith Save
Magic Attack UP
Jump+3

Kunai
Koga Knife
Twist Headband
Mystic Vest
Jade Armlet

Bomb
Blind, Life Drain, Doubt Faith, Silence Song, Paralyze, Sleep, Dark Holy



Silentkaster
Monster
Gemini
52
73
Ultima Demon










JonnyCue
Female
Gemini
54
48
Ninja
Time Magic
Dragon Spirit
Attack UP
Move+1

Sasuke Knife
Kunai
Thief Hat
Secret Clothes
Magic Gauntlet

Bomb, Axe
Demi, Stabilize Time, Galaxy Stop



Latebit
Male
Leo
54
72
Monk
Throw
MA Save
Magic Attack UP
Waterbreathing



Leather Hat
Brigandine
Power Wrist

Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Sword

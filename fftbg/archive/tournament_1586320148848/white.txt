Player: !White
Team: White Team
Palettes: White/Blue



Gooseyourself
Female
Cancer
58
49
Summoner
Steal
Counter Flood
Equip Shield
Move+2

Rainbow Staff
Gold Shield
Triangle Hat
Rubber Costume
Feather Boots

Shiva, Ifrit, Odin, Leviathan, Silf
Gil Taking, Steal Helmet, Steal Shield, Arm Aim



Alacor
Male
Leo
76
75
Knight
Steal
Counter Tackle
Short Charge
Waterbreathing

Ragnarok
Gold Shield
Bronze Helmet
Genji Armor
N-Kai Armlet

Head Break, Speed Break, Mind Break, Night Sword
Steal Armor, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



Pah J
Female
Aries
70
64
Samurai
Charge
Counter Magic
Doublehand
Teleport

Kiyomori

Mythril Helmet
Light Robe
Dracula Mantle

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Charge+3, Charge+7, Charge+20



Humble Fabio
Male
Aries
51
69
Archer
Jump
Caution
Equip Gun
Move+2

Bestiary
Round Shield
Platinum Helmet
Mystic Vest
108 Gems

Charge+1
Level Jump8, Vertical Jump7

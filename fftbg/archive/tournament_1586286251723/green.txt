Player: !Green
Team: Green Team
Palettes: Green/White



MalakoFox
Female
Scorpio
80
51
Monk
Dance
Sunken State
Equip Armor
Jump+2



Genji Helmet
Black Robe
Feather Mantle

Spin Fist, Pummel, Earth Slash, Purification, Revive
Witch Hunt, Disillusion, Nameless Dance, Last Dance



RughSontos
Male
Taurus
48
55
Knight
Charge
Counter Magic
Short Charge
Jump+3

Long Sword
Bronze Shield
Iron Helmet
Plate Mail
Setiemson

Power Break
Charge+1, Charge+2, Charge+5, Charge+7



ShintaroNayaka
Female
Scorpio
61
39
Wizard
Draw Out
Faith Up
Magic Attack UP
Jump+3

Orichalcum

Golden Hairpin
Silk Robe
Jade Armlet

Fire, Fire 2, Ice 2, Ice 4, Empower
Bizen Boat, Heaven's Cloud, Muramasa



ANFz
Male
Scorpio
48
51
Chemist
Time Magic
Auto Potion
Martial Arts
Waterwalking



Holy Miter
Mythril Vest
Diamond Armlet

Ether, Phoenix Down
Quick, Stabilize Time

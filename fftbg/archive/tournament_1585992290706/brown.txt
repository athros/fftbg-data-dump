Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bad1dea
Male
Libra
69
72
Geomancer
Steal
Regenerator
Sicken
Levitate

Rune Blade
Genji Shield
Headgear
Wizard Robe
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind
Steal Armor, Steal Weapon



Sinnyil2
Male
Serpentarius
49
57
Thief
Throw
Counter
Magic Defense UP
Move-HP Up

Mythril Knife

Thief Hat
Judo Outfit
Feather Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Status, Arm Aim
Shuriken, Sword, Staff, Spear



Hzor
Male
Capricorn
46
49
Calculator
Bio
Meatbone Slash
Equip Polearm
Lava Walking

Cypress Rod

Barette
Black Robe
Sprint Shoes

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Alc Trinity
Female
Aries
73
56
Oracle
Throw
Critical Quick
Magic Attack UP
Waterbreathing

Gokuu Rod

Headgear
Light Robe
Feather Mantle

Blind, Spell Absorb, Zombie, Confusion Song, Dispel Magic
Shuriken

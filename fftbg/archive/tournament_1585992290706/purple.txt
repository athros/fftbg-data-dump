Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Hyvi
Male
Capricorn
78
76
Archer
Talk Skill
Regenerator
Dual Wield
Teleport

Mythril Bow

Holy Miter
Black Costume
Magic Ring

Charge+4
Invitation, Threaten, Refute



BenYuPoker
Male
Scorpio
64
60
Chemist
White Magic
Counter
Magic Attack UP
Ignore Height

Star Bag

Green Beret
Earth Clothes
Sprint Shoes

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Remedy
Cure 4, Regen, Protect, Protect 2, Shell 2, Esuna



Go2sleepTV
Male
Capricorn
68
74
Samurai
Throw
Earplug
Long Status
Move+1

Mythril Spear

Barbuta
Chameleon Robe
Magic Gauntlet

Asura, Koutetsu, Bizen Boat
Dictionary



Herrshaun
Male
Sagittarius
80
80
Ninja
Elemental
Absorb Used MP
Equip Gun
Teleport

Romanda Gun
Glacier Gun
Triangle Hat
Leather Outfit
Magic Ring

Shuriken, Bomb, Knife, Spear
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LDSkinny
Female
Libra
58
53
Oracle
Charge
Auto Potion
Concentrate
Move-MP Up

Battle Folio

Feather Hat
Light Robe
Small Mantle

Life Drain, Blind Rage, Paralyze, Petrify, Dark Holy
Charge+1, Charge+4, Charge+10, Charge+20



Baconbacon1207
Male
Leo
69
65
Samurai
Basic Skill
Caution
Attack UP
Jump+3

Heaven's Cloud

Platinum Helmet
Gold Armor
Jade Armlet

Koutetsu, Bizen Boat, Heaven's Cloud
Accumulate, Dash, Tickle, Yell, Wish



ThePineappleSalesman
Female
Scorpio
71
77
Lancer
Talk Skill
Earplug
Equip Polearm
Move+2

Cashmere
Diamond Shield
Platinum Helmet
Reflect Mail
Spike Shoes

Level Jump8, Vertical Jump6
Persuade, Mimic Daravon, Refute



Reltz
Female
Aquarius
54
60
Thief
Black Magic
Counter Magic
Concentrate
Lava Walking

Broad Sword

Holy Miter
Black Costume
Wizard Mantle

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Status
Fire 2, Bolt 2, Ice 4, Frog

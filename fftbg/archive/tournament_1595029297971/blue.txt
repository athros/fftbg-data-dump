Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaateXIII
Monster
Aquarius
56
75
Cockatrice










TasisSai
Male
Cancer
55
43
Wizard
Time Magic
MA Save
Martial Arts
Jump+1



Twist Headband
Chameleon Robe
Elf Mantle

Fire 3, Ice 4, Frog, Flare
Slow, Stop, Demi 2, Meteor



Actual JP
Female
Taurus
68
59
Knight
Yin Yang Magic
Caution
Halve MP
Ignore Height

Defender
Diamond Shield
Cross Helmet
Carabini Mail
Germinas Boots

Shield Break, Speed Break, Stasis Sword, Surging Sword, Explosion Sword
Blind, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Sleep



Roqqqpsi
Female
Libra
50
51
Mime

Brave Save
Magic Attack UP
Fly


Venetian Shield
Thief Hat
Secret Clothes
Wizard Mantle

Mimic


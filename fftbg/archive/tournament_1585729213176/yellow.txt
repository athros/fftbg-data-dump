Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Icy Blue20
Male
Aquarius
68
74
Samurai
Charge
Absorb Used MP
Maintenance
Move+3

Asura Knife

Diamond Helmet
Plate Mail
Angel Ring

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Charge+4, Charge+5, Charge+7, Charge+20



Phytik
Male
Aries
63
52
Archer
Elemental
Arrow Guard
Magic Defense UP
Levitate

Blast Gun
Crystal Shield
Red Hood
Brigandine
Rubber Shoes

Charge+5
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



Jeeboheebo
Male
Libra
79
59
Geomancer
Draw Out
Damage Split
Equip Bow
Jump+3

Mythril Bow

Thief Hat
Chain Vest
Rubber Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Muramasa



YaBoy125
Male
Aquarius
49
63
Knight
Steal
Auto Potion
Dual Wield
Jump+1

Mythril Sword
Blood Sword
Circlet
Diamond Armor
Germinas Boots

Head Break, Armor Break, Shield Break, Night Sword
Steal Armor

Player: !Red
Team: Red Team
Palettes: Red/Brown



Douchetron
Male
Taurus
71
49
Archer
Time Magic
Abandon
Attack UP
Move-MP Up

Bow Gun
Gold Shield
Leather Hat
Power Sleeve
Elf Mantle

Charge+2, Charge+3, Charge+4, Charge+7
Haste, Haste 2, Slow 2, Reflect, Quick, Demi, Demi 2, Stabilize Time, Galaxy Stop



Nifboy
Female
Aquarius
59
50
Wizard
Draw Out
Dragon Spirit
Beastmaster
Swim

Rod

Headgear
Earth Clothes
Cherche

Fire, Bolt 2, Ice 2
Koutetsu, Bizen Boat, Heaven's Cloud



Drusiform
Female
Virgo
58
72
Dancer
Throw
Regenerator
Equip Armor
Fly

Ryozan Silk

Crystal Helmet
Genji Armor
Rubber Shoes

Wiznaibus, Nameless Dance, Last Dance
Shuriken, Knife



Strumisgod
Female
Sagittarius
70
45
Samurai
Yin Yang Magic
MA Save
Halve MP
Fly

Asura Knife

Barbuta
Linen Cuirass
Bracer

Bizen Boat, Kiyomori
Poison, Spell Absorb, Silence Song, Paralyze, Sleep

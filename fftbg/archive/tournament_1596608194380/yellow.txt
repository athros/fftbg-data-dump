Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Fattunaking
Male
Taurus
75
59
Monk
Basic Skill
Regenerator
Beastmaster
Fly



Leather Hat
Mystic Vest
Jade Armlet

Spin Fist, Pummel, Purification, Chakra, Revive
Heal, Yell, Cheer Up, Scream



Flacococo
Monster
Capricorn
35
73
Sekhret










Lydian C
Female
Gemini
55
64
Samurai
Elemental
HP Restore
Magic Defense UP
Move+3

Javelin

Diamond Helmet
Crystal Mail
Reflect Ring

Koutetsu
Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



HaateXIII
Male
Capricorn
63
80
Squire
Draw Out
MA Save
Equip Gun
Retreat

Glacier Gun
Gold Shield
Iron Helmet
Leather Armor
Genji Gauntlet

Heal, Tickle, Cheer Up, Fury, Wish
Bizen Boat, Murasame, Muramasa

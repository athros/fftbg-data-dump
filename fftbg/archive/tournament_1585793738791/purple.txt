Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Twelfthrootoftwo
Female
Aquarius
62
53
Oracle
Summon Magic
Counter Magic
Equip Polearm
Teleport

Holy Lance

Barette
Light Robe
Feather Mantle

Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep
Moogle, Shiva, Golem, Bahamut, Lich



Dreadnxught
Female
Capricorn
78
67
Thief
Summon Magic
Critical Quick
Magic Defense UP
Lava Walking

Dagger

Green Beret
Wizard Outfit
N-Kai Armlet

Steal Helmet, Steal Weapon
Moogle, Shiva, Titan, Carbunkle, Bahamut, Salamander, Silf



Silentperogy
Monster
Sagittarius
46
63
Malboro










Sairentozon7
Female
Leo
54
43
Chemist
Dance
Absorb Used MP
Equip Sword
Ignore Terrain

Platinum Sword

Red Hood
Mystic Vest
Sprint Shoes

Potion, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Wiznaibus, Disillusion, Void Storage, Dragon Pit

Player: !Black
Team: Black Team
Palettes: Black/Red



KaLam1ty
Male
Aquarius
80
52
Mime

Mana Shield
Dual Wield
Lava Walking



Feather Hat
Mystic Vest
Power Wrist

Mimic




Red Lancer
Female
Leo
70
49
Knight
Basic Skill
Dragon Spirit
Equip Armor
Fly

Save the Queen
Genji Shield
Leather Helmet
Clothes
Feather Mantle

Head Break, Weapon Break, Magic Break, Speed Break, Power Break
Accumulate, Cheer Up, Fury



Aldrammech
Monster
Virgo
74
66
Squidraken










Syrail
Female
Gemini
72
44
Monk
Yin Yang Magic
Counter Tackle
Equip Gun
Ignore Height

Fairy Harp

Flash Hat
Clothes
Feather Boots

Secret Fist, Purification, Chakra, Revive
Blind, Pray Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze

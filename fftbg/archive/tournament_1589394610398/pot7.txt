Final Bets: yellow - 14 bets for 10,382G (43.7%, x1.29); black - 18 bets for 13,386G (56.3%, x0.78)

yellow bets:
DavenIII: 3,500G (33.7%, 30,327G)
metagameface: 2,499G (24.1%, 2,499G)
TheChainNerd: 885G (8.5%, 885G)
Lordminsc: 865G (8.3%, 1,730G)
killth3kid: 500G (4.8%, 7,050G)
joewcarson: 432G (4.2%, 432G)
coralreeferz: 348G (3.4%, 348G)
CorpusCav: 348G (3.4%, 16,348G)
SQUiDSQUARKLIN: 300G (2.9%, 4,053G)
carsenhk: 250G (2.4%, 33,455G)
ungabunga_bot: 155G (1.5%, 482,388G)
benticore: 100G (1.0%, 2,231G)
Firesheath: 100G (1.0%, 26,191G)
Sairentozon7: 100G (1.0%, 957G)

black bets:
lobsterlobster: 3,000G (22.4%, 8,176G)
HaateXIII: 1,262G (9.4%, 2,476G)
BirbBrainsBot: 1,000G (7.5%, 24,602G)
E_Ballard: 1,000G (7.5%, 5,795G)
Lionhermit: 1,000G (7.5%, 53,105G)
JumbocactuarX27: 958G (7.2%, 958G)
Estan_AD: 848G (6.3%, 848G)
nifboy: 602G (4.5%, 602G)
monsturmilk: 600G (4.5%, 600G)
thunderducker: 600G (4.5%, 2,584G)
EnemyController: 500G (3.7%, 228,776G)
OmnibotGamma: 500G (3.7%, 23,341G)
Miyokari: 450G (3.4%, 13,087G)
fenaen: 384G (2.9%, 384G)
CapnChaos12: 300G (2.2%, 3,175G)
getthemoneyz: 182G (1.4%, 671,668G)
Sykper: 100G (0.7%, 108G)
datadrivenbot: 100G (0.7%, 15,356G)

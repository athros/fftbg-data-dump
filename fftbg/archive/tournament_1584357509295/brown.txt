Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Raixelol
Male
Scorpio
77
42
Oracle
Throw
Meatbone Slash
Short Charge
Move+1

Battle Bamboo

Green Beret
Judo Outfit
Genji Gauntlet

Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie
Shuriken, Dictionary



Realitydown
Monster
Aquarius
55
77
Great Malboro










Lou3b
Female
Aquarius
50
71
Geomancer
Basic Skill
Counter Flood
Equip Sword
Move+3

Platinum Sword
Crystal Shield
Feather Hat
Wizard Robe
Power Wrist

Pitfall, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Accumulate, Dash, Heal, Yell, Fury, Wish, Ultima



Upvla
Female
Scorpio
42
46
Geomancer
Draw Out
Parry
Halve MP
Waterwalking

Giant Axe
Gold Shield
Leather Hat
Wizard Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Asura, Heaven's Cloud, Kiyomori, Kikuichimoji

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WhiteTigress
Female
Gemini
67
68
Geomancer
Talk Skill
Damage Split
Concentrate
Teleport

Slasher
Crystal Shield
Red Hood
Black Robe
108 Gems

Pitfall, Hell Ivy, Quicksand, Lava Ball
Invitation, Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate



KitchTowel
Female
Aries
63
64
Dancer
Steal
Auto Potion
Magic Defense UP
Ignore Terrain

Ryozan Silk

Black Hood
Earth Clothes
Cursed Ring

Last Dance, Nether Demon
Steal Helmet, Steal Shield



Bbalpert
Male
Leo
71
70
Samurai
Battle Skill
Auto Potion
Defend
Retreat

Bizen Boat

Circlet
Crystal Mail
Leather Mantle

Koutetsu, Bizen Boat, Murasame
Head Break, Armor Break, Magic Break, Power Break, Justice Sword, Dark Sword



Gooseyourself
Female
Libra
79
67
Ninja
White Magic
Blade Grasp
Halve MP
Move+1

Blind Knife
Zorlin Shape
Red Hood
Black Costume
Dracula Mantle

Bomb
Cure, Raise, Protect 2, Shell, Esuna

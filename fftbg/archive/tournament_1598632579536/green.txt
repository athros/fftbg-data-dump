Player: !Green
Team: Green Team
Palettes: Green/White



Kronikle
Male
Cancer
53
77
Archer
Throw
Hamedo
Doublehand
Waterbreathing

Mythril Gun

Holy Miter
Wizard Outfit
Power Wrist

Charge+4, Charge+5, Charge+7
Shuriken, Bomb, Staff, Wand



Laserman1000
Female
Taurus
81
64
Samurai
Punch Art
Parry
Magic Attack UP
Teleport 2

Koutetsu Knife

Cross Helmet
Reflect Mail
Cursed Ring

Koutetsu, Murasame
Spin Fist, Pummel, Earth Slash, Secret Fist, Chakra, Revive



Lord Gwarth
Male
Aquarius
77
42
Knight
Yin Yang Magic
Catch
Doublehand
Move+3

Coral Sword

Mythril Helmet
Carabini Mail
Small Mantle

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword
Poison, Confusion Song



Nhammen
Male
Aries
52
41
Monk
Elemental
Counter Flood
Long Status
Teleport

Panther Bag

Twist Headband
Leather Outfit
Elf Mantle

Spin Fist, Earth Slash, Purification, Chakra
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

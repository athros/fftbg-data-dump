Player: !White
Team: White Team
Palettes: White/Blue



RetroGameCommander
Female
Virgo
54
58
Summoner
Battle Skill
Dragon Spirit
Halve MP
Move+1

Rod

Feather Hat
Leather Outfit
Wizard Mantle

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Fairy
Head Break, Armor Break, Power Break, Mind Break, Dark Sword, Night Sword, Explosion Sword



J2DaBibbles
Female
Taurus
65
72
Time Mage
White Magic
Counter Magic
Short Charge
Lava Walking

Oak Staff

Golden Hairpin
Black Robe
Dracula Mantle

Haste 2, Stop, Quick, Stabilize Time
Cure 4, Raise, Reraise, Wall, Esuna, Holy



UmaiJam
Male
Aries
62
49
Monk
Summon Magic
Critical Quick
Dual Wield
Jump+3



Black Hood
Leather Outfit
Power Wrist

Secret Fist, Purification, Revive
Shiva, Ifrit, Carbunkle, Bahamut, Leviathan, Salamander, Lich, Zodiac



Ko2q
Female
Leo
80
55
Samurai
Throw
MA Save
Defend
Waterwalking

Murasame

Iron Helmet
Mythril Armor
Dracula Mantle

Koutetsu, Bizen Boat, Kiyomori, Muramasa
Shuriken, Stick

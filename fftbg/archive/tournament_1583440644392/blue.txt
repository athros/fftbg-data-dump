Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Winterharte
Male
Leo
71
64
Summoner
Charge
Damage Split
Equip Sword
Jump+3

Kiyomori

Thief Hat
Light Robe
Battle Boots

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Silf, Lich
Charge+2, Charge+3



Rogueain
Male
Aquarius
77
63
Ninja
Time Magic
Counter Magic
Long Status
Move-HP Up

Flame Whip
Spell Edge
Holy Miter
Power Sleeve
Wizard Mantle

Sword, Stick
Haste, Immobilize, Float, Meteor



Spartan Paladin
Male
Taurus
50
68
Knight
Punch Art
MP Restore
Secret Hunt
Waterbreathing

Diamond Sword
Round Shield
Platinum Helmet
Mythril Armor
Angel Ring

Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Justice Sword
Earth Slash, Purification, Revive, Seal Evil



CamelCash007
Female
Aquarius
64
74
Monk
Yin Yang Magic
Auto Potion
Defense UP
Move+2



Leather Hat
Power Sleeve
Feather Mantle

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Poison, Life Drain, Pray Faith, Doubt Faith, Dispel Magic

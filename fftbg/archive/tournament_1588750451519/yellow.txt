Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CapnChaos12
Male
Leo
59
60
Ninja
Charge
MA Save
Equip Armor
Move-MP Up

Iga Knife
Spell Edge
Cachusha
Silk Robe
N-Kai Armlet

Bomb, Axe
Charge+20



Shakarak
Male
Libra
55
74
Ninja
Time Magic
Catch
Equip Knife
Retreat

Spell Edge
Rod
Leather Hat
Chain Vest
Red Shoes

Shuriken, Bomb, Knife, Sword, Wand, Dictionary
Haste, Haste 2, Stop, Immobilize, Float, Demi 2, Stabilize Time, Meteor



Pandasforsale
Female
Serpentarius
74
51
Chemist
Steal
Arrow Guard
Beastmaster
Lava Walking

Platinum Sword

Golden Hairpin
Judo Outfit
Sprint Shoes

Hi-Potion, Eye Drop, Echo Grass, Maiden's Kiss
Gil Taking, Steal Shield



UmaiJam
Female
Virgo
49
72
Lancer
Charge
Parry
Halve MP
Retreat

Spear
Bronze Shield
Bronze Helmet
Linen Cuirass
Magic Ring

Level Jump8, Vertical Jump3
Charge+1, Charge+3, Charge+5, Charge+7, Charge+10

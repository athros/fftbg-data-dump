Player: !Red
Team: Red Team
Palettes: Red/Brown



Sairentozon7
Monster
Taurus
43
57
Ahriman










Gorgewall
Male
Leo
44
58
Mime

Sunken State
Dual Wield
Move+3



Platinum Helmet
Judo Outfit
Small Mantle

Mimic




ApplesauceBoss
Female
Cancer
65
47
Chemist
Battle Skill
Faith Save
Magic Attack UP
Move-HP Up

Mythril Gun

Leather Hat
Mythril Vest
Feather Boots

Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Head Break, Shield Break, Speed Break, Dark Sword



MrFiggleston
Female
Libra
74
41
Chemist
Talk Skill
Speed Save
Magic Defense UP
Move-HP Up

Panther Bag

Triangle Hat
Judo Outfit
N-Kai Armlet

Hi-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Persuade, Praise, Threaten, Preach, Refute, Rehabilitate

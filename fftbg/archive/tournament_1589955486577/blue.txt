Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



T8T3R
Male
Aries
39
47
Summoner
Talk Skill
Auto Potion
Equip Knife
Jump+1

Orichalcum

Black Hood
Silk Robe
Red Shoes

Ifrit, Golem, Bahamut, Odin, Leviathan
Insult, Negotiate, Refute



Evontno
Female
Virgo
45
74
Mime

Auto Potion
Monster Talk
Lava Walking



Flash Hat
Rubber Costume
Diamond Armlet

Mimic




TheJonTerp
Female
Pisces
72
61
Ninja
Yin Yang Magic
Counter
Short Status
Move-MP Up

Sasuke Knife
Flame Whip
Black Hood
Black Costume
108 Gems

Shuriken, Knife
Doubt Faith, Zombie, Silence Song, Petrify



Bryan792
Female
Capricorn
40
64
Wizard
Elemental
Arrow Guard
Short Charge
Swim

Wizard Rod

Thief Hat
Linen Robe
108 Gems

Fire, Fire 2, Fire 4, Ice, Ice 4, Frog, Death
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

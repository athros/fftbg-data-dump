Player: !Green
Team: Green Team
Palettes: Green/White



Laserman1000
Male
Cancer
63
69
Chemist
Draw Out
Dragon Spirit
Beastmaster
Lava Walking

Star Bag

Thief Hat
Black Costume
Feather Mantle

Potion, Hi-Potion, X-Potion, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



Just Here2
Male
Capricorn
69
57
Time Mage
Elemental
PA Save
Equip Shield
Jump+1

Healing Staff
Mythril Shield
Leather Hat
Mythril Vest
Small Mantle

Haste 2, Slow 2, Stop, Float, Demi 2, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



DragonLord Carter
Female
Libra
46
67
Monk
Draw Out
Regenerator
Long Status
Jump+1



Twist Headband
Brigandine
Magic Ring

Spin Fist, Pummel, Secret Fist, Purification, Revive, Seal Evil
Asura, Murasame



Creggers
Monster
Sagittarius
74
45
Vampire








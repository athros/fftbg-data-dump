Player: !Black
Team: Black Team
Palettes: Black/Red



Kahlindra
Female
Taurus
56
68
Dancer
Item
Mana Shield
Equip Polearm
Lava Walking

Battle Bamboo

Feather Hat
Wizard Robe
Diamond Armlet

Slow Dance, Polka Polka, Disillusion, Dragon Pit
Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Remedy



EunosXX
Male
Taurus
77
69
Priest
Steal
Brave Up
Equip Polearm
Move+1

Gungnir

Leather Hat
Chameleon Robe
Defense Ring

Cure, Cure 2, Raise, Regen, Protect 2, Shell, Shell 2, Holy
Steal Heart, Steal Status, Arm Aim



Zodiac192133
Male
Virgo
47
75
Bard
Summon Magic
Caution
Equip Gun
Waterbreathing

Stone Gun

Flash Hat
Linen Cuirass
Germinas Boots

Angel Song, Life Song, Cheer Song, Space Storage, Hydra Pit
Moogle, Shiva, Ramuh, Golem, Carbunkle, Silf



NoxeGS
Female
Gemini
60
54
Chemist
Time Magic
PA Save
Short Charge
Jump+2

Air Knife

Red Hood
Adaman Vest
Defense Armlet

Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down
Haste, Slow 2, Stop, Float, Meteor

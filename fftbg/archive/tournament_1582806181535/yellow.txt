Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Madarius777
Male
Cancer
79
60
Priest
Item
Distribute
Short Charge
Swim

Healing Staff

Golden Hairpin
Mystic Vest
Germinas Boots

Cure 3, Cure 4, Raise, Regen, Shell 2, Wall, Esuna
Potion, Hi-Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down



ForagerCats
Male
Gemini
77
44
Knight
Draw Out
Counter
Equip Knife
Lava Walking

Spell Edge
Mythril Shield
Gold Helmet
Diamond Armor
Germinas Boots

Shield Break, Weapon Break, Power Break, Mind Break
Koutetsu, Kikuichimoji



Upvla
Male
Taurus
67
47
Geomancer
Charge
Caution
Maintenance
Levitate

Ice Brand
Mythril Shield
Green Beret
Wizard Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+2, Charge+4



Adenis222
Female
Sagittarius
44
74
Oracle
Steal
Parry
Long Status
Ignore Terrain

Papyrus Codex

Golden Hairpin
Adaman Vest
Elf Mantle

Blind, Pray Faith, Silence Song, Dispel Magic, Paralyze
Steal Weapon, Steal Status

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ApplesauceBoss
Female
Pisces
47
77
Mediator
Draw Out
Counter
Equip Shield
Jump+3

Blaze Gun
Round Shield
Green Beret
Clothes
Red Shoes

Invitation, Preach, Refute, Rehabilitate
Heaven's Cloud



BlackFireUK
Male
Leo
39
44
Knight
Talk Skill
Auto Potion
Maintenance
Waterbreathing

Save the Queen
Flame Shield
Bronze Helmet
Genji Armor
Rubber Shoes

Head Break, Shield Break, Magic Break, Mind Break, Justice Sword
Invitation, Preach, Death Sentence



Flameatron
Male
Scorpio
48
55
Monk
Basic Skill
Distribute
Dual Wield
Swim



Triangle Hat
Secret Clothes
Red Shoes

Spin Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Dash, Heal, Yell



IphoneDarkness
Male
Capricorn
47
48
Lancer
Draw Out
Earplug
Dual Wield
Retreat

Spear
Obelisk
Genji Helmet
Platinum Armor
Leather Mantle

Level Jump5, Vertical Jump2
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji

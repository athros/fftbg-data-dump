Player: !Black
Team: Black Team
Palettes: Black/Red



Benticore
Male
Scorpio
52
79
Ninja
Battle Skill
Distribute
Equip Armor
Jump+1

Mythril Knife
Flame Whip
Golden Hairpin
Genji Armor
Feather Mantle

Shuriken, Sword
Head Break, Magic Break, Explosion Sword



RunicMagus
Female
Pisces
57
66
Time Mage
Dance
Parry
Defense UP
Levitate

Rainbow Staff

Barette
Clothes
Angel Ring

Haste, Haste 2, Slow, Reflect
Polka Polka, Disillusion



Nizaha
Female
Pisces
74
59
Oracle
White Magic
Arrow Guard
Short Charge
Move-MP Up

Octagon Rod

Triangle Hat
Judo Outfit
Feather Mantle

Spell Absorb, Silence Song, Foxbird, Confusion Song, Dispel Magic, Petrify, Dark Holy
Cure 3, Reraise, Regen, Protect, Protect 2, Shell, Wall



LDHaten
Male
Capricorn
50
45
Archer
White Magic
Arrow Guard
Concentrate
Swim

Night Killer
Aegis Shield
Cachusha
Adaman Vest
Germinas Boots

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Cure, Cure 2, Raise, Reraise, Shell, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Legionx13
Monster
Gemini
58
50
Porky










Creggers
Female
Libra
77
71
Summoner
Dance
Arrow Guard
Short Charge
Swim

Gold Staff

Ribbon
Leather Outfit
Sprint Shoes

Moogle, Ramuh, Golem, Carbunkle, Silf
Polka Polka, Obsidian Blade



Kellios11
Male
Aries
47
76
Calculator
Bio
Counter
Equip Axe
Move+2

Wizard Staff
Gold Shield
Leather Helmet
Chain Vest
Feather Mantle

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Pplvee1
Male
Capricorn
44
51
Knight
Throw
Distribute
Equip Axe
Jump+3

Morning Star
Escutcheon
Barbuta
Leather Armor
108 Gems

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Night Sword
Bomb, Sword

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



LordMaxoss
Male
Gemini
70
78
Samurai
Black Magic
Damage Split
Doublehand
Waterbreathing

Spear

Mythril Helmet
Crystal Mail
N-Kai Armlet

Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa
Fire, Bolt 2



Aldrammech
Female
Taurus
64
41
Archer
Yin Yang Magic
Counter
Defense UP
Retreat

Poison Bow
Aegis Shield
Headgear
Power Sleeve
Leather Mantle

Charge+1, Charge+3, Charge+5
Spell Absorb, Dispel Magic, Sleep



Laserman1000
Female
Sagittarius
71
77
Summoner
Draw Out
Brave Up
Attack UP
Waterbreathing

Thunder Rod

Holy Miter
Silk Robe
Rubber Shoes

Moogle, Shiva, Ramuh, Titan, Carbunkle, Bahamut, Odin, Leviathan, Silf, Cyclops
Asura, Koutetsu, Bizen Boat, Murasame



Lodrak
Male
Aries
80
38
Ninja
Jump
Faith Up
Concentrate
Ignore Terrain

Ninja Edge
Morning Star
Feather Hat
Power Sleeve
Rubber Shoes

Shuriken, Knife, Stick
Level Jump4, Vertical Jump6

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lali Lulelo
Monster
Gemini
56
68
Blue Dragon










I Nod My Head When I Lose
Female
Libra
69
61
Oracle
Time Magic
Hamedo
Short Charge
Retreat

Battle Bamboo

Black Hood
Chameleon Robe
Cursed Ring

Blind, Life Drain, Pray Faith, Silence Song, Blind Rage
Haste, Haste 2, Slow 2, Stop, Float, Stabilize Time



HASTERIOUS
Male
Sagittarius
72
49
Lancer
Black Magic
Faith Up
Equip Gun
Jump+3

Mythril Gun
Diamond Shield
Crystal Helmet
Maximillian
Red Shoes

Level Jump8, Vertical Jump7
Fire, Ice, Death



Gelwain
Female
Pisces
76
61
Archer
White Magic
MA Save
Doublehand
Move+1

Blaze Gun

Barette
Wizard Outfit
108 Gems

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Cure 4, Regen, Protect, Protect 2, Shell, Esuna

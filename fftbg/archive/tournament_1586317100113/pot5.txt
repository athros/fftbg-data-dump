Final Bets: blue - 16 bets for 16,230G (59.3%, x0.69); green - 20 bets for 11,158G (40.7%, x1.45)

blue bets:
sinnyil2: 6,783G (41.8%, 13,567G)
helpimabug: 2,316G (14.3%, 4,633G)
upvla: 2,190G (13.5%, 23,445G)
BirbBrainsBot: 1,000G (6.2%, 248,329G)
Maeveen: 841G (5.2%, 1,683G)
Break3r: 624G (3.8%, 624G)
ColetteMSLP: 400G (2.5%, 14,653G)
V3rdeni: 400G (2.5%, 3,656G)
Meanderandreturn: 400G (2.5%, 9,858G)
ZephyrTempest: 302G (1.9%, 30,527G)
Rethera_: 276G (1.7%, 276G)
Chuckolator: 221G (1.4%, 3,149G)
Aldrammech: 200G (1.2%, 2,001G)
AllInBot: 100G (0.6%, 100G)
BlinkFS: 100G (0.6%, 1,051G)
Ross_from_Cali: 77G (0.5%, 1,045G)

green bets:
Zeroroute: 2,168G (19.4%, 2,168G)
YaBoy125: 1,880G (16.8%, 1,880G)
DeathTaxesAndAnime: 1,848G (16.6%, 3,624G)
HaplessOne: 1,000G (9.0%, 134,072G)
Evewho: 719G (6.4%, 1,438G)
Pie108: 500G (4.5%, 10,790G)
Humble_Fabio: 500G (4.5%, 7,829G)
MirtaiAtana: 422G (3.8%, 422G)
JackOnFire1: 413G (3.7%, 7,413G)
CapnChaos12: 300G (2.7%, 9,060G)
b0shii: 244G (2.2%, 244G)
roofiepops: 200G (1.8%, 2,627G)
KasugaiRoastedPeas: 200G (1.8%, 17,603G)
Ausuri: 169G (1.5%, 3,397G)
ungabunga_bot: 165G (1.5%, 73,691G)
ANFz: 100G (0.9%, 2,358G)
moonliquor: 100G (0.9%, 970G)
datadrivenbot: 100G (0.9%, 26,149G)
getthemoneyz: 80G (0.7%, 444,834G)
cougboi: 50G (0.4%, 4,579G)

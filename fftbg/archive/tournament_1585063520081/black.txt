Player: !Black
Team: Black Team
Palettes: Black/Red



Rrazzoug
Male
Cancer
69
57
Thief
Black Magic
Counter Magic
Long Status
Move+3

Short Edge

Flash Hat
Chain Vest
Bracer

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Ice 2



Phi Sig
Male
Pisces
56
53
Summoner
White Magic
Abandon
Short Charge
Levitate

Rod

Black Hood
Power Sleeve
Reflect Ring

Moogle, Ramuh, Golem, Carbunkle, Leviathan, Silf, Cyclops
Cure 2, Cure 3, Cure 4, Regen, Protect 2, Wall, Esuna



Jordache7K
Female
Gemini
78
70
Priest
Yin Yang Magic
Earplug
Beastmaster
Swim

Healing Staff

Golden Hairpin
Linen Robe
Feather Mantle

Cure, Raise, Protect 2, Esuna
Poison, Foxbird



Daveb
Male
Cancer
80
55
Ninja
Sing
Absorb Used MP
Equip Gun
Move+3

Papyrus Codex
Papyrus Codex
Green Beret
Clothes
Chantage

Shuriken, Bomb
Life Song, Battle Song, Nameless Song, Last Song, Diamond Blade

Player: !Black
Team: Black Team
Palettes: Black/Red



KingofTricksters
Female
Serpentarius
73
43
Calculator
Limit
Regenerator
Magic Defense UP
Waterbreathing

Bestiary

Bronze Helmet
Light Robe
Cursed Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Hamborn
Male
Aquarius
53
41
Thief
Time Magic
Counter Tackle
Concentrate
Move+1

Blind Knife

Green Beret
Chain Vest
Chantage

Gil Taking, Steal Helmet, Steal Weapon, Steal Status
Slow, Stop, Demi, Stabilize Time



Mexskacin
Female
Capricorn
66
71
Time Mage
White Magic
Counter Magic
Equip Sword
Ignore Height

Muramasa

Leather Hat
Black Robe
Magic Gauntlet

Haste, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Cure 3, Cure 4, Shell, Shell 2, Wall, Esuna, Holy



Pandasforsale
Female
Virgo
68
73
Dancer
White Magic
Abandon
Dual Wield
Jump+2

Ryozan Silk
Persia
Golden Hairpin
Mythril Vest
108 Gems

Slow Dance, Polka Polka, Nameless Dance, Last Dance, Dragon Pit
Cure 2, Cure 3, Raise, Reraise, Wall, Esuna

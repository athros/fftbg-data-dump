Final Bets: blue - 8 bets for 3,303G (51.9%, x0.93); champion - 5 bets for 3,060G (48.1%, x1.08)

blue bets:
superdevon1: 1,218G (36.9%, 12,188G)
boosted420: 700G (21.2%, 1,411G)
NIghtdew14: 480G (14.5%, 480G)
DustBirdEX: 355G (10.7%, 5,847G)
AllInBot: 200G (6.1%, 200G)
datadrivenbot: 200G (6.1%, 28,940G)
CosmicTactician: 100G (3.0%, 42,717G)
DouglasDragonThePoet: 50G (1.5%, 410G)

champion bets:
BirbBrainsBot: 1,000G (32.7%, 193,126G)
twelfthrootoftwo: 972G (31.8%, 972G)
getthemoneyz: 496G (16.2%, 1,304,618G)
randgridr: 356G (11.6%, 356G)
Xoomwaffle: 236G (7.7%, 236G)

Player: !Green
Team: Green Team
Palettes: Green/White



BeibeLoide
Male
Aries
59
61
Knight
Summon Magic
Mana Shield
Magic Defense UP
Levitate

Mythril Sword
Escutcheon
Genji Helmet
Diamond Armor
Spike Shoes

Head Break, Armor Break, Weapon Break, Speed Break
Moogle, Ifrit, Golem, Carbunkle, Bahamut, Odin, Leviathan, Silf, Fairy



Shalloween
Female
Taurus
57
73
Dancer
Charge
Sunken State
Doublehand
Fly

Cashmere

Feather Hat
Power Sleeve
Feather Mantle

Last Dance, Nether Demon, Dragon Pit
Charge+2, Charge+4, Charge+7, Charge+10



L2 Sentinel
Female
Cancer
68
54
Chemist
Punch Art
HP Restore
Equip Polearm
Move-MP Up

Partisan

Black Hood
Power Sleeve
Defense Armlet

Potion, X-Potion, Hi-Ether, Echo Grass, Holy Water, Phoenix Down
Secret Fist, Purification



Maakur
Male
Libra
62
78
Archer
Punch Art
HP Restore
Magic Defense UP
Move+2

Night Killer
Gold Shield
Red Hood
Black Costume
Cursed Ring

Charge+2, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive

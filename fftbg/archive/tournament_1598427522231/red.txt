Player: !Red
Team: Red Team
Palettes: Red/Brown



Lifebregin
Male
Cancer
57
72
Lancer
Item
Critical Quick
Attack UP
Swim

Gungnir
Hero Shield
Mythril Helmet
Plate Mail
Dracula Mantle

Level Jump3, Vertical Jump6
Potion, Hi-Potion, X-Potion, Maiden's Kiss, Phoenix Down



Randgridr
Male
Scorpio
72
42
Lancer
Summon Magic
PA Save
Martial Arts
Ignore Height


Flame Shield
Circlet
Diamond Armor
Genji Gauntlet

Level Jump2, Vertical Jump3
Titan, Odin



Gorgewall
Male
Virgo
48
57
Mime

Earplug
Magic Defense UP
Ignore Height



Golden Hairpin
Wizard Outfit
Germinas Boots

Mimic




Neville
Male
Gemini
60
43
Chemist
Charge
Dragon Spirit
Short Status
Move-MP Up

Star Bag

Feather Hat
Leather Outfit
Reflect Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down
Charge+3, Charge+4, Charge+7, Charge+20

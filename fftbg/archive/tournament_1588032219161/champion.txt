Player: !zChamp
Team: Champion Team
Palettes: Green/White



Shalloween
Female
Aries
45
65
Samurai
Punch Art
Regenerator
Dual Wield
Move+3

Koutetsu Knife
Masamune
Bronze Helmet
White Robe
Reflect Ring

Asura, Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Seal Evil



Mirapoix
Male
Capricorn
80
66
Mediator
Draw Out
Critical Quick
Beastmaster
Fly

Battle Folio

Golden Hairpin
Adaman Vest
Genji Gauntlet

Persuade, Preach, Mimic Daravon, Refute
Heaven's Cloud



Kellios11
Male
Leo
53
58
Squire
Black Magic
Parry
Attack UP
Ignore Height

Gastrafitis
Flame Shield
Leather Helmet
Plate Mail
N-Kai Armlet

Dash, Throw Stone, Heal, Fury, Wish
Fire, Fire 2, Fire 3, Bolt 4, Ice, Ice 3, Flare



Astrozin11
Female
Aquarius
69
80
Wizard
Talk Skill
Parry
Short Charge
Move+3

Rod

Golden Hairpin
Black Costume
Genji Gauntlet

Fire 4, Ice, Ice 2, Ice 3, Frog, Flare
Praise, Solution, Insult, Mimic Daravon, Refute

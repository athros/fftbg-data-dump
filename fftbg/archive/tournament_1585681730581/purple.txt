Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



OmnibotGamma
Male
Capricorn
80
47
Mime

Dragon Spirit
Martial Arts
Levitate



Leather Hat
Brigandine
N-Kai Armlet

Mimic




OneHundredFists
Male
Libra
77
47
Bard
Steal
Parry
Magic Defense UP
Move+2

Bloody Strings

Headgear
Mystic Vest
Power Wrist

Angel Song, Magic Song, Last Song
Steal Helmet, Steal Shield, Steal Status, Arm Aim



GrayGhostGaming
Female
Cancer
46
54
Calculator
Black Magic
Critical Quick
Beastmaster
Fly

Bestiary

Red Hood
Wizard Robe
Defense Ring

CT, Height, 4, 3
Fire, Bolt 3, Ice 3, Empower, Death



DudeMonkey77
Male
Cancer
56
65
Lancer
Punch Art
MA Save
Secret Hunt
Waterwalking

Battle Bamboo
Escutcheon
Cross Helmet
Linen Cuirass
Elf Mantle

Level Jump8, Vertical Jump7
Wave Fist, Earth Slash, Purification, Revive, Seal Evil

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Maakur
Male
Cancer
80
67
Lancer
Throw
Meatbone Slash
Equip Armor
Teleport

Spear
Flame Shield
Platinum Helmet
Chameleon Robe
N-Kai Armlet

Level Jump5, Vertical Jump5
Shuriken, Sword, Wand



CCRUNNER149UWP
Male
Aquarius
74
46
Samurai
Steal
Caution
Concentrate
Move-HP Up

Koutetsu Knife

Mythril Helmet
Leather Armor
Feather Boots

Asura, Bizen Boat, Muramasa
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Leg Aim



Chuckolator
Male
Libra
52
47
Lancer
Item
MA Save
Equip Sword
Retreat

Asura Knife
Round Shield
Gold Helmet
Reflect Mail
Germinas Boots

Level Jump2, Vertical Jump5
Potion, Hi-Potion, Antidote, Maiden's Kiss, Soft



ForagerCats
Female
Leo
56
63
Priest
Battle Skill
Counter
Beastmaster
Move+1

Morning Star

Twist Headband
Clothes
Spike Shoes

Cure 3, Cure 4, Raise, Raise 2, Protect, Shell 2, Esuna
Head Break, Armor Break, Dark Sword

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Nhammen
Male
Sagittarius
53
72
Chemist
Jump
Damage Split
Beastmaster
Jump+2

Romanda Gun

Thief Hat
Brigandine
Jade Armlet

Potion, Hi-Potion, X-Potion, Antidote, Holy Water, Phoenix Down
Level Jump4, Vertical Jump5



Grininda
Male
Aries
46
58
Priest
Summon Magic
Distribute
Sicken
Move-HP Up

Oak Staff

Red Hood
Wizard Outfit
Leather Mantle

Cure, Raise, Shell, Esuna, Holy
Moogle, Shiva, Ifrit, Golem, Leviathan



Letdowncity
Male
Aquarius
55
58
Samurai
Black Magic
Speed Save
Maintenance
Teleport 2

Mythril Spear

Bronze Helmet
Crystal Mail
Germinas Boots

Koutetsu, Heaven's Cloud, Kiyomori, Masamune
Fire 3, Bolt, Bolt 2, Ice 2, Ice 3



CosmicTactician
Male
Aquarius
59
67
Mediator
Throw
Parry
Dual Wield
Jump+1

Cute Bag
Cute Bag
Twist Headband
Mythril Vest
Feather Mantle

Invitation, Praise, Preach, Solution, Insult, Refute
Shuriken

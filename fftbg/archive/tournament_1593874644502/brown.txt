Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gggamezzz
Male
Capricorn
69
65
Calculator
Limit
Counter
Sicken
Jump+3

Bestiary
Flame Shield
Leather Hat
Chain Mail
Cursed Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Ar Tactic
Male
Leo
52
48
Monk
Elemental
Earplug
Defend
Move+3



Flash Hat
Power Sleeve
Elf Mantle

Wave Fist, Secret Fist, Purification, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind, Lava Ball



PoroTact
Male
Libra
53
50
Chemist
Battle Skill
Counter
Sicken
Move-HP Up

Dagger

Feather Hat
Mythril Vest
Defense Ring

Potion, Hi-Potion, Ether, Hi-Ether
Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Night Sword, Surging Sword



E Ballard
Male
Taurus
53
61
Knight
Steal
HP Restore
Dual Wield
Move+1

Coral Sword
Rune Blade
Mythril Helmet
Diamond Armor
Rubber Shoes

Head Break, Speed Break
Gil Taking, Steal Shield, Arm Aim

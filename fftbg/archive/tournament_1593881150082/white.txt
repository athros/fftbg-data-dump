Player: !White
Team: White Team
Palettes: White/Blue



Neocarbuncle
Male
Sagittarius
62
69
Calculator
Yin Yang Magic
Earplug
Equip Polearm
Levitate

Persia

Feather Hat
Wizard Outfit
Genji Gauntlet

Height, 5, 4, 3
Spell Absorb, Zombie, Foxbird



ArashiKurobara
Female
Aries
63
78
Oracle
Steal
Critical Quick
Equip Sword
Waterbreathing

Murasame

Golden Hairpin
Silk Robe
Small Mantle

Poison, Life Drain, Foxbird, Dispel Magic, Paralyze
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Arm Aim



Run With Stone GUNs
Monster
Taurus
51
71
Apanda










RampagingRobot
Male
Libra
62
41
Archer
Basic Skill
Speed Save
Magic Defense UP
Ignore Height

Perseus Bow

Holy Miter
Black Costume
Power Wrist

Charge+5, Charge+7
Dash, Heal, Scream

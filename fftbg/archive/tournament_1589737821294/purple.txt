Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



4shcrows
Male
Cancer
43
41
Chemist
White Magic
Parry
Defense UP
Move+2

Glacier Gun

Leather Hat
Mythril Vest
Power Wrist

Potion, X-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Cure 2, Raise, Protect, Shell, Esuna



Inosukeanu
Female
Aquarius
73
43
Dancer
Draw Out
PA Save
Attack UP
Jump+1

Cute Bag

Leather Hat
Chameleon Robe
Magic Gauntlet

Nameless Dance, Void Storage, Nether Demon, Dragon Pit
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Masamune



Dellusionsx
Female
Sagittarius
65
70
Monk
Elemental
Abandon
Defense UP
Retreat



Golden Hairpin
Clothes
Magic Gauntlet

Spin Fist, Pummel, Purification, Revive
Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



FlohIristhrae
Male
Gemini
59
48
Wizard
Battle Skill
Parry
Equip Bow
Fly

Windslash Bow

Twist Headband
Linen Robe
N-Kai Armlet

Fire, Fire 4, Bolt, Bolt 2, Ice, Frog, Death, Flare
Weapon Break, Power Break, Dark Sword

Player: !Black
Team: Black Team
Palettes: Black/Red



Ausar1
Male
Scorpio
40
75
Monk
Summon Magic
Counter
Equip Axe
Move+2

Flail

Red Hood
Wizard Outfit
Vanish Mantle

Pummel, Secret Fist, Purification, Seal Evil
Shiva, Ramuh, Carbunkle



SSwing
Monster
Aries
52
59
Archaic Demon










Theseawolf1
Male
Leo
38
38
Knight
Throw
Damage Split
Sicken
Jump+1

Iron Sword
Mythril Shield
Iron Helmet
Bronze Armor
Feather Boots

Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Hammer, Stick



Lordminsc
Male
Capricorn
77
60
Squire
Time Magic
Earplug
Magic Attack UP
Levitate

Diamond Sword
Round Shield
Cross Helmet
Crystal Mail
Angel Ring

Throw Stone, Heal, Tickle, Fury, Wish
Haste, Slow 2, Immobilize, Demi 2

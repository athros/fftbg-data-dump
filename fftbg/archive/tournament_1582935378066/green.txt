Player: !Green
Team: Green Team
Palettes: Green/White



RjA0zcOQ96
Male
Aries
51
38
Summoner
Punch Art
Absorb Used MP
Concentrate
Fly

Rod

Red Hood
Robe of Lords
108 Gems

Moogle, Ramuh, Ifrit, Odin, Leviathan, Salamander
Spin Fist, Wave Fist, Purification, Chakra, Revive



Xeiphyer
Female
Sagittarius
49
55
Calculator
Time Magic
Faith Up
Maintenance
Move+1

Madlemgen

Triangle Hat
Light Robe
Reflect Ring

Height, Prime Number, 5, 3
Stabilize Time, Meteor



Powergems
Male
Leo
52
63
Oracle
Draw Out
Damage Split
Defend
Move-MP Up

Octagon Rod

Triangle Hat
Leather Outfit
Feather Boots

Poison, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Koutetsu, Bizen Boat, Murasame, Masamune



Grimmace45
Male
Sagittarius
76
62
Summoner
Sing
Catch
Equip Bow
Fly

Poison Bow

Flash Hat
Secret Clothes
Cherche

Moogle, Titan, Leviathan, Lich
Life Song, Cheer Song, Last Song

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ins4n1ty87
Female
Cancer
61
42
Thief
Black Magic
Parry
Equip Gun
Jump+3

Romanda Gun

Golden Hairpin
Brigandine
Leather Mantle

Steal Helmet, Steal Armor, Steal Accessory, Steal Status
Fire, Fire 2, Bolt 2, Ice



GrauKhan
Male
Leo
63
50
Oracle
Jump
Absorb Used MP
Equip Polearm
Jump+3

Gungnir

Black Hood
Wizard Outfit
Jade Armlet

Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze
Level Jump4, Vertical Jump6



Kolonel Panic
Male
Libra
45
51
Time Mage
Basic Skill
Critical Quick
Beastmaster
Swim

Rainbow Staff

Triangle Hat
Black Robe
Angel Ring

Haste 2, Immobilize, Reflect, Demi 2, Stabilize Time
Accumulate, Throw Stone, Heal, Cheer Up



Furorcelt
Male
Capricorn
50
66
Thief
Sing
Hamedo
Attack UP
Retreat

Short Edge

Cachusha
Earth Clothes
Battle Boots

Steal Heart, Steal Helmet, Steal Armor
Life Song, Diamond Blade

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ShintaroNayaka
Male
Scorpio
63
41
Ninja
Basic Skill
HP Restore
Beastmaster
Jump+1

Sasuke Knife
Ninja Edge
Triangle Hat
Earth Clothes
Bracer

Shuriken, Bomb, Knife
Throw Stone, Heal, Scream



Rubenflonne
Monster
Leo
54
66
Red Chocobo










Manamorphose
Male
Scorpio
75
60
Chemist
Summon Magic
Meatbone Slash
Equip Shield
Jump+1

Hydra Bag
Platinum Shield
Black Hood
Black Costume
Elf Mantle

Potion, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Moogle, Ramuh, Carbunkle, Leviathan



Roqqqpsi
Female
Virgo
54
78
Geomancer
Basic Skill
Damage Split
Doublehand
Waterwalking

Heaven's Cloud

Holy Miter
White Robe
Leather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Heal, Yell, Cheer Up

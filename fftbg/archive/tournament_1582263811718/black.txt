Player: !Black
Team: Black Team
Palettes: Black/Red



Tearshang
Female
Virgo
73
51
Knight
Charge
Brave Up
Dual Wield
Teleport

Defender
Battle Axe
Cross Helmet
Mythril Armor
Leather Mantle

Magic Break, Mind Break, Justice Sword, Dark Sword
Charge+1, Charge+4



Frustratedman
Male
Pisces
75
62
Bard
Charge
PA Save
Concentrate
Fly

Bloody Strings

Feather Hat
Platinum Armor
Elf Mantle

Cheer Song, Battle Song, Last Song, Space Storage, Hydra Pit
Charge+2, Charge+3



Runai
Male
Aquarius
58
56
Ninja
Item
MA Save
Sicken
Move-HP Up

Kunai
Ninja Edge
Thief Hat
Leather Outfit
Magic Ring

Shuriken, Hammer, Dictionary
Potion, Hi-Potion, Antidote, Maiden's Kiss, Remedy, Phoenix Down



Victoriolue
Female
Taurus
50
82
Calculator
Black Magic
Counter
Attack UP
Ignore Height

Gokuu Rod

Golden Hairpin
Clothes
Dracula Mantle

CT, 5, 3
Bolt 2, Ice

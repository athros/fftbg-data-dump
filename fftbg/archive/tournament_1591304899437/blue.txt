Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Roofiepops
Male
Virgo
59
60
Ninja
Jump
Arrow Guard
Equip Gun
Jump+3

Battle Folio
Bestiary
Feather Hat
Mythril Vest
Red Shoes

Stick, Wand
Level Jump8, Vertical Jump8



ValtonZenola
Male
Scorpio
58
60
Lancer
Basic Skill
Brave Save
Concentrate
Jump+2

Obelisk
Platinum Shield
Platinum Helmet
Diamond Armor
Feather Mantle

Level Jump3, Vertical Jump7
Dash, Throw Stone



DudeMonkey77
Male
Gemini
42
80
Thief
Throw
Meatbone Slash
Doublehand
Levitate

Coral Sword

Triangle Hat
Chain Vest
Jade Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Leg Aim
Shuriken, Bomb, Dictionary



DLJuggernaut
Male
Taurus
55
49
Geomancer
Steal
Damage Split
Equip Knife
Fly

Air Knife
Genji Shield
Golden Hairpin
Wizard Robe
Chantage

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Steal Helmet, Steal Accessory, Steal Status, Arm Aim

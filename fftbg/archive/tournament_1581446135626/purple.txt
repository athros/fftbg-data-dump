Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mogwin23
Female
Scorpio
45
53
Archer
Yin Yang Magic
Counter Magic
Dual Wield
Move-HP Up

Long Bow

Feather Hat
Adaman Vest
Feather Mantle

Charge+3, Charge+5, Charge+7
Life Drain, Pray Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Sleep



Kaelsun
Male
Taurus
41
58
Mediator
Elemental
PA Save
Halve MP
Waterbreathing

Assassin Dagger

Green Beret
White Robe
Feather Mantle

Persuade, Threaten, Preach, Solution, Mimic Daravon, Refute, Rehabilitate
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand



Jeannot
Monster
Gemini
70
73
Porky










Tanookium
Female
Leo
44
78
Chemist
Throw
Absorb Used MP
Equip Gun
Move+3

Blast Gun

Triangle Hat
Brigandine
N-Kai Armlet

Potion, X-Potion, Elixir, Antidote, Eye Drop, Holy Water, Remedy, Phoenix Down
Knife, Hammer

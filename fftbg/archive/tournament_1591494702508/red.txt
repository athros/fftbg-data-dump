Player: !Red
Team: Red Team
Palettes: Red/Brown



OneHundredFists
Female
Scorpio
51
53
Summoner
Battle Skill
Blade Grasp
Short Charge
Ignore Terrain

Oak Staff

Headgear
White Robe
Bracer

Moogle, Shiva, Titan, Carbunkle, Bahamut, Leviathan, Salamander
Magic Break



Gorgewall
Male
Libra
62
70
Bard
Black Magic
Counter Magic
Short Status
Jump+1

Ramia Harp

Headgear
Secret Clothes
Small Mantle

Angel Song, Cheer Song, Magic Song, Sky Demon
Fire, Fire 3, Ice 2, Ice 4, Empower, Death



Run With Stone GUNs
Male
Cancer
43
58
Lancer
Summon Magic
Dragon Spirit
Martial Arts
Move-MP Up

Spear
Mythril Shield
Bronze Helmet
Reflect Mail
Magic Gauntlet

Level Jump4, Vertical Jump6
Golem, Carbunkle, Leviathan, Salamander



Edgehead62888
Female
Leo
47
52
Wizard
White Magic
MA Save
Short Charge
Jump+1

Flame Rod

Thief Hat
Wizard Outfit
Reflect Ring

Fire 2, Bolt 4, Ice 3, Death
Cure, Cure 4, Raise 2, Protect 2, Shell 2, Wall

Final Bets: white - 9 bets for 13,593G (58.1%, x0.72); black - 19 bets for 9,799G (41.9%, x1.39)

white bets:
dogsandcatsand: 6,020G (44.3%, 11,804G)
douchetron: 4,193G (30.8%, 4,193G)
SkylerBunny: 1,257G (9.2%, 1,257G)
toka222: 1,000G (7.4%, 125,187G)
UmaiJam: 600G (4.4%, 600G)
Tarheels218: 200G (1.5%, 1,211G)
roofiepops: 123G (0.9%, 3,853G)
ko2q: 100G (0.7%, 928G)
nhammen: 100G (0.7%, 26,069G)

black bets:
DeathTaxesAndAnime: 1,669G (17.0%, 1,669G)
PuzzleSecretary: 1,032G (10.5%, 1,032G)
BirbBrainsBot: 1,000G (10.2%, 165,403G)
E_Ballard: 620G (6.3%, 620G)
Zagorsek: 592G (6.0%, 592G)
Forkmore: 528G (5.4%, 528G)
letdowncity: 528G (5.4%, 26,861G)
cho_pin: 500G (5.1%, 7,820G)
HaateXIII: 500G (5.1%, 6,243G)
nekojin: 496G (5.1%, 12,896G)
ShintaroNayaka: 436G (4.4%, 436G)
Treafa: 400G (4.1%, 2,774G)
Shalloween: 400G (4.1%, 13,795G)
triniculo: 292G (3.0%, 292G)
AllInBot: 206G (2.1%, 206G)
datadrivenbot: 200G (2.0%, 66,906G)
TheUnforgivenRage: 200G (2.0%, 7,757G)
Valgram: 100G (1.0%, 1,904G)
mirapoix: 100G (1.0%, 3,651G)

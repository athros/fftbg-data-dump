Player: !Green
Team: Green Team
Palettes: Green/White



Aestheta
Female
Virgo
65
65
Geomancer
Summon Magic
Distribute
Equip Armor
Lava Walking

Iron Sword
Genji Shield
Circlet
Mythril Vest
Wizard Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Moogle, Carbunkle, Salamander, Fairy, Lich



Rilgon
Monster
Scorpio
63
57
Hydra










Coralreeferz
Male
Aquarius
50
40
Thief
Item
PA Save
Throw Item
Waterwalking

Ninja Edge

Golden Hairpin
Leather Outfit
Defense Ring

Steal Helmet, Steal Accessory, Arm Aim, Leg Aim
Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss



Docmitchphd
Female
Aquarius
71
64
Summoner
Yin Yang Magic
Mana Shield
Equip Armor
Jump+3

Flame Rod

Leather Hat
Genji Armor
108 Gems

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Silf, Lich
Life Drain, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep

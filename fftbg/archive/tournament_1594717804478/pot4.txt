Final Bets: purple - 9 bets for 5,300G (51.1%, x0.96); brown - 6 bets for 5,070G (48.9%, x1.05)

purple bets:
sukotsuto: 1,500G (28.3%, 21,993G)
upvla: 1,000G (18.9%, 5,230G)
ShintaroNayaka: 884G (16.7%, 884G)
Hasterious: 822G (15.5%, 822G)
hiros13gts: 400G (7.5%, 799G)
dtrain332: 255G (4.8%, 511G)
datadrivenbot: 200G (3.8%, 57,152G)
Lydian_C: 123G (2.3%, 79,752G)
Zenmaza: 116G (2.2%, 116G)

brown bets:
dantayystv: 2,000G (39.4%, 23,118G)
BirbBrainsBot: 1,000G (19.7%, 143,625G)
NicoSavoy: 1,000G (19.7%, 184,169G)
getthemoneyz: 670G (13.2%, 1,275,667G)
Mysteriousdewd: 200G (3.9%, 6,056G)
Evewho: 200G (3.9%, 5,715G)

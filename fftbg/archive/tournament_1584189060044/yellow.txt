Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Volgrathemoose
Female
Taurus
46
53
Dancer
Black Magic
Arrow Guard
Equip Shield
Move+1

Persia
Bronze Shield
Golden Hairpin
Earth Clothes
Genji Gauntlet

Last Dance
Fire, Fire 4, Bolt 3, Ice 2, Ice 4, Empower, Frog



CilantroGamer
Female
Leo
49
48
Geomancer
Summon Magic
Speed Save
Equip Gun
Jump+3

Glacier Gun
Round Shield
Black Hood
Earth Clothes
Red Shoes

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Salamander, Lich



ColetteMSLP
Male
Scorpio
67
67
Mime

Earplug
Equip Armor
Jump+1



Leather Helmet
Mystic Vest
Jade Armlet

Mimic




Furorcelt
Female
Scorpio
52
42
Samurai
Talk Skill
Earplug
Equip Gun
Retreat

Romanda Gun

Circlet
Crystal Mail
Dracula Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Invitation, Persuade, Praise, Threaten, Preach, Insult, Mimic Daravon, Refute

Player: !Red
Team: Red Team
Palettes: Red/Brown



Peluducer
Male
Leo
74
68
Monk
Throw
Abandon
Concentrate
Swim



Leather Hat
Leather Outfit
Dracula Mantle

Secret Fist, Chakra, Revive
Shuriken



PrudishDuckling
Monster
Aquarius
62
64
Sekhret










Leakimiko
Male
Taurus
40
52
Mime

Faith Up
Dual Wield
Move-HP Up



Triangle Hat
Judo Outfit
Sprint Shoes

Mimic




Wonkierlynx
Male
Gemini
64
69
Squire
Charge
Arrow Guard
Martial Arts
Move-HP Up


Round Shield
Circlet
Genji Armor
N-Kai Armlet

Dash, Heal, Tickle, Fury, Wish
Charge+1

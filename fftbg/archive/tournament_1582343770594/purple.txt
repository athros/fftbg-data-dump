Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Madarius777
Female
Scorpio
68
69
Mediator
White Magic
Arrow Guard
Dual Wield
Move+2

Blast Gun
Glacier Gun
Green Beret
Mystic Vest
Bracer

Insult, Negotiate, Refute
Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Protect 2, Shell, Esuna, Holy



Xeurian
Male
Libra
43
62
Ninja
Item
Catch
Equip Knife
Jump+2

Ninja Edge
Air Knife
Thief Hat
Clothes
Setiemson

Bomb
Potion, Antidote, Eye Drop, Holy Water, Remedy, Phoenix Down



BigDLicious91
Male
Sagittarius
48
63
Geomancer
Summon Magic
Speed Save
Beastmaster
Ignore Terrain

Ancient Sword
Buckler
Headgear
White Robe
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Moogle, Shiva, Ramuh, Titan, Bahamut, Fairy



Minina Bella
Male
Capricorn
62
61
Chemist
Sing
Mana Shield
Sicken
Waterbreathing

Hydra Bag

Thief Hat
Power Sleeve
Battle Boots

Potion, Ether, Hi-Ether, Eye Drop
Diamond Blade, Space Storage

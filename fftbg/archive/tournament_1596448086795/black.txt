Player: !Black
Team: Black Team
Palettes: Black/Red



Rednecknazgul
Female
Pisces
70
41
Dancer
Black Magic
Counter Magic
Concentrate
Levitate

Persia

Leather Hat
White Robe
Magic Ring

Witch Hunt, Disillusion, Nameless Dance, Obsidian Blade, Nether Demon
Fire, Fire 3, Bolt 3, Ice, Ice 3, Ice 4, Empower, Frog, Flare



Superdevon1
Female
Scorpio
47
60
Priest
Steal
Counter
Equip Armor
Waterbreathing

Flail

Iron Helmet
Diamond Armor
Salty Rage

Cure 2, Cure 3, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Gil Taking, Steal Armor, Steal Shield, Steal Status, Leg Aim



Gorgewall
Male
Serpentarius
73
69
Ninja
Talk Skill
Absorb Used MP
Sicken
Fly

Dagger
Iga Knife
Leather Hat
Power Sleeve
Feather Boots

Shuriken, Bomb
Praise, Threaten, Negotiate, Refute



Sairentozon7
Female
Leo
73
39
Dancer
Black Magic
Hamedo
Equip Axe
Retreat

Morning Star

Flash Hat
Wizard Outfit
Jade Armlet

Witch Hunt, Disillusion, Nether Demon
Fire, Bolt, Bolt 3, Ice, Frog

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dragon RW
Female
Taurus
67
67
Ninja
Draw Out
Counter Tackle
Short Status
Swim

Short Edge
Morning Star
Red Hood
Mythril Vest
Defense Ring

Bomb
Koutetsu, Heaven's Cloud, Kikuichimoji



Grininda
Female
Cancer
58
73
Geomancer
Draw Out
PA Save
Sicken
Teleport

Koutetsu Knife
Buckler
Holy Miter
Wizard Robe
Germinas Boots

Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu



UnderOneLight
Male
Virgo
79
77
Calculator
Black Magic
Meatbone Slash
Defend
Ignore Terrain

Papyrus Codex

Holy Miter
Mystic Vest
Rubber Shoes

Height, 5, 4, 3
Fire 3, Bolt 4, Ice 4, Flare



Nestor
Male
Scorpio
70
65
Time Mage
Basic Skill
Counter
Equip Bow
Move+3

Mythril Bow

Leather Hat
Chameleon Robe
Leather Mantle

Haste 2, Slow, Stop, Immobilize, Reflect, Demi 2
Accumulate, Dash, Tickle

Final Bets: white - 27 bets for 21,874G (56.9%, x0.76); black - 12 bets for 16,594G (43.1%, x1.32)

white bets:
Nizaha: 8,468G (38.7%, 16,936G)
volgrathemoose: 1,900G (8.7%, 1,900G)
NoNotBees: 1,840G (8.4%, 1,840G)
getthemoneyz: 1,000G (4.6%, 454,772G)
BirbBrainsBot: 1,000G (4.6%, 16,554G)
SeedSC: 1,000G (4.6%, 254,442G)
OneHundredFists: 1,000G (4.6%, 1,631G)
bpc2163: 1,000G (4.6%, 6,496G)
RunicMagus: 525G (2.4%, 525G)
Pie108: 500G (2.3%, 11,119G)
SeraphPDH: 480G (2.2%, 480G)
SkylerBunny: 356G (1.6%, 356G)
Bulleta: 350G (1.6%, 3,630G)
ZephyrTempest: 340G (1.6%, 43,767G)
Slowbrofist: 292G (1.3%, 292G)
Evewho: 292G (1.3%, 292G)
urieltheflameofgod: 200G (0.9%, 8,751G)
lijarkh: 200G (0.9%, 2,908G)
Oobs56: 200G (0.9%, 3,105G)
Meridian75W: 171G (0.8%, 342G)
The8thZodiac: 160G (0.7%, 160G)
AllInBot: 100G (0.5%, 100G)
BlinkFS: 100G (0.5%, 612G)
maakur_: 100G (0.5%, 3,022G)
Tougou: 100G (0.5%, 7,426G)
jackkrause: 100G (0.5%, 1,799G)
ApplesauceBoss: 100G (0.5%, 2,102G)

black bets:
DeathTaxesAndAnime: 9,090G (54.8%, 17,824G)
kingchadking: 1,974G (11.9%, 1,974G)
Lanshaft: 1,125G (6.8%, 1,125G)
reinoe: 1,000G (6.0%, 20,548G)
OmnibotGamma: 1,000G (6.0%, 7,530G)
joewcarson: 785G (4.7%, 785G)
Aldrammech: 600G (3.6%, 600G)
ANFz: 464G (2.8%, 464G)
Tithonus: 300G (1.8%, 14,835G)
ungabunga_bot: 151G (0.9%, 87,754G)
RughSontos: 100G (0.6%, 3,555G)
ko2q: 5G (0.0%, 4,786G)

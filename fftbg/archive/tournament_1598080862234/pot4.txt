Final Bets: purple - 7 bets for 4,373G (48.1%, x1.08); brown - 10 bets for 4,725G (51.9%, x0.93)

purple bets:
bruubarg: 1,295G (29.6%, 1,295G)
Sairentozon7: 1,000G (22.9%, 22,726G)
BirbBrainsBot: 1,000G (22.9%, 117,595G)
killth3kid: 772G (17.7%, 40,383G)
Lifebregin: 105G (2.4%, 13,905G)
gorgewall: 101G (2.3%, 3,109G)
Hojosu: 100G (2.3%, 100G)

brown bets:
silverChangeling: 1,681G (35.6%, 8,407G)
Seaweed_B: 1,000G (21.2%, 47,515G)
cloudycube: 478G (10.1%, 938G)
resjudicata3: 376G (8.0%, 376G)
Snowfats: 276G (5.8%, 276G)
PuzzleSecretary: 242G (5.1%, 242G)
datadrivenbot: 200G (4.2%, 62,318G)
CT_5_Holy: 200G (4.2%, 2,509G)
getthemoneyz: 172G (3.6%, 1,703,482G)
AllInBot: 100G (2.1%, 100G)

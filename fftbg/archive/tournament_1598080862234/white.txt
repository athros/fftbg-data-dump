Player: !White
Team: White Team
Palettes: White/Blue



Drusiform
Female
Aries
59
65
Archer
Steal
Counter Magic
Doublehand
Waterbreathing

Hunting Bow

Barbuta
Leather Outfit
Feather Boots

Charge+1, Charge+2, Charge+3, Charge+5, Charge+10
Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



Bruubarg
Female
Virgo
79
69
Geomancer
Punch Art
Earplug
Concentrate
Move+2

Battle Axe
Escutcheon
Headgear
Light Robe
Genji Gauntlet

Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Pummel, Wave Fist, Secret Fist, Purification



Cloudycube
Male
Sagittarius
64
45
Calculator
Labyrinth Skill
Counter Tackle
Short Status
Waterwalking

Papyrus Codex
Mythril Shield
Circlet
Adaman Vest
Wizard Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power



Gorgewall
Male
Taurus
79
65
Archer
Time Magic
Counter Flood
Equip Bow
Waterbreathing

Panther Bag
Bronze Shield
Red Hood
Adaman Vest
Genji Gauntlet

Charge+4
Haste, Stop, Demi

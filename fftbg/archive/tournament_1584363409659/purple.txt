Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LDSkinny
Female
Sagittarius
55
72
Squire
Draw Out
Counter Flood
Halve MP
Move+2

Mage Masher
Mythril Shield
Bronze Helmet
Mythril Vest
Sprint Shoes

Accumulate, Dash, Throw Stone, Heal, Cheer Up, Fury, Scream
Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa



Rnark
Male
Aries
67
58
Mediator
White Magic
Damage Split
Doublehand
Waterbreathing

Battle Folio

Ribbon
Earth Clothes
Germinas Boots

Persuade, Praise, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Raise, Esuna



Maeveen
Male
Cancer
52
61
Squire
Item
Regenerator
Throw Item
Move+2

Sleep Sword
Genji Shield
Golden Hairpin
Crystal Mail
Angel Ring

Dash, Heal, Tickle, Yell, Fury, Wish
Potion, Hi-Potion, Ether, Antidote, Echo Grass, Soft, Phoenix Down



Bbalpert
Male
Aquarius
46
80
Calculator
White Magic
Counter Flood
Equip Shield
Move+1

Battle Folio
Gold Shield
Green Beret
White Robe
Genji Gauntlet

CT, Height, Prime Number, 4
Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Shell 2, Wall, Esuna, Magic Barrier

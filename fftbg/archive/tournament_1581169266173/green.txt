Player: !Green
Team: Green Team
Palettes: Green/White



JIDkomu
Monster
Cancer
47
63
Tiamat










Nizdra
Female
Scorpio
53
58
Monk
Battle Skill
Earplug
Maintenance
Fly



Golden Hairpin
Clothes
Defense Armlet

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Speed Break, Power Break, Surging Sword



Mightygerm1
Female
Sagittarius
36
40
Wizard
Battle Skill
Auto Potion
Magic Defense UP
Waterwalking

Poison Rod

Green Beret
Light Robe
Diamond Armlet

Fire 4, Ice, Ice 2
Armor Break, Shield Break, Justice Sword, Night Sword



BenYuPoker
Female
Scorpio
54
54
Summoner
Time Magic
Mana Shield
Short Charge
Waterbreathing

Gold Staff

Black Hood
Earth Clothes
Defense Ring

Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Silf
Haste, Stop, Immobilize, Stabilize Time

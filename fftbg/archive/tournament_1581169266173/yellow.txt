Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CaptainAdmiralSPATULA
Monster
Gemini
43
45
Porky










Yayifications
Male
Virgo
49
43
Knight
Draw Out
Speed Save
Dual Wield
Move+3

Ancient Sword
Giant Axe
Mythril Helmet
Chain Mail
Defense Ring

Armor Break, Magic Break, Speed Break, Power Break
Asura, Muramasa



Wickslee
Male
Cancer
51
40
Lancer
Throw
MA Save
Attack UP
Move-HP Up

Mythril Spear
Mythril Shield
Crystal Helmet
Platinum Armor
Bracer

Level Jump4, Vertical Jump8
Shuriken, Ninja Sword



25or6to4
Male
Leo
38
57
Archer
Steal
Parry
Sicken
Ignore Terrain

Silver Bow

Twist Headband
Leather Outfit
Feather Mantle

Charge+1, Charge+2, Charge+5, Charge+10
Steal Helmet, Steal Weapon, Steal Status, Arm Aim

Player: !Red
Team: Red Team
Palettes: Red/Brown



Jondee10
Male
Leo
71
76
Lancer
Steal
Catch
Equip Axe
Lava Walking

Gold Staff
Bronze Shield
Platinum Helmet
White Robe
Diamond Armlet

Level Jump8, Vertical Jump6
Gil Taking, Steal Heart, Steal Helmet, Steal Weapon



RubenFlonne
Male
Scorpio
42
74
Calculator
Black Magic
Distribute
Equip Gun
Ignore Height

Battle Folio

Red Hood
Power Sleeve
Defense Armlet

CT, Height, Prime Number, 5, 4, 3
Bolt, Ice 2, Ice 3, Ice 4, Frog



Powergems
Male
Aries
46
45
Chemist
Black Magic
Brave Up
Dual Wield
Waterbreathing

Mythril Knife
Star Bag
Twist Headband
Wizard Outfit
Battle Boots

Potion, X-Potion, Maiden's Kiss, Soft, Phoenix Down
Fire 4, Ice, Ice 2, Empower



ANFz
Male
Capricorn
35
69
Lancer
Talk Skill
Auto Potion
Sicken
Lava Walking

Javelin
Gold Shield
Diamond Helmet
Leather Armor
Red Shoes

Level Jump8, Vertical Jump6
Invitation, Persuade, Solution, Death Sentence, Negotiate, Refute

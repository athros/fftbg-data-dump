Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Mirapoix
Female
Gemini
50
65
Wizard
Math Skill
Sunken State
Maintenance
Teleport

Air Knife

Thief Hat
Earth Clothes
Reflect Ring

Fire 3, Bolt, Bolt 4, Ice, Ice 2, Ice 3, Empower
Height, 4, 3



ALY327
Female
Leo
46
63
Priest
Draw Out
Damage Split
Magic Attack UP
Jump+3

Flame Whip

Feather Hat
Power Sleeve
Dracula Mantle

Cure 4, Raise, Regen, Protect, Esuna, Holy
Asura, Muramasa



GrandmasterFrankerZ
Female
Virgo
51
51
Mediator
Black Magic
Brave Up
Maintenance
Jump+2

Mythril Gun

Golden Hairpin
Power Sleeve
Elf Mantle

Invitation, Threaten, Solution, Death Sentence, Negotiate, Refute
Fire, Fire 2, Ice 2, Ice 3, Ice 4, Empower, Frog, Flare



KasugaiRoastedPeas
Female
Scorpio
41
79
Monk
Talk Skill
Damage Split
Equip Knife
Move+1

Spell Edge

Flash Hat
Leather Outfit
Leather Mantle

Earth Slash, Secret Fist, Purification, Revive
Threaten, Preach, Rehabilitate

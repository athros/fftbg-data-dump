Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CorpusCav
Female
Taurus
77
51
Calculator
White Magic
Abandon
Defense UP
Move+1

Cute Bag

Triangle Hat
Black Costume
Spike Shoes

CT, Height, Prime Number, 5, 3
Cure, Cure 2, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna, Holy



Nerf Vergil
Male
Libra
71
64
Time Mage
Sing
Abandon
Equip Shield
Jump+1

Wizard Staff
Ice Shield
Green Beret
Chain Vest
Red Shoes

Haste, Slow 2, Demi
Last Song



Zmoses
Male
Aquarius
48
54
Calculator
Time Magic
Parry
Sicken
Move+3

Thunder Rod

Red Hood
Light Robe
Small Mantle

CT, Height, Prime Number, 4
Haste, Float, Reflect, Demi 2, Stabilize Time



CassiePhoenix
Female
Cancer
72
55
Wizard
Throw
Arrow Guard
Beastmaster
Ignore Terrain

Faith Rod

Black Hood
Wizard Outfit
Rubber Shoes

Fire 3, Bolt 3, Bolt 4, Ice 3, Empower, Frog
Knife

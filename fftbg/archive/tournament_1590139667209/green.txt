Player: !Green
Team: Green Team
Palettes: Green/White



PotionDweller
Male
Taurus
74
38
Wizard
Time Magic
Dragon Spirit
Short Charge
Move+1

Flame Rod

Twist Headband
Silk Robe
Reflect Ring

Fire, Bolt, Ice, Ice 3, Frog, Flare
Haste, Haste 2, Quick, Demi, Stabilize Time



AmaninAmide
Female
Libra
43
52
Archer
Draw Out
Arrow Guard
Dual Wield
Move-HP Up

Ice Bow

Flash Hat
Rubber Costume
Power Wrist

Charge+1, Charge+2, Charge+3, Charge+10
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Gorgewall
Female
Pisces
51
47
Chemist
Draw Out
Catch
Magic Defense UP
Jump+1

Star Bag

Golden Hairpin
Earth Clothes
Angel Ring

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down
Koutetsu, Murasame, Kikuichimoji



YaBoy125
Female
Aquarius
73
43
Priest
Dance
Faith Save
Doublehand
Jump+1

Wizard Staff

Twist Headband
Chameleon Robe
Defense Ring

Cure, Cure 3, Cure 4, Raise 2, Protect, Shell, Wall, Esuna
Witch Hunt, Polka Polka, Last Dance, Void Storage

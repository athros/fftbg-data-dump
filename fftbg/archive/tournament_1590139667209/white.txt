Player: !White
Team: White Team
Palettes: White/Blue



Volgrathemoose
Female
Virgo
67
70
Summoner
Battle Skill
PA Save
Short Charge
Levitate

Flame Rod

Golden Hairpin
White Robe
Small Mantle

Shiva
Shield Break, Magic Break, Power Break, Justice Sword



Candina
Male
Aries
73
76
Ninja
Charge
MP Restore
Long Status
Fly

Hidden Knife
Flame Whip
Leather Hat
Chain Vest
Bracer

Shuriken
Charge+4, Charge+7



Lawndough
Female
Scorpio
58
70
Squire
Draw Out
Critical Quick
Short Charge
Retreat

Diamond Sword
Aegis Shield
Twist Headband
Wizard Outfit
Red Shoes

Accumulate, Dash, Wish
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



DeathTaxesAndAnime
Female
Capricorn
73
68
Geomancer
Item
Damage Split
Throw Item
Move-MP Up

Muramasa
Hero Shield
Red Hood
Mystic Vest
Reflect Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Remedy

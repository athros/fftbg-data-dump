Player: !Green
Team: Green Team
Palettes: Green/White



Silentperogy
Male
Scorpio
55
67
Summoner
Throw
Sunken State
Maintenance
Move+3

Oak Staff

Headgear
White Robe
Defense Ring

Carbunkle, Odin, Salamander
Shuriken, Bomb, Knife, Stick



Lali Lulelo
Male
Cancer
42
44
Time Mage
White Magic
Counter
Defend
Waterbreathing

White Staff

Flash Hat
Wizard Robe
Red Shoes

Haste, Stop, Float, Quick, Demi, Stabilize Time
Cure, Cure 2, Raise, Regen, Protect, Shell 2, Esuna



Heroebal
Male
Taurus
67
58
Chemist
Black Magic
Critical Quick
Secret Hunt
Jump+2

Stone Gun

Feather Hat
Judo Outfit
Bracer

Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Fire 2, Fire 4, Bolt, Death



Bpc2163
Male
Gemini
73
50
Lancer
Time Magic
Catch
Equip Bow
Ignore Height

Bow Gun
Round Shield
Platinum Helmet
Diamond Armor
Chantage

Level Jump5, Vertical Jump2
Float, Quick, Meteor

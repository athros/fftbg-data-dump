Final Bets: red - 8 bets for 24,085G (76.5%, x0.31); green - 11 bets for 7,410G (23.5%, x3.25)

red bets:
Ruvelia_BibeI: 16,731G (69.5%, 16,731G)
DamnThatShark: 4,000G (16.6%, 10,371G)
superdevon1: 1,428G (5.9%, 1,428G)
WitchHunterIX: 1,000G (4.2%, 4,398G)
nifboy: 492G (2.0%, 492G)
elnobueno: 164G (0.7%, 264G)
Coolguye: 150G (0.6%, 2,013G)
Lydian_C: 120G (0.5%, 163,520G)

green bets:
Zachara: 2,067G (27.9%, 149,067G)
lowlf: 1,201G (16.2%, 78,061G)
reinoe: 1,000G (13.5%, 8,505G)
BirbBrainsBot: 1,000G (13.5%, 87,861G)
twelfthrootoftwo: 1,000G (13.5%, 4,920G)
DuraiPapers: 500G (6.7%, 17,234G)
getthemoneyz: 242G (3.3%, 1,014,753G)
SeniorBunk: 100G (1.3%, 9,438G)
serperemagus: 100G (1.3%, 12,689G)
extinctrational: 100G (1.3%, 2,035G)
datadrivenbot: 100G (1.3%, 45,797G)

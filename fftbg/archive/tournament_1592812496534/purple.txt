Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



WitchHunterIX
Monster
Aries
59
44
Archaic Demon










Powermhero
Male
Leo
44
57
Time Mage
Yin Yang Magic
Distribute
Dual Wield
Swim

Healing Staff
Rainbow Staff
Leather Hat
Secret Clothes
Wizard Mantle

Haste, Haste 2, Slow 2, Stop, Demi, Demi 2, Meteor
Spell Absorb, Doubt Faith, Dispel Magic, Sleep



Creggers
Male
Capricorn
75
49
Ninja
Charge
Counter Magic
Equip Gun
Move+2

Papyrus Codex
Bestiary
Feather Hat
Brigandine
Cursed Ring

Bomb, Knife, Spear
Charge+10, Charge+20



Bruubarg
Female
Aries
62
65
Oracle
Math Skill
Counter Magic
Magic Attack UP
Move+1

Iron Fan

Thief Hat
Silk Robe
Wizard Mantle

Zombie, Silence Song, Foxbird, Sleep
CT, Height, 4

Player: !Red
Team: Red Team
Palettes: Red/Brown



Chambs12
Female
Sagittarius
54
72
Mime

Abandon
Secret Hunt
Move-HP Up



Triangle Hat
Power Sleeve
Bracer

Mimic




Fenaen
Female
Virgo
77
56
Squire
Summon Magic
Catch
Equip Bow
Swim

Silver Bow

Twist Headband
Mythril Vest
Chantage

Accumulate, Throw Stone, Yell, Wish
Ramuh, Carbunkle, Salamander, Fairy



Jeffersonsa
Male
Aquarius
54
72
Bard
Steal
Catch
Equip Knife
Jump+1

Wizard Rod

Triangle Hat
Reflect Mail
Leather Mantle

Life Song, Magic Song, Sky Demon
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory



Nifboy
Female
Serpentarius
80
48
Knight
Steal
MP Restore
Magic Defense UP
Waterbreathing

Platinum Sword
Platinum Shield
Gold Helmet
Plate Mail
Rubber Shoes

Armor Break, Magic Break, Power Break, Stasis Sword, Justice Sword, Surging Sword
Steal Heart, Steal Weapon, Steal Accessory, Steal Status, Arm Aim

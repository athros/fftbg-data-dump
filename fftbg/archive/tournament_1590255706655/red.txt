Player: !Red
Team: Red Team
Palettes: Red/Brown



Maakur
Monster
Aries
48
51
Squidraken










ArchKnightX
Male
Sagittarius
72
40
Chemist
Punch Art
Distribute
Equip Armor
Move-MP Up

Blaze Gun

Diamond Helmet
Silk Robe
Elf Mantle

Potion, X-Potion, Antidote, Phoenix Down
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification



Manc0
Male
Aries
69
56
Geomancer
Jump
Counter Magic
Secret Hunt
Move-HP Up

Giant Axe
Bronze Shield
Green Beret
Linen Robe
Small Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Gusty Wind
Level Jump2, Vertical Jump5



Littlebilly3
Female
Virgo
64
43
Monk
Summon Magic
HP Restore
Concentrate
Move+3



Flash Hat
Earth Clothes
Elf Mantle

Secret Fist, Revive, Seal Evil
Moogle, Ramuh, Golem, Carbunkle

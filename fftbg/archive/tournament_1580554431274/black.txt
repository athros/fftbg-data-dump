Player: !Black
Team: Black Team
Palettes: Black/Red



Eco1327
Male
Aries
45
42
Calculator
Ghost Skill
Distribute
Short Charge
Move-HP Up

Musk Rod

Black Hood
Chain Vest
N-Kai Armlet

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



LanseDM
Female
Aries
48
71
Archer
Talk Skill
Parry
Monster Talk
Jump+3

Yoichi Bow

Green Beret
Judo Outfit
Rubber Shoes

Charge+1, Charge+2, Charge+3
Threaten, Preach, Solution, Death Sentence, Negotiate, Refute



Jamgretor
Male
Aquarius
74
38
Mime

Hamedo
Maintenance
Waterwalking



Golden Hairpin
Power Sleeve
Salty Rage

Mimic




Sicklypurp
Male
Virgo
61
47
Monk
White Magic
Absorb Used MP
Short Status
Ignore Terrain



Holy Miter
Wizard Outfit
Germinas Boots

Pummel, Wave Fist, Purification
Cure 2, Cure 3, Raise, Raise 2

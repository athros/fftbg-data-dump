Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mesmaster
Male
Sagittarius
60
47
Lancer
Talk Skill
PA Save
Attack UP
Move-MP Up

Obelisk
Aegis Shield
Diamond Helmet
Leather Armor
Defense Ring

Level Jump4, Vertical Jump2
Persuade, Praise, Threaten, Solution, Insult, Refute, Rehabilitate



D4rr1n
Male
Taurus
46
79
Chemist
Sing
PA Save
Equip Armor
Retreat

Star Bag

Bronze Helmet
Reflect Mail
Defense Ring

Potion, X-Potion, Ether, Hi-Ether, Phoenix Down
Magic Song, Last Song



Lastly
Male
Leo
71
54
Ninja
White Magic
Caution
Equip Sword
Jump+2

Heaven's Cloud
Long Sword
Barette
Power Sleeve
Red Shoes

Shuriken, Axe, Stick
Raise, Protect, Protect 2, Esuna



Pandasforsale
Male
Taurus
67
59
Ninja
Talk Skill
Regenerator
Maintenance
Waterbreathing

Short Edge
Mage Masher
Leather Hat
Secret Clothes
Setiemson

Knife
Invitation, Persuade, Solution, Negotiate, Refute

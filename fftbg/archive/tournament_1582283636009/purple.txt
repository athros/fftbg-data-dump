Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DaveKap
Female
Libra
63
45
Summoner
White Magic
Catch
Attack UP
Waterbreathing

Thunder Rod

Golden Hairpin
Mythril Vest
Reflect Ring

Moogle, Shiva, Ramuh, Ifrit, Golem, Bahamut, Leviathan, Silf, Lich, Cyclops
Cure 4, Protect, Shell, Esuna



Dhekix
Male
Sagittarius
54
65
Samurai
Sing
Sunken State
Attack UP
Ignore Height

Javelin

Leather Helmet
Bronze Armor
Dracula Mantle

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Cheer Song, Battle Song, Diamond Blade, Sky Demon



Luna XIV
Female
Virgo
43
74
Ninja
Dance
Mana Shield
Halve MP
Teleport

Sasuke Knife
Spell Edge
Twist Headband
Leather Outfit
Diamond Armlet

Knife
Wiznaibus, Slow Dance, Void Storage, Dragon Pit



Ralamar
Female
Gemini
58
75
Squire
Jump
Counter
Equip Axe
Move+1

Oak Staff

Cross Helmet
Bronze Armor
Reflect Ring

Accumulate, Throw Stone, Yell, Cheer Up
Level Jump8, Vertical Jump5

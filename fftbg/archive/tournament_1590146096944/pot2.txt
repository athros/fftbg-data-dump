Final Bets: green - 13 bets for 7,598G (70.7%, x0.42); yellow - 9 bets for 3,155G (29.3%, x2.41)

green bets:
Bryon_W: 1,500G (19.7%, 3,214G)
E_Ballard: 1,034G (13.6%, 1,034G)
CorpusCav: 1,000G (13.2%, 19,115G)
Candina: 999G (13.1%, 24,753G)
HASTERIOUS: 710G (9.3%, 14,215G)
mirapoix: 524G (6.9%, 524G)
PotionDweller: 483G (6.4%, 483G)
serperemagus: 428G (5.6%, 428G)
GnielKnows: 232G (3.1%, 232G)
mardokttv: 208G (2.7%, 208G)
d4rr1n: 200G (2.6%, 4,027G)
WunUP: 180G (2.4%, 180G)
datadrivenbot: 100G (1.3%, 22,012G)

yellow bets:
J2DaBibbles: 1,000G (31.7%, 17,441G)
ColetteMSLP: 800G (25.4%, 61,288G)
BirbBrainsBot: 484G (15.3%, 86,708G)
getthemoneyz: 258G (8.2%, 689,019G)
placeholdercats: 234G (7.4%, 1,174G)
roqqqpsi: 129G (4.1%, 12,921G)
Lawndough: 100G (3.2%, 1,920G)
CosmicTactician: 100G (3.2%, 23,283G)
DrAntiSocial: 50G (1.6%, 18,388G)

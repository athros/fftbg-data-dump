Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SkyridgeZero
Male
Libra
48
73
Lancer
Summon Magic
Blade Grasp
Concentrate
Lava Walking

Cypress Rod
Buckler
Diamond Helmet
Black Robe
Bracer

Level Jump3, Vertical Jump6
Odin, Salamander



Luna XIV
Male
Gemini
63
53
Wizard
White Magic
Speed Save
Short Status
Teleport

Wizard Rod

Flash Hat
Black Costume
Magic Gauntlet

Fire, Fire 3, Bolt, Bolt 2, Ice 3
Cure 2, Cure 3, Raise, Raise 2, Protect, Shell 2, Holy



Electric Glass
Female
Aquarius
70
56
Time Mage
Elemental
Counter
Magic Attack UP
Jump+2

Healing Staff

Golden Hairpin
Chameleon Robe
Diamond Armlet

Slow 2, Demi, Meteor
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm



Estan AD
Female
Serpentarius
64
61
Thief
Throw
MA Save
Magic Attack UP
Move-MP Up

Short Edge

Black Hood
Mystic Vest
Chantage

Steal Heart, Steal Armor, Steal Weapon
Bomb, Ninja Sword, Stick

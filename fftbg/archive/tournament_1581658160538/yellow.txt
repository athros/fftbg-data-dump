Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Luna XIV
Female
Scorpio
67
57
Oracle
Summon Magic
HP Restore
Equip Shield
Levitate

Ivory Rod
Crystal Shield
Leather Hat
Light Robe
Sprint Shoes

Blind, Pray Faith, Foxbird, Sleep
Moogle, Ifrit, Golem, Carbunkle, Bahamut, Odin, Salamander



IronMongrel
Male
Pisces
75
79
Oracle
Black Magic
Counter Flood
Equip Sword
Lava Walking

Kikuichimoji

Black Hood
Linen Robe
Red Shoes

Blind, Life Drain, Silence Song, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Fire 2, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 3, Empower, Flare



Sinnyil2
Female
Pisces
63
60
Time Mage
Draw Out
Parry
Short Status
Swim

Battle Folio

Golden Hairpin
Adaman Vest
Genji Gauntlet

Haste 2, Stop, Float, Reflect, Stabilize Time, Meteor
Koutetsu, Kikuichimoji



Powergems
Female
Sagittarius
50
77
Priest
Draw Out
Dragon Spirit
Short Charge
Ignore Height

Flame Whip

Red Hood
Leather Outfit
Wizard Mantle

Cure, Cure 2, Cure 4, Raise, Regen, Protect 2, Shell, Shell 2, Esuna
Asura, Koutetsu, Kiyomori, Muramasa, Kikuichimoji

Player: !White
Team: White Team
Palettes: White/Blue



PatSouI
Female
Aquarius
71
45
Samurai
Summon Magic
Absorb Used MP
Defend
Swim

Kikuichimoji

Bronze Helmet
Carabini Mail
Rubber Shoes

Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa
Moogle, Ramuh, Titan, Silf, Fairy, Cyclops



Billybones5150
Male
Libra
49
63
Calculator
Mighty Sword
Brave Up
Short Status
Move+2

Cypress Rod
Kaiser Plate
Holy Miter
Light Robe
Wizard Mantle

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



ArchKnightX
Female
Taurus
61
68
Geomancer
Summon Magic
HP Restore
Equip Sword
Ignore Terrain

Kikuichimoji
Diamond Shield
Flash Hat
Mystic Vest
Feather Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Titan, Carbunkle, Bahamut, Odin, Salamander



Thefyeman
Male
Cancer
38
52
Bard
Steal
Counter
Concentrate
Move-HP Up

Bloody Strings

Triangle Hat
Mythril Vest
Bracer

Nameless Song, Last Song
Steal Helmet, Steal Armor, Steal Shield, Leg Aim

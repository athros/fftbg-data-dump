Player: !Green
Team: Green Team
Palettes: Green/White



L2 Sentinel
Male
Serpentarius
53
69
Monk
Summon Magic
Meatbone Slash
Dual Wield
Retreat



Triangle Hat
Black Costume
Power Wrist

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil
Moogle, Ramuh, Bahamut, Odin, Silf, Fairy



ALY327
Female
Scorpio
40
71
Archer
Time Magic
Parry
Equip Axe
Jump+1

Rainbow Staff
Bronze Shield
Holy Miter
Wizard Outfit
Reflect Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Float, Demi, Demi 2



CT Remix
Female
Taurus
62
65
Chemist
Battle Skill
Faith Save
Sicken
Waterbreathing

Main Gauche

Golden Hairpin
Earth Clothes
Feather Mantle

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Shield Break, Magic Break, Mind Break, Dark Sword



Superdevon1
Male
Capricorn
47
79
Squire
Punch Art
Absorb Used MP
Long Status
Move+1

Dagger
Ice Shield
Headgear
Chain Mail
Germinas Boots

Accumulate, Tickle, Wish
Purification, Chakra, Revive

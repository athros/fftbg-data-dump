Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Male
Cancer
68
44
Priest
Battle Skill
Distribute
Martial Arts
Levitate

Morning Star

Leather Hat
Brigandine
Rubber Shoes

Cure, Cure 4, Raise, Regen, Holy
Weapon Break, Justice Sword, Night Sword, Surging Sword



UmaiJam
Male
Serpentarius
76
59
Monk
Jump
Distribute
Dual Wield
Jump+2



Leather Hat
Clothes
Magic Ring

Spin Fist, Pummel, Wave Fist, Purification, Revive
Level Jump3, Vertical Jump5



Neerrm
Female
Scorpio
79
64
Time Mage
Summon Magic
PA Save
Dual Wield
Jump+2

Healing Staff
Oak Staff
Black Hood
Brigandine
Sprint Shoes

Haste, Haste 2, Stop, Reflect, Demi, Galaxy Stop
Moogle, Ifrit, Carbunkle, Bahamut, Leviathan



Enkikavlar
Female
Leo
45
44
Geomancer
Summon Magic
Abandon
Martial Arts
Move+1

Slasher
Genji Shield
Leather Hat
Chameleon Robe
Elf Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Gusty Wind, Lava Ball
Ramuh, Titan, Carbunkle, Bahamut, Salamander

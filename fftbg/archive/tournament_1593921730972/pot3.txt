Final Bets: white - 9 bets for 12,625G (37.4%, x1.67); black - 12 bets for 21,101G (62.6%, x0.60)

white bets:
reinoe: 9,359G (74.1%, 37,436G)
UmaiJam: 1,000G (7.9%, 69,509G)
Laserman1000: 604G (4.8%, 24,104G)
n2ash: 500G (4.0%, 2,212G)
prince_rogers_nelson_: 316G (2.5%, 316G)
Shalloween: 300G (2.4%, 59,951G)
superdevon1: 228G (1.8%, 2,287G)
CT_5_Holy: 218G (1.7%, 2,188G)
enkikavlar: 100G (0.8%, 3,533G)

black bets:
soren_of_tyto: 17,123G (81.1%, 17,123G)
ANFz: 1,000G (4.7%, 25,859G)
BirbBrainsBot: 1,000G (4.7%, 43,869G)
RampagingRobot: 700G (3.3%, 3,200G)
twelfthrootoftwo: 300G (1.4%, 10,298G)
getthemoneyz: 286G (1.4%, 1,188,670G)
ThreeMileIsland: 212G (1.0%, 212G)
gorgewall: 201G (1.0%, 4,266G)
GBZero: 108G (0.5%, 108G)
Ring_Wyrm: 100G (0.5%, 8,473G)
1twistedpuppy: 50G (0.2%, 2,995G)
silentkaster: 21G (0.1%, 9,021G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Technominari
Female
Pisces
54
51
Priest
Elemental
Parry
Equip Axe
Move-HP Up

Morning Star

Flash Hat
Secret Clothes
Angel Ring

Raise, Regen, Protect 2, Shell 2, Esuna
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Alacor
Male
Aries
64
65
Knight
Steal
Regenerator
Equip Armor
Move+3

Battle Axe
Flame Shield
Thief Hat
Leather Armor
Power Wrist

Head Break, Shield Break, Mind Break, Justice Sword
Gil Taking, Steal Armor, Steal Accessory, Leg Aim



TheBenjimoto
Male
Taurus
53
80
Squire
Steal
MP Restore
Equip Armor
Move+1

Star Bag

Mythril Helmet
Chain Mail
Genji Gauntlet

Accumulate, Heal, Yell
Gil Taking, Steal Accessory, Leg Aim



ToucansOfSoup
Male
Taurus
70
67
Monk
Steal
Abandon
Defense UP
Swim



Twist Headband
Black Costume
108 Gems

Spin Fist, Pummel, Purification, Chakra, Revive
Arm Aim

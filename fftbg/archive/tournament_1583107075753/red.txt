Player: !Red
Team: Red Team
Palettes: Red/Brown



Dasutin23
Female
Gemini
78
44
Monk
Jump
Absorb Used MP
Long Status
Move-MP Up



Feather Hat
Clothes
Power Wrist

Secret Fist, Purification, Chakra, Revive, Seal Evil
Level Jump5, Vertical Jump6



Pedrossj
Male
Aries
70
48
Mediator
Steal
Counter Tackle
Dual Wield
Teleport

Mythril Gun
Blast Gun
Golden Hairpin
Black Costume
Germinas Boots

Solution, Death Sentence
Steal Heart, Steal Helmet, Steal Accessory



Sinnyil2
Female
Pisces
73
60
Time Mage
Elemental
Absorb Used MP
Short Status
Move-HP Up

Healing Staff

Holy Miter
Silk Robe
Spike Shoes

Haste 2, Stop, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Lava Ball



Clockthief
Male
Aquarius
72
43
Oracle
Steal
Abandon
Halve MP
Jump+2

Musk Rod

Golden Hairpin
Black Robe
Magic Gauntlet

Pray Faith, Foxbird, Confusion Song, Dispel Magic
Steal Helmet, Leg Aim

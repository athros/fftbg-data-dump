Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Female
Pisces
73
65
Archer
Black Magic
Earplug
Equip Bow
Ignore Height

Lightning Bow

Headgear
Power Sleeve
Feather Boots

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 2, Flare



Roofiepops
Female
Aquarius
53
41
Lancer
Yin Yang Magic
PA Save
Doublehand
Move+1

Partisan

Diamond Helmet
Wizard Robe
Wizard Mantle

Level Jump8, Vertical Jump2
Life Drain, Pray Faith, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep



Upvla
Male
Pisces
70
57
Monk
Steal
MP Restore
Dual Wield
Teleport



Red Hood
Clothes
Small Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Steal Helmet, Steal Armor, Steal Weapon



Evewho
Female
Capricorn
44
42
Mime

Counter Magic
Martial Arts
Move+3



Headgear
Mystic Vest
Defense Ring

Mimic


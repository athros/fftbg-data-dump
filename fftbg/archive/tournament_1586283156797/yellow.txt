Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DavenIII
Male
Libra
66
60
Knight
Yin Yang Magic
HP Restore
Magic Attack UP
Waterwalking

Save the Queen
Genji Shield
Mythril Helmet
Gold Armor
Dracula Mantle

Shield Break, Speed Break, Mind Break, Night Sword
Blind, Life Drain, Zombie, Blind Rage, Foxbird, Paralyze



The Fryman
Female
Aquarius
53
55
Geomancer
Throw
Critical Quick
Equip Polearm
Fly

Dragon Whisker
Bronze Shield
Triangle Hat
Power Sleeve
Small Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Bomb, Sword



Daveb
Female
Leo
68
45
Ninja
White Magic
Regenerator
Equip Gun
Jump+3

Bestiary
Battle Folio
Green Beret
Mystic Vest
Battle Boots

Hammer, Dictionary
Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Shell, Wall, Esuna



Old Overholt
Female
Libra
57
51
Knight
Black Magic
Counter Tackle
Equip Knife
Levitate

Hidden Knife
Bronze Shield
Iron Helmet
Gold Armor
Reflect Ring

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break
Fire 3, Bolt 4, Ice

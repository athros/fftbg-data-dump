Player: !Black
Team: Black Team
Palettes: Black/Red



Maakur
Male
Libra
69
76
Time Mage
Steal
Speed Save
Equip Knife
Fly

Air Knife

Red Hood
Secret Clothes
Sprint Shoes

Haste, Slow, Slow 2, Float, Reflect, Demi, Meteor
Steal Heart, Leg Aim



Alacor
Male
Serpentarius
75
61
Knight
Black Magic
Counter Tackle
Attack UP
Ignore Height

Coral Sword
Diamond Shield
Circlet
Plate Mail
Feather Mantle

Weapon Break, Magic Break, Stasis Sword, Dark Sword
Fire 2, Bolt 3



Rico Flex
Female
Gemini
75
72
Oracle
Summon Magic
Parry
Sicken
Ignore Terrain

Iron Fan

Twist Headband
Wizard Outfit
Dracula Mantle

Blind, Life Drain, Pray Faith, Silence Song, Foxbird
Moogle, Titan, Golem, Carbunkle, Bahamut, Odin, Leviathan, Lich



Zeroroute
Male
Aries
78
68
Wizard
Talk Skill
Abandon
Monster Talk
Waterwalking

Wizard Rod

Cachusha
Brigandine
Wizard Mantle

Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 4, Empower, Frog, Death
Invitation, Persuade, Preach, Negotiate, Mimic Daravon, Refute

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Solomongrundy85
Male
Taurus
64
50
Ninja
Punch Art
Absorb Used MP
Equip Bow
Ignore Terrain

Cross Bow
Gastrafitis
Flash Hat
Brigandine
Wizard Mantle

Shuriken, Bomb, Spear
Chakra, Revive, Seal Evil



OneHundredFists
Female
Libra
78
72
Time Mage
White Magic
Regenerator
Equip Sword
Waterwalking

Defender

Red Hood
Silk Robe
Spike Shoes

Haste 2, Slow, Stop, Immobilize, Quick, Demi 2
Cure 4, Raise 2, Reraise, Shell, Shell 2, Esuna, Holy



TheMM42
Female
Capricorn
72
72
Thief
Elemental
Faith Save
Dual Wield
Move+1

Spell Edge
Ninja Edge
Thief Hat
Adaman Vest
Red Shoes

Steal Heart, Steal Helmet, Steal Accessory, Steal Status, Leg Aim
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



CrownOfHorns
Monster
Scorpio
76
58
Tiamat








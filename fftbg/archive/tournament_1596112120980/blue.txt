Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ForgetToSmile
Female
Sagittarius
46
64
Samurai
Elemental
Dragon Spirit
Long Status
Waterbreathing

Muramasa

Iron Helmet
Mythril Armor
Jade Armlet

Asura, Koutetsu, Bizen Boat
Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



LDSkinny
Female
Virgo
53
67
Oracle
Summon Magic
Distribute
Sicken
Jump+2

Whale Whisker

Holy Miter
Linen Robe
Genji Gauntlet

Life Drain, Zombie, Silence Song, Confusion Song, Paralyze, Petrify, Dark Holy
Titan, Carbunkle, Leviathan, Silf, Fairy



Thaetreis
Male
Pisces
47
65
Ninja
Steal
Mana Shield
Equip Gun
Jump+3

Papyrus Codex
Battle Folio
Leather Hat
Chain Vest
Wizard Mantle

Shuriken
Steal Heart, Steal Weapon, Steal Status



Douchetron
Female
Serpentarius
80
68
Thief
Talk Skill
Counter Tackle
Halve MP
Jump+2

Iron Sword

Green Beret
Black Costume
Feather Mantle

Gil Taking, Steal Heart, Steal Helmet
Threaten, Solution, Rehabilitate

Player: !Black
Team: Black Team
Palettes: Black/Red



Lanshaft
Female
Aquarius
73
74
Dancer
Summon Magic
MA Save
Equip Sword
Lava Walking

Ragnarok

Cachusha
Brigandine
Bracer

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance
Ramuh, Golem, Carbunkle



Ant Ihistamine
Male
Libra
65
68
Summoner
Draw Out
Arrow Guard
Doublehand
Move-HP Up

Rainbow Staff

Headgear
Black Robe
Battle Boots

Moogle, Golem, Carbunkle, Leviathan, Silf
Asura, Kikuichimoji



Kai Shee
Female
Aquarius
55
65
Mediator
Elemental
Arrow Guard
Equip Bow
Fly

Lightning Bow

Golden Hairpin
Chameleon Robe
Vanish Mantle

Preach, Insult, Refute
Pitfall, Hell Ivy, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



KupoKel
Female
Sagittarius
50
71
Time Mage
Item
Speed Save
Doublehand
Retreat

Healing Staff

Twist Headband
Wizard Robe
108 Gems

Haste, Slow 2, Stop, Immobilize, Stabilize Time, Meteor
Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down

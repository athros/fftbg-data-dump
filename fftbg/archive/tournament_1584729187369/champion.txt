Player: !zChamp
Team: Champion Team
Palettes: White/Blue



LOKITHUS
Male
Aquarius
58
58
Monk
White Magic
Hamedo
Doublehand
Swim



Black Hood
Secret Clothes
Spike Shoes

Spin Fist, Chakra, Revive, Seal Evil
Raise, Raise 2, Shell, Wall, Esuna, Holy



Bcb Bona
Male
Taurus
65
52
Knight
Summon Magic
Blade Grasp
Equip Gun
Move-HP Up

Blast Gun
Mythril Shield
Leather Helmet
Plate Mail
Genji Gauntlet

Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Moogle, Ramuh, Bahamut, Odin, Leviathan, Salamander, Fairy, Lich, Cyclops, Zodiac



Numbersborne
Female
Leo
58
59
Oracle
Summon Magic
Arrow Guard
Maintenance
Retreat

Gokuu Rod

Twist Headband
Light Robe
Diamond Armlet

Blind, Poison, Life Drain, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Moogle, Shiva, Golem, Silf, Fairy, Lich, Cyclops



HaplessOne
Male
Taurus
49
51
Archer
Steal
Caution
Doublehand
Fly

Perseus Bow

Golden Hairpin
Brigandine
Cursed Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Steal Heart, Arm Aim

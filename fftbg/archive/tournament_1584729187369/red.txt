Player: !Red
Team: Red Team
Palettes: Red/Brown



Friendsquirrel
Female
Sagittarius
78
61
Mediator
Elemental
Counter
Beastmaster
Retreat

Cultist Dagger

Black Hood
Chameleon Robe
Small Mantle

Persuade, Death Sentence, Negotiate, Mimic Daravon, Refute
Pitfall, Hell Ivy, Local Quake, Sand Storm, Blizzard, Lava Ball



Deathmaker06
Female
Virgo
43
50
Knight
Basic Skill
Hamedo
Magic Attack UP
Jump+2

Giant Axe
Aegis Shield
Platinum Helmet
Plate Mail
Elf Mantle

Shield Break, Magic Break, Power Break, Justice Sword, Dark Sword
Dash, Tickle, Yell, Cheer Up, Fury



Ququroon
Female
Aries
36
72
Geomancer
Draw Out
Brave Up
Magic Defense UP
Waterbreathing

Battle Axe
Round Shield
Green Beret
Light Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Muramasa



Kingchadking
Male
Gemini
51
70
Time Mage
Summon Magic
Counter Flood
Equip Armor
Jump+1

Papyrus Codex

Mythril Helmet
Platinum Armor
Feather Mantle

Haste, Haste 2, Slow 2, Float, Stabilize Time
Moogle, Shiva, Ifrit, Bahamut, Fairy

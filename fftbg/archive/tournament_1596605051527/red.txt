Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Female
Aries
41
60
Calculator
Yin Yang Magic
Counter Magic
Equip Polearm
Jump+3

Partisan

Thief Hat
Rubber Costume
108 Gems

CT, Height, 5, 3
Poison, Life Drain, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze



SuppleThink
Male
Cancer
44
80
Bard
White Magic
Hamedo
Dual Wield
Move+3

Perseus Bow

Flash Hat
Linen Cuirass
Leather Mantle

Angel Song, Magic Song, Hydra Pit
Cure, Raise, Raise 2, Regen, Esuna



Resjudicata3
Female
Pisces
65
77
Summoner
White Magic
Brave Save
Magic Attack UP
Levitate

Bestiary

Holy Miter
Linen Robe
Elf Mantle

Moogle, Ramuh, Salamander
Cure 4, Raise, Raise 2, Shell 2, Esuna



GenZealot
Male
Gemini
70
66
Ninja
Draw Out
Critical Quick
Defense UP
Levitate

Sasuke Knife
Spell Edge
Flash Hat
Mythril Vest
Feather Mantle

Shuriken, Staff, Dictionary
Asura, Bizen Boat, Heaven's Cloud, Kikuichimoji

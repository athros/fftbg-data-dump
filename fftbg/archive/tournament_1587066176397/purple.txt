Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



OperatorTheory
Female
Gemini
79
79
Mediator
Dance
Counter Magic
Halve MP
Waterbreathing

Bestiary

Green Beret
Brigandine
Defense Armlet

Praise, Preach, Solution, Insult, Refute, Rehabilitate
Witch Hunt, Wiznaibus, Disillusion



KaLam1ty
Monster
Leo
40
43
Explosive










LazarusAjin
Female
Cancer
53
43
Archer
Dance
Catch
Long Status
Teleport

Hunting Bow
Round Shield
Feather Hat
Leather Outfit
Leather Mantle

Charge+10
Witch Hunt, Slow Dance, Last Dance, Obsidian Blade, Nether Demon



BuffaloCrunch
Male
Libra
56
47
Wizard
Charge
Speed Save
Equip Sword
Waterwalking

Defender

Red Hood
White Robe
Angel Ring

Fire 3, Fire 4, Bolt, Bolt 4, Ice, Ice 2, Ice 4
Charge+1, Charge+3

Final Bets: yellow - 18 bets for 7,051G (13.8%, x6.22); champion - 17 bets for 43,891G (86.2%, x0.16)

yellow bets:
HaplessOne: 1,019G (14.5%, 2,038G)
ColetteMSLP: 1,000G (14.2%, 26,512G)
BirbBrainsBot: 1,000G (14.2%, 14,983G)
alrightbye: 600G (8.5%, 36,201G)
Oreo_Pizza: 500G (7.1%, 6,880G)
PotionDweller: 457G (6.5%, 457G)
ZephyrTempest: 400G (5.7%, 3,971G)
rico_flex: 333G (4.7%, 24,236G)
TheRylander: 300G (4.3%, 2,581G)
EnemyController: 300G (4.3%, 147,633G)
Evewho: 200G (2.8%, 20,482G)
Estan_AD: 200G (2.8%, 1,991G)
getthemoneyz: 190G (2.7%, 588,305G)
LivingHitokiri: 152G (2.2%, 152G)
Arkreaver: 100G (1.4%, 14,401G)
L_M_D: 100G (1.4%, 100G)
Escorian: 100G (1.4%, 9,252G)
Realitydown: 100G (1.4%, 11,352G)

champion bets:
sinnyil2: 28,119G (64.1%, 56,239G)
gooseyourself: 5,000G (11.4%, 19,433G)
XalSmellsBad: 2,000G (4.6%, 27,187G)
ewan_e: 2,000G (4.6%, 9,184G)
Odin007_88: 1,893G (4.3%, 1,893G)
mesmaster: 1,532G (3.5%, 1,532G)
Laserman1000: 649G (1.5%, 51,849G)
notazzie: 500G (1.1%, 1,480G)
jakeduhhsnake: 474G (1.1%, 474G)
vorap: 400G (0.9%, 18,654G)
R_Raynos: 352G (0.8%, 352G)
ungabunga_bot: 333G (0.8%, 267,443G)
Creggers: 200G (0.5%, 918G)
nifboy: 135G (0.3%, 3,390G)
Banzem: 104G (0.2%, 104G)
Firesheath: 100G (0.2%, 12,830G)
datadrivenbot: 100G (0.2%, 2,754G)

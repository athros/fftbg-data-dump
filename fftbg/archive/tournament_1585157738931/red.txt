Player: !Red
Team: Red Team
Palettes: Red/Brown



MoonSlayerRS
Female
Aries
53
75
Priest
Black Magic
Regenerator
Equip Shield
Fly

Oak Staff
Ice Shield
Holy Miter
Silk Robe
Red Shoes

Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect 2, Esuna
Fire 3, Fire 4, Bolt, Ice, Ice 2



Oogthecaveman
Female
Virgo
61
36
Knight
Dance
MP Restore
Doublehand
Levitate

Long Sword

Gold Helmet
Bronze Armor
Genji Gauntlet

Armor Break, Weapon Break
Witch Hunt, Polka Polka, Dragon Pit



Maakur
Male
Aquarius
49
70
Knight
Sing
Counter Tackle
Short Status
Waterbreathing

Ragnarok

Circlet
Black Robe
Angel Ring

Weapon Break, Magic Break, Speed Break, Power Break, Night Sword
Angel Song, Cheer Song, Battle Song, Magic Song, Last Song, Diamond Blade



Arku E
Female
Sagittarius
70
69
Thief
Throw
Distribute
Magic Defense UP
Jump+3

Blind Knife

Flash Hat
Mythril Vest
Spike Shoes

Steal Helmet, Arm Aim, Leg Aim
Shuriken, Bomb, Sword, Ninja Sword

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lanshaft
Female
Virgo
53
48
Mediator
Draw Out
Counter Magic
Dual Wield
Ignore Height

Mythril Gun
Romanda Gun
Holy Miter
Silk Robe
Small Mantle

Praise, Threaten, Solution, Refute, Rehabilitate
Asura, Kiyomori, Muramasa



Porkchop5158
Male
Sagittarius
52
61
Oracle
White Magic
Damage Split
Equip Shield
Jump+3

Bestiary
Kaiser Plate
Leather Hat
Wizard Outfit
Magic Ring

Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy
Cure 3, Cure 4, Reraise, Protect, Protect 2, Shell, Shell 2



Dantayystv
Female
Capricorn
58
60
Knight
White Magic
Speed Save
Maintenance
Move+2

Slasher
Genji Shield
Genji Helmet
Genji Armor
Sprint Shoes

Shield Break, Speed Break, Justice Sword, Dark Sword
Cure, Cure 2, Cure 3, Cure 4, Raise, Protect, Shell, Shell 2, Esuna



TeaTime29
Female
Libra
78
78
Priest
Item
Auto Potion
Long Status
Teleport 2

Wizard Staff

Green Beret
Black Costume
Reflect Ring

Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect, Shell, Holy
Potion, Hi-Ether, Holy Water, Remedy, Phoenix Down

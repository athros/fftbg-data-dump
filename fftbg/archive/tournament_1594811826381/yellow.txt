Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



J2DaBibbles
Female
Sagittarius
48
60
Samurai
Throw
Mana Shield
Maintenance
Jump+1

Javelin

Cross Helmet
Black Robe
Magic Gauntlet

Asura, Murasame
Bomb, Wand



NIghtdew14
Female
Virgo
43
55
Squire
Steal
Abandon
Sicken
Fly

Coral Sword
Aegis Shield
Leather Helmet
Clothes
Dracula Mantle

Accumulate, Heal, Yell, Cheer Up, Fury
Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Steal Status, Arm Aim



JethroThrul
Female
Capricorn
69
50
Dancer
Throw
Catch
Equip Knife
Move-HP Up

Zorlin Shape

Flash Hat
Adaman Vest
Power Wrist

Nameless Dance, Nether Demon, Dragon Pit
Shuriken, Bomb, Stick, Dictionary



Mattheus
Male
Pisces
63
80
Calculator
Labyrinth Skill
Mana Shield
Magic Defense UP
Ignore Terrain

Thunder Rod

Cachusha
Linen Cuirass
Jade Armlet

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power

Player: !Red
Team: Red Team
Palettes: Red/Brown



CassiePhoenix
Female
Leo
48
59
Samurai
Battle Skill
Sunken State
Magic Attack UP
Ignore Terrain

Kiyomori

Mythril Helmet
Plate Mail
Battle Boots

Bizen Boat
Head Break, Shield Break, Weapon Break, Speed Break, Power Break



Powergems
Female
Taurus
82
78
Ninja
Draw Out
Arrow Guard
Equip Gun
Move+1

Romanda Gun
Blast Gun
Triangle Hat
Chain Vest
Wizard Mantle

Shuriken, Bomb, Staff
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



ForagerCats
Female
Leo
62
51
Priest
Draw Out
HP Restore
Defense UP
Move-MP Up

Wizard Staff

Black Hood
White Robe
Dracula Mantle

Cure, Cure 2, Raise, Regen, Protect 2, Shell, Wall, Esuna
Bizen Boat, Murasame, Kikuichimoji



Ar Tactic
Female
Capricorn
51
70
Chemist
Talk Skill
Arrow Guard
Equip Axe
Jump+2

Scorpion Tail

Red Hood
Secret Clothes
Defense Armlet

Potion, Echo Grass, Remedy
Threaten, Solution, Negotiate

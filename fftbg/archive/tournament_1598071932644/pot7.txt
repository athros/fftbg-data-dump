Final Bets: red - 10 bets for 6,240G (64.1%, x0.56); white - 7 bets for 3,490G (35.9%, x1.79)

red bets:
BirbBrainsBot: 1,000G (16.0%, 119,570G)
dogsandcatsand: 1,000G (16.0%, 28,905G)
Sairentozon7: 1,000G (16.0%, 25,605G)
gorgewall: 856G (13.7%, 856G)
jjhull3: 765G (12.3%, 765G)
loveyouallfriends: 500G (8.0%, 52,701G)
maximumcrit: 500G (8.0%, 3,950G)
cloudycube: 277G (4.4%, 544G)
getthemoneyz: 242G (3.9%, 1,704,017G)
AllInBot: 100G (1.6%, 100G)

white bets:
CT_5_Holy: 1,266G (36.3%, 5,066G)
JCBooBot: 1,000G (28.7%, 16,669G)
resjudicata3: 376G (10.8%, 376G)
Tarheels218: 316G (9.1%, 316G)
datadrivenbot: 200G (5.7%, 63,527G)
JCBoorgo: 200G (5.7%, 1,135G)
PuzzleSecretary: 132G (3.8%, 132G)

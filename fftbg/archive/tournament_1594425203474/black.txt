Player: !Black
Team: Black Team
Palettes: Black/Red



Shalloween
Female
Taurus
55
65
Time Mage
Item
Faith Save
Short Charge
Ignore Terrain

White Staff

Green Beret
Power Sleeve
Rubber Shoes

Haste 2, Float, Reflect, Quick, Meteor
Potion, Antidote, Holy Water, Phoenix Down



HaateXIII
Female
Scorpio
74
74
Summoner
Battle Skill
Parry
Sicken
Move+3

Ice Rod

Golden Hairpin
Chameleon Robe
Leather Mantle

Moogle, Ifrit, Golem, Carbunkle, Salamander, Silf, Fairy, Cyclops
Weapon Break, Magic Break, Justice Sword, Dark Sword



JoeykinsX
Female
Pisces
45
60
Summoner
Yin Yang Magic
Caution
Secret Hunt
Jump+1

Rainbow Staff

Feather Hat
Linen Robe
Cursed Ring

Moogle, Carbunkle, Bahamut, Silf
Poison, Pray Faith, Zombie, Confusion Song, Dispel Magic



Redmage4evah
Male
Capricorn
68
67
Samurai
White Magic
Catch
Attack UP
Fly

Heaven's Cloud

Crystal Helmet
Silk Robe
Magic Gauntlet

Asura, Koutetsu, Murasame, Kiyomori, Muramasa
Cure, Cure 2, Protect

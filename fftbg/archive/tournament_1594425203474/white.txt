Player: !White
Team: White Team
Palettes: White/Blue



Miku Shikhu
Female
Leo
42
56
Monk
Battle Skill
Dragon Spirit
Equip Sword
Move+1

Platinum Sword

Black Hood
Mythril Vest
Defense Armlet

Secret Fist, Purification, Chakra, Revive
Head Break, Armor Break, Magic Break, Speed Break, Justice Sword



Rislyeu
Male
Gemini
45
43
Time Mage
Battle Skill
Caution
Short Charge
Retreat

Ivory Rod

Black Hood
Light Robe
Diamond Armlet

Haste, Haste 2, Slow 2, Immobilize, Reflect, Stabilize Time
Armor Break, Shield Break, Weapon Break, Mind Break, Stasis Sword



Roofiepops
Male
Virgo
69
39
Ninja
Punch Art
Counter Flood
Halve MP
Move-MP Up

Short Edge
Ninja Edge
Ribbon
Brigandine
Red Shoes

Shuriken
Secret Fist



Neerrm
Male
Capricorn
57
80
Mime

Auto Potion
Monster Talk
Jump+2



Holy Miter
Power Sleeve
Wizard Mantle

Mimic


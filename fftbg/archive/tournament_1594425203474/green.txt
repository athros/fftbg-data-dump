Player: !Green
Team: Green Team
Palettes: Green/White



Maddrave09
Male
Libra
47
79
Knight
Charge
Damage Split
Sicken
Move-HP Up

Ancient Sword
Round Shield
Gold Helmet
Carabini Mail
Setiemson

Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword
Charge+1, Charge+2, Charge+5



MetaCosmos
Female
Serpentarius
57
47
Calculator
Ranch Skill
MP Restore
Short Charge
Move-MP Up

Papyrus Codex

Red Hood
Secret Clothes
Defense Armlet

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor



Seaweed B
Female
Libra
71
65
Calculator
Black Magic
Counter Magic
Dual Wield
Move-HP Up

Battle Folio
Papyrus Codex
Headgear
Black Costume
N-Kai Armlet

CT, 3
Fire, Fire 3, Ice 2, Death, Flare



TinchoT
Male
Taurus
63
61
Ninja
White Magic
MA Save
Magic Attack UP
Ignore Height

Flame Whip
Flame Whip
Headgear
Leather Outfit
Angel Ring

Ninja Sword, Stick, Wand
Cure, Cure 3, Raise, Raise 2, Wall, Esuna

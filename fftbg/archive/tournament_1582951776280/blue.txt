Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



FroDog5050
Male
Gemini
53
80
Calculator
Yin Yang Magic
Counter
Maintenance
Ignore Terrain

Dragon Rod

Leather Hat
Robe of Lords
Genji Gauntlet

CT, Height, Prime Number, 3
Spell Absorb, Pray Faith, Zombie, Foxbird



PezOfDoom
Male
Leo
79
76
Knight
Item
Mana Shield
Martial Arts
Retreat

Ragnarok

Mythril Helmet
Wizard Robe
Magic Gauntlet

Armor Break, Power Break, Mind Break
Potion, Eye Drop, Holy Water, Remedy, Phoenix Down



SlavetotheCaffeine
Female
Leo
41
72
Mediator
Battle Skill
Mana Shield
Concentrate
Waterbreathing

Stone Gun

Red Hood
Wizard Robe
Small Mantle

Invitation, Persuade, Preach, Mimic Daravon, Refute, Rehabilitate
Armor Break, Shield Break, Weapon Break, Speed Break, Power Break



Ominnous
Male
Leo
77
79
Lancer
Battle Skill
Earplug
Equip Polearm
Waterbreathing

Holy Lance
Genji Shield
Crystal Helmet
Plate Mail
Spike Shoes

Level Jump2, Vertical Jump4
Weapon Break, Justice Sword

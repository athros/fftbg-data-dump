Player: !Red
Team: Red Team
Palettes: Red/Brown



Alc Trinity
Male
Virgo
51
52
Geomancer
Punch Art
Earplug
Martial Arts
Jump+2


Kaiser Plate
Green Beret
Wizard Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Secret Fist, Revive, Seal Evil



Galkife
Female
Pisces
63
68
Archer
Jump
Distribute
Defend
Waterwalking

Blaze Gun
Venetian Shield
Black Hood
Earth Clothes
Wizard Mantle

Charge+1, Charge+2, Charge+4
Level Jump4, Vertical Jump8



AlenaZarek
Female
Aquarius
66
45
Mediator
Time Magic
Faith Up
Beastmaster
Swim

Blind Knife

Green Beret
Earth Clothes
Spike Shoes

Threaten, Solution, Negotiate, Mimic Daravon
Haste 2, Slow, Immobilize, Quick, Demi, Stabilize Time



OmnibotGamma
Female
Aquarius
73
73
Oracle
Item
Distribute
Maintenance
Retreat

Battle Bamboo

Thief Hat
Black Robe
Sprint Shoes

Blind, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze
Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



Mushywaffles
Female
Aries
45
57
Monk
Black Magic
Catch
Magic Defense UP
Move+1



Green Beret
Secret Clothes
Jade Armlet

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Bolt 2, Bolt 3, Ice 2, Flare



HaateXIII
Male
Virgo
55
46
Monk
Item
Dragon Spirit
Throw Item
Swim



Leather Hat
Leather Outfit
Leather Mantle

Spin Fist, Wave Fist, Secret Fist, Purification, Revive
Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Holy Water, Phoenix Down



CosmicTactician
Male
Scorpio
61
65
Summoner
Draw Out
Counter Magic
Equip Shield
Jump+2

White Staff
Crystal Shield
Green Beret
Linen Robe
Dracula Mantle

Shiva, Ifrit, Golem, Carbunkle, Leviathan
Koutetsu, Murasame, Muramasa



Gikokiko
Male
Cancer
71
37
Knight
Yin Yang Magic
Earplug
Dual Wield
Move+3

Defender
Slasher
Barbuta
Chain Mail
Cherche

Head Break, Armor Break, Weapon Break, Mind Break, Stasis Sword
Blind, Spell Absorb, Zombie, Foxbird, Dispel Magic, Sleep, Dark Holy

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



ALY327
Male
Virgo
56
49
Mime

Sunken State
Defend
Ignore Height



Red Hood
Chameleon Robe
Wizard Mantle

Mimic




UmaiJam
Female
Gemini
57
80
Summoner
Time Magic
Faith Up
Short Charge
Move-MP Up

Dragon Rod

Red Hood
Wizard Robe
Elf Mantle

Moogle, Ramuh, Titan, Golem, Carbunkle, Leviathan, Salamander, Lich
Haste, Stop, Immobilize, Float, Demi 2



Marin1987
Monster
Aquarius
48
66
Red Dragon










Sinnyil2
Male
Aquarius
66
51
Knight
White Magic
PA Save
Beastmaster
Teleport 2

Mythril Sword
Diamond Shield
Circlet
Light Robe
Chantage

Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Cure, Cure 4, Raise, Reraise, Protect 2, Wall, Holy

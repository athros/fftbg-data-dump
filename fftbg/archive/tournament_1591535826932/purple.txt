Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Edgehead62888
Female
Aries
74
56
Squire
Draw Out
Absorb Used MP
Magic Attack UP
Levitate

Broad Sword
Buckler
Golden Hairpin
Chain Vest
Magic Gauntlet

Accumulate, Dash, Heal, Tickle, Yell, Wish
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



CosmicTactician
Female
Libra
44
67
Dancer
White Magic
Critical Quick
Concentrate
Move-HP Up

Blind Knife

Feather Hat
Wizard Robe
Elf Mantle

Witch Hunt, Slow Dance, Disillusion
Cure, Raise, Regen, Wall, Esuna, Holy



Gorgewall
Male
Pisces
56
68
Knight
Steal
Catch
Halve MP
Waterbreathing

Ragnarok
Buckler
Crystal Helmet
Maximillian
Magic Gauntlet

Armor Break, Power Break, Stasis Sword, Dark Sword, Surging Sword
Gil Taking, Steal Helmet, Steal Accessory



IphoneDarkness
Female
Gemini
79
62
Priest
Talk Skill
Arrow Guard
Monster Talk
Jump+3

Rainbow Staff

Headgear
Linen Robe
Magic Gauntlet

Cure, Cure 2, Raise, Raise 2, Reraise, Protect, Protect 2, Esuna, Holy
Threaten, Refute, Rehabilitate

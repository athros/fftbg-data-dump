Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Nosablake
Female
Scorpio
68
47
Lancer
Black Magic
PA Save
Equip Armor
Move+2

Obelisk
Kaiser Plate
Crystal Helmet
Mythril Armor
Angel Ring

Level Jump2, Vertical Jump2
Fire, Fire 2, Fire 3, Bolt, Bolt 3, Ice 2, Empower



Wackman3000
Male
Pisces
58
44
Samurai
Yin Yang Magic
Damage Split
Halve MP
Move+3

Mythril Spear

Gold Helmet
Light Robe
Jade Armlet

Koutetsu, Heaven's Cloud, Muramasa, Masamune
Blind, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep



IronMongrel
Male
Aquarius
46
70
Bard
White Magic
Counter
Long Status
Ignore Height

Lightning Bow

Twist Headband
Diamond Armor
Feather Boots

Angel Song, Life Song, Cheer Song, Sky Demon, Hydra Pit
Cure 3, Raise, Esuna, Holy



NBD Rab
Male
Cancer
55
59
Knight
Time Magic
Catch
Defense UP
Swim

Chaos Blade

Leather Helmet
Genji Armor
Wizard Mantle

Head Break, Armor Break, Shield Break, Speed Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Haste, Slow, Immobilize, Reflect, Stabilize Time

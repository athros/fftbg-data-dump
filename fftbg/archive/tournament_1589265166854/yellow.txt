Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Stukerk
Male
Libra
68
71
Monk
Sing
Abandon
Doublehand
Jump+3



Red Hood
Adaman Vest
Feather Mantle

Spin Fist, Wave Fist, Secret Fist, Purification, Revive
Cheer Song, Battle Song, Diamond Blade, Sky Demon



Dogsandcatsand
Male
Cancer
80
73
Wizard
Battle Skill
Damage Split
Equip Shield
Jump+1

Thunder Rod
Aegis Shield
Thief Hat
Linen Robe
Angel Ring

Fire 2, Ice, Ice 2, Ice 4, Frog
Head Break, Speed Break, Justice Sword



SirCherish
Male
Virgo
57
47
Thief
Talk Skill
Hamedo
Equip Armor
Ignore Height

Spell Edge

Bronze Helmet
Linen Robe
Feather Mantle

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Insult, Mimic Daravon, Refute, Rehabilitate



Lali Lulelo
Male
Scorpio
52
54
Ninja
Punch Art
Faith Up
Equip Axe
Swim

Short Edge
Wizard Staff
Black Hood
Brigandine
Rubber Shoes

Bomb, Knife, Wand
Spin Fist, Wave Fist, Revive

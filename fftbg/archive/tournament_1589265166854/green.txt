Player: !Green
Team: Green Team
Palettes: Green/White



ApplesauceBoss
Female
Taurus
42
79
Calculator
Bio
Parry
Equip Bow
Move-MP Up

Night Killer

Barbuta
Crystal Mail
Spike Shoes

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



R Raynos
Male
Capricorn
44
63
Oracle
Black Magic
Abandon
Beastmaster
Move+2

Cypress Rod

Triangle Hat
Brigandine
Small Mantle

Blind, Poison, Spell Absorb, Zombie, Blind Rage
Fire 2, Bolt, Ice 3, Ice 4, Flare



Waterwatereverywhere
Male
Sagittarius
68
44
Lancer
Sing
Auto Potion
Equip Gun
Lava Walking

Battle Folio
Platinum Shield
Cross Helmet
Linen Cuirass
Defense Ring

Level Jump4, Vertical Jump8
Angel Song, Battle Song



Estan AD
Male
Pisces
53
73
Knight
Basic Skill
Dragon Spirit
Equip Polearm
Ignore Terrain

Mythril Spear
Mythril Shield
Bronze Helmet
Gold Armor
Spike Shoes

Armor Break, Shield Break, Speed Break, Surging Sword
Throw Stone, Heal, Tickle, Cheer Up

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Maakur
Female
Sagittarius
63
54
Oracle
Talk Skill
Counter
Secret Hunt
Move-HP Up

Papyrus Codex

Feather Hat
Chameleon Robe
Defense Ring

Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Dispel Magic
Threaten, Death Sentence, Insult, Refute, Rehabilitate



Evdoggity
Male
Taurus
65
52
Squire
Talk Skill
Damage Split
Sicken
Lava Walking

Iron Sword
Diamond Shield
Golden Hairpin
Leather Outfit
Angel Ring

Heal, Tickle, Cheer Up, Fury, Wish
Persuade, Praise, Preach, Solution, Insult, Refute



StealthModeLocke
Female
Taurus
53
44
Mediator
Throw
MA Save
Defend
Jump+2

Romanda Gun

Holy Miter
Chain Vest
Magic Ring

Persuade, Solution, Insult, Negotiate, Mimic Daravon, Refute
Shuriken, Bomb, Stick



Helpimabug
Female
Leo
71
35
Oracle
Charge
Speed Save
Equip Shield
Move+1

Papyrus Codex
Crystal Shield
Triangle Hat
Silk Robe
Cursed Ring

Poison, Life Drain, Pray Faith, Zombie, Foxbird, Dispel Magic, Sleep, Petrify, Dark Holy
Charge+1, Charge+3, Charge+10

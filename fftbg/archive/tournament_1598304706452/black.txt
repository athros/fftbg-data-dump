Player: !Black
Team: Black Team
Palettes: Black/Red



Lord Gwarth
Female
Aries
68
47
Wizard
Talk Skill
Brave Save
Short Status
Jump+2

Main Gauche

Triangle Hat
Secret Clothes
Defense Ring

Fire 4, Bolt, Ice, Ice 2, Ice 3, Frog, Death
Invitation, Persuade, Threaten, Solution, Negotiate



Zeroroute
Female
Libra
69
45
Chemist
Punch Art
Abandon
Long Status
Ignore Terrain

Mythril Knife

Black Hood
Secret Clothes
Defense Armlet

Potion, Hi-Potion, X-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down
Wave Fist, Earth Slash, Purification



Butterbelljedi
Female
Cancer
46
51
Knight
Talk Skill
Auto Potion
Long Status
Move+3

Slasher
Gold Shield
Leather Helmet
Chameleon Robe
Leather Mantle

Armor Break, Shield Break, Weapon Break, Mind Break, Dark Sword
Persuade, Praise, Threaten, Solution, Insult, Refute



Forkmore
Female
Scorpio
46
66
Wizard
Jump
Distribute
Magic Defense UP
Teleport

Cute Bag

Thief Hat
Secret Clothes
Wizard Mantle

Fire, Fire 2, Bolt, Bolt 3, Bolt 4, Ice, Empower
Level Jump5, Vertical Jump2

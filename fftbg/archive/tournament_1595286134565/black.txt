Player: !Black
Team: Black Team
Palettes: Black/Red



Kronikle
Female
Aquarius
75
79
Summoner
Basic Skill
PA Save
Short Charge
Jump+2

Thunder Rod

Flash Hat
Silk Robe
Leather Mantle

Moogle, Ifrit, Titan, Carbunkle, Lich
Dash, Cheer Up, Fury, Wish



Dogsandcatsand
Male
Sagittarius
49
61
Bard
Summon Magic
Blade Grasp
Short Status
Fly

Fairy Harp

Holy Miter
Maximillian
Diamond Armlet

Angel Song, Life Song, Battle Song, Diamond Blade, Space Storage
Moogle, Shiva, Carbunkle, Bahamut, Fairy



Aldrammech
Female
Scorpio
70
66
Monk
Time Magic
Abandon
Defend
Jump+3



Black Hood
Leather Outfit
Elf Mantle

Pummel, Wave Fist, Purification, Revive
Haste, Slow 2



LLight
Male
Scorpio
62
76
Samurai
Throw
Counter
Equip Sword
Lava Walking

Ragnarok

Iron Helmet
Bronze Armor
Sprint Shoes

Bizen Boat, Murasame, Kiyomori
Axe

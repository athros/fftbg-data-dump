Player: !Red
Team: Red Team
Palettes: Red/Brown



TheChainNerd
Female
Serpentarius
71
53
Wizard
Item
Arrow Guard
Beastmaster
Teleport 2

Orichalcum

Flash Hat
Silk Robe
Defense Armlet

Fire 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Flare
Potion, Hi-Potion, Echo Grass, Phoenix Down



Dogsandcatsand
Male
Capricorn
63
49
Priest
Steal
Regenerator
Doublehand
Waterbreathing

Wizard Staff

Holy Miter
Brigandine
Defense Ring

Cure, Cure 2, Cure 3, Raise, Shell, Shell 2, Esuna, Magic Barrier
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Status



Miyokari
Female
Cancer
63
58
Monk
White Magic
Counter Magic
Maintenance
Ignore Height



Red Hood
Mystic Vest
Magic Ring

Spin Fist, Wave Fist, Earth Slash, Purification, Revive
Cure 2, Cure 3, Raise, Protect 2, Shell, Esuna, Holy



Zachara
Male
Sagittarius
51
58
Lancer
Talk Skill
Critical Quick
Monster Talk
Move+2

Mythril Spear
Flame Shield
Iron Helmet
Gold Armor
Battle Boots

Level Jump8, Vertical Jump3
Invitation, Threaten, Solution, Rehabilitate

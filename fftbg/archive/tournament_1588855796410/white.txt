Player: !White
Team: White Team
Palettes: White/Blue



FoeSquirrel
Male
Cancer
69
38
Mediator
Summon Magic
MP Restore
Doublehand
Jump+1

Blaze Gun

Holy Miter
Adaman Vest
Setiemson

Threaten, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Moogle, Ramuh, Titan, Golem, Leviathan, Salamander



Dexsana
Female
Aquarius
45
41
Ninja
Black Magic
Counter Tackle
Martial Arts
Move+3

Mythril Knife
Mythril Knife
Green Beret
Earth Clothes
Magic Gauntlet

Shuriken, Knife, Staff, Spear
Fire 3, Bolt 3, Bolt 4, Ice 4



StealthModeLocke
Monster
Scorpio
51
45
Black Goblin










Midori Ribbon
Male
Gemini
55
45
Summoner
Throw
Parry
Short Charge
Move-HP Up

Gold Staff

Flash Hat
Wizard Robe
Rubber Shoes

Titan, Carbunkle, Odin, Leviathan
Shuriken, Bomb

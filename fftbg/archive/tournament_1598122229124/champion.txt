Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Brokenknight201
Male
Aquarius
56
65
Monk
Time Magic
Arrow Guard
Equip Armor
Lava Walking



Platinum Helmet
Linen Cuirass
Bracer

Wave Fist, Purification, Chakra, Revive
Haste, Haste 2, Immobilize, Float, Reflect, Quick, Stabilize Time



Latebit
Monster
Gemini
50
78
Wyvern










HaateXIII
Monster
Scorpio
57
52
Dryad










MrFlabyo
Female
Capricorn
50
44
Chemist
Jump
Critical Quick
Equip Polearm
Swim

Mythril Spear

Triangle Hat
Black Costume
N-Kai Armlet

Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Soft, Remedy, Phoenix Down
Level Jump3, Vertical Jump8

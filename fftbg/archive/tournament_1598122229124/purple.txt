Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Redmage4evah
Female
Sagittarius
60
68
Lancer
Item
Speed Save
Defend
Teleport

Cypress Rod
Bronze Shield
Circlet
Gold Armor
Chantage

Level Jump2, Vertical Jump6
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Foxfaeez
Monster
Capricorn
60
37
Sekhret










Aldrammech
Male
Pisces
43
35
Calculator
Yin Yang Magic
Damage Split
Long Status
Retreat

Ivory Rod

Feather Hat
Wizard Robe
Reflect Ring

CT, 4, 3
Poison, Pray Faith, Foxbird, Petrify



Evdoggity
Male
Virgo
68
73
Archer
White Magic
MP Restore
Concentrate
Move-MP Up

Night Killer
Flame Shield
Barette
Brigandine
Feather Boots

Charge+3, Charge+7
Cure 3, Raise, Reraise, Shell, Shell 2

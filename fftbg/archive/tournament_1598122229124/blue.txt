Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MrFlabyo
Male
Virgo
47
78
Time Mage
White Magic
Sunken State
Doublehand
Levitate

Wizard Staff

Flash Hat
Black Robe
Red Shoes

Slow, Stop, Immobilize, Float, Meteor, Galaxy Stop
Cure 2, Raise, Regen, Protect 2, Shell 2, Wall, Esuna



DustBirdEX
Female
Virgo
72
43
Time Mage
White Magic
Brave Save
Secret Hunt
Retreat

Cypress Rod

Cachusha
Mythril Vest
Sprint Shoes

Haste, Slow 2, Stop, Float, Reflect, Quick, Demi, Stabilize Time
Cure, Cure 3, Raise, Regen, Protect, Shell 2, Wall, Esuna



DeathTaxesAndAnime
Female
Leo
69
55
Mediator
Item
Absorb Used MP
Dual Wield
Ignore Terrain

Romanda Gun
Stone Gun
Green Beret
Linen Robe
Red Shoes

Invitation, Death Sentence, Insult, Negotiate, Refute
Potion, Hi-Potion, Eye Drop, Holy Water, Phoenix Down



ANFz
Male
Virgo
73
79
Monk
Sing
Caution
Equip Axe
Move+1

Flail

Twist Headband
Earth Clothes
Bracer

Spin Fist, Earth Slash, Purification, Chakra, Revive
Life Song, Space Storage, Sky Demon

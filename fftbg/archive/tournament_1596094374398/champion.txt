Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Gooblz
Male
Scorpio
42
59
Archer
Sing
MP Restore
Concentrate
Levitate

Mythril Gun
Ice Shield
Twist Headband
Earth Clothes
Battle Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Life Song, Nameless Song, Diamond Blade, Sky Demon



Nekojin
Male
Scorpio
69
78
Mime

HP Restore
Martial Arts
Move+3



Headgear
Bronze Armor
Angel Ring

Mimic




Chuckolator
Female
Aquarius
79
70
Archer
Black Magic
Earplug
Doublehand
Lava Walking

Snipe Bow

Flash Hat
Clothes
N-Kai Armlet

Charge+1, Charge+4, Charge+5, Charge+7
Fire 2, Fire 3, Fire 4, Bolt 4, Ice 4



Prince Rogers Nelson
Female
Scorpio
76
58
Time Mage
Punch Art
Auto Potion
Sicken
Move+3

Musk Rod

Black Hood
Leather Outfit
Sprint Shoes

Haste, Slow, Slow 2, Stop, Reflect, Demi, Stabilize Time
Pummel, Earth Slash, Purification, Chakra, Revive

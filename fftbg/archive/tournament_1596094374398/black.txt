Player: !Black
Team: Black Team
Palettes: Black/Red



Firesheath
Male
Sagittarius
69
63
Knight
Talk Skill
Counter Tackle
Secret Hunt
Jump+2

Coral Sword
Mythril Shield
Iron Helmet
Platinum Armor
Red Shoes

Head Break, Shield Break, Dark Sword, Night Sword
Threaten, Preach, Refute



Josephiroth 143
Monster
Pisces
42
80
Red Dragon










Sinnyil2
Male
Capricorn
80
78
Squire
Battle Skill
Meatbone Slash
Equip Gun
Jump+2

Blaze Gun
Flame Shield
Crystal Helmet
Chain Mail
Leather Mantle

Dash, Throw Stone, Heal, Tickle, Wish
Armor Break



BaronHaynes
Female
Virgo
74
38
Archer
Punch Art
Meatbone Slash
Maintenance
Move+1

Poison Bow
Platinum Shield
Holy Miter
Brigandine
Magic Gauntlet

Charge+1, Charge+2, Charge+5, Charge+7, Charge+20
Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lanryte
Male
Libra
44
78
Geomancer
Throw
Brave Up
Short Status
Jump+2

Bizen Boat
Bronze Shield
Leather Hat
Clothes
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Shuriken



DecimalSix
Female
Cancer
59
45
Calculator
Yin Yang Magic
Sunken State
Short Charge
Move+3

Battle Bamboo

Twist Headband
Wizard Outfit
Genji Gauntlet

CT, Prime Number, 5, 3
Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze



IkariAllen
Female
Taurus
57
64
Priest
Summon Magic
Damage Split
Magic Attack UP
Waterwalking

Flail

Triangle Hat
Mystic Vest
Wizard Mantle

Raise, Shell, Shell 2, Esuna
Moogle, Ramuh, Golem, Leviathan, Lich, Cyclops



Esunos
Female
Cancer
73
68
Lancer
Basic Skill
Catch
Equip Bow
Waterwalking

Perseus Bow

Cross Helmet
Diamond Armor
Leather Mantle

Level Jump5, Vertical Jump8
Accumulate, Heal, Tickle, Yell, Wish

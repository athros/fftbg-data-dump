Player: !Green
Team: Green Team
Palettes: Green/White



PatSouI
Male
Sagittarius
65
67
Calculator
Robot Skill
Parry
Equip Gun
Waterwalking

Mythril Gun

Bronze Helmet
Platinum Armor
Diamond Armlet

Blue Magic
Destroy, Compress, Dispose, Repair



AzuriteReaction
Male
Cancer
58
59
Bard
Yin Yang Magic
Counter
Long Status
Jump+2

Mythril Bow

Triangle Hat
Mystic Vest
Magic Gauntlet

Cheer Song, Last Song, Space Storage
Poison, Spell Absorb



CJ Soulstar
Male
Pisces
47
73
Bard
Draw Out
Brave Up
Long Status
Waterbreathing

Fairy Harp

Red Hood
Wizard Outfit
Magic Ring

Angel Song, Nameless Song, Diamond Blade
Koutetsu, Bizen Boat, Murasame, Muramasa, Masamune



Lydian C
Female
Scorpio
71
63
Mediator
Yin Yang Magic
Catch
Dual Wield
Jump+3

Madlemgen
Battle Folio
Headgear
Leather Outfit
108 Gems

Persuade, Preach, Mimic Daravon
Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Dark Holy

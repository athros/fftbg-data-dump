Player: !Green
Team: Green Team
Palettes: Green/White



Riot Jayway
Male
Sagittarius
60
47
Archer
Item
Blade Grasp
Doublehand
Move+3

Hunting Bow

Gold Helmet
Chain Vest
Reflect Ring

Charge+1, Charge+3, Charge+4
Hi-Ether, Antidote, Phoenix Down



RjA0zcOQ96
Female
Scorpio
74
61
Calculator
Bird Skill
Absorb Used MP
Martial Arts
Move+2

Battle Folio
Flame Shield
Circlet
Diamond Armor
Sprint Shoes

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



RongRongArts
Female
Aquarius
65
67
Chemist
Basic Skill
Distribute
Magic Attack UP
Swim

Cute Bag

Triangle Hat
Mystic Vest
Chantage

Potion, Hi-Potion, X-Potion, Ether, Remedy, Phoenix Down
Tickle



Bad1dea
Male
Pisces
45
51
Archer
Throw
Meatbone Slash
Equip Axe
Levitate

Gold Staff
Crystal Shield
Thief Hat
Wizard Outfit
108 Gems

Charge+1, Charge+7
Spear

Player: !Red
Team: Red Team
Palettes: Red/Brown



Grininda
Monster
Leo
77
63
Coeurl










TasisSai
Male
Capricorn
48
50
Squire
White Magic
Parry
Short Status
Move-HP Up

Poison Bow
Aegis Shield
Triangle Hat
Chain Vest
Magic Ring

Accumulate, Throw Stone, Heal, Tickle, Yell, Fury, Wish
Raise, Reraise, Protect, Wall, Esuna



Solomongrundy85
Monster
Leo
74
69
Chocobo










Killth3kid
Male
Aries
51
77
Bard
Elemental
MP Restore
Sicken
Retreat

Fairy Harp

Golden Hairpin
Chain Mail
Leather Mantle

Cheer Song, Battle Song, Nameless Song, Hydra Pit
Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind, Lava Ball

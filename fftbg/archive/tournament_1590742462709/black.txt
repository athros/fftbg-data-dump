Player: !Black
Team: Black Team
Palettes: Black/Red



Maleblackfiora
Female
Capricorn
76
66
Lancer
Yin Yang Magic
Caution
Equip Axe
Teleport

Gold Staff
Diamond Shield
Crystal Helmet
Linen Robe
Germinas Boots

Level Jump4, Vertical Jump7
Blind, Poison, Pray Faith, Zombie, Confusion Song



YaBoy125
Male
Cancer
37
40
Knight
Jump
Parry
Short Charge
Jump+2

Coral Sword
Round Shield
Mythril Helmet
Crystal Mail
Jade Armlet

Armor Break, Speed Break, Power Break, Mind Break, Stasis Sword, Surging Sword
Level Jump5, Vertical Jump8



Daveb
Male
Leo
77
49
Ninja
Jump
Dragon Spirit
Equip Gun
Teleport

Bloody Strings
Battle Folio
Black Hood
Wizard Outfit
Cherche

Shuriken, Knife, Ninja Sword, Wand
Level Jump8, Vertical Jump5



ALY327
Female
Pisces
67
63
Lancer
Draw Out
Damage Split
Secret Hunt
Waterwalking

Partisan
Buckler
Iron Helmet
Genji Armor
Defense Ring

Level Jump8, Vertical Jump4
Bizen Boat, Muramasa

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Holyonline
Female
Virgo
76
49
Lancer
Punch Art
MA Save
Magic Defense UP
Jump+2

Battle Bamboo
Genji Shield
Mythril Helmet
Chameleon Robe
Sprint Shoes

Level Jump5, Vertical Jump5
Pummel, Wave Fist, Earth Slash, Revive, Seal Evil



YaBoy125
Male
Taurus
52
67
Knight
Summon Magic
Counter
Dual Wield
Fly

Defender
Blood Sword
Crystal Helmet
Reflect Mail
Jade Armlet

Head Break, Shield Break, Weapon Break, Magic Break, Speed Break
Moogle, Titan, Carbunkle, Bahamut, Odin, Fairy



LivingHitokiri
Monster
Pisces
40
68
Minotaur










NovaKnight21
Female
Taurus
51
43
Geomancer
Talk Skill
HP Restore
Defend
Move+3

Platinum Sword
Crystal Shield
Feather Hat
Power Sleeve
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Praise, Threaten, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate

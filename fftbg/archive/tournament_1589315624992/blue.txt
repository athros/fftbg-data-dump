Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Maakur
Male
Sagittarius
74
63
Knight
Punch Art
Sunken State
Attack UP
Teleport

Defender
Diamond Shield
Circlet
Platinum Armor
Power Wrist

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Surging Sword, Explosion Sword
Pummel, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil



J2DaBibbles
Female
Cancer
52
52
Calculator
Black Magic
Counter Flood
Attack UP
Jump+2

Flame Rod

Leather Hat
Adaman Vest
Magic Gauntlet

CT, Height, 5, 4, 3
Fire, Fire 3, Bolt 2



Vampyreinabox
Male
Gemini
58
54
Monk
Throw
Auto Potion
Short Charge
Jump+1



Twist Headband
Leather Outfit
Power Wrist

Pummel, Wave Fist, Purification
Axe



Lijarkh
Male
Leo
40
67
Monk
Talk Skill
Caution
Short Status
Levitate



Red Hood
Black Costume
Spike Shoes

Earth Slash, Secret Fist, Purification, Revive
Invitation, Insult, Negotiate, Rehabilitate

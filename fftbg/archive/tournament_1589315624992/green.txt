Player: !Green
Team: Green Team
Palettes: Green/White



LanseDM
Male
Leo
71
79
Priest
Steal
Parry
Halve MP
Move-MP Up

Healing Staff

Holy Miter
Clothes
Sprint Shoes

Cure 2, Cure 3, Raise, Raise 2, Protect, Esuna
Steal Helmet, Steal Status



Joewcarson
Male
Pisces
49
59
Knight
Item
Auto Potion
Throw Item
Jump+3

Blood Sword
Genji Shield
Iron Helmet
Light Robe
Bracer

Head Break, Shield Break, Magic Break, Stasis Sword, Justice Sword, Surging Sword
Potion, Hi-Potion, Hi-Ether, Soft, Phoenix Down



ExecutedGiraffe
Female
Gemini
76
40
Lancer
Time Magic
Counter
Concentrate
Ignore Terrain

Octagon Rod
Platinum Shield
Mythril Helmet
Black Robe
Defense Armlet

Level Jump2, Vertical Jump5
Haste, Stabilize Time, Meteor



Frozentomato
Male
Serpentarius
80
67
Monk
Elemental
Meatbone Slash
Maintenance
Move+2



Green Beret
Power Sleeve
Small Mantle

Pummel, Secret Fist, Purification, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Powergems
Female
Libra
67
77
Ninja
Summon Magic
MP Restore
Equip Sword
Levitate

Morning Star
Long Sword
Golden Hairpin
Adaman Vest
Small Mantle

Shuriken, Knife, Spear
Moogle, Shiva, Ramuh, Ifrit, Golem, Carbunkle, Bahamut



Zachara
Female
Leo
81
41
Mime

Meatbone Slash
Martial Arts
Move+2



Ribbon
Black Costume
Salty Rage

Mimic




Alc Trinity
Male
Virgo
42
74
Samurai
Summon Magic
Damage Split
Short Status
Waterwalking

Asura Knife

Platinum Helmet
Linen Robe
Germinas Boots

Kikuichimoji
Moogle, Ramuh, Golem, Carbunkle, Odin, Silf, Lich, Cyclops



Lastly
Male
Gemini
55
40
Geomancer
White Magic
Critical Quick
Equip Knife
Fly

Short Edge
Escutcheon
Green Beret
Silk Robe
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 3, Esuna

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lali Lulelo
Male
Pisces
52
56
Summoner
Black Magic
Counter Tackle
Short Charge
Jump+2

Rod

Black Hood
Linen Robe
Magic Gauntlet

Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Salamander
Fire 3, Fire 4, Ice, Frog



Gogofromtogo
Male
Cancer
65
57
Ninja
Elemental
PA Save
Beastmaster
Teleport

Hidden Knife
Morning Star
Green Beret
Clothes
Feather Mantle

Bomb
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Reinoe
Male
Virgo
74
71
Wizard
Elemental
Counter Magic
Equip Sword
Move+1

Save the Queen

Twist Headband
Silk Robe
Genji Gauntlet

Fire, Bolt, Ice, Ice 2, Empower
Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball



DaveStrider55
Male
Leo
73
61
Squire
Item
Mana Shield
Throw Item
Teleport

Materia Blade
Mythril Shield
Bronze Helmet
Power Sleeve
Reflect Ring

Accumulate, Dash, Throw Stone, Heal, Fury, Wish
Potion, Soft, Holy Water

Final Bets: white - 9 bets for 6,459G (56.3%, x0.78); purple - 10 bets for 5,011G (43.7%, x1.29)

white bets:
UmaiJam: 2,000G (31.0%, 11,109G)
dowdlron: 1,095G (17.0%, 1,095G)
BirbBrainsBot: 1,000G (15.5%, 13,372G)
maleblackfiora: 1,000G (15.5%, 1,798G)
otakutaylor: 500G (7.7%, 5,273G)
Vaxaldan: 403G (6.2%, 403G)
AllInBot: 207G (3.2%, 207G)
cloud92684: 200G (3.1%, 9,863G)
getthemoneyz: 54G (0.8%, 746,477G)

purple bets:
Zetchryn: 1,890G (37.7%, 1,890G)
VynxYukida: 1,000G (20.0%, 9,521G)
NIghtdew14: 500G (10.0%, 8,611G)
gorgewall: 401G (8.0%, 18,063G)
ShintaroNayaka: 400G (8.0%, 5,396G)
LivingHitokiri: 360G (7.2%, 360G)
serperemagus: 160G (3.2%, 160G)
TeaTime29: 100G (2.0%, 1,171G)
CosmicTactician: 100G (2.0%, 12,269G)
datadrivenbot: 100G (2.0%, 25,706G)

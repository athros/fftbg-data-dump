Final Bets: white - 11 bets for 5,925G (56.6%, x0.77); black - 8 bets for 4,538G (43.4%, x1.31)

white bets:
ShintaroNayaka: 1,055G (17.8%, 3,427G)
BirbBrainsBot: 1,000G (16.9%, 13,540G)
getthemoneyz: 1,000G (16.9%, 746,787G)
UmaiJam: 1,000G (16.9%, 9,672G)
otakutaylor: 500G (8.4%, 4,658G)
Digitalsocrates: 500G (8.4%, 11,958G)
worldly_unbrain: 300G (5.1%, 4,142G)
dowdlron: 200G (3.4%, 1,042G)
gogofromtogo: 200G (3.4%, 2,557G)
datadrivenbot: 100G (1.7%, 25,541G)
maleblackfiora: 70G (1.2%, 1,874G)

black bets:
VynxYukida: 1,600G (35.3%, 12,721G)
Zetchryn: 1,000G (22.0%, 3,390G)
HASTERIOUS: 665G (14.7%, 13,315G)
LivingHitokiri: 533G (11.7%, 533G)
Vaxaldan: 232G (5.1%, 232G)
lanouh: 207G (4.6%, 207G)
gorgewall: 201G (4.4%, 18,031G)
AllInBot: 100G (2.2%, 100G)

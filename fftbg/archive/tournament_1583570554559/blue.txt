Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Virilikus
Female
Serpentarius
47
42
Wizard
Charge
PA Save
Short Charge
Lava Walking

Ice Rod

Headgear
Wizard Robe
Diamond Armlet

Fire 2, Fire 3, Ice, Ice 3, Empower, Death
Charge+5, Charge+7, Charge+20



Konfushus
Monster
Aries
49
51
Tiamat










Cheesetacular
Male
Gemini
42
65
Squire
Throw
PA Save
Beastmaster
Move-MP Up

Slasher

Diamond Helmet
Genji Armor
Feather Mantle

Dash, Throw Stone, Heal
Shuriken, Bomb, Knife, Ninja Sword, Axe, Spear, Wand



Tugboat1
Female
Libra
46
76
Oracle
Basic Skill
Faith Up
Dual Wield
Jump+2

Musk Rod
Gokuu Rod
Triangle Hat
Black Robe
Wizard Mantle

Blind, Spell Absorb, Life Drain, Pray Faith, Zombie, Blind Rage, Foxbird, Paralyze, Sleep, Dark Holy
Throw Stone, Heal, Yell, Wish

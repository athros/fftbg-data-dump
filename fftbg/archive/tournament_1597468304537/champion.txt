Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Phi Sig
Female
Scorpio
68
62
Time Mage
Draw Out
Abandon
Doublehand
Jump+3

Octagon Rod

Thief Hat
Light Robe
Magic Ring

Haste 2, Reflect, Quick
Bizen Boat, Murasame, Kiyomori



Thyrandaal
Monster
Aries
59
59
Red Chocobo










Sans From Snowdin
Female
Leo
70
77
Samurai
Time Magic
Absorb Used MP
Halve MP
Jump+3

Bizen Boat

Circlet
Mythril Armor
Spike Shoes

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Haste, Slow 2, Demi, Stabilize Time



ANFz
Female
Capricorn
70
76
Mediator
Summon Magic
PA Save
Maintenance
Teleport

Mythril Gun

Golden Hairpin
White Robe
Red Shoes

Invitation, Threaten, Preach, Solution, Refute, Rehabilitate
Shiva, Ifrit, Titan, Golem, Carbunkle

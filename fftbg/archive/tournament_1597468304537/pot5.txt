Final Bets: red - 5 bets for 9,725G (47.8%, x1.09); green - 13 bets for 10,620G (52.2%, x0.92)

red bets:
SkylerBunny: 6,695G (68.8%, 13,129G)
sinnyil2: 2,154G (22.1%, 4,308G)
Kronikle: 528G (5.4%, 17,754G)
Galeronis: 248G (2.6%, 248G)
MinBetBot: 100G (1.0%, 8,667G)

green bets:
Thyrandaal: 5,000G (47.1%, 247,418G)
BirbBrainsBot: 1,000G (9.4%, 138,253G)
Creggers: 1,000G (9.4%, 19,791G)
silentkaster: 933G (8.8%, 5,933G)
northkoreancleetus: 805G (7.6%, 805G)
KasugaiRoastedPeas: 400G (3.8%, 400G)
AllInBot: 380G (3.6%, 380G)
Mudrockk: 288G (2.7%, 288G)
getthemoneyz: 274G (2.6%, 1,635,452G)
datadrivenbot: 200G (1.9%, 66,186G)
Klednar21: 164G (1.5%, 164G)
gorgewall: 101G (1.0%, 4,554G)
koeeh: 75G (0.7%, 353G)

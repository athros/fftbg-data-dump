Final Bets: green - 8 bets for 6,681G (53.0%, x0.89); champion - 9 bets for 5,925G (47.0%, x1.13)

green bets:
gorgewall: 4,893G (73.2%, 4,893G)
Firesheath: 608G (9.1%, 608G)
northkoreancleetus: 400G (6.0%, 3,249G)
silentkaster: 280G (4.2%, 7,080G)
datadrivenbot: 200G (3.0%, 66,296G)
MinBetBot: 100G (1.5%, 8,812G)
AllInBot: 100G (1.5%, 100G)
koeeh: 100G (1.5%, 247G)

champion bets:
SkylerBunny: 1,545G (26.1%, 1,545G)
BirbBrainsBot: 1,000G (16.9%, 141,619G)
Foamsoldier: 676G (11.4%, 676G)
silverChangeling: 634G (10.7%, 12,692G)
getthemoneyz: 558G (9.4%, 1,636,585G)
Phi_Sig: 555G (9.4%, 144,807G)
Klednar21: 409G (6.9%, 409G)
WhiteTigress: 300G (5.1%, 1,674G)
Galeronis: 248G (4.2%, 248G)

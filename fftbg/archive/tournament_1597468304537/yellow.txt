Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lube Squid
Monster
Scorpio
73
45
Squidraken










Forkmore
Male
Libra
42
60
Thief
Talk Skill
Speed Save
Monster Talk
Waterwalking

Cultist Dagger

Golden Hairpin
Chain Vest
Wizard Mantle

Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim
Threaten, Preach, Negotiate, Refute, Rehabilitate



Chuckolator
Male
Sagittarius
56
63
Bard
Time Magic
Distribute
Halve MP
Move-MP Up

Ramia Harp

Holy Miter
Gold Armor
Feather Boots

Cheer Song, Space Storage, Sky Demon, Hydra Pit
Haste 2, Slow, Stop, Immobilize, Float, Stabilize Time, Meteor



Laserman1000
Male
Taurus
60
57
Time Mage
Talk Skill
Counter Magic
Short Charge
Move+1

White Staff

Red Hood
Silk Robe
Defense Armlet

Immobilize, Float, Reflect, Demi 2
Threaten, Preach

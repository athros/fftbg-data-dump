Player: !Red
Team: Red Team
Palettes: Red/Brown



ZeroHeat
Male
Gemini
78
44
Oracle
Jump
Brave Up
Dual Wield
Waterwalking

Octagon Rod
Musk Rod
Leather Hat
Chain Vest
Sprint Shoes

Blind, Poison, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep, Dark Holy
Level Jump4, Vertical Jump2



Masta Glenn
Male
Scorpio
49
81
Archer
Talk Skill
Counter Flood
Concentrate
Move+1

Mythril Bow

Leather Hat
Earth Clothes
Small Mantle

Charge+2
Insult, Mimic Daravon



Ninesilvers
Male
Sagittarius
56
44
Ninja
Summon Magic
Dragon Spirit
Equip Gun
Jump+1

Fairy Harp
Papyrus Codex
Headgear
Clothes
Chantage

Shuriken, Knife, Sword, Stick, Wand
Ifrit, Carbunkle, Odin, Leviathan, Fairy, Lich, Cyclops



OniXiion
Female
Aquarius
45
58
Mediator
Item
PA Save
Magic Defense UP
Swim

Romanda Gun

Holy Miter
White Robe
Diamond Armlet

Persuade, Praise, Threaten, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down

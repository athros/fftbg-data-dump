Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BuffaloCrunch
Female
Libra
50
58
Time Mage
Draw Out
MA Save
Short Status
Move-MP Up

Gokuu Rod

Black Hood
Mythril Vest
Leather Mantle

Haste, Haste 2, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



Muffin Money
Female
Cancer
57
65
Samurai
Black Magic
Caution
Doublehand
Lava Walking

Bizen Boat

Bronze Helmet
Linen Robe
Small Mantle

Murasame, Kiyomori, Muramasa
Fire, Fire 3, Fire 4, Bolt, Bolt 4, Empower



RaIshtar
Male
Libra
58
44
Oracle
Basic Skill
Sunken State
Equip Sword
Move-HP Up

Kikuichimoji

Leather Hat
Mystic Vest
Setiemson

Blind, Poison, Pray Faith, Silence Song, Blind Rage, Paralyze
Dash, Throw Stone, Heal, Tickle, Cheer Up



MyFakeLife
Male
Pisces
64
77
Samurai
Basic Skill
Brave Save
Sicken
Teleport

Spear

Circlet
Bronze Armor
Elf Mantle

Asura, Heaven's Cloud, Kiyomori
Throw Stone, Heal, Yell

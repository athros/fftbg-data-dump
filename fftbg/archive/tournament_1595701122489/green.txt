Player: !Green
Team: Green Team
Palettes: Green/White



DeathTaxesAndAnime
Female
Taurus
80
45
Mime

Critical Quick
Dual Wield
Move-HP Up



Barette
Mythril Armor
Sprint Shoes

Mimic




Sinnyil2
Male
Gemini
74
69
Mediator
Throw
Damage Split
Equip Shield
Move+3

Glacier Gun
Mythril Shield
Black Hood
Wizard Outfit
Elf Mantle

Praise, Preach, Insult, Negotiate, Refute, Rehabilitate
Shuriken



Bruubarg
Male
Cancer
48
50
Knight
Elemental
HP Restore
Concentrate
Ignore Terrain

Sleep Sword
Buckler
Gold Helmet
Bronze Armor
Feather Boots

Head Break, Speed Break, Mind Break, Dark Sword, Night Sword
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Gusty Wind, Lava Ball



Actual JP
Male
Aquarius
52
61
Lancer
Draw Out
PA Save
Martial Arts
Ignore Height


Genji Shield
Gold Helmet
Silk Robe
Power Wrist

Level Jump8, Vertical Jump6
Asura, Koutetsu, Muramasa, Kikuichimoji

Player: !Red
Team: Red Team
Palettes: Red/Brown



Gr8keeper
Female
Cancer
61
57
Ninja
Battle Skill
Caution
Equip Armor
Waterwalking

Ninja Edge
Short Edge
Green Beret
White Robe
Genji Gauntlet

Shuriken, Knife, Ninja Sword
Armor Break, Shield Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Night Sword



HV5H
Male
Sagittarius
69
45
Calculator
Limit
Counter Tackle
Equip Polearm
Jump+1

Ryozan Silk

Circlet
Chain Vest
Wizard Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Mesmaster
Male
Leo
66
42
Lancer
Item
Distribute
Magic Defense UP
Move+2

Star Bag
Crystal Shield
Iron Helmet
Chain Mail
Spike Shoes

Level Jump8, Vertical Jump8
Potion, Hi-Potion, Ether, Maiden's Kiss, Phoenix Down



Galkife
Female
Cancer
42
49
Squire
White Magic
Counter
Equip Knife
Retreat

Short Edge
Mythril Shield
Cross Helmet
Wizard Outfit
Magic Gauntlet

Dash, Heal, Tickle, Wish, Ultima
Raise, Regen, Protect, Shell 2, Esuna

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Male
Pisces
67
45
Chemist
Basic Skill
Caution
Equip Knife
Fly

Flame Rod

Flash Hat
Wizard Outfit
Elf Mantle

Potion, Ether, Maiden's Kiss, Soft, Phoenix Down
Dash, Throw Stone, Heal, Yell



Brokenknight201
Male
Virgo
57
53
Samurai
White Magic
Counter
Equip Gun
Move-MP Up

Bestiary

Crystal Helmet
White Robe
Defense Armlet

Asura, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji, Masamune
Cure 2, Cure 4, Raise, Protect, Protect 2, Esuna



Grininda
Male
Taurus
69
58
Mediator
Summon Magic
Auto Potion
Dual Wield
Retreat

Battle Folio
Battle Folio
Green Beret
Silk Robe
108 Gems

Invitation, Praise, Preach, Solution, Mimic Daravon, Rehabilitate
Shiva, Carbunkle, Odin, Silf



VolgraTheMoose
Male
Aries
45
77
Ninja
Yin Yang Magic
Counter Magic
Equip Gun
Levitate

Fairy Harp
Battle Folio
Leather Hat
Judo Outfit
Jade Armlet

Bomb, Axe
Blind, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze

Player: !White
Team: White Team
Palettes: White/Blue



Megamax667
Female
Leo
75
78
Priest
Elemental
Sunken State
Equip Polearm
Move-HP Up

Spear

Feather Hat
Light Robe
Red Shoes

Cure, Cure 2, Cure 4, Raise, Regen, Shell 2, Wall, Esuna, Holy
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Dowdlron
Female
Aquarius
56
79
Calculator
White Magic
MP Restore
Concentrate
Fly

Madlemgen

Twist Headband
Linen Robe
Leather Mantle

CT, Height, Prime Number, 4, 3
Cure 4, Raise 2, Regen, Holy



Jaritras
Male
Leo
43
48
Archer
Yin Yang Magic
Faith Save
Equip Polearm
Jump+2

Partisan
Round Shield
Diamond Helmet
Chain Vest
Angel Ring

Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Spell Absorb, Doubt Faith, Zombie, Blind Rage, Confusion Song, Petrify



Twelfthrootoftwo
Female
Leo
75
59
Geomancer
Draw Out
Dragon Spirit
Halve MP
Move-HP Up

Slasher
Aegis Shield
Feather Hat
Black Robe
Defense Armlet

Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Murasame, Kikuichimoji

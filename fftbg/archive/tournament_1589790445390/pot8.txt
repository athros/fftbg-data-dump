Final Bets: white - 10 bets for 4,537G (52.8%, x0.90); champion - 9 bets for 4,061G (47.2%, x1.12)

white bets:
sinnyil2: 1,124G (24.8%, 1,124G)
BirbBrainsBot: 1,000G (22.0%, 153,073G)
CorpusCav: 649G (14.3%, 649G)
R_Raynos: 564G (12.4%, 564G)
DaveStrider55: 500G (11.0%, 2,417G)
AmaninAmide: 200G (4.4%, 7,182G)
otakutaylor: 200G (4.4%, 1,996G)
MrUbiq: 100G (2.2%, 3,483G)
ApplesauceBoss: 100G (2.2%, 7,331G)
Digitalsocrates: 100G (2.2%, 1,840G)

champion bets:
Mesmaster: 2,000G (49.2%, 80,319G)
ungabunga_bot: 755G (18.6%, 531,999G)
getthemoneyz: 310G (7.6%, 686,315G)
YaBoy125: 300G (7.4%, 3,615G)
thaetreis: 296G (7.3%, 296G)
lastly: 100G (2.5%, 13,815G)
OneHundredFists: 100G (2.5%, 876G)
Evewho: 100G (2.5%, 1,579G)
datadrivenbot: 100G (2.5%, 19,140G)

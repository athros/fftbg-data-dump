Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



HaplessOne
Female
Taurus
61
45
Oracle
Draw Out
Parry
Doublehand
Ignore Height

Octagon Rod

Black Hood
Linen Robe
Vanish Mantle

Poison, Pray Faith, Doubt Faith, Confusion Song, Dispel Magic
Masamune



Rethera
Female
Libra
81
80
Ninja
Basic Skill
Faith Up
Concentrate
Ignore Terrain

Morning Star
Mythril Knife
Thief Hat
Clothes
Magic Gauntlet

Shuriken, Knife, Staff
Yell, Wish



WireLord
Female
Virgo
73
68
Chemist
Charge
MA Save
Magic Defense UP
Jump+2

Mythril Gun

Flash Hat
Earth Clothes
Bracer

Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Charge+1, Charge+2, Charge+4, Charge+10, Charge+20



Laserman1000
Male
Leo
64
47
Ninja
Battle Skill
Parry
Equip Knife
Levitate

Ice Rod
Dragon Rod
Flash Hat
Mythril Vest
Reflect Ring

Bomb, Stick
Head Break, Armor Break, Mind Break, Dark Sword

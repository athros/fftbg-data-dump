Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DejaPoo21
Female
Taurus
80
69
Ninja
Time Magic
Faith Up
Short Status
Move+1

Ninja Edge
Mage Masher
Triangle Hat
Earth Clothes
Elf Mantle

Bomb, Ninja Sword
Haste, Slow, Immobilize, Reflect, Stabilize Time



PotionDweller
Monster
Leo
50
80
Grenade










EunosXX
Female
Capricorn
72
74
Summoner
Battle Skill
Meatbone Slash
Secret Hunt
Move-HP Up

Oak Staff

Twist Headband
Linen Robe
Feather Boots

Shiva, Ifrit, Carbunkle, Leviathan, Silf
Magic Break, Speed Break, Surging Sword



Onsalo
Male
Scorpio
37
45
Thief
Talk Skill
Earplug
Short Charge
Waterwalking

Assassin Dagger

Flash Hat
Leather Outfit
Feather Mantle

Steal Helmet, Steal Status
Threaten, Solution, Negotiate, Refute

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Maakur
Male
Capricorn
45
83
Mediator
Steal
Brave Save
Concentrate
Levitate

Battle Folio

Headgear
Clothes
Germinas Boots

Invitation, Threaten, Preach, Solution, Rehabilitate
Gil Taking, Steal Helmet, Steal Armor, Steal Shield



Ko2q
Male
Aquarius
68
54
Knight
Steal
Dragon Spirit
Magic Defense UP
Waterwalking

Defender
Genji Shield
Grand Helmet
Chain Mail
Battle Boots

Weapon Break, Speed Break, Power Break, Stasis Sword
Gil Taking, Steal Heart, Steal Status



Lowlf
Female
Pisces
44
54
Monk
Time Magic
Distribute
Equip Knife
Jump+1

Sasuke Knife

Barette
Leather Outfit
Genji Gauntlet

Purification, Chakra, Revive, Seal Evil
Haste, Haste 2, Float, Reflect



FriendSquirrel
Male
Aquarius
66
78
Bard
Item
Mana Shield
Equip Gun
Move+2

Romanda Gun

Holy Miter
Clothes
Diamond Armlet

Life Song, Magic Song, Last Song, Diamond Blade
Potion, X-Potion, Phoenix Down

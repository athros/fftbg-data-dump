Player: !White
Team: White Team
Palettes: White/Blue



PlatinumPlume
Male
Sagittarius
59
78
Archer
Yin Yang Magic
Catch
Equip Knife
Ignore Terrain

Mage Masher
Gold Shield
Headgear
Brigandine
Reflect Ring

Charge+4, Charge+7
Blind, Confusion Song, Dispel Magic, Paralyze



WhiteTigress
Male
Leo
53
70
Lancer
Steal
Damage Split
Sicken
Jump+2

Obelisk
Buckler
Crystal Helmet
Platinum Armor
Cursed Ring

Level Jump8, Vertical Jump7
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Arm Aim



TheBlobZ
Female
Libra
81
45
Thief
Battle Skill
Arrow Guard
Equip Axe
Swim

Gold Staff

Flash Hat
Chain Vest
Magic Gauntlet

Steal Helmet, Steal Armor, Leg Aim
Armor Break, Weapon Break, Stasis Sword, Justice Sword, Dark Sword



DaveStrider55
Male
Cancer
73
70
Monk
White Magic
Earplug
Magic Attack UP
Move-MP Up



Flash Hat
Adaman Vest
Germinas Boots

Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive
Cure 2, Cure 3, Raise, Regen, Protect, Protect 2, Esuna, Magic Barrier

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kalthonia
Female
Scorpio
57
61
Squire
White Magic
Counter
Secret Hunt
Teleport

Orichalcum

Red Hood
Mythril Vest
Reflect Ring

Heal, Tickle, Cheer Up
Protect, Shell, Esuna



Nosablake
Male
Gemini
56
52
Priest
Time Magic
Parry
Defend
Levitate

Flame Whip

Headgear
White Robe
108 Gems

Cure, Cure 2, Cure 3, Raise, Raise 2, Protect, Protect 2, Shell 2, Esuna, Holy
Slow, Stop, Reflect, Quick, Demi, Meteor



E Ballard
Male
Gemini
80
64
Thief
Draw Out
Absorb Used MP
Magic Attack UP
Retreat

Air Knife

Cross Helmet
Adaman Vest
Rubber Shoes

Steal Accessory
Koutetsu, Heaven's Cloud



Hasterious
Female
Aquarius
49
48
Ninja
Charge
Earplug
Short Status
Levitate

Ninja Edge
Spell Edge
Flash Hat
Judo Outfit
Battle Boots

Shuriken, Dictionary
Charge+1, Charge+4, Charge+5, Charge+7, Charge+10

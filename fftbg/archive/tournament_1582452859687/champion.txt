Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lowlf
Monster
Libra
41
44
Squidraken










Uryvichk
Female
Sagittarius
68
57
Monk
Black Magic
Auto Potion
Sicken
Lava Walking



Flash Hat
Black Costume
Wizard Mantle

Spin Fist, Pummel, Earth Slash, Purification, Chakra, Seal Evil
Bolt, Ice, Empower, Death



Dynasti
Male
Sagittarius
39
57
Mediator
Sing
Meatbone Slash
Dual Wield
Swim

Stone Gun
Romanda Gun
Red Hood
Power Sleeve
Diamond Armlet

Invitation, Persuade, Preach, Insult, Refute
Cheer Song, Battle Song, Last Song, Diamond Blade



Koryiaki
Female
Libra
72
61
Oracle
Basic Skill
Mana Shield
Equip Gun
Jump+3

Bloody Strings

Leather Hat
Leather Outfit
Feather Mantle

Blind, Poison, Zombie, Blind Rage, Dispel Magic, Paralyze
Dash, Heal, Tickle

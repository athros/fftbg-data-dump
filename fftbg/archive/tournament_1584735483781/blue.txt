Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lythe Caraker
Female
Libra
50
70
Squire
Dance
HP Restore
Attack UP
Move-HP Up

Slasher
Platinum Shield
Gold Helmet
Reflect Mail
Battle Boots

Accumulate, Throw Stone, Heal, Cheer Up, Wish
Witch Hunt, Wiznaibus, Obsidian Blade, Void Storage, Dragon Pit



Kai Shee
Female
Cancer
67
54
Chemist
Draw Out
Absorb Used MP
Short Charge
Waterwalking

Main Gauche

Twist Headband
Mythril Vest
Jade Armlet

X-Potion, Hi-Ether, Eye Drop, Remedy, Phoenix Down
Koutetsu, Murasame, Kikuichimoji



FreedomNM
Male
Gemini
69
53
Knight
Jump
Parry
Halve MP
Ignore Terrain

Blood Sword
Genji Shield
Platinum Helmet
White Robe
Spike Shoes

Head Break, Armor Break, Weapon Break, Power Break, Mind Break
Level Jump2, Vertical Jump4



Baconbacon1207
Male
Taurus
64
75
Priest
Item
Meatbone Slash
Doublehand
Move+3

Morning Star

Triangle Hat
Light Robe
Wizard Mantle

Cure, Cure 4, Raise, Protect 2, Shell 2, Esuna, Holy
Potion, Hi-Potion, Hi-Ether, Eye Drop, Soft, Phoenix Down

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lokenwow
Female
Aquarius
53
40
Oracle
Charge
Speed Save
Defense UP
Move+1

Battle Folio

Triangle Hat
Brigandine
Small Mantle

Poison, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep, Dark Holy
Charge+1, Charge+4, Charge+5, Charge+10



LDSkinny
Female
Aries
70
40
Oracle
Draw Out
Counter
Doublehand
Retreat

Ivory Rod

Twist Headband
Silk Robe
Elf Mantle

Blind, Poison, Life Drain, Pray Faith, Foxbird, Dispel Magic, Petrify
Asura, Bizen Boat, Kiyomori



LAGBOT30000
Male
Aquarius
73
57
Priest
Talk Skill
Caution
Equip Armor
Waterwalking

Wizard Staff

Diamond Helmet
Genji Armor
Cherche

Cure, Cure 2, Cure 4, Raise, Reraise
Insult, Mimic Daravon, Rehabilitate



ExecutedGiraffe
Male
Virgo
36
45
Ninja
Basic Skill
Damage Split
Equip Gun
Jump+3

Romanda Gun
Romanda Gun
Green Beret
Adaman Vest
Chantage

Bomb, Knife, Sword
Accumulate, Throw Stone, Heal, Scream

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Kaidykat
Female
Cancer
54
56
Priest
Draw Out
Regenerator
Defend
Retreat

Flame Whip

Triangle Hat
Adaman Vest
Small Mantle

Cure 2, Raise, Reraise, Regen, Protect 2, Shell 2, Esuna
Asura, Koutetsu, Murasame, Kiyomori, Muramasa



Powergems
Monster
Sagittarius
50
65
Taiju










SirCherish
Female
Aquarius
40
55
Summoner
Punch Art
Dragon Spirit
Sicken
Move+3

Healing Staff

Flash Hat
Power Sleeve
Diamond Armlet

Shiva, Carbunkle, Silf, Lich, Cyclops
Pummel, Purification, Revive



Ko2q
Female
Aries
59
61
Thief
Time Magic
HP Restore
Martial Arts
Jump+2



Golden Hairpin
Clothes
Bracer

Steal Heart, Steal Armor, Steal Shield, Steal Weapon
Haste, Haste 2, Slow, Stop, Stabilize Time

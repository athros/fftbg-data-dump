Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Pandasforsale
Male
Capricorn
78
46
Mime

Counter
Equip Shield
Move+1


Bronze Shield
Leather Hat
Crystal Mail
Salty Rage

Mimic




RedRiderAyame
Female
Taurus
46
72
Dancer
Summon Magic
Absorb Used MP
Beastmaster
Move-HP Up

Ryozan Silk

Red Hood
Chameleon Robe
Genji Gauntlet

Witch Hunt, Wiznaibus, Polka Polka, Obsidian Blade, Dragon Pit
Moogle, Leviathan, Silf, Fairy



Galkife
Female
Taurus
48
61
Lancer
Punch Art
Catch
Sicken
Jump+1

Spear
Flame Shield
Bronze Helmet
Linen Cuirass
Elf Mantle

Level Jump8, Vertical Jump5
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Ayeayex3
Male
Scorpio
51
49
Lancer
Sing
Absorb Used MP
Dual Wield
Teleport

Mythril Spear
Obelisk
Barbuta
Diamond Armor
Power Wrist

Level Jump8, Vertical Jump8
Nameless Song, Last Song

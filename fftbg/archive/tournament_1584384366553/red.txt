Player: !Red
Team: Red Team
Palettes: Red/Brown



BuffaloCrunch
Female
Pisces
70
63
Archer
Elemental
Distribute
Dual Wield
Move+3

Cross Bow
Night Killer
Diamond Helmet
Wizard Outfit
Cherche

Charge+1, Charge+3, Charge+5, Charge+7
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Smegma Sorcerer
Female
Sagittarius
53
74
Samurai
Item
Critical Quick
Throw Item
Move+1

Holy Lance

Gold Helmet
Gold Armor
N-Kai Armlet

Koutetsu, Murasame
Potion, Hi-Ether, Antidote, Soft



Athropos1
Male
Leo
52
50
Samurai
Basic Skill
Speed Save
Defend
Ignore Height

Asura Knife

Barbuta
Mythril Armor
Cursed Ring

Asura, Koutetsu, Heaven's Cloud, Kikuichimoji
Heal, Cheer Up, Wish, Scream



Mizucrux
Male
Virgo
54
40
Samurai
Yin Yang Magic
Counter Magic
Equip Axe
Jump+3

Oak Staff

Cross Helmet
Linen Robe
Dracula Mantle

Koutetsu, Muramasa
Silence Song, Dispel Magic, Petrify

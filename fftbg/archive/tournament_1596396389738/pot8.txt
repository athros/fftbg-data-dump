Final Bets: red - 13 bets for 13,471G (29.4%, x2.40); champion - 24 bets for 32,300G (70.6%, x0.42)

red bets:
Thyrandaal: 3,000G (22.3%, 89,672G)
Zeroroute: 2,212G (16.4%, 2,212G)
dogsandcatsand: 2,000G (14.8%, 37,659G)
helpimabug: 1,624G (12.1%, 3,248G)
Lyner87: 1,001G (7.4%, 75,272G)
BirbBrainsBot: 1,000G (7.4%, 154,588G)
Aldrammech: 1,000G (7.4%, 7,459G)
killth3kid: 592G (4.4%, 11,866G)
g1nger4le: 300G (2.2%, 5,700G)
getthemoneyz: 210G (1.6%, 1,485,674G)
Brainstew29: 209G (1.6%, 16,209G)
cho_pin: 200G (1.5%, 745G)
Lydian_C: 123G (0.9%, 2,388G)

champion bets:
reinoe: 10,000G (31.0%, 56,497G)
toka222: 5,500G (17.0%, 104,203G)
maximumcrit: 5,000G (15.5%, 21,240G)
AllInBot: 1,749G (5.4%, 1,749G)
DeathTaxesAndAnime: 1,525G (4.7%, 2,992G)
Lumberinjack: 1,111G (3.4%, 2,223G)
nifboy: 1,000G (3.1%, 3,785G)
lyans00: 1,000G (3.1%, 2,290G)
Forkmore: 671G (2.1%, 671G)
maakur_: 612G (1.9%, 612G)
sphurnt: 504G (1.6%, 504G)
TasisSai: 500G (1.5%, 16,444G)
WhattayaBrian: 499G (1.5%, 499G)
letdowncity: 488G (1.5%, 16,349G)
Zbgs: 476G (1.5%, 476G)
ammobox69: 300G (0.9%, 474G)
richardserious: 300G (0.9%, 1,647G)
silentperogy: 300G (0.9%, 28,802G)
Wooplestein: 232G (0.7%, 232G)
datadrivenbot: 200G (0.6%, 49,187G)
Kellios11: 100G (0.3%, 25,987G)
EmmaEnema: 100G (0.3%, 2,300G)
Hasterious: 72G (0.2%, 7,246G)
ArlanKels: 61G (0.2%, 545G)

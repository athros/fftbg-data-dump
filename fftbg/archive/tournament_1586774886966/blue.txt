Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LanseDM
Male
Virgo
64
49
Archer
Yin Yang Magic
PA Save
Halve MP
Ignore Terrain

Ice Bow

Diamond Helmet
Earth Clothes
Dracula Mantle

Charge+3, Charge+4, Charge+10
Life Drain, Zombie, Confusion Song, Dispel Magic



Maeveen
Male
Serpentarius
40
58
Monk
Talk Skill
Abandon
Sicken
Fly



Green Beret
Brigandine
Jade Armlet

Wave Fist, Chakra, Revive
Invitation, Threaten, Solution, Death Sentence, Insult



HaplessOne
Female
Aquarius
58
72
Samurai
Basic Skill
Damage Split
Magic Attack UP
Retreat

Bizen Boat

Leather Helmet
Wizard Robe
Feather Mantle

Koutetsu, Muramasa, Kikuichimoji
Throw Stone, Yell, Cheer Up



Treapvort
Female
Gemini
44
73
Oracle
Draw Out
Dragon Spirit
Equip Knife
Ignore Terrain

Ice Rod

Holy Miter
Silk Robe
Angel Ring

Blind, Spell Absorb, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Asura, Bizen Boat, Heaven's Cloud, Muramasa

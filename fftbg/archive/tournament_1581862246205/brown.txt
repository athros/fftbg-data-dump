Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Segomod
Male
Virgo
65
73
Geomancer
Time Magic
Mana Shield
Equip Sword
Jump+1

Nagrarock
Bronze Shield
Triangle Hat
Judo Outfit
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Haste, Haste 2, Slow 2, Float, Quick, Demi, Demi 2, Stabilize Time



KantsugaLoL
Male
Virgo
78
43
Mediator
Black Magic
Dragon Spirit
Dual Wield
Move-MP Up

Romanda Gun
Mythril Gun
Headgear
Chain Vest
Wizard Mantle

Threaten, Preach, Mimic Daravon, Refute
Bolt 2, Ice 3, Ice 4



Elelor
Male
Pisces
47
73
Lancer
Elemental
Abandon
Maintenance
Retreat

Spear
Buckler
Leather Helmet
Chameleon Robe
Elf Mantle

Level Jump5, Vertical Jump4
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Mightygerm1
Female
Gemini
80
57
Calculator
Black Magic
Counter
Equip Polearm
Move-HP Up

Battle Folio

Red Hood
Wizard Robe
Sprint Shoes

CT, Prime Number
Fire 2, Fire 4, Bolt 4, Ice, Empower, Frog

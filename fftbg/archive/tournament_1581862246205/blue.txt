Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Matthewmuscles
Male
Aries
77
66
Time Mage
Jump
Meatbone Slash
Maintenance
Lava Walking

Battle Folio

Feather Hat
Brigandine
Magic Gauntlet

Float, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Level Jump8, Vertical Jump7



Sinikt
Male
Gemini
42
69
Geomancer
Basic Skill
Auto Potion
Maintenance
Move+1

Long Sword
Escutcheon
Leather Hat
Wizard Robe
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Accumulate, Dash, Tickle



Holyonline
Male
Taurus
65
60
Ninja
Basic Skill
Auto Potion
Halve MP
Waterwalking

Morning Star
Flame Whip
Thief Hat
Clothes
Rubber Shoes

Bomb, Wand
Dash, Heal, Cheer Up



MisssTresss
Male
Virgo
69
73
Samurai
Yin Yang Magic
Abandon
Defense UP
Move+3

Asura Knife

Crystal Helmet
Silk Robe
Defense Armlet

Murasame, Heaven's Cloud, Kikuichimoji
Blind, Poison, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep

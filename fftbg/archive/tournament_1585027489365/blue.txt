Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Proper Noun
Female
Sagittarius
61
64
Samurai
Item
Meatbone Slash
Dual Wield
Ignore Height

Kikuichimoji
Heaven's Cloud
Diamond Helmet
Gold Armor
Magic Ring

Heaven's Cloud, Kikuichimoji
X-Potion, Maiden's Kiss, Soft, Holy Water, Remedy



Hirine
Monster
Scorpio
69
46
Apanda










Mirapoix
Male
Taurus
64
47
Calculator
Time Magic
Absorb Used MP
Equip Armor
Lava Walking

Poison Rod

Crystal Helmet
Power Sleeve
Angel Ring

CT, Height, 3
Slow 2, Immobilize, Demi, Stabilize Time



ThanatosXRagnarok
Female
Scorpio
55
50
Oracle
Jump
Absorb Used MP
Equip Armor
Jump+3

Musk Rod

Flash Hat
Chain Vest
Diamond Armlet

Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Level Jump8, Vertical Jump2

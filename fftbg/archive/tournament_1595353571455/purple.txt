Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Run With Stone GUNs
Male
Taurus
47
58
Calculator
Yin Yang Magic
Critical Quick
Short Charge
Levitate

Bestiary

Triangle Hat
White Robe
Small Mantle

CT, Height, 5, 3
Poison, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Dark Holy



Mirapoix
Monster
Serpentarius
66
79
Ahriman










VolgraTheMoose
Male
Pisces
80
73
Geomancer
Black Magic
Speed Save
Defense UP
Ignore Terrain

Rune Blade
Flame Shield
Black Hood
Mystic Vest
Reflect Ring

Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bolt, Ice, Ice 3



Reddwind
Male
Gemini
79
46
Knight
Punch Art
Faith Save
Dual Wield
Move-HP Up

Broad Sword
Ice Brand
Gold Helmet
Robe of Lords
Magic Ring

Power Break, Justice Sword
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification

Final Bets: blue - 6 bets for 4,086G (17.2%, x4.80); green - 10 bets for 19,626G (82.8%, x0.21)

blue bets:
douchetron: 2,189G (53.6%, 4,378G)
dogsandcatsand: 874G (21.4%, 874G)
nekojin: 500G (12.2%, 2,711G)
CapnChaos12: 300G (7.3%, 11,682G)
ohhinm: 123G (3.0%, 4,982G)
Lyner87: 100G (2.4%, 64,999G)

green bets:
HaateXIII: 11,938G (60.8%, 11,938G)
TheChainNerd: 2,063G (10.5%, 2,063G)
E_Ballard: 2,010G (10.2%, 2,010G)
skipsandwiches: 1,769G (9.0%, 1,769G)
VolgraTheMoose: 500G (2.5%, 9,285G)
YaBoy125: 400G (2.0%, 400G)
ColetteMSLP: 400G (2.0%, 4,914G)
BoneMiser: 336G (1.7%, 336G)
lyonslegacy: 200G (1.0%, 19,686G)
ChubbyBroCast: 10G (0.1%, 104G)

Player: !White
Team: White Team
Palettes: White/Blue



Meta Five
Female
Gemini
66
67
Ninja
Time Magic
MP Restore
Halve MP
Waterbreathing

Mage Masher
Short Edge
Flash Hat
Clothes
Genji Gauntlet

Bomb, Sword, Ninja Sword, Stick
Haste, Haste 2



Helpimabug
Male
Capricorn
60
44
Knight
Item
MA Save
Dual Wield
Jump+3

Giant Axe
Battle Axe
Diamond Helmet
Bronze Armor
Genji Gauntlet

Armor Break, Speed Break, Stasis Sword, Justice Sword, Explosion Sword
Potion, Hi-Ether, Antidote, Holy Water, Remedy, Phoenix Down



Kahjeethaswares
Female
Libra
79
59
Calculator
White Magic
Dragon Spirit
Dual Wield
Fly

Battle Folio
Bestiary
Green Beret
Silk Robe
108 Gems

CT, Height, Prime Number, 5, 4, 3
Regen, Protect, Shell, Wall, Esuna



Lyner87
Female
Libra
64
70
Knight
Charge
Dragon Spirit
Doublehand
Move+1

Battle Axe

Barbuta
Reflect Mail
Angel Ring

Head Break, Magic Break, Power Break, Mind Break, Stasis Sword
Charge+1, Charge+3

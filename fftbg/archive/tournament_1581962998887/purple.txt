Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Wolvaroo
Female
Cancer
76
56
Mediator
Elemental
Auto Potion
Sicken
Move+2

Mythril Knife

Black Hood
Adaman Vest
108 Gems

Praise, Preach, Death Sentence
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



OmnibotGamma
Female
Leo
52
75
Knight
Talk Skill
Dragon Spirit
Equip Knife
Levitate

Kunai
Buckler
Leather Helmet
Linen Cuirass
Power Wrist

Magic Break, Speed Break, Power Break, Mind Break, Surging Sword
Persuade, Praise, Preach, Solution, Insult, Refute



EvilLego6
Male
Aquarius
50
48
Ninja
Steal
Sunken State
Equip Gun
Ignore Terrain

Bloody Strings
Bloody Strings
Feather Hat
Adaman Vest
Germinas Boots

Shuriken, Staff
Steal Heart, Steal Accessory, Steal Status



Pelerin
Male
Leo
67
58
Archer
Basic Skill
Sunken State
Secret Hunt
Ignore Height

Mythril Gun
Crystal Shield
Leather Hat
Power Sleeve
Spike Shoes

Charge+3, Charge+7
Dash, Heal, Tickle, Yell, Wish

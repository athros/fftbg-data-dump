Player: !Green
Team: Green Team
Palettes: Green/White



Treapvort
Female
Pisces
59
64
Lancer
Summon Magic
MA Save
Equip Sword
Ignore Height

Musk Rod
Bronze Shield
Mythril Helmet
Plate Mail
Leather Mantle

Level Jump8, Vertical Jump7
Moogle, Shiva, Ramuh, Titan, Golem, Bahamut, Odin, Leviathan, Fairy, Cyclops



0v3rr8d
Female
Gemini
59
65
Squire
Throw
Distribute
Magic Attack UP
Jump+1

Giant Axe

Holy Miter
Mythril Vest
Magic Ring

Accumulate, Dash, Heal, Yell, Wish
Shuriken, Bomb, Knife, Staff, Spear, Wand



Thunderclaude
Female
Cancer
60
70
Oracle
White Magic
Arrow Guard
Maintenance
Move+2

Whale Whisker

Flash Hat
Adaman Vest
Defense Ring

Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Cure 2, Cure 3, Raise, Protect 2, Esuna, Holy



Kolonel Panic
Female
Cancer
55
61
Lancer
Black Magic
Counter Magic
Maintenance
Waterbreathing

Javelin
Mythril Shield
Cross Helmet
Leather Armor
Diamond Armlet

Level Jump5, Vertical Jump8
Bolt, Bolt 3, Ice 2, Death

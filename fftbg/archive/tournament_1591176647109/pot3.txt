Final Bets: white - 11 bets for 4,604G (41.1%, x1.43); black - 13 bets for 6,593G (58.9%, x0.70)

white bets:
BirbBrainsBot: 1,000G (21.7%, 183,513G)
UmaiJam: 1,000G (21.7%, 18,049G)
jaik1231: 547G (11.9%, 1,094G)
ApplesauceBoss: 520G (11.3%, 520G)
volgrathemoose: 500G (10.9%, 9,320G)
helpimabug: 288G (6.3%, 288G)
gorgewall: 201G (4.4%, 8,136G)
Evewho: 200G (4.3%, 7,244G)
DAC169: 144G (3.1%, 144G)
DISTROlD: 104G (2.3%, 104G)
CorpusCav: 100G (2.2%, 26,538G)

black bets:
Kyune: 2,000G (30.3%, 25,331G)
mirapoix: 1,113G (16.9%, 1,113G)
twelfthrootoftwo: 1,000G (15.2%, 11,499G)
AshesSmashes: 860G (13.0%, 860G)
thaetreis: 393G (6.0%, 393G)
ColetteMSLP: 300G (4.6%, 2,018G)
getthemoneyz: 260G (3.9%, 797,713G)
Klednar21: 147G (2.2%, 147G)
blastty: 120G (1.8%, 120G)
Ring_Wyrm: 100G (1.5%, 724G)
MilesDong: 100G (1.5%, 100G)
datadrivenbot: 100G (1.5%, 31,936G)
DefinitelyNotKrisMay: 100G (1.5%, 100G)

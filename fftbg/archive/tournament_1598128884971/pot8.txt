Final Bets: white - 14 bets for 10,041G (37.9%, x1.64); champion - 19 bets for 16,481G (62.1%, x0.61)

white bets:
pandasforsale: 2,269G (22.6%, 2,269G)
SkylerBunny: 1,000G (10.0%, 8,147G)
killth3kid: 1,000G (10.0%, 47,283G)
BirbBrainsBot: 1,000G (10.0%, 131,597G)
LordTomS: 1,000G (10.0%, 19,287G)
blorpy_: 791G (7.9%, 9,897G)
Laserman1000: 723G (7.2%, 15,423G)
SephDarkheart: 600G (6.0%, 134,026G)
Vahn_Blade: 500G (5.0%, 10,447G)
Willjin: 500G (5.0%, 5,192G)
NewbGaming: 250G (2.5%, 640G)
CosmicTactician: 200G (2.0%, 36,795G)
getthemoneyz: 108G (1.1%, 1,704,478G)
Aldrammech: 100G (1.0%, 6,349G)

champion bets:
pplvee1: 5,178G (31.4%, 5,178G)
bruubarg: 2,540G (15.4%, 2,540G)
Forkmore: 1,931G (11.7%, 1,931G)
roofiepops: 1,400G (8.5%, 10,032G)
omegasuspekt: 836G (5.1%, 1,672G)
SeniorBunk: 663G (4.0%, 20,663G)
Lanshaft: 612G (3.7%, 17,202G)
corysteam: 500G (3.0%, 2,893G)
Lord_Gwarth: 500G (3.0%, 1,926G)
latebit: 440G (2.7%, 440G)
Lemonjohns: 420G (2.5%, 9,075G)
theBinklive: 314G (1.9%, 314G)
CT_5_Holy: 300G (1.8%, 6,003G)
neocarbuncle: 210G (1.3%, 3,116G)
datadrivenbot: 200G (1.2%, 61,159G)
HASTERIOUS: 132G (0.8%, 13,229G)
sparsuvar: 104G (0.6%, 104G)
gorgewall: 101G (0.6%, 4,066G)
AllInBot: 100G (0.6%, 100G)

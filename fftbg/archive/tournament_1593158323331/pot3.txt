Final Bets: white - 7 bets for 4,185G (37.5%, x1.67); black - 9 bets for 6,977G (62.5%, x0.60)

white bets:
iamEradicate: 1,000G (23.9%, 1,000G)
blastty: 821G (19.6%, 821G)
BirbBrainsBot: 718G (17.2%, 718G)
nifboy: 536G (12.8%, 536G)
getthemoneyz: 378G (9.0%, 1,053,075G)
AshesSmashes: 369G (8.8%, 369G)
MilesDong: 363G (8.7%, 363G)

black bets:
Mesmaster: 2,000G (28.7%, 87,809G)
SkyridgeZero: 1,500G (21.5%, 44,962G)
DamnThatShark: 1,027G (14.7%, 1,027G)
prince_rogers_nelson_: 1,000G (14.3%, 17,295G)
YaBoy125: 642G (9.2%, 6,420G)
Zetchryn: 408G (5.8%, 408G)
DeathTaxesAndAnime: 200G (2.9%, 2,227G)
Ring_Wyrm: 100G (1.4%, 6,071G)
datadrivenbot: 100G (1.4%, 49,240G)

Final Bets: black - 9 bets for 5,677G (64.6%, x0.55); purple - 5 bets for 3,110G (35.4%, x1.83)

black bets:
DamnThatShark: 1,643G (28.9%, 1,643G)
SkyridgeZero: 1,500G (26.4%, 45,862G)
Mesmaster: 1,000G (17.6%, 88,275G)
vorap: 500G (8.8%, 155,177G)
MilesDong: 406G (7.2%, 406G)
blastty: 228G (4.0%, 228G)
Evewho: 200G (3.5%, 7,493G)
Ring_Wyrm: 100G (1.8%, 6,044G)
datadrivenbot: 100G (1.8%, 49,209G)

purple bets:
prince_rogers_nelson_: 1,000G (32.2%, 16,984G)
getthemoneyz: 720G (23.2%, 1,052,099G)
Zetchryn: 711G (22.9%, 711G)
helpimabug: 500G (16.1%, 5,156G)
BirbBrainsBot: 179G (5.8%, 781G)

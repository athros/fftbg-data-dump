Player: !Red
Team: Red Team
Palettes: Red/Brown



Maximumcrit
Female
Virgo
69
41
Summoner
Item
Sunken State
Throw Item
Teleport

Thunder Rod

Black Hood
Robe of Lords
Rubber Shoes

Moogle, Titan, Golem, Carbunkle, Bahamut, Odin, Leviathan, Silf, Lich, Cyclops
Potion, X-Potion, Eye Drop, Maiden's Kiss, Phoenix Down



StealthModeLocke
Male
Capricorn
79
58
Archer
Black Magic
Damage Split
Equip Polearm
Waterwalking

Ivory Rod
Round Shield
Flash Hat
Chain Vest
Salty Rage

Charge+1, Charge+3, Charge+5, Charge+7
Fire, Fire 2, Fire 3, Bolt 2, Bolt 4, Ice 3, Death



Nizaha
Male
Pisces
76
54
Thief
Black Magic
Counter Flood
Equip Gun
Waterbreathing

Ramia Harp

Black Hood
Earth Clothes
Diamond Armlet

Steal Accessory
Fire, Ice 4, Empower



Richardserious
Female
Capricorn
81
56
Mediator
White Magic
Brave Save
Equip Axe
Levitate

Flail

Red Hood
Silk Robe
Sprint Shoes

Invitation, Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate
Cure 2, Raise 2, Reraise, Protect 2, Shell 2

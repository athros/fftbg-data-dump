Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kronikle
Male
Cancer
48
59
Oracle
Battle Skill
Speed Save
Doublehand
Levitate

Bestiary

Flash Hat
White Robe
Defense Armlet

Spell Absorb, Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Sleep
Shield Break, Magic Break, Justice Sword, Night Sword, Explosion Sword



ALY327
Female
Virgo
63
46
Samurai
Steal
Counter Flood
Maintenance
Teleport

Asura Knife

Genji Helmet
Carabini Mail
Feather Mantle

Koutetsu, Kiyomori, Muramasa
Steal Heart, Steal Helmet, Steal Accessory, Steal Status, Arm Aim



Mesmaster
Female
Cancer
75
46
Wizard
Summon Magic
MA Save
Attack UP
Swim

Air Knife

Black Hood
Earth Clothes
Magic Gauntlet

Bolt, Ice 3
Moogle, Ifrit, Titan, Golem, Bahamut, Silf, Zodiac



Lydian C
Female
Capricorn
46
68
Archer
Dance
Hamedo
Concentrate
Ignore Height

Lightning Bow

Platinum Helmet
Leather Outfit
Wizard Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+10
Polka Polka, Nameless Dance, Last Dance, Dragon Pit

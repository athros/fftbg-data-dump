Final Bets: purple - 9 bets for 8,841G (64.0%, x0.56); brown - 7 bets for 4,971G (36.0%, x1.78)

purple bets:
twelfthrootoftwo: 5,144G (58.2%, 10,088G)
WitchHunterIX: 1,000G (11.3%, 15,039G)
porkchop5158: 1,000G (11.3%, 32,471G)
randgridr: 533G (6.0%, 533G)
BoneMiser: 460G (5.2%, 460G)
JLinkletter: 303G (3.4%, 303G)
datadrivenbot: 200G (2.3%, 29,820G)
Powermhero: 101G (1.1%, 22,716G)
DHaveWord: 100G (1.1%, 9,143G)

brown bets:
Mudrockk: 1,000G (20.1%, 8,689G)
BirbBrainsBot: 1,000G (20.1%, 188,272G)
getthemoneyz: 1,000G (20.1%, 1,303,622G)
Sairentozon7: 1,000G (20.1%, 17,257G)
superdevon1: 547G (11.0%, 10,954G)
AllInBot: 303G (6.1%, 303G)
Lydian_C: 121G (2.4%, 85,465G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Silverthundr
Female
Libra
38
59
Samurai
Dance
Catch
Secret Hunt
Waterbreathing

Kiyomori

Cross Helmet
Mythril Armor
Diamond Armlet

Koutetsu, Heaven's Cloud, Muramasa
Slow Dance, Polka Polka, Last Dance, Obsidian Blade



Electric Glass
Female
Capricorn
45
52
Priest
Talk Skill
MP Restore
Beastmaster
Jump+2

White Staff

Golden Hairpin
Linen Robe
Small Mantle

Cure, Cure 3, Protect, Shell, Shell 2, Wall, Esuna, Holy
Preach, Insult, Refute



Sect Cor
Female
Aquarius
48
71
Geomancer
Draw Out
Counter Tackle
Dual Wield
Teleport

Giant Axe
Slasher
Leather Hat
Silk Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Kikuichimoji



JustDoomathon
Female
Pisces
79
74
Mediator
Charge
Brave Up
Equip Armor
Waterbreathing

Air Knife

Red Hood
Genji Armor
Magic Ring

Praise, Threaten, Solution, Insult, Mimic Daravon, Refute
Charge+4, Charge+10, Charge+20

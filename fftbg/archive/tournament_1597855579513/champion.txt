Player: !zChamp
Team: Champion Team
Palettes: White/Blue



DesertWooder
Female
Virgo
78
42
Dancer
Battle Skill
Catch
Martial Arts
Lava Walking

Cashmere

Twist Headband
Wizard Robe
Leather Mantle

Disillusion, Nameless Dance, Obsidian Blade
Shield Break, Weapon Break, Magic Break, Power Break



Chuckolator
Male
Libra
68
71
Archer
Summon Magic
MP Restore
Concentrate
Teleport

Ultimus Bow

Crystal Helmet
Mythril Vest
Reflect Ring

Charge+1, Charge+4, Charge+7
Moogle, Shiva, Titan, Fairy



Joewcarson
Male
Aries
47
53
Lancer
Draw Out
Counter
Long Status
Move+3

Obelisk
Escutcheon
Crystal Helmet
Genji Armor
Jade Armlet

Level Jump5, Vertical Jump7
Murasame, Muramasa



Forkmore
Male
Virgo
55
80
Lancer
Elemental
HP Restore
Short Status
Move-MP Up

Partisan
Aegis Shield
Gold Helmet
Leather Armor
Dracula Mantle

Level Jump8, Vertical Jump6
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm

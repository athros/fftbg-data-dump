Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Shakarak
Female
Libra
51
77
Wizard
Draw Out
Hamedo
Short Charge
Move+2

Main Gauche

Green Beret
Silk Robe
Magic Ring

Fire 3, Bolt 2, Bolt 3, Bolt 4, Empower
Bizen Boat



Dakren
Male
Aries
56
60
Lancer
Charge
PA Save
Doublehand
Waterbreathing

Obelisk

Diamond Helmet
Genji Armor
Dracula Mantle

Level Jump4, Vertical Jump3
Charge+1



Meyples
Female
Virgo
40
55
Mediator
Battle Skill
Earplug
Equip Knife
Levitate

Poison Rod

Black Hood
Chameleon Robe
Bracer

Invitation, Praise, Preach, Solution, Negotiate
Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword, Night Sword



Thehornfreak84
Female
Pisces
59
65
Calculator
White Magic
Critical Quick
Sicken
Jump+1

Bestiary

Holy Miter
Linen Robe
Magic Gauntlet

Height, Prime Number, 5
Cure 4, Raise, Raise 2, Regen, Shell, Wall, Esuna

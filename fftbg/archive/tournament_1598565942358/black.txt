Player: !Black
Team: Black Team
Palettes: Black/Red



CorpusCav
Female
Taurus
52
59
Summoner
Dance
Parry
Long Status
Ignore Terrain

Papyrus Codex

Holy Miter
Brigandine
Vanish Mantle

Moogle, Ifrit, Carbunkle
Polka Polka



Galkife
Male
Gemini
44
74
Ninja
Elemental
Counter Magic
Equip Bow
Jump+1

Bow Gun
Snipe Bow
Twist Headband
Wizard Outfit
Defense Ring

Bomb, Staff
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



IndecisiveNinja
Male
Aries
57
42
Mime

Counter Magic
Attack UP
Move-HP Up



Triangle Hat
Judo Outfit
Red Shoes

Mimic




Tarheels218
Female
Aries
72
61
Thief
Time Magic
Counter Magic
Long Status
Move+3

Cultist Dagger

Golden Hairpin
Clothes
Diamond Armlet

Steal Heart, Steal Accessory
Haste 2, Float, Quick, Stabilize Time

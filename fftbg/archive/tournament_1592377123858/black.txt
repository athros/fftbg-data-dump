Player: !Black
Team: Black Team
Palettes: Black/Red



Snoopiku
Male
Leo
66
73
Archer
Throw
Damage Split
Magic Attack UP
Move-HP Up

Night Killer
Ice Shield
Holy Miter
Black Costume
108 Gems

Charge+2, Charge+3, Charge+7, Charge+10
Shuriken, Sword



Evewho
Female
Virgo
63
77
Knight
Jump
Absorb Used MP
Short Charge
Jump+1

Rune Blade
Bronze Shield
Gold Helmet
Platinum Armor
Feather Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Stasis Sword
Level Jump8, Vertical Jump3



ALY327
Female
Scorpio
54
50
Monk
Charge
Sunken State
Short Charge
Jump+3



Leather Hat
Mythril Vest
Battle Boots

Spin Fist, Wave Fist, Secret Fist, Purification, Revive
Charge+1, Charge+2, Charge+7



CapnChaos12
Male
Pisces
67
66
Lancer
Black Magic
Counter
Doublehand
Move+3

Spear

Mythril Helmet
Leather Armor
Defense Armlet

Level Jump4, Vertical Jump8
Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 4

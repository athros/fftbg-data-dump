Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Axelrod
Male
Virgo
60
49
Oracle
Steal
Arrow Guard
Equip Shield
Swim

Cypress Rod
Genji Shield
Leather Hat
Chameleon Robe
Sprint Shoes

Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep
Steal Armor, Steal Shield, Steal Accessory, Arm Aim



Loveyouallfriends
Female
Aries
45
69
Mime

Abandon
Martial Arts
Teleport



Red Hood
Judo Outfit
Defense Ring

Mimic




Powergems
Male
Virgo
45
77
Bard
Steal
Brave Up
Dual Wield
Ignore Height

Fairy Harp
Fairy Harp
Golden Hairpin
Chain Vest
Bracer

Angel Song, Life Song, Magic Song
Steal Heart, Steal Shield, Steal Weapon, Steal Status



Av0ndale
Female
Pisces
41
60
Samurai
Basic Skill
Absorb Used MP
Short Status
Jump+3

Obelisk

Genji Helmet
Wizard Robe
N-Kai Armlet

Bizen Boat
Accumulate, Dash, Wish

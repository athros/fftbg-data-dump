Player: !Green
Team: Green Team
Palettes: Green/White



SeedSC
Male
Virgo
80
58
Mediator
Black Magic
Counter Magic
Equip Bow
Move+2

Silver Bow

Headgear
Light Robe
Angel Ring

Threaten, Solution, Negotiate, Mimic Daravon
Fire 3, Fire 4, Bolt, Ice, Ice 2, Ice 3, Ice 4



Rastanar
Male
Sagittarius
73
41
Calculator
Black Magic
Earplug
Magic Attack UP
Levitate

Bestiary

Red Hood
Silk Robe
Power Wrist

CT, Height, 5
Bolt 4, Ice, Flare



Truestrike
Male
Virgo
40
80
Oracle
Draw Out
Sunken State
Equip Bow
Waterbreathing

Windslash Bow

Feather Hat
Linen Robe
Jade Armlet

Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep
Bizen Boat, Murasame, Muramasa, Kikuichimoji



Faplo
Female
Sagittarius
76
63
Priest
Time Magic
MP Restore
Defense UP
Jump+1

Wizard Staff

Headgear
Mythril Vest
108 Gems

Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna, Holy
Haste 2, Immobilize, Quick, Demi 2, Stabilize Time, Galaxy Stop

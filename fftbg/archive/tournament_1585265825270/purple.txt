Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaateXIII
Female
Aquarius
45
69
Summoner
Draw Out
Parry
Short Charge
Ignore Terrain

White Staff

Golden Hairpin
Silk Robe
Defense Armlet

Moogle, Titan, Carbunkle, Salamander, Fairy, Cyclops
Bizen Boat, Muramasa, Kikuichimoji



Dothan89
Male
Pisces
69
69
Lancer
Punch Art
PA Save
Sicken
Waterwalking

Obelisk
Mythril Shield
Circlet
Mythril Armor
Elf Mantle

Level Jump2, Vertical Jump8
Earth Slash, Purification, Chakra, Revive, Seal Evil



BadBlanket
Female
Gemini
43
74
Knight
Talk Skill
Counter
Equip Gun
Levitate

Mythril Gun
Gold Shield
Crystal Helmet
Carabini Mail
Leather Mantle

Head Break, Power Break, Mind Break
Persuade, Threaten, Preach, Mimic Daravon, Rehabilitate



Phrossi V
Male
Pisces
78
58
Thief
Sing
Counter Tackle
Equip Axe
Levitate

Gold Staff

Triangle Hat
Brigandine
Leather Mantle

Steal Shield, Steal Accessory
Magic Song, Diamond Blade, Sky Demon

Player: !White
Team: White Team
Palettes: White/Blue



WhiteTigress
Female
Virgo
49
61
Calculator
Time Magic
Absorb Used MP
Equip Gun
Retreat

Stone Gun

Black Hood
Adaman Vest
Magic Gauntlet

CT, Prime Number, 5, 4, 3
Haste, Haste 2, Stop, Immobilize, Float, Reflect, Stabilize Time



Shalloween
Male
Virgo
76
43
Archer
White Magic
Mana Shield
Attack UP
Jump+3

Bow Gun
Mythril Shield
Platinum Helmet
Wizard Outfit
N-Kai Armlet

Charge+1, Charge+2, Charge+4, Charge+5, Charge+7
Raise 2, Regen, Shell 2, Esuna



ANFz
Male
Scorpio
71
72
Chemist
White Magic
Damage Split
Maintenance
Ignore Height

Cute Bag

Triangle Hat
Adaman Vest
Genji Gauntlet

Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Cure, Cure 3, Shell, Shell 2, Esuna, Holy



Nekojin
Female
Libra
61
63
Knight
Summon Magic
Mana Shield
Equip Gun
Move-HP Up

Romanda Gun
Buckler
Bronze Helmet
Gold Armor
Bracer

Magic Break, Power Break, Justice Sword, Night Sword
Moogle, Golem, Odin, Leviathan, Salamander, Silf, Cyclops

Player: !Black
Team: Black Team
Palettes: Black/Red



Grininda
Female
Taurus
54
49
Knight
Throw
Absorb Used MP
Short Charge
Jump+1

Slasher
Genji Shield
Grand Helmet
Gold Armor
Wizard Mantle

Head Break, Armor Break, Shield Break, Magic Break, Explosion Sword
Shuriken



WillFitzgerald
Female
Sagittarius
70
80
Knight
Yin Yang Magic
Distribute
Equip Axe
Teleport

Slasher
Flame Shield
Iron Helmet
Leather Armor
Battle Boots

Armor Break, Magic Break, Power Break
Life Drain, Blind Rage, Dispel Magic, Sleep



PlatinumPlume
Female
Cancer
75
42
Chemist
Summon Magic
PA Save
Beastmaster
Swim

Panther Bag

Flash Hat
Earth Clothes
Sprint Shoes

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Moogle, Ifrit, Titan, Odin, Leviathan, Fairy



Winterharte
Male
Aries
55
54
Ninja
Basic Skill
MA Save
Equip Gun
Move+2

Bestiary
Battle Folio
Green Beret
Mystic Vest
Jade Armlet

Shuriken, Bomb
Accumulate, Dash, Heal, Yell

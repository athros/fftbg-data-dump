Final Bets: green - 9 bets for 6,995G (67.8%, x0.47); yellow - 7 bets for 3,316G (32.2%, x2.11)

green bets:
Powermhero: 2,444G (34.9%, 24,444G)
serperemagus: 1,195G (17.1%, 1,195G)
BirbBrainsBot: 1,000G (14.3%, 2,115G)
getthemoneyz: 1,000G (14.3%, 948,487G)
Monopool: 483G (6.9%, 483G)
gorgewall: 301G (4.3%, 22,406G)
LAGBOT30000: 250G (3.6%, 2,243G)
Zachara: 222G (3.2%, 102,422G)
Firesheath: 100G (1.4%, 9,140G)

yellow bets:
prince_rogers_nelson_: 1,000G (30.2%, 3,105G)
Evewho: 1,000G (30.2%, 18,733G)
AllInBot: 548G (16.5%, 548G)
ColetteMSLP: 368G (11.1%, 368G)
alithesillybird: 200G (6.0%, 3,465G)
datadrivenbot: 100G (3.0%, 39,725G)
KSops: 100G (3.0%, 1,656G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gooseyourself
Female
Leo
52
59
Archer
Yin Yang Magic
Meatbone Slash
Doublehand
Teleport

Blast Gun

Red Hood
Chain Vest
N-Kai Armlet

Charge+2, Charge+3, Charge+5
Spell Absorb, Life Drain, Silence Song, Foxbird, Dispel Magic



Twelfthrootoftwo
Male
Aries
78
80
Chemist
Time Magic
Caution
Concentrate
Move-MP Up

Mythril Gun

Twist Headband
Chain Vest
Power Wrist

Potion, Hi-Potion, Ether, Antidote, Echo Grass, Soft, Remedy, Phoenix Down
Haste, Haste 2, Stop, Float, Reflect, Quick, Demi 2



Evewho
Female
Scorpio
49
69
Mediator
Yin Yang Magic
Distribute
Equip Armor
Move+3

Blaze Gun

Crystal Helmet
Mystic Vest
Feather Boots

Invitation, Persuade, Praise, Threaten, Negotiate
Poison, Silence Song, Paralyze



Lowlf
Female
Pisces
46
65
Thief
Punch Art
Speed Save
Magic Attack UP
Waterbreathing

Diamond Sword

Triangle Hat
Judo Outfit
Sprint Shoes

Leg Aim
Wave Fist, Purification, Chakra

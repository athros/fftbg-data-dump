Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Foofermoofer
Female
Scorpio
50
74
Lancer
Black Magic
Brave Save
Sicken
Levitate

Octagon Rod
Ice Shield
Grand Helmet
Silk Robe
Reflect Ring

Level Jump4, Vertical Jump8
Fire 2, Bolt, Ice 2, Empower, Death, Flare



Nohurty
Female
Taurus
79
45
Dancer
Black Magic
Counter Magic
Equip Armor
Waterbreathing

Persia

Barette
Chain Mail
Red Shoes

Witch Hunt, Nameless Dance, Dragon Pit
Fire 3, Ice 4



Laserman1000
Male
Gemini
69
67
Calculator
Curse Skill
Auto Potion
Concentrate
Levitate

Papyrus Codex

Feather Hat
Mythril Vest
Angel Ring

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Lawnboxer
Male
Libra
63
46
Oracle
Sing
Caution
Concentrate
Ignore Terrain

Battle Bamboo

Green Beret
White Robe
Germinas Boots

Doubt Faith, Foxbird, Confusion Song, Dispel Magic
Nameless Song, Last Song, Space Storage

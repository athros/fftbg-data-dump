Final Bets: white - 15 bets for 7,544G (47.9%, x1.09); brown - 16 bets for 8,202G (52.1%, x0.92)

white bets:
E_Ballard: 1,573G (20.9%, 1,573G)
regios91: 1,063G (14.1%, 1,063G)
volgrathemoose: 1,001G (13.3%, 3,948G)
Aldrammech: 600G (8.0%, 600G)
WitchHunterIX: 500G (6.6%, 2,209G)
Bryon_W: 500G (6.6%, 4,061G)
ApplesauceBoss: 424G (5.6%, 424G)
ShintaroNayaka: 400G (5.3%, 6,246G)
rico_flex: 400G (5.3%, 29,339G)
JLinkletter: 289G (3.8%, 289G)
d4rr1n: 208G (2.8%, 208G)
Vex_Novisk: 200G (2.7%, 200G)
itsadud: 186G (2.5%, 186G)
datadrivenbot: 100G (1.3%, 24,297G)
alecttox: 100G (1.3%, 15,548G)

brown bets:
CorpusCav: 1,625G (19.8%, 1,625G)
BirbBrainsBot: 1,000G (12.2%, 63,574G)
bruubarg: 1,000G (12.2%, 1,000G)
ColetteMSLP: 800G (9.8%, 63,772G)
Estan_AD: 651G (7.9%, 1,302G)
getthemoneyz: 650G (7.9%, 688,521G)
EnemyController: 500G (6.1%, 287,602G)
HaateXIII: 492G (6.0%, 492G)
Moshyhero: 340G (4.1%, 340G)
StealthModeLocke: 312G (3.8%, 312G)
twelfthrootoftwo: 300G (3.7%, 29,553G)
slash_kamei: 120G (1.5%, 120G)
soren_of_tyto: 112G (1.4%, 112G)
DeathTaxesAndAnime: 100G (1.2%, 3,154G)
Firesheath: 100G (1.2%, 1,736G)
Sykper: 100G (1.2%, 112G)

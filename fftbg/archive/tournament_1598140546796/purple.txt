Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



StealthModeLocke
Male
Capricorn
54
69
Chemist
Elemental
Parry
Equip Sword
Fly

Bizen Boat

Black Hood
Mystic Vest
Magic Ring

Potion, X-Potion, Ether, Hi-Ether, Antidote, Phoenix Down
Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Lava Ball



Nhammen
Male
Cancer
40
53
Lancer
Item
Parry
Sicken
Retreat

Holy Lance
Bronze Shield
Cross Helmet
Leather Armor
Wizard Mantle

Level Jump3, Vertical Jump2
Potion, Antidote, Eye Drop, Echo Grass, Phoenix Down



Brokenknight201
Male
Leo
49
44
Knight
Sing
PA Save
Doublehand
Ignore Height

Ragnarok

Gold Helmet
Plate Mail
Angel Ring

Weapon Break, Speed Break, Power Break, Mind Break
Cheer Song



Ruleof5
Female
Cancer
80
68
Archer
Steal
Counter Flood
Short Status
Move-MP Up

Snipe Bow
Diamond Shield
Leather Helmet
Mystic Vest
Elf Mantle

Charge+3, Charge+4
Steal Helmet, Steal Armor

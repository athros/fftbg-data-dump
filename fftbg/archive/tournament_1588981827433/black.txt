Player: !Black
Team: Black Team
Palettes: Black/Red



Omegasuspekt
Female
Gemini
74
49
Chemist
Time Magic
Parry
Equip Shield
Swim

Assassin Dagger
Venetian Shield
Red Hood
Clothes
Defense Ring

Potion, Ether, Hi-Ether, Eye Drop, Soft, Phoenix Down
Stop, Stabilize Time



TheNGUYENNER
Male
Virgo
62
45
Chemist
White Magic
Hamedo
Maintenance
Jump+3

Panther Bag

Holy Miter
Rubber Costume
Elf Mantle

Hi-Potion, X-Potion, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Cure 2, Raise, Raise 2, Protect, Shell 2, Esuna, Holy



DrAntiSocial
Male
Aries
57
51
Chemist
Summon Magic
Parry
Dual Wield
Jump+3

Zorlin Shape
Hydra Bag
Leather Hat
Power Sleeve
Feather Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Remedy, Phoenix Down
Moogle, Shiva, Bahamut, Salamander



HaateXIII
Male
Scorpio
52
58
Lancer
Talk Skill
Dragon Spirit
Dual Wield
Move+3

Mythril Spear
Iron Fan
Leather Helmet
Genji Armor
Rubber Shoes

Level Jump5, Vertical Jump6
Invitation, Threaten, Refute

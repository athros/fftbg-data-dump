Final Bets: purple - 5 bets for 2,224G (30.8%, x2.25); brown - 8 bets for 4,998G (69.2%, x0.44)

purple bets:
lowlf: 1,111G (50.0%, 96,806G)
Zachara: 447G (20.1%, 136,947G)
getthemoneyz: 354G (15.9%, 1,131,224G)
CITOtheMOSQUITO: 212G (9.5%, 212G)
InskipJester: 100G (4.5%, 1,176G)

brown bets:
prince_rogers_nelson_: 2,414G (48.3%, 2,414G)
BirbBrainsBot: 1,000G (20.0%, 34,574G)
CT_5_Holy: 500G (10.0%, 3,061G)
twelfthrootoftwo: 500G (10.0%, 18,835G)
foofermoofer: 284G (5.7%, 284G)
Ring_Wyrm: 100G (2.0%, 6,307G)
datadrivenbot: 100G (2.0%, 49,903G)
DuneMeta: 100G (2.0%, 5,600G)

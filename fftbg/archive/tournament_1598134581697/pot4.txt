Final Bets: purple - 10 bets for 9,733G (65.4%, x0.53); brown - 9 bets for 5,152G (34.6%, x1.89)

purple bets:
AllInBot: 4,612G (47.4%, 4,612G)
Zeroroute: 1,299G (13.3%, 3,249G)
killth3kid: 1,000G (10.3%, 44,714G)
ruleof5: 1,000G (10.3%, 12,260G)
WhiteTigress: 600G (6.2%, 9,119G)
nekojin: 512G (5.3%, 512G)
ArfArfArfArfArfArfArfArf: 300G (3.1%, 503G)
datadrivenbot: 200G (2.1%, 61,710G)
neocarbuncle: 110G (1.1%, 1,261G)
SephDarkheart: 100G (1.0%, 134,845G)

brown bets:
maximumcrit: 1,086G (21.1%, 1,086G)
BirbBrainsBot: 1,000G (19.4%, 129,497G)
Thyrandaal: 1,000G (19.4%, 28,440G)
SkylerBunny: 800G (15.5%, 3,332G)
getthemoneyz: 438G (8.5%, 1,706,649G)
CosmicTactician: 300G (5.8%, 36,859G)
theBinklive: 216G (4.2%, 216G)
Lemonjohns: 200G (3.9%, 7,413G)
Docrad_: 112G (2.2%, 112G)

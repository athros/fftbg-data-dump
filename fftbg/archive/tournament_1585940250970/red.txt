Player: !Red
Team: Red Team
Palettes: Red/Brown



Yure1x
Male
Gemini
44
74
Samurai
Talk Skill
Counter Tackle
Secret Hunt
Retreat

Kiyomori

Mythril Helmet
Maximillian
Jade Armlet

Asura, Koutetsu, Murasame, Kikuichimoji
Praise, Threaten, Refute



Mrasin
Male
Taurus
45
67
Lancer
Summon Magic
HP Restore
Magic Defense UP
Lava Walking

Mythril Spear
Ice Shield
Iron Helmet
White Robe
Jade Armlet

Level Jump8, Vertical Jump7
Shiva, Titan



SeedSC
Female
Aries
67
56
Priest
Throw
Regenerator
Sicken
Waterbreathing

Mace of Zeus

Red Hood
Black Costume
Feather Mantle

Cure, Cure 2, Cure 3, Raise, Raise 2, Protect, Protect 2, Wall, Esuna
Staff



BenYuPoker
Female
Gemini
79
48
Summoner
Yin Yang Magic
Counter Magic
Short Charge
Jump+1

Ice Rod

Triangle Hat
Power Sleeve
Rubber Shoes

Moogle, Titan, Carbunkle, Bahamut, Salamander, Silf
Poison, Zombie, Blind Rage, Dispel Magic, Paralyze

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Friendsquirrel
Female
Leo
66
48
Knight
Steal
MP Restore
Short Charge
Levitate

Diamond Sword
Aegis Shield
Circlet
Leather Armor
Spike Shoes

Armor Break, Shield Break, Power Break, Justice Sword, Explosion Sword
Steal Armor, Arm Aim



Old Overholt
Female
Cancer
63
50
Geomancer
Talk Skill
Critical Quick
Beastmaster
Fly

Giant Axe
Crystal Shield
Flash Hat
Robe of Lords
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Invitation, Threaten, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate



OneHundredFists
Female
Leo
62
70
Chemist
Basic Skill
Parry
Concentrate
Move+3

Star Bag

Feather Hat
Adaman Vest
Defense Armlet

Potion, Hi-Potion, Elixir, Echo Grass, Phoenix Down
Heal, Tickle, Yell



Rexamajinx
Male
Gemini
51
73
Priest
Elemental
Critical Quick
Beastmaster
Swim

Rainbow Staff

Feather Hat
Earth Clothes
Small Mantle

Cure, Cure 2, Cure 4, Raise, Raise 2, Shell, Wall, Esuna, Holy
Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

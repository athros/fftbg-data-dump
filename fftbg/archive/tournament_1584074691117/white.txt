Player: !White
Team: White Team
Palettes: White/Blue



Cheesetacular
Female
Libra
67
71
Wizard
Item
Mana Shield
Throw Item
Ignore Height

Wizard Rod

Holy Miter
Secret Clothes
Germinas Boots

Fire 3, Bolt 3, Bolt 4
Potion, Hi-Potion, Soft, Holy Water, Remedy, Phoenix Down



AoiTsukiYuri
Female
Cancer
63
76
Calculator
Black Magic
Counter Flood
Maintenance
Jump+2

Ice Rod

Green Beret
Clothes
Defense Armlet

CT, Height, Prime Number, 5, 3
Fire, Fire 3, Bolt 2, Bolt 4



ManaBirb
Female
Gemini
70
49
Time Mage
Summon Magic
Counter Tackle
Doublehand
Move+3

Sage Staff

Feather Hat
Power Sleeve
Jade Armlet

Slow, Slow 2, Immobilize, Reflect, Stabilize Time
Moogle, Shiva, Ifrit, Salamander



FullMetalScrubby
Male
Scorpio
45
62
Chemist
Battle Skill
HP Restore
Equip Shield
Lava Walking

Dagger
Escutcheon
Leather Hat
Black Costume
Power Wrist

Hi-Potion, Eye Drop, Maiden's Kiss, Phoenix Down
Armor Break, Shield Break, Magic Break, Justice Sword, Night Sword

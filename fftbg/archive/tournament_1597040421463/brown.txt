Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zenlion
Male
Aquarius
44
78
Archer
Talk Skill
Faith Save
Martial Arts
Move+2

Romanda Gun
Gold Shield
Headgear
Clothes
Power Wrist

Charge+4, Charge+10
Invitation, Solution, Refute



Sinnyil2
Female
Gemini
56
75
Dancer
Basic Skill
Counter
Beastmaster
Move-MP Up

Persia

Feather Hat
Judo Outfit
108 Gems

Disillusion, Last Dance, Void Storage, Dragon Pit
Heal, Scream



Artea
Male
Capricorn
49
80
Bard
White Magic
Arrow Guard
Defend
Teleport

Ramia Harp

Leather Hat
Mythril Vest
Red Shoes

Angel Song, Life Song, Cheer Song, Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Raise, Shell 2, Wall, Magic Barrier



Go2sleepTV
Male
Scorpio
60
57
Lancer
Summon Magic
Blade Grasp
Dual Wield
Fly

Holy Lance
Battle Bamboo
Bronze Helmet
Leather Armor
Dracula Mantle

Level Jump5, Vertical Jump7
Moogle, Ramuh, Ifrit, Carbunkle

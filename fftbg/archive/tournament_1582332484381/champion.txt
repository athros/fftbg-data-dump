Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



BurningSaph
Male
Serpentarius
68
68
Oracle
Punch Art
Absorb Used MP
Equip Shield
Fly

Gokuu Rod
Round Shield
Green Beret
Chameleon Robe
Bracer

Poison, Spell Absorb, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic
Pummel, Secret Fist, Purification, Chakra, Revive



Panushenko
Male
Leo
68
66
Lancer
Battle Skill
MP Restore
Attack UP
Jump+2

Javelin
Aegis Shield
Bronze Helmet
Chain Mail
Germinas Boots

Level Jump2, Vertical Jump5
Magic Break, Speed Break, Power Break



Mtueni
Monster
Sagittarius
62
67
Red Chocobo










Ominnous
Male
Sagittarius
69
59
Knight
Time Magic
Faith Up
Long Status
Move+3

Ragnarok

Genji Helmet
Robe of Lords
Elf Mantle

Armor Break, Shield Break, Power Break, Mind Break, Justice Sword, Dark Sword
Haste, Float, Reflect, Demi 2, Stabilize Time

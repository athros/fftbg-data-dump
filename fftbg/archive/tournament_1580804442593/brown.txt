Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Victoriolue
Female
Taurus
61
68
Wizard
Draw Out
Catch
Doublehand
Ignore Height

Dragon Rod

Green Beret
Black Robe
Bracer

Fire, Fire 4, Bolt 4, Ice 2, Ice 4, Empower
Asura, Muramasa



CynicalRazor
Female
Pisces
74
74
Dancer
Black Magic
HP Restore
Dual Wield
Move-MP Up

Ryozan Silk
Persia
Flash Hat
Leather Vest
Angel Ring

Wiznaibus, Slow Dance, Nameless Dance, Last Dance
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice, Ice 2, Ice 4



Time  Mage
Male
Capricorn
68
77
Monk
Time Magic
Dragon Spirit
Long Status
Swim



Headgear
Brigandine
Power Wrist

Spin Fist, Pummel, Wave Fist, Purification, Seal Evil
Slow, Slow 2, Quick, Demi, Stabilize Time



LDHaten
Male
Sagittarius
78
73
Lancer
Black Magic
Parry
Equip Shield
Move-HP Up

Javelin
Bronze Shield
Cross Helmet
Bronze Armor
Magic Gauntlet

Level Jump4, Vertical Jump5
Bolt 3, Empower

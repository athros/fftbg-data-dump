Player: !Red
Team: Red Team
Palettes: Red/Brown



Evontno
Male
Aquarius
55
61
Wizard
Elemental
Counter
Magic Defense UP
Teleport

Main Gauche

Leather Hat
Black Robe
Genji Gauntlet

Bolt 2, Bolt 3, Ice, Ice 3, Ice 4, Death
Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball



JoeykinsX
Female
Pisces
52
44
Summoner
Time Magic
MP Restore
Attack UP
Move+1

Thunder Rod

Holy Miter
White Robe
Sprint Shoes

Titan, Carbunkle, Odin, Leviathan, Silf, Fairy, Lich
Slow 2, Quick, Demi 2, Meteor



Hasterious
Female
Capricorn
41
72
Oracle
Summon Magic
Counter Magic
Short Charge
Ignore Terrain

Cypress Rod

Black Hood
Power Sleeve
Power Wrist

Blind, Life Drain, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify
Shiva, Golem, Carbunkle, Leviathan, Salamander, Fairy



TheChainNerd
Male
Aries
75
56
Knight
Draw Out
Sunken State
Attack UP
Levitate

Save the Queen
Mythril Shield
Diamond Helmet
Plate Mail
Leather Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Dark Sword, Night Sword
Koutetsu

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PoroTact
Female
Capricorn
47
67
Dancer
Basic Skill
Counter Magic
Sicken
Jump+2

Persia

Twist Headband
Black Costume
Magic Gauntlet

Witch Hunt, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit
Dash, Heal, Tickle, Fury, Wish



Pyrelli
Female
Gemini
49
42
Thief
Yin Yang Magic
Critical Quick
Equip Polearm
Swim

Javelin

Thief Hat
Leather Outfit
Defense Armlet

Steal Weapon, Steal Status
Pray Faith, Doubt Faith, Blind Rage, Petrify, Dark Holy



Just Here2
Female
Aries
52
73
Dancer
Item
Counter Flood
Throw Item
Move+2

Panther Bag

Feather Hat
Clothes
Elf Mantle

Wiznaibus, Dragon Pit
Potion, Hi-Potion, Ether, Holy Water, Phoenix Down



ShintaroNayaka
Male
Libra
64
48
Archer
Time Magic
Arrow Guard
Equip Sword
Jump+3

Broad Sword
Diamond Shield
Triangle Hat
Mythril Vest
Reflect Ring

Charge+3
Haste, Stop, Float, Stabilize Time, Meteor

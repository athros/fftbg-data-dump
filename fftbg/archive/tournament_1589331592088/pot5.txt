Final Bets: red - 35 bets for 36,012G (68.7%, x0.46); green - 27 bets for 16,386G (31.3%, x2.20)

red bets:
Digitalsocrates: 6,661G (18.5%, 13,322G)
moonliquor: 5,000G (13.9%, 21,353G)
reinoe: 2,903G (8.1%, 2,903G)
Lordminsc: 2,462G (6.8%, 4,924G)
killth3kid: 2,000G (5.6%, 13,049G)
HaateXIII: 1,822G (5.1%, 1,822G)
rico_flex: 1,500G (4.2%, 25,991G)
itsdigitalbro: 1,234G (3.4%, 15,792G)
thunderducker: 1,122G (3.1%, 5,572G)
cupholderr: 1,122G (3.1%, 57,187G)
theNGUYENNER: 1,000G (2.8%, 13,410G)
mannequ1n: 1,000G (2.8%, 7,673G)
Virilikus: 1,000G (2.8%, 143,591G)
reshiramdude16: 901G (2.5%, 901G)
HaplessOne: 555G (1.5%, 23,715G)
Juggaloslip: 508G (1.4%, 508G)
EnemyController: 500G (1.4%, 229,934G)
pstud: 500G (1.4%, 5,104G)
solomongrundy85: 500G (1.4%, 17,422G)
turquoise_mage: 500G (1.4%, 6,113G)
HaychDub: 450G (1.2%, 5,191G)
Cryptopsy70: 420G (1.2%, 2,681G)
SamaelMerihem: 408G (1.1%, 408G)
manawatts: 300G (0.8%, 868G)
LAGBOT30000: 282G (0.8%, 28,264G)
bahamutlagooon: 274G (0.8%, 5,489G)
TheJonTerp: 200G (0.6%, 2,996G)
mpghappiness: 173G (0.5%, 340G)
ANFz: 121G (0.3%, 49,623G)
FaytKion: 104G (0.3%, 104G)
lastly: 100G (0.3%, 9,261G)
Kyune: 100G (0.3%, 5,658G)
monsturmilk: 100G (0.3%, 100G)
datadrivenbot: 100G (0.3%, 13,814G)
Sairentozon7: 90G (0.2%, 2,094G)

green bets:
SkylerBunny: 2,500G (15.3%, 51,465G)
Zeroroute: 1,462G (8.9%, 3,230G)
Lord_Burrah: 1,000G (6.1%, 100,000G)
ungabunga_bot: 1,000G (6.1%, 472,304G)
BirbBrainsBot: 1,000G (6.1%, 10,543G)
LDSkinny: 1,000G (6.1%, 49,257G)
Lionhermit: 1,000G (6.1%, 46,649G)
sinnyil2: 900G (5.5%, 58,410G)
CorpusCav: 800G (4.9%, 15,268G)
concles: 777G (4.7%, 21,688G)
Laserman1000: 627G (3.8%, 24,927G)
otakutaylor: 546G (3.3%, 546G)
Baron_von_Scrub: 535G (3.3%, 25,756G)
vorap: 500G (3.1%, 46,603G)
volgrathemoose: 500G (3.1%, 7,740G)
FriendSquirrel: 300G (1.8%, 300G)
Evewho: 300G (1.8%, 300G)
LeepingJJ: 296G (1.8%, 296G)
nifboy: 250G (1.5%, 1,731G)
old_overholt_: 200G (1.2%, 8,861G)
Kronikle: 200G (1.2%, 4,657G)
GnielKnows: 195G (1.2%, 390G)
E_Ballard: 100G (0.6%, 3,158G)
benticore: 100G (0.6%, 2,765G)
4shcrows: 100G (0.6%, 1,179G)
FoeSquirrel: 100G (0.6%, 5,204G)
getthemoneyz: 98G (0.6%, 671,410G)

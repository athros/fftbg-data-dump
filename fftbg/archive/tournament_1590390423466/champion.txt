Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lowlf
Female
Leo
47
76
Thief
Dance
Counter
Concentrate
Fly

Air Knife

Feather Hat
Judo Outfit
Dracula Mantle

Steal Shield, Steal Status
Witch Hunt, Wiznaibus, Disillusion, Nameless Dance



Reinoe
Male
Gemini
74
78
Lancer
Battle Skill
Speed Save
Equip Armor
Jump+1

Mythril Spear
Bronze Shield
Black Hood
Chain Mail
Germinas Boots

Level Jump5, Vertical Jump7
Shield Break, Power Break, Night Sword



Benticore
Monster
Aries
74
76
Wyvern










Kronikle
Female
Capricorn
70
54
Ninja
Jump
Critical Quick
Equip Gun
Move+2

Fairy Harp
Ramia Harp
Barette
Judo Outfit
Diamond Armlet

Shuriken, Bomb, Sword, Staff, Ninja Sword
Level Jump3, Vertical Jump8

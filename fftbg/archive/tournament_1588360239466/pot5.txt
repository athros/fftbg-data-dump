Final Bets: blue - 23 bets for 19,515G (46.3%, x1.16); green - 27 bets for 22,659G (53.7%, x0.86)

blue bets:
ShintaroNayaka: 2,631G (13.5%, 2,631G)
TheChainNerd: 2,332G (11.9%, 11,660G)
gooseyourself: 2,000G (10.2%, 20,719G)
Shakarak: 2,000G (10.2%, 19,787G)
Zeroroute: 1,556G (8.0%, 1,556G)
SaintOmerville: 1,256G (6.4%, 1,256G)
aStatue: 1,100G (5.6%, 1,100G)
Grimmace45: 941G (4.8%, 9,411G)
fenaen: 820G (4.2%, 820G)
Master_Metroid: 800G (4.1%, 2,846G)
Odin007_88: 736G (3.8%, 736G)
Chuckolator: 513G (2.6%, 22,365G)
HaychDub: 500G (2.6%, 8,103G)
deenglow: 500G (2.6%, 68,989G)
ZephyrTempest: 456G (2.3%, 1,665G)
corwinambrose: 300G (1.5%, 986G)
ungabunga_bot: 261G (1.3%, 288,065G)
LazarusAjin: 200G (1.0%, 22,803G)
arch8000: 200G (1.0%, 8,428G)
roofiepops: 112G (0.6%, 14,866G)
volgrathemoose: 101G (0.5%, 6,879G)
nifboy: 100G (0.5%, 6,515G)
datadrivenbot: 100G (0.5%, 11,685G)

green bets:
DeathTaxesAndAnime: 2,555G (11.3%, 5,010G)
Zbgs: 2,500G (11.0%, 60,972G)
sinnyil2: 2,222G (9.8%, 39,452G)
Pie108: 2,000G (8.8%, 192,025G)
catfashions: 1,853G (8.2%, 1,853G)
HaateXIII: 1,518G (6.7%, 3,036G)
joewcarson: 1,178G (5.2%, 1,178G)
ACSpree: 1,161G (5.1%, 1,161G)
TheRylander: 1,145G (5.1%, 1,145G)
BirbBrainsBot: 1,000G (4.4%, 166,948G)
Fenrislfr: 1,000G (4.4%, 14,526G)
LDSkinny: 1,000G (4.4%, 3,736G)
SQUiDSQUARKLIN: 760G (3.4%, 760G)
Laserman1000: 631G (2.8%, 84,631G)
vampyreinabox: 300G (1.3%, 2,281G)
TheKillerNacho: 250G (1.1%, 880G)
run_with_STONE_GUNS: 250G (1.1%, 1,368G)
getthemoneyz: 236G (1.0%, 612,436G)
old_overholt_: 200G (0.9%, 2,932G)
ANFz: 200G (0.9%, 25,744G)
Arlum: 200G (0.9%, 950G)
Jerboj: 100G (0.4%, 62,413G)
Tougou: 100G (0.4%, 7,935G)
GeeGXP: 100G (0.4%, 100G)
Bioticism: 100G (0.4%, 13,917G)
DrAntiSocial: 50G (0.2%, 7,175G)
brokenknight201: 50G (0.2%, 500G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Carbonpile
Female
Libra
67
66
Ninja
Talk Skill
Parry
Concentrate
Move+1

Cultist Dagger
Main Gauche
Cachusha
Brigandine
Small Mantle

Shuriken, Bomb, Wand
Solution, Death Sentence, Mimic Daravon, Rehabilitate



YaBoy125
Male
Aquarius
62
69
Knight
Punch Art
Mana Shield
Halve MP
Teleport

Ice Brand
Mythril Shield
Bronze Helmet
Leather Armor
Feather Mantle

Head Break, Shield Break, Speed Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Pummel, Wave Fist, Revive



SirOwnzAlot
Female
Aries
61
69
Oracle
Item
MP Restore
Throw Item
Jump+1

Iron Fan

Holy Miter
Leather Vest
Cherche

Spell Absorb, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Potion, X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Sinnyil2
Male
Libra
64
73
Wizard
Time Magic
Counter
Secret Hunt
Teleport

Air Knife

Thief Hat
Black Robe
Diamond Armlet

Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3
Haste, Slow 2, Reflect, Demi

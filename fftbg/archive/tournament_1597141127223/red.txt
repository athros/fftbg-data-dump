Player: !Red
Team: Red Team
Palettes: Red/Brown



PhaseLoss
Male
Capricorn
79
42
Geomancer
Time Magic
Hamedo
Halve MP
Move+2

Kiyomori
Round Shield
Twist Headband
Secret Clothes
Reflect Ring

Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Haste, Reflect, Quick, Demi, Stabilize Time



Gut922
Female
Leo
58
66
Calculator
Demon Skill
Auto Potion
Martial Arts
Teleport



Flash Hat
Reflect Mail
N-Kai Armlet

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Sairentozon7
Male
Sagittarius
62
46
Knight
Charge
Counter
Equip Armor
Jump+1

Slasher
Round Shield
Twist Headband
Leather Armor
Magic Ring

Head Break, Shield Break, Weapon Break, Dark Sword, Night Sword, Explosion Sword
Charge+5, Charge+10, Charge+20



Aithon
Male
Aries
66
47
Summoner
Elemental
Auto Potion
Maintenance
Levitate

Poison Rod

Red Hood
Chain Vest
N-Kai Armlet

Moogle, Titan, Golem, Carbunkle, Salamander
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind

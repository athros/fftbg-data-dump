Player: !White
Team: White Team
Palettes: White/Blue



Solomongrundy85
Male
Aries
48
69
Lancer
Punch Art
Brave Save
Equip Axe
Waterbreathing

Slasher
Diamond Shield
Circlet
Reflect Mail
Feather Mantle

Level Jump8, Vertical Jump7
Spin Fist, Earth Slash, Purification, Seal Evil



Spootoo
Female
Pisces
49
59
Chemist
Draw Out
Distribute
Equip Shield
Move-MP Up

Hydra Bag
Escutcheon
Leather Hat
Judo Outfit
Setiemson

Potion, X-Potion, Ether, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Bizen Boat, Murasame



Evdoggity
Male
Leo
46
54
Chemist
Steal
Damage Split
Doublehand
Fly

Star Bag

Black Hood
Adaman Vest
Diamond Armlet

Potion, Ether, Hi-Ether, Phoenix Down
Steal Armor, Steal Shield, Steal Status, Leg Aim



Nekojin
Male
Aquarius
48
75
Thief
Jump
Critical Quick
Magic Attack UP
Levitate

Coral Sword

Red Hood
Mythril Vest
Chantage

Steal Shield, Steal Accessory, Leg Aim
Level Jump2, Vertical Jump7

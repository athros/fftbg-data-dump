Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Elelor
Female
Leo
56
72
Wizard
Punch Art
Arrow Guard
Attack UP
Move+3

Rod

Holy Miter
Secret Clothes
Diamond Armlet

Fire, Fire 4, Bolt 2, Bolt 4, Ice 2
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Valvalis
Female
Capricorn
67
80
Knight
Dance
Critical Quick
Concentrate
Ignore Terrain

Defender

Diamond Helmet
Reflect Mail
Sprint Shoes

Armor Break, Magic Break, Mind Break, Dark Sword, Night Sword
Slow Dance, Polka Polka, Disillusion, Void Storage



KantsugaLoL
Male
Scorpio
42
57
Lancer
Steal
Parry
Defense UP
Move-HP Up

Holy Lance
Crystal Shield
Crystal Helmet
Gold Armor
Magic Gauntlet

Level Jump4, Vertical Jump8
Steal Weapon, Leg Aim



Taypost
Female
Taurus
75
74
Chemist
Dance
Damage Split
Equip Shield
Move-MP Up

Romanda Gun
Aegis Shield
Holy Miter
Leather Outfit
Bracer

Potion, Echo Grass, Holy Water, Remedy, Phoenix Down
Slow Dance, Polka Polka, Nameless Dance

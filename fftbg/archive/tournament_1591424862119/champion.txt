Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Kronikle
Female
Taurus
76
57
Wizard
Yin Yang Magic
Regenerator
Magic Attack UP
Ignore Height

Dagger

Golden Hairpin
Mythril Vest
Feather Mantle

Fire, Fire 2, Bolt, Bolt 3, Bolt 4, Death
Poison, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic



Digitalsocrates
Female
Libra
66
70
Summoner
Punch Art
Mana Shield
Doublehand
Lava Walking

Faith Rod

Red Hood
Silk Robe
108 Gems

Moogle, Fairy
Spin Fist, Earth Slash, Purification, Chakra, Revive



Powerpinch
Male
Gemini
60
54
Lancer
Black Magic
Distribute
Defense UP
Ignore Terrain

Partisan
Mythril Shield
Mythril Helmet
Linen Cuirass
Jade Armlet

Level Jump5, Vertical Jump8
Fire 2, Fire 4, Bolt, Bolt 3, Ice, Ice 3, Empower, Frog



Flameatron
Male
Gemini
68
54
Time Mage
Talk Skill
Counter Tackle
Monster Talk
Waterbreathing

Rainbow Staff

Twist Headband
Chameleon Robe
Red Shoes

Haste, Haste 2, Reflect, Demi 2
Preach, Negotiate, Refute

Final Bets: green - 12 bets for 8,876G (56.6%, x0.77); champion - 16 bets for 6,795G (43.4%, x1.31)

green bets:
Shalloween: 2,000G (22.5%, 32,161G)
randgridr: 1,801G (20.3%, 3,603G)
NIghtdew14: 1,000G (11.3%, 8,964G)
BirbBrainsBot: 1,000G (11.3%, 33,038G)
pplvee1: 1,000G (11.3%, 5,855G)
coralreeferz: 750G (8.4%, 12,658G)
Aldrammech: 528G (5.9%, 528G)
CapnChaos12: 300G (3.4%, 17,895G)
Wooplestein: 200G (2.3%, 750G)
PoroTact: 100G (1.1%, 6,418G)
holdenmagronik: 100G (1.1%, 1,287G)
LDSkinny: 97G (1.1%, 793G)

champion bets:
JumbocactuarX27: 1,000G (14.7%, 20,565G)
amiture: 1,000G (14.7%, 46,366G)
E_Ballard: 704G (10.4%, 704G)
getthemoneyz: 682G (10.0%, 1,443,834G)
Nizaha: 608G (8.9%, 12,269G)
TasisSai: 500G (7.4%, 8,873G)
Lord_Gwarth: 500G (7.4%, 2,797G)
ShintaroNayaka: 300G (4.4%, 300G)
AllInBot: 269G (4.0%, 269G)
oldchris: 250G (3.7%, 3,781G)
jeffersonsa: 200G (2.9%, 225G)
datadrivenbot: 200G (2.9%, 46,215G)
Calajo: 200G (2.9%, 1,573G)
HolyDragoonXIV: 182G (2.7%, 182G)
DouglasDragonThePoet: 100G (1.5%, 2,626G)
gooblz: 100G (1.5%, 403G)

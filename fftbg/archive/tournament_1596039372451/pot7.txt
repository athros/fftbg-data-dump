Final Bets: green - 15 bets for 12,036G (74.2%, x0.35); purple - 11 bets for 4,179G (25.8%, x2.88)

green bets:
NIghtdew14: 3,819G (31.7%, 7,638G)
randgridr: 1,535G (12.8%, 3,070G)
nifboy: 1,000G (8.3%, 3,162G)
BirbBrainsBot: 1,000G (8.3%, 32,691G)
JumbocactuarX27: 1,000G (8.3%, 20,218G)
DLJuggernaut: 906G (7.5%, 906G)
killth3kid: 552G (4.6%, 10,620G)
Gawyel95: 500G (4.2%, 1,575G)
SephDarkheart: 424G (3.5%, 424G)
Calajo: 400G (3.3%, 1,434G)
CapnChaos12: 300G (2.5%, 17,791G)
AllInBot: 200G (1.7%, 200G)
datadrivenbot: 200G (1.7%, 46,146G)
PoroTact: 100G (0.8%, 6,383G)
DouglasDragonThePoet: 100G (0.8%, 2,591G)

purple bets:
pplvee1: 1,500G (35.9%, 7,355G)
maximumcrit: 500G (12.0%, 4,568G)
HaateXIII: 500G (12.0%, 5,013G)
dogsandcatsand: 388G (9.3%, 2,017G)
ShintaroNayaka: 373G (8.9%, 373G)
TasisSai: 200G (4.8%, 9,073G)
HolyDragoonXIV: 200G (4.8%, 382G)
jeffersonsa: 200G (4.8%, 425G)
getthemoneyz: 118G (2.8%, 1,443,952G)
holdenmagronik: 100G (2.4%, 1,387G)
FoeSquirrel: 100G (2.4%, 14,334G)

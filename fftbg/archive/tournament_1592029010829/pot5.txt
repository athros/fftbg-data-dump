Final Bets: red - 14 bets for 7,813G (41.9%, x1.38); yellow - 13 bets for 10,816G (58.1%, x0.72)

red bets:
reinoe: 2,000G (25.6%, 15,506G)
Lydian_C: 1,200G (15.4%, 6,312G)
WitchHunterIX: 1,000G (12.8%, 15,931G)
Rislyeu: 700G (9.0%, 43,084G)
BirbBrainsBot: 689G (8.8%, 172,518G)
getthemoneyz: 590G (7.6%, 936,641G)
YaBoy125: 420G (5.4%, 2,378G)
ColetteMSLP: 300G (3.8%, 2,220G)
DustBirdEX: 234G (3.0%, 2,050G)
JRHiga: 200G (2.6%, 1,586G)
Arcblazer23: 180G (2.3%, 180G)
Quadh0nk: 100G (1.3%, 995G)
alithesillybird: 100G (1.3%, 3,860G)
datadrivenbot: 100G (1.3%, 37,725G)

yellow bets:
Mesmaster: 2,000G (18.5%, 121,871G)
kaelsun: 1,832G (16.9%, 1,832G)
HaateXIII: 1,252G (11.6%, 1,252G)
Scotobot: 1,200G (11.1%, 33,937G)
HaychDub: 1,001G (9.3%, 23,936G)
ACSpree: 711G (6.6%, 1,422G)
superdevon1: 616G (5.7%, 2,466G)
AllInBot: 603G (5.6%, 603G)
Dymntd: 500G (4.6%, 35,231G)
Powermhero: 500G (4.6%, 22,068G)
HuffFlex: 300G (2.8%, 2,196G)
gorgewall: 201G (1.9%, 20,468G)
DAC169: 100G (0.9%, 1,379G)

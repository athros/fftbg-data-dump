Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DAC169
Male
Cancer
72
78
Time Mage
Item
Damage Split
Throw Item
Ignore Terrain

Iron Fan

Leather Hat
Linen Robe
Wizard Mantle

Haste, Haste 2, Stop, Immobilize, Float, Quick, Demi, Stabilize Time
Hi-Potion, Hi-Ether, Phoenix Down



Lifebregin
Male
Capricorn
62
63
Knight
Throw
Parry
Magic Defense UP
Move+1

Ice Brand
Platinum Shield
Iron Helmet
Bronze Armor
Rubber Shoes

Armor Break, Shield Break, Weapon Break, Surging Sword
Shuriken



Strumisgod
Female
Taurus
69
55
Time Mage
Punch Art
Counter Tackle
Magic Defense UP
Retreat

Gold Staff

Thief Hat
White Robe
Elf Mantle

Stop, Reflect, Quick, Stabilize Time
Wave Fist, Earth Slash, Purification, Chakra



Seaweed B
Female
Gemini
47
73
Mediator
Throw
Earplug
Dual Wield
Move+3

Papyrus Codex
Battle Folio
Golden Hairpin
Black Costume
Angel Ring

Praise, Negotiate, Refute, Rehabilitate
Axe, Stick

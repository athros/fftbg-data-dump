Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Volgrathemoose
Female
Taurus
70
52
Priest
Battle Skill
Arrow Guard
Equip Shield
Ignore Height

Wizard Staff
Aegis Shield
Triangle Hat
Silk Robe
Bracer

Cure 3, Shell 2, Holy
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break



Tougou
Female
Taurus
70
52
Priest
Battle Skill
Arrow Guard
Equip Shield
Ignore Height

Wizard Staff
Aegis Shield
Triangle Hat
Silk Robe
Bracer

Cure 3, Shell 2, Holy
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break



ThePineappleSalesman
Female
Taurus
70
52
Priest
Battle Skill
Arrow Guard
Equip Shield
Ignore Height

Wizard Staff
Aegis Shield
Triangle Hat
Silk Robe
Bracer

Cure 3, Shell 2, Holy
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break



DavenIII
Female
Taurus
70
52
Priest
Battle Skill
Arrow Guard
Equip Shield
Ignore Height

Wizard Staff
Aegis Shield
Triangle Hat
Silk Robe
Bracer

Cure 3, Shell 2, Holy
Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break

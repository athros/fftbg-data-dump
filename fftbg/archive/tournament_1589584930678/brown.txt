Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BoneMiser
Male
Leo
53
65
Bard
Yin Yang Magic
Counter Flood
Dual Wield
Swim

Lightning Bow

Twist Headband
Linen Cuirass
Spike Shoes

Angel Song, Cheer Song, Battle Song, Space Storage, Hydra Pit
Poison, Spell Absorb, Foxbird, Confusion Song, Sleep



ThePineappleSalesman
Male
Virgo
76
56
Monk
Elemental
Catch
Maintenance
Move+3



Ribbon
Mystic Vest
Jade Armlet

Spin Fist, Pummel, Purification, Chakra, Revive
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



TheFALLofLindsay
Male
Gemini
72
41
Time Mage
Jump
Meatbone Slash
Equip Polearm
Retreat

Cypress Rod

Twist Headband
Wizard Robe
Feather Boots

Haste, Slow 2, Immobilize, Reflect, Quick, Demi, Stabilize Time
Level Jump8, Vertical Jump7



CapnChaos12
Female
Scorpio
54
71
Summoner
Charge
Dragon Spirit
Doublehand
Waterbreathing

Thunder Rod

Feather Hat
Wizard Robe
Defense Ring

Moogle, Ifrit, Leviathan, Zodiac
Charge+1, Charge+4, Charge+5, Charge+20

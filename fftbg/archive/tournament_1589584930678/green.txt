Player: !Green
Team: Green Team
Palettes: Green/White



Oops25
Male
Virgo
70
41
Monk
Jump
Counter Tackle
Equip Armor
Swim



Circlet
Plate Mail
Defense Armlet

Pummel, Purification, Chakra, Revive, Seal Evil
Level Jump2, Vertical Jump7



Old Overholt
Female
Taurus
65
63
Thief
Basic Skill
Speed Save
Equip Gun
Move+2

Blast Gun

Black Hood
Leather Outfit
N-Kai Armlet

Gil Taking, Steal Armor, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Accumulate, Dash, Throw Stone, Tickle, Fury



EnemyController
Male
Scorpio
52
73
Knight
Time Magic
Brave Up
Dual Wield
Move+2

Excalibur
Defender
Platinum Helmet
Bronze Armor
Red Shoes

Magic Break, Power Break, Mind Break, Justice Sword
Slow, Quick, Demi 2, Stabilize Time



Dogsandcatsand
Female
Taurus
63
54
Archer
White Magic
Counter Tackle
Defend
Move+2

Long Bow

Gold Helmet
Power Sleeve
Angel Ring

Charge+2, Charge+3, Charge+5, Charge+20
Cure, Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Esuna

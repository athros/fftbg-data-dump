Player: !Red
Team: Red Team
Palettes: Red/Brown



SomthingMore
Female
Scorpio
38
69
Summoner
Punch Art
Parry
Short Status
Levitate

Dragon Rod

Triangle Hat
White Robe
Small Mantle

Titan, Carbunkle, Bahamut, Leviathan, Salamander
Wave Fist, Secret Fist, Revive, Seal Evil



Dirty Naise
Female
Scorpio
58
56
Thief
Throw
Damage Split
Attack UP
Move+1

Mythril Knife

Leather Hat
Earth Clothes
Angel Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Staff



GimmickRPG
Female
Libra
75
53
Summoner
Basic Skill
MP Restore
Equip Gun
Move+2

Battle Folio

Barette
Black Robe
Defense Armlet

Moogle, Shiva, Ifrit, Titan, Bahamut
Heal, Scream



Sairentozon7
Male
Libra
66
38
Geomancer
Talk Skill
MP Restore
Monster Talk
Teleport

Ancient Sword
Bronze Shield
Headgear
Mythril Vest
N-Kai Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Negotiate, Mimic Daravon, Refute

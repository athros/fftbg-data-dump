Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nekojin
Female
Capricorn
52
80
Knight
Item
Arrow Guard
Secret Hunt
Move-MP Up

Long Sword
Bronze Shield
Genji Helmet
Black Robe
Red Shoes

Weapon Break, Magic Break, Justice Sword, Surging Sword
Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



FoeSquirrel
Female
Capricorn
40
78
Archer
White Magic
Sunken State
Equip Bow
Fly

Snipe Bow
Escutcheon
Leather Hat
Brigandine
Cursed Ring

Charge+1, Charge+3, Charge+4
Cure 2, Cure 4, Protect 2, Wall, Esuna



Amiture
Female
Virgo
78
56
Knight
Draw Out
Damage Split
Beastmaster
Move+3

Sleep Sword
Kaiser Plate
Leather Helmet
Chameleon Robe
Wizard Mantle

Weapon Break, Speed Break, Justice Sword, Dark Sword, Night Sword
Koutetsu, Murasame



Actual JP
Male
Scorpio
63
71
Knight
Steal
Parry
Long Status
Move+2

Ragnarok
Genji Shield
Mythril Helmet
Carabini Mail
Power Wrist

Armor Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Justice Sword
Steal Heart, Steal Helmet, Steal Accessory

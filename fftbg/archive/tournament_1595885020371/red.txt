Player: !Red
Team: Red Team
Palettes: Red/Brown



J2DaBibbles
Female
Scorpio
61
61
Calculator
Black Magic
MP Restore
Defend
Jump+2

Cute Bag

Triangle Hat
Power Sleeve
Magic Ring

Height, 4
Fire, Fire 3, Bolt, Bolt 3, Ice 2, Ice 3, Death



Nizaha
Male
Cancer
38
55
Thief
Jump
Counter
Equip Gun
Move-MP Up

Glacier Gun

Golden Hairpin
Chain Vest
Angel Ring

Steal Helmet, Steal Shield, Steal Status, Arm Aim
Level Jump8, Vertical Jump7



Chuckolator
Female
Libra
49
61
Calculator
Yin Yang Magic
Dragon Spirit
Defend
Ignore Terrain

Battle Folio

Holy Miter
Chameleon Robe
Magic Ring

Height, Prime Number, 5, 4
Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze



Mirapoix
Male
Virgo
51
68
Knight
Jump
Counter
Equip Axe
Move+3

Rainbow Staff
Kaiser Plate
Circlet
Bronze Armor
Cursed Ring

Head Break, Weapon Break, Magic Break, Mind Break, Justice Sword, Dark Sword
Level Jump8, Vertical Jump6

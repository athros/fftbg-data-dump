Player: !Black
Team: Black Team
Palettes: Black/Red



AzzaHizza
Male
Gemini
62
67
Squire
Battle Skill
Meatbone Slash
Defense UP
Levitate

Poison Bow

Feather Hat
Judo Outfit
Defense Armlet

Heal
Head Break, Armor Break, Mind Break, Justice Sword, Dark Sword



Cowboycapo2
Female
Leo
60
62
Samurai
Jump
Counter
Beastmaster
Waterbreathing

Muramasa

Mythril Helmet
Plate Mail
Cursed Ring

Asura, Koutetsu, Murasame, Kikuichimoji
Level Jump2, Vertical Jump8



Lwtest
Male
Capricorn
71
38
Monk
Draw Out
Sunken State
Defend
Move+3



Golden Hairpin
Clothes
Feather Boots

Pummel, Purification, Revive
Bizen Boat, Murasame, Kiyomori, Muramasa



Antipathics
Female
Cancer
42
47
Knight
Black Magic
Hamedo
Sicken
Move-MP Up

Slasher
Venetian Shield
Circlet
Light Robe
Power Wrist

Head Break, Shield Break, Mind Break, Stasis Sword
Fire 4, Bolt, Bolt 3

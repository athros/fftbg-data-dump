Player: !Red
Team: Red Team
Palettes: Red/Brown



Thaetreis
Male
Libra
74
43
Ninja
Battle Skill
Counter Tackle
Defend
Jump+3

Flail
Kunai
Black Hood
Clothes
Genji Gauntlet

Shuriken, Knife
Armor Break, Weapon Break, Power Break, Night Sword



CCRUNNER149UWP
Female
Sagittarius
79
69
Archer
Draw Out
Earplug
Secret Hunt
Retreat

Long Bow

Leather Hat
Rubber Costume
Angel Ring

Charge+1, Charge+3, Charge+4, Charge+7
Murasame, Kiyomori



Dothan89
Female
Capricorn
65
62
Lancer
Talk Skill
Arrow Guard
Magic Defense UP
Move-HP Up

Holy Lance
Diamond Shield
Gold Helmet
Diamond Armor
Power Wrist

Level Jump2, Vertical Jump2
Invitation, Persuade, Praise, Mimic Daravon, Refute, Rehabilitate



Ranmilia
Female
Pisces
43
43
Mediator
Draw Out
Blade Grasp
Defense UP
Fly

Stone Gun

Golden Hairpin
Black Robe
Vanish Mantle

Threaten, Solution, Insult, Negotiate, Mimic Daravon, Rehabilitate
Asura, Koutetsu, Kiyomori

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Gorgewall
Male
Libra
41
60
Knight
Basic Skill
Parry
Defend
Move-MP Up

Defender
Escutcheon
Iron Helmet
Leather Armor
Feather Mantle

Magic Break, Power Break, Dark Sword
Accumulate, Heal, Fury, Wish



Solomongrundy85
Monster
Capricorn
49
61
Behemoth










Upvla
Male
Gemini
54
40
Thief
Battle Skill
Caution
Dual Wield
Ignore Height

Iron Sword
Coral Sword
Leather Hat
Leather Outfit
Battle Boots

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon
Head Break, Shield Break, Mind Break, Justice Sword



Choco Joe
Female
Pisces
52
78
Summoner
Yin Yang Magic
HP Restore
Short Charge
Retreat

Mace of Zeus

Flash Hat
Linen Robe
Feather Boots

Shiva, Ramuh, Golem, Carbunkle, Bahamut, Cyclops
Poison, Spell Absorb, Dispel Magic, Paralyze, Sleep

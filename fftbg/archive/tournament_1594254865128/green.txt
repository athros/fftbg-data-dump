Player: !Green
Team: Green Team
Palettes: Green/White



SkylerBunny
Female
Cancer
53
75
Calculator
White Magic
Auto Potion
Long Status
Move-MP Up

Bestiary

Green Beret
Mystic Vest
Elf Mantle

CT, Prime Number, 5, 4, 3
Cure 3, Raise, Reraise, Protect 2, Esuna, Holy



Roofiepops
Female
Leo
46
57
Geomancer
Jump
Counter Flood
Short Charge
Waterbreathing

Heaven's Cloud
Escutcheon
Leather Hat
Secret Clothes
Elf Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Level Jump2, Vertical Jump8



Lastly
Female
Aquarius
72
72
Knight
Summon Magic
MA Save
Defend
Jump+3

Defender
Platinum Shield
Leather Helmet
Plate Mail
Genji Gauntlet

Head Break, Armor Break, Shield Break, Speed Break
Moogle, Shiva, Carbunkle, Leviathan, Silf, Fairy, Cyclops



Prince Rogers Nelson
Male
Leo
42
57
Archer
Steal
Dragon Spirit
Martial Arts
Move+1


Aegis Shield
Leather Hat
Mythril Vest
Genji Gauntlet

Charge+1, Charge+2, Charge+3, Charge+5
Steal Status, Leg Aim

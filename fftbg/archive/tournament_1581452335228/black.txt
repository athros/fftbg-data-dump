Player: !Black
Team: Black Team
Palettes: Black/Red



Sotehr
Female
Pisces
38
44
Chemist
Elemental
Faith Up
Martial Arts
Fly

Main Gauche

Leather Hat
Adaman Vest
Defense Ring

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Lava Ball



ShinoGuy
Male
Gemini
73
63
Priest
Basic Skill
Meatbone Slash
Maintenance
Move+1

Wizard Staff

Triangle Hat
Wizard Robe
Rubber Shoes

Regen, Protect 2, Wall, Esuna
Cheer Up



Go Pack
Male
Sagittarius
43
64
Archer
Battle Skill
Abandon
Attack UP
Jump+2

Cross Bow
Gold Shield
Triangle Hat
Wizard Outfit
Angel Ring

Charge+1, Charge+2, Charge+4, Charge+7
Shield Break, Speed Break, Surging Sword



SacrificialToast
Male
Aries
52
64
Lancer
Basic Skill
Speed Save
Concentrate
Jump+3

Mythril Spear
Bronze Shield
Iron Helmet
Genji Armor
Defense Armlet

Level Jump2, Vertical Jump4
Accumulate, Dash, Throw Stone, Tickle, Scream

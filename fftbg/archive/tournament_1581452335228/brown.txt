Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Anduda
Monster
Scorpio
78
56
Ultima Demon










SomthingMore
Male
Aries
55
65
Archer
Draw Out
PA Save
Dual Wield
Swim

Ultimus Bow

Black Hood
Mythril Vest
Setiemson

Charge+1, Charge+2, Charge+4, Charge+5, Charge+7
Bizen Boat, Murasame



Maakur
Male
Capricorn
45
72
Thief
Elemental
Dragon Spirit
Equip Gun
Move-MP Up

Orichalcum

Bronze Helmet
Power Sleeve
Small Mantle

Steal Heart, Steal Helmet, Steal Shield, Arm Aim
Pitfall, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



LeonStrifeI
Female
Cancer
64
77
Lancer
Black Magic
Sunken State
Doublehand
Swim

Partisan

Bronze Helmet
Black Robe
Magic Gauntlet

Level Jump2, Vertical Jump8
Fire, Ice 4

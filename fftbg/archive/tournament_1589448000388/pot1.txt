Final Bets: red - 12 bets for 3,490G (28.8%, x2.47); blue - 10 bets for 8,612G (71.2%, x0.41)

red bets:
BirbBrainsBot: 1,000G (28.7%, 50,484G)
getthemoneyz: 842G (24.1%, 674,712G)
Kyune: 370G (10.6%, 370G)
otakutaylor: 274G (7.9%, 274G)
Lolprinze: 204G (5.8%, 204G)
Strifu: 200G (5.7%, 1,251G)
Lawndough: 100G (2.9%, 4,641G)
BlackfyreRoy: 100G (2.9%, 1,102G)
datadrivenbot: 100G (2.9%, 16,371G)
Evewho: 100G (2.9%, 18,796G)
Snaccubus: 100G (2.9%, 551G)
Nickaboom11: 100G (2.9%, 1,144G)

blue bets:
HorusTaurus: 5,000G (58.1%, 28,191G)
Shakarak: 1,000G (11.6%, 30,928G)
ko2q: 915G (10.6%, 915G)
Jeeboheebo: 681G (7.9%, 681G)
Phytik: 316G (3.7%, 316G)
twelfthrootoftwo: 300G (3.5%, 9,890G)
victoriolue: 100G (1.2%, 3,741G)
OneHundredFists: 100G (1.2%, 1,916G)
red_celt: 100G (1.2%, 12,559G)
Arcblazer23: 100G (1.2%, 510G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ItotheCtotheE
Male
Capricorn
62
66
Mime

Mana Shield
Attack UP
Move+3



Leather Hat
White Robe
Magic Gauntlet

Mimic




ThePineappleSalesman
Female
Gemini
48
59
Geomancer
Talk Skill
Faith Up
Long Status
Teleport

Slasher
Diamond Shield
Holy Miter
Silk Robe
Diamond Armlet

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Threaten, Solution, Negotiate, Rehabilitate



Aesr86
Male
Cancer
59
63
Thief
Elemental
HP Restore
Equip Sword
Move+1

Kiyomori

Triangle Hat
Chain Vest
Defense Ring

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Saldarin
Female
Scorpio
47
66
Oracle
Time Magic
Mana Shield
Defend
Lava Walking

Iron Fan

Leather Hat
Silk Robe
Spike Shoes

Blind, Spell Absorb, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep, Petrify
Haste, Slow, Stop, Immobilize, Reflect, Demi

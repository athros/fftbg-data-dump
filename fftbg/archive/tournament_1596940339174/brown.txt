Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Jaritras
Female
Scorpio
73
51
Calculator
Black Magic
Counter
Martial Arts
Move+2

Bestiary

Black Hood
Black Robe
108 Gems

CT, Prime Number, 5, 4
Fire 3, Frog, Death, Flare



SuppleThink
Female
Aries
78
73
Wizard
Draw Out
Speed Save
Attack UP
Swim

Assassin Dagger

Triangle Hat
Wizard Outfit
Leather Mantle

Fire 2, Ice, Ice 2
Koutetsu, Murasame



Chuckolator
Female
Taurus
42
69
Priest
Summon Magic
Blade Grasp
Equip Sword
Move+2

Defender

Triangle Hat
Adaman Vest
Leather Mantle

Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Esuna, Holy
Titan, Leviathan, Salamander, Fairy



NIghtdew14
Female
Aquarius
78
62
Dancer
Basic Skill
Earplug
Equip Polearm
Ignore Height

Iron Fan

Flash Hat
Earth Clothes
Rubber Shoes

Last Dance, Nether Demon
Throw Stone, Tickle, Wish

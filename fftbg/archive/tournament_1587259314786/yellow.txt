Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CapnChaos12
Male
Gemini
58
54
Archer
Punch Art
Critical Quick
Doublehand
Teleport

Romanda Gun

Platinum Helmet
Mystic Vest
Jade Armlet

Charge+4, Charge+7, Charge+10
Spin Fist, Pummel, Earth Slash, Revive



Baron Von Scrub
Male
Gemini
43
41
Mime

Brave Up
Defend
Retreat



Black Hood
Brigandine
Diamond Armlet

Mimic




Laserman1000
Male
Taurus
54
50
Bard
Draw Out
Counter Magic
Equip Axe
Lava Walking

Flail

Green Beret
Clothes
Leather Mantle

Cheer Song, Nameless Song
Asura, Murasame, Muramasa



BuffaloCrunch
Female
Scorpio
55
49
Summoner
Battle Skill
Counter
Doublehand
Swim

Faith Rod

Leather Hat
Earth Clothes
Bracer

Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Silf, Fairy, Lich
Shield Break, Magic Break, Speed Break, Power Break

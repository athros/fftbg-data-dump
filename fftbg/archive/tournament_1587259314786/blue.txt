Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bryan792
Male
Leo
70
45
Ninja
Basic Skill
Parry
Equip Gun
Move+2

Battle Folio
Bestiary
Flash Hat
Adaman Vest
Battle Boots

Shuriken
Dash, Yell, Wish



Catfashions
Female
Scorpio
56
74
Time Mage
Basic Skill
MP Restore
Attack UP
Move+1

Battle Folio

Golden Hairpin
Chameleon Robe
Defense Ring

Haste 2, Float, Stabilize Time
Accumulate, Heal



ForagerCats
Female
Gemini
73
40
Monk
Item
Catch
Concentrate
Levitate



Feather Hat
Mythril Vest
Wizard Mantle

Spin Fist, Pummel, Secret Fist, Chakra, Revive
Potion, Hi-Potion, Ether, Hi-Ether, Holy Water, Remedy, Phoenix Down



Mastershango
Male
Virgo
63
75
Summoner
Sing
Meatbone Slash
Concentrate
Jump+1

Panther Bag

Holy Miter
Linen Robe
Defense Ring

Moogle, Titan, Golem, Carbunkle, Leviathan, Fairy
Angel Song, Cheer Song, Last Song

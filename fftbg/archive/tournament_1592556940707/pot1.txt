Final Bets: red - 10 bets for 4,830G (72.9%, x0.37); blue - 5 bets for 1,793G (27.1%, x2.69)

red bets:
Digitalsocrates: 2,000G (41.4%, 62,568G)
BlackFireUK: 1,000G (20.7%, 30,807G)
CassiePhoenix: 455G (9.4%, 455G)
ThisGuyLovesMath: 423G (8.8%, 423G)
AltimaMantoid: 255G (5.3%, 1,155G)
Zachara: 195G (4.0%, 108,795G)
getthemoneyz: 182G (3.8%, 985,568G)
Lydian_C: 120G (2.5%, 27,210G)
Firesheath: 100G (2.1%, 11,782G)
BStarTV: 100G (2.1%, 9,859G)

blue bets:
prince_rogers_nelson_: 722G (40.3%, 722G)
BirbBrainsBot: 521G (29.1%, 91,955G)
Lythe_Caraker: 250G (13.9%, 109,675G)
reddwind_: 200G (11.2%, 806G)
Evewho: 100G (5.6%, 8,577G)

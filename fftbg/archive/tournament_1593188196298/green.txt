Player: !Green
Team: Green Team
Palettes: Green/White



Shalloween
Male
Cancer
63
45
Bard
Jump
Parry
Equip Shield
Move+3

Long Bow
Diamond Shield
Red Hood
Earth Clothes
Magic Gauntlet

Angel Song, Battle Song, Sky Demon
Level Jump3, Vertical Jump2



SkylerBunny
Female
Sagittarius
78
79
Squire
Draw Out
Auto Potion
Equip Knife
Waterbreathing

Main Gauche
Diamond Shield
Bronze Helmet
Chain Mail
Germinas Boots

Accumulate, Dash, Tickle, Cheer Up, Fury, Wish
Bizen Boat, Heaven's Cloud, Kikuichimoji



Sairentozon7
Female
Cancer
74
74
Chemist
Charge
Mana Shield
Long Status
Waterwalking

Romanda Gun

Twist Headband
Clothes
Angel Ring

Potion, Hi-Potion, Maiden's Kiss, Remedy, Phoenix Down
Charge+1, Charge+2, Charge+3



Actual JP
Male
Capricorn
72
55
Ninja
Talk Skill
Mana Shield
Attack UP
Lava Walking

Sasuke Knife
Cultist Dagger
Flash Hat
Adaman Vest
Sprint Shoes

Shuriken, Bomb, Knife
Death Sentence

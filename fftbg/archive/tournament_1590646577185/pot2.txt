Final Bets: green - 9 bets for 3,262G (25.0%, x3.01); yellow - 11 bets for 9,812G (75.0%, x0.33)

green bets:
BirbBrainsBot: 1,000G (30.7%, 29,769G)
Gelwain: 600G (18.4%, 600G)
itsdigitalbro: 500G (15.3%, 2,442G)
getthemoneyz: 362G (11.1%, 750,721G)
YaBoy125: 300G (9.2%, 3,513G)
brogan316: 200G (6.1%, 5,498G)
CorpusCav: 100G (3.1%, 13,097G)
TinchoT: 100G (3.1%, 1,103G)
rackgaming519: 100G (3.1%, 985G)

yellow bets:
lowlf: 5,790G (59.0%, 5,790G)
Mesmaster: 1,000G (10.2%, 101,291G)
Laserman1000: 715G (7.3%, 29,815G)
BoneMiser: 600G (6.1%, 600G)
kaidykat: 453G (4.6%, 9,072G)
Sungazes: 450G (4.6%, 6,406G)
gorgewall: 401G (4.1%, 21,253G)
Klednar21: 200G (2.0%, 1,102G)
AllInBot: 100G (1.0%, 100G)
datadrivenbot: 100G (1.0%, 28,132G)
aeonicvector: 3G (0.0%, 1,265G)

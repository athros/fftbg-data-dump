Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



HorusTaurus
Male
Virgo
78
57
Summoner
White Magic
Counter Magic
Magic Attack UP
Teleport

Ice Rod

Red Hood
Mystic Vest
Wizard Mantle

Moogle, Shiva, Ifrit, Golem, Salamander
Cure 3, Raise, Raise 2, Reraise, Regen



Grandlanzer
Monster
Sagittarius
69
63
Ultima Demon










DeathTaxesAndAnime
Female
Cancer
62
52
Geomancer
Item
Earplug
Throw Item
Jump+3

Mythril Sword
Ice Shield
Feather Hat
Wizard Outfit
Wizard Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp
Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down



ZZ Yoshi
Male
Virgo
79
66
Lancer
Time Magic
Sunken State
Doublehand
Jump+3

Gungnir

Crystal Helmet
Diamond Armor
Dracula Mantle

Level Jump5, Vertical Jump7
Haste, Slow 2, Immobilize, Reflect, Demi, Stabilize Time

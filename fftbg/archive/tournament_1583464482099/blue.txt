Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Shs
Female
Libra
61
72
Thief
Battle Skill
Parry
Halve MP
Jump+1

Hidden Knife

Green Beret
Earth Clothes
Defense Ring

Steal Shield, Steal Status
Shield Break, Mind Break, Dark Sword



Shalloween
Female
Libra
43
52
Mediator
Yin Yang Magic
Abandon
Dual Wield
Levitate

Romanda Gun
Blast Gun
Red Hood
Chameleon Robe
Magic Ring

Invitation, Praise, Threaten, Preach, Death Sentence, Insult, Rehabilitate
Blind, Pray Faith, Foxbird, Dispel Magic, Sleep



Rikrizen
Female
Serpentarius
44
50
Monk
Elemental
Counter Magic
Beastmaster
Teleport



Feather Hat
Judo Outfit
Angel Ring

Spin Fist, Wave Fist, Purification, Chakra
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard



ValtonZenola
Male
Virgo
44
50
Time Mage
Punch Art
MP Restore
Sicken
Lava Walking

Oak Staff

Green Beret
White Robe
Germinas Boots

Haste, Slow 2, Demi, Demi 2, Stabilize Time
Earth Slash, Secret Fist, Purification, Revive

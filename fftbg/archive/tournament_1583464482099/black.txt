Player: !Black
Team: Black Team
Palettes: Black/Red



Rabbitlogik
Male
Gemini
52
69
Squire
Elemental
Counter Flood
Equip Gun
Levitate

Papyrus Codex

Bronze Helmet
Mythril Vest
Jade Armlet

Accumulate
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Galkife
Male
Scorpio
51
56
Bard
Time Magic
Brave Up
Martial Arts
Move+1



Green Beret
Rubber Costume
Leather Mantle

Magic Song, Space Storage
Haste, Demi, Stabilize Time



YaBoy125
Female
Taurus
61
59
Summoner
Black Magic
PA Save
Short Charge
Ignore Terrain

Dragon Rod

Leather Hat
Wizard Robe
Jade Armlet

Titan, Golem, Carbunkle, Bahamut, Silf
Fire 2, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Empower, Death



Blain Cooper
Male
Libra
47
78
Archer
Item
Arrow Guard
Throw Item
Fly

Gastrafitis
Platinum Shield
Barette
Clothes
Sprint Shoes

Charge+2, Charge+5, Charge+10
Antidote, Echo Grass, Holy Water

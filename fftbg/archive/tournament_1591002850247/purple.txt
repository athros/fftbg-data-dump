Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



AlysSariel
Female
Scorpio
80
49
Wizard
Talk Skill
Counter Tackle
Sicken
Waterbreathing

Dragon Rod

Golden Hairpin
Chameleon Robe
Red Shoes

Fire, Fire 2, Bolt 4, Empower, Frog
Death Sentence, Refute, Rehabilitate



Shakarak
Monster
Aquarius
46
66
Goblin










Wyonearth
Male
Scorpio
56
60
Geomancer
Charge
Regenerator
Maintenance
Move+1

Koutetsu Knife
Bronze Shield
Flash Hat
Chain Vest
Sprint Shoes

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7



Klara
Female
Virgo
55
61
Geomancer
Punch Art
Mana Shield
Magic Defense UP
Fly

Coral Sword
Bronze Shield
Green Beret
Light Robe
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Secret Fist, Purification, Chakra, Revive

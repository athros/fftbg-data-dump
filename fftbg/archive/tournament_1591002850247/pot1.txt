Final Bets: red - 8 bets for 3,087G (43.0%, x1.33); blue - 10 bets for 4,097G (57.0%, x0.75)

red bets:
CorpusCav: 1,070G (34.7%, 44,085G)
gorgewall: 564G (18.3%, 564G)
SkyridgeZero: 500G (16.2%, 20,647G)
gary_corson: 401G (13.0%, 8,036G)
serperemagus: 188G (6.1%, 188G)
foofermoofer: 164G (5.3%, 164G)
AlysSariel: 100G (3.2%, 2,762G)
datadrivenbot: 100G (3.2%, 30,227G)

blue bets:
BirbBrainsBot: 1,000G (24.4%, 128,053G)
YaBoy125: 691G (16.9%, 691G)
getthemoneyz: 554G (13.5%, 786,477G)
Vaxaldan: 500G (12.2%, 2,817G)
HASTERIOUS: 400G (9.8%, 400G)
gogofromtogo: 280G (6.8%, 280G)
byrdturbo: 222G (5.4%, 21,028G)
skillomono: 200G (4.9%, 2,801G)
ar_tactic: 150G (3.7%, 52,857G)
Evewho: 100G (2.4%, 3,259G)

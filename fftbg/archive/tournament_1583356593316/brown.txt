Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Trowaba
Male
Virgo
51
39
Lancer
Punch Art
Critical Quick
Secret Hunt
Levitate

Obelisk
Aegis Shield
Platinum Helmet
Mythril Armor
Reflect Ring

Level Jump2, Vertical Jump7
Secret Fist, Revive



Teknakon
Male
Scorpio
71
49
Geomancer
Black Magic
PA Save
Short Charge
Waterwalking

Giant Axe
Buckler
Green Beret
Wizard Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Fire 2, Fire 4, Bolt 2, Bolt 4, Ice 2, Ice 3, Empower, Flare



Rogueain
Male
Pisces
66
77
Chemist
Talk Skill
Absorb Used MP
Maintenance
Fly

Hydra Bag

Leather Hat
Wizard Outfit
Magic Gauntlet

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Remedy, Phoenix Down
Praise, Threaten, Solution, Refute



BruddahPeetz
Female
Capricorn
59
67
Mime

Abandon
Equip Shield
Ignore Terrain


Aegis Shield
Leather Helmet
Mythril Vest
108 Gems

Mimic


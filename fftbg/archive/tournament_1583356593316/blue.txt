Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Shalloween
Male
Taurus
79
50
Samurai
Black Magic
Damage Split
Magic Attack UP
Move+1

Murasame

Crystal Helmet
Chameleon Robe
Genji Gauntlet

Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Fire 2, Bolt 2, Bolt 4, Ice



Alacor
Male
Pisces
47
75
Squire
Item
Dragon Spirit
Equip Armor
Move+1

Hunting Bow

Green Beret
Wizard Robe
Jade Armlet

Heal, Tickle, Yell, Cheer Up, Scream
Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down



Kithmar796
Female
Pisces
78
65
Mediator
Throw
Meatbone Slash
Short Charge
Ignore Terrain

Glacier Gun

Red Hood
Brigandine
Defense Armlet

Invitation, Preach, Solution, Refute
Bomb, Stick, Dictionary



Madangryfrog
Male
Leo
71
55
Archer
Item
Auto Potion
Defense UP
Jump+1

Poison Bow
Aegis Shield
Genji Helmet
Mythril Vest
N-Kai Armlet

Charge+1, Charge+2, Charge+5, Charge+20
Maiden's Kiss, Soft, Phoenix Down

Final Bets: white - 4 bets for 3,064G (17.6%, x4.70); brown - 15 bets for 14,390G (82.4%, x0.21)

white bets:
joewcarson: 2,000G (65.3%, 20,613G)
TheChainNerd: 500G (16.3%, 6,000G)
killth3kid: 464G (15.1%, 1,569G)
E_Ballard: 100G (3.3%, 8,890G)

brown bets:
reinoe: 5,000G (34.7%, 22,096G)
bruubarg: 2,500G (17.4%, 12,860G)
Bryon_W: 1,000G (6.9%, 5,362G)
Draconis345: 1,000G (6.9%, 70,413G)
BirbBrainsBot: 1,000G (6.9%, 106,485G)
DuraiPapers: 866G (6.0%, 866G)
Nickyfive: 600G (4.2%, 600G)
Cryptopsy70: 568G (3.9%, 16,356G)
getthemoneyz: 426G (3.0%, 989,337G)
gorgewall: 401G (2.8%, 19,245G)
mpghappiness: 345G (2.4%, 5,987G)
pplvee1: 284G (2.0%, 284G)
Grandlanzer: 200G (1.4%, 5,050G)
CosmicTactician: 100G (0.7%, 6,034G)
ericzubat: 100G (0.7%, 541G)

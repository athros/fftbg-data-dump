Player: !White
Team: White Team
Palettes: White/Blue



Cryptopsy70
Male
Libra
58
78
Calculator
Nether Skill
Auto Potion
Equip Shield
Move-MP Up

Battle Folio
Escutcheon
Twist Headband
Plate Mail
Dracula Mantle

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



LDSkinny
Female
Leo
68
66
Oracle
Item
Regenerator
Throw Item
Move+2

Iron Fan

Green Beret
Clothes
Defense Armlet

Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy
Potion, Hi-Potion, Antidote, Holy Water, Phoenix Down



Grininda
Male
Aquarius
55
68
Oracle
Jump
Regenerator
Magic Attack UP
Swim

Gokuu Rod

Holy Miter
Silk Robe
Bracer

Doubt Faith, Confusion Song, Dispel Magic, Petrify
Level Jump8, Vertical Jump6



Josephiroth 143
Female
Sagittarius
49
76
Chemist
Basic Skill
Speed Save
Halve MP
Retreat

Air Knife

Flash Hat
Chain Vest
Salty Rage

Potion, Ether, Hi-Ether, Soft
Heal, Scream

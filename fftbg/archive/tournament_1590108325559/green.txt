Player: !Green
Team: Green Team
Palettes: Green/White



Fenaen
Male
Libra
43
49
Archer
Basic Skill
Catch
Doublehand
Teleport

Bow Gun

Twist Headband
Black Costume
N-Kai Armlet

Charge+7, Charge+10
Dash, Throw Stone, Tickle, Yell, Cheer Up



Galkife
Male
Virgo
60
79
Bard
Punch Art
PA Save
Doublehand
Levitate

Ramia Harp

Ribbon
Mythril Vest
Spike Shoes

Battle Song, Magic Song, Sky Demon
Earth Slash



Theseawolf1
Male
Sagittarius
70
46
Mime

Mana Shield
Long Status
Jump+2



Flash Hat
Power Sleeve
108 Gems

Mimic




Gooseyourself
Female
Gemini
53
78
Time Mage
White Magic
PA Save
Attack UP
Ignore Terrain

Wizard Staff

Barette
Adaman Vest
Small Mantle

Haste 2, Stop, Immobilize, Stabilize Time
Cure 2, Cure 4, Raise, Protect, Wall, Esuna, Holy

Player: !Black
Team: Black Team
Palettes: Black/Red



CapnChaos12
Male
Taurus
40
47
Archer
Talk Skill
Parry
Monster Talk
Levitate

Blaze Gun
Aegis Shield
Black Hood
Adaman Vest
Genji Gauntlet

Charge+3
Praise, Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



Rastanar
Monster
Libra
70
65
Steel Giant










EnemyController
Female
Cancer
55
38
Calculator
Yin Yang Magic
Brave Save
Beastmaster
Jump+3

Thunder Rod

Holy Miter
Wizard Robe
N-Kai Armlet

Height, Prime Number, 5, 4
Blind, Spell Absorb, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Volgrathemoose
Male
Leo
56
49
Archer
Yin Yang Magic
Auto Potion
Doublehand
Waterwalking

Ultimus Bow

Golden Hairpin
Black Costume
Magic Gauntlet

Charge+3, Charge+7
Poison, Spell Absorb, Pray Faith, Silence Song, Foxbird, Paralyze, Sleep

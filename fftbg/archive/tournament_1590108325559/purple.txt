Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ScurvyMitch
Male
Libra
50
64
Squire
Black Magic
Parry
Concentrate
Ignore Terrain

Rune Blade
Mythril Shield
Ribbon
Platinum Armor
Jade Armlet

Dash, Heal, Cheer Up
Fire, Ice, Ice 3, Empower



ThePineappleSalesman
Male
Gemini
64
72
Monk
Charge
Meatbone Slash
Dual Wield
Move+1



Golden Hairpin
Brigandine
Diamond Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification
Charge+2, Charge+3



Bruubarg
Male
Cancer
49
79
Squire
Yin Yang Magic
Brave Save
Short Charge
Jump+2

Battle Axe
Buckler
Cross Helmet
Leather Armor
N-Kai Armlet

Heal, Yell, Wish
Poison, Dispel Magic, Petrify



WunUP
Male
Sagittarius
77
42
Chemist
Charge
Parry
Long Status
Jump+2

Star Bag

Cachusha
Leather Outfit
Reflect Ring

Hi-Potion, Ether, Hi-Ether, Echo Grass, Soft, Phoenix Down
Charge+1, Charge+3, Charge+10

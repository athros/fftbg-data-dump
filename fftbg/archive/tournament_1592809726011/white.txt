Player: !White
Team: White Team
Palettes: White/Blue



Choco Joe
Female
Capricorn
79
68
Priest
Talk Skill
Earplug
Martial Arts
Move-MP Up



Cachusha
Power Sleeve
Spike Shoes

Cure 3, Regen, Protect, Shell, Shell 2, Esuna
Persuade, Solution, Insult, Negotiate, Rehabilitate



Evdoggity
Male
Gemini
46
79
Archer
Summon Magic
Speed Save
Maintenance
Jump+3

Night Killer
Platinum Shield
Black Hood
Mystic Vest
Red Shoes

Charge+5
Ramuh, Ifrit, Titan, Golem, Bahamut, Fairy, Lich, Zodiac



OpHendoslice
Male
Leo
54
42
Ninja
Steal
Critical Quick
Short Status
Jump+3

Spell Edge
Kunai
Leather Hat
Adaman Vest
Bracer

Shuriken, Bomb, Knife, Wand
Steal Weapon, Steal Accessory



Mesmaster
Female
Leo
57
42
Samurai
Charge
MP Restore
Short Status
Waterbreathing

Holy Lance

Gold Helmet
Black Robe
Genji Gauntlet

Kiyomori
Charge+5, Charge+7

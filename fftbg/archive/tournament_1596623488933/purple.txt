Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Worldly Unbrain
Female
Scorpio
64
55
Geomancer
Throw
Sunken State
Short Status
Move+1

Slasher
Escutcheon
Thief Hat
Linen Robe
Bracer

Pitfall, Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bomb, Hammer, Ninja Sword, Dictionary



CT 5 Holy
Male
Taurus
43
71
Lancer
Item
Mana Shield
Defense UP
Move-MP Up

Partisan
Gold Shield
Gold Helmet
Plate Mail
Dracula Mantle

Level Jump4, Vertical Jump4
Potion, X-Potion, Ether, Hi-Ether, Holy Water, Phoenix Down



Lokenwow
Male
Cancer
44
51
Chemist
Talk Skill
Absorb Used MP
Equip Sword
Swim

Muramasa

Golden Hairpin
Black Costume
Dracula Mantle

Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Death Sentence, Negotiate



Rodd
Male
Leo
74
56
Time Mage
Yin Yang Magic
Arrow Guard
Dual Wield
Ignore Terrain

Cypress Rod
Cypress Rod
Red Hood
Wizard Robe
Magic Gauntlet

Slow, Slow 2, Float, Demi 2, Meteor, Galaxy Stop
Blind, Doubt Faith, Foxbird, Dispel Magic, Petrify, Dark Holy

Player: !Black
Team: Black Team
Palettes: Black/Red



Goust18
Female
Cancer
62
68
Archer
Throw
Counter Flood
Doublehand
Move+3

Ice Bow

Ribbon
Black Costume
Battle Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Shuriken, Staff, Dictionary



Aislingeach
Female
Libra
69
62
Wizard
Throw
Catch
Short Charge
Move+2

Air Knife

Golden Hairpin
Chain Vest
Leather Mantle

Fire, Fire 3, Fire 4, Bolt 2, Flare
Bomb, Hammer, Staff



Bulleta
Female
Virgo
63
63
Lancer
Summon Magic
Faith Up
Defend
Retreat

Mythril Spear
Escutcheon
Diamond Helmet
Mythril Armor
Vanish Mantle

Level Jump8, Vertical Jump3
Moogle, Ramuh, Golem, Silf, Fairy



Go2sleepTV
Male
Aries
60
73
Ninja
Charge
Counter Magic
Short Charge
Teleport

Mythril Knife
Air Knife
Leather Hat
Leather Outfit
N-Kai Armlet

Shuriken, Wand
Charge+7, Charge+20

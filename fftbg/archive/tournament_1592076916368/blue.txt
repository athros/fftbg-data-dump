Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BlackFireUK
Female
Leo
68
49
Knight
Dance
Blade Grasp
Secret Hunt
Move+1

Mythril Sword
Mythril Shield
Diamond Helmet
Linen Cuirass
N-Kai Armlet

Armor Break, Shield Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Disillusion, Nameless Dance, Last Dance, Dragon Pit



Maakur
Female
Gemini
72
68
Wizard
Item
Brave Save
Equip Bow
Waterwalking

Long Bow

Black Hood
Judo Outfit
Elf Mantle

Fire, Fire 2, Fire 3, Bolt 2, Ice 2, Flare
Potion, Ether, Eye Drop, Maiden's Kiss, Phoenix Down



Just Here2
Male
Aquarius
53
63
Oracle
Summon Magic
Parry
Dual Wield
Waterbreathing

Musk Rod
Gokuu Rod
Holy Miter
Brigandine
Small Mantle

Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Foxbird
Moogle, Shiva, Titan, Golem, Salamander, Silf



Lythe Caraker
Female
Leo
65
75
Time Mage
Summon Magic
Counter Tackle
Doublehand
Move-HP Up

Gold Staff

Green Beret
Clothes
Battle Boots

Slow 2, Immobilize, Stabilize Time
Moogle, Shiva, Golem, Carbunkle, Silf

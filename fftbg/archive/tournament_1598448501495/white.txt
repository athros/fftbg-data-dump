Player: !White
Team: White Team
Palettes: White/Blue



Drusiform
Male
Aquarius
65
55
Mime

Distribute
Defend
Swim


Flame Shield
Green Beret
Leather Outfit
Defense Ring

Mimic




Joewcarson
Male
Gemini
45
57
Knight
Elemental
Auto Potion
Halve MP
Move+2

Platinum Sword
Venetian Shield
Barbuta
Diamond Armor
Reflect Ring

Armor Break, Weapon Break, Speed Break
Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Bryon W
Male
Pisces
72
45
Knight
Basic Skill
Parry
Dual Wield
Move+2

Giant Axe
Broad Sword
Grand Helmet
Diamond Armor
Red Shoes

Armor Break, Weapon Break, Power Break, Stasis Sword, Dark Sword, Explosion Sword
Accumulate, Heal, Yell, Cheer Up, Wish



Ko2q
Female
Aquarius
55
56
Oracle
Punch Art
Faith Save
Equip Sword
Jump+3

Masamune

Feather Hat
Adaman Vest
Elf Mantle

Blind, Poison, Spell Absorb, Pray Faith, Dispel Magic, Sleep
Pummel, Purification, Revive

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Reddwind
Male
Aquarius
77
48
Knight
Elemental
Meatbone Slash
Attack UP
Move+3

Giant Axe
Kaiser Plate
Mythril Helmet
Black Robe
Small Mantle

Magic Break, Night Sword
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



CT 5 Holy
Male
Aquarius
43
52
Bard
Steal
MP Restore
Equip Sword
Lava Walking

Asura Knife

Black Hood
Carabini Mail
108 Gems

Angel Song, Nameless Song
Steal Helmet, Steal Armor, Steal Weapon



NovaKnight21
Female
Leo
57
75
Thief
Item
Arrow Guard
Equip Polearm
Jump+2

Partisan

Golden Hairpin
Clothes
Dracula Mantle

Steal Armor, Steal Shield
Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down



Dorelia
Female
Scorpio
47
79
Wizard
Yin Yang Magic
Auto Potion
Equip Bow
Move-MP Up

Hunting Bow

Flash Hat
Wizard Robe
Leather Mantle

Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 4, Empower, Frog, Flare
Blind, Life Drain, Dispel Magic

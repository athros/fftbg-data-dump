Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Inez
Female
Aries
64
62
Time Mage
Elemental
Faith Up
Long Status
Move+2

Star Bag

Golden Hairpin
Chain Vest
Jade Armlet

Haste, Slow, Slow 2, Stop
Pitfall, Water Ball, Hell Ivy, Static Shock, Gusty Wind



Zaria
Female
Leo
75
75
Thief
Draw Out
PA Save
Secret Hunt
Lava Walking

Kunai

Flash Hat
Clothes
Spike Shoes

Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Koutetsu, Kikuichimoji



Izzi
Female
Libra
61
43
Mediator
Throw
Caution
Concentrate
Jump+1

Blast Gun

Twist Headband
Mystic Vest
Elf Mantle

Praise, Preach, Death Sentence, Insult, Mimic Daravon, Refute
Shuriken, Bomb



Symon
Male
Cancer
49
43
Bard
Charge
Sunken State
Defend
Teleport

Ramia Harp

Green Beret
Wizard Outfit
108 Gems

Angel Song, Cheer Song, Nameless Song, Last Song, Space Storage
Charge+1, Charge+3, Charge+5, Charge+10

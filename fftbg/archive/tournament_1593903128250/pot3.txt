Final Bets: white - 8 bets for 5,004G (33.5%, x1.99); black - 14 bets for 9,942G (66.5%, x0.50)

white bets:
Baron_von_Scrub: 1,070G (21.4%, 1,070G)
vorap: 1,000G (20.0%, 135,067G)
yeepeekayeahmf: 1,000G (20.0%, 4,447G)
Draconis345: 1,000G (20.0%, 46,471G)
BirbBrainsBot: 595G (11.9%, 26,367G)
Lydian_C: 121G (2.4%, 156,846G)
getthemoneyz: 118G (2.4%, 1,180,509G)
lastly: 100G (2.0%, 36,028G)

black bets:
Breakdown777: 3,718G (37.4%, 7,437G)
DeathTaxesAndAnime: 1,465G (14.7%, 1,465G)
RampagingRobot: 885G (8.9%, 3,385G)
Firesheath: 725G (7.3%, 725G)
cam_ATS: 700G (7.0%, 31,848G)
Laserman1000: 600G (6.0%, 25,100G)
Lanshaft: 460G (4.6%, 6,302G)
ColetteMSLP: 400G (4.0%, 6,775G)
g1nger4le: 250G (2.5%, 4,278G)
DustBirdEX: 234G (2.4%, 3,057G)
gorgewall: 201G (2.0%, 2,152G)
GBZero: 104G (1.0%, 104G)
biffcake01: 100G (1.0%, 2,001G)
AO110893: 100G (1.0%, 3,117G)

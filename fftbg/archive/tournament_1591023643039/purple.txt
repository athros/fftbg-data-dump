Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



J2DaBibbles
Female
Scorpio
60
57
Mediator
Punch Art
Catch
Dual Wield
Swim

Mythril Gun
Blaze Gun
Holy Miter
Judo Outfit
Germinas Boots

Invitation, Persuade, Solution, Insult, Refute
Wave Fist, Earth Slash, Secret Fist, Purification, Revive



O Heyno
Male
Leo
52
60
Ninja
Talk Skill
Mana Shield
Attack UP
Move+3

Flail
Flail
Leather Hat
Adaman Vest
Setiemson

Shuriken, Bomb, Staff
Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Zagorsek
Male
Aquarius
56
43
Ninja
Talk Skill
Counter Flood
Monster Talk
Teleport

Short Edge
Sasuke Knife
Leather Hat
Leather Outfit
Chantage

Bomb, Sword, Hammer
Preach, Negotiate



GrayGhostGaming
Male
Leo
68
72
Knight
Elemental
PA Save
Long Status
Teleport

Defender
Buckler
Genji Helmet
Plate Mail
Small Mantle

Head Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Justice Sword
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

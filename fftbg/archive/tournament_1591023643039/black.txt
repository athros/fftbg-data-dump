Player: !Black
Team: Black Team
Palettes: Black/Red



Skillomono
Male
Gemini
55
54
Mime

Counter Tackle
Doublehand
Waterwalking



Flash Hat
Power Sleeve
Magic Ring

Mimic




Lionhermit
Male
Gemini
52
43
Priest
Math Skill
Critical Quick
Halve MP
Waterwalking

Flame Whip

Red Hood
Silk Robe
Angel Ring

Cure 4, Raise, Regen, Protect, Protect 2, Shell, Esuna
CT, 4, 3



Holyonline
Female
Capricorn
52
41
Summoner
Charge
Parry
Equip Armor
Move-MP Up

Wizard Rod

Platinum Helmet
Secret Clothes
N-Kai Armlet

Moogle, Carbunkle, Silf, Fairy, Cyclops
Charge+4, Charge+20



Error72
Female
Aquarius
54
62
Chemist
Steal
Regenerator
Martial Arts
Fly

Panther Bag

Triangle Hat
Chain Vest
Jade Armlet

Potion, X-Potion, Ether, Elixir, Eye Drop, Soft, Holy Water, Phoenix Down
Steal Accessory, Steal Status, Arm Aim

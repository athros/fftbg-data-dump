Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Valentine009
Male
Virgo
44
42
Samurai
Steal
MP Restore
Doublehand
Ignore Height

Spear

Leather Helmet
White Robe
Battle Boots

Heaven's Cloud, Muramasa
Gil Taking, Steal Shield



Mesmaster
Female
Taurus
75
47
Time Mage
Steal
Counter Flood
Equip Polearm
Move+1

Persia

Leather Hat
Chameleon Robe
Chantage

Haste, Slow, Slow 2, Stop, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Steal Helmet



Volgrathemoose
Female
Leo
46
81
Priest
Steal
Meatbone Slash
Concentrate
Teleport

Wizard Staff

Holy Miter
Adaman Vest
Feather Mantle

Cure 3, Cure 4, Raise, Raise 2, Protect 2, Esuna, Holy
Gil Taking, Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim



Beowulfrulez
Male
Virgo
55
72
Lancer
White Magic
PA Save
Equip Sword
Levitate

Kiyomori
Gold Shield
Diamond Helmet
Genji Armor
Elf Mantle

Level Jump2, Vertical Jump8
Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Protect, Shell, Shell 2, Esuna, Holy

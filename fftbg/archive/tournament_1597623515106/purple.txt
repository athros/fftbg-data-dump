Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DavenIII
Male
Aquarius
70
80
Lancer
Steal
Mana Shield
Beastmaster
Move+3

Obelisk
Ice Shield
Diamond Helmet
Bronze Armor
Defense Armlet

Level Jump4, Vertical Jump7
Steal Shield



Killth3kid
Female
Gemini
53
42
Dancer
Item
Counter
Throw Item
Teleport 2

Persia

Thief Hat
Black Costume
N-Kai Armlet

Wiznaibus, Dragon Pit
X-Potion, Eye Drop, Soft, Remedy, Phoenix Down



StealthModeLocke
Male
Scorpio
75
45
Thief
Elemental
Faith Save
Equip Shield
Waterwalking

Blind Knife
Hero Shield
Feather Hat
Earth Clothes
Dracula Mantle

Steal Shield, Arm Aim, Leg Aim
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



Shalloween
Female
Aries
49
47
Geomancer
Talk Skill
Brave Save
Defense UP
Ignore Height

Slasher
Genji Shield
Red Hood
Black Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Praise, Negotiate, Mimic Daravon

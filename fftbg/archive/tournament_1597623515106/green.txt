Player: !Green
Team: Green Team
Palettes: Green/White



Gorgewall
Male
Pisces
43
50
Monk
Steal
Abandon
Equip Shield
Move+2


Ice Shield
Triangle Hat
Mystic Vest
Feather Boots

Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil
Steal Status, Leg Aim



ThreeMileIsland
Female
Virgo
54
41
Mediator
Jump
PA Save
Equip Sword
Move+3

Chirijiraden

Ribbon
Secret Clothes
Jade Armlet

Persuade, Praise, Death Sentence, Insult, Rehabilitate
Level Jump5, Vertical Jump3



Douchetron
Male
Pisces
39
72
Knight
Steal
Arrow Guard
Equip Sword
Move-HP Up

Heaven's Cloud
Ice Shield
Leather Helmet
Mythril Armor
Jade Armlet

Head Break, Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Surging Sword
Steal Heart, Steal Armor, Steal Shield



MagicBottle
Male
Capricorn
79
60
Ninja
Sing
Distribute
Equip Gun
Jump+1

Battle Folio
Battle Folio
Triangle Hat
Power Sleeve
N-Kai Armlet

Bomb, Staff, Dictionary
Magic Song

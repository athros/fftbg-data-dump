Player: !Red
Team: Red Team
Palettes: Red/Brown



Just Here2
Female
Scorpio
42
43
Monk
Steal
Caution
Defense UP
Move+1

Cute Bag

Twist Headband
Rubber Costume
Red Shoes

Pummel, Earth Slash, Purification, Revive
Steal Helmet, Steal Accessory, Arm Aim



Brokenknight201
Female
Libra
74
51
Thief
Battle Skill
Regenerator
Dual Wield
Jump+1

Platinum Sword
Dagger
Barette
Earth Clothes
Magic Ring

Steal Weapon, Steal Status, Arm Aim
Head Break, Shield Break, Magic Break, Dark Sword, Night Sword



DeathTaxesAndAnime
Female
Gemini
45
68
Mediator
Yin Yang Magic
MA Save
Short Status
Waterbreathing

Bestiary

Green Beret
Chameleon Robe
Magic Gauntlet

Praise, Threaten, Death Sentence, Rehabilitate
Spell Absorb, Pray Faith, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy



Sans From Snowdin
Female
Capricorn
55
47
Wizard
Yin Yang Magic
Sunken State
Short Charge
Ignore Height

Thunder Rod

Red Hood
Adaman Vest
Defense Armlet

Fire, Fire 4, Bolt 3, Ice 2, Ice 4, Frog
Blind, Poison, Spell Absorb, Life Drain

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Anak761
Male
Taurus
69
78
Knight
Yin Yang Magic
Sunken State
Equip Sword
Move+2

Defender
Hero Shield
Bronze Helmet
Genji Armor
Rubber Shoes

Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword, Explosion Sword
Poison, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze



Just Here2
Female
Libra
79
58
Time Mage
Talk Skill
MP Restore
Long Status
Jump+3

Gold Staff

Feather Hat
Silk Robe
Rubber Shoes

Haste, Haste 2, Stop, Immobilize, Float, Quick, Demi 2, Meteor, Galaxy Stop
Invitation, Persuade, Praise, Preach, Death Sentence, Insult, Refute



Lastly
Male
Taurus
71
37
Geomancer
Talk Skill
PA Save
Martial Arts
Levitate


Platinum Shield
Holy Miter
Linen Robe
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball
Death Sentence, Insult, Refute



Vorap
Male
Virgo
64
59
Archer
Jump
Absorb Used MP
Sicken
Ignore Height

Ultimus Bow

Thief Hat
Judo Outfit
Red Shoes

Charge+1, Charge+5, Charge+20
Level Jump8, Vertical Jump4

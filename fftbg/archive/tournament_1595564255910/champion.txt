Player: !zChamp
Team: Champion Team
Palettes: White/Blue



LiqwidB
Female
Aries
70
59
Summoner
Jump
Meatbone Slash
Sicken
Move+3

Battle Folio

Triangle Hat
Silk Robe
Jade Armlet

Moogle, Shiva, Ramuh, Titan, Golem, Leviathan, Silf, Fairy
Level Jump5, Vertical Jump8



Killth3kid
Male
Pisces
82
76
Samurai
Jump
Regenerator
Maintenance
Move-HP Up

Partisan

Barbuta
White Robe
Elf Mantle

Asura, Kiyomori
Level Jump3, Vertical Jump7



Seaweed B
Female
Sagittarius
62
68
Calculator
White Magic
Sunken State
Defense UP
Waterwalking

Gokuu Rod

Flash Hat
Power Sleeve
Small Mantle

CT, Height, Prime Number, 4, 3
Cure 4, Raise, Reraise, Regen, Shell, Wall, Holy



Lythe Caraker
Male
Gemini
56
59
Ninja
Steal
Regenerator
Equip Gun
Jump+2

Bestiary
Bestiary
Flash Hat
Wizard Outfit
Rubber Shoes

Knife
Gil Taking, Steal Heart, Steal Helmet, Steal Status

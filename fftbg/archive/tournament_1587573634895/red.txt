Player: !Red
Team: Red Team
Palettes: Red/Brown



SpookyCookieMonster
Male
Capricorn
59
77
Thief
Yin Yang Magic
Counter Magic
Beastmaster
Move+3

Short Edge

Leather Hat
Black Costume
Jade Armlet

Gil Taking, Steal Armor, Steal Weapon, Arm Aim, Leg Aim
Poison, Pray Faith, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze



Volgrathemoose
Male
Libra
40
71
Geomancer
Battle Skill
Sunken State
Equip Gun
Move+3

Battle Folio
Diamond Shield
Holy Miter
Chameleon Robe
Genji Gauntlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Armor Break, Shield Break, Weapon Break, Mind Break, Night Sword, Surging Sword



Davarian
Female
Cancer
42
62
Thief
Black Magic
Counter Magic
Beastmaster
Retreat

Rune Blade

Ribbon
Clothes
Feather Boots

Steal Heart, Steal Helmet, Arm Aim
Bolt, Ice 2, Ice 3, Empower



B0shii
Male
Taurus
54
53
Squire
Charge
Catch
Beastmaster
Lava Walking

Giant Axe
Genji Shield
Holy Miter
Rubber Costume
Small Mantle

Dash, Heal
Charge+4, Charge+5, Charge+7, Charge+10

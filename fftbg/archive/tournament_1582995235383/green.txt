Player: !Green
Team: Green Team
Palettes: Green/White



Mellichor
Female
Taurus
67
48
Dancer
Time Magic
MP Restore
Dual Wield
Move+3

Ryozan Silk
Cashmere
Golden Hairpin
Silk Robe
Sprint Shoes

Wiznaibus
Haste 2, Float, Demi, Demi 2



Maldoree
Female
Aquarius
41
75
Chemist
Yin Yang Magic
Critical Quick
Halve MP
Jump+3

Cute Bag

Triangle Hat
Earth Clothes
Spike Shoes

Potion, Antidote, Maiden's Kiss, Holy Water, Phoenix Down
Life Drain, Zombie, Silence Song, Foxbird, Petrify, Dark Holy



Matthewmuscles
Female
Gemini
61
54
Summoner
Throw
Absorb Used MP
Equip Armor
Move-MP Up

Healing Staff

Black Hood
Bronze Armor
Dracula Mantle

Moogle, Shiva, Golem, Leviathan, Silf, Lich
Shuriken, Staff



Archelous
Male
Scorpio
80
58
Chemist
Punch Art
Auto Potion
Attack UP
Jump+1

Mythril Gun

Black Hood
Power Sleeve
N-Kai Armlet

Potion, Ether, Antidote, Echo Grass, Phoenix Down
Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil

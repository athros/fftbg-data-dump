Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



OtherBrand
Female
Libra
55
74
Thief
Basic Skill
Counter
Equip Armor
Jump+1

Mythril Sword

Triangle Hat
White Robe
Sprint Shoes

Steal Heart, Steal Helmet, Steal Armor, Arm Aim, Leg Aim
Yell, Cheer Up, Fury, Wish



FrostWalrusMan
Male
Sagittarius
79
43
Wizard
Summon Magic
Absorb Used MP
Equip Armor
Jump+1

Cute Bag

Triangle Hat
Chain Mail
Elf Mantle

Fire, Fire 4, Bolt, Bolt 3, Empower, Flare
Moogle, Ifrit, Salamander, Fairy



Reinoe
Monster
Sagittarius
71
62
Great Malboro










Benticore
Female
Taurus
59
71
Monk
Summon Magic
Absorb Used MP
Equip Armor
Levitate



Genji Helmet
Gold Armor
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Revive
Moogle, Shiva, Ifrit, Golem, Carbunkle, Odin, Silf

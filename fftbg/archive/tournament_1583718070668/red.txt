Player: !Red
Team: Red Team
Palettes: Red/Brown



Liamsulli
Female
Aquarius
57
63
Geomancer
Basic Skill
MP Restore
Secret Hunt
Levitate

Muramasa
Genji Shield
Black Hood
Chameleon Robe
Magic Ring

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Heal, Wish



Lodrak
Female
Sagittarius
56
49
Chemist
Charge
Counter Tackle
Equip Axe
Jump+3

Giant Axe

Golden Hairpin
Mythril Vest
Battle Boots

Hi-Potion, Ether, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down
Charge+4, Charge+7



Denamda
Female
Scorpio
78
68
Archer
Elemental
Abandon
Doublehand
Fly

Bow Gun

Cachusha
Mythril Vest
Spike Shoes

Charge+2, Charge+10
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Cougboi
Female
Sagittarius
53
64
Priest
Draw Out
Meatbone Slash
Martial Arts
Teleport

Morning Star

Ribbon
Black Costume
Salty Rage

Cure 3, Cure 4, Raise, Regen, Wall, Esuna, Holy
Asura, Heaven's Cloud, Kiyomori, Kikuichimoji

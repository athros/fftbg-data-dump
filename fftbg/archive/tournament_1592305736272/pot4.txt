Final Bets: purple - 7 bets for 7,933G (44.6%, x1.24); brown - 12 bets for 9,835G (55.4%, x0.81)

purple bets:
prince_rogers_nelson_: 3,533G (44.5%, 3,533G)
Breakdown777: 1,500G (18.9%, 76,381G)
elelor: 1,000G (12.6%, 8,302G)
BirbBrainsBot: 1,000G (12.6%, 40,756G)
joewcarson: 500G (6.3%, 43,311G)
reddwind_: 300G (3.8%, 2,631G)
lastly: 100G (1.3%, 28,429G)

brown bets:
Digitalsocrates: 3,000G (30.5%, 58,682G)
dogsandcatsand: 1,626G (16.5%, 4,929G)
CosmicTactician: 1,500G (15.3%, 7,609G)
solomongrundy85: 1,000G (10.2%, 8,449G)
Bryon_W: 800G (8.1%, 3,755G)
Laserman1000: 700G (7.1%, 73,000G)
cam_ATS: 500G (5.1%, 19,210G)
gorgewall: 301G (3.1%, 20,694G)
ruebyy: 150G (1.5%, 300G)
DuraiPapers: 108G (1.1%, 108G)
AllInBot: 100G (1.0%, 100G)
IphoneDarkness: 50G (0.5%, 3,741G)

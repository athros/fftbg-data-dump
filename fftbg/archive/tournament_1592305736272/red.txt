Player: !Red
Team: Red Team
Palettes: Red/Brown



ScurvyMitch
Monster
Pisces
76
72
Red Dragon










Humble Fabio
Female
Sagittarius
71
59
Priest
Throw
Meatbone Slash
Magic Defense UP
Move-MP Up

Oak Staff

Black Hood
Rubber Costume
Magic Ring

Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect 2, Wall, Esuna, Magic Barrier
Sword



AltimaMantoid
Male
Aries
58
45
Summoner
Elemental
Arrow Guard
Magic Attack UP
Jump+2

Battle Folio

Black Hood
Black Costume
Leather Mantle

Moogle, Ramuh, Titan, Golem, Carbunkle, Odin, Leviathan, Salamander, Silf
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Jeeboheebo
Female
Virgo
55
60
Summoner
Yin Yang Magic
Parry
Short Status
Move-HP Up

Papyrus Codex

Headgear
Black Costume
Red Shoes

Moogle, Ifrit, Carbunkle, Odin, Leviathan, Salamander, Lich
Spell Absorb, Life Drain, Dispel Magic

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Male
Taurus
73
58
Thief
Throw
MP Restore
Doublehand
Swim

Mythril Sword

Feather Hat
Clothes
Dracula Mantle

Steal Accessory, Arm Aim
Shuriken, Knife



KasugaiRoastedPeas
Male
Aries
64
55
Knight
Talk Skill
Damage Split
Martial Arts
Move-MP Up

Battle Axe
Round Shield
Platinum Helmet
Leather Armor
Power Wrist

Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword, Night Sword, Surging Sword
Invitation, Persuade, Praise, Preach, Solution, Refute



Miku Shikhu
Female
Virgo
41
59
Lancer
Summon Magic
PA Save
Beastmaster
Move+1

Iron Fan
Round Shield
Platinum Helmet
Leather Armor
108 Gems

Level Jump3, Vertical Jump3
Moogle, Ramuh, Ifrit, Salamander



CapnChaos12
Female
Libra
59
75
Ninja
Item
MP Restore
Equip Gun
Jump+1

Papyrus Codex
Bloody Strings
Thief Hat
Power Sleeve
Defense Armlet

Shuriken, Dictionary
Potion, Hi-Potion, Ether, Soft

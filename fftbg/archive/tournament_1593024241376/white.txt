Player: !White
Team: White Team
Palettes: White/Blue



HaateXIII
Female
Cancer
54
56
Summoner
Throw
Sunken State
Short Charge
Jump+1

Healing Staff

Leather Hat
Brigandine
Leather Mantle

Moogle, Ifrit, Carbunkle, Leviathan, Fairy, Zodiac
Shuriken, Bomb, Knife



Dogsandcatsand
Male
Aquarius
71
43
Mime

Counter Tackle
Martial Arts
Lava Walking



Thief Hat
Brigandine
Small Mantle

Mimic




CosmicTactician
Female
Aquarius
45
72
Calculator
Limit
MA Save
Defense UP
Lava Walking

Rod
Crystal Shield
Feather Hat
Crystal Mail
Small Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



TheFALLofLindsay
Male
Leo
43
53
Geomancer
Charge
Hamedo
Magic Attack UP
Lava Walking

Long Sword
Diamond Shield
Golden Hairpin
Clothes
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Charge+2, Charge+7, Charge+10

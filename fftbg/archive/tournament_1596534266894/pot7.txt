Final Bets: green - 8 bets for 7,601G (64.7%, x0.55); brown - 6 bets for 4,148G (35.3%, x1.83)

green bets:
UmaiJam: 3,000G (39.5%, 39,308G)
reinoe: 2,000G (26.3%, 68,734G)
EmmaEnema: 1,000G (13.2%, 5,700G)
BirbBrainsBot: 1,000G (13.2%, 10,891G)
AllInBot: 200G (2.6%, 200G)
datadrivenbot: 200G (2.6%, 47,077G)
gorgewall: 101G (1.3%, 6,274G)
BaronHaynes: 100G (1.3%, 9,500G)

brown bets:
Zachara: 1,400G (33.8%, 138,400G)
ColetteMSLP: 1,000G (24.1%, 38,269G)
maximumcrit: 1,000G (24.1%, 10,318G)
Lanshaft: 552G (13.3%, 14,305G)
DesertWooder: 150G (3.6%, 3,464G)
getthemoneyz: 46G (1.1%, 1,518,116G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Baron Von Scrub
Male
Scorpio
57
59
Samurai
Talk Skill
Counter Flood
Long Status
Waterwalking

Asura Knife

Mythril Helmet
Diamond Armor
Battle Boots

Asura, Koutetsu, Bizen Boat
Invitation, Threaten, Negotiate, Mimic Daravon, Refute



LordMaxoss
Male
Aquarius
49
80
Geomancer
Summon Magic
Catch
Dual Wield
Move-HP Up

Heaven's Cloud
Asura Knife
Golden Hairpin
Chain Vest
Jade Armlet

Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shiva, Odin



CapnChaos12
Female
Taurus
53
66
Summoner
Battle Skill
Counter Magic
Short Charge
Levitate

Sage Staff

Black Hood
Chameleon Robe
Genji Gauntlet

Moogle, Titan, Golem, Carbunkle, Leviathan, Salamander, Lich, Cyclops
Head Break, Magic Break, Speed Break, Night Sword



ALY327
Male
Capricorn
55
53
Monk
Throw
Counter Tackle
Halve MP
Move+1



Black Hood
Mythril Vest
Battle Boots

Wave Fist, Earth Slash, Purification
Bomb, Spear

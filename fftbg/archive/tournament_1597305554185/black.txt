Player: !Black
Team: Black Team
Palettes: Black/Red



Gorgewall
Male
Virgo
67
63
Bard
Time Magic
Faith Save
Defense UP
Lava Walking

Fairy Harp

Golden Hairpin
Reflect Mail
108 Gems

Cheer Song, Last Song, Space Storage, Sky Demon
Haste 2, Stop, Reflect, Demi



NovaKnight21
Male
Capricorn
64
46
Bard
Jump
Counter Flood
Magic Defense UP
Teleport

Fairy Harp

Triangle Hat
Chain Mail
Salty Rage

Cheer Song, Battle Song, Sky Demon
Level Jump4, Vertical Jump3



Seaweed B
Male
Gemini
47
78
Calculator
Yin Yang Magic
Earplug
Equip Gun
Move+2

Mythril Gun

Red Hood
Chameleon Robe
Elf Mantle

Height, 4, 3
Doubt Faith, Zombie, Confusion Song, Dark Holy



Cate
Female
Cancer
54
44
Calculator
White Magic
Counter Magic
Equip Knife
Levitate

Spell Edge

Black Hood
Wizard Robe
N-Kai Armlet

Height, Prime Number, 4, 3
Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Esuna

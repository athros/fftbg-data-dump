Player: !Green
Team: Green Team
Palettes: Green/White



Fattunaking
Male
Cancer
74
54
Monk
Throw
Catch
Defend
Fly



Red Hood
Brigandine
Bracer

Spin Fist, Secret Fist, Purification, Revive
Bomb



Superdevon1
Female
Aquarius
53
73
Calculator
Yin Yang Magic
Speed Save
Equip Bow
Move+1

Ice Bow

Flash Hat
Robe of Lords
Diamond Armlet

CT, Prime Number, 5, 4, 3
Blind, Poison, Pray Faith, Confusion Song, Dispel Magic, Paralyze, Petrify



Sairentozon7
Male
Cancer
49
47
Monk
Jump
Catch
Dual Wield
Retreat



Red Hood
Black Costume
Angel Ring

Pummel, Earth Slash, Purification
Level Jump3, Vertical Jump7



Grininda
Female
Aquarius
55
61
Geomancer
Yin Yang Magic
Hamedo
Long Status
Levitate

Kiyomori
Bronze Shield
Headgear
Light Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind
Blind, Life Drain, Silence Song, Paralyze, Petrify

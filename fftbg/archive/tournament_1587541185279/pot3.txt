Final Bets: white - 12 bets for 15,089G (55.4%, x0.81); black - 15 bets for 12,166G (44.6%, x1.24)

white bets:
sinnyil2: 3,500G (23.2%, 150,495G)
Lydian_C: 2,400G (15.9%, 141,500G)
lijarkh: 2,000G (13.3%, 15,179G)
HaplessOne: 2,000G (13.3%, 125,129G)
R_Raynos: 1,101G (7.3%, 1,101G)
ColetteMSLP: 1,000G (6.6%, 28,827G)
BirbBrainsBot: 977G (6.5%, 108,071G)
NovaKnight21: 736G (4.9%, 736G)
getthemoneyz: 698G (4.6%, 542,498G)
Tommy_Havoc: 477G (3.2%, 477G)
placeholdercats: 100G (0.7%, 1,142G)
pLifer: 100G (0.7%, 1,182G)

black bets:
SWATb1gdog: 5,000G (41.1%, 19,904G)
upvla: 1,234G (10.1%, 8,384G)
Evewho: 1,002G (8.2%, 1,002G)
Hamborn: 1,000G (8.2%, 53,285G)
SiNDiLeX: 722G (5.9%, 722G)
Shakarak: 600G (4.9%, 600G)
DarrenDinosaurs: 500G (4.1%, 6,682G)
SteeleIT: 474G (3.9%, 474G)
Kothn: 420G (3.5%, 2,409G)
ungabunga_bot: 310G (2.5%, 191,082G)
Mikumari12: 300G (2.5%, 638G)
DontBackseatGame: 204G (1.7%, 204G)
gorgewall: 200G (1.6%, 10,473G)
AllInBot: 100G (0.8%, 100G)
datadrivenbot: 100G (0.8%, 285G)

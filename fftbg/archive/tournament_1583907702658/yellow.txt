Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Creggers
Male
Gemini
69
54
Squire
Charge
MP Restore
Sicken
Waterwalking

Dagger
Gold Shield
Bronze Helmet
Brigandine
Feather Boots

Heal, Cheer Up, Wish, Scream
Charge+1, Charge+7, Charge+10



The Pengwin
Female
Capricorn
79
52
Monk
Basic Skill
Speed Save
Attack UP
Ignore Terrain



Barette
Brigandine
Elf Mantle

Pummel, Wave Fist, Purification, Revive, Seal Evil
Accumulate, Dash, Throw Stone, Tickle



Rocl
Male
Aquarius
65
45
Lancer
Elemental
HP Restore
Long Status
Retreat

Holy Lance
Buckler
Bronze Helmet
Carabini Mail
Small Mantle

Level Jump8, Vertical Jump2
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



EunosXX
Male
Scorpio
66
54
Ninja
Black Magic
Counter Magic
Attack UP
Fly

Morning Star
Hidden Knife
Black Hood
Mythril Vest
Magic Gauntlet

Bomb, Knife
Empower, Frog

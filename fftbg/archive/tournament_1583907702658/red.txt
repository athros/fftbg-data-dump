Player: !Red
Team: Red Team
Palettes: Red/Brown



Rnark
Female
Scorpio
68
75
Geomancer
Draw Out
Regenerator
Magic Attack UP
Fly

Giant Axe
Flame Shield
Triangle Hat
Earth Clothes
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



LuxorDoesntFrame
Female
Leo
54
77
Samurai
Basic Skill
Counter
Defend
Move+1

Bizen Boat

Barbuta
Black Robe
Small Mantle

Heaven's Cloud, Muramasa
Dash, Tickle, Yell



Azreyam
Female
Libra
37
58
Mime

Brave Up
Equip Armor
Jump+2



Barbuta
Mythril Armor
Leather Mantle

Mimic




Bulleta
Male
Cancer
63
43
Ninja
Time Magic
Mana Shield
Beastmaster
Waterwalking

Mage Masher
Flail
Feather Hat
Judo Outfit
Feather Boots

Ninja Sword, Axe
Haste 2, Slow 2, Stop, Reflect, Demi, Demi 2, Meteor

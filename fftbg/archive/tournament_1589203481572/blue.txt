Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



GrandmasterFrankerZ
Female
Sagittarius
54
72
Knight
Yin Yang Magic
MP Restore
Equip Polearm
Move-HP Up

Javelin
Diamond Shield
Circlet
Leather Armor
Magic Gauntlet

Head Break, Magic Break, Power Break
Blind, Poison, Pray Faith, Blind Rage, Foxbird, Paralyze



Fenixcrest
Female
Libra
58
54
Wizard
Basic Skill
Counter Tackle
Equip Sword
Waterwalking

Kiyomori

Golden Hairpin
Leather Outfit
Angel Ring

Bolt, Bolt 3, Ice 3, Ice 4
Heal, Wish



Mtueni
Female
Aries
52
78
Chemist
Summon Magic
Counter Tackle
Dual Wield
Move+1

Hydra Bag
Cute Bag
Headgear
Judo Outfit
Red Shoes

Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Phoenix Down
Moogle, Shiva, Carbunkle, Salamander, Fairy, Lich



Who Lio42
Female
Virgo
55
49
Thief
Time Magic
Regenerator
Maintenance
Move+3

Main Gauche

Feather Hat
Power Sleeve
Cursed Ring

Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Arm Aim
Haste, Haste 2, Slow, Stop, Immobilize, Quick

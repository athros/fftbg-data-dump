Final Bets: white - 9 bets for 5,670G (48.7%, x1.05); brown - 3 bets for 5,973G (51.3%, x0.95)

white bets:
evontno: 1,317G (23.2%, 1,317G)
Forkmore: 1,111G (19.6%, 10,953G)
dogsandcatsand: 1,000G (17.6%, 4,784G)
BirbBrainsBot: 1,000G (17.6%, 129,393G)
ShintaroNayaka: 632G (11.1%, 632G)
getthemoneyz: 250G (4.4%, 1,219,368G)
datadrivenbot: 200G (3.5%, 55,775G)
Ixionnyu: 150G (2.6%, 1,523G)
raenrond: 10G (0.2%, 1,099G)

brown bets:
ArashiKurobara: 3,986G (66.7%, 3,986G)
TheChainNerd: 1,937G (32.4%, 19,379G)
hiros13gts: 50G (0.8%, 2,699G)

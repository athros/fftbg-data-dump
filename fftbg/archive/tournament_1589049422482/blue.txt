Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Fenrislfr
Male
Virgo
59
70
Squire
Item
PA Save
Equip Axe
Move+1

Slasher
Ice Shield
Green Beret
Crystal Mail
Dracula Mantle

Accumulate, Dash, Throw Stone, Tickle, Yell, Wish
X-Potion, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



Zeroroute
Female
Pisces
61
52
Mime

Counter Flood
Defense UP
Teleport



Headgear
Secret Clothes
Spike Shoes

Mimic




Metagameface
Male
Aries
59
49
Thief
Talk Skill
Catch
Halve MP
Move+3

Hidden Knife

Green Beret
Brigandine
Angel Ring

Gil Taking, Steal Armor, Steal Status, Arm Aim
Invitation, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



HaateXIII
Female
Cancer
49
62
Mediator
Yin Yang Magic
Damage Split
Equip Polearm
Waterwalking

Spear

Red Hood
Chameleon Robe
Power Wrist

Invitation, Persuade, Praise, Threaten, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Spell Absorb, Silence Song, Blind Rage, Paralyze

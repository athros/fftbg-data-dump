Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ko2q
Male
Libra
42
50
Knight
Throw
Critical Quick
Short Status
Move-HP Up

Defender
Mythril Shield
Diamond Helmet
Chameleon Robe
Leather Mantle

Head Break, Armor Break, Speed Break, Surging Sword
Shuriken, Bomb



NovaKnight21
Female
Leo
76
70
Calculator
Black Magic
Catch
Equip Knife
Ignore Height

Dragon Rod

Twist Headband
Silk Robe
Leather Mantle

CT, Prime Number, 5, 4, 3
Bolt 2, Ice 2



Go2sleepTV
Male
Virgo
48
72
Archer
Basic Skill
Mana Shield
Equip Knife
Ignore Height

Short Edge
Mythril Shield
Gold Helmet
Wizard Outfit
Chantage

Charge+1, Charge+3, Charge+10
Dash, Throw Stone, Ultima



Irus
Male
Taurus
41
66
Geomancer
Jump
HP Restore
Secret Hunt
Lava Walking

Koutetsu Knife
Bronze Shield
Black Hood
Wizard Robe
Dracula Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Level Jump2, Vertical Jump5

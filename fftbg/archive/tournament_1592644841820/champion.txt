Player: !zChamp
Team: Champion Team
Palettes: Green/White



Sairentozon7
Female
Taurus
73
62
Mediator
Yin Yang Magic
Earplug
Dual Wield
Teleport 2

Blast Gun
Blast Gun
Headgear
Chain Vest
Rubber Shoes

Invitation, Praise, Death Sentence, Rehabilitate
Life Drain, Doubt Faith, Dispel Magic



Prince Rogers Nelson
Female
Capricorn
45
62
Oracle
Draw Out
Dragon Spirit
Sicken
Fly

Musk Rod

Black Hood
Mythril Vest
Small Mantle

Spell Absorb, Doubt Faith, Sleep
Bizen Boat, Muramasa, Kikuichimoji



CrownOfHorns
Female
Gemini
73
80
Oracle
Talk Skill
Counter
Sicken
Lava Walking

Madlemgen

Green Beret
Silk Robe
Diamond Armlet

Poison, Spell Absorb, Zombie, Silence Song, Dispel Magic, Sleep, Petrify
Persuade, Mimic Daravon



Firesheath
Male
Libra
77
60
Monk
Black Magic
Counter
Short Charge
Lava Walking



Ribbon
Wizard Outfit
Battle Boots

Spin Fist, Secret Fist, Purification
Fire 2, Fire 3, Bolt 4, Ice, Ice 2

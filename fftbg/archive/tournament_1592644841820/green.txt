Player: !Green
Team: Green Team
Palettes: Green/White



CrownOfHorns
Male
Sagittarius
60
72
Samurai
Throw
Counter Flood
Dual Wield
Retreat

Kikuichimoji
Heaven's Cloud
Genji Helmet
Leather Armor
Defense Armlet

Koutetsu, Murasame
Stick



Thaetreis
Male
Gemini
56
56
Thief
Throw
Counter
Magic Attack UP
Jump+3

Kunai

Red Hood
Chain Vest
Rubber Shoes

Steal Shield, Steal Accessory
Shuriken



TeaTime29
Female
Gemini
70
70
Calculator
Time Magic
Catch
Equip Sword
Move+2

Ragnarok

Triangle Hat
Light Robe
Defense Ring

CT, Prime Number, 4, 3
Haste, Immobilize, Float, Quick



BuffaloCrunch
Male
Sagittarius
69
63
Oracle
Summon Magic
MA Save
Doublehand
Ignore Height

Gokuu Rod

Black Hood
Black Costume
Diamond Armlet

Blind, Spell Absorb, Zombie, Blind Rage, Foxbird, Paralyze
Moogle, Shiva, Ifrit, Lich, Cyclops

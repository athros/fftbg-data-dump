Final Bets: white - 11 bets for 5,642G (58.7%, x0.70); black - 9 bets for 3,976G (41.3%, x1.42)

white bets:
BirbBrainsBot: 1,000G (17.7%, 130,189G)
prince_rogers_nelson_: 1,000G (17.7%, 3,949G)
getthemoneyz: 1,000G (17.7%, 995,017G)
reinoe: 600G (10.6%, 600G)
soren_of_tyto: 525G (9.3%, 525G)
TheGuesty: 500G (8.9%, 64,931G)
CassiePhoenix: 416G (7.4%, 416G)
gorgewall: 301G (5.3%, 16,406G)
E_Ballard: 100G (1.8%, 10,308G)
jojo2k: 100G (1.8%, 100G)
delcake: 100G (1.8%, 100G)

black bets:
VolgraTheMoose: 2,001G (50.3%, 31,287G)
benticore: 436G (11.0%, 436G)
reddwind_: 403G (10.1%, 403G)
CT_5_Holy: 316G (7.9%, 316G)
Mermydon: 300G (7.5%, 2,957G)
Evewho: 200G (5.0%, 14,060G)
Lydian_C: 120G (3.0%, 22,877G)
rednecknazgul: 100G (2.5%, 1,391G)
Axciom: 100G (2.5%, 100G)

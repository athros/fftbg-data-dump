Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Roofiepops
Male
Gemini
76
55
Priest
Elemental
Caution
Long Status
Move-MP Up

Wizard Staff

Twist Headband
Wizard Robe
Vanish Mantle

Cure, Cure 3, Raise, Raise 2, Protect 2
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Zeroroute
Male
Virgo
45
70
Ninja
Talk Skill
Abandon
Concentrate
Move-MP Up

Ninja Edge
Star Bag
Golden Hairpin
Wizard Outfit
108 Gems

Shuriken, Bomb, Sword
Praise, Threaten, Solution, Refute, Rehabilitate



ANFz
Male
Pisces
56
44
Lancer
Steal
Arrow Guard
Magic Defense UP
Waterwalking

Holy Lance
Ice Shield
Platinum Helmet
Maximillian
Magic Ring

Level Jump3, Vertical Jump5
Steal Heart, Steal Status



Byrdturbo
Male
Capricorn
55
51
Bard
Battle Skill
Counter Magic
Doublehand
Move-MP Up

Long Bow

Black Hood
Platinum Armor
Magic Gauntlet

Angel Song
Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break

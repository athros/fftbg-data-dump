Final Bets: green - 36 bets for 26,689G (48.8%, x1.05); white - 20 bets for 28,005G (51.2%, x0.95)

green bets:
Virilikus: 5,000G (18.7%, 150,815G)
Laserman1000: 4,604G (17.3%, 78,804G)
HaplessOne: 2,222G (8.3%, 19,141G)
Lordminsc: 1,158G (4.3%, 2,316G)
killth3kid: 1,000G (3.7%, 44,922G)
lijarkh: 1,000G (3.7%, 14,150G)
Aldrammech: 1,000G (3.7%, 8,301G)
cgMcWhiskers: 1,000G (3.7%, 3,631G)
hydroshade: 1,000G (3.7%, 2,792G)
BirbBrainsBot: 1,000G (3.7%, 141,812G)
MaouDono: 751G (2.8%, 751G)
DeathTaxesAndAnime: 612G (2.3%, 612G)
SarrgeQc: 500G (1.9%, 2,959G)
Estan_AD: 500G (1.9%, 1,890G)
zunath: 500G (1.9%, 10,801G)
getthemoneyz: 496G (1.9%, 607,926G)
Grimmace45: 470G (1.8%, 470G)
Aka_Gilly: 400G (1.5%, 41,647G)
coralreeferz: 350G (1.3%, 1,805G)
YaBoy125: 348G (1.3%, 348G)
Rislyeu: 329G (1.2%, 329G)
jhaller2: 250G (0.9%, 4,839G)
AmewTheFox: 250G (0.9%, 15,296G)
Jaritras: 240G (0.9%, 240G)
ZionMorningstar: 209G (0.8%, 409G)
RunicMagus: 200G (0.7%, 41,103G)
Primeval_33: 200G (0.7%, 2,493G)
roffgar: 200G (0.7%, 4,602G)
twelfthrootoftwo: 200G (0.7%, 2,181G)
Evewho: 100G (0.4%, 9,989G)
FoeSquirrel: 100G (0.4%, 4,249G)
ko2q: 100G (0.4%, 9,644G)
FriendSquirrel: 100G (0.4%, 22,761G)
Chompie: 100G (0.4%, 10,296G)
ass_brains: 100G (0.4%, 1,077G)
datadrivenbot: 100G (0.4%, 11,069G)

white bets:
Shalloween: 20,077G (71.7%, 39,367G)
TheMurkGnome: 1,000G (3.6%, 7,408G)
Lionhermit: 1,000G (3.6%, 44,103G)
jakeduhhsnake: 1,000G (3.6%, 55,360G)
DrAntiSocial: 759G (2.7%, 759G)
Zeroroute: 600G (2.1%, 600G)
b0shii: 547G (2.0%, 547G)
ZephyrTempest: 456G (1.6%, 1,320G)
MagicBottle: 356G (1.3%, 356G)
itsdigitalbro: 341G (1.2%, 341G)
ungabunga_bot: 303G (1.1%, 283,449G)
CoderCalamity: 300G (1.1%, 867G)
emperor_knight: 250G (0.9%, 2,411G)
Jahshine: 244G (0.9%, 244G)
babieapple: 200G (0.7%, 1,163G)
Arlum: 200G (0.7%, 200G)
roofiepops: 122G (0.4%, 11,718G)
Sykper: 100G (0.4%, 3,374G)
PorkitoSh0w: 100G (0.4%, 100G)
tmo50x: 50G (0.2%, 9,137G)

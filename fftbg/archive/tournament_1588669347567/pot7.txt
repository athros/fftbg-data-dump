Final Bets: yellow - 11 bets for 9,793G (62.2%, x0.61); black - 9 bets for 5,946G (37.8%, x1.65)

yellow bets:
Dexsana: 3,650G (37.3%, 3,650G)
sinnyil2: 1,631G (16.7%, 1,631G)
HaplessOne: 1,111G (11.3%, 33,936G)
crimson_hellkite: 1,000G (10.2%, 12,000G)
leakimiko: 701G (7.2%, 2,805G)
bahamutlagooon: 600G (6.1%, 1,034G)
amiture: 500G (5.1%, 5,197G)
MaverickUmbracato: 300G (3.1%, 300G)
volgrathemoose: 100G (1.0%, 5,445G)
Evewho: 100G (1.0%, 6,347G)
datadrivenbot: 100G (1.0%, 13,166G)

black bets:
Mesmaster: 2,000G (33.6%, 64,166G)
ungabunga_bot: 1,000G (16.8%, 338,852G)
BirbBrainsBot: 1,000G (16.8%, 35,492G)
CorpusCav: 700G (11.8%, 29,271G)
getthemoneyz: 554G (9.3%, 620,359G)
Kyune: 212G (3.6%, 212G)
sukotsuto: 200G (3.4%, 1,178G)
itsadud: 180G (3.0%, 180G)
nifboy: 100G (1.7%, 9,828G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lodrak
Monster
Cancer
67
76
Behemoth










KaLam1ty
Monster
Scorpio
55
57
Tiamat










RionDoesThings
Female
Libra
53
53
Chemist
Summon Magic
Brave Up
Dual Wield
Ignore Terrain

Stone Gun
Romanda Gun
Green Beret
Power Sleeve
Magic Gauntlet

Potion, Hi-Potion, Antidote, Maiden's Kiss, Holy Water, Remedy
Fairy



ZZ Yoshi
Female
Scorpio
42
67
Time Mage
Steal
Absorb Used MP
Equip Axe
Teleport

Battle Axe

Flash Hat
Wizard Robe
Spike Shoes

Haste, Slow 2, Stop, Float, Reflect, Demi, Stabilize Time
Steal Armor, Steal Shield, Steal Accessory

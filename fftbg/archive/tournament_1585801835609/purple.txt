Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



I Nod My Head When I Lose
Male
Taurus
68
71
Summoner
Yin Yang Magic
Caution
Defend
Jump+3

Wizard Staff

Triangle Hat
Adaman Vest
Vanish Mantle

Ifrit, Golem, Carbunkle, Leviathan
Doubt Faith, Confusion Song, Dispel Magic, Paralyze



DeathTaxesAndAnime
Female
Scorpio
52
69
Priest
Throw
PA Save
Equip Armor
Move+2

Gold Staff

Mythril Helmet
Reflect Mail
Defense Ring

Cure, Cure 2, Cure 4, Raise, Regen, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Spear



WinnerBit
Monster
Sagittarius
76
64
Wyvern










Pandasforsale
Male
Gemini
62
78
Thief
Punch Art
Arrow Guard
Equip Gun
Move+3

Bloody Strings

Red Hood
Power Sleeve
Defense Ring

Steal Shield, Steal Accessory, Leg Aim
Spin Fist, Earth Slash, Purification, Seal Evil

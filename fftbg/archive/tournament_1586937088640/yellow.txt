Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HASTERIOUS
Male
Pisces
69
66
Time Mage
Sing
Auto Potion
Equip Gun
Move+1

Papyrus Codex

Leather Hat
Linen Robe
Sprint Shoes

Haste 2, Slow, Slow 2, Reflect, Quick, Demi, Demi 2
Magic Song



Primeval 33
Male
Sagittarius
53
57
Bard
White Magic
Absorb Used MP
Equip Axe
Ignore Height

Rainbow Staff

Headgear
Gold Armor
Defense Ring

Nameless Song, Sky Demon, Hydra Pit
Cure, Cure 3, Cure 4, Raise, Regen, Protect 2, Esuna



Lali Lulelo
Male
Aquarius
57
76
Oracle
Basic Skill
Meatbone Slash
Defend
Move+3

Musk Rod

Leather Hat
Silk Robe
Bracer

Blind, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic
Dash, Yell, Cheer Up, Wish, Scream



Zachara
Female
Scorpio
57
47
Knight
Elemental
MP Restore
Equip Knife
Waterwalking

Mage Masher
Platinum Shield
Genji Helmet
Crystal Mail
Vanish Mantle

Speed Break, Mind Break, Night Sword
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

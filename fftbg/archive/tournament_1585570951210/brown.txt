Player: !Brown
Team: Brown Team
Palettes: Brown/Green



McGooferson
Female
Leo
61
49
Mediator
Yin Yang Magic
Hamedo
Magic Attack UP
Jump+1

Stone Gun

Golden Hairpin
White Robe
Cursed Ring

Invitation, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Poison, Silence Song, Blind Rage, Foxbird, Dispel Magic, Dark Holy



Fenixcrest
Female
Capricorn
60
59
Geomancer
Throw
Abandon
Attack UP
Move+3

Battle Axe
Genji Shield
Twist Headband
Black Costume
Battle Boots

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bomb



JumbocactuarX27
Female
Taurus
57
67
Thief
Draw Out
Arrow Guard
Equip Knife
Move+2

Spell Edge

Red Hood
Wizard Outfit
Diamond Armlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Arm Aim
Koutetsu, Kiyomori



Jordache7K
Female
Gemini
70
62
Archer
White Magic
Brave Up
Equip Shield
Ignore Height

Poison Bow
Platinum Shield
Feather Hat
Chain Vest
Bracer

Charge+2
Cure 3, Cure 4, Raise 2, Regen, Protect, Protect 2, Wall, Esuna

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RunicMagus
Female
Cancer
37
54
Oracle
Draw Out
Counter Flood
Magic Attack UP
Jump+3

Madlemgen

Twist Headband
White Robe
Red Shoes

Poison, Spell Absorb, Life Drain, Blind Rage, Confusion Song, Dispel Magic
Asura, Heaven's Cloud, Muramasa



Breakdown777
Male
Capricorn
66
48
Knight
Throw
Auto Potion
Secret Hunt
Teleport

Battle Axe
Round Shield
Leather Helmet
Reflect Mail
N-Kai Armlet

Power Break, Dark Sword, Night Sword
Shuriken, Bomb



MattMan119
Male
Aquarius
66
55
Archer
Time Magic
Speed Save
Equip Armor
Fly

Silver Bow

Gold Helmet
Linen Robe
Power Wrist

Charge+4
Haste 2, Slow, Slow 2, Stop, Quick, Demi, Demi 2, Stabilize Time



Humble Fabio
Female
Sagittarius
73
73
Time Mage
Basic Skill
Absorb Used MP
Equip Knife
Jump+3

Spell Edge

Black Hood
Chameleon Robe
Angel Ring

Haste, Stop, Reflect, Demi 2, Meteor
Throw Stone, Heal, Wish, Ultima

Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Female
Scorpio
76
51
Wizard
Punch Art
Counter
Attack UP
Ignore Height

Poison Rod

Black Hood
Linen Robe
Battle Boots

Fire, Bolt 2, Ice 2, Ice 4, Death
Earth Slash, Revive, Seal Evil



Yure1x
Female
Leo
41
64
Squire
Summon Magic
Mana Shield
Dual Wield
Move-HP Up

Flail
Slasher
Black Hood
Wizard Outfit
Feather Boots

Dash, Throw Stone, Heal, Tickle, Cheer Up, Fury, Wish
Moogle, Shiva, Titan, Golem, Carbunkle, Silf



OneHundredFists
Female
Libra
66
68
Time Mage
Punch Art
HP Restore
Equip Sword
Move+1

Mythril Sword

Triangle Hat
Light Robe
Leather Mantle

Haste, Haste 2, Slow, Stop, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification



DrAntiSocial
Monster
Leo
57
38
Mindflayer








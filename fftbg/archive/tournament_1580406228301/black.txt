Player: !Black
Team: Black Team
Palettes: Black/Red



DLJuggernaut
Female
Cancer
72
48
Oracle
Throw
HP Restore
Equip Axe
Jump+2

Gokuu Rod

Headgear
Wizard Robe
Feather Boots

Blind, Poison, Pray Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Petrify
Axe



Sypheck
Male
Aries
77
79
Priest
Throw
Absorb Used MP
Sicken
Ignore Height

Rainbow Staff

Triangle Hat
Earth Clothes
Elf Mantle

Cure 4, Raise, Protect, Shell, Esuna
Shuriken, Staff



Flowtaro
Female
Gemini
47
54
Chemist
Elemental
Hamedo
Equip Gun
Teleport

Battle Folio

Ribbon
Power Sleeve
Germinas Boots

Potion, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Pitfall, Hell Ivy, Quicksand, Sand Storm, Blizzard, Gusty Wind



Sneegler
Male
Scorpio
65
71
Lancer
Battle Skill
Regenerator
Magic Attack UP
Levitate

Partisan
Aegis Shield
Crystal Helmet
Mythril Armor
108 Gems

Level Jump5, Vertical Jump7
Head Break, Armor Break, Speed Break

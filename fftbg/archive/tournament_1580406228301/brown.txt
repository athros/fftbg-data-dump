Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Catatonc
Female
Aquarius
47
68
Geomancer
Throw
Damage Split
Dual Wield
Retreat

Slasher
Mythril Sword
Leather Hat
Power Sleeve
N-Kai Armlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Shuriken, Knife



Brownbearking90
Female
Leo
60
41
Dancer
Draw Out
MP Restore
Beastmaster
Ignore Terrain

Ryozan Silk

Thief Hat
Brigandine
Battle Boots

Wiznaibus, Slow Dance, Polka Polka, Obsidian Blade, Void Storage
Koutetsu, Murasame, Kiyomori, Muramasa



O0Jaxel0o
Female
Pisces
57
59
Geomancer
Basic Skill
Speed Save
Long Status
Move-MP Up

Battle Axe
Gold Shield
Green Beret
Clothes
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Heal



WalkerNash
Male
Leo
48
78
Monk
Throw
HP Restore
Short Charge
Ignore Height



Headgear
Power Sleeve
Battle Boots

Spin Fist, Earth Slash, Purification, Revive
Shuriken

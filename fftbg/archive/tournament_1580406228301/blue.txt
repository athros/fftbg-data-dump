Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Neetneph
Female
Libra
48
46
Chemist
Battle Skill
Blade Grasp
Defend
Levitate

Hydra Bag

Thief Hat
Mythril Vest
Defense Ring

Potion, Hi-Potion, Hi-Ether, Eye Drop, Holy Water, Phoenix Down
Shield Break, Night Sword



Lydian C
Female
Scorpio
64
79
Ninja
Item
Regenerator
Defense UP
Lava Walking

Hidden Knife
Short Edge
Thief Hat
Leather Outfit
Rubber Shoes

Shuriken
Hi-Potion, X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down



KevvTwo
Male
Aries
46
50
Mediator
Battle Skill
Regenerator
Sicken
Retreat

Bestiary

Red Hood
Adaman Vest
Genji Gauntlet

Persuade, Negotiate, Mimic Daravon, Refute, Rehabilitate
Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword, Night Sword



HughJeffner
Male
Leo
71
46
Ninja
Black Magic
Damage Split
Maintenance
Move-MP Up

Mythril Knife
Orichalcum
Black Hood
Black Costume
Wizard Mantle

Shuriken
Bolt 4, Ice, Ice 3, Ice 4

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Roofiepops
Female
Sagittarius
68
61
Chemist
Dance
Counter
Concentrate
Ignore Height

Mythril Gun

Red Hood
Chain Vest
Bracer

Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Witch Hunt, Last Dance, Nether Demon, Dragon Pit



Lyner87
Female
Leo
64
48
Squire
Elemental
Regenerator
Equip Armor
Retreat

Dagger
Platinum Shield
Golden Hairpin
Wizard Robe
Defense Ring

Accumulate, Throw Stone, Cheer Up, Wish
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



RunicMagus
Female
Gemini
58
44
Calculator
Yin Yang Magic
Abandon
Maintenance
Move+1

Papyrus Codex

Golden Hairpin
Wizard Robe
Germinas Boots

CT, Height, Prime Number, 5
Blind, Life Drain, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze



Polygamic
Female
Aries
58
80
Lancer
Throw
Damage Split
Equip Polearm
Jump+3

Cashmere
Bronze Shield
Iron Helmet
Gold Armor
Small Mantle

Level Jump2, Vertical Jump7
Bomb, Knife

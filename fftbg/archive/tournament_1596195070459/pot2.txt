Final Bets: green - 11 bets for 9,444G (52.4%, x0.91); yellow - 8 bets for 8,583G (47.6%, x1.10)

green bets:
Thyrandaal: 2,222G (23.5%, 88,637G)
KSops: 1,778G (18.8%, 1,778G)
Evewho: 1,209G (12.8%, 1,209G)
BirbBrainsBot: 1,000G (10.6%, 61,185G)
Mesmaster: 1,000G (10.6%, 51,381G)
VolgraTheMoose: 860G (9.1%, 860G)
NIghtdew14: 600G (6.4%, 600G)
EmmaEnema: 375G (4.0%, 375G)
datadrivenbot: 200G (2.1%, 45,149G)
DouglasDragonThePoet: 100G (1.1%, 2,561G)
windsah: 100G (1.1%, 7,108G)

yellow bets:
AllInBot: 2,539G (29.6%, 2,539G)
E_Ballard: 2,342G (27.3%, 2,342G)
ShintaroNayaka: 1,096G (12.8%, 1,096G)
ColetteMSLP: 1,000G (11.7%, 25,323G)
getthemoneyz: 862G (10.0%, 1,453,497G)
douchetron: 444G (5.2%, 444G)
hiros13gts: 200G (2.3%, 2,386G)
CosmicTactician: 100G (1.2%, 27,366G)

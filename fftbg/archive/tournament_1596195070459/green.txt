Player: !Green
Team: Green Team
Palettes: Green/White



DavenIII
Male
Libra
76
66
Thief
Sing
Abandon
Equip Shield
Ignore Terrain

Air Knife
Gold Shield
Holy Miter
Mythril Vest
Cursed Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Angel Song, Cheer Song, Magic Song



Grininda
Male
Aquarius
57
45
Monk
Draw Out
Earplug
Attack UP
Move+3



Green Beret
Mystic Vest
Bracer

Spin Fist, Purification, Revive, Seal Evil
Asura, Bizen Boat, Kiyomori, Kikuichimoji, Chirijiraden



NIghtdew14
Female
Virgo
79
76
Lancer
Time Magic
Caution
Equip Bow
Move+2

Ice Bow

Iron Helmet
Platinum Armor
Sprint Shoes

Level Jump5, Vertical Jump7
Haste, Slow, Immobilize, Float, Demi



Dogsandcatsand
Female
Virgo
60
58
Wizard
Summon Magic
HP Restore
Halve MP
Ignore Height

Air Knife

Feather Hat
Black Robe
Wizard Mantle

Fire, Bolt 4, Ice 4
Moogle, Ramuh, Ifrit, Carbunkle, Odin, Lich

Player: !White
Team: White Team
Palettes: White/Blue



Maldoree
Female
Libra
63
55
Summoner
Draw Out
Arrow Guard
Equip Axe
Waterbreathing

Slasher

Black Hood
Robe of Lords
Rubber Shoes

Golem, Carbunkle, Silf
Bizen Boat, Muramasa



Axelrod
Monster
Leo
49
78
Minotaur










Volgrathemoose
Male
Virgo
54
40
Wizard
Item
Mana Shield
Throw Item
Retreat

Poison Rod

Green Beret
Wizard Outfit
Cursed Ring

Fire, Fire 2, Fire 3, Fire 4, Bolt 4, Ice, Ice 2, Ice 3
Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Chuckolator
Male
Capricorn
61
80
Summoner
Time Magic
Caution
Secret Hunt
Fly

Wizard Rod

Red Hood
Leather Outfit
Dracula Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle
Haste, Haste 2, Demi, Demi 2, Meteor

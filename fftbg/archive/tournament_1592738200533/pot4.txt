Final Bets: purple - 7 bets for 9,003G (53.9%, x0.86); brown - 4 bets for 7,701G (46.1%, x1.17)

purple bets:
roqqqpsi: 6,069G (67.4%, 6,069G)
getthemoneyz: 1,000G (11.1%, 1,019,849G)
gooseyourself: 833G (9.3%, 1,583G)
gorgewall: 401G (4.5%, 17,157G)
ColetteMSLP: 300G (3.3%, 412G)
twelfthrootoftwo: 300G (3.3%, 7,878G)
datadrivenbot: 100G (1.1%, 45,229G)

brown bets:
Lydian_C: 4,200G (54.5%, 193,762G)
VolgraTheMoose: 2,001G (26.0%, 15,281G)
BirbBrainsBot: 1,000G (13.0%, 57,623G)
prince_rogers_nelson_: 500G (6.5%, 16,038G)

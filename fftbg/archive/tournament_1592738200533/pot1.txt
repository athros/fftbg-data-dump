Final Bets: red - 8 bets for 4,158G (31.7%, x2.15); blue - 9 bets for 8,948G (68.3%, x0.46)

red bets:
WalkerNash: 1,692G (40.7%, 33,859G)
BirbBrainsBot: 1,000G (24.1%, 57,471G)
gooseyourself: 822G (19.8%, 822G)
getthemoneyz: 266G (6.4%, 1,019,286G)
Lydian_C: 120G (2.9%, 193,744G)
E_Ballard: 100G (2.4%, 17,612G)
nifboy: 100G (2.4%, 1,216G)
roqqqpsi: 58G (1.4%, 5,857G)

blue bets:
VolgraTheMoose: 4,001G (44.7%, 15,914G)
YaBoy125: 1,124G (12.6%, 37,485G)
Draconis345: 1,000G (11.2%, 69,252G)
edgehead62888: 1,000G (11.2%, 28,123G)
CassiePhoenix: 922G (10.3%, 922G)
gorgewall: 401G (4.5%, 17,012G)
ColetteMSLP: 300G (3.4%, 412G)
jojo2k: 100G (1.1%, 200G)
datadrivenbot: 100G (1.1%, 45,182G)

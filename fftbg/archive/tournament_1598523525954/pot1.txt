Final Bets: red - 6 bets for 3,159G (32.1%, x2.12); blue - 7 bets for 6,684G (67.9%, x0.47)

red bets:
WireLord: 1,262G (39.9%, 1,262G)
BirbBrainsBot: 1,000G (31.7%, 66,742G)
getthemoneyz: 394G (12.5%, 1,763,353G)
skipsandwiches: 372G (11.8%, 2,251G)
gorgewall: 101G (3.2%, 11,164G)
7Cerulean7: 30G (0.9%, 3,569G)

blue bets:
bruubarg: 4,176G (62.5%, 4,176G)
ColetteMSLP: 1,000G (15.0%, 18,209G)
CassiePhoenix: 726G (10.9%, 726G)
TakKerna: 250G (3.7%, 573G)
MemoriesofFinal: 232G (3.5%, 232G)
datadrivenbot: 200G (3.0%, 64,188G)
AllInBot: 100G (1.5%, 100G)

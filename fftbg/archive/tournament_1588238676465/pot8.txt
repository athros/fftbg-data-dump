Final Bets: yellow - 9 bets for 5,870G (28.9%, x2.46); champion - 14 bets for 14,413G (71.1%, x0.41)

yellow bets:
leakimiko: 2,222G (37.9%, 27,783G)
Lydian_C: 1,200G (20.4%, 5,541G)
HaplessOne: 666G (11.3%, 11,811G)
ApplesNP: 538G (9.2%, 538G)
lijarkh: 500G (8.5%, 21,125G)
lobsterlobster: 244G (4.2%, 244G)
ar_tactic: 200G (3.4%, 31,659G)
KasugaiRoastedPeas: 200G (3.4%, 7,153G)
crimson_hellkite: 100G (1.7%, 100G)

champion bets:
HorusTaurus: 3,000G (20.8%, 6,445G)
Mesmaster: 3,000G (20.8%, 55,945G)
Shakarak: 2,000G (13.9%, 15,689G)
FoxtrotNovemberCharlie: 1,365G (9.5%, 1,365G)
BirbBrainsBot: 1,000G (6.9%, 157,411G)
Arkreaver: 1,000G (6.9%, 12,954G)
midori_ribbon: 691G (4.8%, 691G)
reinoe: 500G (3.5%, 23,729G)
Jaritras: 427G (3.0%, 427G)
ccordc: 404G (2.8%, 404G)
getthemoneyz: 338G (2.3%, 610,062G)
ko2q: 320G (2.2%, 320G)
ungabunga_bot: 268G (1.9%, 289,027G)
datadrivenbot: 100G (0.7%, 11,696G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Brojimbo
Male
Libra
74
67
Geomancer
Yin Yang Magic
MA Save
Dual Wield
Ignore Terrain

Ice Brand
Platinum Sword
Holy Miter
Chain Vest
Diamond Armlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Poison, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze



Miku Shikhu
Male
Taurus
43
42
Squire
Item
Brave Save
Throw Item
Jump+3

Mythril Sword
Crystal Shield
Cross Helmet
Chain Mail
Feather Boots

Heal, Tickle, Cheer Up, Ultima
Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down



StealthModeLocke
Monster
Aries
55
54
Taiju










LeepingJJ
Male
Libra
53
46
Squire
Yin Yang Magic
Counter Flood
Equip Axe
Ignore Terrain

Battle Axe
Buckler
Black Hood
Carabini Mail
Small Mantle

Dash, Throw Stone, Heal, Tickle
Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep

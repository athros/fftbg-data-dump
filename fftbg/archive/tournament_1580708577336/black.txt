Player: !Black
Team: Black Team
Palettes: Black/Red



Littywhiterock
Female
Cancer
50
70
Archer
Jump
Critical Quick
Doublehand
Waterbreathing

Ice Bow

Feather Hat
Black Costume
Angel Ring

Charge+7
Level Jump2, Vertical Jump5



SkylerBunny
Male
Taurus
48
53
Ninja
Sing
Brave Up
Equip Gun
Jump+1

Flail
Flail
Feather Hat
Secret Clothes
Magic Ring

Bomb, Knife, Dictionary
Life Song, Magic Song, Nameless Song



FroDog5050
Female
Virgo
74
76
Oracle
Black Magic
Caution
Secret Hunt
Move-MP Up

Cypress Rod

Black Hood
Light Robe
Defense Armlet

Blind, Poison, Spell Absorb, Zombie, Foxbird, Confusion Song, Dispel Magic
Bolt, Ice 2, Flare



Koryiaki
Female
Pisces
71
68
Samurai
Talk Skill
Arrow Guard
Attack UP
Waterbreathing

Dragon Whisker

Iron Helmet
Gold Armor
Chantage

Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji, Masamune
Praise, Solution, Death Sentence, Refute

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Genericco
Monster
Cancer
57
46
Red Panther










Terofyin
Female
Scorpio
73
56
Oracle
Summon Magic
Arrow Guard
Concentrate
Fly

Battle Folio

Green Beret
White Robe
Wizard Mantle

Poison, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic
Moogle, Shiva, Odin, Silf, Fairy



Estan AD
Female
Virgo
37
50
Summoner
Draw Out
Absorb Used MP
Defend
Move+2

Wizard Rod

Flash Hat
Judo Outfit
Jade Armlet

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin, Salamander
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



VirulenceXT
Male
Virgo
43
56
Mediator
Summon Magic
Caution
Sicken
Move+1

Glacier Gun

Golden Hairpin
Chameleon Robe
Spike Shoes

Preach, Death Sentence, Insult, Mimic Daravon, Refute
Moogle, Ramuh, Salamander

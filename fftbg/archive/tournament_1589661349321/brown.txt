Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Run With Stone GUNs
Male
Gemini
78
49
Thief
Summon Magic
Parry
Long Status
Waterwalking

Main Gauche

Black Hood
Mythril Vest
Magic Gauntlet

Steal Armor, Steal Weapon, Steal Accessory
Ifrit, Carbunkle



Aneyus
Female
Sagittarius
40
54
Wizard
Punch Art
Mana Shield
Maintenance
Fly

Dragon Rod

Green Beret
Linen Robe
Small Mantle

Fire, Bolt, Bolt 2, Ice 3
Spin Fist, Chakra, Revive



GladiatorLupe
Female
Taurus
78
58
Ninja
Summon Magic
Distribute
Beastmaster
Ignore Terrain

Mythril Knife
Sasuke Knife
Thief Hat
Earth Clothes
Elf Mantle

Shuriken
Moogle, Golem, Carbunkle, Silf



Zebobz
Male
Leo
39
80
Thief
Summon Magic
Sunken State
Magic Attack UP
Move+3

Diamond Sword

Flash Hat
Chain Vest
Germinas Boots

Gil Taking, Steal Heart, Steal Shield, Steal Status, Arm Aim, Leg Aim
Moogle, Ramuh, Leviathan, Salamander

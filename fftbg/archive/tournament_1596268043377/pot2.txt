Final Bets: green - 13 bets for 11,679G (67.7%, x0.48); yellow - 6 bets for 5,570G (32.3%, x2.10)

green bets:
dtrain332: 3,542G (30.3%, 3,542G)
Chambs12: 3,009G (25.8%, 6,018G)
randgridr: 2,001G (17.1%, 4,002G)
sinnyil2: 974G (8.3%, 1,949G)
helpimabug: 826G (7.1%, 1,652G)
maximumcrit: 300G (2.6%, 3,194G)
gorgewall: 201G (1.7%, 15,490G)
AllInBot: 200G (1.7%, 200G)
datadrivenbot: 200G (1.7%, 43,404G)
Laserman1000: 189G (1.6%, 1,289G)
theyoungkroh: 100G (0.9%, 100G)
Drusiform: 100G (0.9%, 500G)
Azzy_Omega: 37G (0.3%, 377G)

yellow bets:
douchetron: 3,968G (71.2%, 7,782G)
BoneMiser: 1,031G (18.5%, 1,031G)
BirbBrainsBot: 339G (6.1%, 96,851G)
phenggy: 100G (1.8%, 908G)
EmmaEnema: 100G (1.8%, 784G)
getthemoneyz: 32G (0.6%, 1,458,701G)

Player: !Green
Team: Green Team
Palettes: Green/White



Jimdandy420
Female
Capricorn
67
74
Dancer
Charge
Counter Tackle
Concentrate
Waterbreathing

Hydra Bag

Black Hood
Leather Vest
Feather Boots

Void Storage
Charge+10, Charge+20



Mizucrux
Male
Serpentarius
67
60
Knight
Draw Out
Sunken State
Equip Shield
Swim

Broad Sword
Buckler
Gold Helmet
Wizard Robe
Cursed Ring

Shield Break, Magic Break, Speed Break, Power Break, Justice Sword
Asura, Muramasa



Terra 32
Female
Cancer
74
68
Oracle
Black Magic
Counter
Equip Axe
Waterbreathing

Battle Bamboo

Twist Headband
Linen Robe
108 Gems

Blind, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Fire 4, Bolt 4, Ice 2



Windsah
Male
Sagittarius
69
53
Archer
Throw
Counter
Doublehand
Move-HP Up

Long Bow

Headgear
Adaman Vest
Sprint Shoes

Charge+2, Charge+4, Charge+5
Bomb, Sword

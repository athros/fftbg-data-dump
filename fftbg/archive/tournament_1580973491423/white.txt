Player: !White
Team: White Team
Palettes: White/Blue



WhiteDog420
Female
Aquarius
65
65
Calculator
Ghost Skill
Speed Save
Long Status
Fly

Ivory Rod

Cachusha
Wizard Robe
Wizard Mantle

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Keeby1
Male
Taurus
76
57
Ninja
Basic Skill
PA Save
Equip Gun
Swim

Spell Edge
Koga Knife
Black Hood
Power Sleeve
Leather Mantle

Bomb, Sword, Staff, Ninja Sword
Throw Stone, Heal, Tickle, Yell, Wish



Ominnous
Female
Libra
80
75
Wizard
Draw Out
Sunken State
Maintenance
Levitate

Dragon Rod

Holy Miter
Black Robe
Bracer

Fire 4, Ice 2, Ice 4
Asura, Koutetsu, Kiyomori, Kikuichimoji



Grininda
Female
Aquarius
73
73
Ninja
Dance
Absorb Used MP
Equip Gun
Lava Walking

Zorlin Shape
Orichalcum
Leather Hat
Leather Outfit
Bracer

Knife, Hammer, Stick
Last Dance, Void Storage

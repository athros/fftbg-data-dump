Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Male
Aries
50
51
Geomancer
White Magic
HP Restore
Equip Sword
Waterwalking

Defender

Thief Hat
Silk Robe
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect, Protect 2, Shell, Esuna



Laserman1000
Male
Gemini
54
80
Mediator
Elemental
Earplug
Short Charge
Move+1

Battle Folio

Red Hood
Black Robe
108 Gems

Invitation, Preach, Insult, Refute
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



SkylerBunny
Female
Pisces
66
54
Time Mage
Draw Out
Regenerator
Equip Armor
Ignore Height

White Staff

Crystal Helmet
Chain Vest
Battle Boots

Haste, Float, Demi 2, Stabilize Time, Meteor
Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji



MagicBottle
Female
Aquarius
67
73
Mime

Meatbone Slash
Sicken
Jump+3



Ribbon
Clothes
Angel Ring

Mimic


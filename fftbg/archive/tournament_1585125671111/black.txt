Player: !Black
Team: Black Team
Palettes: Black/Red



MillionGhosts
Female
Libra
70
75
Monk
Draw Out
Mana Shield
Equip Knife
Jump+1

Blind Knife

Golden Hairpin
Adaman Vest
Sprint Shoes

Purification, Chakra
Koutetsu, Muramasa



Djorama
Male
Aries
51
66
Geomancer
Talk Skill
Counter Flood
Monster Talk
Jump+1

Slasher
Crystal Shield
Black Hood
Earth Clothes
Feather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Invitation, Persuade, Praise, Insult, Mimic Daravon, Rehabilitate



RongRongArts
Male
Leo
72
56
Chemist
Draw Out
Caution
Equip Sword
Move+3

Defender

Triangle Hat
Adaman Vest
Defense Armlet

Potion, Ether, Antidote, Eye Drop, Phoenix Down
Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



BenYuPoker
Female
Cancer
73
53
Dancer
Summon Magic
Parry
Dual Wield
Waterwalking

Persia
Ryozan Silk
Feather Hat
Silk Robe
Power Wrist

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Void Storage, Nether Demon
Ramuh, Ifrit, Titan, Odin, Salamander, Lich

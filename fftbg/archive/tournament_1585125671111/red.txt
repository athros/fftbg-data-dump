Player: !Red
Team: Red Team
Palettes: Red/Brown



Kingchadking
Male
Aries
77
70
Calculator
Black Magic
Mana Shield
Doublehand
Ignore Height

Gokuu Rod

Red Hood
Judo Outfit
Setiemson

CT, Height, 5, 4, 3
Fire 2, Fire 4



Grininda
Female
Leo
61
57
Archer
Battle Skill
Dragon Spirit
Doublehand
Move-HP Up

Bow Gun

Leather Hat
Rubber Costume
Feather Boots

Charge+1, Charge+4, Charge+5, Charge+20
Head Break, Speed Break, Power Break, Justice Sword



Alc Trinity
Female
Aries
77
69
Oracle
White Magic
Counter
Martial Arts
Move+1



Holy Miter
White Robe
Rubber Shoes

Blind, Life Drain, Dispel Magic, Paralyze, Sleep
Cure 4, Raise, Protect 2, Esuna



Thejoden
Male
Sagittarius
39
43
Knight
Steal
PA Save
Doublehand
Levitate

Excalibur

Circlet
Linen Cuirass
Defense Armlet

Armor Break, Speed Break, Mind Break, Dark Sword
Steal Helmet, Steal Armor, Arm Aim

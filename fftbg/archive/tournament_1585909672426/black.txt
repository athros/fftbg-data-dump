Player: !Black
Team: Black Team
Palettes: Black/Red



WireLord
Monster
Cancer
45
59
Apanda










Hzor
Male
Cancer
78
73
Ninja
White Magic
MP Restore
Equip Gun
Move-HP Up

Blast Gun
Glacier Gun
Red Hood
Chain Vest
Setiemson

Hammer
Cure 4, Raise, Reraise, Regen, Holy



Maeveen
Male
Capricorn
46
42
Monk
Charge
Distribute
Defend
Retreat



Green Beret
Power Sleeve
Leather Mantle

Secret Fist, Purification, Revive, Seal Evil
Charge+1, Charge+7



HASTERIOUS
Male
Leo
51
77
Ninja
Battle Skill
Absorb Used MP
Equip Axe
Teleport

Flame Whip
Battle Axe
Flash Hat
Secret Clothes
Small Mantle

Shuriken, Bomb, Staff, Stick
Shield Break, Weapon Break, Magic Break, Power Break, Mind Break

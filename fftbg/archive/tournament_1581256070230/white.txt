Player: !White
Team: White Team
Palettes: White/Blue



TsukikageRyu
Female
Taurus
75
50
Wizard
Yin Yang Magic
Dragon Spirit
Short Charge
Jump+2

Mage Masher

Black Hood
Wizard Robe
Bracer

Fire 4, Ice, Ice 2, Ice 3, Ice 4
Blind, Spell Absorb, Zombie, Foxbird, Confusion Song, Dispel Magic



Katsupdog
Female
Pisces
44
58
Mediator
Time Magic
Dragon Spirit
Secret Hunt
Jump+3

Glacier Gun

Leather Hat
Judo Outfit
Angel Ring

Invitation, Preach, Insult, Mimic Daravon
Haste, Demi 2, Stabilize Time, Meteor



Chocolatetorpedo
Male
Leo
53
65
Ninja
Basic Skill
Counter Tackle
Short Charge
Teleport

Flame Whip
Mage Masher
Black Hood
Black Costume
Diamond Armlet

Shuriken, Bomb, Knife
Accumulate, Tickle



Toropocchan
Male
Leo
51
40
Lancer
Summon Magic
Absorb Used MP
Maintenance
Jump+2

Dragon Whisker
Round Shield
Cross Helmet
Bronze Armor
Defense Armlet

Level Jump8, Vertical Jump6
Moogle, Shiva, Carbunkle, Bahamut, Lich

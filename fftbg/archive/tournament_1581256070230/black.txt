Player: !Black
Team: Black Team
Palettes: Black/Red



HaplessOne
Female
Pisces
80
76
Monk
Draw Out
Sunken State
Long Status
Jump+3



Feather Hat
Chain Vest
Battle Boots

Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Bizen Boat, Murasame, Kiyomori



MetalZebra
Male
Capricorn
61
43
Mediator
Item
Hamedo
Throw Item
Levitate

Blind Knife

Thief Hat
Wizard Outfit
108 Gems

Persuade, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate
X-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Darikis
Male
Pisces
62
45
Thief
Punch Art
Regenerator
Secret Hunt
Waterbreathing

Assassin Dagger

Twist Headband
Power Sleeve
Reflect Ring

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Arm Aim
Wave Fist, Purification, Revive



ZiGaming
Male
Scorpio
55
79
Monk
Item
Mana Shield
Doublehand
Ignore Terrain



Flash Hat
Adaman Vest
108 Gems

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down

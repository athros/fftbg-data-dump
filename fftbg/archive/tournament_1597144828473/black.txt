Player: !Black
Team: Black Team
Palettes: Black/Red



ForagerCats
Female
Gemini
65
54
Oracle
Talk Skill
Parry
Monster Talk
Lava Walking

Whale Whisker

Holy Miter
Wizard Robe
Jade Armlet

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic
Persuade, Insult, Mimic Daravon, Refute



NovaKnight21
Female
Leo
59
61
Oracle
Basic Skill
Caution
Equip Polearm
Move+2

Mythril Spear

Black Hood
White Robe
Magic Ring

Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Accumulate, Throw Stone, Fury, Wish



SeniorBunk
Male
Aquarius
74
68
Summoner
Jump
Dragon Spirit
Attack UP
Retreat

Dragon Rod

Triangle Hat
Chameleon Robe
Magic Ring

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Salamander, Fairy, Lich
Level Jump4, Vertical Jump7



Maddrave09
Male
Cancer
42
55
Knight
Draw Out
MA Save
Magic Attack UP
Teleport

Defender
Flame Shield
Mythril Helmet
Carabini Mail
Rubber Shoes

Head Break, Armor Break, Magic Break, Justice Sword
Asura

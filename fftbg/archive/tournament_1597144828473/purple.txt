Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SephDarkheart
Male
Capricorn
58
80
Priest
Elemental
MA Save
Maintenance
Lava Walking

Flail

Holy Miter
Wizard Robe
Wizard Mantle

Cure, Raise, Reraise, Regen, Shell
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Lava Ball



Sairentozon7
Female
Aquarius
40
68
Summoner
Elemental
MA Save
Short Charge
Waterwalking

Thunder Rod

Flash Hat
Wizard Outfit
Magic Gauntlet

Moogle, Shiva
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Forkmore
Female
Capricorn
72
49
Wizard
Dance
Mana Shield
Long Status
Move+2

Cute Bag

Cachusha
Silk Robe
Feather Mantle

Fire 3, Ice 2, Empower, Frog
Obsidian Blade



ALY327
Male
Taurus
53
56
Monk
Item
Caution
Defend
Move+2



Leather Hat
Adaman Vest
Power Wrist

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Potion, Hi-Potion, Elixir, Echo Grass, Soft, Remedy, Phoenix Down

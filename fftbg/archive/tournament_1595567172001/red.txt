Player: !Red
Team: Red Team
Palettes: Red/Brown



Kronikle
Female
Leo
48
55
Calculator
Yin Yang Magic
Faith Save
Dual Wield
Move-MP Up

Papyrus Codex
Papyrus Codex
Black Hood
Robe of Lords
Power Wrist

CT, Height, Prime Number, 5, 4
Blind, Doubt Faith, Silence Song, Foxbird, Paralyze, Sleep, Petrify



Redmage4evah
Monster
Virgo
44
47
Ochu










Galkife
Male
Cancer
41
71
Summoner
Talk Skill
Caution
Maintenance
Ignore Height

Flame Rod

Leather Hat
Judo Outfit
Rubber Shoes

Moogle, Ramuh, Carbunkle, Salamander
Invitation, Persuade, Negotiate, Refute, Rehabilitate



Hirameki85
Female
Aries
83
76
Squire
Time Magic
MP Restore
Equip Polearm
Jump+3

Partisan
Escutcheon
Green Beret
Mythril Armor
Reflect Ring

Accumulate, Throw Stone, Heal, Yell
Haste, Haste 2, Float, Quick, Demi 2

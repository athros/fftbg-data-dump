Final Bets: black - 7 bets for 6,386G (37.4%, x1.68); purple - 14 bets for 10,704G (62.6%, x0.60)

black bets:
SkylerBunny: 3,500G (54.8%, 61,070G)
killth3kid: 1,444G (22.6%, 1,444G)
Aldrammech: 500G (7.8%, 4,006G)
BlueAbs: 342G (5.4%, 342G)
dem0nj0ns: 300G (4.7%, 4,639G)
AllInBot: 200G (3.1%, 200G)
CT_5_Holy: 100G (1.6%, 7,526G)

purple bets:
roqqqpsi: 2,613G (24.4%, 11,880G)
twelfthrootoftwo: 2,196G (20.5%, 4,307G)
randgridr: 1,803G (16.8%, 3,606G)
BirbBrainsBot: 1,000G (9.3%, 89,780G)
TasisSai: 755G (7.1%, 1,482G)
getthemoneyz: 636G (5.9%, 1,324,512G)
SUGRboi: 348G (3.3%, 348G)
Thyrandaal: 308G (2.9%, 308G)
ZicaX: 244G (2.3%, 1,222G)
gorgewall: 201G (1.9%, 8,396G)
datadrivenbot: 200G (1.9%, 35,359G)
Lemonjohns: 200G (1.9%, 2,851G)
Heroebal: 100G (0.9%, 3,575G)
prince_rogers_nelson_: 100G (0.9%, 1,446G)

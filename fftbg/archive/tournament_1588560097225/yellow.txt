Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zeroroute
Male
Taurus
66
52
Squire
Jump
Arrow Guard
Doublehand
Move+1

Long Sword

Black Hood
Clothes
Defense Ring

Accumulate, Heal, Fury
Level Jump2, Vertical Jump5



Itsadud
Male
Sagittarius
78
71
Thief
Battle Skill
Counter Flood
Magic Attack UP
Levitate

Air Knife

Black Hood
Clothes
Feather Mantle

Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Armor Break, Speed Break



Aneyus
Male
Leo
44
64
Oracle
Summon Magic
Earplug
Equip Bow
Waterwalking

Ice Bow

Flash Hat
Chain Vest
Magic Ring

Life Drain, Pray Faith, Foxbird, Paralyze, Sleep
Golem, Carbunkle, Salamander, Cyclops



Calmcat15
Male
Sagittarius
40
75
Lancer
Battle Skill
Parry
Doublehand
Retreat

Obelisk

Gold Helmet
Light Robe
Magic Gauntlet

Level Jump3, Vertical Jump7
Speed Break, Mind Break

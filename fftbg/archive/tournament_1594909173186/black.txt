Player: !Black
Team: Black Team
Palettes: Black/Red



TasisSai
Monster
Serpentarius
44
48
Dryad










PoroTact
Male
Cancer
72
67
Thief
Talk Skill
Catch
Equip Axe
Move+3

Flail

Headgear
Chain Vest
Rubber Shoes

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Persuade, Praise, Threaten, Preach, Solution, Insult, Negotiate, Refute



Zeroroute
Male
Sagittarius
49
64
Monk
Time Magic
Caution
Defense UP
Fly



Red Hood
Chain Vest
Germinas Boots

Secret Fist, Purification, Chakra
Stabilize Time, Meteor



MagicBottle
Male
Pisces
60
61
Chemist
Black Magic
Absorb Used MP
Concentrate
Jump+3

Blind Knife

Red Hood
Wizard Outfit
Leather Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Remedy
Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Empower

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nifboy
Male
Serpentarius
58
65
Mime

Hamedo
Sicken
Retreat



Feather Hat
Leather Outfit
Feather Boots

Mimic




Hales Bopp It
Male
Libra
44
52
Thief
White Magic
PA Save
Attack UP
Levitate

Blind Knife

Flash Hat
Secret Clothes
Magic Gauntlet

Gil Taking, Steal Accessory, Arm Aim
Protect 2, Esuna



Baron Von Scrub
Female
Scorpio
71
74
Dancer
Yin Yang Magic
MP Restore
Short Status
Move+1

Persia

Holy Miter
Judo Outfit
Small Mantle

Nameless Dance, Obsidian Blade, Dragon Pit
Blind, Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Petrify



Shalloween
Female
Capricorn
65
62
Time Mage
White Magic
Faith Up
Short Charge
Fly

Bestiary

Triangle Hat
Mythril Vest
Feather Mantle

Slow 2, Immobilize, Float, Reflect, Demi 2, Stabilize Time, Meteor
Cure, Cure 4, Raise, Raise 2, Wall, Esuna

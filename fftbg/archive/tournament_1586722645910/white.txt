Player: !White
Team: White Team
Palettes: White/Blue



ShintaroNayaka
Monster
Taurus
60
70
Grenade










Reinoe
Male
Serpentarius
56
68
Mediator
Item
PA Save
Attack UP
Move+2

Stone Gun

Headgear
Earth Clothes
Rubber Shoes

Death Sentence, Refute
Potion, Antidote, Maiden's Kiss, Phoenix Down



Ko2q
Male
Libra
54
53
Knight
Draw Out
Distribute
Concentrate
Move-HP Up

Long Sword
Buckler
Cross Helmet
Gold Armor
Rubber Shoes

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break
Koutetsu, Bizen Boat



Technominari
Female
Libra
60
48
Priest
Punch Art
Counter
Short Status
Levitate

Wizard Staff

Headgear
White Robe
Defense Armlet

Cure, Reraise, Regen, Esuna
Wave Fist, Purification, Revive

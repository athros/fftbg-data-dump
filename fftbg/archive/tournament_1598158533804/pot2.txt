Final Bets: green - 9 bets for 5,252G (32.7%, x2.06); yellow - 13 bets for 10,808G (67.3%, x0.49)

green bets:
lowlf: 1,066G (20.3%, 1,066G)
getthemoneyz: 1,000G (19.0%, 1,717,359G)
BirbBrainsBot: 1,000G (19.0%, 156,337G)
Laserman1000: 610G (11.6%, 13,910G)
Lythe_Caraker: 500G (9.5%, 158,637G)
DLJuggernaut: 500G (9.5%, 12,771G)
corysteam: 276G (5.3%, 276G)
datadrivenbot: 200G (3.8%, 62,622G)
nifboy: 100G (1.9%, 4,105G)

yellow bets:
dogsandcatsand: 2,433G (22.5%, 2,433G)
reinoe: 2,000G (18.5%, 27,331G)
resjudicata3: 1,500G (13.9%, 3,103G)
Thyrandaal: 1,000G (9.3%, 29,345G)
MagicBottle: 832G (7.7%, 832G)
HaateXIII: 792G (7.3%, 792G)
blorpy_: 558G (5.2%, 6,977G)
maximumcrit: 500G (4.6%, 5,057G)
douchetron: 478G (4.4%, 478G)
Snowfats: 280G (2.6%, 280G)
ribbiks: 200G (1.9%, 4,341G)
absalom_20: 135G (1.2%, 135G)
AllInBot: 100G (0.9%, 100G)

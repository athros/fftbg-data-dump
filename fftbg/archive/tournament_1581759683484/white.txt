Player: !White
Team: White Team
Palettes: White/Blue



Sunoccard
Female
Taurus
64
76
Calculator
Lucavi Skill
Distribute
Concentrate
Levitate

Dragon Rod

Golden Hairpin
Chameleon Robe
Dracula Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Gravi 2, Grand Cross, All-Ultima



Mightygerm1
Female
Virgo
43
71
Geomancer
Jump
Regenerator
Maintenance
Levitate

Battle Axe
Escutcheon
Flash Hat
Mythril Vest
Vanish Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Level Jump3, Vertical Jump5



LDHaten
Male
Capricorn
66
49
Archer
Black Magic
Abandon
Concentrate
Move+1

Long Bow

Twist Headband
Judo Outfit
Elf Mantle

Charge+1, Charge+2, Charge+20
Fire, Fire 2, Bolt



Illudia
Female
Scorpio
66
52
Summoner
Black Magic
Meatbone Slash
Defense UP
Waterbreathing

Flame Rod

Golden Hairpin
Chain Vest
Cursed Ring

Moogle, Shiva, Golem, Odin, Fairy
Fire 3, Fire 4, Bolt, Ice, Ice 4, Death

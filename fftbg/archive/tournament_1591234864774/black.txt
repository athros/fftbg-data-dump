Player: !Black
Team: Black Team
Palettes: Black/Red



Solomongrundy85
Male
Aries
75
68
Archer
Battle Skill
Catch
Doublehand
Move+2

Windslash Bow

Iron Helmet
Leather Outfit
Feather Mantle

Charge+3, Charge+4, Charge+5
Weapon Break, Speed Break, Mind Break



Alterworlds
Male
Cancer
54
73
Calculator
Gore Skill
Counter Tackle
Defend
Teleport

Iron Fan
Gold Shield
Cross Helmet
Crystal Mail
Battle Boots

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Firesheath
Female
Libra
65
55
Lancer
Elemental
Faith Save
Short Charge
Waterbreathing

Partisan
Genji Shield
Circlet
Mythril Armor
Wizard Mantle

Level Jump4, Vertical Jump7
Hallowed Ground, Will-O-Wisp, Gusty Wind, Lava Ball



E Ballard
Male
Capricorn
61
48
Knight
Yin Yang Magic
Meatbone Slash
Equip Polearm
Move+1

Partisan
Buckler
Crystal Helmet
Gold Armor
Feather Mantle

Head Break, Weapon Break, Mind Break, Surging Sword
Life Drain, Zombie, Dispel Magic

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



KasugaiRoastedPeas
Male
Leo
70
63
Lancer
Battle Skill
Speed Save
Magic Defense UP
Swim

Holy Lance
Flame Shield
Crystal Helmet
Mythril Armor
Jade Armlet

Level Jump5, Vertical Jump8
Armor Break, Weapon Break, Speed Break, Justice Sword



Red Celt
Male
Capricorn
58
51
Chemist
Talk Skill
PA Save
Sicken
Retreat

Main Gauche

Ribbon
Rubber Costume
Sprint Shoes

Potion, X-Potion, Ether, Hi-Ether, Antidote, Holy Water
Persuade, Threaten, Preach, Refute, Rehabilitate



Herecomedeth
Monster
Scorpio
67
65
Mindflayer










A Kobold Knight
Female
Cancer
74
41
Summoner
Battle Skill
Sunken State
Dual Wield
Move-HP Up

Poison Rod
Gold Staff
Ribbon
Black Costume
Feather Boots

Moogle, Shiva, Leviathan, Lich
Head Break, Shield Break, Speed Break, Stasis Sword, Dark Sword

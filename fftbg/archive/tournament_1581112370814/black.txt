Player: !Black
Team: Black Team
Palettes: Black/Red



Eltaggerung
Male
Leo
53
81
Lancer
Black Magic
Caution
Maintenance
Waterbreathing

Javelin
Ice Shield
Circlet
Bronze Armor
Wizard Mantle

Level Jump8, Vertical Jump2
Bolt, Ice 3, Death



Peebs
Female
Aquarius
57
63
Geomancer
Charge
HP Restore
Long Status
Waterwalking

Giant Axe
Round Shield
Flash Hat
Leather Vest
Red Shoes

Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Lava Ball
Charge+4, Charge+10



Shalloween
Male
Capricorn
63
73
Ninja
Talk Skill
Blade Grasp
Defend
Waterbreathing

Flame Whip
Morning Star
Golden Hairpin
Brigandine
Angel Ring

Shuriken, Bomb, Stick
Negotiate, Refute



Waffle Cake
Male
Serpentarius
74
56
Lancer
Item
MA Save
Doublehand
Swim

Cypress Rod

Gold Helmet
Genji Armor
Defense Ring

Level Jump8, Vertical Jump8
Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop

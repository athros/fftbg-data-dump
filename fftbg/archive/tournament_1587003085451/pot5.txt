Final Bets: red - 21 bets for 7,097G (41.9%, x1.39); green - 16 bets for 9,847G (58.1%, x0.72)

red bets:
upvla: 1,234G (17.4%, 35,132G)
ungabunga_bot: 1,000G (14.1%, 148,425G)
Virilikus: 1,000G (14.1%, 43,414G)
Conome: 500G (7.0%, 6,210G)
JackOnFire1: 465G (6.6%, 1,863G)
itzover9001: 349G (4.9%, 5,557G)
DudeMonkey77: 333G (4.7%, 6,123G)
Skarthe: 300G (4.2%, 2,125G)
killth3kid: 256G (3.6%, 2,435G)
NoNotBees: 250G (3.5%, 9,879G)
DonCardenio: 224G (3.2%, 224G)
loveyouallfriends: 200G (2.8%, 33,948G)
EnemyController: 200G (2.8%, 40,800G)
LazarusAjin: 108G (1.5%, 108G)
ko2q: 100G (1.4%, 18,921G)
CandyTamer: 100G (1.4%, 294G)
Escorian: 100G (1.4%, 4,809G)
Jerboj: 100G (1.4%, 47,984G)
ZephyrTempest: 100G (1.4%, 75,346G)
Nanovolt: 100G (1.4%, 100G)
getthemoneyz: 78G (1.1%, 497,982G)

green bets:
sinnyil2: 2,500G (25.4%, 53,517G)
ThePuss: 1,245G (12.6%, 2,491G)
DejaPoo21: 1,000G (10.2%, 7,601G)
BirbBrainsBot: 1,000G (10.2%, 204,026G)
Shalloween: 1,000G (10.2%, 39,698G)
Adam1949: 999G (10.1%, 3,999G)
Laserman1000: 530G (5.4%, 5,630G)
TheGuesty: 284G (2.9%, 3,284G)
GrandmasterFrankerZ: 250G (2.5%, 250G)
KasugaiRoastedPeas: 200G (2.0%, 4,984G)
Slowbrofist: 200G (2.0%, 4,237G)
astrozin11: 200G (2.0%, 2,363G)
RunicMagus: 150G (1.5%, 1,176G)
madming25: 108G (1.1%, 108G)
ThePanda1016: 100G (1.0%, 4,302G)
datadrivenbot: 81G (0.8%, 136G)

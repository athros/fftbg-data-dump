Final Bets: purple - 12 bets for 6,459G (27.6%, x2.63); brown - 9 bets for 16,969G (72.4%, x0.38)

purple bets:
UmaiJam: 2,000G (31.0%, 17,799G)
BirbBrainsBot: 1,000G (15.5%, 67,488G)
superdevon1: 972G (15.0%, 32,406G)
itsonlyspencer: 880G (13.6%, 880G)
fattunaking: 380G (5.9%, 380G)
getthemoneyz: 342G (5.3%, 1,558,213G)
WhiteTigress: 300G (4.6%, 6,501G)
PantherIscariot: 222G (3.4%, 4,969G)
resjudicata3: 200G (3.1%, 1,231G)
itskage: 100G (1.5%, 1,470G)
Drusiform: 50G (0.8%, 3,416G)
Rook512: 13G (0.2%, 275G)

brown bets:
Galeronis: 11,287G (66.5%, 11,287G)
douchetron: 1,967G (11.6%, 3,858G)
DeathTaxesAndAnime: 1,803G (10.6%, 3,536G)
HaateXIII: 759G (4.5%, 759G)
Aestheta: 437G (2.6%, 437G)
silverChangeling: 216G (1.3%, 216G)
AllInBot: 200G (1.2%, 200G)
ribbiks: 200G (1.2%, 5,387G)
MinBetBot: 100G (0.6%, 17,412G)

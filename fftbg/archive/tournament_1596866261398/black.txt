Player: !Black
Team: Black Team
Palettes: Black/Red



Kronikle
Male
Libra
60
77
Ninja
Black Magic
Damage Split
Attack UP
Waterwalking

Spell Edge
Spell Edge
Headgear
Leather Outfit
Defense Armlet

Shuriken, Staff, Wand
Fire, Bolt 4, Death



NicoSavoy
Male
Capricorn
48
43
Chemist
Talk Skill
Distribute
Equip Sword
Jump+2

Chaos Blade

Golden Hairpin
Earth Clothes
Red Shoes

Potion, X-Potion, Ether, Eye Drop, Soft, Phoenix Down
Threaten, Death Sentence, Refute



Lumberinjack
Male
Cancer
73
45
Thief
White Magic
Counter Tackle
Beastmaster
Jump+1

Coral Sword

Triangle Hat
Mythril Vest
Dracula Mantle

Gil Taking, Steal Armor, Steal Accessory, Steal Status, Arm Aim
Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise



Killth3kid
Male
Cancer
68
62
Monk
Summon Magic
MP Restore
Equip Knife
Levitate

Short Edge

Thief Hat
Secret Clothes
Magic Gauntlet

Pummel, Purification, Chakra, Revive
Shiva, Golem, Leviathan, Salamander

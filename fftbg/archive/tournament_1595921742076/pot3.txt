Final Bets: white - 10 bets for 7,837G (57.4%, x0.74); black - 8 bets for 5,812G (42.6%, x1.35)

white bets:
NicoSavoy: 3,227G (41.2%, 322,774G)
NovaKnight21: 2,000G (25.5%, 5,336G)
BirbBrainsBot: 1,000G (12.8%, 155,934G)
fluffskull: 737G (9.4%, 737G)
gorgewall: 201G (2.6%, 7,112G)
AllInBot: 200G (2.6%, 200G)
cuatro1915: 200G (2.6%, 4,553G)
datadrivenbot: 200G (2.6%, 45,638G)
moonliquor: 50G (0.6%, 26,826G)
getthemoneyz: 22G (0.3%, 1,424,202G)

black bets:
sinnyil2: 1,792G (30.8%, 1,792G)
Chambs12: 1,000G (17.2%, 2,056G)
Firesheath: 1,000G (17.2%, 12,420G)
prince_rogers_nelson_: 620G (10.7%, 620G)
douchetron: 486G (8.4%, 486G)
nekojin: 384G (6.6%, 22,328G)
dtrain332: 308G (5.3%, 308G)
EnemyController: 222G (3.8%, 1,497,151G)

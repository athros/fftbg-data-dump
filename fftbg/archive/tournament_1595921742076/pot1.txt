Final Bets: red - 10 bets for 15,281G (74.2%, x0.35); blue - 7 bets for 5,313G (25.8%, x2.88)

red bets:
NicoSavoy: 9,982G (65.3%, 332,756G)
DamnThatShark: 2,000G (13.1%, 16,024G)
dtrain332: 1,738G (11.4%, 1,738G)
moonliquor: 500G (3.3%, 25,651G)
douchetron: 360G (2.4%, 360G)
AllInBot: 200G (1.3%, 200G)
datadrivenbot: 200G (1.3%, 45,771G)
gorgewall: 101G (0.7%, 7,146G)
Firesheath: 100G (0.7%, 12,630G)
fluffskull: 100G (0.7%, 652G)

blue bets:
CT_5_Holy: 1,501G (28.3%, 1,501G)
EnemyController: 1,111G (20.9%, 1,493,584G)
BirbBrainsBot: 1,000G (18.8%, 152,723G)
prince_rogers_nelson_: 679G (12.8%, 679G)
getthemoneyz: 422G (7.9%, 1,422,869G)
Chambs12: 400G (7.5%, 1,706G)
cuatro1915: 200G (3.8%, 5,978G)

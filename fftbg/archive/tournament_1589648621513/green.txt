Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Female
Libra
60
74
Oracle
Time Magic
Counter
Equip Shield
Jump+2

Bestiary
Bronze Shield
Twist Headband
Chameleon Robe
Angel Ring

Blind, Spell Absorb, Pray Faith, Doubt Faith, Foxbird, Dispel Magic, Petrify
Haste, Haste 2, Float, Quick, Demi, Stabilize Time



CamelCash007
Monster
Gemini
67
50
Bomb










Laserman1000
Male
Gemini
70
69
Mime

Meatbone Slash
Martial Arts
Jump+2



Diamond Helmet
Brigandine
Red Shoes

Mimic




Tsuike
Female
Scorpio
58
75
Priest
Time Magic
Brave Save
Attack UP
Move+2

Gold Staff

Red Hood
Linen Robe
Chantage

Cure 4, Raise, Raise 2, Regen, Protect, Shell, Shell 2, Esuna
Haste 2, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time

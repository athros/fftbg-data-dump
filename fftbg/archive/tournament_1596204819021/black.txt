Player: !Black
Team: Black Team
Palettes: Black/Red



Josephiroth 143
Male
Libra
44
68
Mediator
Charge
Faith Save
Equip Knife
Move+3

Ninja Edge

Black Hood
Linen Robe
Feather Mantle

Preach, Insult, Negotiate, Mimic Daravon
Charge+1, Charge+2, Charge+4



FriendSquirrel
Male
Aquarius
53
42
Mime

Counter Flood
Magic Attack UP
Swim



Thief Hat
Chain Vest
Bracer

Mimic




NIghtdew14
Female
Aquarius
56
77
Dancer
Basic Skill
Auto Potion
Attack UP
Levitate

Ryozan Silk

Headgear
Leather Outfit
Dracula Mantle

Void Storage
Dash, Heal



Powergems
Male
Taurus
43
66
Lancer
Steal
Mana Shield
Dual Wield
Move-HP Up

Javelin
Spear
Gold Helmet
Carabini Mail
Angel Ring

Level Jump8, Vertical Jump3
Steal Helmet, Steal Shield, Steal Weapon, Steal Status

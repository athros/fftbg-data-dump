Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vics
Female
Pisces
55
43
Geomancer
Item
Auto Potion
Throw Item
Waterbreathing

Long Sword
Aegis Shield
Thief Hat
Robe of Lords
Angel Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Lava Ball
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down



Kazanyr
Male
Pisces
46
48
Knight
Draw Out
Parry
Maintenance
Waterwalking

Ragnarok

Crystal Helmet
Platinum Armor
Power Wrist

Speed Break, Stasis Sword, Justice Sword, Dark Sword, Explosion Sword
Murasame



BenYuPoker
Male
Leo
53
45
Ninja
Steal
Blade Grasp
Halve MP
Move-HP Up

Sasuke Knife
Short Edge
Thief Hat
Power Sleeve
Rubber Shoes

Shuriken, Bomb, Knife
Steal Armor, Steal Shield, Arm Aim, Leg Aim



HentaiWriter
Male
Capricorn
63
43
Priest
Item
HP Restore
Dual Wield
Waterbreathing

Rainbow Staff
Rainbow Staff
Thief Hat
Chameleon Robe
Spike Shoes

Cure 2, Raise, Protect, Shell, Shell 2, Esuna, Holy
Potion, Ether, Antidote, Echo Grass, Remedy

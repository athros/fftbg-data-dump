Final Bets: white - 6 bets for 6,299G (37.5%, x1.66); black - 12 bets for 10,480G (62.5%, x0.60)

white bets:
UmaiJam: 3,000G (47.6%, 57,040G)
NovaKnight21: 2,038G (32.4%, 2,038G)
BirbBrainsBot: 369G (5.9%, 369G)
getthemoneyz: 368G (5.8%, 1,511,532G)
RaIshtar: 324G (5.1%, 324G)
AllInBot: 200G (3.2%, 200G)

black bets:
fattunaking: 3,769G (36.0%, 3,769G)
ko2q: 2,855G (27.2%, 2,855G)
gorgewall: 1,122G (10.7%, 1,122G)
randgridr: 600G (5.7%, 600G)
douchetron: 500G (4.8%, 33,309G)
Wooplestein: 500G (4.8%, 6,370G)
Aeriyah: 334G (3.2%, 9,434G)
DesertWooder: 300G (2.9%, 3,058G)
datadrivenbot: 200G (1.9%, 47,240G)
DeathTaxesAndAnime: 100G (1.0%, 6,011G)
CT_5_Holy: 100G (1.0%, 4,468G)
Klednar21: 100G (1.0%, 3,784G)

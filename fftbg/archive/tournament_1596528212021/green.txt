Player: !Green
Team: Green Team
Palettes: Green/White



Sairentozon7
Female
Serpentarius
48
45
Calculator
Black Magic
Damage Split
Attack UP
Retreat

Madlemgen

Holy Miter
Judo Outfit
N-Kai Armlet

CT, Height, 5, 4, 3
Fire 3, Bolt, Ice, Ice 3, Ice 4, Frog, Flare



ALY327
Male
Libra
72
49
Squire
Elemental
Critical Quick
Long Status
Jump+3

Mythril Sword
Aegis Shield
Golden Hairpin
Earth Clothes
Jade Armlet

Accumulate, Heal, Tickle, Fury, Wish
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



TeaTime29
Female
Scorpio
59
56
Calculator
Gore Skill
MA Save
Equip Sword
Fly

Coral Sword

Barbuta
Silk Robe
Spike Shoes

Blue Magic
Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest, Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul



Aeriyah
Female
Leo
76
68
Knight
Basic Skill
Sunken State
Martial Arts
Retreat


Flame Shield
Barbuta
Carabini Mail
Bracer

Power Break
Heal, Wish

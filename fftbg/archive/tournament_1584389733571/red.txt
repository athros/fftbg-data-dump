Player: !Red
Team: Red Team
Palettes: Red/Brown



Ins4n1ty87
Male
Cancer
68
37
Bard
Basic Skill
Counter
Equip Shield
Move-MP Up

Bloody Strings
Gold Shield
Twist Headband
Brigandine
Spike Shoes

Diamond Blade
Accumulate, Heal, Yell, Cheer Up



Lunaticruin
Female
Virgo
47
63
Mediator
Battle Skill
Counter
Sicken
Move-MP Up

Romanda Gun

Cachusha
White Robe
Wizard Mantle

Praise, Preach, Solution, Mimic Daravon
Magic Break, Justice Sword, Night Sword, Explosion Sword



StealthModeLocke
Male
Libra
38
80
Oracle
Math Skill
Counter Flood
Martial Arts
Jump+1



Triangle Hat
Chameleon Robe
Elf Mantle

Blind, Poison, Pray Faith, Zombie, Confusion Song, Dispel Magic, Sleep, Petrify
CT, Height, Prime Number, 3



Dexef
Male
Cancer
71
56
Time Mage
Basic Skill
Counter
Equip Polearm
Waterwalking

Gokuu Rod

Twist Headband
Judo Outfit
Magic Gauntlet

Haste, Haste 2, Slow, Slow 2, Stop, Float, Reflect, Quick, Demi, Stabilize Time
Dash, Heal, Tickle, Scream

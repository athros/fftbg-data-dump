Player: !Red
Team: Red Team
Palettes: Red/Brown



NoxeGS
Male
Serpentarius
49
73
Calculator
Limit
Parry
Secret Hunt
Swim

Bestiary
Mythril Shield
Circlet
Linen Robe
Germinas Boots

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Vacillents
Female
Virgo
47
55
Monk
Dance
Earplug
Sicken
Levitate



Flash Hat
Mythril Vest
Leather Mantle

Earth Slash, Revive
Last Dance, Dragon Pit



ZeroHeat
Monster
Virgo
46
61
Red Panther










Tinglenator
Male
Pisces
54
48
Geomancer
Throw
Mana Shield
Equip Bow
Ignore Terrain

Poison Bow
Ice Shield
Green Beret
Silk Robe
Magic Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Shuriken, Bomb, Stick

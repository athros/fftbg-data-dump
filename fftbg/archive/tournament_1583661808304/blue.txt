Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rnark
Female
Taurus
51
43
Mime

Counter
Magic Defense UP
Jump+2


Escutcheon
Red Hood
Earth Clothes
Elf Mantle

Mimic




FreedomNM
Female
Taurus
66
70
Lancer
Talk Skill
Meatbone Slash
Defend
Ignore Terrain

Obelisk
Hero Shield
Leather Helmet
Mythril Armor
Defense Ring

Level Jump2, Vertical Jump2
Persuade, Solution, Insult, Mimic Daravon



SeniorBunk
Female
Scorpio
51
76
Thief
Elemental
Catch
Magic Attack UP
Jump+1

Diamond Sword

Twist Headband
Mythril Vest
Dracula Mantle

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Nickelbank
Male
Scorpio
53
61
Monk
Throw
Damage Split
Equip Axe
Waterwalking

Giant Axe

Flash Hat
Mystic Vest
Power Wrist

Spin Fist, Wave Fist, Purification, Chakra, Revive
Shuriken, Bomb

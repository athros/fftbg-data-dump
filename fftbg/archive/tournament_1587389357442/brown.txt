Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Mtueni
Female
Aries
72
64
Mediator
Yin Yang Magic
Hamedo
Magic Attack UP
Ignore Terrain

Blaze Gun

Leather Hat
Wizard Robe
Defense Armlet

Invitation, Persuade, Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Poison, Life Drain, Blind Rage, Dispel Magic, Paralyze



Fredxcracklin
Female
Libra
48
60
Mediator
Throw
Counter Tackle
Magic Attack UP
Teleport

Blaze Gun

Feather Hat
Mythril Vest
Defense Ring

Praise, Threaten, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Bomb, Wand, Dictionary



Powergems
Male
Virgo
65
81
Calculator
Demon Skill
Counter
Short Status
Waterbreathing

Bestiary
Crystal Shield
Genji Helmet
Plate Mail
Genji Gauntlet

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



HaychDub
Female
Capricorn
67
44
Lancer
Basic Skill
Counter
Dual Wield
Swim

Partisan
Holy Lance
Circlet
Linen Cuirass
Small Mantle

Level Jump4, Vertical Jump2
Accumulate, Cheer Up

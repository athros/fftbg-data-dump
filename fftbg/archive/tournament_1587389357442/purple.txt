Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ColetteMSLP
Female
Scorpio
53
45
Geomancer
Throw
Abandon
Defend
Lava Walking

Giant Axe
Diamond Shield
Feather Hat
Linen Robe
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Shuriken, Staff



Dictatorhowells
Female
Aries
41
69
Priest
Punch Art
Critical Quick
Equip Bow
Retreat

Hunting Bow

Holy Miter
Silk Robe
Dracula Mantle

Raise, Raise 2, Regen, Protect, Protect 2, Holy
Purification, Chakra



ZephyrTempest
Male
Gemini
50
63
Wizard
Item
Hamedo
Equip Armor
Ignore Terrain

Thunder Rod

Gold Helmet
Genji Armor
Red Shoes

Fire, Fire 3, Bolt 2, Bolt 4
Potion, Ether, Remedy, Phoenix Down



JumbocactuarX27
Female
Virgo
75
56
Mime

Counter
Martial Arts
Jump+2



Cachusha
Clothes
Elf Mantle

Mimic


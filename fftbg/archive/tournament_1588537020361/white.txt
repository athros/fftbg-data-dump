Player: !White
Team: White Team
Palettes: White/Blue



Moises
Male
Scorpio
38
70
Wizard
Sing
Counter
Martial Arts
Move-MP Up

Ice Rod

Black Hood
Leather Outfit
Magic Gauntlet

Fire 3, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 3, Frog
Magic Song, Sky Demon



Renae
Female
Cancer
72
53
Chemist
Black Magic
Caution
Dual Wield
Waterwalking

Hydra Bag
Star Bag
Flash Hat
Mystic Vest
Rubber Shoes

Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Fire 2, Fire 3, Fire 4, Ice, Ice 2



Iobes
Male
Scorpio
55
45
Oracle
Summon Magic
MA Save
Concentrate
Move-MP Up

Iron Fan

Black Hood
Wizard Outfit
108 Gems

Poison, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep, Petrify
Moogle, Ramuh, Titan, Bahamut, Odin, Silf



Perseus
Male
Gemini
78
57
Knight
White Magic
Counter Flood
Equip Polearm
Move+2

Gungnir
Diamond Shield
Barbuta
Mythril Armor
Red Shoes

Head Break, Armor Break, Shield Break, Weapon Break, Power Break
Cure, Raise, Raise 2, Esuna

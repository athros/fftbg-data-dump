Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Genericco
Female
Scorpio
70
78
Geomancer
Draw Out
Counter
Equip Gun
Retreat

Battle Folio
Mythril Shield
Twist Headband
Black Robe
Angel Ring

Pitfall, Water Ball, Static Shock, Quicksand, Gusty Wind, Lava Ball
Kiyomori, Masamune



HaplessOne
Male
Capricorn
79
55
Thief
Draw Out
Speed Save
Dual Wield
Jump+2

Dagger
Ice Brand
Holy Miter
Leather Outfit
Small Mantle

Steal Accessory, Arm Aim, Leg Aim
Bizen Boat, Murasame, Muramasa, Chirijiraden



Carsenhk
Female
Pisces
64
36
Geomancer
Talk Skill
Distribute
Monster Talk
Teleport

Giant Axe
Diamond Shield
Holy Miter
Chameleon Robe
Red Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Preach, Mimic Daravon, Refute, Rehabilitate



OneHundredFists
Female
Cancer
48
69
Priest
Yin Yang Magic
Mana Shield
Dual Wield
Lava Walking

Flame Whip
Flail
Green Beret
Clothes
Elf Mantle

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Esuna, Holy
Spell Absorb, Doubt Faith, Sleep

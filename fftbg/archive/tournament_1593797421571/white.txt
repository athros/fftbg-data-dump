Player: !White
Team: White Team
Palettes: White/Blue



Nizaha
Male
Gemini
58
81
Thief
Jump
Mana Shield
Equip Shield
Teleport

Mythril Knife
Flame Shield
Headgear
Earth Clothes
Defense Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Level Jump2, Vertical Jump4



NovaKnight21
Male
Pisces
69
55
Squire
Item
Absorb Used MP
Sicken
Move+3

Blood Sword
Gold Shield
Thief Hat
Judo Outfit
108 Gems

Heal, Tickle, Yell
Maiden's Kiss, Holy Water, Phoenix Down



Chuckolator
Female
Virgo
73
71
Time Mage
Talk Skill
Parry
Short Status
Fly

Healing Staff

Feather Hat
Chameleon Robe
Cherche

Haste 2, Slow 2, Stop, Reflect, Stabilize Time
Invitation, Praise, Threaten, Preach, Mimic Daravon, Rehabilitate



Aldrammech
Female
Cancer
54
74
Knight
Black Magic
Sunken State
Attack UP
Move+2

Rune Blade
Aegis Shield
Gold Helmet
Linen Cuirass
Magic Gauntlet

Armor Break, Shield Break, Speed Break
Fire 4, Ice 4, Death

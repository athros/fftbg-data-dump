Final Bets: blue - 7 bets for 7,121G (34.1%, x1.93); brown - 13 bets for 13,758G (65.9%, x0.52)

blue bets:
latebit: 4,059G (57.0%, 7,960G)
absalom_20: 963G (13.5%, 963G)
E_Ballard: 736G (10.3%, 48,166G)
soren_of_tyto: 613G (8.6%, 613G)
killth3kid: 500G (7.0%, 1,997G)
datadrivenbot: 200G (2.8%, 67,893G)
captainmilestw: 50G (0.7%, 2,570G)

brown bets:
toka222: 3,600G (26.2%, 235,924G)
pplvee1: 2,500G (18.2%, 36,716G)
EnemyController: 2,222G (16.2%, 1,572,956G)
Firesheath: 1,017G (7.4%, 1,017G)
BirbBrainsBot: 1,000G (7.3%, 3,397G)
dogsandcatsand: 1,000G (7.3%, 3,061G)
skillomono: 930G (6.8%, 1,860G)
WhiteTigress: 500G (3.6%, 22,298G)
Lord_Gwarth: 337G (2.4%, 1,678G)
getthemoneyz: 214G (1.6%, 1,733,973G)
Lyner87: 200G (1.5%, 94,433G)
AllInBot: 138G (1.0%, 138G)
Thyrandaal: 100G (0.7%, 41,451G)

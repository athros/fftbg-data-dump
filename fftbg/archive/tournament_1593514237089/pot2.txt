Final Bets: green - 10 bets for 5,134G (66.8%, x0.50); yellow - 6 bets for 2,557G (33.2%, x2.01)

green bets:
BlackFireUK: 1,286G (25.0%, 1,286G)
BirbBrainsBot: 1,000G (19.5%, 158,659G)
Mesmaster: 801G (15.6%, 801G)
joewcarson: 606G (11.8%, 606G)
reinoe: 500G (9.7%, 13,313G)
Magicandy: 400G (7.8%, 26,101G)
gorgewall: 201G (3.9%, 28,180G)
alecrocks: 140G (2.7%, 143G)
Ring_Wyrm: 100G (1.9%, 3,362G)
datadrivenbot: 100G (1.9%, 47,175G)

yellow bets:
MrFlabyo: 1,000G (39.1%, 394,697G)
SQUiDSQUARKLIN: 621G (24.3%, 621G)
Moshyhero: 500G (19.6%, 8,454G)
getthemoneyz: 236G (9.2%, 1,112,350G)
Evewho: 100G (3.9%, 5,583G)
DuneMeta: 100G (3.9%, 143G)

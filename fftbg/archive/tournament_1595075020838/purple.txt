Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ar Tactic
Female
Cancer
57
74
Priest
Item
Brave Save
Throw Item
Retreat

Rainbow Staff

Ribbon
Power Sleeve
Red Shoes

Cure, Cure 2, Cure 4, Raise 2, Regen, Protect, Shell 2, Esuna
Potion, Maiden's Kiss, Holy Water, Phoenix Down



ALY327
Male
Capricorn
53
55
Samurai
Charge
Sunken State
Equip Sword
Teleport

Masamune

Bronze Helmet
Gold Armor
Magic Gauntlet

Heaven's Cloud, Kikuichimoji
Charge+3, Charge+4, Charge+5



ZCKaiser
Male
Aquarius
65
40
Knight
Time Magic
Parry
Equip Sword
Lava Walking

Heaven's Cloud
Aegis Shield
Genji Helmet
Genji Armor
N-Kai Armlet

Head Break, Magic Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Reflect, Quick, Demi, Stabilize Time, Meteor



Gardiner
Male
Sagittarius
80
62
Monk
Time Magic
Counter Magic
Short Status
Move-HP Up



Red Hood
Black Costume
Diamond Armlet

Spin Fist, Secret Fist, Purification, Seal Evil
Haste 2, Slow 2, Stop, Immobilize, Float, Reflect, Demi 2, Stabilize Time, Meteor

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



NeoSpadeAce
Female
Libra
76
46
Archer
Black Magic
Counter Magic
Concentrate
Move+1

Long Bow

Black Hood
Power Sleeve
Magic Gauntlet

Charge+7, Charge+20
Fire 3, Bolt 2, Ice



MrJamDango
Female
Libra
75
63
Summoner
Charge
Counter
Halve MP
Move+3

Dragon Rod

Leather Hat
Chameleon Robe
Germinas Boots

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Fairy, Cyclops
Charge+10, Charge+20



HuffFlex
Monster
Capricorn
60
65
Wyvern










Mirapoix
Female
Aquarius
67
36
Squire
Elemental
Parry
Equip Gun
Move-MP Up

Romanda Gun
Hero Shield
Barbuta
Chain Mail
Germinas Boots

Heal, Tickle, Yell, Cheer Up, Fury
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind

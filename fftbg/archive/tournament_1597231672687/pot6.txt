Final Bets: white - 4 bets for 2,783G (35.3%, x1.83); brown - 11 bets for 5,098G (64.7%, x0.55)

white bets:
ko2q: 1,021G (36.7%, 1,021G)
BirbBrainsBot: 1,000G (35.9%, 49,608G)
getthemoneyz: 662G (23.8%, 1,616,011G)
arctodus13: 100G (3.6%, 2,330G)

brown bets:
SeniorBunk: 1,622G (31.8%, 1,622G)
Mesmaster: 592G (11.6%, 592G)
SephDarkheart: 544G (10.7%, 107,319G)
dogsandcatsand: 544G (10.7%, 544G)
Tarheels218: 500G (9.8%, 11,797G)
AllInBot: 326G (6.4%, 326G)
Blaze0772: 270G (5.3%, 270G)
CosmicTactician: 200G (3.9%, 8,571G)
datadrivenbot: 200G (3.9%, 59,540G)
TheDeeyo: 200G (3.9%, 1,019G)
MinBetBot: 100G (2.0%, 20,973G)

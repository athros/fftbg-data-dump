Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rocl
Female
Virgo
72
73
Time Mage
Talk Skill
Sunken State
Equip Knife
Ignore Height

Blind Knife

Golden Hairpin
Linen Robe
Germinas Boots

Haste, Slow, Immobilize, Float, Demi, Stabilize Time
Preach, Insult, Mimic Daravon, Refute



TheChainNerd
Female
Virgo
70
42
Dancer
Summon Magic
Damage Split
Defense UP
Jump+2

Zorlin Shape

Thief Hat
White Robe
Jade Armlet

Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Last Dance, Void Storage
Moogle, Ramuh, Ifrit, Carbunkle, Odin, Salamander, Silf, Lich



UmaiJam
Male
Aquarius
60
55
Ninja
Elemental
Blade Grasp
Equip Gun
Move+3

Bloody Strings
Bestiary
Golden Hairpin
Chain Vest
Dracula Mantle

Shuriken, Knife, Dictionary
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



NovaKnight21
Male
Virgo
64
38
Ninja
Jump
Catch
Equip Polearm
Levitate

Ivory Rod
Javelin
Flash Hat
Judo Outfit
Reflect Ring

Sword, Wand
Level Jump4, Vertical Jump6

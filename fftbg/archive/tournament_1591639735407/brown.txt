Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DeathTaxesAndAnime
Monster
Leo
44
49
Holy Dragon










Sans From Snowdin
Male
Sagittarius
77
64
Monk
Battle Skill
MP Restore
Magic Defense UP
Jump+3

Panther Bag

Headgear
Black Costume
Reflect Ring

Wave Fist, Earth Slash, Secret Fist, Purification
Head Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Dark Sword



Roofiepops
Female
Leo
62
38
Ninja
Item
Damage Split
Equip Axe
Move-HP Up

Morning Star
Flame Whip
Triangle Hat
Mystic Vest
Chantage

Bomb
Ether, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down



TeaTime29
Male
Aries
58
54
Geomancer
Item
Mana Shield
Beastmaster
Move+1

Hydra Bag
Platinum Shield
Twist Headband
Earth Clothes
Battle Boots

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Potion, X-Potion, Ether, Eye Drop, Echo Grass

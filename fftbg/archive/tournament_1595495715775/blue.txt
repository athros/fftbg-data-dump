Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



NovaKnight21
Male
Aries
39
62
Mime

Faith Save
Secret Hunt
Ignore Terrain



Holy Miter
Leather Outfit
Leather Mantle

Mimic




Ruleof5
Female
Taurus
76
68
Archer
Throw
MP Restore
Equip Polearm
Ignore Height

Persia
Flame Shield
Red Hood
Wizard Outfit
Sprint Shoes

Charge+2, Charge+5
Bomb, Ninja Sword



RyanTheRebs
Male
Virgo
55
69
Thief
Battle Skill
Meatbone Slash
Equip Polearm
Fly

Battle Bamboo

Red Hood
Wizard Outfit
Genji Gauntlet

Steal Helmet, Steal Armor
Head Break, Shield Break, Speed Break, Power Break, Dark Sword, Night Sword



Carol-Jean
Female
Capricorn
61
59
Ninja
Battle Skill
Faith Save
Martial Arts
Retreat

Blind Knife
Main Gauche
Black Hood
Mythril Vest
Feather Boots

Shuriken, Bomb, Stick
Head Break, Magic Break, Power Break, Mind Break, Justice Sword, Surging Sword

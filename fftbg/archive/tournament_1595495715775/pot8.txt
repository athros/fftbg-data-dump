Final Bets: red - 9 bets for 4,763G (64.5%, x0.55); champion - 9 bets for 2,616G (35.5%, x1.82)

red bets:
Lythe_Caraker: 1,000G (21.0%, 153,778G)
CT_5_Holy: 885G (18.6%, 885G)
ColetteMSLP: 750G (15.7%, 20,552G)
douchetron: 656G (13.8%, 656G)
TeaTime29: 500G (10.5%, 4,774G)
AllInBot: 418G (8.8%, 418G)
superdevon1: 363G (7.6%, 7,278G)
BirbBrainsBot: 117G (2.5%, 9,058G)
getthemoneyz: 74G (1.6%, 1,344,657G)

champion bets:
RyanTheRebs: 728G (27.8%, 728G)
Hasterious: 489G (18.7%, 4,890G)
nekojin: 368G (14.1%, 4,551G)
dem0nj0ns: 300G (11.5%, 2,338G)
artahc: 204G (7.8%, 204G)
datadrivenbot: 200G (7.6%, 35,272G)
Lydian_C: 123G (4.7%, 79,900G)
Chihuahua_Charity: 104G (4.0%, 104G)
readdesert: 100G (3.8%, 2,597G)

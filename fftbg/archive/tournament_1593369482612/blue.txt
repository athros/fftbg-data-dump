Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



O Heyno
Female
Leo
72
79
Summoner
Steal
Meatbone Slash
Equip Sword
Move-HP Up

Long Sword

Triangle Hat
Mystic Vest
Reflect Ring

Moogle, Ramuh, Titan, Carbunkle, Leviathan, Salamander, Fairy, Lich
Steal Shield



Firesheath
Female
Aquarius
59
77
Chemist
Yin Yang Magic
Faith Save
Equip Gun
Teleport

Blast Gun

Holy Miter
Clothes
Wizard Mantle

Potion, X-Potion, Maiden's Kiss, Soft, Phoenix Down
Blind, Spell Absorb, Dispel Magic, Paralyze



DeathTaxesAndAnime
Female
Scorpio
69
62
Wizard
Math Skill
Regenerator
Short Charge
Jump+1

Thunder Rod

Headgear
Linen Robe
Dracula Mantle

Bolt 2, Ice, Ice 3, Death, Flare
Height, 5, 3



Combo Nerd
Male
Sagittarius
62
61
Knight
Elemental
Counter Flood
Equip Axe
Move-MP Up

White Staff
Ice Shield
Circlet
White Robe
Red Shoes

Armor Break, Weapon Break, Power Break, Justice Sword, Night Sword
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

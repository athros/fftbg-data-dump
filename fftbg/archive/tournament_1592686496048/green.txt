Player: !Green
Team: Green Team
Palettes: Green/White



Lawnboxer
Male
Aries
44
51
Thief
Battle Skill
Auto Potion
Short Charge
Retreat

Blind Knife

Leather Hat
Power Sleeve
Defense Armlet

Gil Taking, Steal Helmet, Leg Aim
Weapon Break, Mind Break, Stasis Sword



Galkife
Male
Aries
43
43
Ninja
Basic Skill
Distribute
Short Status
Waterbreathing

Flail
Hidden Knife
Red Hood
Judo Outfit
Magic Ring

Bomb, Knife
Accumulate, Yell, Cheer Up, Fury, Wish



Lowlf
Female
Cancer
53
61
Chemist
Yin Yang Magic
PA Save
Doublehand
Move-MP Up

Hydra Bag

Golden Hairpin
Judo Outfit
Elf Mantle

Potion, Hi-Potion, Ether, Elixir, Maiden's Kiss, Remedy, Phoenix Down
Spell Absorb, Life Drain, Doubt Faith, Zombie, Dispel Magic



Pandasforsale
Male
Taurus
40
72
Wizard
Item
Critical Quick
Concentrate
Retreat

Dagger

Flash Hat
Mystic Vest
Red Shoes

Fire 3, Fire 4, Bolt 3, Ice 2, Ice 3
Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down

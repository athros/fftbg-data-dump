Final Bets: purple - 11 bets for 6,467G (29.1%, x2.43); brown - 14 bets for 15,747G (70.9%, x0.41)

purple bets:
Aldrammech: 2,000G (30.9%, 7,619G)
RunicMagus: 1,200G (18.6%, 29,469G)
dogsandcatsand: 1,172G (18.1%, 16,750G)
GladiatorLupe: 500G (7.7%, 15,884G)
neocarbuncle: 472G (7.3%, 472G)
LionelParks: 364G (5.6%, 364G)
JLinkletter: 259G (4.0%, 9,663G)
Frozeit: 200G (3.1%, 904G)
Lordminsc: 100G (1.5%, 27,378G)
OtherBrand: 100G (1.5%, 6,402G)
datadrivenbot: 100G (1.5%, 44,608G)

brown bets:
VolgraTheMoose: 3,208G (20.4%, 13,208G)
Breakdown777: 3,039G (19.3%, 6,079G)
ericzubat: 1,601G (10.2%, 1,601G)
EnemyController: 1,234G (7.8%, 1,230,244G)
SkylerBunny: 1,200G (7.6%, 628,391G)
BirbBrainsBot: 1,000G (6.4%, 22,475G)
kaidykat: 1,000G (6.4%, 146,199G)
getthemoneyz: 918G (5.8%, 1,003,734G)
theanonymouslurker: 782G (5.0%, 782G)
Laserman1000: 715G (4.5%, 14,615G)
just_here2: 500G (3.2%, 13,183G)
CorpusCav: 250G (1.6%, 3,224G)
Kronikle: 200G (1.3%, 25,376G)
nhammen: 100G (0.6%, 3,621G)

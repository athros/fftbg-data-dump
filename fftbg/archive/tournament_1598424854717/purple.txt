Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Kronikle
Male
Aries
57
73
Ninja
Battle Skill
Counter
Equip Polearm
Move+3

Ryozan Silk
Ryozan Silk
Holy Miter
Wizard Outfit
Feather Mantle

Knife, Staff, Stick
Magic Break, Mind Break



ProteinBiscuit
Female
Cancer
79
73
Dancer
Throw
Brave Save
Secret Hunt
Lava Walking

Cashmere

Triangle Hat
Rubber Costume
Power Wrist

Witch Hunt, Slow Dance, Nameless Dance, Last Dance, Nether Demon
Bomb, Knife, Staff, Spear, Dictionary



Merill
Monster
Virgo
44
67
Chocobo










Sio
Male
Leo
75
78
Mediator
Elemental
Counter
Short Status
Jump+1

Battle Folio

Thief Hat
Light Robe
Small Mantle

Praise, Threaten, Solution, Death Sentence, Refute, Rehabilitate
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind

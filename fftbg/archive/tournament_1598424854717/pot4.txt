Final Bets: purple - 7 bets for 2,717G (27.7%, x2.60); brown - 8 bets for 7,076G (72.3%, x0.38)

purple bets:
BirbBrainsBot: 579G (21.3%, 46,699G)
killth3kid: 520G (19.1%, 520G)
lowlf: 404G (14.9%, 12,914G)
CorpusCav: 400G (14.7%, 4,334G)
Cryptopsy70: 332G (12.2%, 332G)
DaveStrider55: 300G (11.0%, 3,758G)
getthemoneyz: 182G (6.7%, 1,747,272G)

brown bets:
gorgewall: 2,747G (38.8%, 2,747G)
NicoSavoy: 2,000G (28.3%, 69,383G)
thunderducker: 936G (13.2%, 4,680G)
CT_5_Holy: 596G (8.4%, 596G)
MemoriesofFinal: 387G (5.5%, 387G)
datadrivenbot: 200G (2.8%, 62,256G)
Lifebregin: 110G (1.6%, 3,528G)
AllInBot: 100G (1.4%, 100G)

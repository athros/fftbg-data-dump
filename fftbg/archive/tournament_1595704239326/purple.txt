Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Aries
75
58
Archer
Basic Skill
HP Restore
Equip Polearm
Lava Walking

Cashmere
Round Shield
Barette
Clothes
Battle Boots

Charge+1, Charge+3, Charge+4, Charge+5
Accumulate, Dash, Heal, Tickle, Yell, Wish



Flacococo
Male
Virgo
42
54
Thief
Elemental
Damage Split
Dual Wield
Ignore Height

Mythril Knife
Coral Sword
Flash Hat
Brigandine
Feather Boots

Gil Taking, Steal Shield, Leg Aim
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Fenaen
Female
Leo
80
55
Summoner
Battle Skill
Counter
Equip Shield
Move-HP Up

Gold Staff
Diamond Shield
Black Hood
Light Robe
Spike Shoes

Moogle, Ramuh, Titan, Golem, Salamander, Fairy, Lich, Cyclops
Weapon Break, Speed Break, Justice Sword, Surging Sword



Xoomwaffle
Female
Aquarius
72
64
Knight
Steal
Faith Save
Short Charge
Swim

Save the Queen
Diamond Shield
Leather Helmet
Light Robe
Elf Mantle

Head Break, Weapon Break, Magic Break, Speed Break
Steal Heart, Steal Shield

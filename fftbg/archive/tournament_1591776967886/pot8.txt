Final Bets: white - 11 bets for 6,502G (23.5%, x3.26); champion - 7 bets for 21,168G (76.5%, x0.31)

white bets:
KyleWonToLiveForever: 2,000G (30.8%, 4,333G)
AllInBot: 1,191G (18.3%, 1,191G)
BirbBrainsBot: 1,000G (15.4%, 79,431G)
getthemoneyz: 618G (9.5%, 890,877G)
prince_rogers_nelson_: 428G (6.6%, 428G)
HASTERIOUS: 345G (5.3%, 6,902G)
Quadh0nk: 220G (3.4%, 220G)
TeaTime29: 200G (3.1%, 13,952G)
Arcblazer23: 200G (3.1%, 1,476G)
serperemagus: 200G (3.1%, 4,000G)
Evewho: 100G (1.5%, 19,072G)

champion bets:
Kalabain: 11,000G (52.0%, 21,035G)
DeathTaxesAndAnime: 4,949G (23.4%, 9,705G)
NicoSavoy: 2,000G (9.4%, 3,082G)
Mesmaster: 1,627G (7.7%, 1,627G)
ChronoG: 1,092G (5.2%, 1,092G)
kaidykat: 400G (1.9%, 32,671G)
datadrivenbot: 100G (0.5%, 37,309G)

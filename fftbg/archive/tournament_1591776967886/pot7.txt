Final Bets: blue - 9 bets for 16,404G (79.6%, x0.26); white - 9 bets for 4,204G (20.4%, x3.90)

blue bets:
DeathTaxesAndAnime: 9,059G (55.2%, 17,764G)
Kalabain: 4,000G (24.4%, 24,535G)
Evewho: 1,000G (6.1%, 20,072G)
CrownOfHorns: 906G (5.5%, 906G)
NightwolfXVI: 500G (3.0%, 1,206G)
prince_rogers_nelson_: 428G (2.6%, 428G)
Quadh0nk: 311G (1.9%, 311G)
datadrivenbot: 100G (0.6%, 37,409G)
CT_5_Holy: 100G (0.6%, 2,180G)

white bets:
BirbBrainsBot: 1,000G (23.8%, 75,529G)
getthemoneyz: 678G (16.1%, 888,231G)
KyleWonToLiveForever: 600G (14.3%, 1,992G)
serperemagus: 563G (13.4%, 1,803G)
Mesmaster: 332G (7.9%, 332G)
ColetteMSLP: 300G (7.1%, 2,151G)
HASTERIOUS: 288G (6.9%, 5,778G)
AllInBot: 243G (5.8%, 243G)
TeaTime29: 200G (4.8%, 12,672G)

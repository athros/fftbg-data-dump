Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LAGBOT30000
Male
Gemini
69
56
Mediator
Sing
Meatbone Slash
Equip Sword
Move-HP Up

Coral Sword

Golden Hairpin
Mystic Vest
Feather Boots

Threaten, Preach, Solution, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Life Song, Nameless Song, Sky Demon



Gooseyourself
Male
Aquarius
60
75
Chemist
Basic Skill
Mana Shield
Equip Shield
Jump+2

Cute Bag
Diamond Shield
Leather Hat
Clothes
Cursed Ring

Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Dash, Throw Stone, Yell, Cheer Up



Evewho
Female
Gemini
69
48
Archer
Battle Skill
Sunken State
Equip Polearm
Move+1

Holy Lance
Escutcheon
Gold Helmet
Black Costume
108 Gems

Charge+3, Charge+10
Head Break, Armor Break, Shield Break, Speed Break, Power Break, Stasis Sword, Dark Sword, Night Sword, Explosion Sword



ZZ Yoshi
Male
Capricorn
45
80
Lancer
Sing
Mana Shield
Doublehand
Waterbreathing

Cypress Rod

Iron Helmet
Diamond Armor
Cursed Ring

Level Jump5, Vertical Jump6
Angel Song, Battle Song, Magic Song

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Vapetrail
Male
Pisces
77
80
Knight
Item
Brave Up
Equip Gun
Jump+2

Blaze Gun
Diamond Shield
Platinum Helmet
Light Robe
Dracula Mantle

Head Break, Weapon Break, Speed Break, Power Break, Mind Break
X-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



PleXmito
Female
Taurus
79
79
Wizard
Throw
Counter
Magic Attack UP
Swim

Air Knife

Feather Hat
Clothes
Jade Armlet

Bolt 2, Ice
Shuriken



Revvy
Male
Virgo
59
58
Time Mage
Battle Skill
Counter Flood
Attack UP
Teleport

Rainbow Staff

Thief Hat
Chameleon Robe
Setiemson

Haste, Float, Reflect, Demi, Stabilize Time
Speed Break, Justice Sword, Dark Sword, Surging Sword



Chocolatetorpedo
Female
Libra
71
50
Time Mage
Yin Yang Magic
Mana Shield
Long Status
Move+1

Healing Staff

Golden Hairpin
Brigandine
Sprint Shoes

Stop, Immobilize, Float, Demi 2, Meteor
Poison, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep

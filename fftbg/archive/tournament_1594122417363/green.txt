Player: !Green
Team: Green Team
Palettes: Green/White



TheChainNerd
Male
Gemini
74
75
Lancer
Draw Out
Parry
Attack UP
Ignore Height

Spear
Gold Shield
Iron Helmet
Crystal Mail
Leather Mantle

Level Jump4, Vertical Jump8
Koutetsu, Muramasa, Kikuichimoji



Powergems
Female
Gemini
45
75
Archer
Yin Yang Magic
MP Restore
Doublehand
Move+3

Mythril Bow

Headgear
Black Costume
Diamond Armlet

Charge+1, Charge+3, Charge+4, Charge+20
Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep



Kliffw
Female
Leo
68
71
Dancer
Summon Magic
Abandon
Sicken
Jump+1

Ryozan Silk

Twist Headband
Leather Outfit
Magic Ring

Slow Dance, Nether Demon, Dragon Pit
Odin, Fairy



CassiePhoenix
Male
Aquarius
45
67
Monk
Sing
Earplug
Dual Wield
Lava Walking



Black Hood
Adaman Vest
Bracer

Pummel, Purification, Chakra, Revive, Seal Evil
Angel Song, Life Song, Cheer Song, Magic Song, Space Storage

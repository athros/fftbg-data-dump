Final Bets: green - 9 bets for 5,895G (34.0%, x1.94); champion - 7 bets for 11,433G (66.0%, x0.52)

green bets:
BirbBrainsBot: 1,000G (17.0%, 128,920G)
cam_ATS: 1,000G (17.0%, 48,017G)
kliffw: 1,000G (17.0%, 16,395G)
powergems: 1,000G (17.0%, 8,022G)
TheChainNerd: 523G (8.9%, 8,107G)
CassiePhoenix: 500G (8.5%, 2,079G)
DustBirdEX: 345G (5.9%, 1,846G)
pyrelli: 341G (5.8%, 341G)
getthemoneyz: 186G (3.2%, 1,217,509G)

champion bets:
Error72: 6,679G (58.4%, 13,358G)
NIghtdew14: 2,944G (25.8%, 14,724G)
run_with_stone_GUNs: 669G (5.9%, 13,389G)
dogsandcatsand: 641G (5.6%, 4,109G)
datadrivenbot: 200G (1.7%, 55,609G)
hiros13gts: 200G (1.7%, 2,925G)
DeathTaxesAndAnime: 100G (0.9%, 1,433G)

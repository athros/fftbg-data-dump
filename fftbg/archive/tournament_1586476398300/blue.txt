Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



TheKillerNacho
Male
Libra
49
78
Lancer
Draw Out
Counter Flood
Defend
Retreat

Partisan
Aegis Shield
Diamond Helmet
Light Robe
Chantage

Level Jump8, Vertical Jump8
Asura, Koutetsu, Heaven's Cloud



Drgnmstr366
Female
Capricorn
45
75
Thief
Time Magic
Brave Up
Long Status
Retreat

Cultist Dagger

Golden Hairpin
Chain Vest
Elf Mantle

Steal Heart, Steal Armor, Steal Accessory
Haste, Slow, Immobilize, Quick, Stabilize Time



ThePineappleSalesman
Male
Pisces
49
68
Priest
Elemental
Earplug
Sicken
Move+1

Wizard Staff

Leather Hat
Judo Outfit
Angel Ring

Cure 3, Cure 4, Raise, Protect, Shell 2, Wall, Esuna
Water Ball, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Galkife
Female
Aquarius
66
51
Ninja
Yin Yang Magic
Absorb Used MP
Long Status
Swim

Morning Star
Flail
Flash Hat
Wizard Outfit
Defense Armlet

Wand
Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Petrify

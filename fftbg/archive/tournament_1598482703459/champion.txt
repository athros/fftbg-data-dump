Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



CapnChaos12
Male
Taurus
54
46
Chemist
Basic Skill
Regenerator
Equip Axe
Move-MP Up

Flail

Headgear
Mythril Vest
Elf Mantle

Potion, Hi-Potion, Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Throw Stone, Heal, Tickle, Yell, Fury, Ultima



Gelwain
Male
Sagittarius
66
58
Monk
Basic Skill
Counter Tackle
Sicken
Move+2

Hydra Bag

Green Beret
Mystic Vest
Bracer

Spin Fist, Wave Fist, Secret Fist, Chakra, Revive
Dash, Yell



Lokenwow
Male
Aries
53
50
Monk
Jump
Damage Split
Halve MP
Move+3



Flash Hat
Clothes
Magic Ring

Spin Fist, Pummel, Seal Evil
Level Jump5, Vertical Jump2



Shalloween
Female
Pisces
41
59
Thief
Throw
HP Restore
Equip Knife
Move+2

Blind Knife

Twist Headband
Leather Outfit
Dracula Mantle

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Bomb, Knife, Axe, Stick

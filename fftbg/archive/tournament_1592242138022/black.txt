Player: !Black
Team: Black Team
Palettes: Black/Red



Fenixcrest
Male
Sagittarius
65
61
Knight
Elemental
Damage Split
Attack UP
Move-HP Up

Broad Sword
Diamond Shield
Iron Helmet
Chameleon Robe
Power Wrist

Shield Break, Weapon Break, Mind Break, Night Sword
Will-O-Wisp, Blizzard, Gusty Wind



Username123132
Female
Gemini
50
53
Archer
Draw Out
MP Restore
Secret Hunt
Swim

Ice Bow

Triangle Hat
Adaman Vest
Dracula Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+10, Charge+20
Koutetsu, Bizen Boat, Muramasa



L2 Sentinel
Male
Sagittarius
50
79
Knight
Throw
Arrow Guard
Sicken
Swim

Giant Axe
Flame Shield
Gold Helmet
Platinum Armor
Dracula Mantle

Armor Break, Magic Break, Power Break, Mind Break, Dark Sword, Night Sword
Sword



EnemyController
Male
Virgo
59
64
Samurai
Punch Art
Damage Split
Equip Knife
Swim

Ninja Edge

Bronze Helmet
Wizard Robe
Germinas Boots

Asura, Koutetsu, Heaven's Cloud, Muramasa
Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil

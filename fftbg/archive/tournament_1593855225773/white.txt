Player: !White
Team: White Team
Palettes: White/Blue



YaBoy125
Male
Capricorn
44
71
Knight
Summon Magic
Caution
Halve MP
Lava Walking

Mythril Sword
Venetian Shield
Cross Helmet
Linen Cuirass
Reflect Ring

Head Break, Armor Break, Shield Break, Speed Break, Dark Sword, Explosion Sword
Moogle, Shiva, Ramuh, Odin, Leviathan, Salamander, Lich



Zachara
Female
Scorpio
48
49
Calculator
Black Magic
Regenerator
Short Status
Jump+3

Papyrus Codex

Headgear
White Robe
Chantage

CT, 5
Fire 2, Fire 4, Bolt, Bolt 2, Ice 4, Frog



Digitalsocrates
Male
Leo
57
61
Knight
Punch Art
Mana Shield
Sicken
Retreat

Slasher
Diamond Shield
Leather Helmet
Silk Robe
Rubber Shoes

Weapon Break, Surging Sword
Chakra



Colleen
Female
Capricorn
50
78
Priest
Punch Art
Parry
Martial Arts
Move-MP Up

Mace of Zeus

Black Hood
Black Robe
Battle Boots

Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Shell, Esuna, Magic Barrier
Purification

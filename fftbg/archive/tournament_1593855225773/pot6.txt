Final Bets: black - 8 bets for 7,262G (48.0%, x1.08); purple - 5 bets for 7,856G (52.0%, x0.92)

black bets:
Mesmaster: 2,000G (27.5%, 25,355G)
josephiroth_143: 1,850G (25.5%, 1,850G)
BirbBrainsBot: 1,000G (13.8%, 192,259G)
YaBoy125: 656G (9.0%, 656G)
BoneMiser: 500G (6.9%, 500G)
Arcblazer23: 500G (6.9%, 3,123G)
DustBirdEX: 456G (6.3%, 8,810G)
twelfthrootoftwo: 300G (4.1%, 9,817G)

purple bets:
ruebyy: 6,000G (76.4%, 7,497G)
lowlf: 1,122G (14.3%, 99,774G)
bruubarg: 328G (4.2%, 328G)
NotBalrog: 300G (3.8%, 8,140G)
getthemoneyz: 106G (1.3%, 1,175,900G)

Final Bets: green - 4 bets for 4,800G (57.0%, x0.75); yellow - 7 bets for 3,621G (43.0%, x1.33)

green bets:
KasugaiRoastedPeas: 3,000G (62.5%, 7,567G)
Lythe_Caraker: 1,000G (20.8%, 147,036G)
Zachara: 500G (10.4%, 137,200G)
NotBalrog: 300G (6.2%, 8,740G)

yellow bets:
BirbBrainsBot: 1,000G (27.6%, 186,658G)
Mesmaster: 1,000G (27.6%, 25,888G)
getthemoneyz: 876G (24.2%, 1,173,644G)
DustBirdEX: 403G (11.1%, 2,847G)
josephiroth_143: 192G (5.3%, 192G)
bruubarg: 100G (2.8%, 16,457G)
Aquios_: 50G (1.4%, 582G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Powergems
Female
Cancer
49
61
Chemist
Time Magic
Dragon Spirit
Doublehand
Lava Walking

Cute Bag

Twist Headband
Black Costume
Power Wrist

Potion, X-Potion, Hi-Ether, Phoenix Down
Haste, Quick



Cam ATS
Male
Gemini
64
67
Wizard
Draw Out
Damage Split
Equip Armor
Teleport 2

Orichalcum

Leather Helmet
Black Robe
Chantage

Bolt 2, Bolt 3, Bolt 4, Ice, Empower
Asura



Laserman1000
Male
Cancer
61
53
Calculator
White Magic
Auto Potion
Doublehand
Jump+2

Bestiary

Triangle Hat
Chameleon Robe
Spike Shoes

CT, 4, 3
Cure, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Wall, Esuna, Holy



Oobs56
Female
Gemini
65
57
Oracle
Jump
Brave Save
Equip Armor
Waterbreathing

Cypress Rod

Gold Helmet
Reflect Mail
Red Shoes

Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Sleep
Level Jump2, Vertical Jump5

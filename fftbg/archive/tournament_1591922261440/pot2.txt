Final Bets: green - 18 bets for 8,955G (34.1%, x1.93); yellow - 18 bets for 17,277G (65.9%, x0.52)

green bets:
SkylerBunny: 1,100G (12.3%, 101,716G)
killth3kid: 1,000G (11.2%, 14,534G)
BirbBrainsBot: 1,000G (11.2%, 151,109G)
Rislyeu: 1,000G (11.2%, 33,088G)
Mesmaster: 1,000G (11.2%, 72,295G)
HaateXIII: 728G (8.1%, 728G)
YaBoy125: 688G (7.7%, 688G)
twelfthrootoftwo: 511G (5.7%, 511G)
woohoo_brokeback: 456G (5.1%, 7,598G)
solomongrundy85: 400G (4.5%, 4,972G)
YamiKitsune: 250G (2.8%, 500G)
benticore: 182G (2.0%, 3,650G)
VagabondAzulien: 150G (1.7%, 1,987G)
getthemoneyz: 140G (1.6%, 922,081G)
moocaotao: 100G (1.1%, 2,299G)
E_Ballard: 100G (1.1%, 6,760G)
rednecknazgul: 100G (1.1%, 840G)
IphoneDarkness: 50G (0.6%, 3,002G)

yellow bets:
toka222: 3,700G (21.4%, 44,602G)
HASTERIOUS: 3,483G (20.2%, 3,483G)
Thyrandaal: 1,111G (6.4%, 11,360G)
Evewho: 1,000G (5.8%, 17,799G)
Meta_Five: 1,000G (5.8%, 4,627G)
Digitalsocrates: 1,000G (5.8%, 47,460G)
LeepingJJ: 1,000G (5.8%, 4,081G)
Oobs56: 1,000G (5.8%, 3,275G)
superdevon1: 817G (4.7%, 1,634G)
metagameface: 795G (4.6%, 795G)
Laserman1000: 596G (3.4%, 20,796G)
reinoe: 500G (2.9%, 13,020G)
fluffskull: 300G (1.7%, 882G)
CosmicTactician: 300G (1.7%, 24,191G)
DerangedShadow: 274G (1.6%, 274G)
gorgewall: 201G (1.2%, 15,614G)
AllInBot: 100G (0.6%, 100G)
datadrivenbot: 100G (0.6%, 36,369G)

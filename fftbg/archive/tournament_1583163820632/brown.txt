Player: !Brown
Team: Brown Team
Palettes: Brown/Green



OmnibotGamma
Male
Gemini
56
56
Mime

Brave Up
Equip Shield
Waterbreathing


Diamond Shield
Flash Hat
Clothes
108 Gems

Mimic




Liamsulli
Male
Leo
50
60
Mediator
Yin Yang Magic
Auto Potion
Equip Armor
Ignore Terrain

Blast Gun

Iron Helmet
Clothes
Angel Ring

Threaten, Negotiate, Refute
Blind, Life Drain, Silence Song, Confusion Song, Dispel Magic, Paralyze



Athropos1
Male
Pisces
44
55
Bard
Punch Art
Dragon Spirit
Magic Defense UP
Move+3

Fairy Harp

Leather Hat
Power Sleeve
Feather Mantle

Angel Song, Life Song, Battle Song, Magic Song
Earth Slash, Chakra, Revive



SovietCrusierski
Female
Pisces
68
46
Priest
Charge
Sunken State
Equip Knife
Move-MP Up

Blind Knife

Green Beret
Wizard Outfit
Leather Mantle

Cure, Cure 3, Raise, Raise 2, Protect 2, Shell 2, Wall, Esuna
Charge+1, Charge+2, Charge+3

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Loveyouallfriends
Male
Aries
61
54
Samurai
Throw
Counter
Long Status
Move+3

Kiyomori

Mythril Helmet
Carabini Mail
Sprint Shoes

Bizen Boat, Murasame, Muramasa, Kikuichimoji
Shuriken, Axe



Pandasforsale
Female
Cancer
64
68
Samurai
Summon Magic
Auto Potion
Magic Attack UP
Move+1

Murasame

Cross Helmet
Diamond Armor
Bracer

Asura, Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Shiva, Ifrit, Titan



SkylerBunny
Monster
Cancer
71
75
Blue Dragon










Kingchadking
Male
Virgo
80
79
Calculator
Black Magic
Caution
Equip Axe
Waterwalking

White Staff

Golden Hairpin
Silk Robe
Feather Boots

CT, Height, Prime Number, 5
Ice 2, Ice 3, Ice 4, Flare

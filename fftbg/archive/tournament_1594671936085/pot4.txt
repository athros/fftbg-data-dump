Final Bets: purple - 12 bets for 7,236G (48.5%, x1.06); brown - 19 bets for 7,683G (51.5%, x0.94)

purple bets:
Firesheath: 2,310G (31.9%, 2,310G)
NicoSavoy: 1,000G (13.8%, 182,542G)
HaateXIII: 847G (11.7%, 847G)
Laserman1000: 770G (10.6%, 30,070G)
FriendSquirrel: 560G (7.7%, 560G)
Aldrammech: 444G (6.1%, 444G)
Helllyesss: 400G (5.5%, 2,656G)
TasisSai: 343G (4.7%, 674G)
benticore: 250G (3.5%, 17,944G)
skipsandwiches: 112G (1.5%, 905G)
fluffskull: 100G (1.4%, 3,417G)
Jumza: 100G (1.4%, 1,062G)

brown bets:
E_Ballard: 1,000G (13.0%, 3,143G)
BlueAbs: 800G (10.4%, 6,458G)
BirbBrainsBot: 705G (9.2%, 132,878G)
KyleWonToLiveForever: 705G (9.2%, 3,664G)
roqqqpsi: 588G (7.7%, 5,350G)
dem0nj0ns: 502G (6.5%, 1,676G)
DamnThatShark: 500G (6.5%, 4,462G)
helpimabug: 488G (6.4%, 488G)
CapnChaos12: 300G (3.9%, 6,727G)
Grandlanzer: 300G (3.9%, 74,929G)
Lemonjohns: 300G (3.9%, 4,890G)
getthemoneyz: 296G (3.9%, 1,279,179G)
gorgewall: 201G (2.6%, 5,076G)
nsm013: 200G (2.6%, 11,640G)
datadrivenbot: 200G (2.6%, 57,364G)
dtrain332: 200G (2.6%, 420G)
superdevon1: 198G (2.6%, 3,972G)
nhammen: 100G (1.3%, 8,959G)
furrytomahawkk: 100G (1.3%, 4,019G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



YaBoy125
Male
Capricorn
58
53
Ninja
Charge
Brave Up
Equip Gun
Move+2

Stone Gun
Romanda Gun
Twist Headband
Earth Clothes
Germinas Boots

Shuriken, Bomb, Dictionary
Charge+3, Charge+7



NeonLance
Female
Aries
44
74
Calculator
White Magic
Counter Flood
Halve MP
Move-HP Up

Ice Rod

Thief Hat
Robe of Lords
N-Kai Armlet

CT, Prime Number, 4, 3
Raise, Holy



Eco1327
Male
Libra
37
72
Squire
Summon Magic
Counter Magic
Equip Gun
Lava Walking

Stone Gun

Triangle Hat
Leather Vest
Cursed Ring

Heal, Yell
Moogle, Ifrit, Odin, Leviathan, Silf, Fairy, Lich, Cyclops



Draco259
Female
Scorpio
58
80
Squire
White Magic
Faith Up
Sicken
Waterwalking

Flame Whip

Holy Miter
Wizard Robe
Diamond Armlet

Accumulate, Dash, Heal, Tickle, Wish
Cure 3, Raise, Reraise, Regen, Wall, Esuna

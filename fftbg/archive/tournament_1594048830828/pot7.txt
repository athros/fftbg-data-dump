Final Bets: blue - 20 bets for 13,690G (67.9%, x0.47); white - 7 bets for 6,469G (32.1%, x2.12)

blue bets:
Nickyfive: 2,388G (17.4%, 4,776G)
ShintaroNayaka: 2,116G (15.5%, 2,116G)
IDruggedShamu: 1,351G (9.9%, 1,351G)
DudeMonkey77: 1,033G (7.5%, 1,033G)
Nizaha: 1,001G (7.3%, 11,198G)
killth3kid: 1,000G (7.3%, 104,397G)
DeathTaxesAndAnime: 860G (6.3%, 860G)
Shalloween: 600G (4.4%, 65,927G)
BoneMiser: 504G (3.7%, 504G)
DLJuggernaut: 500G (3.7%, 4,181G)
MagicBottle: 500G (3.7%, 15,591G)
ZCKaiser: 316G (2.3%, 5,397G)
ArashiKurobara: 280G (2.0%, 8,649G)
BirbBrainsBot: 240G (1.8%, 84,723G)
gorgewall: 201G (1.5%, 20,415G)
silverelmdor: 200G (1.5%, 2,051G)
datadrivenbot: 200G (1.5%, 57,261G)
reinoe: 200G (1.5%, 56,407G)
lastly: 100G (0.7%, 37,543G)
nhammen: 100G (0.7%, 12,429G)

white bets:
sinnyil2: 3,649G (56.4%, 7,299G)
fenaen: 1,200G (18.6%, 24,743G)
SephDarkheart: 632G (9.8%, 632G)
EizanTayama: 500G (7.7%, 6,208G)
alterworlds: 256G (4.0%, 256G)
getthemoneyz: 124G (1.9%, 1,204,564G)
hiros13gts: 108G (1.7%, 108G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dogsandcatsand
Female
Cancer
61
79
Oracle
Punch Art
PA Save
Dual Wield
Retreat

Musk Rod
Octagon Rod
Green Beret
Black Robe
Defense Armlet

Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Sleep, Petrify
Wave Fist, Earth Slash, Secret Fist, Chakra, Revive



Lastly
Male
Aquarius
53
45
Calculator
Black Magic
Earplug
Dual Wield
Fly

Ivory Rod
Ivory Rod
Black Hood
Black Costume
Small Mantle

Height, 4, 3
Fire 2, Bolt, Ice 3, Frog



Serperemagus
Female
Aquarius
56
60
Summoner
Charge
Absorb Used MP
Concentrate
Move+2

Bestiary

Flash Hat
Light Robe
Setiemson

Moogle, Ramuh, Carbunkle, Cyclops
Charge+3, Charge+10



BoneMiser
Male
Taurus
74
65
Mime

Faith Save
Equip Shield
Move+1


Flame Shield
Black Hood
Brigandine
Red Shoes

Mimic


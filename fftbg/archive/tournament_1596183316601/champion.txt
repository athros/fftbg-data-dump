Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Douchetron
Female
Virgo
80
44
Dancer
Time Magic
Speed Save
Short Status
Ignore Height

Ryozan Silk

Twist Headband
Chameleon Robe
Germinas Boots

Nether Demon
Haste, Meteor



CT 5 Holy
Male
Libra
74
80
Lancer
Punch Art
Earplug
Doublehand
Jump+3

Mythril Spear

Leather Helmet
Diamond Armor
108 Gems

Level Jump4, Vertical Jump8
Earth Slash, Revive, Seal Evil



Chompie
Female
Taurus
77
71
Wizard
Punch Art
Faith Save
Short Charge
Ignore Height

Dagger

Black Hood
Power Sleeve
Angel Ring

Fire, Fire 2, Fire 3, Frog, Flare
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Rasul
Male
Sagittarius
80
59
Knight
Summon Magic
Arrow Guard
Secret Hunt
Move+1

Save the Queen
Genji Shield
Bronze Helmet
Linen Robe
Cursed Ring

Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Justice Sword
Moogle, Shiva, Ramuh, Odin, Leviathan, Lich

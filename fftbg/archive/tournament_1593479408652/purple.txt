Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Snowfats
Male
Sagittarius
46
49
Mediator
Sing
Mana Shield
Short Charge
Teleport

Stone Gun

Flash Hat
Silk Robe
Rubber Shoes

Preach, Solution, Death Sentence, Mimic Daravon, Refute
Magic Song, Sky Demon



Zagorsek
Male
Scorpio
77
45
Lancer
Talk Skill
Dragon Spirit
Short Status
Lava Walking

Javelin
Crystal Shield
Mythril Helmet
Platinum Armor
Magic Ring

Level Jump8, Vertical Jump5
Persuade, Threaten, Solution



Shalloween
Female
Virgo
61
52
Calculator
Black Magic
Abandon
Short Charge
Swim

Poison Rod

Triangle Hat
Adaman Vest
Leather Mantle

CT, Height, 5, 4
Fire, Ice, Ice 2, Ice 3



O Heyno
Female
Leo
46
41
Samurai
Item
Dragon Spirit
Magic Attack UP
Jump+2

Heaven's Cloud

Iron Helmet
Plate Mail
108 Gems

Kiyomori, Kikuichimoji
Potion, Hi-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down

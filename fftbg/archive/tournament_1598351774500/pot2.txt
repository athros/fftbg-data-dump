Final Bets: green - 7 bets for 5,075G (50.3%, x0.99); yellow - 11 bets for 5,023G (49.7%, x1.01)

green bets:
Willjin: 3,000G (59.1%, 106,011G)
Aldrammech: 700G (13.8%, 700G)
skipsandwiches: 674G (13.3%, 674G)
3ngag3: 300G (5.9%, 19,800G)
datadrivenbot: 200G (3.9%, 64,351G)
gorgewall: 101G (2.0%, 20,095G)
AllInBot: 100G (2.0%, 100G)

yellow bets:
NicoSavoy: 2,000G (39.8%, 38,549G)
BirbBrainsBot: 1,000G (19.9%, 24,286G)
bruubarg: 452G (9.0%, 452G)
NIghtdew14: 388G (7.7%, 388G)
MemoriesofFinal: 277G (5.5%, 277G)
getthemoneyz: 206G (4.1%, 1,734,924G)
iBardic: 200G (4.0%, 3,109G)
ar_tactic: 200G (4.0%, 79,210G)
recneps567: 100G (2.0%, 2,607G)
ravingsockmonkey: 100G (2.0%, 1,612G)
FoxMime: 100G (2.0%, 150G)

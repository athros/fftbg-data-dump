Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Male
Serpentarius
66
61
Knight
Punch Art
Damage Split
Halve MP
Move-HP Up

Sleep Sword
Crystal Shield
Circlet
Carabini Mail
Battle Boots

Shield Break, Weapon Break, Magic Break, Power Break, Justice Sword, Dark Sword
Secret Fist, Purification, Revive



Sairentozon7
Male
Gemini
66
67
Mime

Counter
Equip Armor
Waterwalking



Holy Miter
Silk Robe
Defense Armlet

Mimic




Vicemond
Male
Virgo
71
79
Geomancer
White Magic
Earplug
Defense UP
Waterbreathing

Battle Axe
Diamond Shield
Thief Hat
Black Costume
Reflect Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Cure 2, Raise, Regen, Shell 2, Esuna



ALY327
Female
Taurus
62
66
Dancer
Yin Yang Magic
PA Save
Equip Axe
Jump+3

Giant Axe

Black Hood
Judo Outfit
Germinas Boots

Wiznaibus
Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Paralyze

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Reddwind
Male
Aries
49
75
Knight
Talk Skill
MP Restore
Doublehand
Teleport

Ice Brand

Iron Helmet
Chain Mail
Red Shoes

Shield Break, Weapon Break, Magic Break, Speed Break, Power Break
Invitation, Persuade, Refute



Hirameki85
Male
Virgo
50
80
Mime

Absorb Used MP
Equip Shield
Ignore Height


Crystal Shield
Triangle Hat
Mythril Vest
Bracer

Mimic




Sinnyil2
Male
Capricorn
64
56
Time Mage
Yin Yang Magic
Brave Save
Maintenance
Move+2

Wizard Staff

Red Hood
Silk Robe
Feather Boots

Slow, Slow 2, Stop, Float, Reflect, Quick, Meteor
Life Drain, Doubt Faith, Confusion Song, Paralyze, Dark Holy



Flacococo
Female
Virgo
56
42
Thief
Talk Skill
Faith Save
Monster Talk
Waterwalking

Air Knife

Ribbon
Black Costume
Defense Ring

Gil Taking, Steal Shield, Steal Accessory
Praise, Threaten, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate

Player: !Black
Team: Black Team
Palettes: Black/Red



SeniorBunk
Monster
Gemini
53
62
Black Goblin










Sairentozon7
Male
Aries
57
76
Chemist
Time Magic
Parry
Equip Shield
Jump+3

Star Bag
Bronze Shield
Feather Hat
Judo Outfit
Cursed Ring

Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Reflect, Quick, Demi, Stabilize Time



Ko2q
Female
Cancer
63
49
Oracle
Charge
Catch
Dual Wield
Levitate

Iron Fan
Iron Fan
Black Hood
Linen Robe
Elf Mantle

Blind, Spell Absorb, Paralyze, Sleep, Dark Holy
Charge+2



NovaKnight21
Female
Gemini
64
52
Lancer
Throw
Abandon
Equip Gun
Ignore Height

Madlemgen
Platinum Shield
Iron Helmet
Plate Mail
Bracer

Level Jump2, Vertical Jump6
Shuriken, Bomb, Knife

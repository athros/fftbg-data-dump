Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Neerrm
Female
Capricorn
51
59
Calculator
Black Magic
Counter Tackle
Magic Attack UP
Lava Walking

Cypress Rod

Red Hood
Silk Robe
Angel Ring

CT, Height, Prime Number, 4, 3
Fire 3, Bolt 3, Death, Flare



XBizzy
Monster
Scorpio
46
80
Chocobo










Thripacfershur
Female
Taurus
62
68
Oracle
Punch Art
Critical Quick
Magic Attack UP
Move+2

Battle Folio

Red Hood
Chameleon Robe
Salty Rage

Blind, Poison, Spell Absorb, Blind Rage, Foxbird, Dispel Magic, Paralyze
Spin Fist, Pummel, Secret Fist, Purification, Revive



StealthModeLocke
Female
Taurus
72
50
Ninja
Charge
Damage Split
Short Status
Move-MP Up

Ninja Edge
Short Edge
Black Hood
Rubber Costume
Magic Ring

Shuriken, Bomb, Knife, Stick
Charge+1, Charge+2, Charge+5, Charge+7

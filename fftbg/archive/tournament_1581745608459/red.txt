Player: !Red
Team: Red Team
Palettes: Red/Brown



Skipias
Female
Scorpio
70
60
Chemist
Steal
Absorb Used MP
Doublehand
Jump+1

Zorlin Shape

Holy Miter
Adaman Vest
Reflect Ring

Potion, X-Potion, Ether, Phoenix Down
Gil Taking, Steal Heart



Holyonline
Female
Virgo
58
40
Knight
Punch Art
Auto Potion
Attack UP
Jump+2

Defender

Diamond Helmet
Gold Armor
Dracula Mantle

Head Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword, Surging Sword
Wave Fist, Purification, Revive



WhiteTigress
Female
Aries
79
52
Calculator
Yin Yang Magic
Mana Shield
Equip Polearm
Move-HP Up

Musk Rod

Red Hood
Silk Robe
108 Gems

Height, Prime Number, 5, 3
Spell Absorb, Sleep, Petrify



Silverthundr
Female
Gemini
46
38
Geomancer
Throw
MP Restore
Equip Sword
Move+1

Slasher
Hero Shield
Black Hood
Linen Robe
N-Kai Armlet

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Axe

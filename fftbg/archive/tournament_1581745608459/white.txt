Player: !White
Team: White Team
Palettes: White/Blue



Typicalfanboy
Female
Leo
71
74
Mediator
Jump
Meatbone Slash
Equip Bow
Move-HP Up

Ice Bow

Holy Miter
White Robe
Rubber Shoes

Invitation, Persuade, Insult, Negotiate, Refute
Level Jump8, Vertical Jump7



Princessmomonoke
Male
Scorpio
70
72
Bard
Elemental
Caution
Martial Arts
Teleport



Headgear
Carabini Mail
Small Mantle

Life Song, Hydra Pit
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



OniElem
Female
Taurus
49
61
Knight
Black Magic
Speed Save
Dual Wield
Move+3

Blood Sword
Diamond Sword
Mythril Helmet
Mythril Armor
Dracula Mantle

Shield Break, Magic Break, Speed Break, Dark Sword
Ice, Ice 3



Deciimos
Male
Taurus
43
49
Knight
Elemental
Catch
Dual Wield
Move+3

Nagrarock
Long Sword
Platinum Helmet
White Robe
Battle Boots

Speed Break, Mind Break, Surging Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Lava Ball

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



LDHaten
Female
Gemini
53
77
Archer
Yin Yang Magic
Counter Tackle
Magic Attack UP
Ignore Terrain

Hunting Bow
Buckler
Red Hood
Clothes
Leather Mantle

Charge+2, Charge+4, Charge+5, Charge+7, Charge+20
Blind, Poison, Spell Absorb, Confusion Song, Dispel Magic, Paralyze



Sinnyil2
Male
Leo
59
70
Knight
Basic Skill
Auto Potion
Attack UP
Ignore Height

Defender

Barbuta
Gold Armor
108 Gems

Armor Break, Surging Sword, Explosion Sword
Heal, Tickle, Yell



TheFalseTruth
Male
Gemini
57
39
Mediator
Time Magic
Abandon
Equip Armor
Jump+2

Papyrus Codex

Bronze Helmet
Mythril Armor
Elf Mantle

Death Sentence, Negotiate, Refute, Rehabilitate
Slow 2, Stop, Immobilize, Float, Quick, Demi



Ominnous
Male
Sagittarius
47
64
Knight
Talk Skill
Parry
Beastmaster
Move+3

Long Sword
Mythril Shield
Gold Helmet
Platinum Armor
Wizard Mantle

Speed Break, Justice Sword, Dark Sword
Invitation, Threaten, Solution, Insult

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheMurkGnome
Male
Leo
60
78
Wizard
Item
Blade Grasp
Long Status
Fly

Thunder Rod

Flash Hat
Adaman Vest
Magic Ring

Fire, Fire 4, Bolt 4, Ice 4
Potion, X-Potion, Antidote, Phoenix Down



VolgraTheMoose
Female
Aries
57
56
Time Mage
Yin Yang Magic
Regenerator
Short Charge
Jump+2

Healing Staff

Leather Hat
Brigandine
Dracula Mantle

Haste, Demi 2, Stabilize Time
Spell Absorb, Doubt Faith, Silence Song



SephDarkheart
Male
Sagittarius
79
72
Mime

MP Restore
Equip Armor
Move+1



Crystal Helmet
Plate Mail
Germinas Boots

Mimic




Gelwain
Male
Sagittarius
43
57
Ninja
Elemental
Earplug
Defense UP
Move+3

Hidden Knife
Morning Star
Red Hood
Secret Clothes
Diamond Armlet

Shuriken, Bomb, Stick
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

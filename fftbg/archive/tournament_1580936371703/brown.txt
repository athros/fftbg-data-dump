Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Warwizard23
Male
Serpentarius
77
49
Wizard
Talk Skill
HP Restore
Maintenance
Fly

Rod

Red Hood
Linen Robe
Sprint Shoes

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4
Persuade, Preach, Refute, Rehabilitate



Ninesilvers
Monster
Sagittarius
71
42
Cockatrice










SneakyFingerz
Male
Sagittarius
56
56
Mime

Brave Up
Defend
Move+3



Headgear
Chameleon Robe
Defense Ring

Mimic




Ikebr
Male
Aquarius
42
52
Archer
Basic Skill
Regenerator
Sicken
Levitate

Poison Bow
Buckler
Holy Miter
Wizard Outfit
Magic Gauntlet

Charge+1, Charge+4
Yell, Scream

Player: !White
Team: White Team
Palettes: White/Blue



Kakkoine
Monster
Taurus
57
64
Minotaur










Shalloween
Female
Aquarius
61
65
Archer
Time Magic
PA Save
Dual Wield
Move+1

Silver Bow

Leather Hat
Judo Outfit
Defense Armlet

Charge+3, Charge+4
Slow, Quick, Demi 2, Stabilize Time



BlasphemousRoar
Female
Sagittarius
37
80
Samurai
Yin Yang Magic
Parry
Sicken
Retreat

Chirijiraden

Iron Helmet
Linen Cuirass
Magic Gauntlet

Koutetsu, Bizen Boat, Murasame, Kiyomori
Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Dispel Magic



Aelmarkin
Female
Serpentarius
49
40
Mime

MP Restore
Sicken
Ignore Terrain



Leather Hat
Leather Outfit
Wizard Mantle

Mimic


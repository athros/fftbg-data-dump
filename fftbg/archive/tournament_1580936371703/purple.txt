Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mayfu
Monster
Sagittarius
46
57
Juravis










0v3rr8d
Female
Aries
53
41
Samurai
Charge
Absorb Used MP
Equip Armor
Waterwalking

Kiyomori

Bronze Helmet
Diamond Armor
Jade Armlet

Koutetsu
Charge+1, Charge+2, Charge+4, Charge+7



Nizaha
Male
Sagittarius
72
60
Ninja
Draw Out
Counter Magic
Long Status
Move+3

Ninja Edge
Short Edge
Thief Hat
Earth Clothes
Diamond Armlet

Shuriken, Bomb
Bizen Boat



Phrossi V
Male
Cancer
46
72
Monk
Black Magic
Distribute
Sicken
Move+2



Triangle Hat
Black Costume
Small Mantle

Pummel, Earth Slash, Purification, Chakra, Revive
Fire, Bolt 2, Bolt 3, Empower, Frog

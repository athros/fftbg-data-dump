Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



GameCorps
Male
Gemini
46
56
Ninja
Charge
Catch
Secret Hunt
Move-MP Up

Flail
Flame Whip
Red Hood
Leather Outfit
Defense Armlet

Shuriken
Charge+4, Charge+5, Charge+20



Ketalis
Female
Libra
63
78
Monk
Steal
Faith Up
Equip Polearm
Swim

Cashmere

Leather Hat
Mythril Vest
Spike Shoes

Secret Fist, Purification, Chakra, Revive
Gil Taking, Steal Status, Leg Aim



ForagerCats
Female
Gemini
44
62
Knight
Elemental
Damage Split
Dual Wield
Lava Walking

Defender
Slasher
Barbuta
Platinum Armor
Sprint Shoes

Head Break, Shield Break, Speed Break, Power Break, Stasis Sword, Justice Sword
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



VVitchFaye
Male
Aries
64
61
Calculator
Tentacle Skill
Counter
Long Status
Jump+3

Gokuu Rod

Leather Hat
Crystal Mail
Angel Ring

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Trowaba
Male
Leo
44
46
Mediator
Battle Skill
Parry
Dual Wield
Swim

Romanda Gun
Romanda Gun
Headgear
Clothes
Reflect Ring

Solution, Negotiate
Power Break, Mind Break, Justice Sword



Powergems
Male
Scorpio
39
55
Chemist
Time Magic
Counter Flood
Equip Shield
Move+2

Glacier Gun
Aegis Shield
Golden Hairpin
Mythril Vest
Angel Ring

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Soft, Phoenix Down
Haste, Haste 2, Slow 2, Reflect, Quick, Demi 2, Stabilize Time



Kl0kwurk
Monster
Virgo
53
64
Bomb










Tougou
Female
Leo
79
59
Ninja
White Magic
Faith Up
Concentrate
Levitate

Sasuke Knife
Kunai
Golden Hairpin
Brigandine
Vanish Mantle

Shuriken
Cure 3, Cure 4, Raise, Reraise, Wall, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



AlysSariel
Female
Cancer
62
54
Summoner
Time Magic
Counter
Short Charge
Fly

Poison Rod

Headgear
Adaman Vest
Wizard Mantle

Ramuh, Ifrit, Carbunkle
Haste, Stop, Immobilize, Demi, Stabilize Time, Meteor



CapnChaos12
Male
Libra
52
78
Samurai
Charge
Regenerator
Equip Sword
Waterwalking

Chaos Blade

Cross Helmet
Leather Armor
Magic Ring

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Charge+7



PeachPatrol
Female
Pisces
70
51
Oracle
Punch Art
Earplug
Long Status
Move+1

Battle Folio

Headgear
Power Sleeve
Defense Armlet

Blind, Life Drain, Doubt Faith, Confusion Song, Dispel Magic
Wave Fist, Purification, Revive, Seal Evil



EvilLego6
Male
Serpentarius
66
66
Mediator
Punch Art
Arrow Guard
Equip Gun
Waterwalking

Glacier Gun

Ribbon
Black Robe
Cursed Ring

Invitation, Persuade, Threaten, Preach, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification

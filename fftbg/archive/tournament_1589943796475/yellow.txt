Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Byrdturbo
Male
Aquarius
56
80
Bard
Elemental
Counter
Equip Bow
Fly

Windslash Bow

Twist Headband
Leather Armor
Sprint Shoes

Cheer Song, Battle Song, Diamond Blade, Space Storage
Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Manc0
Female
Aquarius
48
50
Time Mage
White Magic
Counter Magic
Halve MP
Teleport

Gokuu Rod

Black Hood
Black Robe
Elf Mantle

Haste, Haste 2, Slow 2, Immobilize, Float, Quick, Demi, Demi 2, Stabilize Time, Meteor
Cure 3, Raise, Raise 2, Regen, Shell, Shell 2, Wall, Esuna



Benticore
Female
Taurus
66
50
Samurai
Black Magic
Auto Potion
Concentrate
Jump+3

Asura Knife

Barbuta
Reflect Mail
Defense Ring

Kiyomori, Kikuichimoji
Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 4, Empower, Frog



Powergems
Monster
Aries
58
50
Ultima Demon








Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CorpusCav
Male
Virgo
59
49
Time Mage
Steal
Counter Magic
Halve MP
Move+3

White Staff

Red Hood
Silk Robe
Genji Gauntlet

Haste, Haste 2, Stop, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim



Jakeduhhsnake
Female
Serpentarius
80
55
Samurai
White Magic
Arrow Guard
Equip Shield
Lava Walking

Murasame
Mythril Shield
Barbuta
Diamond Armor
Diamond Armlet

Koutetsu, Bizen Boat, Murasame, Muramasa
Cure 4, Raise 2, Protect, Protect 2, Shell, Esuna



Vampire Killer
Female
Gemini
76
46
Calculator
Ranch Skill
Brave Save
Equip Polearm
Move+1

Gokuu Rod

Twist Headband
Maximillian
Angel Ring

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor



Lolprinze
Male
Virgo
48
76
Monk
Jump
Brave Save
Doublehand
Move+2



Barette
Adaman Vest
Rubber Shoes

Pummel, Earth Slash, Purification, Revive
Level Jump8, Vertical Jump6

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



AmaninAmide
Female
Gemini
53
54
Knight
Yin Yang Magic
Auto Potion
Equip Bow
Jump+3

Ultimus Bow
Ice Shield
Leather Helmet
Silk Robe
Magic Ring

Armor Break, Shield Break, Mind Break, Explosion Sword
Spell Absorb, Life Drain, Pray Faith, Blind Rage, Foxbird, Paralyze



ANFz
Female
Scorpio
46
52
Wizard
Battle Skill
Dragon Spirit
Concentrate
Waterwalking

Main Gauche

Feather Hat
Black Costume
Genji Gauntlet

Fire 2, Fire 3, Fire 4, Ice 3, Ice 4, Frog
Head Break, Shield Break, Weapon Break, Speed Break, Justice Sword



TheTemperedChocobo
Male
Capricorn
71
68
Monk
Summon Magic
Counter Magic
Equip Polearm
Teleport

Gokuu Rod

Barette
Black Costume
Elf Mantle

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Leviathan, Cyclops



Dellusionsx
Male
Aquarius
47
51
Mediator
Battle Skill
Damage Split
Equip Axe
Jump+2

Healing Staff

Feather Hat
Brigandine
Jade Armlet

Solution, Negotiate, Refute
Stasis Sword

Player: !Red
Team: Red Team
Palettes: Red/Brown



CapnChaos12
Male
Scorpio
64
70
Mime

Abandon
Attack UP
Jump+3



Circlet
Black Costume
N-Kai Armlet

Mimic




Evontno
Male
Aquarius
50
79
Monk
Throw
Mana Shield
Secret Hunt
Move+2



Red Hood
Power Sleeve
Genji Gauntlet

Pummel, Earth Slash, Purification, Revive
Shuriken



TheMM42
Female
Aries
57
59
Priest
Elemental
Sunken State
Attack UP
Fly

Flail

Golden Hairpin
Silk Robe
Sprint Shoes

Cure 4, Reraise, Esuna
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Vorap
Female
Aquarius
64
64
Priest
Jump
Absorb Used MP
Doublehand
Move+3

Oak Staff

Green Beret
Mystic Vest
Diamond Armlet

Cure, Cure 2, Cure 3, Cure 4, Reraise, Regen, Shell 2, Esuna
Level Jump4, Vertical Jump8

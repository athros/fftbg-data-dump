Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powerpinch
Male
Libra
39
71
Mime

Distribute
Monster Talk
Fly



Leather Hat
Mystic Vest
Small Mantle

Mimic




Kliffw
Male
Cancer
43
72
Priest
Yin Yang Magic
Absorb Used MP
Defend
Move+1

White Staff

Red Hood
Silk Robe
Genji Gauntlet

Cure, Cure 2, Raise, Reraise, Regen, Protect 2, Wall, Esuna
Poison, Life Drain, Doubt Faith, Foxbird, Confusion Song



Roqqqpsi
Female
Sagittarius
70
42
Thief
Item
Distribute
Short Status
Waterbreathing

Cultist Dagger

Feather Hat
Black Costume
Feather Mantle

Steal Heart
Potion, Holy Water, Remedy, Phoenix Down



Zmoses
Female
Gemini
57
76
Knight
Summon Magic
Regenerator
Magic Attack UP
Move+2

Defender
Venetian Shield
Bronze Helmet
Plate Mail
Bracer

Head Break, Armor Break, Shield Break, Magic Break, Mind Break, Justice Sword
Moogle, Titan, Carbunkle

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Twelfthrootoftwo
Female
Leo
63
46
Geomancer
Steal
Catch
Concentrate
Levitate

Kikuichimoji
Platinum Shield
Barette
White Robe
Genji Gauntlet

Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind
Gil Taking, Steal Accessory, Steal Status



Quadh0nk
Male
Gemini
48
80
Knight
Throw
Regenerator
Equip Sword
Fly

Masamune
Round Shield
Gold Helmet
Reflect Mail
Jade Armlet

Speed Break
Shuriken, Sword



Evewho
Female
Cancer
62
58
Chemist
Throw
Speed Save
Concentrate
Move-HP Up

Cute Bag

Twist Headband
Secret Clothes
Red Shoes

Potion, Ether, Antidote, Remedy, Phoenix Down
Bomb, Ninja Sword



Lowlf
Female
Aquarius
54
48
Samurai
Black Magic
HP Restore
Maintenance
Move-HP Up

Star Bag

Diamond Helmet
Crystal Mail
Genji Gauntlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Frog, Death

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Prince Rogers Nelson
Female
Taurus
43
50
Ninja
Punch Art
Earplug
Attack UP
Move+2

Ninja Edge
Short Edge
Twist Headband
Mythril Vest
Dracula Mantle

Shuriken
Secret Fist, Purification



CT 5 Holy
Male
Cancer
49
72
Chemist
Battle Skill
Damage Split
Dual Wield
Lava Walking

Hydra Bag
Hydra Bag
Green Beret
Judo Outfit
Red Shoes

Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Dark Sword, Night Sword



DamnThatShark
Male
Cancer
63
79
Monk
White Magic
Hamedo
Equip Polearm
Retreat

Ryozan Silk

Triangle Hat
Clothes
Red Shoes

Pummel, Purification, Chakra, Revive
Cure, Raise, Shell, Shell 2, Esuna, Holy



WitchHunterIX
Male
Leo
65
53
Squire
Elemental
Counter
Attack UP
Jump+1

Scorpion Tail
Bronze Shield
Leather Hat
Leather Armor
Reflect Ring

Heal, Tickle, Wish, Scream
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball

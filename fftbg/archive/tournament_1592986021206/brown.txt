Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ParadiseNinja
Female
Capricorn
73
55
Summoner
Steal
Speed Save
Equip Polearm
Ignore Height

Octagon Rod

Holy Miter
Brigandine
Power Wrist

Moogle, Shiva, Titan, Fairy
Gil Taking, Steal Shield, Steal Accessory, Arm Aim



DuraiPapers
Male
Gemini
44
52
Ninja
Black Magic
Blade Grasp
Concentrate
Swim

Morning Star
Flame Whip
Feather Hat
Clothes
Dracula Mantle

Shuriken, Bomb
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Flare



Grininda
Male
Gemini
67
44
Ninja
Jump
MA Save
Equip Polearm
Levitate

Obelisk
Partisan
Black Hood
Clothes
Battle Boots

Shuriken, Ninja Sword
Level Jump4, Vertical Jump6



Sinnyil2
Male
Capricorn
68
58
Lancer
Summon Magic
Auto Potion
Equip Sword
Move-MP Up

Defender

Cross Helmet
Diamond Armor
Spike Shoes

Level Jump8, Vertical Jump8
Moogle, Shiva, Titan, Carbunkle, Fairy, Zodiac

Player: !Red
Team: Red Team
Palettes: Red/Brown



Maleblackfiora
Female
Aquarius
61
73
Archer
Draw Out
Counter Tackle
Maintenance
Swim

Romanda Gun
Diamond Shield
Twist Headband
Judo Outfit
Power Wrist

Charge+1, Charge+2, Charge+3
Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji



Actual JP
Male
Capricorn
77
53
Knight
Talk Skill
Auto Potion
Doublehand
Retreat

Iron Sword

Gold Helmet
Wizard Robe
Small Mantle

Armor Break, Shield Break, Power Break, Justice Sword
Persuade, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



Error72
Male
Capricorn
60
53
Monk
Basic Skill
HP Restore
Equip Gun
Lava Walking

Bestiary

Thief Hat
Power Sleeve
Red Shoes

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Accumulate, Yell, Cheer Up



NovaKnight21
Male
Aries
47
63
Thief
White Magic
Counter Magic
Equip Armor
Swim

Assassin Dagger

Cross Helmet
Linen Cuirass
Red Shoes

Steal Helmet, Steal Shield, Arm Aim
Cure 2, Raise, Regen, Protect, Shell 2, Esuna

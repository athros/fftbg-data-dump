Player: !Brown
Team: Brown Team
Palettes: Brown/Green



IkariAllen
Male
Virgo
42
66
Squire
Talk Skill
HP Restore
Monster Talk
Ignore Height

Blind Knife
Round Shield
Gold Helmet
Earth Clothes
Feather Boots

Accumulate, Tickle
Negotiate, Refute



Diablos24
Female
Cancer
49
45
Ninja
Talk Skill
Distribute
Defense UP
Retreat

Dagger
Spell Edge
Green Beret
Judo Outfit
Feather Mantle

Shuriken, Hammer, Ninja Sword
Persuade, Death Sentence, Refute, Rehabilitate



Biske13
Female
Libra
60
50
Geomancer
Draw Out
Arrow Guard
Short Charge
Fly

Rune Blade
Bronze Shield
Triangle Hat
White Robe
Feather Boots

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Murasame, Kiyomori



TheKillerNacho
Female
Virgo
66
67
Priest
Draw Out
Counter
Equip Axe
Retreat

Healing Staff

Red Hood
Black Costume
Small Mantle

Cure, Cure 3, Raise, Reraise, Protect 2, Shell, Esuna, Holy
Kikuichimoji

Final Bets: white - 7 bets for 3,178G (35.9%, x1.78); black - 9 bets for 5,666G (64.1%, x0.56)

white bets:
killth3kid: 1,000G (31.5%, 105,671G)
Firesheath: 972G (30.6%, 972G)
ColetteMSLP: 400G (12.6%, 2,973G)
Digitalsocrates: 356G (11.2%, 356G)
Lythe_Caraker: 250G (7.9%, 145,605G)
enkikavlar: 100G (3.1%, 1,335G)
Ring_Wyrm: 100G (3.1%, 8,192G)

black bets:
Mysteriousdewd: 1,500G (26.5%, 3,110G)
Evewho: 1,309G (23.1%, 1,309G)
BirbBrainsBot: 1,000G (17.6%, 47,594G)
DamnThatShark: 500G (8.8%, 1,955G)
phrossi_v: 360G (6.4%, 3,013G)
getthemoneyz: 348G (6.1%, 1,189,612G)
safira125: 252G (4.4%, 252G)
ar_tactic: 200G (3.5%, 65,671G)
superdevon1: 197G (3.5%, 1,975G)

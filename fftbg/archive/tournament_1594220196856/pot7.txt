Final Bets: blue - 12 bets for 5,905G (44.7%, x1.24); black - 10 bets for 7,312G (55.3%, x0.81)

blue bets:
winteriscorning: 1,368G (23.2%, 1,368G)
killth3kid: 1,000G (16.9%, 99,565G)
BirbBrainsBot: 1,000G (16.9%, 149,491G)
ShintaroNayaka: 800G (13.5%, 8,393G)
Grandlanzer: 300G (5.1%, 62,520G)
ArashiKurobara: 296G (5.0%, 7,437G)
Forkmore: 272G (4.6%, 2,809G)
SephDarkheart: 248G (4.2%, 248G)
gorgewall: 201G (3.4%, 14,938G)
datadrivenbot: 200G (3.4%, 61,124G)
getthemoneyz: 120G (2.0%, 1,223,837G)
CosmicTactician: 100G (1.7%, 44,734G)

black bets:
dogsandcatsand: 1,494G (20.4%, 2,931G)
fenaen: 1,200G (16.4%, 19,542G)
TheChainNerd: 1,000G (13.7%, 30,478G)
Zachara: 998G (13.6%, 131,498G)
JethroThrul: 976G (13.3%, 976G)
brokenknight201: 500G (6.8%, 1,343G)
Phi_Sig: 416G (5.7%, 416G)
Zetchryn: 408G (5.6%, 408G)
randgridr: 220G (3.0%, 220G)
DeathTaxesAndAnime: 100G (1.4%, 3,025G)

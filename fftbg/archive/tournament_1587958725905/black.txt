Player: !Black
Team: Black Team
Palettes: Black/Red



HaateXIII
Male
Aries
58
50
Archer
Black Magic
Sunken State
Doublehand
Ignore Terrain

Blast Gun

Holy Miter
Secret Clothes
Chantage

Charge+2
Fire, Fire 2, Fire 4, Ice 2, Ice 4, Death



Evewho
Female
Sagittarius
80
41
Archer
Summon Magic
Meatbone Slash
Doublehand
Jump+1

Blaze Gun

Feather Hat
Wizard Outfit
Feather Mantle

Charge+1
Moogle, Titan, Golem, Carbunkle, Odin



ProdigyAzn
Male
Gemini
55
60
Chemist
White Magic
Speed Save
Short Charge
Swim

Hydra Bag

Leather Hat
Judo Outfit
Germinas Boots

Potion, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down
Cure 3, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna



LeepingJJ
Male
Scorpio
68
71
Calculator
Time Magic
Abandon
Defend
Ignore Height

Star Bag

Thief Hat
Black Robe
Salty Rage

CT, Height, 5
Haste, Stop, Immobilize, Quick, Demi, Demi 2, Stabilize Time

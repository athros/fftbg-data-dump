Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sukotsuto
Female
Aquarius
53
69
Monk
Summon Magic
Speed Save
Equip Knife
Move-HP Up

Koga Knife

Feather Hat
Black Costume
Power Wrist

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Shiva, Golem, Carbunkle, Leviathan, Lich



0utlier
Monster
Leo
68
79
Explosive










Gooseyourself
Female
Taurus
72
56
Mediator
Steal
Hamedo
Maintenance
Swim

Cultist Dagger

Red Hood
Light Robe
Bracer

Persuade, Insult, Mimic Daravon, Refute
Steal Armor, Steal Accessory



Jakeduhhsnake
Male
Virgo
38
80
Oracle
Throw
Caution
Equip Bow
Retreat

Ice Bow

Holy Miter
Mystic Vest
Dracula Mantle

Blind, Spell Absorb, Doubt Faith, Blind Rage, Dispel Magic, Petrify
Shuriken, Bomb, Knife, Sword, Wand, Dictionary

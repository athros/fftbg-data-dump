Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



ANFz
Female
Capricorn
56
72
Mime

Distribute
Equip Shield
Teleport


Aegis Shield
Triangle Hat
Adaman Vest
Salty Rage

Mimic




ZCKaiser
Male
Leo
49
52
Lancer
Yin Yang Magic
PA Save
Short Status
Ignore Height

Gungnir
Platinum Shield
Circlet
Gold Armor
Spike Shoes

Level Jump8, Vertical Jump8
Spell Absorb, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze



Gondolacard
Male
Sagittarius
80
65
Squire
Yin Yang Magic
Dragon Spirit
Attack UP
Move-HP Up

Gastrafitis
Mythril Shield
Feather Hat
Power Sleeve
Rubber Shoes

Accumulate, Heal, Yell, Cheer Up, Fury, Wish
Poison, Doubt Faith, Dispel Magic, Petrify



Actual JP
Male
Serpentarius
63
55
Summoner
Black Magic
Damage Split
Short Charge
Jump+2

Bestiary

Black Hood
White Robe
108 Gems

Moogle, Shiva, Golem, Bahamut, Leviathan, Fairy, Lich
Fire 2, Bolt 4, Ice 2, Ice 4

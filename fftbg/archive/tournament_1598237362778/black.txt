Player: !Black
Team: Black Team
Palettes: Black/Red



Error72
Male
Aquarius
44
52
Knight
Elemental
Parry
Dual Wield
Move+3

Iron Sword
Slasher
Barbuta
White Robe
Defense Armlet

Armor Break, Shield Break, Weapon Break, Stasis Sword, Justice Sword, Surging Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Gusty Wind, Lava Ball



DejaPoo21
Female
Gemini
58
57
Oracle
Time Magic
Brave Save
Equip Sword
Jump+2

Ancient Sword

Ribbon
Chain Vest
Defense Armlet

Blind, Foxbird, Dispel Magic
Haste, Immobilize, Demi, Stabilize Time



Evdoggity
Male
Taurus
56
54
Geomancer
Summon Magic
PA Save
Beastmaster
Teleport

Kiyomori
Bronze Shield
Feather Hat
Light Robe
Battle Boots

Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Ifrit, Carbunkle, Silf



Ruleof5
Female
Aquarius
76
65
Archer
Talk Skill
Counter Magic
Short Status
Swim

Silver Bow

Black Hood
Chain Vest
Elf Mantle

Charge+4, Charge+5, Charge+10
Invitation, Threaten, Preach, Refute, Rehabilitate

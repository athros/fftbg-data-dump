Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Laserman1000
Male
Aries
61
73
Lancer
Item
Mana Shield
Doublehand
Waterwalking

Holy Lance

Bronze Helmet
Bronze Armor
Defense Ring

Level Jump4, Vertical Jump5
Potion, Antidote, Phoenix Down



DeliciousSub
Female
Aries
55
52
Chemist
Dance
Dragon Spirit
Secret Hunt
Swim

Panther Bag

Holy Miter
Earth Clothes
Dracula Mantle

Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Disillusion, Dragon Pit



Omegasuspekt
Male
Aries
75
58
Ninja
Battle Skill
Sunken State
Equip Armor
Move+1

Main Gauche
Orichalcum
Iron Helmet
Gold Armor
Power Wrist

Shuriken, Knife, Axe, Spear
Speed Break, Power Break, Justice Sword, Night Sword



AdmiralPikachu
Male
Libra
61
56
Mediator
Elemental
Regenerator
Dual Wield
Waterwalking

Mythril Gun
Blaze Gun
Thief Hat
Brigandine
Small Mantle

Persuade, Death Sentence, Negotiate, Refute
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

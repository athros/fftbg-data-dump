Player: !Green
Team: Green Team
Palettes: Green/White



Alacor
Male
Cancer
69
39
Knight
White Magic
Auto Potion
Short Charge
Ignore Terrain

Slasher
Genji Shield
Leather Helmet
Wizard Robe
N-Kai Armlet

Speed Break, Power Break, Justice Sword
Cure 2, Raise, Protect, Shell 2, Esuna



Pandasforsale
Female
Pisces
71
64
Lancer
Black Magic
PA Save
Magic Defense UP
Move+3

Javelin
Ice Shield
Diamond Helmet
Linen Cuirass
Leather Mantle

Level Jump2, Vertical Jump7
Ice, Ice 2, Ice 3, Flare



Oreo Pizza
Male
Aquarius
60
64
Chemist
Charge
PA Save
Equip Gun
Teleport

Papyrus Codex

Black Hood
Mythril Vest
N-Kai Armlet

Potion, Antidote, Eye Drop, Holy Water, Phoenix Down
Charge+2, Charge+10



Phrossi V
Male
Aries
39
67
Oracle
Elemental
Absorb Used MP
Defense UP
Jump+3

Octagon Rod

Flash Hat
Wizard Outfit
Jade Armlet

Blind, Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Confusion Song, Sleep
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

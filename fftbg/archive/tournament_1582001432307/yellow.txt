Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Evontno
Female
Capricorn
62
55
Thief
Dance
Counter Tackle
Defense UP
Waterwalking

Air Knife

Triangle Hat
Judo Outfit
Magic Gauntlet

Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Dragon Pit



Reinoe
Female
Capricorn
40
55
Geomancer
Steal
MA Save
Equip Gun
Move+2

Battle Folio
Buckler
Red Hood
Rubber Costume
Angel Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Lava Ball
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Arm Aim, Leg Aim



ThePanda1016
Male
Scorpio
75
56
Ninja
Jump
Absorb Used MP
Equip Bow
Move-MP Up

Silver Bow

Black Hood
Clothes
Feather Mantle

Bomb, Knife, Axe
Level Jump5, Vertical Jump7



RongRongArts
Female
Leo
47
55
Time Mage
Charge
MA Save
Doublehand
Move+1

Gold Staff

Triangle Hat
Linen Robe
N-Kai Armlet

Slow 2, Float, Reflect, Demi 2
Charge+5, Charge+7

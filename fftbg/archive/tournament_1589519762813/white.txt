Player: !White
Team: White Team
Palettes: White/Blue



Jakeduhhsnake
Monster
Leo
52
63
Trent










Omegasuspekt
Male
Aries
67
48
Samurai
Sing
Earplug
Equip Shield
Swim

Kiyomori
Kaiser Plate
Genji Helmet
Platinum Armor
Setiemson

Asura, Bizen Boat, Heaven's Cloud, Kiyomori
Battle Song, Nameless Song, Diamond Blade, Sky Demon



CapnChaos12
Female
Aries
54
70
Ninja
Item
Regenerator
Equip Gun
Move+3

Battle Folio
Bestiary
Thief Hat
Mythril Vest
Angel Ring

Shuriken, Staff
Potion, X-Potion, Soft, Phoenix Down



OneHundredFists
Male
Aries
71
45
Bard
Jump
HP Restore
Dual Wield
Levitate

Fairy Harp
Ramia Harp
Twist Headband
Genji Armor
Chantage

Space Storage
Level Jump8, Vertical Jump4

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



CorpusCav
Female
Capricorn
75
70
Chemist
Draw Out
Counter
Equip Shield
Waterbreathing

Hydra Bag
Buckler
Golden Hairpin
Wizard Outfit
Bracer

Potion, X-Potion, Ether, Elixir, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Heaven's Cloud, Kiyomori, Muramasa



Oreo Pizza
Female
Virgo
73
42
Lancer
Talk Skill
Counter
Monster Talk
Waterbreathing

Mythril Spear
Aegis Shield
Leather Helmet
Plate Mail
Bracer

Level Jump8, Vertical Jump2
Preach, Death Sentence, Insult, Mimic Daravon, Rehabilitate



Gooseyourself
Monster
Cancer
60
58
Wyvern










Puraisu
Female
Capricorn
41
44
Samurai
Jump
Catch
Secret Hunt
Jump+2

Kikuichimoji

Mythril Helmet
Platinum Armor
Feather Boots

Koutetsu, Murasame, Muramasa, Kikuichimoji
Level Jump2, Vertical Jump6

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



NovaKnight21
Male
Gemini
79
53
Ninja
Item
Auto Potion
Equip Armor
Lava Walking

Main Gauche
Dagger
Gold Helmet
Gold Armor
Defense Armlet

Knife, Hammer
Potion, X-Potion, Antidote, Soft



Zenlion
Male
Virgo
78
51
Mime

Counter Magic
Attack UP
Lava Walking



Thief Hat
Mystic Vest
Feather Boots

Mimic




Furorcelt
Male
Aquarius
70
41
Ninja
White Magic
Counter Magic
Magic Defense UP
Levitate

Flame Whip
Flail
Headgear
Mystic Vest
Leather Mantle

Axe, Spear, Dictionary
Raise, Reraise, Regen, Protect, Esuna



FroDog5050
Female
Pisces
50
46
Geomancer
Summon Magic
Damage Split
Concentrate
Move-HP Up

Giant Axe
Mythril Shield
Ribbon
Wizard Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Gusty Wind
Moogle, Ramuh, Golem, Leviathan

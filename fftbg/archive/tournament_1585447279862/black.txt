Player: !Black
Team: Black Team
Palettes: Black/Red



DeliciousSub
Female
Capricorn
74
71
Mime

Counter
Defend
Lava Walking



Black Hood
Gold Armor
Genji Gauntlet

Mimic




RughSontos
Male
Scorpio
78
66
Squire
Steal
Earplug
Beastmaster
Move+3

Air Knife
Bronze Shield
Holy Miter
Brigandine
Defense Ring

Dash, Heal, Fury
Steal Status



Lodrak
Male
Virgo
49
76
Knight
Item
Distribute
Throw Item
Move+3

Defender
Buckler
Cross Helmet
Crystal Mail
Sprint Shoes

Armor Break, Shield Break, Weapon Break, Speed Break, Justice Sword
Potion, Antidote, Eye Drop, Holy Water, Phoenix Down



Volgrathemoose
Female
Libra
73
59
Time Mage
Charge
Parry
Dual Wield
Ignore Terrain

Bestiary
Bestiary
Golden Hairpin
Wizard Robe
Feather Boots

Haste, Haste 2, Slow 2, Reflect, Demi, Meteor
Charge+1, Charge+3

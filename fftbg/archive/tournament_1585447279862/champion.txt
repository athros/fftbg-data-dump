Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Evewho
Female
Leo
76
74
Ninja
Punch Art
Parry
Equip Gun
Move-MP Up

Bestiary
Papyrus Codex
Leather Hat
Wizard Outfit
Cursed Ring

Shuriken, Wand
Spin Fist, Secret Fist, Purification



Darkp Ramza
Male
Cancer
60
71
Monk
Item
PA Save
Concentrate
Jump+3



Twist Headband
Power Sleeve
Germinas Boots

Spin Fist, Earth Slash, Purification, Chakra
X-Potion, Ether, Hi-Ether, Echo Grass, Holy Water



HaplessOne
Male
Taurus
41
64
Knight
Punch Art
Counter
Magic Attack UP
Move+3

Blood Sword
Bronze Shield
Barbuta
Black Robe
Battle Boots

Head Break, Armor Break, Shield Break, Speed Break, Dark Sword, Explosion Sword
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil



Robespyah
Male
Taurus
55
63
Mediator
White Magic
Caution
Equip Axe
Jump+3

Flame Whip

Twist Headband
Light Robe
Leather Mantle

Preach, Mimic Daravon, Refute, Rehabilitate
Cure 3, Raise, Protect, Shell 2, Esuna

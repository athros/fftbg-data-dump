Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Laserman1000
Male
Capricorn
75
70
Geomancer
Item
Counter Flood
Equip Polearm
Move-HP Up

Cypress Rod
Flame Shield
Headgear
Power Sleeve
Defense Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, X-Potion, Eye Drop, Phoenix Down



Estan AD
Female
Scorpio
66
55
Chemist
Draw Out
Distribute
Equip Polearm
Swim

Ryozan Silk

Black Hood
Leather Outfit
Red Shoes

X-Potion, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Asura, Kiyomori



Powergems
Female
Aquarius
42
75
Summoner
Charge
Sunken State
Short Charge
Move+1

Bestiary

Twist Headband
Chameleon Robe
Feather Boots

Ifrit, Titan, Golem, Carbunkle, Odin, Salamander, Fairy, Lich
Charge+2, Charge+3, Charge+5, Charge+10



Interalia
Male
Gemini
48
43
Chemist
Steal
Caution
Halve MP
Ignore Height

Panther Bag

Leather Hat
Mythril Vest
Defense Armlet

Potion, X-Potion, Antidote, Eye Drop, Holy Water, Phoenix Down
Steal Helmet, Steal Shield, Steal Accessory, Arm Aim, Leg Aim

Player: !Green
Team: Green Team
Palettes: Green/White



Kronikle
Male
Aquarius
63
77
Mime

Speed Save
Martial Arts
Move+2



Leather Hat
Mythril Vest
Wizard Mantle

Mimic




Strumisgod
Male
Libra
73
53
Priest
Time Magic
Brave Save
Doublehand
Move+3

Morning Star

Red Hood
Wizard Robe
108 Gems

Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect, Protect 2, Shell, Esuna
Immobilize, Float, Reflect, Demi 2



MonchoStrife
Female
Pisces
43
75
Samurai
Dance
Counter Magic
Attack UP
Move+2

Kikuichimoji

Barbuta
Maximillian
Wizard Mantle

Koutetsu, Murasame
Polka Polka, Disillusion, Nameless Dance, Last Dance



Resjudicata3
Male
Aries
42
56
Lancer
Steal
Auto Potion
Magic Attack UP
Levitate

Javelin
Buckler
Diamond Helmet
Genji Armor
Jade Armlet

Level Jump5, Vertical Jump5
Steal Weapon, Steal Accessory, Arm Aim

Final Bets: red - 18 bets for 10,174G (35.4%, x1.82); green - 24 bets for 18,551G (64.6%, x0.55)

red bets:
Kalabain: 2,000G (19.7%, 46,941G)
RunicMagus: 1,600G (15.7%, 49,328G)
DeathTaxesAndAnime: 1,447G (14.2%, 2,838G)
TheChainNerd: 995G (9.8%, 995G)
joewcarson: 500G (4.9%, 53,569G)
Evewho: 500G (4.9%, 18,469G)
Bilabrin: 489G (4.8%, 489G)
HASTERIOUS: 488G (4.8%, 488G)
roofiepops: 407G (4.0%, 407G)
electric_algus: 400G (3.9%, 13,339G)
dogsandcatsand: 332G (3.3%, 332G)
getthemoneyz: 318G (3.1%, 898,797G)
AllInBot: 215G (2.1%, 215G)
cougboi: 160G (1.6%, 18,491G)
MMagnetMan: 116G (1.1%, 116G)
nhammen: 100G (1.0%, 953G)
datadrivenbot: 100G (1.0%, 35,775G)
J_MacC1991: 7G (0.1%, 158G)

green bets:
VolgraTheMoose: 5,001G (27.0%, 43,611G)
Mesmaster: 2,000G (10.8%, 11,312G)
OneHundredFists: 2,000G (10.8%, 25,455G)
Lydian_C: 1,200G (6.5%, 9,006G)
BirbBrainsBot: 1,000G (5.4%, 98,989G)
GeeGXP: 803G (4.3%, 803G)
Sairentozon7: 769G (4.1%, 769G)
HaateXIII: 712G (3.8%, 712G)
aeonicvector: 577G (3.1%, 577G)
Cryptopsy70: 520G (2.8%, 10,337G)
CosmicTactician: 500G (2.7%, 28,780G)
Riot_Jayway: 500G (2.7%, 230,801G)
CaptainGarlock: 497G (2.7%, 497G)
mpghappiness: 480G (2.6%, 480G)
cam_ATS: 404G (2.2%, 404G)
twelfthrootoftwo: 382G (2.1%, 382G)
bruubarg: 356G (1.9%, 356G)
Creggers: 250G (1.3%, 2,739G)
E_Ballard: 100G (0.5%, 4,931G)
nifboy: 100G (0.5%, 3,143G)
Miku_Shikhu: 100G (0.5%, 17,045G)
Digitalsocrates: 100G (0.5%, 4,209G)
CorpusCav: 100G (0.5%, 2,836G)
Grandlanzer: 100G (0.5%, 4,356G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rockmem21
Male
Aries
55
50
Geomancer
Draw Out
Caution
Magic Defense UP
Move-MP Up

Heaven's Cloud
Flame Shield
Barette
Judo Outfit
Small Mantle

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Asura, Koutetsu



RaIshtar
Female
Aries
61
62
Priest
Battle Skill
Counter Flood
Equip Sword
Fly

Excalibur

Triangle Hat
Robe of Lords
Jade Armlet

Cure, Cure 2, Raise, Protect, Shell 2, Esuna
Mind Break, Justice Sword



Grandlanzer
Female
Aquarius
77
43
Wizard
Summon Magic
Mana Shield
Short Status
Jump+1

Thunder Rod

Flash Hat
Linen Robe
Diamond Armlet

Fire, Bolt 4, Ice 2, Empower, Frog
Moogle, Shiva, Titan



Laserman1000
Male
Taurus
75
49
Monk
Draw Out
Distribute
Equip Gun
Jump+3

Blast Gun

Triangle Hat
Mythril Vest
Feather Boots

Pummel, Secret Fist, Purification, Chakra
Asura, Murasame, Kiyomori

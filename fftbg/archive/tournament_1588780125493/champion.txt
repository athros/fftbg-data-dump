Player: !zChamp
Team: Champion Team
Palettes: White/Blue



SaintOmerville
Male
Cancer
56
50
Squire
White Magic
Caution
Equip Bow
Move+1

Cross Bow
Bronze Shield
Circlet
Adaman Vest
Defense Armlet

Accumulate, Dash, Heal, Tickle, Yell, Wish
Cure 3, Cure 4, Raise, Reraise, Regen, Esuna, Magic Barrier



Wizblizz
Male
Virgo
61
72
Archer
White Magic
MA Save
Doublehand
Move+3

Perseus Bow

Platinum Helmet
Clothes
Battle Boots

Charge+1, Charge+3, Charge+7
Cure, Raise, Raise 2, Wall, Esuna, Holy



DrAntiSocial
Male
Scorpio
70
62
Chemist
Talk Skill
Arrow Guard
Attack UP
Move+1

Glacier Gun

Holy Miter
Black Costume
Setiemson

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Elixir, Antidote, Eye Drop, Maiden's Kiss
Negotiate, Rehabilitate



PatSouI
Male
Capricorn
47
68
Lancer
Draw Out
Critical Quick
Dual Wield
Move-HP Up

Obelisk
Cypress Rod
Circlet
Gold Armor
Setiemson

Level Jump8, Vertical Jump7
Koutetsu, Bizen Boat, Kikuichimoji

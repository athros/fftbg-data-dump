Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DudeMonkey77
Female
Aquarius
76
39
Dancer
Draw Out
Earplug
Dual Wield
Waterwalking

Ryozan Silk
Ryozan Silk
Golden Hairpin
Leather Outfit
Spike Shoes

Witch Hunt, Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Void Storage
Asura, Kiyomori, Chirijiraden



Proper Noun
Female
Scorpio
73
50
Dancer
Item
Distribute
Throw Item
Waterwalking

Ryozan Silk

Holy Miter
Earth Clothes
Battle Boots

Disillusion, Nameless Dance
Potion, Antidote, Remedy, Phoenix Down



Striator
Female
Sagittarius
52
70
Samurai
Charge
Parry
Equip Sword
Move+3

Koutetsu Knife

Leather Helmet
Wizard Robe
Reflect Ring

Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Charge+1, Charge+2, Charge+3, Charge+4, Charge+7



SaintOmerville
Female
Aquarius
44
78
Time Mage
Charge
Distribute
Martial Arts
Lava Walking

Bestiary

Flash Hat
Light Robe
Wizard Mantle

Haste 2, Slow, Slow 2, Stop, Float, Demi, Stabilize Time
Charge+1

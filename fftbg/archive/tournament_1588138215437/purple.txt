Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesauceBoss
Female
Sagittarius
77
55
Samurai
Charge
Parry
Equip Sword
Fly

Kikuichimoji

Genji Helmet
Bronze Armor
Bracer

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Charge+2, Charge+10



Gorgewall
Male
Capricorn
59
57
Bard
Basic Skill
MP Restore
Sicken
Move-MP Up

Fairy Harp

Triangle Hat
Chain Mail
Setiemson

Angel Song, Cheer Song, Nameless Song, Diamond Blade, Sky Demon
Accumulate, Dash, Throw Stone, Heal, Wish



Lijarkh
Female
Aries
46
56
Monk
Draw Out
Mana Shield
Concentrate
Lava Walking



Black Hood
Adaman Vest
Magic Gauntlet

Spin Fist, Earth Slash, Purification, Revive
Bizen Boat, Kikuichimoji



Reinoe
Monster
Scorpio
75
50
Apanda








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zmoses
Female
Scorpio
64
49
Chemist
White Magic
PA Save
Long Status
Ignore Height

Dagger

Headgear
Black Costume
Spike Shoes

Potion, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down
Cure 2, Cure 3, Cure 4, Reraise, Shell, Esuna



Bryan792
Male
Cancer
57
59
Archer
Talk Skill
Damage Split
Attack UP
Waterwalking

Night Killer
Aegis Shield
Black Hood
Wizard Outfit
Feather Mantle

Charge+7, Charge+10
Persuade, Praise, Solution, Insult, Negotiate



Striator
Female
Sagittarius
49
59
Lancer
Yin Yang Magic
Mana Shield
Dual Wield
Jump+2

Spear
Octagon Rod
Cross Helmet
Plate Mail
Sprint Shoes

Level Jump2, Vertical Jump8
Blind, Zombie, Foxbird, Dispel Magic



Eco1327
Male
Cancer
40
77
Monk
Talk Skill
Absorb Used MP
Equip Axe
Jump+2

Oak Staff

Triangle Hat
Black Costume
Magic Gauntlet

Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Praise, Threaten, Refute

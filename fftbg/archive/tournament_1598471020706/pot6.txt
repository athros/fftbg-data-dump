Final Bets: white - 11 bets for 5,631G (37.2%, x1.69); brown - 6 bets for 9,525G (62.8%, x0.59)

white bets:
toka222: 1,500G (26.6%, 272,181G)
Forkmore: 1,000G (17.8%, 14,502G)
BirbBrainsBot: 1,000G (17.8%, 46,205G)
dogsandcatsand: 650G (11.5%, 650G)
Sairentozon7: 432G (7.7%, 432G)
getthemoneyz: 292G (5.2%, 1,747,439G)
AllInBot: 229G (4.1%, 229G)
datadrivenbot: 200G (3.6%, 63,675G)
mattcorn7: 128G (2.3%, 128G)
IndecisiveNinja: 100G (1.8%, 8,270G)
AbandonedHall: 100G (1.8%, 2,037G)

brown bets:
Zeroroute: 5,438G (57.1%, 17,542G)
DustBirdEX: 1,569G (16.5%, 1,569G)
reinoe: 1,000G (10.5%, 87,526G)
thunderducker: 994G (10.4%, 994G)
lowlf: 404G (4.2%, 10,299G)
koeeh: 120G (1.3%, 120G)

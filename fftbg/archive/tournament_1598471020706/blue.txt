Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WhiteTigress
Male
Capricorn
60
51
Lancer
Talk Skill
Damage Split
Monster Talk
Move+2

Javelin
Crystal Shield
Gold Helmet
Linen Cuirass
Sprint Shoes

Level Jump4, Vertical Jump6
Persuade, Insult, Negotiate, Refute



FriendSquirrel
Male
Libra
51
76
Lancer
Punch Art
Counter
Dual Wield
Move+3

Iron Fan
Musk Rod
Mythril Helmet
Carabini Mail
Dracula Mantle

Level Jump2, Vertical Jump8
Wave Fist, Earth Slash, Secret Fist, Purification, Revive



LASERJESUS1337
Female
Libra
78
65
Lancer
Punch Art
HP Restore
Beastmaster
Move+2

Musk Rod
Bronze Shield
Leather Helmet
Gold Armor
Bracer

Level Jump8, Vertical Jump8
Revive, Seal Evil



Reinoe
Female
Scorpio
76
79
Summoner
Elemental
Faith Save
Beastmaster
Teleport

Flame Rod

Headgear
Chameleon Robe
Wizard Mantle

Moogle, Ramuh, Leviathan, Silf, Lich
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !Black
Team: Black Team
Palettes: Black/Red



Thaetreis
Female
Libra
78
45
Chemist
Draw Out
Dragon Spirit
Dual Wield
Fly

Assassin Dagger
Hydra Bag
Feather Hat
Black Costume
Spike Shoes

Potion, Hi-Potion, X-Potion, Echo Grass, Soft, Remedy, Phoenix Down
Asura, Koutetsu, Bizen Boat, Heaven's Cloud



Brokenknight201
Female
Scorpio
72
70
Mime

Counter Magic
Martial Arts
Move+2



Thief Hat
Black Costume
Defense Armlet

Mimic




Solomongrundy85
Male
Capricorn
69
57
Archer
Basic Skill
Distribute
Doublehand
Lava Walking

Poison Bow

Platinum Helmet
Chain Vest
Feather Boots

Charge+7, Charge+10
Dash, Heal, Tickle, Yell, Scream, Ultima



JumbocactuarX27
Male
Taurus
61
66
Time Mage
Sing
Regenerator
Long Status
Jump+1

Oak Staff

Green Beret
Linen Robe
Magic Gauntlet

Haste 2, Slow, Reflect, Demi, Stabilize Time
Angel Song, Cheer Song, Magic Song, Nameless Song

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BlackFireUK
Female
Libra
76
53
Knight
Summon Magic
Counter
Doublehand
Move+2

Ancient Sword

Diamond Helmet
Gold Armor
Germinas Boots

Armor Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Moogle, Shiva, Ifrit, Leviathan



Pandasforsale
Female
Taurus
55
79
Mediator
Basic Skill
Regenerator
Defend
Ignore Height

Papyrus Codex

Green Beret
Clothes
Magic Gauntlet

Invitation, Death Sentence, Negotiate, Refute, Rehabilitate
Dash, Throw Stone, Heal, Tickle



Just Here2
Male
Scorpio
37
50
Knight
Black Magic
Auto Potion
Equip Armor
Waterwalking

Rune Blade
Round Shield
Red Hood
Black Costume
Cursed Ring

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Surging Sword
Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Frog



YaBoy125
Male
Libra
57
41
Knight
Elemental
Brave Save
Concentrate
Move+2

Broad Sword
Buckler
Gold Helmet
Light Robe
Red Shoes

Magic Break, Power Break, Justice Sword
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

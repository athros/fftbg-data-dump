Player: !Green
Team: Green Team
Palettes: Green/White



Hasterious
Male
Virgo
71
46
Monk
Talk Skill
Counter Tackle
Dual Wield
Waterwalking



Headgear
Brigandine
Feather Boots

Secret Fist, Purification, Chakra, Revive
Invitation, Praise, Death Sentence, Negotiate, Refute



Regios91
Female
Libra
55
77
Summoner
Steal
Damage Split
Equip Polearm
Jump+3

Persia

Feather Hat
Adaman Vest
Elf Mantle

Ramuh, Carbunkle, Leviathan, Salamander
Steal Heart, Steal Weapon, Steal Accessory, Leg Aim



DamnThatShark
Male
Aries
41
58
Monk
Item
Absorb Used MP
Defense UP
Ignore Terrain



Cachusha
Adaman Vest
Feather Boots

Pummel, Earth Slash, Chakra, Revive
Potion, Ether, Maiden's Kiss, Soft, Phoenix Down



DeathTaxesAndAnime
Female
Leo
70
51
Mime

Absorb Used MP
Dual Wield
Waterbreathing



Holy Miter
Adaman Vest
Small Mantle

Mimic


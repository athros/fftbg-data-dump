Player: !White
Team: White Team
Palettes: White/Blue



R Raynos
Monster
Scorpio
78
71
Explosive










Leakimiko
Male
Aquarius
43
49
Ninja
Battle Skill
Damage Split
Equip Armor
Retreat

Iga Knife
Scorpion Tail
Bronze Helmet
Black Robe
Diamond Armlet

Shuriken, Bomb, Knife, Hammer, Dictionary
Weapon Break, Justice Sword, Dark Sword, Night Sword, Surging Sword



Pandasforsale
Female
Pisces
57
69
Geomancer
Item
Faith Up
Throw Item
Lava Walking

Giant Axe
Genji Shield
Green Beret
White Robe
Feather Boots

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Ether, Echo Grass, Holy Water, Phoenix Down



Sairentozon7
Male
Scorpio
69
45
Archer
Talk Skill
Regenerator
Equip Bow
Waterwalking

Silver Bow

Green Beret
Earth Clothes
Magic Ring

Charge+1
Invitation, Praise, Preach, Solution, Death Sentence, Insult

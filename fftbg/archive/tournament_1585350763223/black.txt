Player: !Black
Team: Black Team
Palettes: Black/Red



Deathmaker06
Female
Leo
57
75
Knight
Throw
Meatbone Slash
Doublehand
Levitate

Iron Sword

Diamond Helmet
Plate Mail
Diamond Armlet

Shield Break, Magic Break, Mind Break, Night Sword
Ninja Sword



Shakarak
Female
Capricorn
75
62
Samurai
Item
Sunken State
Defend
Move-MP Up

Holy Lance

Leather Helmet
Robe of Lords
Defense Armlet

Koutetsu, Heaven's Cloud, Kikuichimoji
Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



DeathTaxesAndAnime
Female
Sagittarius
44
48
Priest
Draw Out
Absorb Used MP
Equip Axe
Move-MP Up

Flame Whip

Golden Hairpin
Rubber Costume
Feather Boots

Cure, Regen, Shell 2, Esuna, Holy
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa



Tougou
Male
Virgo
69
51
Archer
Jump
Sunken State
Dual Wield
Fly

Bow Gun
Poison Bow
Feather Hat
Black Costume
Small Mantle

Charge+1, Charge+7, Charge+10
Level Jump3, Vertical Jump7

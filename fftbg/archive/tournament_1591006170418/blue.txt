Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Monster
Cancer
71
49
Ghost










D4rr1n
Female
Libra
59
44
Summoner
Black Magic
Regenerator
Halve MP
Move+2

Battle Folio

Feather Hat
Clothes
Sprint Shoes

Ramuh, Ifrit, Golem, Carbunkle, Cyclops
Ice 2, Death



Gary Corson
Female
Scorpio
41
66
Geomancer
Item
Catch
Magic Attack UP
Ignore Terrain

Broad Sword
Mythril Shield
Green Beret
Mystic Vest
Genji Gauntlet

Pitfall, Hallowed Ground, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Potion, Ether, Maiden's Kiss, Phoenix Down



Skillomono
Male
Cancer
48
50
Knight
Black Magic
Dragon Spirit
Equip Gun
Lava Walking

Glacier Gun
Crystal Shield
Genji Helmet
Leather Armor
Elf Mantle

Head Break, Power Break
Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 2, Frog

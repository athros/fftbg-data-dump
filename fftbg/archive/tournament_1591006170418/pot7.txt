Final Bets: yellow - 7 bets for 4,332G (65.3%, x0.53); white - 9 bets for 2,307G (34.7%, x1.88)

yellow bets:
wyonearth: 1,244G (28.7%, 1,244G)
CorpusCav: 1,069G (24.7%, 42,012G)
reinoe: 1,000G (23.1%, 44,631G)
Dymntd: 500G (11.5%, 44,782G)
serperemagus: 219G (5.1%, 219G)
CosmicTactician: 200G (4.6%, 2,007G)
datadrivenbot: 100G (2.3%, 30,133G)

white bets:
BirbBrainsBot: 1,000G (43.3%, 127,437G)
Humble_Fabio: 444G (19.2%, 5,367G)
gogofromtogo: 280G (12.1%, 280G)
foofermoofer: 164G (7.1%, 164G)
mog3003: 104G (4.5%, 104G)
gorgewall: 101G (4.4%, 1,032G)
ApplesauceBoss: 100G (4.3%, 4,877G)
victoriolue: 100G (4.3%, 6,819G)
getthemoneyz: 14G (0.6%, 785,615G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thyrandaal
Monster
Scorpio
69
73
Archaic Demon










ArchKnightX
Male
Aries
77
67
Oracle
Time Magic
Auto Potion
Concentrate
Move+2

Gokuu Rod

Holy Miter
Earth Clothes
Small Mantle

Life Drain, Zombie, Blind Rage, Dispel Magic, Paralyze
Slow, Stop, Immobilize, Reflect, Demi



Evdoggity
Female
Gemini
79
75
Thief
Elemental
Dragon Spirit
Magic Attack UP
Waterwalking

Blind Knife

Golden Hairpin
Chain Vest
Jade Armlet

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Gusty Wind, Lava Ball



Kalabain
Female
Aries
67
79
Samurai
Charge
Absorb Used MP
Short Charge
Levitate

Asura Knife

Gold Helmet
Chain Mail
Rubber Shoes

Asura, Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji
Charge+1, Charge+2

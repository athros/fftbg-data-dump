Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nok
Female
Sagittarius
79
43
Lancer
Yin Yang Magic
Sunken State
Equip Bow
Jump+1

Mythril Bow

Cross Helmet
Platinum Armor
Magic Gauntlet

Level Jump8, Vertical Jump8
Blind, Pray Faith, Doubt Faith, Zombie, Dispel Magic, Sleep



Grininda
Monster
Libra
72
56
Blue Dragon










BuffaloCrunch
Male
Leo
67
44
Squire
Time Magic
MA Save
Magic Attack UP
Move+3

Rune Blade
Crystal Shield
Triangle Hat
Mythril Armor
Genji Gauntlet

Dash, Throw Stone, Yell, Wish, Ultima
Haste, Reflect, Demi 2



NWOW 44
Male
Capricorn
46
66
Archer
Basic Skill
Hamedo
Equip Polearm
Move+2

Obelisk
Hero Shield
Feather Hat
Adaman Vest
Wizard Mantle

Charge+2, Charge+3, Charge+10, Charge+20
Throw Stone, Yell

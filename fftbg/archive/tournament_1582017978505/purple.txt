Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sypheck
Male
Aquarius
52
77
Lancer
Battle Skill
Abandon
Attack UP
Ignore Height

Cypress Rod
Round Shield
Gold Helmet
Chain Mail
Diamond Armlet

Level Jump8, Vertical Jump4
Head Break, Armor Break, Magic Break, Dark Sword, Night Sword, Surging Sword



Lucktheduck
Female
Pisces
44
48
Time Mage
Summon Magic
HP Restore
Beastmaster
Waterwalking

Wizard Staff

Leather Hat
Earth Clothes
Magic Gauntlet

Haste, Haste 2, Slow, Reflect, Demi 2
Moogle, Shiva, Ifrit, Carbunkle, Odin



RongRongArts
Male
Pisces
73
59
Chemist
Time Magic
Meatbone Slash
Secret Hunt
Swim

Blast Gun

Leather Hat
Wizard Outfit
Sprint Shoes

Potion, Hi-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Haste, Reflect, Demi, Stabilize Time



KitchTowel
Male
Pisces
48
38
Samurai
Black Magic
Counter Tackle
Doublehand
Move-MP Up

Kikuichimoji

Genji Helmet
Diamond Armor
Cursed Ring

Bizen Boat, Heaven's Cloud
Fire 2, Bolt, Ice 2, Ice 4, Frog, Death

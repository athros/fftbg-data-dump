Player: !Red
Team: Red Team
Palettes: Red/Brown



BlackFireUK
Male
Leo
55
55
Monk
Jump
Damage Split
Equip Shield
Move+1


Genji Shield
Feather Hat
Chain Vest
Cursed Ring

Wave Fist, Purification, Chakra, Seal Evil
Level Jump2, Vertical Jump6



NovaKnight21
Male
Gemini
38
67
Wizard
Punch Art
MA Save
Sicken
Ignore Height

Poison Rod

Headgear
Chain Vest
Feather Boots

Fire 4, Ice 2, Ice 4
Spin Fist, Wave Fist, Purification, Revive



Digitalsocrates
Female
Pisces
51
61
Summoner
Talk Skill
Damage Split
Long Status
Lava Walking

Mace of Zeus

Black Hood
White Robe
Leather Mantle

Moogle, Ramuh, Ifrit, Carbunkle, Odin, Leviathan, Silf, Fairy, Lich
Invitation, Insult, Refute



Dymntd
Female
Scorpio
71
62
Thief
Talk Skill
HP Restore
Equip Axe
Swim

Battle Axe

Twist Headband
Wizard Outfit
Vanish Mantle

Gil Taking, Steal Helmet, Steal Shield, Steal Status, Leg Aim
Invitation, Persuade, Praise, Negotiate

Player: !Black
Team: Black Team
Palettes: Black/Red



DrAntiSocial
Male
Virgo
61
74
Chemist
Steal
Dragon Spirit
Dual Wield
Swim

Cute Bag
Hydra Bag
Leather Hat
Mythril Vest
Chantage

X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Holy Water
Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Steal Status



Rastanar
Male
Scorpio
72
50
Samurai
Steal
Auto Potion
Equip Knife
Lava Walking

Assassin Dagger

Iron Helmet
Light Robe
Rubber Shoes

Koutetsu, Muramasa, Kikuichimoji
Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim



ALY327
Female
Gemini
80
46
Time Mage
Draw Out
Catch
Equip Armor
Swim

Healing Staff

Platinum Helmet
Gold Armor
Small Mantle

Haste, Haste 2, Slow, Demi, Stabilize Time
Asura, Koutetsu, Bizen Boat, Masamune



Ranmilia
Female
Leo
49
43
Wizard
Math Skill
Blade Grasp
Short Charge
Jump+3

Wizard Rod

Holy Miter
Leather Outfit
108 Gems

Fire, Bolt 2, Ice, Ice 2, Ice 3, Empower
Height, Prime Number, 4, 3

Player: !Red
Team: Red Team
Palettes: Red/Brown



ShintaroNayaka
Male
Leo
75
55
Thief
Punch Art
Catch
Equip Bow
Move+1

Gastrafitis

Headgear
Earth Clothes
Power Wrist

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Spin Fist, Pummel, Secret Fist, Chakra, Revive



Vorap
Monster
Libra
67
78
Skeleton










Lyner87
Female
Pisces
47
72
Dancer
Time Magic
Meatbone Slash
Equip Knife
Ignore Terrain

Dagger

Headgear
Silk Robe
Reflect Ring

Polka Polka, Disillusion, Obsidian Blade
Slow, Slow 2, Stop, Quick, Demi 2, Stabilize Time, Meteor



NIghtdew14
Female
Aries
62
75
Dancer
Charge
Faith Save
Dual Wield
Ignore Terrain

Cashmere
Persia
Golden Hairpin
Wizard Robe
Jade Armlet

Witch Hunt, Slow Dance
Charge+1, Charge+3, Charge+20

Player: !Black
Team: Black Team
Palettes: Black/Red



Deathmaker06
Female
Leo
43
72
Mime

Caution
Equip Shield
Swim


Buckler
Thief Hat
Power Sleeve
Germinas Boots

Mimic




Link1nr
Male
Aries
52
48
Oracle
Charge
Damage Split
Equip Bow
Teleport

Perseus Bow

Black Hood
Mystic Vest
Magic Ring

Blind, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze, Sleep
Charge+7, Charge+20



The Pengwin
Female
Aries
69
79
Thief
Basic Skill
Caution
Martial Arts
Jump+3

Sasuke Knife

Triangle Hat
Earth Clothes
Wizard Mantle

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Accumulate, Dash, Cheer Up, Wish



Lionhermit
Male
Capricorn
53
74
Calculator
Spirit Skill
Critical Quick
Equip Armor
Waterwalking

Thunder Rod

Feather Hat
White Robe
Battle Boots

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch, Bite, Self Destruct, Flame Attack, Small Bomb, Spark

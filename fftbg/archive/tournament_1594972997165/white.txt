Player: !White
Team: White Team
Palettes: White/Blue



Chompie
Female
Serpentarius
81
45
Thief
Item
Counter Magic
Dual Wield
Move+3

Air Knife
Kunai
Headgear
Earth Clothes
Rubber Shoes

Steal Weapon, Steal Status
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Lowlf
Male
Taurus
71
42
Wizard
Battle Skill
Dragon Spirit
Short Charge
Jump+2

Air Knife

Headgear
Mythril Vest
Small Mantle

Fire, Fire 2, Fire 3, Bolt 4, Ice, Ice 4, Empower, Death
Shield Break, Mind Break



SkylerBunny
Female
Taurus
52
77
Mime

Speed Save
Equip Armor
Move-MP Up



Mythril Helmet
Chain Mail
Magic Gauntlet

Mimic




ThreeMileIsland
Male
Libra
53
74
Oracle
Charge
PA Save
Doublehand
Move+1

Papyrus Codex

Golden Hairpin
White Robe
Wizard Mantle

Pray Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify, Dark Holy
Charge+2, Charge+7

Player: !zChamp
Team: Champion Team
Palettes: Green/White



ThreeMileIsland
Female
Aries
62
76
Wizard
Steal
Damage Split
Maintenance
Retreat

Flame Rod

Cachusha
Silk Robe
Sprint Shoes

Bolt, Bolt 2, Ice, Ice 2, Ice 3, Ice 4
Steal Shield, Steal Status



ZZ Yoshi
Male
Taurus
66
80
Mime

Catch
Dual Wield
Jump+3



Triangle Hat
Secret Clothes
Bracer

Mimic




Upvla
Male
Leo
71
71
Ninja
Charge
MP Restore
Equip Gun
Ignore Height

Blast Gun
Blast Gun
Holy Miter
Judo Outfit
Angel Ring

Shuriken, Staff
Charge+1, Charge+4



Lemonjohns
Male
Aquarius
65
48
Geomancer
Draw Out
Mana Shield
Magic Defense UP
Retreat

Giant Axe
Round Shield
Flash Hat
Light Robe
Feather Boots

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Asura, Masamune

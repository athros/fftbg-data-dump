Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Hales Bopp It
Male
Sagittarius
71
60
Summoner
Black Magic
Earplug
Dual Wield
Levitate

Rod
Ice Rod
Leather Hat
White Robe
Genji Gauntlet

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut
Bolt 2, Ice 3, Ice 4, Flare



Lionhermit
Monster
Aquarius
71
72
Ultima Demon










Antifalockhart
Male
Capricorn
73
65
Geomancer
Basic Skill
Earplug
Equip Polearm
Move+2

Obelisk
Genji Shield
Triangle Hat
Chameleon Robe
Jade Armlet

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Heal, Yell



UmaiJam
Male
Aries
76
59
Mime

Counter
Equip Armor
Move+2



Triangle Hat
Black Robe
Genji Gauntlet

Mimic


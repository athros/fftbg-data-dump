Player: !Red
Team: Red Team
Palettes: Red/Brown



StealthModeLocke
Female
Scorpio
78
51
Priest
Charge
Auto Potion
Halve MP
Retreat

Flail

Feather Hat
White Robe
Cursed Ring

Cure, Cure 2, Raise, Raise 2, Protect 2, Shell 2, Esuna
Charge+1, Charge+3



Lythe Caraker
Male
Libra
71
76
Chemist
Charge
Mana Shield
Beastmaster
Jump+3

Panther Bag

Leather Hat
Wizard Outfit
Magic Ring

Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Charge+1, Charge+3, Charge+5



DudeMonkey77
Female
Cancer
45
77
Archer
Battle Skill
Earplug
Magic Attack UP
Jump+3

Snipe Bow
Buckler
Feather Hat
Power Sleeve
Magic Gauntlet

Charge+2, Charge+4, Charge+5
Head Break, Speed Break, Power Break, Dark Sword, Night Sword



Reddwind
Male
Taurus
42
64
Knight
Talk Skill
Abandon
Beastmaster
Jump+3

Ancient Sword
Genji Shield
Cross Helmet
Chain Mail
Spike Shoes

Head Break, Weapon Break, Magic Break, Power Break
Invitation, Praise, Death Sentence

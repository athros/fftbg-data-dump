Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Defaultlybrave
Monster
Libra
64
54
Ghoul










Ruleof5
Female
Taurus
63
64
Archer
Basic Skill
PA Save
Doublehand
Lava Walking

Silver Bow

Headgear
Chain Vest
108 Gems

Charge+1, Charge+2, Charge+3, Charge+4
Heal, Fury



Lord Burrah
Male
Scorpio
74
41
Oracle
Black Magic
Regenerator
Concentrate
Fly

Iron Fan

Green Beret
Adaman Vest
Feather Mantle

Pray Faith, Zombie, Foxbird, Dispel Magic, Paralyze
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2



RunicMagus
Male
Aries
46
55
Thief
Yin Yang Magic
Dragon Spirit
Secret Hunt
Retreat

Mythril Knife

Headgear
Judo Outfit
Defense Armlet

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Blind, Doubt Faith, Silence Song, Dispel Magic

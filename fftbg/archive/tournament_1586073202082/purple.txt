Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HASTERIOUS
Male
Libra
55
77
Ninja
Yin Yang Magic
Speed Save
Equip Gun
Move-HP Up

Battle Folio
Papyrus Codex
Thief Hat
Clothes
Feather Mantle

Shuriken, Staff, Axe
Poison, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



Conome
Female
Taurus
70
39
Chemist
Throw
Parry
Equip Armor
Jump+2

Hydra Bag

Barbuta
Reflect Mail
Angel Ring

Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Shuriken, Hammer, Staff



Cryptopsy70
Male
Aries
44
66
Mime

Mana Shield
Equip Shield
Waterwalking


Genji Shield
Flash Hat
Power Sleeve
Power Wrist

Mimic




Laserman1000
Male
Virgo
69
73
Geomancer
Battle Skill
Damage Split
Secret Hunt
Jump+1

Battle Axe
Platinum Shield
Headgear
Black Costume
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind
Power Break, Mind Break, Justice Sword

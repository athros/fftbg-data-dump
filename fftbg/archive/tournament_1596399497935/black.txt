Player: !Black
Team: Black Team
Palettes: Black/Red



Lydian C
Female
Aquarius
78
74
Priest
Charge
Critical Quick
Equip Polearm
Move+3

Ryozan Silk

Barette
Light Robe
Wizard Mantle

Cure, Cure 4, Raise, Reraise, Protect, Shell, Esuna
Charge+2, Charge+5



Letdowncity
Male
Cancer
52
63
Mime

MP Restore
Equip Armor
Teleport



Iron Helmet
Wizard Robe
Elf Mantle

Mimic




Laserman1000
Female
Aquarius
61
44
Samurai
Item
Auto Potion
Equip Knife
Jump+3

Flame Rod

Mythril Helmet
Leather Armor
Salty Rage

Koutetsu, Bizen Boat, Muramasa
Potion, Hi-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



DeathTaxesAndAnime
Female
Capricorn
70
47
Wizard
Math Skill
HP Restore
Defend
Move-HP Up

Zorlin Shape

Twist Headband
White Robe
Power Wrist

Fire 2, Fire 3, Bolt 3, Ice 2, Ice 3, Ice 4, Empower, Frog
CT, Height, Prime Number, 5

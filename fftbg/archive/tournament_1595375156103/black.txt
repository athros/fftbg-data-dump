Player: !Black
Team: Black Team
Palettes: Black/Red



Butterbelljedi
Male
Libra
58
64
Bard
Item
Regenerator
Dual Wield
Jump+1

Fairy Harp
Fairy Harp
Golden Hairpin
Clothes
Wizard Mantle

Angel Song, Cheer Song
Potion, Hi-Potion, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



CapnChaos12
Male
Scorpio
51
77
Archer
Elemental
Counter Flood
Dual Wield
Swim

Romanda Gun
Blaze Gun
Cross Helmet
Chain Vest
Bracer

Charge+1, Charge+2, Charge+3, Charge+10
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



E Ballard
Monster
Sagittarius
75
37
Plague










Lastly
Female
Sagittarius
57
50
Oracle
Steal
Damage Split
Equip Knife
Ignore Height

Poison Rod

Black Hood
Brigandine
Bracer

Poison, Doubt Faith, Blind Rage, Dispel Magic, Petrify
Steal Armor, Leg Aim

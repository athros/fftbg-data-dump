Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Klednar21
Male
Cancer
53
67
Archer
Throw
Dragon Spirit
Magic Attack UP
Move+2

Bow Gun
Ice Shield
Red Hood
Mythril Vest
Cursed Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+20
Shuriken, Staff



Lewdylew
Male
Libra
60
78
Bard
Jump
Dragon Spirit
Magic Defense UP
Waterbreathing

Bloody Strings

Cachusha
Black Costume
Feather Mantle

Angel Song, Life Song, Magic Song, Last Song, Space Storage
Level Jump3, Vertical Jump8



Sir Uther115
Male
Aries
49
53
Knight
White Magic
Counter
Beastmaster
Move-HP Up

Ancient Sword
Buckler
Iron Helmet
Plate Mail
Chantage

Head Break, Magic Break, Power Break, Dark Sword, Surging Sword
Cure, Esuna, Holy



Just Here2
Male
Cancer
56
51
Chemist
Sing
Counter Tackle
Equip Gun
Swim

Papyrus Codex

Headgear
Clothes
Magic Ring

Potion, X-Potion, Antidote, Holy Water, Remedy, Phoenix Down
Cheer Song, Diamond Blade

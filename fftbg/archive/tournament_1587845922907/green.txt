Player: !Green
Team: Green Team
Palettes: Green/White



Skuldar
Female
Virgo
77
76
Chemist
Dance
Abandon
Halve MP
Ignore Height

Cultist Dagger

Headgear
Earth Clothes
Dracula Mantle

Potion, X-Potion, Ether, Antidote, Echo Grass, Soft, Remedy
Witch Hunt, Nameless Dance



Fenrislfr
Female
Leo
71
67
Knight
Summon Magic
Counter Magic
Magic Defense UP
Fly

Sleep Sword
Gold Shield
Mythril Helmet
Diamond Armor
Angel Ring

Head Break, Shield Break, Weapon Break, Surging Sword
Moogle, Shiva, Ramuh, Bahamut, Odin, Salamander, Fairy, Lich



BuffaloCrunch
Female
Taurus
62
76
Knight
Talk Skill
Critical Quick
Monster Talk
Ignore Height

Save the Queen
Platinum Shield
Mythril Helmet
Chameleon Robe
Cursed Ring

Head Break, Power Break, Justice Sword
Solution, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



WrathfulRemy
Male
Capricorn
66
50
Geomancer
White Magic
Earplug
Magic Attack UP
Move+1

Murasame
Gold Shield
Twist Headband
Brigandine
Salty Rage

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 2, Raise 2, Reraise, Esuna

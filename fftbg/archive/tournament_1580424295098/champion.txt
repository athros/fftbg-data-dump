Player: !zChamp
Team: Champion Team
Palettes: Black/Red



AniZero
Male
Aries
48
75
Monk
Black Magic
Absorb Used MP
Equip Bow
Ignore Height

Lightning Bow

Leather Hat
Leather Outfit
Feather Boots

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Fire 2, Fire 3, Fire 4, Ice, Ice 2, Empower, Frog, Flare



Cyneric M
Male
Aquarius
60
53
Ninja
Elemental
MA Save
Equip Shield
Teleport

Ninja Edge
Round Shield
Green Beret
Earth Clothes
Wizard Mantle

Shuriken
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



KupoKel
Male
Taurus
39
67
Mime

Damage Split
Equip Shield
Retreat


Crystal Shield
Black Hood
Chain Vest
Setiemson

Mimic




MonkMagnus
Female
Capricorn
79
42
Mediator
Draw Out
Sunken State
Sicken
Move+1

Romanda Gun

Feather Hat
White Robe
Wizard Mantle

Invitation, Threaten, Solution, Negotiate, Mimic Daravon
Heaven's Cloud, Muramasa

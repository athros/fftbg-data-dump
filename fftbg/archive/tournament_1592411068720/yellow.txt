Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PoroTact
Male
Gemini
62
65
Monk
Black Magic
Counter Flood
Short Status
Move-MP Up



Leather Hat
Leather Outfit
Spike Shoes

Secret Fist, Purification, Seal Evil
Fire, Bolt 4, Ice 2, Frog



Nekojin
Female
Gemini
47
66
Time Mage
Summon Magic
Distribute
Dual Wield
Move+2

Battle Bamboo
Ivory Rod
Green Beret
Wizard Outfit
Angel Ring

Slow, Immobilize, Reflect, Quick
Titan, Leviathan, Salamander



Ivellioz
Male
Virgo
51
46
Knight
Talk Skill
Dragon Spirit
Sicken
Move+3

Defender
Crystal Shield
Barbuta
Chameleon Robe
Angel Ring

Weapon Break, Power Break, Stasis Sword, Justice Sword, Explosion Sword
Persuade, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Grininda
Male
Aquarius
53
78
Geomancer
Draw Out
Abandon
Secret Hunt
Fly

Hydra Bag
Ice Shield
Flash Hat
Light Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Murasame, Heaven's Cloud

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lawnboxer
Female
Sagittarius
59
56
Wizard
Charge
PA Save
Attack UP
Move+3

Mythril Knife

Leather Hat
Black Robe
108 Gems

Fire, Bolt 3, Bolt 4, Ice, Frog, Death
Charge+10



O Heyno
Male
Gemini
72
69
Knight
Yin Yang Magic
Caution
Martial Arts
Levitate


Bronze Shield
Crystal Helmet
Plate Mail
Bracer

Magic Break, Speed Break, Mind Break, Justice Sword
Blind, Pray Faith, Doubt Faith, Paralyze



BoneMiser
Male
Scorpio
69
79
Bard
Summon Magic
Regenerator
Dual Wield
Waterbreathing

Ramia Harp
Bloody Strings
Twist Headband
Earth Clothes
Defense Ring

Life Song, Nameless Song, Hydra Pit
Moogle, Fairy



Old Overholt
Female
Sagittarius
54
79
Squire
Yin Yang Magic
Counter
Beastmaster
Teleport

Hunting Bow
Mythril Shield
Circlet
Crystal Mail
Magic Gauntlet

Dash, Throw Stone, Heal, Tickle, Yell, Wish
Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep

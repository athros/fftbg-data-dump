Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lordminsc
Male
Sagittarius
53
74
Thief
Item
Speed Save
Halve MP
Lava Walking

Ice Brand

Holy Miter
Judo Outfit
Genji Gauntlet

Steal Armor, Steal Status, Arm Aim
Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Holy Water



Gelwain
Male
Capricorn
50
74
Archer
Talk Skill
Distribute
Martial Arts
Teleport


Gold Shield
Leather Helmet
Black Costume
Magic Gauntlet

Charge+1, Charge+2, Charge+4
Invitation, Praise, Death Sentence



VolgraTheMoose
Male
Scorpio
57
71
Archer
Jump
Regenerator
Dual Wield
Ignore Terrain

Panther Bag
Star Bag
Headgear
Mythril Vest
Battle Boots

Charge+1, Charge+4, Charge+7
Level Jump8, Vertical Jump7



Argentbast
Female
Serpentarius
50
49
Priest
Throw
Dragon Spirit
Beastmaster
Ignore Terrain

Gold Staff

Holy Miter
Chameleon Robe
Jade Armlet

Raise, Reraise, Regen, Protect 2, Shell 2, Wall, Esuna
Shuriken, Bomb

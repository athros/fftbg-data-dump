Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



JumbocactuarX27
Monster
Leo
54
79
Red Chocobo










Kohlingen
Female
Virgo
60
58
Chemist
Black Magic
Brave Up
Defense UP
Move-MP Up

Hydra Bag

Headgear
Power Sleeve
Jade Armlet

Potion, X-Potion, Soft, Holy Water, Remedy, Phoenix Down
Bolt 3



FoeSquirrel
Male
Capricorn
50
44
Monk
Black Magic
Distribute
Sicken
Jump+3



Flash Hat
Brigandine
Power Wrist

Spin Fist, Wave Fist, Earth Slash, Purification, Revive
Fire, Fire 4, Bolt, Bolt 2, Empower



GrayGhostGaming
Male
Aries
49
54
Monk
Summon Magic
Parry
Short Charge
Move-HP Up



Triangle Hat
Adaman Vest
Leather Mantle

Pummel, Purification, Chakra
Moogle, Ramuh, Ifrit, Carbunkle, Leviathan, Salamander, Silf, Fairy, Lich

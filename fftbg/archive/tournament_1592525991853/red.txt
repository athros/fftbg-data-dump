Player: !Red
Team: Red Team
Palettes: Red/Brown



Fluffskull
Monster
Cancer
79
63
Behemoth










TheChainNerd
Female
Scorpio
58
45
Dancer
Black Magic
Sunken State
Defend
Waterbreathing

Cashmere

Holy Miter
Light Robe
Reflect Ring

Witch Hunt, Slow Dance, Disillusion
Fire 2, Fire 4, Bolt 4, Ice



Cryptopsy70
Monster
Aquarius
79
76
Red Dragon










Dictatorhowells
Male
Capricorn
72
63
Geomancer
Charge
Auto Potion
Maintenance
Move-HP Up

Giant Axe
Crystal Shield
Green Beret
White Robe
Red Shoes

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+4, Charge+5, Charge+20

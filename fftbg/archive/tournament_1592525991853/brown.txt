Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DuraiPapers
Male
Aries
53
38
Knight
Punch Art
Parry
Equip Knife
Fly

Thunder Rod
Venetian Shield
Diamond Helmet
Carabini Mail
Diamond Armlet

Armor Break, Shield Break, Weapon Break, Magic Break
Purification, Revive



Baron Von Scrub
Male
Taurus
67
78
Bard
Battle Skill
Counter Flood
Halve MP
Retreat

Cute Bag

Green Beret
Power Sleeve
Spike Shoes

Life Song, Battle Song, Magic Song, Diamond Blade, Sky Demon
Weapon Break, Magic Break, Speed Break, Stasis Sword, Justice Sword, Night Sword



Butterbelljedi
Male
Gemini
77
46
Bard
Charge
HP Restore
Dual Wield
Jump+3

Fairy Harp
Ramia Harp
Black Hood
Crystal Mail
Small Mantle

Battle Song, Nameless Song
Charge+1, Charge+7, Charge+20



Mrfripps
Male
Taurus
80
59
Calculator
White Magic
Absorb Used MP
Halve MP
Waterwalking

Battle Folio

Red Hood
White Robe
108 Gems

CT, Height, 4, 3
Raise, Protect 2, Shell, Wall, Esuna

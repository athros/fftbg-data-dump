Final Bets: red - 9 bets for 4,618G (28.4%, x2.51); black - 9 bets for 11,614G (71.6%, x0.40)

red bets:
BirbBrainsBot: 1,000G (21.7%, 194,241G)
perce90: 1,000G (21.7%, 4,195G)
E_Ballard: 789G (17.1%, 789G)
LordSDB: 645G (14.0%, 645G)
Powerpinch: 500G (10.8%, 11,661G)
Rislyeu: 300G (6.5%, 6,522G)
rednecknazgul: 212G (4.6%, 212G)
KyleWonToLiveForever: 148G (3.2%, 148G)
getthemoneyz: 24G (0.5%, 856,698G)

black bets:
Mesmaster: 5,304G (45.7%, 5,304G)
helpimabug: 2,138G (18.4%, 2,138G)
RaIshtar: 1,200G (10.3%, 9,842G)
prince_rogers_nelson_: 1,000G (8.6%, 7,776G)
Hamborn: 1,000G (8.6%, 73,104G)
waterwatereverywhere: 400G (3.4%, 5,925G)
HASTERIOUS: 372G (3.2%, 7,454G)
Celdia: 100G (0.9%, 3,085G)
datadrivenbot: 100G (0.9%, 35,214G)

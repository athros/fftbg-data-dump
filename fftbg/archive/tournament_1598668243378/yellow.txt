Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WesSideVandal
Female
Taurus
56
68
Summoner
Elemental
Meatbone Slash
Halve MP
Swim

Rod

Green Beret
Power Sleeve
108 Gems

Moogle, Shiva, Ifrit, Titan, Carbunkle, Odin, Silf, Fairy
Water Ball, Hallowed Ground, Quicksand, Blizzard, Gusty Wind, Lava Ball



Resjudicata3
Male
Taurus
52
62
Lancer
Summon Magic
Arrow Guard
Short Status
Move+1

Obelisk
Mythril Shield
Platinum Helmet
Leather Armor
Dracula Mantle

Level Jump8, Vertical Jump8
Moogle, Ifrit, Carbunkle, Odin, Silf



MemoriesofFinal
Female
Pisces
60
59
Mediator
Time Magic
Arrow Guard
Equip Bow
Retreat

Hunting Bow

Flash Hat
Black Robe
Sprint Shoes

Insult, Refute, Rehabilitate
Haste 2, Slow 2, Stop, Stabilize Time



Aldrammech
Male
Gemini
47
70
Calculator
White Magic
Auto Potion
Equip Shield
Jump+1

Thunder Rod
Bronze Shield
Red Hood
Mystic Vest
Leather Mantle

Height, Prime Number, 5, 4, 3
Cure 2, Raise, Regen, Shell, Wall, Esuna

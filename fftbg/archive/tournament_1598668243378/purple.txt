Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Rislyeu
Female
Cancer
63
43
Mime

Counter Flood
Dual Wield
Move+1



Golden Hairpin
Black Costume
108 Gems

Mimic




Galkife
Male
Capricorn
71
58
Bard
Steal
Counter
Defend
Ignore Terrain

Silver Bow

Flash Hat
Mythril Armor
Magic Gauntlet

Angel Song, Cheer Song, Diamond Blade
Steal Heart, Steal Shield, Steal Weapon



Chuckolator
Female
Taurus
52
54
Time Mage
Elemental
Earplug
Equip Knife
Fly

Poison Rod

Leather Hat
Linen Robe
Elf Mantle

Stop, Immobilize, Float, Demi, Demi 2, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Evdoggity
Male
Virgo
75
69
Wizard
Charge
MA Save
Magic Attack UP
Move+3

Wizard Rod

Twist Headband
Secret Clothes
Reflect Ring

Fire 3, Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Frog
Charge+1, Charge+10

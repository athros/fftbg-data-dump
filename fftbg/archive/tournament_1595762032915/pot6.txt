Final Bets: black - 11 bets for 15,850G (84.8%, x0.18); purple - 7 bets for 2,843G (15.2%, x5.58)

black bets:
prince_rogers_nelson_: 5,913G (37.3%, 5,913G)
VolgraTheMoose: 5,203G (32.8%, 10,406G)
DeathTaxesAndAnime: 1,372G (8.7%, 1,372G)
RaIshtar: 1,000G (6.3%, 17,668G)
twelfthrootoftwo: 787G (5.0%, 1,545G)
Netmonmatt: 500G (3.2%, 3,293G)
SephDarkheart: 384G (2.4%, 384G)
ar_tactic: 250G (1.6%, 83,286G)
Zxpr0jk: 216G (1.4%, 216G)
datadrivenbot: 200G (1.3%, 41,288G)
DouglasDragonThePoet: 25G (0.2%, 1,949G)

purple bets:
omnighast: 800G (28.1%, 2,829G)
BirbBrainsBot: 763G (26.8%, 108,129G)
AllInBot: 348G (12.2%, 348G)
AugnosMusic: 333G (11.7%, 6,673G)
getthemoneyz: 332G (11.7%, 1,403,787G)
roqqqpsi: 155G (5.5%, 1,036G)
cho_pin: 112G (3.9%, 112G)

Player: !White
Team: White Team
Palettes: White/Blue



Latebit
Monster
Aquarius
77
60
Squidraken










StealthModeLocke
Male
Scorpio
72
56
Mediator
Steal
Brave Save
Defend
Fly

Romanda Gun

Flash Hat
Brigandine
Reflect Ring

Praise, Preach, Death Sentence, Insult, Negotiate, Refute, Rehabilitate
Gil Taking, Steal Shield, Arm Aim



Windsah
Male
Serpentarius
56
48
Mediator
Summon Magic
Arrow Guard
Equip Polearm
Levitate

Octagon Rod

Golden Hairpin
Judo Outfit
Genji Gauntlet

Invitation, Persuade, Praise, Threaten, Solution, Death Sentence, Refute, Rehabilitate
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Salamander



Krinku
Male
Taurus
48
73
Samurai
Black Magic
Faith Save
Short Charge
Ignore Height

Kikuichimoji

Leather Helmet
Linen Cuirass
Vanish Mantle

Asura, Koutetsu, Murasame, Kikuichimoji
Fire, Fire 2, Bolt, Ice 4, Frog

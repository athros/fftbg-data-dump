Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Error72
Male
Scorpio
64
55
Knight
Time Magic
Abandon
Equip Bow
Jump+1

Night Killer
Diamond Shield
Genji Helmet
Light Robe
Genji Gauntlet

Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword
Slow 2, Demi, Demi 2, Stabilize Time



HaateXIII
Female
Leo
47
68
Archer
Talk Skill
Counter Tackle
Equip Axe
Ignore Height

Giant Axe
Mythril Shield
Headgear
Mystic Vest
Defense Armlet

Charge+4
Refute



Drusiform
Female
Cancer
71
42
Mediator
Draw Out
HP Restore
Equip Sword
Ignore Height

Koutetsu Knife

Leather Hat
Power Sleeve
Rubber Shoes

Persuade, Threaten, Preach, Solution, Death Sentence, Mimic Daravon, Refute
Kiyomori, Muramasa



ColetteMSLP
Male
Virgo
47
71
Samurai
Yin Yang Magic
Arrow Guard
Equip Gun
Swim

Bestiary

Gold Helmet
Leather Armor
Defense Ring

Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Poison, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep

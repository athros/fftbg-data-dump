Player: !White
Team: White Team
Palettes: White/Blue



OpHendoslice
Male
Aries
42
79
Archer
Battle Skill
Dragon Spirit
Doublehand
Swim

Hunting Bow

Flash Hat
Clothes
Feather Boots

Charge+1, Charge+2, Charge+5, Charge+10
Magic Break, Speed Break, Power Break, Dark Sword



DamnThatShark
Female
Cancer
68
47
Wizard
Item
Regenerator
Sicken
Move+2

Zorlin Shape

Red Hood
Chameleon Robe
Reflect Ring

Bolt, Bolt 2, Ice, Ice 3, Frog
Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Electric Algus
Male
Scorpio
72
78
Bard
Item
Meatbone Slash
Throw Item
Retreat

Fairy Harp

Flash Hat
Clothes
Red Shoes

Angel Song, Life Song, Battle Song, Nameless Song, Space Storage
Hi-Potion, Ether, Eye Drop, Phoenix Down



Choco Joe
Female
Libra
46
60
Geomancer
Talk Skill
Counter
Equip Knife
Move+1

Wizard Rod
Gold Shield
Headgear
White Robe
108 Gems

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon

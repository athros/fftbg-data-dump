Player: !Red
Team: Red Team
Palettes: Red/Brown



WinnerBit
Male
Leo
71
79
Ninja
Steal
Sunken State
Equip Axe
Move+3

Slasher
Battle Axe
Flash Hat
Leather Outfit
Defense Ring

Shuriken, Knife, Spear
Steal Shield, Arm Aim



TheManInPlaid
Female
Scorpio
44
45
Ninja
Steal
Distribute
Equip Polearm
Move-MP Up

Holy Lance
Persia
Flash Hat
Leather Outfit
Wizard Mantle

Stick
Gil Taking, Steal Shield, Arm Aim



Tmo50x
Female
Leo
43
71
Monk
Draw Out
Damage Split
Magic Attack UP
Teleport



Flash Hat
Chain Vest
Germinas Boots

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Murasame, Heaven's Cloud, Kikuichimoji



LDSkinny
Female
Leo
62
79
Oracle
Charge
PA Save
Equip Gun
Move+2

Romanda Gun

Leather Hat
White Robe
Sprint Shoes

Life Drain, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Charge+5

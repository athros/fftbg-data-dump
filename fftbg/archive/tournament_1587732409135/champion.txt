Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Diablos24
Male
Virgo
51
49
Archer
Draw Out
Counter
Doublehand
Move+2

Cross Bow

Green Beret
Earth Clothes
Diamond Armlet

Charge+3, Charge+5, Charge+20
Bizen Boat, Murasame, Heaven's Cloud



Mtueni
Male
Taurus
72
60
Summoner
Talk Skill
Faith Up
Short Charge
Ignore Height

Ice Rod

Golden Hairpin
White Robe
Wizard Mantle

Shiva, Ramuh, Titan, Odin, Silf, Fairy, Lich
Praise, Threaten, Preach, Death Sentence, Insult, Mimic Daravon



RjA0zcOQ96
Female
Scorpio
42
72
Chemist
Steal
Counter Magic
Equip Gun
Lava Walking

Fairy Harp

Black Hood
Earth Clothes
Red Shoes

Potion, Eye Drop, Phoenix Down
Gil Taking, Steal Armor, Steal Accessory



Leakimiko
Female
Sagittarius
73
63
Calculator
Time Magic
MP Restore
Equip Armor
Waterwalking

Ice Rod

Iron Helmet
Genji Armor
Setiemson

CT, Height, Prime Number
Haste, Haste 2, Slow, Slow 2, Reflect, Stabilize Time

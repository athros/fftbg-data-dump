Player: !White
Team: White Team
Palettes: White/Blue



CosmicTactician
Female
Capricorn
76
53
Time Mage
Draw Out
Meatbone Slash
Defend
Ignore Height

White Staff

Leather Hat
Secret Clothes
N-Kai Armlet

Haste, Slow 2, Demi
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



Soren Of Tyto
Male
Leo
58
75
Archer
Draw Out
Earplug
Defense UP
Move+2

Ultimus Bow

Red Hood
Mystic Vest
Germinas Boots

Charge+4, Charge+7
Heaven's Cloud, Kikuichimoji



Pplvee1
Female
Scorpio
78
58
Geomancer
Item
HP Restore
Magic Defense UP
Move+3

Slasher
Ice Shield
Black Hood
Black Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Quicksand, Lava Ball
Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down



Dinin991
Female
Leo
77
49
Chemist
Time Magic
Caution
Defense UP
Ignore Height

Romanda Gun

Leather Hat
Black Costume
Defense Armlet

Hi-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Haste, Haste 2, Immobilize, Reflect, Demi 2, Meteor

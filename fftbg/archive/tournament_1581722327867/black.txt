Player: !Black
Team: Black Team
Palettes: Black/Red



Arch8000
Female
Leo
69
74
Knight
Item
Arrow Guard
Magic Defense UP
Move+1

Ancient Sword
Ice Shield
Barbuta
Plate Mail
Leather Mantle

Armor Break, Speed Break, Power Break, Mind Break
Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Killmos
Female
Cancer
65
50
Calculator
White Magic
Speed Save
Secret Hunt
Jump+2

Poison Rod

Holy Miter
Robe of Lords
Bracer

CT, Prime Number, 5, 4
Cure 3, Reraise, Protect, Shell, Wall, Esuna



Ellbent
Female
Capricorn
43
55
Squire
Battle Skill
Damage Split
Short Status
Move+1

Rune Blade

Black Hood
Black Costume
N-Kai Armlet

Accumulate, Dash, Heal, Tickle, Yell, Cheer Up
Shield Break, Weapon Break, Speed Break, Mind Break, Dark Sword



KevvTwo
Male
Libra
55
57
Ninja
Item
Distribute
Equip Sword
Fly

Heaven's Cloud
Diamond Sword
Feather Hat
Chain Vest
Salty Rage

Shuriken, Spear, Dictionary
Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down

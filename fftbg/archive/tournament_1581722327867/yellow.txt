Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zalivandis
Female
Cancer
38
66
Dancer
Yin Yang Magic
Sunken State
Maintenance
Levitate

Ryozan Silk

Leather Hat
Mythril Vest
Diamond Armlet

Witch Hunt, Slow Dance, Disillusion, Nether Demon
Silence Song, Blind Rage, Confusion Song, Dispel Magic



Ggbroccoli
Female
Serpentarius
60
43
Squire
Time Magic
Critical Quick
Equip Gun
Move+3

Diamond Sword

Platinum Helmet
Plate Mail
Feather Mantle

Dash, Heal, Yell, Wish
Slow 2, Stop, Immobilize



Kalthonia
Male
Gemini
45
74
Knight
Yin Yang Magic
Hamedo
Equip Knife
Lava Walking

Thunder Rod
Flame Shield
Mythril Helmet
Wizard Robe
Diamond Armlet

Armor Break, Shield Break, Power Break, Mind Break, Justice Sword
Confusion Song, Paralyze, Sleep



BenYuPoker
Male
Gemini
80
80
Chemist
Jump
Parry
Equip Bow
Jump+2

Hunting Bow

Thief Hat
Secret Clothes
Red Shoes

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Level Jump5, Vertical Jump6

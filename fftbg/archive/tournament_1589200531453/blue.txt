Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ACSpree
Female
Gemini
35
53
Samurai
Summon Magic
Auto Potion
Defend
Waterbreathing

Mythril Spear

Barbuta
Black Robe
Feather Boots

Asura, Muramasa, Masamune
Moogle, Ramuh, Carbunkle, Silf



Jeeboheebo
Male
Scorpio
77
53
Lancer
Item
Critical Quick
Defense UP
Retreat

Holy Lance
Ice Shield
Barbuta
Plate Mail
Diamond Armlet

Level Jump8, Vertical Jump7
Potion, Antidote, Phoenix Down



LordSDB
Male
Capricorn
64
73
Bard
Punch Art
Critical Quick
Equip Armor
Levitate

Bloody Strings

Barbuta
Wizard Robe
Reflect Ring

Angel Song, Life Song, Cheer Song, Nameless Song, Last Song
Secret Fist, Purification, Revive



GrandmasterFrankerZ
Female
Taurus
62
66
Squire
Steal
PA Save
Halve MP
Lava Walking

Cross Bow
Crystal Shield
Gold Helmet
Adaman Vest
Rubber Shoes

Heal, Yell, Ultima
Gil Taking, Steal Heart, Steal Accessory

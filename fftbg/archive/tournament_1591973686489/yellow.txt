Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Fenaen
Male
Virgo
44
79
Monk
Steal
Auto Potion
Magic Attack UP
Retreat



Ribbon
Wizard Outfit
Feather Mantle

Wave Fist, Purification, Revive
Gil Taking, Steal Weapon, Steal Status



PoroTact
Female
Scorpio
50
49
Calculator
Bird Skill
Abandon
Halve MP
Waterwalking

Gokuu Rod

Leather Helmet
Wizard Outfit
Angel Ring

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



Cryptopsy70
Male
Sagittarius
66
71
Ninja
Yin Yang Magic
Counter
Concentrate
Jump+3

Hidden Knife
Sasuke Knife
Flash Hat
Mystic Vest
Red Shoes

Shuriken
Life Drain, Dispel Magic, Sleep



Phi Sig
Male
Sagittarius
65
57
Summoner
Elemental
Counter Magic
Short Charge
Ignore Terrain

Rod

Headgear
Judo Outfit
Magic Ring

Shiva, Titan, Leviathan, Cyclops
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball

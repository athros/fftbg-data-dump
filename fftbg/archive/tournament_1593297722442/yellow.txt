Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Amiture
Female
Scorpio
61
40
Monk
Dance
Caution
Long Status
Move+3



Black Hood
Wizard Outfit
Diamond Armlet

Wave Fist, Purification, Revive
Witch Hunt, Wiznaibus, Disillusion



Choco Joe
Female
Gemini
55
53
Mime

Critical Quick
Magic Attack UP
Ignore Height



Leather Helmet
Brigandine
Bracer

Mimic




Yeepeekayeahmf
Male
Taurus
41
45
Wizard
Punch Art
Meatbone Slash
Beastmaster
Swim

Wizard Rod

Golden Hairpin
Mythril Vest
Battle Boots

Fire, Fire 3, Bolt, Ice, Ice 2
Purification, Revive



Lyner87
Female
Capricorn
74
76
Summoner
Throw
Caution
Short Charge
Jump+3

Wizard Rod

Leather Hat
Linen Robe
Spike Shoes

Moogle, Shiva, Golem, Bahamut, Odin, Silf, Fairy
Staff

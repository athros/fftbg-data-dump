Player: !Black
Team: Black Team
Palettes: Black/Red



JLinkletter
Female
Aquarius
64
52
Chemist
Basic Skill
Sunken State
Equip Shield
Waterwalking

Main Gauche
Round Shield
Black Hood
Black Costume
Small Mantle

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Holy Water, Remedy, Phoenix Down
Tickle, Fury



RampagingRobot
Monster
Sagittarius
50
76
Bomb










Lemonjohns
Male
Scorpio
51
48
Thief
Punch Art
Auto Potion
Maintenance
Move+2

Main Gauche

Triangle Hat
Adaman Vest
Feather Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Weapon, Arm Aim
Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil



ZCKaiser
Male
Aries
67
56
Knight
Basic Skill
Damage Split
Concentrate
Move+3

Defender
Mythril Shield
Grand Helmet
Leather Armor
108 Gems

Armor Break, Shield Break, Magic Break, Power Break, Stasis Sword, Surging Sword
Accumulate, Dash, Throw Stone, Cheer Up, Scream

Final Bets: white - 7 bets for 3,515G (22.2%, x3.51); brown - 9 bets for 12,341G (77.8%, x0.28)

white bets:
Magicandy: 1,001G (28.5%, 12,521G)
BirbBrainsBot: 1,000G (28.4%, 60,552G)
CT_5_Holy: 440G (12.5%, 2,200G)
dtrain332: 380G (10.8%, 1,135G)
twelfthrootoftwo: 300G (8.5%, 6,874G)
josephiroth_143: 200G (5.7%, 1,388G)
getthemoneyz: 194G (5.5%, 1,073,250G)

brown bets:
Lydian_C: 4,200G (34.0%, 196,576G)
VolgraTheMoose: 2,688G (21.8%, 5,377G)
prince_rogers_nelson_: 1,564G (12.7%, 1,564G)
RaIshtar: 1,500G (12.2%, 9,788G)
Ruvelia_BibeI: 1,000G (8.1%, 7,008G)
E_Ballard: 668G (5.4%, 668G)
Heyndog: 420G (3.4%, 433G)
gorgewall: 201G (1.6%, 27,076G)
datadrivenbot: 100G (0.8%, 47,035G)

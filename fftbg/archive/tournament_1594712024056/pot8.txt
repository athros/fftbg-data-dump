Final Bets: yellow - 8 bets for 5,426G (58.8%, x0.70); champion - 9 bets for 3,799G (41.2%, x1.43)

yellow bets:
BirbBrainsBot: 1,000G (18.4%, 137,191G)
dantayystv: 1,000G (18.4%, 27,578G)
Arcblazer23: 750G (13.8%, 750G)
Hasterious: 696G (12.8%, 696G)
nebukin: 600G (11.1%, 604G)
CT_5_Holy: 580G (10.7%, 580G)
Evewho: 500G (9.2%, 8,307G)
randgridr: 300G (5.5%, 5,170G)

champion bets:
NicoSavoy: 1,000G (26.3%, 186,458G)
superdevon1: 583G (15.3%, 5,830G)
getthemoneyz: 482G (12.7%, 1,277,065G)
roofiepops: 464G (12.2%, 464G)
upvla: 450G (11.8%, 4,228G)
twelfthrootoftwo: 404G (10.6%, 794G)
datadrivenbot: 200G (5.3%, 57,447G)
dtrain332: 116G (3.1%, 116G)
Firesheath: 100G (2.6%, 3,603G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Lastly
Male
Leo
70
52
Mediator
Item
Speed Save
Throw Item
Ignore Height

Stone Gun

Red Hood
Linen Robe
Small Mantle

Persuade, Praise, Death Sentence, Insult, Refute
Potion, Antidote, Eye Drop, Echo Grass, Soft, Phoenix Down



Chuckolator
Male
Libra
48
51
Summoner
Throw
Counter
Equip Gun
Move-HP Up

Fairy Harp

Headgear
Black Costume
Defense Ring

Moogle, Shiva, Titan, Odin, Leviathan
Shuriken, Dictionary



ArashiKurobara
Female
Taurus
72
55
Calculator
Demon Skill
HP Restore
Halve MP
Lava Walking

Madlemgen
Aegis Shield
Holy Miter
Genji Armor
Wizard Mantle

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Laserman1000
Male
Virgo
64
65
Squire
Steal
Damage Split
Magic Defense UP
Jump+2

Materia Blade
Buckler
Crystal Helmet
Black Costume
Defense Ring

Dash, Heal, Tickle, Yell, Cheer Up, Fury, Scream
Steal Helmet, Steal Accessory

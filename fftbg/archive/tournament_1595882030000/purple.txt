Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheChainNerd
Male
Aquarius
52
74
Wizard
Time Magic
Absorb Used MP
Equip Sword
Move+1

Rune Blade

Green Beret
Wizard Robe
Small Mantle

Fire, Fire 4, Bolt, Bolt 4, Ice 2, Flare
Slow 2, Reflect, Demi, Stabilize Time



DavenIII
Male
Aries
58
74
Thief
Jump
Earplug
Equip Polearm
Lava Walking

Whale Whisker

Golden Hairpin
Chain Vest
Power Wrist

Steal Helmet, Steal Shield, Steal Weapon
Level Jump2, Vertical Jump2



Aldrammech
Female
Pisces
76
54
Monk
Summon Magic
Counter Magic
Equip Sword
Ignore Height

Sleep Sword

Golden Hairpin
Black Costume
Feather Mantle

Pummel, Earth Slash, Secret Fist, Purification
Moogle, Shiva, Titan, Fairy



Hasterious
Male
Taurus
73
59
Mediator
Punch Art
Counter Tackle
Dual Wield
Jump+2

Bestiary
Bestiary
Holy Miter
White Robe
108 Gems

Persuade, Preach, Solution, Negotiate
Wave Fist, Purification, Revive

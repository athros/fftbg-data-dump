Player: !Black
Team: Black Team
Palettes: Black/Red



Shakarak
Female
Virgo
58
60
Archer
Time Magic
Dragon Spirit
Doublehand
Move+2

Ultimus Bow

Barbuta
Brigandine
Rubber Shoes

Charge+1, Charge+4, Charge+5, Charge+10
Haste, Haste 2, Slow 2, Stop, Immobilize, Reflect, Quick, Demi



Nekojin
Female
Pisces
53
57
Squire
Steal
Mana Shield
Sicken
Teleport

Broad Sword
Aegis Shield
Black Hood
Leather Outfit
Germinas Boots

Dash, Heal, Tickle, Fury
Steal Heart, Steal Helmet, Steal Shield, Steal Accessory



VolgraTheMoose
Male
Capricorn
71
50
Chemist
Time Magic
Critical Quick
Doublehand
Move+2

Panther Bag

Feather Hat
Mythril Vest
Bracer

Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss
Haste, Immobilize, Reflect, Demi, Stabilize Time



Just Here2
Male
Capricorn
53
38
Squire
Black Magic
Dragon Spirit
Equip Gun
Move+3

Bestiary
Platinum Shield
Holy Miter
Wizard Outfit
Cursed Ring

Heal, Fury
Fire 4, Bolt, Ice, Flare

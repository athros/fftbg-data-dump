Player: !zChamp
Team: Champion Team
Palettes: Green/White



Miku Shikhu
Female
Aquarius
75
44
Ninja
Dance
Catch
Beastmaster
Jump+2

Kunai
Flame Whip
Triangle Hat
Black Costume
Red Shoes

Shuriken, Sword, Staff
Witch Hunt, Disillusion, Dragon Pit



PoroTact
Male
Aquarius
62
63
Knight
Time Magic
Meatbone Slash
Martial Arts
Move+3


Round Shield
Barbuta
Bronze Armor
Angel Ring

Weapon Break, Magic Break, Dark Sword, Night Sword
Haste, Haste 2, Slow, Stop, Immobilize



Phi Sig
Male
Taurus
49
80
Wizard
Talk Skill
Counter Flood
Short Charge
Retreat

Orichalcum

Black Hood
White Robe
Sprint Shoes

Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Empower, Death, Flare
Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate



RunicMagus
Monster
Taurus
71
56
Black Chocobo








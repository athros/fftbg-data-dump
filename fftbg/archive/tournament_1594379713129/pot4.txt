Final Bets: purple - 11 bets for 3,993G (21.1%, x3.75); brown - 7 bets for 14,962G (78.9%, x0.27)

purple bets:
loveyouallfriends: 1,000G (25.0%, 10,966G)
BirbBrainsBot: 780G (19.5%, 18,407G)
artahc: 400G (10.0%, 2,915G)
GRRR12: 400G (10.0%, 1,358G)
letdowncity: 380G (9.5%, 3,443G)
ColetteMSLP: 300G (7.5%, 1,799G)
serperemagus: 300G (7.5%, 300G)
Mysteriousdewd: 200G (5.0%, 15,449G)
datadrivenbot: 200G (5.0%, 62,497G)
getthemoneyz: 22G (0.6%, 1,252,701G)
daveb_: 11G (0.3%, 916G)

brown bets:
RaIshtar: 8,535G (57.0%, 8,535G)
DeathTaxesAndAnime: 4,220G (28.2%, 8,276G)
ForagerCats: 1,000G (6.7%, 13,898G)
CassiePhoenix: 356G (2.4%, 356G)
TheMurkGnome: 344G (2.3%, 344G)
dantayystv: 307G (2.1%, 307G)
CosmicTactician: 200G (1.3%, 42,493G)

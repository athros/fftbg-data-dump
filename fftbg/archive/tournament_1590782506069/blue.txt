Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Coralreeferz
Female
Leo
75
67
Dancer
Charge
Counter Tackle
Dual Wield
Ignore Terrain

Ryozan Silk
Ryozan Silk
Flash Hat
Chameleon Robe
Magic Gauntlet

Disillusion, Last Dance, Obsidian Blade
Charge+3, Charge+4, Charge+5



PezPezzington
Male
Aquarius
67
56
Lancer
Punch Art
Caution
Equip Polearm
Lava Walking

Obelisk
Round Shield
Mythril Helmet
Linen Robe
N-Kai Armlet

Level Jump5, Vertical Jump8
Pummel, Purification, Seal Evil



Lionhermit
Male
Cancer
45
53
Samurai
Basic Skill
Blade Grasp
Equip Gun
Levitate

Papyrus Codex

Iron Helmet
Genji Armor
Feather Boots

Asura, Bizen Boat, Muramasa, Kikuichimoji
Throw Stone, Yell, Scream



Powerpinch
Male
Capricorn
54
81
Chemist
Charge
Sunken State
Beastmaster
Lava Walking

Blaze Gun

Twist Headband
Wizard Outfit
Chantage

Potion, Hi-Potion, Eye Drop, Echo Grass, Soft
Charge+1, Charge+4, Charge+20

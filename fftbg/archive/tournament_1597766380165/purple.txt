Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TasisSai
Male
Virgo
59
54
Calculator
White Magic
Brave Save
Halve MP
Swim

Octagon Rod

Leather Hat
Power Sleeve
Bracer

CT, Prime Number, 5
Cure, Cure 2, Raise 2, Reraise, Shell 2, Esuna



SQUiDSQUARKLIN
Male
Leo
58
61
Ninja
Basic Skill
Caution
Equip Gun
Retreat

Bestiary
Bestiary
Green Beret
Mythril Vest
Defense Armlet

Shuriken, Bomb, Staff, Stick
Accumulate, Dash, Heal, Tickle, Scream



VolgraTheMoose
Male
Gemini
53
72
Knight
Throw
PA Save
Halve MP
Move+3

Rune Blade
Round Shield
Barbuta
Bronze Armor
Magic Gauntlet

Power Break, Stasis Sword, Justice Sword
Bomb



Dogsandcatsand
Female
Virgo
52
64
Wizard
Draw Out
Abandon
Secret Hunt
Swim

Flame Rod

Leather Hat
Black Robe
Leather Mantle

Fire 2, Fire 3, Fire 4, Bolt 2, Ice, Ice 3
Heaven's Cloud, Kiyomori, Muramasa

Player: !Black
Team: Black Team
Palettes: Black/Red



Forkmore
Male
Scorpio
42
51
Ninja
Charge
Abandon
Short Status
Jump+3

Mythril Knife
Cultist Dagger
Headgear
Leather Outfit
Chantage

Wand
Charge+1, Charge+2, Charge+5



Chuckolator
Female
Pisces
79
62
Monk
Draw Out
Faith Save
Short Status
Lava Walking



Leather Hat
Brigandine
Defense Armlet

Spin Fist, Earth Slash, Purification, Chakra, Revive
Asura, Koutetsu, Bizen Boat



XBizzy
Female
Cancer
40
68
Squire
Item
Parry
Concentrate
Levitate

Coral Sword
Kaiser Plate
Leather Helmet
Wizard Outfit
Jade Armlet

Throw Stone, Heal, Yell, Cheer Up, Wish, Ultima
Potion, Hi-Ether, Echo Grass, Phoenix Down



Pplvee1
Male
Taurus
51
50
Ninja
Draw Out
MA Save
Defense UP
Retreat

Mythril Knife
Spell Edge
Red Hood
Clothes
Reflect Ring

Knife, Ninja Sword, Axe, Dictionary
Koutetsu, Murasame, Kiyomori, Muramasa

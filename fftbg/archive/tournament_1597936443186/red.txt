Player: !Red
Team: Red Team
Palettes: Red/Brown



Fenaen
Male
Cancer
66
68
Mime

Catch
Magic Defense UP
Teleport



Green Beret
Mystic Vest
Angel Ring

Mimic




Chuckolator
Female
Taurus
74
42
Thief
Throw
Parry
Equip Axe
Jump+1

Healing Staff

Twist Headband
Wizard Outfit
Jade Armlet

Gil Taking, Steal Armor, Steal Status, Leg Aim
Bomb, Ninja Sword



Killth3kid
Male
Leo
73
53
Chemist
Black Magic
MA Save
Short Charge
Fly

Mythril Knife

Green Beret
Brigandine
Wizard Mantle

Potion, Hi-Potion, Antidote, Phoenix Down
Bolt, Ice 4, Empower



KasugaiRoastedPeas
Female
Leo
70
64
Dancer
Summon Magic
Faith Save
Attack UP
Move-HP Up

Cashmere

Thief Hat
Chameleon Robe
Magic Gauntlet

Witch Hunt, Wiznaibus, Disillusion, Obsidian Blade
Moogle, Titan

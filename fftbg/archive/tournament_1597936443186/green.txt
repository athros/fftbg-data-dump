Player: !Green
Team: Green Team
Palettes: Green/White



NovaKnight21
Monster
Virgo
48
59
Dryad










Actual JP
Male
Virgo
74
38
Geomancer
Black Magic
Counter Flood
Maintenance
Ignore Height

Broad Sword
Ice Shield
Twist Headband
Black Robe
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball
Ice, Ice 2



DavenIII
Male
Taurus
64
49
Lancer
Elemental
Parry
Doublehand
Ignore Terrain

Javelin

Gold Helmet
Platinum Armor
N-Kai Armlet

Level Jump2, Vertical Jump4
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Nizaha
Female
Leo
51
64
Thief
Basic Skill
Mana Shield
Doublehand
Jump+3

Spell Edge

Red Hood
Chain Vest
Angel Ring

Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Heal, Tickle, Cheer Up

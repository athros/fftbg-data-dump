Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sparker9
Male
Scorpio
57
69
Geomancer
Yin Yang Magic
Auto Potion
Sicken
Ignore Terrain

Kikuichimoji
Diamond Shield
Golden Hairpin
Linen Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Blind, Spell Absorb, Life Drain, Pray Faith, Blind Rage



Gut922
Female
Capricorn
56
42
Samurai
Dance
Catch
Equip Knife
Ignore Terrain

Air Knife

Gold Helmet
Genji Armor
Sprint Shoes

Heaven's Cloud, Kiyomori
Slow Dance, Disillusion, Void Storage, Nether Demon, Dragon Pit



Raixelol
Male
Pisces
49
56
Ninja
Elemental
Regenerator
Equip Axe
Teleport

Giant Axe
Flail
Black Hood
Adaman Vest
108 Gems

Knife
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



SkylerBunny
Male
Cancer
65
74
Knight
Draw Out
Parry
Equip Sword
Jump+1

Heaven's Cloud
Aegis Shield
Genji Helmet
Platinum Armor
Germinas Boots

Shield Break, Weapon Break, Magic Break, Mind Break, Justice Sword
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa

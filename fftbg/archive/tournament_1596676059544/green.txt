Player: !Green
Team: Green Team
Palettes: Green/White



Coralreeferz
Female
Virgo
74
65
Geomancer
Draw Out
Counter
Equip Armor
Move+1

Giant Axe
Gold Shield
Cross Helmet
Genji Armor
Spike Shoes

Pitfall, Hell Ivy, Hallowed Ground, Sand Storm, Lava Ball
Bizen Boat, Murasame, Kiyomori, Muramasa



Killth3kid
Female
Sagittarius
79
81
Summoner
Item
Abandon
Equip Armor
Teleport

Dragon Rod

Green Beret
Carabini Mail
Sprint Shoes

Moogle, Shiva, Titan, Carbunkle, Leviathan, Fairy
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Remedy, Phoenix Down



Virilikus
Monster
Gemini
73
53
Tiamat










StealthModeLocke
Male
Sagittarius
58
62
Wizard
Elemental
Earplug
Doublehand
Jump+2

Poison Rod

Thief Hat
Chameleon Robe
Genji Gauntlet

Fire 2, Fire 4, Bolt, Flare
Pitfall, Water Ball, Static Shock, Will-O-Wisp, Blizzard, Lava Ball

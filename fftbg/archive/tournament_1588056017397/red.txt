Player: !Red
Team: Red Team
Palettes: Red/Brown



The Pengwin
Monster
Serpentarius
60
44
Revenant










ANFz
Male
Scorpio
48
69
Priest
Charge
Auto Potion
Long Status
Swim

Wizard Staff

Triangle Hat
Brigandine
N-Kai Armlet

Cure 3, Raise, Raise 2, Protect 2, Shell, Esuna
Charge+1, Charge+3, Charge+4, Charge+5



Lawnboxer
Female
Scorpio
64
75
Ninja
Basic Skill
Distribute
Short Status
Waterwalking

Sasuke Knife
Flail
Ribbon
Judo Outfit
Feather Mantle

Sword, Stick
Accumulate, Dash, Yell



Go2sleepTV
Male
Virgo
61
62
Knight
White Magic
Counter
Defense UP
Jump+1

Battle Axe
Genji Shield
Cross Helmet
Reflect Mail
108 Gems

Shield Break, Power Break, Mind Break, Justice Sword
Raise, Shell, Wall, Esuna, Magic Barrier

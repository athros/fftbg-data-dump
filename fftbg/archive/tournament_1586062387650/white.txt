Player: !White
Team: White Team
Palettes: White/Blue



Shakarak
Male
Leo
58
65
Lancer
Talk Skill
Dragon Spirit
Dual Wield
Move+1

Gokuu Rod
Cypress Rod
Mythril Helmet
Diamond Armor
Cherche

Level Jump5, Vertical Jump7
Preach, Insult, Mimic Daravon, Rehabilitate



BurlapChampion
Male
Gemini
52
75
Wizard
Yin Yang Magic
Counter
Short Charge
Teleport

Dragon Rod

Feather Hat
Adaman Vest
Setiemson

Fire, Fire 2, Fire 4, Bolt, Bolt 3, Ice 2, Ice 4, Death
Zombie, Foxbird, Dispel Magic, Paralyze



Roofiepops
Female
Gemini
79
79
Mediator
Elemental
Absorb Used MP
Sicken
Teleport

Papyrus Codex

Feather Hat
Light Robe
Bracer

Invitation, Preach, Solution, Insult, Negotiate
Hallowed Ground, Quicksand, Sand Storm, Lava Ball



PotionDweller
Monster
Gemini
70
71
Ultima Demon








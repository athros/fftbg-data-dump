Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Eldente
Male
Leo
73
36
Ninja
Talk Skill
Meatbone Slash
Concentrate
Retreat

Kunai
Morning Star
Black Hood
Black Costume
Sprint Shoes

Shuriken
Persuade, Praise, Solution, Death Sentence, Negotiate, Mimic Daravon



Sprawl5
Male
Leo
54
54
Ninja
Charge
Counter Flood
Equip Gun
Retreat

Mythril Gun
Blast Gun
Triangle Hat
Judo Outfit
Wizard Mantle

Shuriken, Knife
Charge+2, Charge+3



RawBees
Male
Cancer
56
58
Samurai
Time Magic
Counter
Halve MP
Ignore Height

Kiyomori

Barbuta
Silk Robe
Angel Ring

Bizen Boat, Murasame, Kiyomori, Muramasa
Haste, Slow, Slow 2, Stop, Float, Quick



ParthMakeo
Male
Sagittarius
60
49
Calculator
Black Magic
PA Save
Short Status
Move-MP Up

Star Bag

Holy Miter
Linen Robe
Red Shoes

CT, Prime Number, 5
Fire 2, Fire 4, Bolt 3, Empower, Death

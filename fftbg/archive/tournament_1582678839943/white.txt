Player: !White
Team: White Team
Palettes: White/Blue



UmaiJam
Male
Virgo
53
53
Chemist
Elemental
Auto Potion
Secret Hunt
Jump+3

Air Knife

Flash Hat
Secret Clothes
Diamond Armlet

Potion, X-Potion, Ether, Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind



TheKillerNacho
Female
Scorpio
53
68
Oracle
White Magic
Distribute
Equip Sword
Move-HP Up

Muramasa

Black Hood
White Robe
Magic Gauntlet

Foxbird, Dispel Magic, Sleep
Cure, Raise, Raise 2, Reraise, Shell, Shell 2



Sypheck
Monster
Cancer
49
50
Holy Dragon










ThePineappleSalesman
Male
Taurus
71
78
Priest
Talk Skill
Counter
Sicken
Move+2

Rainbow Staff

Black Hood
Wizard Robe
Elf Mantle

Cure 4, Raise, Regen, Protect, Wall, Esuna
Invitation, Praise, Threaten, Preach, Insult, Negotiate, Refute

Player: !White
Team: White Team
Palettes: White/Blue



CorpusCav
Female
Scorpio
62
65
Dancer
Throw
Auto Potion
Dual Wield
Move+3

Cute Bag
Hydra Bag
Green Beret
Brigandine
Spike Shoes

Wiznaibus, Polka Polka
Bomb, Staff, Wand



SSwing
Male
Libra
60
45
Ninja
Punch Art
Sunken State
Equip Polearm
Jump+2

Battle Bamboo
Whale Whisker
Triangle Hat
Brigandine
Defense Ring

Knife
Pummel, Purification, Chakra, Revive



RjA0zcOQ96
Male
Virgo
53
71
Oracle
Draw Out
Counter
Equip Armor
Move+1

Cypress Rod

Diamond Helmet
Gold Armor
Jade Armlet

Spell Absorb, Pray Faith, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep
Bizen Boat, Heaven's Cloud, Muramasa



Nok
Male
Capricorn
76
66
Geomancer
Sing
MA Save
Martial Arts
Ignore Terrain

Kikuichimoji
Platinum Shield
Feather Hat
Chain Vest
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Life Song, Battle Song, Magic Song

Final Bets: white - 10 bets for 6,571G (56.6%, x0.77); black - 8 bets for 5,044G (43.4%, x1.30)

white bets:
thethorndog1: 1,004G (15.3%, 1,004G)
BirbBrainsBot: 1,000G (15.2%, 66,060G)
metagameface: 1,000G (15.2%, 29,496G)
CapnChaos12: 911G (13.9%, 911G)
otakutaylor: 800G (12.2%, 1,914G)
Dymntd: 656G (10.0%, 656G)
Dexsana: 600G (9.1%, 4,859G)
twelfthrootoftwo: 300G (4.6%, 6,478G)
Lolprinze: 200G (3.0%, 1,061G)
datadrivenbot: 100G (1.5%, 18,475G)

black bets:
CorpusCav: 2,088G (41.4%, 2,088G)
Mesmaster: 1,000G (19.8%, 35,314G)
ungabunga_bot: 644G (12.8%, 495,415G)
ko2q: 556G (11.0%, 556G)
Strifu: 340G (6.7%, 340G)
Aestheta: 216G (4.3%, 216G)
victoriolue: 100G (2.0%, 4,823G)
Kyune: 100G (2.0%, 1,830G)

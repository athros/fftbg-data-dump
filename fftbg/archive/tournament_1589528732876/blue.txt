Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Metagameface
Female
Cancer
57
79
Dancer
Yin Yang Magic
Distribute
Dual Wield
Retreat

Ryozan Silk
Cashmere
Triangle Hat
Linen Robe
Dracula Mantle

Wiznaibus, Nether Demon, Dragon Pit
Blind, Poison, Life Drain, Silence Song, Blind Rage, Foxbird, Dispel Magic



Tronfonne
Monster
Leo
73
52
Dryad










CorpusCav
Female
Aries
41
75
Samurai
Punch Art
Catch
Equip Axe
Move+1

Wizard Staff

Iron Helmet
Linen Robe
N-Kai Armlet

Koutetsu, Murasame, Muramasa
Spin Fist, Purification, Revive, Seal Evil



Strifu
Male
Capricorn
44
74
Priest
Throw
Critical Quick
Equip Shield
Move-MP Up

Healing Staff
Genji Shield
Triangle Hat
Leather Outfit
Genji Gauntlet

Cure 2, Regen, Protect, Protect 2, Shell, Shell 2, Wall, Esuna
Shuriken, Sword

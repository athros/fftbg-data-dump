Player: !Red
Team: Red Team
Palettes: Red/Brown



AM3P Batman
Female
Aquarius
59
67
Chemist
Steal
PA Save
Sicken
Waterwalking

Hydra Bag

Golden Hairpin
Leather Outfit
Magic Gauntlet

Potion, X-Potion, Ether, Hi-Ether, Holy Water, Phoenix Down
Steal Shield



Zeroroute
Male
Leo
54
67
Mime

MA Save
Maintenance
Swim



Green Beret
Mystic Vest
Dracula Mantle

Mimic




SeedSC
Female
Leo
79
64
Thief
Item
Absorb Used MP
Throw Item
Move-HP Up

Orichalcum

Feather Hat
Chain Vest
Sprint Shoes

Gil Taking, Steal Shield, Steal Accessory
Potion, Hi-Potion, Antidote, Remedy, Phoenix Down



Denamda
Female
Gemini
57
79
Thief
Jump
Auto Potion
Halve MP
Waterwalking

Ninja Edge

Red Hood
Adaman Vest
Wizard Mantle

Steal Helmet, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Level Jump5, Vertical Jump8

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Baron Von Scrub
Male
Leo
73
46
Bard
Battle Skill
Parry
Long Status
Ignore Terrain

Ramia Harp

Golden Hairpin
Mythril Vest
Leather Mantle

Angel Song, Life Song, Cheer Song, Last Song, Diamond Blade
Head Break



Thaetreis
Female
Aquarius
50
75
Summoner
Draw Out
Sunken State
Short Charge
Fly

White Staff

Triangle Hat
Robe of Lords
Power Wrist

Moogle, Ifrit, Golem, Carbunkle, Odin, Cyclops
Koutetsu, Murasame, Kiyomori, Kikuichimoji



Firesheath
Male
Scorpio
45
65
Squire
Draw Out
Abandon
Short Charge
Move+1

Poison Bow
Mythril Shield
Twist Headband
Judo Outfit
Sprint Shoes

Accumulate, Heal, Cheer Up, Wish, Scream
Koutetsu



Roqqqpsi
Male
Aquarius
50
53
Thief
Elemental
HP Restore
Equip Bow
Move+2

Snipe Bow

Feather Hat
Wizard Outfit
Bracer

Steal Heart, Steal Armor
Pitfall, Water Ball, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind

Final Bets: white - 8 bets for 5,355G (54.1%, x0.85); purple - 8 bets for 4,552G (45.9%, x1.18)

white bets:
DavenIII: 2,500G (46.7%, 124,692G)
powergems: 1,000G (18.7%, 5,124G)
E_Ballard: 724G (13.5%, 45,786G)
AllInBot: 715G (13.4%, 715G)
Lazarus_DS: 116G (2.2%, 116G)
Evewho: 100G (1.9%, 5,507G)
Aldrammech: 100G (1.9%, 3,790G)
mrfripps: 100G (1.9%, 7,213G)

purple bets:
Willjin: 1,500G (33.0%, 4,577G)
BirbBrainsBot: 1,000G (22.0%, 162,712G)
dcmoragn315: 1,000G (22.0%, 1,989G)
Tarheels218: 481G (10.6%, 481G)
datadrivenbot: 200G (4.4%, 65,987G)
getthemoneyz: 176G (3.9%, 1,731,196G)
mirapoix: 100G (2.2%, 6,288G)
roqqqpsi: 95G (2.1%, 1,063G)

Final Bets: red - 18 bets for 15,170G (61.1%, x0.64); black - 17 bets for 9,674G (38.9%, x1.57)

red bets:
joewcarson: 3,015G (19.9%, 3,015G)
killth3kid: 2,000G (13.2%, 50,577G)
Miyokari: 1,500G (9.9%, 12,454G)
Aldrammech: 1,413G (9.3%, 1,413G)
ShintaroNayaka: 1,100G (7.3%, 1,100G)
beowulfrulez: 1,089G (7.2%, 1,089G)
HaateXIII: 1,037G (6.8%, 2,034G)
Laserman1000: 904G (6.0%, 9,804G)
pandasforsale: 777G (5.1%, 24,727G)
E_Ballard: 600G (4.0%, 600G)
TheGuesty: 500G (3.3%, 31,000G)
maakur_: 424G (2.8%, 424G)
Tekigi: 200G (1.3%, 200G)
RoboticNomad: 200G (1.3%, 5,486G)
byrdturbo: 111G (0.7%, 4,415G)
ass_brains: 100G (0.7%, 1,799G)
datadrivenbot: 100G (0.7%, 11,908G)
Primeval_33: 100G (0.7%, 7,326G)

black bets:
rico_flex: 1,500G (15.5%, 27,247G)
Grimmace45: 1,197G (12.4%, 23,948G)
ungabunga_bot: 1,000G (10.3%, 434,668G)
BirbBrainsBot: 1,000G (10.3%, 155,397G)
NicoSavoy: 1,000G (10.3%, 6,615G)
ndubya: 961G (9.9%, 961G)
Breakdown777: 500G (5.2%, 9,575G)
Baron_von_Scrub: 474G (4.9%, 5,308G)
gooseyourself: 460G (4.8%, 460G)
Estan_AD: 428G (4.4%, 428G)
ACSpree: 292G (3.0%, 292G)
potgodtopdog: 222G (2.3%, 19,694G)
EnemyController: 200G (2.1%, 234,710G)
Tetsuwan_Melty: 150G (1.6%, 1,620G)
benticore: 100G (1.0%, 1,484G)
Dymntd: 100G (1.0%, 18,809G)
getthemoneyz: 90G (0.9%, 653,474G)

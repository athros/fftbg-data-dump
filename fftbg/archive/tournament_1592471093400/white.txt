Player: !White
Team: White Team
Palettes: White/Blue



Paperthinhere
Female
Cancer
50
65
Geomancer
Time Magic
Distribute
Short Status
Swim

Koutetsu Knife
Ice Shield
Headgear
Mystic Vest
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Stop, Float, Reflect, Demi, Stabilize Time, Galaxy Stop



CT 5 Holy
Female
Capricorn
47
53
Geomancer
Summon Magic
Damage Split
Equip Bow
Ignore Terrain

Star Bag
Diamond Shield
Flash Hat
Brigandine
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Moogle, Ramuh, Ifrit, Carbunkle, Odin, Leviathan, Fairy



Vorap
Male
Pisces
80
71
Archer
Punch Art
Speed Save
Magic Defense UP
Move-HP Up

Cross Bow
Genji Shield
Diamond Helmet
Leather Outfit
Feather Mantle

Charge+3, Charge+4, Charge+7
Pummel, Seal Evil



Lydian C
Female
Aries
73
65
Monk
Throw
Abandon
Doublehand
Waterbreathing

Panther Bag

Flash Hat
Mystic Vest
Spike Shoes

Earth Slash, Purification, Seal Evil
Shuriken, Bomb

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rubenflonne
Female
Leo
54
56
Wizard
Battle Skill
MA Save
Short Charge
Ignore Terrain

Ice Rod

Triangle Hat
White Robe
Genji Gauntlet

Fire 2, Fire 3, Fire 4, Bolt 2, Ice, Ice 2, Ice 4, Death
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword



Technominari
Female
Leo
53
79
Priest
Draw Out
MA Save
Concentrate
Waterwalking

Healing Staff

Ribbon
Leather Outfit
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Protect, Esuna
Murasame, Heaven's Cloud, Kikuichimoji



HaplessOne
Female
Libra
64
66
Oracle
Time Magic
Damage Split
Doublehand
Move+1

Ivory Rod

Flash Hat
Wizard Outfit
Bracer

Life Drain, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Haste, Float, Reflect, Quick, Stabilize Time



Blain Cooper
Female
Libra
69
57
Knight
Charge
Absorb Used MP
Dual Wield
Move-HP Up

Defender
Giant Axe
Platinum Helmet
Genji Armor
Feather Mantle

Head Break, Shield Break, Weapon Break, Magic Break, Speed Break
Charge+1, Charge+5, Charge+7, Charge+10

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Evewho
Female
Pisces
65
48
Geomancer
Draw Out
Critical Quick
Equip Knife
Ignore Height

Hidden Knife
Platinum Shield
Ribbon
Mystic Vest
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Chompie
Female
Scorpio
67
50
Samurai
Black Magic
Catch
Defense UP
Move+3

Bizen Boat

Gold Helmet
Silk Robe
Power Wrist

Bizen Boat, Kiyomori
Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3



Mudrockk
Male
Virgo
79
65
Geomancer
Item
Arrow Guard
Halve MP
Jump+2

Sleep Sword
Escutcheon
Ribbon
Black Robe
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down



Reddwind
Male
Cancer
47
51
Knight
White Magic
PA Save
Doublehand
Ignore Height

Chaos Blade

Gold Helmet
Linen Cuirass
Diamond Armlet

Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Dark Sword, Night Sword
Raise, Regen, Shell 2, Wall, Esuna, Holy

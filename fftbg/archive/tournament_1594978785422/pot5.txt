Final Bets: blue - 10 bets for 4,409G (75.8%, x0.32); green - 8 bets for 1,409G (24.2%, x3.13)

blue bets:
randgridr: 1,988G (45.1%, 1,988G)
dantayystv: 1,000G (22.7%, 20,209G)
reddwind_: 500G (11.3%, 54,143G)
AllInBot: 200G (4.5%, 200G)
Evewho: 200G (4.5%, 2,295G)
Lydian_C: 121G (2.7%, 63,769G)
blorpy_: 100G (2.3%, 471G)
Chompie: 100G (2.3%, 9,380G)
Miku_Shikhu: 100G (2.3%, 15,934G)
nerigera: 100G (2.3%, 1,319G)

green bets:
getthemoneyz: 450G (31.9%, 1,301,909G)
red_celt: 276G (19.6%, 276G)
datadrivenbot: 200G (14.2%, 30,636G)
Arkreaver: 100G (7.1%, 10,373G)
DouglasDragonThePoet: 100G (7.1%, 594G)
UnderOneLight: 100G (7.1%, 1,464G)
JohnDuggins: 100G (7.1%, 7,438G)
BirbBrainsBot: 83G (5.9%, 26,770G)

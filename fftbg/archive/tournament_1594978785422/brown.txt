Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ShintaroNayaka
Female
Sagittarius
60
76
Thief
Charge
Dragon Spirit
Defense UP
Ignore Height

Kunai

Thief Hat
Wizard Outfit
Spike Shoes

Steal Shield
Charge+2, Charge+5



UmaiJam
Male
Sagittarius
62
43
Geomancer
Talk Skill
HP Restore
Dual Wield
Move+3

Kiyomori
Slasher
Feather Hat
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Invitation, Death Sentence, Rehabilitate



Hasterious
Female
Cancer
41
73
Geomancer
Talk Skill
Counter Tackle
Magic Attack UP
Jump+3

Slasher
Platinum Shield
Black Hood
Chain Vest
Defense Armlet

Pitfall, Hallowed Ground, Will-O-Wisp, Blizzard
Persuade, Threaten, Solution, Insult, Negotiate, Rehabilitate



Scottie
Male
Gemini
55
56
Lancer
White Magic
PA Save
Equip Bow
Retreat

Mythril Bow

Mythril Helmet
Genji Armor
Diamond Armlet

Level Jump3, Vertical Jump8
Cure 2, Raise, Raise 2, Protect 2, Shell 2, Wall, Esuna

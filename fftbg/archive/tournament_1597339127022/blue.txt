Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JonnyCue
Female
Leo
61
52
Mime

Counter
Short Status
Move-MP Up



Ribbon
Chain Vest
Salty Rage

Mimic




Nekojin
Female
Pisces
62
58
Oracle
Time Magic
PA Save
Equip Knife
Ignore Height

Mythril Knife

Twist Headband
Chameleon Robe
Vanish Mantle

Blind, Spell Absorb, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Haste



Latebit
Male
Sagittarius
42
60
Squire
Elemental
Damage Split
Equip Armor
Jump+2

Flail
Ice Shield
Cross Helmet
White Robe
Setiemson

Dash, Throw Stone, Heal, Tickle, Yell
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball



Scurg
Male
Pisces
63
53
Summoner
Throw
HP Restore
Long Status
Move-MP Up

Healing Staff

Twist Headband
Earth Clothes
Germinas Boots

Moogle, Shiva, Ramuh, Golem, Bahamut, Odin, Salamander, Silf
Bomb, Knife

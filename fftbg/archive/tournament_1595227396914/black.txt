Player: !Black
Team: Black Team
Palettes: Black/Red



Hasterious
Female
Cancer
47
45
Wizard
Draw Out
Brave Save
Martial Arts
Ignore Height



Leather Hat
Chain Vest
Diamond Armlet

Fire, Fire 2, Bolt, Bolt 2, Ice 4, Empower
Asura, Koutetsu, Bizen Boat, Heaven's Cloud



TheMurkGnome
Male
Cancer
40
69
Wizard
Draw Out
Counter
Maintenance
Teleport

Mythril Knife

Thief Hat
Chameleon Robe
Defense Ring

Fire 4, Bolt, Ice 3, Ice 4, Frog, Flare
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Deathmaker06
Monster
Sagittarius
74
80
Red Dragon










Mirapoix
Female
Serpentarius
70
68
Priest
Basic Skill
Speed Save
Defend
Fly

Flame Whip

Twist Headband
Chameleon Robe
Reflect Ring

Shell, Shell 2, Wall, Esuna, Holy
Dash, Throw Stone, Tickle, Cheer Up

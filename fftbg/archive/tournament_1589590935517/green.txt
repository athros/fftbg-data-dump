Player: !Green
Team: Green Team
Palettes: Green/White



Zmoses
Male
Aries
75
62
Bard
Basic Skill
Speed Save
Equip Sword
Retreat

Coral Sword

Red Hood
Bronze Armor
108 Gems

Life Song, Battle Song, Magic Song, Diamond Blade, Sky Demon
Accumulate, Dash, Heal, Fury, Wish



FoeSquirrel
Male
Aquarius
50
61
Mime

Sunken State
Equip Shield
Waterwalking


Gold Shield
Golden Hairpin
Black Robe
Elf Mantle

Mimic




HawkSick
Male
Gemini
47
72
Calculator
Demon Skill
Counter
Short Status
Teleport

Ice Rod

Cachusha
Leather Armor
Reflect Ring

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



The Pengwin
Female
Leo
78
66
Oracle
Steal
Parry
Equip Gun
Waterbreathing

Papyrus Codex

Twist Headband
Black Robe
Rubber Shoes

Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory

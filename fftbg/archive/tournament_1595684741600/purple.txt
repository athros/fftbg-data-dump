Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nekojin
Male
Gemini
39
72
Knight
Yin Yang Magic
Counter
Short Status
Move+1

Slasher
Gold Shield
Crystal Helmet
Diamond Armor
Small Mantle

Armor Break, Shield Break, Weapon Break, Speed Break
Poison, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic



Baconbacon1207
Female
Gemini
62
40
Time Mage
Throw
Dragon Spirit
Equip Armor
Swim

Gokuu Rod

Leather Helmet
Mythril Vest
Feather Boots

Haste 2, Slow, Immobilize, Stabilize Time
Shuriken, Dictionary



Fenaen
Monster
Taurus
55
46
Black Goblin










ALY327
Female
Pisces
51
75
Oracle
Punch Art
Hamedo
Secret Hunt
Move+2

Cypress Rod

Leather Hat
Silk Robe
108 Gems

Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie, Sleep
Spin Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil

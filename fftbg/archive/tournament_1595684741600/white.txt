Player: !White
Team: White Team
Palettes: White/Blue



Ruleof5
Female
Aries
57
39
Archer
Jump
Abandon
Short Charge
Move+1

Ultimus Bow

Black Hood
Power Sleeve
Jade Armlet

Charge+1, Charge+2, Charge+4, Charge+10
Level Jump8, Vertical Jump8



Miku Shikhu
Monster
Taurus
78
51
Ahriman










E Ballard
Male
Leo
38
55
Chemist
Yin Yang Magic
Blade Grasp
Short Status
Move+3

Cultist Dagger

Headgear
Wizard Outfit
Magic Gauntlet

Potion, Remedy
Blind, Poison, Pray Faith, Confusion Song



RaIshtar
Female
Aries
56
65
Geomancer
Yin Yang Magic
Mana Shield
Short Charge
Retreat

Battle Axe
Escutcheon
Flash Hat
Earth Clothes
Power Wrist

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard
Poison, Spell Absorb, Confusion Song

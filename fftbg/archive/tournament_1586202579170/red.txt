Player: !Red
Team: Red Team
Palettes: Red/Brown



Kronikle
Monster
Aries
71
46
Steel Giant










AlenaZarek
Female
Gemini
43
46
Dancer
Elemental
Parry
Defend
Levitate

Hydra Bag

Flash Hat
Wizard Robe
Setiemson

Slow Dance, Disillusion, Last Dance
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Phrossi V
Male
Scorpio
80
72
Oracle
Charge
PA Save
Magic Defense UP
Move+2

Battle Bamboo

Feather Hat
Mythril Vest
Diamond Armlet

Blind, Poison, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep
Charge+1, Charge+3, Charge+4, Charge+5



SeniorBunk
Female
Scorpio
52
61
Wizard
Steal
Earplug
Defend
Fly

Dragon Rod

Flash Hat
Brigandine
Bracer

Bolt, Bolt 3, Ice, Ice 3, Frog, Flare
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield

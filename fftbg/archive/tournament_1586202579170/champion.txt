Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



DarrenDinosaurs
Female
Aquarius
64
40
Calculator
Yin Yang Magic
Mana Shield
Defend
Teleport

Musk Rod

Twist Headband
Leather Outfit
Salty Rage

CT, Prime Number, 5, 4, 3
Spell Absorb, Paralyze, Dark Holy



Mirapoix
Female
Taurus
53
61
Wizard
Math Skill
Arrow Guard
Attack UP
Jump+2

Dragon Rod

Golden Hairpin
Adaman Vest
Power Wrist

Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Ice 4, Empower, Frog, Flare
CT, Height, Prime Number



Kronikle
Female
Gemini
74
56
Priest
Draw Out
Faith Up
Halve MP
Ignore Height

Wizard Staff

Triangle Hat
Linen Robe
Red Shoes

Cure, Cure 2, Raise, Regen, Shell, Shell 2, Wall, Esuna, Magic Barrier
Koutetsu, Kiyomori



Technominari
Female
Scorpio
67
60
Priest
Jump
Regenerator
Short Status
Jump+1

Morning Star

Twist Headband
Adaman Vest
Salty Rage

Cure 3, Cure 4, Raise, Regen, Protect, Protect 2, Shell, Esuna
Level Jump2, Vertical Jump8

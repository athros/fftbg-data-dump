Final Bets: purple - 5 bets for 3,302G (46.6%, x1.15); brown - 7 bets for 3,790G (53.4%, x0.87)

purple bets:
prince_rogers_nelson_: 1,166G (35.3%, 1,166G)
Zachara: 1,036G (31.4%, 106,536G)
sinnyil2: 800G (24.2%, 800G)
amiture: 200G (6.1%, 4,029G)
datadrivenbot: 100G (3.0%, 42,656G)

brown bets:
Lydian_C: 1,200G (31.7%, 14,927G)
BirbBrainsBot: 1,000G (26.4%, 61,988G)
Evewho: 444G (11.7%, 444G)
AllInBot: 317G (8.4%, 317G)
gorgewall: 301G (7.9%, 21,917G)
getthemoneyz: 278G (7.3%, 966,905G)
Lythe_Caraker: 250G (6.6%, 109,192G)

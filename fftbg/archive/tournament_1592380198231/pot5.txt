Final Bets: red - 10 bets for 5,073G (55.8%, x0.79); green - 8 bets for 4,019G (44.2%, x1.26)

red bets:
Draconis345: 1,000G (19.7%, 65,629G)
BirbBrainsBot: 1,000G (19.7%, 62,859G)
maleblackfiora: 1,000G (19.7%, 9,241G)
AllInBot: 593G (11.7%, 593G)
Lythe_Caraker: 500G (9.9%, 109,410G)
amiture: 500G (9.9%, 3,829G)
Bulleta: 250G (4.9%, 966G)
Lydian_C: 120G (2.4%, 15,972G)
serperemagus: 100G (2.0%, 6,803G)
getthemoneyz: 10G (0.2%, 967,147G)

green bets:
bruubarg: 1,000G (24.9%, 18,409G)
Evewho: 831G (20.7%, 831G)
nifboy: 667G (16.6%, 667G)
prince_rogers_nelson_: 548G (13.6%, 548G)
CT_5_Holy: 308G (7.7%, 308G)
gorgewall: 301G (7.5%, 22,179G)
soren_of_tyto: 264G (6.6%, 264G)
datadrivenbot: 100G (2.5%, 42,556G)

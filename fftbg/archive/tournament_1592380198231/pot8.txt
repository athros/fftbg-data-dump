Final Bets: white - 14 bets for 10,838G (67.2%, x0.49); champion - 11 bets for 5,279G (32.8%, x2.05)

white bets:
KyleWonToLiveForever: 2,712G (25.0%, 2,712G)
nifboy: 1,980G (18.3%, 1,980G)
Digitalsocrates: 1,000G (9.2%, 58,649G)
Draconis345: 1,000G (9.2%, 66,059G)
sinnyil2: 800G (7.4%, 800G)
Lydian_C: 760G (7.0%, 15,083G)
porkchop5158: 551G (5.1%, 551G)
maleblackfiora: 500G (4.6%, 10,033G)
CassiePhoenix: 404G (3.7%, 404G)
gorgewall: 401G (3.7%, 22,612G)
AllInBot: 280G (2.6%, 280G)
Lythe_Caraker: 250G (2.3%, 109,715G)
serperemagus: 100G (0.9%, 6,846G)
datadrivenbot: 100G (0.9%, 42,420G)

champion bets:
BirbBrainsBot: 1,000G (18.9%, 61,922G)
BStarTV: 1,000G (18.9%, 4,751G)
getthemoneyz: 664G (12.6%, 966,389G)
vorap: 500G (9.5%, 151,546G)
amiture: 500G (9.5%, 3,525G)
Mermydon: 500G (9.5%, 900G)
prince_rogers_nelson_: 465G (8.8%, 2,513G)
Zachara: 250G (4.7%, 105,500G)
reddwind_: 200G (3.8%, 1,494G)
windsah: 100G (1.9%, 5,910G)
Evewho: 100G (1.9%, 1,244G)

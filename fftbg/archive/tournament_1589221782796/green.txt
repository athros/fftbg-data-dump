Player: !Green
Team: Green Team
Palettes: Green/White



Miyokari
Female
Leo
67
64
Calculator
Yin Yang Magic
PA Save
Magic Attack UP
Waterbreathing

Battle Folio

Flash Hat
Mystic Vest
108 Gems

CT, Prime Number
Blind, Poison, Doubt Faith, Silence Song, Blind Rage, Confusion Song



Killth3kid
Female
Serpentarius
72
47
Lancer
Basic Skill
Distribute
Beastmaster
Jump+2

Gungnir
Round Shield
Genji Helmet
Crystal Mail
Dracula Mantle

Level Jump4, Vertical Jump3
Dash, Heal, Wish



Sairentozon7
Female
Pisces
59
78
Calculator
Time Magic
Meatbone Slash
Equip Axe
Teleport

Morning Star

Headgear
Silk Robe
Leather Mantle

CT, Prime Number, 4, 3
Haste, Slow, Demi, Demi 2, Stabilize Time



Error72
Female
Cancer
53
77
Ninja
Draw Out
Arrow Guard
Equip Gun
Jump+3

Fairy Harp
Battle Folio
Holy Miter
Judo Outfit
Red Shoes

Shuriken, Bomb, Ninja Sword
Asura, Koutetsu, Kiyomori, Kikuichimoji

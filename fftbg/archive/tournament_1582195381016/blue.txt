Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zodiac192133
Female
Gemini
69
49
Wizard
Draw Out
Dragon Spirit
Concentrate
Move+2

Dragon Rod

Red Hood
Silk Robe
Red Shoes

Fire 2, Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Frog
Bizen Boat



Illudia
Female
Taurus
76
73
Summoner
Steal
Abandon
Equip Bow
Jump+1

Yoichi Bow

Thief Hat
Adaman Vest
Feather Mantle

Moogle, Ifrit, Salamander, Cyclops
Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Arm Aim



Revvy
Monster
Capricorn
71
47
Mindflayer










Axiom Zero
Female
Cancer
59
40
Samurai
Time Magic
Dragon Spirit
Attack UP
Ignore Terrain

Kikuichimoji

Cross Helmet
Linen Cuirass
Genji Gauntlet

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Haste, Reflect, Stabilize Time, Meteor

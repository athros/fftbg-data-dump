Final Bets: purple - 10 bets for 5,322G (37.9%, x1.64); brown - 12 bets for 8,705G (62.1%, x0.61)

purple bets:
Thyrandaal: 1,000G (18.8%, 22,078G)
ColetteMSLP: 1,000G (18.8%, 40,175G)
Nizaha: 604G (11.3%, 8,271G)
Zagorsek: 570G (10.7%, 1,141G)
Lanshaft: 528G (9.9%, 26,199G)
Aldrammech: 528G (9.9%, 528G)
AllInBot: 514G (9.7%, 514G)
getthemoneyz: 378G (7.1%, 1,423,304G)
Firesheath: 100G (1.9%, 9,348G)
ANFz: 100G (1.9%, 57,860G)

brown bets:
reinoe: 2,000G (23.0%, 145,173G)
UmaiJam: 2,000G (23.0%, 54,095G)
BirbBrainsBot: 1,000G (11.5%, 176,748G)
nekojin: 1,000G (11.5%, 20,186G)
prince_rogers_nelson_: 806G (9.3%, 806G)
HaateXIII: 604G (6.9%, 604G)
SephDarkheart: 420G (4.8%, 14,295G)
ArlanKels: 244G (2.8%, 3,060G)
TasisSai: 200G (2.3%, 10,146G)
datadrivenbot: 200G (2.3%, 46,131G)
roofiepops: 123G (1.4%, 9,892G)
amhamor: 108G (1.2%, 108G)

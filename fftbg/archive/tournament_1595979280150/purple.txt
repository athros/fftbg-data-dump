Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ruleof5
Female
Capricorn
49
76
Archer
Jump
Auto Potion
Equip Gun
Lava Walking

Blaze Gun
Escutcheon
Holy Miter
Mythril Vest
Elf Mantle

Charge+2, Charge+4, Charge+20
Level Jump5, Vertical Jump7



ColetteMSLP
Male
Serpentarius
67
50
Squire
White Magic
Caution
Short Status
Jump+2

Battle Axe
Flame Shield
Gold Helmet
Carabini Mail
Angel Ring

Accumulate, Heal, Tickle, Yell, Cheer Up, Wish
Cure, Cure 2, Cure 3, Raise, Reraise, Protect, Esuna, Holy



Lythe Caraker
Female
Scorpio
49
79
Mediator
Elemental
Sunken State
Magic Defense UP
Swim

Madlemgen

Flash Hat
Wizard Robe
Genji Gauntlet

Invitation, Solution, Mimic Daravon, Refute
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Daveb
Female
Capricorn
70
72
Ninja
Item
Damage Split
Throw Item
Move+3

Morning Star
Flail
Flash Hat
Mystic Vest
Elf Mantle

Sword, Spear
Hi-Potion, Eye Drop, Echo Grass, Remedy

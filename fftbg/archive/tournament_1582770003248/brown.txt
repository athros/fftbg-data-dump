Player: !Brown
Team: Brown Team
Palettes: Brown/Green



GrauKhan
Male
Scorpio
45
74
Geomancer
Black Magic
Absorb Used MP
Doublehand
Move+2

Blood Sword

Black Hood
Wizard Outfit
Feather Boots

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Fire 2, Fire 4, Bolt, Bolt 2



Hales Bopp It
Female
Cancer
57
63
Knight
Steal
Sunken State
Sicken
Retreat

Ragnarok

Bronze Helmet
Maximillian
Magic Ring

Head Break, Shield Break, Weapon Break, Power Break
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory



Kijin151
Female
Cancer
52
80
Squire
Jump
MA Save
Equip Gun
Teleport

Stone Gun
Platinum Shield
Iron Helmet
Rubber Costume
Small Mantle

Dash, Throw Stone, Heal, Cheer Up
Level Jump3, Vertical Jump2



Fspll
Female
Gemini
42
73
Dancer
Throw
Parry
Equip Axe
Ignore Terrain

Flail

Holy Miter
Black Costume
Leather Mantle

Witch Hunt, Wiznaibus, Polka Polka, Obsidian Blade, Nether Demon
Shuriken, Bomb

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



MalakoFox
Female
Scorpio
68
47
Geomancer
Yin Yang Magic
Earplug
Equip Bow
Levitate

Ice Bow

Twist Headband
Wizard Robe
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Poison, Blind Rage, Dispel Magic



Athropos1
Male
Aries
56
60
Knight
Punch Art
Parry
Secret Hunt
Waterbreathing

Ragnarok

Genji Helmet
Silk Robe
Defense Armlet

Shield Break, Power Break, Mind Break
Spin Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil



MagikarpPKMN
Female
Sagittarius
66
70
Time Mage
Basic Skill
Sunken State
Long Status
Levitate

Mace of Zeus

Black Hood
Wizard Robe
Genji Gauntlet

Haste, Slow 2, Float, Reflect, Quick, Demi, Stabilize Time
Heal



ShinoGuy
Male
Capricorn
78
43
Chemist
Steal
HP Restore
Dual Wield
Jump+1

Orichalcum
Cute Bag
Green Beret
Earth Clothes
Genji Gauntlet

Potion, Ether, Hi-Ether, Echo Grass, Soft, Remedy
Gil Taking, Steal Shield, Steal Weapon, Leg Aim

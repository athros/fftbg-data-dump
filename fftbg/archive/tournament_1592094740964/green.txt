Player: !Green
Team: Green Team
Palettes: Green/White



DustBirdEX
Female
Virgo
78
63
Calculator
Demon Skill
Hamedo
Beastmaster
Retreat

Papyrus Codex
Flame Shield
Red Hood
Black Robe
Jade Armlet

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



BlackFireUK
Female
Aries
58
43
Mime

Mana Shield
Secret Hunt
Ignore Height



Holy Miter
Leather Outfit
Rubber Shoes

Mimic




Baron Von Scrub
Female
Scorpio
47
49
Dancer
Punch Art
Auto Potion
Short Status
Teleport 2

Ryozan Silk

Green Beret
Light Robe
Leather Mantle

Slow Dance, Disillusion, Obsidian Blade
Pummel, Revive, Seal Evil



Flameatron
Male
Sagittarius
45
75
Archer
Time Magic
Auto Potion
Defense UP
Jump+3

Ice Bow

Green Beret
Adaman Vest
Defense Armlet

Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Haste, Slow, Immobilize, Reflect

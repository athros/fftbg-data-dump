Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Taurus
68
48
Oracle
Item
Counter
Equip Shield
Retreat

Ivory Rod
Escutcheon
Triangle Hat
Mystic Vest
Feather Boots

Poison, Spell Absorb, Silence Song, Blind Rage, Foxbird, Paralyze, Petrify, Dark Holy
Potion, Ether, Hi-Ether, Holy Water



Lawnboxer
Female
Libra
45
67
Archer
Summon Magic
Counter Flood
Concentrate
Waterbreathing

Poison Bow
Genji Shield
Red Hood
Judo Outfit
Sprint Shoes

Charge+1
Moogle, Titan, Salamander, Fairy



NeoKevlar
Male
Aquarius
69
70
Bard
Summon Magic
Critical Quick
Halve MP
Fly

Bloody Strings

Cachusha
Wizard Outfit
Sprint Shoes

Life Song, Battle Song, Last Song
Shiva, Titan, Golem, Leviathan, Salamander



Lali Lulelo
Female
Aquarius
79
67
Monk
Time Magic
HP Restore
Maintenance
Waterwalking



Headgear
Adaman Vest
Feather Boots

Pummel, Wave Fist, Purification, Revive
Haste, Stop, Reflect, Quick, Demi, Stabilize Time

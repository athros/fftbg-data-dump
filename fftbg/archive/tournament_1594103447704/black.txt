Player: !Black
Team: Black Team
Palettes: Black/Red



HyC81
Male
Gemini
77
42
Samurai
Time Magic
PA Save
Short Charge
Waterbreathing

Gungnir

Platinum Helmet
Linen Cuirass
Elf Mantle

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Haste, Haste 2, Slow 2, Stop, Quick, Stabilize Time



UmaiJam
Male
Leo
52
49
Knight
Basic Skill
Auto Potion
Equip Bow
Teleport

Poison Bow
Platinum Shield
Circlet
Linen Cuirass
108 Gems

Armor Break, Shield Break, Power Break, Stasis Sword
Dash, Throw Stone, Heal, Fury



Choco Joe
Female
Pisces
51
75
Time Mage
Elemental
Counter
Attack UP
Waterwalking

Cypress Rod

Golden Hairpin
Earth Clothes
Dracula Mantle

Haste, Haste 2, Immobilize, Reflect, Demi, Demi 2, Stabilize Time
Hallowed Ground, Static Shock, Quicksand, Sand Storm, Lava Ball



Amishninja
Female
Gemini
70
74
Ninja
Item
Earplug
Attack UP
Move-HP Up

Scorpion Tail
Main Gauche
Flash Hat
Black Costume
Feather Boots

Shuriken, Bomb
Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

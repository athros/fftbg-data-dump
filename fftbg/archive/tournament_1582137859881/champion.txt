Player: !zChamp
Team: Champion Team
Palettes: Black/Red



AniZero
Male
Sagittarius
54
67
Samurai
Time Magic
Counter
Dual Wield
Move+1

Partisan
Javelin
Diamond Helmet
Linen Robe
Angel Ring

Bizen Boat, Muramasa
Haste, Reflect, Demi, Stabilize Time



Sinnyil2
Male
Aries
64
58
Mediator
Draw Out
Auto Potion
Dual Wield
Waterwalking

Romanda Gun
Blaze Gun
Feather Hat
Chameleon Robe
N-Kai Armlet

Mimic Daravon, Refute
Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



LordSDB
Monster
Leo
60
75
Hydra










Mellichor
Male
Scorpio
65
74
Monk
Battle Skill
Distribute
Attack UP
Retreat



Green Beret
Brigandine
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil
Head Break, Armor Break, Power Break, Justice Sword, Dark Sword

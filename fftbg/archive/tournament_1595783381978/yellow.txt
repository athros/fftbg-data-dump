Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Slotheye17
Monster
Leo
53
40
Draugr










Zarath79
Female
Scorpio
60
69
Dancer
Talk Skill
Parry
Monster Talk
Waterbreathing

Cashmere

Feather Hat
Power Sleeve
Jade Armlet

Wiznaibus, Nether Demon, Dragon Pit
Solution, Refute, Rehabilitate



ShintaroNayaka
Female
Taurus
49
80
Mediator
Yin Yang Magic
Brave Save
Equip Armor
Teleport

Blaze Gun

Thief Hat
Chain Mail
Reflect Ring

Invitation, Threaten, Preach, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Spell Absorb, Life Drain, Pray Faith, Foxbird, Sleep



Meta Five
Female
Gemini
65
59
Geomancer
Item
Counter Flood
Throw Item
Fly

Kikuichimoji
Platinum Shield
Triangle Hat
Silk Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Gusty Wind
Potion, Maiden's Kiss, Soft, Phoenix Down

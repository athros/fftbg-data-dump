Player: !Green
Team: Green Team
Palettes: Green/White



RurouniGeo
Male
Gemini
60
65
Geomancer
Punch Art
Faith Up
Equip Axe
Ignore Terrain

Flail
Platinum Shield
Headgear
White Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Chakra, Seal Evil



Typicalfanboy
Male
Scorpio
50
64
Lancer
Charge
Catch
Equip Knife
Ignore Terrain

Ninja Edge
Diamond Shield
Diamond Helmet
Plate Mail
Bracer

Level Jump4, Vertical Jump3
Charge+2, Charge+7, Charge+10



AzuranGaming
Female
Capricorn
65
55
Knight
Basic Skill
Counter Tackle
Long Status
Waterbreathing

Slasher
Bronze Shield
Circlet
Chain Mail
Germinas Boots

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword
Dash, Heal, Cheer Up



WzzrdBlzrd
Female
Aquarius
42
45
Squire
White Magic
MA Save
Equip Sword
Move-HP Up

Ragnarok

Grand Helmet
Diamond Armor
Spike Shoes

Accumulate, Throw Stone, Yell
Cure, Cure 3, Raise, Reraise, Shell 2, Esuna

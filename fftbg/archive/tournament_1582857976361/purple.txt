Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ArcticXC
Female
Leo
45
46
Dancer
Battle Skill
Damage Split
Equip Bow
Swim

Windslash Bow

Triangle Hat
Mystic Vest
Dracula Mantle

Slow Dance, Polka Polka, Void Storage, Nether Demon
Power Break, Mind Break, Night Sword



Narcius
Male
Sagittarius
48
79
Bard
White Magic
Faith Up
Dual Wield
Waterbreathing

Fairy Harp
Bloody Strings
Leather Hat
Leather Outfit
108 Gems

Angel Song, Battle Song, Magic Song, Diamond Blade
Cure, Reraise, Regen, Shell, Esuna



Ring Wyrm
Female
Sagittarius
60
80
Wizard
Math Skill
HP Restore
Magic Defense UP
Teleport

Rod

Green Beret
Linen Robe
Angel Ring

Bolt 3, Bolt 4, Empower
CT, Height, Prime Number, 4, 3



Raruni
Male
Libra
68
81
Wizard
Draw Out
Speed Save
Dual Wield
Waterwalking

Wizard Rod
Orichalcum
Twist Headband
Black Robe
Germinas Boots

Fire 3, Bolt 3, Bolt 4
Heaven's Cloud

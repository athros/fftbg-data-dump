Player: !Red
Team: Red Team
Palettes: Red/Brown



FreedomNM
Female
Pisces
51
57
Wizard
White Magic
Counter
Magic Attack UP
Waterwalking

Poison Rod

Twist Headband
Wizard Robe
Red Shoes

Fire 3, Bolt 4, Ice, Ice 4, Frog
Cure 2, Cure 3, Protect, Protect 2, Shell, Esuna



DudeMonkey77
Female
Pisces
44
54
Archer
Item
Damage Split
Equip Sword
Jump+1

Ice Brand
Platinum Shield
Flash Hat
Leather Outfit
108 Gems

Charge+1, Charge+4
Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down



ArchKnightX
Male
Pisces
60
42
Monk
Charge
Arrow Guard
Magic Defense UP
Move-MP Up



Triangle Hat
Mythril Vest
Germinas Boots

Spin Fist, Wave Fist, Purification, Chakra, Seal Evil
Charge+1, Charge+3, Charge+5



StryderSeven
Male
Leo
52
42
Squire
Item
Counter Tackle
Sicken
Levitate

Flame Whip
Ice Shield
Black Hood
Maximillian
Dracula Mantle

Accumulate, Throw Stone, Heal, Cheer Up, Wish, Scream
Potion, Hi-Potion, X-Potion, Ether, Elixir, Antidote, Soft, Holy Water, Phoenix Down

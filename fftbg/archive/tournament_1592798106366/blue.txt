Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CrownOfHorns
Male
Cancer
78
52
Samurai
Summon Magic
Regenerator
Defense UP
Teleport

Koutetsu Knife

Genji Helmet
Platinum Armor
N-Kai Armlet

Asura, Bizen Boat
Moogle, Ramuh, Golem, Bahamut, Silf, Fairy



Mesmaster
Male
Gemini
76
66
Mime

Damage Split
Dual Wield
Move+3



Headgear
Mystic Vest
Jade Armlet

Mimic




Mrfripps
Male
Aries
61
56
Squire
Elemental
Brave Save
Equip Armor
Waterbreathing

Night Killer
Escutcheon
Gold Helmet
Black Robe
Red Shoes

Accumulate, Heal, Tickle, Yell, Scream
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



BStarTV
Male
Sagittarius
59
66
Monk
Basic Skill
Dragon Spirit
Dual Wield
Ignore Terrain



Flash Hat
Chain Vest
Spike Shoes

Earth Slash, Secret Fist, Revive
Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish

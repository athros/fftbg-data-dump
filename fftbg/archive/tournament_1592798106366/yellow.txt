Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Heroebal
Male
Taurus
45
62
Samurai
Punch Art
Counter
Equip Gun
Teleport

Fairy Harp

Diamond Helmet
Bronze Armor
Angel Ring

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Pummel, Purification, Seal Evil



SSwing
Female
Capricorn
70
78
Wizard
Talk Skill
MA Save
Equip Knife
Move+2

Dragon Rod

Feather Hat
Judo Outfit
Dracula Mantle

Bolt 2, Ice 4
Invitation, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Rehabilitate



Silentkaster
Female
Cancer
59
78
Time Mage
Draw Out
Faith Save
Beastmaster
Move+1

Sage Staff

Headgear
Power Sleeve
Feather Mantle

Haste, Haste 2, Stabilize Time
Asura, Bizen Boat



Laserman1000
Female
Pisces
53
75
Dancer
Talk Skill
Meatbone Slash
Equip Polearm
Move+2

Spear

Flash Hat
Earth Clothes
Bracer

Wiznaibus, Polka Polka, Disillusion, Last Dance, Nether Demon
Persuade, Threaten, Preach, Death Sentence

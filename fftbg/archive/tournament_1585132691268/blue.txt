Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grininda
Female
Leo
53
67
Mediator
Steal
HP Restore
Defense UP
Swim

Papyrus Codex

Green Beret
Adaman Vest
Feather Boots

Invitation, Praise, Preach, Solution, Death Sentence, Rehabilitate
Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Arm Aim



The Pengwin
Male
Aquarius
51
73
Knight
Punch Art
Sunken State
Attack UP
Move+3

Ice Brand
Flame Shield
Bronze Helmet
Silk Robe
N-Kai Armlet

Head Break, Power Break, Mind Break, Stasis Sword, Night Sword, Surging Sword
Spin Fist, Earth Slash, Chakra, Revive, Seal Evil



Powergems
Male
Cancer
47
42
Samurai
Throw
Speed Save
Equip Armor
Jump+2

Heaven's Cloud

Red Hood
Diamond Armor
Magic Gauntlet

Asura, Bizen Boat, Murasame, Kikuichimoji
Shuriken, Bomb, Sword



Draconis345
Male
Aquarius
77
76
Thief
Black Magic
Counter Magic
Defense UP
Levitate

Dagger

Headgear
Rubber Costume
Leather Mantle

Steal Heart, Steal Helmet, Steal Shield, Steal Accessory
Fire, Fire 2, Fire 3, Ice, Ice 4, Empower

Final Bets: white - 15 bets for 10,810G (53.5%, x0.87); black - 15 bets for 9,412G (46.5%, x1.15)

white bets:
sinnyil2: 4,229G (39.1%, 8,458G)
Aldrammech: 1,444G (13.4%, 1,444G)
roofiepops: 1,219G (11.3%, 1,219G)
BirbBrainsBot: 1,000G (9.3%, 134,441G)
POGpope: 562G (5.2%, 562G)
getthemoneyz: 516G (4.8%, 1,331,831G)
killth3kid: 456G (4.2%, 750G)
Treafa: 276G (2.6%, 1,136G)
ZicaX: 232G (2.1%, 1,863G)
DeathTaxesAndAnime: 200G (1.9%, 1,987G)
iBardic: 200G (1.9%, 4,530G)
GrauKhan: 176G (1.6%, 176G)
nhammen: 100G (0.9%, 6,486G)
Monopool: 100G (0.9%, 3,517G)
Legionx13: 100G (0.9%, 1,994G)

black bets:
reinoe: 3,000G (31.9%, 79,808G)
twelfthrootoftwo: 1,366G (14.5%, 2,680G)
HaateXIII: 1,322G (14.0%, 1,322G)
Thyrandaal: 700G (7.4%, 8,756G)
Zeroroute: 600G (6.4%, 600G)
BasedAzazel: 500G (5.3%, 1,480G)
ColetteMSLP: 300G (3.2%, 1,848G)
OtherBrand: 296G (3.1%, 948G)
silentkaster: 250G (2.7%, 13,585G)
SUGRboi: 216G (2.3%, 216G)
gorgewall: 201G (2.1%, 6,069G)
AllInBot: 200G (2.1%, 200G)
datadrivenbot: 200G (2.1%, 32,706G)
Freezerman47: 161G (1.7%, 161G)
ANFz: 100G (1.1%, 40,244G)

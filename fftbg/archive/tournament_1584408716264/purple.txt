Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zagorsek
Female
Leo
57
74
Dancer
Black Magic
Speed Save
Doublehand
Move+2

Star Bag

Leather Hat
Linen Robe
Power Wrist

Slow Dance, Polka Polka, Void Storage, Nether Demon
Fire, Fire 2, Fire 3, Bolt 3, Ice 3, Empower



ForagerCats
Female
Sagittarius
55
63
Calculator
Time Magic
Earplug
Equip Knife
Move-MP Up

Dagger

Twist Headband
Judo Outfit
Elf Mantle

Height, Prime Number, 5, 4
Haste, Haste 2, Reflect, Quick, Stabilize Time



Meta Five
Monster
Scorpio
53
61
Plague










DudeMonkey77
Female
Libra
45
72
Summoner
Basic Skill
Critical Quick
Short Charge
Ignore Height

Rod

Black Hood
Linen Robe
Diamond Armlet

Moogle, Titan, Carbunkle, Bahamut, Fairy
Throw Stone, Heal, Scream

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ass Brains
Female
Aquarius
51
55
Chemist
Basic Skill
Mana Shield
Long Status
Move-HP Up

Star Bag

Feather Hat
Wizard Outfit
Small Mantle

Potion, Hi-Potion, X-Potion, Elixir, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Accumulate, Heal, Yell, Wish



MrUbiq
Female
Taurus
57
50
Samurai
Black Magic
Regenerator
Long Status
Teleport

Spear

Bronze Helmet
Carabini Mail
108 Gems

Koutetsu, Bizen Boat, Heaven's Cloud
Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Empower, Frog



Sinnyil2
Male
Sagittarius
66
74
Oracle
Battle Skill
Counter
Sicken
Fly

Battle Bamboo

Red Hood
Mystic Vest
108 Gems

Blind, Doubt Faith, Silence Song, Sleep, Petrify
Armor Break, Shield Break, Weapon Break, Mind Break



Carchan131
Female
Serpentarius
43
65
Samurai
Basic Skill
Counter Magic
Martial Arts
Move+1



Platinum Helmet
Mythril Armor
Red Shoes

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Dash, Yell, Wish

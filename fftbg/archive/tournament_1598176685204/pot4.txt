Final Bets: purple - 9 bets for 3,305G (45.8%, x1.18); brown - 6 bets for 3,906G (54.2%, x0.85)

purple bets:
sinnyil2: 1,004G (30.4%, 1,004G)
roqqqpsi: 483G (14.6%, 1,934G)
ZephyrBurst83: 375G (11.3%, 375G)
skipsandwiches: 360G (10.9%, 3,443G)
AllInBot: 335G (10.1%, 335G)
WhiteTigress: 300G (9.1%, 12,227G)
datadrivenbot: 200G (6.1%, 64,734G)
WhattayaBrian: 148G (4.5%, 148G)
Mister_Quof: 100G (3.0%, 2,570G)

brown bets:
BirbBrainsBot: 1,000G (25.6%, 155,955G)
NicoSavoy: 1,000G (25.6%, 9,274G)
Seaweed_B: 1,000G (25.6%, 52,483G)
Sairentozon7: 696G (17.8%, 696G)
Lazarus_DS: 200G (5.1%, 375G)
getthemoneyz: 10G (0.3%, 1,726,125G)

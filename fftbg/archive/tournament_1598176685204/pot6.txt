Final Bets: white - 9 bets for 3,762G (56.5%, x0.77); purple - 7 bets for 2,891G (43.5%, x1.30)

white bets:
BirbBrainsBot: 1,000G (26.6%, 155,322G)
Sairentozon7: 696G (18.5%, 696G)
Mudrockk: 666G (17.7%, 3,353G)
skipsandwiches: 360G (9.6%, 4,000G)
roqqqpsi: 319G (8.5%, 2,130G)
PKayge: 300G (8.0%, 45,604G)
Lazarus_DS: 239G (6.4%, 239G)
AllInBot: 100G (2.7%, 100G)
getthemoneyz: 82G (2.2%, 1,726,188G)

purple bets:
JunmaiRabbit: 1,000G (34.6%, 1,000G)
ZephyrBurst83: 749G (25.9%, 749G)
WhattayaBrian: 441G (15.3%, 441G)
WhiteTigress: 300G (10.4%, 12,692G)
datadrivenbot: 200G (6.9%, 65,043G)
gorgewall: 101G (3.5%, 19,977G)
Mister_Quof: 100G (3.5%, 2,725G)

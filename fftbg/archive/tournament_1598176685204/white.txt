Player: !White
Team: White Team
Palettes: White/Blue



NicoSavoy
Female
Libra
41
73
Monk
Battle Skill
Counter Flood
Concentrate
Jump+2



Green Beret
Adaman Vest
Diamond Armlet

Earth Slash, Purification, Chakra
Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword



Roqqqpsi
Male
Taurus
52
58
Priest
Throw
Earplug
Equip Bow
Move-HP Up

Perseus Bow

Thief Hat
Wizard Outfit
Spike Shoes

Cure, Cure 2, Raise, Raise 2, Reraise, Protect, Shell, Holy
Shuriken, Staff



Lazarus DS
Male
Sagittarius
64
70
Knight
Punch Art
PA Save
Equip Sword
Jump+3

Chaos Blade
Escutcheon
Barbuta
Leather Armor
Magic Gauntlet

Armor Break, Shield Break, Magic Break, Power Break, Stasis Sword, Surging Sword
Wave Fist, Purification, Revive



ALY327
Male
Sagittarius
40
50
Archer
White Magic
Speed Save
Doublehand
Swim

Blaze Gun

Twist Headband
Wizard Outfit
Leather Mantle

Charge+1, Charge+4, Charge+5, Charge+7, Charge+10
Cure, Cure 3, Cure 4, Raise, Raise 2, Protect 2, Wall, Esuna

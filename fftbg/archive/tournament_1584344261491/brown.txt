Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Darthchi71
Female
Pisces
63
62
Monk
Talk Skill
Brave Up
Short Charge
Fly



Triangle Hat
Wizard Outfit
Genji Gauntlet

Wave Fist, Purification, Revive, Seal Evil
Praise, Threaten, Death Sentence, Insult, Negotiate, Refute, Rehabilitate



ZergTwitch
Female
Pisces
53
66
Priest
Talk Skill
Counter Magic
Dual Wield
Move+3

Flail
Scorpion Tail
Holy Miter
Light Robe
Salty Rage

Cure 3, Cure 4, Raise, Regen, Protect, Shell, Wall, Esuna, Holy
Invitation, Praise, Insult, Refute



Chronoxtrigger
Male
Aquarius
73
51
Chemist
Time Magic
Distribute
Beastmaster
Move+1

Romanda Gun

Golden Hairpin
Chain Vest
108 Gems

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft
Haste, Slow, Slow 2, Stop, Quick, Stabilize Time



HaateXIII
Female
Leo
50
50
Time Mage
Throw
Counter Magic
Equip Shield
Lava Walking

White Staff
Flame Shield
Black Hood
Clothes
Rubber Shoes

Haste, Slow 2, Stop, Immobilize, Reflect, Quick, Meteor
Shuriken, Knife

Player: !Red
Team: Red Team
Palettes: Red/Brown



Toksu
Monster
Aries
48
58
Black Goblin










Mirapoix
Male
Sagittarius
56
80
Mediator
Item
Absorb Used MP
Throw Item
Move+2

Stone Gun

Black Hood
Clothes
Jade Armlet

Threaten, Death Sentence, Insult
Potion, X-Potion, Ether, Hi-Ether, Phoenix Down



WhiteTigress
Male
Capricorn
44
65
Squire
Summon Magic
Distribute
Equip Gun
Jump+3

Mythril Gun
Bronze Shield
Barette
Mystic Vest
Diamond Armlet

Throw Stone, Heal, Cheer Up
Ifrit, Salamander, Cyclops



Maeveen
Female
Cancer
55
70
Calculator
White Magic
Meatbone Slash
Magic Attack UP
Fly

Battle Folio

Leather Hat
Mythril Vest
Cursed Ring

CT, Height, Prime Number, 5, 3
Cure 2, Cure 4, Raise, Raise 2, Protect 2, Shell 2, Esuna

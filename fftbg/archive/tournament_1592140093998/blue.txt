Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bruubarg
Female
Taurus
70
44
Ninja
Yin Yang Magic
HP Restore
Doublehand
Jump+3

Mage Masher

Leather Hat
Power Sleeve
Diamond Armlet

Shuriken, Bomb
Life Drain, Pray Faith, Confusion Song, Dispel Magic, Paralyze, Petrify



Skillomono
Male
Aquarius
74
67
Knight
Item
Arrow Guard
Throw Item
Move+3

Defender
Venetian Shield
Gold Helmet
Mythril Armor
Feather Mantle

Speed Break, Surging Sword
Potion, Hi-Potion, X-Potion



Galkife
Male
Leo
71
43
Bard
Talk Skill
HP Restore
Equip Polearm
Move+1

Mythril Spear

Holy Miter
Clothes
Dracula Mantle

Cheer Song, Battle Song, Magic Song, Diamond Blade, Sky Demon
Invitation, Praise, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



Chronolynx42
Female
Libra
47
44
Thief
Item
Distribute
Throw Item
Retreat

Orichalcum

Headgear
Earth Clothes
Bracer

Steal Heart, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Ether, Hi-Ether, Echo Grass, Phoenix Down

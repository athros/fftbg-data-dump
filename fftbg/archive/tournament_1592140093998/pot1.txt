Final Bets: red - 10 bets for 7,015G (46.6%, x1.15); blue - 9 bets for 8,046G (53.4%, x0.87)

red bets:
Alaquane: 2,420G (34.5%, 2,420G)
Breakdown777: 1,500G (21.4%, 73,569G)
serperemagus: 966G (13.8%, 966G)
bruubarg: 525G (7.5%, 525G)
prince_rogers_nelson_: 500G (7.1%, 1,379G)
AllInBot: 404G (5.8%, 404G)
Evewho: 200G (2.9%, 13,168G)
KasugaiRoastedPeas: 200G (2.9%, 4,052G)
d4rr1n: 200G (2.9%, 6,127G)
datadrivenbot: 100G (1.4%, 39,299G)

blue bets:
NovaKnight21: 1,706G (21.2%, 3,412G)
ShintaroNayaka: 1,238G (15.4%, 8,698G)
BirbBrainsBot: 1,000G (12.4%, 18,771G)
Thyrandaal: 1,000G (12.4%, 20,156G)
getthemoneyz: 748G (9.3%, 955,500G)
maakur_: 692G (8.6%, 692G)
DeathTaxesAndAnime: 616G (7.7%, 616G)
reddwind_: 546G (6.8%, 546G)
TheGuesty: 500G (6.2%, 69,500G)

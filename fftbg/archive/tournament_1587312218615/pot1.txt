Final Bets: red - 18 bets for 10,085G (42.9%, x1.33); blue - 21 bets for 13,405G (57.1%, x0.75)

red bets:
Nickyfive: 1,500G (14.9%, 5,966G)
catfashions: 1,000G (9.9%, 8,840G)
MantisFinch: 1,000G (9.9%, 26,851G)
TheMM42: 1,000G (9.9%, 22,012G)
Aldrammech: 1,000G (9.9%, 54,883G)
BirbBrainsBot: 963G (9.5%, 1,428G)
ungabunga_bot: 955G (9.5%, 175,642G)
Tougou: 500G (5.0%, 1,072G)
SkylerBunny: 500G (5.0%, 80,703G)
RyuTsuno: 500G (5.0%, 12,016G)
Webb_MD: 367G (3.6%, 367G)
kaidykat: 200G (2.0%, 1,000G)
mexskacin: 100G (1.0%, 7,127G)
Jeeboheebo: 100G (1.0%, 9,647G)
FoeSquirrel: 100G (1.0%, 2,635G)
IndecisiveNinja: 100G (1.0%, 115,043G)
sect_cor: 100G (1.0%, 26,659G)
datadrivenbot: 100G (1.0%, 1,543G)

blue bets:
volgrathemoose: 2,001G (14.9%, 156,041G)
DustBirdEX: 2,000G (14.9%, 16,900G)
nifboy: 1,871G (14.0%, 1,871G)
ColetteMSLP: 1,200G (9.0%, 51,790G)
Lordminsc: 1,001G (7.5%, 28,602G)
flowinprose: 1,000G (7.5%, 72,968G)
Dexef: 555G (4.1%, 4,904G)
fenaen: 500G (3.7%, 24,044G)
elkydeluxe: 500G (3.7%, 15,204G)
Breakdown777: 500G (3.7%, 36,605G)
Kamira69: 400G (3.0%, 4,002G)
waterwatereverywhere: 380G (2.8%, 380G)
TheGuesty: 350G (2.6%, 16,306G)
Lanshaft: 332G (2.5%, 797G)
Lavatis: 228G (1.7%, 228G)
Sitiran: 155G (1.2%, 1,550G)
AllInBot: 100G (0.7%, 100G)
FriendSquirrel: 100G (0.7%, 24,529G)
ericzubat: 100G (0.7%, 802G)
RunicMagus: 100G (0.7%, 43,048G)
getthemoneyz: 32G (0.2%, 513,415G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Jeeboheebo
Female
Leo
43
40
Wizard
Jump
Critical Quick
Magic Defense UP
Jump+1

Wizard Rod

Holy Miter
Mystic Vest
Sprint Shoes

Fire 3, Fire 4, Bolt 3, Ice, Ice 3
Level Jump5, Vertical Jump7



Pandasforsale
Male
Capricorn
78
44
Knight
Basic Skill
Counter Flood
Long Status
Ignore Height

Save the Queen
Round Shield
Bronze Helmet
White Robe
Elf Mantle

Head Break, Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword
Accumulate, Heal, Tickle, Wish



Gooseyourself
Male
Cancer
68
67
Chemist
Elemental
Auto Potion
Secret Hunt
Jump+2

Stone Gun

Flash Hat
Chain Vest
Reflect Ring

Potion, X-Potion, Ether, Antidote, Echo Grass, Remedy, Phoenix Down
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Galkife
Male
Cancer
79
53
Wizard
Yin Yang Magic
Damage Split
Equip Gun
Lava Walking

Ramia Harp

Triangle Hat
Black Robe
Bracer

Fire, Bolt 4, Ice, Ice 2, Ice 3, Ice 4, Empower
Blind, Poison, Doubt Faith, Foxbird, Dispel Magic, Petrify

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Aldrammech
Male
Cancer
72
78
Ninja
Yin Yang Magic
Parry
Sicken
Waterwalking

Dagger
Flame Whip
Triangle Hat
Mystic Vest
Magic Gauntlet

Shuriken
Poison, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy



Lord Gwarth
Female
Virgo
79
53
Geomancer
Draw Out
Brave Save
Doublehand
Jump+1

Rune Blade

Green Beret
Wizard Robe
Magic Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Asura, Heaven's Cloud, Muramasa



WhiteTigress
Male
Scorpio
41
68
Squire
Battle Skill
Critical Quick
Dual Wield
Teleport

Giant Axe
Blind Knife
Triangle Hat
Leather Armor
Jade Armlet

Throw Stone, Heal, Yell, Cheer Up, Fury, Wish
Head Break, Armor Break, Shield Break, Speed Break, Stasis Sword



CorpusCav
Male
Gemini
55
39
Oracle
Item
Damage Split
Dual Wield
Move+3

Octagon Rod
Gokuu Rod
Green Beret
Wizard Outfit
Small Mantle

Poison, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss

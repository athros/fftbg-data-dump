Final Bets: purple - 18 bets for 13,958G (74.1%, x0.35); brown - 11 bets for 4,881G (25.9%, x2.86)

purple bets:
JumbocactuarX27: 4,632G (33.2%, 4,632G)
Shalloween: 1,500G (10.7%, 26,640G)
CorpusCav: 983G (7.0%, 983G)
evontno: 903G (6.5%, 903G)
dogsandcatsand: 896G (6.4%, 896G)
E_Ballard: 776G (5.6%, 61,449G)
thunderducker: 636G (4.6%, 636G)
Lord_Gwarth: 555G (4.0%, 1,216G)
beccasetsfire: 500G (3.6%, 4,350G)
JonnyCue: 464G (3.3%, 464G)
pplvee1: 448G (3.2%, 72,183G)
Mushufasa_: 333G (2.4%, 333G)
old_overholt_: 300G (2.1%, 6,245G)
PuzzleSecretary: 292G (2.1%, 573G)
dingdingboom: 241G (1.7%, 241G)
datadrivenbot: 200G (1.4%, 64,453G)
AllInBot: 199G (1.4%, 199G)
Aldrammech: 100G (0.7%, 11,940G)

brown bets:
JCBooBot: 1,000G (20.5%, 23,368G)
BirbBrainsBot: 1,000G (20.5%, 78,326G)
Snowfats: 500G (10.2%, 18,310G)
run_with_stone_GUNs: 488G (10.0%, 3,951G)
douchetron: 464G (9.5%, 464G)
DustBirdEX: 404G (8.3%, 404G)
skipsandwiches: 376G (7.7%, 8,568G)
getthemoneyz: 250G (5.1%, 1,766,891G)
ko2q: 199G (4.1%, 12,517G)
TakKerna: 100G (2.0%, 183G)
arumz: 100G (2.0%, 2,674G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Berke
Monster
Virgo
53
62
Behemoth










Heathcliff
Male
Aries
65
43
Oracle
Elemental
Meatbone Slash
Short Charge
Retreat

Battle Folio

Triangle Hat
Power Sleeve
Jade Armlet

Blind, Poison, Pray Faith, Zombie, Dispel Magic
Pitfall, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Sighard
Monster
Gemini
72
68
Red Panther










Carolynn
Female
Gemini
78
38
Ninja
Jump
Distribute
Concentrate
Levitate

Ninja Edge
Flail
Black Hood
Power Sleeve
Red Shoes

Shuriken, Bomb
Level Jump3, Vertical Jump8

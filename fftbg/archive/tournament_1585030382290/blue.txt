Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Waterwatereverywhere
Male
Aquarius
66
66
Oracle
Draw Out
PA Save
Equip Armor
Jump+1

Musk Rod

Golden Hairpin
Linen Robe
108 Gems

Blind, Life Drain, Doubt Faith, Dark Holy
Koutetsu, Kikuichimoji



AoiTsukiYuri
Female
Cancer
51
40
Ninja
Draw Out
Speed Save
Equip Bow
Lava Walking

Hunting Bow
Poison Bow
Green Beret
Brigandine
Small Mantle

Ninja Sword, Axe
Asura, Bizen Boat, Murasame, Kiyomori, Chirijiraden



Cougboi
Female
Aries
80
67
Summoner
Draw Out
Sunken State
Equip Polearm
Waterwalking

Cypress Rod

Holy Miter
White Robe
Bracer

Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Odin, Salamander, Fairy, Lich
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



ELzukkor
Female
Capricorn
49
69
Knight
Basic Skill
Distribute
Maintenance
Move+3

Ragnarok

Mythril Helmet
Chain Mail
Genji Gauntlet

Armor Break, Shield Break, Weapon Break, Night Sword
Dash, Throw Stone, Heal, Tickle, Cheer Up, Ultima

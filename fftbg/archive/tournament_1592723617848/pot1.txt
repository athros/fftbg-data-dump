Final Bets: red - 9 bets for 3,218G (73.4%, x0.36); blue - 7 bets for 1,165G (26.6%, x2.76)

red bets:
Zetchryn: 1,000G (31.1%, 6,525G)
Laserman1000: 600G (18.6%, 12,900G)
biske13: 417G (13.0%, 417G)
gorgewall: 301G (9.4%, 14,194G)
cougboi: 300G (9.3%, 9,746G)
KasugaiRoastedPeas: 200G (6.2%, 9,336G)
twelfthrootoftwo: 200G (6.2%, 6,952G)
datadrivenbot: 100G (3.1%, 44,841G)
Ring_Wyrm: 100G (3.1%, 920G)

blue bets:
getthemoneyz: 380G (32.6%, 1,012,100G)
Spuzzmocker: 268G (23.0%, 268G)
HawkSick: 200G (17.2%, 3,667G)
Lydian_C: 120G (10.3%, 210,113G)
Firesheath: 100G (8.6%, 25,216G)
Ross_from_Cali: 77G (6.6%, 1,308G)
BirbBrainsBot: 20G (1.7%, 39,781G)

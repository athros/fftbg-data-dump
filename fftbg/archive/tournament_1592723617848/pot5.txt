Final Bets: red - 10 bets for 4,423G (41.9%, x1.38); green - 7 bets for 6,123G (58.1%, x0.72)

red bets:
LAGBOT30000: 1,400G (31.7%, 1,400G)
BirbBrainsBot: 1,000G (22.6%, 40,331G)
reddwind_: 500G (11.3%, 46,441G)
josephiroth_143: 364G (8.2%, 364G)
gorgewall: 301G (6.8%, 14,539G)
KyleWonToLiveForever: 292G (6.6%, 292G)
KasugaiRoastedPeas: 200G (4.5%, 9,208G)
Evewho: 200G (4.5%, 2,714G)
datadrivenbot: 100G (2.3%, 44,911G)
getthemoneyz: 66G (1.5%, 1,011,750G)

green bets:
Ross_from_Cali: 3,319G (54.2%, 3,319G)
reinoe: 2,000G (32.7%, 11,344G)
twelfthrootoftwo: 300G (4.9%, 7,605G)
HawkSick: 200G (3.3%, 3,267G)
pancake11112: 104G (1.7%, 104G)
Ring_Wyrm: 100G (1.6%, 1,058G)
cougboi: 100G (1.6%, 10,406G)

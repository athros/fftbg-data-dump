Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lawndough
Male
Sagittarius
64
51
Squire
Punch Art
Counter Flood
Equip Bow
Retreat

Bow Gun
Mythril Shield
Leather Hat
Brigandine
Reflect Ring

Fury
Purification, Chakra, Revive



Seawalker777
Male
Scorpio
54
52
Monk
Summon Magic
Arrow Guard
Equip Gun
Lava Walking

Romanda Gun

Leather Hat
Secret Clothes
Genji Gauntlet

Wave Fist, Secret Fist, Purification
Ramuh, Titan, Carbunkle, Odin, Fairy



Grimmace45
Female
Libra
47
79
Wizard
White Magic
Absorb Used MP
Martial Arts
Move-HP Up



Ribbon
Light Robe
Wizard Mantle

Fire, Bolt, Ice, Ice 4
Cure 3, Regen, Protect 2, Shell, Shell 2, Esuna



Evewho
Female
Aquarius
67
50
Chemist
Steal
Counter Flood
Magic Defense UP
Move-MP Up

Hydra Bag

Triangle Hat
Mythril Vest
Jade Armlet

X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water
Steal Shield, Steal Accessory, Arm Aim

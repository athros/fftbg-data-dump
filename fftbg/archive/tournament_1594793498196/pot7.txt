Final Bets: yellow - 8 bets for 5,235G (40.9%, x1.44); black - 15 bets for 7,562G (59.1%, x0.69)

yellow bets:
ThreeMileIsland: 2,217G (42.3%, 2,217G)
superdevon1: 908G (17.3%, 9,085G)
BoneMiser: 600G (11.5%, 600G)
escobro: 500G (9.6%, 7,506G)
randgridr: 328G (6.3%, 328G)
datadrivenbot: 282G (5.4%, 39,973G)
AllInBot: 200G (3.8%, 200G)
cocomilko: 200G (3.8%, 2,822G)

black bets:
Kronikle: 1,279G (16.9%, 1,279G)
EnemyController: 1,111G (14.7%, 1,432,404G)
BirbBrainsBot: 1,000G (13.2%, 152,434G)
WitchHunterIX: 1,000G (13.2%, 41,080G)
upvla: 550G (7.3%, 8,257G)
Evewho: 512G (6.8%, 512G)
porkchop5158: 500G (6.6%, 34,190G)
KasugaiRoastedPeas: 306G (4.0%, 15,306G)
Lemonjohns: 300G (4.0%, 1,668G)
amiture: 300G (4.0%, 300G)
dem0nj0ns: 300G (4.0%, 4,156G)
getthemoneyz: 130G (1.7%, 1,278,912G)
Geffro1908: 124G (1.6%, 124G)
CT_5_Holy: 100G (1.3%, 4,856G)
nebukin: 50G (0.7%, 1,024G)

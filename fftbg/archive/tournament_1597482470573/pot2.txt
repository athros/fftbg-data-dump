Final Bets: green - 7 bets for 5,128G (66.8%, x0.50); yellow - 8 bets for 2,554G (33.2%, x2.01)

green bets:
ko2q: 1,614G (31.5%, 1,614G)
BirbBrainsBot: 1,000G (19.5%, 142,224G)
BaronHaynes: 1,000G (19.5%, 24,991G)
superdevon1: 740G (14.4%, 74,061G)
getthemoneyz: 492G (9.6%, 1,635,641G)
emptygoat: 182G (3.5%, 182G)
MinBetBot: 100G (2.0%, 8,848G)

yellow bets:
CassiePhoenix: 1,000G (39.2%, 3,512G)
ArrenJevleth: 500G (19.6%, 6,713G)
WhiteTigress: 348G (13.6%, 348G)
datadrivenbot: 200G (7.8%, 64,282G)
AllInBot: 182G (7.1%, 182G)
Lydian_C: 123G (4.8%, 2,581G)
gorgewall: 101G (4.0%, 9,222G)
Klednar21: 100G (3.9%, 1,561G)

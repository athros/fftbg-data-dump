Final Bets: red - 11 bets for 4,794G (54.9%, x0.82); blue - 7 bets for 3,941G (45.1%, x1.22)

red bets:
BirbBrainsBot: 1,000G (20.9%, 141,402G)
BaronHaynes: 1,000G (20.9%, 24,169G)
ko2q: 886G (18.5%, 886G)
E_Ballard: 604G (12.6%, 604G)
Aestheta: 439G (9.2%, 439G)
Drusiform: 200G (4.2%, 16,741G)
datadrivenbot: 200G (4.2%, 64,118G)
getthemoneyz: 164G (3.4%, 1,635,506G)
gorgewall: 101G (2.1%, 9,139G)
AllInBot: 100G (2.1%, 100G)
emptygoat: 100G (2.1%, 100G)

blue bets:
Willjin: 1,000G (25.4%, 18,281G)
WhiteTigress: 777G (19.7%, 777G)
superdevon1: 748G (19.0%, 74,809G)
Firesheath: 716G (18.2%, 716G)
ArrenJevleth: 500G (12.7%, 7,213G)
MinBetBot: 100G (2.5%, 8,948G)
Klednar21: 100G (2.5%, 1,661G)

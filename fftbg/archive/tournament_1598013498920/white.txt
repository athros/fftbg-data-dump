Player: !White
Team: White Team
Palettes: White/Blue



VolgraTheMoose
Male
Taurus
53
65
Ninja
Yin Yang Magic
Blade Grasp
Defend
Move+1

Hidden Knife
Sasuke Knife
Feather Hat
Mythril Vest
Magic Gauntlet

Bomb, Knife, Hammer, Wand
Poison, Spell Absorb, Doubt Faith, Zombie, Foxbird, Dispel Magic



Willjin
Male
Pisces
79
48
Bard
Basic Skill
Auto Potion
Equip Shield
Ignore Height

Bloody Strings
Ice Shield
Flash Hat
Plate Mail
Power Wrist

Cheer Song, Battle Song, Last Song, Space Storage, Hydra Pit
Dash, Throw Stone, Heal, Cheer Up



Dogsandcatsand
Female
Gemini
60
48
Wizard
Punch Art
Brave Save
Short Charge
Levitate

Wizard Rod

Leather Hat
Leather Outfit
Angel Ring

Fire, Fire 3, Bolt, Bolt 2, Bolt 3, Ice 2, Ice 3, Ice 4
Secret Fist, Purification, Seal Evil



NovaKnight21
Female
Aquarius
78
52
Monk
Draw Out
Absorb Used MP
Equip Shield
Fly


Venetian Shield
Headgear
Mystic Vest
Feather Boots

Pummel, Wave Fist, Purification
Koutetsu, Murasame, Heaven's Cloud, Kiyomori

Player: !Black
Team: Black Team
Palettes: Black/Red



Ko2q
Female
Gemini
41
48
Monk
Item
Regenerator
Long Status
Move-MP Up



Red Hood
Earth Clothes
Red Shoes

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Hi-Potion, Hi-Ether, Phoenix Down



Orestes92
Female
Aquarius
55
76
Oracle
Draw Out
Parry
Doublehand
Fly

Ivory Rod

Feather Hat
Chameleon Robe
Diamond Armlet

Blind, Poison, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Asura, Koutetsu



Jeanna
Female
Libra
72
55
Lancer
Summon Magic
Mana Shield
Equip Polearm
Fly

Spear
Crystal Shield
Iron Helmet
Linen Cuirass
Bracer

Level Jump5, Vertical Jump6
Moogle, Ramuh, Golem, Carbunkle, Bahamut



Ceadda
Male
Leo
61
44
Oracle
Elemental
PA Save
Equip Polearm
Fly

Cashmere

Leather Hat
Silk Robe
Battle Boots

Poison, Doubt Faith, Blind Rage, Confusion Song, Sleep, Petrify, Dark Holy
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Aislingeach
Male
Gemini
68
68
Archer
Yin Yang Magic
Counter Magic
Concentrate
Waterbreathing

Night Killer
Mythril Shield
Red Hood
Brigandine
Red Shoes

Charge+1, Charge+2, Charge+3, Charge+7
Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Confusion Song



UnholyCorsair
Male
Virgo
64
62
Ninja
Draw Out
Absorb Used MP
Short Charge
Ignore Height

Short Edge
Hidden Knife
Flash Hat
Clothes
Sprint Shoes

Shuriken, Bomb, Knife
Murasame, Kiyomori, Muramasa



Eltejongrande
Male
Aries
44
74
Wizard
Summon Magic
Counter Flood
Equip Sword
Move-MP Up

Heaven's Cloud

Barette
Silk Robe
Chantage

Fire, Fire 2, Ice, Ice 2, Ice 3
Moogle, Golem, Salamander, Lich



Genoguy
Female
Capricorn
62
69
Ninja
Summon Magic
Damage Split
Defense UP
Move+3

Spell Edge
Zorlin Shape
Triangle Hat
Power Sleeve
Leather Mantle

Bomb, Knife, Sword, Ninja Sword
Moogle, Shiva, Ifrit, Golem, Carbunkle, Fairy

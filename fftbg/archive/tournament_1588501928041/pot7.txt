Final Bets: blue - 10 bets for 14,596G (58.5%, x0.71); black - 8 bets for 10,350G (41.5%, x1.41)

blue bets:
HaplessOne: 8,600G (58.9%, 8,600G)
Mesmaster: 3,000G (20.6%, 55,799G)
Lydian_C: 1,200G (8.2%, 13,694G)
CorpusCav: 500G (3.4%, 17,753G)
leakimiko: 476G (3.3%, 476G)
twelfthrootoftwo: 300G (2.1%, 1,132G)
WrathfulRemy: 220G (1.5%, 220G)
Ultimate_Infinity: 100G (0.7%, 1,073G)
ungabunga_bot: 100G (0.7%, 323,991G)
datadrivenbot: 100G (0.7%, 11,753G)

black bets:
R_Raynos: 8,600G (83.1%, 35,375G)
BirbBrainsBot: 1,000G (9.7%, 189,402G)
FNCardascia_: 204G (2.0%, 204G)
getthemoneyz: 196G (1.9%, 603,831G)
witchlight: 100G (1.0%, 1,021G)
WireLord: 100G (1.0%, 1,681G)
Kyune: 100G (1.0%, 5,178G)
DrAntiSocial: 50G (0.5%, 8,455G)

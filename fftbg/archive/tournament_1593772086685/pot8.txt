Final Bets: blue - 10 bets for 7,068G (49.8%, x1.01); champion - 9 bets for 7,118G (50.2%, x0.99)

blue bets:
NovaKnight21: 2,000G (28.3%, 19,348G)
killth3kid: 1,266G (17.9%, 126,638G)
NicoSavoy: 1,000G (14.1%, 112,089G)
Mesmaster: 1,000G (14.1%, 43,158G)
VolgraTheMoose: 501G (7.1%, 6,415G)
Magicandy: 300G (4.2%, 29,520G)
danielrogercardoso: 300G (4.2%, 1,000G)
ColetteMSLP: 300G (4.2%, 2,358G)
gorgewall: 201G (2.8%, 5,109G)
datadrivenbot: 200G (2.8%, 54,266G)

champion bets:
UmaiJam: 3,600G (50.6%, 83,693G)
BirbBrainsBot: 1,000G (14.0%, 166,924G)
DustBirdEX: 720G (10.1%, 720G)
getthemoneyz: 684G (9.6%, 1,170,420G)
Dexsana: 464G (6.5%, 464G)
Lythe_Caraker: 250G (3.5%, 144,677G)
ar_tactic: 250G (3.5%, 64,335G)
enkikavlar: 100G (1.4%, 2,767G)
SephDarkheart: 50G (0.7%, 5,032G)

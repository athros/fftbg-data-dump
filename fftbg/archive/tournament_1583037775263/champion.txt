Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



ZephyrTempest
Female
Scorpio
75
38
Ninja
Time Magic
Counter
Equip Sword
Ignore Height

Excalibur
Spell Edge
Flash Hat
Clothes
Dracula Mantle

Bomb, Hammer
Haste, Haste 2, Slow 2, Stop, Immobilize, Quick, Stabilize Time, Meteor



Sotehr
Male
Scorpio
55
75
Mime

Absorb Used MP
Maintenance
Move+2


Gold Shield
Holy Miter
Chain Vest
Magic Gauntlet

Mimic




Ring Wyrm
Male
Taurus
42
60
Lancer
Summon Magic
Absorb Used MP
Doublehand
Waterbreathing

Obelisk

Cross Helmet
Carabini Mail
Angel Ring

Level Jump8, Vertical Jump7
Moogle, Ramuh, Titan, Carbunkle, Fairy, Lich



Galkife
Female
Scorpio
51
42
Dancer
Throw
Absorb Used MP
Dual Wield
Lava Walking

Persia
Cashmere
Headgear
Black Costume
Power Wrist

Witch Hunt, Wiznaibus, Last Dance, Nether Demon
Shuriken

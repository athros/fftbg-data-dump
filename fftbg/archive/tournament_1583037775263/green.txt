Player: !Green
Team: Green Team
Palettes: Green/White



Fspll
Male
Pisces
49
73
Thief
Draw Out
PA Save
Beastmaster
Levitate

Dagger

Green Beret
Clothes
Rubber Shoes

Steal Shield, Steal Weapon, Arm Aim, Leg Aim
Kikuichimoji



AniZero
Female
Virgo
64
53
Summoner
Draw Out
Damage Split
Short Charge
Swim

Wizard Rod

Feather Hat
Wizard Outfit
Magic Ring

Ramuh, Ifrit, Titan, Carbunkle, Leviathan, Silf
Asura, Heaven's Cloud, Kikuichimoji



SkylerBunny
Male
Aquarius
46
76
Summoner
Item
Parry
Equip Knife
Move-HP Up

Hidden Knife

Triangle Hat
Chain Vest
Battle Boots

Moogle, Shiva, Titan, Carbunkle, Bahamut, Odin
Potion, Ether, Echo Grass, Maiden's Kiss, Holy Water



TheUnforgivenRage
Female
Cancer
75
59
Squire
Summon Magic
Counter Flood
Beastmaster
Lava Walking

Giant Axe

Iron Helmet
Platinum Armor
N-Kai Armlet

Throw Stone, Heal
Moogle, Ramuh, Ifrit, Odin, Leviathan, Salamander

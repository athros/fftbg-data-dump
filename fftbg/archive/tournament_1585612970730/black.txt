Player: !Black
Team: Black Team
Palettes: Black/Red



DeathTaxesAndAnime
Female
Aries
71
59
Priest
Dance
Damage Split
Sicken
Swim

Morning Star

Black Hood
Silk Robe
Chantage

Raise, Reraise, Shell 2, Esuna
Witch Hunt, Slow Dance, Disillusion, Nameless Dance, Last Dance



Kai Shee
Female
Aquarius
62
60
Mediator
Basic Skill
Distribute
Short Status
Jump+1

Blaze Gun

Holy Miter
Black Robe
Leather Mantle

Praise, Preach, Rehabilitate
Accumulate, Cheer Up



KonzeraLive
Male
Sagittarius
72
50
Knight
Draw Out
Meatbone Slash
Maintenance
Lava Walking

Ragnarok
Round Shield
Diamond Helmet
Robe of Lords
Defense Armlet

Head Break, Shield Break, Speed Break, Power Break, Mind Break
Heaven's Cloud



Basmal
Male
Leo
71
63
Thief
Jump
Counter
Defend
Move-HP Up

Blood Sword

Feather Hat
Wizard Outfit
108 Gems

Steal Shield, Steal Status, Arm Aim
Level Jump8, Vertical Jump8

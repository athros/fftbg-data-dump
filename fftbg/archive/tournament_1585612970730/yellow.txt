Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Amadmet
Monster
Gemini
48
72
Blue Dragon










Evewho
Female
Virgo
53
44
Priest
Battle Skill
Abandon
Sicken
Waterwalking

Gold Staff

Feather Hat
Clothes
Cursed Ring

Cure 2, Raise, Raise 2, Regen, Holy
Magic Break, Stasis Sword



InOzWeTrust
Male
Aquarius
51
80
Monk
Basic Skill
Absorb Used MP
Beastmaster
Retreat



Black Hood
Judo Outfit
Setiemson

Secret Fist, Purification, Revive
Dash, Throw Stone, Heal



GrayGhostGaming
Female
Capricorn
79
55
Wizard
Battle Skill
Counter Flood
Concentrate
Swim

Wizard Rod

Headgear
Mythril Vest
Germinas Boots

Fire, Bolt, Bolt 2
Weapon Break, Magic Break, Stasis Sword, Dark Sword

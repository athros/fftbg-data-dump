Player: !Black
Team: Black Team
Palettes: Black/Red



Volgrathemoose
Female
Scorpio
47
74
Time Mage
Steal
Parry
Short Charge
Move-MP Up

Cypress Rod

Thief Hat
White Robe
Sprint Shoes

Haste, Float, Reflect, Quick, Stabilize Time
Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Steal Status



CorpusCav
Female
Leo
54
52
Mime

Auto Potion
Martial Arts
Ignore Terrain



Holy Miter
Black Costume
Reflect Ring

Mimic




Serperemagus
Male
Pisces
48
43
Priest
Item
Auto Potion
Throw Item
Move+2

White Staff

Feather Hat
Light Robe
Rubber Shoes

Cure 2, Raise, Raise 2, Reraise, Protect 2
Potion, Hi-Ether, Antidote, Echo Grass, Remedy



HASTERIOUS
Male
Cancer
60
68
Archer
Throw
Speed Save
Doublehand
Ignore Height

Silver Bow

Circlet
Earth Clothes
Defense Ring

Charge+2
Bomb

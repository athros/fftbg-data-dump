Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Kalabain
Male
Capricorn
54
62
Thief
Battle Skill
HP Restore
Martial Arts
Levitate

Main Gauche

Thief Hat
Brigandine
Magic Gauntlet

Steal Heart, Steal Armor
Head Break, Shield Break, Magic Break



Ugedakka
Male
Aries
74
51
Oracle
Charge
Critical Quick
Defend
Move+3

Battle Folio

Headgear
White Robe
Genji Gauntlet

Silence Song, Blind Rage, Confusion Song, Paralyze, Petrify
Charge+4, Charge+5, Charge+10



NIghtdew14
Female
Pisces
64
42
Dancer
Charge
Faith Save
Equip Gun
Move+2

Ramia Harp

Golden Hairpin
Silk Robe
Wizard Mantle

Slow Dance, Polka Polka, Disillusion
Charge+1, Charge+2, Charge+5, Charge+10



Ring Wyrm
Male
Capricorn
48
80
Knight
Elemental
Mana Shield
Defense UP
Jump+1

Coral Sword
Diamond Shield
Platinum Helmet
Chameleon Robe
Small Mantle

Head Break, Shield Break, Power Break, Surging Sword
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheAbandonedShoji
Female
Aries
39
78
Time Mage
Black Magic
Auto Potion
Attack UP
Move+1

Octagon Rod

Red Hood
Black Robe
108 Gems

Haste, Slow, Immobilize, Float
Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 2, Ice 4



SQUiDSQUARKLIN
Male
Leo
43
74
Mime

Counter
Short Status
Move+2



Green Beret
Earth Clothes
Setiemson

Mimic




Roofiepops
Male
Aquarius
66
49
Ninja
Charge
Critical Quick
Equip Bow
Move-HP Up

Ice Bow

Flash Hat
Mythril Vest
Cursed Ring

Shuriken, Bomb
Charge+1, Charge+3, Charge+4, Charge+20



Kronikle
Male
Aquarius
79
73
Archer
Basic Skill
Counter Magic
Doublehand
Lava Walking

Mythril Bow

Feather Hat
Judo Outfit
Angel Ring

Charge+1, Charge+3, Charge+5
Cheer Up, Fury

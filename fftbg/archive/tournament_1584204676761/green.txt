Player: !Green
Team: Green Team
Palettes: Green/White



Mtueni
Male
Sagittarius
48
63
Thief
Jump
Faith Up
Doublehand
Teleport

Blind Knife

Twist Headband
Black Costume
Red Shoes

Gil Taking, Steal Heart, Steal Status, Arm Aim
Level Jump4, Vertical Jump2



RongRongArts
Female
Gemini
57
71
Chemist
Draw Out
Counter Magic
Short Charge
Move+3

Main Gauche

Black Hood
Earth Clothes
Angel Ring

Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Koutetsu, Murasame, Kiyomori



1twistedpuppy
Male
Gemini
53
75
Ninja
Yin Yang Magic
Parry
Maintenance
Ignore Terrain

Sasuke Knife
Orichalcum
Red Hood
Mythril Vest
Cursed Ring

Shuriken, Knife, Hammer
Blind, Zombie, Dispel Magic, Paralyze, Dark Holy



Lali Lulelo
Male
Taurus
62
78
Archer
Steal
Arrow Guard
Doublehand
Retreat

Glacier Gun

Green Beret
Mystic Vest
Wizard Mantle

Charge+10
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Steal Status, Arm Aim, Leg Aim

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Grashmu
Female
Cancer
66
69
Geomancer
Battle Skill
Damage Split
Halve MP
Move+3

Masamune
Gold Shield
Black Hood
Chain Vest
Vanish Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball
Head Break, Armor Break, Shield Break, Justice Sword, Explosion Sword



Kuroda222
Female
Leo
42
66
Summoner
Battle Skill
MP Restore
Martial Arts
Swim

Wizard Rod

Golden Hairpin
Mythril Vest
Rubber Shoes

Moogle, Golem, Leviathan, Salamander
Head Break, Armor Break, Speed Break, Dark Sword, Surging Sword



LegalBuddha
Female
Scorpio
58
53
Priest
Summon Magic
Damage Split
Short Charge
Move+1

Oak Staff

Feather Hat
Light Robe
Bracer

Cure 2, Raise, Raise 2, Reraise, Regen, Protect, Shell, Shell 2, Esuna
Moogle, Shiva, Ifrit, Golem, Carbunkle, Bahamut, Odin



Legionofwar
Male
Taurus
73
67
Summoner
Throw
Catch
Secret Hunt
Move+3

Wizard Staff

Twist Headband
Adaman Vest
Genji Gauntlet

Moogle, Shiva, Ramuh, Titan, Carbunkle, Lich, Cyclops
Shuriken, Wand

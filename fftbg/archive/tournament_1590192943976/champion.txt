Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Powergems
Male
Aquarius
45
68
Monk
Draw Out
Arrow Guard
Maintenance
Swim



Green Beret
Mythril Vest
N-Kai Armlet

Spin Fist, Wave Fist, Earth Slash, Revive, Seal Evil
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Lythe Caraker
Male
Cancer
51
46
Monk
Draw Out
Mana Shield
Dual Wield
Waterbreathing



Twist Headband
Mythril Vest
Vanish Mantle

Wave Fist, Secret Fist, Purification, Chakra, Revive
Koutetsu, Heaven's Cloud, Kikuichimoji



DrAntiSocial
Female
Pisces
63
54
Summoner
White Magic
Meatbone Slash
Short Charge
Move+3

Healing Staff

Feather Hat
Chameleon Robe
Jade Armlet

Moogle, Odin, Fairy, Lich
Cure, Cure 2, Cure 4, Reraise, Protect, Protect 2, Shell 2, Magic Barrier



Nifboy
Female
Pisces
73
55
Summoner
Yin Yang Magic
Hamedo
Magic Attack UP
Waterbreathing

Rod

Red Hood
Mystic Vest
Elf Mantle

Moogle, Ramuh, Ifrit, Golem, Odin, Leviathan
Poison, Spell Absorb, Zombie, Confusion Song, Paralyze, Sleep, Dark Holy

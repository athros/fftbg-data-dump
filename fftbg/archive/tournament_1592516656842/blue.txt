Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Female
Virgo
71
71
Dancer
Basic Skill
Caution
Beastmaster
Move+1

Persia

Feather Hat
Chain Vest
Rubber Shoes

Slow Dance, Polka Polka, Disillusion, Nameless Dance, Last Dance, Dragon Pit
Dash, Throw Stone, Heal, Tickle, Cheer Up



Killth3kid
Female
Capricorn
48
60
Wizard
Summon Magic
Sunken State
Equip Bow
Teleport

Gastrafitis

Feather Hat
Wizard Robe
Defense Ring

Fire, Fire 4, Bolt 2
Moogle, Ifrit, Carbunkle, Odin, Salamander, Silf



Ring Wyrm
Male
Leo
70
64
Geomancer
Punch Art
Arrow Guard
Concentrate
Retreat

Giant Axe
Mythril Shield
Black Hood
Light Robe
Leather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Spin Fist, Purification, Revive



RunicMagus
Male
Pisces
67
50
Monk
Item
Damage Split
Attack UP
Ignore Terrain



Headgear
Wizard Outfit
Spike Shoes

Spin Fist, Earth Slash, Chakra, Revive, Seal Evil
Potion, Ether, Eye Drop, Remedy, Phoenix Down

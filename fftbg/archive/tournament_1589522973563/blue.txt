Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LivingHitokiri
Male
Aries
73
69
Ninja
Elemental
Arrow Guard
Equip Armor
Waterbreathing

Ninja Edge
Ninja Edge
Leather Helmet
Linen Cuirass
Rubber Shoes

Shuriken
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Twelfthrootoftwo
Male
Scorpio
72
80
Lancer
Punch Art
Counter Magic
Doublehand
Waterbreathing

Mythril Spear

Gold Helmet
Mythril Armor
Magic Gauntlet

Level Jump8, Vertical Jump8
Spin Fist, Chakra



BuffaloCrunch
Female
Cancer
60
48
Priest
Charge
Absorb Used MP
Equip Shield
Ignore Height

White Staff
Flame Shield
Black Hood
Clothes
Wizard Mantle

Cure 2, Raise, Protect, Protect 2, Shell 2, Wall, Esuna
Charge+1, Charge+5



Mannequ1n
Male
Taurus
39
59
Oracle
Item
HP Restore
Attack UP
Fly

Battle Bamboo

Ribbon
Mythril Vest
Red Shoes

Blind, Pray Faith, Doubt Faith, Zombie, Paralyze, Sleep
Potion, Elixir, Eye Drop, Soft, Remedy, Phoenix Down

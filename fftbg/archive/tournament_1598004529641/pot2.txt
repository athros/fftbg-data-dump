Final Bets: green - 12 bets for 10,550G (78.4%, x0.28); yellow - 6 bets for 2,904G (21.6%, x3.63)

green bets:
gorgewall: 3,694G (35.0%, 3,694G)
Mesmaster: 2,000G (19.0%, 26,588G)
NIghtdew14: 1,749G (16.6%, 3,499G)
Seaweed_B: 1,000G (9.5%, 49,396G)
Lanshaft: 608G (5.8%, 13,657G)
skipsandwiches: 352G (3.3%, 3,994G)
DouglasDragonThePoet: 264G (2.5%, 264G)
VolgraTheMoose: 255G (2.4%, 38,401G)
Starfire6789: 224G (2.1%, 224G)
datadrivenbot: 200G (1.9%, 63,904G)
Lazarus_DS: 104G (1.0%, 104G)
AllInBot: 100G (0.9%, 100G)

yellow bets:
BirbBrainsBot: 1,000G (34.4%, 101,104G)
Sairentozon7: 1,000G (34.4%, 14,947G)
getthemoneyz: 404G (13.9%, 1,696,573G)
WhiteTigress: 300G (10.3%, 5,670G)
Lawndough: 100G (3.4%, 1,758G)
orestes92: 100G (3.4%, 218G)

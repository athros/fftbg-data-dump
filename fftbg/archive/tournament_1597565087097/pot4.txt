Final Bets: purple - 7 bets for 4,705G (69.3%, x0.44); brown - 9 bets for 2,088G (30.7%, x2.25)

purple bets:
E_Ballard: 1,453G (30.9%, 1,453G)
BirbBrainsBot: 1,000G (21.3%, 171,989G)
getthemoneyz: 798G (17.0%, 1,658,191G)
WhiteTigress: 500G (10.6%, 2,086G)
Sans_from_Snowdin: 500G (10.6%, 31,243G)
lowlf: 304G (6.5%, 3,633G)
ryjelsum: 150G (3.2%, 2,209G)

brown bets:
Creggers: 500G (23.9%, 19,929G)
TheMurkGnome: 340G (16.3%, 6,804G)
Smokegiant: 270G (12.9%, 270G)
X0ras: 200G (9.6%, 1,347G)
datadrivenbot: 200G (9.6%, 64,018G)
Aestheta: 184G (8.8%, 1,849G)
AllInBot: 170G (8.1%, 170G)
Lydian_C: 123G (5.9%, 1,931G)
gorgewall: 101G (4.8%, 8,736G)

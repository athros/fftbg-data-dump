Player: !Green
Team: Green Team
Palettes: Green/White



TheMurkGnome
Male
Cancer
78
47
Calculator
Yin Yang Magic
Counter Tackle
Secret Hunt
Move+3

Battle Folio

Golden Hairpin
White Robe
Reflect Ring

CT, 4, 3
Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep



DesertWooder
Male
Leo
62
56
Chemist
White Magic
Catch
Equip Polearm
Ignore Height

Partisan

Feather Hat
Brigandine
Feather Mantle

Potion, Soft, Holy Water, Remedy, Phoenix Down
Cure 2, Cure 3, Cure 4, Regen, Esuna, Holy



Powerpinch
Male
Pisces
64
73
Chemist
Elemental
Parry
Sicken
Jump+1

Assassin Dagger

Flash Hat
Brigandine
Dracula Mantle

Hi-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



X0ras
Female
Sagittarius
57
79
Squire
Talk Skill
Counter Magic
Attack UP
Move-HP Up

Mythril Knife
Round Shield
Iron Helmet
Brigandine
Vanish Mantle

Dash, Throw Stone, Heal, Yell, Cheer Up
Invitation, Persuade, Threaten, Refute

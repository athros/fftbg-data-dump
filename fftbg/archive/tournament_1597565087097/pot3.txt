Final Bets: white - 10 bets for 6,419G (58.9%, x0.70); black - 10 bets for 4,479G (41.1%, x1.43)

white bets:
lowlf: 2,140G (33.3%, 2,140G)
BirbBrainsBot: 1,000G (15.6%, 171,291G)
Willjin: 1,000G (15.6%, 20,754G)
Smokegiant: 748G (11.7%, 748G)
Creggers: 500G (7.8%, 19,580G)
getthemoneyz: 470G (7.3%, 1,657,863G)
TheMurkGnome: 200G (3.1%, 6,664G)
thephunbaba: 161G (2.5%, 161G)
AllInBot: 100G (1.6%, 100G)
ko2q: 100G (1.6%, 4,602G)

black bets:
Lydian_C: 1,200G (26.8%, 3,131G)
reinoe: 1,000G (22.3%, 231,316G)
Firesheath: 728G (16.3%, 728G)
Sans_from_Snowdin: 500G (11.2%, 31,743G)
ryjelsum: 250G (5.6%, 2,459G)
Aestheta: 200G (4.5%, 2,049G)
datadrivenbot: 200G (4.5%, 64,218G)
Zaxxaramas: 200G (4.5%, 3,351G)
gorgewall: 101G (2.3%, 8,837G)
X0ras: 100G (2.2%, 1,447G)

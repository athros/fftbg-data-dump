Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Airemlis
Male
Scorpio
41
78
Knight
Steal
Earplug
Maintenance
Move+1

Ragnarok

Mythril Helmet
Genji Armor
Rubber Shoes

Weapon Break, Speed Break, Stasis Sword, Dark Sword
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Arm Aim



DeathTaxesAndAnime
Female
Aquarius
44
64
Monk
Draw Out
Caution
Concentrate
Move+2



Barette
Clothes
Salty Rage

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Asura, Koutetsu



BenYuPoker
Monster
Aries
77
66
Ultima Demon










Deciimos
Male
Virgo
64
80
Thief
Item
Speed Save
Short Charge
Levitate

Assassin Dagger

Black Hood
Leather Vest
Bracer

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Arm Aim
Potion, X-Potion, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down

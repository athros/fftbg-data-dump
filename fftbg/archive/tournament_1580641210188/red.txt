Player: !Red
Team: Red Team
Palettes: Red/Brown



CrunkyBiscuits
Male
Scorpio
48
53
Summoner
Punch Art
Counter Tackle
Long Status
Fly

Thunder Rod

Triangle Hat
Linen Robe
Magic Gauntlet

Golem, Carbunkle, Leviathan, Silf, Lich, Cyclops
Spin Fist, Earth Slash, Purification, Chakra, Revive



Rocl
Male
Leo
66
68
Lancer
Item
PA Save
Secret Hunt
Move+1

Javelin
Aegis Shield
Mythril Helmet
Crystal Mail
Feather Mantle

Level Jump5, Vertical Jump8
Hi-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



Wonkierlynx
Female
Aries
59
54
Priest
Yin Yang Magic
Brave Up
Defense UP
Jump+2

Gold Staff

Golden Hairpin
Wizard Robe
Genji Gauntlet

Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Shell 2, Esuna, Holy
Spell Absorb, Life Drain, Pray Faith, Silence Song, Dispel Magic, Petrify, Dark Holy



YaBoy125
Male
Virgo
55
44
Archer
Yin Yang Magic
Meatbone Slash
Short Charge
Move-MP Up

Mythril Bow

Red Hood
Chain Vest
Rubber Shoes

Charge+5, Charge+10, Charge+20
Poison, Life Drain, Zombie, Confusion Song, Dispel Magic, Sleep, Petrify

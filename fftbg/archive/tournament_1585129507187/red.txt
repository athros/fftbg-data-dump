Player: !Red
Team: Red Team
Palettes: Red/Brown



OttoRaynar
Monster
Virgo
49
48
Ochu










Mirapoix
Male
Capricorn
57
71
Lancer
Yin Yang Magic
Blade Grasp
Doublehand
Ignore Height

Partisan

Grand Helmet
Linen Cuirass
Small Mantle

Level Jump5, Vertical Jump3
Pray Faith, Zombie



PotionDweller
Male
Virgo
76
62
Archer
Yin Yang Magic
Mana Shield
Defend
Move+1

Cute Bag
Diamond Shield
Ribbon
Secret Clothes
Spike Shoes

Charge+4, Charge+10
Blind, Life Drain, Doubt Faith, Zombie, Dispel Magic, Sleep



Rexamajinx
Female
Leo
58
66
Dancer
Black Magic
Auto Potion
Magic Attack UP
Teleport

Ryozan Silk

Triangle Hat
Brigandine
Defense Armlet

Witch Hunt, Slow Dance, Disillusion, Obsidian Blade
Fire 3, Ice 4

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Jordache7K
Male
Sagittarius
48
75
Bard
Summon Magic
Absorb Used MP
Short Status
Fly

Ramia Harp

Triangle Hat
Carabini Mail
Power Wrist

Life Song, Magic Song
Moogle, Carbunkle, Bahamut, Odin, Silf



Kronikle
Male
Gemini
56
48
Monk
Black Magic
Sunken State
Magic Defense UP
Ignore Terrain



Golden Hairpin
Power Sleeve
Magic Ring

Secret Fist, Purification, Chakra, Revive
Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Ice 4



Kaidykat
Female
Libra
52
47
Squire
Time Magic
Damage Split
Concentrate
Waterbreathing

Bow Gun
Bronze Shield
Black Hood
Linen Cuirass
Small Mantle

Accumulate, Dash, Throw Stone, Heal
Haste, Stop, Immobilize, Float, Quick, Stabilize Time



Gelwain
Male
Scorpio
76
62
Lancer
Talk Skill
Brave Save
Equip Knife
Retreat

Wizard Rod
Mythril Shield
Barbuta
Diamond Armor
Defense Ring

Level Jump5, Vertical Jump6
Threaten, Negotiate

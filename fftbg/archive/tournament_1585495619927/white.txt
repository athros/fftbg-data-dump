Player: !White
Team: White Team
Palettes: White/Blue



Tithonus
Female
Capricorn
43
66
Squire
White Magic
Counter
Halve MP
Move+3

Mage Masher
Genji Shield
Mythril Helmet
Secret Clothes
Feather Boots

Dash, Throw Stone, Heal, Cheer Up, Wish, Scream
Cure, Cure 2, Cure 3, Raise 2, Shell, Esuna



Noztril
Female
Libra
45
54
Summoner
Battle Skill
MP Restore
Equip Knife
Move+3

Sasuke Knife

Holy Miter
Silk Robe
Germinas Boots

Shiva, Ramuh, Golem, Salamander, Silf, Fairy, Lich
Head Break, Shield Break, Power Break



Gooseyourself
Female
Cancer
56
72
Archer
Dance
Abandon
Secret Hunt
Move+3

Poison Bow
Genji Shield
Red Hood
Mystic Vest
Angel Ring

Charge+1, Charge+2, Charge+3
Polka Polka, Disillusion, Nameless Dance



Lord Burrah
Male
Gemini
72
48
Calculator
Robosnake Skill
Counter
Equip Sword
Move-HP Up

Asura Knife

Crystal Helmet
Linen Cuirass
Sprint Shoes

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BaronHaynes
Female
Sagittarius
44
64
Wizard
Yin Yang Magic
Mana Shield
Magic Defense UP
Ignore Terrain

Cultist Dagger

Golden Hairpin
Silk Robe
108 Gems

Fire 3, Fire 4, Bolt 2, Ice 2, Ice 4, Frog
Poison, Pray Faith, Foxbird, Dispel Magic, Petrify



ANFz
Male
Taurus
78
68
Time Mage
Basic Skill
Counter
Equip Bow
Ignore Terrain

Windslash Bow

Green Beret
Silk Robe
Leather Mantle

Haste, Immobilize, Float, Quick, Demi 2, Stabilize Time
Accumulate, Heal, Tickle, Cheer Up



E Ballard
Male
Sagittarius
68
61
Lancer
Battle Skill
PA Save
Defend
Lava Walking

Iron Fan
Flame Shield
Iron Helmet
Linen Robe
Defense Armlet

Level Jump8, Vertical Jump8
Head Break, Armor Break, Power Break, Mind Break, Stasis Sword



Lemonjohns
Female
Virgo
71
46
Oracle
Punch Art
Parry
Attack UP
Ignore Terrain

Iron Fan

Cachusha
Mythril Vest
Genji Gauntlet

Poison, Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic, Petrify
Spin Fist, Earth Slash, Purification, Chakra, Seal Evil

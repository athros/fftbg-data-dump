Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Serperemagus
Female
Cancer
51
63
Squire
Item
Brave Save
Throw Item
Ignore Terrain

Giant Axe
Flame Shield
Green Beret
Clothes
Elf Mantle

Throw Stone, Heal, Yell, Fury, Scream
Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down



HaateXIII
Male
Aquarius
41
59
Oracle
Time Magic
Brave Save
Equip Armor
Jump+2

Octagon Rod

Iron Helmet
Plate Mail
Spike Shoes

Poison, Spell Absorb, Pray Faith, Doubt Faith, Dispel Magic, Sleep, Petrify
Slow, Slow 2, Float



Gelwain
Female
Virgo
67
43
Squire
Elemental
Faith Save
Beastmaster
Jump+1

Flame Whip
Diamond Shield
Mythril Helmet
Earth Clothes
108 Gems

Accumulate, Dash, Heal, Tickle, Fury, Wish
Water Ball, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



DAC169
Male
Gemini
66
61
Geomancer
Talk Skill
Damage Split
Beastmaster
Teleport

Rune Blade
Diamond Shield
Golden Hairpin
Light Robe
Diamond Armlet

Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Lava Ball
Persuade, Solution, Mimic Daravon, Refute

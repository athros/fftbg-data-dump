Player: !White
Team: White Team
Palettes: White/Blue



EunosXX
Male
Capricorn
58
72
Lancer
Summon Magic
Earplug
Magic Defense UP
Swim

Holy Lance
Buckler
Grand Helmet
Bronze Armor
Diamond Armlet

Level Jump5, Vertical Jump2
Moogle, Golem, Fairy



Black Sheep 0213
Female
Capricorn
48
52
Time Mage
Battle Skill
Dragon Spirit
Short Charge
Move-MP Up

Wizard Staff

Headgear
Mystic Vest
Power Wrist

Haste 2, Slow, Slow 2, Demi, Meteor
Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break



Phi Sig
Male
Cancer
76
64
Mime

Auto Potion
Secret Hunt
Waterbreathing



Genji Helmet
Adaman Vest
Genji Gauntlet

Mimic




Hasterious
Female
Taurus
79
66
Dancer
Draw Out
Distribute
Equip Sword
Swim

Broad Sword

Headgear
Mythril Vest
Diamond Armlet

Witch Hunt, Wiznaibus, Slow Dance, Last Dance, Nether Demon, Dragon Pit
Asura, Kiyomori, Muramasa, Chirijiraden

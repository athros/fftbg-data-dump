Player: !Black
Team: Black Team
Palettes: Black/Red



Timthehoodie
Female
Sagittarius
60
45
Chemist
Summon Magic
Speed Save
Equip Shield
Move+3

Cultist Dagger
Platinum Shield
Red Hood
Mythril Vest
Dracula Mantle

Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Moogle, Ifrit, Golem, Leviathan, Salamander, Silf, Lich



BoneMiser
Male
Sagittarius
48
58
Knight
Summon Magic
Auto Potion
Equip Armor
Move+3

Diamond Sword
Genji Shield
Cachusha
Silk Robe
Battle Boots

Armor Break, Shield Break, Speed Break
Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Cyclops



DAC169
Female
Pisces
73
74
Calculator
Nether Skill
Counter
Equip Gun
Lava Walking

Blaze Gun

Mythril Helmet
Linen Robe
Cursed Ring

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Drusiform
Female
Aquarius
78
73
Mime

Mana Shield
Short Status
Jump+2


Gold Shield
Leather Hat
Power Sleeve
Elf Mantle

Mimic


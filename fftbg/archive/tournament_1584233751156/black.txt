Player: !Black
Team: Black Team
Palettes: Black/Red



Technominari
Female
Gemini
77
47
Priest
Black Magic
Damage Split
Equip Axe
Move-MP Up

Battle Axe

Ribbon
Linen Robe
Bracer

Cure, Cure 3, Raise, Regen, Shell, Shell 2, Esuna
Fire 4, Bolt, Empower, Frog



Furorcelt
Monster
Aquarius
46
58
King Behemoth










Denamda
Female
Aquarius
60
69
Mime

Abandon
Magic Attack UP
Move+3



Thief Hat
Power Sleeve
Genji Gauntlet

Mimic




Pandasforsale
Male
Capricorn
72
57
Geomancer
Sing
Parry
Equip Gun
Fly

Bestiary
Crystal Shield
Triangle Hat
Linen Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Space Storage

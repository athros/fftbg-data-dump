Player: !White
Team: White Team
Palettes: White/Blue



Bilabrin
Female
Gemini
59
44
Calculator
Yin Yang Magic
Counter Magic
Magic Defense UP
Levitate

Battle Folio

Flash Hat
Mythril Vest
Red Shoes

CT, Height, Prime Number, 5
Pray Faith, Silence Song, Blind Rage, Sleep



JustDoomathon
Female
Virgo
47
48
Samurai
Item
Auto Potion
Throw Item
Move-MP Up

Koutetsu Knife

Diamond Helmet
Platinum Armor
Magic Gauntlet

Koutetsu, Bizen Boat, Kiyomori
Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down



WoahPOGGERS
Female
Sagittarius
57
81
Priest
Punch Art
Arrow Guard
Dual Wield
Jump+2

Flame Whip
Wizard Staff
Triangle Hat
Wizard Robe
Red Shoes

Cure 4, Raise, Regen, Protect, Shell, Esuna
Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Mirapoix
Female
Virgo
46
76
Geomancer
Draw Out
HP Restore
Long Status
Levitate

Long Sword
Bronze Shield
Red Hood
Earth Clothes
Germinas Boots

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Asura, Koutetsu, Heaven's Cloud

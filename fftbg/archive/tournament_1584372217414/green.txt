Player: !Green
Team: Green Team
Palettes: Green/White



DavenIII
Female
Cancer
57
50
Calculator
Time Magic
Mana Shield
Dual Wield
Levitate

Bestiary
Battle Folio
Flash Hat
Black Robe
Bracer

CT, Height, Prime Number, 5, 4, 3
Reflect, Quick, Demi, Demi 2



DudeMonkey77
Male
Capricorn
78
68
Mediator
Draw Out
Faith Up
Long Status
Ignore Terrain

Bestiary

Feather Hat
Mythril Vest
Diamond Armlet

Persuade, Praise, Threaten, Refute, Rehabilitate
Koutetsu, Muramasa, Kikuichimoji



Klonoa331
Male
Aries
43
49
Knight
Punch Art
Parry
Equip Bow
Jump+3

Bow Gun
Gold Shield
Circlet
Chain Mail
Reflect Ring

Head Break, Armor Break, Shield Break
Spin Fist, Purification, Chakra, Revive



MalakoFox
Female
Capricorn
64
52
Oracle
White Magic
Meatbone Slash
Short Status
Jump+3

Gokuu Rod

Ribbon
Black Robe
Defense Armlet

Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy
Cure, Cure 3, Raise, Wall

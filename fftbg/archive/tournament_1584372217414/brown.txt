Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ctharvey
Male
Aries
49
73
Chemist
Black Magic
Regenerator
Long Status
Move+2

Air Knife

Golden Hairpin
Power Sleeve
Bracer

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Fire 2, Bolt, Bolt 2, Ice 4, Frog



Coralreeferz
Male
Cancer
49
61
Time Mage
Black Magic
MP Restore
Equip Shield
Teleport

Rainbow Staff
Escutcheon
Holy Miter
Adaman Vest
N-Kai Armlet

Haste, Haste 2, Slow, Float, Reflect, Stabilize Time, Galaxy Stop
Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower



Kellios11
Female
Capricorn
79
68
Dancer
Black Magic
Auto Potion
Equip Bow
Move-HP Up

Hunting Bow

Twist Headband
Clothes
Magic Gauntlet

Witch Hunt, Polka Polka, Nameless Dance, Nether Demon
Fire 3, Bolt 2, Bolt 4, Ice, Death



Meta Five
Male
Leo
51
75
Mediator
Summon Magic
Earplug
Doublehand
Ignore Height

Blast Gun

Green Beret
Clothes
Sprint Shoes

Praise, Solution, Insult, Refute
Moogle, Ifrit, Titan, Leviathan, Fairy, Lich

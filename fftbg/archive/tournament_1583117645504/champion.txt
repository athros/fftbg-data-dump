Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ThePineappleSalesman
Male
Aquarius
75
47
Bard
Yin Yang Magic
Distribute
Short Charge
Jump+1

Bloody Strings

Triangle Hat
Black Costume
Defense Ring

Life Song, Cheer Song, Magic Song, Nameless Song, Sky Demon
Poison, Zombie, Blind Rage, Foxbird, Confusion Song, Paralyze



Omegasuspekt
Male
Gemini
58
66
Ninja
Sing
Regenerator
Equip Gun
Move-HP Up

Mythril Gun
Romanda Gun
Flash Hat
Power Sleeve
Setiemson

Bomb, Sword, Stick
Angel Song, Life Song, Battle Song



Lionhermit
Female
Gemini
65
58
Wizard
Yin Yang Magic
Caution
Equip Bow
Swim

Poison Bow

Flash Hat
Mythril Vest
Bracer

Fire, Fire 4, Bolt 3, Ice, Ice 2, Ice 3, Frog, Flare
Spell Absorb, Life Drain, Blind Rage, Sleep, Dark Holy



Nappychicken
Female
Virgo
73
77
Geomancer
Time Magic
Earplug
Magic Attack UP
Move-MP Up

Heaven's Cloud
Genji Shield
Golden Hairpin
Wizard Robe
Spike Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard
Slow 2, Reflect, Demi 2, Stabilize Time

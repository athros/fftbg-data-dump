Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ohsimouse
Female
Libra
61
65
Samurai
Dance
Damage Split
Martial Arts
Move-MP Up

Heaven's Cloud

Mythril Helmet
Chameleon Robe
Rubber Shoes

Asura, Heaven's Cloud, Muramasa
Disillusion, Nameless Dance



Umdstreaming
Male
Scorpio
48
48
Chemist
Time Magic
Counter Magic
Dual Wield
Waterwalking

Panther Bag
Star Bag
Golden Hairpin
Earth Clothes
Genji Gauntlet

Antidote, Soft, Phoenix Down
Haste, Stop, Immobilize, Reflect, Meteor



Wolvaroo
Monster
Aries
43
55
Goblin










Volgrathemoose
Male
Pisces
72
46
Monk
Yin Yang Magic
Counter Tackle
Defend
Waterbreathing



Black Hood
Mystic Vest
Genji Gauntlet

Spin Fist, Wave Fist, Purification, Chakra
Poison, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Sleep

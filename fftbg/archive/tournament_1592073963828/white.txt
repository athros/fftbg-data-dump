Player: !White
Team: White Team
Palettes: White/Blue



Breakdown777
Female
Pisces
75
60
Time Mage
Draw Out
Counter Magic
Equip Gun
Fly

Madlemgen

Red Hood
Linen Robe
Wizard Mantle

Haste, Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi, Stabilize Time
Bizen Boat, Kikuichimoji



Grininda
Male
Aquarius
64
75
Archer
Time Magic
MP Restore
Short Status
Move+3

Bow Gun
Aegis Shield
Golden Hairpin
Power Sleeve
Feather Mantle

Charge+3, Charge+20
Haste 2, Float, Demi, Demi 2



VolgraTheMoose
Male
Scorpio
50
61
Monk
Battle Skill
Distribute
Equip Bow
Move+3

Lightning Bow

Golden Hairpin
Clothes
Salty Rage

Spin Fist, Earth Slash, Purification, Revive
Head Break, Surging Sword



Serperemagus
Female
Gemini
41
65
Wizard
Dance
Critical Quick
Long Status
Lava Walking

Wizard Rod

Feather Hat
Brigandine
Battle Boots

Fire 2, Fire 4, Bolt
Witch Hunt, Slow Dance, Polka Polka, Nameless Dance, Obsidian Blade, Dragon Pit

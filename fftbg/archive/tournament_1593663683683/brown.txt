Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Superdevon1
Male
Aries
50
65
Mime

PA Save
Martial Arts
Levitate



Black Hood
Wizard Outfit
Spike Shoes

Mimic




Neerrm
Monster
Taurus
40
79
Byblos










Turbn
Male
Taurus
50
67
Archer
Yin Yang Magic
Distribute
Defend
Lava Walking

Poison Bow
Platinum Shield
Iron Helmet
Mystic Vest
Wizard Mantle

Charge+1, Charge+5, Charge+7, Charge+10, Charge+20
Blind, Life Drain, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze



Skillomono
Male
Sagittarius
60
43
Samurai
Yin Yang Magic
Counter
Dual Wield
Move-MP Up

Murasame
Murasame
Leather Helmet
Leather Armor
Magic Gauntlet

Asura, Muramasa
Blind, Poison, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify

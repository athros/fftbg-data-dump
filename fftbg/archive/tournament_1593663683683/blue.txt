Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



The Pengwin
Female
Cancer
62
54
Dancer
Battle Skill
Faith Save
Magic Attack UP
Waterbreathing

Persia

Twist Headband
Adaman Vest
Diamond Armlet

Witch Hunt, Wiznaibus, Disillusion, Nameless Dance, Last Dance, Nether Demon, Dragon Pit
Shield Break, Weapon Break, Mind Break, Justice Sword



YaBoy125
Male
Taurus
63
69
Knight
Yin Yang Magic
Counter Magic
Doublehand
Lava Walking

Battle Axe

Circlet
Diamond Armor
Bracer

Armor Break, Power Break, Stasis Sword, Justice Sword, Dark Sword
Foxbird, Paralyze, Dark Holy



CelestialLancer
Female
Cancer
67
78
Monk
Yin Yang Magic
Auto Potion
Beastmaster
Jump+3



Black Hood
Mystic Vest
Magic Gauntlet

Spin Fist, Pummel, Wave Fist, Purification, Revive
Poison, Blind Rage



DamnThatShark
Female
Aries
55
44
Wizard
Jump
Regenerator
Secret Hunt
Teleport

Thunder Rod

Triangle Hat
Mystic Vest
Magic Ring

Fire, Fire 3, Ice 4, Empower, Death, Flare
Level Jump4, Vertical Jump2

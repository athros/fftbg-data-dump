Player: !White
Team: White Team
Palettes: White/Blue



Arch8000
Male
Taurus
50
45
Archer
Throw
Sunken State
Long Status
Jump+2

Long Bow

Leather Hat
Chain Vest
108 Gems

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Shuriken, Bomb



SkylerBunny
Female
Scorpio
42
39
Squire
Steal
Faith Up
Beastmaster
Ignore Terrain

Morning Star
Crystal Shield
Gold Helmet
Adaman Vest
Magic Ring

Dash, Throw Stone, Heal, Cheer Up
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Steal Status



Shakarak
Female
Libra
54
44
Oracle
Basic Skill
Parry
Magic Defense UP
Jump+3

Ivory Rod

Holy Miter
Clothes
Defense Armlet

Pray Faith, Doubt Faith, Blind Rage, Paralyze, Sleep
Accumulate, Dash, Heal, Yell, Wish



Twelfthrootoftwo
Female
Leo
67
61
Monk
Item
Counter Tackle
Throw Item
Jump+1



Thief Hat
Mystic Vest
Small Mantle

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra
Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down

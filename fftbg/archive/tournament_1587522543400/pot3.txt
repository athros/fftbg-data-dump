Final Bets: white - 14 bets for 4,940G (17.4%, x4.76); black - 24 bets for 23,519G (82.6%, x0.21)

white bets:
Aldrammech: 1,000G (20.2%, 39,263G)
SkylerBunny: 1,000G (20.2%, 62,681G)
ungabunga_bot: 1,000G (20.2%, 192,847G)
braumbles: 500G (10.1%, 60,540G)
ANFz: 250G (5.1%, 17,514G)
nifboy: 237G (4.8%, 2,378G)
SerumD: 200G (4.0%, 1,791G)
smurphy000000: 200G (4.0%, 1,175G)
Mandoragan: 120G (2.4%, 120G)
ZephyrTempest: 101G (2.0%, 172,334G)
AllInBot: 100G (2.0%, 100G)
mr_schwifty: 100G (2.0%, 1,803G)
getthemoneyz: 82G (1.7%, 546,486G)
DrAntiSocial: 50G (1.0%, 21,385G)

black bets:
Virilikus: 4,000G (17.0%, 104,545G)
ko2q: 2,678G (11.4%, 5,357G)
upvla: 2,345G (10.0%, 17,823G)
TheMM42: 2,000G (8.5%, 14,423G)
kai_shee: 2,000G (8.5%, 153,613G)
Zeroroute: 1,351G (5.7%, 5,170G)
Lordminsc: 1,205G (5.1%, 42,904G)
Nizaha: 1,001G (4.3%, 4,444G)
BirbBrainsBot: 1,000G (4.3%, 105,582G)
VySaika: 1,000G (4.3%, 16,063G)
Evewho: 738G (3.1%, 738G)
aStatue: 731G (3.1%, 731G)
rico_flex: 666G (2.8%, 15,949G)
Shakarak: 600G (2.6%, 600G)
SWATb1gdog: 500G (2.1%, 10,570G)
Dexsana: 500G (2.1%, 2,572G)
killth3kid: 296G (1.3%, 6,492G)
StormSquad: 208G (0.9%, 208G)
DontBackseatGame: 200G (0.9%, 1,577G)
kaidykat: 100G (0.4%, 1,475G)
gorgewall: 100G (0.4%, 11,360G)
DeathTaxesAndAnime: 100G (0.4%, 14,496G)
datadrivenbot: 100G (0.4%, 167G)
Soy_Soda_Pop: 100G (0.4%, 1,048G)

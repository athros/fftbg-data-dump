Player: !White
Team: White Team
Palettes: White/Blue



Redmage4evah
Male
Leo
78
73
Geomancer
Yin Yang Magic
MP Restore
Equip Armor
Teleport

Battle Axe
Mythril Shield
Circlet
Clothes
Elf Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Gusty Wind
Spell Absorb, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy



MagicBottle
Female
Cancer
52
47
Knight
Time Magic
Regenerator
Attack UP
Jump+3

Giant Axe
Mythril Shield
Leather Helmet
Black Robe
Reflect Ring

Armor Break, Speed Break, Power Break
Float, Demi, Demi 2



Ko2q
Male
Aquarius
47
56
Lancer
Draw Out
Caution
Halve MP
Levitate

Javelin
Round Shield
Circlet
Chameleon Robe
Leather Mantle

Level Jump8, Vertical Jump3
Koutetsu, Heaven's Cloud, Muramasa



Smashy
Female
Aries
73
58
Summoner
Steal
Brave Save
Equip Axe
Move+2

Healing Staff

Black Hood
Power Sleeve
108 Gems

Moogle, Titan, Golem, Carbunkle, Bahamut, Odin, Silf, Fairy, Lich, Zodiac
Steal Helmet, Arm Aim

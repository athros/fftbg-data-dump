Final Bets: purple - 14 bets for 9,652G (78.4%, x0.27); brown - 4 bets for 2,652G (21.6%, x3.64)

purple bets:
GreatRedDragon: 2,500G (25.9%, 45,292G)
DavenIII: 1,000G (10.4%, 26,222G)
BirbBrainsBot: 1,000G (10.4%, 144,970G)
getthemoneyz: 1,000G (10.4%, 567,512G)
Zachara: 914G (9.5%, 59,914G)
upvla: 891G (9.2%, 4,996G)
leakimiko: 778G (8.1%, 5,190G)
ForagerCats: 500G (5.2%, 34,836G)
ungabunga_bot: 314G (3.3%, 221,494G)
PetitFoulard: 232G (2.4%, 232G)
TheManInPlaid: 222G (2.3%, 3,184G)
ZephyrTempest: 101G (1.0%, 185,600G)
lastly: 100G (1.0%, 3,306G)
datadrivenbot: 100G (1.0%, 2,317G)

brown bets:
aStatue: 1,902G (71.7%, 1,902G)
TheChainNerd: 500G (18.9%, 2,256G)
SaintOmerville: 200G (7.5%, 17,558G)
DrAntiSocial: 50G (1.9%, 26,520G)

Final Bets: black - 7 bets for 4,049G (44.1%, x1.27); purple - 11 bets for 5,138G (55.9%, x0.79)

black bets:
aStatue: 1,670G (41.2%, 1,670G)
waterwatereverywhere: 607G (15.0%, 607G)
SwaffleWaffles: 500G (12.3%, 67,259G)
upvla: 456G (11.3%, 5,241G)
Jeeboheebo: 360G (8.9%, 360G)
ungabunga_bot: 355G (8.8%, 221,985G)
ZephyrTempest: 101G (2.5%, 185,808G)

purple bets:
leakimiko: 1,171G (22.8%, 7,812G)
BirbBrainsBot: 1,000G (19.5%, 146,675G)
Zachara: 959G (18.7%, 60,459G)
DavenIII: 500G (9.7%, 25,847G)
ForagerCats: 500G (9.7%, 36,756G)
Aneyus: 306G (6.0%, 30,645G)
getthemoneyz: 252G (4.9%, 567,773G)
perce90: 200G (3.9%, 1,331G)
lastly: 100G (1.9%, 3,511G)
datadrivenbot: 100G (1.9%, 2,522G)
DrAntiSocial: 50G (1.0%, 26,470G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mirapoix
Male
Leo
55
45
Chemist
Battle Skill
Parry
Equip Shield
Lava Walking

Hydra Bag
Diamond Shield
Black Hood
Clothes
Reflect Ring

Potion, Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Armor Break, Shield Break



LDSkinny
Female
Libra
46
41
Oracle
Battle Skill
Distribute
Dual Wield
Move+3

Musk Rod
Cypress Rod
Leather Hat
Mythril Vest
Bracer

Poison, Spell Absorb, Pray Faith, Zombie, Dispel Magic
Mind Break, Justice Sword, Dark Sword



Firesheath
Male
Serpentarius
73
65
Lancer
Summon Magic
Meatbone Slash
Beastmaster
Waterbreathing

Obelisk
Genji Shield
Cross Helmet
Reflect Mail
N-Kai Armlet

Level Jump8, Vertical Jump8
Titan, Leviathan, Fairy



Actual JP
Male
Aries
69
50
Ninja
Jump
Catch
Equip Gun
Move+2

Papyrus Codex
Bloody Strings
Feather Hat
Black Costume
Wizard Mantle

Shuriken, Bomb, Axe, Dictionary
Level Jump5, Vertical Jump3

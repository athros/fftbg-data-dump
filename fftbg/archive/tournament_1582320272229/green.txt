Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Scorpio
73
54
Monk
Yin Yang Magic
Catch
Equip Polearm
Waterbreathing

Battle Bamboo

Feather Hat
Black Costume
Feather Mantle

Spin Fist, Wave Fist, Purification, Chakra, Revive
Pray Faith, Zombie, Foxbird, Paralyze, Sleep



Lodrak
Male
Capricorn
41
47
Chemist
Jump
Mana Shield
Secret Hunt
Jump+2

Assassin Dagger

Leather Hat
Clothes
Spike Shoes

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Soft, Phoenix Down
Level Jump3, Vertical Jump5



Fspll
Male
Libra
54
61
Lancer
Summon Magic
Caution
Short Charge
Lava Walking

Partisan
Round Shield
Mythril Helmet
Crystal Mail
Magic Gauntlet

Level Jump8, Vertical Jump4
Moogle, Ramuh, Carbunkle, Leviathan



EizanTayama
Male
Gemini
41
47
Squire
Sing
Hamedo
Beastmaster
Move+3

Cute Bag
Kaiser Plate
Platinum Helmet
Brigandine
Sprint Shoes

Accumulate, Heal, Yell, Scream
Diamond Blade, Hydra Pit

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Braumbles
Monster
Cancer
65
56
Chocobo










DustBirdEX
Female
Sagittarius
56
54
Geomancer
Yin Yang Magic
Counter Tackle
Equip Armor
Retreat

Giant Axe
Escutcheon
Cross Helmet
Linen Robe
Dracula Mantle

Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Poison, Life Drain, Pray Faith, Silence Song, Dispel Magic, Paralyze



ThePineappleSalesman
Female
Sagittarius
50
65
Ninja
Punch Art
Meatbone Slash
Equip Axe
Swim

Slasher
Battle Axe
Leather Hat
Power Sleeve
N-Kai Armlet

Shuriken, Spear
Wave Fist, Earth Slash, Purification, Seal Evil



Fenrislfr
Female
Taurus
54
48
Lancer
Yin Yang Magic
Parry
Martial Arts
Move+2


Escutcheon
Genji Helmet
Crystal Mail
Red Shoes

Level Jump2, Vertical Jump4
Blind, Poison, Pray Faith, Foxbird, Confusion Song

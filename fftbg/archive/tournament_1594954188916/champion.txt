Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



TheMurkGnome
Male
Cancer
42
74
Thief
Sing
Arrow Guard
Long Status
Levitate

Cultist Dagger

Twist Headband
Brigandine
Angel Ring

Steal Heart, Steal Shield, Arm Aim, Leg Aim
Last Song, Space Storage, Sky Demon



Sinnyil2
Female
Scorpio
79
72
Wizard
Math Skill
Mana Shield
Equip Bow
Jump+1

Poison Bow

Red Hood
Chameleon Robe
Germinas Boots

Fire, Fire 2, Bolt 2, Bolt 3, Ice 3, Ice 4, Empower, Death
Height, Prime Number, 5, 4, 3



PoroTact
Male
Scorpio
60
55
Calculator
Robosnake Skill
Mana Shield
Equip Bow
Ignore Height

Lightning Bow

Holy Miter
Mythril Vest
Red Shoes

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Laserman1000
Female
Leo
60
44
Summoner
Draw Out
HP Restore
Short Charge
Waterbreathing

Ice Rod

Holy Miter
Wizard Robe
Battle Boots

Shiva, Carbunkle, Leviathan, Lich
Koutetsu, Murasame

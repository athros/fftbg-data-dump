Player: !Green
Team: Green Team
Palettes: Green/White



Lijarkh
Male
Virgo
65
59
Monk
Sing
Parry
Equip Shield
Ignore Height


Diamond Shield
Feather Hat
Leather Outfit
Jade Armlet

Earth Slash, Purification, Chakra, Revive, Seal Evil
Battle Song, Diamond Blade



AerodyneX
Female
Taurus
72
56
Monk
Charge
Caution
Attack UP
Jump+3



Twist Headband
Black Costume
Jade Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Charge+3, Charge+10



ALY327
Male
Libra
77
56
Monk
Battle Skill
Critical Quick
Doublehand
Lava Walking



Red Hood
Wizard Outfit
Magic Gauntlet

Earth Slash, Purification, Chakra, Revive
Armor Break, Shield Break, Mind Break, Dark Sword, Surging Sword



JIDkomu
Male
Libra
68
52
Bard
Elemental
Counter
Magic Attack UP
Move+1

Ramia Harp

Red Hood
Secret Clothes
Battle Boots

Battle Song, Magic Song, Nameless Song, Sky Demon
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SkyridgeZero
Monster
Capricorn
71
42
Holy Dragon










BuffaloCrunch
Female
Aquarius
55
59
Squire
Jump
Distribute
Halve MP
Move-HP Up

Coral Sword
Aegis Shield
Triangle Hat
Genji Armor
N-Kai Armlet

Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Fury
Level Jump4, Vertical Jump8



Lali Lulelo
Female
Libra
74
61
Priest
Basic Skill
MA Save
Dual Wield
Ignore Height

Flame Whip
Flame Whip
Holy Miter
Wizard Robe
Wizard Mantle

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Protect, Shell, Wall, Esuna
Dash, Yell, Cheer Up, Wish



NovaKnight21
Male
Aquarius
81
49
Bard
Steal
Distribute
Dual Wield
Retreat

Windslash Bow

Feather Hat
Brigandine
Red Shoes

Life Song, Battle Song, Magic Song, Last Song
Steal Heart, Steal Armor

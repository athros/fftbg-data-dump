Final Bets: purple - 9 bets for 6,641G (42.4%, x1.36); brown - 10 bets for 9,004G (57.6%, x0.74)

purple bets:
Mtueni: 3,000G (45.2%, 226,705G)
R_Raynos: 2,000G (30.1%, 42,511G)
solomongrundy85: 400G (6.0%, 9,633G)
ungabunga_bot: 371G (5.6%, 322,907G)
Eyepoor_: 370G (5.6%, 434G)
Realitydown: 200G (3.0%, 1,874G)
LorianFaust: 100G (1.5%, 100G)
lastly: 100G (1.5%, 1,285G)
datadrivenbot: 100G (1.5%, 12,104G)

brown bets:
Mesmaster: 3,000G (33.3%, 61,439G)
WrathfulRemy: 1,996G (22.2%, 1,996G)
HaplessOne: 1,500G (16.7%, 12,175G)
BirbBrainsBot: 1,000G (11.1%, 186,866G)
FNCardascia_: 442G (4.9%, 442G)
getthemoneyz: 382G (4.2%, 600,985G)
Kyune: 250G (2.8%, 6,148G)
volgrathemoose: 218G (2.4%, 436G)
underscoreedit: 116G (1.3%, 116G)
CassiePhoenix: 100G (1.1%, 4,708G)

Player: !Green
Team: Green Team
Palettes: Green/White



L2 Sentinel
Male
Sagittarius
42
78
Oracle
Draw Out
Auto Potion
Magic Attack UP
Move+3

Octagon Rod

Red Hood
Mystic Vest
Power Wrist

Doubt Faith, Foxbird, Dispel Magic, Dark Holy
Bizen Boat, Kiyomori, Kikuichimoji



Prince Rogers Nelson
Female
Sagittarius
64
57
Samurai
Charge
Counter Tackle
Short Charge
Jump+1

Kikuichimoji

Grand Helmet
Crystal Mail
Rubber Shoes

Koutetsu, Muramasa
Charge+4, Charge+5, Charge+7, Charge+10



Actual JP
Male
Scorpio
53
66
Lancer
Elemental
Counter Magic
Secret Hunt
Jump+1

Holy Lance
Aegis Shield
Gold Helmet
Leather Armor
N-Kai Armlet

Level Jump3, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand



Anselm
Male
Virgo
75
78
Monk
Steal
Damage Split
Long Status
Ignore Height



Flash Hat
Leather Outfit
108 Gems

Wave Fist, Purification, Revive, Seal Evil
Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Leg Aim

Final Bets: red - 8 bets for 4,491G (47.3%, x1.11); green - 6 bets for 5,004G (52.7%, x0.90)

red bets:
BlackFireUK: 2,255G (50.2%, 2,255G)
Mesmaster: 1,000G (22.3%, 104,967G)
OppressivelyMinty: 500G (11.1%, 5,751G)
roqqqpsi: 236G (5.3%, 2,147G)
TeaTime29: 200G (4.5%, 11,955G)
AllInBot: 100G (2.2%, 100G)
E_Ballard: 100G (2.2%, 6,645G)
datadrivenbot: 100G (2.2%, 39,082G)

green bets:
VolgraTheMoose: 2,500G (50.0%, 72,999G)
reddwind_: 1,000G (20.0%, 6,037G)
BirbBrainsBot: 1,000G (20.0%, 16,293G)
getthemoneyz: 356G (7.1%, 954,990G)
dinin991: 100G (2.0%, 1,965G)
IphoneDarkness: 48G (1.0%, 5,168G)

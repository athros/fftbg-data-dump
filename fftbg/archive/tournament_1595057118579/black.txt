Player: !Black
Team: Black Team
Palettes: Black/Red



ALY327
Male
Libra
63
53
Time Mage
Sing
Faith Save
Equip Sword
Waterwalking

Asura Knife

Red Hood
White Robe
Feather Boots

Slow, Slow 2, Float, Stabilize Time
Angel Song, Life Song, Cheer Song, Magic Song, Last Song, Hydra Pit



StealthModeLocke
Female
Aquarius
59
80
Knight
Punch Art
Regenerator
Magic Defense UP
Retreat

Broad Sword
Bronze Shield
Gold Helmet
Plate Mail
Salty Rage

Armor Break, Weapon Break, Mind Break, Dark Sword, Night Sword
Spin Fist



Lythe Caraker
Female
Leo
77
76
Dancer
White Magic
Parry
Magic Attack UP
Jump+2

Star Bag

Holy Miter
Robe of Lords
Battle Boots

Nameless Dance, Dragon Pit
Cure 2, Raise, Regen, Protect, Shell, Esuna



Belkra
Male
Leo
79
75
Knight
Talk Skill
Counter Flood
Secret Hunt
Move+3

Rune Blade
Kaiser Plate
Circlet
Mythril Armor
Magic Gauntlet

Weapon Break, Power Break, Mind Break, Night Sword
Persuade, Praise, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate

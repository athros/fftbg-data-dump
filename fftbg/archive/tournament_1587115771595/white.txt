Player: !White
Team: White Team
Palettes: White/Blue



Zachara
Male
Leo
57
50
Ninja
Black Magic
HP Restore
Long Status
Levitate

Flail
Flail
Black Hood
Mystic Vest
Cursed Ring

Shuriken, Stick
Fire 2, Bolt 3, Ice, Ice 2, Ice 3



Volgrathemoose
Female
Pisces
67
69
Dancer
Battle Skill
Speed Save
Equip Gun
Ignore Height

Ramia Harp

Red Hood
Brigandine
Germinas Boots

Polka Polka, Disillusion, Obsidian Blade, Nether Demon
Head Break, Weapon Break, Speed Break, Night Sword



Mirapoix
Male
Taurus
46
72
Thief
Draw Out
Parry
Short Status
Lava Walking

Mythril Knife

Flash Hat
Earth Clothes
Magic Ring

Steal Armor, Steal Weapon, Steal Status
Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori



ApplesauceBoss
Male
Gemini
79
69
Mediator
Jump
Counter
Dual Wield
Move-MP Up

Blast Gun
Glacier Gun
Black Hood
Silk Robe
N-Kai Armlet

Praise, Threaten, Preach, Refute
Level Jump8, Vertical Jump6

Final Bets: green - 11 bets for 7,102G (62.1%, x0.61); yellow - 8 bets for 4,336G (37.9%, x1.64)

green bets:
UmaiJam: 1,700G (23.9%, 77,600G)
BirbBrainsBot: 1,000G (14.1%, 71,709G)
getthemoneyz: 878G (12.4%, 1,388,186G)
E_Ballard: 777G (10.9%, 777G)
thaetreis: 700G (9.9%, 9,908G)
Nizaha: 580G (8.2%, 23,570G)
Xoomwaffle: 500G (7.0%, 9,210G)
Error72: 364G (5.1%, 364G)
roqqqpsi: 341G (4.8%, 1,364G)
datadrivenbot: 200G (2.8%, 41,322G)
ArlanKels: 62G (0.9%, 2,068G)

yellow bets:
twelfthrootoftwo: 1,158G (26.7%, 2,272G)
VolgraTheMoose: 892G (20.6%, 1,785G)
resjudicata3: 595G (13.7%, 595G)
prince_rogers_nelson_: 568G (13.1%, 568G)
AllInBot: 551G (12.7%, 551G)
Lydian_C: 420G (9.7%, 8,852G)
DouglasDragonThePoet: 100G (2.3%, 3,693G)
renkei_fukai: 52G (1.2%, 104G)

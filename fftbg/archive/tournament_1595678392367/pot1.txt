Final Bets: red - 7 bets for 3,127G (36.3%, x1.76); blue - 11 bets for 5,490G (63.7%, x0.57)

red bets:
Lydian_C: 1,200G (38.4%, 6,745G)
twelfthrootoftwo: 611G (19.5%, 1,199G)
NIghtdew14: 500G (16.0%, 63,095G)
ruleof5: 300G (9.6%, 14,168G)
resjudicata3: 216G (6.9%, 216G)
AllInBot: 200G (6.4%, 200G)
GrayGhostGaming: 100G (3.2%, 28,716G)

blue bets:
UmaiJam: 1,700G (31.0%, 79,300G)
BirbBrainsBot: 1,000G (18.2%, 72,709G)
Error72: 622G (11.3%, 622G)
Nizaha: 580G (10.6%, 24,150G)
prince_rogers_nelson_: 568G (10.3%, 568G)
getthemoneyz: 256G (4.7%, 1,388,442G)
roqqqpsi: 240G (4.4%, 1,604G)
Musclestache: 200G (3.6%, 244G)
datadrivenbot: 200G (3.6%, 41,522G)
renkei_fukai: 94G (1.7%, 188G)
DouglasDragonThePoet: 30G (0.5%, 3,723G)

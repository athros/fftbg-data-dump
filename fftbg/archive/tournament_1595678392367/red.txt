Player: !Red
Team: Red Team
Palettes: Red/Brown



GrayGhostGaming
Female
Serpentarius
64
68
Calculator
Mighty Sword
Meatbone Slash
Sicken
Levitate

Madlemgen
Flame Shield
Genji Helmet
Chameleon Robe
Elf Mantle

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Lydian C
Female
Libra
75
64
Samurai
Talk Skill
Critical Quick
Monster Talk
Fly

Javelin

Diamond Helmet
Platinum Armor
Cherche

Asura, Murasame, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Threaten, Preach, Mimic Daravon, Refute



Prince Rogers Nelson
Female
Pisces
79
61
Archer
Elemental
Counter Flood
Long Status
Waterbreathing

Ice Bow

Cross Helmet
Adaman Vest
Wizard Mantle

Charge+1, Charge+2, Charge+7, Charge+10
Water Ball, Hell Ivy, Local Quake, Blizzard



Twelfthrootoftwo
Male
Aquarius
65
54
Archer
White Magic
Distribute
Dual Wield
Jump+2

Ice Bow

Headgear
Chain Vest
Red Shoes

Charge+1, Charge+4, Charge+5
Cure, Cure 3, Cure 4, Raise, Raise 2, Protect, Holy

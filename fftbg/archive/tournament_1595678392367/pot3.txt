Final Bets: white - 9 bets for 3,331G (30.7%, x2.26); black - 10 bets for 7,512G (69.3%, x0.44)

white bets:
LDSkinny: 1,000G (30.0%, 35,162G)
prince_rogers_nelson_: 568G (17.1%, 568G)
Lydian_C: 420G (12.6%, 8,432G)
getthemoneyz: 360G (10.8%, 1,388,722G)
thaetreis: 328G (9.8%, 10,335G)
resjudicata3: 216G (6.5%, 216G)
datadrivenbot: 200G (6.0%, 41,444G)
Musclestache: 176G (5.3%, 176G)
ArlanKels: 63G (1.9%, 2,106G)

black bets:
UmaiJam: 1,700G (22.6%, 78,638G)
E_Ballard: 1,251G (16.7%, 1,251G)
twelfthrootoftwo: 1,114G (14.8%, 1,114G)
BirbBrainsBot: 1,000G (13.3%, 72,320G)
VolgraTheMoose: 893G (11.9%, 893G)
Nizaha: 584G (7.8%, 23,928G)
SephDarkheart: 372G (5.0%, 372G)
Xoomwaffle: 320G (4.3%, 9,515G)
AllInBot: 200G (2.7%, 200G)
roqqqpsi: 78G (1.0%, 1,572G)

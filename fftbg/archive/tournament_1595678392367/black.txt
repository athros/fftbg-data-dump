Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Female
Capricorn
53
66
Thief
Draw Out
Counter
Dual Wield
Waterwalking

Orichalcum
Mythril Knife
Red Hood
Chain Vest
Power Wrist

Gil Taking, Steal Weapon, Steal Status
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



Letdowncity
Male
Capricorn
70
72
Samurai
Time Magic
Catch
Concentrate
Jump+1

Kikuichimoji

Mythril Helmet
Leather Armor
Red Shoes

Koutetsu, Bizen Boat, Murasame, Chirijiraden
Slow, Immobilize, Float



SephDarkheart
Male
Virgo
75
79
Lancer
Steal
Dragon Spirit
Dual Wield
Jump+2

Gungnir
Partisan
Bronze Helmet
Linen Robe
Feather Boots

Level Jump5, Vertical Jump8
Steal Heart, Steal Armor, Steal Status, Arm Aim



Bogdan
Male
Leo
61
50
Samurai
Talk Skill
Caution
Martial Arts
Swim



Genji Helmet
Linen Cuirass
Sprint Shoes

Koutetsu, Bizen Boat, Murasame
Death Sentence, Mimic Daravon, Rehabilitate

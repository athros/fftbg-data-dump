Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Go2sleepTV
Female
Gemini
57
66
Lancer
Throw
Counter Tackle
Concentrate
Teleport

Battle Bamboo
Escutcheon
Gold Helmet
Carabini Mail
Jade Armlet

Level Jump8, Vertical Jump8
Ninja Sword



Sinnyil2
Male
Pisces
44
63
Archer
Time Magic
Arrow Guard
Short Charge
Move+3

Poison Bow
Crystal Shield
Thief Hat
Chain Vest
Angel Ring

Charge+3, Charge+4, Charge+5, Charge+10
Haste, Haste 2, Slow 2, Float, Stabilize Time



ALY327
Male
Gemini
71
80
Squire
Item
Counter
Doublehand
Move+3

Iron Sword

Golden Hairpin
Plate Mail
Feather Boots

Accumulate, Yell
Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



Bioticism
Female
Cancer
47
68
Ninja
Battle Skill
Distribute
Defend
Swim

Iga Knife
Kunai
Black Hood
Adaman Vest
Reflect Ring

Shuriken, Bomb
Weapon Break, Magic Break, Power Break, Justice Sword, Surging Sword

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



WireLord
Male
Sagittarius
72
76
Monk
Steal
MA Save
Equip Knife
Ignore Height

Thunder Rod

Flash Hat
Black Costume
Bracer

Spin Fist, Pummel, Earth Slash, Secret Fist, Revive
Steal Shield, Steal Weapon



RyuTsuno
Female
Gemini
50
78
Ninja
Punch Art
Counter
Equip Bow
Move+2

Gastrafitis
Poison Bow
Cachusha
Judo Outfit
Defense Ring

Shuriken
Wave Fist, Purification, Chakra, Revive



DonCardenio
Male
Aries
73
38
Time Mage
Talk Skill
MA Save
Equip Axe
Jump+1

Battle Axe

Feather Hat
Chameleon Robe
Battle Boots

Haste, Haste 2, Slow, Stop, Quick
Persuade, Refute



Yasmosis
Male
Capricorn
78
50
Samurai
Item
PA Save
Doublehand
Ignore Terrain

Muramasa

Diamond Helmet
Light Robe
Cursed Ring

Murasame, Kiyomori, Muramasa
Ether, Hi-Ether, Antidote

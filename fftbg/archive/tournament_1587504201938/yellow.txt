Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nizaha
Female
Gemini
39
62
Oracle
Summon Magic
Counter
Sicken
Retreat

Gokuu Rod

Triangle Hat
Chain Vest
Angel Ring

Blind, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Confusion Song, Dispel Magic
Moogle, Golem, Carbunkle, Leviathan



Jeeboheebo
Female
Cancer
67
47
Knight
Item
Regenerator
Equip Gun
Jump+1

Stone Gun
Mythril Shield
Leather Helmet
Chameleon Robe
Leather Mantle

Head Break, Shield Break, Speed Break, Power Break
Ether, Eye Drop, Soft, Phoenix Down



LeepingJJ
Male
Virgo
56
66
Lancer
Time Magic
Distribute
Short Status
Jump+2

Dragon Whisker
Escutcheon
Gold Helmet
Carabini Mail
N-Kai Armlet

Level Jump5, Vertical Jump8
Float, Demi



Rico Flex
Male
Gemini
77
44
Monk
Time Magic
Counter Flood
Equip Gun
Jump+1

Bestiary

Leather Hat
Power Sleeve
Genji Gauntlet

Spin Fist, Pummel, Earth Slash, Revive
Haste 2, Immobilize, Float, Demi 2

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Female
Capricorn
46
48
Summoner
Basic Skill
Regenerator
Defense UP
Waterwalking

Bestiary

Twist Headband
Wizard Outfit
Small Mantle

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Leviathan, Silf, Cyclops
Throw Stone, Heal, Cheer Up, Scream



RNdOrchestra
Male
Leo
60
71
Wizard
Summon Magic
Distribute
Secret Hunt
Levitate

Thunder Rod

Black Hood
White Robe
Magic Ring

Fire, Bolt 3, Bolt 4, Ice, Death
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Silf



Tougou
Male
Pisces
54
51
Archer
Basic Skill
Caution
Dual Wield
Ignore Terrain

Poison Bow
Snipe Bow
Flash Hat
Mythril Vest
Angel Ring

Charge+1, Charge+2, Charge+20
Accumulate, Throw Stone, Fury, Ultima



E Ballard
Male
Aries
58
73
Chemist
White Magic
Auto Potion
Halve MP
Move-HP Up

Star Bag

Black Hood
Secret Clothes
Magic Ring

Ether, Antidote, Echo Grass, Soft
Cure, Raise, Regen, Protect

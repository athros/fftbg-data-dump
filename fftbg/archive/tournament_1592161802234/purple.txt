Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Itszaane
Monster
Cancer
73
46
Holy Dragon










Kaelsun
Female
Gemini
74
80
Knight
Basic Skill
Faith Save
Magic Defense UP
Retreat

Long Sword
Crystal Shield
Crystal Helmet
Carabini Mail
Power Wrist

Weapon Break, Magic Break, Surging Sword
Dash, Heal, Yell, Cheer Up, Scream



SSwing
Female
Taurus
66
62
Calculator
Curse Skill
Regenerator
Short Charge
Retreat

Cypress Rod
Hero Shield
Triangle Hat
White Robe
Germinas Boots

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Pandasforsale
Male
Libra
70
69
Bard
Talk Skill
Faith Save
Concentrate
Jump+1

Bloody Strings

Flash Hat
Linen Cuirass
Dracula Mantle

Cheer Song, Nameless Song, Sky Demon
Invitation, Persuade, Praise, Solution, Insult, Refute, Rehabilitate

Player: !Red
Team: Red Team
Palettes: Red/Brown



Eudes89
Male
Cancer
55
66
Samurai
Sing
Parry
Attack UP
Levitate

Dragon Whisker

Mythril Helmet
White Robe
Power Wrist

Koutetsu, Murasame, Heaven's Cloud
Life Song, Battle Song, Diamond Blade, Space Storage



Spuzzmocker
Female
Taurus
46
50
Lancer
Throw
Regenerator
Sicken
Move-MP Up

Obelisk
Genji Shield
Iron Helmet
Linen Robe
108 Gems

Level Jump8, Vertical Jump6
Wand



TeaTime29
Female
Sagittarius
61
53
Mime

Critical Quick
Martial Arts
Waterwalking



Headgear
Rubber Costume
Power Wrist

Mimic




NeoKevlar
Male
Pisces
52
78
Oracle
White Magic
HP Restore
Equip Knife
Fly

Rod

Leather Hat
Silk Robe
N-Kai Armlet

Poison, Spell Absorb, Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep, Petrify
Cure 3, Cure 4, Protect, Shell 2, Esuna, Magic Barrier

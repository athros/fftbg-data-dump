Player: !Black
Team: Black Team
Palettes: Black/Red



Pokezel
Male
Aries
64
67
Mime

MP Restore
Dual Wield
Teleport



Red Hood
Rubber Costume
Feather Boots

Mimic




Genericllama
Female
Virgo
74
73
Calculator
Bio
Parry
Equip Knife
Levitate

Bestiary

Headgear
Earth Clothes
Defense Ring

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Mirapoix
Male
Serpentarius
75
57
Priest
Draw Out
Mana Shield
Equip Polearm
Teleport

Gokuu Rod

Holy Miter
Black Costume
Jade Armlet

Cure, Raise, Regen, Protect, Protect 2, Shell, Esuna
Asura, Koutetsu, Heaven's Cloud



Glideadon
Male
Scorpio
79
47
Monk
Elemental
MA Save
Halve MP
Waterbreathing



Golden Hairpin
Power Sleeve
Germinas Boots

Spin Fist, Wave Fist, Chakra, Revive, Seal Evil
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

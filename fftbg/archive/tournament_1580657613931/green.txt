Player: !Green
Team: Green Team
Palettes: Green/White



Lord Burrah
Monster
Sagittarius
38
78
Skeleton










NBD Rab
Female
Sagittarius
51
43
Geomancer
Summon Magic
Meatbone Slash
Halve MP
Jump+3

Battle Axe
Escutcheon
Black Hood
Silk Robe
Feather Mantle

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shiva, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Odin, Salamander



SupahTree
Male
Scorpio
64
39
Chemist
Yin Yang Magic
HP Restore
Equip Gun
Jump+2

Mythril Gun

Leather Hat
Wizard Outfit
Battle Boots

Potion, X-Potion, Hi-Ether, Echo Grass, Soft, Phoenix Down
Life Drain, Pray Faith, Silence Song, Dispel Magic, Sleep, Dark Holy



Vampire Killer
Female
Aries
52
71
Thief
White Magic
Earplug
Equip Sword
Jump+2

Air Knife

Ribbon
Adaman Vest
Defense Armlet

Steal Heart, Arm Aim, Leg Aim
Cure, Raise, Protect, Shell, Shell 2, Wall, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



BoneMiser
Female
Taurus
54
71
Knight
Talk Skill
MP Restore
Monster Talk
Jump+2

Giant Axe
Kaiser Plate
Cross Helmet
Mythril Armor
Small Mantle

Head Break, Power Break, Surging Sword
Persuade, Solution, Death Sentence, Refute, Rehabilitate



I Nod My Head When I Lose
Male
Virgo
42
42
Thief
Draw Out
Damage Split
Martial Arts
Move+2



Feather Hat
Adaman Vest
Defense Armlet

Steal Helmet, Steal Armor, Steal Status
Murasame, Muramasa



TheKillerNacho
Monster
Taurus
79
73
Blue Dragon










Oogthecaveman
Female
Taurus
51
50
Chemist
Basic Skill
MA Save
Secret Hunt
Jump+3

Star Bag

Flash Hat
Wizard Outfit
Sprint Shoes

Potion, Phoenix Down
Accumulate, Throw Stone, Heal, Fury, Ultima

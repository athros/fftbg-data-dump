Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rechaun
Male
Taurus
63
78
Monk
Yin Yang Magic
Counter Tackle
Attack UP
Move+3



Ribbon
Black Costume
Red Shoes

Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Blind, Life Drain, Dispel Magic, Sleep



Ar Tactic
Female
Aries
49
47
Monk
Black Magic
Catch
Equip Knife
Ignore Terrain

Assassin Dagger

Leather Hat
Power Sleeve
Sprint Shoes

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Fire, Fire 2, Fire 3, Fire 4, Bolt 2, Ice



SaintOmerville
Female
Scorpio
68
46
Knight
Charge
Counter Magic
Halve MP
Ignore Height

Long Sword
Escutcheon
Circlet
Plate Mail
Battle Boots

Magic Break, Speed Break, Power Break
Charge+1, Charge+2, Charge+10, Charge+20



Bryon W
Female
Taurus
52
74
Samurai
Charge
Meatbone Slash
Long Status
Jump+1

Holy Lance

Circlet
Black Robe
Germinas Boots

Asura, Murasame, Muramasa, Masamune
Charge+4, Charge+5, Charge+20

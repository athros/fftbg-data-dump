Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



GrandmasterFrankerZ
Female
Serpentarius
55
47
Oracle
Elemental
Counter Magic
Martial Arts
Move-HP Up



Holy Miter
Wizard Robe
Battle Boots

Blind, Poison, Pray Faith, Foxbird, Dispel Magic, Dark Holy
Water Ball, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Megaman2202
Male
Capricorn
78
75
Chemist
Summon Magic
Absorb Used MP
Equip Shield
Ignore Height

Blaze Gun
Escutcheon
Thief Hat
Black Costume
Spike Shoes

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Moogle, Carbunkle, Salamander, Silf, Fairy



DavenIII
Male
Capricorn
66
41
Archer
Jump
Distribute
Doublehand
Swim

Stone Gun

Thief Hat
Wizard Outfit
Reflect Ring

Charge+2, Charge+5, Charge+10
Level Jump2, Vertical Jump7



HaychDub
Male
Serpentarius
73
62
Thief
Item
Faith Save
Equip Shield
Swim

Ancient Sword
Gold Shield
Golden Hairpin
Power Sleeve
Rubber Shoes

Gil Taking, Steal Armor, Steal Accessory, Leg Aim
Remedy, Phoenix Down

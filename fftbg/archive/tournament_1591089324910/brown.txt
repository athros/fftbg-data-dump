Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Evewho
Female
Gemini
39
46
Oracle
Time Magic
Sunken State
Short Charge
Move+3

Battle Bamboo

Holy Miter
Light Robe
Battle Boots

Blind, Poison, Silence Song, Foxbird, Petrify
Haste, Slow 2, Immobilize, Float, Demi, Stabilize Time



WitchHunterIX
Monster
Sagittarius
49
72
Minotaur










Ruleof5
Male
Cancer
59
46
Wizard
Throw
MA Save
Equip Shield
Move+1

Rod
Venetian Shield
Green Beret
Silk Robe
Bracer

Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice, Ice 4, Flare
Shuriken, Bomb, Wand



L2 Sentinel
Male
Aquarius
45
41
Chemist
Charge
Damage Split
Secret Hunt
Fly

Star Bag

Golden Hairpin
Leather Outfit
Germinas Boots

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Charge+3, Charge+4, Charge+5, Charge+10, Charge+20

Player: !Red
Team: Red Team
Palettes: Red/Brown



Breakdown777
Female
Aries
73
47
Summoner
Draw Out
Brave Up
Short Charge
Move-HP Up

Healing Staff

Black Hood
Mythril Vest
Leather Mantle

Moogle, Titan, Carbunkle, Leviathan, Salamander
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa



Tanookium
Monster
Taurus
73
64
Dryad










Possumm
Male
Capricorn
65
50
Archer
Talk Skill
Catch
Dual Wield
Retreat

Silver Bow

Feather Hat
Adaman Vest
Germinas Boots

Charge+3
Invitation, Praise, Mimic Daravon, Refute



Sinnyil2
Male
Libra
54
55
Mediator
Draw Out
Counter Flood
Short Status
Move+2

Papyrus Codex

Red Hood
Mythril Vest
Dracula Mantle

Persuade, Preach, Insult
Koutetsu, Heaven's Cloud, Kiyomori, Muramasa

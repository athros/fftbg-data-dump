Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



PKayge
Female
Gemini
39
65
Geomancer
Draw Out
Critical Quick
Dual Wield
Move-HP Up

Kikuichimoji
Giant Axe
Green Beret
Judo Outfit
Red Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Koutetsu, Bizen Boat



1twistedpuppy
Female
Gemini
81
52
Mediator
Yin Yang Magic
Counter
Equip Polearm
Move+2

Octagon Rod

Feather Hat
Light Robe
Small Mantle

Invitation, Persuade, Praise, Preach, Refute, Rehabilitate
Poison, Spell Absorb, Zombie, Foxbird, Dispel Magic, Paralyze, Sleep



Hyvi
Male
Scorpio
79
62
Bard
Charge
Abandon
Doublehand
Jump+1

Bloody Strings

Twist Headband
Bronze Armor
Cursed Ring

Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Nameless Song, Sky Demon
Charge+1, Charge+10



EunosXX
Female
Capricorn
80
63
Geomancer
Yin Yang Magic
Counter Flood
Equip Polearm
Move+2

Whale Whisker
Venetian Shield
Twist Headband
Power Sleeve
Rubber Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Life Drain, Pray Faith, Doubt Faith, Zombie, Confusion Song

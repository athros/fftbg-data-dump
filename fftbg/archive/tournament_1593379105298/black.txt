Player: !Black
Team: Black Team
Palettes: Black/Red



E Ballard
Male
Gemini
70
65
Ninja
White Magic
Earplug
Magic Attack UP
Teleport

Flame Whip
Hidden Knife
Red Hood
Brigandine
108 Gems

Shuriken, Bomb
Cure, Cure 2, Raise, Reraise, Shell, Esuna



Lowlf
Female
Libra
40
73
Lancer
Talk Skill
Parry
Short Charge
Move+3

Mythril Spear
Aegis Shield
Genji Helmet
White Robe
Rubber Shoes

Level Jump2, Vertical Jump7
Invitation, Solution, Insult, Mimic Daravon



Fluffskull
Male
Aries
45
69
Geomancer
Yin Yang Magic
Speed Save
Halve MP
Move+1

Murasame
Diamond Shield
Twist Headband
White Robe
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Poison, Life Drain, Zombie, Confusion Song, Dispel Magic, Petrify, Dark Holy



Fenaen
Female
Aries
79
73
Calculator
Labyrinth Skill
Absorb Used MP
Equip Shield
Jump+3

Battle Folio
Flame Shield
Iron Helmet
Reflect Mail
Wizard Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power

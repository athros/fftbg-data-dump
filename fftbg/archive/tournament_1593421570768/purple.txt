Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Aquios
Male
Sagittarius
75
42
Summoner
Time Magic
Brave Save
Defend
Lava Walking

Rod

Red Hood
Power Sleeve
Dracula Mantle

Moogle, Ramuh, Titan, Fairy, Cyclops
Haste, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time



DAC169
Monster
Gemini
74
54
Byblos










Serperemagus
Male
Taurus
63
68
Bard
Elemental
MP Restore
Secret Hunt
Swim

Ramia Harp

Flash Hat
Platinum Armor
Angel Ring

Cheer Song, Nameless Song, Diamond Blade, Sky Demon, Hydra Pit
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard



VolgraTheMoose
Male
Capricorn
74
48
Squire
Punch Art
Catch
Doublehand
Swim

Mythril Sword

Headgear
Platinum Armor
Elf Mantle

Dash, Heal, Yell, Cheer Up, Scream
Secret Fist, Purification, Revive

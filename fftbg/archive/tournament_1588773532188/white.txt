Player: !White
Team: White Team
Palettes: White/Blue



UmbraKnights
Male
Taurus
51
70
Knight
Draw Out
Mana Shield
Equip Gun
Swim

Bestiary
Hero Shield
Circlet
Silk Robe
Defense Armlet

Head Break, Magic Break, Mind Break, Justice Sword, Dark Sword
Asura, Koutetsu, Murasame, Kikuichimoji



Maakur
Monster
Virgo
40
78
Red Chocobo










ThePineappleSalesman
Female
Capricorn
73
71
Calculator
Ranch Skill
Counter Magic
Equip Knife
Fly

Dragon Rod

Circlet
Diamond Armor
Power Wrist

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor



JumbocactuarX27
Male
Aries
63
57
Squire
Sing
PA Save
Doublehand
Jump+3

Rune Blade

Red Hood
Chain Vest
Leather Mantle

Throw Stone, Heal, Fury
Angel Song, Life Song, Magic Song, Last Song, Sky Demon

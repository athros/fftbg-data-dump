Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Estan AD
Female
Aries
58
38
Chemist
Summon Magic
Brave Save
Beastmaster
Ignore Terrain

Hydra Bag

Red Hood
Power Sleeve
108 Gems

Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle



HaateXIII
Male
Capricorn
74
80
Archer
White Magic
Critical Quick
Equip Armor
Jump+3

Windslash Bow

Diamond Helmet
Platinum Armor
Angel Ring

Charge+3, Charge+4, Charge+10
Raise, Protect, Shell 2, Esuna, Holy



DLJuggernaut
Monster
Aquarius
46
78
Dragon










Mtueni
Male
Cancer
75
79
Priest
Item
Earplug
Halve MP
Levitate

Morning Star

Red Hood
Black Robe
Vanish Mantle

Cure, Cure 3, Cure 4, Raise, Raise 2, Protect, Protect 2, Wall, Esuna, Holy
Potion, X-Potion, Eye Drop, Echo Grass, Phoenix Down

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Eldente
Female
Pisces
42
43
Mime

PA Save
Equip Armor
Jump+1


Mythril Shield
Barbuta
White Robe
Angel Ring

Mimic




IndecisiveNinja
Female
Virgo
54
66
Time Mage
Draw Out
Parry
Doublehand
Move+3

Oak Staff

Green Beret
Secret Clothes
108 Gems

Haste, Slow, Float, Quick, Demi, Stabilize Time, Meteor
Murasame



DeathTaxesAndAnime
Female
Aries
60
50
Priest
Jump
Catch
Short Charge
Jump+1

White Staff

Leather Hat
Black Robe
Cursed Ring

Raise, Raise 2, Shell, Esuna, Holy
Level Jump4, Vertical Jump8



Baron Von Scrub
Male
Libra
63
74
Geomancer
Punch Art
Caution
Magic Defense UP
Move+2

Battle Axe
Gold Shield
Feather Hat
Silk Robe
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Lava Ball
Pummel, Revive, Seal Evil

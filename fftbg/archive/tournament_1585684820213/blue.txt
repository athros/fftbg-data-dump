Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Fenixcrest
Female
Capricorn
53
79
Priest
Dance
Counter Tackle
Dual Wield
Lava Walking

White Staff
Wizard Staff
Triangle Hat
Light Robe
Small Mantle

Cure, Cure 2, Raise, Reraise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna
Witch Hunt, Wiznaibus, Polka Polka



Gelwain
Male
Virgo
68
47
Archer
Punch Art
Earplug
Doublehand
Levitate

Lightning Bow

Black Hood
Wizard Outfit
Dracula Mantle

Charge+1, Charge+2, Charge+4, Charge+5, Charge+7, Charge+20
Wave Fist, Purification



I Nod My Head When I Lose
Male
Virgo
76
53
Geomancer
Summon Magic
Parry
Equip Bow
Jump+2

Yoichi Bow

Holy Miter
Chain Vest
Setiemson

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Moogle, Odin, Salamander, Lich, Cyclops



Anomandaris
Male
Sagittarius
74
53
Geomancer
Talk Skill
MP Restore
Defense UP
Lava Walking

Slasher
Mythril Shield
Green Beret
Brigandine
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Death Sentence

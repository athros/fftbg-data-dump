Player: !zChamp
Team: Champion Team
Palettes: Green/White



Lijarkh
Monster
Cancer
50
81
Serpentarius










Alacor
Male
Scorpio
69
80
Knight
Summon Magic
Damage Split
Short Charge
Lava Walking

Sleep Sword
Bronze Shield
Grand Helmet
Chain Mail
Genji Gauntlet

Shield Break, Speed Break, Power Break, Mind Break, Stasis Sword
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Salamander, Fairy



Blain Cooper
Male
Capricorn
60
71
Wizard
Basic Skill
Counter
Short Charge
Move+3

Ice Rod

Red Hood
Mystic Vest
Defense Armlet

Fire 2, Fire 3, Bolt, Bolt 2, Ice 2, Ice 3, Flare
Heal, Tickle, Wish



JumbocactuarX27
Monster
Taurus
60
58
Revenant








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Powermhero
Male
Aries
68
64
Ninja
Punch Art
Regenerator
Equip Knife
Fly

Blind Knife
Rod
Leather Hat
Chain Vest
Bracer

Shuriken, Knife
Pummel, Secret Fist, Purification, Revive, Seal Evil



CapnChaos12
Male
Cancer
53
60
Monk
Item
Catch
Defend
Move-MP Up



Twist Headband
Black Costume
Diamond Armlet

Spin Fist, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down



Deathmaker06
Female
Virgo
49
74
Squire
Yin Yang Magic
Critical Quick
Equip Knife
Lava Walking

Dagger
Escutcheon
Leather Helmet
Bronze Armor
Dracula Mantle

Cheer Up, Wish
Blind, Poison, Spell Absorb, Life Drain, Blind Rage, Foxbird, Petrify



Digitalsocrates
Female
Cancer
73
74
Archer
Battle Skill
Brave Save
Equip Shield
Move+2

Mythril Bow
Escutcheon
Ribbon
Chain Vest
Reflect Ring

Charge+20
Armor Break, Speed Break, Stasis Sword

Player: !White
Team: White Team
Palettes: White/Blue



Foxfaeez
Monster
Leo
51
64
Coeurl










Run With Stone GUNs
Female
Scorpio
50
62
Summoner
Throw
Counter
Magic Attack UP
Move-MP Up

Thunder Rod

Flash Hat
Mythril Vest
Feather Boots

Shiva, Ramuh, Titan, Carbunkle, Leviathan, Silf
Shuriken



Cho Pin
Male
Capricorn
58
48
Bard
Battle Skill
PA Save
Defense UP
Retreat

Fairy Harp

Feather Hat
Chain Mail
Genji Gauntlet

Life Song, Cheer Song, Magic Song, Sky Demon
Head Break, Armor Break, Weapon Break, Magic Break, Speed Break, Stasis Sword



Lowlf
Male
Scorpio
47
48
Mediator
Steal
Absorb Used MP
Short Charge
Ignore Terrain

Stone Gun

Twist Headband
White Robe
Magic Gauntlet

Invitation, Persuade, Threaten, Preach, Death Sentence, Refute, Rehabilitate
Steal Heart, Steal Status

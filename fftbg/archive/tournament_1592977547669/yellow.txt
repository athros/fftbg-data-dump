Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Gelwain
Male
Aries
64
55
Monk
Draw Out
Faith Save
Attack UP
Move+3



Headgear
Brigandine
Angel Ring

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Asura, Koutetsu, Kiyomori



Reddwind
Female
Scorpio
62
39
Knight
Yin Yang Magic
Earplug
Short Charge
Teleport

Save the Queen
Platinum Shield
Iron Helmet
Reflect Mail
Sprint Shoes

Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Poison, Life Drain, Pray Faith, Blind Rage, Dispel Magic



DuraiPapers
Female
Virgo
69
72
Mime

Regenerator
Monster Talk
Levitate



Thief Hat
Linen Cuirass
Defense Ring

Mimic




Nekojin
Female
Leo
71
58
Time Mage
Dance
Counter Tackle
Defense UP
Jump+2

Sage Staff

Twist Headband
Wizard Outfit
Rubber Shoes

Haste 2, Slow 2, Stop, Immobilize, Float, Demi 2
Witch Hunt, Slow Dance, Nameless Dance, Void Storage

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evewho
Female
Virgo
70
60
Knight
Item
Critical Quick
Equip Knife
Waterbreathing

Flame Rod
Ice Shield
Diamond Helmet
Maximillian
Cursed Ring

Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword
Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Remedy, Phoenix Down



Nhammen
Female
Leo
57
71
Geomancer
Summon Magic
Mana Shield
Doublehand
Jump+2

Koutetsu Knife

Headgear
White Robe
Defense Ring

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Moogle, Titan



NicoSavoy
Female
Taurus
63
64
Time Mage
Black Magic
Counter Tackle
Equip Bow
Waterbreathing

Windslash Bow

Black Hood
Linen Robe
Magic Ring

Slow, Stop, Reflect, Stabilize Time
Fire 2, Fire 4, Bolt 2, Ice 2, Ice 3, Death



Mrfripps
Male
Virgo
53
48
Ninja
Yin Yang Magic
Meatbone Slash
Halve MP
Retreat

Morning Star
Flail
Golden Hairpin
Clothes
Defense Ring

Ninja Sword, Dictionary
Blind, Zombie, Paralyze, Petrify

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Shakarak
Male
Pisces
44
78
Bard
Charge
Counter
Equip Knife
Move+3

Dagger

Twist Headband
Maximillian
Defense Ring

Angel Song, Last Song, Space Storage, Sky Demon
Charge+1, Charge+4, Charge+5, Charge+20



Just Here2
Male
Capricorn
70
70
Time Mage
Talk Skill
Faith Save
Secret Hunt
Jump+3

White Staff

Holy Miter
Adaman Vest
Small Mantle

Haste, Haste 2, Slow, Slow 2, Float, Reflect, Demi, Demi 2
Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Refute



Shalloween
Male
Sagittarius
82
50
Geomancer
White Magic
Caution
Equip Knife
Swim

Blind Knife
Kaiser Plate
Feather Hat
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Cure, Cure 2, Cure 4, Raise, Reraise, Protect, Protect 2, Esuna



Choco Joe
Female
Capricorn
60
55
Samurai
Battle Skill
Critical Quick
Sicken
Waterbreathing

Murasame

Circlet
Diamond Armor
Spike Shoes

Heaven's Cloud, Kiyomori, Kikuichimoji
Head Break, Armor Break, Weapon Break, Magic Break, Stasis Sword, Surging Sword

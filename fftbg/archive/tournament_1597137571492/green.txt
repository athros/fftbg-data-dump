Player: !Green
Team: Green Team
Palettes: Green/White



Helpimabug
Female
Sagittarius
75
76
Samurai
Dance
Caution
Magic Defense UP
Move+3

Kikuichimoji

Diamond Helmet
Chameleon Robe
Feather Mantle

Asura, Bizen Boat, Kiyomori, Muramasa
Witch Hunt, Nameless Dance, Obsidian Blade, Void Storage



Lydian C
Female
Taurus
77
71
Ninja
Draw Out
Auto Potion
Equip Gun
Move-HP Up

Bestiary
Papyrus Codex
Holy Miter
Mythril Vest
Bracer

Shuriken, Bomb, Staff, Wand, Dictionary
Asura, Koutetsu, Bizen Boat



ColetteMSLP
Female
Aquarius
61
71
Ninja
Item
Critical Quick
Concentrate
Ignore Terrain

Cultist Dagger
Short Edge
Feather Hat
Mythril Vest
N-Kai Armlet

Shuriken
Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



CT 5 Holy
Male
Virgo
59
47
Thief
Black Magic
Arrow Guard
Concentrate
Retreat

Hidden Knife

Green Beret
Earth Clothes
Bracer

Gil Taking, Steal Accessory
Ice 2, Frog

Final Bets: red - 6 bets for 3,302G (38.4%, x1.60); blue - 12 bets for 5,294G (61.6%, x0.62)

red bets:
DeathTaxesAndAnime: 1,074G (32.5%, 1,074G)
peeronid: 1,000G (30.3%, 11,529G)
mirapoix: 905G (27.4%, 905G)
Lydian_C: 123G (3.7%, 10,314G)
ko2q: 100G (3.0%, 2,675G)
BaronHaynes: 100G (3.0%, 5,408G)

blue bets:
helpimabug: 1,265G (23.9%, 2,531G)
BirbBrainsBot: 1,000G (18.9%, 15,034G)
getthemoneyz: 914G (17.3%, 1,602,594G)
CassiePhoenix: 814G (15.4%, 814G)
LAGBOT30000: 260G (4.9%, 2,607G)
vorap: 212G (4.0%, 212G)
AllInBot: 200G (3.8%, 200G)
datadrivenbot: 200G (3.8%, 57,421G)
maddrave09: 128G (2.4%, 199G)
gorgewall: 101G (1.9%, 3,789G)
MinBetBot: 100G (1.9%, 22,827G)
DHaveWord: 100G (1.9%, 2,207G)

Player: !White
Team: White Team
Palettes: White/Blue



Douchetron
Female
Gemini
80
49
Squire
Talk Skill
Abandon
Magic Defense UP
Move+1

Flame Whip
Diamond Shield
Twist Headband
Chain Mail
Feather Mantle

Accumulate, Dash, Throw Stone, Yell, Cheer Up
Persuade, Preach, Death Sentence, Negotiate, Mimic Daravon



BaronHaynes
Male
Libra
49
54
Summoner
Basic Skill
Speed Save
Maintenance
Levitate

Ice Rod

Headgear
Linen Robe
Rubber Shoes

Moogle, Shiva, Ramuh, Titan, Silf, Fairy
Accumulate, Dash, Throw Stone, Heal, Cheer Up, Fury



UmaiJam
Male
Taurus
75
80
Knight
Basic Skill
HP Restore
Attack UP
Ignore Terrain

Rune Blade
Escutcheon
Leather Helmet
Leather Armor
Defense Ring

Weapon Break, Speed Break, Mind Break, Night Sword
Throw Stone, Yell, Cheer Up, Fury, Ultima



ALY327
Female
Cancer
62
44
Wizard
Draw Out
Dragon Spirit
Short Status
Move+1

Air Knife

Headgear
Silk Robe
Germinas Boots

Fire 2, Bolt 2
Koutetsu

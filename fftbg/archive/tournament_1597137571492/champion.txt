Player: !zChamp
Team: Champion Team
Palettes: Green/White



Sairentozon7
Male
Pisces
69
56
Ninja
Battle Skill
Dragon Spirit
Attack UP
Jump+1

Koga Knife
Spell Edge
Flash Hat
Mythril Vest
Defense Ring

Bomb, Knife, Sword
Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break



NovaKnight21
Male
Pisces
52
78
Summoner
Item
Abandon
Defense UP
Retreat

Ice Rod

Black Hood
Black Robe
Bracer

Moogle, Ramuh, Bahamut, Odin, Lich
Potion, Soft



Midori Ribbon
Male
Aquarius
60
68
Geomancer
White Magic
Speed Save
Equip Armor
Lava Walking

Mythril Sword
Gold Shield
Iron Helmet
Platinum Armor
Magic Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind
Raise, Reraise, Protect 2, Shell, Esuna



TeaTime29
Female
Aquarius
76
75
Ninja
Summon Magic
MP Restore
Equip Gun
Jump+3

Bestiary
Bestiary
Ribbon
Secret Clothes
Leather Mantle

Bomb, Knife, Wand
Moogle, Carbunkle, Salamander, Silf

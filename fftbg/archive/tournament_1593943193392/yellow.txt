Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



AO110893
Female
Capricorn
40
62
Thief
Basic Skill
Hamedo
Halve MP
Jump+1

Short Edge

Red Hood
Judo Outfit
Sprint Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Status
Dash, Heal, Wish



Gooseyourself
Male
Capricorn
65
82
Priest
Elemental
Abandon
Defend
Move-HP Up

Wizard Staff

Red Hood
Judo Outfit
Defense Armlet

Cure, Cure 3, Raise, Reraise, Protect 2
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Enkikavlar
Male
Sagittarius
48
75
Oracle
Jump
Critical Quick
Equip Knife
Move+1

Poison Rod

Headgear
Black Robe
Power Wrist

Poison, Spell Absorb, Silence Song, Blind Rage, Paralyze, Sleep, Dark Holy
Level Jump5, Vertical Jump7



E Ballard
Male
Pisces
60
65
Knight
Black Magic
PA Save
Concentrate
Move+1

Mythril Sword
Platinum Shield
Crystal Helmet
Mythril Armor
Reflect Ring

Shield Break, Power Break, Dark Sword, Night Sword
Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Death

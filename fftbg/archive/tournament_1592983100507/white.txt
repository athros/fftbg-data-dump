Player: !White
Team: White Team
Palettes: White/Blue



Reinoe
Female
Virgo
77
40
Thief
Draw Out
Hamedo
Beastmaster
Lava Walking

Sasuke Knife

Feather Hat
Judo Outfit
108 Gems

Gil Taking, Steal Heart, Steal Accessory, Leg Aim
Bizen Boat, Kikuichimoji, Masamune



Prince Rogers Nelson
Female
Leo
76
75
Summoner
Basic Skill
Mana Shield
Dual Wield
Levitate

Wizard Staff
Thunder Rod
Holy Miter
Power Sleeve
Magic Gauntlet

Moogle, Ramuh, Ifrit, Golem, Bahamut, Silf, Fairy
Dash, Throw Stone, Heal, Fury



Arcblazer23
Male
Taurus
71
51
Calculator
White Magic
Arrow Guard
Maintenance
Jump+2

Madlemgen

Red Hood
Brigandine
Chantage

CT, Height, Prime Number, 5, 3
Cure 2, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna



Powermhero
Male
Gemini
69
43
Geomancer
Item
Brave Save
Halve MP
Jump+2

Kiyomori
Buckler
Triangle Hat
Light Robe
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm
Hi-Potion, Antidote, Maiden's Kiss, Soft, Phoenix Down

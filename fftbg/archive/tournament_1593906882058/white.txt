Player: !White
Team: White Team
Palettes: White/Blue



Prince Rogers Nelson
Female
Libra
71
73
Wizard
Elemental
Hamedo
Magic Attack UP
Move-MP Up

Cultist Dagger

Black Hood
Leather Outfit
Magic Gauntlet

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4, Death
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball



NIghtdew14
Female
Libra
45
52
Geomancer
Draw Out
Brave Save
Magic Attack UP
Swim

Slasher
Diamond Shield
Thief Hat
Mythril Vest
Wizard Mantle

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind
Koutetsu



J2DaBibbles
Female
Gemini
77
43
Calculator
Black Magic
Abandon
Equip Gun
Levitate

Papyrus Codex

Red Hood
Wizard Robe
Elf Mantle

CT, Prime Number, 5, 4
Fire 2, Fire 3, Bolt 2, Bolt 4, Ice 2, Ice 3, Ice 4, Flare



Nekojin
Female
Serpentarius
46
52
Lancer
Time Magic
Dragon Spirit
Dual Wield
Jump+1

Iron Fan
Musk Rod
Diamond Helmet
Plate Mail
Defense Armlet

Level Jump8, Vertical Jump7
Haste, Haste 2, Slow 2, Immobilize, Demi, Stabilize Time, Meteor

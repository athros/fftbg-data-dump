Player: !Red
Team: Red Team
Palettes: Red/Brown



HuffFlex
Male
Gemini
57
59
Priest
Black Magic
MP Restore
Doublehand
Jump+1

Morning Star

Holy Miter
Black Robe
Feather Boots

Cure 2, Cure 4, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna
Fire 3, Bolt 3, Ice 2



G1nger4le
Female
Aries
55
64
Dancer
Yin Yang Magic
Critical Quick
Halve MP
Move+3

Assassin Dagger

Holy Miter
White Robe
Battle Boots

Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Last Dance
Life Drain, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Petrify, Dark Holy



Baron Von Scrub
Male
Libra
53
69
Priest
Math Skill
Critical Quick
Equip Sword
Jump+2

Sleep Sword

Thief Hat
White Robe
Feather Boots

Cure 2, Cure 3, Cure 4, Raise, Reraise, Shell 2, Wall, Esuna
CT, Height, 5, 3



Neerrm
Female
Cancer
66
64
Oracle
Steal
Caution
Equip Bow
Retreat

Ultimus Bow

Feather Hat
Light Robe
Cursed Ring

Blind, Poison, Spell Absorb, Pray Faith, Zombie, Foxbird, Dispel Magic
Steal Heart, Steal Shield, Arm Aim

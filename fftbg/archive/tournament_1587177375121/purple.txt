Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheMurkGnome
Male
Capricorn
78
54
Knight
Sing
Parry
Equip Sword
Waterbreathing

Defender
Crystal Shield
Genji Helmet
Platinum Armor
Diamond Armlet

Shield Break, Justice Sword
Angel Song, Life Song, Cheer Song, Last Song



Estan AD
Male
Aquarius
71
77
Calculator
Yin Yang Magic
Auto Potion
Short Status
Fly

Madlemgen

Black Hood
Black Robe
Feather Mantle

Height, Prime Number, 3
Blind, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic



ZZ Yoshi
Female
Aries
55
49
Calculator
Undeath Skill
Speed Save
Defense UP
Move+2

Madlemgen
Escutcheon
Diamond Helmet
Platinum Armor
Salty Rage

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Volgrathemoose
Male
Cancer
45
53
Archer
White Magic
Earplug
Doublehand
Swim

Bow Gun

Red Hood
Adaman Vest
Defense Ring

Charge+2, Charge+3, Charge+7, Charge+20
Raise 2, Shell 2, Wall, Esuna

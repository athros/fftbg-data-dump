Player: !Black
Team: Black Team
Palettes: Black/Red



Zeroroute
Male
Scorpio
62
58
Time Mage
Draw Out
Parry
Short Charge
Retreat

Bestiary

Twist Headband
Black Robe
Elf Mantle

Haste 2, Slow 2, Immobilize, Demi 2, Stabilize Time
Asura, Bizen Boat, Murasame, Heaven's Cloud



DustBirdEX
Male
Aquarius
80
59
Mime

Speed Save
Magic Attack UP
Lava Walking



Headgear
Clothes
Jade Armlet

Mimic




Lythe Caraker
Male
Scorpio
76
65
Samurai
Black Magic
Regenerator
Magic Defense UP
Lava Walking

Bizen Boat

Leather Helmet
Plate Mail
Feather Mantle

Muramasa
Bolt 4, Ice 2, Ice 4



Blain Cooper
Male
Aries
67
71
Archer
Sing
Brave Up
Dual Wield
Waterbreathing

Bow Gun
Night Killer
Headgear
Adaman Vest
Reflect Ring

Charge+7, Charge+10
Life Song

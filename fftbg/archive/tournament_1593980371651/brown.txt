Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Gorgewall
Female
Capricorn
67
53
Summoner
Throw
Counter Tackle
Equip Axe
Move-MP Up

Battle Axe

Feather Hat
Power Sleeve
Defense Armlet

Moogle, Ramuh, Titan, Odin, Leviathan, Lich, Cyclops
Shuriken, Bomb



NotBalrog
Monster
Capricorn
50
59
Ghoul










DLJuggernaut
Male
Aquarius
44
80
Mediator
Sing
Earplug
Martial Arts
Ignore Height



Leather Hat
Black Robe
Vanish Mantle

Persuade, Preach, Insult, Negotiate, Refute, Rehabilitate
Magic Song, Diamond Blade, Hydra Pit



Bruubarg
Male
Gemini
49
71
Archer
White Magic
Absorb Used MP
Doublehand
Fly

Snipe Bow

Red Hood
Clothes
Feather Boots

Charge+3, Charge+5, Charge+20
Cure, Cure 4, Raise 2, Shell 2, Wall, Esuna

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Evdoggity
Female
Aries
41
61
Time Mage
Draw Out
Parry
Magic Attack UP
Swim

Mace of Zeus

Leather Hat
Judo Outfit
Battle Boots

Haste, Haste 2, Slow 2, Immobilize, Reflect, Demi, Demi 2, Stabilize Time, Meteor
Koutetsu, Bizen Boat, Murasame, Muramasa



HaateXIII
Female
Sagittarius
74
66
Thief
Punch Art
Counter
Long Status
Retreat

Cultist Dagger

Black Hood
Earth Clothes
Bracer

Gil Taking, Steal Heart, Steal Weapon
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



Mpghappiness
Male
Gemini
71
52
Samurai
Summon Magic
Earplug
Doublehand
Teleport

Asura Knife

Bronze Helmet
Mythril Armor
Diamond Armlet

Asura, Kiyomori
Moogle, Ramuh, Titan, Golem, Carbunkle, Leviathan



Nekojin
Male
Capricorn
75
54
Mime

Mana Shield
Dual Wield
Waterbreathing



Green Beret
Brigandine
Dracula Mantle

Mimic


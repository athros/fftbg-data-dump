Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Randgridr
Female
Pisces
46
63
Squire
Charge
PA Save
Secret Hunt
Retreat

Long Sword
Bronze Shield
Flash Hat
Carabini Mail
Defense Armlet

Accumulate, Dash, Throw Stone, Wish
Charge+1, Charge+5, Charge+7



G1nger4le
Female
Scorpio
63
79
Time Mage
Steal
Counter Magic
Attack UP
Fly

Gokuu Rod

Thief Hat
Silk Robe
N-Kai Armlet

Haste, Haste 2, Slow, Reflect, Stabilize Time
Gil Taking, Steal Status



Thaetreis
Male
Cancer
74
42
Archer
Time Magic
Dragon Spirit
Doublehand
Jump+3

Long Bow

Thief Hat
Power Sleeve
Magic Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7, Charge+20
Haste, Slow, Slow 2, Stop, Immobilize, Reflect, Quick, Demi 2, Meteor



Colm Erk9
Male
Leo
74
69
Chemist
Black Magic
Critical Quick
Concentrate
Jump+3

Orichalcum

Red Hood
Mystic Vest
Cursed Ring

Potion, Hi-Potion, Ether, Eye Drop, Echo Grass, Remedy, Phoenix Down
Fire 2, Bolt, Bolt 4, Flare

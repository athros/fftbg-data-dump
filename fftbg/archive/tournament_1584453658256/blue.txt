Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LASERJESUS1337
Female
Aries
59
73
Oracle
Black Magic
Auto Potion
Beastmaster
Move-MP Up

Octagon Rod

Green Beret
Wizard Robe
Elf Mantle

Blind, Poison, Life Drain, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy
Fire, Fire 3, Fire 4, Bolt 2, Bolt 4, Frog



WhiteN0lse
Female
Taurus
49
49
Oracle
Battle Skill
Parry
Dual Wield
Levitate

Battle Bamboo
Cypress Rod
Headgear
Linen Robe
Battle Boots

Spell Absorb, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Armor Break, Weapon Break, Magic Break, Stasis Sword, Justice Sword



AdmiralPikachu
Male
Cancer
67
70
Mime

Critical Quick
Attack UP
Move-MP Up



Black Hood
Earth Clothes
Magic Gauntlet

Mimic




RunicMagus
Male
Gemini
56
41
Mime

PA Save
Secret Hunt
Lava Walking



Thief Hat
Earth Clothes
Leather Mantle

Mimic


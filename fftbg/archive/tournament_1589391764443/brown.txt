Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Catfashions
Male
Cancer
47
48
Mime

Mana Shield
Secret Hunt
Move+2



Headgear
Mythril Vest
Diamond Armlet

Mimic




RjA0zcOQ96
Female
Serpentarius
77
58
Summoner
Black Magic
MP Restore
Short Charge
Move-HP Up

Thunder Rod

Black Hood
Linen Robe
Magic Gauntlet

Moogle, Shiva, Golem, Odin, Leviathan, Lich
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice



Kyune
Male
Aquarius
39
55
Geomancer
Punch Art
Arrow Guard
Concentrate
Jump+2

Slasher
Ice Shield
Thief Hat
Clothes
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Seal Evil



MrFlabyo
Female
Taurus
76
60
Geomancer
Talk Skill
Parry
Equip Polearm
Move+2

Cypress Rod
Ice Shield
Triangle Hat
Chameleon Robe
Angel Ring

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Persuade, Praise, Negotiate

Player: !White
Team: White Team
Palettes: White/Blue



Mudrockk
Monster
Virgo
54
72
Bull Demon










Coralreeferz
Female
Aquarius
58
64
Archer
Battle Skill
Counter
Doublehand
Levitate

Silver Bow

Crystal Helmet
Chain Vest
Battle Boots

Charge+2, Charge+3, Charge+4, Charge+10, Charge+20
Head Break, Armor Break, Justice Sword, Dark Sword, Night Sword



Fenrislfr
Female
Gemini
79
72
Samurai
Throw
PA Save
Defend
Move+2

Koutetsu Knife

Mythril Helmet
Diamond Armor
Defense Armlet

Bizen Boat, Muramasa
Shuriken, Dictionary



Joewcarson
Male
Pisces
78
69
Lancer
White Magic
Damage Split
Doublehand
Move+3

Obelisk

Platinum Helmet
Genji Armor
Rubber Shoes

Level Jump5, Vertical Jump6
Cure, Cure 2, Raise, Reraise, Regen, Protect 2, Esuna, Holy

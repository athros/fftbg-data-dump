Player: !Black
Team: Black Team
Palettes: Black/Red



NIghtdew14
Female
Virgo
63
69
Dancer
Time Magic
Regenerator
Magic Attack UP
Move-HP Up

Persia

Headgear
Silk Robe
Angel Ring

Void Storage
Haste, Slow 2, Immobilize, Reflect, Demi, Demi 2, Stabilize Time



E Ballard
Female
Capricorn
45
44
Dancer
Summon Magic
Sunken State
Equip Axe
Jump+1

Flail

Green Beret
Wizard Robe
Elf Mantle

Nameless Dance, Obsidian Blade
Ifrit, Golem, Carbunkle, Odin



Dogsandcatsand
Female
Aquarius
65
63
Wizard
Summon Magic
Speed Save
Magic Attack UP
Teleport

Main Gauche

Green Beret
Leather Outfit
Leather Mantle

Fire, Fire 2, Fire 3, Fire 4, Bolt, Ice 3, Ice 4, Empower, Frog
Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Leviathan, Salamander, Lich



SomthingMore
Male
Aquarius
58
72
Knight
Talk Skill
Catch
Equip Bow
Swim

Bow Gun
Escutcheon
Diamond Helmet
Gold Armor
Germinas Boots

Head Break, Speed Break
Persuade, Threaten, Solution, Rehabilitate

Final Bets: white - 10 bets for 4,050G (38.4%, x1.60); black - 9 bets for 6,484G (61.6%, x0.62)

white bets:
Rytor: 1,069G (26.4%, 10,690G)
ko2q: 672G (16.6%, 672G)
Aldrammech: 632G (15.6%, 632G)
letdowncity: 512G (12.6%, 27,983G)
Smashy: 392G (9.7%, 392G)
gorgewall: 201G (5.0%, 2,900G)
datadrivenbot: 200G (4.9%, 51,439G)
Tarheels218: 200G (4.9%, 1,474G)
Zephyrik: 100G (2.5%, 315G)
getthemoneyz: 72G (1.8%, 1,562,747G)

black bets:
randgridr: 1,365G (21.1%, 1,365G)
BirbBrainsBot: 1,000G (15.4%, 138,025G)
dogsandcatsand: 1,000G (15.4%, 46,804G)
Thyrandaal: 1,000G (15.4%, 206,271G)
powergems: 818G (12.6%, 818G)
VolgraTheMoose: 501G (7.7%, 2,903G)
NIghtdew14: 500G (7.7%, 2,987G)
AllInBot: 200G (3.1%, 200G)
MinBetBot: 100G (1.5%, 16,537G)

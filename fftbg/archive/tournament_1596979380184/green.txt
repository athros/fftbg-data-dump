Player: !Green
Team: Green Team
Palettes: Green/White



Just Here2
Female
Sagittarius
40
62
Monk
Elemental
Counter Flood
Attack UP
Ignore Terrain



Cachusha
Mystic Vest
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Reinoe
Male
Virgo
76
56
Bard
White Magic
Dragon Spirit
Attack UP
Lava Walking

Bloody Strings

Feather Hat
Plate Mail
Genji Gauntlet

Space Storage, Hydra Pit
Cure 2, Raise, Reraise, Esuna



Beowulfrulez
Female
Aquarius
63
58
Priest
Battle Skill
Regenerator
Dual Wield
Waterbreathing

Morning Star
Star Bag
Headgear
Power Sleeve
Angel Ring

Raise, Regen, Protect, Protect 2, Wall, Esuna
Armor Break, Magic Break, Justice Sword



Maakur
Female
Cancer
74
47
Mime

Regenerator
Doublehand
Move+1



Cachusha
Mystic Vest
Bracer

Mimic


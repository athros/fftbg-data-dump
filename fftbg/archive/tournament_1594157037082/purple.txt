Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ZCKaiser
Female
Virgo
39
63
Archer
Draw Out
Counter Magic
Doublehand
Ignore Terrain

Ultimus Bow

Feather Hat
Black Costume
Diamond Armlet

Charge+1, Charge+4
Asura, Heaven's Cloud, Muramasa



Randgridr
Male
Aquarius
60
40
Mime

Brave Save
Equip Shield
Waterwalking


Crystal Shield
Headgear
Chameleon Robe
Red Shoes

Mimic




Nekojin
Male
Capricorn
48
69
Bard
Steal
Counter Tackle
Magic Attack UP
Move+1

Bloody Strings

Green Beret
Linen Cuirass
Magic Ring

Life Song, Cheer Song, Battle Song, Space Storage
Gil Taking, Steal Weapon, Leg Aim



Meta Five
Male
Cancer
47
47
Mediator
Battle Skill
Mana Shield
Defense UP
Move+3

Stone Gun

Feather Hat
Leather Outfit
Angel Ring

Invitation, Threaten, Negotiate, Mimic Daravon, Rehabilitate
Armor Break, Weapon Break, Speed Break, Dark Sword

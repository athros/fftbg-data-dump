Final Bets: white - 13 bets for 6,250G (50.6%, x0.98); black - 11 bets for 6,094G (49.4%, x1.03)

white bets:
Aldrammech: 2,235G (35.8%, 2,235G)
roqqqpsi: 1,449G (23.2%, 72,455G)
killth3kid: 1,000G (16.0%, 109,929G)
Zagorsek: 380G (6.1%, 380G)
redrhinofever: 280G (4.5%, 2,866G)
gorgewall: 201G (3.2%, 19,818G)
datadrivenbot: 200G (3.2%, 55,886G)
SephDarkheart: 140G (2.2%, 140G)
TasisSai: 104G (1.7%, 104G)
nhammen: 100G (1.6%, 12,484G)
turbn: 100G (1.6%, 665G)
raenrond: 50G (0.8%, 581G)
roofiepops: 11G (0.2%, 3,679G)

black bets:
Firesheath: 1,371G (22.5%, 1,371G)
TheChainNerd: 1,000G (16.4%, 31,581G)
BirbBrainsBot: 1,000G (16.4%, 127,957G)
DudeMonkey77: 721G (11.8%, 721G)
Cryptopsy70: 600G (9.8%, 20,866G)
reinoe: 500G (8.2%, 38,577G)
getthemoneyz: 302G (5.0%, 1,223,807G)
Rytor: 200G (3.3%, 19,252G)
randgridr: 200G (3.3%, 2,049G)
OtherBrand: 100G (1.6%, 1,099G)
upidstupid: 100G (1.6%, 1,577G)

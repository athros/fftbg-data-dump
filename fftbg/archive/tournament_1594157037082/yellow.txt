Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zagorsek
Female
Gemini
69
64
Archer
Throw
Meatbone Slash
Long Status
Jump+3

Cute Bag
Crystal Shield
Black Hood
Secret Clothes
Dracula Mantle

Charge+1, Charge+3, Charge+4, Charge+7
Knife



Aldrammech
Male
Virgo
73
70
Ninja
Jump
Auto Potion
Halve MP
Move+2

Ninja Edge
Ninja Edge
Holy Miter
Brigandine
Rubber Shoes

Staff, Axe
Level Jump8, Vertical Jump8



Just Here2
Female
Taurus
57
65
Lancer
Draw Out
Counter Flood
Secret Hunt
Waterwalking

Spear
Aegis Shield
Crystal Helmet
Chain Mail
Bracer

Level Jump3, Vertical Jump8
Asura, Bizen Boat, Murasame, Masamune



DeathTaxesAndAnime
Female
Capricorn
72
77
Lancer
Talk Skill
Damage Split
Equip Axe
Move+3

Sage Staff
Bronze Shield
Crystal Helmet
White Robe
Power Wrist

Level Jump8, Vertical Jump5
Invitation, Persuade, Praise, Threaten, Refute

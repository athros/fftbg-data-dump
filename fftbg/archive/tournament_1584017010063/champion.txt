Player: !zChamp
Team: Champion Team
Palettes: Green/White



I Nod My Head When I Lose
Female
Aries
71
41
Archer
Black Magic
HP Restore
Doublehand
Move-HP Up

Mythril Bow

Platinum Helmet
Mythril Vest
Spike Shoes

Charge+1, Charge+2, Charge+7, Charge+10
Fire 2, Bolt 2, Flare



Maeveen
Female
Aries
75
65
Priest
Draw Out
Earplug
Defend
Ignore Terrain

Sage Staff

Thief Hat
Silk Robe
Sprint Shoes

Cure, Cure 3, Raise 2, Reraise, Regen, Protect 2, Shell, Esuna
Koutetsu, Bizen Boat, Kiyomori, Masamune



E7bbk
Male
Cancer
75
70
Archer
White Magic
Meatbone Slash
Doublehand
Levitate

Gastrafitis

Headgear
Clothes
Elf Mantle

Charge+3, Charge+4, Charge+10
Cure, Raise, Raise 2, Reraise, Protect, Shell, Shell 2, Esuna, Magic Barrier



Rnark
Male
Aries
51
70
Ninja
Elemental
Damage Split
Equip Bow
Waterbreathing

Night Killer
Bow Gun
Feather Hat
Leather Outfit
Feather Boots

Shuriken, Wand
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

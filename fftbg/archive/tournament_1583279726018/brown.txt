Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Krowbird
Female
Aquarius
42
45
Priest
Elemental
Mana Shield
Equip Polearm
Fly

Iron Fan

Triangle Hat
Black Robe
Battle Boots

Cure, Raise, Raise 2, Protect, Esuna
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind



Vivithegr8
Male
Gemini
46
61
Mediator
Sing
Brave Up
Sicken
Move+3

Bestiary

Golden Hairpin
Robe of Lords
Sprint Shoes

Solution, Refute, Rehabilitate
Angel Song, Life Song, Cheer Song, Battle Song, Sky Demon



Elelor
Female
Cancer
59
42
Geomancer
Yin Yang Magic
Sunken State
Equip Gun
Move+2

Madlemgen
Crystal Shield
Golden Hairpin
Black Costume
Bracer

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Blind, Spell Absorb, Pray Faith, Doubt Faith, Dispel Magic



Vampire Killer
Male
Gemini
71
58
Geomancer
Throw
Regenerator
Defense UP
Teleport

Battle Axe
Genji Shield
Green Beret
White Robe
Magic Ring

Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Bomb, Sword, Stick

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Hales Bopp It
Female
Gemini
68
75
Ninja
Steal
Speed Save
Doublehand
Move-HP Up

Hydra Bag

Red Hood
Adaman Vest
Germinas Boots

Bomb
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory



Dem0gency
Female
Virgo
72
50
Chemist
Dance
Speed Save
Martial Arts
Retreat



Leather Hat
Chain Vest
Sprint Shoes

Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Witch Hunt, Wiznaibus, Polka Polka, Nameless Dance, Obsidian Blade



Dantetouhou
Female
Aries
76
72
Squire
Jump
Parry
Beastmaster
Retreat

Night Killer
Buckler
Red Hood
Mythril Vest
Sprint Shoes

Throw Stone, Heal, Yell, Scream, Ultima
Level Jump4, Vertical Jump8



Rocl
Male
Gemini
48
72
Chemist
Steal
Faith Up
Defense UP
Move+1

Mage Masher

Twist Headband
Mystic Vest
Cherche

Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Remedy, Phoenix Down
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Arm Aim

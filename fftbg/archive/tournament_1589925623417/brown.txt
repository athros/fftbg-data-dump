Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Galkife
Female
Libra
80
41
Calculator
White Magic
Faith Save
Defense UP
Move+2

Papyrus Codex

Barette
Chameleon Robe
Jade Armlet

Height, Prime Number, 5, 3
Protect, Protect 2, Shell, Esuna, Holy



SQUiDSQUARKLIN
Male
Capricorn
53
61
Geomancer
Summon Magic
Sunken State
Long Status
Swim

Battle Axe
Gold Shield
Flash Hat
Chameleon Robe
108 Gems

Pitfall, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Moogle, Carbunkle, Bahamut, Odin, Lich



Baconbacon1207
Male
Capricorn
74
76
Thief
Punch Art
Counter
Equip Polearm
Jump+1

Musk Rod

Golden Hairpin
Mystic Vest
Power Wrist

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Arm Aim
Spin Fist, Pummel, Earth Slash, Revive



O Heyno
Female
Taurus
78
53
Mediator
Yin Yang Magic
Counter Tackle
Beastmaster
Ignore Height

Bestiary

Holy Miter
Light Robe
Reflect Ring

Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute
Blind, Life Drain, Zombie, Silence Song, Blind Rage, Confusion Song

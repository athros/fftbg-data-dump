Player: !zChamp
Team: Champion Team
Palettes: Black/Red



R Raynos
Male
Gemini
66
52
Lancer
Draw Out
Damage Split
Doublehand
Retreat

Gokuu Rod

Iron Helmet
Plate Mail
Cursed Ring

Level Jump8, Vertical Jump7
Asura, Koutetsu, Heaven's Cloud, Kikuichimoji



Superdevon1
Monster
Taurus
67
41
Black Goblin










Arkreaver
Male
Scorpio
77
78
Knight
Steal
Abandon
Short Status
Move+1

Save the Queen
Genji Shield
Mythril Helmet
Carabini Mail
Battle Boots

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Night Sword, Explosion Sword
Gil Taking, Steal Heart, Steal Armor, Steal Shield



WireLord
Female
Leo
73
80
Calculator
Black Magic
Mana Shield
Equip Bow
Jump+3

Poison Bow

Thief Hat
Wizard Robe
Defense Armlet

CT, 5, 4
Bolt, Ice, Ice 3, Ice 4, Empower, Frog

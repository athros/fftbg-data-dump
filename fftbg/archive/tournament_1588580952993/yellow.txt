Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Female
Taurus
65
60
Geomancer
Dance
MP Restore
Halve MP
Swim

Battle Axe
Platinum Shield
Feather Hat
Light Robe
Defense Armlet

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Slow Dance, Polka Polka



Witchlight
Female
Pisces
81
69
Ninja
Yin Yang Magic
MP Restore
Martial Arts
Waterbreathing

Hidden Knife
Kunai
Red Hood
Chain Vest
Angel Ring

Knife, Ninja Sword
Poison, Silence Song, Blind Rage, Confusion Song, Sleep



Anethum
Male
Leo
67
48
Mime

Brave Up
Dual Wield
Ignore Height



Twist Headband
Mythril Armor
Power Wrist

Mimic




ALY327
Male
Taurus
74
39
Oracle
Battle Skill
Earplug
Equip Sword
Retreat

Koutetsu Knife

Headgear
Adaman Vest
Small Mantle

Poison, Spell Absorb, Life Drain, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep
Armor Break, Weapon Break, Explosion Sword

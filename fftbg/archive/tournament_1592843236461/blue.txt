Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Extinctrational
Male
Aries
75
65
Samurai
Summon Magic
Speed Save
Defend
Move-MP Up

Kiyomori

Iron Helmet
Wizard Robe
Diamond Armlet

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Odin



Anasetsuken
Male
Scorpio
70
71
Oracle
Draw Out
Critical Quick
Equip Armor
Jump+2

Papyrus Codex

Platinum Helmet
Diamond Armor
Defense Ring

Life Drain, Blind Rage, Dispel Magic
Kikuichimoji



Zagorsek
Female
Aquarius
79
42
Priest
Jump
Abandon
Defense UP
Retreat

White Staff

Cachusha
Wizard Robe
Bracer

Cure, Cure 2, Raise, Raise 2, Wall, Esuna
Level Jump5, Vertical Jump6



Grininda
Male
Capricorn
48
70
Mime

Counter
Maintenance
Jump+2



Triangle Hat
Wizard Outfit
Defense Ring

Mimic


Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Female
Scorpio
51
57
Mediator
Elemental
Regenerator
Maintenance
Jump+1

Blaze Gun

Black Hood
Adaman Vest
Red Shoes

Persuade, Solution, Insult, Negotiate, Refute
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind



Miku Shikhu
Female
Aquarius
76
45
Thief
Talk Skill
MA Save
Magic Defense UP
Swim

Platinum Sword

Flash Hat
Black Costume
Spike Shoes

Steal Accessory, Steal Status, Leg Aim
Invitation, Persuade, Preach, Solution, Death Sentence



Gorgewall
Male
Sagittarius
52
78
Monk
Charge
Faith Save
Equip Shield
Jump+1


Escutcheon
Green Beret
Clothes
Genji Gauntlet

Pummel, Purification, Chakra
Charge+5



HaychDub
Female
Leo
49
53
Ninja
Black Magic
Counter Magic
Magic Attack UP
Move+1

Spell Edge
Short Edge
Feather Hat
Leather Outfit
Magic Ring

Bomb, Knife, Hammer, Ninja Sword, Spear, Wand
Fire 4, Bolt 3, Ice 2, Frog

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



GladiatorLupe
Female
Leo
52
39
Time Mage
Elemental
HP Restore
Maintenance
Move-MP Up

White Staff

Red Hood
Earth Clothes
Feather Boots

Haste 2, Slow 2, Immobilize, Demi, Demi 2, Meteor
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Sinnyil2
Female
Aquarius
76
77
Knight
Elemental
Caution
Short Charge
Waterwalking

Defender
Aegis Shield
Cross Helmet
Diamond Armor
Defense Armlet

Head Break, Shield Break, Weapon Break, Magic Break
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



L2 Sentinel
Male
Scorpio
65
64
Monk
Draw Out
Counter Tackle
Dual Wield
Jump+3



Triangle Hat
Mythril Vest
Elf Mantle

Revive
Asura, Heaven's Cloud, Kikuichimoji



ValtonZenola
Male
Aries
53
71
Summoner
Punch Art
Faith Save
Concentrate
Move-HP Up

Ice Rod

Holy Miter
Earth Clothes
Feather Boots

Moogle, Carbunkle
Pummel, Secret Fist, Chakra, Revive

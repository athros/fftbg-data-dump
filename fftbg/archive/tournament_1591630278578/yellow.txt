Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Error72
Male
Aries
44
67
Bard
Steal
Faith Save
Equip Gun
Jump+2

Glacier Gun

Thief Hat
Clothes
Bracer

Angel Song, Cheer Song, Magic Song, Nameless Song, Sky Demon
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Leg Aim



FoeSquirrel
Female
Cancer
53
45
Lancer
Dance
Critical Quick
Dual Wield
Move+1

Spear
Partisan
Platinum Helmet
Silk Robe
Diamond Armlet

Level Jump2, Vertical Jump2
Wiznaibus, Disillusion, Nameless Dance, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit



Laserman1000
Female
Libra
49
55
Summoner
Throw
Catch
Short Charge
Swim

Panther Bag

Thief Hat
Linen Robe
Feather Boots

Ramuh, Titan, Carbunkle, Bahamut, Leviathan, Silf, Lich
Shuriken



VolgraTheMoose
Male
Aquarius
40
68
Archer
Punch Art
Abandon
Doublehand
Teleport

Glacier Gun

Twist Headband
Wizard Outfit
Germinas Boots

Charge+3, Charge+5, Charge+7, Charge+10
Earth Slash, Purification, Revive

Player: !Green
Team: Green Team
Palettes: Green/White



Alc Trinity
Monster
Cancer
37
80
Explosive










Maeveen
Male
Aquarius
80
64
Chemist
Punch Art
MP Restore
Magic Defense UP
Jump+3

Panther Bag

Cachusha
Wizard Outfit
Defense Armlet

Hi-Potion, X-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down
Spin Fist, Chakra, Revive



Neetneph
Female
Cancer
63
60
Calculator
Black Magic
Auto Potion
Defend
Levitate

Papyrus Codex

Twist Headband
Black Robe
Rubber Shoes

CT, Prime Number
Fire 4, Bolt, Bolt 3, Ice, Ice 2, Ice 4, Death



Strumisgod
Female
Sagittarius
41
61
Calculator
Limit
Brave Up
Secret Hunt
Move-MP Up

Bestiary

Grand Helmet
Earth Clothes
Angel Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

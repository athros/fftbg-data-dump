Player: !Black
Team: Black Team
Palettes: Black/Red



Mechjeasus
Male
Serpentarius
59
68
Priest
Summon Magic
Regenerator
Attack UP
Retreat

Wizard Staff

Flash Hat
Brigandine
Small Mantle

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell, Esuna
Shiva, Ramuh, Bahamut, Salamander, Lich



AutomaticZen
Monster
Cancer
59
73
Dryad










Willjin
Male
Leo
74
40
Ninja
Summon Magic
Blade Grasp
Equip Polearm
Retreat

Cashmere
Gokuu Rod
Green Beret
Chain Vest
Feather Mantle

Staff, Stick, Wand
Moogle, Silf, Zodiac



Filly Pls
Female
Sagittarius
58
40
Thief
Charge
Brave Up
Equip Bow
Retreat

Cross Bow

Triangle Hat
Leather Outfit
Feather Mantle

Steal Heart, Steal Armor, Steal Shield
Charge+2, Charge+10

Final Bets: red - 14 bets for 20,973G (70.9%, x0.41); blue - 11 bets for 8,614G (29.1%, x2.43)

red bets:
reinoe: 5,000G (23.8%, 56,443G)
AllInBot: 3,908G (18.6%, 3,908G)
roofiepops: 3,803G (18.1%, 3,803G)
Aldrammech: 2,435G (11.6%, 2,435G)
Forkmore: 2,038G (9.7%, 2,038G)
SkylerBunny: 1,528G (7.3%, 1,528G)
Laserman1000: 600G (2.9%, 25,800G)
iBardic: 500G (2.4%, 5,733G)
koeeh: 297G (1.4%, 297G)
SWATb1gdog: 264G (1.3%, 264G)
datadrivenbot: 200G (1.0%, 69,081G)
CosmicTactician: 200G (1.0%, 39,585G)
Hojosu: 100G (0.5%, 100G)
Thyrandaal: 100G (0.5%, 30,626G)

blue bets:
HuffFlex: 3,000G (34.8%, 34,356G)
BirbBrainsBot: 1,000G (11.6%, 181,524G)
brokenknight201: 800G (9.3%, 14,399G)
E_Ballard: 732G (8.5%, 55,053G)
ANFz: 627G (7.3%, 627G)
DLJuggernaut: 500G (5.8%, 13,157G)
Helllyesss: 500G (5.8%, 1,062G)
Zbgs: 500G (5.8%, 21,983G)
getthemoneyz: 494G (5.7%, 1,733,037G)
holdenmagronik: 360G (4.2%, 3,514G)
gorgewall: 101G (1.2%, 21,220G)

Player: !Green
Team: Green Team
Palettes: Green/White



Superdevon1
Female
Aquarius
68
46
Oracle
Time Magic
Caution
Short Status
Move-HP Up

Battle Folio

Thief Hat
Earth Clothes
Leather Mantle

Blind, Poison, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Sleep
Haste, Immobilize, Float, Reflect, Quick, Stabilize Time



Lyonslegacy
Male
Leo
77
46
Archer
Time Magic
Counter
Concentrate
Lava Walking

Ultimus Bow

Gold Helmet
Earth Clothes
Defense Ring

Charge+1
Haste, Slow 2, Float, Reflect, Quick, Demi 2, Stabilize Time



Gooblz
Monster
Capricorn
68
48
Iron Hawk










Sinnyil2
Female
Aquarius
77
48
Thief
Talk Skill
Counter Flood
Equip Bow
Move+2

Snipe Bow

Leather Hat
Chain Vest
Bracer

Gil Taking, Steal Heart, Steal Shield, Steal Status, Leg Aim
Death Sentence, Insult, Mimic Daravon, Refute

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Blain Cooper
Male
Libra
78
45
Ninja
Item
Counter Flood
Throw Item
Retreat

Kunai
Flail
Twist Headband
Judo Outfit
Cursed Ring

Shuriken, Wand
Potion, Ether, Holy Water, Phoenix Down



R Raynos
Monster
Libra
56
51
Explosive










Numbersborne
Female
Taurus
45
80
Summoner
Draw Out
Dragon Spirit
Equip Gun
Teleport

Battle Folio

Green Beret
Power Sleeve
Angel Ring

Moogle, Shiva, Ifrit, Titan, Carbunkle, Leviathan, Silf, Lich
Koutetsu, Bizen Boat, Heaven's Cloud



Omegasuspekt
Male
Aries
53
47
Ninja
Talk Skill
Damage Split
Equip Gun
Levitate

Bestiary
Battle Folio
Black Hood
Power Sleeve
Red Shoes

Shuriken
Invitation, Threaten, Insult, Refute

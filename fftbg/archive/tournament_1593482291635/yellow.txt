Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RageImmortaI
Female
Cancer
57
62
Geomancer
Black Magic
MP Restore
Long Status
Levitate

Slasher
Buckler
Green Beret
Power Sleeve
Chantage

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire 4, Bolt, Bolt 4, Ice 4, Empower



Shalloween
Female
Libra
79
80
Oracle
Charge
Parry
Doublehand
Waterbreathing

Octagon Rod

Triangle Hat
Judo Outfit
Red Shoes

Blind, Spell Absorb, Life Drain, Zombie, Silence Song, Blind Rage, Dispel Magic, Petrify
Charge+4, Charge+5, Charge+10



Oobs56
Female
Libra
57
63
Dancer
Basic Skill
Speed Save
Equip Armor
Move+1

Cashmere

Grand Helmet
Plate Mail
Diamond Armlet

Slow Dance, Polka Polka, Obsidian Blade, Void Storage
Heal, Yell



HaateXIII
Male
Libra
69
77
Thief
Battle Skill
Damage Split
Equip Sword
Fly

Defender

Holy Miter
Secret Clothes
Power Wrist

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Head Break, Shield Break, Speed Break, Mind Break, Justice Sword

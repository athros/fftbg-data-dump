Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Digitalsocrates
Male
Virgo
66
66
Knight
Talk Skill
HP Restore
Concentrate
Move+1

Mythril Sword
Ice Shield
Circlet
Linen Robe
Leather Mantle

Head Break, Armor Break, Speed Break, Mind Break, Stasis Sword, Explosion Sword
Praise, Death Sentence, Insult, Refute, Rehabilitate



Flowinprose
Male
Aries
39
54
Calculator
Yin Yang Magic
Auto Potion
Dual Wield
Swim

Papyrus Codex
Papyrus Codex
Headgear
White Robe
Sprint Shoes

CT, Height, Prime Number, 4, 3
Doubt Faith, Zombie, Blind Rage, Sleep



Rislyeu
Female
Gemini
51
69
Mime

Meatbone Slash
Maintenance
Move-MP Up



Twist Headband
Wizard Outfit
Spike Shoes

Mimic




Laserman1000
Male
Pisces
69
78
Time Mage
Throw
Mana Shield
Short Charge
Waterwalking

Iron Fan

Cachusha
Linen Robe
108 Gems

Haste, Haste 2, Slow 2, Immobilize, Quick, Demi, Stabilize Time
Shuriken, Bomb, Ninja Sword

Final Bets: blue - 11 bets for 4,807G (40.8%, x1.45); green - 12 bets for 6,980G (59.2%, x0.69)

blue bets:
BirbBrainsBot: 1,000G (20.8%, 96,030G)
flowinprose: 1,000G (20.8%, 114,923G)
getthemoneyz: 722G (15.0%, 826,959G)
Laserman1000: 600G (12.5%, 27,600G)
Thyrandaal: 384G (8.0%, 384G)
Breakdown777: 300G (6.2%, 6,914G)
gorgewall: 201G (4.2%, 2,149G)
gogofromtogo: 200G (4.2%, 1,123G)
twelfthrootoftwo: 200G (4.2%, 709G)
nsm013: 100G (2.1%, 2,132G)
CosmicTactician: 100G (2.1%, 17,604G)

green bets:
Digitalsocrates: 2,000G (28.7%, 21,273G)
cam_ATS: 1,148G (16.4%, 1,148G)
LeoNightFury: 1,000G (14.3%, 4,152G)
Joaneyy: 692G (9.9%, 692G)
BlackFireUK: 462G (6.6%, 462G)
richardserious: 400G (5.7%, 1,668G)
E_Ballard: 384G (5.5%, 384G)
Milkman153: 220G (3.2%, 220G)
Polygamic: 212G (3.0%, 212G)
Monopool: 200G (2.9%, 3,671G)
nhammen: 162G (2.3%, 323G)
datadrivenbot: 100G (1.4%, 33,988G)

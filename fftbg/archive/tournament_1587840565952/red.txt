Player: !Red
Team: Red Team
Palettes: Red/Brown



LDSkinny
Female
Gemini
73
55
Oracle
Draw Out
Counter
Dual Wield
Lava Walking

Octagon Rod
Octagon Rod
Red Hood
Mythril Vest
Sprint Shoes

Blind, Poison, Zombie, Blind Rage, Petrify
Koutetsu, Murasame, Heaven's Cloud



Waterwatereverywhere
Female
Aquarius
78
52
Chemist
Steal
Parry
Dual Wield
Move-HP Up

Panther Bag
Hydra Bag
Triangle Hat
Mythril Vest
Leather Mantle

Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Phoenix Down
Gil Taking, Steal Heart, Steal Accessory



DrAntiSocial
Male
Leo
41
78
Lancer
Punch Art
Regenerator
Dual Wield
Waterwalking

Ivory Rod
Mythril Spear
Circlet
Diamond Armor
Feather Boots

Level Jump5, Vertical Jump6
Purification, Chakra, Revive



TheUnforgivenRage
Female
Scorpio
59
48
Samurai
Yin Yang Magic
Arrow Guard
Secret Hunt
Jump+2

Heaven's Cloud

Cross Helmet
Gold Armor
Defense Armlet

Kiyomori
Blind, Life Drain, Pray Faith, Foxbird

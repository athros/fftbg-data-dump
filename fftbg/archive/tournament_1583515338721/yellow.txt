Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WalkerNash
Female
Cancer
73
71
Monk
Summon Magic
Absorb Used MP
Equip Gun
Move+2

Bestiary

Flash Hat
Power Sleeve
Salty Rage

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Chakra, Seal Evil
Moogle, Titan, Carbunkle, Leviathan, Salamander, Silf



Meta Five
Female
Aquarius
71
50
Priest
Summon Magic
MA Save
Sicken
Jump+3

Rainbow Staff

Golden Hairpin
White Robe
Leather Mantle

Raise, Raise 2, Protect 2, Wall, Holy
Moogle, Ramuh, Carbunkle, Leviathan, Salamander, Silf



Fspll
Male
Leo
56
44
Archer
Throw
Caution
Doublehand
Fly

Long Bow

Red Hood
Mythril Vest
Angel Ring

Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Bomb



RegrettableLifeDecisions
Male
Aquarius
55
52
Summoner
Battle Skill
Parry
Equip Shield
Move+1

Rod
Mythril Shield
Flash Hat
Linen Robe
Power Wrist

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Leviathan, Salamander, Silf
Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Dark Sword

Player: !Red
Team: Red Team
Palettes: Red/Brown



Jeeboheebo
Male
Gemini
75
56
Mediator
Yin Yang Magic
Speed Save
Beastmaster
Move-HP Up

Glacier Gun

Headgear
Light Robe
Diamond Armlet

Praise, Solution, Refute
Blind, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Petrify



HASTERIOUS
Female
Cancer
47
51
Ninja
Charge
Meatbone Slash
Equip Polearm
Teleport

Partisan
Gungnir
Black Hood
Black Costume
Power Wrist

Staff
Charge+1, Charge+2, Charge+3, Charge+5, Charge+7, Charge+20



Estan AD
Monster
Capricorn
62
51
Dryad










Roqqqpsi
Female
Capricorn
51
54
Oracle
Talk Skill
Caution
Short Status
Fly

Iron Fan

Leather Hat
Brigandine
Germinas Boots

Poison, Spell Absorb, Silence Song, Dispel Magic, Paralyze, Sleep
Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate

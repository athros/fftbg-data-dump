Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesauceBoss
Male
Sagittarius
52
75
Ninja
Jump
Speed Save
Equip Gun
Jump+2

Stone Gun
Glacier Gun
Black Hood
Leather Outfit
Sprint Shoes

Shuriken, Ninja Sword
Level Jump4, Vertical Jump7



DrAntiSocial
Female
Capricorn
49
51
Chemist
Jump
Dragon Spirit
Magic Attack UP
Move+3

Star Bag

Golden Hairpin
Judo Outfit
Red Shoes

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down
Level Jump2, Vertical Jump5



Anethum
Male
Cancer
51
49
Ninja
White Magic
Counter
Secret Hunt
Waterbreathing

Sasuke Knife
Flame Whip
Green Beret
Chain Vest
Jade Armlet

Shuriken, Bomb, Hammer, Staff
Cure 2, Cure 3, Raise, Reraise, Protect, Shell 2, Wall



Waterwatereverywhere
Male
Cancer
49
69
Monk
Basic Skill
Caution
Beastmaster
Move+3



Golden Hairpin
Brigandine
Angel Ring

Pummel, Secret Fist, Purification, Seal Evil
Throw Stone, Heal, Cheer Up, Wish

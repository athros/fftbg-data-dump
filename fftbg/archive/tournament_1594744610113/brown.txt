Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Cryptopsy70
Male
Scorpio
65
69
Geomancer
Talk Skill
Auto Potion
Short Charge
Jump+2

Cute Bag
Round Shield
Barette
Black Robe
Feather Boots

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Praise, Threaten, Refute



Kithmar796
Female
Taurus
70
78
Priest
Jump
Sunken State
Maintenance
Move-MP Up

Oak Staff

Red Hood
Linen Robe
Battle Boots

Cure, Raise, Regen, Protect, Protect 2, Shell, Shell 2, Esuna
Level Jump4, Vertical Jump3



SQUiDSQUARKLIN
Male
Cancer
56
55
Lancer
White Magic
Distribute
Concentrate
Move-HP Up

Ivory Rod
Ice Shield
Bronze Helmet
Linen Cuirass
Bracer

Level Jump3, Vertical Jump6
Cure 4, Raise, Regen, Protect 2, Shell 2, Esuna



Hamborn
Male
Gemini
42
75
Knight
Basic Skill
MP Restore
Equip Knife
Waterbreathing

Air Knife
Ice Shield
Diamond Helmet
Bronze Armor
Magic Gauntlet

Head Break, Shield Break, Weapon Break, Justice Sword
Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up

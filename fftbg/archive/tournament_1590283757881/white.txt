Player: !White
Team: White Team
Palettes: White/Blue



Mpghappiness
Female
Leo
55
73
Geomancer
Steal
MP Restore
Dual Wield
Move+1

Giant Axe
Battle Axe
Leather Hat
Black Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Steal Armor, Steal Shield, Steal Weapon, Arm Aim, Leg Aim



ExecutedGiraffe
Male
Sagittarius
61
51
Summoner
White Magic
Mana Shield
Concentrate
Move+1

Bestiary

Flash Hat
Silk Robe
Elf Mantle

Shiva, Ifrit, Titan, Carbunkle, Leviathan, Silf
Cure, Cure 3, Reraise, Protect, Shell, Shell 2, Wall, Esuna, Holy



HaplessOne
Female
Leo
48
41
Samurai
Charge
Meatbone Slash
Defense UP
Move+3

Asura Knife

Gold Helmet
Diamond Armor
Defense Armlet

Bizen Boat, Muramasa
Charge+2, Charge+5, Charge+7



Powergems
Male
Leo
51
69
Monk
Talk Skill
Dragon Spirit
Magic Attack UP
Retreat



Green Beret
Mystic Vest
Magic Ring

Wave Fist, Earth Slash, Chakra, Revive
Threaten, Preach, Negotiate

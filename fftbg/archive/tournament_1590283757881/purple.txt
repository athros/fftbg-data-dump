Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zagorsek
Male
Leo
59
66
Archer
Throw
Speed Save
Long Status
Teleport

Hunting Bow
Escutcheon
Triangle Hat
Leather Outfit
Magic Ring

Charge+1, Charge+4
Shuriken, Bomb, Dictionary



Shalloween
Male
Capricorn
38
69
Chemist
Talk Skill
Meatbone Slash
Equip Sword
Levitate

Koutetsu Knife

Triangle Hat
Secret Clothes
Battle Boots

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Phoenix Down
Praise, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



O Heyno
Male
Taurus
64
50
Samurai
Basic Skill
Auto Potion
Doublehand
Ignore Terrain

Holy Lance

Crystal Helmet
Mythril Armor
Small Mantle

Asura, Bizen Boat, Murasame, Kikuichimoji
Throw Stone, Cheer Up, Scream



HASTERIOUS
Female
Sagittarius
72
42
Samurai
Basic Skill
Regenerator
Beastmaster
Ignore Height

Kikuichimoji

Mythril Helmet
Mythril Armor
Wizard Mantle

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Wish

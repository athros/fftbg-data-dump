Player: !Red
Team: Red Team
Palettes: Red/Brown



Urieltheflameofgod
Male
Aries
74
71
Chemist
Battle Skill
Counter Flood
Equip Bow
Fly

Windslash Bow

Black Hood
Wizard Outfit
Rubber Shoes

Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Soft, Phoenix Down
Shield Break, Weapon Break, Magic Break, Mind Break



Lawnboxer
Female
Sagittarius
65
63
Monk
Throw
Parry
Equip Armor
Teleport



Platinum Helmet
Genji Armor
Reflect Ring

Spin Fist, Purification, Seal Evil
Shuriken, Bomb, Sword, Dictionary



Lanshaft
Male
Gemini
59
78
Squire
Item
Counter
Equip Gun
Retreat

Papyrus Codex
Flame Shield
Green Beret
Gold Armor
Reflect Ring

Dash, Throw Stone, Heal, Tickle, Yell, Wish
Potion, Ether, Hi-Ether, Holy Water, Remedy, Phoenix Down



MoonSlayerRS
Female
Cancer
63
40
Summoner
Yin Yang Magic
Critical Quick
Long Status
Move+1

Wizard Rod

Holy Miter
Silk Robe
Elf Mantle

Moogle, Shiva, Ifrit, Golem, Carbunkle, Odin, Salamander, Cyclops
Blind, Spell Absorb, Pray Faith

Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Libra
49
50
Dancer
Basic Skill
Auto Potion
Magic Defense UP
Jump+3

Hydra Bag

Ribbon
Black Robe
Defense Ring

Wiznaibus, Last Dance, Obsidian Blade, Nether Demon
Accumulate, Yell, Ultima



Concles
Female
Sagittarius
58
65
Mime

Counter
Monster Talk
Levitate



Leather Hat
Mystic Vest
Defense Ring

Mimic




Holyonline
Male
Scorpio
77
51
Squire
Punch Art
Catch
Doublehand
Move+2

Broad Sword

Cross Helmet
Power Sleeve
Feather Boots

Dash, Heal, Tickle, Cheer Up, Scream
Spin Fist, Wave Fist, Purification, Revive



Aneyus
Male
Libra
69
67
Geomancer
Draw Out
Distribute
Defend
Move+2

Battle Axe
Gold Shield
Feather Hat
Wizard Robe
Diamond Armlet

Pitfall, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud

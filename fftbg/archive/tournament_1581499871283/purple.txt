Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Frencydark
Female
Gemini
59
67
Mediator
Punch Art
Critical Quick
Short Status
Teleport

Papyrus Codex

Headgear
Silk Robe
N-Kai Armlet

Praise, Negotiate, Mimic Daravon
Pummel, Purification, Revive



DaveStrider55
Male
Pisces
39
76
Archer
Throw
MP Restore
Doublehand
Waterwalking

Ice Bow

Green Beret
Wizard Outfit
Setiemson

Charge+2, Charge+7, Charge+20
Dictionary



MithrasArslan
Female
Virgo
45
51
Summoner
Charge
Speed Save
Equip Armor
Fly

Dragon Rod

Triangle Hat
Wizard Robe
Small Mantle

Moogle, Ifrit, Carbunkle, Leviathan, Silf
Charge+2, Charge+5, Charge+10



Buddychrist10
Female
Virgo
40
46
Squire
Charge
Brave Up
Beastmaster
Swim

Poison Bow

Triangle Hat
Wizard Outfit
Defense Ring

Accumulate, Dash, Throw Stone, Heal, Wish
Charge+1, Charge+2, Charge+4

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



LeonStrifeI
Monster
Scorpio
75
43
Tiamat










Kl0kwurk
Female
Sagittarius
54
52
Time Mage
Jump
Parry
Doublehand
Swim

Healing Staff

Headgear
Robe of Lords
Dracula Mantle

Haste, Haste 2, Stop, Immobilize, Reflect, Demi, Demi 2
Level Jump8, Vertical Jump5



Upvla
Male
Sagittarius
56
74
Lancer
Elemental
Absorb Used MP
Dual Wield
Ignore Terrain

Dragon Whisker
Obelisk
Cross Helmet
Chameleon Robe
Bracer

Level Jump8, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Gusty Wind, Lava Ball



HaychDub
Male
Pisces
41
70
Geomancer
Draw Out
MA Save
Defend
Teleport

Murasame
Mythril Shield
Ribbon
Clothes
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa

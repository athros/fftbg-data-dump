Player: !Green
Team: Green Team
Palettes: Green/White



Elelor
Female
Sagittarius
63
46
Summoner
Item
Sunken State
Equip Shield
Jump+2

Flame Rod
Gold Shield
Leather Hat
Silk Robe
Feather Boots

Moogle, Ramuh, Ifrit, Carbunkle, Fairy, Lich
Potion, Hi-Potion, X-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down



Baconbacon1207
Female
Cancer
43
43
Mime

Dragon Spirit
Dual Wield
Levitate



Flash Hat
Mythril Vest
Leather Mantle

Mimic




UmaiJam
Male
Aries
48
47
Knight
White Magic
Distribute
Attack UP
Move-MP Up

Defender
Escutcheon
Iron Helmet
Leather Armor
Diamond Armlet

Head Break, Weapon Break, Magic Break, Justice Sword
Cure, Cure 2, Reraise, Protect, Protect 2, Esuna



ScurvyMitch
Male
Cancer
50
64
Monk
Jump
Counter
Equip Polearm
Jump+1

Persia

Headgear
Power Sleeve
Elf Mantle

Pummel, Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Level Jump3, Vertical Jump7

Player: !Black
Team: Black Team
Palettes: Black/Red



Puraisu
Female
Libra
77
68
Geomancer
White Magic
Sunken State
Equip Polearm
Move+3

Murasame
Mythril Shield
Thief Hat
Mythril Vest
Setiemson

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Raise, Reraise, Protect 2, Shell, Esuna



Illudia
Female
Libra
74
75
Priest
Black Magic
PA Save
Doublehand
Jump+1

Wizard Staff

Red Hood
Robe of Lords
Battle Boots

Cure 2, Raise, Raise 2, Reraise, Protect, Shell 2, Esuna
Fire, Fire 2, Bolt 4, Ice, Empower, Death



HaplessOne
Female
Gemini
59
42
Wizard
Talk Skill
Catch
Short Charge
Jump+2

Poison Rod

Holy Miter
White Robe
Genji Gauntlet

Fire, Fire 2, Bolt, Bolt 2, Ice 2, Ice 3, Death
Invitation, Praise, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Rehabilitate



Extinctosaurus
Monster
Taurus
38
73
Bull Demon








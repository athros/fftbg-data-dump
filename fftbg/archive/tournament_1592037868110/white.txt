Player: !White
Team: White Team
Palettes: White/Blue



Bruubarg
Female
Scorpio
75
78
Ninja
Yin Yang Magic
Distribute
Beastmaster
Levitate

Spell Edge
Flail
Flash Hat
Mythril Vest
Rubber Shoes

Knife, Staff, Wand
Poison, Spell Absorb, Life Drain, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep



Lythe Caraker
Male
Aries
74
61
Samurai
Time Magic
Arrow Guard
Equip Knife
Move+1

Thunder Rod

Circlet
Mythril Armor
Spike Shoes

Asura, Koutetsu, Murasame
Haste, Stop, Immobilize, Reflect, Demi



CassiePhoenix
Female
Scorpio
69
73
Dancer
Summon Magic
Abandon
Concentrate
Jump+3

Ryozan Silk

Golden Hairpin
Mythril Vest
Wizard Mantle

Slow Dance, Nether Demon
Shiva, Titan, Silf



Mr Kindaro
Male
Virgo
48
69
Knight
Basic Skill
Speed Save
Halve MP
Ignore Height

Slasher
Gold Shield
Leather Helmet
Crystal Mail
Bracer

Magic Break, Speed Break, Dark Sword, Night Sword
Dash, Heal, Tickle, Yell, Fury, Wish

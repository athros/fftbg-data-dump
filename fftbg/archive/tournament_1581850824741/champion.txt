Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Mrasin
Male
Pisces
47
79
Time Mage
Item
Catch
Throw Item
Jump+1

Madlemgen

Black Hood
Leather Outfit
Power Wrist

Slow, Slow 2, Demi, Stabilize Time
Potion, Hi-Potion, X-Potion, Holy Water, Phoenix Down



Tristar49
Female
Leo
45
80
Lancer
Battle Skill
Earplug
Doublehand
Retreat

Mythril Spear

Genji Helmet
Reflect Mail
Bracer

Level Jump8, Vertical Jump3
Head Break, Shield Break, Magic Break, Stasis Sword, Justice Sword



Leakimiko
Male
Virgo
76
79
Bard
Punch Art
Earplug
Doublehand
Move+1

Bloody Strings

Flash Hat
Leather Armor
Power Wrist

Battle Song
Spin Fist, Secret Fist, Purification, Chakra, Revive



Byrdturbo
Male
Gemini
60
65
Bard
Charge
Damage Split
Doublehand
Retreat

Windslash Bow

Flash Hat
Carabini Mail
108 Gems

Battle Song, Nameless Song
Charge+1

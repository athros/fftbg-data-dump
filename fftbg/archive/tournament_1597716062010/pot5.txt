Final Bets: red - 8 bets for 21,357G (60.3%, x0.66); yellow - 10 bets for 14,039G (39.7%, x1.52)

red bets:
MagicBottle: 9,330G (43.7%, 18,295G)
Sairentozon7: 5,826G (27.3%, 11,652G)
Zeroroute: 2,846G (13.3%, 14,231G)
Drusiform: 1,349G (6.3%, 2,699G)
BirbBrainsBot: 1,000G (4.7%, 9,448G)
KasugaiRoastedPeas: 400G (1.9%, 11,045G)
latebit: 356G (1.7%, 356G)
muffin_money: 250G (1.2%, 12,396G)

yellow bets:
sinnyil2: 8,036G (57.2%, 16,072G)
SkylerBunny: 2,000G (14.2%, 16,485G)
Thyrandaal: 2,000G (14.2%, 340,200G)
Laserman1000: 650G (4.6%, 6,450G)
roofiepops: 474G (3.4%, 7,045G)
getthemoneyz: 378G (2.7%, 1,657,467G)
datadrivenbot: 200G (1.4%, 69,871G)
gorgewall: 101G (0.7%, 6,613G)
MinBetBot: 100G (0.7%, 6,098G)
AllInBot: 100G (0.7%, 100G)

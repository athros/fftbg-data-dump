Player: !Green
Team: Green Team
Palettes: Green/White



Gorgewall
Female
Capricorn
54
64
Oracle
Dance
Auto Potion
Magic Attack UP
Fly

Papyrus Codex

Headgear
Chain Vest
Germinas Boots

Blind, Life Drain, Zombie, Silence Song, Foxbird
Disillusion, Nameless Dance, Dragon Pit



Error72
Male
Gemini
51
44
Knight
Jump
Counter
Equip Sword
Waterwalking

Kikuichimoji
Platinum Shield
Bronze Helmet
Plate Mail
N-Kai Armlet

Armor Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Level Jump2, Vertical Jump7



Butterbelljedi
Female
Cancer
55
68
Geomancer
Black Magic
PA Save
Secret Hunt
Retreat

Giant Axe
Flame Shield
Holy Miter
Earth Clothes
108 Gems

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball
Fire 2, Fire 3, Bolt, Ice, Ice 2, Ice 3, Ice 4



Thyrandaal
Female
Sagittarius
59
44
Oracle
Draw Out
PA Save
Dual Wield
Move+2

Ivory Rod
Whale Whisker
Triangle Hat
Silk Robe
Elf Mantle

Doubt Faith, Zombie, Paralyze
Bizen Boat, Muramasa

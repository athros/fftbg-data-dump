Player: !White
Team: White Team
Palettes: White/Blue



LDSkinny
Male
Virgo
73
79
Oracle
Item
Mana Shield
Long Status
Move-HP Up

Battle Folio

Feather Hat
Chain Vest
Power Wrist

Poison, Doubt Faith, Zombie, Silence Song, Dispel Magic
Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Phoenix Down



Shalloween
Female
Aquarius
62
55
Summoner
Jump
MP Restore
Short Charge
Jump+3

Papyrus Codex

Headgear
Linen Robe
Sprint Shoes

Moogle, Shiva, Bahamut
Level Jump8, Vertical Jump4



Drusiform
Female
Aries
44
42
Wizard
Charge
Counter
Maintenance
Move-HP Up

Thunder Rod

Feather Hat
Silk Robe
Red Shoes

Fire 2, Fire 3, Bolt, Ice 2, Ice 3
Charge+5



TheKillerNacho
Female
Gemini
44
73
Summoner
Elemental
Earplug
Equip Shield
Waterbreathing

Rod
Platinum Shield
Golden Hairpin
Wizard Robe
Feather Mantle

Moogle, Ramuh, Carbunkle, Leviathan, Silf, Lich
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

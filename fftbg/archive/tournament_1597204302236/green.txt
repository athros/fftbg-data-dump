Player: !Green
Team: Green Team
Palettes: Green/White



Mirapoix
Male
Aquarius
72
48
Monk
Battle Skill
Counter
Attack UP
Move+3



Cachusha
Mythril Vest
Rubber Shoes

Wave Fist, Earth Slash, Secret Fist, Chakra, Revive
Weapon Break, Magic Break, Dark Sword



Thyrandaal
Male
Scorpio
72
75
Lancer
Talk Skill
Abandon
Halve MP
Ignore Height

Partisan
Gold Shield
Diamond Helmet
Diamond Armor
Red Shoes

Level Jump8, Vertical Jump8
Praise, Solution, Refute



Killth3kid
Monster
Gemini
70
54
Ghoul










ShanksMcShiv
Female
Taurus
50
41
Priest
Summon Magic
Dragon Spirit
Concentrate
Waterbreathing

Wizard Staff

Feather Hat
Leather Outfit
Feather Mantle

Cure, Cure 3, Raise, Raise 2, Esuna, Holy
Ramuh, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Fairy

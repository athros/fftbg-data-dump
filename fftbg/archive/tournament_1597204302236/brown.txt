Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Shadricklul
Female
Virgo
46
56
Ninja
Talk Skill
Auto Potion
Maintenance
Move-HP Up

Kunai
Ninja Edge
Green Beret
Black Costume
Defense Ring

Shuriken, Bomb, Knife, Sword, Hammer, Staff
Persuade, Praise, Threaten, Mimic Daravon, Refute



DeathTaxesAndAnime
Monster
Sagittarius
79
47
Wild Boar










Mesmaster
Female
Aquarius
49
79
Wizard
Punch Art
Mana Shield
Short Charge
Teleport

Rod

Triangle Hat
Linen Robe
Red Shoes

Fire, Fire 2, Fire 4, Bolt, Bolt 2, Ice, Empower, Frog, Flare
Secret Fist, Purification, Chakra, Revive, Seal Evil



Drusiform
Male
Gemini
73
78
Time Mage
Item
Counter Flood
Dual Wield
Levitate

Oak Staff
Wizard Staff
Twist Headband
Chameleon Robe
Red Shoes

Haste, Slow 2, Float, Demi, Demi 2, Stabilize Time
Potion, Elixir, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down

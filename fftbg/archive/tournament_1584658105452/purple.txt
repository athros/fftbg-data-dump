Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Rogueain
Female
Virgo
63
57
Knight
Elemental
Parry
Equip Axe
Levitate

Rainbow Staff
Kaiser Plate
Mythril Helmet
Chain Mail
Cherche

Armor Break, Shield Break, Weapon Break, Justice Sword
Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard



MantisFinch
Male
Aquarius
70
63
Knight
Item
Counter Tackle
Magic Attack UP
Lava Walking

Defender

Crystal Helmet
Linen Cuirass
Rubber Shoes

Speed Break, Justice Sword, Explosion Sword
Potion, Hi-Potion, Antidote, Soft, Holy Water, Phoenix Down



Wargles
Female
Capricorn
70
47
Ninja
Jump
Counter Flood
Equip Gun
Move+1

Ramia Harp
Ramia Harp
Holy Miter
Mystic Vest
Power Wrist

Shuriken, Ninja Sword
Level Jump3, Vertical Jump3



Darkp Ramza
Female
Gemini
59
73
Knight
White Magic
Damage Split
Doublehand
Lava Walking

Ancient Sword

Circlet
Carabini Mail
Reflect Ring

Power Break, Stasis Sword, Justice Sword, Night Sword
Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Shell, Wall, Esuna, Holy

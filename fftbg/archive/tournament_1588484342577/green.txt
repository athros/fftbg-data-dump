Player: !Green
Team: Green Team
Palettes: Green/White



Nifboy
Female
Sagittarius
50
43
Calculator
Demon Skill
MP Restore
Beastmaster
Ignore Height

Battle Folio

Headgear
Brigandine
Spike Shoes

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



ALY327
Female
Capricorn
59
38
Oracle
Time Magic
Speed Save
Short Status
Jump+1

Battle Bamboo

Holy Miter
White Robe
Spike Shoes

Pray Faith, Zombie, Foxbird, Dispel Magic, Paralyze
Haste 2, Stop, Immobilize, Reflect, Quick, Stabilize Time



Arlum
Male
Capricorn
68
77
Time Mage
Black Magic
Sunken State
Equip Sword
Move-HP Up

Iron Sword

Flash Hat
Light Robe
N-Kai Armlet

Haste, Slow, Slow 2, Stabilize Time
Fire 2, Fire 3, Empower



Powermhero
Female
Gemini
47
54
Mime

MA Save
Doublehand
Retreat



Thief Hat
Power Sleeve
Defense Ring

Mimic


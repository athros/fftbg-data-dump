Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Anox Skell
Male
Pisces
52
43
Knight
Throw
Counter Flood
Defend
Move+1

Defender
Buckler
Crystal Helmet
Bronze Armor
Defense Ring

Armor Break, Mind Break, Night Sword
Shuriken, Hammer



CT 5 Holy
Male
Leo
54
76
Chemist
Elemental
Distribute
Equip Polearm
Move+3

Iron Fan

Barette
Leather Outfit
Feather Mantle

Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Bioticism
Female
Aries
68
64
Summoner
Elemental
Counter Magic
Maintenance
Waterbreathing

Wizard Rod

Red Hood
White Robe
Rubber Shoes

Shiva, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Silf, Fairy
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Jaritras
Female
Taurus
76
55
Priest
Time Magic
Counter
Sicken
Teleport

Flame Whip

Cachusha
Linen Robe
N-Kai Armlet

Cure 2, Cure 4, Raise, Reraise, Protect, Shell 2, Wall, Esuna
Slow, Stop, Stabilize Time, Meteor

Player: !Black
Team: Black Team
Palettes: Black/Red



Glitchminded
Male
Libra
77
77
Monk
Black Magic
Counter Flood
Concentrate
Move+2



Twist Headband
Power Sleeve
Wizard Mantle

Spin Fist, Wave Fist, Earth Slash, Purification
Fire 4, Bolt, Bolt 2, Bolt 3, Ice 2, Ice 3, Ice 4



Stablo
Male
Aquarius
47
65
Time Mage
Yin Yang Magic
Damage Split
Equip Shield
Fly

Gold Staff
Escutcheon
Black Hood
Judo Outfit
Red Shoes

Haste, Haste 2, Slow 2, Float, Demi, Demi 2, Stabilize Time
Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify



Ragnarok784
Female
Scorpio
70
48
Summoner
Punch Art
Absorb Used MP
Short Charge
Move-HP Up

Ice Rod

Flash Hat
Rubber Costume
Red Shoes

Moogle, Ifrit, Leviathan, Salamander, Silf, Fairy, Lich
Pummel, Wave Fist, Earth Slash, Chakra, Revive



Danwithpleasure
Monster
Cancer
69
70
Dragon








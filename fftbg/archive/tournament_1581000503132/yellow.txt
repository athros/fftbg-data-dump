Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Artifact911
Male
Aquarius
54
69
Mime

Counter Magic
Equip Shield
Fly


Escutcheon
Golden Hairpin
Platinum Armor
Dracula Mantle

Mimic




Zerguzen
Female
Leo
40
66
Monk
Summon Magic
Mana Shield
Attack UP
Move+2



Green Beret
Earth Clothes
Diamond Armlet

Earth Slash, Purification, Revive, Seal Evil
Moogle, Odin, Leviathan, Salamander, Silf, Cyclops



DeathOfMarat
Male
Aquarius
54
47
Wizard
Basic Skill
Meatbone Slash
Magic Attack UP
Waterwalking

Assassin Dagger

Twist Headband
Wizard Outfit
Jade Armlet

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Ice 2, Frog
Accumulate, Dash, Heal, Tickle, Yell, Wish



DavenIII
Female
Pisces
59
62
Wizard
Jump
Regenerator
Equip Shield
Jump+2

Mythril Knife
Gold Shield
Red Hood
Chain Vest
Small Mantle

Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Death, Flare
Level Jump4, Vertical Jump6

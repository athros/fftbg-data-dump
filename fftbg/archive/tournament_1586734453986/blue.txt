Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Tougou
Female
Gemini
56
62
Geomancer
Draw Out
Absorb Used MP
Short Status
Waterbreathing

Asura Knife
Buckler
Holy Miter
Linen Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Muramasa



ArchKnightX
Male
Pisces
73
38
Chemist
Basic Skill
Regenerator
Halve MP
Levitate

Glacier Gun

Twist Headband
Mystic Vest
Small Mantle

X-Potion, Ether, Antidote, Soft, Phoenix Down
Accumulate, Dash, Throw Stone, Fury, Wish, Scream



Kronikle
Monster
Aquarius
43
42
Bomb










Kohlingen
Male
Aries
46
44
Oracle
Item
Faith Up
Equip Shield
Fly

Musk Rod
Diamond Shield
Feather Hat
Black Robe
N-Kai Armlet

Silence Song, Blind Rage, Dispel Magic, Dark Holy
Potion, X-Potion, Eye Drop, Echo Grass, Holy Water

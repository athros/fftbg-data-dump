Player: !Red
Team: Red Team
Palettes: Red/Brown



HASTERIOUS
Female
Virgo
72
48
Chemist
White Magic
Dragon Spirit
Magic Defense UP
Move+1

Star Bag

Golden Hairpin
Clothes
Feather Boots

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Cure 3, Raise, Reraise, Protect, Shell 2, Esuna



Humble Fabio
Monster
Aquarius
58
79
Iron Hawk










Alacor
Male
Serpentarius
50
71
Knight
Sing
MP Restore
Attack UP
Ignore Terrain

Rune Blade
Aegis Shield
Crystal Helmet
Mythril Armor
Dracula Mantle

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break
Cheer Song, Last Song, Hydra Pit



ThePineappleSalesman
Female
Cancer
62
74
Knight
Black Magic
Absorb Used MP
Magic Attack UP
Jump+2

Defender
Platinum Shield
Genji Helmet
Mythril Armor
Chantage

Armor Break, Mind Break, Dark Sword, Night Sword, Surging Sword
Bolt 4, Ice, Ice 4, Empower, Death

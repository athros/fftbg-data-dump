Player: !Green
Team: Green Team
Palettes: Green/White



Jeeboheebo
Monster
Scorpio
65
80
Gobbledeguck










Roofiepops
Female
Taurus
50
53
Thief
Punch Art
Speed Save
Short Status
Lava Walking

Short Edge

Headgear
Wizard Outfit
Genji Gauntlet

Steal Heart, Steal Armor, Steal Status
Purification, Chakra, Revive



StealthModeLocke
Female
Capricorn
80
60
Chemist
Throw
Critical Quick
Short Charge
Levitate

Romanda Gun

Twist Headband
Chain Vest
Dracula Mantle

Potion, Hi-Potion, Ether, Eye Drop, Soft, Phoenix Down
Shuriken, Bomb, Wand



Webb MD
Female
Scorpio
68
39
Chemist
Black Magic
Earplug
Beastmaster
Move-MP Up

Cute Bag

Leather Hat
Mystic Vest
Diamond Armlet

Potion, X-Potion, Eye Drop, Phoenix Down
Fire 3, Fire 4, Bolt, Ice 2, Ice 3

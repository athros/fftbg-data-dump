Player: !Black
Team: Black Team
Palettes: Black/Red



CrownOfHorns
Male
Pisces
46
61
Time Mage
Draw Out
Auto Potion
Maintenance
Swim

Battle Bamboo

Headgear
Judo Outfit
N-Kai Armlet

Haste, Haste 2, Slow, Immobilize, Reflect, Demi 2, Stabilize Time
Kiyomori, Kikuichimoji



Bruubarg
Female
Leo
69
64
Knight
Punch Art
Arrow Guard
Equip Axe
Move-HP Up

Flail
Flame Shield
Barbuta
Light Robe
Rubber Shoes

Armor Break, Shield Break, Speed Break
Spin Fist, Wave Fist, Chakra



Argentbast
Male
Virgo
43
80
Samurai
Item
Catch
Secret Hunt
Move+3

Kikuichimoji

Diamond Helmet
Diamond Armor
Feather Mantle

Asura, Koutetsu, Kiyomori, Muramasa
Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Remedy



CorpusCav
Female
Libra
55
50
Thief
Draw Out
Parry
Equip Shield
Move+1

Sleep Sword
Mythril Shield
Holy Miter
Judo Outfit
Jade Armlet

Steal Armor, Steal Shield, Steal Accessory, Steal Status
Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa

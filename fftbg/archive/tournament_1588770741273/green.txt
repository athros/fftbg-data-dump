Player: !Green
Team: Green Team
Palettes: Green/White



DavenIII
Male
Scorpio
59
74
Archer
Black Magic
Arrow Guard
Doublehand
Jump+2

Hunting Bow

Golden Hairpin
Wizard Outfit
Bracer

Charge+1, Charge+4, Charge+5
Bolt 2, Ice, Empower



LDSkinny
Female
Sagittarius
55
59
Oracle
Summon Magic
Parry
Martial Arts
Move+3

Battle Folio

Black Hood
Secret Clothes
N-Kai Armlet

Blind, Spell Absorb, Life Drain, Pray Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Moogle, Carbunkle, Odin, Salamander, Silf, Fairy, Lich



HaplessOne
Male
Capricorn
65
51
Knight
Draw Out
Faith Up
Magic Attack UP
Move-MP Up

Defender
Mythril Shield
Platinum Helmet
Leather Armor
Angel Ring

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword, Night Sword
Koutetsu, Bizen Boat, Murasame, Muramasa



Arch8000
Male
Aries
41
79
Time Mage
Charge
Brave Up
Beastmaster
Jump+2

Gold Staff

Twist Headband
Wizard Outfit
Genji Gauntlet

Haste, Haste 2, Slow, Slow 2, Stop, Reflect, Demi, Stabilize Time, Meteor
Charge+2, Charge+3, Charge+10

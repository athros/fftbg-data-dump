Final Bets: white - 9 bets for 3,257G (59.5%, x0.68); black - 6 bets for 2,218G (40.5%, x1.47)

white bets:
Seaweed_B: 1,000G (30.7%, 103,119G)
Willjin: 600G (18.4%, 16,666G)
Aestheta: 470G (14.4%, 470G)
WhiteTigress: 348G (10.7%, 348G)
Blaze0772: 338G (10.4%, 338G)
datadrivenbot: 200G (6.1%, 64,712G)
gorgewall: 101G (3.1%, 7,406G)
AllInBot: 100G (3.1%, 100G)
brenogarwin: 100G (3.1%, 1,139G)

black bets:
BirbBrainsBot: 1,000G (45.1%, 142,617G)
LAGBOT30000: 500G (22.5%, 11,061G)
getthemoneyz: 318G (14.3%, 1,636,129G)
Omally_: 200G (9.0%, 356G)
MinBetBot: 100G (4.5%, 8,962G)
ko2q: 100G (4.5%, 4,494G)

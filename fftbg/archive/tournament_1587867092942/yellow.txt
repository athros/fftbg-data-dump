Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



LivingHitokiri
Male
Libra
46
74
Geomancer
Summon Magic
Counter Flood
Equip Polearm
Move+1

Javelin
Gold Shield
Headgear
Wizard Robe
Battle Boots

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Ramuh, Titan, Leviathan, Salamander, Lich



PrysmGames
Male
Pisces
41
79
Geomancer
Charge
Arrow Guard
Concentrate
Jump+2

Giant Axe
Gold Shield
Twist Headband
Adaman Vest
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Blizzard, Lava Ball
Charge+1, Charge+5



Twelfthrootoftwo
Male
Capricorn
62
43
Squire
Item
Damage Split
Throw Item
Move+1

Hydra Bag
Flame Shield
Black Hood
Power Sleeve
Genji Gauntlet

Heal, Yell, Cheer Up, Fury
Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Holy Water, Phoenix Down



Conome
Male
Leo
81
72
Mediator
Jump
Sunken State
Equip Shield
Retreat

Dagger
Round Shield
Thief Hat
White Robe
Jade Armlet

Persuade, Death Sentence, Negotiate, Mimic Daravon, Refute
Level Jump8, Vertical Jump8

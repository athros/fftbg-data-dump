Player: !White
Team: White Team
Palettes: White/Blue



Maakur
Male
Capricorn
55
67
Knight
Sing
Faith Up
Attack UP
Waterwalking

Iron Sword
Diamond Shield
Gold Helmet
Maximillian
Red Shoes

Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword
Battle Song, Magic Song, Nameless Song, Space Storage, Sky Demon



0v3rr8d
Male
Pisces
63
76
Mime

Distribute
Monster Talk
Ignore Height



Flash Hat
Mystic Vest
Germinas Boots

Mimic




Skuldar
Female
Aries
66
65
Summoner
White Magic
Meatbone Slash
Long Status
Jump+3

Dragon Rod

Holy Miter
Black Robe
Diamond Armlet

Shiva, Golem, Salamander, Silf, Fairy, Lich
Cure 3, Cure 4, Raise, Esuna, Holy



Shakarak
Male
Scorpio
47
68
Ninja
Item
Hamedo
Beastmaster
Ignore Terrain

Morning Star
Zorlin Shape
Black Hood
Secret Clothes
Spike Shoes

Shuriken, Bomb, Knife, Staff
Potion, Ether, Elixir, Antidote, Remedy, Phoenix Down

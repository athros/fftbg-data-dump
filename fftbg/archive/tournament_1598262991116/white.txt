Player: !White
Team: White Team
Palettes: White/Blue



Sairentozon7
Female
Aries
78
55
Samurai
Yin Yang Magic
Damage Split
Equip Armor
Retreat

Murasame

Feather Hat
Brigandine
Reflect Ring

Asura, Koutetsu, Bizen Boat, Heaven's Cloud
Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze, Dark Holy



Soren Of Tyto
Female
Aquarius
55
39
Squire
White Magic
Mana Shield
Doublehand
Move-MP Up

Diamond Sword

Genji Helmet
Maximillian
Angel Ring

Heal, Tickle
Cure, Raise, Shell, Esuna



Chihuahua Charity
Female
Cancer
68
64
Summoner
Time Magic
Parry
Magic Attack UP
Jump+3

Dragon Rod

Red Hood
White Robe
Cursed Ring

Moogle, Shiva, Ramuh, Bahamut, Salamander
Haste, Immobilize, Stabilize Time



Vultuous
Female
Virgo
45
77
Chemist
Steal
Counter Tackle
Equip Sword
Lava Walking

Ragnarok

Leather Hat
Mythril Vest
Wizard Mantle

Potion, Hi-Potion, Antidote, Phoenix Down
Gil Taking, Steal Shield, Leg Aim

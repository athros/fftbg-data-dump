Player: !Red
Team: Red Team
Palettes: Red/Brown



TheKillerNacho
Monster
Scorpio
76
78
Goblin










VolgraTheMoose
Female
Gemini
68
49
Geomancer
Draw Out
MA Save
Concentrate
Waterwalking

Battle Axe
Buckler
Twist Headband
Black Costume
Small Mantle

Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Bizen Boat, Kiyomori, Muramasa



TeaTime29
Monster
Sagittarius
41
41
Bull Demon










Willjin
Male
Cancer
56
61
Knight
Basic Skill
Auto Potion
Maintenance
Levitate

Hydra Bag
Escutcheon
Platinum Helmet
Gold Armor
Elf Mantle

Shield Break, Magic Break, Power Break, Stasis Sword, Justice Sword
Dash, Heal, Yell

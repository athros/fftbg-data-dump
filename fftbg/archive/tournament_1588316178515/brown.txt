Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rislyeu
Male
Sagittarius
47
68
Lancer
Throw
Parry
Doublehand
Move+1

Battle Bamboo

Diamond Helmet
Carabini Mail
Elf Mantle

Level Jump4, Vertical Jump2
Shuriken, Knife, Axe



Waterwatereverywhere
Male
Leo
71
74
Squire
Yin Yang Magic
Speed Save
Dual Wield
Swim

Flame Whip
Flame Whip
Holy Miter
Wizard Outfit
Germinas Boots

Accumulate, Throw Stone, Heal, Tickle, Cheer Up, Scream
Blind, Poison, Pray Faith, Zombie, Dispel Magic



Lijarkh
Male
Aries
57
44
Monk
Summon Magic
Hamedo
Dual Wield
Lava Walking



Ribbon
Mystic Vest
Germinas Boots

Pummel, Purification, Chakra, Revive
Moogle, Ramuh, Odin, Lich



CorpusCav
Female
Aquarius
74
71
Ninja
White Magic
Counter
Magic Attack UP
Move-MP Up

Flail
Kunai
Headgear
Adaman Vest
Defense Ring

Shuriken, Staff, Axe
Cure, Reraise, Regen, Shell 2, Wall, Holy

Player: !Green
Team: Green Team
Palettes: Green/White



HaychDub
Female
Cancer
68
59
Wizard
Draw Out
Damage Split
Attack UP
Move+3

Thunder Rod

Black Hood
Robe of Lords
Power Wrist

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Ice 2, Death
Asura, Bizen Boat, Heaven's Cloud



ACSpree
Male
Capricorn
51
53
Time Mage
Basic Skill
Sunken State
Maintenance
Move-HP Up

Madlemgen

Holy Miter
Light Robe
N-Kai Armlet

Haste 2, Slow 2, Immobilize, Float, Demi, Demi 2
Dash, Tickle, Yell, Fury, Wish



Electric Glass
Male
Capricorn
50
53
Ninja
Item
Parry
Equip Gun
Move+3

Fairy Harp
Ramia Harp
Thief Hat
Chain Vest
Magic Gauntlet

Shuriken, Ninja Sword
Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Sairentozon7
Female
Libra
39
49
Wizard
Summon Magic
Sunken State
Magic Defense UP
Jump+3

Assassin Dagger

Feather Hat
Brigandine
Wizard Mantle

Fire, Fire 2, Fire 4, Ice, Ice 2, Flare
Ifrit, Carbunkle, Salamander, Lich, Cyclops

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Vergible
Monster
Cancer
57
64
Black Goblin










Omegasuspekt
Male
Leo
65
76
Ninja
Charge
Damage Split
Equip Armor
Move-MP Up

Morning Star
Flame Whip
Platinum Helmet
Plate Mail
Angel Ring

Shuriken
Charge+1, Charge+2, Charge+4, Charge+5



Mellichor
Male
Scorpio
59
60
Ninja
Steal
Parry
Beastmaster
Fly

Orichalcum
Ninja Edge
Thief Hat
Mythril Vest
Battle Boots

Bomb, Sword, Dictionary
Steal Heart, Steal Shield, Steal Status



StryderSeven
Female
Aquarius
52
79
Priest
Math Skill
MA Save
Long Status
Jump+2

Mace of Zeus

Headgear
Chameleon Robe
Angel Ring

Cure, Raise, Reraise, Regen, Shell, Shell 2, Esuna, Magic Barrier
Height, Prime Number, 5, 4, 3

Final Bets: purple - 10 bets for 10,077G (61.2%, x0.63); brown - 9 bets for 6,388G (38.8%, x1.58)

purple bets:
DavenIII: 2,000G (19.8%, 19,864G)
FriendSquirrel: 1,833G (18.2%, 1,833G)
dogsandcatsand: 1,817G (18.0%, 7,269G)
TheChainNerd: 1,244G (12.3%, 1,244G)
serperemagus: 663G (6.6%, 663G)
killth3kid: 660G (6.5%, 6,607G)
Cryptopsy70: 544G (5.4%, 20,920G)
prince_rogers_nelson_: 540G (5.4%, 540G)
joewcarson: 500G (5.0%, 36,539G)
neerrm: 276G (2.7%, 276G)

brown bets:
KasugaiRoastedPeas: 3,000G (47.0%, 8,801G)
Rislyeu: 1,000G (15.7%, 59,773G)
Akrae: 906G (14.2%, 906G)
DuraiPapers: 608G (9.5%, 608G)
BirbBrainsBot: 524G (8.2%, 53,590G)
AllInBot: 100G (1.6%, 100G)
CosmicTactician: 100G (1.6%, 7,783G)
Lordminsc: 100G (1.6%, 4,396G)
IphoneDarkness: 50G (0.8%, 1,011G)

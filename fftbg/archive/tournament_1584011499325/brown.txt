Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NoxeGS
Female
Capricorn
57
35
Knight
White Magic
Auto Potion
Defense UP
Move+3

Nagrarock
Gold Shield
Bronze Helmet
Gold Armor
Spike Shoes

Armor Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Night Sword, Explosion Sword
Cure 2, Cure 3, Raise, Shell, Shell 2, Esuna



HysteriaDays
Male
Libra
49
73
Ninja
Draw Out
Regenerator
Equip Gun
Levitate

Fairy Harp
Papyrus Codex
Leather Hat
Leather Outfit
Feather Boots

Shuriken, Staff, Wand
Koutetsu



Aneyus
Female
Libra
61
56
Monk
White Magic
Earplug
Doublehand
Waterwalking



Holy Miter
Brigandine
Dracula Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Cure 3, Cure 4, Shell 2, Esuna, Holy



Alc Trinity
Female
Sagittarius
50
53
Archer
Basic Skill
Meatbone Slash
Concentrate
Move+1

Blaze Gun
Gold Shield
Gold Helmet
Mystic Vest
Spike Shoes

Charge+3, Charge+4, Charge+5, Charge+10
Accumulate, Throw Stone, Heal, Yell, Scream

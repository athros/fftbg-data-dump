Player: !Brown
Team: Brown Team
Palettes: Brown/Green



LDSkinny
Female
Taurus
41
79
Oracle
White Magic
Regenerator
Equip Sword
Move+2

Muramasa

Black Hood
Leather Outfit
Jade Armlet

Poison, Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Petrify
Cure, Cure 2, Cure 3, Cure 4, Raise, Regen, Shell 2, Wall, Esuna



Gorgewall
Female
Gemini
77
72
Lancer
Item
Parry
Maintenance
Move+1

Musk Rod
Diamond Shield
Crystal Helmet
Chameleon Robe
Small Mantle

Level Jump8, Vertical Jump3
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down



Lyner87
Female
Pisces
75
55
Wizard
White Magic
Meatbone Slash
Magic Attack UP
Levitate

Flame Rod

Headgear
Black Robe
Battle Boots

Fire 3, Fire 4, Bolt, Bolt 3, Ice, Ice 2, Frog, Death
Cure 4, Regen, Esuna



Scurg
Male
Gemini
43
60
Wizard
Steal
Damage Split
Equip Knife
Move-MP Up

Dragon Rod

Holy Miter
Black Robe
Power Wrist

Fire 2, Bolt, Bolt 2, Ice, Ice 2, Ice 3
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Arm Aim

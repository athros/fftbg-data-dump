Player: !Red
Team: Red Team
Palettes: Red/Brown



Nok
Male
Scorpio
51
49
Mime

Caution
Martial Arts
Move+3



Green Beret
Carabini Mail
Elf Mantle

Mimic




SQUiDSQUARKLIN
Male
Capricorn
67
77
Ninja
Sing
PA Save
Equip Sword
Jump+2

Defender
Mythril Sword
Barette
Mystic Vest
N-Kai Armlet

Shuriken
Angel Song, Cheer Song, Space Storage



Pplvee1
Male
Leo
55
61
Archer
Steal
Counter Magic
Doublehand
Ignore Height

Glacier Gun

Flash Hat
Mystic Vest
Diamond Armlet

Charge+1, Charge+5, Charge+7
Gil Taking, Steal Status



Brokenknight201
Male
Scorpio
63
44
Bard
Time Magic
Counter Flood
Doublehand
Swim

Bloody Strings

Red Hood
Chain Mail
Bracer

Magic Song
Slow 2, Stabilize Time

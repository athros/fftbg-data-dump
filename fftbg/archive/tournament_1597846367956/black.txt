Player: !Black
Team: Black Team
Palettes: Black/Red



Docrad
Monster
Cancer
74
79
Holy Dragon










Gongonono
Male
Capricorn
48
44
Thief
Basic Skill
Counter
Equip Bow
Ignore Terrain

Ultimus Bow

Cachusha
Mystic Vest
Battle Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Leg Aim
Cheer Up, Fury, Wish



Sparker9
Male
Leo
43
58
Thief
Battle Skill
Abandon
Equip Armor
Levitate

Sasuke Knife

Golden Hairpin
Crystal Mail
N-Kai Armlet

Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Weapon Break, Magic Break, Mind Break, Stasis Sword



Neerrm
Female
Sagittarius
77
60
Ninja
White Magic
Mana Shield
Magic Defense UP
Jump+3

Flail
Ninja Edge
Golden Hairpin
Brigandine
108 Gems

Bomb, Staff
Cure 3, Cure 4, Raise 2, Reraise, Shell, Esuna

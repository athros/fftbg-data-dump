Player: !Red
Team: Red Team
Palettes: Red/Brown



Sinnyil2
Male
Taurus
68
58
Squire
Item
Counter Magic
Throw Item
Move+2

Broad Sword
Mythril Shield
Crystal Helmet
Mythril Vest
Diamond Armlet

Accumulate, Throw Stone, Heal, Cheer Up, Wish
X-Potion, Ether, Elixir, Maiden's Kiss, Remedy, Phoenix Down



Oreo Pizza
Female
Aries
79
63
Samurai
Jump
Dragon Spirit
Equip Axe
Retreat

Giant Axe

Diamond Helmet
Mythril Armor
N-Kai Armlet

Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji
Level Jump3, Vertical Jump6



Oldmanbody
Male
Aquarius
68
61
Bard
Talk Skill
Counter
Beastmaster
Swim

Bloody Strings

Golden Hairpin
Leather Armor
Leather Mantle

Life Song, Magic Song, Sky Demon, Hydra Pit
Praise, Solution, Refute, Rehabilitate



Coralreeferz
Male
Virgo
46
47
Archer
Elemental
MP Restore
Doublehand
Jump+1

Poison Bow

Green Beret
Mystic Vest
108 Gems

Charge+2, Charge+3, Charge+7
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm

Player: !White
Team: White Team
Palettes: White/Blue



Gikokiko
Female
Scorpio
67
73
Ninja
Charge
Caution
Magic Attack UP
Ignore Terrain

Short Edge
Ninja Edge
Holy Miter
Rubber Costume
Magic Gauntlet

Shuriken, Bomb, Knife
Charge+1



CorpusCav
Female
Capricorn
73
48
Samurai
Elemental
Meatbone Slash
Equip Gun
Jump+1

Bestiary

Gold Helmet
Carabini Mail
Dracula Mantle

Koutetsu, Murasame, Muramasa
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Lava Ball



EnemyController
Male
Libra
56
65
Lancer
Time Magic
Counter Tackle
Halve MP
Waterbreathing

Holy Lance
Bronze Shield
Gold Helmet
Platinum Armor
Diamond Armlet

Level Jump5, Vertical Jump6
Haste, Haste 2, Slow 2, Immobilize, Quick, Demi, Galaxy Stop



DrAntiSocial
Female
Taurus
44
56
Oracle
White Magic
MA Save
Defense UP
Fly

Ivory Rod

Green Beret
Chameleon Robe
Elf Mantle

Pray Faith, Doubt Faith, Dispel Magic, Sleep
Raise, Raise 2, Shell, Shell 2, Esuna

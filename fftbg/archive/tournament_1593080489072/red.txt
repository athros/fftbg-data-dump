Player: !Red
Team: Red Team
Palettes: Red/Brown



CassiePhoenix
Female
Cancer
52
45
Monk
Yin Yang Magic
Counter
Long Status
Levitate



Headgear
Rubber Costume
Dracula Mantle

Pummel, Wave Fist, Secret Fist, Purification, Revive
Life Drain, Confusion Song, Sleep



Zeando
Male
Aquarius
40
66
Summoner
Item
Dragon Spirit
Magic Attack UP
Ignore Terrain

Ice Rod

Triangle Hat
Light Robe
Wizard Mantle

Moogle, Ramuh, Golem, Carbunkle, Bahamut, Silf, Fairy, Cyclops
Ether, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



TeaTime29
Male
Leo
46
58
Thief
Elemental
Distribute
Equip Bow
Ignore Terrain

Silver Bow

Red Hood
Mythril Vest
Magic Gauntlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Tyron
Monster
Gemini
66
50
Black Goblin








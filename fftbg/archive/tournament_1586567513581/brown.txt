Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Upvla
Monster
Gemini
44
70
Tiamat










WireLord
Male
Virgo
51
43
Bard
Punch Art
Auto Potion
Dual Wield
Move-HP Up

Bloody Strings
Fairy Harp
Headgear
Power Sleeve
Cursed Ring

Angel Song, Cheer Song, Magic Song, Sky Demon
Earth Slash, Revive



ALY327
Female
Virgo
77
65
Priest
Charge
Hamedo
Equip Gun
Lava Walking

Battle Folio

Twist Headband
Black Robe
Diamond Armlet

Cure, Cure 2, Cure 3, Raise, Raise 2, Protect, Protect 2, Wall, Esuna, Holy
Charge+3, Charge+4, Charge+10



Lodrak
Female
Sagittarius
52
74
Thief
Draw Out
Brave Up
Magic Defense UP
Waterbreathing

Orichalcum

Black Hood
Brigandine
Defense Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Arm Aim
Murasame, Kikuichimoji

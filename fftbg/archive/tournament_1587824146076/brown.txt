Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MirtaiAtana
Male
Libra
77
62
Summoner
Yin Yang Magic
Earplug
Defense UP
Swim

Oak Staff

Triangle Hat
Light Robe
Cursed Ring

Moogle, Shiva, Ramuh, Titan, Golem, Leviathan, Salamander, Silf, Lich, Cyclops
Poison, Silence Song, Foxbird, Dispel Magic, Sleep



ZephyrTempest
Female
Gemini
79
46
Mime

Dragon Spirit
Martial Arts
Move+2



Cachusha
Clothes
Rubber Shoes

Mimic




J2DaBibbles
Male
Aquarius
57
68
Archer
Punch Art
Counter Tackle
Doublehand
Fly

Windslash Bow

Red Hood
Earth Clothes
Leather Mantle

Charge+1, Charge+7
Wave Fist, Earth Slash, Purification, Revive



Breakdown777
Male
Virgo
52
63
Summoner
Draw Out
Dragon Spirit
Equip Knife
Waterwalking

Main Gauche

Black Hood
Brigandine
Battle Boots

Shiva, Ramuh, Leviathan, Silf, Fairy, Lich
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa

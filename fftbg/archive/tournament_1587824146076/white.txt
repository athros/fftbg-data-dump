Player: !White
Team: White Team
Palettes: White/Blue



Estan AD
Female
Taurus
69
56
Summoner
Draw Out
Parry
Short Status
Teleport

Flame Rod

Golden Hairpin
Light Robe
Small Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Silf, Cyclops
Koutetsu, Masamune



Hamborn
Male
Cancer
67
69
Monk
Basic Skill
Counter Tackle
Equip Axe
Move+1

Healing Staff

Green Beret
Judo Outfit
Red Shoes

Spin Fist, Wave Fist, Purification, Revive
Accumulate, Throw Stone, Heal, Wish



CosmicTactician
Male
Aries
47
76
Ninja
Charge
Counter
Concentrate
Jump+1

Zorlin Shape
Spell Edge
Black Hood
Leather Outfit
Defense Ring

Shuriken, Knife, Stick, Dictionary
Charge+1, Charge+10



Upvla
Female
Libra
50
66
Mediator
Dance
Absorb Used MP
Dual Wield
Waterbreathing

Stone Gun
Romanda Gun
Twist Headband
Earth Clothes
Defense Armlet

Persuade, Threaten, Death Sentence, Refute, Rehabilitate
Witch Hunt, Wiznaibus, Dragon Pit

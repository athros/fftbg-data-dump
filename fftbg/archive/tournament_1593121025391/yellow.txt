Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Digitalsocrates
Male
Gemini
73
55
Ninja
Battle Skill
Counter
Short Status
Waterwalking

Kunai
Morning Star
Twist Headband
Mythril Vest
Power Wrist

Shuriken, Bomb, Staff, Axe, Wand
Magic Break, Power Break, Night Sword



Killth3kid
Male
Pisces
60
60
Wizard
Draw Out
Arrow Guard
Equip Shield
Jump+3

Thunder Rod
Bronze Shield
Leather Hat
Silk Robe
Magic Ring

Fire, Fire 2, Fire 3, Fire 4, Bolt 2, Ice, Ice 3
Asura, Bizen Boat, Murasame, Kiyomori, Muramasa



ArchKnightX
Female
Gemini
65
53
Geomancer
Draw Out
Abandon
Secret Hunt
Move+1

Heaven's Cloud
Genji Shield
Golden Hairpin
Black Robe
Germinas Boots

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Asura, Koutetsu



TheChainNerd
Female
Scorpio
72
62
Wizard
Math Skill
Sunken State
Beastmaster
Jump+1

Wizard Rod

Green Beret
Mythril Vest
Jade Armlet

Fire, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Flare
CT, Prime Number

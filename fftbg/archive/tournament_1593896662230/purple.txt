Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Error72
Female
Taurus
44
73
Oracle
Draw Out
Dragon Spirit
Dual Wield
Move+3

Battle Bamboo
Ivory Rod
Triangle Hat
Black Robe
Chantage

Blind, Poison, Spell Absorb, Life Drain, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Bizen Boat, Muramasa



Alterworlds
Male
Libra
63
68
Knight
Basic Skill
Faith Save
Magic Attack UP
Levitate

Blood Sword
Escutcheon
Mythril Helmet
Chameleon Robe
Red Shoes

Head Break, Armor Break, Shield Break, Weapon Break, Stasis Sword, Night Sword
Throw Stone, Tickle, Cheer Up, Fury, Wish



Ciprofloxacin
Male
Leo
61
65
Geomancer
Yin Yang Magic
Dragon Spirit
Secret Hunt
Retreat

Platinum Sword
Escutcheon
Leather Hat
Adaman Vest
Feather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Confusion Song



AO110893
Male
Taurus
40
54
Time Mage
Basic Skill
Damage Split
Halve MP
Swim

Wizard Staff

Feather Hat
Black Costume
Magic Ring

Haste, Slow, Slow 2, Quick, Demi, Stabilize Time
Throw Stone, Yell

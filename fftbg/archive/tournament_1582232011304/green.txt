Player: !Green
Team: Green Team
Palettes: Green/White



Morgan Webbs Thong
Male
Aries
48
47
Monk
Charge
HP Restore
Equip Gun
Ignore Terrain

Glacier Gun

Headgear
Secret Clothes
N-Kai Armlet

Spin Fist, Earth Slash, Purification
Charge+1, Charge+2, Charge+3, Charge+5



Ivalice Tavernmaster
Male
Virgo
69
40
Chemist
White Magic
Dragon Spirit
Dual Wield
Fly

Blind Knife
Star Bag
Feather Hat
Earth Clothes
Small Mantle

Potion, Hi-Potion, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Cure, Cure 4, Raise, Reraise, Regen, Esuna



Ominnous
Male
Capricorn
72
75
Lancer
Battle Skill
Caution
Attack UP
Jump+2

Battle Bamboo
Genji Shield
Iron Helmet
White Robe
Sprint Shoes

Level Jump4, Vertical Jump7
Head Break, Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword, Dark Sword



KevvTwo
Male
Capricorn
50
68
Archer
Throw
Arrow Guard
Dual Wield
Fly

Ice Bow

Triangle Hat
Wizard Outfit
Feather Mantle

Charge+3, Charge+5
Stick

Player: !Black
Team: Black Team
Palettes: Black/Red



TrueRhyme
Male
Aries
73
60
Monk
White Magic
Catch
Equip Axe
Move+1

Oak Staff

Golden Hairpin
Brigandine
Power Wrist

Wave Fist, Purification, Chakra, Revive
Cure, Cure 2, Protect, Wall, Holy



Old Overholt
Female
Scorpio
74
75
Lancer
Item
Damage Split
Beastmaster
Levitate

Spear
Kaiser Plate
Genji Helmet
Mythril Armor
Battle Boots

Level Jump4, Vertical Jump4
Potion, Hi-Potion, Eye Drop, Maiden's Kiss, Soft, Holy Water



Tetsuwan Melty
Female
Pisces
75
72
Squire
Charge
Auto Potion
Short Charge
Move+2

Giant Axe
Gold Shield
Green Beret
Black Costume
Feather Boots

Accumulate, Heal, Cheer Up
Charge+20



StealthModeLocke
Male
Leo
55
61
Ninja
White Magic
PA Save
Beastmaster
Teleport

Hidden Knife
Kunai
Red Hood
Brigandine
Rubber Shoes

Shuriken
Cure, Raise, Protect, Protect 2, Shell 2, Esuna

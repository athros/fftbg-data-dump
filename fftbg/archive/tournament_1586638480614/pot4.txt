Final Bets: purple - 17 bets for 15,601G (70.7%, x0.41); brown - 16 bets for 6,473G (29.3%, x2.41)

purple bets:
MoonSlayerRS: 5,512G (35.3%, 5,512G)
Dothan89: 3,000G (19.2%, 26,986G)
RubenFlonne: 1,147G (7.4%, 1,147G)
BlinkFS: 1,000G (6.4%, 14,682G)
Break3r: 965G (6.2%, 965G)
datadrivenbot: 878G (5.6%, 22,048G)
Lanshaft: 600G (3.8%, 2,119G)
luminarii: 466G (3.0%, 13,438G)
jhaller2: 400G (2.6%, 1,346G)
DejaPoo21: 372G (2.4%, 372G)
ArdentDrops: 300G (1.9%, 997G)
V3rdeni: 300G (1.9%, 4,224G)
AllInBot: 232G (1.5%, 232G)
SkepticalMinotaur: 200G (1.3%, 673G)
edgehead62888: 128G (0.8%, 128G)
maakur_: 100G (0.6%, 2,173G)
RughSontos: 1G (0.0%, 1,873G)

brown bets:
ungabunga_bot: 1,000G (15.4%, 114,121G)
BirbBrainsBot: 1,000G (15.4%, 78,710G)
DeathTaxesAndAnime: 927G (14.3%, 927G)
Conome: 566G (8.7%, 566G)
mirapoix: 560G (8.7%, 560G)
getthemoneyz: 548G (8.5%, 480,314G)
b0shii: 321G (5.0%, 1,359G)
ZephyrTempest: 311G (4.8%, 52,900G)
DLJuggernaut: 300G (4.6%, 8,230G)
SWATb1gdog: 240G (3.7%, 240G)
LordPaimon: 200G (3.1%, 2,870G)
ApplesauceBoss: 100G (1.5%, 4,299G)
ko2q: 100G (1.5%, 4,090G)
uncharreted: 100G (1.5%, 1,759G)
hutna: 100G (1.5%, 3,322G)
DrAntiSocial: 100G (1.5%, 2,035G)

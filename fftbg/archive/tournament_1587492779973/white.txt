Player: !White
Team: White Team
Palettes: White/Blue



Nifboy
Monster
Aquarius
55
81
Black Chocobo










J2DaBibbles
Male
Leo
53
60
Knight
Throw
Counter
Doublehand
Ignore Height

Defender

Crystal Helmet
Black Robe
Sprint Shoes

Head Break, Shield Break, Weapon Break, Magic Break, Speed Break
Knife, Sword



Gelwain
Male
Cancer
53
49
Samurai
Basic Skill
Caution
Equip Shield
Swim

Gungnir
Escutcheon
Genji Helmet
Crystal Mail
Elf Mantle

Bizen Boat, Kiyomori
Accumulate, Throw Stone, Heal, Wish



Mtueni
Female
Scorpio
69
45
Squire
Talk Skill
PA Save
Equip Polearm
Teleport

Holy Lance
Flame Shield
Gold Helmet
Linen Cuirass
Dracula Mantle

Cheer Up, Fury
Solution, Refute, Rehabilitate

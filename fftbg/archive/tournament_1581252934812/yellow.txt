Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mechjeasus
Monster
Aries
53
72
Ochu










SweetHat
Male
Scorpio
70
50
Bard
White Magic
Faith Up
Short Status
Waterbreathing

Bloody Strings

Feather Hat
Secret Clothes
108 Gems

Battle Song, Magic Song
Raise, Raise 2, Protect, Wall, Esuna



Snkey
Female
Capricorn
80
73
Wizard
Basic Skill
Damage Split
Magic Defense UP
Move-MP Up

Wizard Rod

Feather Hat
Robe of Lords
Jade Armlet

Fire, Fire 2, Fire 3, Fire 4, Ice 2
Accumulate, Dash, Heal, Tickle, Yell, Cheer Up, Wish



HappyOK
Female
Aries
75
74
Monk
Yin Yang Magic
Earplug
Long Status
Jump+2



Feather Hat
Leather Vest
Dracula Mantle

Wave Fist, Purification, Chakra, Revive
Pray Faith, Blind Rage, Confusion Song, Paralyze, Sleep, Petrify

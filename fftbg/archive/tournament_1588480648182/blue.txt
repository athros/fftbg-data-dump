Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ZephyrTempest
Male
Libra
54
74
Priest
Draw Out
Distribute
Equip Shield
Jump+1

White Staff
Escutcheon
Golden Hairpin
Black Robe
Dracula Mantle

Cure 3, Raise, Shell 2, Wall, Esuna
Koutetsu, Bizen Boat, Kiyomori, Muramasa



RyuTsuno
Male
Sagittarius
70
67
Wizard
Draw Out
Meatbone Slash
Concentrate
Waterbreathing

Flame Rod

Triangle Hat
Robe of Lords
Wizard Mantle

Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Death
Bizen Boat



Squatting Bear
Male
Sagittarius
55
42
Priest
Charge
Earplug
Martial Arts
Ignore Terrain



Leather Hat
Silk Robe
Diamond Armlet

Cure, Cure 4, Raise, Raise 2, Shell, Shell 2, Wall, Esuna, Magic Barrier
Charge+3, Charge+7, Charge+10



Cougboi
Male
Leo
48
67
Wizard
Elemental
Brave Up
Equip Knife
Move+1

Ninja Edge

Feather Hat
Mythril Vest
Magic Ring

Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Frog
Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball

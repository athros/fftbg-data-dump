Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BlackFireUK
Female
Capricorn
53
46
Chemist
Dance
Brave Up
Equip Gun
Swim

Stone Gun

Thief Hat
Clothes
Rubber Shoes

Potion, Hi-Ether, Maiden's Kiss, Phoenix Down
Wiznaibus, Disillusion, Nameless Dance, Last Dance



Rolodex
Female
Serpentarius
69
48
Priest
Jump
Brave Up
Attack UP
Swim

White Staff

Feather Hat
White Robe
Reflect Ring

Cure 3, Cure 4, Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna
Level Jump2, Vertical Jump6



WalkerNash
Male
Capricorn
67
61
Squire
Draw Out
Counter Tackle
Equip Axe
Jump+1

Flail

Triangle Hat
Adaman Vest
Genji Gauntlet

Throw Stone, Tickle, Cheer Up, Wish
Murasame, Heaven's Cloud



Glitchminded
Male
Leo
80
48
Archer
Punch Art
Distribute
Concentrate
Move-MP Up

Windslash Bow

Headgear
Leather Outfit
Bracer

Charge+1, Charge+2, Charge+4, Charge+5
Purification, Revive

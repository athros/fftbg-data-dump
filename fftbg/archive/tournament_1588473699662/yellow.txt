Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zollie
Male
Leo
76
43
Chemist
Talk Skill
Dragon Spirit
Equip Bow
Move-HP Up

Ultimus Bow

Triangle Hat
Clothes
Sprint Shoes

Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Invitation, Negotiate, Refute



Damita
Monster
Libra
70
67
Squidraken










Shirl
Female
Leo
46
78
Calculator
Imp Skill
Caution
Martial Arts
Swim


Crystal Shield
Circlet
Chameleon Robe
Spike Shoes

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam



Fredrick
Male
Virgo
59
80
Bard
Basic Skill
Auto Potion
Magic Attack UP
Move+2

Fairy Harp

Black Hood
Brigandine
Angel Ring

Life Song, Battle Song, Last Song, Space Storage
Heal, Tickle, Yell, Cheer Up, Wish

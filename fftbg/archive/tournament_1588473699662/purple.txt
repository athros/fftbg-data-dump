Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Indira
Monster
Libra
51
56
Byblos










Baderon
Male
Cancer
64
67
Bard
Charge
Meatbone Slash
Equip Shield
Jump+1

Ramia Harp
Flame Shield
Leather Hat
Black Costume
Wizard Mantle

Nameless Song, Diamond Blade
Charge+3, Charge+5, Charge+10



Wilber
Male
Aquarius
77
47
Summoner
Steal
Critical Quick
Concentrate
Jump+1

Ice Rod

Red Hood
Earth Clothes
Red Shoes

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Salamander, Zodiac
Gil Taking, Steal Shield



Michel
Male
Capricorn
53
46
Chemist
Basic Skill
Arrow Guard
Defense UP
Move+1

Glacier Gun

Black Hood
Adaman Vest
Red Shoes

Potion, Hi-Ether, Echo Grass
Throw Stone, Heal

Final Bets: black - 19 bets for 6,573G (58.0%, x0.72); brown - 8 bets for 4,763G (42.0%, x1.38)

black bets:
HorusTaurus: 1,000G (15.2%, 4,167G)
Lionhermit: 1,000G (15.2%, 36,845G)
SarrgeQc: 501G (7.6%, 790G)
gggamezzz: 500G (7.6%, 11,351G)
TheMurkGnome: 477G (7.3%, 3,893G)
ZephyrTempest: 456G (6.9%, 3,651G)
itsdigitalbro: 313G (4.8%, 313G)
ccordc: 300G (4.6%, 2,496G)
FNCardascia_: 290G (4.4%, 290G)
DHaveWord: 288G (4.4%, 288G)
Bioticism: 240G (3.7%, 240G)
Arlum: 212G (3.2%, 212G)
benticore: 200G (3.0%, 1,323G)
Meta_Five: 200G (3.0%, 6,659G)
getthemoneyz: 164G (2.5%, 606,052G)
Kyune: 120G (1.8%, 120G)
Chuckolator: 112G (1.7%, 29,908G)
datadrivenbot: 100G (1.5%, 12,240G)
Firesheath: 100G (1.5%, 9,382G)

brown bets:
Zeroroute: 2,507G (52.6%, 2,507G)
SquidgyXom: 526G (11.0%, 10,522G)
nifboy: 500G (10.5%, 5,627G)
fenaen: 500G (10.5%, 3,670G)
Tithonus: 308G (6.5%, 308G)
ungabunga_bot: 190G (4.0%, 313,597G)
Dustmasc: 132G (2.8%, 132G)
maakur_: 100G (2.1%, 117,871G)

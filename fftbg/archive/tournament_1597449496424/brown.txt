Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ArchKnightX
Male
Virgo
74
71
Squire
Summon Magic
Damage Split
Equip Knife
Levitate

Orichalcum
Diamond Shield
Golden Hairpin
Plate Mail
Genji Gauntlet

Dash, Throw Stone, Heal, Scream
Moogle, Ramuh, Ifrit, Titan, Golem, Leviathan, Salamander, Silf, Fairy



Ar Tactic
Female
Gemini
79
42
Ninja
Time Magic
Sunken State
Concentrate
Swim

Hidden Knife
Koga Knife
Golden Hairpin
Judo Outfit
Spike Shoes

Shuriken, Bomb
Haste, Immobilize, Stabilize Time, Meteor



BuffaloCrunch
Female
Taurus
45
43
Chemist
Time Magic
Counter Flood
Equip Bow
Fly

Bow Gun

Flash Hat
Leather Outfit
108 Gems

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Phoenix Down
Haste 2, Slow, Float



Dasutin23
Female
Libra
78
63
Priest
Basic Skill
Arrow Guard
Halve MP
Jump+3

Flame Whip

Leather Hat
Black Robe
Angel Ring

Raise, Raise 2, Shell, Shell 2, Wall, Esuna, Magic Barrier
Accumulate, Heal, Tickle, Cheer Up

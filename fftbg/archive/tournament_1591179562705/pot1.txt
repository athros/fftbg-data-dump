Final Bets: red - 10 bets for 2,567G (36.7%, x1.73); blue - 8 bets for 4,433G (63.3%, x0.58)

red bets:
CT_5_Holy: 948G (36.9%, 948G)
Rislyeu: 500G (19.5%, 14,631G)
CorpusCav: 250G (9.7%, 36,296G)
gorgewall: 201G (7.8%, 6,219G)
DAC169: 144G (5.6%, 144G)
blastty: 120G (4.7%, 120G)
DISTROlD: 104G (4.1%, 104G)
ApplesauceBoss: 100G (3.9%, 1,719G)
Ring_Wyrm: 100G (3.9%, 462G)
datadrivenbot: 100G (3.9%, 31,729G)

blue bets:
CassiePhoenix: 1,521G (34.3%, 1,521G)
BirbBrainsBot: 1,000G (22.6%, 180,753G)
UmaiJam: 1,000G (22.6%, 19,921G)
volgrathemoose: 500G (11.3%, 8,033G)
thaetreis: 192G (4.3%, 192G)
Klednar21: 124G (2.8%, 124G)
HASTERIOUS: 56G (1.3%, 1,133G)
getthemoneyz: 40G (0.9%, 798,691G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



L2 Sentinel
Male
Sagittarius
59
68
Lancer
Sing
Critical Quick
Short Status
Jump+2

Holy Lance
Hero Shield
Iron Helmet
Light Robe
Leather Mantle

Level Jump3, Vertical Jump7
Angel Song, Cheer Song, Magic Song, Last Song



ShintaroNayaka
Male
Virgo
56
58
Calculator
Undeath Skill
Counter Flood
Defense UP
Swim

Bestiary

Diamond Helmet
Mythril Vest
Jade Armlet

Blue Magic
Knife Hand, Thunder Soul, Ice Soul, Aqua Soul, Wind Soul, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Mirapoix
Female
Sagittarius
42
45
Geomancer
Black Magic
Mana Shield
Equip Knife
Jump+3

Dragon Rod
Gold Shield
Flash Hat
Mythril Vest
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire, Fire 2, Bolt 2, Ice 3, Death



Rislyeu
Male
Libra
42
48
Mime

HP Restore
Equip Shield
Move+2


Platinum Shield
Black Hood
Judo Outfit
Bracer

Mimic


Final Bets: red - 9 bets for 6,831G (47.0%, x1.13); green - 7 bets for 7,701G (53.0%, x0.89)

red bets:
KasugaiRoastedPeas: 2,416G (35.4%, 2,416G)
SkylerBunny: 2,000G (29.3%, 45,087G)
TheMurkGnome: 1,000G (14.6%, 4,484G)
superdevon1: 464G (6.8%, 11,606G)
Lythe_Caraker: 250G (3.7%, 170,121G)
gorgewall: 201G (2.9%, 11,018G)
AllInBot: 200G (2.9%, 200G)
datadrivenbot: 200G (2.9%, 30,822G)
BaronHaynes: 100G (1.5%, 17,608G)

green bets:
manc0_: 3,824G (49.7%, 3,824G)
bahamutlagooon: 1,233G (16.0%, 6,166G)
BirbBrainsBot: 1,000G (13.0%, 26,662G)
dantayystv: 1,000G (13.0%, 21,914G)
randgridr: 500G (6.5%, 3,494G)
zah69: 108G (1.4%, 108G)
getthemoneyz: 36G (0.5%, 1,303,070G)

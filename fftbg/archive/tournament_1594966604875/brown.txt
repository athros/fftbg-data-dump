Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bahamutlagooon
Male
Taurus
47
50
Wizard
Jump
Regenerator
Equip Knife
Move+2

Thunder Rod

Black Hood
Judo Outfit
Angel Ring

Fire 4, Bolt, Bolt 3, Ice 4
Level Jump2, Vertical Jump7



NicoSavoy
Female
Leo
47
68
Thief
Yin Yang Magic
Counter Flood
Defend
Ignore Terrain

Dagger

Twist Headband
Leather Outfit
Small Mantle

Steal Armor, Steal Status, Leg Aim
Poison, Sleep, Dark Holy



TheMurkGnome
Male
Libra
78
68
Ninja
Yin Yang Magic
Earplug
Equip Gun
Move+2

Battle Folio
Battle Folio
Headgear
Judo Outfit
Rubber Shoes

Bomb, Axe, Spear, Dictionary
Blind, Poison, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep



Twelfthrootoftwo
Male
Virgo
69
58
Ninja
Elemental
Brave Save
Long Status
Fly

Air Knife
Sasuke Knife
Thief Hat
Secret Clothes
Sprint Shoes

Shuriken, Bomb, Knife
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball

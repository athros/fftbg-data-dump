Player: !Black
Team: Black Team
Palettes: Black/Red



Silentperogy
Female
Leo
62
62
Knight
Jump
Parry
Martial Arts
Ignore Height

Mythril Sword
Escutcheon
Leather Helmet
Mythril Armor
Power Wrist

Shield Break, Mind Break
Level Jump3, Vertical Jump7



Harbinger605
Female
Leo
53
55
Ninja
Steal
Meatbone Slash
Martial Arts
Move-MP Up



Triangle Hat
Mythril Vest
Cursed Ring

Shuriken
Steal Accessory, Leg Aim



Omega2040
Female
Libra
71
68
Chemist
Charge
Mana Shield
Equip Gun
Retreat

Mythril Gun

Headgear
Judo Outfit
Magic Gauntlet

Potion, Ether, Eye Drop, Echo Grass, Phoenix Down
Charge+1, Charge+2, Charge+3, Charge+7



NoxeGS
Female
Aries
73
40
Dancer
Talk Skill
Meatbone Slash
Beastmaster
Jump+3

Persia

Feather Hat
Brigandine
Power Wrist

Polka Polka, Last Dance, Obsidian Blade, Nether Demon
Praise, Solution, Mimic Daravon, Refute

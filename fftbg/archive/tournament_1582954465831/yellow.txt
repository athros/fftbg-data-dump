Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Skellibean
Male
Gemini
44
68
Mediator
Summon Magic
Damage Split
Maintenance
Move+1

Bestiary

Twist Headband
Light Robe
Power Wrist

Invitation, Preach, Refute
Shiva, Ifrit, Titan, Golem, Carbunkle, Leviathan



DudeMonkey77
Female
Pisces
44
54
Dancer
White Magic
Brave Up
Magic Attack UP
Ignore Height

Panther Bag

Leather Hat
Secret Clothes
Genji Gauntlet

Polka Polka, Last Dance, Dragon Pit
Raise, Protect, Esuna



Aldrammech
Female
Cancer
55
76
Mime

MA Save
Equip Shield
Lava Walking


Crystal Shield
Feather Hat
Mythril Vest
Spike Shoes

Mimic




MyFakeLife
Male
Aquarius
59
79
Bard
White Magic
Counter Flood
Dual Wield
Waterwalking

Windslash Bow

Holy Miter
Black Costume
Reflect Ring

Life Song, Battle Song, Nameless Song, Diamond Blade, Space Storage, Hydra Pit
Raise, Protect 2, Shell 2, Esuna, Holy

Player: !Red
Team: Red Team
Palettes: Red/Brown



DustBirdEX
Male
Taurus
67
60
Archer
Elemental
Sunken State
Doublehand
Jump+2

Poison Bow

Gold Helmet
Chain Vest
Setiemson

Charge+1, Charge+3, Charge+4, Charge+10
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Digitalsocrates
Female
Pisces
71
63
Summoner
Time Magic
Speed Save
Concentrate
Jump+2

Gold Staff

Holy Miter
Earth Clothes
Defense Armlet

Moogle, Ifrit, Titan, Leviathan, Lich
Slow, Slow 2, Immobilize, Demi, Stabilize Time



Victoriolue
Male
Pisces
67
60
Archer
Throw
Counter Tackle
Martial Arts
Lava Walking

Mythril Gun
Bronze Shield
Red Hood
Leather Outfit
Power Wrist

Charge+1, Charge+5, Charge+7, Charge+10
Bomb, Knife, Stick, Dictionary



Evewho
Female
Virgo
44
69
Knight
Punch Art
Counter Tackle
Halve MP
Lava Walking

Cute Bag
Ice Shield
Leather Helmet
Light Robe
Germinas Boots

Armor Break, Mind Break, Dark Sword
Wave Fist, Earth Slash, Purification, Revive

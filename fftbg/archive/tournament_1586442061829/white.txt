Player: !White
Team: White Team
Palettes: White/Blue



Kai Shee
Monster
Sagittarius
43
81
Coeurl










Mexskacin
Monster
Aries
80
56
Mindflayer










CorpusCav
Female
Leo
43
74
Dancer
Draw Out
Sunken State
Defense UP
Waterwalking

Persia

Flash Hat
Brigandine
Red Shoes

Witch Hunt, Wiznaibus, Polka Polka, Last Dance
Asura, Koutetsu, Muramasa



LDSkinny
Male
Taurus
58
53
Ninja
Summon Magic
HP Restore
Magic Attack UP
Move+1

Flail
Ninja Edge
Cachusha
Rubber Costume
Genji Gauntlet

Shuriken
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Salamander

Player: !Green
Team: Green Team
Palettes: Green/White



SeedSC
Female
Gemini
52
38
Priest
Elemental
Abandon
Beastmaster
Ignore Height

Flail

Triangle Hat
Chameleon Robe
Leather Mantle

Cure 3, Raise, Shell, Shell 2, Wall, Esuna
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Dynasti
Male
Aries
62
73
Ninja
Time Magic
Distribute
Halve MP
Move+2

Flail
Kunai
Flash Hat
Secret Clothes
Rubber Shoes

Shuriken, Knife, Sword
Haste, Haste 2, Slow 2, Stop, Demi, Demi 2, Stabilize Time



Action Otter
Male
Aries
54
51
Oracle
Steal
PA Save
Attack UP
Move-HP Up

Battle Bamboo

Holy Miter
Wizard Robe
Defense Ring

Poison, Life Drain, Silence Song, Blind Rage, Dispel Magic, Sleep, Dark Holy
Gil Taking, Steal Helmet, Steal Accessory, Arm Aim



DeathTaxesAndAnime
Female
Scorpio
66
67
Priest
Punch Art
Parry
Short Charge
Waterbreathing

Gold Staff

Feather Hat
Wizard Robe
Leather Mantle

Cure, Cure 2, Cure 4, Raise, Shell, Esuna
Secret Fist, Revive

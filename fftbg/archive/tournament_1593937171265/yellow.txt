Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Female
Aquarius
60
47
Geomancer
Charge
Parry
Short Status
Teleport

Battle Axe
Mythril Shield
Triangle Hat
Black Robe
Feather Mantle

Pitfall, Hell Ivy, Local Quake, Blizzard, Lava Ball
Charge+2, Charge+20



Ring Wyrm
Monster
Capricorn
54
43
Floating Eye










TeaTime29
Monster
Taurus
62
59
Minotaur










Blastty
Male
Aries
70
52
Time Mage
Elemental
Damage Split
Sicken
Move-HP Up

Whale Whisker

Black Hood
Chameleon Robe
Cursed Ring

Haste 2, Stop, Float, Reflect, Demi
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Green
Team: Green Team
Palettes: Green/White



Zetchryn
Female
Virgo
43
60
Knight
Yin Yang Magic
Parry
Magic Attack UP
Teleport

Long Sword
Aegis Shield
Barbuta
Wizard Robe
Bracer

Head Break, Armor Break, Magic Break, Power Break, Night Sword
Life Drain, Pray Faith, Confusion Song, Dispel Magic, Petrify



YaBoy125
Female
Scorpio
50
50
Samurai
Battle Skill
Counter
Attack UP
Ignore Terrain

Partisan

Barbuta
Linen Cuirass
Germinas Boots

Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa
Head Break, Shield Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Surging Sword



ExecutedGiraffe
Female
Cancer
41
39
Summoner
Elemental
Faith Save
Defense UP
Fly

Madlemgen

Headgear
Silk Robe
Power Wrist

Moogle, Ifrit, Titan, Carbunkle, Odin
Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Kronikle
Male
Gemini
46
75
Monk
Talk Skill
Speed Save
Attack UP
Ignore Terrain



Golden Hairpin
Clothes
Dracula Mantle

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Invitation, Solution, Death Sentence, Insult, Mimic Daravon, Refute

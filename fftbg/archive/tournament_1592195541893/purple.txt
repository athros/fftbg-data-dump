Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



JLinkletter
Female
Cancer
48
63
Dancer
Yin Yang Magic
Speed Save
Attack UP
Waterwalking

Main Gauche

Black Hood
Brigandine
Dracula Mantle

Slow Dance, Obsidian Blade, Nether Demon
Poison, Zombie, Blind Rage, Foxbird, Paralyze



Phrossi V
Female
Virgo
78
75
Squire
Talk Skill
Catch
Defense UP
Move+3

Flail
Crystal Shield
Triangle Hat
Bronze Armor
Cursed Ring

Dash, Throw Stone, Cheer Up
Threaten, Death Sentence, Rehabilitate



Powergems
Male
Aquarius
77
52
Calculator
Robosnake Skill
Distribute
Doublehand
Fly

Battle Folio

Circlet
Leather Armor
N-Kai Armlet

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



SSwing
Male
Libra
68
67
Knight
Summon Magic
HP Restore
Dual Wield
Teleport

Defender
Slasher
Genji Helmet
Plate Mail
Defense Armlet

Head Break, Armor Break, Mind Break, Stasis Sword, Night Sword
Moogle, Ramuh, Titan, Odin, Leviathan, Silf

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Spuzzmocker
Female
Cancer
80
71
Calculator
Farm Skill
Counter Tackle
Magic Defense UP
Ignore Terrain

Papyrus Codex
Flame Shield
Crystal Helmet
Wizard Robe
Salty Rage

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



Firesheath
Male
Taurus
63
74
Summoner
Black Magic
Caution
Defense UP
Move+3

Rainbow Staff

Green Beret
Black Robe
Sprint Shoes

Moogle, Golem, Carbunkle, Salamander, Cyclops, Zodiac
Fire 3, Bolt 3, Empower, Frog



SkylerBunny
Male
Capricorn
60
79
Wizard
White Magic
Damage Split
Magic Attack UP
Ignore Terrain

Assassin Dagger

Flash Hat
Brigandine
Magic Gauntlet

Fire, Fire 2, Bolt 2, Ice 4
Cure 2, Raise, Raise 2, Shell, Shell 2, Holy



Evewho
Female
Capricorn
77
79
Chemist
Jump
HP Restore
Dual Wield
Swim

Romanda Gun
Glacier Gun
Black Hood
Chain Vest
Sprint Shoes

Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down
Level Jump5, Vertical Jump4

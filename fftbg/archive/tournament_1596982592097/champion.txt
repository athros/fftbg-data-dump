Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Fattunaking
Male
Leo
70
79
Monk
Time Magic
Auto Potion
Equip Bow
Retreat

Snipe Bow

Leather Hat
Brigandine
N-Kai Armlet

Spin Fist, Wave Fist, Secret Fist, Purification
Haste, Slow, Immobilize, Float, Reflect, Quick



Skillomono
Male
Leo
39
75
Samurai
Jump
Auto Potion
Equip Gun
Lava Walking

Glacier Gun

Mythril Helmet
Silk Robe
Power Wrist

Asura, Koutetsu, Bizen Boat, Murasame, Muramasa, Kikuichimoji
Level Jump4, Vertical Jump4



LordTomS
Male
Cancer
77
41
Knight
Draw Out
Dragon Spirit
Equip Bow
Fly

Bow Gun
Mythril Shield
Leather Helmet
Gold Armor
Feather Boots

Head Break, Armor Break, Weapon Break, Power Break, Mind Break
Murasame, Kiyomori, Kikuichimoji



Dogsandcatsand
Male
Virgo
53
56
Knight
Talk Skill
Parry
Halve MP
Teleport 2

Defender
Buckler
Platinum Helmet
Plate Mail
108 Gems

Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Stasis Sword
Praise, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute

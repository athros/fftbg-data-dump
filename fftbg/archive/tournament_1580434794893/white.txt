Player: !White
Team: White Team
Palettes: White/Blue



Jeramie
Male
Aries
57
67
Squire
Elemental
Sunken State
Long Status
Move+3

Night Killer

Leather Hat
Silk Robe
Genji Gauntlet

Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Lava Ball



Alberto
Male
Cancer
63
67
Samurai
White Magic
MP Restore
Magic Defense UP
Lava Walking

Holy Lance

Crystal Helmet
Plate Mail
Feather Boots

Koutetsu, Murasame, Kikuichimoji
Cure, Raise, Protect, Shell 2



Lesley
Male
Virgo
44
75
Chemist
Summon Magic
Earplug
Short Status
Fly

Romanda Gun

Feather Hat
Clothes
Germinas Boots

Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Moogle, Titan, Fairy



Tougou
Male
Leo
69
41
Knight
Jump
Abandon
Equip Polearm
Retreat

Giant Axe
Round Shield
Mythril Helmet
White Robe
Angel Ring

Speed Break, Dark Sword, Surging Sword
Level Jump3, Vertical Jump2

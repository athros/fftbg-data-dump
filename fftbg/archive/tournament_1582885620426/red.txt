Player: !Red
Team: Red Team
Palettes: Red/Brown



Hzor
Male
Taurus
40
47
Lancer
Battle Skill
MP Restore
Dual Wield
Fly

Holy Lance
Ivory Rod
Platinum Helmet
Mythril Armor
Germinas Boots

Level Jump4, Vertical Jump7
Armor Break, Shield Break, Power Break, Justice Sword



Mots45
Female
Gemini
52
78
Priest
Steal
Damage Split
Short Charge
Swim

Flame Whip

Golden Hairpin
Silk Robe
Sprint Shoes

Raise, Raise 2, Reraise, Wall, Esuna
Steal Weapon, Leg Aim



Hyvi
Male
Scorpio
72
46
Bard
Elemental
Counter
Doublehand
Jump+2

Windslash Bow

Triangle Hat
Carabini Mail
Defense Armlet

Angel Song, Life Song, Magic Song, Last Song, Diamond Blade, Sky Demon
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Clar3d
Male
Capricorn
69
58
Archer
Basic Skill
MP Restore
Secret Hunt
Fly

Hunting Bow
Flame Shield
Green Beret
Judo Outfit
Power Wrist

Charge+2, Charge+3
Throw Stone, Heal, Tickle, Yell, Cheer Up

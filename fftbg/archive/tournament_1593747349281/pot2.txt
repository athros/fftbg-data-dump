Final Bets: green - 8 bets for 4,942G (53.9%, x0.85); yellow - 10 bets for 4,221G (46.1%, x1.17)

green bets:
killth3kid: 1,177G (23.8%, 117,758G)
BirbBrainsBot: 1,000G (20.2%, 155,349G)
NicoSavoy: 1,000G (20.2%, 107,983G)
DarrenDinosaurs: 687G (13.9%, 687G)
Shalloween: 400G (8.1%, 69,180G)
ColetteMSLP: 400G (8.1%, 4,105G)
getthemoneyz: 178G (3.6%, 1,164,251G)
nifboy: 100G (2.0%, 1,121G)

yellow bets:
DeathTaxesAndAnime: 1,848G (43.8%, 1,848G)
RampagingRobot: 500G (11.8%, 4,500G)
SkylerBunny: 408G (9.7%, 408G)
HaateXIII: 250G (5.9%, 4,057G)
Mathlexis: 220G (5.2%, 220G)
loveyouallfriends: 216G (5.1%, 216G)
Evewho: 200G (4.7%, 11,882G)
datadrivenbot: 200G (4.7%, 50,769G)
twelfthrootoftwo: 200G (4.7%, 11,398G)
ArashiKurobara: 179G (4.2%, 11,579G)

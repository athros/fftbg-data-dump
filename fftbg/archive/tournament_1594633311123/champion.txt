Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Prince Rogers Nelson
Male
Gemini
60
50
Bard
Yin Yang Magic
Parry
Doublehand
Jump+2

Bloody Strings

Green Beret
Power Sleeve
Feather Mantle

Angel Song, Cheer Song, Battle Song, Nameless Song, Sky Demon
Poison, Silence Song, Foxbird, Dispel Magic, Paralyze



Shineeyo
Male
Cancer
69
40
Mime

Arrow Guard
Equip Shield
Move-MP Up


Diamond Shield
Headgear
Mystic Vest
Cursed Ring

Mimic




SkylerBunny
Male
Capricorn
57
40
Time Mage
Draw Out
Parry
Attack UP
Jump+3

Ivory Rod

Golden Hairpin
Earth Clothes
Defense Ring

Haste, Haste 2, Slow 2, Immobilize, Demi, Stabilize Time
Asura, Koutetsu, Heaven's Cloud, Kikuichimoji, Masamune



The Pengwin
Female
Pisces
71
77
Oracle
Jump
Absorb Used MP
Martial Arts
Teleport

Star Bag

Headgear
Linen Robe
Elf Mantle

Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze
Level Jump8, Vertical Jump4

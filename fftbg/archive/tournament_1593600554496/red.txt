Player: !Red
Team: Red Team
Palettes: Red/Brown



Just Here2
Male
Aquarius
72
76
Calculator
Black Magic
Damage Split
Equip Polearm
Move+2

Javelin

Green Beret
Silk Robe
Spike Shoes

CT, Prime Number, 4
Fire, Fire 3, Bolt, Bolt 4, Ice, Empower



NovaKnight21
Female
Aries
65
40
Mime

Auto Potion
Martial Arts
Move+3



Headgear
Mystic Vest
Defense Ring

Mimic




BlackFireUK
Male
Cancer
55
79
Mime

Critical Quick
Martial Arts
Ignore Terrain



Twist Headband
Mythril Vest
Angel Ring

Mimic




Grininda
Male
Gemini
59
67
Lancer
Basic Skill
Parry
Dual Wield
Move+3

Partisan
Gokuu Rod
Mythril Helmet
Carabini Mail
Diamond Armlet

Level Jump8, Vertical Jump2
Accumulate, Dash, Tickle, Yell, Cheer Up

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DuneMeta
Male
Capricorn
46
57
Geomancer
Talk Skill
Caution
Sicken
Ignore Terrain

Kiyomori
Crystal Shield
Feather Hat
Mystic Vest
Angel Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Gusty Wind, Lava Ball
Invitation, Persuade, Praise, Threaten, Death Sentence, Insult, Mimic Daravon, Refute



Hasterious
Male
Aries
66
66
Lancer
Elemental
Mana Shield
Doublehand
Move+1

Ivory Rod

Genji Helmet
Gold Armor
Salty Rage

Level Jump8, Vertical Jump8
Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Ar Tactic
Female
Aries
75
57
Summoner
Jump
Damage Split
Martial Arts
Levitate

Oak Staff

Green Beret
Light Robe
Germinas Boots

Moogle, Shiva, Golem, Carbunkle, Salamander, Fairy
Level Jump8, Vertical Jump5



Emmott
Male
Aquarius
51
69
Time Mage
Draw Out
MA Save
Short Charge
Ignore Terrain

Cypress Rod

Flash Hat
Mythril Vest
Leather Mantle

Haste, Slow, Reflect, Demi
Heaven's Cloud

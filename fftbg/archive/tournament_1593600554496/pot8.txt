Final Bets: red - 6 bets for 3,236G (61.8%, x0.62); champion - 5 bets for 1,997G (38.2%, x1.62)

red bets:
BlackFireUK: 1,000G (30.9%, 3,649G)
BirbBrainsBot: 1,000G (30.9%, 32,815G)
amiture: 1,000G (30.9%, 17,522G)
serperemagus: 100G (3.1%, 27,386G)
CosmicTactician: 100G (3.1%, 59,070G)
getthemoneyz: 36G (1.1%, 1,133,027G)

champion bets:
gorgewall: 1,274G (63.8%, 1,274G)
twelfthrootoftwo: 300G (15.0%, 20,693G)
Mathlexis: 223G (11.2%, 223G)
E_Ballard: 100G (5.0%, 3,294G)
datadrivenbot: 100G (5.0%, 51,090G)

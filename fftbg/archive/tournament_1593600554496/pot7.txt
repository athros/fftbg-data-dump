Final Bets: red - 8 bets for 5,289G (91.4%, x0.09); brown - 3 bets for 500G (8.6%, x10.58)

red bets:
prince_rogers_nelson_: 1,569G (29.7%, 1,569G)
gorgewall: 1,164G (22.0%, 1,164G)
BirbBrainsBot: 1,000G (18.9%, 32,720G)
twelfthrootoftwo: 500G (9.5%, 20,646G)
helpimabug: 452G (8.5%, 452G)
Bryon_W: 300G (5.7%, 2,311G)
Mathlexis: 204G (3.9%, 204G)
datadrivenbot: 100G (1.9%, 51,081G)

brown bets:
FFTBattleground: 274G (54.8%, 500G)
getthemoneyz: 126G (25.2%, 1,133,153G)
E_Ballard: 100G (20.0%, 3,394G)

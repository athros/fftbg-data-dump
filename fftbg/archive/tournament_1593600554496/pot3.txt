Final Bets: white - 4 bets for 1,946G (26.9%, x2.72); black - 10 bets for 5,296G (73.1%, x0.37)

white bets:
DamnThatShark: 1,000G (51.4%, 4,129G)
gorgewall: 696G (35.8%, 696G)
ar_tactic: 150G (7.7%, 63,885G)
datadrivenbot: 100G (5.1%, 50,648G)

black bets:
ShintaroNayaka: 1,137G (21.5%, 1,137G)
BirbBrainsBot: 1,000G (18.9%, 35,183G)
amiture: 1,000G (18.9%, 19,522G)
prince_rogers_nelson_: 600G (11.3%, 600G)
RaIshtar: 500G (9.4%, 13,145G)
helpimabug: 452G (8.5%, 452G)
Smudgenosferatu: 207G (3.9%, 207G)
Mathlexis: 200G (3.8%, 850G)
E_Ballard: 100G (1.9%, 3,333G)
serperemagus: 100G (1.9%, 27,416G)

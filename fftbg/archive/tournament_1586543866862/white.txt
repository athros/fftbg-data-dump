Player: !White
Team: White Team
Palettes: White/Blue



Silentperogy
Male
Sagittarius
73
49
Bard
Basic Skill
Meatbone Slash
Secret Hunt
Move+1

Fairy Harp

Headgear
Carabini Mail
Feather Mantle

Angel Song, Life Song, Battle Song, Sky Demon
Dash, Yell



Jerboj
Female
Sagittarius
69
48
Ninja
Steal
Counter Flood
Equip Gun
Swim

Bestiary
Battle Folio
Leather Hat
Clothes
Defense Armlet

Shuriken, Knife, Staff, Spear
Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim, Leg Aim



Kronikle
Male
Aquarius
47
69
Priest
Draw Out
Earplug
Equip Polearm
Jump+3

Ivory Rod

Green Beret
White Robe
Feather Mantle

Cure 4, Raise, Reraise, Regen, Wall, Esuna
Koutetsu, Kikuichimoji, Masamune



Phrossi V
Female
Virgo
75
48
Priest
Throw
Dragon Spirit
Magic Attack UP
Swim

Flame Whip

Thief Hat
White Robe
Elf Mantle

Cure 2, Raise, Reraise, Regen, Wall, Esuna
Shuriken, Knife, Stick

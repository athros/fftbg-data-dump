Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lord Burrah
Male
Capricorn
70
46
Archer
Item
Caution
Doublehand
Swim

Lightning Bow

Black Hood
Rubber Costume
Leather Mantle

Charge+1, Charge+4, Charge+5
Potion, Ether, Eye Drop, Phoenix Down



IndecisiveNinja
Male
Gemini
50
44
Knight
White Magic
Brave Up
Short Charge
Ignore Height

Defender
Mythril Shield
Grand Helmet
Maximillian
Sprint Shoes

Armor Break, Power Break, Mind Break
Raise, Protect, Shell 2



EnemyController
Female
Sagittarius
66
65
Knight
Item
Counter
Defend
Waterwalking

Iron Sword
Venetian Shield
Cross Helmet
Diamond Armor
Dracula Mantle

Weapon Break, Magic Break, Power Break, Mind Break, Night Sword, Surging Sword
Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down



Old Overholt
Female
Cancer
45
60
Lancer
Item
Damage Split
Magic Attack UP
Move-HP Up

Mythril Spear
Ice Shield
Circlet
Robe of Lords
Spike Shoes

Level Jump4, Vertical Jump2
Potion, X-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down

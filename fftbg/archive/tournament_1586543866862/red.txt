Player: !Red
Team: Red Team
Palettes: Red/Brown



Kai Shee
Male
Cancer
67
58
Mediator
Jump
Auto Potion
Concentrate
Swim

Romanda Gun

Flash Hat
Mythril Vest
Spike Shoes

Persuade, Solution, Insult, Mimic Daravon, Refute
Level Jump4, Vertical Jump8



ApplesauceBoss
Female
Cancer
50
79
Squire
Charge
Dragon Spirit
Long Status
Move-HP Up

Mythril Sword
Bronze Shield
Black Hood
Wizard Outfit
Reflect Ring

Accumulate, Dash, Throw Stone, Heal, Scream
Charge+1, Charge+4, Charge+5, Charge+10



Joewcarson
Male
Gemini
54
50
Chemist
Sing
Damage Split
Magic Attack UP
Waterwalking

Cute Bag

Headgear
Black Costume
Elf Mantle

Potion, Maiden's Kiss, Holy Water, Phoenix Down
Life Song, Magic Song, Nameless Song, Space Storage, Sky Demon, Hydra Pit



Chuckolator
Male
Aquarius
46
59
Ninja
Draw Out
Catch
Sicken
Teleport

Scorpion Tail
Flail
Golden Hairpin
Brigandine
Leather Mantle

Shuriken, Ninja Sword
Koutetsu, Bizen Boat, Murasame, Kiyomori

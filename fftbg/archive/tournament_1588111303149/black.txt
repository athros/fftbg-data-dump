Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Leo
52
71
Ninja
Draw Out
Meatbone Slash
Magic Defense UP
Ignore Height

Short Edge
Sasuke Knife
Green Beret
Earth Clothes
Defense Armlet

Shuriken, Wand
Asura, Koutetsu, Heaven's Cloud



Baron Von Scrub
Female
Libra
50
76
Time Mage
Elemental
MA Save
Equip Polearm
Move+2

Octagon Rod

Golden Hairpin
Brigandine
Jade Armlet

Haste, Reflect, Quick, Demi
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



ACSpree
Male
Serpentarius
70
44
Ninja
Item
PA Save
Attack UP
Jump+3

Morning Star
Morning Star
Headgear
Adaman Vest
Rubber Shoes

Shuriken, Knife, Wand
Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down



Gelwain
Male
Gemini
55
78
Summoner
Item
Abandon
Equip Armor
Fly

Wizard Staff

Platinum Helmet
Wizard Robe
Elf Mantle

Shiva, Golem, Carbunkle, Odin, Fairy, Cyclops
Potion, X-Potion, Antidote, Eye Drop, Soft, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Aldrammech
Female
Gemini
63
60
Knight
Time Magic
Counter Tackle
Short Charge
Ignore Terrain

Defender
Mythril Shield
Bronze Helmet
Plate Mail
Defense Ring

Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Stasis Sword, Surging Sword
Haste, Slow 2, Stop, Immobilize, Float



ANFz
Male
Libra
51
73
Monk
Summon Magic
Counter Tackle
Concentrate
Levitate



Red Hood
Judo Outfit
Bracer

Secret Fist, Purification, Chakra, Revive
Ifrit, Titan, Golem, Leviathan, Lich, Cyclops



Gawyel95
Monster
Leo
80
45
Tiamat










Nekojin
Female
Sagittarius
57
56
Knight
Basic Skill
Mana Shield
Halve MP
Move-MP Up

Sleep Sword
Buckler
Circlet
Genji Armor
Power Wrist

Armor Break, Shield Break, Magic Break, Speed Break, Power Break, Night Sword, Surging Sword
Accumulate, Throw Stone, Heal, Scream

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nekojin
Female
Scorpio
63
45
Archer
Steal
HP Restore
Short Charge
Move+2

Windslash Bow

Barbuta
Leather Outfit
Defense Ring

Charge+2, Charge+5
Gil Taking, Arm Aim



HaateXIII
Female
Gemini
48
58
Summoner
Basic Skill
Caution
Long Status
Lava Walking

Faith Rod

Leather Hat
Linen Robe
N-Kai Armlet

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Silf
Accumulate, Throw Stone, Heal



Silentkaster
Male
Virgo
44
50
Mime

MP Restore
Equip Shield
Jump+3


Crystal Shield
Headgear
Clothes
108 Gems

Mimic




Doroku9
Female
Aries
42
63
Priest
Jump
Arrow Guard
Equip Gun
Move+3

Madlemgen

Holy Miter
Silk Robe
Angel Ring

Cure, Cure 2, Cure 4, Shell 2, Esuna
Level Jump5, Vertical Jump8

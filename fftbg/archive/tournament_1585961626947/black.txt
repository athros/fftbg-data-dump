Player: !Black
Team: Black Team
Palettes: Black/Red



ColetteMSLP
Female
Taurus
63
53
Dancer
Elemental
Damage Split
Dual Wield
Move+3

Persia
Persia
Leather Hat
Black Costume
Reflect Ring

Wiznaibus, Disillusion, Nameless Dance, Obsidian Blade
Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Lodrak
Male
Sagittarius
69
69
Ninja
Basic Skill
Blade Grasp
Defend
Ignore Height

Assassin Dagger
Dagger
Black Hood
Mystic Vest
Rubber Shoes

Shuriken, Bomb, Staff, Wand
Throw Stone, Heal, Tickle, Cheer Up



ApplesauceBoss
Male
Capricorn
44
55
Archer
Sing
Caution
Doublehand
Ignore Terrain

Lightning Bow

Leather Hat
Brigandine
Diamond Armlet

Charge+1, Charge+3, Charge+10
Life Song, Cheer Song, Battle Song, Last Song, Hydra Pit



Shakarak
Female
Leo
64
57
Dancer
Summon Magic
MA Save
Short Charge
Ignore Terrain

Panther Bag

Holy Miter
Light Robe
Sprint Shoes

Last Dance, Obsidian Blade
Moogle, Shiva, Odin, Leviathan, Silf

Player: !White
Team: White Team
Palettes: White/Blue



Deathmaker06
Male
Sagittarius
60
65
Ninja
Black Magic
Sunken State
Martial Arts
Ignore Height

Hidden Knife
Flail
Barette
Brigandine
Defense Ring

Shuriken, Bomb, Wand
Fire



GnielKnows
Male
Scorpio
68
47
Knight
Draw Out
Absorb Used MP
Sicken
Fly

Defender
Escutcheon
Platinum Helmet
Leather Armor
Magic Ring

Head Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Koutetsu, Muramasa



B0shii
Male
Leo
72
43
Knight
White Magic
Auto Potion
Short Charge
Lava Walking

Ragnarok
Diamond Shield
Genji Helmet
Bronze Armor
Power Wrist

Head Break, Armor Break, Shield Break, Mind Break, Explosion Sword
Cure, Cure 2, Raise, Regen, Protect, Esuna



NicoSavoy
Male
Cancer
60
50
Mime

Counter
Short Status
Lava Walking



Black Hood
Gold Armor
Magic Ring

Mimic


Player: !White
Team: White Team
Palettes: White/Blue



Estan AD
Male
Virgo
46
50
Wizard
Elemental
Blade Grasp
Equip Polearm
Move-HP Up

Ivory Rod

Flash Hat
Wizard Robe
Sprint Shoes

Fire 2, Bolt, Ice, Ice 3, Death, Flare
Water Ball, Static Shock, Blizzard, Lava Ball



Bruubarg
Male
Pisces
64
44
Geomancer
Black Magic
Damage Split
Martial Arts
Teleport


Gold Shield
Green Beret
Rubber Costume
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Fire, Fire 3, Fire 4, Bolt, Bolt 2, Ice 2, Frog



Sinnyil2
Female
Virgo
37
67
Calculator
White Magic
Mana Shield
Equip Bow
Teleport

Lightning Bow

Red Hood
Light Robe
Chantage

CT, Height, 5, 3
Cure, Cure 2, Cure 4, Raise, Reraise, Wall, Esuna



Galkife
Male
Aquarius
79
54
Chemist
Time Magic
Mana Shield
Maintenance
Jump+2

Star Bag

Golden Hairpin
Earth Clothes
Spike Shoes

Potion, Hi-Potion, X-Potion, Eye Drop, Echo Grass, Phoenix Down
Haste, Stop, Immobilize, Stabilize Time

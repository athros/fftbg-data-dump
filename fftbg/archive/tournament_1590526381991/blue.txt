Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powermhero
Monster
Taurus
41
42
Floating Eye










Seawalker777
Female
Libra
54
69
Calculator
Time Magic
Auto Potion
Magic Attack UP
Move+1

Battle Folio

Golden Hairpin
White Robe
Magic Ring

CT, 5, 4, 3
Haste 2, Immobilize, Quick, Demi



Old Overholt
Female
Sagittarius
62
74
Wizard
Yin Yang Magic
Parry
Short Charge
Move+2

Wizard Rod

Flash Hat
Silk Robe
Red Shoes

Bolt, Ice 4
Zombie, Foxbird, Paralyze, Sleep, Dark Holy



Baron Von Scrub
Male
Sagittarius
71
74
Monk
Black Magic
Counter
Magic Defense UP
Ignore Terrain



Headgear
Black Costume
Vanish Mantle

Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Fire 3, Fire 4, Bolt, Bolt 2, Ice 2, Ice 4, Empower

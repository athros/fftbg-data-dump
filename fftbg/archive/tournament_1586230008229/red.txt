Player: !Red
Team: Red Team
Palettes: Red/Brown



SystemShut
Female
Aquarius
77
61
Priest
Throw
Brave Up
Attack UP
Teleport

Flame Whip

Headgear
Leather Outfit
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell 2, Esuna
Shuriken, Bomb, Staff



RughSontos
Male
Taurus
72
40
Ninja
Draw Out
Abandon
Maintenance
Swim

Main Gauche
Orichalcum
Headgear
Power Sleeve
Germinas Boots

Axe
Koutetsu, Bizen Boat, Muramasa, Kikuichimoji



StealthModeLocke
Male
Pisces
54
74
Squire
Battle Skill
Damage Split
Beastmaster
Move-HP Up

Flame Whip
Kaiser Plate
Twist Headband
Platinum Armor
Power Wrist

Accumulate, Heal, Tickle, Yell, Cheer Up
Head Break, Shield Break, Magic Break



Bryan792
Male
Virgo
64
74
Lancer
White Magic
Critical Quick
Dual Wield
Teleport

Partisan
Ivory Rod
Leather Helmet
Plate Mail
Defense Ring

Level Jump3, Vertical Jump5
Cure 3, Cure 4, Reraise, Regen, Wall, Esuna

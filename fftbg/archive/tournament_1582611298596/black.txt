Player: !Black
Team: Black Team
Palettes: Black/Red



Clar3d
Female
Virgo
74
57
Chemist
Elemental
Critical Quick
Equip Bow
Jump+1

Cross Bow

Red Hood
Earth Clothes
Germinas Boots

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Clippopo
Male
Aquarius
54
69
Mediator
Punch Art
Speed Save
Defense UP
Levitate

Blaze Gun

Golden Hairpin
Robe of Lords
Defense Armlet

Invitation, Threaten, Death Sentence, Refute
Earth Slash, Revive, Seal Evil



AoiTsukiYuri
Female
Cancer
75
62
Ninja
Dance
Counter
Equip Gun
Jump+1

Papyrus Codex
Bestiary
Headgear
Chain Vest
Cherche

Axe, Spear
Slow Dance, Disillusion, Last Dance, Void Storage



Blain Cooper
Male
Virgo
76
70
Ninja
Basic Skill
Parry
Magic Attack UP
Move-HP Up

Blind Knife
Kunai
Leather Hat
Earth Clothes
Angel Ring

Shuriken, Bomb
Accumulate, Dash, Heal, Tickle, Cheer Up, Wish

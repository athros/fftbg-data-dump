Player: !Green
Team: Green Team
Palettes: Green/White



Electric Glass
Male
Sagittarius
46
49
Oracle
Summon Magic
Absorb Used MP
Magic Defense UP
Lava Walking

Gokuu Rod

Flash Hat
Earth Clothes
Angel Ring

Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Foxbird, Sleep, Petrify, Dark Holy
Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander, Cyclops



Sirbranedamuj
Male
Virgo
58
68
Chemist
Time Magic
Caution
Magic Defense UP
Levitate

Glacier Gun

Twist Headband
Clothes
Wizard Mantle

Potion, Hi-Potion, Ether, Remedy, Phoenix Down
Slow, Immobilize, Float, Demi 2, Stabilize Time, Meteor



OhThatsNice
Female
Sagittarius
77
47
Mime

Mana Shield
Attack UP
Ignore Terrain



Green Beret
Black Costume
Vanish Mantle

Mimic




Xskyeeblueex
Male
Scorpio
50
75
Ninja
Elemental
HP Restore
Equip Gun
Move-MP Up

Battle Folio
Papyrus Codex
Golden Hairpin
Chain Vest
Bracer

Shuriken, Bomb, Knife, Sword, Staff
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

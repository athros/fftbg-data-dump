Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Just Here2
Male
Cancer
76
76
Geomancer
Yin Yang Magic
Abandon
Equip Axe
Swim

Rainbow Staff
Mythril Shield
Headgear
Chain Vest
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Blizzard
Spell Absorb, Blind Rage, Paralyze, Sleep



PLifer
Female
Capricorn
80
74
Monk
Talk Skill
Caution
Monster Talk
Move-HP Up



Feather Hat
Brigandine
Magic Ring

Chakra
Persuade, Threaten, Insult, Refute, Rehabilitate



ThreeMileIsland
Male
Pisces
53
77
Wizard
Summon Magic
Earplug
Equip Gun
Move+1

Blast Gun

Headgear
Power Sleeve
Feather Boots

Fire 2, Fire 4, Bolt, Bolt 3, Ice, Ice 2, Ice 4
Ramuh, Leviathan, Salamander, Silf



Pplvee1
Male
Scorpio
74
40
Knight
Punch Art
Absorb Used MP
Magic Attack UP
Retreat

Slasher
Kaiser Plate
Mythril Helmet
Mythril Armor
Feather Boots

Weapon Break, Speed Break, Mind Break, Stasis Sword
Spin Fist, Earth Slash, Purification, Revive

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



HASTERIOUS
Female
Virgo
78
55
Samurai
Item
Meatbone Slash
Equip Knife
Move+1

Dagger

Mythril Helmet
Platinum Armor
Small Mantle

Murasame
Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down



Volgrathemoose
Male
Leo
77
42
Archer
Talk Skill
Catch
Doublehand
Waterbreathing

Romanda Gun

Red Hood
Leather Outfit
Dracula Mantle

Charge+10
Invitation, Praise, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute



DrAntiSocial
Female
Sagittarius
71
60
Squire
Time Magic
Dragon Spirit
Short Charge
Teleport

Morning Star
Buckler
Genji Helmet
Bronze Armor
Defense Armlet

Dash, Fury, Wish
Haste, Slow 2, Stop, Immobilize, Float, Demi, Demi 2, Stabilize Time



Roqqqpsi
Female
Libra
67
63
Lancer
Summon Magic
Abandon
Equip Bow
Retreat

Bow Gun
Diamond Shield
Mythril Helmet
Mythril Armor
Cursed Ring

Level Jump8, Vertical Jump5
Moogle, Shiva, Golem, Bahamut, Leviathan

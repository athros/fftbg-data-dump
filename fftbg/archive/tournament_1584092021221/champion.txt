Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Alness49
Female
Aquarius
80
68
Wizard
Battle Skill
Distribute
Equip Bow
Lava Walking

Lightning Bow

Feather Hat
Black Robe
Diamond Armlet

Fire 2, Fire 3, Bolt 3, Death
Head Break, Armor Break, Shield Break, Mind Break, Dark Sword



ZergTwitch
Female
Pisces
42
55
Geomancer
Time Magic
Mana Shield
Doublehand
Waterwalking

Murasame

Feather Hat
Black Costume
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Float, Reflect, Stabilize Time, Galaxy Stop



Ar Tactic
Female
Aquarius
54
52
Time Mage
Summon Magic
MP Restore
Martial Arts
Levitate



Triangle Hat
Silk Robe
Dracula Mantle

Haste, Slow, Float, Stabilize Time
Moogle, Ramuh, Ifrit, Titan, Silf, Lich



Vacillents
Male
Aquarius
68
47
Lancer
Sing
Counter Tackle
Defend
Swim

Gungnir
Bronze Shield
Mythril Helmet
Plate Mail
Small Mantle

Level Jump8, Vertical Jump8
Angel Song, Life Song, Sky Demon

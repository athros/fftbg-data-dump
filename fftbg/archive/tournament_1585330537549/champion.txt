Player: !zChamp
Team: Champion Team
Palettes: Green/White



Wargles
Male
Scorpio
77
68
Thief
Basic Skill
Mana Shield
Magic Defense UP
Jump+1

Dagger

Green Beret
Adaman Vest
Feather Mantle

Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Throw Stone, Heal



AdaephonD
Female
Sagittarius
54
50
Monk
Talk Skill
Brave Up
Monster Talk
Move+3



Thief Hat
Earth Clothes
Germinas Boots

Pummel, Secret Fist, Purification, Seal Evil
Invitation, Persuade, Praise, Preach, Negotiate, Refute



Vampire Killer
Male
Scorpio
49
79
Thief
Talk Skill
Mana Shield
Magic Defense UP
Ignore Terrain

Main Gauche

Flash Hat
Earth Clothes
Cherche

Gil Taking, Steal Shield, Steal Weapon, Arm Aim
Invitation, Persuade, Preach, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Powergems
Male
Sagittarius
58
38
Samurai
Black Magic
Brave Up
Doublehand
Move+3

Asura Knife

Iron Helmet
Platinum Armor
Wizard Mantle

Koutetsu, Murasame, Kiyomori, Muramasa
Fire, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4

Player: !Green
Team: Green Team
Palettes: Green/White



Chambs12
Female
Scorpio
64
69
Ninja
Time Magic
Counter
Equip Gun
Swim

Papyrus Codex
Bestiary
Feather Hat
Brigandine
Bracer

Shuriken, Staff, Wand
Haste, Slow 2, Quick, Demi 2, Stabilize Time



WoooBlaaa
Male
Gemini
58
61
Calculator
Yin Yang Magic
Auto Potion
Magic Attack UP
Swim

Wizard Rod

Green Beret
Secret Clothes
Angel Ring

CT, Height, 5, 3
Blind, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep



SQUiDSQUARKLIN
Male
Cancer
73
40
Chemist
Steal
Counter Tackle
Dual Wield
Ignore Height

Stone Gun
Glacier Gun
Leather Hat
Mystic Vest
Rubber Shoes

Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Steal Status



Silentkaster
Female
Virgo
80
60
Wizard
White Magic
Counter Magic
Attack UP
Teleport

Flame Rod

Headgear
White Robe
Magic Gauntlet

Fire, Fire 3, Bolt 2, Bolt 3, Empower
Raise 2, Protect, Shell 2, Wall, Esuna

Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Monster
Cancer
63
53
Pisco Demon










Hasterious
Female
Leo
79
49
Chemist
Summon Magic
Damage Split
Beastmaster
Levitate

Star Bag

Headgear
Leather Outfit
Rubber Shoes

Potion, Ether, Holy Water, Phoenix Down
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle



CosmicTactician
Male
Aquarius
40
60
Lancer
Basic Skill
Absorb Used MP
Attack UP
Ignore Height

Holy Lance
Round Shield
Barbuta
Plate Mail
Jade Armlet

Level Jump2, Vertical Jump8
Dash, Heal, Tickle



J2DaBibbles
Male
Taurus
64
52
Chemist
Jump
Caution
Equip Armor
Ignore Height

Air Knife

Holy Miter
Light Robe
Elf Mantle

Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down
Level Jump3, Vertical Jump7

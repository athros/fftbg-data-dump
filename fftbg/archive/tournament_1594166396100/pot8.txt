Final Bets: black - 20 bets for 11,097G (34.4%, x1.91); champion - 18 bets for 21,141G (65.6%, x0.52)

black bets:
KyleWonToLiveForever: 2,450G (22.1%, 2,450G)
lowlf: 1,111G (10.0%, 138,650G)
electric_algus: 1,000G (9.0%, 48,651G)
BirbBrainsBot: 1,000G (9.0%, 137,947G)
Zeroroute: 600G (5.4%, 600G)
EnemyController: 555G (5.0%, 1,384,561G)
MagicBottle: 512G (4.6%, 16,318G)
BoneMiser: 500G (4.5%, 500G)
Blakelmenakle: 500G (4.5%, 2,070G)
moonliquor: 500G (4.5%, 1,657G)
Rytor: 500G (4.5%, 21,131G)
Snoopiku: 333G (3.0%, 30,942G)
ZCKaiser: 328G (3.0%, 6,410G)
RageImmortaI: 284G (2.6%, 2,018G)
flacococo: 208G (1.9%, 208G)
J_MacC1991: 200G (1.8%, 1,928G)
datadrivenbot: 200G (1.8%, 55,448G)
dolzytwitch: 150G (1.4%, 2,490G)
TasisSai: 100G (0.9%, 108G)
getthemoneyz: 66G (0.6%, 1,225,466G)

champion bets:
run_with_stone_GUNs: 6,453G (30.5%, 12,906G)
ValtonZenola: 3,576G (16.9%, 3,576G)
Virilikus: 2,000G (9.5%, 195,546G)
reinoe: 2,000G (9.5%, 39,868G)
Evewho: 1,000G (4.7%, 5,451G)
Zbgs: 1,000G (4.7%, 25,732G)
Seaweed_B: 1,000G (4.7%, 2,073G)
DeathTaxesAndAnime: 884G (4.2%, 884G)
dogsandcatsand: 654G (3.1%, 654G)
Laserman1000: 588G (2.8%, 21,288G)
nekojin: 500G (2.4%, 8,166G)
twelfthrootoftwo: 500G (2.4%, 5,398G)
sososteve: 350G (1.7%, 4,319G)
Blastinus: 200G (0.9%, 578G)
turbn: 128G (0.6%, 128G)
raenrond: 108G (0.5%, 108G)
CosmicTactician: 100G (0.5%, 42,834G)
Quadh0nk: 100G (0.5%, 5,015G)

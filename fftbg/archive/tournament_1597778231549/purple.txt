Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Redmage4evah
Male
Virgo
44
75
Summoner
Jump
Caution
Short Charge
Teleport

Rod

Holy Miter
Linen Robe
Germinas Boots

Moogle, Shiva, Titan, Golem, Carbunkle, Silf, Fairy, Lich, Cyclops
Level Jump5, Vertical Jump5



JumbocactuarX27
Female
Scorpio
43
72
Knight
Dance
Caution
Defend
Lava Walking

Iron Sword
Escutcheon
Mythril Helmet
Chain Mail
Jade Armlet

Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break
Wiznaibus, Polka Polka, Disillusion, Last Dance



Zeroroute
Male
Aries
38
57
Calculator
Lucavi Skill
Counter Flood
Equip Polearm
Retreat

Javelin

Crystal Helmet
Genji Armor
Cursed Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Aldrammech
Female
Capricorn
56
41
Calculator
Yin Yang Magic
HP Restore
Martial Arts
Ignore Height



Twist Headband
White Robe
Reflect Ring

CT, Height, Prime Number, 5
Spell Absorb, Life Drain, Doubt Faith, Zombie, Dispel Magic, Paralyze

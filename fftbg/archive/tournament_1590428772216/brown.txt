Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NovaKnight21
Monster
Libra
44
80
Dark Behemoth










HaplessOne
Male
Aries
48
78
Knight
Draw Out
Dragon Spirit
Defend
Ignore Height

Defender
Bronze Shield
Iron Helmet
Plate Mail
Spike Shoes

Armor Break, Justice Sword
Murasame, Kikuichimoji



Nifboy
Female
Virgo
57
71
Summoner
Talk Skill
Regenerator
Magic Attack UP
Move-HP Up

Oak Staff

Golden Hairpin
Wizard Robe
108 Gems

Moogle, Carbunkle, Bahamut, Salamander, Silf
Praise, Death Sentence, Mimic Daravon, Refute



SSwing
Male
Aquarius
53
43
Summoner
Draw Out
Mana Shield
Beastmaster
Jump+1

Poison Rod

Triangle Hat
Linen Robe
Reflect Ring

Moogle, Carbunkle
Asura, Heaven's Cloud, Kikuichimoji

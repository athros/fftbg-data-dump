Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DavenIII
Male
Libra
70
55
Monk
Basic Skill
PA Save
Attack UP
Move+3



Thief Hat
Power Sleeve
108 Gems

Pummel, Earth Slash, Purification, Chakra, Revive
Dash, Heal, Yell, Fury



Riot Jayway
Female
Libra
77
46
Wizard
Dance
Parry
Equip Axe
Fly

Wizard Staff

Holy Miter
Wizard Outfit
Magic Gauntlet

Fire, Fire 3, Bolt 3, Bolt 4, Ice, Ice 2, Empower
Slow Dance, Polka Polka, Nether Demon



Lordminsc
Monster
Aries
71
79
Porky










Theatheologist
Male
Aries
49
79
Thief
Sing
Sunken State
Long Status
Retreat

Hidden Knife

Feather Hat
Adaman Vest
Cursed Ring

Gil Taking, Steal Helmet, Steal Status, Arm Aim
Battle Song, Magic Song, Sky Demon

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KasugaiRoastedPeas
Female
Cancer
38
49
Mime

HP Restore
Martial Arts
Jump+2



Flash Hat
Rubber Costume
Feather Boots

Mimic




Oogthecaveman
Monster
Libra
76
52
Grenade










Pandasforsale
Male
Taurus
76
76
Thief
Item
MP Restore
Dual Wield
Ignore Terrain

Iga Knife
Assassin Dagger
Green Beret
Power Sleeve
Bracer

Steal Helmet, Steal Weapon, Steal Status, Arm Aim
Hi-Ether, Eye Drop, Holy Water, Phoenix Down



Lowlf
Female
Libra
71
79
Oracle
Elemental
Auto Potion
Halve MP
Swim

Ivory Rod

Flash Hat
Black Costume
108 Gems

Poison, Doubt Faith, Blind Rage, Dispel Magic, Paralyze
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind

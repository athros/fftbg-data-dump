Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DudeMonkey77
Male
Sagittarius
45
74
Time Mage
Steal
Counter
Equip Armor
Ignore Terrain

Healing Staff

Leather Helmet
Mythril Armor
Jade Armlet

Haste 2, Stop, Immobilize, Reflect, Stabilize Time, Galaxy Stop
Gil Taking, Steal Heart, Steal Armor, Arm Aim



SacrificialToast
Male
Aquarius
52
75
Bard
Draw Out
Brave Up
Short Status
Swim

Ramia Harp

Black Hood
Clothes
Dracula Mantle

Life Song, Battle Song, Magic Song, Nameless Song, Diamond Blade
Koutetsu, Muramasa, Kikuichimoji



Reverie3
Female
Gemini
62
70
Oracle
Dance
Regenerator
Equip Polearm
Fly

Mythril Spear

Cachusha
Chain Vest
Spike Shoes

Blind, Poison, Spell Absorb, Life Drain, Blind Rage, Paralyze, Petrify
Slow Dance



OtherBrand
Female
Aquarius
45
44
Time Mage
Talk Skill
Auto Potion
Short Charge
Teleport

Rainbow Staff

Headgear
Mythril Vest
Cursed Ring

Slow, Quick, Demi, Demi 2
Persuade, Preach, Solution

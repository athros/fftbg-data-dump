Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nizaha
Female
Sagittarius
75
64
Thief
Battle Skill
Mana Shield
Secret Hunt
Move-MP Up

Broad Sword

Flash Hat
Black Costume
Germinas Boots

Steal Helmet, Steal Weapon, Steal Status, Arm Aim
Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword



Wurstwaesserchen
Male
Cancer
42
40
Bard
Jump
Hamedo
Dual Wield
Jump+1

Ramia Harp
Bloody Strings
Green Beret
Diamond Armor
Reflect Ring

Nameless Song, Diamond Blade, Space Storage, Sky Demon
Level Jump5, Vertical Jump2



Douchetron
Monster
Capricorn
53
60
Dark Behemoth










Dogsandcatsand
Female
Virgo
41
73
Samurai
Punch Art
MA Save
Doublehand
Move+1

Javelin

Leather Helmet
Crystal Mail
Salty Rage

Asura, Bizen Boat, Murasame, Kiyomori, Muramasa
Earth Slash, Secret Fist, Revive

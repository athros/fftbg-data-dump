Player: !White
Team: White Team
Palettes: White/Blue



Gooblz
Male
Capricorn
78
51
Wizard
Item
Earplug
Equip Polearm
Waterbreathing

Octagon Rod

Feather Hat
Adaman Vest
Spike Shoes

Fire 3, Fire 4, Bolt 3, Empower
Hi-Potion, Phoenix Down



Grininda
Male
Taurus
66
72
Monk
Yin Yang Magic
Counter Flood
Sicken
Move+1



Golden Hairpin
Brigandine
Bracer

Pummel, Wave Fist, Purification
Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



Ribbiks
Female
Capricorn
46
45
Geomancer
Black Magic
Absorb Used MP
Equip Polearm
Swim

Ivory Rod
Escutcheon
Ribbon
Secret Clothes
N-Kai Armlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Fire 4, Bolt 2, Ice 2, Death



NovaKnight21
Female
Virgo
75
57
Squire
Draw Out
Abandon
Short Status
Move+2

Hunting Bow
Bronze Shield
Holy Miter
Carabini Mail
Feather Boots

Accumulate, Dash, Cheer Up, Fury, Wish, Ultima
Bizen Boat

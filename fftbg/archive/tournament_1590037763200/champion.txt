Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



HaateXIII
Female
Pisces
40
75
Monk
Summon Magic
Critical Quick
Beastmaster
Move+1



Flash Hat
Power Sleeve
Feather Boots

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive
Moogle, Golem, Leviathan, Silf



OperatorTheory
Female
Cancer
58
57
Ninja
Basic Skill
Parry
Magic Defense UP
Fly

Flail
Flame Whip
Triangle Hat
Brigandine
Cherche

Knife, Axe, Dictionary
Dash, Heal, Yell, Cheer Up



Mardokttv
Male
Cancer
40
52
Wizard
Talk Skill
MP Restore
Equip Polearm
Retreat

Persia

Black Hood
Wizard Outfit
Germinas Boots

Fire 2, Bolt, Bolt 2, Bolt 4, Ice 3, Death
Invitation, Persuade, Threaten, Preach, Solution, Refute



Reltz
Female
Libra
66
69
Lancer
White Magic
Speed Save
Equip Axe
Jump+1

Scorpion Tail
Genji Shield
Circlet
Black Robe
108 Gems

Level Jump3, Vertical Jump5
Cure 4, Raise, Protect, Esuna, Holy, Magic Barrier

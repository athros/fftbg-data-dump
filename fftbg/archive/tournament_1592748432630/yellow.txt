Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galkife
Female
Cancer
77
43
Priest
Math Skill
Absorb Used MP
Equip Polearm
Move+1

Cypress Rod

Holy Miter
Linen Robe
Dracula Mantle

Raise, Reraise, Protect, Shell, Wall, Esuna
CT, Height, 5, 4, 3



Oobs56
Female
Leo
60
81
Samurai
Summon Magic
Caution
Maintenance
Teleport

Koutetsu Knife

Diamond Helmet
Linen Cuirass
Defense Ring

Koutetsu, Heaven's Cloud
Titan, Carbunkle, Leviathan



PoroTact
Male
Cancer
61
73
Monk
Battle Skill
Counter Tackle
Secret Hunt
Move+2



Feather Hat
Chain Vest
Leather Mantle

Spin Fist, Earth Slash, Secret Fist, Purification
Armor Break, Shield Break, Weapon Break, Speed Break, Justice Sword



Alaquane
Female
Leo
42
56
Geomancer
Steal
Hamedo
Equip Polearm
Move-MP Up

Octagon Rod
Escutcheon
Feather Hat
Leather Outfit
Germinas Boots

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind
Gil Taking, Steal Helmet, Steal Weapon, Steal Status

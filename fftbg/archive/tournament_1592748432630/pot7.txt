Final Bets: green - 15 bets for 7,644G (41.9%, x1.39); purple - 10 bets for 10,620G (58.1%, x0.72)

green bets:
prince_rogers_nelson_: 1,593G (20.8%, 1,593G)
eudes89: 1,280G (16.7%, 1,280G)
Alaquane: 1,192G (15.6%, 1,192G)
BirbBrainsBot: 1,000G (13.1%, 58,394G)
skillomono: 1,000G (13.1%, 1,821G)
Nizaha: 501G (6.6%, 18,922G)
conradharlock: 260G (3.4%, 260G)
IphoneDarkness: 150G (2.0%, 7,688G)
Arbitae: 100G (1.3%, 1,016G)
Quadh0nk: 100G (1.3%, 3,310G)
E_Ballard: 100G (1.3%, 18,809G)
mrfripps: 100G (1.3%, 5,485G)
datadrivenbot: 100G (1.3%, 45,756G)
maakur_: 100G (1.3%, 71,915G)
getthemoneyz: 68G (0.9%, 1,021,989G)

purple bets:
Lydian_C: 5,600G (52.7%, 175,215G)
mirapoix: 1,500G (14.1%, 1,500G)
cam_ATS: 1,000G (9.4%, 22,150G)
Breakdown777: 675G (6.4%, 1,351G)
superdevon1: 600G (5.6%, 3,418G)
Rytor: 500G (4.7%, 5,612G)
DustBirdEX: 345G (3.2%, 4,847G)
ar_tactic: 200G (1.9%, 56,886G)
nhammen: 100G (0.9%, 3,996G)
nekojin: 100G (0.9%, 1,434G)

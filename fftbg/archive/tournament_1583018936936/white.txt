Player: !White
Team: White Team
Palettes: White/Blue



Fenixcrest
Female
Scorpio
77
76
Mediator
Elemental
Counter Flood
Equip Polearm
Teleport

Mythril Spear

Holy Miter
Earth Clothes
Feather Boots

Invitation, Persuade, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Lordminsc
Male
Gemini
63
69
Time Mage
Item
Mana Shield
Martial Arts
Move-MP Up



Black Hood
Silk Robe
Magic Ring

Haste, Haste 2, Slow 2, Stop, Immobilize, Reflect, Demi, Demi 2
Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Aldrammech
Male
Sagittarius
74
55
Knight
White Magic
Counter Flood
Sicken
Move-MP Up

Nagrarock
Genji Shield
Circlet
Light Robe
N-Kai Armlet

Magic Break, Speed Break, Power Break, Mind Break, Dark Sword
Raise, Protect, Shell, Shell 2, Esuna



Powergems
Male
Aries
75
75
Knight
Talk Skill
Absorb Used MP
Defense UP
Retreat

Battle Axe
Escutcheon
Diamond Helmet
Diamond Armor
Defense Armlet

Head Break, Armor Break, Shield Break, Magic Break, Power Break
Threaten, Solution, Death Sentence, Negotiate, Refute, Rehabilitate

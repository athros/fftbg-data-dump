Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Brokenknight201
Male
Aquarius
77
55
Mime

Counter Magic
Secret Hunt
Waterbreathing



Golden Hairpin
Power Sleeve
Angel Ring

Mimic




ScurvyMitch
Female
Taurus
49
75
Chemist
Yin Yang Magic
Abandon
Doublehand
Swim

Star Bag

Golden Hairpin
Brigandine
Sprint Shoes

Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Blind, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Paralyze



GrandmasterFrankerZ
Female
Capricorn
44
63
Chemist
Jump
Arrow Guard
Sicken
Waterbreathing

Blast Gun

Black Hood
Secret Clothes
Defense Armlet

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Level Jump4, Vertical Jump7



ArchKnightX
Male
Virgo
76
50
Mime

Sunken State
Martial Arts
Move-MP Up


Gold Shield
Leather Hat
Wizard Outfit
Bracer

Mimic


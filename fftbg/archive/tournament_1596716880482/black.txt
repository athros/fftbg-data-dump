Player: !Black
Team: Black Team
Palettes: Black/Red



Dinin991
Female
Cancer
73
63
Oracle
Summon Magic
Faith Save
Long Status
Teleport

Battle Folio

Green Beret
Linen Robe
Defense Ring

Blind, Poison, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Petrify
Moogle, Ramuh, Carbunkle, Lich



Thyrandaal
Male
Gemini
50
46
Knight
Charge
Auto Potion
Equip Sword
Swim

Ragnarok
Round Shield
Mythril Helmet
Gold Armor
Small Mantle

Armor Break, Shield Break, Speed Break, Mind Break, Stasis Sword, Dark Sword
Charge+3, Charge+5



Powergems
Female
Sagittarius
71
65
Knight
Dance
HP Restore
Doublehand
Levitate

Rune Blade

Platinum Helmet
Silk Robe
Rubber Shoes

Armor Break, Weapon Break, Magic Break, Justice Sword, Dark Sword
Witch Hunt, Wiznaibus, Polka Polka, Disillusion



EmmaEnema
Female
Capricorn
59
49
Thief
Punch Art
Sunken State
Doublehand
Jump+2

Main Gauche

Leather Hat
Earth Clothes
Leather Mantle

Steal Shield
Spin Fist, Pummel, Secret Fist, Purification, Seal Evil

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Grininda
Female
Pisces
71
47
Samurai
Steal
Regenerator
Sicken
Lava Walking

Javelin

Bronze Helmet
Mythril Armor
Angel Ring

Muramasa
Steal Helmet



Miku Shikhu
Female
Cancer
66
68
Thief
Draw Out
Brave Save
Concentrate
Fly

Assassin Dagger

Triangle Hat
Wizard Outfit
Magic Ring

Steal Armor, Steal Accessory
Heaven's Cloud



Gamesage53
Male
Scorpio
45
67
Knight
Elemental
Abandon
Long Status
Jump+3

Giant Axe
Platinum Shield
Diamond Helmet
Platinum Armor
Small Mantle

Shield Break, Speed Break, Mind Break, Explosion Sword
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



LordMaxoss
Female
Taurus
53
75
Mediator
Dance
HP Restore
Halve MP
Retreat

Blaze Gun

Thief Hat
Wizard Outfit
Angel Ring

Preach, Solution, Death Sentence, Insult, Negotiate, Refute
Polka Polka

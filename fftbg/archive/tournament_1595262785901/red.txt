Player: !Red
Team: Red Team
Palettes: Red/Brown



AND4H
Female
Pisces
61
68
Samurai
Basic Skill
Dragon Spirit
Martial Arts
Lava Walking



Crystal Helmet
Mythril Armor
Red Shoes

Koutetsu, Heaven's Cloud, Kiyomori
Dash, Throw Stone, Heal, Yell



Laserman1000
Female
Pisces
63
62
Mediator
Dance
Regenerator
Equip Shield
Lava Walking

Papyrus Codex
Gold Shield
Triangle Hat
Wizard Outfit
Bracer

Persuade, Praise, Threaten, Preach, Solution, Refute
Wiznaibus, Polka Polka, Disillusion, Nameless Dance



Cryptopsy70
Female
Cancer
56
61
Oracle
Time Magic
Parry
Equip Knife
Ignore Terrain

Dragon Rod

Black Hood
Clothes
Wizard Mantle

Life Drain, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep
Haste, Slow, Slow 2, Reflect, Demi 2, Stabilize Time, Meteor



TasisSai
Male
Scorpio
54
73
Ninja
Steal
Critical Quick
Equip Axe
Swim

Morning Star
Oak Staff
Red Hood
Earth Clothes
Small Mantle

Shuriken
Gil Taking, Steal Armor, Steal Accessory

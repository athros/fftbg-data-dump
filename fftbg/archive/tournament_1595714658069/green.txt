Player: !Green
Team: Green Team
Palettes: Green/White



Lydian C
Female
Sagittarius
78
50
Oracle
Math Skill
Sunken State
Magic Attack UP
Retreat

Musk Rod

Black Hood
Black Costume
Small Mantle

Blind, Poison, Doubt Faith, Silence Song, Dispel Magic, Petrify
Height, Prime Number, 5, 3



Legitimized
Male
Taurus
51
62
Thief
Item
Sunken State
Long Status
Swim

Orichalcum

Twist Headband
Mythril Vest
Defense Ring

Steal Heart, Steal Weapon, Steal Accessory, Steal Status
X-Potion, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



GrayGhostGaming
Male
Leo
61
51
Thief
Talk Skill
Absorb Used MP
Halve MP
Move-HP Up

Blind Knife

Black Hood
Judo Outfit
Genji Gauntlet

Gil Taking, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim
Invitation, Persuade, Threaten



Douchetron
Male
Sagittarius
68
77
Lancer
Black Magic
Meatbone Slash
Defend
Move+3

Ivory Rod
Escutcheon
Bronze Helmet
Bronze Armor
Chantage

Level Jump2, Vertical Jump4
Fire 2, Bolt, Bolt 3, Bolt 4, Ice 2, Ice 4

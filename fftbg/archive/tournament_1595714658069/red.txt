Player: !Red
Team: Red Team
Palettes: Red/Brown



PoroTact
Female
Aries
48
53
Ninja
Elemental
Meatbone Slash
Secret Hunt
Lava Walking

Kunai
Orichalcum
Green Beret
Mystic Vest
Sprint Shoes

Shuriken, Spear
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind



Omegasuspekt
Male
Cancer
55
77
Lancer
Punch Art
Blade Grasp
Short Status
Fly

Mythril Spear
Ice Shield
Gold Helmet
Maximillian
N-Kai Armlet

Level Jump8, Vertical Jump7
Secret Fist, Purification, Chakra, Revive



Xoomwaffle
Female
Pisces
58
66
Squire
Throw
Speed Save
Equip Gun
Jump+2

Ramia Harp
Ice Shield
Feather Hat
Chain Vest
Small Mantle

Heal, Yell, Cheer Up, Fury, Scream
Shuriken, Stick



Seaweed B
Female
Cancer
41
75
Squire
Jump
Sunken State
Equip Knife
Jump+1

Short Edge
Genji Shield
Barbuta
Diamond Armor
Defense Armlet

Accumulate, Heal, Tickle, Fury, Wish
Level Jump4, Vertical Jump8

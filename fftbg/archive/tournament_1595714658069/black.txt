Player: !Black
Team: Black Team
Palettes: Black/Red



SkylerBunny
Female
Aries
51
53
Dancer
Black Magic
Arrow Guard
Magic Attack UP
Teleport

Cute Bag

Flash Hat
Mystic Vest
Spike Shoes

Witch Hunt, Wiznaibus, Slow Dance
Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Empower



Dogsandcatsand
Male
Serpentarius
56
74
Mime

MA Save
Magic Attack UP
Ignore Terrain



Green Beret
Linen Robe
Battle Boots

Mimic




Chaerie
Male
Libra
44
74
Bard
Summon Magic
Absorb Used MP
Dual Wield
Fly

Ultimus Bow

Green Beret
Linen Cuirass
Small Mantle

Cheer Song, Hydra Pit
Moogle, Shiva, Ramuh, Titan, Salamander, Fairy



Run With Stone GUNs
Male
Gemini
74
65
Chemist
Throw
Mana Shield
Equip Shield
Move+1

Cultist Dagger
Crystal Shield
Ribbon
Mystic Vest
Spike Shoes

Potion, Hi-Potion, Ether, Echo Grass, Remedy, Phoenix Down
Shuriken

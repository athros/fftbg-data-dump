Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



RuneS 77
Female
Capricorn
78
46
Geomancer
Talk Skill
Absorb Used MP
Sicken
Move+1

Blood Sword
Round Shield
Green Beret
Robe of Lords
Defense Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Invitation, Solution, Negotiate, Rehabilitate



JonnyCue
Male
Virgo
49
76
Bard
White Magic
Counter Magic
Defend
Lava Walking

Ramia Harp

Thief Hat
Plate Mail
Battle Boots

Angel Song, Life Song, Battle Song, Magic Song, Nameless Song, Sky Demon, Hydra Pit
Cure 2, Raise, Protect, Shell 2, Esuna



Verxl
Female
Capricorn
48
49
Lancer
Item
Parry
Throw Item
Move+3

Holy Lance
Escutcheon
Platinum Helmet
Platinum Armor
Bracer

Level Jump5, Vertical Jump6
Potion, Hi-Potion, Ether, Hi-Ether, Holy Water, Phoenix Down



Liamsulli
Male
Taurus
55
76
Archer
Elemental
Mana Shield
Equip Gun
Fly

Romanda Gun
Aegis Shield
Twist Headband
Mythril Vest
Diamond Armlet

Charge+1, Charge+7, Charge+10
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball

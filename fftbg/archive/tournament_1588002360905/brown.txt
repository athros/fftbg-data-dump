Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Roofiepops
Male
Aries
61
71
Ninja
Draw Out
Parry
Equip Armor
Lava Walking

Kunai
Flail
Circlet
Genji Armor
Germinas Boots

Bomb, Knife, Hammer, Staff, Spear
Koutetsu, Bizen Boat, Heaven's Cloud



Lanshaft
Male
Serpentarius
60
44
Knight
White Magic
Catch
Short Charge
Levitate

Platinum Sword
Escutcheon
Crystal Helmet
Bronze Armor
Angel Ring

Shield Break, Weapon Break, Magic Break, Dark Sword
Cure, Cure 3, Raise, Shell, Wall



IndecisiveNinja
Monster
Gemini
67
60
Black Chocobo










Nifboy
Male
Capricorn
41
55
Archer
Steal
PA Save
Short Charge
Move+1

Glacier Gun
Bronze Shield
Leather Hat
Brigandine
Power Wrist

Charge+1, Charge+2, Charge+10, Charge+20
Gil Taking, Steal Accessory, Arm Aim

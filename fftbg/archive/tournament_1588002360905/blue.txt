Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zeroroute
Female
Aries
63
54
Thief
Time Magic
Sunken State
Defend
Waterbreathing

Cute Bag

Triangle Hat
Mystic Vest
Defense Ring

Steal Heart, Steal Armor, Steal Status
Immobilize



DrAntiSocial
Female
Pisces
60
52
Knight
Draw Out
Counter
Dual Wield
Move+3

Giant Axe
Iron Sword
Platinum Helmet
Gold Armor
Cursed Ring

Head Break, Power Break, Mind Break
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



Nuka1120
Male
Scorpio
80
52
Bard
Jump
Mana Shield
Sicken
Move-MP Up

Ramia Harp

Feather Hat
Earth Clothes
Wizard Mantle

Battle Song, Last Song
Level Jump8, Vertical Jump2



Deenglow
Female
Virgo
75
55
Calculator
White Magic
Meatbone Slash
Beastmaster
Retreat

Dragon Rod

Feather Hat
Linen Robe
Salty Rage

CT, Height, Prime Number, 5, 3
Raise, Reraise, Shell, Shell 2, Wall, Esuna

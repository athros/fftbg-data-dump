Player: !Black
Team: Black Team
Palettes: Black/Red



Peanutvendor
Male
Leo
70
70
Ninja
Draw Out
Regenerator
Equip Bow
Lava Walking

Poison Bow
Snipe Bow
Triangle Hat
Rubber Costume
Feather Boots

Shuriken, Bomb, Hammer
Bizen Boat, Heaven's Cloud



Amorial
Male
Sagittarius
75
49
Chemist
Battle Skill
Dragon Spirit
Equip Bow
Jump+2

Ultimus Bow

Triangle Hat
Power Sleeve
Magic Ring

Potion, Antidote, Eye Drop, Echo Grass, Soft, Holy Water
Shield Break, Speed Break, Power Break, Mind Break



Drusiform
Male
Aquarius
73
46
Calculator
Byblos
Hamedo
Equip Polearm
Move-MP Up

Persia

Cross Helmet
Linen Cuirass
Diamond Armlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken



Thyrandaal
Male
Libra
70
48
Geomancer
Sing
Faith Save
Defend
Move+2

Heaven's Cloud
Bronze Shield
Barette
Adaman Vest
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Angel Song, Magic Song

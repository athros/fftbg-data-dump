Final Bets: purple - 15 bets for 8,602G (54.6%, x0.83); brown - 11 bets for 7,151G (45.4%, x1.20)

purple bets:
reinoe: 2,000G (23.3%, 16,124G)
superdevon1: 1,107G (12.9%, 1,107G)
Forkmore: 1,000G (11.6%, 31,714G)
RampagingRobot: 912G (10.6%, 13,912G)
placidphoenix: 811G (9.4%, 811G)
ANFz: 500G (5.8%, 11,010G)
Rurk: 500G (5.8%, 14,061G)
E_Ballard: 444G (5.2%, 444G)
killth3kid: 444G (5.2%, 132,552G)
Aldrammech: 384G (4.5%, 384G)
mpghappiness: 100G (1.2%, 1,594G)
maakur_: 100G (1.2%, 75,536G)
Miku_Shikhu: 100G (1.2%, 22,075G)
datadrivenbot: 100G (1.2%, 52,448G)
holy_explosion: 100G (1.2%, 195G)

brown bets:
alekzanndrr: 2,324G (32.5%, 2,324G)
Evewho: 1,000G (14.0%, 16,519G)
BirbBrainsBot: 1,000G (14.0%, 120,779G)
Firesheath: 684G (9.6%, 684G)
prince_rogers_nelson_: 600G (8.4%, 600G)
Nizaha: 504G (7.0%, 6,829G)
getthemoneyz: 280G (3.9%, 1,154,794G)
Mathlexis: 208G (2.9%, 208G)
gorgewall: 201G (2.8%, 4,220G)
CosmicTactician: 200G (2.8%, 52,151G)
Neuros91: 150G (2.1%, 2,775G)

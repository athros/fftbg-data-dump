Player: !Green
Team: Green Team
Palettes: Green/White



Pie108
Female
Aquarius
72
51
Squire
Steal
Dragon Spirit
Sicken
Jump+1

Bow Gun
Escutcheon
Bronze Helmet
Plate Mail
Red Shoes

Dash, Tickle, Cheer Up, Wish
Steal Armor, Steal Shield, Steal Status, Leg Aim



Treapvort
Female
Pisces
74
62
Wizard
Time Magic
Caution
Short Charge
Fly

Main Gauche

Black Hood
Light Robe
Magic Gauntlet

Fire 3, Bolt 3, Ice 3, Empower
Haste, Haste 2, Slow, Slow 2, Immobilize, Stabilize Time



PlatinumPlume
Female
Pisces
61
60
Monk
Summon Magic
Brave Up
Equip Polearm
Waterwalking

Iron Fan

Twist Headband
Earth Clothes
Power Wrist

Pummel, Purification, Revive
Moogle, Shiva, Titan, Golem, Carbunkle, Salamander, Silf, Fairy



Orangejuliuscaesar
Male
Libra
69
47
Chemist
Time Magic
MP Restore
Beastmaster
Move+3

Romanda Gun

Twist Headband
Black Costume
Reflect Ring

Potion, X-Potion, Ether, Eye Drop, Phoenix Down
Haste, Immobilize, Quick, Stabilize Time

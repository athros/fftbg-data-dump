Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mikeyoffbeat
Female
Taurus
59
57
Summoner
Item
Damage Split
Equip Polearm
Move+2

Whale Whisker

Headgear
Linen Robe
Wizard Mantle

Ifrit, Odin, Leviathan, Lich
Hi-Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Lyner87
Monster
Libra
55
65
Revenant










BadBlanket
Female
Taurus
42
68
Squire
Battle Skill
Dragon Spirit
Sicken
Waterwalking

Rune Blade
Flame Shield
Crystal Helmet
Mystic Vest
Sprint Shoes

Heal, Yell, Fury
Armor Break, Shield Break, Magic Break, Power Break



RunicMagus
Male
Scorpio
51
47
Time Mage
White Magic
MA Save
Equip Axe
Move+1

Giant Axe

Feather Hat
Brigandine
Magic Ring

Haste, Slow, Stop, Immobilize, Float, Reflect, Demi, Stabilize Time
Cure 3, Cure 4, Raise, Reraise, Shell, Shell 2, Wall, Esuna

Player: !Red
Team: Red Team
Palettes: Red/Brown



DudeMonkey77
Female
Pisces
64
66
Knight
Steal
PA Save
Magic Defense UP
Waterbreathing

Nagrarock
Aegis Shield
Platinum Helmet
Leather Armor
Germinas Boots

Shield Break, Magic Break, Speed Break, Mind Break
Steal Helmet, Steal Armor, Steal Weapon, Arm Aim



Kellios11
Female
Aries
64
43
Calculator
Time Magic
Sunken State
Halve MP
Retreat

Octagon Rod

Flash Hat
Rubber Costume
Wizard Mantle

Height, Prime Number, 5, 4
Haste, Haste 2, Reflect, Demi, Stabilize Time



OneHundredFists
Female
Cancer
58
48
Wizard
Draw Out
Earplug
Equip Armor
Ignore Height

Rod

Mythril Helmet
Maximillian
Rubber Shoes

Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4, Flare
Murasame, Kiyomori, Chirijiraden



Akrae
Male
Virgo
80
51
Chemist
White Magic
Counter Tackle
Sicken
Lava Walking

Cute Bag

Leather Hat
Adaman Vest
Defense Armlet

Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Raise, Shell, Shell 2, Wall, Esuna

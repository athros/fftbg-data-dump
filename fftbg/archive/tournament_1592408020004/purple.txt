Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Female
Gemini
54
50
Samurai
Punch Art
Hamedo
Doublehand
Jump+3

Bizen Boat

Bronze Helmet
Diamond Armor
Spike Shoes

Asura, Bizen Boat, Heaven's Cloud
Pummel, Wave Fist, Purification, Revive



ShintaroNayaka
Male
Cancer
70
81
Knight
Jump
Meatbone Slash
Equip Polearm
Move+1

Ivory Rod
Aegis Shield
Gold Helmet
Chameleon Robe
Leather Mantle

Shield Break, Magic Break, Power Break, Night Sword
Level Jump8, Vertical Jump2



O Heyno
Male
Scorpio
54
70
Chemist
Charge
Mana Shield
Attack UP
Move-MP Up

Cute Bag

Triangle Hat
Adaman Vest
Defense Ring

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down
Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+20



Laserman1000
Male
Virgo
49
58
Lancer
Item
Catch
Throw Item
Move+3

Mythril Spear
Genji Shield
Platinum Helmet
Genji Armor
Diamond Armlet

Level Jump8, Vertical Jump3
X-Potion, Hi-Ether, Phoenix Down

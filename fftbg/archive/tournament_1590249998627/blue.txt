Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lastly
Female
Leo
69
74
Priest
Black Magic
MA Save
Equip Gun
Move+1

Mythril Gun

Golden Hairpin
Clothes
Setiemson

Cure 2, Cure 3, Raise 2, Esuna
Fire 3, Bolt, Bolt 4, Ice 3



Toka222
Male
Sagittarius
63
44
Monk
Throw
Counter
Equip Knife
Levitate

Sasuke Knife

Triangle Hat
Chain Vest
108 Gems

Spin Fist, Wave Fist, Earth Slash, Purification
Shuriken, Bomb



ANFz
Female
Taurus
80
43
Mediator
Summon Magic
Counter Flood
Equip Polearm
Swim

Gokuu Rod

Golden Hairpin
Brigandine
Dracula Mantle

Threaten, Preach, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Moogle, Shiva, Ramuh, Salamander, Silf, Fairy



MrFlabyo
Female
Aquarius
47
60
Ninja
Time Magic
Counter
Equip Axe
Teleport

Star Bag
Morning Star
Twist Headband
Judo Outfit
Small Mantle

Bomb, Knife, Sword, Dictionary
Haste, Slow 2, Reflect, Stabilize Time, Meteor

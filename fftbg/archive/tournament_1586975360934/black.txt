Player: !Black
Team: Black Team
Palettes: Black/Red



Lythe Caraker
Male
Aries
59
80
Bard
Summon Magic
Abandon
Attack UP
Swim

Yoichi Bow

Green Beret
Plate Mail
Wizard Mantle

Life Song
Moogle, Shiva, Titan, Golem, Salamander, Cyclops



Shakarak
Female
Virgo
49
74
Summoner
Black Magic
Speed Save
Short Status
Move+2

Poison Rod

Green Beret
Silk Robe
Battle Boots

Moogle, Carbunkle, Silf, Fairy, Cyclops
Fire 2, Fire 3, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 4, Flare



DavenIII
Female
Cancer
39
70
Summoner
Throw
MA Save
Short Charge
Swim

Faith Rod

Feather Hat
Brigandine
Magic Gauntlet

Ramuh, Odin, Leviathan
Shuriken



Rico Flex
Male
Gemini
60
47
Lancer
Basic Skill
Counter
Doublehand
Jump+2

Spear

Circlet
Maximillian
Rubber Shoes

Level Jump8, Vertical Jump7
Dash, Heal, Yell

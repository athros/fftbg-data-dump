Player: !Red
Team: Red Team
Palettes: Red/Brown



Shalloween
Male
Aquarius
65
56
Squire
Black Magic
Counter Flood
Equip Knife
Move+2

Zorlin Shape
Platinum Shield
Holy Miter
Diamond Armor
Sprint Shoes

Accumulate, Yell
Ice 3, Ice 4



NovaKnight21
Male
Libra
71
49
Ninja
Time Magic
Parry
Halve MP
Move+3

Mage Masher
Spell Edge
Cachusha
Wizard Outfit
Magic Gauntlet

Shuriken, Bomb
Haste, Immobilize, Reflect, Quick, Demi 2



DrAntiSocial
Female
Taurus
43
69
Time Mage
Talk Skill
Catch
Monster Talk
Jump+2

Bestiary

Golden Hairpin
Black Robe
Dracula Mantle

Haste 2, Slow, Reflect
Invitation, Death Sentence, Negotiate, Mimic Daravon, Refute



IsPRIZElive
Male
Sagittarius
56
42
Calculator
Black Magic
Hamedo
Equip Knife
Levitate

Mage Masher

Ribbon
Linen Robe
Diamond Armlet

CT, Height, Prime Number, 5, 4, 3
Fire 3, Fire 4, Bolt, Bolt 2, Ice 4

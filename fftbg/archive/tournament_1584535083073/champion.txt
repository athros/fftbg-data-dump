Player: !zChamp
Team: Champion Team
Palettes: Green/White



Volgrathemoose
Male
Aquarius
41
57
Mime

MA Save
Attack UP
Jump+3



Green Beret
Earth Clothes
Cursed Ring

Mimic




Waterwatereverywhere
Female
Serpentarius
39
52
Dancer
Time Magic
Regenerator
Equip Shield
Ignore Height

Persia
Flame Shield
Red Hood
Adaman Vest
Magic Gauntlet

Wiznaibus, Disillusion, Void Storage
Haste, Haste 2, Slow 2, Float, Quick



HaplessOne
Male
Virgo
68
58
Knight
Punch Art
Counter Flood
Attack UP
Move-HP Up

Slasher
Mythril Shield
Genji Helmet
Chain Mail
Defense Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Mind Break, Stasis Sword
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive



J2DaBibbles
Female
Pisces
51
80
Mediator
Battle Skill
Auto Potion
Equip Armor
Retreat

Blast Gun

Leather Hat
Mythril Armor
Diamond Armlet

Threaten, Preach, Death Sentence, Negotiate, Refute, Rehabilitate
Weapon Break, Mind Break

Player: !Red
Team: Red Team
Palettes: Red/Brown



Digitalsocrates
Female
Serpentarius
41
68
Oracle
Elemental
Critical Quick
Dual Wield
Jump+1

Cypress Rod
Cypress Rod
Flash Hat
Black Costume
Vanish Mantle

Blind, Poison, Spell Absorb, Pray Faith, Zombie, Dispel Magic, Paralyze, Sleep
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Dogsandcatsand
Female
Libra
63
41
Geomancer
Talk Skill
Sunken State
Doublehand
Teleport 2

Kiyomori

Twist Headband
Earth Clothes
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Lava Ball
Invitation, Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate



Ivellioz
Female
Leo
55
41
Calculator
Lucavi Skill
Sunken State
Halve MP
Teleport

Battle Folio

Gold Helmet
White Robe
Power Wrist

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



TheChainNerd
Female
Capricorn
51
74
Dancer
Basic Skill
Critical Quick
Defense UP
Move+2

Cute Bag

Red Hood
Adaman Vest
N-Kai Armlet

Disillusion
Heal, Yell, Fury, Wish

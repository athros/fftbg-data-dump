Final Bets: green - 13 bets for 5,411G (60.2%, x0.66); yellow - 5 bets for 3,577G (39.8%, x1.51)

green bets:
NicoSavoy: 2,244G (41.5%, 56,108G)
ColetteMSLP: 1,000G (18.5%, 25,581G)
AllInBot: 621G (11.5%, 621G)
shadricklul: 400G (7.4%, 1,142G)
datadrivenbot: 200G (3.7%, 57,429G)
BirbBrainsBot: 200G (3.7%, 200G)
maddrave09: 124G (2.3%, 250G)
roofiepops: 123G (2.3%, 18,963G)
PantherIscariot: 111G (2.1%, 4,492G)
MinBetBot: 100G (1.8%, 22,366G)
ko2q: 100G (1.8%, 5,380G)
Drusiform: 100G (1.8%, 22,833G)
getthemoneyz: 88G (1.6%, 1,596,323G)

yellow bets:
DeathTaxesAndAnime: 1,793G (50.1%, 1,793G)
Lythe_Caraker: 1,000G (28.0%, 161,130G)
WhiteTigress: 500G (14.0%, 5,256G)
NovaKnight21: 184G (5.1%, 5,760G)
Firesheath: 100G (2.8%, 29,968G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Helpimabug
Male
Cancer
42
80
Thief
Item
Regenerator
Equip Axe
Move+2

Slasher

Flash Hat
Clothes
Wizard Mantle

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim
Potion, Eye Drop, Remedy, Phoenix Down



VolgraTheMoose
Female
Aries
73
76
Samurai
Item
Regenerator
Magic Attack UP
Move+3

Murasame

Barbuta
Diamond Armor
Vanish Mantle

Asura, Bizen Boat, Kiyomori, Muramasa
Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



O Heyno
Female
Pisces
72
55
Mediator
Time Magic
Earplug
Concentrate
Teleport

Stone Gun

Green Beret
Adaman Vest
Elf Mantle

Preach, Insult, Refute
Haste, Haste 2, Slow, Stop, Reflect, Demi 2, Stabilize Time



ZombiFrancis
Female
Gemini
76
57
Summoner
Draw Out
Catch
Dual Wield
Jump+1

White Staff
Ice Rod
Black Hood
White Robe
Sprint Shoes

Moogle, Ramuh, Carbunkle, Odin, Silf, Fairy
Bizen Boat

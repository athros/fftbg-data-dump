Player: !Red
Team: Red Team
Palettes: Red/Brown



HuffFlex
Female
Cancer
50
79
Oracle
Throw
Faith Save
Dual Wield
Jump+1

Cypress Rod
Ivory Rod
Feather Hat
Earth Clothes
Cursed Ring

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Foxbird, Confusion Song
Bomb, Staff



Chronolynx42
Female
Cancer
50
79
Oracle
Throw
Faith Save
Dual Wield
Jump+1

Cypress Rod
Ivory Rod
Feather Hat
Earth Clothes
Cursed Ring

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Foxbird, Confusion Song
Bomb, Staff



Thyrandaal
Female
Cancer
50
79
Oracle
Throw
Faith Save
Dual Wield
Jump+1

Cypress Rod
Ivory Rod
Feather Hat
Earth Clothes
Cursed Ring

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Foxbird, Confusion Song
Bomb, Staff



Lanshaft
Female
Cancer
50
79
Oracle
Throw
Faith Save
Dual Wield
Jump+1

Cypress Rod
Ivory Rod
Feather Hat
Earth Clothes
Cursed Ring

Blind, Poison, Spell Absorb, Doubt Faith, Silence Song, Foxbird, Confusion Song
Bomb, Staff

Final Bets: white - 11 bets for 10,799G (59.0%, x0.69); black - 7 bets for 7,496G (41.0%, x1.44)

white bets:
Mesmaster: 3,000G (27.8%, 49,010G)
prince_rogers_nelson_: 2,275G (21.1%, 2,275G)
roofiepops: 1,157G (10.7%, 1,157G)
EnemyController: 1,111G (10.3%, 1,352,619G)
Magicandy: 1,000G (9.3%, 24,927G)
Ring_Wyrm: 500G (4.6%, 500G)
twelfthrootoftwo: 500G (4.6%, 10,023G)
Evewho: 500G (4.6%, 10,000G)
superdevon1: 356G (3.3%, 356G)
datadrivenbot: 200G (1.9%, 51,733G)
alekzanndrr: 200G (1.9%, 2,493G)

black bets:
OneHundredFists: 4,000G (53.4%, 30,881G)
NicoSavoy: 1,000G (13.3%, 110,575G)
getthemoneyz: 888G (11.8%, 1,169,638G)
readdesert: 711G (9.5%, 711G)
BirbBrainsBot: 533G (7.1%, 171,787G)
placidphoenix: 264G (3.5%, 264G)
fluffskull: 100G (1.3%, 4,938G)

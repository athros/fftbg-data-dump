Player: !Red
Team: Red Team
Palettes: Red/Brown



Thyrandaal
Monster
Virgo
41
61
Minotaur










Nekojin
Female
Capricorn
58
71
Oracle
White Magic
Brave Save
Equip Knife
Move-HP Up

Flame Rod

Triangle Hat
Adaman Vest
Angel Ring

Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep
Raise, Reraise, Protect 2, Shell 2, Wall, Esuna



Zachara
Female
Scorpio
48
51
Summoner
Battle Skill
Arrow Guard
Short Charge
Teleport

Oak Staff

Golden Hairpin
Wizard Robe
Rubber Shoes

Ramuh, Titan, Golem, Carbunkle, Fairy
Armor Break, Magic Break, Power Break, Mind Break, Justice Sword



Moocaotao
Male
Scorpio
53
69
Thief
Summon Magic
Dragon Spirit
Equip Bow
Ignore Height

Hunting Bow

Triangle Hat
Black Costume
Sprint Shoes

Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory
Ifrit, Golem, Carbunkle, Bahamut, Salamander

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Roofiepops
Male
Virgo
56
64
Geomancer
Charge
Counter Flood
Equip Gun
Move+3

Romanda Gun
Diamond Shield
Feather Hat
Adaman Vest
Cursed Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+2, Charge+3



DudeMonkey77
Male
Cancer
57
70
Mediator
Elemental
Absorb Used MP
Martial Arts
Fly

Star Bag

Green Beret
Light Robe
Defense Armlet

Threaten, Preach, Mimic Daravon
Pitfall, Static Shock, Quicksand, Blizzard, Lava Ball



EnemyController
Male
Taurus
75
41
Monk
Battle Skill
Absorb Used MP
Secret Hunt
Fly



Green Beret
Clothes
Defense Armlet

Purification, Revive, Seal Evil
Head Break, Armor Break, Shield Break, Magic Break, Mind Break, Justice Sword



Evdoggity
Male
Pisces
40
64
Bard
Talk Skill
HP Restore
Attack UP
Swim

Fairy Harp

Flash Hat
Crystal Mail
Reflect Ring

Angel Song, Cheer Song, Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Persuade, Praise, Solution, Death Sentence, Insult, Refute

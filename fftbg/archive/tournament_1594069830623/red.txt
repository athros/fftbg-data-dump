Player: !Red
Team: Red Team
Palettes: Red/Brown



Hasterious
Male
Sagittarius
62
67
Lancer
Sing
Auto Potion
Concentrate
Jump+2

Partisan
Aegis Shield
Gold Helmet
Wizard Robe
Bracer

Level Jump8, Vertical Jump6
Angel Song, Life Song, Battle Song, Diamond Blade, Sky Demon



Oobs56
Male
Leo
54
73
Thief
Jump
Catch
Equip Shield
Move-MP Up

Main Gauche
Aegis Shield
Feather Hat
Black Costume
Diamond Armlet

Steal Armor, Steal Weapon, Steal Accessory, Arm Aim
Level Jump5, Vertical Jump6



0v3rr8d
Female
Scorpio
60
65
Oracle
Dance
Regenerator
Equip Gun
Jump+1

Battle Folio

Holy Miter
Clothes
Diamond Armlet

Poison, Life Drain, Zombie, Blind Rage
Disillusion, Dragon Pit



Upidstupid
Female
Sagittarius
77
44
Wizard
White Magic
Absorb Used MP
Equip Knife
Ignore Terrain

Sasuke Knife

Green Beret
Chameleon Robe
Jade Armlet

Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 2, Ice 3, Ice 4
Cure 3, Raise, Regen, Protect, Protect 2, Esuna

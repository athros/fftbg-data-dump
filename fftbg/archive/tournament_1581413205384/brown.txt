Player: !Brown
Team: Brown Team
Palettes: Brown/Green



JarMustard
Male
Pisces
42
61
Lancer
Black Magic
Regenerator
Short Charge
Jump+1

Spear
Escutcheon
Iron Helmet
Platinum Armor
Elf Mantle

Level Jump8, Vertical Jump6
Fire 3, Fire 4



Maeveen
Female
Aquarius
43
68
Dancer
Elemental
Counter
Halve MP
Jump+3

Hydra Bag

Barette
Adaman Vest
Angel Ring

Witch Hunt, Obsidian Blade, Void Storage
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Nickelbank
Male
Leo
75
70
Summoner
White Magic
Earplug
Equip Shield
Levitate

Wizard Staff
Mythril Shield
Leather Hat
Linen Robe
Magic Gauntlet

Moogle, Ramuh, Titan, Carbunkle, Salamander, Silf, Fairy, Lich
Cure, Cure 3, Raise, Raise 2, Regen, Protect, Protect 2, Esuna



Hyvi
Male
Sagittarius
56
54
Calculator
Bio
Hamedo
Secret Hunt
Fly

Gokuu Rod

Leather Hat
Silk Robe
Sprint Shoes

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis

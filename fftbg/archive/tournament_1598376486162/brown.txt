Player: !Brown
Team: Brown Team
Palettes: Brown/Green



1twistedpuppy
Monster
Leo
80
40
Red Panther










Actual JP
Female
Taurus
46
72
Ninja
Punch Art
Faith Save
Magic Defense UP
Move+1

Hidden Knife
Short Edge
Feather Hat
Adaman Vest
Battle Boots

Wand
Wave Fist, Purification, Chakra, Revive, Seal Evil



Run With Stone GUNs
Female
Aries
71
49
Time Mage
Draw Out
Parry
Doublehand
Lava Walking

Wizard Staff

Golden Hairpin
Wizard Robe
Angel Ring

Haste, Slow, Slow 2, Stop, Immobilize, Float
Heaven's Cloud, Kiyomori



Gut922
Female
Scorpio
61
42
Oracle
Elemental
Abandon
Magic Attack UP
Ignore Terrain

Ivory Rod

Black Hood
Leather Outfit
Battle Boots

Blind, Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze, Petrify
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Flacococo
Monster
Gemini
49
51
Wyvern










Evdoggity
Female
Aries
70
54
Time Mage
Draw Out
Brave Save
Dual Wield
Move-HP Up

Oak Staff
White Staff
Green Beret
Power Sleeve
Germinas Boots

Haste 2, Slow, Slow 2, Reflect, Demi 2, Meteor
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Redmage4evah
Monster
Gemini
79
61
Sekhret










StealthModeLocke
Female
Virgo
65
62
Geomancer
Time Magic
Parry
Attack UP
Move+1

Broad Sword
Flame Shield
Thief Hat
Earth Clothes
Wizard Mantle

Pitfall, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Haste, Slow, Reflect, Demi, Meteor

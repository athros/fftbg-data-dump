Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Anethum
Male
Taurus
62
60
Squire
Item
Counter
Equip Bow
Move+2

Silver Bow

Mythril Helmet
Genji Armor
Diamond Armlet

Heal, Tickle, Cheer Up
Potion, X-Potion, Hi-Ether, Antidote, Holy Water



Raruni
Female
Aries
61
58
Mediator
Item
MA Save
Equip Gun
Fly

Papyrus Codex

Triangle Hat
Mythril Vest
108 Gems

Persuade, Threaten, Insult, Negotiate
Potion, Hi-Potion, Ether, Echo Grass, Remedy



Dynasti
Female
Scorpio
58
65
Geomancer
Time Magic
Auto Potion
Short Charge
Jump+3

Slasher
Gold Shield
Black Hood
Black Robe
Angel Ring

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Haste, Immobilize, Float, Reflect, Demi, Demi 2



RRazza
Female
Taurus
51
63
Dancer
Draw Out
Mana Shield
Maintenance
Waterwalking

Ryozan Silk

Triangle Hat
Mystic Vest
Genji Gauntlet

Witch Hunt, Wiznaibus, Disillusion, Void Storage
Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji

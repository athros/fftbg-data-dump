Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jaritras
Male
Cancer
66
69
Geomancer
Basic Skill
Critical Quick
Equip Armor
Fly

Bizen Boat
Platinum Shield
Leather Hat
Linen Cuirass
Battle Boots

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Accumulate, Throw Stone, Heal, Wish



LeoNightFury
Monster
Aries
66
56
Apanda










Carledo
Male
Virgo
64
62
Calculator
Yin Yang Magic
Counter Magic
Secret Hunt
Teleport

Battle Folio

Twist Headband
Chain Vest
108 Gems

Height, 4
Spell Absorb, Life Drain, Foxbird, Dispel Magic



Outer Monologue
Female
Scorpio
44
67
Geomancer
Yin Yang Magic
Abandon
Equip Bow
Waterbreathing

Windslash Bow

Thief Hat
Chain Vest
Battle Boots

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Blind Rage, Dispel Magic, Sleep

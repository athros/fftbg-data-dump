Final Bets: red - 7 bets for 3,486G (24.1%, x3.16); white - 10 bets for 11,005G (75.9%, x0.32)

red bets:
BirbBrainsBot: 1,000G (28.7%, 62,500G)
Mesmaster: 1,000G (28.7%, 271,164G)
RaIshtar: 500G (14.3%, 2,658G)
Grandlanzer: 416G (11.9%, 416G)
getthemoneyz: 270G (7.7%, 1,019,496G)
letdowncity: 200G (5.7%, 4,081G)
E_Ballard: 100G (2.9%, 17,940G)

white bets:
toka222: 6,900G (62.7%, 117,230G)
VolgraTheMoose: 1,001G (9.1%, 3,865G)
superdevon1: 600G (5.5%, 2,431G)
ShintaroNayaka: 516G (4.7%, 516G)
Shalloween: 500G (4.5%, 63,666G)
nifboy: 488G (4.4%, 488G)
joewcarson: 400G (3.6%, 1,844G)
KonzeraLive: 300G (2.7%, 1,187G)
Magicandy: 200G (1.8%, 3,486G)
datadrivenbot: 100G (0.9%, 45,828G)

Player: !White
Team: White Team
Palettes: White/Blue



VolgraTheMoose
Male
Aquarius
55
52
Chemist
Yin Yang Magic
Abandon
Magic Defense UP
Move+1

Stone Gun

Red Hood
Wizard Outfit
Cursed Ring

Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Phoenix Down
Poison, Life Drain, Pray Faith, Confusion Song, Dispel Magic



Mesmaster
Male
Aquarius
76
68
Knight
Elemental
Regenerator
Equip Bow
Teleport 2

Windslash Bow
Crystal Shield
Crystal Helmet
Light Robe
Reflect Ring

Head Break, Armor Break, Weapon Break, Speed Break, Justice Sword, Explosion Sword
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Gusty Wind



0v3rr8d
Male
Pisces
49
75
Oracle
Basic Skill
Parry
Short Charge
Move-MP Up

Gokuu Rod

Leather Hat
Linen Robe
108 Gems

Spell Absorb, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Petrify, Dark Holy
Throw Stone, Heal, Scream



Serperemagus
Female
Taurus
67
50
Wizard
Talk Skill
MA Save
Monster Talk
Move-MP Up

Main Gauche

Feather Hat
Mythril Vest
Red Shoes

Fire 2, Fire 3, Bolt 2, Ice 2, Empower, Death
Invitation, Death Sentence, Insult, Mimic Daravon, Refute

Player: !Green
Team: Green Team
Palettes: Green/White



Theseawolf1
Female
Aries
71
64
Oracle
Battle Skill
Critical Quick
Equip Gun
Jump+3

Blast Gun

Twist Headband
Chain Vest
Power Wrist

Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Petrify
Head Break, Armor Break, Power Break, Mind Break, Surging Sword, Explosion Sword



Vorap
Female
Gemini
59
72
Squire
Summon Magic
Parry
Equip Bow
Waterbreathing

Cross Bow
Venetian Shield
Leather Hat
Secret Clothes
Elf Mantle

Accumulate, Yell
Moogle, Titan, Golem, Salamander, Fairy, Lich, Zodiac



Jakeduhhsnake
Male
Gemini
72
54
Lancer
Charge
Distribute
Sicken
Ignore Height

Partisan
Bronze Shield
Genji Helmet
Linen Cuirass
Power Wrist

Level Jump4, Vertical Jump7
Charge+1, Charge+3, Charge+4, Charge+7, Charge+10



Benticore
Male
Capricorn
73
46
Knight
Basic Skill
Meatbone Slash
Martial Arts
Jump+1


Round Shield
Gold Helmet
Chain Mail
Feather Boots

Armor Break, Magic Break, Mind Break, Justice Sword
Accumulate, Heal

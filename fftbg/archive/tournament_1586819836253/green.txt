Player: !Green
Team: Green Team
Palettes: Green/White



Aldrammech
Female
Gemini
51
62
Chemist
Steal
PA Save
Long Status
Teleport

Blaze Gun

Red Hood
Earth Clothes
Dracula Mantle

Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Steal Weapon, Arm Aim



ApplesauceBoss
Female
Cancer
76
70
Geomancer
Steal
HP Restore
Equip Axe
Waterbreathing

Slasher
Round Shield
Red Hood
Judo Outfit
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Steal Armor, Leg Aim



Meta Five
Female
Taurus
57
68
Dancer
Draw Out
Dragon Spirit
Martial Arts
Retreat



Thief Hat
Black Robe
Chantage

Slow Dance, Polka Polka, Disillusion, Obsidian Blade
Bizen Boat, Heaven's Cloud



Old Overholt
Monster
Taurus
49
72
Dark Behemoth








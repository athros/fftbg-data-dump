Player: !White
Team: White Team
Palettes: White/Blue



Pplvee1
Male
Pisces
56
58
Knight
Charge
Auto Potion
Magic Attack UP
Teleport

Defender
Gold Shield
Leather Helmet
Carabini Mail
Power Wrist

Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword
Charge+2, Charge+5, Charge+7, Charge+20



Mirapoix
Male
Leo
74
46
Lancer
Charge
Critical Quick
Short Charge
Move+1

Partisan
Diamond Shield
Iron Helmet
Plate Mail
Sprint Shoes

Level Jump5, Vertical Jump3
Charge+1, Charge+20



Reddwind
Male
Libra
62
53
Knight
Black Magic
Hamedo
Magic Attack UP
Lava Walking

Platinum Sword
Buckler
Circlet
Linen Robe
Diamond Armlet

Head Break, Armor Break, Shield Break, Power Break, Night Sword
Fire 2, Fire 3, Bolt 4, Ice 4, Flare



Galkife
Female
Pisces
68
62
Knight
Throw
MA Save
Short Status
Waterwalking

Ice Brand
Ice Shield
Bronze Helmet
Light Robe
Reflect Ring

Shield Break, Speed Break
Shuriken, Bomb, Knife

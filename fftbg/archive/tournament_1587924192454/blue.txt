Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lawnboxer
Male
Gemini
48
76
Calculator
Time Magic
Counter
Magic Defense UP
Fly

Papyrus Codex

Holy Miter
Judo Outfit
Elf Mantle

CT, Prime Number, 5, 4, 3
Haste 2, Slow 2, Stop, Demi, Stabilize Time



Aldrammech
Female
Aries
64
47
Wizard
Elemental
Mana Shield
Doublehand
Ignore Height

Rod

Golden Hairpin
Chain Vest
Leather Mantle

Fire, Fire 4, Bolt
Pitfall, Water Ball, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



DudeMonkey77
Female
Aquarius
69
72
Lancer
Item
Faith Up
Equip Polearm
Teleport 2

Mythril Spear
Escutcheon
Cross Helmet
White Robe
Defense Armlet

Level Jump3, Vertical Jump2
Ether, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down



Ewan E
Female
Gemini
61
40
Knight
Draw Out
Damage Split
Equip Polearm
Move+2

Musk Rod
Flame Shield
Platinum Helmet
Leather Armor
108 Gems

Weapon Break, Stasis Sword, Justice Sword, Surging Sword
Bizen Boat, Heaven's Cloud

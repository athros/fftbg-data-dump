Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ninjasteepler
Monster
Sagittarius
45
52
Steel Giant










Maakur
Male
Taurus
64
43
Chemist
Charge
Catch
Short Status
Fly

Hydra Bag

Flash Hat
Adaman Vest
Sprint Shoes

Potion, Hi-Potion, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Charge+2, Charge+5, Charge+7, Charge+20



Zetchryn
Male
Libra
49
59
Ninja
Black Magic
Brave Up
Equip Polearm
Swim

Gokuu Rod
Spear
Headgear
Chain Vest
Spike Shoes

Staff
Fire, Fire 2, Ice 3



Yawgmothsupp
Female
Pisces
59
48
Oracle
Punch Art
Counter
Equip Armor
Move+1

Battle Folio

Barbuta
Maximillian
Dracula Mantle

Spell Absorb, Silence Song, Foxbird, Confusion Song, Paralyze, Sleep
Pummel, Purification, Revive

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Kellios11
Female
Aquarius
61
40
Mediator
Time Magic
Earplug
Dual Wield
Jump+2

Romanda Gun
Mythril Gun
Twist Headband
Chain Vest
Jade Armlet

Invitation, Persuade, Threaten, Preach, Death Sentence, Negotiate
Haste, Slow 2, Float, Stabilize Time



Forkmore
Male
Scorpio
38
67
Knight
Steal
Speed Save
Secret Hunt
Levitate

Battle Axe
Gold Shield
Diamond Helmet
White Robe
Germinas Boots

Shield Break, Mind Break
Gil Taking, Steal Armor, Arm Aim, Leg Aim



SephDarkheart
Female
Cancer
74
79
Wizard
Dance
Damage Split
Concentrate
Move+2

Dragon Rod

Golden Hairpin
Brigandine
Leather Mantle

Fire 3, Bolt 2, Bolt 3, Bolt 4, Ice, Death
Witch Hunt, Polka Polka, Disillusion



Zagorsek
Male
Aquarius
78
76
Oracle
Draw Out
Parry
Equip Sword
Waterwalking

Sleep Sword

Flash Hat
Robe of Lords
Leather Mantle

Blind, Life Drain, Silence Song
Koutetsu, Bizen Boat, Muramasa

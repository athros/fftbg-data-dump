Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Hypstyr
Male
Taurus
45
80
Lancer
Steal
Critical Quick
Equip Bow
Retreat

Ice Bow

Barbuta
Bronze Armor
Battle Boots

Level Jump5, Vertical Jump7
Steal Accessory, Arm Aim



Itsadud
Female
Gemini
73
60
Monk
Time Magic
Arrow Guard
Equip Knife
Lava Walking

Poison Rod

Golden Hairpin
Clothes
Feather Mantle

Pummel, Earth Slash, Purification, Chakra
Haste, Immobilize, Reflect



Killth3kid
Female
Libra
43
79
Ninja
Punch Art
MP Restore
Equip Bow
Move-MP Up

Snipe Bow
Bow Gun
Thief Hat
Brigandine
Elf Mantle

Shuriken, Bomb
Spin Fist, Wave Fist, Secret Fist, Purification, Seal Evil



DLJuggernaut
Female
Libra
65
77
Samurai
Elemental
Damage Split
Short Status
Move+3

Asura Knife

Genji Helmet
Linen Cuirass
Germinas Boots

Koutetsu, Kiyomori
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind

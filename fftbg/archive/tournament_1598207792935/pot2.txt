Final Bets: green - 19 bets for 9,219G (44.8%, x1.23); yellow - 14 bets for 11,347G (55.2%, x0.81)

green bets:
bruubarg: 1,059G (11.5%, 1,059G)
VolgraTheMoose: 1,002G (10.9%, 62,292G)
BirbBrainsBot: 1,000G (10.8%, 166,962G)
reinoe: 1,000G (10.8%, 43,561G)
killth3kid: 796G (8.6%, 49,316G)
Lanshaft: 620G (6.7%, 21,833G)
goth_Applebees_hostess: 604G (6.6%, 604G)
Thyrandaal: 554G (6.0%, 554G)
Sairentozon7: 545G (5.9%, 1,945G)
dogsandcatsand: 425G (4.6%, 425G)
fattunaking: 424G (4.6%, 424G)
Raixelol: 250G (2.7%, 9,580G)
getthemoneyz: 238G (2.6%, 1,733,338G)
ravingsockmonkey: 234G (2.5%, 234G)
roqqqpsi: 144G (1.6%, 761G)
Lydian_C: 123G (1.3%, 3,440G)
Aldrammech: 100G (1.1%, 26,072G)
cloudycube: 100G (1.1%, 1,053G)
SephDarkheart: 1G (0.0%, 162,616G)

yellow bets:
latebit: 4,148G (36.6%, 8,134G)
sinnyil2: 2,182G (19.2%, 4,364G)
DavenIII: 1,000G (8.8%, 79,764G)
E_Ballard: 728G (6.4%, 49,909G)
DustBirdEX: 640G (5.6%, 640G)
Laserman1000: 577G (5.1%, 21,477G)
technominari: 500G (4.4%, 83,738G)
maximumcrit: 500G (4.4%, 4,496G)
Lord_Gwarth: 372G (3.3%, 372G)
Ayntlerz: 200G (1.8%, 662G)
datadrivenbot: 200G (1.8%, 67,358G)
AllInBot: 100G (0.9%, 100G)
RunicMagus: 100G (0.9%, 6,677G)
nifboy: 100G (0.9%, 3,441G)

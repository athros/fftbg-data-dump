Player: !Black
Team: Black Team
Palettes: Black/Red



Mtueni
Male
Aquarius
71
57
Chemist
Elemental
Catch
Defend
Move+3

Assassin Dagger

Feather Hat
Chain Vest
Magic Gauntlet

Potion, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



Tougou
Male
Gemini
79
56
Archer
Draw Out
Sunken State
Dual Wield
Move+2

Romanda Gun
Glacier Gun
Platinum Helmet
Wizard Outfit
N-Kai Armlet

Charge+2
Murasame, Heaven's Cloud, Muramasa



Pandasforsale
Female
Pisces
38
42
Archer
Summon Magic
Parry
Dual Wield
Jump+2

Hunting Bow
Cross Bow
Cross Helmet
Wizard Outfit
Genji Gauntlet

Charge+4, Charge+5, Charge+7, Charge+10
Golem, Carbunkle, Bahamut, Odin, Salamander



Bryan792
Male
Leo
79
75
Ninja
Black Magic
Parry
Equip Gun
Levitate

Bestiary
Madlemgen
Leather Hat
Chain Vest
Reflect Ring

Shuriken, Knife, Spear
Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Death, Flare

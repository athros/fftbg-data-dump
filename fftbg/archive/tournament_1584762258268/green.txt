Player: !Green
Team: Green Team
Palettes: Green/White



Omegasuspekt
Male
Libra
48
55
Ninja
Steal
Speed Save
Sicken
Waterwalking

Ninja Edge
Assassin Dagger
Twist Headband
Leather Outfit
Genji Gauntlet

Knife, Staff, Ninja Sword
Steal Shield, Steal Weapon, Steal Accessory



SolarisFall
Female
Sagittarius
75
81
Oracle
Item
Arrow Guard
Concentrate
Move+2

Musk Rod

Leather Hat
Chameleon Robe
Red Shoes

Poison, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze
Potion, Hi-Potion, Antidote



Spartan Paladin
Male
Aries
73
44
Knight
Item
Meatbone Slash
Dual Wield
Jump+2

Slasher
Ancient Sword
Iron Helmet
Crystal Mail
Battle Boots

Armor Break, Shield Break, Power Break, Mind Break, Dark Sword, Explosion Sword
Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Uyjhm
Female
Taurus
69
76
Samurai
Steal
Brave Up
Equip Axe
Waterwalking

Gold Staff

Platinum Helmet
Silk Robe
Magic Gauntlet

Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Status, Arm Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



RughSontos
Female
Gemini
69
51
Geomancer
Jump
MA Save
Beastmaster
Move-MP Up

Giant Axe
Buckler
Barette
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump4, Vertical Jump6



Toksu
Male
Leo
71
46
Monk
Battle Skill
Mana Shield
Defend
Move-HP Up



Twist Headband
Mystic Vest
Power Wrist

Spin Fist, Pummel, Purification, Chakra, Revive
Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword



Bad1dea
Female
Leo
80
60
Knight
Item
Regenerator
Equip Axe
Move-MP Up

Flame Whip
Aegis Shield
Genji Helmet
Black Robe
Genji Gauntlet

Armor Break, Shield Break, Magic Break, Speed Break, Stasis Sword, Surging Sword
Hi-Potion, Maiden's Kiss, Soft, Phoenix Down



Chuckolator
Female
Gemini
55
44
Priest
Throw
Counter
Short Charge
Teleport

Sage Staff

Flash Hat
Silk Robe
Red Shoes

Cure 4, Reraise, Regen, Protect 2, Shell, Shell 2, Esuna
Staff, Ninja Sword

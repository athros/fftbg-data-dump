Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



RyuTsuno
Male
Sagittarius
38
71
Time Mage
Basic Skill
Parry
Magic Defense UP
Waterbreathing

Healing Staff

Triangle Hat
White Robe
Angel Ring

Haste, Haste 2, Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time
Accumulate, Heal, Tickle, Yell, Scream



Typicalfanboy
Female
Gemini
59
57
Calculator
Animal Skill
MP Restore
Short Charge
Jump+3

Battle Folio
Mythril Shield
Holy Miter
Brigandine
Reflect Ring

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



Bazel2020
Male
Leo
58
49
Mediator
Elemental
Distribute
Beastmaster
Levitate

Papyrus Codex

Golden Hairpin
Black Costume
Diamond Armlet

Persuade, Praise, Solution, Death Sentence, Mimic Daravon, Refute
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Gooseyourself
Male
Serpentarius
50
56
Monk
Basic Skill
Counter Flood
Beastmaster
Move-HP Up



Triangle Hat
Chain Vest
Rubber Shoes

Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil
Accumulate, Dash, Heal, Scream, Ultima

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Littlep2000
Female
Scorpio
60
73
Calculator
Yin Yang Magic
Regenerator
Sicken
Fly

Thunder Rod

Thief Hat
Mythril Vest
Cherche

CT, 5
Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



TheBlobZ
Male
Cancer
43
55
Thief
Time Magic
HP Restore
Secret Hunt
Move-HP Up

Mythril Knife

Flash Hat
Chain Vest
Magic Ring

Steal Heart, Steal Weapon, Steal Status
Haste, Haste 2, Slow, Immobilize, Stabilize Time, Meteor



Winterharte
Male
Aries
81
58
Oracle
Battle Skill
Absorb Used MP
Magic Defense UP
Move-MP Up

Battle Bamboo

Leather Hat
Mythril Vest
Power Wrist

Blind, Pray Faith, Zombie, Paralyze
Armor Break, Magic Break, Mind Break



Teknakon
Male
Capricorn
76
37
Wizard
Basic Skill
Auto Potion
Attack UP
Retreat

Wizard Rod

Black Hood
Judo Outfit
Diamond Armlet

Fire, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Flare
Dash, Throw Stone, Heal, Yell, Cheer Up

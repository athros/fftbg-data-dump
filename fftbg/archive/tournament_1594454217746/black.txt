Player: !Black
Team: Black Team
Palettes: Black/Red



LAGBOT30000
Female
Libra
48
50
Archer
Battle Skill
Counter Tackle
Short Charge
Move+2

Snipe Bow
Buckler
Mythril Helmet
Mystic Vest
Power Wrist

Charge+1, Charge+2, Charge+7, Charge+20
Head Break, Shield Break



MilesDong
Male
Gemini
68
65
Monk
Basic Skill
Earplug
Attack UP
Ignore Height



Headgear
Mythril Vest
Leather Mantle

Spin Fist, Pummel, Purification, Revive
Throw Stone, Cheer Up, Wish



Lythe Caraker
Female
Leo
52
55
Wizard
Steal
Brave Save
Maintenance
Move-MP Up

Ice Rod

Flash Hat
Mythril Vest
Feather Mantle

Fire 2, Fire 4, Bolt, Bolt 2, Ice, Ice 2, Frog, Flare
Steal Heart, Steal Accessory, Leg Aim



Powermhero
Female
Scorpio
75
79
Priest
Yin Yang Magic
Counter
Doublehand
Move-HP Up

Healing Staff

Triangle Hat
Black Costume
Power Wrist

Cure 2, Cure 3, Cure 4, Reraise, Protect
Blind, Life Drain, Doubt Faith, Silence Song, Foxbird, Petrify

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Baconbacon1207
Male
Aries
75
54
Squire
Black Magic
Critical Quick
Beastmaster
Move+1

Mythril Knife
Diamond Shield
Red Hood
Plate Mail
Sprint Shoes

Dash, Heal, Cheer Up
Fire, Fire 4, Bolt, Bolt 3, Ice, Ice 4



Byrdturbo
Male
Gemini
42
52
Calculator
Time Magic
PA Save
Equip Gun
Levitate

Bestiary

Flash Hat
Light Robe
Diamond Armlet

CT, Height, Prime Number, 5, 4, 3
Reflect, Quick, Demi, Stabilize Time, Meteor



ALY327
Female
Virgo
58
42
Squire
Summon Magic
Mana Shield
Short Charge
Waterwalking

Flail
Platinum Shield
Flash Hat
Diamond Armor
N-Kai Armlet

Accumulate, Dash, Cheer Up, Fury
Moogle, Ramuh, Titan, Carbunkle, Bahamut, Odin, Silf, Lich, Cyclops



Killth3kid
Female
Aquarius
56
54
Lancer
Charge
HP Restore
Dual Wield
Lava Walking

Gungnir
Partisan
Barbuta
Platinum Armor
N-Kai Armlet

Level Jump8, Vertical Jump8
Charge+1, Charge+2, Charge+3

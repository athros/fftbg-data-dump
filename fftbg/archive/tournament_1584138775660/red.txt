Player: !Red
Team: Red Team
Palettes: Red/Brown



Tougou
Male
Leo
59
59
Geomancer
Battle Skill
Damage Split
Secret Hunt
Lava Walking

Kikuichimoji
Gold Shield
Triangle Hat
Adaman Vest
Diamond Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Stasis Sword



Sinnyil2
Male
Cancer
67
44
Archer
Time Magic
Regenerator
Doublehand
Move+2

Mythril Gun

Red Hood
Chain Vest
Germinas Boots

Charge+1, Charge+2, Charge+3, Charge+4
Haste, Float, Reflect, Demi 2, Stabilize Time, Meteor



MyFakeLife
Female
Aries
54
58
Dancer
Black Magic
PA Save
Equip Shield
Fly

Ryozan Silk
Buckler
Feather Hat
Judo Outfit
Elf Mantle

Wiznaibus, Disillusion, Last Dance, Obsidian Blade, Nether Demon, Dragon Pit
Bolt 3, Ice 4



BadBlanket
Female
Gemini
66
46
Squire
Draw Out
Faith Up
Magic Defense UP
Move+3

Giant Axe
Crystal Shield
Flash Hat
Adaman Vest
Sprint Shoes

Accumulate, Dash, Heal, Tickle, Yell, Wish
Koutetsu, Murasame, Masamune

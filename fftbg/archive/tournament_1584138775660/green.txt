Player: !Green
Team: Green Team
Palettes: Green/White



Saucy Wings
Female
Aries
81
76
Chemist
Basic Skill
Critical Quick
Long Status
Levitate

Blaze Gun

Golden Hairpin
Power Sleeve
Cherche

Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Accumulate, Heal, Tickle, Yell



Darkp Ramza
Female
Scorpio
73
77
Archer
Battle Skill
Counter Flood
Doublehand
Move+2

Romanda Gun

Golden Hairpin
Chain Vest
Germinas Boots

Charge+2, Charge+7
Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword, Surging Sword



GrindFox1
Male
Gemini
38
56
Squire
Talk Skill
Damage Split
Beastmaster
Move-HP Up

Bow Gun
Aegis Shield
Cross Helmet
Clothes
Genji Gauntlet

Accumulate, Dash, Throw Stone, Heal
Solution, Death Sentence, Insult, Refute, Rehabilitate



HaateXIII
Male
Capricorn
79
68
Squire
Sing
Damage Split
Equip Axe
Levitate

Battle Axe
Aegis Shield
Mythril Helmet
Gold Armor
Small Mantle

Tickle, Yell
Angel Song, Battle Song, Last Song

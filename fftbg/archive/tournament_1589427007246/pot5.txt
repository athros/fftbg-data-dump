Final Bets: red - 15 bets for 9,873G (18.6%, x4.37); green - 29 bets for 43,160G (81.4%, x0.23)

red bets:
gooseyourself: 1,964G (19.9%, 1,964G)
E_Ballard: 1,894G (19.2%, 1,894G)
thethorndog1: 1,023G (10.4%, 1,023G)
Lawndough: 1,000G (10.1%, 1,854G)
Arcblazer23: 844G (8.5%, 1,689G)
ColetteMSLP: 800G (8.1%, 46,573G)
killth3kid: 540G (5.5%, 6,239G)
dogsandcatsand: 500G (5.1%, 12,233G)
fenaen: 388G (3.9%, 388G)
twelfthrootoftwo: 300G (3.0%, 9,747G)
otakutaylor: 200G (2.0%, 2,252G)
KasugaiRoastedPeas: 200G (2.0%, 4,421G)
ANFz: 100G (1.0%, 3,009G)
ACSpree: 100G (1.0%, 6,069G)
aeonicvector: 20G (0.2%, 1,509G)

green bets:
NicoSavoy: 15,057G (34.9%, 30,115G)
sinnyil2: 10,121G (23.4%, 20,242G)
Mesmaster: 2,000G (4.6%, 58,769G)
Dymntd: 2,000G (4.6%, 6,945G)
leakimiko: 1,500G (3.5%, 54,505G)
HaplessOne: 1,500G (3.5%, 33,698G)
Baron_von_Scrub: 1,111G (2.6%, 3,086G)
ko2q: 1,098G (2.5%, 1,098G)
ungabunga_bot: 1,000G (2.3%, 486,748G)
BirbBrainsBot: 1,000G (2.3%, 30,964G)
Lionhermit: 1,000G (2.3%, 48,055G)
mpghappiness: 852G (2.0%, 852G)
RunicMagus: 800G (1.9%, 42,291G)
Mtueni: 700G (1.6%, 700G)
Coyote81_G: 648G (1.5%, 648G)
Oobs56: 404G (0.9%, 5,156G)
CorpusCav: 401G (0.9%, 7,401G)
SerumD: 350G (0.8%, 4,146G)
genericco: 284G (0.7%, 284G)
metagameface: 260G (0.6%, 260G)
Ungaiii: 236G (0.5%, 236G)
LeoNightFury: 200G (0.5%, 4,105G)
TimeJannies: 100G (0.2%, 6,222G)
Evewho: 100G (0.2%, 17,909G)
datadrivenbot: 100G (0.2%, 15,932G)
HawkSick: 100G (0.2%, 4,755G)
victoriolue: 100G (0.2%, 5,147G)
getthemoneyz: 88G (0.2%, 673,674G)
DrAntiSocial: 50G (0.1%, 15,178G)

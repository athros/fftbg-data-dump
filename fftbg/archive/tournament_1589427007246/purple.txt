Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ungaiii
Male
Capricorn
56
48
Archer
Sing
Counter
Beastmaster
Waterbreathing

Poison Bow
Round Shield
Barbuta
Mythril Vest
Sprint Shoes

Charge+3
Last Song, Hydra Pit



Sinnyil2
Female
Taurus
78
63
Samurai
White Magic
Damage Split
Short Status
Move+2

Bizen Boat

Crystal Helmet
Mythril Armor
Defense Ring

Bizen Boat, Kikuichimoji
Raise, Regen, Shell, Wall, Esuna



Maakur
Male
Aquarius
59
47
Lancer
Steal
Critical Quick
Defense UP
Move+2

Cypress Rod
Hero Shield
Mythril Helmet
Genji Armor
Jade Armlet

Level Jump2, Vertical Jump2
Gil Taking, Steal Heart, Steal Shield, Steal Accessory



Metagameface
Female
Leo
58
77
Knight
Item
MP Restore
Equip Polearm
Waterwalking

Persia
Mythril Shield
Circlet
Linen Cuirass
Sprint Shoes

Armor Break, Weapon Break, Magic Break
Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down

Player: !White
Team: White Team
Palettes: White/Blue



SerumD
Female
Taurus
58
42
Chemist
Charge
Parry
Short Charge
Jump+2

Cute Bag

Leather Hat
Chain Vest
Germinas Boots

Potion, Ether, Soft, Holy Water, Remedy, Phoenix Down
Charge+3



Nosablake
Male
Virgo
66
44
Archer
Talk Skill
Damage Split
Equip Polearm
Teleport

Spear
Gold Shield
Black Hood
Chain Vest
Jade Armlet

Charge+3, Charge+20
Invitation, Persuade, Negotiate, Mimic Daravon, Refute, Rehabilitate



Alaylle
Monster
Virgo
44
64
Squidraken










MrUbiq
Male
Gemini
53
72
Squire
Summon Magic
Counter Flood
Dual Wield
Levitate

Coral Sword
Assassin Dagger
Black Hood
Secret Clothes
Defense Armlet

Heal, Tickle, Wish
Shiva, Ifrit, Carbunkle, Salamander, Lich, Cyclops

Player: !Red
Team: Red Team
Palettes: Red/Brown



P0jothen00b
Female
Aquarius
73
64
Oracle
Battle Skill
Abandon
Short Status
Levitate

Ivory Rod

Green Beret
Black Robe
Reflect Ring

Spell Absorb, Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic
Shield Break, Magic Break, Power Break, Justice Sword, Night Sword



HASTERIOUS
Female
Gemini
77
71
Geomancer
Item
Counter Flood
Throw Item
Jump+1

Battle Axe
Buckler
Triangle Hat
Clothes
Feather Mantle

Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Gusty Wind
Hi-Potion, Ether, Hi-Ether, Remedy, Phoenix Down



Vorap
Male
Libra
80
68
Chemist
Time Magic
Counter Magic
Martial Arts
Move+1



Golden Hairpin
Adaman Vest
Wizard Mantle

Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Slow, Stop, Quick, Demi



Benticore
Female
Pisces
63
67
Lancer
Time Magic
Brave Save
Equip Sword
Jump+1

Defender

Cross Helmet
Chain Mail
Sprint Shoes

Level Jump4, Vertical Jump8
Haste 2, Slow, Slow 2, Stop, Float, Quick, Demi, Demi 2, Stabilize Time, Meteor

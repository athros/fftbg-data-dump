Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LionelParks
Female
Capricorn
67
72
Lancer
Throw
PA Save
Martial Arts
Swim


Aegis Shield
Gold Helmet
Bronze Armor
Germinas Boots

Level Jump2, Vertical Jump8
Shuriken, Stick, Dictionary



CorpusCav
Male
Leo
41
63
Bard
Basic Skill
Counter Magic
Equip Gun
Lava Walking

Madlemgen

Triangle Hat
Platinum Armor
Sprint Shoes

Angel Song, Diamond Blade, Space Storage
Dash, Heal, Yell, Cheer Up



ItsZaes
Male
Serpentarius
73
80
Squire
Summon Magic
PA Save
Short Status
Waterwalking

Assassin Dagger
Buckler
Golden Hairpin
Earth Clothes
Magic Gauntlet

Accumulate, Throw Stone, Heal, Tickle, Wish
Moogle, Ifrit, Lich



Lali Lulelo
Male
Serpentarius
47
69
Mediator
Summon Magic
Meatbone Slash
Equip Shield
Lava Walking

Blast Gun
Buckler
Thief Hat
Robe of Lords
Bracer

Persuade, Praise, Insult, Negotiate, Refute
Moogle, Shiva, Carbunkle, Silf, Zodiac

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LeepingJJ
Female
Leo
69
46
Oracle
Charge
Counter Tackle
Maintenance
Lava Walking

Gokuu Rod

Golden Hairpin
Black Robe
Rubber Shoes

Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Paralyze
Charge+1, Charge+5, Charge+7, Charge+20



CapnChaos12
Male
Aquarius
78
74
Ninja
Elemental
PA Save
Martial Arts
Fly



Holy Miter
Mystic Vest
Cursed Ring

Shuriken
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp



TheChainNerd
Female
Scorpio
46
72
Geomancer
Yin Yang Magic
Catch
Equip Knife
Jump+2

Main Gauche
Buckler
Holy Miter
Chain Vest
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Gusty Wind, Lava Ball
Spell Absorb, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy



Daveb
Male
Cancer
46
65
Ninja
Black Magic
Damage Split
Equip Sword
Waterwalking

Ragnarok
Rune Blade
Red Hood
Power Sleeve
108 Gems

Knife
Bolt 3, Frog

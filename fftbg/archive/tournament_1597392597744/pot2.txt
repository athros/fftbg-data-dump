Final Bets: green - 13 bets for 4,568G (61.9%, x0.61); yellow - 5 bets for 2,808G (38.1%, x1.63)

green bets:
BirbBrainsBot: 1,000G (21.9%, 89,173G)
getthemoneyz: 1,000G (21.9%, 1,621,774G)
Mesmaster: 698G (15.3%, 698G)
fattunaking: 396G (8.7%, 396G)
BoneMiser: 300G (6.6%, 300G)
Wooplestein: 260G (5.7%, 260G)
AllInBot: 200G (4.4%, 200G)
datadrivenbot: 200G (4.4%, 67,408G)
ProteinBiscuit: 200G (4.4%, 2,685G)
uncharreted: 112G (2.5%, 112G)
gorgewall: 101G (2.2%, 3,329G)
koeeh: 100G (2.2%, 430G)
Chuckolator: 1G (0.0%, 3,367G)

yellow bets:
SkylerBunny: 1,463G (52.1%, 2,869G)
blorpy_: 921G (32.8%, 23,045G)
ArrenJevleth: 200G (7.1%, 5,226G)
Lydian_C: 124G (4.4%, 2,646G)
ko2q: 100G (3.6%, 2,819G)

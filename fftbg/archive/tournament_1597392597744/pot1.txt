Final Bets: red - 7 bets for 5,546G (61.3%, x0.63); blue - 10 bets for 3,504G (38.7%, x1.58)

red bets:
SkylerBunny: 1,758G (31.7%, 1,758G)
Lydian_C: 1,200G (21.6%, 1,888G)
BirbBrainsBot: 1,000G (18.0%, 88,541G)
reddwind_: 1,000G (18.0%, 22,677G)
Mesmaster: 428G (7.7%, 428G)
ArrenJevleth: 150G (2.7%, 5,131G)
Chuckolator: 10G (0.2%, 3,361G)

blue bets:
fattunaking: 1,299G (37.1%, 1,299G)
BoneMiser: 500G (14.3%, 500G)
Wooplestein: 394G (11.2%, 394G)
getthemoneyz: 330G (9.4%, 1,622,104G)
Chambs12: 268G (7.6%, 268G)
AllInBot: 200G (5.7%, 200G)
datadrivenbot: 200G (5.7%, 67,608G)
uncharreted: 112G (3.2%, 112G)
gorgewall: 101G (2.9%, 3,430G)
ko2q: 100G (2.9%, 2,919G)

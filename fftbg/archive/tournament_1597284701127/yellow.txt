Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RunicMagus
Male
Taurus
74
76
Squire
Item
Counter Flood
Maintenance
Ignore Height

Mythril Knife
Bronze Shield
Leather Helmet
Adaman Vest
Feather Mantle

Accumulate, Dash, Heal, Tickle
Hi-Potion, Ether, Maiden's Kiss



HASTERIOUS
Male
Aquarius
51
62
Lancer
Basic Skill
Sunken State
Equip Bow
Jump+2

Ultimus Bow

Genji Helmet
Mythril Armor
Spike Shoes

Level Jump2, Vertical Jump5
Accumulate, Throw Stone, Heal, Cheer Up, Fury



Zeroroute
Male
Gemini
76
56
Mediator
Summon Magic
Sunken State
Defense UP
Move+1

Bestiary

Golden Hairpin
Judo Outfit
N-Kai Armlet

Invitation, Persuade, Threaten, Mimic Daravon, Refute, Rehabilitate
Ifrit, Titan, Odin, Fairy



Chuckolator
Female
Pisces
71
78
Geomancer
Talk Skill
Earplug
Equip Knife
Move-HP Up

Assassin Dagger
Round Shield
Feather Hat
Black Costume
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Invitation, Refute

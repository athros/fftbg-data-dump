Player: !Brown
Team: Brown Team
Palettes: Brown/Green



J2DaBibbles
Female
Serpentarius
65
79
Time Mage
Black Magic
Hamedo
Short Charge
Teleport

Battle Bamboo

Holy Miter
Linen Robe
Feather Boots

Haste, Slow 2, Immobilize, Demi 2
Fire, Fire 2, Bolt, Bolt 2, Ice, Ice 2, Frog



Rabbitlogik
Male
Aquarius
41
76
Knight
Jump
Counter
Short Status
Teleport

Slasher
Escutcheon
Gold Helmet
Diamond Armor
Leather Mantle

Magic Break, Speed Break, Power Break, Dark Sword
Level Jump2, Vertical Jump2



Killth3kid
Monster
Capricorn
78
55
Gobbledeguck










Mrfripps
Female
Sagittarius
61
79
Oracle
Time Magic
Meatbone Slash
Short Charge
Jump+2

Battle Bamboo

Twist Headband
Light Robe
Bracer

Blind, Poison, Life Drain, Doubt Faith, Zombie, Foxbird, Confusion Song, Paralyze
Haste, Haste 2, Slow 2, Stop, Demi 2, Stabilize Time

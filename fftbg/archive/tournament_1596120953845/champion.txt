Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Ar Tactic
Female
Scorpio
63
62
Summoner
Yin Yang Magic
Absorb Used MP
Attack UP
Move+3

Flame Rod

Red Hood
Chameleon Robe
Diamond Armlet

Moogle, Shiva, Carbunkle, Odin, Leviathan, Salamander, Fairy, Lich
Blind, Poison, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Sleep, Petrify



UmaiJam
Male
Sagittarius
68
64
Ninja
Item
HP Restore
Equip Gun
Jump+1

Stone Gun
Blaze Gun
Flash Hat
Mythril Vest
Red Shoes

Shuriken, Bomb
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Belkra
Male
Capricorn
69
50
Knight
Summon Magic
Dragon Spirit
Short Charge
Move-MP Up

Save the Queen
Escutcheon
Mythril Helmet
Mythril Armor
108 Gems

Weapon Break, Mind Break, Justice Sword
Moogle, Ramuh, Bahamut



Prince Rogers Nelson
Female
Taurus
46
43
Samurai
Elemental
Damage Split
Magic Attack UP
Move+3

Kiyomori

Genji Helmet
Platinum Armor
Elf Mantle

Asura, Koutetsu, Bizen Boat, Kiyomori
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball

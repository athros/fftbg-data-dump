Player: !Red
Team: Red Team
Palettes: Red/Brown



Pplvee1
Male
Virgo
41
55
Chemist
Elemental
Counter Flood
Dual Wield
Fly

Cute Bag
Hydra Bag
Holy Miter
Power Sleeve
Angel Ring

X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



CosmicTactician
Female
Aries
62
47
Time Mage
Basic Skill
Parry
Short Charge
Ignore Height

White Staff

Ribbon
Light Robe
Defense Ring

Haste 2, Slow 2, Stop, Immobilize, Reflect, Stabilize Time
Accumulate, Heal, Tickle



Muffin Money
Female
Libra
67
67
Time Mage
Dance
Earplug
Equip Bow
Jump+3

Poison Bow

Thief Hat
Secret Clothes
N-Kai Armlet

Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time
Witch Hunt, Wiznaibus, Polka Polka, Last Dance



HaateXIII
Female
Taurus
58
48
Monk
Steal
MP Restore
Equip Bow
Levitate

Night Killer

Thief Hat
Mystic Vest
Defense Ring

Spin Fist, Wave Fist, Secret Fist, Purification
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Meanderandreturn
Female
Sagittarius
76
51
Oracle
Steal
Earplug
Equip Shield
Waterbreathing

Whale Whisker
Bronze Shield
Green Beret
Leather Outfit
Feather Mantle

Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Gil Taking, Steal Heart, Steal Helmet, Steal Shield



DudeMonkey77
Male
Taurus
67
48
Archer
Yin Yang Magic
Arrow Guard
Attack UP
Fly

Silver Bow

Green Beret
Mystic Vest
Small Mantle

Charge+1, Charge+2
Blind, Life Drain, Pray Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Dark Holy



Fenixcrest
Female
Pisces
65
72
Dancer
Battle Skill
Counter
Equip Shield
Jump+3

Star Bag
Flame Shield
Red Hood
Leather Outfit
Leather Mantle

Wiznaibus, Slow Dance, Polka Polka, Dragon Pit
Armor Break, Mind Break



Zagorsek
Male
Gemini
48
44
Archer
Sing
Dragon Spirit
Equip Sword
Ignore Height

Coral Sword
Buckler
Headgear
Mystic Vest
Defense Armlet

Charge+2, Charge+4, Charge+5
Cheer Song, Magic Song, Nameless Song

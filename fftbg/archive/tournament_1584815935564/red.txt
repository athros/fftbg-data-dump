Player: !Red
Team: Red Team
Palettes: Red/Brown



Lythe Caraker
Female
Taurus
48
45
Summoner
Item
MP Restore
Short Charge
Ignore Height

Poison Rod

Triangle Hat
Mythril Vest
Germinas Boots

Shiva, Ramuh, Golem, Carbunkle, Odin, Leviathan, Salamander, Silf, Lich
Potion, Hi-Ether, Antidote, Remedy



ThePineappleSalesman
Female
Sagittarius
61
51
Summoner
Punch Art
Mana Shield
Secret Hunt
Move-HP Up

Wizard Rod

Leather Hat
Black Costume
Diamond Armlet

Shiva, Ramuh, Golem, Carbunkle, Leviathan, Salamander, Silf, Fairy, Cyclops
Spin Fist, Purification, Revive



Pandasforsale
Male
Virgo
58
72
Samurai
Black Magic
Brave Up
Equip Sword
Move+3

Ancient Sword

Circlet
Linen Robe
Germinas Boots

Koutetsu
Fire 2, Fire 4, Ice 4, Flare



Roqqqpsi
Male
Sagittarius
75
58
Oracle
Jump
Brave Up
Long Status
Teleport

Battle Bamboo

Holy Miter
White Robe
Magic Gauntlet

Blind, Pray Faith, Zombie, Foxbird, Dispel Magic
Level Jump2, Vertical Jump4

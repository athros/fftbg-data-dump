Player: !Green
Team: Green Team
Palettes: Green/White



Lawnboxer
Monster
Leo
49
75
Bomb










Laserman1000
Female
Libra
70
46
Time Mage
Talk Skill
Earplug
Short Charge
Move-HP Up

Wizard Staff

Leather Hat
Black Robe
Diamond Armlet

Haste, Reflect, Quick, Demi, Meteor
Persuade, Praise, Solution, Death Sentence, Negotiate, Refute



Dothan89
Female
Cancer
51
42
Summoner
Yin Yang Magic
Counter
Dual Wield
Levitate

Ice Rod
Thunder Rod
Twist Headband
White Robe
Defense Armlet

Moogle, Shiva, Ramuh, Titan, Carbunkle, Leviathan, Silf, Fairy, Lich
Spell Absorb, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze



Lordminsc
Female
Virgo
52
75
Wizard
Steal
Damage Split
Magic Attack UP
Waterwalking

Thunder Rod

Holy Miter
Wizard Robe
Elf Mantle

Fire 2, Fire 4, Bolt 2, Ice, Frog, Flare
Steal Accessory, Leg Aim

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Arch8000
Monster
Sagittarius
56
73
Red Chocobo










Rubenflonne
Male
Aries
40
63
Ninja
Item
Hamedo
Throw Item
Ignore Height

Zorlin Shape
Mythril Knife
Thief Hat
Earth Clothes
Wizard Mantle

Shuriken
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Maakur
Female
Leo
57
76
Geomancer
Draw Out
Absorb Used MP
Equip Sword
Fly

Defender

Flash Hat
Mystic Vest
Wizard Mantle

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Masamune



Maldoree
Female
Cancer
44
42
Monk
White Magic
Parry
Concentrate
Ignore Terrain



Flash Hat
Earth Clothes
Wizard Mantle

Spin Fist, Wave Fist, Earth Slash, Purification
Cure, Cure 4, Raise, Shell, Wall, Esuna

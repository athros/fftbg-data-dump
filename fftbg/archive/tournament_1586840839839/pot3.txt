Final Bets: white - 14 bets for 6,198G (30.5%, x2.28); black - 23 bets for 14,149G (69.5%, x0.44)

white bets:
Aldrammech: 1,064G (17.2%, 1,064G)
NovaKnight21: 760G (12.3%, 6,490G)
ezefeld: 700G (11.3%, 5,023G)
YaBoy125: 600G (9.7%, 600G)
Laserman1000: 587G (9.5%, 11,787G)
pandasforsale: 555G (9.0%, 12,870G)
DeathTaxesAndAnime: 428G (6.9%, 428G)
Shalloween: 400G (6.5%, 34,384G)
DejaPoo21: 384G (6.2%, 384G)
TheOneRanger: 320G (5.2%, 320G)
ApplesauceBoss: 100G (1.6%, 6,152G)
Heroebal: 100G (1.6%, 5,853G)
Chompie: 100G (1.6%, 3,030G)
Evewho: 100G (1.6%, 3,248G)

black bets:
WireLord: 2,190G (15.5%, 2,190G)
haplo064: 2,000G (14.1%, 8,468G)
HaplessOne: 1,500G (10.6%, 55,374G)
BirbBrainsBot: 1,000G (7.1%, 101,013G)
Arunic: 1,000G (7.1%, 11,394G)
mirapoix: 996G (7.0%, 996G)
ungabunga_bot: 850G (6.0%, 117,930G)
getthemoneyz: 738G (5.2%, 482,032G)
joewcarson: 500G (3.5%, 7,448G)
fallendj77: 444G (3.1%, 444G)
datadrivenbot: 421G (3.0%, 13,028G)
Shadowkept: 400G (2.8%, 1,980G)
electric_glass: 333G (2.4%, 5,629G)
EnemyController: 300G (2.1%, 38,800G)
JIDkomu: 296G (2.1%, 296G)
ANFz: 200G (1.4%, 3,910G)
potgodtopdog: 200G (1.4%, 819G)
HaateXIII: 200G (1.4%, 1,563G)
AllInBot: 177G (1.3%, 177G)
TimeJannies: 104G (0.7%, 104G)
ko2q: 100G (0.7%, 6,292G)
Jerboj: 100G (0.7%, 42,371G)
HorusTaurus: 100G (0.7%, 471G)

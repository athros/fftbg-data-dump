Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Actual JP
Female
Capricorn
50
68
Ninja
Summon Magic
Speed Save
Equip Armor
Waterbreathing

Short Edge
Hidden Knife
Grand Helmet
Platinum Armor
Defense Armlet

Shuriken, Bomb, Ninja Sword, Dictionary
Moogle, Shiva, Titan, Leviathan



Joewcarson
Female
Virgo
72
75
Calculator
White Magic
MA Save
Equip Shield
Ignore Terrain

Dragon Rod
Gold Shield
Black Hood
Brigandine
Dracula Mantle

CT, 3
Cure, Cure 4, Raise, Shell 2, Wall, Esuna, Holy



LordTomS
Male
Scorpio
54
40
Ninja
Summon Magic
Critical Quick
Concentrate
Move+2

Mage Masher
Morning Star
Feather Hat
Brigandine
Leather Mantle

Shuriken, Staff
Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Lich



Error72
Male
Aries
57
61
Knight
Jump
Abandon
Equip Polearm
Jump+1

Ivory Rod
Ice Shield
Gold Helmet
Wizard Robe
Small Mantle

Armor Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Night Sword
Level Jump8, Vertical Jump7

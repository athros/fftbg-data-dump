Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ThePineappleSalesman
Male
Scorpio
38
71
Oracle
Sing
MP Restore
Doublehand
Move-HP Up

Cypress Rod

Leather Hat
Power Sleeve
Sprint Shoes

Blind, Poison, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic
Life Song, Cheer Song, Nameless Song



GrandmasterFrankerZ
Female
Capricorn
55
45
Squire
Jump
Parry
Beastmaster
Fly

Giant Axe
Bronze Shield
Flash Hat
Mystic Vest
Vanish Mantle

Throw Stone, Heal, Tickle, Cheer Up, Fury, Wish
Level Jump2, Vertical Jump2



Run With Stone GUNs
Female
Virgo
68
64
Oracle
Draw Out
Abandon
Magic Attack UP
Ignore Terrain

Ivory Rod

Cachusha
Black Robe
Vanish Mantle

Blind, Poison, Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Sleep
Asura, Muramasa



Jeeboheebo
Male
Cancer
48
70
Ninja
Elemental
HP Restore
Equip Sword
Move+1

Ragnarok
Save the Queen
Ribbon
Clothes
Reflect Ring

Knife, Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

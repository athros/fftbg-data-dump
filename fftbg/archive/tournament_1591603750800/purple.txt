Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Victoriolue
Female
Leo
68
69
Priest
Draw Out
Absorb Used MP
Halve MP
Move-MP Up

White Staff

Twist Headband
Mystic Vest
108 Gems

Cure, Cure 3, Raise, Regen, Shell, Esuna
Asura, Bizen Boat, Murasame, Kiyomori



Mistblade
Monster
Aries
64
61
Cockatrice










BuffaloCrunch
Male
Leo
45
42
Thief
Summon Magic
Counter Flood
Concentrate
Move-MP Up

Orichalcum

Golden Hairpin
Earth Clothes
Cursed Ring

Steal Helmet, Steal Shield, Steal Status, Leg Aim
Moogle, Ifrit, Leviathan, Cyclops



Rednecknazgul
Female
Virgo
47
49
Knight
Charge
Brave Save
Long Status
Lava Walking

Ragnarok
Crystal Shield
Circlet
Plate Mail
Spike Shoes

Head Break
Charge+3, Charge+4

Player: !zChamp
Team: Champion Team
Palettes: Green/White



PrudishDuckling
Male
Taurus
63
58
Bard
Charge
Counter Flood
Halve MP
Waterwalking

Ramia Harp

Twist Headband
Genji Armor
Power Wrist

Cheer Song, Battle Song, Nameless Song, Sky Demon
Charge+4, Charge+5, Charge+7



RjA0zcOQ96
Female
Gemini
66
52
Knight
Charge
Critical Quick
Equip Sword
Move+3

Kikuichimoji
Genji Shield
Platinum Helmet
Wizard Robe
Sprint Shoes

Weapon Break, Mind Break, Dark Sword, Night Sword
Charge+2, Charge+4, Charge+7, Charge+10



Zebobz
Male
Leo
45
64
Lancer
White Magic
Counter Flood
Halve MP
Move-HP Up

Gungnir
Crystal Shield
Diamond Helmet
Platinum Armor
Wizard Mantle

Level Jump8, Vertical Jump3
Cure 2, Cure 4, Reraise, Esuna



Aka Gilly
Monster
Cancer
62
54
Malboro








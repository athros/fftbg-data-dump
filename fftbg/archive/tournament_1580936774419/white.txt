Player: !White
Team: White Team
Palettes: White/Blue



Morrowsaurs
Male
Cancer
71
57
Wizard
Charge
Arrow Guard
Secret Hunt
Levitate

Mage Masher

Golden Hairpin
Light Robe
Power Wrist

Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 2, Frog
Charge+2, Charge+4, Charge+10, Charge+20



Stablo
Male
Capricorn
43
69
Summoner
White Magic
Meatbone Slash
Equip Shield
Ignore Height

Rod
Flame Shield
Thief Hat
Silk Robe
Jade Armlet

Moogle, Ramuh, Titan, Carbunkle, Leviathan, Salamander, Fairy
Cure, Cure 2, Cure 3, Raise 2, Regen, Shell 2, Esuna, Holy



Disindjinnuous
Male
Capricorn
36
43
Monk
Sing
Damage Split
Defend
Move+1



Holy Miter
Wizard Outfit
Rubber Shoes

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Angel Song, Magic Song, Hydra Pit



Kingbananabear
Male
Libra
52
54
Samurai
Throw
HP Restore
Secret Hunt
Ignore Height

Koutetsu Knife

Barbuta
Diamond Armor
N-Kai Armlet

Asura, Heaven's Cloud, Kiyomori, Kikuichimoji
Knife

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Who Lio42
Male
Taurus
55
39
Archer
Battle Skill
HP Restore
Equip Armor
Levitate

Hunting Bow
Flame Shield
Genji Helmet
Power Sleeve
N-Kai Armlet

Charge+1, Charge+2, Charge+4, Charge+10
Head Break, Weapon Break, Speed Break, Mind Break, Night Sword



Dymntd
Female
Virgo
75
69
Time Mage
Summon Magic
Counter
Equip Sword
Ignore Terrain

Koutetsu Knife

Triangle Hat
Robe of Lords
Genji Gauntlet

Haste, Haste 2, Slow 2, Stop, Immobilize, Float, Stabilize Time
Moogle, Ramuh, Titan, Golem, Bahamut, Odin, Salamander, Cyclops



CapnChaos12
Male
Aquarius
73
55
Knight
Draw Out
MA Save
Martial Arts
Teleport 2

Coral Sword
Escutcheon
Circlet
Reflect Mail
Rubber Shoes

Head Break, Speed Break, Surging Sword
Asura, Koutetsu, Muramasa



HaychDub
Female
Scorpio
63
61
Mime

Regenerator
Dual Wield
Waterwalking



Crystal Helmet
Earth Clothes
Leather Mantle

Mimic


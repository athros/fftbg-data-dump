Player: !Red
Team: Red Team
Palettes: Red/Brown



NovaKnight21
Male
Taurus
58
58
Archer
White Magic
Dragon Spirit
Doublehand
Move+1

Cross Bow

Twist Headband
Mythril Vest
Battle Boots

Charge+3, Charge+7, Charge+10
Cure 2, Cure 4, Raise 2, Regen, Esuna



CosmicTactician
Male
Aries
58
79
Mediator
Punch Art
Counter Tackle
Equip Polearm
Move-MP Up

Spear

Triangle Hat
Silk Robe
Magic Ring

Persuade, Preach, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Spin Fist, Secret Fist, Chakra, Revive



Lali Lulelo
Female
Scorpio
80
76
Dancer
Elemental
Earplug
Equip Polearm
Move+1

Obelisk

Flash Hat
Wizard Outfit
Magic Ring

Witch Hunt, Disillusion, Nameless Dance, Last Dance
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Evewho
Female
Libra
74
50
Chemist
Summon Magic
Dragon Spirit
Equip Gun
Waterwalking

Stone Gun

Red Hood
Rubber Costume
Small Mantle

Potion, X-Potion, Soft, Phoenix Down
Moogle, Golem, Odin, Leviathan

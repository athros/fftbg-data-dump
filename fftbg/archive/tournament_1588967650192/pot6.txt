Final Bets: white - 23 bets for 19,665G (56.1%, x0.78); purple - 23 bets for 15,385G (43.9%, x1.28)

white bets:
Lydian_C: 3,400G (17.3%, 8,195G)
Mesmaster: 3,000G (15.3%, 32,474G)
TheFALLofLindsay: 2,000G (10.2%, 15,945G)
JumbocactuarX27: 1,000G (5.1%, 26,550G)
Aldrammech: 1,000G (5.1%, 27,869G)
EnemyController: 1,000G (5.1%, 210,554G)
ungabunga_bot: 1,000G (5.1%, 432,343G)
BirbBrainsBot: 1,000G (5.1%, 128,740G)
aznwanderor: 847G (4.3%, 847G)
vorap: 800G (4.1%, 29,840G)
DeathTaxesAndAnime: 796G (4.0%, 796G)
Laserman1000: 600G (3.1%, 3,600G)
thunderducker: 555G (2.8%, 10,745G)
roqqqpsi: 516G (2.6%, 516G)
NicoSavoy: 500G (2.5%, 3,723G)
Cryptopsy70: 404G (2.1%, 2,910G)
DustBirdEX: 321G (1.6%, 5,512G)
Lythe_Caraker: 250G (1.3%, 118,301G)
proper__noun__: 200G (1.0%, 3,154G)
Jampck: 150G (0.8%, 1,049G)
FNCardascia_: 126G (0.6%, 252G)
brokenknight201: 100G (0.5%, 1,006G)
datadrivenbot: 100G (0.5%, 14,201G)

purple bets:
nifboy: 5,905G (38.4%, 11,580G)
killth3kid: 2,000G (13.0%, 51,228G)
Grimmace45: 1,517G (9.9%, 3,035G)
GnielKnows: 961G (6.2%, 1,923G)
BoneMiser: 500G (3.2%, 9,537G)
ZephyrTempest: 456G (3.0%, 1,157G)
maakur_: 400G (2.6%, 400G)
CorpusCav: 400G (2.6%, 5,614G)
ShintaroNayaka: 400G (2.6%, 17,757G)
YaBoy125: 368G (2.4%, 368G)
fenaen: 356G (2.3%, 356G)
UFOCATCHERDAILY: 300G (1.9%, 3,124G)
KasugaiRoastedPeas: 300G (1.9%, 890G)
SQUiDSQUARKLIN: 284G (1.8%, 284G)
Miyokari: 250G (1.6%, 3,191G)
cupholderr: 208G (1.4%, 208G)
LivingHitokiri: 200G (1.3%, 4,190G)
eudes89: 128G (0.8%, 128G)
Firesheath: 100G (0.6%, 19,573G)
FriendSquirrel: 100G (0.6%, 23,949G)
mannequ1n: 100G (0.6%, 3,811G)
Rytor: 100G (0.6%, 1,893G)
getthemoneyz: 52G (0.3%, 649,062G)

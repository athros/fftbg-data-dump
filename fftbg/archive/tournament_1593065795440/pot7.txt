Final Bets: green - 27 bets for 26,875G (12.8%, x6.81); black - 6 bets for 183,057G (87.2%, x0.15)

green bets:
Digitalsocrates: 5,000G (18.6%, 27,718G)
Rislyeu: 3,250G (12.1%, 3,250G)
Evewho: 2,247G (8.4%, 2,247G)
electric_algus: 2,103G (7.8%, 2,103G)
TeaTime29: 2,000G (7.4%, 4,424G)
neerrm: 1,001G (3.7%, 1,001G)
Chompie: 1,000G (3.7%, 10,596G)
Ring_Wyrm: 1,000G (3.7%, 2,370G)
cougboi: 1,000G (3.7%, 7,398G)
BirbBrainsBot: 1,000G (3.7%, 143,674G)
EnemyController: 1,000G (3.7%, 1,246,891G)
ZZ_Yoshi: 770G (2.9%, 1,529G)
gorgewall: 701G (2.6%, 3,687G)
GatsbysGhost: 635G (2.4%, 635G)
superdevon1: 600G (2.2%, 600G)
OneHundredFists: 584G (2.2%, 584G)
serperemagus: 528G (2.0%, 528G)
evontno: 508G (1.9%, 508G)
DuraiPapers: 500G (1.9%, 27,606G)
getthemoneyz: 344G (1.3%, 1,026,526G)
megamax667: 238G (0.9%, 5,238G)
genericco: 236G (0.9%, 236G)
oops25: 210G (0.8%, 2,410G)
Lydian_C: 120G (0.4%, 153,985G)
Firesheath: 100G (0.4%, 4,062G)
autlzmoo: 100G (0.4%, 100G)
datadrivenbot: 100G (0.4%, 48,942G)

black bets:
Mesmaster: 180,449G (98.6%, 200,499G)
SkylerBunny: 1,000G (0.5%, 560,183G)
gamesage53: 598G (0.3%, 598G)
Sairentozon7: 490G (0.3%, 490G)
DaveStrider55: 420G (0.2%, 7,683G)
foofermoofer: 100G (0.1%, 2,188G)

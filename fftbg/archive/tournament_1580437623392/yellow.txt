Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



MrJamDango
Male
Aquarius
71
64
Monk
Time Magic
Counter Tackle
Dual Wield
Jump+2



Holy Miter
Brigandine
108 Gems

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Haste, Float, Demi 2, Stabilize Time, Meteor



Shellback27
Female
Cancer
48
65
Summoner
Yin Yang Magic
Blade Grasp
Magic Attack UP
Jump+2

Wizard Rod

Leather Hat
Light Robe
Power Wrist

Ramuh, Ifrit, Golem, Carbunkle, Salamander, Fairy, Lich
Spell Absorb, Life Drain, Doubt Faith, Silence Song, Confusion Song, Dark Holy



Willjin
Female
Sagittarius
67
43
Priest
Steal
Absorb Used MP
Equip Axe
Move-MP Up

Battle Axe

Leather Hat
Leather Vest
Rubber Shoes

Cure 4, Raise, Reraise, Regen, Shell 2, Wall, Esuna
Steal Helmet, Steal Weapon, Steal Accessory



Belted Kingfisher
Male
Capricorn
69
67
Ninja
Elemental
Blade Grasp
Beastmaster
Jump+3

Hidden Knife
Spell Edge
Flash Hat
Wizard Outfit
Elf Mantle

Shuriken, Knife, Dictionary
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Black
Team: Black Team
Palettes: Black/Red



Azure Storm
Male
Aries
40
56
Lancer
Draw Out
Blade Grasp
Defense UP
Waterbreathing

Partisan
Escutcheon
Leather Helmet
Diamond Armor
Magic Ring

Level Jump3, Vertical Jump6
Muramasa, Kikuichimoji



Betbeez
Male
Scorpio
53
65
Summoner
Talk Skill
Brave Up
Attack UP
Ignore Terrain

Dragon Rod

Golden Hairpin
Leather Outfit
Rubber Shoes

Moogle, Shiva, Golem, Carbunkle, Odin, Leviathan, Salamander
Solution, Mimic Daravon, Refute, Rehabilitate



CidDurai
Male
Gemini
51
54
Time Mage
Sing
Regenerator
Defense UP
Move+1

Wizard Staff

Headgear
Silk Robe
Battle Boots

Haste, Haste 2, Slow 2, Float, Reflect, Quick, Stabilize Time, Meteor
Angel Song, Life Song, Magic Song, Nameless Song



CassiePhoenix
Female
Cancer
42
42
Time Mage
Draw Out
HP Restore
Long Status
Move-MP Up

White Staff

Twist Headband
Wizard Robe
108 Gems

Haste, Haste 2, Slow, Float, Reflect, Demi, Stabilize Time, Meteor
Asura, Heaven's Cloud, Muramasa

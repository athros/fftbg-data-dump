Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Jeeboheebo
Monster
Sagittarius
79
75
Porky










Kyver
Female
Gemini
58
43
Lancer
Summon Magic
Damage Split
Beastmaster
Move+3

Musk Rod
Ice Shield
Platinum Helmet
Carabini Mail
Chantage

Level Jump8, Vertical Jump7
Moogle, Shiva, Ramuh, Golem, Bahamut, Fairy, Cyclops



Baron Von Scrub
Male
Aries
71
57
Priest
Draw Out
Regenerator
Short Charge
Move+1

Sage Staff

Red Hood
Black Robe
Feather Boots

Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Kiyomori, Muramasa, Kikuichimoji



Just Here2
Male
Leo
75
67
Bard
White Magic
Damage Split
Equip Axe
Move+3

Slasher

Flash Hat
Adaman Vest
N-Kai Armlet

Life Song, Battle Song
Cure, Cure 3, Raise, Reraise, Protect 2, Esuna

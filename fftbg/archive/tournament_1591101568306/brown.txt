Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Evewho
Female
Sagittarius
44
66
Monk
Summon Magic
Parry
Equip Knife
Teleport

Cultist Dagger

Holy Miter
Chain Vest
Jade Armlet

Spin Fist, Wave Fist, Secret Fist, Purification, Revive
Moogle, Ifrit, Titan, Golem, Leviathan, Salamander, Silf, Fairy



Nifboy
Male
Gemini
44
55
Oracle
Summon Magic
MA Save
Defense UP
Jump+1

Iron Fan

Black Hood
Black Robe
Small Mantle

Blind, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep
Moogle, Shiva, Ramuh, Titan, Carbunkle, Salamander



Coralreeferz
Male
Aries
53
56
Summoner
Charge
Counter Tackle
Short Charge
Move-MP Up

Oak Staff

Red Hood
Light Robe
Rubber Shoes

Moogle, Ramuh, Ifrit, Odin, Salamander, Fairy, Lich
Charge+2, Charge+4



Pplvee1
Male
Cancer
42
51
Summoner
Punch Art
Absorb Used MP
Sicken
Jump+3

Battle Folio

Flash Hat
Light Robe
Chantage

Ramuh, Odin, Fairy
Pummel, Purification

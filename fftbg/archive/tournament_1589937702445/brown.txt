Player: !Brown
Team: Brown Team
Palettes: Brown/Green



StealthModeLocke
Female
Serpentarius
63
68
Priest
Item
Damage Split
Equip Gun
Jump+2

Bestiary

Holy Miter
Wizard Outfit
Bracer

Cure, Raise 2, Protect, Wall, Esuna, Magic Barrier
Hi-Potion, X-Potion, Ether, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down



RunicMagus
Female
Aries
54
61
Calculator
Black Magic
Faith Save
Short Charge
Fly

Battle Folio

Black Hood
Chain Vest
Feather Mantle

CT, Height, Prime Number, 5, 3
Fire, Bolt, Bolt 4, Ice 2, Flare



Firesheath
Male
Leo
53
69
Archer
Time Magic
HP Restore
Maintenance
Jump+3

Mythril Gun
Round Shield
Diamond Helmet
Mystic Vest
Spike Shoes

Charge+2, Charge+3, Charge+4, Charge+5
Haste, Meteor



Genericco
Female
Gemini
62
57
Chemist
Dance
Blade Grasp
Short Status
Jump+2

Main Gauche

Black Hood
Judo Outfit
Feather Boots

Potion, X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Slow Dance

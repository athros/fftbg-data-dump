Final Bets: purple - 20 bets for 15,458G (62.7%, x0.60); brown - 18 bets for 9,198G (37.3%, x1.68)

purple bets:
MrDiggs49: 2,609G (16.9%, 2,609G)
lowlf: 2,501G (16.2%, 24,589G)
vorap: 1,500G (9.7%, 62,526G)
HaplessOne: 1,500G (9.7%, 9,618G)
TheGuesty: 1,202G (7.8%, 36,202G)
coralreeferz: 1,000G (6.5%, 4,781G)
ColetteMSLP: 800G (5.2%, 64,292G)
EnemyController: 600G (3.9%, 281,023G)
killth3kid: 600G (3.9%, 9,973G)
otakutaylor: 438G (2.8%, 438G)
FoeSquirrel: 404G (2.6%, 404G)
deadseascrolldier: 397G (2.6%, 397G)
FriendSquirrel: 352G (2.3%, 352G)
byrdturbo: 333G (2.2%, 20,748G)
twelfthrootoftwo: 300G (1.9%, 17,984G)
CapnChaos12: 300G (1.9%, 1,726G)
CorpusCav: 222G (1.4%, 6,735G)
KasugaiRoastedPeas: 200G (1.3%, 1,888G)
StealthModeLocke: 100G (0.6%, 42,028G)
jakeduhhsnake: 100G (0.6%, 893G)

brown bets:
RunicMagus: 1,200G (13.0%, 32,988G)
Laserman1000: 1,077G (11.7%, 1,077G)
volgrathemoose: 1,001G (10.9%, 7,541G)
BirbBrainsBot: 1,000G (10.9%, 166,838G)
getthemoneyz: 1,000G (10.9%, 680,373G)
E_Ballard: 876G (9.5%, 876G)
Nizaha: 501G (5.4%, 16,801G)
evontno: 500G (5.4%, 7,797G)
Lanshaft: 392G (4.3%, 1,178G)
AUrato: 300G (3.3%, 6,581G)
LionelParks: 300G (3.3%, 3,110G)
holyonline: 250G (2.7%, 9,932G)
Digitalsocrates: 236G (2.6%, 236G)
Dymntd: 200G (2.2%, 7,262G)
Evewho: 100G (1.1%, 7,468G)
UnholyCorsair: 100G (1.1%, 2,559G)
datadrivenbot: 100G (1.1%, 20,851G)
AmaninAmide: 65G (0.7%, 1,794G)

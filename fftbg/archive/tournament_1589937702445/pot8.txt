Final Bets: white - 17 bets for 8,418G (23.4%, x3.27); champion - 28 bets for 27,544G (76.6%, x0.31)

white bets:
killth3kid: 1,000G (11.9%, 10,238G)
Draconis345: 1,000G (11.9%, 72,356G)
ColetteMSLP: 1,000G (11.9%, 63,968G)
BirbBrainsBot: 1,000G (11.9%, 167,058G)
Virilikus: 1,000G (11.9%, 169,601G)
powergems: 650G (7.7%, 6,328G)
solomongrundy85: 500G (5.9%, 19,370G)
evontno: 500G (5.9%, 8,022G)
AUrato: 376G (4.5%, 9,409G)
coralreeferz: 350G (4.2%, 6,804G)
LeoNightFury: 300G (3.6%, 4,632G)
otakutaylor: 200G (2.4%, 2,021G)
Lolprinze: 200G (2.4%, 828G)
jakeduhhsnake: 100G (1.2%, 1,229G)
Firesheath: 100G (1.2%, 808G)
deadseascrolldier: 100G (1.2%, 4,700G)
DeathTaxesAndAnime: 42G (0.5%, 1,902G)

champion bets:
ZZ_Yoshi: 6,400G (23.2%, 36,474G)
HaplessOne: 4,444G (16.1%, 8,714G)
E_Ballard: 2,252G (8.2%, 2,252G)
NicoSavoy: 2,000G (7.3%, 31,691G)
LionelParks: 1,110G (4.0%, 1,110G)
Nizaha: 1,001G (3.6%, 16,007G)
Deathmaker06: 1,000G (3.6%, 12,811G)
electric_glass: 1,000G (3.6%, 10,035G)
Dymntd: 1,000G (3.6%, 7,368G)
JoeBlowForever: 1,000G (3.6%, 2,280G)
cougboi: 1,000G (3.6%, 13,420G)
EnemyController: 1,000G (3.6%, 282,490G)
Reltz_: 500G (1.8%, 3,446G)
nekojin: 500G (1.8%, 4,801G)
Rytor: 500G (1.8%, 4,195G)
Laserman1000: 460G (1.7%, 460G)
Lanshaft: 396G (1.4%, 396G)
CapnChaos12: 300G (1.1%, 2,241G)
twelfthrootoftwo: 300G (1.1%, 18,523G)
byrdturbo: 269G (1.0%, 20,895G)
Kyune: 250G (0.9%, 11,155G)
AmaninAmide: 200G (0.7%, 1,931G)
aeonicvector: 200G (0.7%, 2,106G)
getthemoneyz: 112G (0.4%, 679,528G)
StealthModeLocke: 100G (0.4%, 42,710G)
Tougou: 100G (0.4%, 4,247G)
datadrivenbot: 100G (0.4%, 20,897G)
moonliquor: 50G (0.2%, 1,068G)

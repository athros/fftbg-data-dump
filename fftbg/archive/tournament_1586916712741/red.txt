Player: !Red
Team: Red Team
Palettes: Red/Brown



MantisFinch
Male
Cancer
45
47
Oracle
Basic Skill
MP Restore
Equip Knife
Move-MP Up

Short Edge

Headgear
Adaman Vest
Defense Armlet

Blind, Poison, Zombie, Dispel Magic, Paralyze, Sleep
Accumulate, Throw Stone, Heal, Fury



Sairentozon7
Female
Libra
73
72
Archer
Time Magic
Earplug
Equip Knife
Waterbreathing

Short Edge
Platinum Shield
Golden Hairpin
Judo Outfit
Elf Mantle

Charge+1, Charge+3, Charge+4, Charge+5, Charge+20
Haste, Haste 2, Immobilize



LeepingJJ
Female
Aquarius
67
61
Time Mage
Summon Magic
Absorb Used MP
Magic Attack UP
Fly

Gold Staff

Feather Hat
Chameleon Robe
Spike Shoes

Stabilize Time, Meteor
Moogle, Golem, Leviathan, Lich



Evewho
Female
Pisces
62
64
Knight
Elemental
Auto Potion
Equip Gun
Ignore Height

Bloody Strings
Buckler
Circlet
Gold Armor
Genji Gauntlet

Armor Break, Weapon Break, Magic Break, Speed Break, Justice Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

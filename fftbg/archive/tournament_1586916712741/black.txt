Player: !Black
Team: Black Team
Palettes: Black/Red



TheGuesty
Male
Virgo
80
53
Archer
Sing
Meatbone Slash
Doublehand
Retreat

Stone Gun

Flash Hat
Wizard Outfit
Magic Gauntlet

Charge+2, Charge+10
Angel Song, Cheer Song, Nameless Song



Alacor
Male
Virgo
41
41
Knight
Sing
Sunken State
Short Charge
Jump+1

Save the Queen
Aegis Shield
Crystal Helmet
Black Robe
Defense Armlet

Weapon Break, Power Break, Mind Break
Battle Song, Magic Song, Nameless Song, Last Song, Space Storage, Hydra Pit



HaateXIII
Female
Taurus
66
53
Geomancer
Basic Skill
Counter Magic
Defense UP
Waterbreathing

Asura Knife
Bronze Shield
Headgear
Wizard Outfit
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Accumulate, Dash, Heal, Yell



Byrdturbo
Male
Taurus
64
69
Mediator
Throw
Damage Split
Short Charge
Jump+3

Battle Folio

Red Hood
Brigandine
Cursed Ring

Persuade, Praise, Threaten, Preach, Solution, Insult, Refute, Rehabilitate
Ninja Sword

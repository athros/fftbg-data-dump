Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Dynasti
Male
Virgo
50
40
Mediator
Time Magic
Hamedo
Sicken
Fly

Blast Gun

Green Beret
Leather Vest
Defense Armlet

Invitation, Praise, Threaten, Preach, Mimic Daravon, Refute
Haste, Haste 2, Reflect, Quick, Demi, Stabilize Time



Juggalettefurbaby420
Female
Capricorn
48
68
Archer
Item
HP Restore
Doublehand
Move+2

Romanda Gun

Headgear
Adaman Vest
Rubber Shoes

Charge+1, Charge+5, Charge+10, Charge+20
Potion, X-Potion, Antidote, Eye Drop



Hamborn
Female
Virgo
48
64
Lancer
Yin Yang Magic
HP Restore
Equip Bow
Teleport

Cross Bow
Escutcheon
Platinum Helmet
Leather Armor
Magic Ring

Level Jump3, Vertical Jump6
Blind, Poison, Pray Faith, Blind Rage, Confusion Song



Zachara
Female
Libra
39
70
Dancer
Yin Yang Magic
Caution
Equip Polearm
Retreat

Persia

Headgear
Secret Clothes
108 Gems

Witch Hunt, Wiznaibus, Slow Dance, Last Dance, Dragon Pit
Spell Absorb, Life Drain, Zombie, Foxbird, Petrify

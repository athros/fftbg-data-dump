Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Fenaen
Male
Virgo
43
51
Knight
Punch Art
Faith Save
Beastmaster
Move-HP Up

Long Sword
Mythril Shield
Cross Helmet
Robe of Lords
Spike Shoes

Head Break, Speed Break, Mind Break, Justice Sword, Night Sword, Surging Sword
Pummel, Wave Fist, Chakra, Seal Evil



Arctodus13
Male
Taurus
49
51
Oracle
Black Magic
Faith Save
Equip Sword
Teleport

Save the Queen

Green Beret
Black Robe
Spike Shoes

Poison, Spell Absorb, Dispel Magic, Sleep
Fire, Bolt 2, Ice, Ice 3



RunicMagus
Female
Cancer
80
40
Priest
Time Magic
Parry
Doublehand
Lava Walking

Rainbow Staff

Flash Hat
Silk Robe
Dracula Mantle

Cure, Cure 4, Raise, Reraise, Protect, Shell 2, Wall, Esuna
Haste, Slow, Slow 2, Stop, Float, Quick, Demi



Dogsandcatsand
Male
Aries
50
52
Chemist
Sing
Counter
Concentrate
Waterwalking

Star Bag

Feather Hat
Brigandine
Jade Armlet

Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Life Song, Battle Song, Magic Song, Nameless Song, Diamond Blade

Final Bets: white - 17 bets for 5,869G (46.3%, x1.16); brown - 14 bets for 6,820G (53.7%, x0.86)

white bets:
NIghtdew14: 1,700G (29.0%, 6,801G)
midnight_blues3: 1,000G (17.0%, 9,721G)
maakur_: 536G (9.1%, 536G)
DamnThatShark: 415G (7.1%, 415G)
letdowncity: 392G (6.7%, 2,603G)
dem0nj0ns: 300G (5.1%, 599G)
RageImmortaI: 288G (4.9%, 4,100G)
Treafa: 250G (4.3%, 1,278G)
Lemonjohns: 200G (3.4%, 2,306G)
datadrivenbot: 200G (3.4%, 61,066G)
Lydian_C: 123G (2.1%, 104,965G)
Ring_Wyrm: 100G (1.7%, 15,648G)
SephDarkheart: 100G (1.7%, 20,983G)
CosmicTactician: 100G (1.7%, 44,081G)
Powermhero: 100G (1.7%, 24,270G)
getthemoneyz: 60G (1.0%, 1,269,251G)
moonliquor: 5G (0.1%, 4,370G)

brown bets:
amiture: 1,000G (14.7%, 7,650G)
BirbBrainsBot: 1,000G (14.7%, 69,697G)
E_Ballard: 1,000G (14.7%, 2,671G)
Sairentozon7: 1,000G (14.7%, 6,532G)
Breakdown777: 500G (7.3%, 1,945G)
Zeroroute: 500G (7.3%, 500G)
Lanshaft: 480G (7.0%, 15,839G)
Forkmore: 300G (4.4%, 971G)
Blastinus: 224G (3.3%, 224G)
winteriscorning: 215G (3.2%, 1,079G)
gorgewall: 201G (2.9%, 5,327G)
regios91: 200G (2.9%, 2,371G)
DeathTaxesAndAnime: 100G (1.5%, 7,565G)
Kellios11: 100G (1.5%, 34,621G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Rexamajinx
Male
Leo
62
76
Samurai
Talk Skill
Meatbone Slash
Equip Axe
Move-HP Up

Slasher

Genji Helmet
Light Robe
108 Gems

Asura, Murasame
Invitation, Persuade, Solution, Insult, Mimic Daravon, Refute



Victoriolue
Male
Aries
70
80
Thief
Throw
Hamedo
Martial Arts
Fly

Assassin Dagger

Green Beret
Leather Outfit
Jade Armlet

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Dictionary



Ins4n1ty87
Female
Scorpio
57
54
Lancer
White Magic
Parry
Attack UP
Teleport

Holy Lance
Kaiser Plate
Diamond Helmet
Bronze Armor
Battle Boots

Level Jump2, Vertical Jump2
Raise, Regen, Shell, Esuna



Chronoxtrigger
Male
Scorpio
43
53
Lancer
Black Magic
Counter Magic
Dual Wield
Move-HP Up

Octagon Rod
Battle Bamboo
Barbuta
Crystal Mail
Jade Armlet

Level Jump5, Vertical Jump7
Fire, Bolt 3, Ice 2, Death

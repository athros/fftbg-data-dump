Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MoxOb
Male
Capricorn
52
66
Oracle
Time Magic
Counter
Defend
Jump+3

Gokuu Rod

Twist Headband
Chameleon Robe
Defense Armlet

Spell Absorb, Blind Rage, Dispel Magic, Sleep
Immobilize, Demi 2, Stabilize Time, Meteor



Sinnyil2
Male
Pisces
60
46
Thief
Throw
Dragon Spirit
Equip Sword
Move+3

Koutetsu Knife

Holy Miter
Brigandine
Wizard Mantle

Steal Helmet, Steal Weapon, Steal Status, Leg Aim
Shuriken, Bomb



Winterharte
Male
Sagittarius
54
66
Wizard
Sing
Mana Shield
Equip Sword
Ignore Terrain

Koutetsu Knife

Black Hood
Silk Robe
Bracer

Fire 3, Bolt, Bolt 2, Ice 2
Magic Song, Hydra Pit



Volgrathemoose
Female
Cancer
55
42
Dancer
Item
Faith Up
Equip Shield
Jump+3

Cashmere
Escutcheon
Black Hood
Wizard Robe
N-Kai Armlet

Polka Polka, Nameless Dance, Obsidian Blade
Potion, Ether, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down

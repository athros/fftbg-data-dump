Player: !Red
Team: Red Team
Palettes: Red/Brown



ZephyrTempest
Male
Virgo
37
63
Geomancer
Throw
Catch
Magic Attack UP
Jump+3

Battle Axe
Crystal Shield
Thief Hat
Chameleon Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Shuriken, Bomb, Axe



Narcius
Female
Sagittarius
62
38
Mime

Sunken State
Long Status
Move-MP Up



Feather Hat
Mythril Vest
Reflect Ring

Mimic




Lionhermit
Male
Scorpio
39
45
Mime

Speed Save
Dual Wield
Move+2



Black Hood
Leather Armor
Diamond Armlet

Mimic




TrollOfGod
Male
Pisces
79
68
Mediator
Summon Magic
Critical Quick
Long Status
Ignore Terrain

Romanda Gun

Black Hood
Wizard Robe
Salty Rage

Persuade, Preach, Solution, Death Sentence, Rehabilitate
Moogle, Shiva, Leviathan, Silf, Cyclops

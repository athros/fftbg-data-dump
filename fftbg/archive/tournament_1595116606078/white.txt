Player: !White
Team: White Team
Palettes: White/Blue



Just Here2
Male
Pisces
66
70
Knight
Steal
Faith Save
Magic Defense UP
Jump+2

Defender
Crystal Shield
Platinum Helmet
Chameleon Robe
108 Gems

Armor Break, Shield Break, Weapon Break, Power Break, Stasis Sword, Dark Sword
Steal Helmet, Steal Armor, Steal Shield, Leg Aim



Nifboy
Female
Capricorn
68
65
Thief
Charge
Mana Shield
Equip Armor
Move+3

Mage Masher

Ribbon
Chameleon Robe
Leather Mantle

Gil Taking, Steal Heart, Arm Aim
Charge+5, Charge+7



Kellios11
Male
Taurus
48
43
Geomancer
Draw Out
Counter Flood
Attack UP
Jump+2

Kiyomori
Crystal Shield
Feather Hat
Secret Clothes
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Bizen Boat, Heaven's Cloud



Deathmaker06
Female
Scorpio
64
41
Dancer
Punch Art
Speed Save
Martial Arts
Move-HP Up

Cashmere

Red Hood
Mystic Vest
Diamond Armlet

Witch Hunt, Slow Dance, Dragon Pit
Revive

Player: !Green
Team: Green Team
Palettes: Green/White



DeathTaxesAndAnime
Female
Taurus
67
58
Mime

Damage Split
Equip Armor
Move+2



Diamond Helmet
Plate Mail
Sprint Shoes

Mimic




Lastly
Male
Serpentarius
59
74
Mime

Counter
Martial Arts
Lava Walking



Twist Headband
Power Sleeve
Elf Mantle

Mimic




Mirapoix
Female
Aquarius
57
45
Samurai
Item
MA Save
Equip Gun
Jump+2

Romanda Gun

Barbuta
Linen Robe
Reflect Ring

Asura, Koutetsu, Murasame
X-Potion, Eye Drop, Maiden's Kiss, Soft



ArchKnightX
Male
Taurus
78
47
Priest
Talk Skill
Distribute
Equip Axe
Lava Walking

Giant Axe

Green Beret
Judo Outfit
Elf Mantle

Reraise, Regen, Shell, Shell 2, Esuna
Preach, Solution, Insult, Negotiate, Refute, Rehabilitate

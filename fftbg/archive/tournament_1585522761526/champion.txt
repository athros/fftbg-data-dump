Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lodrak
Male
Sagittarius
55
77
Monk
Jump
MA Save
Doublehand
Jump+3



Green Beret
Earth Clothes
Genji Gauntlet

Wave Fist, Earth Slash, Purification, Chakra, Revive
Level Jump8, Vertical Jump8



Dothan89
Female
Gemini
57
69
Geomancer
Punch Art
Dragon Spirit
Equip Sword
Move+1

Platinum Sword
Flame Shield
Headgear
Black Robe
Angel Ring

Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Pummel, Wave Fist, Earth Slash, Purification, Revive



Jhazor
Male
Virgo
54
52
Archer
Summon Magic
Hamedo
Beastmaster
Ignore Height

Mythril Gun
Round Shield
Triangle Hat
Mystic Vest
Spike Shoes

Charge+2, Charge+4, Charge+7
Moogle, Shiva, Bahamut, Fairy, Lich, Cyclops



SleepyBenzo
Male
Scorpio
60
57
Chemist
Yin Yang Magic
Dragon Spirit
Magic Defense UP
Waterwalking

Hydra Bag

Golden Hairpin
Brigandine
Setiemson

X-Potion, Ether, Hi-Ether, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Blind, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Sleep, Dark Holy

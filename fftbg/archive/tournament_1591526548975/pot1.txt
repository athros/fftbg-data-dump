Final Bets: red - 10 bets for 8,318G (16.6%, x5.04); blue - 6 bets for 41,888G (83.4%, x0.20)

red bets:
Vaxaldan: 2,146G (25.8%, 2,146G)
Sairentozon7: 1,072G (12.9%, 1,072G)
VolgraTheMoose: 1,001G (12.0%, 9,161G)
conradharlock: 1,000G (12.0%, 1,988G)
BirbBrainsBot: 1,000G (12.0%, 155,430G)
DeathTaxesAndAnime: 938G (11.3%, 938G)
Rislyeu: 500G (6.0%, 3,034G)
waterwatereverywhere: 400G (4.8%, 8,061G)
nhammen: 161G (1.9%, 161G)
datadrivenbot: 100G (1.2%, 35,632G)

blue bets:
Mesmaster: 38,666G (92.3%, 57,566G)
roqqqpsi: 1,874G (4.5%, 17,040G)
Zachara: 1,000G (2.4%, 103,000G)
prince_rogers_nelson_: 200G (0.5%, 200G)
Celdia: 100G (0.2%, 2,157G)
getthemoneyz: 48G (0.1%, 841,507G)

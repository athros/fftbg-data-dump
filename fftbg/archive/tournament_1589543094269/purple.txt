Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SuicideKing XIX
Female
Cancer
68
42
Dancer
Item
Sunken State
Defend
Jump+2

Cashmere

Twist Headband
Secret Clothes
Red Shoes

Wiznaibus, Disillusion, Last Dance, Dragon Pit
Potion, X-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down



Zachara
Female
Virgo
49
62
Wizard
Talk Skill
Auto Potion
Magic Defense UP
Ignore Terrain

Dragon Rod

Twist Headband
White Robe
Diamond Armlet

Fire 4, Bolt 2, Bolt 3, Frog, Death
Praise, Preach



Aestheta
Male
Aries
46
66
Ninja
White Magic
Faith Up
Short Status
Waterbreathing

Morning Star
Morning Star
Holy Miter
Brigandine
Reflect Ring

Bomb, Knife, Dictionary
Cure 2, Cure 3, Raise, Shell, Wall



PatSouI
Male
Capricorn
70
70
Ninja
Talk Skill
Distribute
Equip Polearm
Move+2

Mythril Spear
Obelisk
Red Hood
Wizard Outfit
Leather Mantle

Bomb
Praise, Preach, Solution, Negotiate, Refute, Rehabilitate

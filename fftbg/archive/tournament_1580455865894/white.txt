Player: !White
Team: White Team
Palettes: White/Blue



MaxCrowbar
Male
Scorpio
46
58
Mime

Counter Tackle
Dual Wield
Waterwalking



Flash Hat
Earth Clothes
Feather Mantle

Mimic




IronMongrel
Male
Gemini
53
56
Summoner
Elemental
PA Save
Short Charge
Waterwalking

Flame Rod

Feather Hat
Silk Robe
Sprint Shoes

Moogle, Ifrit, Titan, Bahamut, Leviathan, Salamander
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



HV5H
Monster
Capricorn
48
72
Blue Dragon










Dakren
Male
Capricorn
60
66
Knight
Basic Skill
Blade Grasp
Equip Knife
Teleport

Short Edge
Ice Shield
Iron Helmet
Reflect Mail
Leather Mantle

Head Break, Weapon Break, Speed Break, Dark Sword, Night Sword
Accumulate, Heal, Tickle, Cheer Up, Scream

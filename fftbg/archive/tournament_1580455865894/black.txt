Player: !Black
Team: Black Team
Palettes: Black/Red



Skipias
Monster
Cancer
80
43
Bull Demon










Willjin
Male
Aquarius
61
54
Knight
Basic Skill
Counter Flood
Equip Armor
Levitate

Excalibur

Red Hood
Reflect Mail
Genji Gauntlet

Head Break, Stasis Sword
Dash, Heal, Yell



Sun Pixie
Female
Leo
62
66
Monk
Yin Yang Magic
Arrow Guard
Equip Armor
Ignore Height



Leather Hat
Power Sleeve
Feather Mantle

Pummel, Purification, Chakra, Revive, Seal Evil
Spell Absorb, Doubt Faith, Silence Song, Dispel Magic, Sleep



Spideycloned
Female
Taurus
58
44
Squire
Summon Magic
Meatbone Slash
Beastmaster
Move+2

Night Killer

Green Beret
Chameleon Robe
Sprint Shoes

Dash, Throw Stone, Heal, Cheer Up
Moogle, Ramuh, Bahamut, Fairy

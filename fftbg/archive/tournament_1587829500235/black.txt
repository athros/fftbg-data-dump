Player: !Black
Team: Black Team
Palettes: Black/Red



CosmicTactician
Female
Pisces
69
74
Lancer
Talk Skill
Damage Split
Monster Talk
Move+2

Mythril Spear
Venetian Shield
Platinum Helmet
Mythril Armor
Diamond Armlet

Level Jump5, Vertical Jump7
Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Insult, Mimic Daravon



Placeholdercats
Male
Pisces
51
42
Wizard
Sing
Regenerator
Sicken
Move+2

Poison Rod

Red Hood
Wizard Outfit
Rubber Shoes

Fire, Bolt 3, Frog
Cheer Song, Battle Song, Sky Demon



DrAntiSocial
Female
Leo
63
72
Archer
Steal
Sunken State
Doublehand
Move-HP Up

Glacier Gun

Twist Headband
Brigandine
Cursed Ring

Charge+1, Charge+2, Charge+3, Charge+7
Gil Taking, Steal Weapon, Leg Aim



Maakur
Female
Libra
43
44
Wizard
Dance
PA Save
Equip Shield
Jump+2

Thunder Rod
Bronze Shield
Headgear
Silk Robe
108 Gems

Fire 3, Bolt, Bolt 3, Bolt 4, Ice 2, Flare
Witch Hunt, Wiznaibus, Polka Polka, Obsidian Blade

Player: !Black
Team: Black Team
Palettes: Black/Red



Reddwind
Female
Sagittarius
51
74
Knight
Draw Out
Earplug
Dual Wield
Move+3

Battle Axe
Giant Axe
Leather Helmet
Light Robe
Reflect Ring

Head Break, Armor Break, Shield Break, Speed Break, Power Break
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



Choco Joe
Female
Sagittarius
61
56
Time Mage
Battle Skill
Faith Save
Martial Arts
Move+1



Leather Hat
Judo Outfit
Power Wrist

Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Stabilize Time
Head Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break



Serperemagus
Monster
Gemini
80
58
Ultima Demon










Ruebyy
Female
Gemini
62
78
Summoner
Basic Skill
Arrow Guard
Maintenance
Ignore Height

Rainbow Staff

Triangle Hat
Light Robe
Leather Mantle

Ifrit, Titan, Carbunkle, Leviathan, Silf
Dash, Yell, Cheer Up, Fury, Wish

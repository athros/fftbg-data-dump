Final Bets: green - 7 bets for 10,196G (50.3%, x0.99); yellow - 11 bets for 10,055G (49.7%, x1.01)

green bets:
ShintaroNayaka: 5,430G (53.3%, 5,430G)
KyleWonToLiveForever: 1,032G (10.1%, 1,032G)
getthemoneyz: 1,000G (9.8%, 1,059,805G)
BirbBrainsBot: 1,000G (9.8%, 13,044G)
Draconis345: 1,000G (9.8%, 55,967G)
reddwind_: 510G (5.0%, 510G)
Ruvelia_BibeI: 224G (2.2%, 224G)

yellow bets:
Evewho: 2,000G (19.9%, 5,893G)
Error72: 2,000G (19.9%, 14,828G)
Mesmaster: 2,000G (19.9%, 89,829G)
BlackFireUK: 1,000G (9.9%, 39,670G)
prince_rogers_nelson_: 994G (9.9%, 994G)
serperemagus: 544G (5.4%, 544G)
CosmicTactician: 500G (5.0%, 20,613G)
dogsandcatsand: 416G (4.1%, 416G)
Rytor: 300G (3.0%, 7,401G)
gorgewall: 201G (2.0%, 25,923G)
datadrivenbot: 100G (1.0%, 49,515G)

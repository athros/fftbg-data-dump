Final Bets: red - 6 bets for 2,991G (30.7%, x2.26); blue - 9 bets for 6,753G (69.3%, x0.44)

red bets:
Draconis345: 1,000G (33.4%, 56,967G)
serperemagus: 540G (18.1%, 540G)
dogsandcatsand: 517G (17.3%, 517G)
BirbBrainsBot: 406G (13.6%, 13,450G)
getthemoneyz: 304G (10.2%, 1,060,109G)
Ruvelia_BibeI: 224G (7.5%, 224G)

blue bets:
ShintaroNayaka: 3,763G (55.7%, 3,763G)
NovaKnight21: 1,000G (14.8%, 7,349G)
prince_rogers_nelson_: 689G (10.2%, 689G)
CosmicTactician: 500G (7.4%, 20,392G)
gorgewall: 201G (3.0%, 25,834G)
Rytor: 200G (3.0%, 7,312G)
Evewho: 200G (3.0%, 5,804G)
nifboy: 100G (1.5%, 5,727G)
datadrivenbot: 100G (1.5%, 49,471G)

Player: !Green
Team: Green Team
Palettes: Green/White



HaychDub
Female
Pisces
60
69
Calculator
Time Magic
Parry
Secret Hunt
Jump+3

Papyrus Codex

Holy Miter
Linen Robe
Defense Armlet

CT, Height, Prime Number, 5, 4, 3
Immobilize, Reflect, Demi, Meteor



Volgrathemoose
Male
Serpentarius
48
53
Wizard
Draw Out
Earplug
Equip Shield
Jump+1

Mythril Knife
Platinum Shield
Leather Hat
Wizard Outfit
Rubber Shoes

Fire 2, Bolt 2, Bolt 3, Ice, Ice 4, Frog, Death
Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



Lesserkarl
Male
Pisces
69
55
Monk
Elemental
Counter
Secret Hunt
Waterbreathing



Twist Headband
Brigandine
108 Gems

Spin Fist, Pummel, Secret Fist, Chakra, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Reverie3
Female
Leo
70
62
Lancer
Draw Out
PA Save
Attack UP
Retreat

Javelin
Gold Shield
Gold Helmet
Leather Armor
Small Mantle

Level Jump8, Vertical Jump7
Bizen Boat, Murasame, Muramasa

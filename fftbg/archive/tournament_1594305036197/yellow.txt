Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Alterworlds
Male
Taurus
55
65
Knight
Throw
Abandon
Equip Sword
Jump+1

Ancient Sword
Bronze Shield
Iron Helmet
Mythril Armor
Salty Rage

Head Break, Shield Break, Magic Break, Justice Sword, Dark Sword
Staff



Grandlanzer
Male
Pisces
59
67
Time Mage
Summon Magic
Meatbone Slash
Doublehand
Move+1

Wizard Staff

Triangle Hat
Power Sleeve
Bracer

Haste, Haste 2, Immobilize, Demi
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Bahamut, Lich, Cyclops



Run With Stone GUNs
Male
Gemini
68
40
Archer
Basic Skill
Meatbone Slash
Beastmaster
Jump+1

Bow Gun
Escutcheon
Thief Hat
Adaman Vest
Battle Boots

Charge+4, Charge+5, Charge+10
Heal, Tickle, Scream



Zmoses
Male
Aquarius
65
61
Lancer
Summon Magic
Catch
Equip Bow
Teleport

Ultimus Bow

Iron Helmet
Gold Armor
Diamond Armlet

Level Jump2, Vertical Jump4
Moogle, Golem, Carbunkle, Bahamut, Fairy

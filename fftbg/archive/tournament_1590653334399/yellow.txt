Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Female
Capricorn
41
77
Priest
Time Magic
Regenerator
Equip Sword
Move+2

Koutetsu Knife

Flash Hat
Wizard Robe
Defense Armlet

Cure, Raise 2, Regen, Protect, Protect 2, Shell, Wall, Esuna
Haste, Haste 2, Slow, Immobilize, Demi, Stabilize Time



HorusTaurus
Male
Gemini
69
38
Lancer
Talk Skill
Auto Potion
Secret Hunt
Move+2

Javelin
Round Shield
Bronze Helmet
Diamond Armor
Power Wrist

Level Jump2, Vertical Jump4
Threaten, Preach, Refute, Rehabilitate



Striator
Male
Virgo
43
53
Monk
Draw Out
Parry
Short Status
Retreat



Feather Hat
Power Sleeve
Feather Boots

Spin Fist, Pummel, Purification
Bizen Boat, Murasame, Kikuichimoji



BuffaloCrunch
Female
Taurus
77
62
Wizard
Draw Out
Damage Split
Short Status
Jump+1

Poison Rod

Flash Hat
Linen Robe
Feather Boots

Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Frog
Bizen Boat, Heaven's Cloud

Final Bets: black - 10 bets for 3,601G (60.1%, x0.66); brown - 7 bets for 2,390G (39.9%, x1.51)

black bets:
BirbBrainsBot: 1,000G (27.8%, 36,725G)
Mesmaster: 1,000G (27.8%, 98,935G)
Digitalsocrates: 500G (13.9%, 5,080G)
gogofromtogo: 216G (6.0%, 216G)
gorgewall: 201G (5.6%, 20,653G)
thenextlantern: 200G (5.6%, 204G)
TeaTime29: 200G (5.6%, 1,875G)
gr8keeper: 108G (3.0%, 706G)
datadrivenbot: 100G (2.8%, 28,044G)
getthemoneyz: 76G (2.1%, 753,298G)

brown bets:
Rislyeu: 769G (32.2%, 769G)
AllInBot: 557G (23.3%, 557G)
SkyridgeZero: 500G (20.9%, 21,009G)
itsdigitalbro: 252G (10.5%, 252G)
KyleWonToLiveForever: 112G (4.7%, 112G)
Evewho: 100G (4.2%, 13,114G)
Klednar21: 100G (4.2%, 108G)

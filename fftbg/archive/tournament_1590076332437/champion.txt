Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Roqqqpsi
Female
Aquarius
71
53
Calculator
Black Magic
Mana Shield
Short Charge
Move-HP Up

Bestiary

Headgear
Chameleon Robe
Leather Mantle

CT, Prime Number, 5, 4, 3
Fire, Fire 2, Fire 3, Bolt 3, Ice, Ice 3



Marin1987
Female
Scorpio
46
43
Monk
Time Magic
Distribute
Defend
Teleport



Holy Miter
Brigandine
Dracula Mantle

Pummel, Earth Slash, Purification, Revive
Haste, Slow 2, Stop, Immobilize, Reflect, Demi 2, Stabilize Time



Hamborn
Male
Libra
58
74
Mime

Absorb Used MP
Defend
Move-HP Up



Thief Hat
Chain Vest
Elf Mantle

Mimic




MattMan119
Male
Serpentarius
80
73
Mime

Abandon
Martial Arts
Jump+1



Twist Headband
Judo Outfit
Small Mantle

Mimic


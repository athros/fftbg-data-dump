Player: !Red
Team: Red Team
Palettes: Red/Brown



Hamborn
Female
Scorpio
45
47
Dancer
Black Magic
Caution
Equip Armor
Levitate

Cute Bag

Holy Miter
Mythril Armor
Cursed Ring

Witch Hunt, Wiznaibus, Slow Dance
Fire 3, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4



Volgrathemoose
Female
Virgo
62
67
Summoner
Draw Out
Earplug
Short Charge
Move+2

Battle Folio

Feather Hat
Chain Vest
Spike Shoes

Moogle, Shiva, Ramuh, Titan, Golem, Salamander, Lich
Muramasa



Hydroshade
Female
Scorpio
66
49
Archer
White Magic
Critical Quick
Attack UP
Move+3

Mythril Gun
Ice Shield
Cross Helmet
Earth Clothes
Feather Boots

Charge+5, Charge+7, Charge+20
Cure, Raise, Protect, Wall, Esuna



Lawnboxer
Male
Virgo
50
55
Lancer
Sing
Damage Split
Dual Wield
Ignore Height

Partisan
Javelin
Gold Helmet
Mythril Armor
Elf Mantle

Level Jump3, Vertical Jump7
Battle Song, Nameless Song, Diamond Blade, Space Storage, Sky Demon

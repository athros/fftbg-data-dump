Player: !Black
Team: Black Team
Palettes: Black/Red



Rislyeu
Monster
Taurus
72
51
Tiamat










DavenIII
Male
Pisces
51
45
Lancer
Talk Skill
Sunken State
Maintenance
Move+2

Cypress Rod
Escutcheon
Mythril Helmet
Carabini Mail
Vanish Mantle

Level Jump8, Vertical Jump5
Solution, Mimic Daravon, Refute, Rehabilitate



Toka222
Female
Aquarius
78
58
Samurai
Charge
Damage Split
Magic Attack UP
Move-MP Up

Bizen Boat

Platinum Helmet
Genji Armor
Spike Shoes

Koutetsu, Kikuichimoji
Charge+1, Charge+5, Charge+10



FoeSquirrel
Male
Serpentarius
43
72
Time Mage
Elemental
MA Save
Equip Axe
Move+1

Battle Axe

Feather Hat
Black Robe
Jade Armlet

Stop, Demi, Stabilize Time, Meteor
Hell Ivy, Sand Storm, Blizzard, Gusty Wind

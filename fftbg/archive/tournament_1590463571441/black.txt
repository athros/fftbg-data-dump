Player: !Black
Team: Black Team
Palettes: Black/Red



HaateXIII
Male
Aquarius
63
61
Knight
Time Magic
Sunken State
Long Status
Ignore Height

Defender
Platinum Shield
Leather Helmet
Chameleon Robe
Feather Mantle

Armor Break, Stasis Sword, Justice Sword, Dark Sword
Haste, Immobilize, Float, Reflect



Byrdturbo
Male
Aries
42
44
Mime

Absorb Used MP
Equip Armor
Move+3



Leather Helmet
Silk Robe
Magic Gauntlet

Mimic




Serperemagus
Male
Sagittarius
68
72
Oracle
Math Skill
Auto Potion
Defend
Waterbreathing

Gokuu Rod

Golden Hairpin
Wizard Robe
Defense Ring

Poison, Spell Absorb, Doubt Faith, Foxbird, Dispel Magic
CT, Height, 4



Xcessive30
Male
Aquarius
66
55
Monk
Basic Skill
Absorb Used MP
Sicken
Move+2



Green Beret
Mythril Vest
Wizard Mantle

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Accumulate, Heal, Wish

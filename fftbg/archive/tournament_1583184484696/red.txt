Player: !Red
Team: Red Team
Palettes: Red/Brown



Theatheologist
Female
Leo
43
74
Calculator
Black Magic
Speed Save
Equip Sword
Swim

Ice Brand

Red Hood
Black Robe
Feather Mantle

CT, Height, Prime Number, 3
Fire 3, Bolt 2, Ice, Ice 2



Gelwain
Female
Aries
74
72
Samurai
Battle Skill
Caution
Short Charge
Move+2

Murasame

Genji Helmet
Light Robe
Jade Armlet

Asura, Kiyomori
Head Break, Stasis Sword



LuckyLuckLuc2
Male
Sagittarius
58
49
Monk
Summon Magic
Mana Shield
Equip Knife
Fly

Sasuke Knife

Triangle Hat
Adaman Vest
Spike Shoes

Wave Fist, Earth Slash, Secret Fist, Revive
Shiva, Ifrit, Carbunkle, Odin, Leviathan, Fairy



Shs
Male
Leo
64
69
Summoner
Throw
Auto Potion
Equip Shield
Teleport

Dragon Rod
Crystal Shield
Cachusha
White Robe
Spike Shoes

Moogle, Ifrit, Titan, Odin, Lich
Shuriken, Bomb

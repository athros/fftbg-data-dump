Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sneeza
Male
Capricorn
66
72
Geomancer
Charge
Regenerator
Equip Shield
Swim

Heaven's Cloud
Genji Shield
Red Hood
Chameleon Robe
Feather Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+2, Charge+3, Charge+4



Leaveustankin
Female
Pisces
63
44
Wizard
Draw Out
PA Save
Dual Wield
Swim

Orichalcum
Assassin Dagger
Triangle Hat
Black Robe
Power Wrist

Fire 3, Bolt 2, Death
Bizen Boat, Murasame, Kikuichimoji, Masamune



Ragnarok784
Male
Virgo
59
44
Ninja
Black Magic
Hamedo
Concentrate
Ignore Terrain

Kunai
Orichalcum
Feather Hat
Brigandine
Red Shoes

Shuriken, Wand
Fire, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Empower



PandaBattalion
Female
Taurus
49
68
Priest
Yin Yang Magic
Speed Save
Defend
Move-MP Up

Morning Star

Cachusha
Adaman Vest
Cursed Ring

Cure, Cure 2, Raise, Reraise, Regen, Shell, Esuna
Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep

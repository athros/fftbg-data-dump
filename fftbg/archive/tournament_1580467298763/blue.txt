Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



AWellTrainedFerret
Female
Capricorn
45
55
Calculator
Yin Yang Magic
Mana Shield
Beastmaster
Ignore Height

Battle Folio

Black Hood
Chameleon Robe
Spike Shoes

CT, Height, Prime Number, 5, 3
Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze



Adenis222
Male
Scorpio
81
50
Lancer
Talk Skill
Earplug
Attack UP
Lava Walking

Javelin
Buckler
Platinum Helmet
Wizard Robe
Magic Ring

Level Jump8, Vertical Jump8
Persuade, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



Cyuub
Female
Leo
79
40
Calculator
Yin Yang Magic
Damage Split
Sicken
Move+1

Bestiary

Golden Hairpin
Brigandine
N-Kai Armlet

CT, Prime Number, 5, 4
Pray Faith, Silence Song, Foxbird, Dispel Magic, Sleep, Dark Holy



Binjin
Female
Aquarius
42
56
Knight
Draw Out
Faith Up
Dual Wield
Lava Walking

Save the Queen
Giant Axe
Mythril Helmet
Silk Robe
N-Kai Armlet

Armor Break, Shield Break, Night Sword
Muramasa

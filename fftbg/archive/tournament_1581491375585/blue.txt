Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JIDkomu
Male
Libra
57
57
Lancer
Talk Skill
Counter Flood
Monster Talk
Lava Walking

Holy Lance
Crystal Shield
Iron Helmet
Wizard Robe
Angel Ring

Level Jump4, Vertical Jump7
Invitation, Praise, Solution, Insult, Refute, Rehabilitate



Sotehr
Male
Aries
68
70
Archer
Black Magic
Auto Potion
Doublehand
Move-MP Up

Lightning Bow

Thief Hat
Power Sleeve
Defense Ring

Charge+2, Charge+3, Charge+5
Bolt, Bolt 3, Ice



Kinsleth
Female
Cancer
74
64
Samurai
White Magic
Counter Tackle
Short Status
Lava Walking

Koutetsu Knife

Circlet
Chameleon Robe
Rubber Shoes

Murasame
Cure 3, Raise, Shell 2, Wall



Fireball87
Female
Pisces
50
68
Time Mage
White Magic
Meatbone Slash
Equip Gun
Jump+1

Healing Staff

Twist Headband
Brigandine
Feather Boots

Haste, Haste 2, Slow 2, Demi, Demi 2, Stabilize Time
Cure 4, Raise, Raise 2, Reraise, Protect 2, Esuna

Player: !Red
Team: Red Team
Palettes: Red/Brown



Steveicus
Female
Sagittarius
63
41
Oracle
Jump
Earplug
Halve MP
Move-MP Up

Cypress Rod

Feather Hat
Light Robe
Elf Mantle

Blind, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Level Jump4, Vertical Jump3



Birdlady420
Male
Aries
67
75
Mediator
Summon Magic
Catch
Equip Armor
Levitate

Bestiary

Platinum Helmet
Carabini Mail
Germinas Boots

Invitation, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute
Moogle, Titan, Carbunkle, Leviathan, Lich, Cyclops



Sypheck
Male
Taurus
37
68
Thief
Black Magic
Blade Grasp
Attack UP
Fly

Dagger

Red Hood
Earth Clothes
Magic Gauntlet

Gil Taking, Steal Heart, Steal Status, Arm Aim
Bolt, Bolt 2, Bolt 3, Ice 2, Ice 4, Frog



ANFz
Female
Libra
52
64
Time Mage
Item
Mana Shield
Sicken
Levitate

Battle Folio

Leather Hat
Power Sleeve
Magic Gauntlet

Slow 2, Stop, Quick, Demi
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Breakdown777
Female
Virgo
73
56
Oracle
Summon Magic
Speed Save
Short Charge
Move+1

Iron Fan

Leather Hat
Silk Robe
Dracula Mantle

Life Drain, Pray Faith, Confusion Song, Dispel Magic, Dark Holy
Moogle, Ramuh, Carbunkle, Odin, Lich



Nickyfive
Female
Cancer
73
49
Priest
Draw Out
Speed Save
Long Status
Move+3

Wizard Staff

Golden Hairpin
Robe of Lords
Bracer

Cure, Cure 4, Raise, Raise 2, Reraise, Shell, Shell 2, Wall, Esuna
Koutetsu, Murasame, Heaven's Cloud, Muramasa



Blinkmachine
Male
Libra
69
53
Ninja
Jump
Critical Quick
Short Status
Waterwalking

Koga Knife
Flame Whip
Golden Hairpin
Black Costume
Battle Boots

Shuriken, Bomb, Knife, Hammer, Axe, Stick
Level Jump5, Vertical Jump6



Gooseyourself
Male
Scorpio
59
78
Ninja
Draw Out
Arrow Guard
Equip Polearm
Waterbreathing

Gungnir
Javelin
Thief Hat
Mystic Vest
Magic Gauntlet

Shuriken, Bomb, Knife
Koutetsu, Heaven's Cloud, Muramasa

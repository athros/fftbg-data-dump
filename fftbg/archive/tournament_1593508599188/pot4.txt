Final Bets: purple - 6 bets for 7,464G (62.3%, x0.60); brown - 12 bets for 4,514G (37.7%, x1.65)

purple bets:
prince_rogers_nelson_: 2,941G (39.4%, 2,941G)
KasugaiRoastedPeas: 2,222G (29.8%, 16,546G)
twelfthrootoftwo: 1,000G (13.4%, 24,082G)
Evewho: 500G (6.7%, 5,634G)
shs_: 500G (6.7%, 15,308G)
gorgewall: 301G (4.0%, 28,376G)

brown bets:
BirbBrainsBot: 1,000G (22.2%, 156,554G)
Lemonjohns: 852G (18.9%, 852G)
Digitalsocrates: 634G (14.0%, 634G)
reinoe: 500G (11.1%, 11,903G)
dem0nj0ns: 500G (11.1%, 999G)
Moshyhero: 236G (5.2%, 236G)
CT_5_Holy: 200G (4.4%, 1,796G)
getthemoneyz: 188G (4.2%, 1,113,864G)
Smokegiant: 104G (2.3%, 104G)
DuneMeta: 100G (2.2%, 111G)
victoriolue: 100G (2.2%, 4,522G)
datadrivenbot: 100G (2.2%, 46,559G)

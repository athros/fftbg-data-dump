Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Jerdensperg
Male
Sagittarius
66
71
Bard
Battle Skill
Brave Up
Equip Shield
Teleport

Fairy Harp
Flame Shield
Golden Hairpin
Secret Clothes
Battle Boots

Angel Song, Nameless Song, Diamond Blade
Armor Break, Weapon Break, Justice Sword, Night Sword, Surging Sword



Catboysbestboys
Female
Scorpio
68
65
Priest
Basic Skill
PA Save
Equip Knife
Ignore Terrain

Main Gauche

Holy Miter
Silk Robe
Cursed Ring

Cure, Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Esuna
Dash, Heal, Wish



Strumisgod
Female
Aquarius
73
64
Knight
Time Magic
Mana Shield
Attack UP
Fly

Battle Axe
Aegis Shield
Cross Helmet
Plate Mail
Genji Gauntlet

Magic Break, Power Break, Stasis Sword, Night Sword
Haste 2, Slow, Immobilize, Quick, Demi 2, Stabilize Time



KSops
Male
Scorpio
61
47
Oracle
Jump
Sunken State
Equip Armor
Ignore Terrain

Iron Fan

Barbuta
Plate Mail
Dracula Mantle

Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep, Petrify
Level Jump5, Vertical Jump2

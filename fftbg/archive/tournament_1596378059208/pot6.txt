Final Bets: white - 8 bets for 7,258G (43.0%, x1.33); brown - 11 bets for 9,637G (57.0%, x0.75)

white bets:
NIghtdew14: 3,195G (44.0%, 21,304G)
douchetron: 1,103G (15.2%, 2,164G)
maximumcrit: 1,000G (13.8%, 10,246G)
GoatShow2: 783G (10.8%, 783G)
killth3kid: 592G (8.2%, 9,651G)
skipsandwiches: 284G (3.9%, 284G)
datadrivenbot: 200G (2.8%, 49,535G)
loveyouallfriends: 101G (1.4%, 15,863G)

brown bets:
reinoe: 3,000G (31.1%, 65,375G)
RaIshtar: 1,001G (10.4%, 3,791G)
BirbBrainsBot: 1,000G (10.4%, 144,680G)
Thyrandaal: 1,000G (10.4%, 71,280G)
E_Ballard: 772G (8.0%, 772G)
Nizaha: 644G (6.7%, 644G)
Mister_Quof: 641G (6.7%, 641G)
AllInBot: 621G (6.4%, 621G)
Shalloween: 600G (6.2%, 21,853G)
rNdOrchestra: 200G (2.1%, 2,707G)
getthemoneyz: 158G (1.6%, 1,478,439G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Aldrammech
Monster
Cancer
64
67
Ochu










E Ballard
Female
Gemini
76
69
Archer
Dance
Dragon Spirit
Equip Polearm
Move+3

Battle Bamboo
Buckler
Gold Helmet
Mythril Vest
Battle Boots

Charge+1, Charge+2, Charge+3, Charge+7, Charge+10
Witch Hunt, Slow Dance, Last Dance, Void Storage, Dragon Pit



Maximumcrit
Male
Aries
61
42
Mediator
Elemental
Parry
Short Status
Lava Walking

Battle Folio

Golden Hairpin
Leather Outfit
Bracer

Persuade, Preach, Solution, Insult, Mimic Daravon, Refute
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Upvla
Male
Gemini
56
53
Monk
Summon Magic
Abandon
Magic Defense UP
Move+2



Red Hood
Rubber Costume
Jade Armlet

Spin Fist, Pummel, Earth Slash, Revive
Moogle, Leviathan, Silf, Cyclops

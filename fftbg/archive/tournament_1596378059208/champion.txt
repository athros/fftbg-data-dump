Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Grininda
Male
Sagittarius
73
41
Ninja
Punch Art
Damage Split
Equip Gun
Jump+2

Mythril Gun
Blast Gun
Golden Hairpin
Clothes
Chantage

Shuriken, Bomb
Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil



Lastly
Male
Pisces
81
81
Wizard
Steal
Hamedo
Dual Wield
Move+3

Rod
Poison Rod
Holy Miter
White Robe
Feather Mantle

Fire, Fire 2, Fire 4, Bolt, Bolt 3, Ice 2, Ice 4, Frog, Death
Steal Armor, Steal Accessory, Leg Aim



RaIshtar
Female
Scorpio
61
48
Mediator
Dance
Earplug
Attack UP
Jump+1

Romanda Gun

Twist Headband
Linen Robe
Dracula Mantle

Persuade, Negotiate
Disillusion, Nether Demon



Mister Quof
Male
Pisces
55
67
Lancer
Draw Out
Counter Magic
Equip Axe
Move-HP Up

Flame Whip
Diamond Shield
Genji Helmet
Carabini Mail
N-Kai Armlet

Level Jump4, Vertical Jump6
Asura, Koutetsu, Murasame

Player: !White
Team: White Team
Palettes: White/Blue



Bongomon7
Male
Scorpio
64
65
Summoner
Draw Out
Sunken State
Equip Shield
Move-MP Up

Sage Staff
Bronze Shield
Red Hood
Mythril Vest
Vanish Mantle

Titan, Golem, Carbunkle, Odin, Salamander, Fairy
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Chirijiraden



Rislyeu
Male
Sagittarius
69
44
Mime

Auto Potion
Equip Shield
Move+3


Bronze Shield
Barbuta
Clothes
Cursed Ring

Mimic




Serperemagus
Male
Pisces
60
73
Time Mage
Throw
MA Save
Equip Armor
Retreat

Battle Bamboo

Black Hood
Mythril Armor
Spike Shoes

Immobilize, Quick
Shuriken, Ninja Sword



Zagorsek
Male
Gemini
56
55
Thief
Elemental
Meatbone Slash
Secret Hunt
Move+3

Hidden Knife

Golden Hairpin
Mystic Vest
Angel Ring

Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

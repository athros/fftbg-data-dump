Final Bets: red - 20 bets for 21,737G (67.6%, x0.48); champion - 12 bets for 10,405G (32.4%, x2.09)

red bets:
gorgewall: 4,391G (20.2%, 4,391G)
latebit: 4,165G (19.2%, 8,168G)
thaetreis: 2,400G (11.0%, 49,216G)
ko2q: 2,119G (9.7%, 2,119G)
dogsandcatsand: 2,000G (9.2%, 104,508G)
reinoe: 1,000G (4.6%, 81,174G)
BirbBrainsBot: 1,000G (4.6%, 24,516G)
Thyrandaal: 1,000G (4.6%, 119,507G)
killth3kid: 616G (2.8%, 4,219G)
maximumcrit: 606G (2.8%, 606G)
Zeroroute: 600G (2.8%, 600G)
DarrenDinosaurs: 588G (2.7%, 588G)
butterbelljedi: 288G (1.3%, 288G)
AllInBot: 200G (0.9%, 200G)
datadrivenbot: 200G (0.9%, 49,275G)
ArlanKels: 150G (0.7%, 5,132G)
getthemoneyz: 106G (0.5%, 1,516,126G)
Gunz232323: 104G (0.5%, 104G)
JonnyCue: 104G (0.5%, 104G)
Wooplestein: 100G (0.5%, 368G)

champion bets:
amiture: 3,000G (28.8%, 36,194G)
RaIshtar: 1,432G (13.8%, 1,432G)
Shalloween: 1,400G (13.5%, 50,449G)
maakur_: 1,089G (10.5%, 1,089G)
Laserman1000: 800G (7.7%, 4,700G)
OneHundredFists: 660G (6.3%, 660G)
ShintaroNayaka: 500G (4.8%, 3,536G)
SuppleThink: 500G (4.8%, 3,754G)
DeathTaxesAndAnime: 400G (3.8%, 1,716G)
Lord_Gwarth: 268G (2.6%, 1,232G)
resjudicata3: 256G (2.5%, 256G)
Bioticism: 100G (1.0%, 17,815G)

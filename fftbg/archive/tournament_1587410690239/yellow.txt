Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ko2q
Male
Leo
67
54
Mime

HP Restore
Martial Arts
Teleport



Golden Hairpin
Silk Robe
Genji Gauntlet

Mimic




Hales Bopp It
Male
Sagittarius
67
44
Ninja
Draw Out
Meatbone Slash
Sicken
Levitate

Kunai
Assassin Dagger
Headgear
Power Sleeve
Angel Ring

Shuriken
Asura, Koutetsu, Murasame, Kiyomori, Muramasa



OneHundredFists
Male
Capricorn
63
80
Knight
Punch Art
Meatbone Slash
Long Status
Move+3

Coral Sword
Round Shield
Cross Helmet
Carabini Mail
Elf Mantle

Shield Break, Magic Break, Justice Sword
Secret Fist, Purification, Revive



LDSkinny
Female
Leo
41
58
Summoner
Yin Yang Magic
MP Restore
Short Charge
Swim

White Staff

Triangle Hat
Mythril Vest
Genji Gauntlet

Moogle, Ramuh, Silf
Poison, Spell Absorb, Foxbird

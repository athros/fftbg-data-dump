Player: !White
Team: White Team
Palettes: White/Blue



Lijarkh
Male
Gemini
38
74
Monk
Black Magic
Damage Split
Defense UP
Waterbreathing



Black Hood
Mythril Vest
Small Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 3, Ice 4, Empower, Flare



DrAntiSocial
Monster
Pisces
61
59
Black Goblin










Panushenko
Female
Gemini
53
57
Summoner
Jump
Damage Split
Short Charge
Jump+1

Poison Rod

Holy Miter
Chameleon Robe
Magic Gauntlet

Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Leviathan
Level Jump5, Vertical Jump7



Cataphract116
Male
Capricorn
50
46
Geomancer
Draw Out
Earplug
Doublehand
Move-MP Up

Slasher

Black Hood
Mythril Vest
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Bizen Boat, Murasame, Kiyomori

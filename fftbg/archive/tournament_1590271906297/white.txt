Player: !White
Team: White Team
Palettes: White/Blue



Powergems
Female
Aries
54
43
Chemist
Dance
Counter
Magic Attack UP
Move+3

Blaze Gun

Flash Hat
Secret Clothes
Defense Ring

Antidote, Eye Drop, Soft, Remedy
Wiznaibus, Nether Demon



Deathmaker06
Male
Gemini
49
69
Thief
Item
Counter Flood
Beastmaster
Jump+3

Blind Knife

Holy Miter
Adaman Vest
Wizard Mantle

Gil Taking, Steal Armor, Steal Weapon
Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Phoenix Down



Letdowncity
Female
Pisces
75
62
Squire
Time Magic
Counter Magic
Short Status
Teleport 2

Flail
Platinum Shield
Golden Hairpin
Mystic Vest
Magic Ring

Accumulate, Dash, Throw Stone, Yell, Fury, Wish
Slow 2, Immobilize, Float, Reflect, Stabilize Time



Estan AD
Male
Aries
60
50
Knight
Throw
Arrow Guard
Magic Attack UP
Swim

Defender
Flame Shield
Leather Helmet
Plate Mail
108 Gems

Head Break, Shield Break, Weapon Break, Speed Break, Power Break
Shuriken

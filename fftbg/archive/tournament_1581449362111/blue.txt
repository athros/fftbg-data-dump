Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ins4n1ty87
Female
Cancer
76
66
Mime

MA Save
Magic Defense UP
Lava Walking



Headgear
Adaman Vest
Sprint Shoes

Mimic




EvilLego6
Male
Capricorn
45
56
Priest
Summon Magic
Critical Quick
Short Status
Levitate

White Staff

Twist Headband
Wizard Robe
Small Mantle

Cure 3, Raise, Protect 2, Wall, Esuna, Holy
Ramuh, Carbunkle, Bahamut, Odin, Silf, Cyclops



Dasutin23
Male
Aries
58
51
Samurai
White Magic
Critical Quick
Doublehand
Ignore Height

Heaven's Cloud

Gold Helmet
Platinum Armor
Diamond Armlet

Asura, Bizen Boat, Murasame, Kiyomori, Muramasa
Cure 4, Raise, Reraise, Protect 2, Shell 2, Esuna, Magic Barrier



Cixron
Male
Scorpio
67
56
Squire
Summon Magic
Auto Potion
Equip Armor
Jump+2

Rune Blade
Genji Shield
Feather Hat
Linen Robe
Wizard Mantle

Dash, Heal, Tickle, Cheer Up, Wish
Moogle, Ifrit, Leviathan

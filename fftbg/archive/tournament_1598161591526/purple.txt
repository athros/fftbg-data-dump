Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nifboy
Female
Sagittarius
63
65
Calculator
Bio
Arrow Guard
Equip Sword
Fly

Muramasa

Red Hood
Mythril Armor
Defense Ring

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Galkife
Female
Capricorn
56
80
Time Mage
Jump
Meatbone Slash
Equip Knife
Waterwalking

Ice Rod

Leather Hat
Silk Robe
Jade Armlet

Haste, Stop, Immobilize, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Level Jump3, Vertical Jump7



Ar Tactic
Female
Virgo
64
61
Geomancer
Summon Magic
Hamedo
Attack UP
Fly

Giant Axe
Mythril Shield
Feather Hat
Mystic Vest
Power Wrist

Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Moogle, Fairy



Lythe Caraker
Female
Taurus
80
56
Geomancer
Black Magic
Regenerator
Defense UP
Jump+1

Giant Axe
Ice Shield
Headgear
Judo Outfit
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Bolt, Bolt 3, Bolt 4, Ice, Ice 4, Frog

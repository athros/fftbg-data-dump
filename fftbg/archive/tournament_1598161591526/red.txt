Player: !Red
Team: Red Team
Palettes: Red/Brown



Killth3kid
Female
Taurus
70
65
Priest
Item
Parry
Short Status
Move-HP Up

Healing Staff

Cachusha
Black Costume
Defense Ring

Cure, Cure 3, Raise, Regen, Protect 2, Esuna, Magic Barrier
Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Soft



ANFz
Male
Taurus
79
60
Thief
Item
MP Restore
Throw Item
Fly

Mythril Knife

Feather Hat
Mythril Vest
Rubber Shoes

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Potion, Holy Water, Remedy, Phoenix Down



Ruleof5
Female
Taurus
61
77
Archer
Elemental
Absorb Used MP
Dual Wield
Move-HP Up

Stone Gun
Glacier Gun
Thief Hat
Mythril Vest
Wizard Mantle

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+20
Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Gusty Wind



Forkmore
Female
Capricorn
61
78
Wizard
Summon Magic
Distribute
Short Charge
Move+1

Ice Rod

Leather Hat
Chain Vest
Spike Shoes

Fire 3, Fire 4, Bolt 4, Ice 2
Moogle, Lich

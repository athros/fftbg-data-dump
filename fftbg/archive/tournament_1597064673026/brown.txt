Player: !Brown
Team: Brown Team
Palettes: Brown/Green



PoroTact
Female
Aries
37
54
Lancer
Summon Magic
Speed Save
Magic Defense UP
Teleport

Mythril Spear
Buckler
Grand Helmet
Platinum Armor
Jade Armlet

Level Jump4, Vertical Jump2
Moogle, Carbunkle, Bahamut, Leviathan



Coralreeferz
Female
Scorpio
76
81
Dancer
Steal
Parry
Martial Arts
Move-MP Up

Panther Bag

Black Hood
Light Robe
Power Wrist

Wiznaibus, Polka Polka, Last Dance, Dragon Pit
Gil Taking, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Dogsandcatsand
Female
Leo
58
71
Wizard
Draw Out
Counter Flood
Defend
Move+2

Air Knife

Black Hood
Clothes
108 Gems

Fire, Fire 4, Bolt 3, Bolt 4, Ice, Ice 4, Flare
Koutetsu, Murasame



Ohhinm
Female
Sagittarius
42
49
Wizard
White Magic
HP Restore
Equip Axe
Waterbreathing

Gold Staff

Golden Hairpin
White Robe
Diamond Armlet

Fire, Bolt 3, Ice 2, Ice 3, Ice 4, Empower
Raise 2, Regen, Esuna

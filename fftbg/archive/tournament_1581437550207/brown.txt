Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nachtkrieger
Male
Capricorn
65
59
Monk
Summon Magic
Dragon Spirit
Equip Polearm
Fly

Cypress Rod

Twist Headband
Mythril Vest
Diamond Armlet

Wave Fist, Purification, Revive, Seal Evil
Leviathan, Salamander, Lich



HughJeffner
Male
Cancer
58
41
Oracle
Draw Out
Caution
Equip Sword
Fly

Koutetsu Knife

Flash Hat
Adaman Vest
Red Shoes

Poison, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Dark Holy
Kiyomori, Kikuichimoji



E Ballard
Male
Taurus
43
77
Mime

Counter
Equip Armor
Swim



Gold Helmet
Chameleon Robe
108 Gems

Mimic




RagequitSA
Female
Virgo
78
81
Priest
Jump
Distribute
Dual Wield
Fly

Flame Whip
Healing Staff
Leather Hat
Wizard Outfit
Battle Boots

Cure, Cure 4, Raise, Esuna
Level Jump8, Vertical Jump8

Player: !Red
Team: Red Team
Palettes: Red/Brown



Eryzion
Female
Sagittarius
75
59
Geomancer
Jump
Distribute
Magic Defense UP
Move+2

Mythril Sword
Escutcheon
Leather Hat
Silk Robe
Jade Armlet

Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump3



Lodrak
Male
Sagittarius
48
61
Archer
Battle Skill
Brave Up
Dual Wield
Ignore Terrain

Stone Gun
Blast Gun
Leather Hat
Wizard Outfit
Angel Ring

Charge+1, Charge+2, Charge+5, Charge+10
Magic Break, Stasis Sword, Dark Sword, Night Sword



ZeroHeat
Male
Taurus
56
56
Chemist
Sing
Regenerator
Attack UP
Waterwalking

Hydra Bag

Golden Hairpin
Clothes
Reflect Ring

Hi-Potion, Ether, Echo Grass, Soft, Phoenix Down
Angel Song, Life Song, Cheer Song



WzzrdBlzrd
Female
Aquarius
45
49
Archer
Elemental
Auto Potion
Short Charge
Fly

Gastrafitis
Buckler
Black Hood
Secret Clothes
Jade Armlet

Charge+1, Charge+3, Charge+4, Charge+20
Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind

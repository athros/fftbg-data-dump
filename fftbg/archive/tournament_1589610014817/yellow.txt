Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Reinoe
Female
Capricorn
70
60
Priest
Black Magic
MP Restore
Short Charge
Levitate

Healing Staff

Headgear
Wizard Robe
Elf Mantle

Reraise, Regen, Protect, Protect 2, Shell, Wall, Esuna, Holy
Fire, Fire 3



KasugaiRoastedPeas
Male
Gemini
44
50
Priest
Elemental
Hamedo
Doublehand
Move+1

Wizard Staff

Thief Hat
Silk Robe
Feather Mantle

Cure 2, Cure 3, Raise, Regen, Protect, Protect 2, Wall, Esuna
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Estan AD
Male
Cancer
57
78
Lancer
Sing
Parry
Equip Gun
Jump+2

Ramia Harp
Gold Shield
Circlet
Black Robe
Diamond Armlet

Level Jump8, Vertical Jump8
Cheer Song, Battle Song, Magic Song



CrownOfHorns
Male
Cancer
52
55
Calculator
Forest Skill
HP Restore
Short Status
Levitate

Bestiary
Round Shield
Red Hood
Maximillian
Genji Gauntlet

Blue Magic
Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak

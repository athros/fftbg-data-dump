Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BenYuPoker
Female
Aries
71
64
Dancer
Jump
Damage Split
Equip Sword
Ignore Height

Koutetsu Knife

Flash Hat
Wizard Robe
Elf Mantle

Polka Polka, Last Dance, Void Storage
Level Jump2, Vertical Jump4



Brownbearking90
Male
Gemini
80
65
Samurai
Sing
Earplug
Martial Arts
Ignore Terrain

Muramasa

Gold Helmet
Reflect Mail
Magic Ring

Koutetsu, Kikuichimoji
Angel Song, Last Song



Rnark
Female
Cancer
78
79
Oracle
Steal
Counter
Long Status
Ignore Terrain

Gokuu Rod

Cachusha
Wizard Outfit
Dracula Mantle

Blind, Spell Absorb, Life Drain, Silence Song, Foxbird, Dispel Magic
Steal Heart, Leg Aim



CorpusCav
Male
Capricorn
61
67
Archer
Jump
Critical Quick
Equip Gun
Move+1

Blaze Gun
Gold Shield
Twist Headband
Power Sleeve
Leather Mantle

Charge+10
Level Jump8, Vertical Jump7

Player: !Green
Team: Green Team
Palettes: Green/White



Nekross92
Female
Leo
47
59
Monk
Time Magic
Counter
Equip Polearm
Swim

Obelisk

Golden Hairpin
Black Costume
Angel Ring

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Haste, Haste 2, Slow, Float, Meteor



HaychDub
Female
Scorpio
58
72
Dancer
Time Magic
Brave Save
Short Charge
Retreat

Ryozan Silk

Leather Hat
Chameleon Robe
Wizard Mantle

Wiznaibus
Haste 2, Slow 2, Demi, Meteor



TheFALLofLindsay
Female
Capricorn
71
66
Wizard
Yin Yang Magic
Auto Potion
Equip Bow
Ignore Terrain

Windslash Bow

Green Beret
Mythril Vest
Small Mantle

Fire 2, Fire 3, Ice, Ice 3, Empower
Life Drain, Pray Faith, Blind Rage, Confusion Song, Paralyze



Mirapoix
Male
Aquarius
50
65
Chemist
Battle Skill
Abandon
Short Status
Ignore Terrain

Romanda Gun

Barette
Earth Clothes
Genji Gauntlet

Potion, X-Potion, Eye Drop, Echo Grass, Phoenix Down
Armor Break, Speed Break, Power Break

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mesmaster
Male
Gemini
72
58
Monk
Throw
MA Save
Dual Wield
Lava Walking



Holy Miter
Adaman Vest
Magic Gauntlet

Spin Fist, Pummel, Secret Fist
Bomb, Stick, Wand



Joewcarson
Female
Libra
71
49
Priest
Talk Skill
Parry
Equip Axe
Fly

Flail

Golden Hairpin
Chameleon Robe
Dracula Mantle

Cure, Raise, Raise 2, Regen, Protect, Protect 2, Shell, Wall, Esuna, Magic Barrier
Praise, Preach, Solution, Refute



Meta Five
Male
Taurus
44
74
Bard
Draw Out
Dragon Spirit
Equip Armor
Move+3

Ramia Harp

Leather Helmet
Crystal Mail
Dracula Mantle

Hydra Pit
Koutetsu, Bizen Boat, Murasame, Kiyomori



ThePineappleSalesman
Male
Virgo
63
61
Lancer
Throw
Meatbone Slash
Doublehand
Jump+2

Mythril Spear

Mythril Helmet
Genji Armor
Magic Gauntlet

Level Jump8, Vertical Jump5
Bomb

Player: !Green
Team: Green Team
Palettes: Green/White



Catfashions
Male
Cancer
44
69
Oracle
Jump
Counter Magic
Equip Polearm
Waterwalking

Octagon Rod

Headgear
Brigandine
108 Gems

Blind, Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Foxbird, Sleep, Petrify
Level Jump4, Vertical Jump4



ColetteMSLP
Female
Sagittarius
66
67
Lancer
Talk Skill
Distribute
Concentrate
Jump+2

Holy Lance
Mythril Shield
Bronze Helmet
Chameleon Robe
Red Shoes

Level Jump2, Vertical Jump2
Invitation, Threaten, Refute



SeniorBunk
Female
Taurus
52
58
Chemist
Throw
Meatbone Slash
Equip Armor
Move+2

Air Knife

Barbuta
Leather Armor
Magic Gauntlet

Potion, X-Potion, Antidote, Soft, Phoenix Down
Shuriken, Staff



Kyune
Female
Libra
66
50
Monk
White Magic
Earplug
Halve MP
Move-MP Up



Twist Headband
Clothes
N-Kai Armlet

Spin Fist, Secret Fist, Purification, Revive
Cure 4, Raise, Regen, Esuna

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Phytik
Monster
Scorpio
78
75
Ultima Demon










DustyRelic
Female
Sagittarius
39
80
Knight
Item
Meatbone Slash
Throw Item
Fly

Defender
Mythril Shield
Crystal Helmet
Chain Mail
Feather Boots

Armor Break, Weapon Break, Magic Break, Stasis Sword, Justice Sword, Surging Sword
Potion, Hi-Potion, Eye Drop, Remedy, Phoenix Down



Ar Tactic
Female
Taurus
75
64
Dancer
Talk Skill
Meatbone Slash
Beastmaster
Levitate

Ryozan Silk

Green Beret
White Robe
Magic Gauntlet

Disillusion
Invitation, Preach, Solution, Death Sentence, Mimic Daravon



Mesmaster
Male
Cancer
49
57
Geomancer
Black Magic
Auto Potion
Attack UP
Move-HP Up

Slasher
Diamond Shield
Flash Hat
Black Costume
Salty Rage

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind
Fire 2, Fire 4, Bolt, Bolt 2, Ice 2, Ice 3, Flare

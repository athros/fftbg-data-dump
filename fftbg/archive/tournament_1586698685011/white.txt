Player: !White
Team: White Team
Palettes: White/Blue



Roqqqpsi
Monster
Sagittarius
66
54
Archaic Demon










UmaiJam
Female
Aries
51
62
Time Mage
Yin Yang Magic
Auto Potion
Short Charge
Levitate

Bestiary

Black Hood
Mystic Vest
Wizard Mantle

Haste, Slow, Slow 2, Demi, Stabilize Time
Blind, Foxbird, Dispel Magic, Sleep



Jeeboheebo
Monster
Aquarius
68
72
Skeleton










Slowbrofist
Male
Serpentarius
44
79
Archer
Basic Skill
MA Save
Equip Bow
Lava Walking

Mythril Bow

Leather Hat
Earth Clothes
Rubber Shoes

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Dash, Heal

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Just Here2
Male
Aries
64
46
Monk
Throw
MA Save
Equip Shield
Move+2


Ice Shield
Feather Hat
Clothes
Wizard Mantle

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Bomb, Knife, Axe



DeathTaxesAndAnime
Monster
Taurus
61
56
Ultima Demon










NicoSavoy
Female
Gemini
73
50
Archer
Time Magic
Dragon Spirit
Dual Wield
Move-HP Up

Romanda Gun
Glacier Gun
Grand Helmet
Earth Clothes
Magic Ring

Charge+7, Charge+10
Haste, Slow, Immobilize, Float, Meteor



Laserman1000
Male
Leo
71
51
Geomancer
White Magic
HP Restore
Defense UP
Ignore Height

Battle Axe
Diamond Shield
Feather Hat
Light Robe
108 Gems

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Reraise, Protect, Protect 2, Shell 2, Wall, Esuna

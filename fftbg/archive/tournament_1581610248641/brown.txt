Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Beccasetsfire
Male
Aries
40
74
Calculator
Lucavi Skill
Counter Magic
Sicken
Move+1

Papyrus Codex

Triangle Hat
Robe of Lords
Leather Mantle

Blue Magic
Melt, Tornado, Quake, Flare 2, Gravi 2, Grand Cross, All-Ultima



Sparker9
Female
Virgo
41
67
Calculator
Bird Skill
Earplug
Equip Bow
Move+2

Star Bag

Barette
White Robe
Feather Boots

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



Luna XIV
Female
Sagittarius
51
59
Calculator
Animal Skill
HP Restore
Equip Shield
Retreat

Bestiary
Ice Shield
Flash Hat
Leather Armor
Sprint Shoes

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



Zachara
Male
Leo
69
66
Calculator
Lucavi Skill
Auto Potion
Secret Hunt
Levitate

Bestiary

Flash Hat
Wizard Outfit
Cherche

Blue Magic
Melt, Tornado, Quake, Flare 2, Gravi 2, Grand Cross, All-Ultima

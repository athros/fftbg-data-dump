Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CassiePhoenix
Male
Capricorn
56
40
Lancer
Sing
Counter Tackle
Doublehand
Jump+3

Mythril Spear

Gold Helmet
Diamond Armor
N-Kai Armlet

Level Jump5, Vertical Jump8
Life Song, Last Song



TeaTime29
Male
Sagittarius
40
76
Oracle
Item
Catch
Halve MP
Move+1

Gokuu Rod

Feather Hat
Rubber Costume
Angel Ring

Poison, Spell Absorb, Confusion Song, Dispel Magic
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Randgridr
Male
Sagittarius
55
68
Ninja
Steal
Mana Shield
Equip Bow
Jump+1

Yoichi Bow

Flash Hat
Black Costume
Cherche

Bomb
Steal Status, Leg Aim



Adnan
Male
Virgo
43
64
Bard
Charge
Faith Save
Doublehand
Jump+2

Windslash Bow

Ribbon
Adaman Vest
Leather Mantle

Cheer Song, Last Song
Charge+3, Charge+4

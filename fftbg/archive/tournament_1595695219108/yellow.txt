Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Cyril Jay
Female
Capricorn
57
64
Mime

Sunken State
Martial Arts
Move+2



Thief Hat
Brigandine
Spike Shoes

Mimic




Bryon W
Female
Scorpio
75
72
Monk
Summon Magic
Auto Potion
Equip Armor
Teleport



Green Beret
Chameleon Robe
Germinas Boots

Wave Fist, Secret Fist, Purification, Chakra, Revive
Moogle, Carbunkle, Leviathan, Silf, Fairy



Grininda
Male
Libra
64
73
Mime

Counter
Magic Attack UP
Swim



Barette
Judo Outfit
N-Kai Armlet

Mimic




Shalloween
Male
Leo
54
70
Archer
White Magic
Catch
Doublehand
Ignore Terrain

Bow Gun

Feather Hat
Judo Outfit
Magic Gauntlet

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Cure, Raise, Raise 2, Regen, Shell, Wall, Esuna

Player: !Red
Team: Red Team
Palettes: Red/Brown



Thyrandaal
Female
Leo
75
69
Mediator
Time Magic
Parry
Beastmaster
Ignore Height

Papyrus Codex

Feather Hat
Silk Robe
Cherche

Invitation, Persuade, Death Sentence, Refute
Haste, Haste 2, Slow, Demi 2



Smugzug
Male
Pisces
50
54
Lancer
Battle Skill
Counter Magic
Short Status
Retreat

Whale Whisker
Genji Shield
Bronze Helmet
Carabini Mail
Magic Ring

Level Jump8, Vertical Jump3
Shield Break, Speed Break, Stasis Sword, Justice Sword



PoroTact
Female
Aries
61
80
Archer
Steal
Mana Shield
Dual Wield
Move+2

Bow Gun
Poison Bow
Holy Miter
Mystic Vest
Dracula Mantle

Charge+3, Charge+4, Charge+7, Charge+20
Steal Weapon, Steal Status



Aldrammech
Male
Cancer
77
46
Squire
Battle Skill
Caution
Doublehand
Move-MP Up

Bow Gun

Mythril Helmet
Clothes
Angel Ring

Accumulate, Dash, Throw Stone, Heal, Yell, Wish
Armor Break, Speed Break, Mind Break

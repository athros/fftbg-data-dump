Final Bets: purple - 22 bets for 28,000G (73.4%, x0.36); brown - 16 bets for 10,134G (26.6%, x2.76)

purple bets:
ko2q: 5,386G (19.2%, 10,773G)
MantisFinch: 5,000G (17.9%, 19,528G)
OmnibotGamma: 4,119G (14.7%, 4,119G)
RyuTsuno: 2,500G (8.9%, 14,414G)
reinoe: 1,865G (6.7%, 1,865G)
Aldrammech: 1,000G (3.6%, 28,462G)
Pochitchi: 1,000G (3.6%, 9,945G)
SkylerBunny: 1,000G (3.6%, 59,160G)
EnemyController: 1,000G (3.6%, 69,771G)
Estan_AD: 995G (3.6%, 995G)
DeathTaxesAndAnime: 600G (2.1%, 10,222G)
R_Raynos: 595G (2.1%, 595G)
roofiepops: 550G (2.0%, 5,393G)
superdevon1: 500G (1.8%, 5,300G)
AllInBot: 430G (1.5%, 430G)
Powermhero: 400G (1.4%, 9,400G)
mastershango: 300G (1.1%, 11,304G)
killth3kid: 288G (1.0%, 4,024G)
regios91: 172G (0.6%, 172G)
DontBackseatGame: 100G (0.4%, 100G)
ANFz: 100G (0.4%, 15,318G)
CandyTamer: 100G (0.4%, 786G)

brown bets:
sinnyil2: 2,500G (24.7%, 80,245G)
HaplessOne: 1,500G (14.8%, 118,279G)
Pie108: 1,000G (9.9%, 50,859G)
ungabunga_bot: 1,000G (9.9%, 205,941G)
BirbBrainsBot: 1,000G (9.9%, 56,513G)
leakimiko: 546G (5.4%, 10,932G)
JumbocactuarX27: 500G (4.9%, 13,496G)
alrightbye: 500G (4.9%, 24,881G)
getthemoneyz: 494G (4.9%, 525,373G)
RunicMagus: 400G (3.9%, 34,918G)
MeleeWizard: 264G (2.6%, 264G)
TJ_FordCDN: 100G (1.0%, 6,869G)
electric_glass: 100G (1.0%, 5,157G)
Evewho: 100G (1.0%, 1,432G)
silentperogy: 100G (1.0%, 22,987G)
ZZ_Yoshi: 30G (0.3%, 67,439G)

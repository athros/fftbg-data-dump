Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Heyifoundawhistle
Female
Scorpio
46
40
Calculator
Yin Yang Magic
Auto Potion
Defense UP
Jump+2

Poison Rod

Green Beret
Silk Robe
108 Gems

CT, Prime Number, 4, 3
Blind, Poison, Doubt Faith, Foxbird, Paralyze, Dark Holy



Sev Orion
Female
Cancer
78
48
Thief
Elemental
Abandon
Doublehand
Jump+3

Ice Brand

Red Hood
Adaman Vest
Battle Boots

Arm Aim
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Sinnyil2
Male
Aquarius
40
78
Priest
Draw Out
Hamedo
Equip Knife
Retreat

Flame Rod

Thief Hat
Earth Clothes
N-Kai Armlet

Cure, Cure 3, Cure 4, Raise, Reraise, Regen, Protect, Protect 2, Wall, Esuna
Koutetsu



Clutchy42
Female
Aquarius
73
72
Oracle
Throw
Arrow Guard
Short Charge
Move-HP Up

Ivory Rod

Flash Hat
Adaman Vest
Leather Mantle

Blind, Poison, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy
Bomb, Ninja Sword, Wand

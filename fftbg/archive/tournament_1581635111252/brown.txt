Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Koryiaki
Monster
Capricorn
74
45
Steel Giant










Coyowijuju
Female
Pisces
71
65
Oracle
Elemental
Sunken State
Defend
Ignore Height

Octagon Rod

Flash Hat
White Robe
Elf Mantle

Poison, Spell Absorb, Life Drain, Zombie, Foxbird, Dispel Magic, Paralyze
Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



SpencoJFrog
Monster
Aquarius
67
42
Floating Eye










Sypheck
Female
Aries
60
78
Geomancer
Time Magic
MA Save
Beastmaster
Waterwalking

Coral Sword
Escutcheon
Thief Hat
Robe of Lords
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Stabilize Time

Player: !Green
Team: Green Team
Palettes: Green/White



Ominnous
Female
Aquarius
47
45
Mediator
Yin Yang Magic
Counter Magic
Dual Wield
Waterwalking

Blaze Gun
Glacier Gun
Black Hood
Clothes
Spike Shoes

Invitation, Persuade, Praise, Death Sentence, Insult, Negotiate, Rehabilitate
Blind, Life Drain, Doubt Faith, Silence Song, Dispel Magic



Z32o
Female
Libra
49
71
Summoner
Jump
Distribute
Attack UP
Teleport

Rainbow Staff

Leather Hat
Mystic Vest
Sprint Shoes

Moogle, Ifrit, Carbunkle, Bahamut, Fairy
Level Jump8, Vertical Jump7



BenYuPoker
Female
Capricorn
47
51
Samurai
Item
MP Restore
Magic Attack UP
Waterbreathing

Koutetsu Knife

Bronze Helmet
Wizard Robe
Cursed Ring

Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down



Treapvort
Female
Pisces
77
75
Summoner
Talk Skill
Arrow Guard
Short Charge
Jump+2

Wizard Rod

Red Hood
Black Robe
Battle Boots

Ifrit, Titan, Carbunkle, Leviathan, Salamander, Fairy
Invitation, Praise, Preach, Solution, Refute, Rehabilitate

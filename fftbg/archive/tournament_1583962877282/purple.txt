Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mythren
Monster
Libra
64
53
Serpentarius










Sypheck
Male
Capricorn
67
61
Monk
Item
Damage Split
Short Status
Jump+2



Headgear
Judo Outfit
Bracer

Spin Fist, Pummel, Purification
X-Potion, Soft, Holy Water, Phoenix Down



Trowaba
Female
Capricorn
43
63
Dancer
Battle Skill
Sunken State
Long Status
Jump+3

Persia

Black Hood
Mythril Vest
Spike Shoes

Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Void Storage, Dragon Pit
Armor Break, Magic Break, Mind Break



Cheezybuda
Male
Cancer
66
60
Monk
Throw
Distribute
Equip Shield
Ignore Terrain


Ice Shield
Flash Hat
Brigandine
Chantage

Spin Fist, Earth Slash, Purification, Revive
Shuriken, Bomb, Sword

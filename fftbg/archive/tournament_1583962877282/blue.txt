Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



AlenaZarek
Female
Libra
54
56
Wizard
Elemental
Counter
Beastmaster
Jump+1

Dragon Rod

Thief Hat
Power Sleeve
Sprint Shoes

Fire 3
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Mr G00SE
Monster
Serpentarius
62
49
Floating Eye










KupoKel
Male
Gemini
63
70
Archer
White Magic
Counter Flood
Doublehand
Levitate

Night Killer

Twist Headband
Power Sleeve
Dracula Mantle

Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Cure 2, Raise, Protect, Protect 2, Esuna



LanseDM
Male
Aquarius
74
79
Lancer
Battle Skill
MP Restore
Doublehand
Ignore Terrain

Gokuu Rod

Cross Helmet
Light Robe
Magic Gauntlet

Level Jump8, Vertical Jump4
Head Break, Armor Break, Justice Sword

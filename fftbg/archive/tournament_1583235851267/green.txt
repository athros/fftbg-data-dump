Player: !Green
Team: Green Team
Palettes: Green/White



Superdevon1
Male
Aquarius
40
71
Thief
Item
Caution
Equip Bow
Move+3

Poison Bow

Green Beret
Wizard Outfit
Spike Shoes

Steal Helmet, Steal Shield, Arm Aim
Potion, Ether, Soft, Phoenix Down



Thaetreis
Female
Libra
62
63
Monk
Yin Yang Magic
Parry
Doublehand
Waterbreathing



Triangle Hat
Power Sleeve
Spike Shoes

Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil
Blind, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Sleep



FreedomNM
Male
Aquarius
79
76
Mime

Caution
Short Status
Move-HP Up



Feather Hat
Black Robe
Cursed Ring

Mimic




Ralamar
Female
Aquarius
44
78
Knight
Charge
Distribute
Equip Knife
Move+2

Air Knife
Gold Shield
Circlet
Plate Mail
Small Mantle

Head Break, Armor Break, Magic Break, Speed Break, Power Break, Justice Sword, Surging Sword
Charge+2, Charge+7

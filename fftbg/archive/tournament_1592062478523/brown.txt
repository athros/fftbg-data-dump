Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fluffskull
Female
Cancer
63
48
Archer
Elemental
Dragon Spirit
Secret Hunt
Swim

Mythril Gun
Escutcheon
Red Hood
Mystic Vest
Rubber Shoes

Charge+4
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Alaquane
Female
Aries
59
58
Summoner
Steal
Abandon
Equip Armor
Jump+2

Wizard Rod

Barbuta
Chameleon Robe
Red Shoes

Shiva, Titan, Golem, Carbunkle, Bahamut, Leviathan, Silf, Fairy, Cyclops
Gil Taking, Steal Helmet



Rislyeu
Female
Libra
78
65
Time Mage
Summon Magic
Parry
Equip Armor
Move-MP Up

Gokuu Rod

Crystal Helmet
Leather Armor
Diamond Armlet

Slow, Immobilize, Float, Reflect, Quick, Demi 2, Meteor
Shiva, Ramuh, Titan, Carbunkle, Odin, Salamander, Cyclops



Grininda
Male
Sagittarius
46
55
Ninja
Yin Yang Magic
Counter Magic
Equip Gun
Move+3

Papyrus Codex
Bestiary
Golden Hairpin
Secret Clothes
Sprint Shoes

Shuriken, Knife
Blind, Poison, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy

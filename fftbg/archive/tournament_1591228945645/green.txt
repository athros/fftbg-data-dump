Player: !Green
Team: Green Team
Palettes: Green/White



TinchoT
Male
Serpentarius
70
61
Geomancer
Item
Speed Save
Halve MP
Retreat

Battle Axe
Platinum Shield
Flash Hat
Leather Outfit
Leather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball
X-Potion, Hi-Ether, Echo Grass, Holy Water, Remedy



Evewho
Female
Aquarius
57
59
Knight
Black Magic
Absorb Used MP
Dual Wield
Jump+3

Chaos Blade
Materia Blade
Gold Helmet
Bronze Armor
Sprint Shoes

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Mind Break, Dark Sword, Surging Sword
Fire 2, Empower



ApplesauceBoss
Male
Pisces
80
52
Monk
Battle Skill
Brave Save
Attack UP
Move+3



Black Hood
Mythril Vest
Battle Boots

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Armor Break, Shield Break, Mind Break, Justice Sword, Dark Sword



Kronikle
Female
Gemini
39
43
Samurai
Item
MA Save
Defense UP
Move+2

Koutetsu Knife

Platinum Helmet
Crystal Mail
Feather Mantle

Asura, Bizen Boat, Murasame, Kiyomori
Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ArchKnightX
Female
Libra
68
79
Knight
Elemental
Counter Tackle
Secret Hunt
Teleport

Blood Sword
Genji Shield
Genji Helmet
Reflect Mail
Power Wrist

Head Break, Armor Break, Shield Break
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Lionhermit
Female
Taurus
50
48
Priest
Black Magic
Absorb Used MP
Maintenance
Swim

Rainbow Staff

Golden Hairpin
Judo Outfit
Jade Armlet

Cure, Cure 2, Cure 4, Raise, Protect, Protect 2, Esuna, Holy
Fire, Fire 3, Bolt 3, Ice 4



SSwing
Male
Scorpio
36
58
Lancer
Item
Auto Potion
Sicken
Lava Walking

Ivory Rod
Crystal Shield
Platinum Helmet
Plate Mail
Diamond Armlet

Level Jump8, Vertical Jump8
Potion, Hi-Ether, Phoenix Down



Killth3kid
Male
Scorpio
49
62
Thief
Yin Yang Magic
Counter Flood
Equip Axe
Fly

Healing Staff

Holy Miter
Chain Vest
108 Gems

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status
Doubt Faith, Zombie, Silence Song, Confusion Song, Sleep, Petrify

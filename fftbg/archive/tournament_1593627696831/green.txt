Player: !Green
Team: Green Team
Palettes: Green/White



ArashiKurobara
Female
Taurus
52
74
Time Mage
White Magic
Mana Shield
Short Charge
Waterbreathing

Musk Rod

Golden Hairpin
White Robe
Sprint Shoes

Haste, Haste 2, Slow, Slow 2, Reflect, Demi 2, Stabilize Time
Raise, Raise 2, Shell 2, Esuna



O Heyno
Male
Virgo
45
75
Bard
Throw
Sunken State
Defend
Jump+1

Mythril Bow

Leather Hat
Wizard Outfit
N-Kai Armlet

Battle Song, Nameless Song, Sky Demon
Wand



Thyrandaal
Male
Cancer
59
77
Geomancer
Summon Magic
HP Restore
Long Status
Jump+2

Battle Axe
Round Shield
Green Beret
Leather Outfit
Dracula Mantle

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Leviathan, Silf



ValtonZenola
Female
Sagittarius
80
80
Calculator
Pet Skill
MP Restore
Martial Arts
Move+3


Flame Shield
Red Hood
Crystal Mail
Magic Ring

Blue Magic
Scratch Up, Feather Bomb, Shine Lover, Peck, Beak, Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Matthewmuscles
Male
Leo
41
65
Priest
Throw
Earplug
Magic Defense UP
Fly

Flame Whip

Black Hood
Linen Robe
Germinas Boots

Cure, Cure 3, Cure 4, Raise, Protect, Esuna
Shuriken, Axe



Flowtaro
Female
Scorpio
52
70
Samurai
Jump
Auto Potion
Magic Attack UP
Levitate

Muramasa

Barbuta
Leather Armor
Genji Gauntlet

Asura, Bizen Boat, Kiyomori, Muramasa
Level Jump2, Vertical Jump4



Dynasti
Male
Cancer
68
40
Archer
Draw Out
Counter
Doublehand
Retreat

Romanda Gun

Cross Helmet
Judo Outfit
Small Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10
Bizen Boat



Ranmilia
Female
Aries
44
54
Oracle
Talk Skill
Faith Up
Monster Talk
Jump+1

Battle Bamboo

Holy Miter
Brigandine
Reflect Ring

Life Drain, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Sleep
Invitation, Persuade, Threaten, Insult, Mimic Daravon, Refute, Rehabilitate

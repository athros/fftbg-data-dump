Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sociallubricant1
Male
Cancer
43
62
Thief
Yin Yang Magic
MA Save
Long Status
Teleport

Platinum Sword

Thief Hat
Chain Vest
Magic Gauntlet

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Life Drain, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify



Sairentozon7
Male
Aries
76
41
Lancer
Draw Out
Counter
Equip Axe
Jump+2

Scorpion Tail
Aegis Shield
Circlet
Chain Mail
Wizard Mantle

Level Jump5, Vertical Jump8
Bizen Boat, Heaven's Cloud



Basmal
Female
Sagittarius
62
60
Calculator
Black Magic
Counter
Concentrate
Ignore Terrain

Bestiary

Headgear
Silk Robe
Rubber Shoes

CT, Prime Number, 5, 3
Fire, Bolt 2, Bolt 4, Ice, Flare



Rastanar
Male
Aries
80
56
Ninja
Black Magic
Arrow Guard
Magic Attack UP
Jump+3

Mythril Knife
Kunai
Twist Headband
Rubber Costume
Jade Armlet

Shuriken, Bomb, Ninja Sword
Fire 4, Bolt 3, Ice 3, Empower, Frog

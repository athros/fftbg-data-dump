Player: !White
Team: White Team
Palettes: White/Blue



Roofiepops
Female
Sagittarius
65
40
Ninja
Summon Magic
Arrow Guard
Attack UP
Levitate

Flame Whip
Morning Star
Leather Hat
Adaman Vest
Defense Ring

Shuriken, Knife, Dictionary
Shiva, Golem, Cyclops



Phrossi V
Female
Virgo
75
71
Thief
Throw
Earplug
Beastmaster
Teleport

Nagrarock

Green Beret
Chain Vest
Feather Boots

Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Staff



DrAntiSocial
Female
Capricorn
53
79
Archer
Yin Yang Magic
HP Restore
Equip Bow
Move+3

Bow Gun
Genji Shield
Feather Hat
Black Costume
Diamond Armlet

Charge+2, Charge+3, Charge+4, Charge+5, Charge+20
Poison, Spell Absorb, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep



Urieltheflameofgod
Female
Virgo
56
49
Lancer
White Magic
Regenerator
Magic Attack UP
Swim

Obelisk
Escutcheon
Cross Helmet
Chameleon Robe
Bracer

Level Jump8, Vertical Jump8
Cure, Cure 2, Raise, Protect, Shell, Esuna, Magic Barrier

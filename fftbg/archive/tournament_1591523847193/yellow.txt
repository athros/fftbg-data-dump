Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ALY327
Male
Leo
60
49
Bard
Punch Art
Earplug
Equip Polearm
Jump+3

Octagon Rod

Leather Hat
Earth Clothes
Defense Ring

Cheer Song, Diamond Blade, Sky Demon
Pummel, Wave Fist, Earth Slash, Purification, Chakra



Maleblackfiora
Female
Capricorn
69
58
Lancer
Basic Skill
Earplug
Defend
Ignore Height

Spear
Mythril Shield
Barbuta
Diamond Armor
Dracula Mantle

Level Jump5, Vertical Jump7
Dash, Throw Stone, Heal, Tickle



Mesmaster
Male
Pisces
62
44
Lancer
Battle Skill
Damage Split
Magic Defense UP
Ignore Terrain

Javelin
Mythril Shield
Leather Helmet
Light Robe
Jade Armlet

Level Jump5, Vertical Jump8
Weapon Break, Speed Break, Justice Sword, Surging Sword



E Ballard
Male
Pisces
79
72
Monk
Steal
Counter
Long Status
Jump+2



Leather Hat
Wizard Outfit
Defense Armlet

Spin Fist, Wave Fist, Secret Fist, Revive
Steal Heart, Steal Accessory, Arm Aim

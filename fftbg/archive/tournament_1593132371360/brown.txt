Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Baron Von Scrub
Female
Scorpio
50
42
Dancer
Steal
Abandon
Halve MP
Fly

Ryozan Silk

Black Hood
Adaman Vest
Cherche

Slow Dance, Polka Polka, Disillusion, Last Dance, Dragon Pit
Gil Taking, Steal Armor, Steal Shield, Steal Accessory, Leg Aim



Lythe Caraker
Monster
Pisces
60
78
Cockatrice










SkylerBunny
Male
Sagittarius
56
60
Wizard
Item
Mana Shield
Sicken
Ignore Terrain

Rod

Ribbon
Adaman Vest
Feather Boots

Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Flare
Hi-Potion, Echo Grass, Soft, Phoenix Down



BlackfyreRoy
Female
Sagittarius
52
62
Monk
Charge
Distribute
Equip Polearm
Levitate

Octagon Rod

Flash Hat
Chain Vest
Spike Shoes

Spin Fist, Secret Fist, Purification, Revive, Seal Evil
Charge+1, Charge+4, Charge+10

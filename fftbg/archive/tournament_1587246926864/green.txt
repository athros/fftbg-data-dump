Player: !Green
Team: Green Team
Palettes: Green/White



B0shii
Female
Gemini
59
75
Ninja
Dance
Caution
Magic Defense UP
Jump+2

Kunai
Spell Edge
Triangle Hat
Leather Outfit
Angel Ring

Bomb, Wand
Nameless Dance



DrAntiSocial
Female
Taurus
75
79
Chemist
Steal
Absorb Used MP
Dual Wield
Jump+2

Hydra Bag
Panther Bag
Red Hood
Secret Clothes
Red Shoes

Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Steal Helmet, Steal Armor, Steal Shield, Steal Status, Arm Aim



Zbgs
Male
Cancer
68
41
Archer
Item
Earplug
Doublehand
Jump+2

Night Killer

Twist Headband
Judo Outfit
N-Kai Armlet

Charge+3, Charge+4, Charge+5, Charge+10, Charge+20
Potion, Hi-Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



ThePineappleSalesman
Female
Aquarius
49
67
Mediator
Punch Art
Arrow Guard
Secret Hunt
Move+3

Blast Gun

Headgear
Chameleon Robe
Reflect Ring

Praise, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Earth Slash, Purification, Revive

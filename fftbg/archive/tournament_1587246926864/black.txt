Player: !Black
Team: Black Team
Palettes: Black/Red



ANFz
Male
Virgo
47
78
Samurai
Steal
Caution
Defend
Move-HP Up

Kiyomori

Iron Helmet
Platinum Armor
Genji Gauntlet

Murasame
Steal Accessory, Leg Aim



ApplesauceBoss
Female
Virgo
61
71
Time Mage
Basic Skill
Catch
Short Charge
Jump+3

Rainbow Staff

Golden Hairpin
Light Robe
108 Gems

Reflect, Stabilize Time
Yell, Cheer Up, Wish



Fluffywormhole
Female
Aries
45
52
Mediator
White Magic
Regenerator
Sicken
Levitate

Stone Gun

Twist Headband
Chameleon Robe
Elf Mantle

Persuade, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Refute
Cure 2, Raise, Protect 2, Esuna, Holy



Breakdown777
Male
Cancer
48
65
Mime

Faith Up
Equip Shield
Teleport


Diamond Shield
Feather Hat
Mystic Vest
Spike Shoes

Mimic


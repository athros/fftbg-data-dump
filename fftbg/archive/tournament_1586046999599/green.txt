Player: !Green
Team: Green Team
Palettes: Green/White



Mirapoix
Male
Virgo
54
73
Ninja
Black Magic
Catch
Equip Shield
Ignore Height

Cultist Dagger
Platinum Shield
Leather Hat
Brigandine
Red Shoes

Bomb, Hammer
Fire, Fire 2, Ice, Ice 3



BattleshipAsuka
Male
Virgo
72
78
Chemist
Elemental
Brave Up
Equip Polearm
Teleport

Ryozan Silk

Holy Miter
Mythril Vest
Feather Boots

Potion, X-Potion, Antidote, Echo Grass, Remedy, Phoenix Down
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



GrayGhostGaming
Female
Pisces
43
70
Summoner
Battle Skill
HP Restore
Secret Hunt
Ignore Terrain

White Staff

Thief Hat
Chameleon Robe
108 Gems

Moogle, Shiva, Ramuh, Titan, Carbunkle, Odin, Salamander, Lich, Cyclops
Shield Break, Magic Break, Speed Break, Power Break



Bryan792
Female
Scorpio
43
80
Geomancer
Draw Out
Counter Flood
Doublehand
Jump+1

Koutetsu Knife

Golden Hairpin
Linen Robe
Power Wrist

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Koutetsu, Kikuichimoji

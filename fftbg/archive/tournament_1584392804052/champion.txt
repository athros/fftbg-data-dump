Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



TrogdorTheMemeinator
Monster
Aquarius
68
73
Byblos










MantisFinch
Female
Leo
75
48
Summoner
Item
Mana Shield
Short Charge
Move-HP Up

Poison Rod

Golden Hairpin
Silk Robe
Spike Shoes

Bahamut, Salamander, Fairy
X-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down



A Mo22
Male
Gemini
48
69
Lancer
Yin Yang Magic
Mana Shield
Halve MP
Swim

Battle Bamboo
Aegis Shield
Gold Helmet
Maximillian
Bracer

Level Jump2, Vertical Jump5
Pray Faith, Zombie, Foxbird, Dispel Magic, Sleep



MrFlabyo
Female
Aries
44
70
Monk
Elemental
Counter
Equip Armor
Move+2



Platinum Helmet
Black Robe
Defense Ring

Pummel, Secret Fist, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

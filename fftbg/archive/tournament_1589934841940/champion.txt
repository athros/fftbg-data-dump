Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Lowlf
Monster
Sagittarius
77
63
Behemoth










Heroebal
Male
Aries
74
58
Knight
Elemental
MP Restore
Attack UP
Waterbreathing

Save the Queen
Ice Shield
Platinum Helmet
Leather Armor
Small Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Xxbronamastexx
Monster
Libra
47
51
Byblos










ApplesauceBoss
Male
Cancer
40
46
Geomancer
Item
HP Restore
Halve MP
Fly

Slasher
Escutcheon
Holy Miter
Chameleon Robe
Cherche

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down

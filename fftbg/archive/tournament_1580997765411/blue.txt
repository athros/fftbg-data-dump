Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Hzor
Female
Leo
38
63
Geomancer
White Magic
Dragon Spirit
Halve MP
Jump+3

Koutetsu Knife
Mythril Shield
Green Beret
Silk Robe
Magic Gauntlet

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball
Raise, Raise 2, Reraise, Protect, Wall, Esuna



Knivesrobotica
Male
Aries
60
80
Wizard
Elemental
Counter
Halve MP
Jump+3

Blind Knife

Golden Hairpin
Leather Vest
Feather Boots

Fire 3, Bolt, Bolt 2, Ice 2, Ice 3, Ice 4, Frog, Death
Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



CynicalRazor
Monster
Cancer
74
48
Steel Giant










MoonSlayerRS
Female
Taurus
42
52
Priest
Dance
Earplug
Equip Axe
Lava Walking

Giant Axe

Headgear
White Robe
Dracula Mantle

Raise, Regen, Shell, Shell 2, Esuna
Wiznaibus, Polka Polka, Disillusion, Void Storage

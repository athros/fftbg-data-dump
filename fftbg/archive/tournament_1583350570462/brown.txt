Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lanshaft
Female
Leo
50
41
Calculator
White Magic
Counter Flood
Attack UP
Ignore Terrain

Bestiary

Twist Headband
Adaman Vest
Wizard Mantle

CT, Prime Number, 4
Raise, Raise 2, Reraise, Protect, Shell, Wall, Esuna



ZeroHeat
Monster
Virgo
62
42
Red Chocobo










Lydian C
Female
Cancer
68
49
Oracle
White Magic
Dragon Spirit
Dual Wield
Move+3

Bestiary
Papyrus Codex
Green Beret
Light Robe
Rubber Shoes

Life Drain, Pray Faith, Zombie, Blind Rage, Dispel Magic
Reraise, Protect, Shell, Esuna



DavenIII
Male
Capricorn
60
40
Calculator
Black Magic
Distribute
Dual Wield
Move+3

Rod
Ice Rod
Leather Hat
Black Robe
Angel Ring

CT, Prime Number, 5, 4
Bolt 2, Ice 3, Empower, Death

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kai Shee
Female
Serpentarius
79
64
Time Mage
Battle Skill
Meatbone Slash
Short Charge
Move-HP Up

Battle Folio

Ribbon
Black Costume
Feather Boots

Haste, Haste 2, Slow, Stop, Immobilize, Demi 2, Stabilize Time, Meteor
Head Break, Armor Break, Speed Break, Mind Break, Justice Sword, Dark Sword



Volgrathemoose
Male
Sagittarius
48
45
Calculator
White Magic
Faith Up
Equip Knife
Levitate

Kunai

Green Beret
White Robe
Dracula Mantle

CT, Prime Number, 5, 4
Cure 2, Raise, Raise 2, Protect, Shell, Shell 2, Wall, Esuna



GrayGhostGaming
Male
Leo
46
62
Priest
Draw Out
Earplug
Halve MP
Ignore Terrain

Gold Staff

Holy Miter
Light Robe
Jade Armlet

Cure 3, Raise, Reraise, Shell, Shell 2, Wall, Esuna
Asura, Muramasa, Kikuichimoji, Chirijiraden



Leakimiko
Male
Cancer
62
70
Ninja
Elemental
Parry
Equip Polearm
Move-MP Up

Dragon Whisker
Obelisk
Golden Hairpin
Mystic Vest
Elf Mantle

Shuriken
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

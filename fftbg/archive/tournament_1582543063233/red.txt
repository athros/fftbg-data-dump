Player: !Red
Team: Red Team
Palettes: Red/Brown



MithrasArslan
Female
Taurus
52
65
Summoner
Draw Out
MP Restore
Doublehand
Move+3

Poison Rod

Triangle Hat
Light Robe
Spike Shoes

Moogle, Titan, Golem, Carbunkle, Odin, Salamander
Koutetsu, Bizen Boat, Heaven's Cloud



MadManDan
Male
Cancer
44
55
Mediator
Elemental
HP Restore
Equip Shield
Swim

Orichalcum
Round Shield
Thief Hat
Linen Robe
Angel Ring

Persuade, Praise, Threaten, Preach, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Gusty Wind



Saiko707
Monster
Scorpio
47
52
Skeleton










Malus55
Female
Cancer
68
78
Oracle
Throw
Earplug
Equip Axe
Move-HP Up

Giant Axe

Twist Headband
Chameleon Robe
Red Shoes

Blind, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie, Foxbird, Sleep
Shuriken, Bomb, Knife

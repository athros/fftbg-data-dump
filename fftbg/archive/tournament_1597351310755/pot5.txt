Final Bets: blue - 14 bets for 11,038G (68.6%, x0.46); green - 14 bets for 5,041G (31.4%, x2.19)

blue bets:
Forkmore: 3,436G (31.1%, 6,739G)
Tarheels218: 2,000G (18.1%, 10,158G)
dogsandcatsand: 1,640G (14.9%, 1,640G)
resjudicata3: 1,000G (9.1%, 4,296G)
Nizaha: 732G (6.6%, 14,961G)
SephDarkheart: 564G (5.1%, 103,736G)
Mesmaster: 412G (3.7%, 412G)
ArrenJevleth: 400G (3.6%, 3,161G)
brokenknight201: 200G (1.8%, 16,519G)
datadrivenbot: 200G (1.8%, 64,884G)
Lord_Gwarth: 137G (1.2%, 910G)
Gunz232323: 136G (1.2%, 1,136G)
MonchoStrife: 112G (1.0%, 541G)
nhammen: 69G (0.6%, 6,950G)

green bets:
VolgraTheMoose: 1,209G (24.0%, 2,418G)
amiture: 762G (15.1%, 762G)
nifboy: 596G (11.8%, 596G)
AllInBot: 537G (10.7%, 537G)
Kronikle: 520G (10.3%, 13,810G)
latebit: 328G (6.5%, 31,830G)
TrogdorTheMemeinator: 240G (4.8%, 240G)
BirbBrainsBot: 163G (3.2%, 82,613G)
getthemoneyz: 146G (2.9%, 1,615,712G)
maddrave09: 140G (2.8%, 387G)
MinBetBot: 100G (2.0%, 5,274G)
fesight: 100G (2.0%, 1,651G)
Scuba_Steve3: 100G (2.0%, 1,391G)
CosmicTactician: 100G (2.0%, 19,861G)

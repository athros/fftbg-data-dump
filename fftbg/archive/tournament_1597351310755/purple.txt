Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sinnyil2
Female
Aries
67
62
Archer
Throw
MP Restore
Magic Defense UP
Jump+3

Night Killer
Buckler
Diamond Helmet
Black Costume
Cursed Ring

Charge+1, Charge+2
Shuriken, Bomb



Bruubarg
Male
Sagittarius
68
70
Mime

Caution
Doublehand
Move+1



Flash Hat
Chain Vest
Angel Ring

Mimic




MonchoStrife
Male
Virgo
70
76
Wizard
Basic Skill
Counter Flood
Short Status
Jump+3

Wizard Rod

Black Hood
Judo Outfit
Leather Mantle

Fire, Fire 2, Fire 3, Bolt, Ice 3, Empower, Flare
Accumulate, Dash, Heal, Tickle, Wish



Nizaha
Male
Leo
54
80
Thief
Jump
MA Save
Equip Gun
Ignore Height

Bloody Strings

Golden Hairpin
Clothes
Wizard Mantle

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Level Jump8, Vertical Jump7

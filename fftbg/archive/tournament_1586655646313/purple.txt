Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CapnChaos12
Female
Libra
53
58
Summoner
Time Magic
Brave Up
Short Charge
Move+3

Poison Rod

Golden Hairpin
Brigandine
Dracula Mantle

Ramuh, Carbunkle, Leviathan, Silf, Cyclops, Zodiac
Haste 2, Stop, Immobilize, Float, Quick, Stabilize Time, Meteor



KasugaiRoastedPeas
Male
Cancer
48
70
Archer
Battle Skill
Counter Flood
Doublehand
Swim

Ice Bow

Gold Helmet
Judo Outfit
Feather Boots

Charge+2, Charge+10
Head Break, Armor Break, Power Break



Shadowmare250
Female
Libra
51
80
Priest
Black Magic
Catch
Martial Arts
Jump+3



Leather Hat
Black Robe
Jade Armlet

Cure, Cure 4, Protect, Shell 2, Esuna
Bolt 3, Bolt 4, Ice 3, Ice 4, Empower, Frog, Death



LordMaxoss
Male
Scorpio
76
70
Knight
Yin Yang Magic
Faith Up
Short Charge
Jump+3

Mythril Sword
Diamond Shield
Gold Helmet
Chameleon Robe
Small Mantle

Shield Break, Magic Break, Power Break, Justice Sword, Dark Sword, Surging Sword
Blind, Life Drain, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



NovaKnight21
Female
Leo
54
65
Geomancer
Draw Out
Abandon
Equip Sword
Waterbreathing

Heaven's Cloud
Aegis Shield
Triangle Hat
Mythril Vest
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Murasame, Kikuichimoji



Ququroon
Male
Sagittarius
48
54
Geomancer
Draw Out
Sunken State
Equip Armor
Lava Walking

Slasher
Aegis Shield
Crystal Helmet
Leather Outfit
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa, Kikuichimoji, Masamune



RurouniGeo
Male
Gemini
54
40
Calculator
Time Magic
Mana Shield
Magic Defense UP
Ignore Terrain

Battle Folio

Twist Headband
Silk Robe
108 Gems

CT, Height, 3
Haste, Stop, Immobilize, Reflect, Demi, Demi 2, Stabilize Time



L2 Sentinel
Male
Libra
60
56
Lancer
Talk Skill
Catch
Attack UP
Move-HP Up

Gokuu Rod
Bronze Shield
Platinum Helmet
Silk Robe
Magic Gauntlet

Level Jump2, Vertical Jump8
Invitation, Preach, Negotiate, Mimic Daravon, Refute

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zmoses
Female
Capricorn
47
63
Wizard
Math Skill
Speed Save
Concentrate
Lava Walking

Rod

Red Hood
Wizard Robe
Bracer

Bolt, Bolt 3, Ice 2, Ice 4, Empower
CT, Height, 5



SeedSC
Male
Taurus
70
62
Ninja
Basic Skill
MP Restore
Equip Bow
Swim

Lightning Bow

Holy Miter
Mythril Vest
Magic Ring

Bomb, Stick
Throw Stone, Heal, Yell, Fury



TycerNova
Female
Sagittarius
62
50
Geomancer
Black Magic
Caution
Defend
Levitate

Giant Axe
Genji Shield
Feather Hat
Black Robe
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Bolt 2, Bolt 3



WireLord
Monster
Aquarius
54
54
Blue Dragon








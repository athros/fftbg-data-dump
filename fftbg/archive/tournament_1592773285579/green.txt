Player: !Green
Team: Green Team
Palettes: Green/White



Deathmaker06
Male
Pisces
40
38
Geomancer
Charge
Counter
Doublehand
Move+2

Mythril Sword

Leather Hat
Leather Outfit
Power Wrist

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Charge+10



ColetteMSLP
Male
Scorpio
78
65
Summoner
White Magic
Counter
Equip Armor
Move+1

Dragon Rod

Leather Hat
Plate Mail
Feather Mantle

Moogle, Shiva, Ramuh, Carbunkle, Leviathan, Fairy, Lich, Cyclops
Reraise, Regen, Protect, Shell, Wall, Magic Barrier



Nizaha
Male
Cancer
75
65
Chemist
Basic Skill
Hamedo
Defense UP
Levitate

Mythril Gun

Triangle Hat
Rubber Costume
N-Kai Armlet

Potion, Hi-Potion, Ether, Echo Grass, Phoenix Down
Heal



Firesheath
Female
Aries
64
58
Mediator
Time Magic
Counter
Halve MP
Levitate

Mythril Gun

Red Hood
White Robe
Spike Shoes

Invitation, Praise, Preach, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Haste, Stop, Quick, Demi, Demi 2, Stabilize Time

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ShintaroNayaka
Female
Aries
49
72
Squire
Throw
Parry
Doublehand
Lava Walking

Slasher

Triangle Hat
Adaman Vest
108 Gems

Accumulate, Dash, Heal, Tickle, Cheer Up
Knife



Safira125
Male
Aquarius
80
79
Mediator
Steal
Counter Magic
Halve MP
Move-MP Up

Mythril Gun

Red Hood
Black Costume
Small Mantle

Threaten, Preach, Insult, Negotiate, Mimic Daravon, Refute
Gil Taking, Steal Helmet, Leg Aim



Athros13
Female
Leo
63
58
Summoner
Black Magic
Sunken State
Magic Attack UP
Move+2

Thunder Rod

Thief Hat
Black Robe
Wizard Mantle

Ifrit, Golem, Carbunkle, Salamander, Silf, Fairy
Fire 4, Bolt 2, Bolt 3, Ice 2, Empower



GrandmasterFrankerZ
Male
Pisces
58
77
Archer
Yin Yang Magic
Counter Flood
Doublehand
Move+3

Poison Bow

Triangle Hat
Black Costume
Dracula Mantle

Charge+1, Charge+10
Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



RaIshtar
Male
Sagittarius
69
43
Thief
Punch Art
Parry
Defense UP
Jump+3

Blood Sword

Barette
Adaman Vest
Battle Boots

Steal Heart, Steal Armor, Steal Shield, Steal Status
Secret Fist, Chakra, Revive



GrandmasterFrankerZ
Male
Aries
71
50
Knight
Time Magic
Counter
Equip Knife
Move-MP Up

Wizard Rod
Bronze Shield
Bronze Helmet
Chain Mail
Vanish Mantle

Magic Break, Speed Break, Justice Sword
Haste, Slow 2, Stabilize Time, Meteor



Electric Algus
Male
Sagittarius
52
71
Thief
Charge
MP Restore
Equip Gun
Waterwalking

Stone Gun

Thief Hat
Black Costume
Germinas Boots

Steal Heart, Steal Armor, Arm Aim
Charge+3, Charge+5, Charge+20



Reddwind
Male
Serpentarius
79
70
Knight
Steal
Critical Quick
Dual Wield
Move+3

Giant Axe
Slasher
Circlet
Plate Mail
Salty Rage

Head Break, Power Break, Mind Break, Justice Sword
Steal Heart, Steal Armor, Steal Weapon, Arm Aim, Leg Aim

Final Bets: purple - 8 bets for 2,991G (39.0%, x1.57); brown - 11 bets for 4,681G (61.0%, x0.64)

purple bets:
Breakdown777: 1,453G (48.6%, 2,907G)
Thyrandaal: 532G (17.8%, 532G)
enkikavlar: 292G (9.8%, 292G)
ArashiKurobara: 264G (8.8%, 10,519G)
ar_tactic: 150G (5.0%, 65,772G)
mpghappiness: 100G (3.3%, 6,877G)
E_Ballard: 100G (3.3%, 886G)
Miku_Shikhu: 100G (3.3%, 8,662G)

brown bets:
BirbBrainsBot: 1,000G (21.4%, 51,153G)
edgehead62888: 1,000G (21.4%, 14,070G)
ShintaroNayaka: 624G (13.3%, 624G)
athros13: 500G (10.7%, 20,890G)
brokenknight201: 308G (6.6%, 308G)
RampagingRobot: 263G (5.6%, 11,263G)
Oobs56: 250G (5.3%, 44,245G)
getthemoneyz: 236G (5.0%, 1,186,655G)
just_here2: 200G (4.3%, 22,837G)
Rytor: 200G (4.3%, 15,169G)
nifboy: 100G (2.1%, 14,678G)

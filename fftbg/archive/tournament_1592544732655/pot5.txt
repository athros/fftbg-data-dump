Final Bets: red - 11 bets for 6,568G (60.1%, x0.66); yellow - 12 bets for 4,361G (39.9%, x1.51)

red bets:
SkylerBunny: 2,000G (30.5%, 113,987G)
Mesmaster: 1,000G (15.2%, 84,288G)
NicoSavoy: 1,000G (15.2%, 15,027G)
prince_rogers_nelson_: 576G (8.8%, 576G)
Laserman1000: 548G (8.3%, 548G)
NovaKnight21: 500G (7.6%, 500G)
ColetteMSLP: 300G (4.6%, 963G)
placidphoenix: 224G (3.4%, 224G)
KasugaiRoastedPeas: 200G (3.0%, 18,330G)
Lydian_C: 120G (1.8%, 22,908G)
holoatomos: 100G (1.5%, 100G)

yellow bets:
BirbBrainsBot: 1,000G (22.9%, 94,294G)
eudes89: 788G (18.1%, 788G)
HaateXIII: 532G (12.2%, 532G)
CapnChaos12: 500G (11.5%, 15,842G)
BStarTV: 500G (11.5%, 9,917G)
getthemoneyz: 254G (5.8%, 984,653G)
maleblackfiora: 224G (5.1%, 10,563G)
AltimaMantoid: 163G (3.7%, 1,063G)
ThisGuyLovesMath: 100G (2.3%, 100G)
Evewho: 100G (2.3%, 13,078G)
Avin_Chaos: 100G (2.3%, 18,452G)
Gatsbysghost: 100G (2.3%, 100G)

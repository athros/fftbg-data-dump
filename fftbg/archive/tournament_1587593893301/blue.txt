Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nizaha
Female
Sagittarius
52
44
Oracle
Dance
Critical Quick
Maintenance
Jump+3

Gokuu Rod

Green Beret
Mystic Vest
Defense Armlet

Blind, Spell Absorb, Life Drain, Pray Faith, Zombie, Confusion Song, Paralyze, Sleep
Witch Hunt, Wiznaibus



Roqqqpsi
Male
Taurus
65
57
Archer
Steal
Sunken State
Halve MP
Move+3

Silver Bow

Holy Miter
Wizard Outfit
Spike Shoes

Charge+1
Gil Taking, Steal Helmet, Steal Shield



Nedryerson01
Monster
Leo
70
63
Grenade










Maldoree
Female
Gemini
78
51
Geomancer
Talk Skill
Meatbone Slash
Defense UP
Lava Walking

Bizen Boat
Round Shield
Twist Headband
Chameleon Robe
Bracer

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Praise, Preach, Death Sentence, Insult, Refute

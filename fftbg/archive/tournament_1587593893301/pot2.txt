Final Bets: green - 14 bets for 7,885G (35.3%, x1.83); yellow - 23 bets for 14,421G (64.7%, x0.55)

green bets:
kai_shee: 2,000G (25.4%, 133,957G)
Lordminsc: 1,205G (15.3%, 44,185G)
fenixcrest: 1,128G (14.3%, 5,644G)
ungabunga_bot: 1,000G (12.7%, 217,055G)
braumbles: 500G (6.3%, 67,072G)
Lanshaft: 360G (4.6%, 846G)
Chuckolator: 338G (4.3%, 25,253G)
EnemyController: 338G (4.3%, 75,885G)
FoeSquirrel: 316G (4.0%, 632G)
lijarkh: 200G (2.5%, 14,366G)
KasugaiRoastedPeas: 200G (2.5%, 4,387G)
FriendSquirrel: 100G (1.3%, 34,409G)
readdesert: 100G (1.3%, 4,006G)
EmperorBeef: 100G (1.3%, 31,151G)

yellow bets:
Baron_von_Scrub: 2,500G (17.3%, 108,021G)
gooseyourself: 2,000G (13.9%, 18,838G)
HASTERIOUS: 2,000G (13.9%, 6,228G)
ko2q: 1,049G (7.3%, 2,099G)
BirbBrainsBot: 1,000G (6.9%, 132,795G)
edgehead62888: 1,000G (6.9%, 19,807G)
volgrathemoose: 559G (3.9%, 559G)
OmnibotGamma: 500G (3.5%, 3,609G)
marin1987: 500G (3.5%, 4,326G)
TheMurkGnome: 500G (3.5%, 14,864G)
getthemoneyz: 428G (3.0%, 555,596G)
Firesheath: 400G (2.8%, 8,789G)
waterwatereverywhere: 392G (2.7%, 392G)
killth3kid: 300G (2.1%, 10,439G)
nedryerson01: 266G (1.8%, 266G)
SoupyNootNoot: 200G (1.4%, 1,000G)
AllInBot: 177G (1.2%, 177G)
trigger15: 150G (1.0%, 742G)
Evewho: 100G (0.7%, 816G)
twelfthrootoftwo: 100G (0.7%, 7,281G)
pLifer: 100G (0.7%, 342G)
DeathTaxesAndAnime: 100G (0.7%, 13,441G)
datadrivenbot: 100G (0.7%, 703G)

Player: !Green
Team: Green Team
Palettes: Green/White



Bryan792
Female
Cancer
63
72
Wizard
Draw Out
Catch
Short Charge
Jump+2

Ice Rod

Holy Miter
Light Robe
Red Shoes

Fire 2, Fire 3, Fire 4, Bolt, Ice, Death
Koutetsu, Bizen Boat, Kikuichimoji, Chirijiraden



Evewho
Female
Virgo
49
43
Chemist
White Magic
Mana Shield
Maintenance
Lava Walking

Blind Knife

Golden Hairpin
Secret Clothes
Feather Boots

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Cure 2, Cure 3, Raise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna, Holy



Kai Shee
Male
Scorpio
77
43
Knight
Elemental
Mana Shield
Equip Knife
Levitate

Wizard Rod
Crystal Shield
Genji Helmet
Plate Mail
Wizard Mantle

Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Fenixcrest
Male
Capricorn
71
74
Chemist
Jump
Distribute
Long Status
Jump+3

Mage Masher

Flash Hat
Mystic Vest
Cherche

Hi-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Level Jump3, Vertical Jump6

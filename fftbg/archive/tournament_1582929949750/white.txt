Player: !White
Team: White Team
Palettes: White/Blue



Melon366
Female
Libra
48
43
Mime

Arrow Guard
Equip Armor
Levitate



Diamond Helmet
Carabini Mail
Angel Ring

Mimic




RunicMagus
Male
Serpentarius
76
45
Thief
Sing
Arrow Guard
Equip Gun
Move+2

Mythril Gun

Feather Hat
Black Costume
Small Mantle

Steal Armor
Magic Song, Nameless Song



Bad1dea
Monster
Sagittarius
80
41
Bomb










KingSevens
Female
Aries
66
52
Squire
Jump
Counter Flood
Martial Arts
Move+1



Flash Hat
Plate Mail
Angel Ring

Accumulate, Heal, Yell, Cheer Up, Wish
Level Jump4, Vertical Jump7

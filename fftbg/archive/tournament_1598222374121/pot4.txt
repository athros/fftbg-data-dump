Final Bets: purple - 8 bets for 2,579G (26.9%, x2.72); brown - 9 bets for 7,007G (73.1%, x0.37)

purple bets:
getthemoneyz: 532G (20.6%, 1,730,670G)
maximumcrit: 500G (19.4%, 12,599G)
killth3kid: 500G (19.4%, 500G)
Sairentozon7: 400G (15.5%, 400G)
7Cerulean7: 240G (9.3%, 4,130G)
AllInBot: 220G (8.5%, 220G)
gorgewall: 101G (3.9%, 20,792G)
BirbBrainsBot: 86G (3.3%, 158,908G)

brown bets:
Shalloween: 2,000G (28.5%, 24,341G)
DustBirdEX: 1,428G (20.4%, 1,428G)
nifboy: 1,000G (14.3%, 10,584G)
Forkmore: 600G (8.6%, 600G)
nekojin: 512G (7.3%, 512G)
Thyrandaal: 440G (6.3%, 440G)
cloudycube: 423G (6.0%, 423G)
dogsandcatsand: 404G (5.8%, 404G)
datadrivenbot: 200G (2.9%, 68,627G)

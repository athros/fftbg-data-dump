Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KasugaiRoastedPeas
Male
Sagittarius
52
58
Wizard
Steal
Meatbone Slash
Magic Defense UP
Levitate

Mythril Knife

Thief Hat
Earth Clothes
Sprint Shoes

Fire, Fire 3, Fire 4, Bolt 3, Ice 2, Ice 3, Ice 4, Empower, Flare
Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Accessory



Nifboy
Male
Sagittarius
75
62
Wizard
Math Skill
Sunken State
Magic Attack UP
Jump+2

Poison Rod

Black Hood
White Robe
Cursed Ring

Fire 2, Fire 4, Bolt 3, Bolt 4, Empower, Death, Flare
Height, Prime Number, 5, 4, 3



Miku Shikhu
Female
Libra
57
74
Chemist
Punch Art
Blade Grasp
Maintenance
Waterbreathing

Cute Bag

Flash Hat
Adaman Vest
Magic Ring

Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Purification, Revive, Seal Evil



WhattayaBrian
Female
Taurus
70
69
Oracle
Charge
Distribute
Magic Attack UP
Jump+1

Battle Bamboo

Feather Hat
Black Robe
Leather Mantle

Blind, Poison, Pray Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Charge+1, Charge+3, Charge+5, Charge+10

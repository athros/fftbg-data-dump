Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



RunicMagus
Female
Taurus
62
44
Mediator
Draw Out
Regenerator
Concentrate
Fly

Papyrus Codex

Black Hood
Adaman Vest
Sprint Shoes

Praise, Solution, Negotiate, Refute, Rehabilitate
Murasame, Kiyomori



Omegasuspekt
Male
Capricorn
63
61
Geomancer
Sing
Parry
Equip Gun
Teleport

Stone Gun
Flame Shield
Golden Hairpin
Earth Clothes
Power Wrist

Pitfall, Water Ball, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Life Song, Cheer Song, Battle Song, Magic Song, Nameless Song



Maximumcrit
Female
Cancer
79
45
Mediator
Elemental
Auto Potion
Equip Gun
Teleport

Battle Folio

Flash Hat
Chameleon Robe
Power Wrist

Persuade, Threaten, Solution, Mimic Daravon, Refute
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Strumisgod
Monster
Virgo
66
72
Steel Giant








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Baron Von Scrub
Monster
Cancer
55
77
Black Chocobo










ColetteMSLP
Male
Gemini
56
70
Wizard
Charge
Counter
Short Charge
Teleport 2

Mage Masher

Holy Miter
Leather Outfit
Battle Boots

Fire 4, Bolt, Bolt 4, Ice 2, Ice 4
Charge+1, Charge+5, Charge+10



Error72
Female
Aquarius
57
47
Knight
Jump
MA Save
Equip Polearm
Waterwalking

Holy Lance
Mythril Shield
Diamond Helmet
Chameleon Robe
Magic Ring

Shield Break, Weapon Break, Speed Break, Justice Sword
Level Jump8, Vertical Jump7



Megaman2202
Female
Capricorn
76
54
Chemist
Steal
Auto Potion
Equip Axe
Move+1

Flame Whip

Cachusha
Power Sleeve
Feather Boots

Potion, X-Potion, Maiden's Kiss, Soft, Phoenix Down
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Leg Aim

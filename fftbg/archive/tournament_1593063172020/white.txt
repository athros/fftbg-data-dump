Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Aries
50
44
Knight
Draw Out
Critical Quick
Maintenance
Move+2

Ragnarok
Genji Shield
Circlet
Linen Robe
Elf Mantle

Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Night Sword
Koutetsu, Bizen Boat



KaLam1ty
Male
Scorpio
62
64
Mime

Abandon
Equip Armor
Ignore Terrain



Twist Headband
Silk Robe
Vanish Mantle

Mimic




ColetteMSLP
Monster
Cancer
55
80
Squidraken










Evewho
Female
Virgo
51
79
Chemist
Dance
Damage Split
Maintenance
Waterbreathing

Mage Masher

Headgear
Mystic Vest
Diamond Armlet

Potion, X-Potion, Ether, Hi-Ether, Holy Water, Remedy
Wiznaibus, Disillusion, Nameless Dance, Void Storage

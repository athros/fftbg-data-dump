Player: !Black
Team: Black Team
Palettes: Black/Red



ApplesauceBoss
Male
Cancer
64
46
Lancer
Elemental
Counter Magic
Maintenance
Teleport 2

Obelisk
Buckler
Diamond Helmet
Leather Armor
Bracer

Level Jump8, Vertical Jump8
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind, Lava Ball



SuicideKing XIX
Male
Cancer
48
47
Geomancer
Summon Magic
Counter
Martial Arts
Lava Walking


Flame Shield
Flash Hat
Wizard Robe
Magic Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball
Moogle, Shiva, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Lich



CorpusCav
Female
Taurus
63
47
Dancer
Yin Yang Magic
Damage Split
Dual Wield
Teleport 2

Cashmere
Ryozan Silk
Black Hood
Linen Robe
Genji Gauntlet

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Void Storage, Nether Demon, Dragon Pit
Pray Faith, Zombie, Foxbird, Sleep, Petrify



Strifu
Male
Virgo
46
58
Samurai
White Magic
Counter Magic
Dual Wield
Ignore Height

Asura Knife
Kikuichimoji
Mythril Helmet
Carabini Mail
Feather Mantle

Bizen Boat, Heaven's Cloud, Kiyomori
Cure, Raise, Regen, Shell 2, Wall, Esuna

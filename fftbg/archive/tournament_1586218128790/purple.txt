Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Laserman1000
Female
Scorpio
62
78
Thief
Punch Art
Arrow Guard
Defense UP
Lava Walking

Short Edge

Golden Hairpin
Secret Clothes
Dracula Mantle

Gil Taking, Steal Helmet, Steal Shield, Steal Status, Arm Aim, Leg Aim
Wave Fist, Secret Fist, Chakra, Revive, Seal Evil



ZephyrTempest
Male
Capricorn
45
45
Bard
Elemental
Sunken State
Equip Axe
Levitate

Giant Axe

Feather Hat
Secret Clothes
Defense Armlet

Diamond Blade, Space Storage, Sky Demon, Hydra Pit
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Mtueni
Male
Capricorn
42
43
Oracle
Summon Magic
Parry
Magic Defense UP
Jump+1

Musk Rod

Golden Hairpin
Chameleon Robe
Feather Mantle

Poison, Doubt Faith, Silence Song, Dispel Magic, Paralyze
Moogle, Titan, Golem, Leviathan, Salamander, Lich



Jonusfatson
Male
Sagittarius
77
72
Squire
Talk Skill
Abandon
Long Status
Teleport

Slasher
Mythril Shield
Bronze Helmet
Leather Armor
Magic Gauntlet

Accumulate, Dash
Persuade, Solution, Death Sentence, Mimic Daravon, Rehabilitate

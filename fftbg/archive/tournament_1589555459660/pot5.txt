Final Bets: red - 18 bets for 9,421G (56.8%, x0.76); green - 13 bets for 7,174G (43.2%, x1.31)

red bets:
JumbocactuarX27: 1,856G (19.7%, 1,856G)
ACSpree: 1,434G (15.2%, 2,868G)
DavenIII: 1,200G (12.7%, 7,432G)
LeepingJJ: 696G (7.4%, 696G)
Zeroroute: 600G (6.4%, 600G)
Bilabrin: 500G (5.3%, 12,485G)
tinytittyfiend: 500G (5.3%, 14,659G)
holyonline: 500G (5.3%, 2,337G)
AUrato: 500G (5.3%, 2,845G)
CapnChaos12: 356G (3.8%, 356G)
DLJuggernaut: 328G (3.5%, 328G)
nifboy: 250G (2.7%, 2,179G)
Aestheta: 200G (2.1%, 1,963G)
O_Heyno: 101G (1.1%, 1,383G)
GrayGhostGaming: 100G (1.1%, 5,472G)
CosmicTactician: 100G (1.1%, 9,547G)
datadrivenbot: 100G (1.1%, 19,281G)
rawb2: 100G (1.1%, 1,149G)

green bets:
toka222: 1,300G (18.1%, 44,248G)
sinnyil2: 1,200G (16.7%, 33,993G)
ungabunga_bot: 1,000G (13.9%, 491,116G)
BirbBrainsBot: 885G (12.3%, 76,813G)
rocl: 596G (8.3%, 596G)
DustBirdEX: 516G (7.2%, 516G)
HaateXIII: 500G (7.0%, 1,542G)
fenaen: 396G (5.5%, 396G)
imafella: 285G (4.0%, 2,859G)
PookaPrime: 260G (3.6%, 260G)
MalakiGenesys: 112G (1.6%, 1,128G)
otakutaylor: 100G (1.4%, 1,445G)
getthemoneyz: 24G (0.3%, 670,397G)

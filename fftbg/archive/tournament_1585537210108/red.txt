Player: !Red
Team: Red Team
Palettes: Red/Brown



Bad1dea
Female
Taurus
78
69
Oracle
Punch Art
Speed Save
Equip Polearm
Move-HP Up

Ivory Rod

Thief Hat
Silk Robe
Angel Ring

Blind, Life Drain, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Earth Slash, Secret Fist, Purification, Revive



BuffaloCrunch
Female
Virgo
61
70
Priest
Draw Out
HP Restore
Magic Defense UP
Teleport

Flail

Headgear
Silk Robe
Feather Boots

Cure, Cure 2, Cure 4, Raise 2, Reraise, Regen, Shell 2, Esuna
Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji



Roofiepops
Monster
Serpentarius
59
43
Holy Dragon










HASTERIOUS
Female
Capricorn
61
55
Monk
Elemental
MA Save
Short Charge
Waterwalking



Thief Hat
Judo Outfit
Magic Ring

Spin Fist, Earth Slash, Purification, Chakra, Seal Evil
Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind

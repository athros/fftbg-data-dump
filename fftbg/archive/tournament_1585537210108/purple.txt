Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Silentperogy
Male
Capricorn
72
81
Mime

Mana Shield
Dual Wield
Fly



Black Hood
Wizard Outfit
108 Gems

Mimic




RjA0zcOQ96
Monster
Gemini
65
46
Wyvern










L2 Sentinel
Male
Cancer
67
52
Monk
Charge
MA Save
Equip Armor
Retreat



Cross Helmet
Diamond Armor
Bracer

Wave Fist, Secret Fist
Charge+1, Charge+3, Charge+4



Lali Lulelo
Female
Gemini
59
52
Geomancer
White Magic
Distribute
Dual Wield
Jump+3

Ancient Sword
Slasher
Flash Hat
Silk Robe
108 Gems

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Cure, Cure 3, Raise, Wall, Esuna

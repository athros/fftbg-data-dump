Player: !White
Team: White Team
Palettes: White/Blue



Twelfthrootoftwo
Male
Aquarius
53
61
Knight
Item
Counter Flood
Dual Wield
Move+3

Sleep Sword
Giant Axe
Genji Helmet
Black Robe
Dracula Mantle

Head Break, Armor Break, Shield Break, Speed Break, Mind Break, Stasis Sword
Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down



Chuckolator
Male
Aries
62
52
Wizard
Draw Out
Counter Tackle
Sicken
Ignore Terrain

Dragon Rod

Flash Hat
Wizard Robe
Genji Gauntlet

Fire, Bolt 2, Ice 3
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa



HaychDub
Male
Aries
62
75
Monk
Time Magic
Counter Tackle
Equip Shield
Swim


Diamond Shield
Flash Hat
Clothes
Germinas Boots

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Haste, Haste 2, Slow, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time



MrDiggs49
Male
Gemini
53
74
Priest
Steal
Meatbone Slash
Equip Axe
Lava Walking

Flail

Black Hood
Linen Robe
Rubber Shoes

Cure, Cure 2, Cure 4, Raise, Protect 2, Shell 2, Wall, Esuna
Steal Heart, Steal Shield, Steal Weapon, Steal Status

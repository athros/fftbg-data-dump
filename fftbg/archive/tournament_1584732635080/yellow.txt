Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ZephyrTempest
Female
Gemini
53
67
Chemist
Elemental
Parry
Equip Knife
Move+3

Main Gauche

Twist Headband
Wizard Outfit
Feather Mantle

Potion, Ether, Hi-Ether, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Evolvingdev
Female
Virgo
63
68
Monk
Jump
Auto Potion
Magic Defense UP
Move+1



Black Hood
Clothes
Power Wrist

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Level Jump8, Vertical Jump8



HaateXIII
Female
Virgo
62
80
Chemist
Draw Out
Brave Up
Magic Attack UP
Fly

Cute Bag

Green Beret
Judo Outfit
Power Wrist

Potion, Hi-Ether, Remedy
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



Winterharte
Female
Sagittarius
74
67
Chemist
Dance
Mana Shield
Short Charge
Retreat

Cute Bag

Black Hood
Earth Clothes
Cursed Ring

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water
Wiznaibus, Disillusion, Last Dance, Void Storage

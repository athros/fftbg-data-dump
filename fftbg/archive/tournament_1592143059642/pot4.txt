Final Bets: purple - 12 bets for 8,193G (43.3%, x1.31); brown - 7 bets for 10,741G (56.7%, x0.76)

purple bets:
Rytor: 3,665G (44.7%, 3,665G)
richardserious: 900G (11.0%, 4,240G)
Aldrammech: 882G (10.8%, 882G)
BirbBrainsBot: 692G (8.4%, 17,567G)
DeathTaxesAndAnime: 620G (7.6%, 620G)
KasugaiRoastedPeas: 400G (4.9%, 5,085G)
getthemoneyz: 352G (4.3%, 953,751G)
powergems: 250G (3.1%, 10,351G)
alterworlds: 132G (1.6%, 132G)
nappychicken: 100G (1.2%, 2,940G)
DeadGirlzREasy: 100G (1.2%, 10,107G)
datadrivenbot: 100G (1.2%, 38,931G)

brown bets:
maakur_: 6,087G (56.7%, 6,087G)
Breakdown777: 1,500G (14.0%, 74,229G)
d4rr1n: 1,000G (9.3%, 4,312G)
Thyrandaal: 1,000G (9.3%, 20,240G)
TheChainNerd: 954G (8.9%, 954G)
AllInBot: 100G (0.9%, 100G)
E_Ballard: 100G (0.9%, 6,557G)

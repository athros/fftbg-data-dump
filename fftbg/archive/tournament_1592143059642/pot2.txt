Final Bets: green - 13 bets for 6,106G (29.4%, x2.40); yellow - 6 bets for 14,683G (70.6%, x0.42)

green bets:
Baron_von_Scrub: 1,111G (18.2%, 29,896G)
BirbBrainsBot: 1,000G (16.4%, 19,567G)
Thyrandaal: 1,000G (16.4%, 20,554G)
SeniorBunk: 760G (12.4%, 8,660G)
DeathTaxesAndAnime: 673G (11.0%, 673G)
getthemoneyz: 362G (5.9%, 955,113G)
KittymanAdvance: 300G (4.9%, 7,788G)
d4rr1n: 300G (4.9%, 5,612G)
sososteve: 200G (3.3%, 3,877G)
AllInBot: 100G (1.6%, 100G)
DeadGirlzREasy: 100G (1.6%, 10,207G)
datadrivenbot: 100G (1.6%, 39,131G)
IphoneDarkness: 100G (1.6%, 3,080G)

yellow bets:
VolgraTheMoose: 7,636G (52.0%, 68,296G)
KasugaiRoastedPeas: 3,747G (25.5%, 3,747G)
Breakdown777: 1,500G (10.2%, 72,576G)
Rytor: 1,000G (6.8%, 3,249G)
TheChainNerd: 400G (2.7%, 400G)
ShintaroNayaka: 400G (2.7%, 10,078G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Arkreaver
Female
Sagittarius
60
58
Knight
Item
Abandon
Throw Item
Retreat

Save the Queen
Crystal Shield
Bronze Helmet
Genji Armor
Defense Armlet

Head Break, Stasis Sword, Justice Sword
Potion, X-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



Jaritras
Female
Scorpio
48
58
Knight
Summon Magic
Meatbone Slash
Equip Gun
Swim

Fairy Harp
Aegis Shield
Crystal Helmet
Black Robe
Defense Ring

Head Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Night Sword
Moogle, Lich



Nifboy
Male
Aries
57
58
Mime

Distribute
Magic Defense UP
Retreat



Cross Helmet
Earth Clothes
Magic Ring

Mimic




Lawndough
Female
Scorpio
39
72
Mime

Mana Shield
Doublehand
Jump+3



Feather Hat
Judo Outfit
Battle Boots

Mimic


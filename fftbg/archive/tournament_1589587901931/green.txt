Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Female
Aquarius
75
56
Mediator
Elemental
Counter Magic
Equip Bow
Lava Walking

Hunting Bow

Holy Miter
Chameleon Robe
N-Kai Armlet

Invitation, Praise, Threaten, Preach, Solution, Negotiate, Mimic Daravon
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



Volgrathemoose
Monster
Virgo
50
79
Ultima Demon










SkylerBunny
Female
Capricorn
48
51
Dancer
Battle Skill
Brave Up
Martial Arts
Ignore Terrain

Persia

Feather Hat
Chameleon Robe
N-Kai Armlet

Wiznaibus, Slow Dance, Disillusion
Head Break, Shield Break, Weapon Break, Speed Break, Night Sword, Surging Sword



Who Lio42
Female
Sagittarius
43
71
Geomancer
Talk Skill
Distribute
Defense UP
Move+1

Asura Knife
Venetian Shield
Golden Hairpin
Linen Robe
N-Kai Armlet

Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Persuade, Praise, Threaten, Insult

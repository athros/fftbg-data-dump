Player: !White
Team: White Team
Palettes: White/Blue



Cryptopsy70
Male
Pisces
67
46
Bard
Punch Art
MP Restore
Equip Shield
Teleport

Fairy Harp
Diamond Shield
Triangle Hat
Power Sleeve
Elf Mantle

Cheer Song, Magic Song, Hydra Pit
Secret Fist, Purification, Revive



Strumisgod
Monster
Libra
72
66
Minotaur










Chuckolator
Female
Virgo
49
63
Priest
Math Skill
MP Restore
Equip Knife
Move+3

Wizard Rod

Black Hood
Black Costume
Jade Armlet

Cure, Raise 2, Protect, Esuna
CT, Prime Number, 5, 4



Gelwain
Male
Libra
74
54
Archer
Yin Yang Magic
Auto Potion
Equip Axe
Ignore Terrain

Mace of Zeus
Buckler
Ribbon
Leather Outfit
Setiemson

Charge+3, Charge+5, Charge+7
Life Drain, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze

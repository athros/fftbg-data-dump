Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ALY327
Male
Taurus
75
76
Knight
Elemental
Abandon
Short Charge
Jump+2

Defender
Gold Shield
Barbuta
Silk Robe
Spike Shoes

Head Break, Armor Break, Power Break, Justice Sword, Surging Sword
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Mr Schwifty
Monster
Taurus
78
79
Byblos










ShintaroNayaka
Male
Aries
61
38
Squire
Item
Counter
Equip Sword
Retreat

Save the Queen

Mythril Helmet
Power Sleeve
Spike Shoes

Accumulate, Heal, Yell, Cheer Up, Fury
Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



TheChainNerd
Female
Libra
67
52
Lancer
Talk Skill
Parry
Monster Talk
Move+1

Ivory Rod
Aegis Shield
Mythril Helmet
Bronze Armor
108 Gems

Level Jump2, Vertical Jump5
Preach, Refute

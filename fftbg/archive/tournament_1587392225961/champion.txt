Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Zachara
Female
Aries
53
49
Summoner
Draw Out
Faith Up
Short Charge
Swim

Rod

Feather Hat
Chameleon Robe
Elf Mantle

Moogle, Ramuh, Golem, Bahamut, Leviathan, Silf, Cyclops
Asura, Koutetsu, Bizen Boat, Masamune



LDSkinny
Female
Gemini
46
68
Summoner
Charge
HP Restore
Short Charge
Levitate

Thunder Rod

Feather Hat
White Robe
Sprint Shoes

Moogle, Titan, Carbunkle, Bahamut
Charge+2, Charge+4



Coralreeferz
Female
Pisces
50
42
Mediator
Punch Art
Blade Grasp
Dual Wield
Lava Walking

Stone Gun
Romanda Gun
Headgear
Black Costume
Red Shoes

Invitation, Praise
Pummel, Earth Slash, Purification, Revive



StealthModeLocke
Female
Virgo
48
78
Wizard
Yin Yang Magic
Counter
Short Charge
Ignore Height

Thunder Rod

Leather Hat
Linen Robe
Reflect Ring

Fire 2, Bolt 2, Ice, Ice 2, Ice 4, Frog, Death
Blind, Poison, Life Drain, Dispel Magic, Sleep, Petrify

Player: !Black
Team: Black Team
Palettes: Black/Red



FoeSquirrel
Monster
Taurus
48
79
Gobbledeguck










KasugaiRoastedPeas
Male
Sagittarius
64
40
Mime

Mana Shield
Magic Defense UP
Teleport



Flash Hat
Brigandine
108 Gems

Mimic




Luminarii
Male
Aries
66
65
Summoner
Punch Art
Mana Shield
Short Charge
Teleport

Dragon Rod

Twist Headband
Black Robe
Germinas Boots

Titan, Fairy
Spin Fist, Secret Fist, Chakra, Revive



Fenrislfr
Male
Leo
44
52
Ninja
Item
Speed Save
Magic Attack UP
Jump+2

Scorpion Tail
Flail
Golden Hairpin
Judo Outfit
Small Mantle

Bomb
Potion, Hi-Potion, X-Potion, Echo Grass, Phoenix Down

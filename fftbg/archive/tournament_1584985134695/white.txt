Player: !White
Team: White Team
Palettes: White/Blue



Lodrak
Female
Virgo
75
40
Samurai
Item
Counter Tackle
Dual Wield
Levitate

Asura Knife
Heaven's Cloud
Mythril Helmet
Genji Armor
Small Mantle

Koutetsu, Murasame, Muramasa
Potion, Hi-Potion, Ether, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



GameCorps
Male
Cancer
74
49
Geomancer
Talk Skill
Catch
Monster Talk
Move-HP Up

Giant Axe
Kaiser Plate
Green Beret
Robe of Lords
Angel Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard
Invitation, Praise, Preach, Death Sentence, Insult, Negotiate, Refute



Zeroroute
Female
Cancer
67
73
Mime

Absorb Used MP
Long Status
Waterwalking


Aegis Shield
Black Hood
Adaman Vest
Magic Ring

Mimic




Dantetouhou
Female
Aries
74
78
Knight
Basic Skill
Caution
Doublehand
Waterwalking

Ragnarok

Crystal Helmet
Plate Mail
Feather Boots

Armor Break, Shield Break, Weapon Break, Power Break, Stasis Sword, Dark Sword
Heal, Tickle, Yell, Fury, Scream

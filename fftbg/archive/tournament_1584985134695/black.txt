Player: !Black
Team: Black Team
Palettes: Black/Red



Spit8866
Male
Capricorn
49
73
Wizard
Charge
Critical Quick
Attack UP
Swim

Thunder Rod

Feather Hat
Mythril Vest
Bracer

Fire 2, Bolt, Bolt 2, Ice 2, Ice 3
Charge+1, Charge+3, Charge+4



Evdoggity
Female
Aries
47
77
Mime

MP Restore
Doublehand
Waterbreathing



Headgear
Wizard Outfit
Vanish Mantle

Mimic




Leakimiko
Male
Scorpio
79
55
Geomancer
Time Magic
MA Save
Magic Attack UP
Lava Walking

Koutetsu Knife
Gold Shield
Feather Hat
Mystic Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Haste 2, Slow 2, Immobilize, Float, Quick, Stabilize Time



Jethrothrul
Male
Sagittarius
79
61
Time Mage
Black Magic
Distribute
Equip Bow
Move+2

Night Killer

Triangle Hat
Chameleon Robe
Spike Shoes

Haste, Haste 2, Slow, Slow 2, Immobilize, Stabilize Time
Bolt, Bolt 2, Ice 4, Frog, Flare

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Gorgewall
Male
Aries
48
49
Thief
Punch Art
Brave Save
Dual Wield
Move+3

Orichalcum
Coral Sword
Golden Hairpin
Rubber Costume
Magic Gauntlet

Steal Accessory, Leg Aim
Pummel, Wave Fist, Purification, Revive



Lemonjohns
Female
Pisces
49
47
Geomancer
Steal
Counter Magic
Defend
Fly

Murasame
Flame Shield
Leather Hat
Mythril Vest
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Steal Shield, Steal Accessory



Resjudicata3
Monster
Taurus
70
73
Red Dragon










Dtrain332
Female
Serpentarius
76
64
Priest
Time Magic
Counter
Equip Gun
Waterwalking

Bloody Strings

Leather Hat
Wizard Robe
Magic Ring

Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell, Wall
Haste, Slow, Immobilize, Float, Demi 2, Stabilize Time

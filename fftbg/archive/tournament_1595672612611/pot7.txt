Final Bets: blue - 10 bets for 5,227G (46.8%, x1.14); white - 7 bets for 5,950G (53.2%, x0.88)

blue bets:
AllInBot: 2,199G (42.1%, 2,199G)
VolgraTheMoose: 780G (14.9%, 780G)
NovaKnight21: 764G (14.6%, 764G)
getthemoneyz: 400G (7.7%, 1,388,235G)
ruleof5: 300G (5.7%, 15,557G)
dtrain332: 284G (5.4%, 284G)
datadrivenbot: 200G (3.8%, 41,607G)
DouglasDragonThePoet: 100G (1.9%, 4,164G)
renkei_fukai: 100G (1.9%, 100G)
GrayGhostGaming: 100G (1.9%, 28,816G)

white bets:
UmaiJam: 1,700G (28.6%, 70,001G)
prince_rogers_nelson_: 1,000G (16.8%, 3,535G)
ColetteMSLP: 1,000G (16.8%, 34,869G)
BirbBrainsBot: 1,000G (16.8%, 66,358G)
Evewho: 991G (16.7%, 991G)
douchetron: 200G (3.4%, 6,196G)
ArlanKels: 59G (1.0%, 1,972G)

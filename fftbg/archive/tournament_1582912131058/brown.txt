Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Markese
Male
Cancer
73
42
Monk
Black Magic
Parry
Short Charge
Teleport



Green Beret
Brigandine
Germinas Boots

Pummel, Purification, Chakra
Fire 3, Fire 4, Bolt 4, Ice 3, Empower



Aislingeach
Male
Taurus
79
68
Thief
Summon Magic
Counter Flood
Short Status
Move+2

Short Edge

Golden Hairpin
Judo Outfit
Sprint Shoes

Gil Taking, Steal Shield, Arm Aim
Moogle, Ifrit, Carbunkle, Odin, Leviathan, Salamander, Silf



UFOCATCHERDAILY
Female
Scorpio
38
78
Oracle
Punch Art
Distribute
Equip Knife
Ignore Terrain

Short Edge

Headgear
Black Robe
Jade Armlet

Zombie, Blind Rage, Sleep
Pummel, Wave Fist, Earth Slash, Purification, Revive



Mushmeal
Male
Aquarius
72
73
Time Mage
Charge
PA Save
Maintenance
Retreat

White Staff

Feather Hat
Black Robe
Feather Boots

Haste, Slow, Slow 2, Float, Stabilize Time
Charge+1, Charge+7, Charge+20

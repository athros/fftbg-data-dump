Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Hyvi
Male
Cancer
65
61
Mime

Caution
Equip Armor
Jump+2



Mythril Helmet
Light Robe
Reflect Ring

Mimic




Omegasuspekt
Female
Leo
54
44
Wizard
Punch Art
Sunken State
Short Charge
Move-HP Up

Rod

Feather Hat
Chain Vest
Small Mantle

Fire, Bolt, Bolt 4, Ice 2, Ice 3, Flare
Pummel, Wave Fist, Purification, Chakra, Revive



TheKillerNacho
Male
Scorpio
55
49
Samurai
Elemental
Counter
Equip Sword
Jump+3

Bizen Boat

Diamond Helmet
Black Robe
Feather Boots

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Pitfall, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



MrUbiq
Male
Cancer
71
49
Bard
Draw Out
Brave Up
Halve MP
Lava Walking

Ice Bow

Red Hood
Black Costume
Setiemson

Cheer Song, Battle Song, Magic Song, Nameless Song, Sky Demon
Asura, Heaven's Cloud, Muramasa, Kikuichimoji

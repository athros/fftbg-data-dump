Player: !zChamp
Team: Champion Team
Palettes: Green/White



Ranmilia
Female
Taurus
63
75
Geomancer
White Magic
Caution
Magic Attack UP
Move+3

Asura Knife
Ice Shield
Flash Hat
Wizard Robe
Rubber Shoes

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Cure 4, Raise, Reraise, Regen, Protect, Shell, Wall, Esuna



WhiteTigress
Male
Sagittarius
46
73
Lancer
Item
Arrow Guard
Halve MP
Move+1

Partisan
Kaiser Plate
Gold Helmet
Light Robe
Genji Gauntlet

Level Jump8, Vertical Jump7
Soft, Holy Water, Phoenix Down



Sinnyil2
Male
Pisces
52
68
Archer
Talk Skill
Arrow Guard
Doublehand
Move-MP Up

Cross Bow

Bronze Helmet
Leather Outfit
Battle Boots

Charge+1, Charge+3, Charge+5, Charge+7
Invitation, Threaten, Negotiate, Mimic Daravon, Rehabilitate



Shs
Male
Taurus
77
67
Chemist
Yin Yang Magic
MA Save
Maintenance
Teleport

Hydra Bag

Flash Hat
Mythril Vest
Defense Armlet

X-Potion, Ether, Phoenix Down
Blind, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy

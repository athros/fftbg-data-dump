Player: !Black
Team: Black Team
Palettes: Black/Red



MyFakeLife
Male
Libra
79
56
Wizard
Time Magic
Brave Up
Concentrate
Move+1

Wizard Rod

Flash Hat
White Robe
Genji Gauntlet

Fire 2, Fire 3, Bolt 3, Ice 2, Ice 4, Frog, Death
Haste 2, Slow, Slow 2, Immobilize, Float, Meteor



Saga A
Female
Taurus
73
54
Priest
Draw Out
Meatbone Slash
Magic Attack UP
Waterwalking

Mace of Zeus

Leather Hat
Wizard Outfit
Red Shoes

Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell 2, Esuna
Koutetsu, Bizen Boat, Kikuichimoji, Chirijiraden



DudeMonkey77
Female
Scorpio
78
44
Samurai
Item
PA Save
Doublehand
Teleport

Holy Lance

Circlet
Chain Mail
Reflect Ring

Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Potion, Maiden's Kiss, Soft, Phoenix Down



Xplosf
Female
Aries
69
49
Mediator
Throw
Distribute
Magic Attack UP
Jump+1

Star Bag

Red Hood
Chameleon Robe
Genji Gauntlet

Threaten, Refute
Wand

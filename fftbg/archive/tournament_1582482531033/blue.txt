Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



EizanTayama
Male
Pisces
78
67
Mediator
Punch Art
Speed Save
Long Status
Swim

Bestiary

Cachusha
Clothes
Red Shoes

Invitation, Threaten, Solution, Death Sentence, Insult
Wave Fist, Purification, Revive



Cainsith
Male
Libra
62
49
Monk
Sing
HP Restore
Attack UP
Lava Walking



Leather Hat
Leather Outfit
Bracer

Purification, Revive
Life Song, Magic Song, Space Storage, Sky Demon



OniXiion
Male
Taurus
39
69
Lancer
Charge
Auto Potion
Halve MP
Move-HP Up

Javelin
Genji Shield
Iron Helmet
Bronze Armor
Wizard Mantle

Level Jump4, Vertical Jump2
Charge+5, Charge+7, Charge+10, Charge+20



Athros13
Male
Libra
71
63
Knight
Elemental
Sunken State
Defend
Jump+2

Coral Sword
Flame Shield
Diamond Helmet
Genji Armor
Diamond Armlet

Armor Break, Shield Break, Magic Break, Power Break
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

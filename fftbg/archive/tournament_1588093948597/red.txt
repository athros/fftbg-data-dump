Player: !Red
Team: Red Team
Palettes: Red/Brown



Volgrathemoose
Female
Sagittarius
74
50
Mediator
Battle Skill
Damage Split
Defense UP
Move-HP Up

Mythril Gun

Triangle Hat
Wizard Outfit
Power Wrist

Invitation, Persuade, Threaten, Preach, Insult, Mimic Daravon, Refute
Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword



DeathTaxesAndAnime
Female
Aquarius
71
54
Samurai
Summon Magic
Absorb Used MP
Magic Defense UP
Teleport

Kikuichimoji

Crystal Helmet
Reflect Mail
Magic Gauntlet

Asura, Murasame, Heaven's Cloud, Muramasa
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Silf



Marrrrrrrrrrrrrrrrrrrr
Male
Serpentarius
79
51
Geomancer
Item
PA Save
Secret Hunt
Ignore Terrain

Platinum Sword
Crystal Shield
Headgear
Linen Robe
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Potion, Hi-Potion, X-Potion, Echo Grass, Phoenix Down



HorusTaurus
Male
Gemini
54
78
Monk
Draw Out
PA Save
Equip Armor
Move-MP Up



Iron Helmet
Linen Cuirass
Magic Ring

Spin Fist, Wave Fist, Purification
Asura, Murasame, Kikuichimoji

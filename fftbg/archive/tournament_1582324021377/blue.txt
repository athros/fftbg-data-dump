Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Error72
Male
Taurus
63
77
Bard
Battle Skill
MP Restore
Dual Wield
Move+3

Ultimus Bow

Golden Hairpin
Linen Cuirass
Battle Boots

Life Song, Magic Song, Space Storage, Sky Demon
Head Break, Power Break



Roselii49
Female
Taurus
55
56
Priest
Jump
Counter Flood
Secret Hunt
Jump+1

Oak Staff

Leather Hat
Chameleon Robe
Feather Mantle

Cure, Cure 2, Cure 3, Raise 2, Regen, Shell, Esuna
Level Jump8, Vertical Jump5



Vivithegr8
Female
Libra
59
63
Wizard
Elemental
Absorb Used MP
Martial Arts
Teleport



Barette
White Robe
Spike Shoes

Fire 3, Fire 4, Bolt, Ice 2, Ice 3, Death, Flare
Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind



Toka222
Male
Virgo
40
47
Archer
Time Magic
Counter Flood
Attack UP
Jump+3

Mythril Bow

Flash Hat
Mythril Vest
Reflect Ring

Charge+5, Charge+7
Haste, Reflect, Stabilize Time

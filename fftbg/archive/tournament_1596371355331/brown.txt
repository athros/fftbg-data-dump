Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dogsandcatsand
Female
Cancer
50
54
Oracle
Charge
Brave Save
Equip Polearm
Move-MP Up

Javelin

Triangle Hat
Black Robe
Wizard Mantle

Blind, Poison, Life Drain, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Charge+2, Charge+4



ALY327
Male
Aquarius
59
76
Knight
Basic Skill
Counter
Long Status
Levitate

Sleep Sword
Crystal Shield
Barbuta
Reflect Mail
Rubber Shoes

Head Break, Shield Break, Weapon Break, Stasis Sword, Dark Sword, Night Sword
Accumulate, Heal, Yell, Scream



Grininda
Male
Pisces
45
51
Lancer
Punch Art
Parry
Doublehand
Swim

Partisan

Crystal Helmet
Reflect Mail
Diamond Armlet

Level Jump3, Vertical Jump7
Spin Fist, Wave Fist, Secret Fist, Purification, Revive



Georgetta
Female
Virgo
78
45
Priest
Black Magic
Sunken State
Equip Axe
Levitate

Slasher

Golden Hairpin
Wizard Robe
Wizard Mantle

Cure, Cure 2, Raise, Reraise, Regen, Wall, Esuna
Fire 2, Fire 4, Bolt 4, Ice, Ice 3

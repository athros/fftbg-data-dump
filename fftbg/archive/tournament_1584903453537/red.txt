Player: !Red
Team: Red Team
Palettes: Red/Brown



Lowlf
Female
Sagittarius
64
54
Time Mage
Elemental
Parry
Magic Attack UP
Jump+3

Gold Staff

Flash Hat
Mystic Vest
Feather Boots

Haste, Haste 2, Slow 2, Stop, Reflect, Quick, Stabilize Time
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



Volgrathemoose
Female
Serpentarius
57
70
Thief
Yin Yang Magic
Brave Up
Defense UP
Jump+1

Main Gauche

Feather Hat
Mystic Vest
Defense Ring

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Arm Aim
Foxbird, Confusion Song, Paralyze, Petrify



Fenaen
Female
Scorpio
74
54
Mediator
Item
Hamedo
Throw Item
Waterbreathing

Glacier Gun

Twist Headband
Black Robe
N-Kai Armlet

Persuade, Praise, Preach, Solution, Death Sentence, Refute
Potion, Antidote, Eye Drop, Soft, Remedy, Phoenix Down



AoiTsukiYuri
Male
Virgo
55
76
Samurai
White Magic
Abandon
Equip Axe
Move+3

Morning Star

Bronze Helmet
Linen Robe
Reflect Ring

Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Cure 3, Raise, Raise 2, Reraise, Protect 2, Esuna

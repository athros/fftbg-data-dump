Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Evewho
Female
Cancer
68
60
Knight
Black Magic
Faith Up
Magic Defense UP
Fly

Defender
Buckler
Cross Helmet
Black Robe
Vanish Mantle

Head Break, Armor Break, Mind Break, Surging Sword
Fire 2, Fire 4, Bolt 2, Bolt 4, Ice, Ice 2, Frog, Flare



J2DaBibbles
Female
Pisces
62
78
Ninja
Time Magic
Meatbone Slash
Equip Gun
Ignore Height

Bestiary
Fairy Harp
Barette
Rubber Costume
Magic Ring

Shuriken, Ninja Sword
Slow, Slow 2, Float, Reflect, Quick, Demi 2



Mandoragan
Male
Aries
46
44
Samurai
Jump
Critical Quick
Sicken
Move+1

Murasame

Genji Helmet
Gold Armor
Salty Rage

Heaven's Cloud, Muramasa
Level Jump4, Vertical Jump3



Leakimiko
Male
Leo
76
79
Wizard
Talk Skill
Faith Up
Defend
Move+2

Poison Rod

Red Hood
Judo Outfit
Angel Ring

Fire 3, Bolt, Bolt 4, Ice 2
Threaten, Preach, Death Sentence, Refute, Rehabilitate

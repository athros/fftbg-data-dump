Player: !Red
Team: Red Team
Palettes: Red/Brown



RunicMagus
Male
Sagittarius
68
79
Lancer
Basic Skill
Absorb Used MP
Magic Attack UP
Jump+2

Partisan
Crystal Shield
Platinum Helmet
Mythril Armor
Defense Armlet

Level Jump5, Vertical Jump5
Accumulate, Dash, Heal, Tickle, Wish



Moonliquor
Female
Cancer
61
54
Lancer
Summon Magic
Faith Save
Maintenance
Teleport

Obelisk
Kaiser Plate
Leather Helmet
Genji Armor
Dracula Mantle

Level Jump3, Vertical Jump7
Moogle, Ifrit, Titan, Salamander, Fairy, Zodiac



Randgridr
Female
Libra
39
62
Samurai
Time Magic
Dragon Spirit
Secret Hunt
Waterwalking

Heaven's Cloud

Circlet
Linen Robe
Genji Gauntlet

Asura, Koutetsu, Murasame, Kiyomori, Kikuichimoji
Haste 2, Slow 2, Immobilize, Float, Demi 2, Stabilize Time



CT 5 Holy
Female
Pisces
79
75
Calculator
White Magic
Dragon Spirit
Magic Defense UP
Levitate

Wizard Rod

Triangle Hat
Mystic Vest
Wizard Mantle

CT, 5, 4, 3
Cure 2, Raise, Regen, Protect, Protect 2, Wall, Esuna

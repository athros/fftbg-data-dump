Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Waterwatereverywhere
Male
Aries
63
52
Priest
Time Magic
Faith Up
Equip Axe
Levitate

Giant Axe

Thief Hat
Power Sleeve
Power Wrist

Cure, Raise, Reraise, Esuna
Haste, Haste 2, Slow, Stop, Reflect, Quick, Stabilize Time



Ultraballer2000
Female
Cancer
73
72
Dancer
Basic Skill
Abandon
Defense UP
Jump+2

Cute Bag

Black Hood
Silk Robe
Small Mantle

Polka Polka, Disillusion, Last Dance, Obsidian Blade, Dragon Pit
Throw Stone, Heal, Tickle, Scream



RRazza
Male
Aquarius
40
74
Ninja
Steal
Abandon
Equip Gun
Ignore Height

Bestiary
Battle Folio
Triangle Hat
Brigandine
Diamond Armlet

Shuriken, Stick
Gil Taking, Steal Heart, Steal Shield, Steal Weapon



RonaldoTheGypsy
Male
Taurus
65
66
Knight
Summon Magic
Absorb Used MP
Equip Gun
Waterwalking

Battle Folio
Bronze Shield
Leather Helmet
Light Robe
Diamond Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Surging Sword
Moogle, Carbunkle, Leviathan, Lich

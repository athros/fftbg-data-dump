Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Realitydown
Female
Taurus
75
54
Geomancer
Summon Magic
Regenerator
Dual Wield
Levitate

Broad Sword
Battle Axe
Holy Miter
Chain Vest
Red Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Titan, Fairy



BenYuPoker
Male
Leo
44
57
Oracle
Draw Out
Absorb Used MP
Magic Defense UP
Lava Walking

Bestiary

Triangle Hat
Chameleon Robe
108 Gems

Poison, Spell Absorb, Silence Song, Dispel Magic
Koutetsu, Heaven's Cloud, Kikuichimoji, Masamune



Azreyam
Female
Taurus
78
46
Dancer
White Magic
Counter Flood
Short Status
Jump+3

Ryozan Silk

Triangle Hat
Linen Robe
Diamond Armlet

Witch Hunt, Wiznaibus, Disillusion, Nameless Dance, Last Dance
Cure, Cure 3, Cure 4, Raise, Reraise, Regen, Esuna



Rnark
Male
Scorpio
52
59
Time Mage
Charge
Faith Up
Sicken
Move+2

White Staff

Flash Hat
White Robe
Defense Ring

Haste, Haste 2, Slow, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time
Charge+2, Charge+4, Charge+5, Charge+7

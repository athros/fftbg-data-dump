Final Bets: black - 6 bets for 1,344G (16.1%, x5.22); brown - 7 bets for 7,018G (83.9%, x0.19)

black bets:
Zetchryn: 432G (32.1%, 432G)
AllInBot: 422G (31.4%, 422G)
getthemoneyz: 256G (19.0%, 1,316,718G)
Lydian_C: 121G (9.0%, 56,127G)
Firesheath: 100G (7.4%, 11,357G)
BirbBrainsBot: 13G (1.0%, 58,582G)

brown bets:
upvla: 3,000G (42.7%, 13,709G)
electric_algus: 1,000G (14.2%, 30,849G)
prince_rogers_nelson_: 970G (13.8%, 970G)
sinnyil2: 928G (13.2%, 928G)
UnderOneLight: 500G (7.1%, 1,026G)
byrdturbo: 420G (6.0%, 32,416G)
Avin_Chaos: 200G (2.8%, 9,859G)

Final Bets: blue - 5 bets for 2,670G (19.2%, x4.21); brown - 10 bets for 11,235G (80.8%, x0.24)

blue bets:
Sairentozon7: 1,000G (37.5%, 22,341G)
BirbBrainsBot: 1,000G (37.5%, 58,569G)
getthemoneyz: 420G (15.7%, 1,316,462G)
Evewho: 200G (7.5%, 4,082G)
douchetron: 50G (1.9%, 211G)

brown bets:
electric_algus: 5,000G (44.5%, 31,041G)
upvla: 3,000G (26.7%, 14,284G)
TheMurkGnome: 850G (7.6%, 13,354G)
byrdturbo: 666G (5.9%, 32,496G)
pandasforsale: 666G (5.9%, 18,089G)
Zetchryn: 432G (3.8%, 432G)
AllInBot: 200G (1.8%, 200G)
Avin_Chaos: 200G (1.8%, 9,897G)
Lydian_C: 121G (1.1%, 56,006G)
Firesheath: 100G (0.9%, 11,257G)

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Zmoses
Female
Gemini
54
65
Priest
Punch Art
Mana Shield
Short Charge
Jump+2

White Staff

Cachusha
Brigandine
Feather Mantle

Cure 3, Cure 4, Raise, Protect, Protect 2, Shell, Shell 2, Esuna
Wave Fist, Purification, Revive, Seal Evil



Bioticism
Male
Serpentarius
52
59
Mime

Counter
Equip Armor
Move+3



Crystal Helmet
Wizard Robe
Magic Ring

Mimic




Lythe Caraker
Female
Libra
59
54
Wizard
Talk Skill
Faith Save
Concentrate
Waterwalking

Dragon Rod

Triangle Hat
Robe of Lords
Red Shoes

Fire, Fire 3, Bolt, Bolt 3, Ice 3, Frog, Flare
Praise, Mimic Daravon, Refute, Rehabilitate



ALY327
Monster
Taurus
65
66
Red Dragon








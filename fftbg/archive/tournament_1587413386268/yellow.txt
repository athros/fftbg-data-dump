Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Evewho
Female
Aries
59
44
Calculator
White Magic
Parry
Magic Defense UP
Move+3

Battle Folio

Flash Hat
Wizard Robe
N-Kai Armlet

Height, Prime Number, 5, 4, 3
Cure 3, Raise, Wall, Esuna, Holy



Sairentozon7
Male
Scorpio
44
54
Oracle
Item
Caution
Maintenance
Jump+3

Battle Bamboo

Golden Hairpin
White Robe
Red Shoes

Foxbird, Dispel Magic, Petrify
Potion, Hi-Ether, Eye Drop, Echo Grass



Joewcarson
Male
Scorpio
49
43
Archer
Time Magic
Counter Flood
Dual Wield
Lava Walking

Silver Bow

Golden Hairpin
Judo Outfit
Power Wrist

Charge+2, Charge+5, Charge+10, Charge+20
Haste, Slow 2, Stop, Float, Reflect



LordMaxoss
Female
Taurus
61
65
Ninja
Item
Critical Quick
Equip Knife
Swim

Dragon Rod
Short Edge
Flash Hat
Adaman Vest
Battle Boots

Shuriken, Bomb
Hi-Potion, Ether, Eye Drop, Maiden's Kiss, Phoenix Down

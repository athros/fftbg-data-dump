Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Electric Glass
Male
Leo
80
79
Mime

Counter Flood
Dual Wield
Retreat



Platinum Helmet
Judo Outfit
Magic Gauntlet

Mimic




Byrdturbo
Male
Virgo
48
77
Oracle
Time Magic
Hamedo
Magic Defense UP
Move+3

Cypress Rod

Green Beret
Black Robe
Spike Shoes

Poison, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep
Haste, Slow 2, Stop, Immobilize, Reflect



Lijarkh
Male
Sagittarius
43
57
Monk
Time Magic
Counter Tackle
Equip Axe
Move-HP Up

Gold Staff

Headgear
Mythril Vest
Wizard Mantle

Pummel, Earth Slash, Purification, Revive, Seal Evil
Haste, Slow 2, Reflect, Quick, Demi, Demi 2, Stabilize Time



Laserman1000
Male
Leo
71
46
Monk
Draw Out
Faith Up
Halve MP
Jump+3



Feather Hat
Power Sleeve
Feather Boots

Pummel, Wave Fist, Purification, Revive, Seal Evil
Koutetsu, Muramasa, Masamune

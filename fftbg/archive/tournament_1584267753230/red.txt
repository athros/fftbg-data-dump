Player: !Red
Team: Red Team
Palettes: Red/Brown



LordSDB
Male
Aries
57
59
Thief
Punch Art
Speed Save
Beastmaster
Move+2

Sleep Sword

Triangle Hat
Judo Outfit
Rubber Shoes

Gil Taking, Steal Heart, Steal Weapon, Steal Status, Arm Aim
Earth Slash, Secret Fist, Seal Evil



Hzor
Male
Cancer
78
67
Ninja
Elemental
Sunken State
Equip Armor
Ignore Terrain

Orichalcum
Kunai
Crystal Helmet
Chain Mail
N-Kai Armlet

Shuriken, Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind



HASTERIOUS
Male
Virgo
59
43
Calculator
Yin Yang Magic
Damage Split
Defense UP
Retreat

Papyrus Codex

Leather Hat
Black Costume
108 Gems

CT, Prime Number, 5, 4
Blind, Spell Absorb, Foxbird, Dispel Magic, Sleep, Petrify



L2 Sentinel
Male
Taurus
55
58
Lancer
Time Magic
Counter Flood
Martial Arts
Ignore Terrain

Ivory Rod
Escutcheon
Bronze Helmet
Mythril Armor
Magic Gauntlet

Level Jump5, Vertical Jump7
Slow 2, Immobilize, Demi, Stabilize Time

Player: !White
Team: White Team
Palettes: White/Blue



NIghtdew14
Female
Virgo
42
42
Dancer
Summon Magic
Faith Save
Attack UP
Swim

Cashmere

Triangle Hat
Light Robe
Salty Rage

Witch Hunt, Slow Dance, Polka Polka, Disillusion
Moogle, Shiva, Ifrit, Carbunkle, Leviathan, Silf, Lich



Gr8keeper
Female
Aries
60
67
Knight
Basic Skill
Blade Grasp
Defense UP
Move+2

Battle Axe
Ice Shield
Circlet
Leather Armor
Defense Ring

Armor Break, Weapon Break, Speed Break
Throw Stone, Heal, Yell, Cheer Up



DudeMonkey77
Male
Virgo
37
51
Time Mage
Draw Out
Damage Split
Concentrate
Move-MP Up

White Staff

Flash Hat
Chameleon Robe
Diamond Armlet

Haste, Slow, Slow 2, Stop, Reflect, Quick, Demi, Stabilize Time
Koutetsu, Bizen Boat, Murasame, Kiyomori



Killth3kid
Male
Aquarius
77
59
Knight
Charge
Abandon
Magic Attack UP
Retreat

Sleep Sword
Flame Shield
Genji Helmet
Gold Armor
Feather Boots

Head Break, Power Break, Justice Sword
Charge+1, Charge+2, Charge+4

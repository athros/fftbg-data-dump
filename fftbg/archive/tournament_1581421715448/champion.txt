Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Sphintus Koyote
Monster
Scorpio
54
56
Floating Eye










Babinsquared
Female
Leo
62
50
Archer
Item
Damage Split
Throw Item
Ignore Height

Bow Gun
Round Shield
Triangle Hat
Clothes
Rubber Shoes

Charge+1, Charge+2
Eye Drop, Holy Water, Phoenix Down



Godsblackarm
Female
Aquarius
64
73
Summoner
Time Magic
Counter
Short Charge
Move+2

Wizard Rod

Thief Hat
Robe of Lords
Power Wrist

Moogle, Shiva, Carbunkle, Bahamut, Salamander, Cyclops
Haste 2, Slow 2, Stop, Quick, Demi



Astrozin11
Male
Leo
58
55
Bard
Punch Art
Meatbone Slash
Defend
Move+3

Bloody Strings

Leather Hat
Leather Vest
Power Wrist

Life Song, Cheer Song, Magic Song, Sky Demon
Purification, Revive

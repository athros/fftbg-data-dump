Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RughSontos
Male
Gemini
70
72
Geomancer
Jump
Counter Magic
Dual Wield
Move+2

Ancient Sword
Long Sword
Holy Miter
Linen Robe
108 Gems

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump5, Vertical Jump4



Bahumat989
Male
Libra
67
59
Geomancer
Steal
Counter Tackle
Concentrate
Teleport

Mythril Sword
Genji Shield
Red Hood
Chain Vest
Wizard Mantle

Pitfall, Hell Ivy, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Helmet, Steal Armor, Leg Aim



Vyroxe
Male
Leo
61
51
Oracle
Basic Skill
Abandon
Equip Sword
Jump+1

Diamond Sword

Ribbon
Linen Robe
Wizard Mantle

Blind, Poison, Blind Rage, Foxbird, Paralyze
Heal, Tickle, Yell, Wish



Hamborn
Male
Serpentarius
48
67
Bard
Basic Skill
Speed Save
Attack UP
Teleport

Ramia Harp

Flash Hat
Genji Armor
Genji Gauntlet

Life Song, Battle Song
Dash, Fury, Wish

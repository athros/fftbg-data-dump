Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ant Ihistamine
Female
Leo
54
68
Priest
Draw Out
Auto Potion
Secret Hunt
Ignore Terrain

Healing Staff

Golden Hairpin
Clothes
Salty Rage

Cure 2, Cure 3, Raise 2, Shell 2, Wall, Esuna
Bizen Boat, Heaven's Cloud



Realitydown
Male
Aquarius
75
79
Time Mage
Jump
Hamedo
Secret Hunt
Ignore Terrain

Bestiary

Headgear
White Robe
N-Kai Armlet

Haste
Level Jump4, Vertical Jump7



Z32o
Male
Aries
64
38
Monk
Elemental
Damage Split
Equip Knife
Move+3

Dragon Rod

Green Beret
Adaman Vest
Leather Mantle

Pummel, Secret Fist, Chakra, Revive
Pitfall, Will-O-Wisp, Gusty Wind, Lava Ball



KasugaiRoastedPeas
Female
Leo
53
81
Time Mage
Steal
HP Restore
Equip Knife
Ignore Terrain

Poison Rod

Twist Headband
Rubber Costume
Rubber Shoes

Haste, Haste 2, Slow, Slow 2, Immobilize, Float, Quick, Meteor
Steal Helmet

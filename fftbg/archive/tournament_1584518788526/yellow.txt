Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SeedSC
Female
Sagittarius
42
40
Summoner
Basic Skill
Parry
Short Charge
Fly

Rod

Golden Hairpin
Adaman Vest
Salty Rage

Moogle, Ramuh, Ifrit, Bahamut, Fairy, Lich, Cyclops
Throw Stone, Heal, Tickle, Scream



Negativedivide
Male
Aries
52
67
Oracle
Time Magic
Caution
Doublehand
Move+1

Battle Bamboo

Black Hood
Linen Robe
Magic Gauntlet

Life Drain, Pray Faith, Zombie, Foxbird
Slow 2, Stop, Float, Reflect, Demi, Stabilize Time, Galaxy Stop



Humble Fabio
Male
Pisces
77
51
Wizard
Charge
Earplug
Dual Wield
Swim

Blind Knife
Ice Rod
Black Hood
Wizard Robe
Red Shoes

Fire 2, Bolt, Bolt 2, Bolt 3, Death, Flare
Charge+1, Charge+2, Charge+3, Charge+7, Charge+20



ThanatosXRagnarok
Male
Aries
44
66
Oracle
Summon Magic
Critical Quick
Halve MP
Lava Walking

Iron Fan

Holy Miter
Secret Clothes
Battle Boots

Blind, Poison, Pray Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Odin, Salamander, Silf

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Roqqqpsi
Female
Scorpio
56
46
Priest
Talk Skill
MA Save
Magic Attack UP
Jump+1

Gold Staff

Golden Hairpin
Wizard Robe
Bracer

Raise, Reraise, Protect, Protect 2, Esuna, Holy
Invitation, Persuade, Solution, Insult, Negotiate, Mimic Daravon, Refute



TheGuesty
Female
Virgo
67
50
Geomancer
Basic Skill
Arrow Guard
Equip Sword
Move+3

Platinum Sword
Diamond Shield
Headgear
Wizard Robe
Genji Gauntlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm
Heal, Tickle, Ultima



UmaiJam
Male
Capricorn
51
75
Mediator
Steal
Distribute
Equip Knife
Move-HP Up

Flame Rod

Black Hood
Power Sleeve
Angel Ring

Solution, Death Sentence, Insult
Steal Heart, Steal Helmet, Steal Weapon, Steal Status, Leg Aim



Leakimiko
Female
Capricorn
82
73
Geomancer
Jump
HP Restore
Defend
Move+3

Muramasa
Hero Shield
Holy Miter
Robe of Lords
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Level Jump3, Vertical Jump8

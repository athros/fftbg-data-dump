Final Bets: black - 10 bets for 9,648G (74.2%, x0.35); purple - 8 bets for 3,352G (25.8%, x2.88)

black bets:
UmaiJam: 2,000G (20.7%, 19,772G)
TheChainNerd: 1,500G (15.5%, 18,673G)
conradharlock: 1,500G (15.5%, 12,019G)
Sairentozon7: 1,154G (12.0%, 1,154G)
Shinzutalos: 1,121G (11.6%, 1,121G)
Jeeboheebo: 1,000G (10.4%, 13,876G)
Zachara: 545G (5.6%, 101,545G)
megaman2202: 452G (4.7%, 452G)
LeoNightFury: 276G (2.9%, 276G)
datadrivenbot: 100G (1.0%, 32,883G)

purple bets:
BlackFireUK: 1,173G (35.0%, 1,173G)
BirbBrainsBot: 1,000G (29.8%, 19,304G)
getthemoneyz: 478G (14.3%, 809,373G)
gorgewall: 201G (6.0%, 4,007G)
gogofromtogo: 200G (6.0%, 3,997G)
E_Ballard: 100G (3.0%, 5,013G)
marin1987: 100G (3.0%, 974G)
CosmicTactician: 100G (3.0%, 16,752G)

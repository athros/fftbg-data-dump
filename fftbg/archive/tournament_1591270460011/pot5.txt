Final Bets: blue - 7 bets for 5,363G (53.4%, x0.87); green - 11 bets for 4,680G (46.6%, x1.15)

blue bets:
TheChainNerd: 1,500G (28.0%, 20,173G)
BirbBrainsBot: 1,000G (18.6%, 20,304G)
UmaiJam: 1,000G (18.6%, 20,772G)
LeoNightFury: 563G (10.5%, 563G)
BlackFireUK: 500G (9.3%, 1,673G)
megaman2202: 452G (8.4%, 452G)
getthemoneyz: 348G (6.5%, 809,721G)

green bets:
conradharlock: 1,500G (32.1%, 10,300G)
ShintaroNayaka: 1,089G (23.3%, 1,089G)
Sairentozon7: 536G (11.5%, 536G)
marin1987: 454G (9.7%, 454G)
gorgewall: 201G (4.3%, 3,777G)
gogofromtogo: 200G (4.3%, 3,768G)
outer_monologue: 200G (4.3%, 1,701G)
Zachara: 200G (4.3%, 101,316G)
CosmicTactician: 100G (2.1%, 16,637G)
E_Ballard: 100G (2.1%, 4,898G)
datadrivenbot: 100G (2.1%, 32,768G)

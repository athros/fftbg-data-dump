Player: !White
Team: White Team
Palettes: White/Blue



Breakdown777
Female
Capricorn
46
75
Wizard
Draw Out
Caution
Equip Knife
Fly

Sasuke Knife

Golden Hairpin
Secret Clothes
Battle Boots

Fire, Bolt 3, Bolt 4, Ice 2
Asura, Bizen Boat, Heaven's Cloud



ForagerCats
Monster
Libra
79
56
Vampire










Mesmaster
Male
Leo
54
61
Knight
White Magic
Dragon Spirit
Attack UP
Jump+1

Chaos Blade
Venetian Shield
Genji Helmet
Linen Cuirass
Cursed Ring

Head Break, Speed Break
Raise, Raise 2, Regen, Protect 2, Esuna



FoeSquirrel
Female
Taurus
78
68
Samurai
Basic Skill
Counter Flood
Long Status
Move+2

Bizen Boat

Leather Helmet
Black Robe
Rubber Shoes

Murasame, Kiyomori, Kikuichimoji
Accumulate, Throw Stone, Heal, Tickle, Wish

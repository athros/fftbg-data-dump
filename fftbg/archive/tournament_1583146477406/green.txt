Player: !Green
Team: Green Team
Palettes: Green/White



KitchTowel
Female
Capricorn
55
40
Priest
Black Magic
PA Save
Short Charge
Jump+1

White Staff

Red Hood
Wizard Robe
Sprint Shoes

Cure, Cure 2, Raise, Protect 2, Shell 2, Wall, Esuna
Ice 4, Frog, Death



Riot Jayway
Female
Sagittarius
36
41
Mime

Auto Potion
Martial Arts
Jump+2



Flash Hat
Brigandine
Genji Gauntlet

Mimic




Vapetrail
Female
Cancer
64
58
Thief
Throw
Absorb Used MP
Equip Knife
Levitate

Spell Edge

Ribbon
Rubber Costume
108 Gems

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Shuriken, Bomb, Staff



WhiteDog420
Female
Aries
71
73
Mediator
Black Magic
Earplug
Magic Defense UP
Move-HP Up

Battle Folio

Leather Hat
Chain Vest
Defense Ring

Praise, Threaten, Insult, Mimic Daravon, Refute
Fire, Fire 3, Ice 2

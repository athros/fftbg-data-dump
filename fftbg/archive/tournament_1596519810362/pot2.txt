Final Bets: green - 8 bets for 4,582G (38.6%, x1.59); yellow - 10 bets for 7,294G (61.4%, x0.63)

green bets:
Lydian_C: 1,200G (26.2%, 6,495G)
ColetteMSLP: 1,000G (21.8%, 19,071G)
Laserman1000: 600G (13.1%, 3,100G)
Evewho: 576G (12.6%, 576G)
nifboy: 524G (11.4%, 524G)
getthemoneyz: 382G (8.3%, 1,507,272G)
datadrivenbot: 200G (4.4%, 48,098G)
Brainstew29: 100G (2.2%, 37,037G)

yellow bets:
amiture: 3,000G (41.1%, 30,302G)
DeathTaxesAndAnime: 1,506G (20.6%, 1,506G)
Mesmaster: 997G (13.7%, 997G)
BirbBrainsBot: 718G (9.8%, 198,550G)
AllInBot: 272G (3.7%, 272G)
gorgewall: 201G (2.8%, 5,359G)
DesertWooder: 200G (2.7%, 3,370G)
UnderOneLight: 200G (2.7%, 289G)
slickdang: 100G (1.4%, 3,416G)
TheseJeans: 100G (1.4%, 1,825G)

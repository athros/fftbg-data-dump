Final Bets: green - 6 bets for 3,096G (39.5%, x1.53); black - 9 bets for 4,734G (60.5%, x0.65)

green bets:
BirbBrainsBot: 1,000G (32.3%, 199,103G)
WhiteTigress: 800G (25.8%, 18,064G)
getthemoneyz: 668G (21.6%, 1,731,907G)
Tarheels218: 328G (10.6%, 328G)
ProteinBiscuit: 200G (6.5%, 7,480G)
MemoriesofFinal: 100G (3.2%, 806G)

black bets:
DeathTaxesAndAnime: 2,875G (60.7%, 2,875G)
ForagerCats: 500G (10.6%, 3,278G)
dogsandcatsand: 404G (8.5%, 404G)
Mushufasa_: 224G (4.7%, 224G)
gongonono: 200G (4.2%, 1,839G)
datadrivenbot: 200G (4.2%, 66,600G)
AllInBot: 130G (2.7%, 130G)
gorgewall: 101G (2.1%, 20,310G)
FoxMime: 100G (2.1%, 907G)

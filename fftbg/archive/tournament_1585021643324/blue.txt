Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lodrak
Female
Gemini
78
42
Samurai
Talk Skill
Distribute
Equip Bow
Move-HP Up

Night Killer

Barbuta
Bronze Armor
Cherche

Asura, Bizen Boat, Murasame, Kikuichimoji
Invitation, Praise, Threaten, Negotiate



HASTERIOUS
Female
Taurus
72
47
Wizard
Elemental
Meatbone Slash
Short Charge
Retreat

Rod

Leather Hat
White Robe
Germinas Boots

Fire, Bolt 2, Bolt 3, Ice 2, Ice 4, Flare
Water Ball, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind



KasugaiRoastedPeas
Female
Virgo
48
68
Time Mage
Draw Out
Damage Split
Short Status
Teleport

Papyrus Codex

Leather Hat
Mystic Vest
Cherche

Stop, Float, Stabilize Time
Koutetsu, Murasame, Kiyomori



HRak050
Female
Sagittarius
55
71
Lancer
Throw
Regenerator
Equip Armor
Jump+1

Partisan
Mythril Shield
Cross Helmet
Earth Clothes
Magic Ring

Level Jump8, Vertical Jump7
Dictionary

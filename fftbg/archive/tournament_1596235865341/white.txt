Player: !White
Team: White Team
Palettes: White/Blue



Grandlanzer
Male
Aquarius
54
62
Samurai
Punch Art
Critical Quick
Doublehand
Lava Walking

Koutetsu Knife

Iron Helmet
Genji Armor
Leather Mantle

Asura, Murasame, Kiyomori, Muramasa
Wave Fist, Secret Fist



HaateXIII
Female
Gemini
79
62
Wizard
Math Skill
PA Save
Dual Wield
Teleport

Dragon Rod
Flame Rod
Thief Hat
Light Robe
Jade Armlet

Fire, Fire 2, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Empower
CT, Height, 3



Calajo
Male
Taurus
58
72
Ninja
Battle Skill
Faith Save
Halve MP
Fly

Sasuke Knife
Morning Star
Twist Headband
Rubber Costume
Feather Mantle

Shuriken, Spear
Shield Break, Speed Break, Power Break, Mind Break, Stasis Sword



CapnChaos12
Female
Gemini
49
74
Oracle
Talk Skill
PA Save
Doublehand
Swim

Battle Bamboo

Golden Hairpin
Clothes
Red Shoes

Blind, Zombie, Foxbird, Dispel Magic
Persuade, Threaten, Solution, Insult, Refute, Rehabilitate

Final Bets: blue - 16 bets for 7,586G (33.7%, x1.96); black - 26 bets for 14,893G (66.3%, x0.51)

blue bets:
Baron_von_Scrub: 1,111G (14.6%, 77,964G)
ungabunga_bot: 1,000G (13.2%, 176,897G)
BirbBrainsBot: 1,000G (13.2%, 5,188G)
Zeroroute: 809G (10.7%, 809G)
Shalloween: 600G (7.9%, 62,484G)
AllInBot: 542G (7.1%, 542G)
RunicMagus: 500G (6.6%, 37,767G)
flowinprose: 500G (6.6%, 74,566G)
Lanshaft: 336G (4.4%, 1,524G)
EnemyController: 300G (4.0%, 62,800G)
rico_flex: 300G (4.0%, 12,608G)
getthemoneyz: 188G (2.5%, 515,854G)
mexskacin: 100G (1.3%, 6,997G)
Jeeboheebo: 100G (1.3%, 6,547G)
FoeSquirrel: 100G (1.3%, 1,100G)
MeleeWizard: 100G (1.3%, 3,870G)

black bets:
Oreo_Pizza: 2,500G (16.8%, 10,553G)
volgrathemoose: 2,005G (13.5%, 150,947G)
Fenrislfr: 1,523G (10.2%, 15,230G)
Nizaha: 1,001G (6.7%, 4,832G)
roofiepops: 1,000G (6.7%, 17,799G)
DaveStrider55: 781G (5.2%, 781G)
waterwatereverywhere: 661G (4.4%, 661G)
Phi_Sig: 555G (3.7%, 145,400G)
killth3kid: 540G (3.6%, 540G)
fenaen: 500G (3.4%, 29,769G)
HaateXIII: 500G (3.4%, 20,760G)
ff_tactics_is_best_ff: 500G (3.4%, 949G)
Tougou: 410G (2.8%, 410G)
JackOnFire1: 383G (2.6%, 383G)
carsenhk: 300G (2.0%, 6,159G)
WittyNames: 231G (1.6%, 231G)
ericzubat: 212G (1.4%, 212G)
roqqqpsi: 208G (1.4%, 946G)
ANFz: 200G (1.3%, 11,585G)
elijahrockers: 200G (1.3%, 600G)
SwaffleWaffles: 182G (1.2%, 18,242G)
ZephyrTempest: 101G (0.7%, 160,492G)
IndecisiveNinja: 100G (0.7%, 114,220G)
GiantSeagull: 100G (0.7%, 100G)
datadrivenbot: 100G (0.7%, 1,213G)
FriendSquirrel: 100G (0.7%, 23,930G)

Final Bets: green - 10 bets for 7,099G (42.2%, x1.37); white - 7 bets for 9,730G (57.8%, x0.73)

green bets:
douchetron: 1,932G (27.2%, 3,790G)
Aldrammech: 1,095G (15.4%, 1,095G)
BirbBrainsBot: 1,000G (14.1%, 92,959G)
E_Ballard: 871G (12.3%, 871G)
Gunz232323: 668G (9.4%, 668G)
getthemoneyz: 590G (8.3%, 1,561,777G)
fenaen: 508G (7.2%, 6,127G)
ruleof5: 300G (4.2%, 8,653G)
MinBetBot: 100G (1.4%, 17,333G)
wkhumes: 35G (0.5%, 899G)

white bets:
reinoe: 3,000G (30.8%, 125,300G)
KasugaiRoastedPeas: 2,252G (23.1%, 2,252G)
DustBirdEX: 1,370G (14.1%, 2,741G)
ko2q: 1,137G (11.7%, 1,137G)
DeathTaxesAndAnime: 980G (10.1%, 980G)
AllInBot: 735G (7.6%, 735G)
DesertWooder: 256G (2.6%, 256G)

Final Bets: blue - 11 bets for 11,406G (52.0%, x0.92); green - 12 bets for 10,543G (48.0%, x1.08)

blue bets:
douchetron: 3,944G (34.6%, 7,734G)
Ayntlerz: 1,224G (10.7%, 1,224G)
ko2q: 1,197G (10.5%, 1,197G)
Aldrammech: 1,160G (10.2%, 1,160G)
reinoe: 1,000G (8.8%, 125,534G)
Seaweed_B: 1,000G (8.8%, 62,267G)
Smugzug: 581G (5.1%, 581G)
killth3kid: 552G (4.8%, 5,703G)
E_Ballard: 548G (4.8%, 548G)
MinBetBot: 100G (0.9%, 17,533G)
CosmicTactician: 100G (0.9%, 5,308G)

green bets:
dogsandcatsand: 5,000G (47.4%, 7,642G)
getthemoneyz: 1,000G (9.5%, 1,560,657G)
BirbBrainsBot: 1,000G (9.5%, 91,111G)
DeathTaxesAndAnime: 980G (9.3%, 980G)
Miku_Shikhu: 521G (4.9%, 521G)
letdowncity: 508G (4.8%, 29,095G)
brokenknight201: 500G (4.7%, 3,458G)
DesertWooder: 434G (4.1%, 434G)
AllInBot: 200G (1.9%, 200G)
nok____: 200G (1.9%, 2,400G)
lastly: 100G (0.9%, 51,356G)
nifboy: 100G (0.9%, 2,238G)

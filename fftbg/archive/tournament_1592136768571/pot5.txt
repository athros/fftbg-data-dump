Final Bets: blue - 6 bets for 3,212G (33.0%, x2.03); green - 9 bets for 6,526G (67.0%, x0.49)

blue bets:
BlackFireUK: 1,112G (34.6%, 1,112G)
Mesmaster: 1,000G (31.1%, 105,540G)
prince_rogers_nelson_: 500G (15.6%, 500G)
d4rr1n: 300G (9.3%, 5,412G)
Evewho: 200G (6.2%, 13,162G)
datadrivenbot: 100G (3.1%, 39,089G)

green bets:
Thyrandaal: 2,900G (44.4%, 24,728G)
NovaKnight21: 1,000G (15.3%, 4,612G)
BirbBrainsBot: 1,000G (15.3%, 19,662G)
reddwind_: 652G (10.0%, 652G)
TheGuesty: 500G (7.7%, 70,000G)
getthemoneyz: 174G (2.7%, 955,523G)
AllInBot: 100G (1.5%, 100G)
E_Ballard: 100G (1.5%, 6,460G)
mrfripps: 100G (1.5%, 4,118G)

Final Bets: blue - 10 bets for 4,571G (57.9%, x0.73); purple - 7 bets for 3,323G (42.1%, x1.38)

blue bets:
BirbBrainsBot: 1,000G (21.9%, 17,662G)
Mesmaster: 1,000G (21.9%, 107,572G)
Chronolynx42: 612G (13.4%, 1,225G)
prince_rogers_nelson_: 500G (10.9%, 1,516G)
ShintaroNayaka: 400G (8.8%, 8,282G)
TeaTime29: 300G (6.6%, 13,594G)
IphoneDarkness: 200G (4.4%, 5,207G)
d4rr1n: 200G (4.4%, 6,091G)
getthemoneyz: 190G (4.2%, 955,159G)
AllInBot: 169G (3.7%, 169G)

purple bets:
Thyrandaal: 1,000G (30.1%, 19,828G)
DeathTaxesAndAnime: 882G (26.5%, 882G)
bruubarg: 641G (19.3%, 641G)
BlackFireUK: 500G (15.0%, 4,526G)
mrfripps: 100G (3.0%, 4,087G)
datadrivenbot: 100G (3.0%, 39,361G)
E_Ballard: 100G (3.0%, 6,360G)

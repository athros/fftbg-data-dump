Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Sypheck
Monster
Capricorn
50
73
Explosive










Lydian C
Female
Libra
71
64
Samurai
Talk Skill
MA Save
Magic Attack UP
Lava Walking

Kiyomori

Platinum Helmet
Silk Robe
Genji Gauntlet

Asura, Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Invitation, Persuade, Praise, Death Sentence, Negotiate, Rehabilitate



Goust18
Monster
Cancer
55
62
Apanda










Musclestache
Male
Capricorn
52
65
Archer
Talk Skill
Meatbone Slash
Equip Gun
Jump+2

Yoichi Bow

Red Hood
Earth Clothes
108 Gems

Charge+1, Charge+4
Invitation, Threaten, Death Sentence, Refute

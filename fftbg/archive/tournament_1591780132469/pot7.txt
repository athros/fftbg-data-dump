Final Bets: yellow - 9 bets for 8,389G (42.3%, x1.36); black - 9 bets for 11,427G (57.7%, x0.73)

yellow bets:
Shakarak: 3,000G (35.8%, 167,499G)
Mesmaster: 2,619G (31.2%, 2,619G)
BirbBrainsBot: 1,000G (11.9%, 76,707G)
k1ngofthechill: 500G (6.0%, 7,115G)
BlackFireUK: 500G (6.0%, 23,602G)
helpimabug: 300G (3.6%, 300G)
getthemoneyz: 270G (3.2%, 889,831G)
GrayGhostGaming: 100G (1.2%, 9,210G)
Evewho: 100G (1.2%, 19,089G)

black bets:
DeathTaxesAndAnime: 6,439G (56.3%, 12,627G)
ChronoG: 3,612G (31.6%, 7,084G)
prince_rogers_nelson_: 432G (3.8%, 432G)
serperemagus: 240G (2.1%, 240G)
DAC169: 204G (1.8%, 204G)
kaidykat: 200G (1.8%, 32,794G)
AllInBot: 100G (0.9%, 100G)
Quadh0nk: 100G (0.9%, 578G)
datadrivenbot: 100G (0.9%, 37,273G)

Final Bets: yellow - 9 bets for 6,095G (45.7%, x1.19); champion - 8 bets for 7,229G (54.3%, x0.84)

yellow bets:
DeathTaxesAndAnime: 3,155G (51.8%, 6,188G)
BirbBrainsBot: 1,000G (16.4%, 78,069G)
BlackFireUK: 500G (8.2%, 24,283G)
Mesmaster: 500G (8.2%, 6,186G)
k1ngofthechill: 300G (4.9%, 7,796G)
serperemagus: 240G (3.9%, 240G)
TeaTime29: 200G (3.3%, 11,952G)
AllInBot: 100G (1.6%, 100G)
GrayGhostGaming: 100G (1.6%, 9,346G)

champion bets:
ChronoG: 3,472G (48.0%, 3,472G)
Evewho: 2,000G (27.7%, 19,225G)
helpimabug: 709G (9.8%, 709G)
prince_rogers_nelson_: 432G (6.0%, 432G)
kaidykat: 400G (5.5%, 32,594G)
Quadh0nk: 100G (1.4%, 478G)
datadrivenbot: 100G (1.4%, 37,173G)
getthemoneyz: 16G (0.2%, 890,199G)

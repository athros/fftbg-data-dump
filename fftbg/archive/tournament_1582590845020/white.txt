Player: !White
Team: White Team
Palettes: White/Blue



BlackFireUK
Male
Pisces
55
77
Knight
Draw Out
Arrow Guard
Equip Gun
Waterbreathing

Blaze Gun
Genji Shield
Mythril Helmet
Mythril Armor
Defense Ring

Head Break, Weapon Break, Speed Break, Power Break, Mind Break
Koutetsu, Muramasa



ValtonZenola
Female
Gemini
77
60
Ninja
Black Magic
PA Save
Equip Armor
Move+1

Flame Whip
Ninja Edge
Barbuta
Diamond Armor
108 Gems

Stick, Dictionary
Bolt 2, Bolt 4, Ice 3, Flare



Segomod
Female
Sagittarius
75
55
Dancer
White Magic
Speed Save
Secret Hunt
Swim

Persia

Holy Miter
Chameleon Robe
N-Kai Armlet

Wiznaibus, Void Storage
Cure, Cure 2, Raise, Raise 2, Reraise, Protect 2, Esuna, Holy



Meyples
Male
Virgo
43
46
Priest
Black Magic
Counter
Doublehand
Move-HP Up

Healing Staff

Triangle Hat
Wizard Robe
Diamond Armlet

Cure, Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect, Esuna
Fire 3, Fire 4, Bolt 3, Ice 2, Ice 4, Flare

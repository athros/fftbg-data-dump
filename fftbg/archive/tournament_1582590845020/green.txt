Player: !Green
Team: Green Team
Palettes: Green/White



Draaaan
Male
Sagittarius
75
65
Oracle
Throw
Distribute
Halve MP
Ignore Height

Madlemgen

Triangle Hat
Earth Clothes
Cherche

Poison, Dispel Magic, Paralyze
Shuriken, Bomb, Knife, Staff



Onetrickwolf
Male
Libra
68
80
Thief
Sing
Damage Split
Martial Arts
Ignore Height



Golden Hairpin
Chain Vest
Red Shoes

Steal Heart, Steal Accessory, Leg Aim
Angel Song, Battle Song, Magic Song, Sky Demon



G Regulate
Monster
Taurus
59
50
Gobbledeguck










Omegasuspekt
Male
Libra
59
50
Priest
Punch Art
MA Save
Equip Gun
Move+1

Papyrus Codex

Feather Hat
Leather Outfit
N-Kai Armlet

Reraise, Protect, Shell 2, Esuna
Wave Fist, Earth Slash, Purification, Chakra

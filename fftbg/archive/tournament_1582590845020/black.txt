Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Virgo
44
56
Mime

Counter
Attack UP
Retreat


Round Shield
Golden Hairpin
Mystic Vest
Elf Mantle

Mimic




Zerguzen
Female
Capricorn
63
64
Wizard
Punch Art
Sunken State
Doublehand
Swim

Orichalcum

Holy Miter
Chameleon Robe
Red Shoes

Fire, Bolt, Ice 3, Empower
Purification



Swordoftheseven
Male
Taurus
58
41
Geomancer
Item
Faith Up
Sicken
Levitate

Giant Axe
Diamond Shield
Triangle Hat
Mythril Vest
Defense Ring

Pitfall, Water Ball, Hell Ivy, Gusty Wind
Potion, Phoenix Down



ApplesauceBoss
Male
Sagittarius
48
81
Squire
Steal
Hamedo
Beastmaster
Retreat

Giant Axe
Gold Shield
Crystal Helmet
Brigandine
N-Kai Armlet

Dash, Throw Stone, Heal, Cheer Up, Wish, Scream
Gil Taking

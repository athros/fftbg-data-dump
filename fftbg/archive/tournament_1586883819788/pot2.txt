Final Bets: green - 12 bets for 4,632G (26.6%, x2.77); yellow - 18 bets for 12,808G (73.4%, x0.36)

green bets:
sinnyil2: 800G (17.3%, 36,219G)
AllInBot: 651G (14.1%, 651G)
roofiepops: 643G (13.9%, 643G)
Lanshaft: 600G (13.0%, 6,599G)
Baron_von_Scrub: 505G (10.9%, 5,811G)
joewcarson: 300G (6.5%, 11,032G)
DLJuggernaut: 300G (6.5%, 9,106G)
killth3kid: 244G (5.3%, 1,586G)
jame3200: 239G (5.2%, 939G)
getthemoneyz: 150G (3.2%, 495,137G)
Lodrak: 100G (2.2%, 6,057G)
metagameface: 100G (2.2%, 100G)

yellow bets:
Jeeboheebo: 3,947G (30.8%, 3,947G)
Shalloween: 1,400G (10.9%, 35,578G)
JonnyCue: 1,243G (9.7%, 1,243G)
TheChainNerd: 1,000G (7.8%, 9,631G)
BirbBrainsBot: 1,000G (7.8%, 134,763G)
datadrivenbot: 821G (6.4%, 1,820G)
Nizaha: 652G (5.1%, 652G)
JumbocactuarX27: 521G (4.1%, 1,043G)
catfashions: 500G (3.9%, 3,445G)
OmnibotGamma: 384G (3.0%, 384G)
ZephyrTempest: 356G (2.8%, 56,986G)
TheManInPlaid: 222G (1.7%, 4,423G)
ungabunga_bot: 211G (1.6%, 132,910G)
WoooBlaaa: 132G (1.0%, 132G)
wollise89: 120G (0.9%, 2,074G)
ApplesauceBoss: 100G (0.8%, 7,838G)
HeroponThrawn: 100G (0.8%, 6,230G)
daveb_: 99G (0.8%, 4,483G)

Final Bets: red - 7 bets for 4,721G (29.2%, x2.42); champion - 12 bets for 11,434G (70.8%, x0.41)

red bets:
roqqqpsi: 1,509G (32.0%, 6,861G)
XalSmellsBad: 1,000G (21.2%, 29,236G)
BirbBrainsBot: 1,000G (21.2%, 34,359G)
ColetteMSLP: 500G (10.6%, 24,236G)
getthemoneyz: 312G (6.6%, 585,159G)
ewan_e: 300G (6.4%, 10,286G)
HorusTaurus: 100G (2.1%, 273G)

champion bets:
Lydian_C: 5,600G (49.0%, 46,532G)
Sovattaro: 1,000G (8.7%, 2,557G)
Mtueni: 1,000G (8.7%, 295,276G)
aStatue: 895G (7.8%, 895G)
potgodtopdog: 666G (5.8%, 13,613G)
ungabunga_bot: 656G (5.7%, 275,746G)
Jeeboheebo: 549G (4.8%, 549G)
Cataphract116: 399G (3.5%, 24,553G)
CassiePhoenix: 288G (2.5%, 288G)
nuka1120: 177G (1.5%, 177G)
Reductions: 104G (0.9%, 104G)
datadrivenbot: 100G (0.9%, 2,790G)

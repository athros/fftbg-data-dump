Player: !Black
Team: Black Team
Palettes: Black/Red



Jaritras
Female
Sagittarius
39
74
Archer
Black Magic
Auto Potion
Martial Arts
Jump+2

Bow Gun
Flame Shield
Thief Hat
Power Sleeve
Rubber Shoes

Charge+1, Charge+2, Charge+4
Fire 3, Bolt, Ice 4, Empower



Reductions
Monster
Aquarius
44
79
Reaper










XalSmellsBad
Female
Libra
56
65
Mediator
Battle Skill
Mana Shield
Dual Wield
Move+3

Main Gauche
Blind Knife
Red Hood
Linen Robe
Cursed Ring

Invitation, Persuade, Praise, Threaten, Preach, Negotiate
Shield Break, Stasis Sword



Dexef
Male
Virgo
39
41
Thief
Elemental
Counter
Equip Polearm
Levitate

Partisan

Green Beret
Chain Vest
Red Shoes

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard

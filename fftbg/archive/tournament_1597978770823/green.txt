Player: !Green
Team: Green Team
Palettes: Green/White



Amorial
Male
Aries
52
40
Mediator
Time Magic
Parry
Concentrate
Lava Walking

Mythril Gun

Leather Hat
Black Costume
Wizard Mantle

Invitation, Persuade, Praise, Threaten, Death Sentence, Negotiate, Refute
Slow, Slow 2, Demi, Meteor



Silentkaster
Female
Sagittarius
45
80
Geomancer
Black Magic
Dragon Spirit
Defend
Move-HP Up

Kiyomori
Ice Shield
Cachusha
Black Robe
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Fire 2, Fire 3, Bolt, Bolt 3, Ice 2, Empower



HaateXIII
Male
Taurus
66
55
Thief
Throw
Distribute
Maintenance
Lava Walking

Mythril Sword

Leather Hat
Brigandine
Defense Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield
Shuriken, Bomb, Ninja Sword



DAC169
Female
Pisces
50
72
Chemist
Charge
Caution
Martial Arts
Levitate

Romanda Gun

Headgear
Mythril Vest
Reflect Ring

Potion, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Charge+4

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Galkife
Male
Sagittarius
59
74
Geomancer
Sing
Counter Tackle
Equip Armor
Retreat

Bizen Boat
Gold Shield
Diamond Helmet
Crystal Mail
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Angel Song, Battle Song, Last Song, Diamond Blade, Hydra Pit



ANFz
Male
Taurus
56
50
Lancer
Black Magic
Catch
Defense UP
Retreat

Gungnir
Crystal Shield
Cross Helmet
Genji Armor
Feather Boots

Level Jump8, Vertical Jump7
Fire 3, Bolt 2, Ice 3, Flare



Dogsandcatsand
Female
Aquarius
48
70
Wizard
Steal
Counter
Attack UP
Move+1

Thunder Rod

Feather Hat
Brigandine
Genji Gauntlet

Fire 2, Fire 3, Bolt 2, Death, Flare
Steal Weapon, Steal Accessory, Arm Aim



StealthModeLocke
Female
Aries
64
52
Geomancer
Throw
Speed Save
Attack UP
Move-MP Up

Bizen Boat
Mythril Shield
Green Beret
Adaman Vest
Dracula Mantle

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Axe

Player: !Red
Team: Red Team
Palettes: Red/Brown



I Nod My Head When I Lose
Monster
Aquarius
75
75
Apanda










Lanshaft
Female
Cancer
69
66
Monk
Dance
Counter Tackle
Magic Attack UP
Jump+2



Thief Hat
Wizard Outfit
Reflect Ring

Spin Fist, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Witch Hunt, Wiznaibus, Nameless Dance, Last Dance, Dragon Pit



Moonliquor
Female
Aries
48
43
Mime

MA Save
Long Status
Move-MP Up



Twist Headband
Mythril Vest
Feather Mantle

Mimic




Technominari
Female
Taurus
58
48
Priest
Basic Skill
Earplug
Beastmaster
Jump+2

Flame Whip

Triangle Hat
Light Robe
Cursed Ring

Cure, Cure 2, Cure 4, Raise, Regen, Wall, Esuna
Dash, Heal, Cheer Up

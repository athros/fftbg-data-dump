Player: !Green
Team: Green Team
Palettes: Green/White



Lydian C
Monster
Sagittarius
48
77
Bomb










HASTERIOUS
Male
Sagittarius
71
50
Chemist
Yin Yang Magic
Mana Shield
Doublehand
Lava Walking

Main Gauche

Triangle Hat
Wizard Outfit
Power Wrist

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water
Poison, Pray Faith, Silence Song, Dispel Magic



Vorap
Male
Aries
48
58
Monk
Yin Yang Magic
PA Save
Short Status
Jump+2



Flash Hat
Chain Vest
108 Gems

Spin Fist, Secret Fist, Purification
Blind, Pray Faith, Silence Song, Foxbird



Gogofromtogo
Female
Scorpio
57
62
Thief
Item
Damage Split
Equip Sword
Waterbreathing

Defender

Red Hood
Secret Clothes
Germinas Boots

Steal Helmet, Steal Armor
Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Holy Water

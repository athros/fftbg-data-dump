Final Bets: purple - 8 bets for 7,955G (58.9%, x0.70); brown - 15 bets for 5,540G (41.1%, x1.44)

purple bets:
roqqqpsi: 2,058G (25.9%, 18,717G)
twelfthrootoftwo: 2,000G (25.1%, 34,924G)
volgrathemoose: 1,282G (16.1%, 1,282G)
ShintaroNayaka: 646G (8.1%, 646G)
MrFlabyo: 500G (6.3%, 52,148G)
d4rr1n: 500G (6.3%, 5,673G)
Nelrith: 500G (6.3%, 6,704G)
Digitalsocrates: 469G (5.9%, 4,691G)

brown bets:
BirbBrainsBot: 1,000G (18.1%, 94,892G)
Mesmaster: 1,000G (18.1%, 149,168G)
E_Ballard: 959G (17.3%, 959G)
NovaKnight21: 624G (11.3%, 624G)
LivingHitokiri: 392G (7.1%, 392G)
gogofromtogo: 248G (4.5%, 248G)
getthemoneyz: 208G (3.8%, 764,707G)
Mr_Greed029: 200G (3.6%, 1,563G)
wyonearth: 200G (3.6%, 1,576G)
thaetreis: 188G (3.4%, 188G)
Lydian_C: 120G (2.2%, 7,349G)
gorgewall: 101G (1.8%, 22,689G)
Qaz12301: 100G (1.8%, 3,434G)
datadrivenbot: 100G (1.8%, 30,727G)
Furgers: 100G (1.8%, 1,229G)

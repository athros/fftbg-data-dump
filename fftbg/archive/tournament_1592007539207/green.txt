Player: !Green
Team: Green Team
Palettes: Green/White



Reddwind
Male
Capricorn
69
53
Knight
Item
Counter
Dual Wield
Jump+2

Diamond Sword
Diamond Sword
Mythril Helmet
Reflect Mail
Angel Ring

Head Break, Power Break, Dark Sword
Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Lawnboxer
Male
Taurus
56
61
Samurai
Throw
Counter Flood
Short Status
Jump+3

Bizen Boat

Barbuta
Reflect Mail
Leather Mantle

Asura, Koutetsu, Murasame, Muramasa
Shuriken, Bomb



Kaelsun
Male
Gemini
40
64
Monk
Battle Skill
Caution
Halve MP
Lava Walking



Triangle Hat
Chain Vest
Germinas Boots

Spin Fist, Wave Fist, Chakra, Revive
Shield Break, Magic Break, Speed Break, Mind Break, Dark Sword



Tougou
Male
Capricorn
43
46
Archer
Sing
Brave Save
Dual Wield
Jump+1

Cross Bow
Hunting Bow
Triangle Hat
Mystic Vest
Small Mantle

Charge+1, Charge+7
Life Song, Space Storage

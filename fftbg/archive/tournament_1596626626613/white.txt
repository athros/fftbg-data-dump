Player: !White
Team: White Team
Palettes: White/Blue



DouglasDragonThePoet
Female
Scorpio
69
41
Mime

HP Restore
Sicken
Jump+3



Leather Hat
Linen Robe
Rubber Shoes

Mimic




NovaKnight21
Male
Pisces
49
67
Lancer
Black Magic
Arrow Guard
Long Status
Ignore Terrain

Mythril Spear
Kaiser Plate
Gold Helmet
Bronze Armor
Cursed Ring

Level Jump4, Vertical Jump8
Fire 2, Bolt, Bolt 3, Bolt 4, Ice, Frog, Death, Flare



ZCKaiser
Female
Aries
68
54
Knight
Throw
Auto Potion
Dual Wield
Ignore Terrain

Defender
Diamond Sword
Crystal Helmet
Mythril Armor
Leather Mantle

Head Break, Armor Break, Shield Break, Magic Break, Stasis Sword, Justice Sword
Shuriken, Knife, Spear



Just Here2
Female
Virgo
47
41
Lancer
Elemental
Damage Split
Secret Hunt
Retreat

Spear
Hero Shield
Diamond Helmet
Carabini Mail
Elf Mantle

Level Jump3, Vertical Jump8
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

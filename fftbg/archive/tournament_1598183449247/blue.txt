Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



17RJ
Male
Taurus
65
76
Bard
Black Magic
Earplug
Magic Defense UP
Lava Walking

Lightning Bow

Barette
Black Costume
Reflect Ring

Angel Song, Battle Song, Magic Song, Diamond Blade, Hydra Pit
Fire 4, Bolt 3, Bolt 4, Ice, Ice 3, Frog



NewbGaming
Female
Leo
51
80
Ninja
Steal
Counter Magic
Martial Arts
Fly

Flail
Morning Star
Holy Miter
Adaman Vest
Red Shoes

Dictionary
Steal Helmet, Steal Accessory, Arm Aim



Tecc2
Male
Taurus
75
62
Chemist
Charge
MP Restore
Equip Axe
Lava Walking

Slasher

Golden Hairpin
Judo Outfit
Rubber Shoes

Potion, Ether, Hi-Ether, Echo Grass, Holy Water, Phoenix Down
Charge+1, Charge+3



ALY327
Male
Sagittarius
68
62
Lancer
Basic Skill
Auto Potion
Dual Wield
Ignore Height

Obelisk
Musk Rod
Genji Helmet
Bronze Armor
N-Kai Armlet

Level Jump5, Vertical Jump7
Accumulate, Heal, Tickle, Cheer Up, Fury, Scream

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaateXIII
Monster
Virgo
44
53
Sekhret










Superdevon1
Male
Libra
49
61
Thief
Sing
Meatbone Slash
Sicken
Waterbreathing

Broad Sword

Green Beret
Brigandine
N-Kai Armlet

Gil Taking, Steal Heart, Steal Accessory, Arm Aim
Angel Song, Battle Song, Last Song, Space Storage



Legionx13
Male
Aries
60
54
Monk
Basic Skill
Meatbone Slash
Equip Sword
Waterwalking

Iron Sword

Headgear
Chain Vest
Battle Boots

Pummel, Purification, Chakra, Revive
Accumulate, Throw Stone, Scream



StealthModeLocke
Female
Capricorn
56
50
Chemist
Steal
Abandon
Secret Hunt
Ignore Terrain

Main Gauche

Headgear
Adaman Vest
Small Mantle

Potion, Hi-Potion, Hi-Ether, Remedy, Phoenix Down
Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim

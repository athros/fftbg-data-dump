Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Rockmem21
Monster
Gemini
80
63
Reaper










Ricky Ortease
Male
Aquarius
66
76
Wizard
Punch Art
Parry
Equip Sword
Teleport

Asura Knife

Leather Hat
Wizard Robe
Wizard Mantle

Fire 3, Bolt 3, Bolt 4, Empower
Purification, Chakra, Revive



Lali Lulelo
Monster
Scorpio
37
53
Vampire










Hales Bopp It
Male
Libra
62
53
Wizard
Elemental
Counter
Long Status
Move+2

Mythril Knife

Headgear
Wizard Robe
Defense Ring

Fire 2, Bolt
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball

Final Bets: green - 12 bets for 7,875G (26.9%, x2.72); yellow - 23 bets for 21,382G (73.1%, x0.37)

green bets:
mirapoix: 3,358G (42.6%, 3,358G)
ko2q: 1,709G (21.7%, 3,418G)
Aldrammech: 1,000G (12.7%, 29,124G)
superdevon1: 500G (6.3%, 3,681G)
MeleeWizard: 264G (3.4%, 264G)
Estan_AD: 228G (2.9%, 228G)
Billybones5150: 200G (2.5%, 14,181G)
Powermhero: 200G (2.5%, 10,700G)
CandyTamer: 116G (1.5%, 116G)
MirtaiAtana: 100G (1.3%, 1,277G)
SkylerBunny: 100G (1.3%, 63,532G)
ANFz: 100G (1.3%, 14,334G)

yellow bets:
HaateXIII: 5,135G (24.0%, 5,135G)
sinnyil2: 2,500G (11.7%, 92,762G)
R_Raynos: 2,076G (9.7%, 2,076G)
HaplessOne: 1,500G (7.0%, 119,723G)
aStatue: 1,000G (4.7%, 1,000G)
ungabunga_bot: 1,000G (4.7%, 208,227G)
BirbBrainsBot: 1,000G (4.7%, 57,844G)
Pie108: 1,000G (4.7%, 56,883G)
AllInBot: 721G (3.4%, 721G)
leakimiko: 610G (2.9%, 12,219G)
reinoe: 604G (2.8%, 604G)
getthemoneyz: 554G (2.6%, 526,125G)
roofiepops: 550G (2.6%, 6,251G)
alrightbye: 550G (2.6%, 27,535G)
pandasforsale: 516G (2.4%, 516G)
flowinprose: 500G (2.3%, 100,439G)
OmnibotGamma: 440G (2.1%, 440G)
electric_glass: 333G (1.6%, 4,393G)
killth3kid: 288G (1.3%, 4,324G)
Mikumari12: 204G (1.0%, 204G)
ZephyrTempest: 101G (0.5%, 166,343G)
rjA0zcOQ96: 100G (0.5%, 33,138G)
Evewho: 100G (0.5%, 2,145G)

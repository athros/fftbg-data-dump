Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



VolgraTheMoose
Male
Pisces
45
47
Lancer
Elemental
Distribute
Attack UP
Jump+2

Javelin
Kaiser Plate
Gold Helmet
Silk Robe
Power Wrist

Level Jump8, Vertical Jump7
Hell Ivy, Static Shock, Quicksand, Blizzard, Lava Ball



CapnChaos12
Female
Virgo
56
42
Geomancer
Draw Out
Abandon
Equip Sword
Retreat

Murasame
Platinum Shield
Holy Miter
White Robe
Angel Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



Thyrandaal
Male
Aries
46
56
Archer
Elemental
Dragon Spirit
Dual Wield
Jump+2

Perseus Bow

Barbuta
Clothes
Small Mantle

Charge+2, Charge+3, Charge+7, Charge+20
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Dogsandcatsand
Female
Leo
46
81
Wizard
Battle Skill
Abandon
Short Charge
Retreat

Dragon Rod

Thief Hat
Chameleon Robe
Genji Gauntlet

Fire, Bolt 2, Bolt 3, Ice 3, Ice 4, Frog, Death
Head Break, Shield Break, Magic Break, Power Break, Justice Sword

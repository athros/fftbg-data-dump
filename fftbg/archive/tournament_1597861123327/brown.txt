Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NewbGaming
Female
Gemini
79
42
Monk
Throw
Parry
Dual Wield
Fly



Headgear
Power Sleeve
Battle Boots

Secret Fist, Purification, Chakra
Shuriken, Axe, Spear



Itsonlyspencer
Male
Capricorn
60
72
Lancer
White Magic
Counter
Doublehand
Move+1

Whale Whisker

Diamond Helmet
Mythril Armor
Power Wrist

Level Jump4, Vertical Jump2
Cure, Cure 2, Raise, Raise 2, Reraise, Protect, Shell, Wall, Esuna



Chuckolator
Female
Taurus
58
79
Geomancer
Charge
Speed Save
Equip Bow
Waterwalking

Hunting Bow
Gold Shield
Green Beret
Power Sleeve
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+4



Gelwain
Female
Pisces
56
61
Priest
Draw Out
Speed Save
Secret Hunt
Levitate

Healing Staff

Twist Headband
Linen Robe
Sprint Shoes

Cure, Cure 2, Raise, Reraise, Protect
Asura, Kiyomori, Kikuichimoji

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Phi Sig
Monster
Gemini
54
43
Red Chocobo










Benticore
Male
Virgo
56
57
Oracle
Basic Skill
Faith Save
Attack UP
Waterwalking

Octagon Rod

Flash Hat
Silk Robe
Sprint Shoes

Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Dispel Magic, Paralyze
Cheer Up



Kronikle
Monster
Libra
45
47
Red Chocobo










Sir Uther115
Male
Sagittarius
44
70
Knight
Basic Skill
Brave Save
Secret Hunt
Waterbreathing

Mythril Sword
Genji Shield
Iron Helmet
Wizard Robe
Small Mantle

Head Break, Armor Break, Weapon Break, Magic Break, Mind Break, Justice Sword
Accumulate, Dash, Heal, Cheer Up, Wish

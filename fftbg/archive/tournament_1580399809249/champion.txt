Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black




Kalthonia
Male
Capricorn
75
66
Priest
Battle Skill
Mana Shield
Secret Hunt
Ignore Height

Scorpion Tail

Green Beret
Judo Outfit
Vanish Mantle

Raise, Protect, Wall, Esuna, Holy
Head Break, Weapon Break, Stasis Sword, Justice Sword



Brondius
Male
Gemini
58
45
Monk
Time Magic
Parry
Attack UP
Waterwalking



Flash Hat
Clothes
Feather Mantle

Earth Slash, Purification
Haste, Haste 2, Slow 2, Float, Reflect, Demi, Stabilize Time, Meteor



SALTY  RAGE
Male
Cancer
61
75
Lancer
Summon Magic
MP Restore
Dual Wield
Ignore Height

Holy Lance
Mythril Spear
Grand Helmet
Black Robe
Setiemson

Level Jump4, Vertical Jump6
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Silf



Imranimran
Female
Sagittarius
59
65
Monk
Item
Abandon
Equip Shield
Move+3


Escutcheon
Holy Miter
Brigandine
Sprint Shoes

Pummel, Wave Fist, Secret Fist, Purification, Revive
X-Potion, Maiden's Kiss, Phoenix Down

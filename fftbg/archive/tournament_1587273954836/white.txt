Player: !White
Team: White Team
Palettes: White/Blue



ArchKnightX
Female
Cancer
79
56
Archer
Dance
PA Save
Equip Polearm
Ignore Terrain

Javelin
Flame Shield
Barbuta
Judo Outfit
N-Kai Armlet

Charge+2, Charge+10
Witch Hunt, Disillusion, Obsidian Blade



CT 5 Holy
Male
Pisces
75
52
Calculator
White Magic
Meatbone Slash
Magic Attack UP
Ignore Terrain

Battle Folio

Holy Miter
Light Robe
Wizard Mantle

CT, Prime Number, 3
Cure 3, Raise, Raise 2, Regen, Protect 2, Shell 2



Jjones032109
Female
Capricorn
66
80
Lancer
Steal
Brave Up
Attack UP
Move+2

Spear
Escutcheon
Platinum Helmet
Platinum Armor
Defense Armlet

Level Jump8, Vertical Jump8
Steal Helmet



GrandmasterFrankerZ
Female
Aquarius
75
49
Time Mage
Basic Skill
Caution
Martial Arts
Move-MP Up



Red Hood
Linen Robe
Germinas Boots

Haste, Slow 2, Stabilize Time
Heal, Yell

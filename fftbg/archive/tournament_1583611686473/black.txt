Player: !Black
Team: Black Team
Palettes: Black/Red



Lifeguard Dan
Female
Gemini
50
44
Knight
Time Magic
Regenerator
Equip Bow
Move+1

Ice Bow

Circlet
Black Robe
Power Wrist

Magic Break, Dark Sword, Night Sword
Haste 2, Stop, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time



Awakeninfinity
Male
Aquarius
67
58
Bard
Battle Skill
Regenerator
Equip Axe
Jump+3

Flame Whip

Leather Hat
Carabini Mail
Germinas Boots

Angel Song, Life Song, Diamond Blade
Head Break, Weapon Break, Speed Break



CorpusCav
Female
Scorpio
59
61
Mime

Auto Potion
Doublehand
Teleport



Leather Hat
Black Costume
Genji Gauntlet

Mimic




Archelous
Male
Aries
49
68
Squire
Yin Yang Magic
MA Save
Equip Polearm
Lava Walking

Persia
Escutcheon
Barbuta
Secret Clothes
Feather Boots

Accumulate, Heal, Tickle, Yell
Blind, Life Drain, Foxbird, Dispel Magic, Sleep

Player: !Green
Team: Green Team
Palettes: Green/White



Tapemeasure
Male
Aquarius
50
80
Ninja
Basic Skill
Sunken State
Magic Defense UP
Move+2

Flame Whip
Sasuke Knife
Leather Hat
Adaman Vest
Magic Gauntlet

Shuriken, Bomb
Accumulate, Throw Stone, Yell, Scream



ForagerCats
Male
Virgo
72
67
Squire
Punch Art
Caution
Equip Shield
Waterbreathing

Poison Bow
Crystal Shield
Green Beret
Mystic Vest
Genji Gauntlet

Accumulate, Dash, Tickle, Cheer Up, Ultima
Earth Slash, Purification, Chakra, Revive



NovaKnight21
Female
Sagittarius
57
69
Priest
Battle Skill
Dragon Spirit
Equip Knife
Waterwalking

Poison Rod

Flash Hat
Judo Outfit
Defense Ring

Cure, Raise, Esuna
Speed Break, Stasis Sword, Justice Sword



Oogthecaveman
Male
Sagittarius
67
72
Ninja
Jump
Hamedo
Equip Polearm
Ignore Terrain

Mythril Spear
Persia
Holy Miter
Adaman Vest
Magic Gauntlet

Shuriken, Axe
Level Jump8, Vertical Jump5

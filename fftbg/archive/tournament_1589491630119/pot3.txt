Final Bets: white - 10 bets for 4,324G (16.3%, x5.15); black - 32 bets for 22,261G (83.7%, x0.19)

white bets:
Grimmace45: 1,207G (27.9%, 24,149G)
Lionhermit: 1,000G (23.1%, 54,856G)
DeathTaxesAndAnime: 400G (9.3%, 3,151G)
NA_Bloodline: 400G (9.3%, 1,185G)
thethorndog1: 358G (8.3%, 358G)
nifboy: 250G (5.8%, 3,805G)
Lythe_Caraker: 250G (5.8%, 124,304G)
getthemoneyz: 206G (4.8%, 676,632G)
ungabunga_bot: 153G (3.5%, 500,475G)
Evewho: 100G (2.3%, 20,084G)

black bets:
reinoe: 3,000G (13.5%, 16,005G)
toka222: 2,600G (11.7%, 48,213G)
Mesmaster: 2,000G (9.0%, 35,360G)
holyonline: 1,502G (6.7%, 1,502G)
Estan_AD: 1,251G (5.6%, 2,503G)
Nizaha: 1,001G (4.5%, 11,213G)
MrFlabyo: 1,000G (4.5%, 58,721G)
NicoSavoy: 1,000G (4.5%, 23,376G)
volgrathemoose: 973G (4.4%, 1,947G)
Laserman1000: 868G (3.9%, 33,268G)
TheUnforgivenRage: 742G (3.3%, 742G)
HaateXIII: 683G (3.1%, 683G)
DustBirdEX: 508G (2.3%, 508G)
CosmicTactician: 500G (2.2%, 10,539G)
AUrato: 500G (2.2%, 4,788G)
EnemyController: 500G (2.2%, 233,130G)
Oobs56: 500G (2.2%, 5,394G)
ko2q: 428G (1.9%, 428G)
Cryptopsy70: 428G (1.9%, 428G)
KasugaiRoastedPeas: 340G (1.5%, 340G)
FriendSquirrel: 316G (1.4%, 316G)
happyfundude: 252G (1.1%, 252G)
alecttox: 236G (1.1%, 236G)
TheFALLofLindsay: 228G (1.0%, 228G)
CorpusCav: 200G (0.9%, 9,761G)
BirbBrainsBot: 154G (0.7%, 64,891G)
MalakiGenesys: 146G (0.7%, 146G)
mannequ1n: 100G (0.4%, 5,794G)
Firesheath: 100G (0.4%, 28,082G)
datadrivenbot: 100G (0.4%, 17,325G)
Antipathics: 100G (0.4%, 7,518G)
moonliquor: 5G (0.0%, 815G)

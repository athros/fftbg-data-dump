Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ColetteMSLP
Male
Aries
80
81
Archer
White Magic
HP Restore
Defend
Fly

Poison Bow
Gold Shield
Golden Hairpin
Clothes
Diamond Armlet

Charge+3, Charge+4, Charge+5, Charge+10
Cure 2, Raise, Esuna, Holy



Run With Stone GUNs
Female
Leo
78
81
Calculator
Black Magic
Meatbone Slash
Halve MP
Lava Walking

Battle Bamboo

Thief Hat
Chain Vest
Feather Mantle

Height, 5, 3
Fire 3, Bolt



Killth3kid
Female
Aquarius
46
78
Summoner
Steal
Meatbone Slash
Equip Axe
Move-HP Up

Healing Staff

Feather Hat
Clothes
Battle Boots

Ifrit, Titan, Carbunkle, Leviathan, Fairy, Lich
Steal Heart, Steal Helmet, Steal Armor, Leg Aim



Solomongrundy85
Female
Gemini
63
75
Archer
Dance
PA Save
Halve MP
Move-MP Up

Lightning Bow

Flash Hat
Earth Clothes
Red Shoes

Charge+2, Charge+5, Charge+7
Witch Hunt, Wiznaibus, Polka Polka, Dragon Pit

Player: !Green
Team: Green Team
Palettes: Green/White



Bahumat989
Female
Capricorn
73
46
Geomancer
Charge
Counter Magic
Martial Arts
Ignore Height


Crystal Shield
Triangle Hat
Black Robe
Dracula Mantle

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind
Charge+1, Charge+2, Charge+7



Dynasti
Male
Virgo
49
75
Thief
Item
Mana Shield
Throw Item
Swim

Main Gauche

Triangle Hat
Earth Clothes
Diamond Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Potion, Remedy, Phoenix Down



BillyTheCop
Male
Leo
69
40
Ninja
Sing
Arrow Guard
Equip Sword
Move+2

Platinum Sword
Ancient Sword
Leather Hat
Power Sleeve
Power Wrist

Shuriken, Staff
Life Song, Battle Song, Nameless Song



Markese
Female
Sagittarius
60
42
Geomancer
Draw Out
Faith Up
Equip Axe
Fly

Flail
Round Shield
Twist Headband
Chameleon Robe
Magic Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Murasame, Muramasa

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ass Brains
Male
Scorpio
78
43
Ninja
Steal
Hamedo
Equip Shield
Teleport

Air Knife
Genji Shield
Black Hood
Black Costume
Dracula Mantle

Shuriken, Bomb, Knife, Dictionary
Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Pandasforsale
Female
Cancer
41
42
Monk
Draw Out
Damage Split
Equip Axe
Waterwalking

Wizard Staff

Flash Hat
Mythril Vest
108 Gems

Pummel, Purification, Revive
Koutetsu, Kiyomori



IndecisiveNinja
Female
Scorpio
66
76
Wizard
Throw
Faith Up
Concentrate
Move-HP Up

Dragon Rod

Red Hood
Mythril Vest
Diamond Armlet

Fire, Fire 3, Bolt, Death
Shuriken, Spear



Oogthecaveman
Male
Aries
45
40
Archer
Sing
Counter
Magic Defense UP
Levitate

Cross Bow
Gold Shield
Thief Hat
Mythril Vest
Leather Mantle

Charge+2, Charge+5, Charge+7, Charge+10, Charge+20
Life Song, Last Song

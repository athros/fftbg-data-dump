Player: !Red
Team: Red Team
Palettes: Red/Brown



ApplesauceBoss
Female
Gemini
69
47
Wizard
Punch Art
Faith Save
Defense UP
Move-MP Up

Dragon Rod

Twist Headband
Earth Clothes
Magic Gauntlet

Fire, Fire 2, Bolt 3, Ice 2, Ice 3
Spin Fist, Wave Fist, Purification, Revive, Seal Evil



Lyner87
Female
Gemini
42
48
Lancer
Punch Art
Critical Quick
Magic Defense UP
Move+2

Spear
Ice Shield
Barbuta
Light Robe
Reflect Ring

Level Jump5, Vertical Jump8
Spin Fist, Pummel, Revive



E Ballard
Male
Sagittarius
79
45
Ninja
Elemental
Counter Tackle
Equip Bow
Retreat

Mythril Bow

Flash Hat
Black Costume
Dracula Mantle

Shuriken, Knife, Ninja Sword, Axe
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind



Flameatron
Female
Sagittarius
49
43
Thief
Battle Skill
Sunken State
Secret Hunt
Move-HP Up

Assassin Dagger

Feather Hat
Clothes
N-Kai Armlet

Steal Shield
Armor Break, Weapon Break, Magic Break, Mind Break, Justice Sword

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Female
Cancer
46
57
Mediator
Time Magic
Speed Save
Doublehand
Teleport

Papyrus Codex

Leather Hat
Linen Robe
Small Mantle

Invitation, Preach, Solution, Insult, Negotiate
Haste, Haste 2, Slow, Slow 2, Float, Quick



Sairentozon7
Female
Aries
49
61
Ninja
Jump
Caution
Short Charge
Jump+1

Blind Knife
Assassin Dagger
Headgear
Chain Vest
108 Gems

Shuriken
Level Jump2, Vertical Jump2



Nizaha
Male
Libra
70
74
Lancer
Time Magic
Abandon
Defense UP
Waterbreathing

Javelin
Mythril Shield
Barbuta
Reflect Mail
Vanish Mantle

Level Jump8, Vertical Jump7
Haste 2, Float, Demi, Stabilize Time



Fenaen
Male
Virgo
62
80
Lancer
Yin Yang Magic
MP Restore
Equip Bow
Move+1

Windslash Bow

Cross Helmet
Black Robe
Leather Mantle

Level Jump4, Vertical Jump8
Life Drain, Doubt Faith, Silence Song, Blind Rage

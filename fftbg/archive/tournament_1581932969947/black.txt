Player: !Black
Team: Black Team
Palettes: Black/Red



Leakimiko
Female
Gemini
48
73
Geomancer
Talk Skill
Mana Shield
Halve MP
Ignore Terrain

Muramasa
Escutcheon
Twist Headband
Chameleon Robe
Jade Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard
Persuade, Praise, Death Sentence, Refute



Nachtkrieger
Male
Pisces
61
52
Ninja
White Magic
Counter
Defend
Move-HP Up

Iga Knife
Short Edge
Barette
Black Costume
Magic Ring

Shuriken, Knife, Staff
Raise, Regen, Protect, Esuna



Alaylle
Female
Leo
78
77
Summoner
Elemental
MP Restore
Equip Sword
Waterbreathing

Sleep Sword

Feather Hat
Silk Robe
Elf Mantle

Moogle, Shiva, Ramuh, Ifrit, Golem
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



NeoLuxor
Male
Scorpio
69
63
Ninja
Elemental
Counter Magic
Long Status
Jump+3

Sasuke Knife
Hidden Knife
Holy Miter
Clothes
Power Wrist

Shuriken, Knife
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Lava Ball

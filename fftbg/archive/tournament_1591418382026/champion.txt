Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



UmaiJam
Female
Serpentarius
69
55
Geomancer
Draw Out
Counter Flood
Equip Polearm
Waterwalking

Whale Whisker
Bronze Shield
Holy Miter
Chameleon Robe
Germinas Boots

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Koutetsu, Murasame, Heaven's Cloud, Kiyomori



Gogofromtogo
Female
Scorpio
79
72
Dancer
Draw Out
Caution
Secret Hunt
Move+1

Cute Bag

Black Hood
Wizard Outfit
Setiemson

Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Dragon Pit
Asura, Koutetsu, Murasame, Kikuichimoji



DAC169
Female
Libra
48
56
Mediator
Yin Yang Magic
Auto Potion
Equip Armor
Ignore Terrain

Papyrus Codex

Platinum Helmet
Reflect Mail
Germinas Boots

Persuade, Negotiate, Refute
Blind, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy



Fenaen
Male
Aries
50
67
Time Mage
White Magic
Hamedo
Equip Sword
Fly

Defender

Holy Miter
Black Robe
Feather Mantle

Haste 2, Slow, Stop, Immobilize, Float, Stabilize Time
Cure 4, Raise, Esuna

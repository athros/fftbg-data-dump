Player: !Black
Team: Black Team
Palettes: Black/Red



KasugaiRoastedPeas
Monster
Taurus
69
77
Steel Giant










Rethera
Male
Libra
46
50
Geomancer
Sing
Counter
Halve MP
Jump+1

Giant Axe
Crystal Shield
Golden Hairpin
Wizard Robe
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Life Song, Cheer Song, Last Song, Diamond Blade, Hydra Pit



DarrenDinosaurs
Female
Scorpio
48
64
Knight
Steal
Parry
Defend
Teleport 2

Ragnarok

Bronze Helmet
Chain Mail
Battle Boots

Head Break, Armor Break, Shield Break, Stasis Sword, Justice Sword, Dark Sword, Surging Sword
Steal Heart, Steal Helmet, Steal Shield, Leg Aim



HASTERIOUS
Male
Gemini
70
78
Geomancer
Yin Yang Magic
Blade Grasp
Magic Defense UP
Fly

Battle Axe
Diamond Shield
Golden Hairpin
Linen Robe
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Blind, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic

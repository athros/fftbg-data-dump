Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



B3lial
Monster
Virgo
58
76
Pisco Demon










AzuriteReaction
Male
Scorpio
77
53
Geomancer
Battle Skill
Damage Split
Doublehand
Jump+1

Koutetsu Knife

Triangle Hat
Silk Robe
Power Wrist

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball
Weapon Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Night Sword



Redcrystal27
Male
Scorpio
66
80
Summoner
Talk Skill
Parry
Monster Talk
Jump+1

Flame Rod

Headgear
Earth Clothes
Jade Armlet

Moogle, Shiva, Ramuh, Carbunkle, Silf, Lich, Cyclops
Preach, Insult, Refute



ZergTwitch
Male
Cancer
73
76
Lancer
Punch Art
Absorb Used MP
Dual Wield
Waterwalking

Ivory Rod
Javelin
Gold Helmet
Genji Armor
Reflect Ring

Level Jump8, Vertical Jump2
Spin Fist, Secret Fist, Purification

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Meta Five
Male
Leo
75
69
Monk
Steal
Counter
Beastmaster
Levitate



Triangle Hat
Leather Outfit
Bracer

Pummel, Wave Fist, Purification, Revive
Gil Taking



Amishninja
Male
Cancer
77
67
Samurai
Punch Art
Parry
Magic Attack UP
Fly

Obelisk

Iron Helmet
Gold Armor
Defense Armlet

Kiyomori
Spin Fist, Purification



Kronikle
Female
Pisces
62
72
Summoner
Black Magic
Abandon
Short Charge
Retreat

Rod

Headgear
Robe of Lords
Elf Mantle

Moogle, Titan, Golem, Fairy
Fire 3, Bolt, Ice 3, Flare



SSwing
Female
Aries
74
59
Monk
Battle Skill
Sunken State
Defense UP
Levitate



Triangle Hat
Mythril Vest
Battle Boots

Spin Fist, Pummel, Revive, Seal Evil
Armor Break, Shield Break, Speed Break, Mind Break

Player: !Black
Team: Black Team
Palettes: Black/Red



SUGRboi
Female
Capricorn
79
40
Monk
Draw Out
Speed Save
Short Charge
Jump+1



Cachusha
Brigandine
Wizard Mantle

Spin Fist, Earth Slash, Purification, Chakra, Revive
Koutetsu, Bizen Boat, Heaven's Cloud



Lythe Caraker
Monster
Taurus
71
47
Bull Demon










Flacococo
Female
Sagittarius
56
77
Geomancer
Dance
Counter Flood
Magic Attack UP
Ignore Terrain

Giant Axe
Platinum Shield
Black Hood
Wizard Robe
Rubber Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Witch Hunt, Disillusion, Void Storage



ThreeMileIsland
Female
Virgo
45
48
Wizard
Punch Art
Counter Flood
Concentrate
Teleport

Zorlin Shape

Green Beret
Black Robe
Rubber Shoes

Fire 2, Fire 3, Fire 4, Ice, Ice 4, Empower
Spin Fist, Pummel, Earth Slash, Purification, Chakra

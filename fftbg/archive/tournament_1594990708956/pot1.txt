Final Bets: red - 7 bets for 3,327G (22.0%, x3.54); blue - 12 bets for 11,763G (78.0%, x0.28)

red bets:
J2DaBibbles: 1,000G (30.1%, 23,765G)
BirbBrainsBot: 738G (22.2%, 21,526G)
dogsandcatsand: 576G (17.3%, 15,543G)
getthemoneyz: 416G (12.5%, 1,301,127G)
brokenknight201: 347G (10.4%, 347G)
Xoomwaffle: 150G (4.5%, 5,872G)
mrfripps: 100G (3.0%, 5,468G)

blue bets:
TheChainNerd: 5,942G (50.5%, 59,423G)
NIghtdew14: 2,263G (19.2%, 11,318G)
FriendSquirrel: 1,072G (9.1%, 1,072G)
Sairentozon7: 1,000G (8.5%, 7,329G)
boosted420: 500G (4.3%, 2,887G)
AllInBot: 385G (3.3%, 385G)
datadrivenbot: 200G (1.7%, 33,677G)
DouglasDragonThePoet: 100G (0.9%, 2,109G)
lastly: 100G (0.9%, 30,990G)
nhammen: 100G (0.9%, 14,782G)
Bongomon7: 100G (0.9%, 12,649G)
SephDarkheart: 1G (0.0%, 54,584G)

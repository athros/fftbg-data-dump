Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zeroroute
Female
Capricorn
49
64
Oracle
Talk Skill
PA Save
Magic Attack UP
Move+1

Madlemgen

Green Beret
Earth Clothes
Cursed Ring

Blind, Spell Absorb, Life Drain
Praise, Insult, Refute, Rehabilitate



PrudishDuckling
Female
Sagittarius
73
82
Dancer
Yin Yang Magic
Damage Split
Dual Wield
Teleport

Cashmere
Ryozan Silk
Leather Hat
Black Robe
Red Shoes

Witch Hunt, Disillusion, Void Storage
Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



Pplvee1
Female
Cancer
48
43
Geomancer
Time Magic
Dragon Spirit
Equip Polearm
Teleport

Spear
Platinum Shield
Green Beret
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Haste, Haste 2, Stop, Immobilize, Demi



PoroTact
Female
Leo
74
69
Thief
Black Magic
Caution
Magic Defense UP
Ignore Terrain

Dagger

Holy Miter
Rubber Costume
Reflect Ring

Steal Accessory, Steal Status, Arm Aim
Fire, Fire 3, Fire 4, Bolt 3, Ice 3, Ice 4

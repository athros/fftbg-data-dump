Player: !Black
Team: Black Team
Palettes: Black/Red



Snoopiku
Male
Taurus
61
56
Lancer
Punch Art
Brave Save
Equip Knife
Swim

Assassin Dagger
Mythril Shield
Barbuta
Leather Armor
Leather Mantle

Level Jump5, Vertical Jump8
Spin Fist, Earth Slash, Purification, Seal Evil



Firesheath
Female
Aries
77
50
Chemist
Punch Art
Arrow Guard
Martial Arts
Waterwalking

Panther Bag

Twist Headband
Leather Outfit
Diamond Armlet

Potion, Elixir, Antidote, Soft, Holy Water, Phoenix Down
Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil



Wonser
Male
Virgo
76
65
Oracle
Time Magic
Brave Save
Dual Wield
Retreat

Octagon Rod
Battle Bamboo
Headgear
Silk Robe
N-Kai Armlet

Confusion Song, Sleep
Haste, Slow



Galkife
Male
Gemini
65
75
Mime

Critical Quick
Equip Shield
Jump+3


Venetian Shield
Golden Hairpin
Mystic Vest
Bracer

Mimic


Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Male
Scorpio
52
54
Ninja
Elemental
Critical Quick
Beastmaster
Ignore Terrain

Flame Whip
Sasuke Knife
Feather Hat
Rubber Costume
N-Kai Armlet

Shuriken, Bomb
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Butterbelljedi
Monster
Aquarius
59
75
Bull Demon










Mesmaster
Female
Virgo
64
64
Wizard
Draw Out
Parry
Equip Armor
Jump+1

Rod

Cachusha
Leather Armor
Angel Ring

Bolt 4, Ice 4
Koutetsu



Vorap
Male
Libra
57
70
Monk
Black Magic
Caution
Long Status
Waterbreathing



Holy Miter
Chain Vest
Battle Boots

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil
Fire 4, Bolt, Ice 2, Frog

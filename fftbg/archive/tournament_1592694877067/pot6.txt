Final Bets: white - 6 bets for 16,491G (45.6%, x1.19); purple - 11 bets for 19,649G (54.4%, x0.84)

white bets:
Aldrammech: 14,607G (88.6%, 29,214G)
cam_ATS: 936G (5.7%, 936G)
DAC169: 348G (2.1%, 348G)
skillomono: 300G (1.8%, 3,663G)
Magicandy: 200G (1.2%, 2,578G)
Fibrew1re: 100G (0.6%, 2,983G)

purple bets:
Mesmaster: 14,607G (74.3%, 262,342G)
soren_of_tyto: 1,776G (9.0%, 1,776G)
BirbBrainsBot: 1,000G (5.1%, 31,258G)
getthemoneyz: 652G (3.3%, 1,010,691G)
Breakdown777: 504G (2.6%, 504G)
Laserman1000: 500G (2.5%, 10,300G)
neocarbuncle: 210G (1.1%, 538G)
Heroebal: 100G (0.5%, 4,885G)
Neo_Exodus: 100G (0.5%, 2,523G)
datadrivenbot: 100G (0.5%, 43,344G)
Yiroep2: 100G (0.5%, 10,887G)

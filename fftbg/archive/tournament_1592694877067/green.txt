Player: !Green
Team: Green Team
Palettes: Green/White



Neocarbuncle
Male
Sagittarius
75
52
Knight
Punch Art
Counter
Magic Attack UP
Swim

Giant Axe
Bronze Shield
Bronze Helmet
Platinum Armor
Angel Ring

Head Break, Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Surging Sword
Spin Fist, Pummel, Purification, Revive



Galkife
Male
Capricorn
58
40
Knight
Talk Skill
Counter
Doublehand
Levitate

Ragnarok

Iron Helmet
White Robe
Leather Mantle

Armor Break, Weapon Break, Magic Break, Mind Break
Persuade, Threaten, Solution, Mimic Daravon, Rehabilitate



Pandasforsale
Female
Aries
44
75
Geomancer
Yin Yang Magic
Counter
Secret Hunt
Swim

Slasher
Ice Shield
Headgear
Silk Robe
108 Gems

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Confusion Song



CT 5 Holy
Male
Capricorn
53
60
Bard
Throw
Arrow Guard
Dual Wield
Move-MP Up

Ramia Harp
Ramia Harp
Headgear
Chain Vest
Rubber Shoes

Life Song, Battle Song, Magic Song, Space Storage, Hydra Pit
Knife, Hammer, Ninja Sword

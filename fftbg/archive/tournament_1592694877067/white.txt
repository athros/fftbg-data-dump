Player: !White
Team: White Team
Palettes: White/Blue



Ruebyy
Male
Aries
43
54
Knight
Elemental
Arrow Guard
Equip Bow
Move+3

Silver Bow
Round Shield
Bronze Helmet
Linen Cuirass
Genji Gauntlet

Head Break, Shield Break, Magic Break, Night Sword, Explosion Sword
Water Ball, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind, Lava Ball



KaLam1ty
Male
Capricorn
55
61
Samurai
Sing
Counter Flood
Equip Shield
Move+1

Murasame
Crystal Shield
Diamond Helmet
Carabini Mail
Wizard Mantle

Koutetsu, Heaven's Cloud
Life Song, Battle Song, Nameless Song, Last Song, Diamond Blade



Reddwind
Male
Cancer
64
64
Knight
Talk Skill
Counter
Doublehand
Lava Walking

Long Sword

Cross Helmet
Platinum Armor
Spike Shoes

Armor Break, Weapon Break, Power Break, Mind Break, Stasis Sword, Surging Sword
Preach, Death Sentence, Negotiate



Skillomono
Male
Leo
47
57
Time Mage
White Magic
Meatbone Slash
Equip Gun
Jump+2

Romanda Gun

Feather Hat
Silk Robe
Bracer

Haste, Slow, Slow 2, Reflect, Demi 2, Stabilize Time
Cure 4, Raise, Raise 2, Regen, Protect 2, Holy

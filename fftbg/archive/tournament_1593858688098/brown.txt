Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Prince Rogers Nelson
Female
Pisces
42
63
Chemist
Yin Yang Magic
MP Restore
Equip Gun
Ignore Height

Papyrus Codex

Feather Hat
Leather Outfit
Sprint Shoes

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Phoenix Down
Poison, Spell Absorb, Life Drain, Zombie, Blind Rage, Dispel Magic, Paralyze



Mesmaster
Male
Pisces
58
52
Knight
Punch Art
Speed Save
Maintenance
Jump+1

Coral Sword
Gold Shield
Platinum Helmet
Bronze Armor
Bracer

Head Break, Shield Break, Magic Break, Speed Break, Mind Break, Justice Sword
Wave Fist, Revive



KyleWonToLiveForever
Male
Aries
66
67
Thief
White Magic
Arrow Guard
Beastmaster
Move+1

Broad Sword

Cachusha
Secret Clothes
Magic Ring

Gil Taking, Steal Accessory, Arm Aim, Leg Aim
Raise, Reraise, Regen



Hulda
Female
Cancer
40
53
Priest
Yin Yang Magic
Regenerator
Long Status
Jump+3

Morning Star

Green Beret
Power Sleeve
Red Shoes

Cure 2, Cure 4, Raise, Reraise, Regen, Protect 2, Esuna
Blind, Zombie, Confusion Song, Paralyze, Sleep

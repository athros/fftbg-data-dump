Player: !Black
Team: Black Team
Palettes: Black/Red



Roqqqpsi
Male
Serpentarius
65
50
Wizard
Yin Yang Magic
Counter Flood
Equip Axe
Ignore Height

Flail

Golden Hairpin
Black Robe
Feather Mantle

Fire 2, Fire 3, Bolt 4, Ice
Dispel Magic



OpHendoslice
Male
Capricorn
67
58
Samurai
Throw
Mana Shield
Short Charge
Move+3

Heaven's Cloud

Grand Helmet
Wizard Robe
Magic Gauntlet

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Shuriken, Bomb, Stick, Dictionary



Reddwind
Male
Aquarius
78
68
Knight
Talk Skill
HP Restore
Halve MP
Ignore Terrain

Diamond Sword
Diamond Shield
Cross Helmet
Leather Armor
Diamond Armlet

Head Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Surging Sword
Threaten, Negotiate, Refute



Ruebyy
Female
Aquarius
41
76
Wizard
Summon Magic
MA Save
Equip Shield
Lava Walking

Wizard Rod
Escutcheon
Golden Hairpin
Black Robe
Magic Ring

Bolt, Bolt 2, Bolt 3, Ice 2, Death, Flare
Moogle, Carbunkle, Bahamut, Fairy, Cyclops

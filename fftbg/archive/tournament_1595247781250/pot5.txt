Final Bets: red - 8 bets for 8,278G (49.2%, x1.03); green - 10 bets for 8,564G (50.8%, x0.97)

red bets:
AllInBot: 5,589G (67.5%, 5,589G)
LDSkinny: 1,000G (12.1%, 29,127G)
skipsandwiches: 513G (6.2%, 513G)
Legitimized: 468G (5.7%, 468G)
Ungaiii: 334G (4.0%, 334G)
datadrivenbot: 200G (2.4%, 32,542G)
Beastmages: 124G (1.5%, 124G)
DouglasDragonThePoet: 50G (0.6%, 1,258G)

green bets:
RaIshtar: 2,500G (29.2%, 68,796G)
Error72: 1,758G (20.5%, 1,758G)
NIghtdew14: 1,000G (11.7%, 12,834G)
Lord_Burrah: 1,000G (11.7%, 77,000G)
BirbBrainsBot: 1,000G (11.7%, 141,069G)
SephDarkheart: 332G (3.9%, 53,391G)
ruleof5: 300G (3.5%, 12,358G)
nhammen: 300G (3.5%, 9,738G)
getthemoneyz: 274G (3.2%, 1,330,576G)
Bongomon7: 100G (1.2%, 11,162G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Triskiut
Monster
Aries
61
74
Bomb










E Ballard
Monster
Virgo
58
56
Bull Demon










Fenaen
Female
Aquarius
70
55
Geomancer
Charge
Speed Save
Beastmaster
Teleport 2

Battle Axe
Diamond Shield
Twist Headband
Rubber Costume
Sprint Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Charge+4



Forkmore
Male
Gemini
41
55
Calculator
Black Magic
MP Restore
Doublehand
Move+3

Iron Fan

Golden Hairpin
Light Robe
Sprint Shoes

CT, Height, Prime Number, 5, 3
Fire, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 3

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Eightflyingcars
Male
Taurus
68
55
Priest
Item
Regenerator
Dual Wield
Waterwalking

Flame Whip
Morning Star
Headgear
Light Robe
Angel Ring

Cure 4, Raise, Reraise, Regen, Protect, Shell 2, Holy
Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down



WhiteTigress
Female
Scorpio
82
56
Wizard
Math Skill
Critical Quick
Equip Gun
Move+1

Fairy Harp

Black Hood
Earth Clothes
Small Mantle

Fire, Fire 3, Fire 4, Bolt 3
CT, Height, 4, 3



Sinnyil2
Male
Aries
66
58
Oracle
Throw
PA Save
Dual Wield
Swim

Iron Fan
Battle Bamboo
Feather Hat
Earth Clothes
Defense Armlet

Doubt Faith, Confusion Song, Dispel Magic, Sleep
Shuriken, Staff, Ninja Sword, Axe, Dictionary



Holdenmagronik
Male
Virgo
41
56
Lancer
Draw Out
Auto Potion
Equip Axe
Move+1

Battle Axe
Diamond Shield
Leather Helmet
Linen Robe
Diamond Armlet

Level Jump8, Vertical Jump7
Murasame, Kiyomori, Masamune

Final Bets: purple - 11 bets for 6,429G (40.6%, x1.47); brown - 7 bets for 9,423G (59.4%, x0.68)

purple bets:
douchetron: 3,816G (59.4%, 7,484G)
BirbBrainsBot: 1,000G (15.6%, 134,618G)
getthemoneyz: 496G (7.7%, 1,558,046G)
DesertWooder: 340G (5.3%, 340G)
datadrivenbot: 200G (3.1%, 48,577G)
foxfaeez: 127G (2.0%, 1,272G)
eightflyingcars: 100G (1.6%, 1,231G)
srdonko2098: 100G (1.6%, 2,814G)
alex115daniels: 100G (1.6%, 1,365G)
Twisted_Nutsatchel: 100G (1.6%, 15,899G)
northkoreancleetus: 50G (0.8%, 1,058G)

brown bets:
DeathTaxesAndAnime: 6,116G (64.9%, 11,994G)
UmaiJam: 2,000G (21.2%, 39,672G)
CassiePhoenix: 647G (6.9%, 647G)
AllInBot: 259G (2.7%, 259G)
gorgewall: 201G (2.1%, 2,557G)
MinBetBot: 100G (1.1%, 16,393G)
Zeera98: 100G (1.1%, 442G)

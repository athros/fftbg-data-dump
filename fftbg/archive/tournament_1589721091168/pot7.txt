Final Bets: red - 12 bets for 2,756G (25.2%, x2.96); brown - 18 bets for 8,167G (74.8%, x0.34)

red bets:
superdevon1: 509G (18.5%, 10,182G)
elburritofuerte: 500G (18.1%, 8,092G)
DustBirdEX: 456G (16.5%, 13,074G)
Arbitae: 242G (8.8%, 242G)
marin1987: 240G (8.7%, 240G)
BirbBrainsBot: 213G (7.7%, 127,110G)
BetooSalvaje: 150G (5.4%, 599G)
TrueRhyme: 100G (3.6%, 8,860G)
Lolprinze: 100G (3.6%, 3,054G)
ausar1: 100G (3.6%, 489G)
josephiroth_143: 100G (3.6%, 2,049G)
getthemoneyz: 46G (1.7%, 680,420G)

brown bets:
maakur_: 1,906G (23.3%, 1,906G)
TheFALLofLindsay: 1,010G (12.4%, 1,010G)
leakimiko: 1,000G (12.2%, 42,468G)
Estan_AD: 942G (11.5%, 1,884G)
SuicideKing_XIX: 727G (8.9%, 727G)
AUrato: 500G (6.1%, 3,472G)
Drusic: 500G (6.1%, 7,489G)
HorusTaurus: 300G (3.7%, 300G)
roqqqpsi: 233G (2.9%, 1,947G)
wooyuji: 220G (2.7%, 220G)
eudes89: 156G (1.9%, 156G)
MalakiGenesys: 125G (1.5%, 1,278G)
Hellotrixxy: 123G (1.5%, 974G)
ko2q: 100G (1.2%, 9,146G)
gggamezzz: 100G (1.2%, 3,558G)
ungabunga_bot: 100G (1.2%, 519,864G)
datadrivenbot: 100G (1.2%, 18,962G)
isolatedchapelsix: 25G (0.3%, 920G)

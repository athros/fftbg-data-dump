Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KevvTwo
Female
Aries
53
41
Archer
Dance
Absorb Used MP
Equip Shield
Ignore Terrain

Silver Bow
Bronze Shield
Genji Helmet
Judo Outfit
Leather Mantle

Charge+2, Charge+3
Polka Polka, Disillusion, Nameless Dance, Obsidian Blade



Ezefeld
Male
Leo
64
78
Knight
Item
Mana Shield
Martial Arts
Jump+1

Diamond Sword
Bronze Shield
Bronze Helmet
Leather Armor
Reflect Ring

Weapon Break, Magic Break
Ether, Hi-Ether



King Moogle Mog
Male
Cancer
52
78
Monk
Talk Skill
MA Save
Dual Wield
Jump+1



Green Beret
Brigandine
Sprint Shoes

Pummel, Secret Fist, Chakra, Revive, Seal Evil
Persuade, Praise, Solution, Mimic Daravon, Refute



TheGuesty
Male
Sagittarius
62
57
Samurai
Charge
Distribute
Magic Defense UP
Ignore Terrain

Mythril Spear

Leather Helmet
Chain Mail
Sprint Shoes

Koutetsu, Heaven's Cloud, Muramasa
Charge+2, Charge+7

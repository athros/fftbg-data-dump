Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Willjin
Male
Aquarius
74
47
Archer
White Magic
Dragon Spirit
Doublehand
Ignore Height

Ultimus Bow

Holy Miter
Clothes
Magic Ring

Charge+3, Charge+5, Charge+10, Charge+20
Raise, Shell, Shell 2, Esuna



Skipsandwiches
Male
Aquarius
73
76
Lancer
Yin Yang Magic
MA Save
Equip Knife
Move-MP Up

Thunder Rod
Genji Shield
Gold Helmet
Platinum Armor
Angel Ring

Level Jump4, Vertical Jump2
Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy



Scurg
Male
Gemini
69
50
Summoner
Punch Art
MP Restore
Long Status
Lava Walking

Ice Rod

Green Beret
Chameleon Robe
Battle Boots

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Silf
Spin Fist, Secret Fist, Purification, Chakra



UmaiJam
Female
Scorpio
69
51
Summoner
Yin Yang Magic
Distribute
Short Charge
Move+1

Flame Rod

Feather Hat
Clothes
N-Kai Armlet

Moogle, Shiva, Carbunkle
Blind, Poison, Life Drain, Pray Faith, Foxbird, Sleep, Petrify

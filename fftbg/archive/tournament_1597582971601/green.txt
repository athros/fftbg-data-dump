Player: !Green
Team: Green Team
Palettes: Green/White



VolgraTheMoose
Female
Scorpio
80
67
Geomancer
Draw Out
Arrow Guard
Halve MP
Levitate

Muramasa
Diamond Shield
Headgear
Chain Vest
Cherche

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Asura, Koutetsu, Kikuichimoji



Ko2q
Male
Capricorn
43
61
Knight
Steal
Mana Shield
Short Charge
Teleport

Slasher
Buckler
Barbuta
Chain Mail
Leather Mantle

Armor Break, Weapon Break, Justice Sword
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Leg Aim



WhiteTigress
Female
Virgo
79
50
Priest
Item
MA Save
Throw Item
Move+1

Healing Staff

Leather Hat
Wizard Robe
Magic Gauntlet

Cure 2, Raise, Raise 2, Regen, Protect 2, Wall, Esuna, Holy
Potion, Hi-Potion, X-Potion, Maiden's Kiss, Phoenix Down



Lokenwow
Monster
Cancer
65
75
Behemoth








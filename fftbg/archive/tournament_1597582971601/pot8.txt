Final Bets: purple - 15 bets for 8,403G (63.6%, x0.57); champion - 7 bets for 4,814G (36.4%, x1.75)

purple bets:
dogsandcatsand: 2,000G (23.8%, 3,321G)
Tarheels218: 1,160G (13.8%, 1,160G)
BirbBrainsBot: 1,000G (11.9%, 174,901G)
roqqqpsi: 621G (7.4%, 4,777G)
E_Ballard: 620G (7.4%, 3,604G)
douchetron: 600G (7.1%, 600G)
Draconis345: 500G (6.0%, 13,267G)
VolgraTheMoose: 500G (6.0%, 6,399G)
getthemoneyz: 350G (4.2%, 1,659,263G)
skipsandwiches: 324G (3.9%, 324G)
WhiteTigress: 200G (2.4%, 2,713G)
datadrivenbot: 200G (2.4%, 66,071G)
arctodus13: 128G (1.5%, 128G)
Thyrandaal: 100G (1.2%, 255,201G)
Error72: 100G (1.2%, 1,833G)

champion bets:
SeniorBunk: 1,710G (35.5%, 1,710G)
Mesmaster: 1,000G (20.8%, 23,879G)
AllInBot: 725G (15.1%, 725G)
Smashy: 500G (10.4%, 8,244G)
ko2q: 480G (10.0%, 480G)
Ayntlerz: 250G (5.2%, 1,419G)
richardserious: 149G (3.1%, 2,982G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Neerrm
Male
Taurus
80
73
Lancer
Basic Skill
PA Save
Magic Attack UP
Move-MP Up

Mythril Spear
Gold Shield
Grand Helmet
Robe of Lords
Feather Boots

Level Jump5, Vertical Jump8
Dash, Heal, Fury, Wish, Ultima



Error72
Male
Aquarius
68
56
Knight
Elemental
Earplug
Martial Arts
Jump+2

Ragnarok
Diamond Shield
Mythril Helmet
Black Robe
Spike Shoes

Weapon Break, Justice Sword, Dark Sword, Night Sword, Surging Sword
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Tohim
Female
Cancer
65
69
Monk
Basic Skill
Damage Split
Short Status
Move+2



Green Beret
Leather Outfit
Spike Shoes

Wave Fist, Secret Fist, Purification
Accumulate, Dash, Throw Stone, Tickle, Yell, Cheer Up, Wish



Powergems
Female
Pisces
42
67
Calculator
White Magic
MA Save
Dual Wield
Jump+1

Madlemgen
Bestiary
Thief Hat
Earth Clothes
Bracer

CT, Height, Prime Number, 4
Cure, Raise, Raise 2, Reraise, Protect, Shell, Shell 2

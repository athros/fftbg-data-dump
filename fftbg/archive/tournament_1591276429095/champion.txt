Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Volgrathemoose
Male
Cancer
76
52
Lancer
Charge
Sunken State
Doublehand
Move+3

Obelisk

Cross Helmet
Chain Mail
Sprint Shoes

Level Jump8, Vertical Jump5
Charge+2, Charge+4, Charge+20



Zachara
Female
Libra
61
40
Geomancer
Draw Out
Critical Quick
Doublehand
Waterwalking

Giant Axe

Black Hood
Power Sleeve
Wizard Mantle

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Asura, Koutetsu, Kikuichimoji



Rislyeu
Female
Pisces
61
66
Knight
Time Magic
Regenerator
Beastmaster
Move-MP Up

Ragnarok
Aegis Shield
Crystal Helmet
Leather Armor
Feather Boots

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Dark Sword
Haste, Haste 2, Slow, Immobilize, Reflect, Demi, Stabilize Time



RaIshtar
Male
Pisces
46
77
Priest
Black Magic
Distribute
Doublehand
Jump+1

Oak Staff

Holy Miter
Light Robe
Magic Gauntlet

Raise, Raise 2, Reraise, Protect 2, Esuna
Fire 2, Bolt 2, Ice, Ice 3, Empower

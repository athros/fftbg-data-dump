Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Odin007 88
Female
Taurus
56
67
Squire
Elemental
Catch
Doublehand
Move-MP Up

Morning Star

Iron Helmet
Judo Outfit
Dracula Mantle

Throw Stone, Heal, Cheer Up
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Catfashions
Female
Leo
74
42
Archer
Item
Auto Potion
Beastmaster
Jump+2

Hunting Bow
Diamond Shield
Green Beret
Wizard Outfit
Bracer

Charge+1, Charge+2
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Holy Water, Phoenix Down



HaateXIII
Male
Capricorn
74
67
Thief
Elemental
MP Restore
Equip Gun
Waterwalking

Blast Gun

Ribbon
Judo Outfit
Sprint Shoes

Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind



Mtueni
Male
Sagittarius
43
50
Geomancer
Steal
Hamedo
Equip Gun
Move+3

Battle Folio
Mythril Shield
Feather Hat
Light Robe
Battle Boots

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Gil Taking, Steal Helmet, Leg Aim

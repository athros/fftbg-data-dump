Final Bets: purple - 15 bets for 7,419G (36.8%, x1.72); brown - 11 bets for 12,757G (63.2%, x0.58)

purple bets:
Aldrammech: 1,000G (13.5%, 13,179G)
getthemoneyz: 1,000G (13.5%, 1,229,080G)
killth3kid: 1,000G (13.5%, 95,906G)
DLJuggernaut: 754G (10.2%, 754G)
DamnThatShark: 608G (8.2%, 608G)
evontno: 540G (7.3%, 540G)
Nizaha: 532G (7.2%, 5,454G)
superdevon1: 408G (5.5%, 408G)
alekzanndrr: 400G (5.4%, 5,794G)
prince_rogers_nelson_: 348G (4.7%, 348G)
Seaweed_B: 228G (3.1%, 13,562G)
gorgewall: 201G (2.7%, 15,083G)
chaos_sux_unless_its_ds: 200G (2.7%, 5,469G)
TasisSai: 100G (1.3%, 784G)
nifboy: 100G (1.3%, 17,058G)

brown bets:
Lydian_C: 4,200G (32.9%, 113,178G)
Evewho: 2,000G (15.7%, 24,752G)
Mesmaster: 2,000G (15.7%, 13,215G)
Sairentozon7: 1,468G (11.5%, 1,468G)
BirbBrainsBot: 1,000G (7.8%, 163,461G)
Laserman1000: 741G (5.8%, 28,141G)
letdowncity: 376G (2.9%, 5,140G)
ACSpree: 364G (2.9%, 364G)
Shalloween: 300G (2.4%, 56,921G)
flacococo: 208G (1.6%, 208G)
rednecknazgul: 100G (0.8%, 1,161G)

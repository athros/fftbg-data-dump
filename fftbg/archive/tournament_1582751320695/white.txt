Player: !White
Team: White Team
Palettes: White/Blue



BlackFireUK
Male
Libra
59
51
Mime

Mana Shield
Magic Defense UP
Ignore Terrain



Flash Hat
Adaman Vest
Battle Boots

Mimic




Silgryn
Male
Sagittarius
45
66
Ninja
Jump
Counter Flood
Equip Shield
Ignore Height

Flail
Diamond Shield
Triangle Hat
Judo Outfit
Small Mantle

Shuriken, Ninja Sword, Spear
Level Jump2, Vertical Jump8



TheKillerNacho
Female
Leo
78
51
Archer
Item
Faith Up
Martial Arts
Waterwalking


Bronze Shield
Black Hood
Mystic Vest
Genji Gauntlet

Charge+1, Charge+3, Charge+4
Potion, Hi-Ether, Maiden's Kiss



Bad1dea
Male
Taurus
75
62
Monk
Steal
Counter Magic
Equip Sword
Jump+1

Kikuichimoji

Thief Hat
Brigandine
Jade Armlet

Purification, Revive, Seal Evil
Steal Helmet, Steal Shield, Steal Status

Player: !Green
Team: Green Team
Palettes: Green/White



Goust18
Female
Gemini
52
67
Summoner
Throw
PA Save
Short Charge
Move+3

Rod

Thief Hat
Wizard Robe
Feather Mantle

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Silf, Fairy
Bomb, Dictionary



HaplessOne
Male
Capricorn
51
68
Lancer
Time Magic
Abandon
Doublehand
Jump+1

Gungnir

Circlet
Mythril Armor
Rubber Shoes

Level Jump5, Vertical Jump2
Slow 2, Reflect



EvilLego6
Male
Scorpio
50
75
Archer
Battle Skill
Catch
Short Status
Move+3

Ultimus Bow

Circlet
Mystic Vest
Red Shoes

Charge+4, Charge+7, Charge+10, Charge+20
Head Break, Shield Break, Mind Break, Justice Sword



Hales Bopp It
Female
Capricorn
50
71
Calculator
Time Magic
Speed Save
Equip Axe
Move+1

Slasher

Headgear
Linen Robe
Sprint Shoes

Height, Prime Number, 4
Float, Reflect, Demi, Stabilize Time

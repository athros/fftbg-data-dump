Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Oobs56
Monster
Pisces
66
62
Goblin










Kyune
Male
Capricorn
80
45
Thief
Time Magic
PA Save
Equip Gun
Teleport

Mythril Gun

Triangle Hat
Chain Vest
Wizard Mantle

Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Haste 2, Slow, Float, Reflect, Quick



Arch8000
Male
Aquarius
72
67
Summoner
Punch Art
Parry
Magic Defense UP
Move+1

Flame Rod

Thief Hat
Wizard Outfit
Elf Mantle

Shiva, Ramuh, Lich, Cyclops
Spin Fist, Wave Fist, Purification, Seal Evil



Roofiepops
Female
Gemini
51
51
Geomancer
Talk Skill
Regenerator
Attack UP
Ignore Height

Kiyomori
Ice Shield
Ribbon
Chameleon Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Persuade, Praise, Threaten, Solution, Insult, Refute, Rehabilitate

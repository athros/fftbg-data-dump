Player: !Black
Team: Black Team
Palettes: Black/Red



Arkivegoof
Monster
Capricorn
48
79
Hydra










Shalloween
Male
Capricorn
45
71
Monk
Time Magic
PA Save
Dual Wield
Move+2



Triangle Hat
Earth Clothes
Magic Ring

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Haste 2, Stop, Quick, Demi 2, Stabilize Time, Meteor



Virilikus
Female
Aquarius
74
50
Geomancer
Draw Out
Counter
Magic Attack UP
Waterbreathing

Slasher
Buckler
Red Hood
Robe of Lords
Battle Boots

Hell Ivy, Hallowed Ground, Static Shock, Quicksand
Bizen Boat



Actual JP
Female
Libra
69
60
Calculator
Beast Skill
Caution
Maintenance
Move+2

Bestiary
Escutcheon
Cross Helmet
Robe of Lords
Defense Armlet

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest

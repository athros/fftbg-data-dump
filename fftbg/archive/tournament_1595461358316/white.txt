Player: !White
Team: White Team
Palettes: White/Blue



HaateXIII
Female
Sagittarius
43
43
Ninja
Item
Dragon Spirit
Concentrate
Jump+3

Blind Knife
Short Edge
Red Hood
Chain Vest
Small Mantle

Shuriken
Potion, Hi-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down



Lanshaft
Male
Aries
52
75
Bard
Talk Skill
Parry
Short Charge
Jump+2

Ramia Harp

Thief Hat
Clothes
Rubber Shoes

Battle Song, Nameless Song
Invitation, Praise, Preach, Solution, Death Sentence, Insult, Refute



Douchetron
Male
Cancer
46
48
Bard
Steal
Caution
Halve MP
Ignore Height

Bloody Strings

Headgear
Mystic Vest
Setiemson

Life Song, Diamond Blade, Sky Demon
Arm Aim, Leg Aim



Hasterious
Male
Taurus
77
73
Ninja
Basic Skill
Counter Tackle
Secret Hunt
Waterbreathing

Assassin Dagger
Blind Knife
Twist Headband
Mythril Vest
108 Gems

Staff
Accumulate, Dash, Wish

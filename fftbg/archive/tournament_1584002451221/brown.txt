Player: !Brown
Team: Brown Team
Palettes: Brown/Green



E7bbk
Male
Leo
59
68
Chemist
Basic Skill
Speed Save
Concentrate
Levitate

Panther Bag

Thief Hat
Brigandine
Genji Gauntlet

Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Heal, Tickle, Yell, Cheer Up



WhiteTigress
Female
Pisces
66
71
Geomancer
Draw Out
Dragon Spirit
Beastmaster
Waterwalking

Giant Axe
Aegis Shield
Feather Hat
Mystic Vest
Sprint Shoes

Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Gusty Wind
Asura, Koutetsu, Bizen Boat



Azreyam
Male
Taurus
77
54
Priest
Throw
Counter Tackle
Defense UP
Ignore Terrain

White Staff

Cachusha
White Robe
Spike Shoes

Cure 2, Raise, Reraise, Protect 2, Wall, Esuna
Shuriken, Staff, Axe



LanseDM
Female
Libra
53
75
Summoner
Charge
Parry
Short Charge
Jump+1

Ice Rod

Green Beret
Clothes
Bracer

Moogle, Bahamut, Odin, Salamander, Silf, Fairy, Cyclops
Charge+1, Charge+3, Charge+20

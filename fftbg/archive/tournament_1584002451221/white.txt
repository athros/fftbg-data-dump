Player: !White
Team: White Team
Palettes: White/Blue



OlafNorthfire
Male
Scorpio
55
77
Lancer
Draw Out
Meatbone Slash
Equip Bow
Jump+1

Night Killer
Genji Shield
Barbuta
Chain Mail
Sprint Shoes

Level Jump8, Vertical Jump2
Asura, Bizen Boat, Murasame, Kiyomori, Muramasa



Superdevon1
Male
Leo
70
77
Monk
Jump
Abandon
Beastmaster
Move+2



Flash Hat
Black Costume
Reflect Ring

Spin Fist, Purification, Chakra, Revive
Level Jump2, Vertical Jump8



ZeracheilGames
Male
Sagittarius
72
64
Monk
Charge
Parry
Short Charge
Ignore Height



Triangle Hat
Mystic Vest
Feather Mantle

Pummel, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil
Charge+5, Charge+7, Charge+20



Dynasti
Male
Gemini
51
62
Knight
Time Magic
Damage Split
Defense UP
Teleport

Defender

Diamond Helmet
Wizard Robe
Genji Gauntlet

Armor Break, Justice Sword, Surging Sword
Haste, Slow 2, Stabilize Time

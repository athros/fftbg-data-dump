Final Bets: red - 8 bets for 4,142G (48.2%, x1.08); blue - 9 bets for 4,459G (51.8%, x0.93)

red bets:
ShintaroNayaka: 1,314G (31.7%, 6,571G)
BirbBrainsBot: 1,000G (24.1%, 163,415G)
Error72: 1,000G (24.1%, 13,311G)
loveyouallfriends: 208G (5.0%, 208G)
CosmicTactician: 200G (4.8%, 20,038G)
joewcarson: 200G (4.8%, 1,069G)
Lydian_C: 120G (2.9%, 217,131G)
maakur_: 100G (2.4%, 75,444G)

blue bets:
dogsandcatsand: 1,000G (22.4%, 2,075G)
defaultlybrave: 1,000G (22.4%, 8,132G)
getthemoneyz: 796G (17.9%, 1,036,471G)
TheChainNerd: 508G (11.4%, 508G)
Ruvelia_BibeI: 500G (11.2%, 3,042G)
NotBalrog: 300G (6.7%, 8,342G)
ar_tactic: 250G (5.6%, 54,736G)
datadrivenbot: 100G (2.2%, 48,885G)
O_Heyno: 5G (0.1%, 1,476G)

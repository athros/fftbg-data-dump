Final Bets: red - 6 bets for 1,941G (20.9%, x3.77); green - 11 bets for 7,326G (79.1%, x0.26)

red bets:
DeathTaxesAndAnime: 992G (51.1%, 992G)
AllInBot: 349G (18.0%, 349G)
Lumberinjack: 300G (15.5%, 6,684G)
MinBetBot: 100G (5.2%, 16,700G)
RunicMagus: 100G (5.2%, 3,032G)
Brainstew29: 100G (5.2%, 38,673G)

green bets:
Forkmore: 1,665G (22.7%, 3,330G)
sinnyil2: 1,657G (22.6%, 1,657G)
BirbBrainsBot: 1,000G (13.7%, 110,616G)
reinoe: 1,000G (13.7%, 150,603G)
Smashy: 500G (6.8%, 5,199G)
CorpusCav: 444G (6.1%, 4,144G)
Amorial: 311G (4.2%, 311G)
coralreeferz: 250G (3.4%, 28,359G)
datadrivenbot: 200G (2.7%, 49,412G)
getthemoneyz: 154G (2.1%, 1,554,025G)
Skjodo: 145G (2.0%, 145G)

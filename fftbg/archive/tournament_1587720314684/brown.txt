Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HaplessOne
Male
Libra
43
49
Geomancer
Battle Skill
Caution
Beastmaster
Move-MP Up

Iron Sword
Bronze Shield
Red Hood
Chameleon Robe
Chantage

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Head Break, Armor Break, Shield Break, Power Break, Mind Break, Justice Sword, Dark Sword



Vorap
Female
Gemini
80
58
Chemist
Charge
Counter
Defend
Waterwalking

Stone Gun

Twist Headband
Black Costume
Germinas Boots

Potion, X-Potion, Ether, Maiden's Kiss
Charge+4



Perce90
Male
Capricorn
79
61
Calculator
Bird Skill
Counter
Beastmaster
Ignore Height

Bestiary
Hero Shield
Iron Helmet
Black Robe
Genji Gauntlet

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



RubenFlonne
Male
Aries
63
68
Knight
Jump
Auto Potion
Secret Hunt
Ignore Height

Battle Axe
Platinum Shield
Crystal Helmet
Silk Robe
Small Mantle

Armor Break, Shield Break, Weapon Break, Speed Break, Mind Break, Dark Sword, Surging Sword
Level Jump2, Vertical Jump8

Player: !Black
Team: Black Team
Palettes: Black/Red



Evewho
Female
Leo
59
60
Priest
Draw Out
MP Restore
Equip Gun
Waterbreathing

Bestiary

Golden Hairpin
Adaman Vest
Genji Gauntlet

Cure, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna
Asura, Heaven's Cloud, Kiyomori



AStatue
Monster
Taurus
59
73
Archaic Demon










HASTERIOUS
Female
Pisces
46
38
Monk
Steal
Earplug
Beastmaster
Ignore Terrain



Thief Hat
Judo Outfit
Power Wrist

Spin Fist, Pummel, Wave Fist, Purification, Chakra
Steal Armor, Steal Accessory, Steal Status



Mirapoix
Male
Serpentarius
58
47
Geomancer
Talk Skill
Counter Flood
Attack UP
Ignore Height

Iron Sword
Aegis Shield
Headgear
Linen Robe
Diamond Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Invitation, Praise, Preach, Insult

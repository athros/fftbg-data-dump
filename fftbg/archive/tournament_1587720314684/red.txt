Player: !Red
Team: Red Team
Palettes: Red/Brown



Roqqqpsi
Male
Virgo
67
47
Summoner
Yin Yang Magic
Speed Save
Defense UP
Move+2

Ice Rod

Feather Hat
Leather Outfit
Angel Ring

Moogle, Ifrit, Titan, Golem, Carbunkle, Silf, Cyclops
Blind, Foxbird, Confusion Song, Dispel Magic



CassiePhoenix
Female
Aquarius
66
80
Geomancer
Draw Out
Parry
Equip Sword
Move-MP Up

Ancient Sword
Buckler
Black Hood
Adaman Vest
Diamond Armlet

Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Murasame, Kikuichimoji



Shakarak
Female
Pisces
56
57
Geomancer
Charge
MA Save
Short Status
Teleport

Materia Blade
Mythril Shield
Holy Miter
Adaman Vest
Leather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Charge+3, Charge+4, Charge+7, Charge+10



Lydian C
Female
Pisces
63
77
Calculator
Time Magic
Arrow Guard
Equip Armor
Jump+1

Ice Rod

Iron Helmet
Carabini Mail
Reflect Ring

CT, Height, 5, 4, 3
Haste, Haste 2, Slow 2, Stop, Float, Demi, Stabilize Time

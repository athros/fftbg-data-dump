Player: !Green
Team: Green Team
Palettes: Green/White



Ewan E
Monster
Taurus
80
58
Blue Dragon










Dexef
Female
Gemini
48
66
Samurai
Charge
Critical Quick
Short Charge
Jump+1

Bizen Boat

Circlet
Reflect Mail
Defense Ring

Murasame, Heaven's Cloud, Kiyomori, Muramasa
Charge+1, Charge+7



Estan AD
Female
Taurus
71
54
Time Mage
Punch Art
Abandon
Dual Wield
Lava Walking

Rainbow Staff
Rainbow Staff
Green Beret
Judo Outfit
Angel Ring

Slow, Immobilize, Demi, Stabilize Time
Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil



Ashintar
Female
Cancer
60
51
Squire
Yin Yang Magic
Counter
Defend
Jump+3

Orichalcum
Platinum Shield
Platinum Helmet
Maximillian
Feather Boots

Accumulate, Dash, Heal, Tickle, Cheer Up, Wish
Pray Faith, Foxbird, Confusion Song, Paralyze

Player: !Green
Team: Green Team
Palettes: Green/White



MillionGhosts
Female
Taurus
39
43
Dancer
Battle Skill
Mana Shield
Long Status
Jump+1

Persia

Headgear
Robe of Lords
Genji Gauntlet

Polka Polka, Nameless Dance, Last Dance
Head Break, Armor Break, Weapon Break, Power Break



DeathTaxesAndAnime
Female
Scorpio
53
64
Summoner
Yin Yang Magic
Catch
Short Charge
Lava Walking

Poison Rod

Twist Headband
Chameleon Robe
Defense Armlet

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Fairy, Cyclops
Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify, Dark Holy



Marceleas
Female
Capricorn
62
50
Priest
Punch Art
Brave Up
Long Status
Levitate

Rainbow Staff

Barette
Brigandine
Genji Gauntlet

Cure, Cure 4, Raise, Protect 2, Shell, Wall, Esuna, Holy
Wave Fist, Revive, Seal Evil



Nizaha
Monster
Leo
62
58
Red Chocobo








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Roqqqpsi
Female
Gemini
77
64
Squire
Elemental
Damage Split
Equip Sword
Move-MP Up

Save the Queen

Diamond Helmet
Bronze Armor
Wizard Mantle

Dash, Yell, Fury, Wish, Scream
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Alc Trinity
Male
Scorpio
45
68
Wizard
Jump
Parry
Short Status
Jump+1

Dragon Rod

Black Hood
Linen Robe
Reflect Ring

Fire, Bolt 3, Ice 3
Level Jump4, Vertical Jump7



Maeveen
Male
Cancer
75
41
Thief
Throw
Catch
Beastmaster
Levitate

Koga Knife

Triangle Hat
Earth Clothes
Germinas Boots

Gil Taking, Steal Heart, Arm Aim
Knife



RongRongArts
Female
Capricorn
53
56
Chemist
Draw Out
Counter Tackle
Defense UP
Move+3

Air Knife

Holy Miter
Adaman Vest
Spike Shoes

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Remedy
Kiyomori, Muramasa, Chirijiraden

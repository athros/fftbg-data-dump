Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Xnoticemesempaix
Male
Leo
44
65
Summoner
Black Magic
MP Restore
Equip Knife
Jump+2

Flame Rod

Triangle Hat
Wizard Robe
Feather Mantle

Moogle, Titan, Carbunkle, Odin, Leviathan, Salamander, Lich
Fire, Bolt



Natat
Female
Leo
72
63
Geomancer
Basic Skill
Counter Magic
Secret Hunt
Move+1

Kiyomori
Bronze Shield
Flash Hat
Black Costume
Angel Ring

Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Accumulate, Heal, Tickle, Cheer Up



RRazza
Male
Capricorn
50
60
Oracle
Basic Skill
Absorb Used MP
Equip Bow
Waterbreathing

Cypress Rod

Thief Hat
Judo Outfit
Elf Mantle

Spell Absorb, Life Drain, Foxbird, Dark Holy
Heal, Yell, Cheer Up, Wish



Soldier Firstclass
Female
Scorpio
39
55
Chemist
Battle Skill
Regenerator
Doublehand
Waterwalking

Mythril Knife

Black Hood
Power Sleeve
Wizard Mantle

Potion, X-Potion, Antidote, Echo Grass, Phoenix Down
Head Break, Magic Break, Justice Sword

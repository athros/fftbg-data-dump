Player: !Red
Team: Red Team
Palettes: Red/Brown



Ladylyllith
Female
Aries
63
44
Summoner
Punch Art
Caution
Magic Defense UP
Retreat

Faith Rod

Green Beret
Chameleon Robe
Defense Ring

Moogle, Ramuh, Carbunkle, Fairy
Spin Fist, Earth Slash, Secret Fist, Purification



Ring Wyrm
Male
Virgo
44
43
Monk
Jump
Counter
Equip Gun
Move+1

Battle Folio

Leather Hat
Chain Vest
Magic Ring

Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil
Level Jump2, Vertical Jump7



Segomod
Male
Aquarius
74
47
Wizard
Sing
Earplug
Short Status
Move+1

Thunder Rod

Flash Hat
Chameleon Robe
Diamond Armlet

Fire 2, Fire 3, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Flare
Life Song, Magic Song, Last Song, Diamond Blade



Shalloween
Female
Libra
60
45
Mediator
Jump
Critical Quick
Dual Wield
Fly

Stone Gun
Mythril Gun
Leather Hat
Linen Robe
Angel Ring

Persuade, Praise, Preach, Solution, Mimic Daravon, Refute, Rehabilitate
Level Jump3, Vertical Jump3

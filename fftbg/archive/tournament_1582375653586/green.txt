Player: !Green
Team: Green Team
Palettes: Green/White



SALTY  RAGE
Male
Aries
59
62
Ninja
Steal
Regenerator
Equip Gun
Jump+2

Bloody Strings
Bestiary
Ribbon
Mythril Vest
N-Kai Armlet

Shuriken, Bomb, Knife
Steal Heart, Steal Accessory



ColetteMSLP
Male
Sagittarius
62
47
Priest
Basic Skill
Counter
Halve MP
Ignore Terrain

Flail

Twist Headband
Mythril Vest
Germinas Boots

Cure, Raise, Regen, Protect, Protect 2, Shell, Wall, Esuna
Throw Stone, Heal



Flox Silverbow
Monster
Cancer
44
70
Red Chocobo










Kalthonia
Female
Virgo
55
43
Mediator
Time Magic
MP Restore
Defend
Jump+2

Madlemgen

Flash Hat
Leather Outfit
Magic Gauntlet

Praise, Threaten, Solution, Negotiate, Refute
Haste, Slow 2, Stop, Demi, Demi 2

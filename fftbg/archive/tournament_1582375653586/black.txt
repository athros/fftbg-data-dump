Player: !Black
Team: Black Team
Palettes: Black/Red



Nachtkrieger
Male
Aries
59
77
Monk
White Magic
Hamedo
Dual Wield
Move-MP Up



Cachusha
Mystic Vest
108 Gems

Earth Slash, Secret Fist, Purification, Revive
Cure 2, Raise, Shell 2, Esuna



Upvla
Female
Pisces
79
74
Samurai
Punch Art
Regenerator
Equip Sword
Ignore Terrain

Platinum Sword

Cross Helmet
Bronze Armor
Sprint Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud
Pummel, Wave Fist, Secret Fist, Purification, Chakra



JohnDuggins
Monster
Leo
66
41
Reaper










ThePineappleSalesman
Male
Scorpio
74
42
Chemist
Time Magic
Hamedo
Equip Knife
Waterbreathing

Air Knife

Triangle Hat
Secret Clothes
Small Mantle

X-Potion, Ether, Holy Water, Phoenix Down
Slow 2, Stop, Float, Demi, Stabilize Time

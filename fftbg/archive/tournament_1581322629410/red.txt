Player: !Red
Team: Red Team
Palettes: Red/Brown



DieInObscurity
Male
Scorpio
56
63
Chemist
Charge
Parry
Attack UP
Jump+2

Cute Bag

Leather Hat
Brigandine
Magic Gauntlet

Potion, X-Potion, Echo Grass, Remedy, Phoenix Down
Charge+1, Charge+4, Charge+5



Lyronmkii
Female
Scorpio
76
73
Dancer
Punch Art
HP Restore
Secret Hunt
Ignore Height

Cashmere

Black Hood
Black Costume
Jade Armlet

Wiznaibus, Slow Dance, Nameless Dance, Obsidian Blade, Nether Demon
Purification, Chakra, Revive



Setsaku
Male
Virgo
52
76
Lancer
Draw Out
Catch
Equip Knife
Fly

Ivory Rod
Flame Shield
Circlet
Chain Mail
N-Kai Armlet

Level Jump4, Vertical Jump7
Bizen Boat, Murasame, Kikuichimoji



Powermhero
Male
Gemini
63
42
Thief
Sing
Damage Split
Doublehand
Teleport

Air Knife

Twist Headband
Clothes
Genji Gauntlet

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Angel Song

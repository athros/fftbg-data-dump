Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Happyfundude
Monster
Libra
67
74
Red Dragon










Shakarak
Female
Cancer
78
76
Thief
Punch Art
HP Restore
Equip Shield
Retreat

Dagger
Platinum Shield
Triangle Hat
Wizard Outfit
Elf Mantle

Steal Weapon
Pummel, Secret Fist, Purification, Chakra, Revive



Sinnyil2
Male
Gemini
43
41
Knight
Elemental
Brave Up
Secret Hunt
Teleport 2

Save the Queen

Circlet
Plate Mail
Red Shoes

Weapon Break, Magic Break, Power Break
Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard



Grininda
Male
Scorpio
55
60
Monk
Elemental
Parry
Short Status
Move+2



Triangle Hat
Black Costume
Defense Armlet

Pummel, Purification, Chakra, Revive, Seal Evil
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

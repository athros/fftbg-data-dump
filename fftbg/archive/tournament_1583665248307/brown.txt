Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Readdesert
Male
Sagittarius
62
73
Squire
Yin Yang Magic
Auto Potion
Equip Polearm
Fly

Cashmere
Crystal Shield
Twist Headband
Adaman Vest
Feather Mantle

Accumulate, Throw Stone, Heal, Tickle, Wish
Spell Absorb, Life Drain, Blind Rage, Paralyze



SkyridgeZero
Male
Gemini
60
62
Ninja
Item
Earplug
Equip Polearm
Fly

Musk Rod
Javelin
Thief Hat
Brigandine
Angel Ring

Shuriken, Bomb
Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water



ZeroHeat
Female
Cancer
65
60
Priest
Draw Out
Counter
Martial Arts
Retreat



Golden Hairpin
Chameleon Robe
Cherche

Cure 2, Cure 4, Raise, Protect, Esuna
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Upvla
Male
Pisces
57
56
Monk
Time Magic
Earplug
Sicken
Move+3



Black Hood
Rubber Costume
Jade Armlet

Pummel, Wave Fist, Purification, Revive
Haste, Slow 2, Float, Demi, Meteor

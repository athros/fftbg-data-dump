Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Theseawolf1
Female
Cancer
67
73
Wizard
Talk Skill
Counter Tackle
Monster Talk
Fly

Assassin Dagger

Flash Hat
Mythril Vest
Power Wrist

Fire, Fire 3, Bolt, Bolt 3, Ice, Ice 4, Empower, Death, Flare
Death Sentence, Negotiate, Mimic Daravon, Refute



Kronikle
Female
Aries
64
77
Time Mage
Draw Out
Dragon Spirit
Equip Knife
Move+2

Ice Rod

Golden Hairpin
Chain Vest
Sprint Shoes

Haste, Stop, Stabilize Time
Asura, Koutetsu, Kiyomori



Aldrammech
Male
Cancer
72
54
Knight
Jump
Counter Magic
Sicken
Fly

Sleep Sword
Round Shield
Circlet
Genji Armor
Magic Ring

Head Break, Weapon Break, Speed Break, Mind Break
Level Jump5, Vertical Jump4



Error72
Male
Libra
62
53
Knight
Basic Skill
Dragon Spirit
Doublehand
Swim

Defender

Genji Helmet
Crystal Mail
Defense Ring

Head Break, Weapon Break, Speed Break, Power Break, Justice Sword, Night Sword
Throw Stone, Heal, Yell, Wish

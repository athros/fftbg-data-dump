Final Bets: yellow - 8 bets for 11,945G (81.9%, x0.22); purple - 6 bets for 2,638G (18.1%, x4.53)

yellow bets:
Virilikus: 5,000G (41.9%, 333,169G)
Smashy: 2,000G (16.7%, 11,226G)
Theseawolf1: 2,000G (16.7%, 13,185G)
Sairentozon7: 1,000G (8.4%, 16,936G)
douchetron: 600G (5.0%, 600G)
JCBooBot: 574G (4.8%, 3,829G)
AllInBot: 463G (3.9%, 463G)
MagicBottle: 308G (2.6%, 2,879G)

purple bets:
Thyrandaal: 1,122G (42.5%, 1,122G)
Forkmore: 580G (22.0%, 2,586G)
Kronikle: 400G (15.2%, 14,214G)
3ngag3: 368G (13.9%, 9,500G)
gorgewall: 101G (3.8%, 7,970G)
Lifebregin: 67G (2.5%, 13,667G)

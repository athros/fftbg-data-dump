Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Armorgames1
Male
Taurus
50
61
Priest
Battle Skill
Counter Tackle
Equip Knife
Move+1

Poison Rod

Leather Hat
Power Sleeve
Red Shoes

Cure 3, Raise, Reraise, Protect, Shell, Esuna
Head Break, Stasis Sword



Kaidykat
Female
Virgo
46
56
Summoner
Charge
Counter Magic
Maintenance
Waterbreathing

Rod

Ribbon
Black Robe
Spike Shoes

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Silf, Cyclops
Charge+2, Charge+3



Galkife
Female
Aquarius
61
62
Chemist
Basic Skill
Absorb Used MP
Sicken
Move+2

Romanda Gun

Leather Hat
Clothes
Magic Ring

Potion, Hi-Potion, Echo Grass, Soft, Holy Water, Phoenix Down
Accumulate, Yell, Fury



ArchKnightX
Male
Scorpio
75
73
Knight
Black Magic
Dragon Spirit
Secret Hunt
Fly

Defender
Gold Shield
Diamond Helmet
Silk Robe
Germinas Boots

Armor Break, Shield Break, Power Break, Mind Break, Night Sword
Ice, Frog, Flare

Final Bets: red - 9 bets for 6,134G (50.0%, x1.00); yellow - 11 bets for 6,129G (50.0%, x1.00)

red bets:
fattunaking: 3,000G (48.9%, 12,070G)
Mesmaster: 1,000G (16.3%, 66,181G)
prince_rogers_nelson_: 696G (11.3%, 696G)
gorgewall: 508G (8.3%, 508G)
douchetron: 420G (6.8%, 825G)
Calajo: 248G (4.0%, 248G)
gooblz: 116G (1.9%, 116G)
Arkreaver: 100G (1.6%, 17,174G)
dtrain332: 46G (0.7%, 4,670G)

yellow bets:
ColetteMSLP: 1,000G (16.3%, 24,473G)
amiture: 1,000G (16.3%, 36,127G)
evontno: 1,000G (16.3%, 18,130G)
josephiroth_143: 1,000G (16.3%, 6,756G)
superdevon1: 724G (11.8%, 36,221G)
BirbBrainsBot: 333G (5.4%, 60,083G)
Wooplestein: 250G (4.1%, 1,376G)
daveb_: 222G (3.6%, 4,018G)
AllInBot: 200G (3.3%, 200G)
getthemoneyz: 200G (3.3%, 1,449,884G)
datadrivenbot: 200G (3.3%, 46,022G)

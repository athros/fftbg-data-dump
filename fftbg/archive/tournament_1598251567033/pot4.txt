Final Bets: purple - 11 bets for 11,750G (78.4%, x0.28); brown - 6 bets for 3,242G (21.6%, x3.62)

purple bets:
reinoe: 5,000G (42.6%, 61,453G)
NicoSavoy: 2,000G (17.0%, 23,623G)
superdevon1: 1,077G (9.2%, 107,740G)
ruleof5: 1,000G (8.5%, 19,441G)
BirbBrainsBot: 1,000G (8.5%, 195,919G)
CT_5_Holy: 592G (5.0%, 6,893G)
lowlf: 380G (3.2%, 8,049G)
iBardic: 300G (2.6%, 2,044G)
datadrivenbot: 200G (1.7%, 67,894G)
gorgewall: 101G (0.9%, 20,626G)
RonaldoTheGypsy: 100G (0.9%, 578G)

brown bets:
DaveStrider55: 1,242G (38.3%, 1,242G)
Seaweed_B: 1,000G (30.8%, 48,901G)
evdoggity: 576G (17.8%, 232,754G)
getthemoneyz: 224G (6.9%, 1,731,987G)
AllInBot: 100G (3.1%, 100G)
Lifebregin: 100G (3.1%, 14,000G)

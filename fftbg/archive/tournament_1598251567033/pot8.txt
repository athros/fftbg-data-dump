Final Bets: purple - 14 bets for 8,828G (65.7%, x0.52); champion - 8 bets for 4,608G (34.3%, x1.92)

purple bets:
NicoSavoy: 2,000G (22.7%, 24,175G)
superdevon1: 1,050G (11.9%, 105,057G)
Theseawolf1: 1,000G (11.3%, 11,742G)
BirbBrainsBot: 1,000G (11.3%, 197,108G)
DaveStrider55: 963G (10.9%, 963G)
getthemoneyz: 644G (7.3%, 1,731,920G)
nok____: 500G (5.7%, 5,500G)
Axciom: 479G (5.4%, 479G)
iBardic: 308G (3.5%, 308G)
ar_tactic: 250G (2.8%, 80,816G)
Lifebregin: 233G (2.6%, 17,233G)
datadrivenbot: 200G (2.3%, 67,859G)
gorgewall: 101G (1.1%, 20,609G)
RonaldoTheGypsy: 100G (1.1%, 630G)

champion bets:
EnemyController: 1,111G (24.1%, 1,565,892G)
sinnyil2: 1,016G (22.0%, 1,016G)
Seaweed_B: 1,000G (21.7%, 46,901G)
SkylerBunny: 677G (14.7%, 677G)
lowlf: 384G (8.3%, 8,719G)
vorap: 216G (4.7%, 216G)
FoxMime: 104G (2.3%, 104G)
AllInBot: 100G (2.2%, 100G)

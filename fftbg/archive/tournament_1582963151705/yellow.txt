Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Turtlekyng
Male
Taurus
59
50
Wizard
Basic Skill
Parry
Beastmaster
Jump+3

Ice Rod

Green Beret
Light Robe
Elf Mantle

Fire, Bolt, Bolt 2, Ice 3, Frog
Accumulate, Dash, Heal, Cheer Up



YaBoy125
Male
Sagittarius
70
54
Knight
Black Magic
Regenerator
Beastmaster
Jump+2

Defender

Platinum Helmet
Black Robe
Magic Ring

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Justice Sword, Night Sword
Fire, Bolt 3, Frog



Upvla
Male
Scorpio
78
46
Monk
Steal
Auto Potion
Dual Wield
Move-HP Up



Flash Hat
Clothes
Magic Gauntlet

Wave Fist, Secret Fist, Purification, Chakra, Revive
Steal Helmet, Steal Armor, Steal Accessory, Leg Aim



Wonderkush
Male
Gemini
46
69
Geomancer
Sing
Meatbone Slash
Sicken
Ignore Height

Slasher
Gold Shield
Holy Miter
Power Sleeve
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Cheer Song, Magic Song

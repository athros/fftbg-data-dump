Player: !Black
Team: Black Team
Palettes: Black/Red



Byrdturbo
Male
Capricorn
72
68
Mediator
Elemental
Catch
Short Charge
Retreat

Stone Gun

Barette
Light Robe
Defense Armlet

Praise, Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon
Pitfall, Water Ball, Static Shock, Sand Storm, Blizzard, Lava Ball



NovaKnight21
Monster
Sagittarius
62
38
Pisco Demon










Lyner87
Male
Serpentarius
68
46
Time Mage
Elemental
Damage Split
Beastmaster
Move+2

Rainbow Staff

Red Hood
Black Robe
Magic Gauntlet

Haste, Quick, Stabilize Time
Pitfall, Hell Ivy, Local Quake, Static Shock, Gusty Wind



Jethrothrul
Male
Serpentarius
53
70
Squire
Charge
Dragon Spirit
Maintenance
Waterbreathing

Hunting Bow
Diamond Shield
Green Beret
Crystal Mail
Battle Boots

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Wish
Charge+1, Charge+2, Charge+5, Charge+7

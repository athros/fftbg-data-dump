Player: !Red
Team: Red Team
Palettes: Red/Brown



Synoda
Female
Cancer
42
59
Priest
Steal
Parry
Martial Arts
Retreat



Ribbon
Black Robe
Magic Gauntlet

Cure, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna, Magic Barrier
Gil Taking, Steal Armor, Steal Accessory, Steal Status



Laserman1000
Male
Libra
62
44
Wizard
Punch Art
MP Restore
Halve MP
Fly

Rod

Barette
Judo Outfit
Reflect Ring

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Death, Flare
Secret Fist, Purification



Numbersborne
Female
Aries
76
61
Knight
Basic Skill
Parry
Beastmaster
Retreat

Defender
Escutcheon
Diamond Helmet
Crystal Mail
Vanish Mantle

Head Break, Armor Break, Weapon Break, Stasis Sword, Night Sword
Accumulate, Wish



The Pengwin
Monster
Aquarius
59
55
Minotaur








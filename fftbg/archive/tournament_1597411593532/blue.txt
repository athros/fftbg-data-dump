Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



JumbocactuarX27
Female
Taurus
65
59
Geomancer
Talk Skill
Auto Potion
Equip Axe
Move+2

Wizard Staff
Crystal Shield
Feather Hat
Judo Outfit
Wizard Mantle

Pitfall, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Persuade, Preach, Death Sentence, Refute, Rehabilitate



Nhammen
Male
Aries
79
75
Knight
Steal
Counter Magic
Defend
Waterwalking

Rune Blade
Bronze Shield
Diamond Helmet
Bronze Armor
Germinas Boots

Armor Break, Shield Break, Mind Break
Steal Armor, Steal Shield



Forkmore
Male
Gemini
46
68
Calculator
Limit
Critical Quick
Equip Axe
Move+1

Gold Staff
Platinum Shield
Feather Hat
Platinum Armor
Red Shoes

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



StealthModeLocke
Male
Capricorn
43
49
Monk
Draw Out
Parry
Magic Attack UP
Swim



Leather Hat
Brigandine
Magic Ring

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra
Kiyomori, Muramasa

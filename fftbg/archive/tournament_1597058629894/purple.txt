Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SeniorBunk
Female
Taurus
61
62
Time Mage
Elemental
Regenerator
Equip Shield
Swim

Rainbow Staff
Gold Shield
Flash Hat
Chameleon Robe
Rubber Shoes

Haste, Haste 2, Float, Quick, Demi, Demi 2, Stabilize Time, Meteor
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Chuckolator
Male
Cancer
65
42
Oracle
Steal
Blade Grasp
Equip Shield
Ignore Terrain

Iron Fan
Round Shield
Leather Hat
Silk Robe
108 Gems

Poison, Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Dark Holy
Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Leg Aim



DesertWooder
Male
Pisces
79
64
Bard
Elemental
Counter
Long Status
Teleport

Bloody Strings

Black Hood
Clothes
Battle Boots

Magic Song, Diamond Blade, Sky Demon
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Bartholomeo
Male
Serpentarius
50
52
Squire
Item
Catch
Magic Defense UP
Move-HP Up

Blind Knife
Flame Shield
Triangle Hat
Gold Armor
Rubber Shoes

Throw Stone, Heal, Tickle, Yell, Wish
Potion, X-Potion, Ether, Soft, Holy Water, Remedy, Phoenix Down

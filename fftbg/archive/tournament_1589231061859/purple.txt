Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CorpusCav
Female
Capricorn
57
65
Chemist
Yin Yang Magic
Speed Save
Doublehand
Lava Walking

Hydra Bag

Leather Hat
Earth Clothes
Angel Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Blind, Spell Absorb, Zombie, Silence Song, Dispel Magic, Dark Holy



Luminarii
Male
Scorpio
65
55
Monk
Yin Yang Magic
Arrow Guard
Equip Knife
Teleport

Rod

Black Hood
Earth Clothes
Battle Boots

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Poison, Spell Absorb, Foxbird, Confusion Song, Dispel Magic, Paralyze



ExecutedGiraffe
Female
Aquarius
58
72
Monk
Elemental
HP Restore
Beastmaster
Move-HP Up



Green Beret
Mythril Vest
Magic Ring

Earth Slash, Secret Fist, Purification, Revive
Pitfall, Hell Ivy, Blizzard, Gusty Wind, Lava Ball



Digitalsocrates
Female
Gemini
75
60
Squire
Dance
Speed Save
Maintenance
Move+3

Hunting Bow
Bronze Shield
Black Hood
Clothes
Rubber Shoes

Throw Stone, Heal
Slow Dance, Nameless Dance, Last Dance, Dragon Pit

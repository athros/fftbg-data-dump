Player: !Black
Team: Black Team
Palettes: Black/Red



OpHendoslice
Male
Cancer
72
62
Archer
Steal
Caution
Doublehand
Jump+3

Lightning Bow

Black Hood
Brigandine
Power Wrist

Charge+1, Charge+20
Gil Taking, Steal Status, Arm Aim



Galkife
Male
Gemini
69
51
Calculator
White Magic
PA Save
Concentrate
Ignore Terrain

Poison Rod

Leather Hat
Brigandine
Wizard Mantle

CT, 4, 3
Regen, Shell 2, Wall, Esuna



Sairentozon7
Male
Aries
52
55
Oracle
Punch Art
Auto Potion
Equip Axe
Move-HP Up

Rainbow Staff

Flash Hat
Wizard Robe
Wizard Mantle

Spell Absorb, Life Drain, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify, Dark Holy
Pummel, Earth Slash, Purification



Moocaotao
Male
Taurus
76
60
Oracle
Draw Out
Parry
Magic Attack UP
Fly

Papyrus Codex

Triangle Hat
Black Robe
Germinas Boots

Blind, Pray Faith, Doubt Faith, Silence Song, Dispel Magic
Bizen Boat

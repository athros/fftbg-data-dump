Player: !Green
Team: Green Team
Palettes: Green/White



Technominari
Female
Libra
53
50
Priest
Item
Distribute
Short Charge
Ignore Terrain

Flail

Triangle Hat
Wizard Outfit
Power Wrist

Cure 3, Reraise, Protect, Esuna
Potion, Hi-Potion, Ether, Remedy, Phoenix Down



LordTomS
Male
Aries
59
58
Thief
Summon Magic
Catch
Defense UP
Ignore Height

Air Knife

Twist Headband
Adaman Vest
108 Gems

Steal Heart, Steal Weapon, Steal Accessory, Arm Aim
Shiva, Ifrit, Bahamut, Lich, Cyclops



Zenlion
Male
Cancer
68
61
Summoner
Jump
Auto Potion
Equip Polearm
Move-HP Up

Octagon Rod

Feather Hat
Wizard Outfit
Red Shoes

Moogle, Bahamut, Silf, Zodiac
Level Jump8, Vertical Jump2



DeathTaxesAndAnime
Female
Scorpio
55
70
Mime

Counter
Monster Talk
Levitate



Flash Hat
Mystic Vest
Rubber Shoes

Mimic


Final Bets: green - 7 bets for 5,880G (80.8%, x0.24); purple - 5 bets for 1,401G (19.2%, x4.20)

green bets:
Arcblazer23: 2,898G (49.3%, 5,796G)
BirbBrainsBot: 880G (15.0%, 130,878G)
getthemoneyz: 602G (10.2%, 1,156,695G)
SaltiestMage: 500G (8.5%, 2,739G)
salted_cliff_racer: 500G (8.5%, 800G)
twelfthrootoftwo: 300G (5.1%, 11,929G)
Evewho: 200G (3.4%, 20,854G)

purple bets:
Dragon_RW: 500G (35.7%, 3,097G)
ColetteMSLP: 401G (28.6%, 4,566G)
Digitalsocrates: 200G (14.3%, 2,489G)
CT_5_Holy: 200G (14.3%, 3,399G)
datadrivenbot: 100G (7.1%, 51,942G)

Player: !Black
Team: Black Team
Palettes: Black/Red



RaIshtar
Male
Virgo
50
71
Archer
Elemental
Arrow Guard
Doublehand
Teleport

Mythril Gun

Green Beret
Power Sleeve
108 Gems

Charge+2, Charge+3, Charge+7
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Lava Ball



Nekojin
Female
Gemini
48
51
Knight
Item
Brave Save
Short Charge
Waterbreathing

Ice Brand
Hero Shield
Gold Helmet
Leather Armor
Magic Gauntlet

Armor Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Dark Sword
Potion, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down



Coralreeferz
Female
Libra
71
80
Samurai
Summon Magic
MP Restore
Equip Gun
Lava Walking

Glacier Gun

Genji Helmet
Linen Robe
Genji Gauntlet

Bizen Boat, Muramasa, Kikuichimoji
Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Fairy



Lydian C
Female
Pisces
60
61
Geomancer
Battle Skill
Earplug
Defend
Teleport

Giant Axe
Platinum Shield
Leather Hat
Robe of Lords
Bracer

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard
Head Break, Shield Break, Speed Break, Power Break, Justice Sword, Dark Sword, Explosion Sword

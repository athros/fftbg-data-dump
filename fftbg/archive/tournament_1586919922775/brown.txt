Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Mirapoix
Female
Cancer
54
53
Monk
Jump
HP Restore
Equip Knife
Ignore Terrain

Ninja Edge

Headgear
Earth Clothes
Genji Gauntlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive
Level Jump4, Vertical Jump5



Itzover9001
Female
Cancer
44
58
Time Mage
Draw Out
Critical Quick
Defend
Levitate

Bestiary

Red Hood
Mystic Vest
Dracula Mantle

Haste, Slow, Reflect, Demi 2
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



RunicMagus
Female
Taurus
54
72
Monk
Steal
Speed Save
Long Status
Move+2

Star Bag

Golden Hairpin
Clothes
Reflect Ring

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Steal Heart, Steal Armor



Alacor
Male
Scorpio
43
46
Knight
Jump
Abandon
Attack UP
Waterbreathing

Defender
Bronze Shield
Bronze Helmet
Leather Armor
Magic Ring

Head Break, Weapon Break, Speed Break
Level Jump8, Vertical Jump3

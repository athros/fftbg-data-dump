Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Gorgewall
Male
Scorpio
70
79
Ninja
Jump
Meatbone Slash
Equip Knife
Ignore Terrain

Poison Rod
Faith Rod
Triangle Hat
Mystic Vest
Dracula Mantle

Shuriken, Sword, Wand, Dictionary
Level Jump8, Vertical Jump2



KyleWonToLiveForever
Female
Taurus
53
80
Ninja
Talk Skill
Parry
Monster Talk
Move-HP Up

Mythril Knife
Kunai
Headgear
Brigandine
Defense Ring

Knife
Invitation, Preach, Solution, Death Sentence, Refute



Serperemagus
Male
Aquarius
54
76
Lancer
Summon Magic
Auto Potion
Equip Bow
Jump+3

Silver Bow

Platinum Helmet
Light Robe
Cursed Ring

Level Jump5, Vertical Jump8
Moogle, Shiva, Ifrit, Leviathan



Vorap
Male
Taurus
69
59
Knight
Elemental
MP Restore
Martial Arts
Jump+3


Flame Shield
Platinum Helmet
Bronze Armor
Magic Gauntlet

Head Break, Magic Break, Speed Break, Power Break
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard

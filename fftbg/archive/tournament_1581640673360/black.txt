Player: !Black
Team: Black Team
Palettes: Black/Red



Black Sheep 0213
Male
Scorpio
47
70
Mime

Abandon
Martial Arts
Swim



Leather Hat
Black Costume
Small Mantle

Mimic




Terofyin
Male
Sagittarius
48
76
Lancer
Throw
HP Restore
Equip Polearm
Jump+3

Spear
Bronze Shield
Barbuta
Genji Armor
Small Mantle

Level Jump2, Vertical Jump2
Shuriken



Meta Five
Female
Sagittarius
44
75
Dancer
Summon Magic
Counter Flood
Concentrate
Move+2

Ryozan Silk

Green Beret
Light Robe
Jade Armlet

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka
Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Odin, Salamander



BenYuPoker
Male
Aquarius
56
77
Ninja
Basic Skill
Faith Up
Equip Gun
Jump+2

Bloody Strings
Bestiary
Thief Hat
Clothes
Power Wrist

Bomb
Dash, Heal, Wish

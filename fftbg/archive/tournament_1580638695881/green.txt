Player: !Green
Team: Green Team
Palettes: Green/White



Tresbienhien
Female
Cancer
52
78
Wizard
Draw Out
Counter Magic
Equip Gun
Lava Walking

Dragon Rod

Ribbon
Light Robe
Sprint Shoes

Fire, Bolt 2, Ice, Death
Heaven's Cloud



LAGBOT30000
Female
Taurus
43
77
Samurai
Item
Earplug
Secret Hunt
Ignore Height

Muramasa

Iron Helmet
Gold Armor
Sprint Shoes

Asura, Koutetsu, Bizen Boat, Kikuichimoji
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down



Willjin
Monster
Libra
63
77
Cockatrice










HV5H
Female
Sagittarius
40
46
Wizard
Draw Out
Counter
Maintenance
Ignore Terrain

Flame Rod

Thief Hat
Wizard Robe
Leather Mantle

Fire, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 3, Ice 4, Frog, Death
Kiyomori, Muramasa

Player: !White
Team: White Team
Palettes: White/Blue



TinchoT
Female
Libra
46
61
Wizard
Basic Skill
Regenerator
Defend
Jump+1

Flame Rod

Green Beret
Black Robe
Wizard Mantle

Bolt 3, Bolt 4, Ice, Ice 2, Ice 4, Death
Dash, Throw Stone, Heal, Cheer Up, Scream



Moocaotao
Monster
Libra
69
71
Pisco Demon










ExecutedGiraffe
Female
Sagittarius
59
58
Mime

Auto Potion
Martial Arts
Retreat



Flash Hat
Adaman Vest
Genji Gauntlet

Mimic




HaateXIII
Male
Scorpio
73
62
Lancer
Punch Art
Catch
Attack UP
Jump+2

Dragon Whisker
Gold Shield
Barbuta
Wizard Robe
Wizard Mantle

Level Jump3, Vertical Jump8
Earth Slash, Secret Fist, Chakra, Revive, Seal Evil

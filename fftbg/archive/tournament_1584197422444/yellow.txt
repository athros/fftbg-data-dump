Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Omegasuspekt
Female
Libra
42
56
Time Mage
Draw Out
MA Save
Halve MP
Fly

Battle Folio

Twist Headband
Linen Robe
Feather Mantle

Haste, Slow, Slow 2, Immobilize, Demi, Meteor
Asura



Pandasforsale
Male
Aries
55
75
Priest
Basic Skill
Counter Tackle
Equip Sword
Fly

Mythril Sword

Twist Headband
Mystic Vest
Power Wrist

Cure 3, Raise, Raise 2, Regen, Protect 2, Shell, Wall, Esuna
Heal, Yell



Draconis345
Monster
Taurus
50
51
Ultima Demon










HaplessOne
Female
Aquarius
71
70
Samurai
Time Magic
Arrow Guard
Long Status
Teleport

Heaven's Cloud

Bronze Helmet
Leather Armor
Magic Gauntlet

Murasame, Heaven's Cloud, Muramasa
Haste, Haste 2, Slow, Slow 2, Stop, Quick, Stabilize Time

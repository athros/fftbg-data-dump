Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Mtueni
Male
Taurus
72
60
Mediator
Steal
HP Restore
Equip Armor
Waterbreathing

Glacier Gun

Mythril Helmet
Gold Armor
Battle Boots

Invitation, Persuade, Threaten, Preach, Negotiate, Mimic Daravon
Steal Status



Imafella
Monster
Aries
69
66
Wild Boar










LDSkinny
Female
Capricorn
78
57
Oracle
Item
MA Save
Equip Shield
Move+3

Musk Rod
Venetian Shield
Black Hood
Mythril Vest
Chantage

Poison, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Hi-Potion, Holy Water



DeathTaxesAndAnime
Female
Libra
75
46
Samurai
Time Magic
Regenerator
Beastmaster
Move+1

Kikuichimoji

Leather Helmet
Black Robe
Magic Gauntlet

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji
Haste, Haste 2, Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time

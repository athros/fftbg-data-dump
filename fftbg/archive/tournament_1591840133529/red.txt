Player: !Red
Team: Red Team
Palettes: Red/Brown



DLJuggernaut
Male
Aquarius
76
40
Ninja
White Magic
Caution
Martial Arts
Waterbreathing



Leather Hat
Black Costume
Defense Armlet

Wand
Cure 2, Cure 4, Raise, Raise 2, Regen, Protect, Esuna



Gooseyourself
Female
Taurus
51
62
Calculator
Yin Yang Magic
MA Save
Magic Attack UP
Jump+3

Battle Folio

Twist Headband
Wizard Robe
Genji Gauntlet

CT, Height, 5, 4
Spell Absorb, Doubt Faith, Foxbird



OneHundredFists
Monster
Libra
59
65
Minotaur










NoNotBees
Male
Libra
41
78
Knight
Draw Out
Hamedo
Equip Armor
Move-HP Up

Defender
Diamond Shield
Diamond Helmet
Earth Clothes
Bracer

Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Dark Sword
Asura, Muramasa

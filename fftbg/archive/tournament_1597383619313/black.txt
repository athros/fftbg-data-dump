Player: !Black
Team: Black Team
Palettes: Black/Red



Evdoggity
Female
Pisces
45
54
Geomancer
Basic Skill
Sunken State
Short Status
Lava Walking

Slasher
Escutcheon
Green Beret
Mystic Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Accumulate, Throw Stone, Heal, Fury, Wish



NovaKnight21
Male
Cancer
68
50
Summoner
White Magic
Parry
Maintenance
Move+2

Rod

Feather Hat
Earth Clothes
Genji Gauntlet

Shiva, Golem, Carbunkle, Salamander, Silf, Lich
Cure, Cure 2, Raise, Reraise, Regen, Protect 2, Shell, Shell 2, Esuna



Starfire6789
Female
Libra
65
48
Calculator
Black Magic
Catch
Doublehand
Jump+3

Poison Rod

Red Hood
Wizard Outfit
Sprint Shoes

CT, Height, Prime Number, 5, 4, 3
Fire 2, Fire 4, Ice 3, Ice 4, Frog



Flacococo
Female
Virgo
65
72
Calculator
Time Magic
Critical Quick
Equip Sword
Move+1

Save the Queen

Black Hood
Chameleon Robe
Defense Ring

CT, Height, Prime Number, 3
Haste, Haste 2, Slow, Quick, Demi 2

Final Bets: white - 18 bets for 22,485G (82.9%, x0.21); brown - 11 bets for 4,652G (17.1%, x4.83)

white bets:
DudeMonkey77: 4,432G (19.7%, 4,432G)
VolgraTheMoose: 4,424G (19.7%, 8,848G)
BoneMiser: 4,415G (19.6%, 4,415G)
HaateXIII: 3,961G (17.6%, 7,923G)
mpghappiness: 1,000G (4.4%, 3,714G)
Draconis345: 1,000G (4.4%, 50,013G)
letdowncity: 500G (2.2%, 19,591G)
joewcarson: 500G (2.2%, 32,565G)
sSwing: 498G (2.2%, 6,330G)
ThePineappleSalesman: 400G (1.8%, 400G)
Sans_from_Snowdin: 264G (1.2%, 264G)
Anak761: 250G (1.1%, 8,954G)
Belkra: 240G (1.1%, 240G)
Aldrammech: 200G (0.9%, 804G)
gorgewall: 101G (0.4%, 949G)
Grandlanzer: 100G (0.4%, 2,200G)
outer_monologue: 100G (0.4%, 1,979G)
datadrivenbot: 100G (0.4%, 35,631G)

brown bets:
BirbBrainsBot: 1,000G (21.5%, 163,775G)
red__lancer: 1,000G (21.5%, 87,866G)
superdevon1: 648G (13.9%, 6,483G)
flowinprose: 500G (10.7%, 103,233G)
powergems: 416G (8.9%, 416G)
sinnyil2: 300G (6.4%, 4,489G)
Thyrandaal: 244G (5.2%, 244G)
DejaPoo21: 200G (4.3%, 8,315G)
getthemoneyz: 136G (2.9%, 843,417G)
Spuzzmocker: 108G (2.3%, 108G)
Celdia: 100G (2.1%, 4,662G)

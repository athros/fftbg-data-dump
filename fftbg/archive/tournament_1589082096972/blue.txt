Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Tithonus
Monster
Libra
47
64
Ultima Demon










Mayfu
Male
Virgo
81
70
Chemist
Basic Skill
Critical Quick
Halve MP
Ignore Height

Star Bag

Golden Hairpin
Mystic Vest
Battle Boots

Potion, Hi-Potion, Ether, Hi-Ether, Elixir, Soft, Holy Water, Phoenix Down
Accumulate, Dash, Throw Stone, Heal, Tickle, Scream



Dexsana
Male
Cancer
71
77
Lancer
Punch Art
Meatbone Slash
Secret Hunt
Lava Walking

Battle Bamboo
Gold Shield
Leather Helmet
Crystal Mail
Defense Armlet

Level Jump2, Vertical Jump6
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



Ungaiii
Female
Pisces
74
46
Archer
White Magic
Faith Up
Magic Attack UP
Move+1

Ice Bow

Golden Hairpin
Earth Clothes
Jade Armlet

Charge+1, Charge+4, Charge+7, Charge+10
Cure, Raise, Reraise, Protect, Protect 2, Esuna

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Daveb
Male
Aries
50
51
Bard
Item
Damage Split
Short Status
Jump+1

Bloody Strings

Flash Hat
Leather Armor
Battle Boots

Life Song, Cheer Song, Battle Song, Space Storage, Hydra Pit
Potion, X-Potion, Soft, Holy Water, Remedy, Phoenix Down



Ggbroccoli
Male
Cancer
77
41
Lancer
Basic Skill
Regenerator
Maintenance
Move-HP Up

Spear
Genji Shield
Barbuta
Linen Robe
N-Kai Armlet

Level Jump5, Vertical Jump7
Throw Stone, Tickle, Scream



Zmobie
Female
Pisces
79
74
Mime

Absorb Used MP
Maintenance
Retreat



Bronze Helmet
Leather Outfit
Leather Mantle

Mimic




InkyLink
Male
Capricorn
52
40
Lancer
Punch Art
Counter Magic
Equip Gun
Lava Walking

Bloody Strings
Flame Shield
Barbuta
Crystal Mail
Sprint Shoes

Level Jump5, Vertical Jump7
Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive

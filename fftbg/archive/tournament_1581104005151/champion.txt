Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Strawberry Panic
Male
Cancer
51
73
Bard
Jump
Speed Save
Defense UP
Move-HP Up

Fairy Harp

Flash Hat
Brigandine
Rubber Shoes

Life Song, Sky Demon
Level Jump2, Vertical Jump8



AniZero
Male
Aries
79
80
Monk
Basic Skill
Blade Grasp
Attack UP
Jump+3



Red Hood
Leather Outfit
Sprint Shoes

Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil
Accumulate, Dash, Throw Stone, Heal



WeepinbeII
Male
Taurus
58
45
Ninja
Time Magic
Parry
Beastmaster
Jump+1

Kunai
Short Edge
Twist Headband
Black Costume
Jade Armlet

Shuriken, Bomb, Stick
Immobilize, Demi, Stabilize Time



Rikimaruhitori
Male
Pisces
38
63
Wizard
Elemental
Critical Quick
Beastmaster
Move-MP Up

Zorlin Shape

Triangle Hat
Secret Clothes
Genji Gauntlet

Fire 2, Bolt 2, Frog, Death
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ring Wyrm
Female
Capricorn
72
77
Ninja
Jump
Speed Save
Magic Defense UP
Levitate

Air Knife
Spell Edge
Black Hood
Wizard Outfit
Red Shoes

Shuriken, Bomb, Staff
Level Jump4, Vertical Jump3



Grandlanzer
Female
Scorpio
52
52
Thief
Item
Catch
Sicken
Fly

Assassin Dagger

Golden Hairpin
Chain Vest
Rubber Shoes

Steal Weapon, Steal Accessory, Arm Aim
Hi-Potion, X-Potion, Echo Grass, Soft



Tehjam
Male
Virgo
61
77
Lancer
Draw Out
Sunken State
Equip Polearm
Move+3

Octagon Rod
Platinum Shield
Mythril Helmet
Chameleon Robe
Feather Mantle

Level Jump8, Vertical Jump5
Murasame, Heaven's Cloud, Muramasa, Kikuichimoji



Gorgewall
Female
Scorpio
61
59
Thief
Basic Skill
Caution
Halve MP
Levitate

Diamond Sword

Feather Hat
Black Costume
Spike Shoes

Steal Heart, Steal Armor, Leg Aim
Dash, Throw Stone, Fury, Wish

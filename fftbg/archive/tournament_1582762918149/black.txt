Player: !Black
Team: Black Team
Palettes: Black/Red



Smegma Sorcerer
Male
Gemini
77
46
Summoner
White Magic
MA Save
Short Status
Move-HP Up

Dragon Rod

Black Hood
Chameleon Robe
Small Mantle

Moogle, Carbunkle, Bahamut, Fairy, Lich
Raise, Shell, Wall, Esuna



EizanTayama
Female
Aquarius
49
43
Thief
Elemental
Counter Tackle
Concentrate
Lava Walking

Cultist Dagger

Holy Miter
Brigandine
Dracula Mantle

Steal Shield
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Coledot
Female
Aries
47
79
Geomancer
Draw Out
Dragon Spirit
Sicken
Move-HP Up

Giant Axe
Round Shield
Red Hood
Black Robe
Dracula Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bizen Boat



FroDog5050
Female
Aries
52
50
Geomancer
Punch Art
Sunken State
Beastmaster
Swim

Battle Axe
Buckler
Flash Hat
Adaman Vest
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind, Lava Ball
Pummel, Secret Fist, Purification, Chakra, Revive

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HuffFlex
Female
Gemini
51
55
Lancer
Talk Skill
Sunken State
Monster Talk
Retreat

Spear
Flame Shield
Platinum Helmet
Linen Robe
Rubber Shoes

Level Jump8, Vertical Jump8
Solution, Insult, Negotiate, Rehabilitate



Seaweed B
Male
Scorpio
71
50
Summoner
Battle Skill
Brave Save
Short Charge
Move-HP Up

Battle Folio

Leather Hat
Adaman Vest
Genji Gauntlet

Moogle, Shiva, Carbunkle, Odin, Silf, Fairy
Head Break, Weapon Break, Night Sword



Roofiepops
Female
Pisces
57
51
Knight
Yin Yang Magic
Blade Grasp
Defense UP
Move+3

Blood Sword
Crystal Shield
Platinum Helmet
Silk Robe
Cursed Ring

Shield Break, Night Sword
Spell Absorb, Zombie, Silence Song, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy



Nifboy
Female
Taurus
57
77
Mediator
Jump
Speed Save
Short Status
Waterwalking

Stone Gun

Thief Hat
Wizard Robe
Diamond Armlet

Invitation, Preach, Solution, Refute
Level Jump2, Vertical Jump7

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Maximumcrit
Female
Cancer
66
80
Calculator
Mighty Sword
Counter Tackle
Equip Gun
Retreat

Papyrus Codex

Headgear
Light Robe
Sprint Shoes

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



Thyrandaal
Male
Libra
82
78
Archer
Draw Out
Counter
Dual Wield
Retreat

Hunting Bow
Poison Bow
Headgear
Chain Vest
Feather Mantle

Charge+3, Charge+10
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Gunz232323
Male
Aries
64
63
Summoner
Charge
Distribute
Dual Wield
Jump+1

Papyrus Codex
Battle Folio
Golden Hairpin
Black Robe
Spike Shoes

Shiva, Titan, Carbunkle, Salamander
Charge+1, Charge+2, Charge+4, Charge+20



Helllyesss
Male
Serpentarius
63
76
Thief
Jump
Brave Save
Attack UP
Levitate

Cultist Dagger

Golden Hairpin
Black Costume
Red Shoes

Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Level Jump3, Vertical Jump2

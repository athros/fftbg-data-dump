Player: !Black
Team: Black Team
Palettes: Black/Red



Valdimar 5467
Female
Sagittarius
47
57
Knight
Basic Skill
HP Restore
Sicken
Teleport

Defender
Mythril Shield
Gold Helmet
Linen Robe
108 Gems

Head Break, Armor Break, Magic Break, Surging Sword
Accumulate, Dash, Throw Stone, Heal, Yell, Cheer Up



ANFz
Male
Cancer
45
54
Thief
Item
Abandon
Sicken
Move-MP Up

Cultist Dagger

Headgear
Power Sleeve
Power Wrist

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Arm Aim
Antidote, Holy Water, Phoenix Down



Butterbelljedi
Male
Aries
65
49
Archer
Basic Skill
Dragon Spirit
Defense UP
Move+1

Blast Gun
Ice Shield
Headgear
Chain Vest
Chantage

Charge+4
Dash, Throw Stone, Heal, Cheer Up



Grandlanzer
Male
Pisces
73
50
Wizard
Talk Skill
Mana Shield
Monster Talk
Move+1

Assassin Dagger

Holy Miter
Judo Outfit
Small Mantle

Fire 4, Bolt, Bolt 4, Ice 3, Empower
Threaten, Refute

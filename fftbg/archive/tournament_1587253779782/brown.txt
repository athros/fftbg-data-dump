Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Joewcarson
Male
Capricorn
52
68
Lancer
Elemental
Auto Potion
Dual Wield
Fly

Gungnir
Mythril Spear
Iron Helmet
Chain Mail
Reflect Ring

Level Jump2, Vertical Jump7
Pitfall, Water Ball, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Galkife
Male
Aquarius
52
63
Archer
Summon Magic
HP Restore
Beastmaster
Waterbreathing

Glacier Gun
Gold Shield
Twist Headband
Brigandine
Wizard Mantle

Charge+2, Charge+7
Moogle, Ramuh, Golem, Odin, Silf, Fairy, Cyclops



Alacor
Male
Taurus
62
54
Knight
Summon Magic
Parry
Attack UP
Fly

Slasher
Diamond Shield
Genji Helmet
Linen Cuirass
Cursed Ring

Speed Break, Power Break, Mind Break
Titan, Golem, Carbunkle, Odin, Salamander, Fairy



Lanshaft
Male
Aquarius
67
79
Archer
Elemental
Brave Up
Concentrate
Swim

Stone Gun
Escutcheon
Genji Helmet
Rubber Costume
Diamond Armlet

Charge+2, Charge+3
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm

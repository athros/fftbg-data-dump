Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DrakeRowan
Male
Libra
50
76
Mime

Damage Split
Martial Arts
Teleport



Red Hood
Light Robe
Feather Mantle

Mimic




SteelVictim
Female
Libra
74
53
Archer
Basic Skill
Faith Up
Halve MP
Waterbreathing

Hunting Bow
Round Shield
Golden Hairpin
Chain Vest
Salty Rage

Charge+3, Charge+7
Heal, Tickle, Scream



BoneMiser
Female
Scorpio
79
45
Ninja
White Magic
Caution
Defend
Teleport

Sasuke Knife
Spell Edge
Barette
Chain Vest
Feather Mantle

Shuriken, Staff
Raise 2, Shell 2, Esuna



ZantetsukenX
Female
Taurus
41
80
Oracle
Black Magic
Auto Potion
Dual Wield
Move+3

Madlemgen
Bestiary
Flash Hat
Linen Robe
Small Mantle

Life Drain, Silence Song, Blind Rage, Foxbird, Paralyze
Fire 2

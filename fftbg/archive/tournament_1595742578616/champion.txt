Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Seaweed B
Female
Aquarius
70
48
Summoner
Draw Out
Brave Save
Concentrate
Move+2

Poison Rod

Red Hood
Adaman Vest
Reflect Ring

Moogle, Odin, Fairy
Heaven's Cloud



CT 5 Holy
Female
Capricorn
71
49
Squire
Time Magic
Absorb Used MP
Sicken
Jump+2

Dagger
Genji Shield
Crystal Helmet
Diamond Armor
Magic Ring

Accumulate, Heal, Tickle
Haste, Haste 2, Slow, Stop, Immobilize, Float, Quick, Demi, Stabilize Time



Dogsandcatsand
Male
Scorpio
54
54
Ninja
Steal
Abandon
Equip Bow
Ignore Height

Lightning Bow

Triangle Hat
Secret Clothes
Cursed Ring

Bomb
Steal Heart, Steal Shield, Steal Weapon, Steal Status, Arm Aim



JDogg2K4
Male
Pisces
73
72
Calculator
Yin Yang Magic
Parry
Equip Armor
Jump+1

Bestiary

Leather Helmet
Leather Armor
Feather Mantle

CT, Height, 5, 4, 3
Blind, Poison, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Dark Holy

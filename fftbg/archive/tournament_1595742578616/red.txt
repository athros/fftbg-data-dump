Player: !Red
Team: Red Team
Palettes: Red/Brown



Just Here2
Male
Capricorn
54
69
Chemist
Sing
Mana Shield
Equip Armor
Swim

Orichalcum

Diamond Helmet
Wizard Robe
Germinas Boots

Echo Grass, Soft, Holy Water, Phoenix Down
Battle Song



CT 5 Holy
Male
Pisces
67
74
Knight
Time Magic
PA Save
Short Charge
Waterwalking

Excalibur
Venetian Shield
Iron Helmet
Reflect Mail
Diamond Armlet

Armor Break, Weapon Break, Speed Break, Mind Break
Haste, Haste 2, Stop, Immobilize, Demi, Demi 2, Stabilize Time



DamnThatShark
Male
Taurus
72
80
Monk
Yin Yang Magic
Auto Potion
Equip Bow
Ignore Terrain

Lightning Bow

Twist Headband
Mystic Vest
Magic Gauntlet

Spin Fist, Earth Slash, Chakra, Revive
Life Drain, Pray Faith, Foxbird, Confusion Song, Dispel Magic



Flacococo
Female
Capricorn
58
59
Geomancer
Yin Yang Magic
Faith Save
Maintenance
Jump+2

Giant Axe
Diamond Shield
Green Beret
Linen Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Blind, Spell Absorb, Blind Rage, Foxbird, Dispel Magic

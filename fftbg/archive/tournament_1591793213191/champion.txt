Player: !zChamp
Team: Champion Team
Palettes: White/Blue



NovaKnight21
Female
Gemini
79
62
Archer
Battle Skill
PA Save
Short Charge
Retreat

Yoichi Bow

Flash Hat
Power Sleeve
Genji Gauntlet

Charge+3, Charge+7, Charge+10
Head Break, Shield Break, Weapon Break, Speed Break, Dark Sword



Helpimabug
Male
Pisces
69
44
Monk
Basic Skill
PA Save
Dual Wield
Move-MP Up



Black Hood
Judo Outfit
Defense Armlet

Pummel, Earth Slash, Purification, Revive, Seal Evil
Accumulate, Heal



ColetteMSLP
Monster
Capricorn
48
56
King Behemoth










BlackFireUK
Male
Libra
73
56
Mediator
Draw Out
Counter
Dual Wield
Swim

Madlemgen
Papyrus Codex
Twist Headband
Chain Vest
Magic Gauntlet

Persuade, Praise, Preach, Insult, Refute
Murasame, Muramasa

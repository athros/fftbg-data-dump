Player: !White
Team: White Team
Palettes: White/Blue



Nosablake
Male
Capricorn
63
78
Archer
Basic Skill
Meatbone Slash
Doublehand
Swim

Poison Bow

Red Hood
Clothes
Diamond Armlet

Charge+1, Charge+3, Charge+4, Charge+7, Charge+20
Throw Stone, Heal



Lijarkh
Male
Taurus
65
38
Calculator
Black Magic
Counter
Halve MP
Teleport

Papyrus Codex

Leather Hat
Black Robe
Diamond Armlet

CT, Height, Prime Number, 4, 3
Fire 3, Bolt 3, Ice, Ice 2



HASTERIOUS
Male
Gemini
46
64
Lancer
Throw
Faith Up
Doublehand
Move+3

Mythril Spear

Circlet
Linen Cuirass
Leather Mantle

Level Jump8, Vertical Jump2
Shuriken, Bomb, Knife



Snowfats
Female
Gemini
54
67
Squire
Summon Magic
Critical Quick
Halve MP
Retreat

Slasher
Platinum Shield
Diamond Helmet
Bronze Armor
Dracula Mantle

Accumulate, Throw Stone, Scream
Moogle, Golem, Odin, Cyclops

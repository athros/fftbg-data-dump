Player: !Red
Team: Red Team
Palettes: Red/Brown



HaateXIII
Female
Libra
37
79
Summoner
Punch Art
Dragon Spirit
Short Charge
Move+3

Wizard Rod

Green Beret
Secret Clothes
Reflect Ring

Carbunkle, Odin, Fairy, Lich
Pummel, Purification, Chakra, Revive



Aldrammech
Female
Taurus
78
54
Dancer
Item
Regenerator
Secret Hunt
Retreat

Cashmere

Red Hood
Light Robe
Small Mantle

Slow Dance, Disillusion, Obsidian Blade, Void Storage, Dragon Pit
Ether, Eye Drop, Holy Water, Phoenix Down



J2DaBibbles
Female
Aries
64
59
Priest
Dance
Auto Potion
Short Charge
Jump+3

Healing Staff

Black Hood
Mythril Vest
Feather Boots

Cure 3, Raise, Regen, Shell, Shell 2, Wall
Slow Dance, Last Dance



StealthModeLocke
Male
Leo
48
64
Monk
Item
Dragon Spirit
Maintenance
Ignore Height



Leather Hat
Leather Outfit
Defense Ring

Spin Fist, Wave Fist, Secret Fist, Purification
Potion, Antidote, Maiden's Kiss, Soft, Remedy, Phoenix Down

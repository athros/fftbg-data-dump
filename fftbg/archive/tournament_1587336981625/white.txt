Player: !White
Team: White Team
Palettes: White/Blue



Legitimized
Female
Gemini
58
61
Thief
Jump
Parry
Martial Arts
Swim

Rune Blade

Twist Headband
Wizard Outfit
Wizard Mantle

Steal Armor, Steal Shield, Leg Aim
Level Jump4, Vertical Jump5



Sairentozon7
Female
Virgo
49
46
Mediator
Dance
MP Restore
Defend
Swim

Orichalcum

Thief Hat
Linen Robe
Leather Mantle

Invitation, Persuade, Praise, Insult, Refute, Rehabilitate
Witch Hunt, Slow Dance, Last Dance, Obsidian Blade, Void Storage



Maakur
Female
Gemini
58
39
Geomancer
Draw Out
Faith Up
Long Status
Teleport

Blood Sword
Mythril Shield
Flash Hat
Secret Clothes
Spike Shoes

Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Kikuichimoji, Masamune



Omegasuspekt
Male
Cancer
66
58
Time Mage
Battle Skill
Parry
Equip Polearm
Ignore Terrain

Mythril Spear

Green Beret
White Robe
Reflect Ring

Quick, Demi, Demi 2, Stabilize Time
Armor Break, Weapon Break, Dark Sword

Player: !White
Team: White Team
Palettes: White/Blue



CosmicTactician
Male
Libra
63
61
Summoner
Basic Skill
Sunken State
Secret Hunt
Move-HP Up

Poison Rod

Holy Miter
White Robe
Magic Gauntlet

Moogle, Titan, Carbunkle
Heal, Tickle, Yell, Fury



SSwing
Female
Aries
75
71
Summoner
Draw Out
MA Save
Equip Axe
Ignore Terrain

Battle Axe

Headgear
Chameleon Robe
Dracula Mantle

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Lich
Asura, Bizen Boat, Muramasa



Lowlf
Female
Libra
79
69
Calculator
Yin Yang Magic
Arrow Guard
Short Status
Jump+2

Battle Folio

Green Beret
Judo Outfit
Diamond Armlet

CT, Height, 4, 3
Blind, Poison, Spell Absorb, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic



Poulol
Male
Gemini
73
81
Archer
Elemental
Parry
Doublehand
Jump+2

Blaze Gun

Feather Hat
Earth Clothes
Jade Armlet

Charge+1, Charge+3, Charge+5, Charge+7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Lava Ball

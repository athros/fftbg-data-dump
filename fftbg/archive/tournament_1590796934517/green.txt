Player: !Green
Team: Green Team
Palettes: Green/White



Maakur
Male
Virgo
79
74
Lancer
Punch Art
Speed Save
Equip Polearm
Waterbreathing

Ryozan Silk
Gold Shield
Mythril Helmet
Gold Armor
Chantage

Level Jump8, Vertical Jump8
Pummel, Earth Slash, Secret Fist, Purification, Revive



Killth3kid
Monster
Aries
64
51
Squidraken










LeepingJJ
Female
Sagittarius
47
52
Samurai
Elemental
Regenerator
Equip Knife
Teleport

Short Edge

Leather Helmet
Leather Armor
Cursed Ring

Koutetsu, Bizen Boat, Murasame, Heaven's Cloud
Water Ball, Hell Ivy, Static Shock, Quicksand, Blizzard



TheMM42
Female
Libra
43
59
Oracle
Time Magic
Arrow Guard
Maintenance
Waterbreathing

Bestiary

Triangle Hat
White Robe
Red Shoes

Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Slow, Stop, Float, Quick, Demi, Stabilize Time

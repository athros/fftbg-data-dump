Player: !Black
Team: Black Team
Palettes: Black/Red



Kaidykat
Male
Virgo
39
61
Geomancer
Summon Magic
Mana Shield
Defense UP
Teleport

Sleep Sword
Diamond Shield
Golden Hairpin
Mythril Vest
Magic Gauntlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shiva, Ramuh, Ifrit, Titan, Odin, Leviathan, Salamander, Cyclops



Breakdown777
Male
Libra
69
75
Lancer
Item
Damage Split
Attack UP
Move-HP Up

Obelisk
Round Shield
Cross Helmet
Black Robe
Dracula Mantle

Level Jump2, Vertical Jump8
Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Kyune
Male
Pisces
63
43
Ninja
Yin Yang Magic
Counter Magic
Equip Gun
Retreat

Bestiary
Battle Folio
Red Hood
Secret Clothes
Red Shoes

Shuriken
Life Drain, Doubt Faith, Blind Rage, Confusion Song



Lionhermit
Male
Virgo
61
78
Lancer
Punch Art
Critical Quick
Magic Attack UP
Waterwalking

Obelisk
Platinum Shield
Platinum Helmet
Diamond Armor
Elf Mantle

Level Jump3, Vertical Jump7
Spin Fist, Secret Fist, Revive

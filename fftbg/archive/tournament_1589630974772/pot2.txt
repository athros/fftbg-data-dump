Final Bets: green - 16 bets for 6,761G (29.4%, x2.41); yellow - 9 bets for 16,261G (70.6%, x0.42)

green bets:
BirbBrainsBot: 1,000G (14.8%, 90,267G)
volgrathemoose: 764G (11.3%, 1,529G)
wooyuji: 732G (10.8%, 732G)
ShintaroNayaka: 696G (10.3%, 696G)
killth3kid: 556G (8.2%, 10,560G)
Error72: 500G (7.4%, 3,725G)
Dexsana: 500G (7.4%, 2,787G)
LeoNightFury: 496G (7.3%, 496G)
Rislyeu: 344G (5.1%, 344G)
getthemoneyz: 282G (4.2%, 681,310G)
RollingWithNat20: 226G (3.3%, 226G)
PotionDweller: 200G (3.0%, 2,201G)
Arbitae: 165G (2.4%, 1,065G)
ko2q: 100G (1.5%, 3,236G)
E_Ballard: 100G (1.5%, 7,613G)
datadrivenbot: 100G (1.5%, 18,421G)

yellow bets:
BoneMiser: 12,216G (75.1%, 12,216G)
leakimiko: 1,000G (6.1%, 42,155G)
Dexef: 905G (5.6%, 905G)
superdevon1: 823G (5.1%, 16,463G)
maakur_: 484G (3.0%, 484G)
JLinkletter: 430G (2.6%, 430G)
MalakiGenesys: 200G (1.2%, 819G)
ungabunga_bot: 103G (0.6%, 514,366G)
ar_tactic: 100G (0.6%, 42,549G)

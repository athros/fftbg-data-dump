Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Theatheologist
Female
Gemini
68
71
Wizard
Punch Art
Damage Split
Equip Polearm
Teleport

Iron Fan

Flash Hat
Power Sleeve
Cursed Ring

Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2
Wave Fist, Earth Slash, Purification



Yasmosis
Female
Cancer
40
65
Summoner
Time Magic
Blade Grasp
Equip Axe
Waterbreathing

Flame Whip

Red Hood
Chameleon Robe
Dracula Mantle

Shiva, Ramuh, Golem, Bahamut, Salamander
Haste, Stop, Reflect, Quick, Stabilize Time, Galaxy Stop



Hasterious
Monster
Aquarius
71
72
Steel Giant










Nosablake
Female
Aries
62
53
Knight
Talk Skill
Brave Up
Attack UP
Lava Walking

Giant Axe
Hero Shield
Crystal Helmet
Platinum Armor
Battle Boots

Shield Break, Power Break, Mind Break, Stasis Sword
Threaten, Insult, Rehabilitate

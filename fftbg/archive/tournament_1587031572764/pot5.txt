Final Bets: blue - 6 bets for 3,805G (32.4%, x2.08); green - 11 bets for 7,925G (67.6%, x0.48)

blue bets:
HaplessOne: 2,000G (52.6%, 77,387G)
getthemoneyz: 682G (17.9%, 499,801G)
mayormcfunbags: 500G (13.1%, 1,239G)
gorgewall: 323G (8.5%, 323G)
Dexsana: 200G (5.3%, 2,552G)
Tithonus: 100G (2.6%, 6,546G)

green bets:
sinnyil2: 3,333G (42.1%, 36,833G)
WireLord: 1,642G (20.7%, 1,642G)
BirbBrainsBot: 1,000G (12.6%, 184,711G)
Anomandaris__: 500G (6.3%, 7,341G)
datadrivenbot: 405G (5.1%, 792G)
Jeeboheebo: 360G (4.5%, 360G)
KasugaiRoastedPeas: 340G (4.3%, 5,011G)
Scotty297: 100G (1.3%, 100G)
Cervani: 100G (1.3%, 2,549G)
Maeveen: 100G (1.3%, 3,708G)
daveb_: 45G (0.6%, 2,526G)

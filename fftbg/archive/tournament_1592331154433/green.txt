Player: !Green
Team: Green Team
Palettes: Green/White



Powergems
Female
Pisces
41
79
Dancer
Talk Skill
Damage Split
Monster Talk
Fly

Cashmere

Triangle Hat
Black Costume
Magic Gauntlet

Witch Hunt, Obsidian Blade, Nether Demon
Persuade, Negotiate, Refute



Laserman1000
Male
Sagittarius
59
79
Calculator
Yin Yang Magic
Brave Save
Defense UP
Jump+2

Iron Fan

Red Hood
Light Robe
Sprint Shoes

CT, Prime Number, 4, 3
Spell Absorb, Pray Faith, Dispel Magic



MXsora
Male
Virgo
77
47
Squire
Black Magic
MP Restore
Halve MP
Waterbreathing

Mythril Knife
Buckler
Red Hood
Judo Outfit
Wizard Mantle

Accumulate, Dash, Throw Stone, Heal
Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Frog



Kronikle
Male
Aries
75
52
Monk
Throw
Brave Save
Equip Shield
Waterwalking


Flame Shield
Golden Hairpin
Clothes
Angel Ring

Spin Fist, Chakra
Shuriken, Wand

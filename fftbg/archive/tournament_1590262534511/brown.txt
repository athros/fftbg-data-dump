Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fenaen
Female
Sagittarius
50
52
Oracle
Item
Counter Magic
Equip Gun
Jump+2

Bestiary

Triangle Hat
Wizard Robe
Battle Boots

Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Sleep, Petrify
Potion, Antidote, Eye Drop, Phoenix Down



Maakur
Female
Libra
68
65
Mime

Counter
Attack UP
Levitate



Holy Miter
Wizard Outfit
Jade Armlet

Mimic




Lawndough
Male
Aries
43
68
Lancer
Charge
Damage Split
Equip Polearm
Jump+3

Partisan
Escutcheon
Crystal Helmet
Mythril Armor
Defense Armlet

Level Jump8, Vertical Jump3
Charge+2, Charge+5, Charge+20



Byrdturbo
Monster
Taurus
67
52
Porky








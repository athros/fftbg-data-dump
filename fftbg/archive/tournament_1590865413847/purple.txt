Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



FoeSquirrel
Male
Scorpio
56
80
Time Mage
Punch Art
Catch
Concentrate
Move+3

Octagon Rod

Flash Hat
Silk Robe
Feather Boots

Slow, Slow 2, Reflect, Stabilize Time
Spin Fist, Purification



Rabbitlogik
Male
Cancer
79
76
Squire
Yin Yang Magic
Counter Flood
Long Status
Waterwalking

Flail
Hero Shield
Barette
Mythril Vest
Elf Mantle

Accumulate, Heal, Tickle, Yell, Wish
Poison, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic, Sleep



ThePineappleSalesman
Monster
Aries
64
59
Tiamat










TheMM42
Female
Cancer
77
60
Priest
Jump
Caution
Long Status
Move+2

White Staff

Black Hood
White Robe
Red Shoes

Cure 2, Cure 3, Cure 4, Regen, Shell 2, Wall, Esuna
Level Jump5, Vertical Jump5

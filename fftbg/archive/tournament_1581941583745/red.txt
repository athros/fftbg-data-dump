Player: !Red
Team: Red Team
Palettes: Red/Brown



Lakart
Monster
Cancer
36
72
Plague










KoreanUsher
Male
Capricorn
75
76
Samurai
Battle Skill
Damage Split
Attack UP
Jump+3

Bizen Boat

Cross Helmet
Carabini Mail
Battle Boots

Bizen Boat, Kiyomori
Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Dark Sword



Zmoses
Male
Gemini
77
62
Ninja
Draw Out
Counter Tackle
Equip Polearm
Jump+3

Hidden Knife
Iga Knife
Holy Miter
Clothes
Feather Mantle

Bomb, Hammer
Koutetsu, Kiyomori



Optiblast
Male
Pisces
74
49
Time Mage
Draw Out
MA Save
Short Charge
Jump+2

Madlemgen

Flash Hat
Linen Robe
Red Shoes

Haste, Slow, Float, Quick, Demi 2, Stabilize Time
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori

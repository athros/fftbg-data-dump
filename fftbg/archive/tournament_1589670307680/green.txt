Player: !Green
Team: Green Team
Palettes: Green/White



Nifboy
Male
Scorpio
69
46
Time Mage
Math Skill
MP Restore
Concentrate
Move+3

Rainbow Staff

Black Hood
Black Robe
Defense Armlet

Haste, Slow 2, Reflect, Stabilize Time
CT, 5, 4



AmaninAmide
Female
Scorpio
57
44
Lancer
Punch Art
Blade Grasp
Martial Arts
Lava Walking

Partisan
Venetian Shield
Iron Helmet
Chain Mail
Germinas Boots

Level Jump8, Vertical Jump7
Spin Fist, Wave Fist, Purification



Pandasforsale
Male
Sagittarius
50
63
Squire
Steal
Counter Tackle
Beastmaster
Fly

Poison Bow
Platinum Shield
Feather Hat
Black Costume
Diamond Armlet

Accumulate, Dash, Heal, Tickle
Gil Taking, Steal Heart, Steal Armor, Steal Accessory, Arm Aim



RunicMagus
Male
Pisces
59
59
Knight
Yin Yang Magic
Sunken State
Short Status
Move+3

Slasher
Hero Shield
Barbuta
Silk Robe
Elf Mantle

Head Break, Shield Break, Magic Break, Power Break, Dark Sword
Poison, Spell Absorb, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic

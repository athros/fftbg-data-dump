Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dymntd
Male
Libra
63
37
Time Mage
White Magic
Absorb Used MP
Attack UP
Teleport

Oak Staff

Leather Hat
Mystic Vest
Leather Mantle

Haste, Haste 2, Slow, Immobilize, Demi
Raise, Raise 2, Protect, Protect 2, Shell, Shell 2, Wall, Esuna, Magic Barrier



SirCherish
Male
Aquarius
45
80
Archer
Basic Skill
MA Save
Magic Defense UP
Levitate

Blast Gun
Crystal Shield
Crystal Helmet
Black Costume
Defense Armlet

Charge+1, Charge+2, Charge+3
Accumulate, Heal



Go2sleepTV
Female
Taurus
41
51
Geomancer
Draw Out
Abandon
Equip Knife
Fly

Ninja Edge
Buckler
Green Beret
Chain Vest
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Kiyomori, Chirijiraden



Itsadud
Male
Cancer
68
71
Priest
Item
Critical Quick
Throw Item
Ignore Height

Rainbow Staff

Holy Miter
Wizard Outfit
Reflect Ring

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell 2, Esuna
Potion, Antidote, Phoenix Down

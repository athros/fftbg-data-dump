Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Prince Rogers Nelson
Monster
Leo
67
74
Pisco Demon










Meta Five
Male
Sagittarius
47
47
Samurai
White Magic
Hamedo
Equip Shield
Teleport 2

Koutetsu Knife
Platinum Shield
Genji Helmet
Chameleon Robe
Magic Gauntlet

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori
Raise, Shell 2, Wall, Esuna, Holy



Dogsandcatsand
Female
Libra
70
44
Summoner
Jump
Sunken State
Short Charge
Retreat

Flame Rod

Flash Hat
Light Robe
Sprint Shoes

Ifrit, Leviathan, Salamander, Silf
Level Jump3, Vertical Jump5



Flameatron
Male
Pisces
65
62
Lancer
White Magic
MP Restore
Halve MP
Lava Walking

Spear
Round Shield
Barbuta
Crystal Mail
Salty Rage

Level Jump8, Vertical Jump7
Cure, Reraise, Regen, Protect, Protect 2, Esuna

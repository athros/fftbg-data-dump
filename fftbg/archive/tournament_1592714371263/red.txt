Player: !Red
Team: Red Team
Palettes: Red/Brown



Powergems
Male
Virgo
78
81
Monk
Elemental
Parry
Equip Gun
Lava Walking

Battle Folio

Black Hood
Adaman Vest
Defense Armlet

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



DarrenDinosaurs
Female
Libra
44
75
Squire
Yin Yang Magic
Dragon Spirit
Short Charge
Move+3

Ancient Sword
Flame Shield
Holy Miter
Genji Armor
Red Shoes

Dash, Throw Stone, Tickle, Fury, Wish
Spell Absorb



Grininda
Male
Cancer
79
70
Lancer
Battle Skill
Speed Save
Equip Gun
Jump+2

Ramia Harp
Aegis Shield
Gold Helmet
Linen Robe
N-Kai Armlet

Level Jump5, Vertical Jump2
Shield Break, Magic Break, Mind Break, Dark Sword, Surging Sword, Explosion Sword



Itsadud
Female
Libra
74
52
Wizard
Summon Magic
Distribute
Maintenance
Lava Walking

Faith Rod

Twist Headband
Clothes
Elf Mantle

Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice, Death, Flare
Shiva, Ramuh, Ifrit, Silf

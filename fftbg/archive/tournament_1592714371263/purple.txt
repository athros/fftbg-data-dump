Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ArchKnightX
Female
Capricorn
57
70
Geomancer
Time Magic
HP Restore
Maintenance
Fly

Sleep Sword
Ice Shield
Thief Hat
Clothes
Bracer

Pitfall, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Slow, Slow 2, Reflect, Stabilize Time



Benticore
Male
Gemini
70
63
Mime

Catch
Sicken
Move+2


Diamond Shield
Headgear
Mythril Vest
108 Gems

Mimic




Zetchryn
Monster
Taurus
49
73
Pisco Demon










Biffcake01
Male
Taurus
55
72
Lancer
Charge
Catch
Dual Wield
Move-HP Up

Spear
Javelin
Diamond Helmet
Reflect Mail
Spike Shoes

Level Jump8, Vertical Jump4
Charge+3, Charge+5

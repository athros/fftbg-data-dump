Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SkylerBunny
Female
Libra
52
68
Squire
Punch Art
Arrow Guard
Equip Knife
Move-HP Up

Short Edge
Bronze Shield
Genji Helmet
Gold Armor
Dracula Mantle

Throw Stone, Heal, Yell, Cheer Up
Spin Fist, Chakra, Revive



Mesmaster
Male
Taurus
51
50
Geomancer
White Magic
MP Restore
Defense UP
Jump+1

Battle Axe
Escutcheon
Flash Hat
White Robe
108 Gems

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Cure, Cure 3, Raise, Protect 2, Shell 2, Holy



Lowlf
Monster
Capricorn
53
65
Chocobo










Seaweed B
Female
Virgo
80
71
Summoner
Dance
Distribute
Short Charge
Move+1

Battle Folio

Holy Miter
Wizard Robe
Genji Gauntlet

Shiva, Ramuh, Golem, Carbunkle, Silf
Witch Hunt, Last Dance, Void Storage

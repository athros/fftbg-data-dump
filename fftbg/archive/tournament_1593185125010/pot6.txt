Final Bets: white - 12 bets for 9,158G (53.6%, x0.87); brown - 6 bets for 7,939G (46.4%, x1.15)

white bets:
ShintaroNayaka: 4,715G (51.5%, 4,715G)
dogsandcatsand: 1,390G (15.2%, 7,390G)
BirbBrainsBot: 1,000G (10.9%, 20,014G)
killth3kid: 500G (5.5%, 7,992G)
d4rr1n: 400G (4.4%, 5,264G)
benticore: 375G (4.1%, 7,509G)
TheChainNerd: 250G (2.7%, 3,028G)
CosmicTactician: 200G (2.2%, 22,638G)
acid_flashback: 112G (1.2%, 112G)
datadrivenbot: 100G (1.1%, 48,921G)
rmelzer1986: 100G (1.1%, 1,738G)
getthemoneyz: 16G (0.2%, 1,062,107G)

brown bets:
Lydian_C: 4,200G (52.9%, 203,284G)
Zeroroute: 1,577G (19.9%, 1,577G)
SkylerBunny: 800G (10.1%, 513,575G)
Shalloween: 600G (7.6%, 82,646G)
Laserman1000: 561G (7.1%, 5,461G)
gorgewall: 201G (2.5%, 26,322G)

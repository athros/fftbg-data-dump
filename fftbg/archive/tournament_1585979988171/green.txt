Player: !Green
Team: Green Team
Palettes: Green/White



KaLam1ty
Male
Leo
76
41
Bard
Battle Skill
Auto Potion
Beastmaster
Levitate

Ramia Harp

Leather Hat
Leather Outfit
N-Kai Armlet

Cheer Song, Nameless Song, Last Song, Space Storage
Speed Break



Moonliquor
Male
Aries
44
44
Monk
Black Magic
Speed Save
Short Status
Teleport



Leather Hat
Leather Outfit
Dracula Mantle

Spin Fist, Pummel, Earth Slash, Chakra, Revive
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4, Death



ApplesauceBoss
Female
Serpentarius
79
78
Samurai
Summon Magic
Counter
Magic Attack UP
Fly

Kikuichimoji

Crystal Helmet
Linen Cuirass
Reflect Ring

Asura
Shiva, Carbunkle, Salamander



Leakimiko
Male
Libra
58
38
Ninja
Black Magic
Sunken State
Equip Gun
Fly

Papyrus Codex
Papyrus Codex
Twist Headband
Power Sleeve
N-Kai Armlet

Shuriken, Knife
Fire 2, Fire 3, Bolt 2, Ice

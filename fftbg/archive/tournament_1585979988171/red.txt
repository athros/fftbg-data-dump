Player: !Red
Team: Red Team
Palettes: Red/Brown



Rogueain
Male
Libra
52
79
Oracle
Time Magic
Meatbone Slash
Magic Attack UP
Jump+2

Cypress Rod

Golden Hairpin
Linen Robe
Small Mantle

Blind, Poison, Life Drain, Blind Rage, Foxbird, Confusion Song, Paralyze
Haste, Slow, Reflect, Demi 2



StealthModeLocke
Male
Pisces
45
40
Ninja
Punch Art
Distribute
Equip Polearm
Swim

Gokuu Rod
Holy Lance
Leather Hat
Power Sleeve
Magic Ring

Shuriken, Knife, Hammer, Ninja Sword
Wave Fist, Purification, Revive, Seal Evil



DeathTaxesAndAnime
Female
Cancer
66
70
Priest
Basic Skill
HP Restore
Short Charge
Move-MP Up

Scorpion Tail

Red Hood
Linen Robe
Magic Gauntlet

Cure 3, Raise, Raise 2, Regen, Protect 2, Shell 2, Esuna
Heal



Chuckolator
Male
Cancer
80
42
Oracle
Talk Skill
MA Save
Short Charge
Waterbreathing

Battle Bamboo

Twist Headband
Linen Robe
Small Mantle

Pray Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy
Threaten, Solution, Insult

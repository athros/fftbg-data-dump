Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CT 5 Holy
Male
Scorpio
72
74
Bard
White Magic
Auto Potion
Magic Attack UP
Jump+2

Ramia Harp

Headgear
Chain Mail
Cursed Ring

Angel Song, Cheer Song, Magic Song, Last Song, Space Storage
Cure 2, Raise, Reraise, Wall, Esuna



Kyune
Male
Cancer
74
74
Ninja
Jump
HP Restore
Equip Sword
Move+3

Kikuichimoji
Platinum Sword
Thief Hat
Brigandine
Defense Armlet

Bomb
Level Jump4, Vertical Jump8



ApplesauceBoss
Male
Scorpio
77
79
Ninja
Talk Skill
HP Restore
Defense UP
Levitate

Mage Masher
Hidden Knife
Leather Hat
Wizard Outfit
Small Mantle

Shuriken, Bomb, Hammer
Solution, Death Sentence



ScootyPff
Female
Leo
38
71
Squire
Battle Skill
Parry
Dual Wield
Swim

Dagger
Morning Star
Green Beret
Chain Mail
Dracula Mantle

Throw Stone, Heal, Yell, Fury
Magic Break, Speed Break, Justice Sword

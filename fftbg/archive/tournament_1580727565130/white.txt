Player: !White
Team: White Team
Palettes: White/Blue



PandaBattalion
Male
Virgo
68
72
Priest
Summon Magic
MP Restore
Secret Hunt
Waterwalking

Healing Staff

Leather Hat
Wizard Robe
Magic Gauntlet

Cure, Cure 4, Raise, Reraise, Protect, Wall, Esuna, Holy
Moogle, Shiva, Carbunkle, Salamander, Fairy



Shoosler
Female
Sagittarius
63
60
Geomancer
Jump
Meatbone Slash
Equip Gun
Waterwalking

Heaven's Cloud
Hero Shield
Headgear
Leather Outfit
Feather Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump3, Vertical Jump6



Neetneph
Female
Pisces
46
72
Ninja
Steal
Brave Up
Equip Gun
Jump+2

Mythril Knife
Ninja Edge
Twist Headband
Mythril Vest
Feather Mantle

Bomb, Knife
Gil Taking, Steal Heart, Steal Armor, Steal Shield



Legionofwar
Female
Sagittarius
39
55
Dancer
Punch Art
Earplug
Secret Hunt
Waterbreathing

Ryozan Silk

Leather Hat
Adaman Vest
Genji Gauntlet

Witch Hunt, Wiznaibus, Polka Polka, Last Dance, Obsidian Blade
Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive

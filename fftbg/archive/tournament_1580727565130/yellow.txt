Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Artea
Female
Virgo
79
37
Wizard
Throw
Parry
Maintenance
Levitate

Flame Rod

Red Hood
Mythril Vest
Magic Gauntlet

Fire, Bolt, Bolt 2, Ice, Ice 3, Ice 4, Empower, Death
Shuriken



Fozzington
Female
Gemini
47
56
Squire
White Magic
Meatbone Slash
Short Charge
Move-MP Up

Cute Bag

Headgear
Clothes
Defense Armlet

Heal, Yell, Wish
Cure 2, Cure 4, Raise, Raise 2, Wall, Esuna, Holy



Shakarak
Male
Serpentarius
79
53
Geomancer
Basic Skill
Catch
Concentrate
Move+1

Ice Brand
Buckler
Green Beret
Mythril Vest
Feather Boots

Water Ball, Hell Ivy, Will-O-Wisp, Quicksand
Accumulate, Throw Stone, Heal, Yell, Cheer Up



TheRedMelody
Female
Scorpio
42
42
Wizard
Time Magic
Counter Magic
Magic Defense UP
Waterbreathing

Mage Masher

Holy Miter
Adaman Vest
Rubber Shoes

Fire 3, Bolt 2, Bolt 4, Ice, Ice 3, Ice 4, Death
Haste 2, Slow, Demi, Demi 2, Stabilize Time

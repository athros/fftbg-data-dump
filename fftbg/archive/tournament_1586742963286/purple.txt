Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Omegasuspekt
Male
Aries
78
72
Oracle
Talk Skill
Counter
Equip Axe
Move-MP Up

Morning Star

Twist Headband
Earth Clothes
Defense Armlet

Blind, Life Drain, Pray Faith, Zombie, Silence Song
Invitation, Persuade, Praise, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



Astrozin11
Male
Virgo
71
44
Knight
Basic Skill
Earplug
Magic Defense UP
Move+3

Ancient Sword
Aegis Shield
Crystal Helmet
Genji Armor
Angel Ring

Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword, Surging Sword
Accumulate, Dash, Heal, Cheer Up, Fury, Wish



Sect Cor
Female
Virgo
77
50
Chemist
Dance
Counter Tackle
Long Status
Move+2

Cute Bag

Black Hood
Mythril Vest
Leather Mantle

Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Disillusion, Last Dance



Oogthecaveman
Female
Libra
68
60
Ninja
Charge
Counter Flood
Short Status
Move+3

Mythril Knife
Dagger
Triangle Hat
Leather Outfit
Power Wrist

Bomb, Staff, Ninja Sword
Charge+1, Charge+3, Charge+7, Charge+10

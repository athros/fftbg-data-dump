Player: !Green
Team: Green Team
Palettes: Green/White



Ps36wii
Female
Cancer
45
45
Calculator
Yin Yang Magic
Brave Up
Short Status
Jump+1

Octagon Rod

Triangle Hat
Wizard Robe
Red Shoes

CT, Height, Prime Number, 5
Blind, Poison, Blind Rage, Foxbird, Confusion Song, Sleep



ThanatosXRagnarok
Female
Capricorn
77
76
Wizard
Battle Skill
Distribute
Equip Sword
Teleport

Nagrarock

Flash Hat
Clothes
Defense Ring

Fire, Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Empower, Death
Weapon Break, Magic Break, Night Sword



GamingKanon
Male
Capricorn
79
43
Samurai
Elemental
Brave Up
Concentrate
Ignore Height

Chirijiraden

Crystal Helmet
Black Robe
Defense Ring

Murasame, Heaven's Cloud, Muramasa
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



VirulenceXT
Male
Leo
64
56
Mediator
Jump
Abandon
Equip Shield
Waterbreathing

Glacier Gun
Escutcheon
Twist Headband
Black Robe
Power Wrist

Persuade, Threaten, Preach, Insult, Negotiate, Refute
Level Jump2, Vertical Jump2

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Shalloween
Female
Cancer
53
78
Wizard
Battle Skill
MP Restore
Short Charge
Move+3

Rod

Green Beret
Leather Outfit
Spike Shoes

Fire 2, Fire 4, Bolt, Bolt 2, Ice, Ice 2, Ice 3, Ice 4
Weapon Break, Magic Break, Speed Break, Power Break, Dark Sword, Surging Sword



Timidpimp11
Male
Serpentarius
59
59
Oracle
Battle Skill
Parry
Concentrate
Ignore Terrain

Battle Folio

Golden Hairpin
Silk Robe
Cursed Ring

Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Dark Holy
Magic Break, Power Break, Mind Break, Justice Sword, Explosion Sword



AM3P Batman
Male
Virgo
59
51
Chemist
Draw Out
Meatbone Slash
Sicken
Swim

Panther Bag

Golden Hairpin
Power Sleeve
Diamond Armlet

Hi-Potion, Ether, Hi-Ether, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Bizen Boat, Heaven's Cloud



DarrenDinosaurs
Female
Scorpio
46
48
Wizard
Summon Magic
PA Save
Short Status
Retreat

Wizard Rod

Red Hood
White Robe
Vanish Mantle

Fire, Bolt 2, Ice, Ice 2, Ice 3
Moogle, Ifrit, Golem, Leviathan, Salamander, Cyclops

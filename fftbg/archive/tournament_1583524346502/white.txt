Player: !White
Team: White Team
Palettes: White/Blue



Athropos1
Female
Libra
65
76
Mediator
Jump
Dragon Spirit
Secret Hunt
Move-MP Up

Dagger

Green Beret
Silk Robe
Wizard Mantle

Threaten, Solution, Death Sentence, Negotiate
Level Jump5, Vertical Jump6



OmnibotGamma
Male
Taurus
59
37
Geomancer
Yin Yang Magic
Caution
Martial Arts
Ignore Terrain


Crystal Shield
Headgear
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Petrify



Astrozin11
Male
Gemini
67
59
Samurai
Basic Skill
Speed Save
Equip Polearm
Ignore Height

Persia

Bronze Helmet
Silk Robe
Rubber Shoes

Koutetsu, Muramasa, Kikuichimoji
Heal, Scream, Ultima



Eltejongrande
Male
Cancer
54
70
Summoner
Jump
Counter Magic
Equip Axe
Jump+3

Giant Axe

Black Hood
Mystic Vest
Red Shoes

Titan, Carbunkle, Leviathan, Salamander, Lich
Level Jump8, Vertical Jump7

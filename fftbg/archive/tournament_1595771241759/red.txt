Player: !Red
Team: Red Team
Palettes: Red/Brown



Netmonmatt
Female
Scorpio
79
58
Chemist
Basic Skill
Damage Split
Magic Defense UP
Lava Walking

Dagger

Red Hood
Wizard Outfit
Dracula Mantle

Hi-Potion, Ether, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Accumulate, Throw Stone, Tickle



Grininda
Male
Aries
48
45
Monk
Item
Hamedo
Throw Item
Retreat



Cachusha
Power Sleeve
Magic Ring

Wave Fist, Earth Slash, Purification
Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down



Killth3kid
Female
Taurus
60
78
Calculator
Spirit Skill
Mana Shield
Beastmaster
Move+2

Panther Bag
Venetian Shield
Circlet
Plate Mail
Reflect Ring

Blue Magic
Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch, Bite, Self Destruct, Flame Attack, Small Bomb, Spark



Sinnyil2
Female
Pisces
74
50
Squire
Steal
MA Save
Defense UP
Move-MP Up

Mage Masher
Mythril Shield
Feather Hat
Chain Mail
Power Wrist

Dash, Heal, Tickle, Fury
Steal Accessory

Player: !Green
Team: Green Team
Palettes: Green/White



Madarius777
Male
Sagittarius
80
75
Ninja
Black Magic
Meatbone Slash
Equip Gun
Jump+3

Ninja Edge
Dagger
Leather Hat
Mythril Vest
Power Wrist

Shuriken, Knife, Wand
Fire, Bolt 2, Ice 2, Ice 3, Ice 4



Patrickmurder
Female
Aquarius
73
67
Samurai
Steal
Arrow Guard
Magic Attack UP
Move-HP Up

Kikuichimoji

Gold Helmet
Crystal Mail
Magic Ring

Asura, Bizen Boat, Heaven's Cloud, Muramasa
Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



ArchKnightX
Male
Sagittarius
77
70
Wizard
Basic Skill
Brave Up
Attack UP
Retreat

Ice Rod

Thief Hat
Light Robe
Germinas Boots

Fire 2, Bolt 4, Ice 2, Ice 3
Dash, Heal, Cheer Up, Wish, Scream



Omegasuspekt
Male
Libra
54
77
Ninja
Jump
Counter Flood
Concentrate
Waterbreathing

Flame Whip
Air Knife
Green Beret
Power Sleeve
Jade Armlet

Shuriken, Bomb, Hammer, Dictionary
Level Jump5, Vertical Jump6

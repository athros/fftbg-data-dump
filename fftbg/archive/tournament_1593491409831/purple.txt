Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lowlf
Male
Libra
75
80
Ninja
Sing
Auto Potion
Equip Shield
Teleport

Morning Star
Crystal Shield
Twist Headband
Leather Outfit
Spike Shoes

Shuriken, Bomb, Wand
Battle Song, Nameless Song, Last Song, Diamond Blade, Space Storage



Holyonline
Monster
Libra
55
66
Swine










UmaiJam
Female
Sagittarius
42
75
Wizard
Charge
MA Save
Halve MP
Move+1

Orichalcum

Holy Miter
Wizard Robe
Reflect Ring

Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Frog, Death, Flare
Charge+3, Charge+4, Charge+10



Baron Von Scrub
Male
Cancer
64
62
Bard
Elemental
Brave Save
Equip Shield
Move-HP Up

Long Bow
Genji Shield
Headgear
Wizard Outfit
Genji Gauntlet

Angel Song, Life Song, Nameless Song, Last Song
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm

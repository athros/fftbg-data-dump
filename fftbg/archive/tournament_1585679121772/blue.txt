Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ko2q
Male
Sagittarius
43
47
Bard
White Magic
MP Restore
Martial Arts
Fly



Headgear
Wizard Outfit
Defense Ring

Angel Song, Cheer Song, Diamond Blade, Space Storage, Sky Demon
Raise, Raise 2, Protect 2, Shell, Esuna



Wizblizz
Female
Aries
36
58
Priest
Jump
Dragon Spirit
Halve MP
Move+1

Healing Staff

Headgear
Earth Clothes
Power Wrist

Cure 2, Raise, Regen, Shell, Shell 2, Wall, Esuna
Level Jump2, Vertical Jump8



Sinnyil2
Female
Gemini
48
45
Samurai
White Magic
Damage Split
Equip Shield
Jump+2

Muramasa
Hero Shield
Leather Helmet
Crystal Mail
Magic Gauntlet

Asura, Kikuichimoji
Cure, Cure 4, Raise, Regen, Esuna



Legendaryax4
Male
Gemini
44
67
Ninja
Time Magic
PA Save
Equip Axe
Retreat

Slasher
White Staff
Black Hood
Wizard Outfit
Bracer

Bomb, Knife
Haste, Haste 2, Immobilize, Reflect, Stabilize Time

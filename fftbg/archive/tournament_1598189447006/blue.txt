Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Just Here2
Monster
Cancer
76
55
Archaic Demon










Gorgewall
Male
Sagittarius
47
45
Time Mage
Jump
Abandon
Halve MP
Retreat

Cypress Rod

Barette
Leather Outfit
Small Mantle

Haste 2, Slow, Demi 2, Stabilize Time
Level Jump3, Vertical Jump6



Willjin
Female
Aries
47
73
Knight
Item
HP Restore
Equip Polearm
Swim

Persia
Ice Shield
Platinum Helmet
Mythril Armor
Cursed Ring

Head Break, Shield Break, Weapon Break, Mind Break
X-Potion, Echo Grass, Maiden's Kiss, Phoenix Down



Evewho
Female
Aquarius
68
40
Oracle
Time Magic
Counter
Maintenance
Lava Walking

Ivory Rod

Thief Hat
Light Robe
Bracer

Blind, Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic
Haste, Slow, Quick, Stabilize Time

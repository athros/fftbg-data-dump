Player: !Red
Team: Red Team
Palettes: Red/Brown



Breakdown777
Male
Scorpio
53
65
Mediator
White Magic
Parry
Magic Defense UP
Waterbreathing

Romanda Gun

Barette
Chameleon Robe
Jade Armlet

Invitation, Threaten, Refute
Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Esuna



Monopool
Monster
Virgo
77
43
King Behemoth










Powergems
Male
Taurus
63
79
Time Mage
Jump
Speed Save
Equip Gun
Waterbreathing

Bloody Strings

Headgear
Adaman Vest
N-Kai Armlet

Haste, Slow, Stop, Quick, Stabilize Time
Level Jump4, Vertical Jump6



TheChainNerd
Male
Cancer
59
77
Lancer
Punch Art
Catch
Dual Wield
Move+2

Spear
Mythril Spear
Diamond Helmet
White Robe
Defense Armlet

Level Jump5, Vertical Jump8
Spin Fist, Purification, Revive

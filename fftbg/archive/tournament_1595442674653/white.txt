Player: !White
Team: White Team
Palettes: White/Blue



JumbocactuarX27
Male
Aquarius
55
52
Mime

Dragon Spirit
Defense UP
Move+1



Black Hood
Wizard Outfit
Diamond Armlet

Mimic




SQUiDSQUARKLIN
Female
Scorpio
57
72
Summoner
Charge
MP Restore
Halve MP
Move+3

Wizard Staff

Feather Hat
Brigandine
Cherche

Moogle, Ifrit, Titan, Golem, Carbunkle, Bahamut
Charge+10



Maakur
Female
Taurus
73
61
Time Mage
Summon Magic
Faith Save
Equip Armor
Move+3

Octagon Rod

Circlet
Maximillian
Cursed Ring

Haste, Slow 2, Stop, Reflect, Quick, Demi 2, Stabilize Time
Moogle, Shiva, Titan, Carbunkle, Leviathan, Fairy



CapnChaos12
Male
Cancer
56
75
Archer
Time Magic
Faith Save
Doublehand
Swim

Gastrafitis

Leather Helmet
Leather Outfit
Genji Gauntlet

Charge+1, Charge+2, Charge+3, Charge+10
Haste, Slow, Reflect, Quick, Demi, Stabilize Time

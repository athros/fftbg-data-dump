Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Alaylle
Male
Pisces
72
53
Summoner
Basic Skill
Parry
Dual Wield
Ignore Height

Flame Rod
Wizard Rod
Feather Hat
Chain Vest
Defense Ring

Moogle, Shiva, Carbunkle, Odin, Fairy
Dash, Heal, Tickle



Therealelmdor
Male
Sagittarius
64
80
Ninja
Punch Art
Distribute
Defend
Lava Walking

Cute Bag
Morning Star
Barette
Adaman Vest
Defense Armlet

Knife
Spin Fist, Secret Fist, Purification, Revive



EvilLego6
Male
Pisces
61
74
Squire
Talk Skill
MP Restore
Halve MP
Ignore Terrain

Orichalcum
Platinum Shield
Bronze Helmet
Leather Outfit
Diamond Armlet

Dash, Yell, Cheer Up
Invitation, Praise, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



DudeMonkey77
Male
Leo
39
46
Monk
Talk Skill
Arrow Guard
Dual Wield
Move-HP Up



Holy Miter
Brigandine
108 Gems

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra
Invitation, Solution, Death Sentence, Negotiate, Refute, Rehabilitate

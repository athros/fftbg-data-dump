Player: !zChamp
Team: Champion Team
Palettes: White/Blue



MyFakeLife
Female
Leo
61
66
Monk
Talk Skill
Regenerator
Short Status
Move+1



Barette
Judo Outfit
Spike Shoes

Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Persuade, Solution, Death Sentence, Mimic Daravon, Refute



DarrenDinosaurs
Male
Scorpio
55
45
Knight
Talk Skill
Abandon
Defend
Fly

Excalibur

Genji Helmet
Crystal Mail
Genji Gauntlet

Weapon Break, Power Break
Invitation, Persuade, Threaten, Solution, Death Sentence, Negotiate, Mimic Daravon



KingOfTheFist
Monster
Aquarius
69
75
Steel Giant










StealthModeLocke
Female
Libra
73
55
Monk
Jump
Regenerator
Short Status
Move-MP Up



Flash Hat
Earth Clothes
Feather Mantle

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Level Jump2, Vertical Jump6

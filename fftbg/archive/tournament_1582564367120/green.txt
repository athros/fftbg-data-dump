Player: !Green
Team: Green Team
Palettes: Green/White



SpencoJFrog
Male
Aries
78
55
Summoner
Black Magic
Regenerator
Dual Wield
Move+3

Rainbow Staff
Rod
Red Hood
Chameleon Robe
Jade Armlet

Moogle, Carbunkle, Bahamut
Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice, Ice 4, Empower, Death



Nizaha
Female
Aries
73
45
Calculator
Animal Skill
Dragon Spirit
Sicken
Levitate

Poison Rod

Mythril Helmet
Gold Armor
Cursed Ring

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



MeleeWizard
Female
Leo
63
59
Dancer
Throw
Arrow Guard
Concentrate
Retreat

Ryozan Silk

Holy Miter
Linen Robe
Feather Boots

Slow Dance, Disillusion, Last Dance
Hammer, Wand, Dictionary



Viewtifulrexx
Female
Aquarius
57
68
Wizard
Dance
MP Restore
Attack UP
Swim

Ice Rod

Red Hood
Chain Vest
Defense Ring

Bolt, Bolt 3, Ice 2, Ice 3, Empower, Flare
Disillusion, Void Storage

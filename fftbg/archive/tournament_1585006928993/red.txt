Player: !Red
Team: Red Team
Palettes: Red/Brown



Deathmaker06
Female
Libra
50
47
Knight
Throw
Distribute
Equip Knife
Waterwalking

Blind Knife
Round Shield
Platinum Helmet
Linen Cuirass
Defense Armlet

Head Break, Weapon Break, Magic Break, Night Sword
Shuriken



Ophiala
Male
Scorpio
45
64
Lancer
Battle Skill
Distribute
Secret Hunt
Waterbreathing

Musk Rod
Hero Shield
Barbuta
Reflect Mail
Leather Mantle

Level Jump8, Vertical Jump7
Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Stasis Sword



Ar Tactic
Monster
Capricorn
47
40
Red Panther










ColetteMSLP
Male
Taurus
75
58
Oracle
Black Magic
Absorb Used MP
Attack UP
Jump+2

Iron Fan

Black Hood
Chain Vest
Defense Ring

Blind, Zombie, Dispel Magic, Petrify
Bolt, Bolt 4, Ice, Ice 2

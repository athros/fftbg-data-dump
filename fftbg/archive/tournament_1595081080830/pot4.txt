Final Bets: purple - 11 bets for 5,122G (50.5%, x0.98); brown - 7 bets for 5,022G (49.5%, x1.02)

purple bets:
UmaiJam: 1,000G (19.5%, 57,300G)
DeathTaxesAndAnime: 724G (14.1%, 724G)
getthemoneyz: 558G (10.9%, 1,320,302G)
kliffw: 500G (9.8%, 19,174G)
RedRiderAyame: 500G (9.8%, 8,700G)
prince_rogers_nelson_: 472G (9.2%, 472G)
killth3kid: 436G (8.5%, 5,121G)
bahumat989: 336G (6.6%, 336G)
Xoomwaffle: 256G (5.0%, 1,642G)
skipsandwiches: 240G (4.7%, 240G)
nhammen: 100G (2.0%, 6,866G)

brown bets:
Thyrandaal: 3,000G (59.7%, 9,640G)
randgridr: 500G (10.0%, 2,825G)
AllInBot: 449G (8.9%, 449G)
Error72: 324G (6.5%, 324G)
BirbBrainsBot: 299G (6.0%, 71,028G)
JiroDoge: 250G (5.0%, 750G)
cwilliams013: 200G (4.0%, 200G)

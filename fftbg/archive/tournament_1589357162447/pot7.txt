Final Bets: blue - 14 bets for 14,724G (73.1%, x0.37); brown - 6 bets for 5,409G (26.9%, x2.72)

blue bets:
Mesmaster: 5,000G (34.0%, 61,356G)
Anethum: 2,854G (19.4%, 2,854G)
superdevon1: 1,754G (11.9%, 17,541G)
leakimiko: 1,500G (10.2%, 59,444G)
BirbBrainsBot: 1,000G (6.8%, 9,555G)
Estan_AD: 704G (4.8%, 1,409G)
roofiepops: 666G (4.5%, 6,881G)
otakutaylor: 300G (2.0%, 2,441G)
twelfthrootoftwo: 300G (2.0%, 2,513G)
Aestheta: 208G (1.4%, 208G)
getthemoneyz: 130G (0.9%, 672,559G)
Strifu: 108G (0.7%, 108G)
ko2q: 100G (0.7%, 2,505G)
datadrivenbot: 100G (0.7%, 14,231G)

brown bets:
Shakarak: 2,001G (37.0%, 28,368G)
HorusTaurus: 2,000G (37.0%, 45,164G)
R_Raynos: 1,000G (18.5%, 16,028G)
Digitalsocrates: 208G (3.8%, 208G)
Kyune: 100G (1.8%, 1,624G)
Sairentozon7: 100G (1.8%, 1,609G)

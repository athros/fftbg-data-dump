Player: !Green
Team: Green Team
Palettes: Green/White



Digitalsocrates
Female
Pisces
74
80
Oracle
Time Magic
Damage Split
Short Status
Move+2

Gokuu Rod

Flash Hat
Silk Robe
Bracer

Blind, Poison, Spell Absorb, Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify
Haste, Haste 2, Slow 2, Stop, Float, Quick, Demi, Stabilize Time



Mrfripps
Male
Pisces
51
73
Monk
Draw Out
PA Save
Equip Bow
Move-HP Up

Windslash Bow

Green Beret
Adaman Vest
Magic Ring

Wave Fist, Secret Fist, Purification, Revive
Koutetsu, Murasame, Heaven's Cloud



Solomongrundy85
Female
Aries
71
68
Summoner
Elemental
Critical Quick
Magic Attack UP
Fly

Dragon Rod

Golden Hairpin
Light Robe
Rubber Shoes

Moogle, Golem, Carbunkle, Silf, Fairy
Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Red Lancer
Male
Capricorn
60
59
Ninja
Time Magic
Hamedo
Short Status
Fly

Morning Star
Flame Whip
Barette
Power Sleeve
Feather Mantle

Shuriken, Dictionary
Haste, Haste 2, Float, Stabilize Time

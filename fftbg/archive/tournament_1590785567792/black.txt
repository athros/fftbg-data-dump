Player: !Black
Team: Black Team
Palettes: Black/Red



Kronikle
Male
Taurus
49
49
Ninja
Time Magic
Damage Split
Attack UP
Move-MP Up

Hidden Knife
Dagger
Ribbon
Brigandine
Diamond Armlet

Shuriken
Haste 2, Slow 2, Immobilize, Reflect, Demi 2, Stabilize Time



ColetteMSLP
Female
Libra
54
60
Monk
Yin Yang Magic
Absorb Used MP
Defend
Teleport



Red Hood
Wizard Outfit
Small Mantle

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Blind, Poison, Spell Absorb, Pray Faith, Blind Rage, Dispel Magic



Rislyeu
Female
Aquarius
58
56
Lancer
Elemental
Meatbone Slash
Short Charge
Jump+1

Partisan
Platinum Shield
Iron Helmet
Bronze Armor
Defense Ring

Level Jump2, Vertical Jump5
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Poulol
Female
Capricorn
50
54
Priest
Talk Skill
Counter
Magic Attack UP
Move+2

White Staff

Red Hood
Wizard Robe
N-Kai Armlet

Cure 4, Raise, Protect, Shell, Esuna
Praise, Preach, Refute

Player: !Red
Team: Red Team
Palettes: Red/Brown



Anethum
Female
Capricorn
66
80
Geomancer
Time Magic
Hamedo
Equip Armor
Lava Walking

Battle Axe
Bronze Shield
Cross Helmet
Leather Outfit
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Slow, Slow 2, Stop, Reflect, Quick, Demi, Stabilize Time, Galaxy Stop



Phytik
Female
Virgo
43
45
Archer
Steal
Caution
Equip Axe
Ignore Height

Flail
Mythril Shield
Crystal Helmet
Power Sleeve
Defense Armlet

Charge+1, Charge+2, Charge+5, Charge+7
Steal Helmet, Steal Status, Arm Aim



Cataphract116
Female
Taurus
61
40
Samurai
Punch Art
HP Restore
Concentrate
Jump+3

Kikuichimoji

Gold Helmet
Linen Cuirass
Feather Boots

Asura, Bizen Boat, Kiyomori, Masamune
Earth Slash, Purification, Seal Evil



Yoshima75
Female
Taurus
69
78
Mime

Sunken State
Equip Armor
Move+1



Flash Hat
White Robe
Battle Boots

Mimic


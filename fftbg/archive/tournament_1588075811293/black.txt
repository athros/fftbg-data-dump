Player: !Black
Team: Black Team
Palettes: Black/Red



Ar Tactic
Male
Serpentarius
63
48
Mediator
Item
Speed Save
Concentrate
Waterwalking

Star Bag

Golden Hairpin
White Robe
108 Gems

Invitation, Persuade, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Potion, Hi-Potion, Ether, Hi-Ether, Phoenix Down



UmaiJam
Female
Libra
54
73
Ninja
Punch Art
Meatbone Slash
Equip Gun
Ignore Terrain

Romanda Gun
Blast Gun
Red Hood
Black Costume
Setiemson

Bomb
Pummel, Earth Slash, Purification, Chakra



Potgodtopdog
Monster
Aquarius
43
51
Dark Behemoth










Lijarkh
Male
Virgo
45
59
Monk
Elemental
Damage Split
Dual Wield
Jump+3



Triangle Hat
Power Sleeve
Germinas Boots

Spin Fist, Earth Slash, Purification, Revive
Pitfall, Water Ball, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball

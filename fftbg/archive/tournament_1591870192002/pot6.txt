Final Bets: white - 10 bets for 6,314G (53.4%, x0.87); brown - 9 bets for 5,518G (46.6%, x1.14)

white bets:
Mesmaster: 2,000G (31.7%, 31,670G)
prince_rogers_nelson_: 1,309G (20.7%, 2,619G)
BirbBrainsBot: 1,000G (15.8%, 110,650G)
HASTERIOUS: 492G (7.8%, 492G)
ShintaroNayaka: 400G (6.3%, 15,238G)
serperemagus: 315G (5.0%, 315G)
ColetteMSLP: 300G (4.8%, 1,204G)
ar_tactic: 200G (3.2%, 63,460G)
suumcuiquex: 198G (3.1%, 198G)
datadrivenbot: 100G (1.6%, 36,404G)

brown bets:
Shakarak: 2,000G (36.2%, 164,937G)
Rislyeu: 1,094G (19.8%, 2,188G)
Digitalsocrates: 1,000G (18.1%, 27,891G)
Alaquane: 600G (10.9%, 600G)
AllInBot: 217G (3.9%, 217G)
getthemoneyz: 206G (3.7%, 905,336G)
gorgewall: 201G (3.6%, 18,400G)
RetroGameCommander: 100G (1.8%, 708G)
E_Ballard: 100G (1.8%, 1,874G)

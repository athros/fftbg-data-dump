Player: !White
Team: White Team
Palettes: White/Blue



Daveb
Male
Libra
49
48
Ninja
Draw Out
Arrow Guard
Equip Gun
Lava Walking

Romanda Gun
Glacier Gun
Green Beret
Power Sleeve
Defense Armlet

Bomb, Knife
Koutetsu, Kiyomori, Kikuichimoji



Elelor
Female
Libra
44
64
Thief
Summon Magic
Regenerator
Equip Armor
Waterwalking

Panther Bag

Circlet
Platinum Armor
Bracer

Gil Taking, Steal Armor, Steal Accessory, Leg Aim
Moogle, Ramuh, Salamander, Fairy, Lich



Volgrathemoose
Male
Scorpio
56
54
Knight
Summon Magic
MA Save
Halve MP
Teleport

Ragnarok
Genji Shield
Crystal Helmet
Chameleon Robe
108 Gems

Armor Break, Weapon Break, Speed Break, Justice Sword
Moogle, Shiva, Ifrit, Golem, Bahamut, Odin, Salamander, Lich, Cyclops



Ivellioz
Female
Cancer
42
59
Samurai
White Magic
Earplug
Equip Armor
Move+1

Kikuichimoji

Red Hood
Adaman Vest
Defense Armlet

Murasame, Kiyomori
Cure 2, Cure 3, Raise, Regen, Protect, Shell 2, Esuna

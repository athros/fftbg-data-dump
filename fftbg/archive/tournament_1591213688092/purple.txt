Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vorap
Male
Scorpio
49
65
Summoner
Draw Out
MA Save
Magic Defense UP
Move-MP Up

Flame Rod

Golden Hairpin
Linen Robe
Diamond Armlet

Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Lich
Heaven's Cloud, Muramasa, Kikuichimoji



DLJuggernaut
Female
Aquarius
70
80
Squire
Draw Out
HP Restore
Halve MP
Jump+1

Sleep Sword
Mythril Shield
Headgear
Adaman Vest
Defense Ring

Dash, Heal, Fury, Scream
Koutetsu, Murasame



Killth3kid
Female
Virgo
78
73
Calculator
White Magic
Counter Tackle
Equip Shield
Retreat

Ivory Rod
Diamond Shield
Feather Hat
Light Robe
Angel Ring

Height, Prime Number, 5, 4, 3
Cure 3, Cure 4, Raise, Raise 2, Regen, Shell 2, Wall



Zagorsek
Male
Sagittarius
75
39
Lancer
Time Magic
Faith Save
Equip Armor
Lava Walking

Iron Fan
Escutcheon
Feather Hat
Brigandine
108 Gems

Level Jump5, Vertical Jump8
Haste, Haste 2, Slow, Float, Demi, Stabilize Time

Player: !White
Team: White Team
Palettes: White/Blue



Redmage4evah
Male
Capricorn
73
51
Bard
Yin Yang Magic
Faith Save
Maintenance
Retreat

Ramia Harp

Headgear
Mythril Armor
Feather Boots

Angel Song, Life Song
Blind, Pray Faith, Silence Song, Dispel Magic, Paralyze



Starfire6789
Male
Libra
80
54
Calculator
Yin Yang Magic
Counter Magic
Defend
Move+3

Bestiary

Red Hood
Silk Robe
Defense Ring

Height, Prime Number, 3
Blind, Spell Absorb, Silence Song



Friskycc
Male
Scorpio
61
68
Time Mage
Talk Skill
Absorb Used MP
Magic Defense UP
Waterwalking

Rainbow Staff

Golden Hairpin
Power Sleeve
Jade Armlet

Haste 2, Slow 2, Reflect, Stabilize Time
Invitation



Gelwain
Male
Virgo
45
70
Bard
Jump
Earplug
Equip Gun
Waterwalking

Romanda Gun

Barette
Black Costume
Wizard Mantle

Life Song, Magic Song, Sky Demon
Level Jump2, Vertical Jump5

Player: !White
Team: White Team
Palettes: White/Blue



Laserman1000
Male
Libra
65
65
Mime

Meatbone Slash
Magic Defense UP
Waterbreathing



Leather Hat
Judo Outfit
Defense Ring

Mimic




Soltaker21
Male
Serpentarius
64
37
Monk
Draw Out
Mana Shield
Defense UP
Retreat



Cachusha
Judo Outfit
Vanish Mantle

Spin Fist, Pummel, Earth Slash, Purification, Chakra, Seal Evil
Koutetsu, Kikuichimoji



Ququroon
Male
Libra
74
45
Priest
Time Magic
Auto Potion
Long Status
Lava Walking

Gold Staff

Golden Hairpin
Chameleon Robe
Feather Mantle

Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Protect 2, Shell 2, Wall, Esuna
Stop, Demi 2



Ominnous
Male
Sagittarius
64
80
Knight
Elemental
Faith Up
Long Status
Move+3

Coral Sword
Flame Shield
Gold Helmet
Bronze Armor
Diamond Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Power Break, Mind Break
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Egrazor
Female
Libra
74
70
Calculator
Black Magic
Arrow Guard
Equip Shield
Retreat

Octagon Rod
Ice Shield
Green Beret
Adaman Vest
Defense Armlet

CT, Height, Prime Number, 5, 4, 3
Fire, Fire 2, Bolt 4, Ice 2, Ice 3



Haaaaaank
Male
Leo
49
46
Bard
Black Magic
MP Restore
Defend
Move-MP Up

Fairy Harp

Headgear
Genji Armor
Power Wrist

Life Song, Nameless Song, Space Storage, Sky Demon
Fire 2, Fire 3, Bolt 2, Bolt 3, Ice, Ice 2



Peebs
Male
Gemini
46
43
Lancer
Black Magic
Mana Shield
Doublehand
Teleport

Javelin

Gold Helmet
Bronze Armor
Battle Boots

Level Jump3, Vertical Jump7
Fire, Fire 3, Fire 4, Ice 4, Empower



Zeroroute
Male
Aries
47
51
Thief
Elemental
Dragon Spirit
Equip Axe
Jump+3

Ninja Edge

Leather Helmet
Chain Vest
Jade Armlet

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Alacor
Male
Cancer
80
69
Knight
Throw
Damage Split
Short Charge
Move+3

Defender

Gold Helmet
Mythril Armor
Jade Armlet

Armor Break, Shield Break, Weapon Break, Justice Sword, Dark Sword
Bomb, Knife, Wand



Waterwatereverywhere
Female
Libra
47
64
Chemist
Time Magic
Abandon
Maintenance
Swim

Glacier Gun

Black Hood
Adaman Vest
Angel Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down
Stop, Demi 2, Stabilize Time



Ikebr
Female
Capricorn
68
70
Summoner
Battle Skill
Counter
Equip Shield
Waterbreathing

Mace of Zeus
Platinum Shield
Thief Hat
Leather Vest
Sprint Shoes

Ifrit, Odin, Cyclops
Head Break, Armor Break, Magic Break, Speed Break, Mind Break, Surging Sword



Shalloween
Male
Cancer
77
43
Knight
Summon Magic
Mana Shield
Dual Wield
Move+3

Iron Sword
Sleep Sword
Crystal Helmet
Diamond Armor
Germinas Boots

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword
Moogle, Shiva, Ifrit, Titan, Cyclops

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Moocaotao
Male
Aquarius
63
79
Bard
Time Magic
Regenerator
Concentrate
Move-MP Up

Lightning Bow

Feather Hat
Gold Armor
Dracula Mantle

Angel Song, Diamond Blade
Haste, Quick, Demi, Demi 2



CT 5 Holy
Female
Taurus
66
79
Oracle
Item
Mana Shield
Short Charge
Swim

Cypress Rod

Holy Miter
Mythril Vest
Power Wrist

Blind, Poison, Foxbird, Paralyze, Sleep
Potion, Hi-Ether, Phoenix Down



KasugaiRoastedPeas
Male
Cancer
46
63
Oracle
Item
Brave Save
Throw Item
Ignore Terrain

Battle Bamboo

Thief Hat
Power Sleeve
Feather Boots

Poison, Doubt Faith, Confusion Song, Dispel Magic, Sleep
Echo Grass, Soft, Phoenix Down



0v3rr8d
Male
Aries
41
67
Lancer
Punch Art
MA Save
Short Charge
Jump+3

Spear
Ice Shield
Iron Helmet
Bronze Armor
Cursed Ring

Level Jump5, Vertical Jump6
Pummel, Earth Slash, Chakra, Revive

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DuneMeta
Monster
Serpentarius
54
50
Pisco Demon










Superdevon1
Female
Cancer
67
64
Calculator
Yin Yang Magic
Parry
Short Status
Jump+3

Madlemgen

Twist Headband
Clothes
Leather Mantle

CT, Prime Number, 4
Blind, Poison, Spell Absorb, Blind Rage, Confusion Song, Paralyze



Victoriolue
Male
Taurus
79
57
Monk
Black Magic
Abandon
Dual Wield
Move-HP Up



Green Beret
Earth Clothes
Reflect Ring

Spin Fist, Purification, Revive
Fire 3, Bolt, Ice, Ice 4



DamnThatShark
Male
Gemini
60
64
Bard
Summon Magic
Parry
Beastmaster
Jump+1

Fairy Harp

Golden Hairpin
Carabini Mail
Dracula Mantle

Angel Song, Life Song, Last Song, Hydra Pit
Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Salamander

Player: !White
Team: White Team
Palettes: White/Blue



KingJurgs
Female
Taurus
60
60
Summoner
Elemental
Mana Shield
Equip Gun
Swim

Mythril Gun

Thief Hat
Leather Outfit
Bracer

Moogle, Ramuh, Bahamut, Leviathan, Salamander, Fairy, Lich, Cyclops
Water Ball, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



RoachCarnival
Male
Leo
44
75
Monk
Item
Faith Up
Equip Shield
Move-MP Up


Buckler
Red Hood
Clothes
Bracer

Wave Fist, Purification, Revive, Seal Evil
Potion, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Bigshowens
Female
Aquarius
42
63
Lancer
Battle Skill
Auto Potion
Magic Defense UP
Move+1

Holy Lance
Diamond Shield
Bronze Helmet
Plate Mail
108 Gems

Level Jump2, Vertical Jump7
Head Break, Armor Break, Power Break, Justice Sword, Explosion Sword



Sun Pixie
Female
Scorpio
80
47
Ninja
White Magic
Speed Save
Magic Attack UP
Retreat

Short Edge
Spell Edge
Ribbon
Adaman Vest
Small Mantle

Bomb
Cure, Raise 2, Reraise, Regen, Protect, Shell, Esuna

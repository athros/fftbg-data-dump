Final Bets: purple - 8 bets for 48,460G (86.9%, x0.15); brown - 10 bets for 7,298G (13.1%, x6.64)

purple bets:
reinoe: 30,306G (62.5%, 60,612G)
just_here2: 11,702G (24.1%, 11,702G)
shinji202: 4,000G (8.3%, 115,008G)
ShintaroNayaka: 1,354G (2.8%, 1,354G)
Forkmore: 588G (1.2%, 2,082G)
neocarbuncle: 210G (0.4%, 882G)
datadrivenbot: 200G (0.4%, 62,681G)
ko2q: 100G (0.2%, 2,739G)

brown bets:
fattunaking: 2,005G (27.5%, 2,005G)
E_Ballard: 1,585G (21.7%, 1,585G)
BirbBrainsBot: 1,000G (13.7%, 125,747G)
SkylerBunny: 532G (7.3%, 532G)
itsonlyspencer: 500G (6.9%, 6,656G)
maximumcrit: 500G (6.9%, 3,813G)
WhiteTigress: 500G (6.9%, 4,329G)
getthemoneyz: 410G (5.6%, 1,701,108G)
AllInBot: 166G (2.3%, 166G)
WillFitzgerald: 100G (1.4%, 3,631G)

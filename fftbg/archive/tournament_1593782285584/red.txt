Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Female
Aquarius
40
67
Mime

Sunken State
Martial Arts
Ignore Terrain



Holy Miter
Chain Mail
Battle Boots

Mimic




GrandmasterFrankerZ
Female
Taurus
75
71
Squire
Talk Skill
Speed Save
Magic Defense UP
Teleport

Giant Axe
Genji Shield
Gold Helmet
Leather Outfit
Bracer

Dash, Heal, Tickle, Cheer Up, Fury, Ultima
Invitation, Praise, Preach, Refute



Dogsandcatsand
Female
Libra
47
43
Time Mage
Draw Out
Sunken State
Sicken
Teleport

Cypress Rod

Twist Headband
White Robe
Defense Ring

Haste, Haste 2, Slow 2, Reflect, Demi, Demi 2, Stabilize Time
Koutetsu, Murasame, Kiyomori, Muramasa



L2 Sentinel
Male
Cancer
64
48
Priest
Draw Out
Parry
Equip Axe
Jump+3

Battle Axe

Cachusha
Wizard Robe
Magic Gauntlet

Cure 2, Cure 3, Raise, Raise 2, Protect 2, Shell 2, Esuna, Holy
Koutetsu, Murasame

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Oobs56
Female
Pisces
70
60
Time Mage
Item
Sunken State
Equip Gun
Waterbreathing

Battle Folio

Flash Hat
Chain Vest
Magic Ring

Haste, Immobilize, Reflect, Demi 2, Stabilize Time
Potion, Eye Drop, Echo Grass, Phoenix Down



Skillomono
Male
Libra
49
46
Archer
White Magic
Absorb Used MP
Doublehand
Waterbreathing

Poison Bow

Leather Hat
Leather Outfit
Wizard Mantle

Charge+1, Charge+3
Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell 2, Wall



Soren Of Tyto
Female
Pisces
74
75
Oracle
Steal
Abandon
Defend
Move-HP Up

Ivory Rod

Holy Miter
Mythril Vest
Red Shoes

Silence Song, Foxbird, Confusion Song, Dark Holy
Gil Taking, Steal Heart, Steal Helmet



KonzeraLive
Female
Capricorn
44
74
Summoner
Yin Yang Magic
Abandon
Dual Wield
Lava Walking

Thunder Rod
Thunder Rod
Twist Headband
Linen Robe
Cursed Ring

Ramuh, Titan, Golem, Carbunkle, Bahamut, Fairy
Life Drain, Zombie

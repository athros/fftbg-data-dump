Player: !White
Team: White Team
Palettes: White/Blue



ColetteMSLP
Male
Libra
76
55
Samurai
White Magic
Regenerator
Defend
Ignore Height

Kiyomori

Leather Helmet
Bronze Armor
Red Shoes

Muramasa
Cure, Cure 4, Regen, Shell, Esuna, Holy



HaychDub
Female
Pisces
68
69
Thief
Item
MA Save
Equip Knife
Move-MP Up

Mythril Knife

Headgear
Chain Vest
Small Mantle

Steal Heart, Steal Armor, Steal Status
Potion, Hi-Potion, Ether, Hi-Ether, Echo Grass, Soft, Holy Water



Wyonearth
Female
Leo
66
50
Ninja
Item
Counter
Equip Knife
Lava Walking

Dagger
Kunai
Leather Hat
Mystic Vest
Battle Boots

Shuriken, Bomb, Knife
Potion, Hi-Potion, Antidote, Soft, Holy Water, Remedy



Gorgewall
Female
Sagittarius
39
70
Mime

Faith Save
Short Status
Jump+2



Red Hood
Black Costume
Bracer

Mimic


Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



BlackFireUK
Female
Cancer
40
68
Squire
Draw Out
Counter Tackle
Equip Knife
Move-MP Up

Mage Masher
Gold Shield
Iron Helmet
Black Costume
Battle Boots

Dash, Throw Stone, Heal, Tickle, Cheer Up
Asura, Murasame, Heaven's Cloud, Kiyomori



HaychDub
Female
Virgo
46
62
Mime

Damage Split
Sicken
Levitate



Feather Hat
Linen Robe
Feather Boots

Mimic




Ring Wyrm
Female
Gemini
61
76
Lancer
Basic Skill
Counter Tackle
Maintenance
Retreat

Javelin
Platinum Shield
Circlet
Gold Armor
Wizard Mantle

Level Jump8, Vertical Jump8
Heal, Tickle, Scream



Ruvelia BibeI
Female
Sagittarius
74
49
Summoner
Dance
Mana Shield
Sicken
Lava Walking

Poison Rod

Twist Headband
Wizard Robe
Germinas Boots

Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Fairy, Zodiac
Witch Hunt, Slow Dance, Polka Polka

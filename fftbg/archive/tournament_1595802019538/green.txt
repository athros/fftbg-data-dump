Player: !Green
Team: Green Team
Palettes: Green/White



StealthModeLocke
Female
Leo
72
67
Ninja
Yin Yang Magic
Parry
Beastmaster
Waterwalking

Short Edge
Orichalcum
Ribbon
Mythril Vest
Battle Boots

Knife, Staff
Blind, Poison, Doubt Faith, Zombie, Dispel Magic



Lydian C
Female
Aquarius
63
70
Time Mage
Draw Out
Hamedo
Equip Shield
Jump+1

Ivory Rod
Gold Shield
Twist Headband
Chameleon Robe
Wizard Mantle

Haste, Haste 2, Slow 2, Float, Reflect, Demi, Demi 2, Stabilize Time
Asura, Koutetsu, Bizen Boat, Heaven's Cloud



Hasterious
Female
Sagittarius
57
81
Time Mage
Basic Skill
Damage Split
Doublehand
Move+2

Battle Bamboo

Thief Hat
Linen Robe
Germinas Boots

Haste, Stop, Float, Reflect, Demi 2, Stabilize Time
Accumulate, Dash, Heal, Yell, Cheer Up, Fury, Wish, Scream



Mirapoix
Female
Capricorn
56
67
Samurai
Throw
Brave Save
Short Charge
Move+2

Gungnir

Barbuta
Gold Armor
Power Wrist

Asura, Bizen Boat, Kiyomori, Kikuichimoji
Shuriken, Hammer

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ALY327
Male
Cancer
74
77
Chemist
Punch Art
PA Save
Beastmaster
Move+1

Cute Bag

Cachusha
Leather Outfit
Feather Mantle

Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Purification, Revive



Vorap
Female
Serpentarius
44
70
Ninja
Punch Art
Catch
Equip Polearm
Jump+2

Holy Lance
Holy Lance
Red Hood
Clothes
Angel Ring

Shuriken, Bomb, Ninja Sword, Axe
Spin Fist, Wave Fist, Purification, Chakra, Seal Evil



Krasny1944
Female
Libra
77
46
Wizard
Elemental
Counter Tackle
Short Status
Ignore Height

Thunder Rod

Headgear
Mystic Vest
Spike Shoes

Fire 3, Bolt, Ice, Ice 3, Ice 4, Death
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Triskiut
Female
Scorpio
56
46
Geomancer
Yin Yang Magic
Sunken State
Beastmaster
Move+3

Giant Axe
Ice Shield
Golden Hairpin
Earth Clothes
Power Wrist

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Blind, Silence Song

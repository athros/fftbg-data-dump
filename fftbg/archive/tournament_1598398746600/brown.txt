Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Twisted Nutsatchel
Male
Scorpio
66
70
Lancer
Summon Magic
HP Restore
Equip Sword
Fly

Save the Queen

Diamond Helmet
Silk Robe
Feather Boots

Level Jump8, Vertical Jump4
Moogle, Shiva, Ifrit, Golem, Carbunkle, Fairy



MemoriesofFinal
Male
Gemini
38
61
Time Mage
Item
Counter Flood
Throw Item
Swim

Iron Fan

Holy Miter
Silk Robe
Rubber Shoes

Stop, Immobilize, Float, Reflect, Stabilize Time
Potion, Hi-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down



Killth3kid
Male
Aries
47
65
Calculator
Dragon Skill
PA Save
Doublehand
Jump+1

Bestiary

Iron Helmet
Clothes
Dracula Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Gorgewall
Male
Leo
45
77
Knight
Charge
Caution
Defense UP
Lava Walking

Ancient Sword
Ice Shield
Circlet
Carabini Mail
Defense Armlet

Magic Break, Mind Break
Charge+1, Charge+4, Charge+5

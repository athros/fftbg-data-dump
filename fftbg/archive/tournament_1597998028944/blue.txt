Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Mesmaster
Male
Serpentarius
60
54
Lancer
Sing
Damage Split
Halve MP
Jump+3

Iron Fan
Genji Shield
Genji Helmet
Gold Armor
Wizard Mantle

Level Jump4, Vertical Jump6
Life Song, Magic Song, Last Song, Diamond Blade, Space Storage



Lokenwow
Monster
Taurus
43
64
Serpentarius










Lemonjohns
Female
Capricorn
54
77
Oracle
Steal
MA Save
Short Status
Jump+3

Iron Fan

Holy Miter
White Robe
Rubber Shoes

Poison, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep, Dark Holy
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim



Lesley
Male
Gemini
79
44
Squire
Time Magic
Counter Magic
Equip Knife
Jump+1

Assassin Dagger
Bronze Shield
Genji Helmet
Leather Armor
Leather Mantle

Dash, Heal, Yell, Ultima
Haste, Haste 2, Slow 2, Stop, Demi 2

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Drusiform
Female
Taurus
55
46
Dancer
Battle Skill
HP Restore
Defense UP
Ignore Terrain

Main Gauche

Green Beret
Chameleon Robe
Genji Gauntlet

Wiznaibus, Polka Polka, Nameless Dance, Void Storage
Weapon Break, Magic Break, Mind Break, Justice Sword, Explosion Sword



Peeronid
Female
Taurus
64
45
Wizard
Draw Out
Sunken State
Sicken
Swim

Poison Rod

Green Beret
Wizard Outfit
Small Mantle

Fire, Fire 2, Fire 3, Bolt, Ice 3, Ice 4
Koutetsu, Bizen Boat, Heaven's Cloud



HuffFlex
Male
Virgo
63
54
Oracle
Charge
Mana Shield
Equip Shield
Swim

Cypress Rod
Mythril Shield
Thief Hat
Wizard Outfit
Leather Mantle

Blind, Poison, Spell Absorb, Zombie, Blind Rage, Confusion Song, Dark Holy
Charge+1, Charge+2, Charge+7, Charge+10



Gunz232323
Male
Aquarius
74
71
Archer
Yin Yang Magic
Counter Tackle
Halve MP
Swim

Glacier Gun
Crystal Shield
Feather Hat
Black Costume
Spike Shoes

Charge+1
Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Foxbird

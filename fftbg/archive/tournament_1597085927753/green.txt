Player: !Green
Team: Green Team
Palettes: Green/White



CosmicTactician
Female
Virgo
39
51
Time Mage
Steal
Damage Split
Concentrate
Move+1

Iron Fan

Twist Headband
Wizard Outfit
Defense Ring

Haste, Haste 2, Slow, Stop, Demi 2, Stabilize Time
Steal Heart, Steal Helmet, Steal Accessory, Steal Status



Dogsandcatsand
Female
Aries
77
59
Wizard
Battle Skill
Absorb Used MP
Equip Armor
Move+2

Flame Rod

Cross Helmet
Linen Robe
108 Gems

Fire 2, Fire 3, Fire 4, Bolt, Ice 2, Empower, Death, Flare
Weapon Break, Speed Break, Mind Break, Stasis Sword, Dark Sword, Surging Sword



Grininda
Male
Libra
46
54
Time Mage
Punch Art
Meatbone Slash
Equip Shield
Waterbreathing

Battle Bamboo
Mythril Shield
Headgear
Black Robe
Spike Shoes

Haste, Slow, Immobilize, Quick, Demi
Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil



Laserman1000
Female
Taurus
48
73
Summoner
Charge
Earplug
Short Charge
Jump+2

Dragon Rod

Leather Hat
Chameleon Robe
Defense Armlet

Moogle, Shiva, Odin, Leviathan, Fairy
Charge+5, Charge+10

Player: !Black
Team: Black Team
Palettes: Black/Red



RyuTsuno
Male
Aries
55
79
Calculator
Yin Yang Magic
MP Restore
Short Charge
Move+1

Battle Folio

Feather Hat
Earth Clothes
Magic Gauntlet

CT, Height, 5, 4
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Blind Rage, Dispel Magic



Firesheath
Male
Gemini
51
73
Chemist
Battle Skill
Abandon
Short Status
Jump+2

Hydra Bag

Thief Hat
Earth Clothes
Genji Gauntlet

Potion, Ether, Hi-Ether, Soft, Remedy, Phoenix Down
Armor Break, Night Sword



Jeardra
Monster
Aries
73
48
Red Panther










Sotehr
Female
Libra
45
50
Summoner
Charge
Auto Potion
Short Charge
Move+2

Oak Staff

Feather Hat
Chameleon Robe
Defense Armlet

Moogle, Titan, Odin, Leviathan, Lich, Cyclops
Charge+1, Charge+3, Charge+4, Charge+20

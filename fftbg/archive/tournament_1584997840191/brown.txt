Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DustBirdEX
Monster
Gemini
72
69
Dragon










J2DaBibbles
Male
Virgo
50
48
Squire
Throw
Absorb Used MP
Equip Armor
Fly

Blind Knife
Diamond Shield
Cross Helmet
Linen Robe
Bracer

Dash, Heal, Fury, Scream
Shuriken, Bomb, Spear



Nofreewill
Male
Taurus
66
57
Geomancer
Item
Catch
Concentrate
Move-MP Up

Mythril Sword
Round Shield
Red Hood
Clothes
Bracer

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Phoenix Down



Lanryte
Male
Gemini
46
43
Knight
Item
Damage Split
Short Status
Levitate

Sleep Sword
Hero Shield
Grand Helmet
Silk Robe
Wizard Mantle

Head Break, Shield Break, Power Break, Mind Break
Potion, Ether, Hi-Ether, Maiden's Kiss, Remedy

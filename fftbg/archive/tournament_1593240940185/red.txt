Player: !Red
Team: Red Team
Palettes: Red/Brown



Twelfthrootoftwo
Male
Aquarius
58
57
Archer
Yin Yang Magic
Abandon
Dual Wield
Move-MP Up

Stone Gun
Blast Gun
Flash Hat
Secret Clothes
Battle Boots

Charge+1
Life Drain, Doubt Faith, Foxbird, Confusion Song, Dispel Magic



CT 5 Holy
Female
Cancer
40
48
Mime

Sunken State
Dual Wield
Jump+2



Golden Hairpin
Linen Robe
Bracer

Mimic




KasugaiRoastedPeas
Male
Sagittarius
69
50
Monk
Sing
MA Save
Beastmaster
Levitate



Holy Miter
Earth Clothes
Elf Mantle

Spin Fist, Earth Slash, Purification, Chakra, Revive
Battle Song, Nameless Song



Aquios
Female
Scorpio
66
50
Summoner
Black Magic
MP Restore
Sicken
Move+3

Wizard Staff

Barette
Brigandine
Magic Gauntlet

Moogle, Titan, Carbunkle, Leviathan, Salamander, Silf
Bolt, Bolt 2

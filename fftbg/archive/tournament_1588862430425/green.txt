Player: !Green
Team: Green Team
Palettes: Green/White



DustBirdEX
Male
Libra
67
66
Bard
White Magic
Counter
Doublehand
Lava Walking

Ramia Harp

Twist Headband
Diamond Armor
N-Kai Armlet

Cheer Song, Magic Song, Nameless Song, Last Song, Diamond Blade
Cure 4, Raise, Raise 2, Reraise, Regen, Shell



Benticore
Monster
Aries
69
68
Dark Behemoth










Richardserious
Male
Scorpio
79
61
Mediator
Basic Skill
Counter Magic
Equip Polearm
Jump+3

Holy Lance

Black Hood
Linen Robe
Reflect Ring

Praise, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate
Accumulate, Dash, Heal, Tickle, Cheer Up



GrayGhostGaming
Female
Virgo
67
46
Geomancer
Steal
Brave Up
Secret Hunt
Swim

Coral Sword
Round Shield
Golden Hairpin
Mythril Vest
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Steal Heart, Steal Armor, Steal Shield

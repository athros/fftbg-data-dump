Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Falchii
Female
Taurus
73
64
Mime

Distribute
Magic Attack UP
Levitate



Holy Miter
Rubber Costume
Cursed Ring

Mimic




Rawbees
Female
Pisces
67
53
Time Mage
Steal
Absorb Used MP
Long Status
Move-HP Up

Rainbow Staff

Triangle Hat
Clothes
Dracula Mantle

Haste 2, Float, Demi 2, Stabilize Time, Galaxy Stop
Gil Taking, Steal Accessory, Arm Aim, Leg Aim



Ranmilia
Male
Libra
64
57
Squire
Yin Yang Magic
Counter
Halve MP
Jump+3

Giant Axe

Flash Hat
Black Costume
Rubber Shoes

Heal, Tickle, Cheer Up, Wish
Life Drain, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Petrify



Kalibak1
Female
Taurus
45
72
Time Mage
Yin Yang Magic
Critical Quick
Halve MP
Jump+2

Oak Staff

Barette
Black Robe
Defense Ring

Haste, Haste 2, Slow 2, Stop, Reflect, Demi, Stabilize Time
Blind, Poison, Pray Faith, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep

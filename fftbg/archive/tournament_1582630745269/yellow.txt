Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Laserman1000
Male
Gemini
71
80
Lancer
Basic Skill
Mana Shield
Dual Wield
Jump+3

Obelisk
Spear
Grand Helmet
Mythril Armor
Magic Ring

Level Jump8, Vertical Jump8
Heal, Yell, Scream



Leakimiko
Male
Sagittarius
64
63
Squire
Punch Art
Meatbone Slash
Equip Gun
Fly

Papyrus Codex
Gold Shield
Red Hood
Leather Outfit
Genji Gauntlet

Dash, Throw Stone, Heal, Cheer Up
Spin Fist, Purification, Chakra, Revive



Musclestache
Male
Sagittarius
47
47
Ninja
White Magic
Counter Flood
Equip Gun
Ignore Terrain

Battle Folio
Ramia Harp
Black Hood
Black Costume
Leather Mantle

Bomb
Cure 3, Raise, Esuna



Basmal
Male
Aries
51
47
Knight
Jump
Parry
Doublehand
Levitate

Giant Axe

Diamond Helmet
Reflect Mail
Leather Mantle

Power Break, Justice Sword, Dark Sword
Level Jump3, Vertical Jump4

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



BlackFireUK
Monster
Taurus
66
54
Holy Dragon










Polygamic
Female
Aries
64
55
Squire
Charge
Speed Save
Short Charge
Waterbreathing

Morning Star
Genji Shield
Golden Hairpin
Wizard Outfit
Rubber Shoes

Accumulate, Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish
Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+10



Galkife
Female
Aquarius
38
47
Wizard
Math Skill
Arrow Guard
Defense UP
Levitate

Wizard Rod

Red Hood
Silk Robe
Feather Boots

Fire, Fire 4, Bolt, Ice 3, Ice 4
CT, Height, Prime Number, 5, 4, 3



Jeeboheebo
Female
Scorpio
43
69
Archer
Dance
MP Restore
Defend
Retreat

Stone Gun
Aegis Shield
Cross Helmet
Wizard Outfit
Battle Boots

Charge+1, Charge+2, Charge+7, Charge+10
Witch Hunt, Disillusion, Last Dance

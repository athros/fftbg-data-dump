Player: !White
Team: White Team
Palettes: White/Blue



Lowlf
Male
Virgo
79
54
Archer
Black Magic
Speed Save
Long Status
Swim

Hunting Bow
Flame Shield
Green Beret
Chain Vest
Reflect Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Fire, Fire 3, Bolt 4, Frog, Death, Flare



Abaven Alistair
Monster
Gemini
69
45
Minotaur










Rislyeu
Female
Capricorn
61
76
Mime

Counter Tackle
Equip Armor
Fly



Twist Headband
Chain Mail
Bracer

Mimic




ExecutedGiraffe
Male
Capricorn
81
73
Lancer
Item
Regenerator
Sicken
Move-HP Up

Ivory Rod
Gold Shield
Platinum Helmet
Crystal Mail
Red Shoes

Level Jump4, Vertical Jump8
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down

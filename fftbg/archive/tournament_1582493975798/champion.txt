Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



OmnibotGamma
Monster
Leo
63
67
Minotaur










Mrdaravon
Male
Aries
64
64
Summoner
Sing
Auto Potion
Short Charge
Move+2

Oak Staff

Ribbon
Black Robe
Sprint Shoes

Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Salamander, Silf, Fairy
Last Song, Diamond Blade, Hydra Pit



Reginaldjtrotsfield
Male
Cancer
73
43
Monk
Steal
Auto Potion
Dual Wield
Move-HP Up



Black Hood
Adaman Vest
Red Shoes

Purification, Revive
Steal Heart, Steal Helmet, Steal Status, Leg Aim



Tougou
Male
Taurus
74
67
Archer
Steal
Catch
Dual Wield
Waterwalking

Hunting Bow
Cross Bow
Flash Hat
Secret Clothes
Chantage

Charge+1, Charge+10
Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Arm Aim

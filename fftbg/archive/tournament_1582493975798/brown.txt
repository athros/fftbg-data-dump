Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Anamalous Accountant
Male
Aries
75
60
Monk
Battle Skill
Counter
Long Status
Teleport



Flash Hat
Leather Outfit
Battle Boots

Earth Slash, Purification, Seal Evil
Head Break, Armor Break, Shield Break, Weapon Break, Mind Break



Arachness
Male
Aries
80
56
Geomancer
White Magic
Counter
Equip Gun
Jump+1

Mythril Gun
Bronze Shield
Triangle Hat
Linen Robe
Germinas Boots

Hallowed Ground, Will-O-Wisp, Blizzard, Gusty Wind
Cure, Cure 2, Raise, Raise 2, Regen, Wall, Esuna



Mrdaravon
Monster
Libra
80
59
Red Dragon










Kennymillennium
Male
Aries
71
65
Wizard
Throw
MP Restore
Long Status
Jump+2

Orichalcum

Thief Hat
Wizard Robe
Feather Boots

Fire 3, Bolt 2, Bolt 3, Bolt 4, Empower
Shuriken, Stick

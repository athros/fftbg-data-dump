Player: !Black
Team: Black Team
Palettes: Black/Red



Old Overholt
Male
Sagittarius
77
59
Monk
White Magic
PA Save
Defend
Jump+2



Barette
Mystic Vest
Bracer

Spin Fist, Purification, Chakra, Revive
Raise, Reraise, Protect 2, Wall, Esuna, Holy



IndecisiveNinja
Female
Aries
54
66
Oracle
Item
Auto Potion
Defense UP
Move-HP Up

Papyrus Codex

Flash Hat
Clothes
Elf Mantle

Spell Absorb, Silence Song, Dispel Magic, Paralyze, Sleep
Potion, Hi-Potion, Echo Grass, Soft, Remedy, Phoenix Down



Mpghappiness
Male
Scorpio
68
73
Mediator
Steal
Hamedo
Equip Sword
Ignore Terrain

Mythril Sword

Green Beret
Black Robe
Small Mantle

Invitation, Persuade, Solution, Negotiate, Mimic Daravon, Rehabilitate
Gil Taking, Steal Weapon, Leg Aim



SerumD
Female
Virgo
66
38
Chemist
Throw
Auto Potion
Magic Defense UP
Move+2

Blast Gun

Headgear
Power Sleeve
Small Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Maiden's Kiss, Holy Water, Phoenix Down
Ninja Sword

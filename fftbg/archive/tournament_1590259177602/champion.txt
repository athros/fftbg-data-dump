Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Namamu
Female
Gemini
70
52
Monk
Throw
HP Restore
Doublehand
Teleport



Green Beret
Black Costume
Wizard Mantle

Spin Fist, Earth Slash, Purification, Seal Evil
Bomb



Baron Von Scrub
Male
Gemini
67
67
Monk
Draw Out
Sunken State
Equip Gun
Lava Walking

Fairy Harp

Thief Hat
Secret Clothes
Battle Boots

Spin Fist, Purification, Chakra, Revive
Bizen Boat, Heaven's Cloud



Mpghappiness
Female
Sagittarius
62
67
Priest
Time Magic
Faith Save
Sicken
Waterwalking

Wizard Staff

Triangle Hat
Adaman Vest
Feather Mantle

Cure 2, Raise, Protect 2, Wall, Esuna
Haste, Slow, Slow 2, Stop, Float, Quick



Grimmace45
Female
Virgo
74
53
Geomancer
Jump
Catch
Equip Gun
Teleport 2

Papyrus Codex
Gold Shield
Feather Hat
Leather Outfit
Germinas Boots

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump7

Player: !Black
Team: Black Team
Palettes: Black/Red



BoneMiser
Monster
Aries
71
40
Grenade










Megakim
Female
Leo
71
53
Squire
White Magic
Brave Save
Equip Sword
Jump+2

Kikuichimoji
Aegis Shield
Genji Helmet
Chain Mail
Germinas Boots

Throw Stone, Heal, Yell, Cheer Up, Fury
Cure, Cure 3, Raise, Reraise, Regen, Shell 2, Esuna



Error72
Male
Gemini
60
71
Monk
Steal
Counter
Doublehand
Lava Walking



Leather Hat
Adaman Vest
Bracer

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Gil Taking, Steal Accessory



Powergems
Female
Scorpio
52
56
Geomancer
Basic Skill
HP Restore
Martial Arts
Move-MP Up


Escutcheon
Golden Hairpin
Clothes
Magic Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Heal, Yell, Fury, Wish, Scream

Player: !White
Team: White Team
Palettes: White/Blue



Itszaane
Female
Taurus
44
56
Chemist
Charge
Meatbone Slash
Secret Hunt
Jump+1

Hydra Bag

Cachusha
Wizard Outfit
Angel Ring

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Charge+3, Charge+4, Charge+5, Charge+7, Charge+10



Oreo Pizza
Male
Aries
54
68
Time Mage
White Magic
Speed Save
Equip Sword
Lava Walking

Platinum Sword

Leather Hat
Wizard Robe
Spike Shoes

Haste, Slow, Slow 2, Immobilize, Float, Quick, Demi
Cure, Raise, Raise 2, Reraise, Regen, Protect 2, Esuna



Aurosaerondo
Male
Taurus
43
54
Ninja
Jump
Parry
Concentrate
Swim

Flail
Morning Star
Holy Miter
Mythril Vest
Chantage

Shuriken, Bomb, Dictionary
Level Jump2, Vertical Jump4



Nekojin
Female
Pisces
63
77
Time Mage
Jump
Caution
Equip Knife
Move+3

Orichalcum

Feather Hat
Black Robe
Feather Mantle

Haste, Slow, Slow 2, Stop, Float, Quick, Demi, Stabilize Time, Meteor
Level Jump3, Vertical Jump2

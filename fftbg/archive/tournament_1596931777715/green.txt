Player: !Green
Team: Green Team
Palettes: Green/White



Amorial
Female
Aquarius
57
67
Monk
Dance
Counter Flood
Equip Polearm
Waterwalking

Musk Rod

Leather Hat
Black Costume
Battle Boots

Spin Fist, Wave Fist, Chakra, Seal Evil
Witch Hunt, Polka Polka



Smashy
Male
Aries
78
48
Ninja
Time Magic
Brave Save
Equip Polearm
Waterwalking

Cypress Rod
Battle Bamboo
Ribbon
Black Costume
Magic Ring

Shuriken, Spear
Haste 2, Stop, Reflect, Demi 2, Stabilize Time



Kronikle
Male
Aries
75
64
Monk
Steal
Absorb Used MP
Dual Wield
Waterwalking



Black Hood
Brigandine
Angel Ring

Pummel, Purification
Steal Helmet, Steal Accessory, Leg Aim



CosmicTactician
Female
Aquarius
62
58
Squire
Charge
Distribute
Short Status
Teleport

Hunting Bow
Flame Shield
Thief Hat
Chain Mail
Bracer

Accumulate, Throw Stone, Heal, Tickle, Fury
Charge+1, Charge+2, Charge+3, Charge+7, Charge+10

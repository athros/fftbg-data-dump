Final Bets: blue - 9 bets for 6,203G (72.9%, x0.37); green - 7 bets for 2,302G (27.1%, x2.69)

blue bets:
Rytor: 1,243G (20.0%, 1,243G)
EnemyController: 1,000G (16.1%, 611,633G)
Oobs56: 943G (15.2%, 943G)
Phi_Sig: 840G (13.5%, 840G)
DLJuggernaut: 750G (12.1%, 23,975G)
HaateXIII: 512G (8.3%, 512G)
killth3kid: 450G (7.3%, 5,187G)
DuraiPapers: 365G (5.9%, 365G)
datadrivenbot: 100G (1.6%, 44,188G)

green bets:
BirbBrainsBot: 720G (31.3%, 69,585G)
Evewho: 630G (27.4%, 630G)
dogsandcatsand: 364G (15.8%, 364G)
Azelgrim: 224G (9.7%, 224G)
getthemoneyz: 164G (7.1%, 970,552G)
AllInBot: 100G (4.3%, 100G)
IphoneDarkness: 100G (4.3%, 4,251G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WitchHunterIX
Monster
Sagittarius
79
73
Ultima Demon










Twelfthrootoftwo
Male
Cancer
74
60
Oracle
Basic Skill
Regenerator
Beastmaster
Waterwalking

Bestiary

Headgear
Black Robe
Spike Shoes

Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Dark Holy
Dash, Heal, Yell, Cheer Up, Fury



Skillomono
Male
Aries
47
78
Archer
Throw
Counter Magic
Doublehand
Jump+2

Cross Bow

Barbuta
Earth Clothes
108 Gems

Charge+1, Charge+2, Charge+4
Shuriken



Deathmaker06
Male
Libra
43
47
Samurai
Black Magic
Catch
Equip Armor
Move+1

Muramasa

Feather Hat
Linen Cuirass
Chantage

Koutetsu, Kiyomori
Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4, Death

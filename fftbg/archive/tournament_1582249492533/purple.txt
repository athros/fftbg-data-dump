Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



BoneMiser
Male
Pisces
64
67
Monk
Charge
Sunken State
Equip Polearm
Move+3

Gokuu Rod

Feather Hat
Earth Clothes
Battle Boots

Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Charge+3, Charge+5, Charge+7, Charge+20



Meyples
Female
Libra
39
76
Summoner
White Magic
PA Save
Equip Shield
Teleport

Poison Rod
Round Shield
Red Hood
Wizard Robe
Red Shoes

Moogle, Shiva, Titan, Salamander
Cure 2, Cure 3, Cure 4, Raise, Reraise, Regen, Shell, Shell 2, Wall



ShinoGuy
Female
Sagittarius
43
78
Ninja
Item
Earplug
Equip Sword
Fly

Koga Knife
Heaven's Cloud
Green Beret
Earth Clothes
Battle Boots

Shuriken, Sword, Staff, Spear
X-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Neuros91
Male
Libra
48
57
Archer
White Magic
Caution
Defense UP
Jump+2

Long Bow

Triangle Hat
Leather Outfit
Battle Boots

Charge+5
Cure, Cure 3, Cure 4, Raise, Reraise, Protect 2, Esuna

Player: !Green
Team: Green Team
Palettes: Green/White



Rislyeu
Female
Gemini
79
72
Mime

Caution
Equip Shield
Jump+2


Kaiser Plate
Feather Hat
Clothes
Wizard Mantle

Mimic




MrFlabyo
Male
Cancer
76
71
Mime

Blade Grasp
Magic Defense UP
Swim



Twist Headband
Earth Clothes
Feather Boots

Mimic




SeniorBunk
Female
Aquarius
53
62
Wizard
Throw
Dragon Spirit
Equip Armor
Waterwalking

Dragon Rod

Gold Helmet
Power Sleeve
Magic Gauntlet

Fire 2, Fire 3, Bolt, Bolt 4, Ice 2
Bomb, Knife



Jeeboheebo
Male
Sagittarius
80
65
Chemist
Charge
Auto Potion
Doublehand
Lava Walking

Cute Bag

Holy Miter
Mythril Vest
Jade Armlet

Potion, Ether, Hi-Ether, Eye Drop, Echo Grass
Charge+2, Charge+3, Charge+4, Charge+10

Player: !Red
Team: Red Team
Palettes: Red/Brown



DustyRelic
Male
Virgo
50
53
Lancer
Battle Skill
Parry
Short Status
Teleport

Spear
Gold Shield
Cross Helmet
Chain Mail
Angel Ring

Level Jump8, Vertical Jump6
Shield Break, Magic Break, Power Break, Night Sword



Marin1987
Male
Aquarius
47
67
Oracle
Draw Out
Absorb Used MP
Short Status
Levitate

Musk Rod

Golden Hairpin
Linen Robe
Diamond Armlet

Blind, Poison, Spell Absorb, Blind Rage, Dispel Magic, Paralyze
Asura, Bizen Boat, Heaven's Cloud



Bruubarg
Female
Pisces
59
78
Oracle
Jump
Earplug
Equip Shield
Levitate

Musk Rod
Flame Shield
Ribbon
Power Sleeve
Reflect Ring

Blind, Poison, Spell Absorb, Doubt Faith, Foxbird, Dispel Magic, Sleep
Level Jump4, Vertical Jump7



Just Here2
Female
Sagittarius
56
46
Squire
Time Magic
Caution
Doublehand
Levitate

Hunting Bow

Black Hood
Black Costume
Reflect Ring

Throw Stone, Heal, Cheer Up, Scream
Haste, Slow, Stabilize Time

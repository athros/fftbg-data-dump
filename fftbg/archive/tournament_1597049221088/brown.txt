Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Douchetron
Female
Libra
51
44
Summoner
Dance
MP Restore
Short Charge
Waterbreathing

Thunder Rod

Leather Hat
Earth Clothes
N-Kai Armlet

Ramuh, Titan, Golem, Leviathan, Silf, Cyclops
Slow Dance, Disillusion, Nether Demon



Reinoe
Male
Serpentarius
76
52
Knight
Black Magic
Absorb Used MP
Dual Wield
Move+3

Coral Sword
Rune Blade
Platinum Helmet
White Robe
Dracula Mantle

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Justice Sword, Dark Sword, Explosion Sword
Ice 4



Mirapoix
Male
Pisces
68
62
Priest
Summon Magic
Counter Flood
Doublehand
Teleport 2

Flail

Flash Hat
Mythril Vest
Wizard Mantle

Cure 3, Raise, Reraise, Protect, Protect 2, Shell 2, Wall, Esuna
Shiva, Ifrit, Golem, Carbunkle, Bahamut, Fairy



Anatoly
Male
Aquarius
50
78
Calculator
Time Magic
Blade Grasp
Beastmaster
Move+1

Panther Bag

Feather Hat
Adaman Vest
Feather Mantle

CT, Height, 5, 3
Haste, Haste 2, Slow, Immobilize, Quick

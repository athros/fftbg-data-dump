Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Male
Capricorn
56
75
Thief
Battle Skill
Meatbone Slash
Equip Sword
Waterbreathing

Save the Queen

Black Hood
Earth Clothes
Defense Armlet

Gil Taking, Steal Heart
Shield Break, Speed Break, Power Break, Mind Break, Dark Sword



Thaetreis
Female
Sagittarius
68
49
Chemist
Steal
Mana Shield
Short Charge
Move+2

Hydra Bag

Red Hood
Chain Vest
Feather Mantle

Potion, Hi-Potion, X-Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down
Steal Helmet, Steal Weapon



Nine07
Female
Taurus
65
49
Mediator
White Magic
Absorb Used MP
Attack UP
Move+1

Stone Gun

Twist Headband
Power Sleeve
Red Shoes

Invitation, Persuade, Death Sentence, Negotiate, Rehabilitate
Cure, Cure 3, Cure 4, Reraise, Protect, Shell 2



Ko2q
Male
Pisces
56
61
Oracle
Item
Arrow Guard
Defense UP
Levitate

Gokuu Rod

Black Hood
Black Robe
Feather Boots

Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Paralyze, Sleep, Petrify, Dark Holy
Potion, Hi-Ether, Maiden's Kiss, Holy Water, Phoenix Down

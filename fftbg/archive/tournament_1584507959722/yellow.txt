Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Meta Five
Male
Virgo
72
49
Calculator
Time Magic
Abandon
Martial Arts
Move+3



Flash Hat
Adaman Vest
Magic Ring

CT, Height, Prime Number, 5, 3
Haste, Haste 2, Slow 2, Quick, Demi 2, Stabilize Time



Summmmmmmmmm
Male
Virgo
65
55
Ninja
Battle Skill
Parry
Equip Shield
Waterbreathing

Flail
Bronze Shield
Golden Hairpin
Adaman Vest
Dracula Mantle

Shuriken, Spear
Magic Break, Speed Break, Power Break, Mind Break



LeepingJJ
Male
Libra
49
45
Ninja
Elemental
MA Save
Sicken
Move+1

Main Gauche
Flail
Leather Hat
Earth Clothes
Magic Ring

Shuriken
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Tougou
Female
Scorpio
40
69
Geomancer
Time Magic
Counter
Magic Defense UP
Lava Walking

Giant Axe
Genji Shield
Twist Headband
Leather Outfit
Genji Gauntlet

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Haste, Slow 2, Demi 2, Stabilize Time

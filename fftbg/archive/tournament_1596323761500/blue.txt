Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Eoika
Female
Libra
51
57
Summoner
Time Magic
MP Restore
Equip Sword
Fly

Long Sword

Holy Miter
Secret Clothes
Feather Boots

Moogle, Ifrit, Titan, Golem, Bahamut, Leviathan
Haste, Stop, Float, Quick, Demi 2, Stabilize Time



Randgridr
Female
Gemini
57
46
Knight
Yin Yang Magic
Absorb Used MP
Long Status
Move+3

Long Sword
Buckler
Leather Helmet
Crystal Mail
108 Gems

Head Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Dark Sword
Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep, Dark Holy



DeathTaxesAndAnime
Female
Gemini
67
58
Mime

Counter Tackle
Dual Wield
Jump+2



Triangle Hat
Earth Clothes
Magic Gauntlet

Mimic




ForagerCats
Female
Libra
44
67
Geomancer
Black Magic
Damage Split
Sicken
Ignore Terrain

Giant Axe
Diamond Shield
Headgear
Silk Robe
Sprint Shoes

Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Fire 4, Bolt 4, Ice 3, Death

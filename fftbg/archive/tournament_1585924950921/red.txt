Player: !Red
Team: Red Team
Palettes: Red/Brown



StealthModeLocke
Male
Scorpio
60
56
Lancer
Black Magic
Distribute
Equip Gun
Fly

Blaze Gun
Escutcheon
Barbuta
Chameleon Robe
Bracer

Level Jump2, Vertical Jump2
Fire 4, Bolt 3, Bolt 4, Ice, Ice 2



Fenixcrest
Male
Virgo
57
52
Mime

Critical Quick
Martial Arts
Jump+2



Flash Hat
Secret Clothes
Defense Armlet

Mimic




Loveyouallfriends
Female
Scorpio
41
80
Archer
Summon Magic
Counter Flood
Short Charge
Ignore Height

Glacier Gun
Mythril Shield
Twist Headband
Mystic Vest
108 Gems

Charge+1, Charge+2, Charge+5, Charge+7
Moogle, Ramuh, Titan, Golem, Carbunkle, Odin



Lanshaft
Female
Scorpio
54
69
Monk
Dance
MA Save
Magic Attack UP
Ignore Height



Green Beret
Earth Clothes
Magic Ring

Wave Fist, Earth Slash, Revive
Wiznaibus, Disillusion, Last Dance

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Primeval 33
Male
Scorpio
43
51
Thief
Punch Art
Faith Save
Short Charge
Retreat

Orichalcum

Holy Miter
Judo Outfit
Setiemson

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Pummel, Wave Fist, Earth Slash, Purification, Seal Evil



StealthModeLocke
Female
Gemini
77
56
Dancer
White Magic
MA Save
Martial Arts
Swim



Headgear
Wizard Outfit
Jade Armlet

Slow Dance, Polka Polka, Nether Demon, Dragon Pit
Cure 3, Raise 2, Shell, Shell 2, Wall, Esuna



Zbgs
Male
Aquarius
53
58
Chemist
Jump
Parry
Dual Wield
Waterwalking

Assassin Dagger
Orichalcum
Barette
Secret Clothes
Jade Armlet

X-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Level Jump5, Vertical Jump3



Metagameface
Male
Cancer
63
76
Calculator
Time Magic
Absorb Used MP
Maintenance
Teleport

Papyrus Codex

Holy Miter
Wizard Outfit
Red Shoes

CT, 4, 3
Haste, Slow, Quick, Demi 2

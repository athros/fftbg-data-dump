Player: !Black
Team: Black Team
Palettes: Black/Red



Nifboy
Male
Leo
46
73
Squire
Throw
PA Save
Short Charge
Levitate

Scorpion Tail
Gold Shield
Black Hood
Leather Outfit
Rubber Shoes

Heal, Fury
Hammer



UmaiJam
Male
Scorpio
45
71
Archer
Talk Skill
Regenerator
Doublehand
Teleport

Windslash Bow

Leather Hat
Leather Outfit
Defense Ring

Charge+3, Charge+5, Charge+7
Invitation, Preach, Solution, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



SeniorBunk
Female
Aquarius
58
73
Oracle
Throw
Auto Potion
Martial Arts
Move+1



Feather Hat
Linen Robe
Cursed Ring

Poison, Life Drain, Doubt Faith, Foxbird, Dark Holy
Bomb, Knife, Ninja Sword, Wand, Dictionary



Grininda
Male
Gemini
46
59
Archer
Sing
Parry
Doublehand
Jump+1

Night Killer

Feather Hat
Judo Outfit
Dracula Mantle

Charge+1, Charge+3, Charge+4
Cheer Song, Battle Song, Last Song

Player: !Red
Team: Red Team
Palettes: Red/Brown



LAGBOT30000
Male
Aries
52
73
Samurai
Charge
Brave Save
Beastmaster
Ignore Terrain

Koutetsu Knife

Leather Helmet
White Robe
Defense Ring

Asura, Kiyomori
Charge+1, Charge+2, Charge+4, Charge+5



Verratic
Male
Cancer
69
62
Lancer
Summon Magic
Parry
Secret Hunt
Lava Walking

Musk Rod
Genji Shield
Gold Helmet
Bronze Armor
Diamond Armlet

Level Jump5, Vertical Jump8
Moogle, Ramuh, Titan, Golem, Leviathan



Gorgewall
Male
Cancer
51
42
Ninja
Punch Art
Counter
Sicken
Waterwalking

Kunai
Flame Whip
Golden Hairpin
Leather Outfit
Elf Mantle

Shuriken, Bomb, Knife, Hammer
Seal Evil



ZZ Yoshi
Female
Sagittarius
68
47
Calculator
Robosnake Skill
Absorb Used MP
Equip Axe
Waterwalking

Flail
Diamond Shield
Cross Helmet
Crystal Mail
Setiemson

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm

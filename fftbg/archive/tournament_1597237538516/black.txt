Player: !Black
Team: Black Team
Palettes: Black/Red



RunicMagus
Male
Taurus
60
50
Knight
White Magic
MA Save
Short Charge
Ignore Height

Iron Sword
Genji Shield
Iron Helmet
Crystal Mail
Defense Ring

Shield Break, Speed Break, Mind Break
Cure, Cure 4, Raise, Reraise, Protect 2, Wall, Esuna



LordTomS
Female
Gemini
42
79
Squire
White Magic
Hamedo
Attack UP
Lava Walking

Ice Brand
Diamond Shield
Mythril Helmet
Brigandine
Wizard Mantle

Heal, Cheer Up
Raise, Reraise, Regen, Protect 2, Shell, Wall, Esuna



WhiteTigress
Female
Gemini
50
65
Dancer
Jump
Auto Potion
Halve MP
Fly

Star Bag

Headgear
Wizard Robe
108 Gems

Slow Dance, Nameless Dance, Nether Demon
Level Jump8, Vertical Jump5



Galkife
Monster
Leo
72
74
Taiju








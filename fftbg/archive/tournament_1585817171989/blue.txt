Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Bad1dea
Female
Capricorn
56
39
Mediator
Jump
Counter Tackle
Maintenance
Levitate

Romanda Gun

Black Hood
Mystic Vest
Leather Mantle

Persuade, Praise, Preach, Negotiate, Rehabilitate
Level Jump8, Vertical Jump2



Mushywaffles
Male
Libra
72
68
Lancer
White Magic
Damage Split
Long Status
Jump+1

Obelisk
Platinum Shield
Gold Helmet
Genji Armor
Small Mantle

Level Jump5, Vertical Jump2
Raise, Regen, Shell 2



Herrshaun
Female
Cancer
65
74
Mediator
Elemental
Speed Save
Martial Arts
Move-HP Up

Air Knife

Leather Hat
White Robe
Red Shoes

Preach, Solution, Rehabilitate
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Lava Ball



ArcFox4
Female
Pisces
47
61
Lancer
Item
Caution
Beastmaster
Fly

Gungnir
Gold Shield
Genji Helmet
White Robe
Battle Boots

Level Jump2, Vertical Jump4
X-Potion, Echo Grass, Remedy, Phoenix Down

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Omegasuspekt
Male
Libra
55
71
Ninja
Battle Skill
Counter Flood
Equip Armor
Lava Walking

Flail
Scorpion Tail
Circlet
Chain Mail
N-Kai Armlet

Shuriken, Bomb, Knife
Weapon Break, Speed Break



Hirine
Female
Gemini
73
63
Wizard
Battle Skill
Speed Save
Halve MP
Ignore Height

Dagger

Holy Miter
Silk Robe
Cherche

Bolt 3, Ice 4, Frog
Head Break, Shield Break, Weapon Break, Magic Break, Speed Break, Dark Sword



Boku No Tabiyume
Male
Capricorn
54
68
Chemist
Time Magic
Dragon Spirit
Doublehand
Fly

Romanda Gun

Golden Hairpin
Power Sleeve
Sprint Shoes

Hi-Potion, Ether, Antidote, Eye Drop, Holy Water, Remedy, Phoenix Down
Demi 2



Ratchety
Female
Aries
65
60
Samurai
Yin Yang Magic
Dragon Spirit
Equip Armor
Move-MP Up

Javelin

Leather Hat
Black Costume
Sprint Shoes

Bizen Boat
Pray Faith, Doubt Faith, Zombie, Silence Song, Foxbird, Paralyze, Sleep

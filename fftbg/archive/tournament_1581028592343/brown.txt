Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Novapuppets
Male
Virgo
51
44
Chemist
Steal
MA Save
Maintenance
Teleport

Panther Bag

Triangle Hat
Chain Vest
Spike Shoes

Potion, Hi-Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Steal Armor, Steal Shield, Steal Weapon



Lionhermit
Female
Pisces
51
80
Geomancer
Throw
Damage Split
Equip Bow
Move+3

Night Killer
Ice Shield
Golden Hairpin
Leather Vest
Leather Mantle

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shuriken, Knife, Sword



Haamis
Male
Capricorn
61
60
Chemist
Time Magic
Arrow Guard
Equip Armor
Waterbreathing

Cute Bag

Leather Hat
Light Robe
Angel Ring

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Phoenix Down
Haste 2, Slow, Reflect, Demi 2, Stabilize Time



Nizaha
Male
Sagittarius
72
42
Thief
Talk Skill
Speed Save
Halve MP
Retreat

Rune Blade

Black Hood
Wizard Outfit
N-Kai Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Status
Praise, Threaten, Solution, Death Sentence, Refute

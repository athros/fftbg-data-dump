Player: !Red
Team: Red Team
Palettes: Red/Brown



NIghtdew14
Female
Cancer
63
41
Dancer
Battle Skill
Earplug
Dual Wield
Move-HP Up

Persia
Ryozan Silk
Red Hood
Chameleon Robe
Bracer

Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Void Storage, Nether Demon, Dragon Pit
Shield Break, Speed Break, Power Break, Surging Sword



Alaylle
Female
Sagittarius
72
59
Summoner
Charge
Counter
Concentrate
Move-HP Up

Rod

Twist Headband
Leather Outfit
Battle Boots

Moogle, Golem, Carbunkle
Charge+2, Charge+10, Charge+20



Fspll
Female
Aquarius
50
48
Wizard
Draw Out
Faith Up
Magic Attack UP
Retreat

Assassin Dagger

Leather Hat
Linen Robe
Reflect Ring

Fire, Fire 2, Bolt, Bolt 3, Ice 2, Ice 3, Flare
Asura, Heaven's Cloud, Kiyomori, Muramasa



HawkSick
Female
Virgo
57
55
Calculator
Yin Yang Magic
Regenerator
Equip Knife
Move-HP Up

Main Gauche

Red Hood
Chain Vest
Germinas Boots

CT, Height, Prime Number, 4
Blind, Spell Absorb, Sleep

Player: !Green
Team: Green Team
Palettes: Green/White



RNdOrchestra
Male
Gemini
73
73
Wizard
Draw Out
Counter Magic
Concentrate
Fly

Ice Rod

Feather Hat
Linen Robe
Power Wrist

Fire 2, Fire 3, Bolt 2, Empower, Death
Heaven's Cloud, Kiyomori, Muramasa



Kingchadking
Male
Pisces
42
46
Wizard
Sing
Mana Shield
Equip Shield
Ignore Height

Ice Rod
Gold Shield
Ribbon
Linen Robe
Dracula Mantle

Fire, Fire 2, Bolt, Ice 2, Ice 3
Angel Song, Battle Song, Nameless Song, Last Song



TheKillerNacho
Female
Cancer
47
40
Summoner
Basic Skill
Hamedo
Defend
Move-MP Up

White Staff

Green Beret
Chameleon Robe
Power Wrist

Shiva, Ramuh, Golem, Carbunkle, Odin, Leviathan, Silf, Lich
Dash, Heal



DavenIII
Male
Aquarius
41
55
Mime

HP Restore
Martial Arts
Move-HP Up



Flash Hat
Leather Outfit
Power Wrist

Mimic


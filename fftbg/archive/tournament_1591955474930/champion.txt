Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



D4rr1n
Female
Taurus
61
68
Lancer
Punch Art
Counter Tackle
Doublehand
Jump+2

Javelin

Leather Helmet
Linen Cuirass
Elf Mantle

Level Jump8, Vertical Jump8
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive



Prince Rogers Nelson
Male
Cancer
59
50
Ninja
Draw Out
Speed Save
Equip Gun
Move+3

Stone Gun
Blaze Gun
Holy Miter
Leather Outfit
Rubber Shoes

Knife, Spear
Bizen Boat, Kiyomori, Muramasa



Genericco
Male
Virgo
59
80
Time Mage
Charge
Absorb Used MP
Dual Wield
Fly

Gold Staff
White Staff
Leather Hat
Black Costume
Genji Gauntlet

Haste, Slow 2, Stop, Float, Reflect, Stabilize Time
Charge+1, Charge+2, Charge+3, Charge+5, Charge+20



Solomongrundy85
Male
Aquarius
38
63
Time Mage
Draw Out
Brave Save
Magic Attack UP
Fly

Cypress Rod

Flash Hat
Earth Clothes
Dracula Mantle

Haste, Slow 2, Stop, Reflect, Quick, Demi, Stabilize Time
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud

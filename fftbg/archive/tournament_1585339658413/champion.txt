Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Fenixcrest
Female
Taurus
75
79
Monk
Elemental
Abandon
Dual Wield
Jump+1



Green Beret
Black Costume
Germinas Boots

Pummel, Earth Slash, Purification, Revive
Pitfall, Water Ball, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



Ko2q
Male
Cancer
80
40
Knight
Throw
Damage Split
Defend
Waterbreathing

Ragnarok
Mythril Shield
Leather Helmet
Carabini Mail
Sprint Shoes

Shield Break, Speed Break, Power Break, Justice Sword
Shuriken, Knife, Sword, Hammer, Staff



Chuckolator
Female
Taurus
77
45
Geomancer
Basic Skill
Sunken State
Doublehand
Move-MP Up

Kikuichimoji

Twist Headband
Silk Robe
Sprint Shoes

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Dash, Heal, Cheer Up, Fury



Buddychrist10
Female
Sagittarius
49
64
Priest
Throw
Blade Grasp
Sicken
Move+1

Healing Staff

Twist Headband
Light Robe
Dracula Mantle

Cure 2, Raise, Raise 2, Protect, Protect 2, Shell 2, Esuna
Shuriken

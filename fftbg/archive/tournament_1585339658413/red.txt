Player: !Red
Team: Red Team
Palettes: Red/Brown



Theallmightyensiron
Male
Libra
57
71
Bard
Charge
MP Restore
Halve MP
Swim

Ramia Harp

Leather Hat
Chain Vest
Jade Armlet

Angel Song, Cheer Song, Battle Song, Magic Song, Last Song
Charge+1, Charge+2, Charge+3, Charge+7



Urieltheflameofgod
Male
Taurus
73
64
Summoner
Throw
PA Save
Equip Shield
Move-HP Up

Thunder Rod
Escutcheon
Twist Headband
Black Robe
N-Kai Armlet

Shiva, Ramuh, Ifrit, Lich
Spear, Dictionary



Phi Sig
Male
Virgo
39
76
Summoner
Steal
Counter Flood
Defense UP
Move-MP Up

Dragon Rod

Ribbon
Secret Clothes
Wizard Mantle

Moogle, Ramuh, Carbunkle, Leviathan, Salamander, Silf
Steal Armor, Steal Accessory



Rockmem21
Male
Leo
70
64
Samurai
Elemental
Regenerator
Secret Hunt
Waterbreathing

Bizen Boat

Iron Helmet
Crystal Mail
Red Shoes

Heaven's Cloud, Muramasa
Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball

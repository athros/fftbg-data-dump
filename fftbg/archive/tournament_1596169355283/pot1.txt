Final Bets: red - 13 bets for 7,177G (32.4%, x2.09); blue - 17 bets for 14,979G (67.6%, x0.48)

red bets:
pandasforsale: 1,743G (24.3%, 1,743G)
douchetron: 1,386G (19.3%, 2,719G)
Lydian_C: 1,200G (16.7%, 2,249G)
sinnyil2: 1,028G (14.3%, 2,057G)
getthemoneyz: 460G (6.4%, 1,446,279G)
rednecknazgul: 228G (3.2%, 228G)
Foamsoldier: 204G (2.8%, 204G)
AllInBot: 200G (2.8%, 200G)
reddwind_: 200G (2.8%, 20,418G)
TasisSai: 200G (2.8%, 12,800G)
MiseryKoss: 128G (1.8%, 128G)
Firesheath: 100G (1.4%, 6,199G)
TheseJeans: 100G (1.4%, 928G)

blue bets:
Mesmaster: 3,000G (20.0%, 43,197G)
randgridr: 2,725G (18.2%, 2,725G)
Zeroroute: 1,983G (13.2%, 1,983G)
evontno: 1,200G (8.0%, 25,230G)
poGpopE: 1,000G (6.7%, 45,034G)
BirbBrainsBot: 1,000G (6.7%, 53,687G)
resjudicata3: 1,000G (6.7%, 3,457G)
NovaKnight21: 678G (4.5%, 678G)
NIghtdew14: 600G (4.0%, 600G)
nhammen: 400G (2.7%, 27,026G)
Grandlanzer: 300G (2.0%, 124,765G)
Arcblazer23: 288G (1.9%, 288G)
Lythe_Caraker: 250G (1.7%, 180,611G)
Chambs12: 248G (1.7%, 3,064G)
datadrivenbot: 200G (1.3%, 46,853G)
DHaveWord: 100G (0.7%, 3,703G)
dtrain332: 7G (0.0%, 736G)

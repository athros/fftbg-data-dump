Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Maakur
Female
Capricorn
80
56
Lancer
Basic Skill
Parry
Short Charge
Move+1

Holy Lance
Flame Shield
Bronze Helmet
Plate Mail
Small Mantle

Level Jump2, Vertical Jump8
Accumulate, Dash, Heal, Fury



Mtueni
Monster
Taurus
71
61
Draugr










CandyTamer
Female
Sagittarius
73
75
Monk
Summon Magic
Counter
Secret Hunt
Teleport 2



Black Hood
Earth Clothes
Defense Ring

Pummel, Wave Fist, Earth Slash, Secret Fist, Chakra, Revive
Moogle, Shiva, Titan, Golem, Silf



Jaritras
Male
Capricorn
63
45
Bard
Draw Out
Counter Flood
Equip Polearm
Waterwalking

Ivory Rod

Twist Headband
Platinum Armor
108 Gems

Battle Song, Last Song, Diamond Blade, Hydra Pit
Asura, Heaven's Cloud, Kiyomori, Kikuichimoji

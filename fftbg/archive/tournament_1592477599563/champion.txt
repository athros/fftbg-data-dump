Player: !zChamp
Team: Champion Team
Palettes: Black/Red



HaychDub
Male
Scorpio
41
54
Knight
Black Magic
Arrow Guard
Magic Defense UP
Ignore Height

Defender
Platinum Shield
Iron Helmet
Silk Robe
Salty Rage

Head Break, Armor Break, Shield Break, Justice Sword
Fire 2, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 3



DuraiPapers
Male
Pisces
69
76
Ninja
Steal
Counter Tackle
Equip Axe
Move+3

Flail
Battle Axe
Green Beret
Power Sleeve
Feather Mantle

Knife, Hammer, Axe
Steal Shield



Kaidykat
Male
Pisces
64
58
Knight
Punch Art
PA Save
Equip Sword
Retreat

Iron Sword
Crystal Shield
Diamond Helmet
Mythril Armor
Power Wrist

Head Break, Armor Break, Weapon Break, Mind Break, Dark Sword, Night Sword
Spin Fist, Pummel, Purification, Chakra, Revive



Grininda
Female
Cancer
54
57
Lancer
Time Magic
Counter Magic
Concentrate
Jump+2

Holy Lance
Bronze Shield
Mythril Helmet
Plate Mail
Wizard Mantle

Level Jump2, Vertical Jump6
Haste, Immobilize, Reflect, Quick, Demi, Meteor

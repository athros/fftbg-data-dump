Final Bets: white - 22 bets for 11,898G (27.0%, x2.71); purple - 35 bets for 32,210G (73.0%, x0.37)

white bets:
fenaen: 3,607G (30.3%, 3,607G)
JumbocactuarX27: 1,000G (8.4%, 5,101G)
BirbBrainsBot: 1,000G (8.4%, 85,745G)
Lionhermit: 1,000G (8.4%, 38,736G)
Evewho: 833G (7.0%, 1,634G)
Oreo_Pizza: 500G (4.2%, 13,174G)
Who_lio42: 500G (4.2%, 6,344G)
ZephyrTempest: 456G (3.8%, 3,033G)
Meta_Five: 404G (3.4%, 404G)
Mudrockk: 400G (3.4%, 9,486G)
TheKillerNacho: 380G (3.2%, 380G)
rico_flex: 333G (2.8%, 41,639G)
FNCardascia_: 311G (2.6%, 311G)
Kyune: 264G (2.2%, 264G)
KasugaiRoastedPeas: 200G (1.7%, 15,341G)
aznwanderor: 112G (0.9%, 112G)
maakur_: 100G (0.8%, 121,356G)
alecttox: 100G (0.8%, 11,177G)
benticore: 100G (0.8%, 2,628G)
mannequ1n: 100G (0.8%, 941G)
RestIessNight: 100G (0.8%, 914G)
getthemoneyz: 98G (0.8%, 639,896G)

purple bets:
Mesmaster: 5,000G (15.5%, 19,073G)
LASERJESUS1337: 3,000G (9.3%, 61,541G)
Sairentozon7: 2,088G (6.5%, 2,088G)
RongRongArts: 2,000G (6.2%, 349,182G)
killth3kid: 2,000G (6.2%, 54,817G)
Epicureial: 2,000G (6.2%, 30,739G)
DeathTaxesAndAnime: 1,954G (6.1%, 3,833G)
TheChainNerd: 1,695G (5.3%, 1,695G)
Grimmace45: 1,553G (4.8%, 7,768G)
vorap: 1,200G (3.7%, 28,460G)
DavenIII: 1,000G (3.1%, 8,382G)
FoeSquirrel: 848G (2.6%, 848G)
Laserman1000: 800G (2.5%, 8,300G)
AeglosCrescens: 557G (1.7%, 557G)
nifboy: 528G (1.6%, 528G)
CT_5_Holy: 506G (1.6%, 506G)
EnemyController: 500G (1.6%, 194,931G)
luminarii: 500G (1.6%, 16,178G)
SerumD: 500G (1.6%, 3,618G)
CorpusCav: 500G (1.6%, 4,136G)
Fathuran: 467G (1.4%, 467G)
Miyokari: 406G (1.3%, 406G)
Baron_von_Scrub: 394G (1.2%, 394G)
Boultson1337: 343G (1.1%, 343G)
NicoSavoy: 322G (1.0%, 322G)
dinin991: 322G (1.0%, 322G)
xxnamexx: 284G (0.9%, 284G)
dorman_777: 240G (0.7%, 240G)
ungabunga_bot: 103G (0.3%, 404,489G)
Firesheath: 100G (0.3%, 11,832G)
CosmicTactician: 100G (0.3%, 20,016G)
bahamutlagooon: 100G (0.3%, 1,017G)
carsenhk: 100G (0.3%, 20,465G)
datadrivenbot: 100G (0.3%, 12,537G)
brokenknight201: 100G (0.3%, 1,285G)

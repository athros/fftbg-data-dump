Player: !Red
Team: Red Team
Palettes: Red/Brown



SSwing
Female
Virgo
55
43
Monk
Black Magic
MP Restore
Equip Armor
Lava Walking



Headgear
Gold Armor
Rubber Shoes

Pummel, Earth Slash, Secret Fist, Chakra, Revive
Fire, Fire 2, Fire 4, Bolt 3, Ice 2, Ice 3, Ice 4, Frog



StealthModeLocke
Male
Sagittarius
62
49
Priest
Jump
Arrow Guard
Beastmaster
Move+1

Gold Staff

Feather Hat
Chain Vest
Jade Armlet

Cure 2, Cure 4, Raise, Reraise, Shell 2, Esuna, Holy
Level Jump4, Vertical Jump6



Just Here2
Female
Capricorn
52
44
Dancer
Steal
Speed Save
Attack UP
Teleport

Ryozan Silk

Golden Hairpin
Black Robe
Wizard Mantle

Witch Hunt, Nameless Dance
Steal Helmet, Steal Armor, Steal Weapon, Steal Status



UmaiJam
Male
Virgo
57
42
Thief
Time Magic
Counter
Concentrate
Swim

Main Gauche

Twist Headband
Adaman Vest
Jade Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Haste, Immobilize, Float, Quick, Demi 2, Stabilize Time

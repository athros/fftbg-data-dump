Player: !Black
Team: Black Team
Palettes: Black/Red



DusterW
Female
Sagittarius
48
68
Dancer
White Magic
Mana Shield
Equip Shield
Waterbreathing

Panther Bag
Round Shield
Golden Hairpin
Power Sleeve
Defense Ring

Nameless Dance, Nether Demon
Cure 2, Raise, Protect 2, Wall



Lydian C
Female
Gemini
77
80
Geomancer
Battle Skill
Dragon Spirit
Defend
Levitate

Murasame
Gold Shield
Golden Hairpin
Linen Robe
Leather Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock
Shield Break, Mind Break, Night Sword, Surging Sword, Explosion Sword



Lowlf
Female
Aries
48
54
Mediator
White Magic
Counter Tackle
Secret Hunt
Fly

Stone Gun

Golden Hairpin
Power Sleeve
Battle Boots

Persuade, Praise, Threaten, Death Sentence, Refute
Cure, Cure 3, Cure 4, Raise, Raise 2



ColetteMSLP
Female
Scorpio
55
68
Oracle
Jump
Arrow Guard
Equip Shield
Fly

Papyrus Codex
Venetian Shield
Twist Headband
Wizard Outfit
Magic Gauntlet

Zombie, Silence Song, Foxbird
Level Jump3, Vertical Jump8

Final Bets: yellow - 6 bets for 4,498G (29.5%, x2.39); champion - 17 bets for 10,728G (70.5%, x0.42)

yellow bets:
E_Ballard: 2,541G (56.5%, 2,541G)
Hasterious: 699G (15.5%, 69,986G)
Belkra: 558G (12.4%, 558G)
ColetteMSLP: 300G (6.7%, 2,705G)
Kellios11: 300G (6.7%, 57,732G)
nifboy: 100G (2.2%, 2,269G)

champion bets:
Baron_von_Scrub: 2,996G (27.9%, 2,996G)
fenaen: 1,200G (11.2%, 25,219G)
CosmicTactician: 1,000G (9.3%, 15,385G)
Shalloween: 1,000G (9.3%, 42,922G)
BirbBrainsBot: 1,000G (9.3%, 146,172G)
DLJuggernaut: 580G (5.4%, 580G)
Oreo_Pizza: 500G (4.7%, 7,510G)
Rilgon: 424G (4.0%, 8,495G)
TheChainNerd: 399G (3.7%, 1,403G)
roofiepops: 380G (3.5%, 380G)
gorgewall: 301G (2.8%, 4,606G)
getthemoneyz: 248G (2.3%, 1,022,313G)
Kronikle: 200G (1.9%, 5,160G)
TheBigSalarius: 200G (1.9%, 757G)
Firesheath: 100G (0.9%, 3,061G)
thewondertrickster: 100G (0.9%, 100G)
datadrivenbot: 100G (0.9%, 47,793G)

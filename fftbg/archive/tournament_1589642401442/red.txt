Player: !Red
Team: Red Team
Palettes: Red/Brown



KasugaiRoastedPeas
Female
Cancer
67
56
Wizard
Item
Auto Potion
Equip Bow
Move+1

Windslash Bow

Headgear
Light Robe
Magic Gauntlet

Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Death
Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down



HaplessOne
Male
Cancer
41
61
Ninja
Draw Out
Critical Quick
Concentrate
Jump+2

Flail
Kunai
Leather Hat
Chain Vest
Angel Ring

Shuriken
Asura, Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji



Miyokari
Male
Virgo
64
70
Bard
Throw
Auto Potion
Defense UP
Jump+2

Bloody Strings

Leather Hat
Clothes
Spike Shoes

Cheer Song, Battle Song
Shuriken, Bomb, Knife, Spear, Stick



SQUiDSQUARKLIN
Male
Leo
57
73
Ninja
Steal
Parry
Equip Bow
Swim

Panther Bag
Hydra Bag
Golden Hairpin
Wizard Outfit
Magic Ring

Shuriken, Hammer, Staff
Steal Accessory

Final Bets: white - 7 bets for 4,974G (54.2%, x0.84); black - 8 bets for 4,201G (45.8%, x1.18)

white bets:
UmaiJam: 1,700G (34.2%, 65,293G)
BirbBrainsBot: 1,000G (20.1%, 156,574G)
NovaKnight21: 780G (15.7%, 780G)
prince_rogers_nelson_: 620G (12.5%, 620G)
benticore: 500G (10.1%, 46,453G)
Chambs12: 300G (6.0%, 3,607G)
getthemoneyz: 74G (1.5%, 1,423,177G)

black bets:
twelfthrootoftwo: 1,630G (38.8%, 3,197G)
dtrain332: 613G (14.6%, 613G)
AllInBot: 572G (13.6%, 572G)
moonliquor: 500G (11.9%, 25,106G)
douchetron: 364G (8.7%, 364G)
Thyrandaal: 222G (5.3%, 13,382G)
datadrivenbot: 200G (4.8%, 45,564G)
captainmilestw: 100G (2.4%, 1,062G)

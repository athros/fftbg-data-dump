Player: !zChamp
Team: Champion Team
Palettes: Green/White



Tifakitteh
Female
Gemini
53
43
Ninja
Battle Skill
Critical Quick
Defense UP
Waterwalking

Sasuke Knife
Kunai
Leather Hat
Clothes
Elf Mantle

Shuriken, Bomb, Hammer
Head Break, Stasis Sword, Surging Sword



Icegoten
Monster
Pisces
47
74
Dragon










HughJeffner
Male
Capricorn
62
70
Monk
White Magic
MA Save
Attack UP
Move+2



Triangle Hat
Black Costume
N-Kai Armlet

Wave Fist, Secret Fist, Seal Evil
Raise 2, Protect, Shell, Esuna



Azure Storm
Male
Scorpio
48
40
Monk
Elemental
Sunken State
Attack UP
Move+3



Twist Headband
Mythril Vest
Wizard Mantle

Wave Fist, Purification, Revive
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

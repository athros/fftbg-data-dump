Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SephDarkheart
Male
Aquarius
60
80
Wizard
Battle Skill
Arrow Guard
Short Charge
Move+3

Thunder Rod

Leather Hat
Black Robe
Cursed Ring

Fire 2, Fire 4, Ice, Ice 4, Flare
Head Break, Armor Break, Stasis Sword, Dark Sword



Just Here2
Female
Gemini
59
53
Ninja
Steal
Sunken State
Long Status
Move-MP Up

Ninja Edge
Spell Edge
Headgear
Mystic Vest
Germinas Boots

Shuriken, Dictionary
Gil Taking, Steal Armor, Steal Accessory, Steal Status, Leg Aim



Reveriestarsong
Male
Capricorn
67
68
Lancer
Steal
Parry
Magic Attack UP
Move+3

Javelin
Flame Shield
Grand Helmet
Bronze Armor
Feather Boots

Level Jump5, Vertical Jump7
Gil Taking, Steal Helmet, Steal Weapon, Steal Status, Arm Aim, Leg Aim



MemoriesofFinal
Female
Sagittarius
42
74
Lancer
Elemental
Parry
Concentrate
Teleport

Spear
Mythril Shield
Barbuta
Genji Armor
Wizard Mantle

Level Jump8, Vertical Jump8
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard

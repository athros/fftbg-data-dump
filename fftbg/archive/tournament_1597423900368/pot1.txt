Final Bets: red - 18 bets for 17,266G (85.9%, x0.16); blue - 11 bets for 2,831G (14.1%, x6.10)

red bets:
Rytor: 6,785G (39.3%, 6,785G)
DavenIII: 2,317G (13.4%, 11,589G)
Zeroroute: 1,448G (8.4%, 1,448G)
Error72: 1,424G (8.2%, 1,424G)
E_Ballard: 931G (5.4%, 931G)
JumbocactuarX27: 907G (5.3%, 907G)
LDSkinny: 820G (4.7%, 820G)
Nizaha: 736G (4.3%, 9,824G)
WhiteTigress: 500G (2.9%, 1,650G)
itskage: 268G (1.6%, 2,681G)
captainmilestw: 232G (1.3%, 232G)
datadrivenbot: 200G (1.2%, 67,645G)
Gunz232323: 200G (1.2%, 2,190G)
maddrave09: 148G (0.9%, 738G)
wafflereaper212: 100G (0.6%, 1,301G)
TheDali23: 100G (0.6%, 1,844G)
Thyrandaal: 100G (0.6%, 281,081G)
PuzzleSecretary: 50G (0.3%, 950G)

blue bets:
Forkmore: 654G (23.1%, 654G)
oldmanbody: 448G (15.8%, 448G)
VolgraTheMoose: 300G (10.6%, 1,992G)
getthemoneyz: 282G (10.0%, 1,620,055G)
JonnyCue: 256G (9.0%, 256G)
BirbBrainsBot: 215G (7.6%, 94,007G)
AllInBot: 200G (7.1%, 200G)
Valgram: 152G (5.4%, 152G)
MonchoStrife: 124G (4.4%, 124G)
koeeh: 100G (3.5%, 363G)
Brainstew29: 100G (3.5%, 37,189G)

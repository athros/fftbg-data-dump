Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Fenaen
Male
Gemini
80
41
Ninja
Item
Faith Save
Martial Arts
Move+2



Twist Headband
Mythril Vest
Battle Boots

Staff
Potion, Hi-Potion, Antidote



DavenIII
Male
Aries
45
66
Geomancer
Battle Skill
Mana Shield
Equip Polearm
Teleport

Mythril Spear
Aegis Shield
Green Beret
Linen Robe
Defense Armlet

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Head Break, Armor Break, Power Break, Stasis Sword



JumbocactuarX27
Female
Scorpio
79
45
Wizard
Dance
Dragon Spirit
Halve MP
Levitate

Main Gauche

Black Hood
Wizard Robe
Defense Armlet

Fire 4, Bolt, Bolt 4, Empower, Flare
Slow Dance, Polka Polka, Disillusion, Nameless Dance, Void Storage



TheMurkGnome
Male
Virgo
78
49
Archer
Talk Skill
Mana Shield
Attack UP
Waterbreathing

Ultimus Bow

Black Hood
Judo Outfit
Diamond Armlet

Charge+3
Threaten, Death Sentence

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Redmage4evah
Female
Taurus
68
79
Dancer
Battle Skill
HP Restore
Defend
Move-HP Up

Cashmere

Holy Miter
White Robe
Defense Ring

Witch Hunt, Last Dance, Obsidian Blade
Weapon Break, Speed Break, Mind Break



Bruubarg
Female
Libra
57
82
Lancer
Talk Skill
Speed Save
Equip Bow
Swim

Poison Bow
Round Shield
Bronze Helmet
Chain Mail
Diamond Armlet

Level Jump8, Vertical Jump6
Praise, Threaten, Mimic Daravon, Refute



Aquios
Female
Taurus
67
64
Summoner
Steal
Auto Potion
Halve MP
Swim

Flame Rod

Headgear
White Robe
Leather Mantle

Ramuh, Ifrit, Odin, Leviathan, Silf, Lich
Gil Taking, Steal Helmet



Butterbelljedi
Male
Capricorn
43
72
Knight
Draw Out
Auto Potion
Equip Axe
Waterwalking

Mace of Zeus
Escutcheon
Iron Helmet
Gold Armor
Feather Boots

Shield Break, Magic Break, Speed Break, Mind Break
Bizen Boat, Kikuichimoji, Masamune

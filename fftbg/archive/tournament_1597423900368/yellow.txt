Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ruleof5
Female
Serpentarius
67
75
Archer
Dance
Abandon
Equip Shield
Ignore Terrain

Lightning Bow
Bronze Shield
Twist Headband
Judo Outfit
Dracula Mantle

Charge+1, Charge+2, Charge+3, Charge+5
Witch Hunt, Wiznaibus, Slow Dance, Last Dance, Obsidian Blade



SQUiDSQUARKLIN
Female
Sagittarius
71
57
Time Mage
White Magic
MA Save
Sicken
Move-HP Up

Gold Staff

Leather Hat
Chain Vest
Power Wrist

Stop, Immobilize, Stabilize Time
Cure 2, Raise 2, Reraise, Regen, Wall, Esuna



SuppleThink
Monster
Gemini
73
76
Chocobo










IndecisiveNinja
Female
Aquarius
67
56
Archer
Steal
Counter
Attack UP
Waterbreathing

Ultimus Bow

Thief Hat
Earth Clothes
Dracula Mantle

Charge+3, Charge+5
Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim

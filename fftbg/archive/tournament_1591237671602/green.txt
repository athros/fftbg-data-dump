Player: !Green
Team: Green Team
Palettes: Green/White



Moonliquor
Male
Taurus
77
54
Archer
Elemental
Counter Tackle
Dual Wield
Swim

Bow Gun
Bow Gun
Circlet
Leather Outfit
Feather Boots

Charge+5
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



HASTERIOUS
Female
Gemini
53
61
Time Mage
Dance
Caution
Martial Arts
Waterbreathing



Twist Headband
Leather Outfit
Small Mantle

Haste, Slow 2, Float, Demi, Demi 2, Stabilize Time, Galaxy Stop
Witch Hunt, Slow Dance, Obsidian Blade, Dragon Pit



NeoKevlar
Male
Scorpio
80
45
Mime

Meatbone Slash
Martial Arts
Ignore Height



Black Hood
Brigandine
Rubber Shoes

Mimic




ExecutedGiraffe
Female
Aries
75
40
Ninja
Dance
Hamedo
Attack UP
Retreat

Flame Whip
Flail
Triangle Hat
Power Sleeve
Leather Mantle

Shuriken, Bomb, Ninja Sword, Wand
Wiznaibus, Last Dance

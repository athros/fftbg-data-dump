Player: !Black
Team: Black Team
Palettes: Black/Red



Powerpinch
Female
Taurus
66
79
Priest
Punch Art
Parry
Equip Shield
Lava Walking

Flail
Round Shield
Twist Headband
Wizard Robe
108 Gems

Cure, Raise, Raise 2, Reraise, Protect, Wall, Esuna
Purification, Revive



ApplesauceBoss
Female
Leo
78
41
Summoner
Dance
Caution
Short Charge
Levitate

Star Bag

Flash Hat
White Robe
Bracer

Moogle, Ifrit, Titan, Bahamut, Leviathan, Fairy, Cyclops
Witch Hunt, Slow Dance, Polka Polka, Nameless Dance



Maakur
Male
Cancer
55
66
Priest
Math Skill
Counter Magic
Sicken
Move+1

Rainbow Staff

Holy Miter
Linen Robe
Feather Boots

Cure, Cure 4, Raise, Regen, Protect, Shell, Shell 2
CT, Height, 5, 4, 3



Gr8keeper
Male
Capricorn
46
61
Oracle
Summon Magic
MP Restore
Secret Hunt
Move+3

Bestiary

Leather Hat
Silk Robe
Elf Mantle

Blind, Poison, Silence Song, Dispel Magic, Petrify
Moogle, Golem, Carbunkle, Leviathan, Fairy

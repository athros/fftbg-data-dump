Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Evdoggity
Female
Scorpio
60
43
Time Mage
Draw Out
Critical Quick
Magic Attack UP
Move-MP Up

Ivory Rod

Flash Hat
Silk Robe
Power Wrist

Haste, Float, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Asura, Koutetsu, Murasame, Muramasa, Kikuichimoji



Silentkaster
Male
Aries
67
48
Ninja
Punch Art
Dragon Spirit
Equip Gun
Move+1

Papyrus Codex
Papyrus Codex
Flash Hat
Wizard Outfit
108 Gems

Shuriken, Bomb
Spin Fist, Purification, Revive



SeniorBunk
Female
Taurus
43
68
Lancer
Item
Speed Save
Dual Wield
Move+1

Partisan
Spear
Gold Helmet
Wizard Robe
Cursed Ring

Level Jump8, Vertical Jump3
X-Potion, Hi-Ether, Eye Drop, Soft, Remedy, Phoenix Down



Amiture
Female
Pisces
55
56
Ninja
Item
PA Save
Equip Gun
Teleport

Bloody Strings
Madlemgen
Golden Hairpin
Chain Vest
Spike Shoes

Bomb, Stick
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water

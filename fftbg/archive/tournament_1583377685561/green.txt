Player: !Green
Team: Green Team
Palettes: Green/White



Shalloween
Male
Pisces
69
55
Chemist
Charge
Auto Potion
Dual Wield
Waterwalking

Blind Knife
Star Bag
Thief Hat
Brigandine
Defense Armlet

Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Charge+5



Fortysw4rm
Female
Pisces
52
75
Chemist
Summon Magic
Parry
Beastmaster
Move-MP Up

Cute Bag

Black Hood
Leather Outfit
108 Gems

Potion, Ether, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down
Moogle, Titan, Carbunkle, Leviathan, Silf



Phrossi V
Male
Virgo
50
60
Thief
Sing
PA Save
Long Status
Move-HP Up

Spell Edge

Flash Hat
Judo Outfit
Elf Mantle

Steal Shield, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Angel Song, Life Song, Battle Song, Last Song, Sky Demon



Zodiac192133
Female
Scorpio
53
73
Knight
Elemental
Sunken State
Equip Knife
Ignore Terrain

Air Knife
Mythril Shield
Grand Helmet
Chain Mail
Defense Armlet

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Night Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind

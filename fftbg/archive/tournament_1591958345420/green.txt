Player: !Green
Team: Green Team
Palettes: Green/White



NovaKnight21
Male
Cancer
42
47
Thief
Basic Skill
Sunken State
Beastmaster
Move-HP Up

Mythril Knife

Holy Miter
Mystic Vest
Dracula Mantle

Steal Heart
Accumulate, Heal, Cheer Up, Wish, Scream



Just Here2
Female
Sagittarius
41
52
Thief
Punch Art
Counter
Equip Gun
Teleport

Blast Gun

Twist Headband
Power Sleeve
Defense Ring

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Arm Aim, Leg Aim
Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive



ExecutedGiraffe
Male
Virgo
73
39
Knight
Talk Skill
Parry
Defense UP
Waterbreathing

Rune Blade
Crystal Shield
Circlet
Black Robe
Feather Boots

Shield Break, Magic Break, Power Break, Mind Break, Dark Sword, Surging Sword
Invitation, Praise, Threaten, Preach, Solution, Negotiate, Mimic Daravon



UmaiJam
Male
Scorpio
76
68
Lancer
Throw
PA Save
Equip Knife
Ignore Terrain

Mythril Knife
Gold Shield
Platinum Helmet
Genji Armor
Red Shoes

Level Jump3, Vertical Jump6
Shuriken, Bomb, Knife, Dictionary

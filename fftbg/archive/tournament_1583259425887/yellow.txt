Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PleXmito
Male
Scorpio
49
62
Monk
Time Magic
Brave Up
Magic Attack UP
Lava Walking



Golden Hairpin
Power Sleeve
Bracer

Spin Fist, Wave Fist, Earth Slash, Purification, Revive
Haste, Haste 2, Slow 2, Stop, Immobilize, Float, Demi, Stabilize Time



Exuno
Male
Capricorn
42
38
Chemist
Summon Magic
Distribute
Equip Axe
Lava Walking

Morning Star

Black Hood
Mythril Vest
Leather Mantle

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Fairy, Lich, Cyclops



TheManInPlaid
Male
Taurus
58
52
Lancer
Summon Magic
Auto Potion
Equip Axe
Lava Walking

Oak Staff
Gold Shield
Cross Helmet
Diamond Armor
Germinas Boots

Level Jump4, Vertical Jump8
Shiva, Ramuh, Carbunkle, Bahamut, Leviathan, Fairy, Lich



MrUbiq
Male
Aquarius
75
58
Samurai
Summon Magic
Counter
Equip Gun
Move+1

Blaze Gun

Cross Helmet
Platinum Armor
Magic Gauntlet

Asura, Koutetsu, Kiyomori
Moogle, Carbunkle, Leviathan, Salamander, Silf, Lich

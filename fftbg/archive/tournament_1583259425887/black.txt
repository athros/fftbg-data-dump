Player: !Black
Team: Black Team
Palettes: Black/Red



Kennymillennium
Male
Aquarius
56
79
Mediator
Steal
Counter Magic
Equip Polearm
Ignore Terrain

Ivory Rod

Red Hood
Leather Outfit
N-Kai Armlet

Invitation, Praise, Solution
Gil Taking, Steal Heart, Steal Accessory, Steal Status, Leg Aim



Manart
Female
Scorpio
42
65
Ninja
Punch Art
Earplug
Sicken
Move+3

Scorpion Tail
Kunai
Red Hood
Wizard Outfit
Setiemson

Shuriken, Stick
Secret Fist, Purification, Chakra, Revive



Wurstwaesserchen
Female
Cancer
59
56
Calculator
Black Magic
Caution
Attack UP
Jump+1

Iron Fan

Barette
Black Robe
Feather Boots

Height, 5, 4
Fire 3, Bolt, Ice 4, Frog, Flare



Mirapoix
Male
Libra
51
55
Thief
Yin Yang Magic
Regenerator
Defense UP
Jump+2

Mage Masher

Flash Hat
Chain Vest
Wizard Mantle

Steal Heart, Steal Helmet, Steal Armor, Steal Accessory
Blind, Life Drain, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep, Petrify

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Helpimabug
Male
Virgo
60
41
Knight
Elemental
Meatbone Slash
Sicken
Move+3

Slasher
Bronze Shield
Cross Helmet
Gold Armor
N-Kai Armlet

Magic Break, Speed Break, Stasis Sword, Justice Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



CassiePhoenix
Female
Gemini
57
63
Summoner
Throw
Counter Tackle
Short Charge
Jump+1

Poison Rod

Thief Hat
Linen Robe
Wizard Mantle

Moogle, Shiva, Carbunkle, Bahamut, Odin, Leviathan
Shuriken, Bomb, Ninja Sword



Isreal
Male
Taurus
54
77
Priest
Talk Skill
Blade Grasp
Beastmaster
Swim

Flail

Triangle Hat
Chameleon Robe
N-Kai Armlet

Cure, Cure 4, Raise, Raise 2, Protect 2, Wall, Esuna, Holy
Mimic Daravon, Refute



Dyane
Female
Cancer
68
43
Lancer
Steal
Parry
Long Status
Jump+3

Ivory Rod
Bronze Shield
Leather Helmet
Diamond Armor
Angel Ring

Level Jump8, Vertical Jump8
Steal Shield, Steal Status, Arm Aim, Leg Aim

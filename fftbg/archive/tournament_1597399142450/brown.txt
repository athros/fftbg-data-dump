Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DesertWooder
Female
Scorpio
44
59
Squire
Battle Skill
Damage Split
Equip Gun
Lava Walking

Madlemgen
Flame Shield
Headgear
Chain Vest
Small Mantle

Accumulate, Tickle, Cheer Up, Wish
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword



McGooferson
Male
Gemini
80
62
Monk
Charge
PA Save
Dual Wield
Jump+3



Black Hood
Earth Clothes
Jade Armlet

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Charge+1



Matthus
Male
Scorpio
73
44
Wizard
Summon Magic
Parry
Long Status
Move+3

Air Knife

Leather Hat
Wizard Outfit
Sprint Shoes

Fire, Fire 3, Bolt 2, Ice 2, Ice 4, Empower, Frog, Flare
Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Salamander, Silf, Fairy, Lich



ColetteMSLP
Male
Scorpio
80
79
Archer
Battle Skill
Mana Shield
Equip Gun
Move+3

Battle Folio
Buckler
Iron Helmet
Secret Clothes
Angel Ring

Charge+1, Charge+2, Charge+4, Charge+7
Shield Break, Speed Break, Mind Break

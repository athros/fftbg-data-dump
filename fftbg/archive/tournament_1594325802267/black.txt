Player: !Black
Team: Black Team
Palettes: Black/Red



Lemonjohns
Female
Scorpio
44
68
Squire
Steal
MP Restore
Long Status
Move+1

Mythril Sword
Ice Shield
Golden Hairpin
Leather Outfit
Feather Boots

Accumulate
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Leg Aim



Extinctrational
Male
Taurus
52
59
Lancer
Battle Skill
Damage Split
Equip Axe
Swim

Morning Star
Round Shield
Cross Helmet
Mythril Armor
Bracer

Level Jump8, Vertical Jump8
Magic Break, Stasis Sword, Dark Sword, Surging Sword



NWOW 44
Male
Pisces
57
54
Lancer
Talk Skill
Absorb Used MP
Concentrate
Ignore Terrain

Javelin
Genji Shield
Genji Helmet
Silk Robe
Feather Mantle

Level Jump4, Vertical Jump6
Invitation, Persuade, Preach, Mimic Daravon, Rehabilitate



DejaPoo21
Male
Aries
61
63
Oracle
White Magic
Counter Magic
Maintenance
Move+1

Gokuu Rod

Red Hood
Mystic Vest
Bracer

Poison, Spell Absorb, Pray Faith, Zombie, Foxbird, Dispel Magic, Dark Holy
Cure 2, Raise, Reraise, Shell, Esuna

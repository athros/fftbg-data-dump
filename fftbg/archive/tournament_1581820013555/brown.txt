Player: !Brown
Team: Brown Team
Palettes: Brown/Green



AdaephonD
Female
Gemini
76
49
Thief
Yin Yang Magic
Regenerator
Magic Attack UP
Waterwalking

Cultist Dagger

Leather Hat
Clothes
Genji Gauntlet

Gil Taking, Steal Heart, Steal Helmet
Poison, Spell Absorb, Pray Faith, Silence Song, Dispel Magic, Paralyze, Sleep, Dark Holy



SkyridgeZero
Female
Gemini
39
61
Priest
Throw
Counter Flood
Short Status
Lava Walking

Rainbow Staff

Red Hood
Clothes
Magic Gauntlet

Cure, Cure 3, Raise, Reraise, Regen, Protect 2, Shell, Esuna
Bomb, Dictionary



Cougboi
Male
Taurus
64
60
Geomancer
Jump
Earplug
Defense UP
Ignore Terrain

Mythril Sword
Genji Shield
Green Beret
Linen Robe
Feather Boots

Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Level Jump2, Vertical Jump8



DaurosTD
Male
Gemini
74
68
Calculator
Yin Yang Magic
Critical Quick
Maintenance
Waterwalking

Poison Rod

Leather Hat
Wizard Outfit
Angel Ring

Height, Prime Number, 5
Spell Absorb, Pray Faith, Zombie, Dispel Magic, Paralyze, Sleep, Petrify

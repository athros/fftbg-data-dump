Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thyrandaal
Monster
Cancer
61
47
Vampire










VolgraTheMoose
Male
Aries
80
54
Lancer
Battle Skill
Regenerator
Maintenance
Ignore Terrain

Mythril Spear
Flame Shield
Barbuta
Carabini Mail
N-Kai Armlet

Level Jump3, Vertical Jump2
Speed Break



Evewho
Female
Libra
61
56
Archer
Throw
Parry
Secret Hunt
Move-HP Up

Gastrafitis
Ice Shield
Cachusha
Mythril Vest
Defense Ring

Charge+2, Charge+3, Charge+5, Charge+20
Knife



TimeJannies
Male
Sagittarius
51
46
Ninja
Time Magic
Meatbone Slash
Equip Gun
Retreat

Battle Folio
Battle Folio
Headgear
Power Sleeve
Rubber Shoes

Dictionary
Haste 2, Reflect

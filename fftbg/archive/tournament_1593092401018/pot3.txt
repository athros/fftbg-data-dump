Final Bets: white - 8 bets for 8,359G (63.7%, x0.57); black - 9 bets for 4,772G (36.3%, x1.75)

white bets:
prince_rogers_nelson_: 5,115G (61.2%, 5,115G)
Chronolynx42: 1,000G (12.0%, 51,831G)
TheChainNerd: 799G (9.6%, 799G)
E_Ballard: 636G (7.6%, 636G)
arcadiues: 290G (3.5%, 290G)
Shinzutalos: 216G (2.6%, 216G)
BirbBrainsBot: 203G (2.4%, 175,719G)
nifboy: 100G (1.2%, 2,903G)

black bets:
NovaKnight21: 968G (20.3%, 968G)
getthemoneyz: 936G (19.6%, 1,038,921G)
joewcarson: 632G (13.2%, 632G)
BoneMiser: 500G (10.5%, 500G)
CosmicTactician: 500G (10.5%, 21,602G)
NightwolfXVI: 428G (9.0%, 428G)
sparker9: 400G (8.4%, 19,046G)
Zagorsek: 308G (6.5%, 308G)
datadrivenbot: 100G (2.1%, 49,036G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TasisSai
Female
Aquarius
46
56
Wizard
Talk Skill
Catch
Monster Talk
Move+1

Mage Masher

Holy Miter
Light Robe
Reflect Ring

Fire 2, Fire 3, Bolt, Bolt 3, Ice, Ice 3, Ice 4, Empower, Death
Threaten, Solution, Death Sentence, Mimic Daravon, Refute



Upvla
Male
Aquarius
61
44
Ninja
Sing
Parry
Concentrate
Move+3

Mage Masher
Sasuke Knife
Holy Miter
Wizard Outfit
N-Kai Armlet

Shuriken, Bomb
Angel Song, Battle Song



StealthModeLocke
Male
Cancer
75
68
Wizard
Draw Out
Mana Shield
Long Status
Levitate

Flame Rod

Headgear
Brigandine
Jade Armlet

Fire 4, Bolt 2, Bolt 3, Ice, Ice 2, Ice 4, Empower, Death, Flare
Asura, Murasame, Heaven's Cloud, Kikuichimoji



Lastly
Male
Leo
78
49
Archer
Draw Out
Counter
Equip Armor
Jump+3

Night Killer
Aegis Shield
Leather Helmet
Gold Armor
Rubber Shoes

Charge+2, Charge+4, Charge+5, Charge+10, Charge+20
Koutetsu, Heaven's Cloud, Kikuichimoji

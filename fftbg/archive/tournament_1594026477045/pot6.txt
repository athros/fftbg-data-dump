Final Bets: white - 7 bets for 8,164G (79.3%, x0.26); purple - 6 bets for 2,136G (20.7%, x3.82)

white bets:
helpimabug: 3,882G (47.6%, 7,764G)
UmaiJam: 1,800G (22.0%, 81,674G)
BirbBrainsBot: 1,000G (12.2%, 80,984G)
Evewho: 728G (8.9%, 1,429G)
serperemagus: 600G (7.3%, 600G)
fluffskull: 100G (1.2%, 3,312G)
getthemoneyz: 54G (0.7%, 1,207,232G)

purple bets:
Zachara: 1,100G (51.5%, 131,100G)
Smokegiant: 500G (23.4%, 85,909G)
datadrivenbot: 200G (9.4%, 54,467G)
martymcfly2021: 136G (6.4%, 136G)
DeathTaxesAndAnime: 100G (4.7%, 3,083G)
Spuzzmocker: 100G (4.7%, 662G)

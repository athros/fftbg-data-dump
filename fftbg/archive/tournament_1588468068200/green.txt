Player: !Green
Team: Green Team
Palettes: Green/White



Volgrathemoose
Male
Sagittarius
46
41
Bard
Steal
Damage Split
Magic Attack UP
Ignore Height

Bloody Strings

Ribbon
Crystal Mail
Diamond Armlet

Life Song, Diamond Blade
Steal Weapon, Arm Aim



Byrdturbo
Monster
Cancer
51
68
Steel Giant










Twelfthrootoftwo
Male
Cancer
58
70
Mediator
Black Magic
MP Restore
Long Status
Fly

Romanda Gun

Flash Hat
Wizard Robe
Diamond Armlet

Praise, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon, Refute
Fire 4, Bolt 4, Ice, Ice 4, Flare



WrathfulRemy
Female
Cancer
39
44
Lancer
Dance
MA Save
Equip Knife
Move+3

Cultist Dagger
Gold Shield
Mythril Helmet
Diamond Armor
Feather Mantle

Level Jump5, Vertical Jump8
Slow Dance, Disillusion, Obsidian Blade, Nether Demon

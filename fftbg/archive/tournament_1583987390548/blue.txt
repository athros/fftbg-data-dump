Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Victoriolue
Male
Aries
43
75
Ninja
Steal
Parry
Equip Gun
Move+2

Bestiary
Battle Folio
Headgear
Leather Outfit
N-Kai Armlet

Shuriken, Bomb
Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Arm Aim



Z32o
Male
Gemini
61
66
Time Mage
Summon Magic
Counter Tackle
Short Charge
Move+3

Mace of Zeus

Triangle Hat
Brigandine
Germinas Boots

Haste, Float, Reflect
Moogle, Carbunkle, Silf, Lich



Sotehr
Male
Pisces
67
75
Lancer
Battle Skill
Caution
Doublehand
Retreat

Mythril Spear

Circlet
Crystal Mail
Dracula Mantle

Level Jump5, Vertical Jump8
Head Break, Magic Break, Stasis Sword



SkylerBunny
Female
Libra
77
49
Samurai
Throw
Faith Up
Dual Wield
Move+3

Asura Knife
Asura Knife
Crystal Helmet
Linen Cuirass
Battle Boots

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
Shuriken

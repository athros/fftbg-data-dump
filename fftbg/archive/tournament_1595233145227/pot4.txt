Final Bets: purple - 11 bets for 14,005G (51.7%, x0.93); brown - 8 bets for 13,062G (48.3%, x1.07)

purple bets:
Musclestache: 9,304G (66.4%, 9,304G)
twelfthrootoftwo: 1,855G (13.2%, 1,855G)
BirbBrainsBot: 1,000G (7.1%, 140,082G)
ColetteMSLP: 600G (4.3%, 1,563G)
getthemoneyz: 260G (1.9%, 1,329,151G)
iBardic: 212G (1.5%, 212G)
AllInBot: 200G (1.4%, 200G)
datadrivenbot: 200G (1.4%, 33,290G)
chloromaster: 174G (1.2%, 1,610G)
CT_5_Holy: 100G (0.7%, 1,607G)
douchetron: 100G (0.7%, 2,408G)

brown bets:
nifboy: 6,276G (48.0%, 12,552G)
shineeyo: 2,956G (22.6%, 2,956G)
JLinkletter: 1,026G (7.9%, 1,026G)
Draconis345: 1,000G (7.7%, 48,077G)
HaateXIII: 808G (6.2%, 808G)
randgridr: 432G (3.3%, 7,406G)
superdevon1: 424G (3.2%, 4,243G)
Modoh: 140G (1.1%, 140G)

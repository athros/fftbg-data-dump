Final Bets: red - 9 bets for 15,846G (67.3%, x0.48); blue - 10 bets for 7,683G (32.7%, x2.06)

red bets:
AllInBot: 6,186G (39.0%, 6,186G)
pLifer: 4,727G (29.8%, 4,727G)
ColetteMSLP: 1,457G (9.2%, 1,457G)
twelfthrootoftwo: 1,249G (7.9%, 1,249G)
nifboy: 1,000G (6.3%, 12,674G)
randgridr: 432G (2.7%, 7,459G)
iBardic: 345G (2.2%, 345G)
Lythe_Caraker: 250G (1.6%, 156,401G)
datadrivenbot: 200G (1.3%, 33,593G)

blue bets:
SkyridgeZero: 1,500G (19.5%, 38,384G)
lowlf: 1,111G (14.5%, 111,657G)
Draconis345: 1,000G (13.0%, 49,684G)
BirbBrainsBot: 1,000G (13.0%, 136,512G)
amiture: 1,000G (13.0%, 3,737G)
getthemoneyz: 850G (11.1%, 1,329,796G)
superdevon1: 711G (9.3%, 2,371G)
JLinkletter: 300G (3.9%, 1,155G)
gorgewall: 201G (2.6%, 6,252G)
douchetron: 10G (0.1%, 2,000G)

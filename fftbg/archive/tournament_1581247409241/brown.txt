Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Cloudycube
Female
Sagittarius
62
61
Time Mage
Item
PA Save
Equip Bow
Waterbreathing

Long Bow

Headgear
White Robe
Reflect Ring

Stop, Reflect, Demi 2, Stabilize Time
Potion, Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Neerrm
Female
Scorpio
57
74
Mime

Counter Magic
Defense UP
Move-HP Up



Green Beret
Wizard Robe
Cursed Ring

Mimic




Insonder
Male
Sagittarius
71
43
Knight
Elemental
Brave Up
Attack UP
Retreat

Ragnarok

Gold Helmet
Leather Armor
Angel Ring

Head Break, Shield Break, Weapon Break, Power Break, Justice Sword, Dark Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Lava Ball



Ytmnd7
Male
Leo
74
75
Time Mage
Yin Yang Magic
Regenerator
Halve MP
Waterwalking

Papyrus Codex

Triangle Hat
Linen Robe
Feather Mantle

Haste, Slow 2, Stop, Float, Quick, Demi, Stabilize Time
Blind, Spell Absorb, Life Drain, Zombie, Foxbird, Dispel Magic

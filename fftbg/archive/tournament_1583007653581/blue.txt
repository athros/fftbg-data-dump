Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zebobz
Monster
Capricorn
38
41
Black Goblin










Metal Gear Flex
Monster
Capricorn
71
64
Dark Behemoth










Galkife
Female
Virgo
42
74
Thief
Talk Skill
Catch
Equip Shield
Swim

Assassin Dagger
Round Shield
Triangle Hat
Mythril Vest
Jade Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon
Praise, Threaten, Death Sentence, Insult, Refute



Noreseto
Female
Scorpio
64
41
Samurai
Talk Skill
Counter Magic
Equip Sword
Move+3

Asura Knife

Diamond Helmet
White Robe
Genji Gauntlet

Asura, Kiyomori, Kikuichimoji, Masamune
Invitation, Insult

Final Bets: purple - 11 bets for 8,372G (69.2%, x0.44); brown - 9 bets for 3,722G (30.8%, x2.25)

purple bets:
DavenIII: 1,500G (17.9%, 10,179G)
Draconis345: 1,000G (11.9%, 57,319G)
BirbBrainsBot: 1,000G (11.9%, 47,753G)
kaidykat: 1,000G (11.9%, 32,103G)
LDSkinny: 978G (11.7%, 978G)
prince_rogers_nelson_: 974G (11.6%, 974G)
DLJuggernaut: 500G (6.0%, 21,603G)
BlackFireUK: 500G (6.0%, 15,905G)
serperemagus: 420G (5.0%, 420G)
Anasetsuken: 400G (4.8%, 16,919G)
lastly: 100G (1.2%, 27,470G)

brown bets:
joewcarson: 2,000G (53.7%, 38,539G)
TheChainNerd: 424G (11.4%, 424G)
nifboy: 424G (11.4%, 424G)
autnagrag: 222G (6.0%, 598G)
AltimaMantoid: 216G (5.8%, 216G)
dinin991: 136G (3.7%, 136G)
AllInBot: 100G (2.7%, 100G)
maakur_: 100G (2.7%, 21,572G)
Kellios11: 100G (2.7%, 10,587G)

Player: !White
Team: White Team
Palettes: White/Blue



Ninjapenguim
Monster
Taurus
64
48
Great Malboro










Upvla
Female
Pisces
82
81
Mediator
Draw Out
MP Restore
Dual Wield
Move-HP Up

Mythril Gun
Blast Gun
Twist Headband
Brigandine
Elf Mantle

Invitation, Praise, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Koutetsu, Bizen Boat, Murasame, Muramasa



Manc0
Female
Libra
62
54
Geomancer
Throw
Meatbone Slash
Long Status
Teleport

Mythril Sword
Round Shield
Barette
Chameleon Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Wand



NoxeGS
Female
Leo
65
59
Mediator
Yin Yang Magic
Mana Shield
Equip Gun
Lava Walking

Battle Folio

Leather Hat
Brigandine
Dracula Mantle

Invitation, Praise, Preach, Insult, Negotiate, Refute, Rehabilitate
Doubt Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Dark Holy

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Princessmomonoke
Male
Aquarius
67
39
Mime

PA Save
Long Status
Ignore Terrain



Red Hood
Black Robe
Wizard Mantle

Mimic




Denamda
Female
Capricorn
59
64
Oracle
Punch Art
Blade Grasp
Doublehand
Move+3

Iron Fan

Holy Miter
Judo Outfit
Defense Armlet

Zombie, Silence Song, Foxbird, Dispel Magic, Dark Holy
Pummel, Chakra, Revive



Tougou
Male
Pisces
54
81
Lancer
Elemental
Abandon
Attack UP
Lava Walking

Mythril Spear
Genji Shield
Circlet
Mythril Armor
Bracer

Level Jump3, Vertical Jump8
Hell Ivy, Quicksand, Blizzard, Gusty Wind, Lava Ball



Kuroda222
Female
Aquarius
77
42
Monk
Elemental
Dragon Spirit
Short Status
Move-MP Up



Red Hood
Black Costume
Wizard Mantle

Spin Fist, Pummel, Wave Fist, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RunicMagus
Male
Gemini
75
69
Ninja
Talk Skill
MA Save
Beastmaster
Teleport

Assassin Dagger
Flail
Leather Hat
Brigandine
Defense Armlet

Bomb, Knife, Sword
Preach, Death Sentence, Insult, Negotiate, Refute



LDSkinny
Female
Aquarius
46
54
Oracle
Item
MA Save
Throw Item
Ignore Height

Battle Folio

Feather Hat
White Robe
Genji Gauntlet

Doubt Faith, Dispel Magic, Sleep
Potion, Hi-Potion, X-Potion, Eye Drop, Soft, Holy Water



Wooplestein
Male
Capricorn
51
59
Knight
Steal
Caution
Magic Defense UP
Fly

Defender
Escutcheon
Leather Helmet
Diamond Armor
Bracer

Head Break, Weapon Break, Power Break, Mind Break, Night Sword
Gil Taking, Steal Accessory



MagicBottle
Male
Gemini
67
59
Archer
Elemental
Counter Tackle
Concentrate
Move-MP Up

Ice Bow

Headgear
Leather Outfit
Genji Gauntlet

Charge+2, Charge+4, Charge+5, Charge+7, Charge+10
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball

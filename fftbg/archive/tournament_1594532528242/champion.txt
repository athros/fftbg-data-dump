Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Laserman1000
Male
Aquarius
49
59
Oracle
Charge
Absorb Used MP
Defense UP
Move-HP Up

Ivory Rod

Flash Hat
Adaman Vest
Red Shoes

Poison, Zombie, Confusion Song, Sleep
Charge+1



Maddrave09
Monster
Taurus
65
66
Taiju










Reddwind
Male
Virgo
67
47
Knight
Item
Arrow Guard
Dual Wield
Jump+3

Mythril Sword
Blood Sword
Iron Helmet
Gold Armor
Jade Armlet

Head Break, Shield Break, Speed Break, Surging Sword
Potion, Holy Water



CosmicTactician
Female
Taurus
52
64
Summoner
Time Magic
Meatbone Slash
Short Charge
Teleport

Flame Rod

Thief Hat
White Robe
Small Mantle

Moogle, Shiva, Ramuh, Ifrit, Titan, Golem, Carbunkle, Salamander, Silf, Lich, Cyclops
Haste, Stop, Quick, Demi, Demi 2

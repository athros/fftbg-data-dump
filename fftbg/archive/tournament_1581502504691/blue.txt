Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rollopeo
Male
Aquarius
42
57
Squire
Charge
Counter
Halve MP
Retreat

Battle Axe
Aegis Shield
Platinum Helmet
Leather Outfit
N-Kai Armlet

Accumulate, Heal, Yell, Wish
Charge+2, Charge+3, Charge+10



Neetneph
Monster
Sagittarius
54
75
Plague










Stopher121
Male
Pisces
48
53
Geomancer
Talk Skill
Counter Tackle
Monster Talk
Jump+2

Platinum Sword
Diamond Shield
Feather Hat
Silk Robe
Defense Ring

Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind
Persuade, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate



Solomongrundy85
Female
Aries
56
63
Geomancer
Draw Out
Earplug
Magic Attack UP
Move+2

Heaven's Cloud
Escutcheon
Green Beret
Linen Robe
Defense Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm
Asura, Murasame, Kikuichimoji

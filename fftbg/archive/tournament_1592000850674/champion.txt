Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Powerpinch
Male
Leo
79
57
Ninja
Steal
Counter
Concentrate
Retreat

Mage Masher
Sasuke Knife
Black Hood
Wizard Outfit
Defense Armlet

Knife, Sword, Spear
Steal Helmet, Steal Armor, Steal Status, Leg Aim



StealthModeLocke
Female
Libra
78
72
Calculator
Lucavi Skill
Auto Potion
Equip Sword
Fly

Kikuichimoji

Black Hood
Mystic Vest
Reflect Ring

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



ValtonZenola
Male
Libra
70
47
Archer
Yin Yang Magic
Hamedo
Equip Sword
Move+3

Ragnarok

Twist Headband
Adaman Vest
Small Mantle

Charge+4, Charge+5, Charge+10, Charge+20
Pray Faith, Confusion Song, Dispel Magic, Paralyze



Zeando
Female
Scorpio
44
68
Archer
Item
Dragon Spirit
Throw Item
Jump+2

Hunting Bow
Diamond Shield
Twist Headband
Adaman Vest
Salty Rage

Charge+2, Charge+4, Charge+7
X-Potion, Echo Grass, Phoenix Down

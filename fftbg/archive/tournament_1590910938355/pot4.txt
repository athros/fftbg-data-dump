Final Bets: purple - 13 bets for 6,912G (75.4%, x0.33); brown - 6 bets for 2,253G (24.6%, x3.07)

purple bets:
Mesmaster: 2,000G (28.9%, 154,037G)
killth3kid: 1,000G (14.5%, 23,338G)
BirbBrainsBot: 1,000G (14.5%, 109,619G)
getthemoneyz: 968G (14.0%, 781,174G)
OneHundredFists: 588G (8.5%, 588G)
CT_Remix: 300G (4.3%, 1,476G)
gorgewall: 201G (2.9%, 1,810G)
VynxYukida: 200G (2.9%, 264G)
Valentine009: 200G (2.9%, 200G)
ASkyNightly: 155G (2.2%, 274G)
waterwatereverywhere: 100G (1.4%, 8,046G)
datadrivenbot: 100G (1.4%, 29,654G)
spootoo: 100G (1.4%, 3,532G)

brown bets:
HASTERIOUS: 619G (27.5%, 12,392G)
sukotsuto: 500G (22.2%, 17,505G)
YaBoy125: 480G (21.3%, 480G)
benticore: 250G (11.1%, 2,270G)
wyonearth: 204G (9.1%, 204G)
gogofromtogo: 200G (8.9%, 4,018G)

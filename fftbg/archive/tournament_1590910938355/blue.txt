Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Solomongrundy85
Male
Gemini
63
80
Ninja
Jump
Brave Save
Equip Gun
Retreat

Bestiary
Battle Folio
Thief Hat
Secret Clothes
Wizard Mantle

Shuriken, Bomb, Ninja Sword, Axe
Level Jump8, Vertical Jump7



Benticore
Male
Libra
71
57
Monk
Throw
Parry
Magic Defense UP
Jump+2



Black Hood
Wizard Outfit
Reflect Ring

Pummel, Earth Slash, Purification, Revive
Shuriken, Wand



Sairentozon7
Female
Virgo
76
50
Priest
Jump
Abandon
Sicken
Move+3

Rainbow Staff

Triangle Hat
Wizard Robe
Feather Mantle

Cure 2, Cure 4, Raise, Raise 2, Esuna
Level Jump3, Vertical Jump8



Jaritras
Male
Pisces
46
44
Calculator
Time Magic
Counter
Equip Shield
Move-HP Up

Madlemgen
Round Shield
Ribbon
Wizard Robe
Angel Ring

CT, Prime Number, 5, 4, 3
Haste, Slow 2, Stop, Demi

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



KasugaiRoastedPeas
Male
Libra
73
46
Oracle
Talk Skill
Catch
Monster Talk
Move-HP Up

Cypress Rod

Flash Hat
Silk Robe
Feather Mantle

Spell Absorb, Life Drain, Pray Faith, Zombie, Blind Rage
Persuade, Preach, Insult, Mimic Daravon, Refute, Rehabilitate



CapnChaos12
Male
Virgo
56
72
Mime

MP Restore
Martial Arts
Fly


Aegis Shield
Red Hood
Power Sleeve
Chantage

Mimic




Waterwatereverywhere
Female
Aquarius
75
41
Wizard
Draw Out
Mana Shield
Short Status
Waterwalking

Mythril Knife

Flash Hat
Adaman Vest
Small Mantle

Fire, Bolt, Bolt 4, Ice, Ice 3, Frog, Flare
Koutetsu, Heaven's Cloud



Kaelsun
Male
Leo
47
57
Priest
Battle Skill
Counter Flood
Halve MP
Move+2

Morning Star

Black Hood
Brigandine
Power Wrist

Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Esuna, Magic Barrier
Shield Break, Magic Break, Speed Break

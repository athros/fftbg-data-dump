Final Bets: blue - 9 bets for 8,204G (47.4%, x1.11); champion - 14 bets for 9,098G (52.6%, x0.90)

blue bets:
DudeMonkey77: 3,484G (42.5%, 3,484G)
Rurk: 1,500G (18.3%, 11,255G)
Smokegiant: 1,000G (12.2%, 84,820G)
reinoe: 1,000G (12.2%, 35,939G)
Cryptopsy70: 620G (7.6%, 17,777G)
ArashiKurobara: 200G (2.4%, 18,275G)
casteia: 200G (2.4%, 3,098G)
E_Ballard: 100G (1.2%, 4,780G)
nhammen: 100G (1.2%, 9,858G)

champion bets:
ShintaroNayaka: 3,050G (33.5%, 4,654G)
Zagorsek: 1,290G (14.2%, 1,290G)
BirbBrainsBot: 1,000G (11.0%, 133,716G)
Sans_from_Snowdin: 1,000G (11.0%, 22,344G)
TheChainNerd: 802G (8.8%, 3,366G)
getthemoneyz: 520G (5.7%, 1,161,050G)
NightwolfXVI: 236G (2.6%, 236G)
SaltiestMage: 212G (2.3%, 212G)
gorgewall: 201G (2.2%, 3,511G)
DuneMeta: 200G (2.2%, 8,420G)
ar_tactic: 200G (2.2%, 63,346G)
ZCKaiser: 187G (2.1%, 375G)
CosmicTactician: 100G (1.1%, 51,125G)
datadrivenbot: 100G (1.1%, 51,578G)

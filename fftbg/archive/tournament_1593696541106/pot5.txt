Final Bets: blue - 9 bets for 8,250G (72.5%, x0.38); yellow - 10 bets for 3,132G (27.5%, x2.63)

blue bets:
Ruvelia_BibeI: 4,000G (48.5%, 12,048G)
reinoe: 2,000G (24.2%, 29,656G)
Rurk: 1,000G (12.1%, 1,871G)
ShintaroNayaka: 604G (7.3%, 604G)
NightwolfXVI: 236G (2.9%, 236G)
DuneMeta: 200G (2.4%, 7,456G)
datadrivenbot: 100G (1.2%, 51,609G)
Sans_from_Snowdin: 100G (1.2%, 26,806G)
SephDarkheart: 10G (0.1%, 1,016G)

yellow bets:
BirbBrainsBot: 1,000G (31.9%, 132,158G)
DudeMonkey77: 700G (22.3%, 700G)
TheChainNerd: 393G (12.5%, 3,930G)
ArashiKurobara: 200G (6.4%, 19,165G)
SaltiestMage: 200G (6.4%, 1,287G)
casteia: 200G (6.4%, 3,236G)
getthemoneyz: 138G (4.4%, 1,160,310G)
gorgewall: 101G (3.2%, 3,320G)
E_Ballard: 100G (3.2%, 4,480G)
Salted_Cliff_Racer: 100G (3.2%, 703G)

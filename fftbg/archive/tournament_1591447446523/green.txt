Player: !Green
Team: Green Team
Palettes: Green/White



LeoNightFury
Male
Capricorn
63
69
Priest
Talk Skill
Counter Flood
Short Charge
Retreat

Healing Staff

Flash Hat
Chameleon Robe
Defense Armlet

Cure, Cure 2, Cure 3, Cure 4, Reraise, Regen, Esuna
Solution, Refute



Victoriolue
Male
Libra
39
74
Squire
Jump
MP Restore
Long Status
Jump+3

Materia Blade
Flame Shield
Headgear
Crystal Mail
Leather Mantle

Accumulate, Throw Stone, Heal, Yell, Ultima
Level Jump4, Vertical Jump3



Nok
Female
Serpentarius
60
70
Geomancer
Black Magic
Counter Tackle
Equip Polearm
Waterwalking

Mythril Spear
Round Shield
Feather Hat
Silk Robe
Power Wrist

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Fire, Fire 2, Ice 3, Ice 4



Error72
Male
Gemini
76
44
Knight
Item
Abandon
Short Charge
Move+3

Mythril Sword
Genji Shield
Iron Helmet
Mythril Armor
N-Kai Armlet

Weapon Break, Magic Break, Speed Break, Justice Sword, Night Sword, Surging Sword
X-Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Remedy, Phoenix Down

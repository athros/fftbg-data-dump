Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Letdowncity
Female
Cancer
67
51
Dancer
Black Magic
Speed Save
Equip Armor
Waterwalking

Hydra Bag

Bronze Helmet
Genji Armor
Genji Gauntlet

Witch Hunt, Slow Dance, Void Storage
Fire



Ruleof5
Monster
Cancer
42
65
Steel Giant










Mesmaster
Female
Cancer
52
62
Wizard
Battle Skill
MP Restore
Short Charge
Move+2

Ice Rod

Golden Hairpin
Black Robe
Magic Gauntlet

Fire 4, Bolt 2, Ice 2, Ice 3, Empower
Head Break, Magic Break, Justice Sword, Night Sword



Lemonjohns
Female
Cancer
76
44
Lancer
Time Magic
Mana Shield
Beastmaster
Move-HP Up

Gungnir
Round Shield
Diamond Helmet
Mythril Armor
Battle Boots

Level Jump8, Vertical Jump8
Haste, Haste 2, Immobilize, Quick, Stabilize Time

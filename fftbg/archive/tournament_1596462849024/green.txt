Player: !Green
Team: Green Team
Palettes: Green/White



PoroTact
Monster
Sagittarius
83
68
Pisco Demon










Sairentozon7
Female
Capricorn
56
49
Summoner
Draw Out
Sunken State
Short Charge
Move+2

Healing Staff

Holy Miter
Linen Robe
Small Mantle

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin
Kiyomori



Sinnyil2
Male
Capricorn
82
63
Geomancer
Item
HP Restore
Martial Arts
Lava Walking


Hero Shield
Cachusha
Light Robe
108 Gems

Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard
Potion, Maiden's Kiss, Holy Water, Phoenix Down



ShintaroNayaka
Female
Taurus
70
57
Thief
Charge
Parry
Equip Shield
Waterbreathing

Coral Sword
Flame Shield
Holy Miter
Brigandine
Germinas Boots

Steal Armor, Steal Status
Charge+2, Charge+3, Charge+4, Charge+20

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rislyeu
Male
Taurus
76
63
Knight
Item
Counter Tackle
Halve MP
Waterbreathing

Long Sword
Escutcheon
Barbuta
Chameleon Robe
Elf Mantle

Head Break, Power Break
Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



Megaman2202
Male
Serpentarius
54
80
Summoner
Steal
Counter Flood
Equip Polearm
Swim

Holy Lance

Twist Headband
Linen Robe
Jade Armlet

Moogle, Shiva, Bahamut, Silf
Steal Heart, Steal Helmet, Arm Aim



TheChainNerd
Female
Cancer
52
81
Calculator
Dragon Skill
Critical Quick
Beastmaster
Jump+1

Thunder Rod

Bronze Helmet
Platinum Armor
Elf Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper



Kellios11
Female
Scorpio
55
40
Lancer
Steal
Sunken State
Martial Arts
Fly


Crystal Shield
Leather Helmet
Platinum Armor
Feather Boots

Level Jump4, Vertical Jump2
Gil Taking, Steal Heart

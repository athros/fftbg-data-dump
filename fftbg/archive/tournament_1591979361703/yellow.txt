Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Thenextlantern
Monster
Scorpio
70
60
Dark Behemoth










Neerrm
Female
Scorpio
40
48
Dancer
Throw
Dragon Spirit
Dual Wield
Fly

Panther Bag
Hydra Bag
Triangle Hat
Leather Outfit
108 Gems

Witch Hunt, Disillusion
Spear, Stick, Wand



Firesheath
Female
Aquarius
43
69
Lancer
Steal
Speed Save
Equip Knife
Waterbreathing

Iga Knife
Diamond Shield
Leather Helmet
Genji Armor
Spike Shoes

Level Jump2, Vertical Jump5
Steal Shield, Steal Accessory, Leg Aim



Old Overholt
Female
Aquarius
51
53
Calculator
Yin Yang Magic
Dragon Spirit
Equip Polearm
Jump+2

Ryozan Silk

Leather Hat
Light Robe
Reflect Ring

CT, Prime Number, 5
Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Sleep

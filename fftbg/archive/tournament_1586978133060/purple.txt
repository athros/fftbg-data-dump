Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ANFz
Male
Sagittarius
56
59
Bard
Talk Skill
Counter Magic
Doublehand
Waterbreathing

Lightning Bow

Feather Hat
Reflect Mail
Defense Armlet

Life Song, Battle Song, Last Song
Invitation, Threaten, Negotiate, Refute



CapnChaos12
Female
Libra
62
54
Summoner
Yin Yang Magic
Hamedo
Short Charge
Jump+2

Ice Rod

Cachusha
Chain Vest
Spike Shoes

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Salamander
Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy



OneHundredFists
Female
Gemini
61
68
Ninja
Jump
Parry
Martial Arts
Swim



Leather Hat
Leather Outfit
N-Kai Armlet

Bomb, Knife, Ninja Sword, Stick
Level Jump5, Vertical Jump8



Chuckolator
Female
Aries
43
37
Time Mage
Math Skill
MP Restore
Beastmaster
Move-HP Up

Papyrus Codex

Holy Miter
Silk Robe
Dracula Mantle

Haste, Slow, Stop, Immobilize, Quick, Stabilize Time
CT, Height, 5, 3

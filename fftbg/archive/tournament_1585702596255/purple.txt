Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Baron Von Scrub
Female
Virgo
71
78
Calculator
Bio
MA Save
Equip Knife
Move-HP Up

Spell Edge

Barette
Leather Outfit
Feather Boots

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Thed63
Male
Leo
55
52
Ninja
Charge
Regenerator
Doublehand
Move-MP Up

Assassin Dagger

Black Hood
Chain Vest
Defense Armlet

Dictionary
Charge+2, Charge+4



SeraphPDH
Male
Gemini
43
78
Oracle
Draw Out
Abandon
Defense UP
Jump+2

Cypress Rod

Headgear
Wizard Robe
Bracer

Spell Absorb, Confusion Song, Dispel Magic
Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji



Alrightbye
Female
Gemini
48
74
Chemist
Draw Out
Catch
Equip Axe
Fly

Flame Whip

Green Beret
Adaman Vest
Sprint Shoes

Potion, X-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Heaven's Cloud, Kiyomori, Muramasa

Player: !Green
Team: Green Team
Palettes: Green/White



Old Overholt
Monster
Cancer
50
61
Explosive










ThePineappleSalesman
Male
Scorpio
63
64
Mediator
Throw
Dragon Spirit
Maintenance
Move+1

Papyrus Codex

Twist Headband
Light Robe
108 Gems

Persuade, Solution, Negotiate, Refute, Rehabilitate
Bomb



HaateXIII
Female
Leo
46
53
Summoner
Elemental
Distribute
Equip Gun
Lava Walking

Romanda Gun

Holy Miter
Brigandine
Magic Gauntlet

Moogle, Shiva, Ramuh, Titan, Carbunkle, Leviathan, Cyclops
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard



GrandmasterFrankerZ
Male
Capricorn
72
75
Samurai
Basic Skill
PA Save
Beastmaster
Levitate

Murasame

Bronze Helmet
Genji Armor
Jade Armlet

Koutetsu, Heaven's Cloud, Kikuichimoji
Heal, Yell

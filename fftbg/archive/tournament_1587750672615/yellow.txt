Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Laserman1000
Male
Taurus
76
56
Archer
Punch Art
Counter Magic
Equip Knife
Retreat

Thunder Rod
Flame Shield
Leather Hat
Power Sleeve
Dracula Mantle

Charge+3
Purification, Revive



GenieMilenko
Female
Taurus
62
48
Monk
Summon Magic
Dragon Spirit
Sicken
Move-MP Up



Golden Hairpin
Power Sleeve
Angel Ring

Spin Fist, Secret Fist, Purification, Revive, Seal Evil
Moogle, Shiva, Silf, Cyclops



Killth3kid
Monster
Taurus
71
78
Ultima Demon










Joewcarson
Male
Taurus
49
73
Summoner
Battle Skill
Absorb Used MP
Concentrate
Ignore Terrain

Wizard Rod

Holy Miter
Black Costume
Genji Gauntlet

Moogle, Ifrit, Carbunkle, Leviathan, Silf
Shield Break, Speed Break, Power Break

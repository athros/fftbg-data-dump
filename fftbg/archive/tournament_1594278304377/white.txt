Player: !White
Team: White Team
Palettes: White/Blue



Evewho
Female
Aquarius
79
54
Samurai
Black Magic
MA Save
Equip Armor
Teleport 2

Holy Lance

Twist Headband
Chain Mail
Elf Mantle

Koutetsu, Heaven's Cloud
Fire 2, Bolt 2, Bolt 3, Ice 2, Frog



Prince Rogers Nelson
Female
Cancer
76
69
Priest
Summon Magic
Counter Tackle
Equip Axe
Waterwalking

Slasher

Black Hood
White Robe
Genji Gauntlet

Cure, Cure 2, Cure 3, Raise, Raise 2, Protect 2, Shell, Wall, Esuna
Moogle, Titan, Golem, Carbunkle, Leviathan, Salamander, Silf, Cyclops, Zodiac



Lowlf
Female
Libra
72
48
Wizard
Summon Magic
Earplug
Equip Bow
Ignore Height

Lightning Bow

Red Hood
Wizard Outfit
Magic Gauntlet

Bolt, Bolt 2, Bolt 4, Empower, Frog, Death
Ramuh, Ifrit, Golem, Carbunkle, Fairy, Lich



Extinctrational
Male
Taurus
54
80
Ninja
Summon Magic
Hamedo
Equip Armor
Move+3

Hidden Knife
Sasuke Knife
Twist Headband
Platinum Armor
Germinas Boots

Shuriken, Bomb, Wand
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Salamander, Silf, Lich

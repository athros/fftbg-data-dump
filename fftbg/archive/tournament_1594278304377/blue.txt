Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Twelfthrootoftwo
Female
Libra
50
55
Geomancer
Yin Yang Magic
MP Restore
Magic Attack UP
Move-MP Up

Slasher
Diamond Shield
Thief Hat
Light Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Life Drain, Zombie, Blind Rage, Dispel Magic



UmaiJam
Female
Gemini
48
51
Wizard
Elemental
Counter
Attack UP
Jump+3

Dragon Rod

Holy Miter
Chameleon Robe
Magic Gauntlet

Fire 2, Bolt 2, Bolt 3, Ice 3, Empower, Flare
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



TeaTime29
Male
Sagittarius
44
59
Time Mage
Steal
Distribute
Defense UP
Fly

Wizard Staff

Red Hood
Chameleon Robe
N-Kai Armlet

Haste, Slow 2, Float, Quick
Steal Armor, Steal Shield, Arm Aim



Deathmaker06
Female
Serpentarius
46
60
Mime

Counter Magic
Monster Talk
Move+2



Twist Headband
Brigandine
Cherche

Mimic


Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Estan AD
Male
Libra
72
79
Mime

Regenerator
Attack UP
Move+2



Golden Hairpin
Earth Clothes
Jade Armlet

Mimic




ALY327
Female
Leo
60
46
Oracle
Talk Skill
Damage Split
Equip Polearm
Move+3

Iron Fan

Leather Hat
Black Robe
Magic Ring

Poison, Dispel Magic, Paralyze, Petrify
Threaten, Preach, Mimic Daravon, Refute



Shalloween
Male
Leo
73
72
Ninja
Steal
Speed Save
Attack UP
Move+1

Sasuke Knife
Hidden Knife
Flash Hat
Power Sleeve
Elf Mantle

Shuriken, Bomb
Steal Heart, Steal Helmet, Steal Accessory, Steal Status, Leg Aim



Zepharoth89
Female
Taurus
73
41
Summoner
Steal
Catch
Short Charge
Fly

Ice Rod

Twist Headband
Adaman Vest
Rubber Shoes

Titan, Carbunkle, Leviathan, Fairy
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory

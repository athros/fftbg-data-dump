Player: !Green
Team: Green Team
Palettes: Green/White



EnemyController
Female
Libra
74
49
Geomancer
Draw Out
HP Restore
Halve MP
Retreat

Koutetsu Knife
Genji Shield
Triangle Hat
White Robe
Cherche

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Murasame, Kiyomori, Kikuichimoji



Helpimabug
Monster
Taurus
52
63
Ahriman










KaLam1ty
Male
Pisces
69
69
Calculator
Dungeon Skill
Sunken State
Beastmaster
Waterbreathing

Wizard Rod
Round Shield
Thief Hat
Silk Robe
Wizard Mantle

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



LDSkinny
Female
Virgo
77
65
Oracle
Charge
Damage Split
Doublehand
Move+3

Musk Rod

Triangle Hat
Mythril Vest
Salty Rage

Spell Absorb, Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Charge+4, Charge+5, Charge+7

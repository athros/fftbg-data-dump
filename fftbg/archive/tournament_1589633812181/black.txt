Player: !Black
Team: Black Team
Palettes: Black/Red



J2DaBibbles
Female
Capricorn
42
45
Oracle
Battle Skill
Counter
Equip Axe
Ignore Height

Wizard Staff

Black Hood
White Robe
Vanish Mantle

Pray Faith, Zombie, Foxbird, Paralyze, Sleep, Petrify
Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Night Sword



Wooyuji
Female
Scorpio
53
55
Wizard
Basic Skill
MP Restore
Equip Gun
Levitate

Bestiary

Ribbon
Earth Clothes
Dracula Mantle

Fire 2, Fire 3, Bolt 2, Ice 3, Ice 4, Flare
Heal, Fury



Leakimiko
Female
Cancer
72
81
Wizard
Summon Magic
Regenerator
Short Charge
Retreat

Cultist Dagger

Holy Miter
Chameleon Robe
Wizard Mantle

Fire, Bolt 2, Bolt 4, Ice 2, Flare
Ramuh, Ifrit, Titan, Golem



Durand
Male
Virgo
55
36
Ninja
Sing
Critical Quick
Long Status
Levitate

Ninja Edge
Air Knife
Golden Hairpin
Wizard Outfit
Battle Boots

Shuriken, Bomb, Knife
Angel Song, Life Song, Battle Song

Player: !Green
Team: Green Team
Palettes: Green/White



Killth3kid
Male
Aries
64
78
Ninja
Steal
Arrow Guard
Equip Gun
Ignore Terrain

Romanda Gun
Blast Gun
Golden Hairpin
Mystic Vest
Bracer

Shuriken, Ninja Sword
Gil Taking, Steal Shield, Steal Accessory, Steal Status, Arm Aim



ExecutedGiraffe
Male
Taurus
50
50
Wizard
Throw
Hamedo
Equip Axe
Retreat

Flame Whip

Headgear
Mystic Vest
Small Mantle

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 4
Shuriken



PotionDweller
Male
Aries
75
79
Wizard
Summon Magic
Arrow Guard
Equip Shield
Waterbreathing

Thunder Rod
Round Shield
Feather Hat
Judo Outfit
Chantage

Fire 3, Bolt 4, Ice, Death
Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Salamander, Silf, Cyclops



Superdevon1
Male
Leo
51
60
Mime

Counter Tackle
Martial Arts
Jump+3



Green Beret
Chain Vest
108 Gems

Mimic


Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Male
Gemini
78
75
Monk
Battle Skill
Meatbone Slash
Attack UP
Retreat



Black Hood
Clothes
Wizard Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Night Sword



Tony Saco
Monster
Scorpio
56
61
Dryad










ElephantChunks
Male
Pisces
73
55
Geomancer
Punch Art
Abandon
Attack UP
Waterwalking

Giant Axe
Buckler
Holy Miter
Mythril Vest
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Spin Fist, Pummel, Earth Slash, Purification, Revive



Astral1986
Male
Leo
46
41
Squire
Sing
Counter Magic
Equip Sword
Teleport

Defender

Iron Helmet
Chain Mail
Genji Gauntlet

Throw Stone, Heal, Yell, Wish
Battle Song, Magic Song, Space Storage, Sky Demon

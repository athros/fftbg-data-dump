Player: !White
Team: White Team
Palettes: White/Blue



ZCKaiser
Female
Virgo
38
65
Dancer
Draw Out
Critical Quick
Defense UP
Waterwalking

Hydra Bag

Triangle Hat
Robe of Lords
Bracer

Witch Hunt, Slow Dance, Polka Polka, Obsidian Blade
Heaven's Cloud, Muramasa



J2DaBibbles
Female
Libra
78
50
Time Mage
Yin Yang Magic
MA Save
Doublehand
Levitate

Octagon Rod

Black Hood
Mythril Vest
N-Kai Armlet

Slow, Stop, Immobilize, Float, Demi, Meteor
Spell Absorb, Blind Rage, Confusion Song, Dispel Magic, Sleep, Petrify



Boosted420
Monster
Leo
54
80
Steel Giant










Bahamutlagooon
Male
Gemini
45
64
Knight
Sing
Faith Save
Equip Bow
Retreat

Silver Bow
Flame Shield
Iron Helmet
Linen Robe
Magic Ring

Speed Break, Stasis Sword, Justice Sword
Angel Song, Diamond Blade, Space Storage, Sky Demon

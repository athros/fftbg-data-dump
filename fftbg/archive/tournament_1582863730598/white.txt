Player: !White
Team: White Team
Palettes: White/Blue



LanseDM
Female
Sagittarius
73
81
Knight
Basic Skill
Damage Split
Equip Sword
Waterbreathing

Excalibur

Mythril Helmet
Carabini Mail
Reflect Ring

Weapon Break, Magic Break, Surging Sword
Throw Stone, Heal, Tickle



Dexsana
Female
Capricorn
52
50
Squire
Elemental
Abandon
Beastmaster
Swim

Broad Sword

Platinum Helmet
Earth Clothes
Diamond Armlet

Dash, Throw Stone, Heal, Yell, Ultima
Pitfall, Local Quake, Will-O-Wisp, Blizzard



JustDoomathon
Male
Libra
66
72
Monk
Draw Out
PA Save
Concentrate
Swim



Holy Miter
Chain Vest
Small Mantle

Wave Fist, Purification, Revive, Seal Evil
Asura



I Nod My Head When I Lose
Female
Sagittarius
74
64
Mime

Earplug
Maintenance
Move-MP Up



Holy Miter
Leather Outfit
Cursed Ring

Mimic


Player: !Black
Team: Black Team
Palettes: Black/Red



Zeroroute
Male
Capricorn
80
61
Oracle
Jump
Critical Quick
Halve MP
Jump+3

Iron Fan

Headgear
Linen Robe
Defense Ring

Blind, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song
Level Jump2, Vertical Jump4



Pizzapartytime
Monster
Leo
52
48
Ultima Demon










Lordminsc
Male
Aries
54
73
Lancer
Black Magic
Abandon
Long Status
Jump+1

Spear
Diamond Shield
Cross Helmet
Silk Robe
Cursed Ring

Level Jump5, Vertical Jump5
Bolt 2



RoachCarnival
Female
Aquarius
53
66
Calculator
Time Magic
Meatbone Slash
Defend
Move+3

Bestiary

Black Hood
Chameleon Robe
Elf Mantle

CT, Height, Prime Number, 4
Stop, Immobilize, Float, Reflect, Stabilize Time, Meteor

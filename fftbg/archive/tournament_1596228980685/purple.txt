Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



KSops
Female
Sagittarius
76
49
Archer
Dance
Meatbone Slash
Equip Gun
Lava Walking

Blaze Gun
Mythril Shield
Cachusha
Adaman Vest
Germinas Boots

Charge+2, Charge+7
Disillusion, Obsidian Blade



E Ballard
Male
Aries
57
47
Thief
Elemental
Distribute
Magic Defense UP
Levitate

Orichalcum

Black Hood
Wizard Outfit
Power Wrist

Steal Weapon, Steal Accessory, Steal Status
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard



Grandlanzer
Female
Libra
56
46
Archer
Punch Art
Sunken State
Concentrate
Fly

Windslash Bow

Circlet
Black Costume
Diamond Armlet

Charge+1, Charge+5, Charge+10
Wave Fist, Earth Slash, Purification, Revive



Moonliquor
Female
Virgo
76
72
Calculator
Wildfire Skill
Absorb Used MP
Sicken
Waterbreathing

Flame Rod
Flame Shield
Gold Helmet
Chain Mail
108 Gems

Blue Magic
Bite, Self Destruct, Flame Attack, Small Bomb, Spark, Leaf Dance, Protect Spirit, Calm Spirit, Spirit of Life, Magic Spirit

Player: !White
Team: White Team
Palettes: White/Blue



LordTomS
Male
Pisces
51
73
Monk
Charge
PA Save
Equip Bow
Ignore Height

Night Killer

Red Hood
Mystic Vest
Elf Mantle

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Charge+4, Charge+7, Charge+10, Charge+20



Killth3kid
Female
Sagittarius
73
71
Mediator
Battle Skill
MP Restore
Defend
Lava Walking

Blaze Gun

Twist Headband
Mythril Vest
Rubber Shoes

Invitation, Preach, Mimic Daravon, Refute
Armor Break, Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword



Nizaha
Female
Aries
65
68
Thief
Item
Counter Flood
Short Status
Levitate

Mythril Knife

Headgear
Secret Clothes
Diamond Armlet

Steal Heart, Steal Armor, Steal Shield, Steal Status
Potion, X-Potion, Hi-Ether, Remedy



Forkmore
Male
Libra
56
58
Summoner
Item
Arrow Guard
Martial Arts
Lava Walking

Thunder Rod

Black Hood
Judo Outfit
Genji Gauntlet

Moogle, Shiva, Titan, Carbunkle, Odin, Salamander, Lich
Potion, Hi-Potion, X-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



Grininda
Male
Virgo
47
40
Chemist
Steal
Counter
Long Status
Ignore Height

Star Bag

Leather Hat
Brigandine
Sprint Shoes

Potion, X-Potion, Maiden's Kiss, Holy Water, Phoenix Down
Steal Shield, Steal Status



Maakur
Female
Cancer
56
71
Samurai
Black Magic
Abandon
Doublehand
Teleport 2

Muramasa

Bronze Helmet
Carabini Mail
Leather Mantle

Koutetsu, Murasame, Heaven's Cloud
Fire, Fire 3, Bolt, Bolt 4, Ice, Empower



Toka222
Male
Aries
71
50
Archer
Steal
Parry
Secret Hunt
Teleport 2

Glacier Gun
Crystal Shield
Green Beret
Power Sleeve
Vanish Mantle

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Steal Helmet, Steal Weapon, Steal Accessory



TeaTime29
Female
Libra
58
46
Thief
Talk Skill
Absorb Used MP
Dual Wield
Move+2

Spell Edge
Ninja Edge
Red Hood
Mystic Vest
Feather Mantle

Gil Taking, Steal Helmet, Steal Shield, Steal Status, Leg Aim
Invitation, Solution, Insult, Refute, Rehabilitate

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Male
Aries
49
66
Chemist
Jump
Counter Magic
Attack UP
Ignore Terrain

Panther Bag

Holy Miter
Judo Outfit
Germinas Boots

Potion, Hi-Ether, Eye Drop, Soft, Phoenix Down
Level Jump8, Vertical Jump3



BigDLicious91
Female
Aries
70
42
Chemist
Summon Magic
Counter Tackle
Short Status
Waterbreathing

Cute Bag

Green Beret
Clothes
Genji Gauntlet

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Shiva, Titan, Carbunkle, Leviathan, Lich



Deciimos
Male
Cancer
48
46
Archer
Draw Out
Counter Magic
Doublehand
Jump+2

Poison Bow

Twist Headband
Black Costume
Germinas Boots

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Muramasa



DavenIII
Male
Taurus
41
46
Monk
Charge
HP Restore
Equip Polearm
Move+3

Gokuu Rod

Feather Hat
Judo Outfit
Bracer

Spin Fist, Purification, Chakra, Revive, Seal Evil
Charge+3, Charge+4, Charge+5

Player: !White
Team: White Team
Palettes: White/Blue



RjA0zcOQ96
Monster
Pisces
79
80
Cockatrice










Omegasuspekt
Male
Cancer
65
38
Monk
Charge
Caution
Equip Axe
Levitate

Gold Staff

Golden Hairpin
Leather Outfit
Wizard Mantle

Spin Fist, Pummel, Wave Fist, Chakra, Seal Evil
Charge+4



KasugaiRoastedPeas
Male
Aries
67
53
Ninja
Charge
Damage Split
Martial Arts
Jump+2

Sasuke Knife
Flail
Twist Headband
Wizard Outfit
Reflect Ring

Bomb, Stick, Wand
Charge+1, Charge+2, Charge+7



Gorgewall
Male
Capricorn
80
76
Monk
Elemental
Counter Magic
Equip Axe
Ignore Terrain

White Staff

Triangle Hat
Rubber Costume
Leather Mantle

Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

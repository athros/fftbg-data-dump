Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lijarkh
Male
Gemini
61
67
Monk
Sing
Absorb Used MP
Magic Attack UP
Jump+1



Black Hood
Judo Outfit
Bracer

Wave Fist, Purification, Revive, Seal Evil
Nameless Song



Vicemond
Female
Leo
70
74
Oracle
Charge
Abandon
Long Status
Move-MP Up

Iron Fan

Headgear
Judo Outfit
Spike Shoes

Spell Absorb, Silence Song, Dispel Magic
Charge+1, Charge+2, Charge+3, Charge+20



L2 Sentinel
Male
Scorpio
63
48
Monk
Yin Yang Magic
Counter
Attack UP
Swim



Flash Hat
Leather Outfit
Magic Gauntlet

Secret Fist, Purification, Revive
Spell Absorb, Dispel Magic, Paralyze, Petrify, Dark Holy



Anox Skell
Female
Leo
68
57
Oracle
Throw
Sunken State
Defense UP
Jump+3

Ivory Rod

Black Hood
Black Robe
Cherche

Blind, Spell Absorb, Life Drain, Zombie, Blind Rage, Dispel Magic, Sleep, Petrify, Dark Holy
Staff

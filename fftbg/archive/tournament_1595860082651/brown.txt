Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Phi Sig
Male
Pisces
72
65
Calculator
Black Magic
Parry
Short Charge
Move+1

Octagon Rod

Red Hood
Power Sleeve
Cursed Ring

CT, Prime Number, 5, 4, 3
Fire, Fire 3, Bolt 3, Ice 3



Brainstew29
Male
Pisces
73
42
Chemist
Steal
Arrow Guard
Concentrate
Waterbreathing

Hydra Bag

Flash Hat
Brigandine
Power Wrist

Potion, Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Steal Armor, Steal Shield, Arm Aim



CosmicTactician
Male
Virgo
59
76
Calculator
Yin Yang Magic
Parry
Equip Sword
Move-HP Up

Muramasa

Triangle Hat
Power Sleeve
Magic Gauntlet

CT, Prime Number, 5, 4
Life Drain, Silence Song, Sleep



Just Here2
Female
Taurus
59
63
Squire
Item
Faith Save
Defense UP
Teleport

Long Sword
Aegis Shield
Barbuta
Mythril Vest
Diamond Armlet

Dash, Throw Stone, Heal, Yell
Hi-Potion, X-Potion, Ether, Antidote, Holy Water, Remedy, Phoenix Down

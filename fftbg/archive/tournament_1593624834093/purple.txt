Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



FriendSquirrel
Male
Libra
55
68
Lancer
Elemental
Counter Flood
Doublehand
Move+2

Holy Lance

Iron Helmet
Silk Robe
Sprint Shoes

Level Jump8, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball



ShintaroNayaka
Male
Capricorn
79
56
Priest
Talk Skill
Caution
Secret Hunt
Move+3

Flame Whip

Red Hood
Clothes
Cursed Ring

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell, Esuna
Persuade, Preach



Fenaen
Male
Leo
70
54
Geomancer
Throw
Counter Tackle
Halve MP
Teleport

Rune Blade
Platinum Shield
Red Hood
Wizard Robe
Magic Ring

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Staff



ZombiFrancis
Female
Gemini
61
76
Lancer
Yin Yang Magic
MP Restore
Short Status
Move+3

Mythril Spear
Escutcheon
Bronze Helmet
Gold Armor
Cursed Ring

Level Jump5, Vertical Jump7
Blind, Poison, Doubt Faith, Silence Song, Confusion Song, Dispel Magic, Dark Holy

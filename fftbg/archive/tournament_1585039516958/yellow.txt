Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Djorama
Male
Capricorn
62
50
Oracle
Jump
Critical Quick
Doublehand
Fly

Battle Bamboo

Holy Miter
Linen Robe
N-Kai Armlet

Silence Song, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Level Jump3, Vertical Jump7



Clar3d
Male
Aries
71
61
Knight
Throw
Mana Shield
Equip Polearm
Fly

Mythril Spear
Mythril Shield
Bronze Helmet
Platinum Armor
Dracula Mantle

Shield Break, Speed Break, Power Break, Justice Sword
Dictionary



Chuckolator
Male
Pisces
76
57
Wizard
Jump
Catch
Halve MP
Jump+1

Wizard Rod

Ribbon
Wizard Outfit
Wizard Mantle

Bolt 2, Ice 3, Ice 4
Level Jump2, Vertical Jump3



Enders57
Male
Leo
56
68
Priest
Black Magic
Counter Flood
Defense UP
Jump+1

Flame Whip

Golden Hairpin
White Robe
Small Mantle

Cure, Cure 3, Raise, Raise 2, Reraise, Regen, Shell, Shell 2, Esuna, Holy
Fire 3, Bolt 2, Ice 2, Ice 3

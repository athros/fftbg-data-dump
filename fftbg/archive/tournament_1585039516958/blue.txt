Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Theatheologist
Female
Gemini
43
61
Geomancer
Charge
Mana Shield
Equip Bow
Levitate

Bow Gun
Buckler
Golden Hairpin
Adaman Vest
N-Kai Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Charge+1, Charge+2



Bad1dea
Male
Cancer
41
43
Squire
Item
Distribute
Dual Wield
Move+2

Scorpion Tail
Flame Whip
Iron Helmet
Power Sleeve
Power Wrist

Throw Stone, Heal
Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down



MalakoFox
Female
Gemini
43
47
Thief
Dance
Speed Save
Equip Shield
Lava Walking

Assassin Dagger
Diamond Shield
Leather Hat
Chain Vest
Defense Armlet

Gil Taking, Steal Accessory, Arm Aim
Witch Hunt, Slow Dance, Disillusion, Nether Demon, Dragon Pit



Jethrothrul
Male
Serpentarius
75
49
Mediator
Jump
Earplug
Long Status
Move+2

Glacier Gun

Thief Hat
Black Robe
Vanish Mantle

Solution, Death Sentence, Negotiate, Refute, Rehabilitate
Level Jump8, Vertical Jump6

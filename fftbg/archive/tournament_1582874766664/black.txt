Player: !Black
Team: Black Team
Palettes: Black/Red



Deathmaker06
Male
Aries
67
39
Time Mage
Basic Skill
Catch
Equip Shield
Jump+2

Wizard Staff
Mythril Shield
Golden Hairpin
Secret Clothes
Bracer

Haste 2, Slow 2, Stop, Demi 2, Stabilize Time
Dash, Heal, Tickle, Wish, Scream



Omega2040
Male
Virgo
48
52
Ninja
Charge
Sunken State
Equip Knife
Jump+2

Dragon Rod
Poison Rod
Twist Headband
Mystic Vest
N-Kai Armlet

Knife, Hammer
Charge+3



NoNotBees
Female
Taurus
75
79
Lancer
White Magic
Hamedo
Equip Axe
Move+2

Morning Star
Escutcheon
Platinum Helmet
Black Robe
Jade Armlet

Level Jump4, Vertical Jump5
Cure 2, Cure 3, Cure 4, Raise, Shell 2, Wall



Upvla
Male
Taurus
68
68
Bard
Yin Yang Magic
Caution
Beastmaster
Jump+3

Bloody Strings

Black Hood
Chain Mail
Jade Armlet

Magic Song, Diamond Blade, Space Storage, Sky Demon
Poison, Spell Absorb, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze

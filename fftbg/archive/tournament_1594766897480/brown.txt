Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Kellios11
Male
Sagittarius
59
58
Time Mage
Basic Skill
Catch
Halve MP
Waterwalking

Gokuu Rod

Red Hood
Clothes
Battle Boots

Slow, Float, Reflect, Demi 2
Dash, Heal, Tickle, Cheer Up, Fury, Wish



KonzeraLive
Male
Leo
57
42
Lancer
Black Magic
Distribute
Short Charge
Lava Walking

Partisan
Crystal Shield
Genji Helmet
Linen Cuirass
Jade Armlet

Level Jump8, Vertical Jump7
Fire, Bolt 2, Bolt 4, Ice 2, Frog



Joewcarson
Female
Libra
66
74
Priest
Punch Art
PA Save
Equip Armor
Move+1

Rainbow Staff

Platinum Helmet
Linen Cuirass
Magic Ring

Cure 4, Raise, Raise 2, Reraise, Shell, Shell 2, Esuna
Chakra, Revive, Seal Evil



O Heyno
Female
Libra
76
43
Ninja
Item
Faith Save
Throw Item
Jump+1

Short Edge
Flame Whip
Headgear
Brigandine
Elf Mantle

Shuriken, Bomb, Axe
Potion, X-Potion, Ether, Antidote, Eye Drop, Remedy, Phoenix Down

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



0v3rr8d
Male
Libra
38
64
Oracle
Black Magic
Parry
Short Charge
Jump+1

Battle Folio

Triangle Hat
Judo Outfit
Spike Shoes

Poison, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Sleep
Fire 3, Fire 4, Bolt, Ice, Ice 4, Flare



Fenixcrest
Female
Gemini
67
62
Calculator
Yin Yang Magic
Arrow Guard
Equip Shield
Jump+1

Cute Bag
Genji Shield
Green Beret
Judo Outfit
Spike Shoes

CT, Height, Prime Number, 5, 3
Blind, Poison, Foxbird, Dispel Magic, Sleep



Butterbelljedi
Male
Taurus
69
67
Calculator
Farm Skill
Counter Flood
Concentrate
Jump+2

Dragon Rod
Aegis Shield
Red Hood
Black Costume
Feather Mantle

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



Gelwain
Female
Leo
68
54
Knight
Draw Out
Critical Quick
Doublehand
Waterwalking

Ragnarok

Crystal Helmet
Platinum Armor
Feather Mantle

Head Break, Shield Break, Weapon Break, Speed Break
Asura, Koutetsu

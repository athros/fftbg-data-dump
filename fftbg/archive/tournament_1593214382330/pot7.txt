Final Bets: green - 14 bets for 10,919G (37.2%, x1.69); purple - 16 bets for 18,404G (62.8%, x0.59)

green bets:
SkylerBunny: 2,000G (18.3%, 513,740G)
ShintaroNayaka: 1,355G (12.4%, 1,355G)
fenixcrest: 1,069G (9.8%, 1,069G)
dogsandcatsand: 1,000G (9.2%, 8,037G)
BirbBrainsBot: 1,000G (9.2%, 38,319G)
UmaiJam: 1,000G (9.2%, 19,918G)
Theseawolf1: 1,000G (9.2%, 40,041G)
E_Ballard: 664G (6.1%, 664G)
killth3kid: 500G (4.6%, 11,758G)
YaBoy125: 431G (3.9%, 4,316G)
just_here2: 400G (3.7%, 11,692G)
twelfthrootoftwo: 300G (2.7%, 6,090G)
ApplesauceBoss: 100G (0.9%, 7,137G)
nhammen: 100G (0.9%, 1,079G)

purple bets:
DarrenDinosaurs: 8,163G (44.4%, 8,163G)
Nizaha: 2,652G (14.4%, 2,652G)
Mesmaster: 2,000G (10.9%, 99,414G)
roofiepops: 955G (5.2%, 955G)
Rilgon: 807G (4.4%, 807G)
HaateXIII: 596G (3.2%, 596G)
VolgraTheMoose: 504G (2.7%, 504G)
Shalloween: 500G (2.7%, 76,938G)
sososteve: 400G (2.2%, 3,342G)
getthemoneyz: 382G (2.1%, 1,067,054G)
Oreo_Pizza: 356G (1.9%, 356G)
fluffskull: 300G (1.6%, 1,190G)
RampagingRobot: 281G (1.5%, 281G)
cactus_tony_: 208G (1.1%, 208G)
Ring_Wyrm: 200G (1.1%, 2,300G)
datadrivenbot: 100G (0.5%, 47,893G)

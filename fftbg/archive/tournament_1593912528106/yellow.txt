Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Kaynin Shadowwalker
Male
Sagittarius
63
75
Ninja
Punch Art
Counter Magic
Defense UP
Fly

Mythril Knife
Blind Knife
Black Hood
Clothes
Reflect Ring

Shuriken, Bomb, Axe
Pummel, Wave Fist, Secret Fist, Purification, Revive



Fenixcrest
Male
Gemini
66
79
Archer
Punch Art
Counter
Concentrate
Move+2

Ultimus Bow

Platinum Helmet
Black Costume
Leather Mantle

Charge+1, Charge+3
Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



Breakdown777
Monster
Virgo
64
52
Vampire










DamnThatShark
Male
Serpentarius
66
75
Summoner
Talk Skill
Counter Flood
Equip Knife
Teleport

Orichalcum

Golden Hairpin
Earth Clothes
Rubber Shoes

Moogle, Ifrit, Carbunkle, Odin, Leviathan
Persuade, Praise, Solution, Death Sentence

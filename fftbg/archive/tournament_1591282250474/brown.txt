Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Galkife
Male
Leo
71
74
Priest
Time Magic
Distribute
Long Status
Jump+1

Scorpion Tail

Holy Miter
Chameleon Robe
Cursed Ring

Cure 3, Raise, Reraise, Protect 2, Shell, Shell 2, Esuna
Haste 2, Slow, Immobilize, Reflect, Demi, Stabilize Time



Brokenknight201
Female
Leo
53
65
Ninja
Punch Art
Faith Save
Equip Gun
Move+2

Fairy Harp
Battle Folio
Flash Hat
Power Sleeve
Power Wrist

Shuriken, Staff
Purification, Revive, Seal Evil



DashXero
Male
Aries
53
59
Calculator
Yin Yang Magic
Regenerator
Equip Armor
Move+1

Bestiary

Cross Helmet
Clothes
Magic Gauntlet

CT, Prime Number, 3
Blind, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Petrify



DavenIII
Male
Aquarius
74
77
Thief
Item
Mana Shield
Equip Armor
Waterwalking

Short Edge

Feather Hat
Black Robe
Magic Ring

Steal Armor, Steal Shield
Potion, Antidote, Eye Drop

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RjA0zcOQ96
Female
Sagittarius
70
78
Time Mage
Battle Skill
Critical Quick
Short Status
Jump+2

Healing Staff

Feather Hat
Wizard Outfit
Dracula Mantle

Haste, Float, Reflect, Quick, Demi 2, Stabilize Time
Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword, Night Sword



Ko2q
Male
Scorpio
41
76
Bard
Battle Skill
Distribute
Magic Defense UP
Lava Walking

Mythril Bow

Triangle Hat
Judo Outfit
Defense Armlet

Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Last Song, Space Storage, Hydra Pit
Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Justice Sword



Kronikle
Male
Virgo
79
71
Mime

Counter Tackle
Dual Wield
Ignore Terrain



Twist Headband
Secret Clothes
Germinas Boots

Mimic




Edgehead62888
Male
Cancer
48
67
Monk
Charge
Counter
Equip Polearm
Waterwalking

Cypress Rod

Red Hood
Black Costume
Small Mantle

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Charge+1, Charge+3, Charge+4, Charge+7

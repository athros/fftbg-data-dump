Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ArcLazerbeam
Male
Cancer
52
49
Mime

Caution
Dual Wield
Ignore Height



Headgear
Mystic Vest
Angel Ring

Mimic




Mirapoix
Monster
Sagittarius
75
45
Malboro










Rikrizen
Female
Aries
42
71
Archer
Throw
Critical Quick
Equip Sword
Retreat

Asura Knife
Diamond Shield
Holy Miter
Adaman Vest
108 Gems

Charge+1, Charge+10
Shuriken, Bomb



WhiteDog420
Male
Virgo
47
53
Geomancer
Charge
Mana Shield
Equip Armor
Ignore Terrain

Muramasa
Diamond Shield
Bronze Helmet
Platinum Armor
Reflect Ring

Pitfall, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Charge+5, Charge+7

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Seaweed B
Female
Sagittarius
45
58
Summoner
Draw Out
Distribute
Magic Attack UP
Levitate

Wizard Rod

Leather Hat
White Robe
Defense Armlet

Moogle, Ifrit, Fairy, Lich
Asura, Murasame



J2DaBibbles
Female
Aries
50
75
Time Mage
Throw
PA Save
Short Charge
Waterbreathing

White Staff

Thief Hat
Wizard Outfit
Battle Boots

Haste, Haste 2, Demi 2, Stabilize Time
Shuriken, Bomb



Evewho
Male
Capricorn
70
71
Knight
Item
Earplug
Martial Arts
Fly


Ice Shield
Mythril Helmet
Mythril Armor
Salty Rage

Shield Break, Power Break
Hi-Potion, Soft, Holy Water, Remedy, Phoenix Down



SkylerBunny
Monster
Taurus
64
79
Ultima Demon








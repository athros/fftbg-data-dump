Final Bets: purple - 8 bets for 6,734G (51.5%, x0.94); brown - 9 bets for 6,330G (48.5%, x1.06)

purple bets:
AllInBot: 2,738G (40.7%, 2,738G)
Zeera98: 1,000G (14.9%, 5,872G)
gorgewall: 612G (9.1%, 612G)
Lanshaft: 560G (8.3%, 10,833G)
BaronHaynes: 500G (7.4%, 12,734G)
DesertWooder: 500G (7.4%, 9,616G)
CT_5_Holy: 496G (7.4%, 496G)
ShintaroNayaka: 328G (4.9%, 328G)

brown bets:
DeathTaxesAndAnime: 2,776G (43.9%, 2,776G)
ko2q: 1,000G (15.8%, 11,221G)
NovaKnight21: 552G (8.7%, 552G)
DouglasDragonThePoet: 516G (8.2%, 516G)
Zachara: 500G (7.9%, 141,000G)
BirbBrainsBot: 451G (7.1%, 57,010G)
getthemoneyz: 334G (5.3%, 1,512,603G)
datadrivenbot: 200G (3.2%, 52,159G)
puls3evo: 1G (0.0%, 193G)

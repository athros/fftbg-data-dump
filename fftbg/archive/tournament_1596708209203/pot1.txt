Final Bets: red - 10 bets for 8,126G (48.7%, x1.05); blue - 8 bets for 8,559G (51.3%, x0.95)

red bets:
amiture: 2,000G (24.6%, 20,653G)
ShintaroNayaka: 1,490G (18.3%, 1,490G)
BirbBrainsBot: 1,000G (12.3%, 57,771G)
ColetteMSLP: 1,000G (12.3%, 26,755G)
DouglasDragonThePoet: 669G (8.2%, 669G)
CT_5_Holy: 585G (7.2%, 585G)
Lanshaft: 560G (6.9%, 11,259G)
DesertWooder: 500G (6.2%, 10,247G)
getthemoneyz: 222G (2.7%, 1,513,665G)
Arkreaver: 100G (1.2%, 18,221G)

blue bets:
ko2q: 3,727G (43.5%, 7,454G)
Jeeboheebo: 1,500G (17.5%, 18,315G)
DeathTaxesAndAnime: 1,424G (16.6%, 1,424G)
Zeera98: 1,000G (11.7%, 1,619G)
AllInBot: 307G (3.6%, 307G)
gorgewall: 301G (3.5%, 2,581G)
datadrivenbot: 200G (2.3%, 51,960G)
puls3evo: 100G (1.2%, 100G)

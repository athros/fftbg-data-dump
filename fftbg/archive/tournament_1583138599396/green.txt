Player: !Green
Team: Green Team
Palettes: Green/White



SeedSC
Female
Libra
53
70
Dancer
White Magic
Faith Up
Equip Armor
Swim

Ryozan Silk

Black Hood
Crystal Mail
Bracer

Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Last Dance, Void Storage
Raise, Raise 2, Protect 2, Shell 2, Wall, Esuna



Shs
Male
Scorpio
50
49
Ninja
White Magic
Counter
Equip Armor
Levitate

Dagger
Flame Whip
Crystal Helmet
Bronze Armor
Feather Boots

Shuriken, Bomb, Staff, Axe
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect, Protect 2, Esuna



Manamorphose
Monster
Aquarius
45
77
Steel Giant










Unyubaby
Male
Aquarius
64
61
Chemist
Punch Art
Counter Magic
Magic Defense UP
Jump+1

Blind Knife

Thief Hat
Leather Outfit
Rubber Shoes

Potion, Hi-Potion, Antidote, Echo Grass, Soft, Phoenix Down
Spin Fist, Pummel, Earth Slash, Revive, Seal Evil

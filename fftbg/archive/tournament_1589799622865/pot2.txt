Final Bets: green - 9 bets for 3,000G (42.6%, x1.35); yellow - 6 bets for 4,043G (57.4%, x0.74)

green bets:
Mesmaster: 1,000G (33.3%, 82,442G)
megaman2202: 428G (14.3%, 428G)
SaintOmerville: 381G (12.7%, 381G)
rechaun: 376G (12.5%, 376G)
gorgewall: 201G (6.7%, 12,823G)
AmaninAmide: 200G (6.7%, 8,646G)
Monopool: 200G (6.7%, 1,148G)
Ross_from_Cali: 114G (3.8%, 2,070G)
datadrivenbot: 100G (3.3%, 19,405G)

yellow bets:
ungabunga_bot: 1,000G (24.7%, 526,481G)
BirbBrainsBot: 1,000G (24.7%, 155,636G)
volgrathemoose: 964G (23.8%, 964G)
BoneMiser: 741G (18.3%, 741G)
HorusTaurus: 308G (7.6%, 308G)
getthemoneyz: 30G (0.7%, 683,978G)

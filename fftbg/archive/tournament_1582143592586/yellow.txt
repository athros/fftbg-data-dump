Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Saucy Wings
Female
Leo
55
53
Wizard
Basic Skill
Auto Potion
Secret Hunt
Jump+1

Ice Rod

Headgear
White Robe
Red Shoes

Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Ice, Ice 2
Heal, Tickle, Yell



Off The Crossbar
Male
Aries
71
51
Geomancer
Summon Magic
Catch
Equip Sword
Waterwalking

Save the Queen

Red Hood
Adaman Vest
Leather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Ramuh, Ifrit, Golem, Salamander, Silf



Novapuppets
Female
Sagittarius
48
68
Dancer
Battle Skill
Faith Up
Halve MP
Retreat

Persia

Thief Hat
Earth Clothes
Dracula Mantle

Witch Hunt, Wiznaibus, Disillusion, Last Dance, Dragon Pit
Head Break, Magic Break, Speed Break, Stasis Sword



JonnyCue
Male
Sagittarius
51
75
Bard
Charge
MA Save
Beastmaster
Waterbreathing

Fairy Harp

Red Hood
Gold Armor
N-Kai Armlet

Magic Song, Nameless Song, Hydra Pit
Charge+5, Charge+7, Charge+20

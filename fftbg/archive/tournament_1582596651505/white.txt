Player: !White
Team: White Team
Palettes: White/Blue



Lymonayde
Female
Leo
51
79
Dancer
Summon Magic
HP Restore
Equip Sword
Move-HP Up

Ragnarok

Cachusha
Mythril Vest
Jade Armlet

Witch Hunt, Last Dance, Void Storage
Moogle, Shiva, Titan, Carbunkle, Bahamut, Salamander



WireLord
Male
Aries
70
57
Chemist
White Magic
Parry
Dual Wield
Swim

Main Gauche
Mage Masher
Headgear
Brigandine
Battle Boots

Potion, Hi-Potion, Antidote, Soft, Holy Water, Remedy, Phoenix Down
Cure 2, Cure 3, Cure 4, Raise, Regen, Protect, Protect 2, Shell 2, Esuna, Holy



Sinnyil2
Female
Aquarius
51
66
Samurai
Talk Skill
Counter
Equip Knife
Teleport 2

Spell Edge

Leather Helmet
Maximillian
Sprint Shoes

Koutetsu, Bizen Boat, Kiyomori
Preach, Refute, Rehabilitate



Maicovisky
Female
Libra
60
45
Calculator
White Magic
Counter Flood
Equip Knife
Move+2

Mythril Knife

Triangle Hat
Chain Vest
Jade Armlet

CT, Height, Prime Number, 5, 3
Cure 3, Raise, Shell 2, Esuna, Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Beowulfrulez
Female
Gemini
45
67
Monk
Yin Yang Magic
Distribute
Equip Polearm
Waterbreathing

Partisan

Barette
Mystic Vest
Magic Gauntlet

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Life Drain, Zombie, Silence Song, Blind Rage, Paralyze, Sleep



Joewcarson
Male
Pisces
79
56
Knight
Steal
Distribute
Dual Wield
Lava Walking

Ragnarok
Battle Axe
Barbuta
Silk Robe
Bracer

Weapon Break, Magic Break
Steal Accessory, Arm Aim



Gogofromtogo
Male
Libra
69
65
Knight
Charge
Counter Flood
Beastmaster
Move+2

Defender
Crystal Shield
Iron Helmet
Mythril Armor
Bracer

Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Charge+2, Charge+7, Charge+10



Grandlanzer
Female
Sagittarius
46
45
Chemist
Draw Out
Parry
Short Charge
Retreat

Star Bag

Flash Hat
Judo Outfit
Battle Boots

Potion, Hi-Potion, X-Potion, Ether, Maiden's Kiss, Holy Water
Murasame, Muramasa

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



E Ballard
Male
Cancer
72
80
Knight
Yin Yang Magic
Dragon Spirit
Maintenance
Move-HP Up

Rune Blade
Kaiser Plate
Iron Helmet
Leather Armor
Diamond Armlet

Weapon Break, Power Break, Explosion Sword
Blind, Blind Rage, Confusion Song, Sleep



Kronikle
Female
Virgo
55
67
Lancer
Summon Magic
Speed Save
Long Status
Move+2

Javelin
Diamond Shield
Cross Helmet
Linen Cuirass
Genji Gauntlet

Level Jump8, Vertical Jump2
Moogle, Carbunkle, Bahamut, Odin, Leviathan, Salamander



StealthModeLocke
Male
Leo
73
76
Ninja
Elemental
Speed Save
Halve MP
Lava Walking

Short Edge
Sasuke Knife
Green Beret
Leather Outfit
Leather Mantle

Shuriken, Bomb
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball



Galkife
Female
Scorpio
67
41
Squire
Elemental
Distribute
Magic Attack UP
Ignore Terrain

Flame Whip
Aegis Shield
Golden Hairpin
Crystal Mail
Power Wrist

Heal, Tickle
Pitfall, Water Ball, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

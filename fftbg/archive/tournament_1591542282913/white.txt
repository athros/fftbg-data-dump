Player: !White
Team: White Team
Palettes: White/Blue



CrownOfHorns
Female
Pisces
63
48
Oracle
Time Magic
Critical Quick
Doublehand
Move+3

Cypress Rod

Feather Hat
Black Costume
Germinas Boots

Spell Absorb, Silence Song, Foxbird, Confusion Song, Dispel Magic
Haste 2, Stop, Immobilize, Quick, Demi, Demi 2



RaIshtar
Female
Gemini
71
55
Lancer
Talk Skill
Counter Tackle
Sicken
Jump+3

Spear
Kaiser Plate
Cross Helmet
Chameleon Robe
N-Kai Armlet

Level Jump8, Vertical Jump7
Persuade, Praise, Preach, Negotiate, Rehabilitate



Powergems
Female
Aquarius
50
44
Calculator
Yin Yang Magic
PA Save
Short Status
Lava Walking

Musk Rod

Green Beret
Wizard Outfit
Salty Rage

CT, Height, 5
Doubt Faith, Silence Song, Blind Rage, Paralyze, Dark Holy



BigGlorious
Male
Leo
77
75
Geomancer
Talk Skill
Faith Save
Magic Defense UP
Move-HP Up

Slasher
Escutcheon
Green Beret
Linen Robe
Salty Rage

Water Ball, Hell Ivy, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Threaten, Solution, Death Sentence, Insult

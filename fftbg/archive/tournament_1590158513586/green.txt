Player: !Green
Team: Green Team
Palettes: Green/White



D4rr1n
Female
Capricorn
75
72
Lancer
Black Magic
Sunken State
Equip Knife
Move-HP Up

Main Gauche
Round Shield
Platinum Helmet
Plate Mail
Reflect Ring

Level Jump2, Vertical Jump2
Fire 4, Bolt 2, Bolt 3, Ice 2



DavenIII
Male
Scorpio
60
58
Thief
Throw
Sunken State
Maintenance
Fly

Dagger

Leather Hat
Clothes
Feather Mantle

Steal Armor, Steal Accessory, Arm Aim
Bomb, Ninja Sword, Wand



Zepharoth89
Male
Aries
62
62
Squire
Item
Abandon
Throw Item
Levitate

Cultist Dagger
Gold Shield
Bronze Helmet
Mythril Vest
Red Shoes

Dash, Throw Stone, Heal, Tickle, Yell, Wish
Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down



Lawnboxer
Male
Pisces
68
75
Ninja
Yin Yang Magic
Dragon Spirit
Equip Axe
Ignore Terrain

Gold Staff
Cultist Dagger
Green Beret
Brigandine
Magic Gauntlet

Shuriken, Knife
Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Dark Holy

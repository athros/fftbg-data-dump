Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



D4rr1n
Female
Scorpio
48
44
Knight
Talk Skill
Parry
Monster Talk
Swim

Coral Sword
Buckler
Bronze Helmet
Linen Cuirass
Defense Ring

Armor Break, Weapon Break, Speed Break, Power Break, Stasis Sword
Persuade, Solution, Death Sentence



Ar Tactic
Male
Gemini
78
63
Wizard
Punch Art
Counter Tackle
Long Status
Jump+2

Main Gauche

Thief Hat
Leather Outfit
N-Kai Armlet

Fire, Fire 2, Bolt 2, Ice, Ice 2, Ice 4, Empower
Spin Fist, Earth Slash, Purification, Revive, Seal Evil



Run With Stone GUNs
Male
Gemini
52
62
Lancer
Sing
Mana Shield
Defend
Teleport 2

Holy Lance
Round Shield
Cross Helmet
Genji Armor
Angel Ring

Level Jump8, Vertical Jump7
Angel Song, Battle Song, Magic Song, Last Song, Sky Demon, Hydra Pit



LAGBOT30000
Female
Libra
46
62
Chemist
Throw
Damage Split
Concentrate
Ignore Terrain

Hydra Bag

Feather Hat
Earth Clothes
Spike Shoes

Potion, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Shuriken, Bomb, Sword, Staff

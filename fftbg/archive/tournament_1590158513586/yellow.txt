Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ALY327
Male
Aries
39
50
Lancer
Time Magic
Faith Save
Dual Wield
Swim

Obelisk
Iron Fan
Bronze Helmet
Plate Mail
Power Wrist

Level Jump8, Vertical Jump8
Haste, Slow, Slow 2, Demi, Demi 2, Stabilize Time



Joewcarson
Male
Gemini
42
43
Ninja
Item
MA Save
Attack UP
Move+1

Spell Edge
Kunai
Holy Miter
Clothes
N-Kai Armlet

Bomb, Wand
Potion, Hi-Potion, X-Potion, Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down



Lanouh
Male
Capricorn
51
72
Priest
Talk Skill
Distribute
Dual Wield
Move+3

Morning Star
Gold Staff
Feather Hat
White Robe
Leather Mantle

Cure 2, Shell 2
Preach, Solution, Death Sentence, Insult



JLinkletter
Female
Aries
58
53
Monk
Summon Magic
Counter
Short Charge
Jump+1



Golden Hairpin
Black Costume
N-Kai Armlet

Secret Fist, Purification, Chakra, Revive
Moogle, Ramuh, Ifrit, Golem, Odin, Fairy, Cyclops

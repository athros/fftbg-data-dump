Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Grandlanzer
Male
Libra
80
68
Mediator
Item
Parry
Long Status
Jump+2

Stone Gun

Flash Hat
Brigandine
Wizard Mantle

Praise, Preach, Death Sentence, Insult, Negotiate, Refute
Hi-Potion, Hi-Ether, Echo Grass, Soft, Holy Water, Phoenix Down



HaychDub
Male
Taurus
62
47
Mime

Speed Save
Attack UP
Swim



Feather Hat
Brigandine
N-Kai Armlet

Mimic




HASTERIOUS
Male
Leo
57
59
Ninja
Black Magic
Meatbone Slash
Equip Bow
Move+1

Poison Bow
Poison Bow
Red Hood
Adaman Vest
Sprint Shoes

Shuriken, Knife, Staff
Fire 3, Bolt 2, Bolt 3, Ice 3, Ice 4, Death, Flare



Dogsandcatsand
Male
Capricorn
54
79
Archer
Talk Skill
Hamedo
Monster Talk
Move-HP Up

Yoichi Bow

Leather Hat
Earth Clothes
Diamond Armlet

Charge+1, Charge+3, Charge+5, Charge+7, Charge+20
Threaten, Solution, Death Sentence, Refute, Rehabilitate

Final Bets: green - 21 bets for 19,137G (71.2%, x0.40); purple - 18 bets for 7,748G (28.8%, x2.47)

green bets:
loveyouallfriends: 11,329G (59.2%, 11,329G)
KingGoldenPanda: 1,000G (5.2%, 10,423G)
ungabunga_bot: 991G (5.2%, 48,411G)
BirbBrainsBot: 739G (3.9%, 239,441G)
bad1dea: 700G (3.7%, 52,750G)
InOzWeTrust: 680G (3.6%, 680G)
Laserman1000: 530G (2.8%, 10,230G)
getthemoneyz: 518G (2.7%, 440,507G)
HaychDub: 350G (1.8%, 4,980G)
upvla: 322G (1.7%, 42,705G)
madming25: 294G (1.5%, 294G)
truestrike: 289G (1.5%, 289G)
ZephyrTempest: 241G (1.3%, 22,746G)
kingdelas2: 214G (1.1%, 1,614G)
b0shii: 200G (1.0%, 2,211G)
maichenambrai: 200G (1.0%, 7,658G)
FoxtrotNovemberCharlie: 140G (0.7%, 140G)
AllInBot: 100G (0.5%, 100G)
Jakazu: 100G (0.5%, 100G)
Langrisser: 100G (0.5%, 334G)
maakur_: 100G (0.5%, 4,142G)

purple bets:
sinnyil2: 1,500G (19.4%, 56,979G)
OneHundredFists: 1,000G (12.9%, 4,054G)
Shalloween: 700G (9.0%, 2,809G)
DrAntiSocial: 588G (7.6%, 588G)
reinoe: 576G (7.4%, 576G)
Oreo_Pizza: 500G (6.5%, 9,053G)
JIDkomu: 500G (6.5%, 5,479G)
Vampire_Killer: 500G (6.5%, 4,828G)
RRazza: 500G (6.5%, 3,175G)
HaateXIII: 250G (3.2%, 26,674G)
dj_ghost_phrat: 232G (3.0%, 232G)
soapydragonfly: 224G (2.9%, 224G)
ugoplatamia: 200G (2.6%, 2,318G)
Zeyl: 112G (1.4%, 112G)
zepi_zza: 100G (1.3%, 4,735G)
datadrivenbot: 100G (1.3%, 20,844G)
Tithonus: 100G (1.3%, 7,487G)
JudgeSkrapz: 66G (0.9%, 1,326G)

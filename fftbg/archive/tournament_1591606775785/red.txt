Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Female
Leo
76
68
Oracle
Throw
Parry
Equip Axe
Retreat

White Staff

Green Beret
Mythril Vest
Reflect Ring

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Blind Rage, Confusion Song, Paralyze
Bomb, Knife, Staff



Wollip
Male
Capricorn
49
68
Wizard
Talk Skill
Counter Tackle
Equip Polearm
Move+2

Battle Bamboo

Golden Hairpin
Chameleon Robe
Defense Armlet

Fire, Bolt 3, Bolt 4, Flare
Invitation, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



HaychDub
Female
Virgo
61
57
Chemist
White Magic
Dragon Spirit
Maintenance
Waterbreathing

Air Knife

Black Hood
Earth Clothes
Sprint Shoes

Potion, X-Potion, Hi-Ether, Eye Drop, Soft, Remedy, Phoenix Down
Cure, Raise, Protect 2, Shell 2, Esuna



DAC169
Male
Aquarius
69
51
Summoner
Yin Yang Magic
Damage Split
Attack UP
Move+2

Poison Rod

Headgear
White Robe
Magic Gauntlet

Moogle, Shiva, Carbunkle, Cyclops
Spell Absorb, Life Drain, Doubt Faith, Zombie, Silence Song, Dispel Magic

Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Male
Taurus
38
65
Mime

Brave Save
Equip Shield
Ignore Terrain


Escutcheon
Cachusha
Clothes
Leather Mantle

Mimic




Laserman1000
Male
Aries
41
73
Oracle
Jump
Damage Split
Dual Wield
Swim

Cypress Rod
Octagon Rod
Headgear
Adaman Vest
Wizard Mantle

Blind, Poison, Spell Absorb, Life Drain, Zombie, Sleep
Level Jump5, Vertical Jump7



StealthModeLocke
Monster
Gemini
68
77
Dark Behemoth










Elelor
Female
Aquarius
46
74
Thief
Charge
PA Save
Defend
Move+3

Orichalcum

Barette
Chain Vest
Defense Ring

Gil Taking
Charge+2, Charge+4, Charge+5, Charge+7, Charge+20

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



OneHundredFists
Male
Gemini
75
78
Mime

Counter Tackle
Defend
Fly



Flash Hat
Silk Robe
Magic Gauntlet

Mimic




Elimit404
Male
Pisces
55
72
Calculator
Yin Yang Magic
Meatbone Slash
Attack UP
Waterwalking

Papyrus Codex

Leather Hat
White Robe
Battle Boots

CT, Height, 5, 3
Poison, Spell Absorb, Blind Rage, Confusion Song, Sleep, Petrify, Dark Holy



Grandlanzer
Male
Aquarius
61
81
Monk
Battle Skill
Counter Magic
Attack UP
Jump+1

Hydra Bag

Twist Headband
Leather Outfit
Leather Mantle

Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Head Break, Weapon Break, Magic Break, Justice Sword, Night Sword



Galkife
Male
Sagittarius
49
61
Time Mage
Charge
Dragon Spirit
Sicken
Levitate

White Staff

Twist Headband
White Robe
Magic Gauntlet

Haste, Haste 2, Slow, Immobilize
Charge+2, Charge+4, Charge+20

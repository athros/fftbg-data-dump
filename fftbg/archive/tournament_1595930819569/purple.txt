Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ALY327
Female
Libra
46
69
Dancer
Punch Art
Counter Tackle
Dual Wield
Fly

Persia
Ryozan Silk
Ribbon
Brigandine
Magic Gauntlet

Disillusion, Last Dance
Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive



Lanshaft
Monster
Aquarius
65
81
Steel Giant










Douchetron
Female
Serpentarius
73
67
Geomancer
Punch Art
Parry
Halve MP
Ignore Terrain

Kiyomori
Hero Shield
Feather Hat
Mystic Vest
Magic Ring

Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Pummel, Wave Fist, Chakra, Revive



Sergei
Male
Gemini
47
46
Wizard
Punch Art
Critical Quick
Equip Sword
Move+3

Defender

Ribbon
Chameleon Robe
108 Gems

Fire 3, Ice 3, Flare
Purification, Revive

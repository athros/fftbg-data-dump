Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Stoww
Female
Aquarius
58
76
Geomancer
Item
Regenerator
Throw Item
Levitate

Heaven's Cloud
Diamond Shield
Golden Hairpin
Judo Outfit
Small Mantle

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Remedy, Phoenix Down



Off The Crossbar
Male
Cancer
67
78
Ninja
Yin Yang Magic
Speed Save
Defend
Move+3

Morning Star
Main Gauche
Twist Headband
Clothes
Spike Shoes

Shuriken, Bomb
Poison, Life Drain, Pray Faith, Silence Song



Gabbagooluigi
Female
Aquarius
53
50
Monk
Draw Out
Speed Save
Magic Attack UP
Move+2



Feather Hat
Power Sleeve
Reflect Ring

Pummel, Earth Slash, Secret Fist, Purification, Revive
Koutetsu, Bizen Boat, Kikuichimoji



Anachronity
Female
Leo
44
38
Summoner
Talk Skill
Speed Save
Short Charge
Move-MP Up

White Staff

Black Hood
Wizard Outfit
Magic Gauntlet

Shiva, Titan, Golem, Bahamut, Fairy, Lich
Invitation, Threaten, Insult, Negotiate, Mimic Daravon

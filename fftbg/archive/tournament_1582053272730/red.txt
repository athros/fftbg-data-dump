Player: !Red
Team: Red Team
Palettes: Red/Brown



UndesiredEffect
Female
Virgo
68
45
Time Mage
Summon Magic
Counter Magic
Magic Attack UP
Waterbreathing

Papyrus Codex

Feather Hat
Chameleon Robe
Setiemson

Haste, Slow, Slow 2, Stop, Demi, Stabilize Time
Shiva, Golem, Carbunkle, Salamander



1twistedpuppy
Female
Aries
80
57
Ninja
Basic Skill
Mana Shield
Secret Hunt
Retreat

Short Edge
Hidden Knife
Green Beret
Black Costume
Magic Ring

Shuriken, Stick
Throw Stone, Tickle, Wish, Scream



GUY KAZAMA
Male
Capricorn
67
43
Mediator
Sing
Earplug
Maintenance
Move+1

Bestiary

Black Hood
Earth Clothes
Cursed Ring

Invitation, Threaten, Refute, Rehabilitate
Angel Song, Sky Demon, Hydra Pit



ThePineappleSalesman
Male
Sagittarius
54
73
Chemist
Summon Magic
Faith Up
Attack UP
Jump+2

Mythril Knife

Barette
Judo Outfit
108 Gems

X-Potion, Hi-Ether, Antidote, Phoenix Down
Moogle, Ramuh, Titan, Carbunkle, Leviathan, Fairy

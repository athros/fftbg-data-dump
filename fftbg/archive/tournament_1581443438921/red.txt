Player: !Red
Team: Red Team
Palettes: Red/Brown



Toka222
Male
Cancer
56
56
Ninja
White Magic
Sunken State
Defense UP
Move+2

Flail
Spell Edge
Flash Hat
Mystic Vest
Magic Gauntlet

Bomb, Knife
Cure 4, Raise 2, Shell, Esuna



BenYuPoker
Male
Libra
46
71
Thief
Black Magic
Earplug
Halve MP
Waterbreathing

Main Gauche

Cachusha
Earth Clothes
Reflect Ring

Steal Heart, Steal Helmet, Leg Aim
Fire, Bolt 3, Ice 2, Frog



Jamesdhanks
Male
Leo
49
68
Mediator
Summon Magic
HP Restore
Sicken
Move+1

Bestiary

Headgear
Black Robe
Cherche

Invitation, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute
Golem, Carbunkle, Odin, Silf, Fairy, Cyclops



Kellios11
Male
Cancer
81
74
Monk
Steal
Sunken State
Dual Wield
Levitate



Holy Miter
Secret Clothes
Small Mantle

Wave Fist, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Steal Heart, Steal Weapon, Leg Aim

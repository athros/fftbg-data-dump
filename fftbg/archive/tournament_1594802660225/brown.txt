Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lowlf
Female
Libra
46
67
Monk
Time Magic
Absorb Used MP
Dual Wield
Teleport



Golden Hairpin
Adaman Vest
Sprint Shoes

Spin Fist, Earth Slash, Revive
Slow 2, Immobilize, Quick, Demi 2, Meteor



Superdevon1
Male
Gemini
68
40
Chemist
Sing
Damage Split
Equip Gun
Move+1

Bestiary

Thief Hat
Mythril Vest
Vanish Mantle

Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Soft, Phoenix Down
Angel Song, Life Song, Cheer Song, Battle Song, Magic Song, Diamond Blade



TeaTime29
Male
Capricorn
71
42
Time Mage
Yin Yang Magic
Counter Flood
Equip Sword
Jump+1

Bizen Boat

Holy Miter
Mystic Vest
Sprint Shoes

Haste, Slow, Reflect, Quick, Demi 2
Dispel Magic, Petrify



Avraham
Male
Gemini
50
55
Thief
Summon Magic
PA Save
Equip Polearm
Jump+2

Javelin

Triangle Hat
Leather Outfit
Cursed Ring

Leg Aim
Moogle, Titan, Carbunkle

Final Bets: white - 11 bets for 6,820G (37.7%, x1.66); black - 6 bets for 11,291G (62.3%, x0.60)

white bets:
NovaKnight21: 2,000G (29.3%, 10,686G)
Evewho: 1,055G (15.5%, 1,055G)
BirbBrainsBot: 1,000G (14.7%, 152,529G)
superdevon1: 577G (8.5%, 11,555G)
dem0nj0ns: 500G (7.3%, 6,141G)
dantayystv: 500G (7.3%, 5,291G)
getthemoneyz: 408G (6.0%, 1,281,706G)
datadrivenbot: 373G (5.5%, 37,239G)
cocomilko: 300G (4.4%, 2,907G)
randgridr: 100G (1.5%, 1,230G)
ZZ_Yoshi: 7G (0.1%, 5,960G)

black bets:
Chuckolator: 7,777G (68.9%, 99,751G)
Arcblazer23: 1,274G (11.3%, 1,274G)
Lythe_Caraker: 1,000G (8.9%, 145,741G)
AllInBot: 705G (6.2%, 705G)
prince_rogers_nelson_: 412G (3.6%, 412G)
Lydian_C: 123G (1.1%, 73,588G)

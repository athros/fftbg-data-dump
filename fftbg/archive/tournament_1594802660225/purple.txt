Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Choco Joe
Male
Aries
61
39
Lancer
Battle Skill
Caution
Dual Wield
Teleport

Obelisk
Spear
Mythril Helmet
Reflect Mail
Battle Boots

Level Jump3, Vertical Jump7
Weapon Break, Power Break, Night Sword, Surging Sword



WitchHunterIX
Female
Aquarius
43
59
Lancer
Summon Magic
Parry
Secret Hunt
Waterbreathing

Partisan
Escutcheon
Mythril Helmet
Carabini Mail
Battle Boots

Level Jump8, Vertical Jump6
Moogle, Shiva, Ramuh, Carbunkle, Bahamut, Salamander, Silf, Fairy



Bruubarg
Male
Aries
64
71
Wizard
Jump
Regenerator
Magic Attack UP
Teleport 2

Flame Rod

Holy Miter
Silk Robe
Wizard Mantle

Fire, Fire 2, Bolt 4, Ice, Ice 2
Level Jump5, Vertical Jump8



Kirstin
Female
Pisces
44
66
Ninja
Item
Auto Potion
Equip Armor
Jump+1

Hydra Bag
Sasuke Knife
Headgear
Wizard Robe
Small Mantle

Shuriken, Sword, Staff, Ninja Sword
X-Potion, Eye Drop, Holy Water, Phoenix Down

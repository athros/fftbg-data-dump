Final Bets: purple - 9 bets for 20,217G (57.3%, x0.75); brown - 9 bets for 15,090G (42.7%, x1.34)

purple bets:
Phytik: 11,886G (58.8%, 11,886G)
WireLord: 2,237G (11.1%, 4,474G)
leakimiko: 2,206G (10.9%, 27,578G)
ko2q: 1,311G (6.5%, 1,311G)
BirbBrainsBot: 1,000G (4.9%, 154,646G)
HaplessOne: 777G (3.8%, 9,557G)
lijarkh: 500G (2.5%, 18,554G)
twelfthrootoftwo: 200G (1.0%, 4,569G)
datadrivenbot: 100G (0.5%, 11,373G)

brown bets:
TheRedMelody: 11,886G (78.8%, 28,389G)
ungabunga_bot: 1,000G (6.6%, 291,609G)
Odin007_88: 750G (5.0%, 4,840G)
ADeatonic: 500G (3.3%, 500G)
ewan_e: 240G (1.6%, 240G)
Bioticism: 212G (1.4%, 212G)
midori_ribbon: 202G (1.3%, 954G)
getthemoneyz: 200G (1.3%, 611,265G)
Lythe_Caraker: 100G (0.7%, 119,740G)

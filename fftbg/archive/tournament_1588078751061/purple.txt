Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ZephyrTempest
Female
Capricorn
80
53
Samurai
Dance
Mana Shield
Equip Sword
Move+1

Muramasa

Barbuta
Plate Mail
Cursed Ring

Koutetsu, Heaven's Cloud
Slow Dance, Last Dance



FoeSquirrel
Female
Serpentarius
77
60
Mime

Counter Flood
Defense UP
Jump+3



Flash Hat
Brigandine
Rubber Shoes

Mimic




Skuldar
Female
Pisces
77
46
Samurai
Jump
Brave Up
Attack UP
Move+1

Murasame

Barbuta
Linen Cuirass
Spike Shoes

Asura, Koutetsu, Bizen Boat, Muramasa, Kikuichimoji
Level Jump2, Vertical Jump6



Nifboy
Male
Cancer
75
59
Wizard
Elemental
Arrow Guard
Equip Knife
Move+3

Main Gauche

Headgear
Judo Outfit
Defense Ring

Fire, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Empower
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

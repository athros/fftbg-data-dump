Player: !Green
Team: Green Team
Palettes: Green/White



Lodrak
Male
Aries
64
44
Knight
Basic Skill
Caution
Halve MP
Move+3

Mythril Sword
Diamond Shield
Mythril Helmet
Platinum Armor
Feather Boots

Speed Break, Mind Break, Justice Sword
Accumulate, Dash, Heal, Cheer Up, Fury



DeathTaxesAndAnime
Female
Aquarius
56
40
Priest
Elemental
Parry
Short Status
Ignore Height

Flame Whip

Holy Miter
White Robe
Cursed Ring

Cure, Cure 4, Raise, Protect, Protect 2, Shell 2, Esuna
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Holyonline
Monster
Aries
70
69
Holy Dragon










Bryan792
Male
Aquarius
42
75
Ninja
Punch Art
Mana Shield
Equip Gun
Retreat

Bestiary
Battle Folio
Thief Hat
Earth Clothes
Wizard Mantle

Shuriken, Bomb, Knife
Wave Fist, Purification, Revive

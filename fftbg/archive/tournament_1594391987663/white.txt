Player: !White
Team: White Team
Palettes: White/Blue



JethroThrul
Male
Virgo
68
46
Archer
Time Magic
Distribute
Martial Arts
Move-HP Up

Lightning Bow

Barbuta
Judo Outfit
Germinas Boots

Charge+2, Charge+7
Haste 2, Slow, Slow 2, Quick, Stabilize Time



DavenIII
Male
Serpentarius
43
55
Thief
Battle Skill
Brave Save
Equip Armor
Lava Walking

Kunai

Crystal Helmet
Judo Outfit
Spike Shoes

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Armor Break, Shield Break, Night Sword



Lyner87
Female
Aquarius
42
54
Mime

Counter Flood
Martial Arts
Lava Walking



Green Beret
Earth Clothes
Feather Mantle

Mimic




Dogsandcatsand
Female
Aquarius
72
61
Oracle
Charge
Caution
Defense UP
Move-HP Up

Bestiary

Leather Hat
Chameleon Robe
N-Kai Armlet

Spell Absorb, Life Drain, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep
Charge+1, Charge+3, Charge+7

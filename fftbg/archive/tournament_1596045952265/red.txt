Player: !Red
Team: Red Team
Palettes: Red/Brown



DavenIII
Male
Leo
47
46
Thief
Time Magic
Counter Flood
Dual Wield
Teleport

Dagger
Ancient Sword
Green Beret
Leather Outfit
Vanish Mantle

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory
Haste, Haste 2, Slow, Reflect



StealthModeLocke
Male
Virgo
44
80
Calculator
Yin Yang Magic
Abandon
Concentrate
Levitate

Bestiary

Black Hood
Adaman Vest
Genji Gauntlet

CT, 3
Poison, Life Drain, Confusion Song, Petrify



Reddwind
Male
Pisces
75
61
Knight
Yin Yang Magic
PA Save
Dual Wield
Waterwalking

Defender
Slasher
Gold Helmet
Silk Robe
Magic Ring

Shield Break, Speed Break
Spell Absorb, Silence Song, Foxbird, Petrify



Gawyel95
Female
Capricorn
79
42
Summoner
White Magic
Auto Potion
Attack UP
Jump+2

Papyrus Codex

Black Hood
Chameleon Robe
Cursed Ring

Shiva, Golem, Carbunkle, Odin, Salamander, Lich
Cure, Cure 4, Raise, Regen, Protect, Protect 2, Shell 2, Esuna

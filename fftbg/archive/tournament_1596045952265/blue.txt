Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Calajo
Female
Sagittarius
73
49
Knight
Elemental
Brave Save
Maintenance
Fly

Rune Blade
Mythril Shield
Genji Helmet
Leather Armor
Spike Shoes

Shield Break, Speed Break, Power Break, Justice Sword, Dark Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball



DustBirdEX
Female
Gemini
64
47
Mime

Distribute
Long Status
Ignore Terrain



Headgear
Brigandine
Rubber Shoes

Mimic




NIghtdew14
Female
Pisces
63
40
Dancer
Elemental
Mana Shield
Equip Sword
Retreat

Blood Sword

Golden Hairpin
Wizard Outfit
Diamond Armlet

Polka Polka, Disillusion
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Nekojin
Male
Libra
72
44
Knight
Jump
Parry
Sicken
Fly

Star Bag
Genji Shield
Iron Helmet
Chameleon Robe
Rubber Shoes

Shield Break, Speed Break, Mind Break, Surging Sword
Level Jump2, Vertical Jump7

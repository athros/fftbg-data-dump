Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Galkife
Female
Aries
60
78
Wizard
Jump
Arrow Guard
Equip Sword
Move-HP Up

Chirijiraden

Leather Hat
Mystic Vest
Power Wrist

Fire, Fire 2, Ice, Ice 2, Ice 3, Ice 4
Level Jump4, Vertical Jump2



Dogsandcatsand
Female
Libra
81
71
Wizard
Punch Art
PA Save
Equip Shield
Waterwalking

Mage Masher
Escutcheon
Headgear
White Robe
Magic Gauntlet

Fire 2, Fire 4, Bolt 3, Ice 2, Ice 3, Flare
Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive



Thyrandaal
Male
Gemini
68
45
Monk
Time Magic
Hamedo
Dual Wield
Move-HP Up



Feather Hat
Mystic Vest
Setiemson

Spin Fist, Pummel, Secret Fist, Purification, Revive
Stop, Reflect, Quick, Demi



Neerrm
Male
Scorpio
42
72
Bard
Item
HP Restore
Equip Bow
Retreat

Night Killer

Black Hood
Brigandine
Feather Boots

Battle Song, Last Song
Potion, Echo Grass, Maiden's Kiss

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Vorap
Male
Virgo
42
58
Calculator
Black Magic
Faith Save
Maintenance
Move+1

Bestiary

Holy Miter
Brigandine
Angel Ring

CT, 5, 4, 3
Bolt, Bolt 2, Bolt 3, Ice, Empower



Killing Moon
Male
Libra
59
77
Ninja
Draw Out
Distribute
Concentrate
Ignore Terrain

Spell Edge
Short Edge
Headgear
Power Sleeve
N-Kai Armlet

Shuriken, Wand
Koutetsu, Bizen Boat, Kiyomori



Ruvelia BibeI
Female
Capricorn
56
63
Dancer
Draw Out
Abandon
Dual Wield
Waterbreathing

Persia
Cashmere
Thief Hat
Judo Outfit
Sprint Shoes

Last Dance
Bizen Boat, Muramasa



Rislyeu
Male
Aries
59
46
Ninja
White Magic
Caution
Magic Defense UP
Move+3

Morning Star
Flame Whip
Red Hood
Clothes
108 Gems

Shuriken, Staff
Reraise, Protect 2, Esuna

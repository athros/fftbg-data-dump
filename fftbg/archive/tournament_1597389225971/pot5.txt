Final Bets: blue - 11 bets for 4,575G (55.6%, x0.80); yellow - 7 bets for 3,653G (44.4%, x1.25)

blue bets:
itsonlyspencer: 1,146G (25.0%, 1,146G)
BirbBrainsBot: 1,000G (21.9%, 90,116G)
SkylerBunny: 849G (18.6%, 849G)
Mesmaster: 424G (9.3%, 424G)
BoneMiser: 300G (6.6%, 300G)
getthemoneyz: 244G (5.3%, 1,622,739G)
datadrivenbot: 200G (4.4%, 66,993G)
uncharreted: 112G (2.4%, 112G)
ko2q: 100G (2.2%, 2,963G)
koeeh: 100G (2.2%, 569G)
Wooplestein: 100G (2.2%, 1,207G)

yellow bets:
fattunaking: 2,000G (54.7%, 5,699G)
AllInBot: 929G (25.4%, 929G)
ArrenJevleth: 300G (8.2%, 5,345G)
Lydian_C: 124G (3.4%, 3,949G)
MinBetBot: 100G (2.7%, 8,016G)
ProteinBiscuit: 100G (2.7%, 2,657G)
windsah: 100G (2.7%, 4,843G)

Final Bets: blue - 13 bets for 7,267G (67.5%, x0.48); brown - 8 bets for 3,502G (32.5%, x2.08)

blue bets:
SkylerBunny: 3,483G (47.9%, 3,483G)
blorpy_: 893G (12.3%, 22,346G)
Lydian_C: 650G (8.9%, 2,625G)
BoneMiser: 540G (7.4%, 540G)
AllInBot: 456G (6.3%, 456G)
Mesmaster: 424G (5.8%, 424G)
datadrivenbot: 200G (2.8%, 67,409G)
PantherIscariot: 111G (1.5%, 7,909G)
koeeh: 110G (1.5%, 777G)
MinBetBot: 100G (1.4%, 8,044G)
ko2q: 100G (1.4%, 3,171G)
windsah: 100G (1.4%, 4,871G)
Chuckolator: 100G (1.4%, 3,318G)

brown bets:
NovaKnight21: 1,000G (28.6%, 5,532G)
getthemoneyz: 672G (19.2%, 1,622,920G)
Wooplestein: 655G (18.7%, 655G)
BirbBrainsBot: 373G (10.7%, 89,914G)
ArrenJevleth: 300G (8.6%, 4,745G)
uncharreted: 201G (5.7%, 201G)
Jaosen_: 200G (5.7%, 728G)
gorgewall: 101G (2.9%, 3,579G)

Final Bets: blue - 10 bets for 3,197G (34.0%, x1.94); champion - 8 bets for 6,216G (66.0%, x0.51)

blue bets:
BirbBrainsBot: 1,000G (31.3%, 89,541G)
AllInBot: 676G (21.1%, 676G)
Lydian_C: 650G (20.3%, 2,938G)
koeeh: 300G (9.4%, 830G)
getthemoneyz: 144G (4.5%, 1,622,248G)
uncharreted: 112G (3.5%, 112G)
HASTERIOUS: 110G (3.4%, 2,204G)
ko2q: 100G (3.1%, 3,219G)
windsah: 100G (3.1%, 4,919G)
Chuckolator: 5G (0.2%, 3,366G)

champion bets:
soren_of_tyto: 2,000G (32.2%, 5,712G)
ArrenJevleth: 1,333G (21.4%, 4,445G)
SkylerBunny: 1,161G (18.7%, 1,161G)
blorpy_: 911G (14.7%, 22,776G)
Wooplestein: 260G (4.2%, 260G)
coralreeferz: 250G (4.0%, 1,722G)
datadrivenbot: 200G (3.2%, 67,505G)
gorgewall: 101G (1.6%, 3,478G)

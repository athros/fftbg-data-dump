Player: !White
Team: White Team
Palettes: White/Blue



SkylerBunny
Female
Sagittarius
79
40
Time Mage
Basic Skill
Parry
Magic Attack UP
Jump+3

Healing Staff

Cachusha
Chameleon Robe
Sprint Shoes

Slow, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Stabilize Time
Accumulate, Throw Stone, Heal, Wish



Sairentozon7
Female
Aries
56
48
Samurai
Jump
PA Save
Halve MP
Jump+3

Heaven's Cloud

Bronze Helmet
Gold Armor
Salty Rage

Koutetsu, Murasame, Muramasa
Level Jump8, Vertical Jump3



Sinnyil2
Female
Aries
63
44
Summoner
Draw Out
Speed Save
Defense UP
Ignore Terrain

Ice Rod

Flash Hat
Wizard Robe
Angel Ring

Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Fairy
Kiyomori, Muramasa



Blorpy
Male
Aquarius
48
40
Mime

Distribute
Dual Wield
Jump+3



Triangle Hat
Mystic Vest
Bracer

Mimic


Player: !White
Team: White Team
Palettes: White/Blue



Laserman1000
Male
Gemini
41
62
Lancer
Punch Art
Counter
Doublehand
Waterwalking

Spear

Gold Helmet
Mythril Armor
Red Shoes

Level Jump8, Vertical Jump7
Spin Fist, Secret Fist, Purification, Chakra, Revive



Kyune
Female
Pisces
68
80
Calculator
Catfish Skill
Dragon Spirit
Equip Shield
Ignore Height

Cypress Rod
Bronze Shield
Black Hood
Plate Mail
Setiemson

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



Lali Lulelo
Female
Gemini
70
78
Monk
Yin Yang Magic
Counter
Attack UP
Jump+2



Leather Hat
Adaman Vest
Reflect Ring

Spin Fist, Pummel, Secret Fist, Purification, Revive, Seal Evil
Spell Absorb, Silence Song, Blind Rage, Dispel Magic, Sleep



Lythe Caraker
Female
Leo
54
59
Priest
Basic Skill
PA Save
Sicken
Waterwalking

Flame Whip

Green Beret
Chameleon Robe
108 Gems

Cure 2, Cure 3, Raise, Regen, Protect, Esuna
Heal, Yell, Cheer Up

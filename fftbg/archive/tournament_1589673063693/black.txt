Player: !Black
Team: Black Team
Palettes: Black/Red



B0shii
Male
Taurus
45
44
Lancer
White Magic
HP Restore
Doublehand
Waterbreathing

Mythril Spear

Circlet
Bronze Armor
Cursed Ring

Level Jump5, Vertical Jump8
Cure 2, Raise, Regen, Wall, Esuna



Zzaanexx
Male
Scorpio
56
76
Knight
Item
Faith Save
Throw Item
Ignore Height

Platinum Sword
Aegis Shield
Iron Helmet
Light Robe
Elf Mantle

Head Break, Armor Break, Magic Break, Power Break, Mind Break, Justice Sword
X-Potion, Eye Drop, Phoenix Down



Theseawolf1
Male
Scorpio
80
52
Ninja
Basic Skill
Sunken State
Equip Armor
Jump+1

Ninja Edge
Ninja Edge
Bronze Helmet
Carabini Mail
Magic Ring

Bomb
Accumulate, Heal, Cheer Up, Scream



Red Lancer
Female
Cancer
73
66
Monk
Black Magic
Counter
Concentrate
Move+3



Feather Hat
Secret Clothes
Battle Boots

Earth Slash, Secret Fist, Purification
Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Ice 3, Ice 4, Empower, Frog, Flare

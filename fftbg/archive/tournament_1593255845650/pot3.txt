Final Bets: white - 6 bets for 2,143G (40.1%, x1.50); black - 7 bets for 3,206G (59.9%, x0.67)

white bets:
NovaKnight21: 1,000G (46.7%, 10,314G)
RaIshtar: 500G (23.3%, 7,366G)
Heyndog: 242G (11.3%, 242G)
gorgewall: 201G (9.4%, 26,569G)
peeronid: 100G (4.7%, 2,091G)
datadrivenbot: 100G (4.7%, 47,072G)

black bets:
BirbBrainsBot: 1,000G (31.2%, 60,434G)
Evewho: 666G (20.8%, 666G)
prince_rogers_nelson_: 600G (18.7%, 600G)
vorap: 400G (12.5%, 155,996G)
twelfthrootoftwo: 300G (9.4%, 7,093G)
getthemoneyz: 140G (4.4%, 1,075,105G)
CT_5_Holy: 100G (3.1%, 1,587G)

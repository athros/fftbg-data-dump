Player: !Red
Team: Red Team
Palettes: Red/Brown



ALY327
Male
Virgo
51
61
Geomancer
Item
Critical Quick
Throw Item
Levitate

Long Sword
Ice Shield
Leather Hat
Mystic Vest
Chantage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Hi-Potion, Soft, Holy Water, Phoenix Down



VolgraTheMoose
Male
Pisces
57
43
Ninja
Elemental
Parry
Equip Sword
Fly

Short Edge
Muramasa
Cachusha
Mythril Vest
Spike Shoes

Shuriken
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Serperemagus
Male
Leo
44
63
Lancer
Item
Sunken State
Throw Item
Move+2

Javelin
Crystal Shield
Genji Helmet
Mythril Armor
Diamond Armlet

Level Jump8, Vertical Jump4
Potion, Ether, Hi-Ether, Maiden's Kiss, Phoenix Down



Mesmaster
Male
Leo
68
66
Knight
Jump
Auto Potion
Equip Armor
Jump+2

Defender
Aegis Shield
Green Beret
Black Costume
Spike Shoes

Shield Break, Magic Break, Justice Sword
Level Jump5, Vertical Jump3

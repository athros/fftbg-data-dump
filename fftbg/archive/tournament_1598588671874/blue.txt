Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RagequitSA
Male
Libra
68
75
Ninja
Basic Skill
Hamedo
Secret Hunt
Teleport

Short Edge
Short Edge
Thief Hat
Earth Clothes
Feather Boots

Shuriken, Bomb
Accumulate, Throw Stone, Yell



Solomongrundy85
Female
Gemini
73
66
Geomancer
White Magic
Sunken State
Equip Polearm
Fly

Partisan
Crystal Shield
Golden Hairpin
Earth Clothes
Genji Gauntlet

Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Cure, Cure 3, Raise, Raise 2, Shell, Shell 2, Wall, Esuna



Nhammen
Female
Scorpio
40
63
Dancer
Basic Skill
Auto Potion
Dual Wield
Waterwalking

Cashmere
Persia
Flash Hat
Wizard Robe
Dracula Mantle

Nameless Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon
Dash, Throw Stone, Yell, Cheer Up, Fury



DeathTaxesAndAnime
Female
Aries
50
74
Mediator
Yin Yang Magic
Sunken State
Dual Wield
Waterbreathing

Madlemgen
Papyrus Codex
Cachusha
Mythril Vest
Bracer

Praise, Preach, Solution, Refute, Rehabilitate
Silence Song, Blind Rage, Confusion Song, Dispel Magic

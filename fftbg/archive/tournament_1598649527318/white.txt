Player: !White
Team: White Team
Palettes: White/Blue



DeathTaxesAndAnime
Female
Gemini
72
67
Mediator
Yin Yang Magic
PA Save
Dual Wield
Move+3

Mythril Gun
Mythril Gun
Black Hood
Light Robe
108 Gems

Persuade, Threaten, Preach, Insult, Refute
Pray Faith, Confusion Song, Dispel Magic, Paralyze



Ruleof5
Monster
Taurus
50
70
Steel Giant










ArchKnightX
Male
Scorpio
79
51
Summoner
Black Magic
Meatbone Slash
Long Status
Ignore Terrain

Madlemgen

Black Hood
Black Robe
Magic Ring

Moogle, Golem, Carbunkle, Odin, Leviathan, Fairy, Cyclops
Bolt 3, Ice, Ice 3, Flare



Reddwind
Male
Cancer
61
52
Knight
Draw Out
Hamedo
Magic Defense UP
Move+2

Coral Sword
Platinum Shield
Crystal Helmet
Carabini Mail
Feather Mantle

Head Break, Weapon Break, Power Break, Mind Break, Justice Sword, Surging Sword
Koutetsu, Bizen Boat, Kikuichimoji

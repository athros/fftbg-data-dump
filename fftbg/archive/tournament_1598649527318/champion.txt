Player: !zChamp
Team: Champion Team
Palettes: Green/White



LDSkinny
Female
Cancer
62
52
Oracle
Item
PA Save
Throw Item
Jump+2

Musk Rod

Twist Headband
Leather Outfit
Elf Mantle

Spell Absorb, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Potion, Hi-Potion, X-Potion, Antidote, Phoenix Down



Runeseeker22
Male
Capricorn
67
62
Lancer
Charge
Parry
Doublehand
Move-HP Up

Iron Fan

Mythril Helmet
Platinum Armor
Red Shoes

Level Jump5, Vertical Jump8
Charge+1, Charge+2, Charge+5, Charge+7, Charge+10



Arumz
Male
Capricorn
52
44
Geomancer
Talk Skill
Damage Split
Magic Attack UP
Move+3

Mythril Sword
Crystal Shield
Leather Hat
Chameleon Robe
Rubber Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Praise, Solution, Insult, Negotiate



DeathTaxesAndAnime
Female
Scorpio
81
53
Mediator
Item
HP Restore
Attack UP
Move-HP Up

Blast Gun

Thief Hat
Black Robe
Feather Boots

Praise, Preach, Death Sentence, Rehabilitate
Potion, Maiden's Kiss, Soft

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zetchryn
Male
Capricorn
74
75
Geomancer
Punch Art
Parry
Equip Axe
Ignore Height

Flame Whip
Gold Shield
Red Hood
Wizard Robe
Diamond Armlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Pummel, Earth Slash, Purification, Seal Evil



JoeykinsX
Male
Scorpio
72
46
Samurai
Basic Skill
HP Restore
Dual Wield
Move-HP Up

Koutetsu Knife
Kikuichimoji
Circlet
Chameleon Robe
108 Gems

Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Heal, Fury



E Ballard
Male
Cancer
78
51
Wizard
Sing
Mana Shield
Beastmaster
Teleport

Thunder Rod

Black Hood
Wizard Robe
Red Shoes

Fire, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Ice 2, Frog, Death, Flare
Angel Song, Life Song



BlackFireUK
Female
Serpentarius
71
43
Samurai
Basic Skill
Parry
Maintenance
Fly

Asura Knife

Gold Helmet
Wizard Robe
Reflect Ring

Asura, Kiyomori, Muramasa
Dash, Heal

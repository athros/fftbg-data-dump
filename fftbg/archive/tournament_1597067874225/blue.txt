Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Miku Shikhu
Monster
Cancer
81
63
Steel Giant










Thyrandaal
Male
Aries
72
68
Mediator
White Magic
Arrow Guard
Dual Wield
Lava Walking

Blast Gun
Blaze Gun
Twist Headband
Chameleon Robe
Spike Shoes

Praise, Solution, Insult
Cure 2, Raise, Shell 2, Esuna, Holy, Magic Barrier



Ruleof5
Female
Gemini
39
76
Archer
Elemental
Speed Save
Secret Hunt
Waterbreathing

Panther Bag
Escutcheon
Feather Hat
Wizard Outfit
Angel Ring

Charge+1, Charge+4, Charge+5, Charge+10
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind



Forkmore
Male
Serpentarius
62
71
Bard
Talk Skill
HP Restore
Monster Talk
Ignore Height

Fairy Harp

Feather Hat
Brigandine
Leather Mantle

Life Song, Diamond Blade, Hydra Pit
Persuade, Threaten, Refute, Rehabilitate

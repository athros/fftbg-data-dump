Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Blastinus
Female
Libra
48
75
Ninja
Talk Skill
Catch
Martial Arts
Move+1

Sasuke Knife
Flame Whip
Holy Miter
Leather Outfit
Bracer

Shuriken, Knife, Dictionary
Insult, Negotiate



RunicMagus
Male
Taurus
63
77
Mime

Hamedo
Equip Armor
Teleport


Genji Shield
Barbuta
Chain Mail
N-Kai Armlet

Mimic




SeniorBunk
Male
Virgo
47
79
Chemist
Talk Skill
PA Save
Monster Talk
Move-MP Up

Panther Bag

Golden Hairpin
Wizard Outfit
Jade Armlet

Hi-Potion, Ether, Hi-Ether, Soft, Remedy, Phoenix Down
Preach, Death Sentence, Refute



Ko2q
Male
Sagittarius
61
54
Monk
Throw
Mana Shield
Attack UP
Move-HP Up



Headgear
Black Costume
108 Gems

Wave Fist, Earth Slash, Secret Fist
Shuriken

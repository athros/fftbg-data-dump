Player: !Green
Team: Green Team
Palettes: Green/White



Gabbagooluigi
Female
Sagittarius
40
63
Priest
Steal
Auto Potion
Equip Armor
Ignore Height

Gold Staff

Iron Helmet
Crystal Mail
N-Kai Armlet

Cure 3, Raise, Raise 2, Regen, Wall, Esuna
Steal Shield, Steal Accessory



OrganicArtist
Monster
Aries
72
79
Mindflayer










Alecttox
Male
Pisces
43
78
Knight
Yin Yang Magic
Sunken State
Halve MP
Retreat

Ragnarok

Genji Helmet
Mythril Armor
Elf Mantle

Weapon Break, Power Break, Mind Break, Justice Sword, Night Sword
Spell Absorb, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Sleep



G00gly
Male
Leo
79
79
Mediator
Black Magic
Mana Shield
Equip Bow
Move-MP Up

Cross Bow

Leather Hat
White Robe
Battle Boots

Negotiate, Refute
Fire, Bolt, Bolt 4, Ice, Ice 2, Ice 3, Ice 4

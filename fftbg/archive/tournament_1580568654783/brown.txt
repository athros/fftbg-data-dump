Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Yayifications
Monster
Cancer
80
44
Vampire










Grininda
Female
Gemini
80
66
Monk
Black Magic
Sunken State
Attack UP
Fly



Triangle Hat
Mythril Vest
Angel Ring

Pummel, Earth Slash, Purification, Chakra, Seal Evil
Fire, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 3



SweetHat
Male
Sagittarius
64
48
Samurai
Charge
Meatbone Slash
Defend
Move+2

Kiyomori

Mythril Helmet
Light Robe
108 Gems

Asura, Kiyomori, Muramasa
Charge+3, Charge+7, Charge+10



Maldoree
Male
Aries
61
63
Thief
Yin Yang Magic
PA Save
Magic Defense UP
Lava Walking

Cultist Dagger

Golden Hairpin
Power Sleeve
Rubber Shoes

Steal Weapon, Steal Status
Blind, Life Drain, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify

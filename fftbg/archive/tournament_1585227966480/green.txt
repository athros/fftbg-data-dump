Player: !Green
Team: Green Team
Palettes: Green/White



TheKillerNacho
Female
Aries
53
67
Monk
Time Magic
Arrow Guard
Equip Armor
Move+2



Genji Helmet
Leather Outfit
Elf Mantle

Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Slow 2, Stop, Immobilize, Quick, Stabilize Time, Meteor



Trez
Male
Virgo
78
59
Mediator
Jump
HP Restore
Equip Polearm
Jump+3

Mythril Spear

Triangle Hat
Chain Vest
Defense Armlet

Invitation, Persuade, Death Sentence, Negotiate
Level Jump8, Vertical Jump8



Daveb
Male
Cancer
68
59
Ninja
Charge
HP Restore
Equip Gun
Waterbreathing

Ramia Harp
Ramia Harp
Black Hood
Power Sleeve
Germinas Boots

Spear, Wand
Charge+4, Charge+5, Charge+7



Megaman2202
Female
Virgo
62
61
Oracle
Basic Skill
Distribute
Short Status
Retreat

Gokuu Rod

Green Beret
Linen Robe
Genji Gauntlet

Poison, Life Drain, Pray Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Accumulate, Heal, Tickle, Yell, Fury

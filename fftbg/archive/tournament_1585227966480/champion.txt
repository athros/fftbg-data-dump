Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ZephyrTempest
Female
Capricorn
60
67
Archer
Draw Out
MA Save
Long Status
Move+3

Hunting Bow
Buckler
Leather Hat
Judo Outfit
Defense Ring

Charge+5, Charge+7, Charge+20
Asura, Kiyomori, Masamune, Chirijiraden



J2DaBibbles
Female
Leo
46
67
Samurai
Black Magic
Counter Flood
Magic Attack UP
Move+3

Bizen Boat

Cross Helmet
Platinum Armor
Sprint Shoes

Asura, Kiyomori, Muramasa, Kikuichimoji
Ice, Ice 3, Death



Jordache7K
Female
Aquarius
71
47
Mediator
Time Magic
Abandon
Maintenance
Waterwalking

Glacier Gun

Flash Hat
Brigandine
Defense Armlet

Death Sentence, Insult, Negotiate, Rehabilitate
Haste 2, Slow, Reflect, Stabilize Time



Gum44
Monster
Sagittarius
59
47
Bull Demon








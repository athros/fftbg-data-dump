Player: !Black
Team: Black Team
Palettes: Black/Red



Aestheta
Monster
Scorpio
46
46
Ochu










Lawnboxer
Male
Capricorn
58
69
Priest
Black Magic
Caution
Sicken
Move+2

White Staff

Headgear
Silk Robe
Defense Ring

Cure, Raise, Shell, Shell 2, Esuna
Bolt 2, Ice 2



Sinnyil2
Female
Capricorn
61
80
Summoner
Charge
Auto Potion
Short Charge
Waterwalking

Wizard Staff

Holy Miter
Clothes
Battle Boots

Moogle, Ifrit, Titan, Bahamut, Leviathan, Salamander, Silf, Fairy, Lich
Charge+1, Charge+2



OneHundredFists
Female
Capricorn
74
80
Summoner
Punch Art
Speed Save
Short Charge
Waterbreathing

Wizard Staff

Twist Headband
Black Costume
Reflect Ring

Moogle, Shiva, Ramuh, Ifrit, Golem, Leviathan, Lich
Earth Slash, Secret Fist, Purification, Chakra

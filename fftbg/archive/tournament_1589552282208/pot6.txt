Final Bets: black - 13 bets for 16,939G (68.1%, x0.47); purple - 14 bets for 7,936G (31.9%, x2.13)

black bets:
toka222: 7,500G (44.3%, 41,665G)
HaplessOne: 2,222G (13.1%, 68,722G)
DLJuggernaut: 1,811G (10.7%, 1,811G)
DustBirdEX: 1,579G (9.3%, 1,579G)
BirbBrainsBot: 1,000G (5.9%, 76,257G)
rocl: 596G (3.5%, 596G)
ShintaroNayaka: 590G (3.5%, 590G)
reinoe: 574G (3.4%, 574G)
FriendSquirrel: 367G (2.2%, 367G)
holyonline: 300G (1.8%, 4,884G)
brokenknight201: 200G (1.2%, 2,207G)
E_Ballard: 100G (0.6%, 8,679G)
lifeguard_dan: 100G (0.6%, 2,580G)

purple bets:
Shakarak: 2,000G (25.2%, 65,018G)
JumbocactuarX27: 1,166G (14.7%, 1,166G)
DavenIII: 770G (9.7%, 10,845G)
getthemoneyz: 764G (9.6%, 671,885G)
ChrisWado: 600G (7.6%, 2,802G)
shs_: 500G (6.3%, 10,714G)
GnielKnows: 476G (6.0%, 952G)
ungabunga_bot: 423G (5.3%, 494,940G)
Baron_von_Scrub: 353G (4.4%, 1,983G)
rottings0ul: 200G (2.5%, 2,025G)
otakutaylor: 200G (2.5%, 2,494G)
Aestheta: 200G (2.5%, 3,374G)
maleblackfiora: 184G (2.3%, 184G)
datadrivenbot: 100G (1.3%, 19,510G)

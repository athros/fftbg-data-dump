Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Achloryn
Monster
Leo
77
51
Apanda










Tikotikotiko
Female
Libra
52
77
Chemist
Yin Yang Magic
Counter Magic
Beastmaster
Retreat

Romanda Gun

Cachusha
Wizard Outfit
108 Gems

Potion, Hi-Potion, X-Potion, Echo Grass, Soft, Holy Water, Remedy, Phoenix Down
Blind, Spell Absorb, Pray Faith, Zombie, Confusion Song, Dispel Magic



Wolvaroo
Female
Sagittarius
55
55
Dancer
Summon Magic
Arrow Guard
Long Status
Retreat

Cashmere

Green Beret
Clothes
108 Gems

Witch Hunt, Nether Demon
Ifrit, Bahamut, Leviathan, Salamander, Silf



XyzzySqrl
Female
Aquarius
79
69
Wizard
White Magic
PA Save
Concentrate
Lava Walking

Poison Rod

Triangle Hat
Chameleon Robe
Chantage

Fire, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Flare
Cure 3, Protect

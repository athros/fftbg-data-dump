Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DeathTaxesAndAnime
Female
Cancer
73
48
Mime

Hamedo
Dual Wield
Levitate



Twist Headband
Mythril Vest
Germinas Boots

Mimic




ColetteMSLP
Male
Sagittarius
50
44
Thief
Jump
Sunken State
Secret Hunt
Jump+2

Blood Sword

Golden Hairpin
Power Sleeve
Angel Ring

Steal Heart, Steal Shield, Steal Weapon, Steal Accessory
Level Jump4, Vertical Jump6



Resjudicata3
Female
Cancer
67
54
Chemist
Time Magic
Distribute
Dual Wield
Teleport

Cultist Dagger
Mythril Knife
Golden Hairpin
Adaman Vest
Power Wrist

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Stop, Immobilize, Float, Quick, Stabilize Time



Wooplestein
Female
Sagittarius
52
49
Time Mage
Draw Out
Dragon Spirit
Equip Knife
Move+3

Flame Rod

Black Hood
Wizard Outfit
Reflect Ring

Immobilize, Demi 2
Koutetsu, Heaven's Cloud

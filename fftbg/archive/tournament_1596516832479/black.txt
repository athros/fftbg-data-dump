Player: !Black
Team: Black Team
Palettes: Black/Red



Hasterious
Male
Leo
43
69
Lancer
Time Magic
MA Save
Maintenance
Waterbreathing

Obelisk
Mythril Shield
Gold Helmet
Genji Armor
Defense Ring

Level Jump3, Vertical Jump7
Slow, Slow 2, Immobilize, Demi, Stabilize Time



Zeroroute
Female
Gemini
79
39
Dancer
Jump
Arrow Guard
Short Charge
Retreat

Cashmere

Holy Miter
Silk Robe
Wizard Mantle

Slow Dance, Polka Polka
Level Jump8, Vertical Jump7



Mesmaster
Male
Cancer
48
65
Mime

Speed Save
Dual Wield
Move+2



Thief Hat
Mystic Vest
Genji Gauntlet

Mimic




Terminusterminal
Male
Pisces
79
51
Thief
Item
Counter Magic
Dual Wield
Swim

Kunai
Iron Sword
Triangle Hat
Chain Vest
Diamond Armlet

Gil Taking, Steal Shield
Antidote, Eye Drop, Remedy, Phoenix Down

Player: !Green
Team: Green Team
Palettes: Green/White



Roqqqpsi
Female
Scorpio
52
80
Thief
Black Magic
Faith Save
Equip Bow
Jump+1

Night Killer

Twist Headband
Judo Outfit
Salty Rage

Steal Shield, Steal Weapon, Leg Aim
Fire, Fire 4, Bolt, Ice 2, Ice 4, Death



Grimm Noir
Male
Cancer
75
51
Oracle
Summon Magic
Faith Save
Equip Gun
Lava Walking

Ramia Harp

Green Beret
Adaman Vest
Salty Rage

Spell Absorb, Pray Faith, Doubt Faith, Paralyze, Sleep
Moogle, Shiva, Salamander, Fairy, Lich



Baron Von Scrub
Female
Taurus
36
41
Calculator
White Magic
HP Restore
Halve MP
Jump+2

Battle Folio

Headgear
Mythril Vest
Rubber Shoes

CT, Prime Number, 5, 4, 3
Cure, Raise, Reraise, Regen, Protect, Protect 2



ScurvyMitch
Male
Capricorn
71
70
Lancer
White Magic
Brave Save
Equip Polearm
Move+1

Cashmere
Buckler
Cross Helmet
Robe of Lords
Germinas Boots

Level Jump3, Vertical Jump8
Cure 3, Cure 4, Raise, Reraise, Regen, Shell, Esuna

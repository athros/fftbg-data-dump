Player: !White
Team: White Team
Palettes: White/Blue



O Heyno
Male
Aries
59
68
Geomancer
White Magic
Caution
Short Status
Teleport

Slasher
Buckler
Feather Hat
Chameleon Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Cure, Raise, Reraise, Regen, Protect 2



OpHendoslice
Male
Cancer
47
42
Ninja
Yin Yang Magic
Faith Save
Equip Polearm
Waterwalking

Persia
Partisan
Feather Hat
Leather Outfit
Red Shoes

Shuriken, Bomb, Axe, Spear
Doubt Faith, Zombie, Dispel Magic, Sleep



Galkife
Male
Virgo
59
61
Monk
Jump
HP Restore
Attack UP
Swim



Feather Hat
Brigandine
Wizard Mantle

Pummel, Secret Fist, Revive
Level Jump4, Vertical Jump7



Phi Sig
Male
Sagittarius
69
61
Thief
Battle Skill
Distribute
Doublehand
Waterbreathing

Mage Masher

Holy Miter
Clothes
Elf Mantle

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory
Armor Break, Shield Break, Dark Sword

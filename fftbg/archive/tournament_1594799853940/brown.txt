Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Creggers
Female
Scorpio
59
54
Calculator
Black Magic
MA Save
Magic Defense UP
Jump+1

Papyrus Codex

Golden Hairpin
Power Sleeve
N-Kai Armlet

CT, Height, Prime Number
Fire 4, Bolt, Bolt 3, Ice 2, Ice 3, Ice 4



Blorpy
Male
Aquarius
61
47
Wizard
Charge
Counter
Dual Wield
Waterbreathing

Mythril Knife
Flame Rod
Triangle Hat
Earth Clothes
Leather Mantle

Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Empower, Flare
Charge+4



KasugaiRoastedPeas
Female
Aquarius
59
80
Monk
Item
Parry
Throw Item
Jump+2



Twist Headband
Clothes
Feather Boots

Pummel, Revive
Potion, Hi-Potion, Ether, Soft, Phoenix Down



Choco Joe
Female
Libra
54
73
Geomancer
Draw Out
Parry
Magic Attack UP
Jump+2

Giant Axe
Ice Shield
Feather Hat
Silk Robe
Setiemson

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Asura, Bizen Boat, Murasame

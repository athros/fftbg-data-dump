Player: !Green
Team: Green Team
Palettes: Green/White



Geffro1908
Female
Aquarius
59
45
Calculator
Labyrinth Skill
Regenerator
Halve MP
Jump+3

Bestiary

Cross Helmet
Black Costume
Wizard Mantle

Blue Magic
Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath, Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power



Nebukin
Female
Sagittarius
75
79
Samurai
Battle Skill
Meatbone Slash
Defend
Fly

Holy Lance

Mythril Helmet
Linen Cuirass
Genji Gauntlet

Koutetsu, Kikuichimoji
Weapon Break, Speed Break, Power Break, Stasis Sword



EnemyController
Female
Aquarius
51
41
Wizard
Elemental
Earplug
Equip Sword
Ignore Height

Platinum Sword

Triangle Hat
Mystic Vest
108 Gems

Fire 2, Bolt, Bolt 3, Bolt 4, Ice, Frog, Death
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



TeaTime29
Male
Sagittarius
48
40
Mime

Counter
Equip Armor
Teleport


Crystal Shield
Iron Helmet
Mystic Vest
Spike Shoes

Mimic


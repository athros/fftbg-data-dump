Final Bets: blue - 7 bets for 8,529G (46.3%, x1.16); purple - 12 bets for 9,899G (53.7%, x0.86)

blue bets:
bruubarg: 2,500G (29.3%, 20,293G)
amiture: 1,607G (18.8%, 1,607G)
superdevon1: 1,288G (15.1%, 12,884G)
prince_rogers_nelson_: 1,051G (12.3%, 1,051G)
dantayystv: 1,000G (11.7%, 2,599G)
datadrivenbot: 783G (9.2%, 38,172G)
dem0nj0ns: 300G (3.5%, 5,180G)

purple bets:
Chuckolator: 3,568G (36.0%, 103,568G)
Evewho: 1,512G (15.3%, 1,512G)
porkchop5158: 1,000G (10.1%, 39,181G)
WitchHunterIX: 1,000G (10.1%, 40,031G)
BirbBrainsBot: 1,000G (10.1%, 152,758G)
CT_5_Holy: 500G (5.1%, 5,675G)
cocomilko: 400G (4.0%, 4,195G)
randgridr: 328G (3.3%, 328G)
AllInBot: 200G (2.0%, 200G)
Lemonjohns: 200G (2.0%, 1,997G)
Lydian_C: 123G (1.2%, 77,609G)
getthemoneyz: 68G (0.7%, 1,280,763G)

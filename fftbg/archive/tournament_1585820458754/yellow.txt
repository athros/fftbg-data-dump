Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Dynasti
Male
Sagittarius
57
61
Thief
Charge
Abandon
Equip Gun
Fly

Battle Folio

Triangle Hat
Power Sleeve
Setiemson

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status
Charge+2, Charge+3, Charge+5, Charge+7



SeedSC
Female
Capricorn
71
70
Wizard
Draw Out
Abandon
Dual Wield
Fly

Orichalcum
Faith Rod
Golden Hairpin
Robe of Lords
Feather Boots

Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Empower, Frog, Death
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Kiyomori, Muramasa



DeathTaxesAndAnime
Female
Taurus
61
77
Priest
Yin Yang Magic
Parry
Equip Gun
Waterwalking

Glacier Gun

Triangle Hat
Wizard Robe
Cursed Ring

Cure 3, Raise, Reraise, Protect 2, Esuna
Blind, Life Drain, Zombie, Silence Song, Dispel Magic, Paralyze



Leakimiko
Female
Pisces
45
47
Samurai
Elemental
PA Save
Defend
Move+3

Asura Knife

Iron Helmet
White Robe
Chantage

Heaven's Cloud, Kiyomori
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball

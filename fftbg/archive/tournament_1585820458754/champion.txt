Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



RRazza
Female
Leo
40
63
Mediator
Steal
Mana Shield
Defense UP
Waterwalking

Glacier Gun

Thief Hat
Judo Outfit
Feather Mantle

Persuade, Threaten, Solution, Death Sentence, Refute, Rehabilitate
Steal Helmet, Steal Shield, Steal Status



Omegasuspekt
Male
Aquarius
51
75
Thief
Punch Art
Counter Magic
Magic Defense UP
Move+3

Sleep Sword

Cachusha
Earth Clothes
Leather Mantle

Steal Shield, Steal Accessory, Arm Aim
Chakra, Revive



TheOneRanger
Female
Leo
47
65
Wizard
Time Magic
Blade Grasp
Short Charge
Fly

Assassin Dagger

Holy Miter
White Robe
Magic Gauntlet

Fire, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 4, Flare
Haste 2, Slow, Slow 2, Float, Reflect, Quick, Stabilize Time



ApplesauceBoss
Female
Scorpio
61
65
Summoner
Throw
Mana Shield
Short Charge
Jump+3

Oak Staff

Black Hood
Chameleon Robe
Germinas Boots

Moogle, Ifrit, Golem, Leviathan, Salamander, Silf
Shuriken, Bomb, Knife, Wand

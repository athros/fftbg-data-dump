Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Midori Ribbon
Male
Pisces
79
76
Lancer
Basic Skill
Absorb Used MP
Defend
Waterbreathing

Dragon Whisker
Escutcheon
Mythril Helmet
Linen Cuirass
Jade Armlet

Level Jump4, Vertical Jump8
Accumulate, Heal



Fenixcrest
Male
Leo
58
49
Ninja
Steal
Distribute
Equip Gun
Move+3

Papyrus Codex
Bestiary
Thief Hat
Black Costume
Battle Boots

Spear, Wand
Steal Helmet, Steal Armor, Steal Accessory, Leg Aim



HaateXIII
Female
Pisces
43
75
Dancer
White Magic
Distribute
Short Status
Move-MP Up

Cashmere

Green Beret
Chameleon Robe
Cursed Ring

Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Last Dance, Obsidian Blade, Nether Demon
Raise 2, Protect, Shell, Wall, Magic Barrier



Jerboj
Male
Cancer
65
54
Knight
Black Magic
Counter Flood
Martial Arts
Jump+2


Mythril Shield
Gold Helmet
Linen Robe
Diamond Armlet

Magic Break, Speed Break
Fire 3, Bolt, Bolt 2, Bolt 4, Ice

Player: !White
Team: White Team
Palettes: White/Blue



HaplessOne
Female
Taurus
75
67
Oracle
Steal
Regenerator
Martial Arts
Retreat

Cypress Rod

Thief Hat
Wizard Robe
Magic Gauntlet

Poison, Life Drain, Zombie, Blind Rage, Foxbird, Dispel Magic, Dark Holy
Steal Helmet, Steal Weapon, Steal Accessory



Domeil
Female
Cancer
67
59
Mime

Auto Potion
Doublehand
Move+1



Gold Helmet
Earth Clothes
Defense Armlet

Mimic




Buddychrist10
Female
Aries
43
40
Time Mage
Draw Out
Faith Up
Equip Sword
Waterwalking

Rune Blade

Golden Hairpin
Silk Robe
108 Gems

Slow, Slow 2, Reflect, Quick, Demi 2
Koutetsu, Muramasa, Chirijiraden



Aislingeach
Male
Aries
44
61
Time Mage
Sing
Dragon Spirit
Equip Axe
Lava Walking

Flail

Green Beret
Silk Robe
Battle Boots

Haste, Haste 2, Quick
Life Song, Nameless Song

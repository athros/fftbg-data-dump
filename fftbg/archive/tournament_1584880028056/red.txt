Player: !Red
Team: Red Team
Palettes: Red/Brown



Maakur
Female
Libra
62
73
Summoner
Punch Art
Distribute
Short Charge
Ignore Height

Dragon Rod

Flash Hat
Secret Clothes
Power Wrist

Shiva, Ramuh, Bahamut, Leviathan, Fairy
Spin Fist, Pummel, Earth Slash, Secret Fist



Shs
Female
Sagittarius
45
46
Archer
Yin Yang Magic
Counter Flood
Equip Sword
Swim

Muramasa
Crystal Shield
Golden Hairpin
Judo Outfit
Leather Mantle

Charge+1, Charge+2, Charge+7
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Dispel Magic



Skarthe
Female
Cancer
48
73
Wizard
Steal
Sunken State
Attack UP
Ignore Height

Poison Rod

Black Hood
Black Robe
Power Wrist

Fire, Bolt 4, Ice, Ice 2, Ice 4
Gil Taking, Steal Shield, Steal Weapon, Arm Aim



Dantetouhou
Female
Taurus
62
58
Knight
Yin Yang Magic
Distribute
Equip Armor
Fly

Ice Brand
Venetian Shield
Green Beret
Earth Clothes
Cursed Ring

Head Break, Shield Break, Magic Break, Mind Break, Stasis Sword, Justice Sword, Explosion Sword
Poison, Life Drain, Pray Faith, Blind Rage

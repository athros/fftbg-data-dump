Final Bets: blue - 23 bets for 13,399G (65.9%, x0.52); white - 11 bets for 6,947G (34.1%, x1.93)

blue bets:
Estan_AD: 2,551G (19.0%, 5,103G)
volgrathemoose: 1,792G (13.4%, 3,584G)
sinnyil2: 1,300G (9.7%, 21,097G)
maakur_: 1,279G (9.5%, 1,279G)
EnemyController: 1,000G (7.5%, 231,615G)
E_Ballard: 600G (4.5%, 600G)
JumbocactuarX27: 540G (4.0%, 540G)
old_overholt_: 500G (3.7%, 4,272G)
CosmicTactician: 500G (3.7%, 11,735G)
gary_corson: 420G (3.1%, 5,867G)
TheFALLofLindsay: 350G (2.6%, 4,146G)
MaouDono: 344G (2.6%, 344G)
DudeMonkey77: 333G (2.5%, 10,893G)
Rislyeu: 324G (2.4%, 324G)
Mullet_Knight: 300G (2.2%, 6,889G)
Lolprinze: 287G (2.1%, 574G)
nifboy: 250G (1.9%, 2,687G)
Drusic: 216G (1.6%, 216G)
byrdturbo: 169G (1.3%, 10,390G)
ungabunga_bot: 111G (0.8%, 497,774G)
ko2q: 100G (0.7%, 1,918G)
datadrivenbot: 100G (0.7%, 16,801G)
daveb_: 33G (0.2%, 3,415G)

white bets:
metagameface: 2,000G (28.8%, 15,509G)
BirbBrainsBot: 1,000G (14.4%, 58,609G)
Mesmaster: 1,000G (14.4%, 45,255G)
AUrato: 684G (9.8%, 684G)
killth3kid: 540G (7.8%, 540G)
toka222: 500G (7.2%, 33,748G)
DavenIII: 500G (7.2%, 16,852G)
getthemoneyz: 274G (3.9%, 675,072G)
amhamor: 250G (3.6%, 767G)
Firesheath: 100G (1.4%, 27,530G)
CorpusCav: 99G (1.4%, 11,445G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



FriendSquirrel
Male
Serpentarius
56
48
Bard
Talk Skill
Counter Magic
Defend
Ignore Height

Bloody Strings

Golden Hairpin
Chain Mail
Reflect Ring

Nameless Song, Last Song, Diamond Blade, Hydra Pit
Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate



Angelomortis
Male
Virgo
80
79
Squire
Punch Art
Meatbone Slash
Beastmaster
Move+2

Sleep Sword
Aegis Shield
Triangle Hat
Plate Mail
Genji Gauntlet

Heal, Wish, Scream
Pummel, Wave Fist, Earth Slash, Purification, Revive, Seal Evil



DustBirdEX
Male
Scorpio
79
46
Calculator
White Magic
Absorb Used MP
Equip Bow
Swim

Cross Bow

Headgear
Power Sleeve
Power Wrist

CT, Height, Prime Number, 5, 3
Cure 2, Cure 3, Regen, Protect, Wall, Holy



StealthModeLocke
Male
Libra
54
59
Geomancer
Item
Critical Quick
Short Charge
Waterbreathing

Battle Axe
Aegis Shield
Golden Hairpin
Earth Clothes
Rubber Shoes

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Potion, X-Potion, Antidote, Maiden's Kiss, Holy Water

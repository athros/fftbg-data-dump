Player: !Green
Team: Green Team
Palettes: Green/White



Reinoe
Female
Pisces
66
59
Time Mage
Steal
Absorb Used MP
Equip Armor
Ignore Height

White Staff

Cross Helmet
Carabini Mail
Cursed Ring

Haste, Haste 2, Slow, Slow 2, Quick, Demi, Demi 2, Stabilize Time
Steal Heart, Steal Shield, Steal Accessory, Steal Status, Arm Aim



Pplvee1
Male
Sagittarius
59
49
Ninja
White Magic
Mana Shield
Long Status
Move+3

Flail
Morning Star
Leather Hat
Secret Clothes
Diamond Armlet

Shuriken, Sword, Hammer
Cure, Raise, Raise 2, Regen, Protect, Protect 2, Shell 2, Esuna



Miku Shikhu
Female
Sagittarius
54
73
Time Mage
Steal
Dragon Spirit
Short Status
Move-MP Up

Healing Staff

Twist Headband
Wizard Robe
Bracer

Haste, Haste 2, Stop, Reflect, Demi 2, Stabilize Time, Meteor
Steal Heart, Steal Armor, Steal Accessory, Leg Aim



Amiture
Male
Capricorn
50
67
Knight
Black Magic
Counter Tackle
Martial Arts
Jump+3

Ancient Sword
Kaiser Plate
Bronze Helmet
Linen Cuirass
Jade Armlet

Speed Break, Power Break, Mind Break
Fire 4, Bolt 4, Frog

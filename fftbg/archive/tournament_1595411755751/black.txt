Player: !Black
Team: Black Team
Palettes: Black/Red



ALY327
Female
Virgo
58
47
Priest
Item
Faith Save
Sicken
Levitate

Gold Staff

Holy Miter
Light Robe
Chantage

Cure, Raise, Raise 2, Regen, Protect 2, Esuna
Potion, Remedy, Phoenix Down



YaBoy125
Female
Gemini
67
66
Ninja
Time Magic
Counter
Equip Gun
Move+3

Papyrus Codex
Battle Folio
Feather Hat
Brigandine
N-Kai Armlet

Bomb
Slow, Slow 2, Immobilize, Demi



Amiture
Male
Taurus
72
74
Bard
Punch Art
Parry
Attack UP
Move-HP Up

Ice Bow

Golden Hairpin
Leather Outfit
Feather Mantle

Cheer Song, Diamond Blade, Hydra Pit
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive



Carole
Female
Aquarius
59
80
Time Mage
Draw Out
MA Save
Equip Shield
Levitate

Octagon Rod
Mythril Shield
Cachusha
Light Robe
Diamond Armlet

Haste 2, Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time
Heaven's Cloud

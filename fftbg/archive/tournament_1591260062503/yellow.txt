Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RaIshtar
Female
Gemini
48
59
Ninja
Time Magic
Absorb Used MP
Beastmaster
Swim

Assassin Dagger
Short Edge
Leather Hat
Chain Vest
Spike Shoes

Shuriken, Axe, Spear
Haste, Slow, Demi, Stabilize Time



Twelfthrootoftwo
Male
Capricorn
41
58
Samurai
Talk Skill
Caution
Magic Defense UP
Waterwalking

Chirijiraden

Barbuta
Silk Robe
Small Mantle

Asura, Koutetsu, Murasame, Muramasa
Persuade, Refute, Rehabilitate



Solomongrundy85
Female
Virgo
73
67
Time Mage
Draw Out
Caution
Defend
Ignore Terrain

Octagon Rod

Feather Hat
Mystic Vest
Power Wrist

Stop, Float, Reflect, Quick, Stabilize Time
Koutetsu, Kiyomori, Kikuichimoji



Red Celt
Male
Capricorn
67
72
Knight
Charge
Critical Quick
Doublehand
Levitate

Chaos Blade

Circlet
Linen Robe
Rubber Shoes

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Dark Sword, Night Sword
Charge+1, Charge+4, Charge+5, Charge+7, Charge+10

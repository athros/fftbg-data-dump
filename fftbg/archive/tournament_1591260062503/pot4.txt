Final Bets: purple - 4 bets for 3,914G (29.5%, x2.38); brown - 12 bets for 9,333G (70.5%, x0.42)

purple bets:
NovaKnight21: 3,000G (76.6%, 13,949G)
Evewho: 548G (14.0%, 548G)
KasugaiRoastedPeas: 200G (5.1%, 4,741G)
HASTERIOUS: 166G (4.2%, 3,330G)

brown bets:
reinoe: 3,902G (41.8%, 3,902G)
CorpusCav: 1,001G (10.7%, 16,030G)
BirbBrainsBot: 1,000G (10.7%, 11,928G)
getthemoneyz: 916G (9.8%, 806,319G)
Ring_Wyrm: 590G (6.3%, 590G)
ChronoG: 567G (6.1%, 567G)
thenextlantern: 500G (5.4%, 3,037G)
outer_monologue: 300G (3.2%, 2,051G)
Kalabain: 200G (2.1%, 2,189G)
DAC169: 156G (1.7%, 156G)
gorgewall: 101G (1.1%, 3,220G)
datadrivenbot: 100G (1.1%, 32,948G)

Player: !White
Team: White Team
Palettes: White/Blue



Ar Tactic
Female
Cancer
66
58
Summoner
Dance
Hamedo
Equip Axe
Move+3

Wizard Staff

Ribbon
Power Sleeve
N-Kai Armlet

Shiva, Titan, Carbunkle, Bahamut, Odin, Silf, Fairy, Lich
Slow Dance, Nameless Dance, Last Dance



Draconis345
Female
Virgo
68
65
Samurai
Charge
Counter
Equip Axe
Move+1

Giant Axe

Bronze Helmet
Bronze Armor
Feather Boots

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Charge+1



Ring Wyrm
Female
Pisces
68
76
Summoner
Punch Art
Hamedo
Martial Arts
Swim

Wizard Staff

Flash Hat
Power Sleeve
Diamond Armlet

Ifrit, Golem, Carbunkle, Salamander, Silf
Wave Fist, Earth Slash, Chakra, Seal Evil



HASTERIOUS
Male
Taurus
39
64
Archer
Throw
Parry
Equip Gun
Move+1

Bestiary
Aegis Shield
Headgear
Leather Outfit
Magic Ring

Charge+1, Charge+3, Charge+10
Ninja Sword

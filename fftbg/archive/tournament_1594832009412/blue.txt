Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Creggers
Male
Virgo
49
39
Mime

Auto Potion
Defense UP
Fly



Cachusha
Rubber Costume
Spike Shoes

Mimic




Run With Stone GUNs
Male
Cancer
74
81
Ninja
Talk Skill
Counter
Equip Bow
Teleport

Yoichi Bow

Leather Hat
Mystic Vest
Germinas Boots

Shuriken, Bomb, Wand
Invitation, Praise, Preach, Death Sentence, Refute, Rehabilitate



Phi Sig
Female
Aries
59
79
Priest
Time Magic
Absorb Used MP
Equip Gun
Teleport

Ramia Harp

Flash Hat
Earth Clothes
Wizard Mantle

Cure, Cure 2, Raise, Raise 2, Protect 2, Shell 2, Wall
Stop, Immobilize, Demi, Galaxy Stop



Fluffywormhole
Male
Sagittarius
58
66
Monk
Sing
Absorb Used MP
Maintenance
Ignore Height



Holy Miter
Judo Outfit
Magic Ring

Pummel, Purification, Revive, Seal Evil
Life Song, Magic Song, Last Song

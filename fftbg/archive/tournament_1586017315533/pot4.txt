Final Bets: purple - 22 bets for 17,621G (58.2%, x0.72); brown - 19 bets for 12,659G (41.8%, x1.39)

purple bets:
catfashions: 2,443G (13.9%, 2,443G)
Aldrammech: 2,347G (13.3%, 2,347G)
Anethum: 1,744G (9.9%, 1,744G)
Zeroroute: 1,715G (9.7%, 5,045G)
upvla: 1,219G (6.9%, 14,292G)
kai_shee: 1,001G (5.7%, 110,052G)
TheChainNerd: 1,000G (5.7%, 8,078G)
Arunic: 1,000G (5.7%, 7,918G)
MoonSlayerRS: 884G (5.0%, 884G)
sinnyil2: 712G (4.0%, 712G)
HaychDub: 600G (3.4%, 4,418G)
Lodrak: 501G (2.8%, 34,042G)
lastly: 458G (2.6%, 458G)
AllInBot: 433G (2.5%, 433G)
Athropos1: 336G (1.9%, 336G)
urieltheflameofgod: 316G (1.8%, 316G)
ig_ats: 300G (1.7%, 12,934G)
ugoplatamia: 200G (1.1%, 14,969G)
Jam643d: 108G (0.6%, 108G)
Anomandaris__: 104G (0.6%, 104G)
ko2q: 100G (0.6%, 2,509G)
datadrivenbot: 100G (0.6%, 16,349G)

brown bets:
soapydragonfly: 2,500G (19.7%, 11,667G)
SkylerBunny: 2,408G (19.0%, 4,817G)
BirbBrainsBot: 1,000G (7.9%, 210,511G)
bad1dea: 1,000G (7.9%, 28,928G)
Shalloween: 1,000G (7.9%, 104,625G)
robespyah: 738G (5.8%, 738G)
Thesamot: 652G (5.2%, 652G)
Lanshaft: 600G (4.7%, 1,893G)
kingdelas2: 500G (3.9%, 3,000G)
Laserman1000: 427G (3.4%, 427G)
kingchadking: 400G (3.2%, 400G)
Baron_von_Scrub: 314G (2.5%, 11,279G)
ZephyrTempest: 270G (2.1%, 19,060G)
KasugaiRoastedPeas: 200G (1.6%, 15,374G)
Aka_Gilly: 200G (1.6%, 6,156G)
rNdOrchestra: 200G (1.6%, 1,397G)
ungabunga_bot: 100G (0.8%, 22,840G)
SeedSC: 100G (0.8%, 139,178G)
getthemoneyz: 50G (0.4%, 434,019G)

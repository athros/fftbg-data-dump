Player: !White
Team: White Team
Palettes: White/Blue



Zeroroute
Female
Pisces
47
49
Mime

Meatbone Slash
Defense UP
Ignore Terrain



Leather Hat
Black Costume
Small Mantle

Mimic




RNdOrchestra
Male
Sagittarius
47
72
Summoner
Black Magic
Abandon
Magic Attack UP
Jump+2

Rod

Red Hood
White Robe
Leather Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle, Odin, Leviathan, Salamander, Lich
Bolt 4, Ice 3, Empower



Leakimiko
Female
Aries
59
43
Summoner
White Magic
Caution
Short Charge
Move+3

Ice Rod

Green Beret
White Robe
Jade Armlet

Carbunkle, Bahamut, Silf
Raise, Raise 2, Reraise, Regen, Protect 2, Esuna



Theatheologist
Female
Capricorn
47
56
Mediator
Yin Yang Magic
Brave Up
Dual Wield
Jump+2

Battle Folio
Battle Folio
Leather Hat
Silk Robe
Battle Boots

Invitation, Persuade, Praise, Insult, Negotiate, Refute, Rehabilitate
Pray Faith, Doubt Faith, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify

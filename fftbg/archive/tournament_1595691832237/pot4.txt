Final Bets: purple - 9 bets for 13,674G (85.1%, x0.17); brown - 7 bets for 2,391G (14.9%, x5.72)

purple bets:
VolgraTheMoose: 3,481G (25.5%, 6,962G)
NovaKnight21: 3,225G (23.6%, 3,225G)
HaateXIII: 2,688G (19.7%, 2,688G)
sinnyil2: 2,630G (19.2%, 5,260G)
nhammen: 650G (4.8%, 10,982G)
amiture: 500G (3.7%, 7,766G)
TasisSai: 200G (1.5%, 4,074G)
datadrivenbot: 200G (1.5%, 41,227G)
ribbiks: 100G (0.7%, 5,601G)

brown bets:
BoneMiser: 655G (27.4%, 655G)
sukotsuto: 500G (20.9%, 7,320G)
BirbBrainsBot: 480G (20.1%, 81,358G)
Miku_Shikhu: 332G (13.9%, 332G)
AllInBot: 200G (8.4%, 200G)
getthemoneyz: 124G (5.2%, 1,391,124G)
Cyril_Jay: 100G (4.2%, 937G)

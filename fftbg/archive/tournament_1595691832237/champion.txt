Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Scurg
Male
Scorpio
64
47
Wizard
Item
Caution
Throw Item
Teleport

Dagger

Holy Miter
Mystic Vest
Red Shoes

Fire 3, Bolt 2, Bolt 4, Ice 2, Ice 3, Ice 4, Empower, Death
X-Potion, Eye Drop, Soft



Lanshaft
Female
Virgo
70
63
Lancer
Black Magic
Distribute
Sicken
Waterbreathing

Gungnir
Round Shield
Grand Helmet
Mythril Armor
Vanish Mantle

Level Jump8, Vertical Jump7
Fire, Fire 2, Fire 4, Bolt 2, Bolt 3, Ice



PoroTact
Male
Scorpio
41
76
Ninja
Item
Counter Flood
Beastmaster
Jump+2

Kunai
Hidden Knife
Feather Hat
Power Sleeve
Feather Boots

Bomb, Staff, Dictionary
Potion, Hi-Potion, X-Potion, Hi-Ether, Soft, Remedy, Phoenix Down



Dtrain332
Female
Taurus
75
57
Samurai
Time Magic
Meatbone Slash
Secret Hunt
Teleport

Muramasa

Bronze Helmet
Carabini Mail
Rubber Shoes

Asura, Koutetsu, Masamune
Slow 2, Float, Galaxy Stop

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Rastanar
Female
Libra
48
65
Lancer
Dance
Critical Quick
Concentrate
Move+2

Partisan
Aegis Shield
Gold Helmet
Genji Armor
Defense Armlet

Level Jump8, Vertical Jump8
Wiznaibus, Void Storage, Dragon Pit



Shakarak
Female
Leo
75
67
Geomancer
Throw
Auto Potion
Attack UP
Retreat

Battle Axe
Round Shield
Leather Hat
Adaman Vest
Cursed Ring

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bomb



Daveb
Female
Gemini
45
39
Ninja
Basic Skill
Auto Potion
Equip Axe
Move-HP Up

Giant Axe
Hidden Knife
Thief Hat
Earth Clothes
Cursed Ring

Shuriken, Bomb, Staff
Dash, Heal, Yell, Wish



Fenaen
Female
Libra
42
56
Wizard
Elemental
Earplug
Equip Gun
Levitate

Romanda Gun

Red Hood
Light Robe
Magic Ring

Fire, Fire 2, Fire 4, Bolt 2, Ice 2
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard

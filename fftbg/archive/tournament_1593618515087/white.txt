Player: !White
Team: White Team
Palettes: White/Blue



DuneMeta
Male
Cancer
81
54
Calculator
Time Magic
Parry
Defend
Move+1

Wizard Rod

Green Beret
Power Sleeve
Reflect Ring

CT, Height, Prime Number, 5, 4, 3
Slow 2, Stop, Demi, Demi 2, Stabilize Time



Mpghappiness
Female
Pisces
70
72
Samurai
Elemental
Brave Save
Equip Bow
Jump+1

Ultimus Bow

Cross Helmet
Black Robe
Magic Gauntlet

Asura, Koutetsu, Heaven's Cloud, Muramasa
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Blizzard, Gusty Wind



Amiture
Female
Virgo
72
70
Archer
White Magic
Sunken State
Sicken
Move-MP Up

Bow Gun
Bronze Shield
Barbuta
Black Costume
Defense Armlet

Charge+2, Charge+5, Charge+7
Cure 2, Raise, Protect, Shell, Wall, Esuna



BlackFireUK
Female
Pisces
40
43
Squire
Draw Out
Critical Quick
Doublehand
Jump+2

Long Sword

Platinum Helmet
Brigandine
Sprint Shoes

Throw Stone, Heal, Tickle, Fury
Murasame, Kiyomori, Muramasa, Kikuichimoji

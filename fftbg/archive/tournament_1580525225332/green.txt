Player: !Green
Team: Green Team
Palettes: Green/White



HV5H
Male
Sagittarius
65
70
Monk
Talk Skill
Distribute
Equip Axe
Retreat

Flame Whip

Headgear
Wizard Outfit
Battle Boots

Revive
Praise, Preach, Insult, Refute, Rehabilitate



SEGA Zack
Male
Scorpio
72
70
Ninja
Black Magic
Counter
Equip Armor
Levitate

Morning Star
Morning Star
Platinum Helmet
Clothes
Diamond Armlet

Shuriken, Bomb, Knife, Dictionary
Fire, Fire 3, Ice, Ice 4, Flare



Shakarak
Male
Virgo
58
61
Wizard
Summon Magic
Mana Shield
Halve MP
Levitate

Orichalcum

Golden Hairpin
Light Robe
Dracula Mantle

Fire, Fire 3, Bolt, Bolt 3, Bolt 4, Ice 3, Frog
Moogle, Ramuh, Carbunkle, Leviathan, Salamander, Silf



Kremath
Female
Gemini
50
78
Thief
White Magic
Catch
Doublehand
Waterwalking

Sasuke Knife

Triangle Hat
Mythril Vest
Magic Gauntlet

Steal Heart, Arm Aim
Cure, Cure 3, Raise, Esuna, Magic Barrier

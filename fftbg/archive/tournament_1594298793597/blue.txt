Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Draconis345
Female
Aquarius
58
74
Calculator
Limit
Arrow Guard
Equip Armor
Move+2

Bestiary

Diamond Helmet
Gold Armor
Wizard Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



CosmicTactician
Female
Virgo
60
44
Wizard
Draw Out
Absorb Used MP
Equip Sword
Ignore Height

Iron Sword

Holy Miter
Chameleon Robe
Cursed Ring

Fire 2, Fire 4, Bolt 2, Bolt 4
Koutetsu, Bizen Boat, Murasame



VolgraTheMoose
Male
Libra
45
65
Samurai
Time Magic
Counter Magic
Defense UP
Teleport 2

Koutetsu Knife

Platinum Helmet
Reflect Mail
Spike Shoes

Bizen Boat, Heaven's Cloud
Haste, Stop, Immobilize, Float, Quick, Demi 2, Stabilize Time, Meteor



Just Here2
Female
Gemini
49
63
Summoner
Punch Art
Hamedo
Equip Polearm
Ignore Terrain

Battle Bamboo

Triangle Hat
Wizard Robe
Reflect Ring

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Salamander, Fairy
Secret Fist, Purification

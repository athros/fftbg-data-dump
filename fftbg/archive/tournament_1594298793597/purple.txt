Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SephDarkheart
Male
Capricorn
43
46
Thief
Throw
Catch
Long Status
Waterbreathing

Mythril Knife

Golden Hairpin
Wizard Outfit
Genji Gauntlet

Steal Heart, Steal Armor, Steal Weapon
Bomb, Axe, Wand



Winteriscorning
Female
Gemini
45
68
Geomancer
Item
Meatbone Slash
Dual Wield
Move+1

Slasher
Panther Bag
Holy Miter
Chameleon Robe
Rubber Shoes

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, Hi-Potion, Ether, Hi-Ether, Antidote, Soft



Prince Rogers Nelson
Male
Sagittarius
62
61
Wizard
Steal
Parry
Magic Defense UP
Move-HP Up

Dragon Rod

Leather Hat
Earth Clothes
Wizard Mantle

Fire, Bolt 3, Ice 3, Ice 4, Death
Gil Taking, Arm Aim



J2DaBibbles
Female
Scorpio
42
63
Time Mage
Steal
Auto Potion
Doublehand
Levitate

Gokuu Rod

Feather Hat
Clothes
Defense Ring

Haste, Slow 2, Immobilize, Demi 2, Stabilize Time, Meteor
Steal Accessory, Steal Status

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



OneHundredFists
Male
Libra
39
62
Ninja
Elemental
Abandon
Concentrate
Ignore Height

Short Edge
Spell Edge
Headgear
Wizard Outfit
N-Kai Armlet

Shuriken
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind



MattMan119
Male
Aries
61
66
Lancer
Draw Out
Meatbone Slash
Secret Hunt
Lava Walking

Mythril Spear
Genji Shield
Leather Helmet
Light Robe
Defense Armlet

Level Jump8, Vertical Jump7
Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



Phi Sig
Male
Gemini
72
69
Mediator
Elemental
Parry
Dual Wield
Ignore Terrain

Romanda Gun
Blast Gun
Feather Hat
Black Robe
Reflect Ring

Invitation, Solution, Negotiate, Mimic Daravon, Refute, Rehabilitate
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Toka222
Male
Serpentarius
79
65
Geomancer
Item
HP Restore
Throw Item
Swim

Kikuichimoji
Aegis Shield
Twist Headband
Judo Outfit
Magic Gauntlet

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft

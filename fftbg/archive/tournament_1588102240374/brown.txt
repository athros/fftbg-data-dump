Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Laserman1000
Monster
Aries
72
36
Archaic Demon










Maakur
Female
Capricorn
74
79
Time Mage
White Magic
Damage Split
Equip Polearm
Ignore Terrain

Whale Whisker

Triangle Hat
White Robe
Bracer

Haste, Slow 2, Stop, Float, Quick, Demi 2, Stabilize Time
Cure, Cure 3, Raise, Reraise, Shell 2, Esuna



Richardserious
Female
Sagittarius
60
53
Oracle
Dance
Brave Up
Short Status
Swim

Musk Rod

Feather Hat
Adaman Vest
Small Mantle

Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Sleep
Wiznaibus, Polka Polka, Last Dance, Dragon Pit



Gooseyourself
Female
Leo
59
49
Oracle
Punch Art
PA Save
Equip Axe
Waterbreathing

Flame Whip

Barette
Clothes
Defense Ring

Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep
Secret Fist

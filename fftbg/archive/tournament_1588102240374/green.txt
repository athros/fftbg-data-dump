Player: !Green
Team: Green Team
Palettes: Green/White



Old Overholt
Monster
Cancer
49
43
Vampire










CosmicTactician
Female
Cancer
63
42
Oracle
Basic Skill
Parry
Short Status
Jump+3

Octagon Rod

Twist Headband
Adaman Vest
Magic Gauntlet

Poison, Zombie, Silence Song, Confusion Song, Dispel Magic
Accumulate, Throw Stone



JumbocactuarX27
Male
Taurus
53
51
Bard
Steal
Auto Potion
Equip Knife
Jump+3

Air Knife

Black Hood
Clothes
Angel Ring

Nameless Song, Diamond Blade
Steal Armor, Steal Shield, Steal Weapon, Leg Aim



Run With STONE GUNS
Male
Scorpio
56
65
Mediator
Time Magic
Sunken State
Dual Wield
Jump+2

Bestiary
Battle Folio
Holy Miter
Secret Clothes
Sprint Shoes

Invitation, Persuade, Praise, Solution, Death Sentence, Insult, Rehabilitate
Haste, Stop, Immobilize, Quick, Demi 2, Stabilize Time

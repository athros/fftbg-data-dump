Final Bets: blue - 5 bets for 4,719G (68.2%, x0.47); white - 8 bets for 2,199G (31.8%, x2.15)

blue bets:
NovaKnight21: 1,919G (40.7%, 1,919G)
Lydian_C: 1,100G (23.3%, 11,395G)
BirbBrainsBot: 1,000G (21.2%, 18,044G)
iBardic: 500G (10.6%, 849G)
reddwind_: 200G (4.2%, 44,593G)

white bets:
3ngag3: 846G (38.5%, 846G)
Lanshaft: 600G (27.3%, 3,457G)
datadrivenbot: 200G (9.1%, 70,844G)
HASTERIOUS: 152G (6.9%, 3,058G)
gorgewall: 101G (4.6%, 6,513G)
MinBetBot: 100G (4.5%, 6,215G)
AllInBot: 100G (4.5%, 100G)
ko2q: 100G (4.5%, 1,765G)

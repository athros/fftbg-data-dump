Player: !Green
Team: Green Team
Palettes: Green/White



MrJamDango
Female
Libra
58
53
Summoner
Black Magic
Critical Quick
Doublehand
Retreat

Faith Rod

Twist Headband
Mythril Vest
Elf Mantle

Moogle, Golem, Carbunkle, Bahamut, Odin
Bolt 2, Bolt 4, Ice



ArchKnightX
Female
Virgo
51
62
Dancer
Elemental
HP Restore
Short Charge
Jump+2

Hydra Bag

Holy Miter
Chain Vest
Reflect Ring

Wiznaibus, Polka Polka, Nameless Dance, Void Storage, Dragon Pit
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Nedryerson01
Male
Gemini
74
75
Chemist
Sing
Parry
Magic Attack UP
Levitate

Hydra Bag

Holy Miter
Leather Outfit
Small Mantle

Potion, Hi-Potion, Eye Drop, Remedy, Phoenix Down
Battle Song, Diamond Blade, Space Storage, Sky Demon, Hydra Pit



Mirapoix
Female
Taurus
48
49
Wizard
Jump
Sunken State
Equip Axe
Ignore Height

Giant Axe

Black Hood
Secret Clothes
108 Gems

Fire, Fire 3, Fire 4, Bolt 2, Bolt 3, Bolt 4, Ice 3, Empower, Frog
Level Jump8, Vertical Jump6

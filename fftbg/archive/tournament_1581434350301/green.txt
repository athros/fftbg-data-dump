Player: !Green
Team: Green Team
Palettes: Green/White



MrUbiq
Male
Cancer
58
58
Calculator
White Magic
HP Restore
Defense UP
Levitate

Flame Rod

Twist Headband
Earth Clothes
Elf Mantle

CT, Height, Prime Number
Raise, Protect 2, Esuna



Mayfu
Female
Taurus
81
55
Archer
Throw
Counter
Doublehand
Move+2

Windslash Bow

Leather Hat
Clothes
Leather Mantle

Charge+1
Shuriken



Mrasin
Male
Pisces
73
50
Summoner
Talk Skill
Sunken State
Long Status
Ignore Height

Rod

Red Hood
Chameleon Robe
Sprint Shoes

Moogle, Carbunkle, Leviathan, Salamander, Lich, Cyclops
Invitation, Praise, Insult, Mimic Daravon, Rehabilitate



Off The Crossbar
Male
Serpentarius
43
42
Thief
Draw Out
Critical Quick
Equip Sword
Ignore Terrain

Kikuichimoji

Black Hood
Leather Outfit
Cherche

Steal Accessory
Koutetsu, Muramasa

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SimplyRed
Male
Sagittarius
79
73
Geomancer
Battle Skill
Dragon Spirit
Sicken
Jump+2

Ice Brand
Buckler
Red Hood
Black Costume
Vanish Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Armor Break, Weapon Break, Power Break



Miyokari
Female
Scorpio
43
49
Monk
Basic Skill
Regenerator
Sicken
Waterbreathing



Golden Hairpin
Black Costume
Battle Boots

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Accumulate, Throw Stone, Heal, Yell, Scream



Gorgewall
Monster
Aquarius
79
41
Black Goblin










0v3rr8d
Male
Cancer
75
69
Geomancer
Item
Counter
Equip Armor
Waterbreathing

Asura Knife
Bronze Shield
Triangle Hat
Crystal Mail
Elf Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Potion, X-Potion, Ether, Hi-Ether, Elixir, Remedy, Phoenix Down

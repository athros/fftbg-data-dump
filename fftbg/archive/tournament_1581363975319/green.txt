Player: !Green
Team: Green Team
Palettes: Green/White



MoonSlayerRS
Male
Aries
44
49
Bard
Elemental
Damage Split
Short Charge
Teleport

Fairy Harp

Flash Hat
Adaman Vest
Diamond Armlet

Angel Song, Life Song, Cheer Song, Nameless Song, Diamond Blade, Hydra Pit
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



BigDLicious91
Female
Aries
60
51
Knight
Jump
Absorb Used MP
Short Charge
Move-HP Up

Chaos Blade

Leather Helmet
Silk Robe
Rubber Shoes

Shield Break, Power Break, Stasis Sword, Justice Sword
Level Jump2, Vertical Jump8



BenYuPoker
Female
Libra
69
75
Priest
Elemental
Sunken State
Short Charge
Move+1

Healing Staff

Triangle Hat
Linen Robe
Jade Armlet

Cure, Cure 4, Raise, Raise 2, Shell, Holy
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Gusty Wind



Withhelde
Male
Aquarius
75
68
Mime

Abandon
Martial Arts
Move-MP Up



Headgear
Leather Outfit
Reflect Ring

Mimic


Player: !Red
Team: Red Team
Palettes: Red/Brown



Lokenwow
Monster
Scorpio
67
43
Squidraken










HaateXIII
Male
Leo
58
50
Lancer
Charge
Faith Save
Equip Armor
Ignore Terrain

Javelin
Platinum Shield
Flash Hat
Leather Armor
Cursed Ring

Level Jump4, Vertical Jump5
Charge+1, Charge+2, Charge+3, Charge+5, Charge+10



Rislyeu
Male
Sagittarius
54
50
Mime

Meatbone Slash
Dual Wield
Swim



Black Hood
Brigandine
Rubber Shoes

Mimic




KasugaiRoastedPeas
Male
Capricorn
76
50
Monk
Summon Magic
Dragon Spirit
Equip Axe
Fly

Sage Staff

Red Hood
Earth Clothes
Spike Shoes

Spin Fist, Pummel, Secret Fist, Purification, Revive, Seal Evil
Moogle, Shiva, Ramuh, Bahamut, Leviathan, Fairy

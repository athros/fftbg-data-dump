Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ColetteMSLP
Male
Gemini
49
47
Lancer
Sing
PA Save
Maintenance
Ignore Height

Gungnir
Crystal Shield
Iron Helmet
Carabini Mail
Germinas Boots

Level Jump5, Vertical Jump8
Cheer Song, Space Storage, Sky Demon, Hydra Pit



Sairentozon7
Male
Scorpio
58
71
Summoner
Draw Out
Counter Tackle
Equip Gun
Levitate

Stone Gun

Leather Hat
Wizard Robe
Defense Ring

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin, Leviathan, Silf, Fairy, Cyclops
Koutetsu, Kiyomori



Lali Lulelo
Male
Gemini
79
50
Chemist
Punch Art
Damage Split
Dual Wield
Waterwalking

Blind Knife
Mythril Knife
Holy Miter
Clothes
Small Mantle

Hi-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Spin Fist, Wave Fist, Purification, Chakra, Revive



Waterwatereverywhere
Male
Sagittarius
54
41
Thief
Yin Yang Magic
Earplug
Equip Polearm
Move-MP Up

Cashmere

Flash Hat
Adaman Vest
Angel Ring

Gil Taking, Steal Heart, Steal Weapon, Arm Aim
Life Drain, Foxbird, Dispel Magic, Paralyze, Sleep

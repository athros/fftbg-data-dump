Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Arcblazer23
Male
Capricorn
42
79
Priest
Jump
Faith Save
Dual Wield
Teleport

Wizard Staff
Rainbow Staff
Green Beret
Silk Robe
Feather Boots

Cure, Raise, Reraise, Shell 2, Esuna
Level Jump8, Vertical Jump7



ALY327
Female
Libra
66
44
Geomancer
Steal
Regenerator
Long Status
Ignore Height

Heaven's Cloud
Escutcheon
Triangle Hat
Silk Robe
Red Shoes

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Gil Taking, Steal Shield, Arm Aim



TeaTime29
Female
Taurus
61
43
Thief
Draw Out
Dragon Spirit
Equip Polearm
Jump+2

Battle Bamboo

Barette
Mystic Vest
Red Shoes

Steal Helmet, Steal Accessory, Steal Status
Asura, Bizen Boat, Muramasa



Munmro
Male
Sagittarius
75
53
Samurai
Sing
Arrow Guard
Doublehand
Jump+1

Asura Knife

Barbuta
Platinum Armor
Feather Mantle

Murasame, Heaven's Cloud
Angel Song, Life Song, Nameless Song, Last Song

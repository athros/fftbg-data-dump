Final Bets: green - 8 bets for 4,755G (51.3%, x0.95); yellow - 8 bets for 4,507G (48.7%, x1.06)

green bets:
Arcblazer23: 1,655G (34.8%, 3,310G)
reinoe: 1,000G (21.0%, 13,729G)
prince_rogers_nelson_: 1,000G (21.0%, 9,914G)
Lythe_Caraker: 500G (10.5%, 105,863G)
KyleWonToLiveForever: 300G (6.3%, 300G)
Spuzzmocker: 100G (2.1%, 500G)
datadrivenbot: 100G (2.1%, 46,439G)
serperemagus: 100G (2.1%, 13,255G)

yellow bets:
VolgraTheMoose: 1,001G (22.2%, 7,126G)
BirbBrainsBot: 1,000G (22.2%, 90,735G)
DuraiPapers: 1,000G (22.2%, 31,164G)
solomongrundy85: 544G (12.1%, 544G)
Powermhero: 500G (11.1%, 22,064G)
getthemoneyz: 262G (5.8%, 1,012,965G)
nifboy: 100G (2.2%, 3,921G)
extinctrational: 100G (2.2%, 937G)

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Jerboj
Female
Taurus
69
49
Monk
Draw Out
Counter
Secret Hunt
Retreat



Flash Hat
Mythril Vest
Feather Boots

Pummel, Earth Slash, Secret Fist, Purification, Revive
Asura



Lord Burrah
Female
Libra
56
62
Archer
Throw
Critical Quick
Magic Defense UP
Move+3

Cross Bow
Gold Shield
Twist Headband
Leather Outfit
Defense Ring

Charge+1, Charge+3, Charge+5, Charge+10, Charge+20
Shuriken, Dictionary



Red Lancer
Male
Aries
70
62
Mime

Counter
Dual Wield
Jump+1



Triangle Hat
Mystic Vest
Dracula Mantle

Mimic




OmnibotGamma
Male
Leo
66
67
Lancer
Draw Out
Arrow Guard
Doublehand
Move+3

Javelin

Crystal Helmet
Gold Armor
Dracula Mantle

Level Jump8, Vertical Jump3
Asura, Heaven's Cloud, Kiyomori

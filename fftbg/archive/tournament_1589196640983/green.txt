Player: !Green
Team: Green Team
Palettes: Green/White



Tekigi
Monster
Capricorn
40
76
Trent










DaveStrider55
Male
Taurus
60
46
Archer
Yin Yang Magic
Auto Potion
Long Status
Move+2

Poison Bow
Ice Shield
Feather Hat
Earth Clothes
Feather Mantle

Charge+1
Spell Absorb, Paralyze



Sairentozon7
Male
Libra
78
68
Mime

Brave Up
Long Status
Levitate



Black Hood
Wizard Outfit
Battle Boots

Mimic




Gorgewall
Male
Cancer
80
68
Squire
Jump
Earplug
Long Status
Waterbreathing

Rune Blade
Ice Shield
Mythril Helmet
Mythril Vest
Magic Gauntlet

Accumulate, Throw Stone, Tickle, Yell, Wish
Level Jump3, Vertical Jump8

Final Bets: purple - 7 bets for 2,516G (20.1%, x3.98); brown - 12 bets for 10,009G (79.9%, x0.25)

purple bets:
Bryon_W: 689G (27.4%, 689G)
BirbBrainsBot: 686G (27.3%, 185,911G)
getthemoneyz: 440G (17.5%, 658,978G)
Tekigi: 266G (10.6%, 266G)
Miyokari: 210G (8.3%, 12,205G)
twelfthrootoftwo: 200G (7.9%, 1,834G)
roqqqpsi: 25G (1.0%, 2,527G)

brown bets:
HorusTaurus: 4,000G (40.0%, 10,619G)
HaplessOne: 1,085G (10.8%, 1,085G)
ThePineappleSalesman: 1,000G (10.0%, 32,692G)
Mtueni: 1,000G (10.0%, 227,727G)
nifboy: 1,000G (10.0%, 33,569G)
Dexsana: 537G (5.4%, 537G)
CustomGameMaster: 400G (4.0%, 400G)
Evewho: 325G (3.2%, 3,259G)
gorgewall: 301G (3.0%, 5,670G)
ungabunga_bot: 161G (1.6%, 453,892G)
lastly: 100G (1.0%, 3,929G)
datadrivenbot: 100G (1.0%, 11,079G)

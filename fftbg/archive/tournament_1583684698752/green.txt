Player: !Green
Team: Green Team
Palettes: Green/White



Victoriolue
Female
Cancer
76
76
Mediator
Yin Yang Magic
Arrow Guard
Dual Wield
Swim

Mythril Knife
Blind Knife
Green Beret
Chameleon Robe
Battle Boots

Persuade, Threaten, Solution, Negotiate, Refute, Rehabilitate
Blind, Spell Absorb, Silence Song, Blind Rage, Foxbird, Confusion Song, Sleep



SkylerBunny
Male
Scorpio
64
65
Oracle
Talk Skill
Earplug
Doublehand
Jump+2

Musk Rod

Thief Hat
Black Costume
Power Wrist

Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Blind Rage, Dispel Magic
Death Sentence, Refute



Mirapoix
Male
Scorpio
57
59
Monk
Steal
Dragon Spirit
Dual Wield
Move+3



Triangle Hat
Chain Vest
Germinas Boots

Purification, Revive
Steal Weapon, Steal Status



MattMan119
Male
Aries
68
81
Samurai
Jump
PA Save
Martial Arts
Teleport



Gold Helmet
Wizard Robe
Spike Shoes

Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa
Level Jump8, Vertical Jump4

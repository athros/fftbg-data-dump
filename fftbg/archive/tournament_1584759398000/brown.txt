Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Smashy
Male
Sagittarius
69
59
Wizard
Steal
Dragon Spirit
Secret Hunt
Waterwalking

Dragon Rod

Red Hood
Clothes
Rubber Shoes

Fire, Fire 3, Fire 4, Bolt, Bolt 4, Ice 3, Ice 4
Steal Armor, Steal Status



UmaiJam
Female
Sagittarius
80
71
Monk
Steal
PA Save
Secret Hunt
Ignore Terrain



Holy Miter
Chain Vest
Feather Mantle

Wave Fist, Secret Fist, Purification, Revive
Leg Aim



Powergems
Male
Sagittarius
66
40
Chemist
Charge
Brave Up
Equip Shield
Move+2

Cute Bag
Diamond Shield
Ribbon
Earth Clothes
Battle Boots

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Holy Water
Charge+1



Lanshaft
Female
Taurus
54
58
Monk
Black Magic
Catch
Secret Hunt
Move+2



Triangle Hat
Black Costume
Small Mantle

Spin Fist, Revive, Seal Evil
Bolt, Bolt 3, Ice, Death

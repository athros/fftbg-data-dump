Player: !Green
Team: Green Team
Palettes: Green/White



Peeronid
Female
Scorpio
74
78
Archer
Time Magic
Parry
Doublehand
Jump+2

Blaze Gun

Headgear
Black Costume
Small Mantle

Charge+1, Charge+2, Charge+4, Charge+5
Haste 2, Slow 2, Reflect, Demi, Stabilize Time



Thaetreis
Male
Libra
65
55
Monk
Jump
Dragon Spirit
Defense UP
Move+3



Holy Miter
Leather Outfit
Magic Gauntlet

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Level Jump5, Vertical Jump7



DeathTaxesAndAnime
Female
Aquarius
58
54
Mime

Counter Flood
Defense UP
Move+2



Green Beret
Adaman Vest
Bracer

Mimic




Lyner87
Male
Sagittarius
66
45
Mediator
Draw Out
Mana Shield
Defense UP
Move-HP Up

Bestiary

Green Beret
Judo Outfit
Diamond Armlet

Invitation, Praise, Threaten, Preach, Insult
Bizen Boat, Murasame, Heaven's Cloud

Player: !Black
Team: Black Team
Palettes: Black/Red



Omegasuspekt
Monster
Capricorn
62
73
Skeleton










AnthraX85
Female
Capricorn
47
73
Knight
Punch Art
Regenerator
Halve MP
Ignore Height

Platinum Sword
Aegis Shield
Crystal Helmet
Crystal Mail
Elf Mantle

Head Break, Stasis Sword, Justice Sword
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification



ShintaroNayaka
Female
Gemini
80
64
Wizard
Basic Skill
Counter Magic
Long Status
Move+2

Flame Rod

Golden Hairpin
Power Sleeve
Elf Mantle

Fire 2, Bolt 3, Ice, Ice 4, Flare
Accumulate, Heal, Tickle



MoonSlayerRS
Male
Libra
45
47
Thief
Time Magic
HP Restore
Equip Bow
Ignore Height

Mythril Bow

Thief Hat
Power Sleeve
Cursed Ring

Steal Helmet, Steal Armor, Steal Accessory
Haste 2, Slow 2, Reflect, Demi 2, Stabilize Time

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Tithonus
Female
Libra
52
66
Thief
Dance
Critical Quick
Sicken
Teleport

Spell Edge

Green Beret
Leather Outfit
Battle Boots

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon
Dragon Pit



RunicMagus
Monster
Sagittarius
73
51
Dark Behemoth










Dreadnxught
Male
Cancer
55
53
Samurai
Jump
Mana Shield
Attack UP
Retreat

Kikuichimoji

Barbuta
Gold Armor
Elf Mantle

Muramasa, Kikuichimoji
Level Jump2, Vertical Jump6



HaplessOne
Female
Scorpio
54
46
Samurai
Dance
Counter
Magic Attack UP
Ignore Height

Holy Lance

Mythril Helmet
Silk Robe
Magic Ring

Asura, Koutetsu, Kikuichimoji
Disillusion, Void Storage, Nether Demon, Dragon Pit

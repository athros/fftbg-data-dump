Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Langrisser
Female
Cancer
48
80
Archer
Steal
Counter Magic
Secret Hunt
Retreat

Mythril Gun
Bronze Shield
Golden Hairpin
Mythril Vest
108 Gems

Charge+1, Charge+2, Charge+20
Steal Heart, Steal Armor, Steal Shield, Arm Aim



RageImmortaI
Male
Libra
59
57
Calculator
Imp Skill
HP Restore
Sicken
Move+2

Bestiary
Mythril Shield
Barbuta
Brigandine
Feather Boots

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam



Chompie
Female
Aquarius
65
59
Mediator
Item
Abandon
Beastmaster
Move-MP Up

Mythril Gun

Green Beret
Black Robe
Reflect Ring

Praise, Threaten, Solution, Insult, Negotiate, Refute, Rehabilitate
Potion, Hi-Potion, Ether, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



YaBoy125
Female
Aries
74
73
Monk
Talk Skill
Meatbone Slash
Magic Attack UP
Move-HP Up



Golden Hairpin
Mystic Vest
108 Gems

Earth Slash, Purification, Revive
Persuade, Preach, Insult, Refute, Rehabilitate

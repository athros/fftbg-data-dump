Player: !White
Team: White Team
Palettes: White/Blue



Laserman1000
Male
Capricorn
79
77
Bard
Black Magic
Distribute
Doublehand
Waterbreathing

Ice Bow

Feather Hat
Adaman Vest
Reflect Ring

Angel Song, Cheer Song, Magic Song, Space Storage, Sky Demon
Fire 2, Fire 3, Ice, Death



ScurvyMitch
Female
Libra
40
51
Monk
Dance
Blade Grasp
Defense UP
Move+2



Leather Hat
Black Costume
Magic Ring

Secret Fist, Purification, Chakra
Wiznaibus, Slow Dance, Polka Polka, Obsidian Blade



Baron Von Scrub
Female
Aries
61
48
Chemist
Basic Skill
Counter Tackle
Dual Wield
Jump+2

Assassin Dagger
Blood Sword
Flash Hat
Chain Vest
Feather Boots

X-Potion, Ether, Echo Grass, Maiden's Kiss
Accumulate, Dash, Throw Stone, Heal, Tickle



Spuzzmocker
Male
Libra
58
41
Geomancer
Sing
Damage Split
Beastmaster
Jump+1

Giant Axe
Round Shield
Black Hood
Wizard Outfit
Battle Boots

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Life Song, Magic Song, Nameless Song, Last Song, Diamond Blade

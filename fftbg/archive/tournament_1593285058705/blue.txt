Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Male
Aquarius
42
68
Mediator
Jump
Earplug
Magic Attack UP
Move+1

Battle Folio

Leather Hat
White Robe
Chantage

Persuade, Threaten, Negotiate, Mimic Daravon, Refute
Level Jump5, Vertical Jump3



Sairentozon7
Male
Capricorn
77
55
Calculator
Yin Yang Magic
Speed Save
Attack UP
Jump+1

Bestiary

Thief Hat
Mystic Vest
Spike Shoes

CT, Height, Prime Number, 4
Blind, Poison, Spell Absorb, Dispel Magic



OpHendoslice
Male
Virgo
71
55
Squire
Draw Out
Counter Magic
Dual Wield
Retreat

Platinum Sword
Flame Whip
Barbuta
Crystal Mail
Sprint Shoes

Dash, Heal, Yell, Cheer Up, Scream
Asura, Koutetsu, Murasame



Rilgon
Male
Leo
49
67
Priest
Talk Skill
Distribute
Halve MP
Ignore Height

Flame Whip

Feather Hat
Silk Robe
Diamond Armlet

Cure, Raise, Raise 2, Shell 2, Esuna, Magic Barrier
Invitation, Persuade, Praise, Mimic Daravon, Refute

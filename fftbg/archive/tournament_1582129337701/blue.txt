Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Songdeh
Female
Pisces
57
79
Archer
Draw Out
Sunken State
Equip Gun
Jump+2

Bestiary
Ice Shield
Flash Hat
Black Costume
Sprint Shoes

Charge+1, Charge+4, Charge+20
Heaven's Cloud, Kikuichimoji



TheBlobZ
Male
Pisces
78
43
Archer
Draw Out
Parry
Doublehand
Levitate

Blast Gun

Twist Headband
Wizard Outfit
Diamond Armlet

Charge+2, Charge+3, Charge+10
Koutetsu, Murasame, Muramasa, Masamune



Narcius
Male
Virgo
66
52
Calculator
Imp Skill
Distribute
Equip Axe
Fly

Morning Star

Barbuta
Chain Mail
Small Mantle

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Wing Attack, Look of Fright, Look of Devil, Doom, Beam



Astrozin11
Female
Taurus
77
79
Summoner
White Magic
Abandon
Equip Shield
Retreat

Ice Rod
Crystal Shield
Leather Hat
Chain Vest
Germinas Boots

Moogle, Golem, Salamander, Fairy
Cure 3, Raise, Raise 2, Regen, Shell, Shell 2, Esuna

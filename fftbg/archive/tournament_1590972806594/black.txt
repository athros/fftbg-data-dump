Player: !Black
Team: Black Team
Palettes: Black/Red



Nifboy
Female
Pisces
43
61
Summoner
Yin Yang Magic
Distribute
Halve MP
Waterwalking

Dragon Rod

Green Beret
Power Sleeve
Rubber Shoes

Moogle, Shiva, Ramuh, Ifrit, Titan, Bahamut, Salamander
Blind, Life Drain, Pray Faith, Zombie, Dispel Magic, Sleep



Grandlanzer
Female
Scorpio
70
43
Monk
Time Magic
Brave Save
Short Charge
Teleport



Black Hood
Secret Clothes
Diamond Armlet

Spin Fist, Pummel, Wave Fist, Purification, Revive
Haste, Haste 2, Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time



Xcessive30
Male
Leo
69
72
Oracle
Time Magic
MP Restore
Magic Defense UP
Move+3

Octagon Rod

Cachusha
Leather Outfit
Feather Boots

Blind, Spell Absorb, Zombie, Foxbird, Dispel Magic, Paralyze
Haste, Haste 2, Slow 2, Float, Reflect, Demi, Stabilize Time, Meteor



Laserman1000
Male
Virgo
41
46
Time Mage
Draw Out
Meatbone Slash
Short Charge
Ignore Terrain

Gokuu Rod

Feather Hat
Linen Robe
Vanish Mantle

Haste, Slow, Immobilize, Stabilize Time
Bizen Boat, Murasame, Kiyomori

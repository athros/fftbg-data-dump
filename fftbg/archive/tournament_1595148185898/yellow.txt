Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lowlf
Female
Serpentarius
56
48
Oracle
Draw Out
Hamedo
Doublehand
Move+2

Papyrus Codex

Headgear
Linen Robe
108 Gems

Poison, Doubt Faith, Blind Rage, Foxbird
Koutetsu, Kiyomori



Creggers
Male
Scorpio
70
58
Ninja
Draw Out
Abandon
Equip Gun
Move+3

Bestiary
Bestiary
Holy Miter
Earth Clothes
N-Kai Armlet

Knife
Asura, Koutetsu, Murasame, Kiyomori



StealthModeLocke
Male
Scorpio
63
59
Chemist
Battle Skill
Brave Save
Martial Arts
Lava Walking

Cute Bag

Golden Hairpin
Mythril Vest
Spike Shoes

Potion, Antidote, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Armor Break, Weapon Break, Speed Break, Justice Sword



NeonTarget
Male
Libra
60
71
Thief
Charge
Faith Save
Equip Bow
Lava Walking

Poison Bow

Green Beret
Mystic Vest
Defense Armlet

Arm Aim
Charge+7

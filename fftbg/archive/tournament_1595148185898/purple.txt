Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Randgridr
Female
Scorpio
54
76
Chemist
Yin Yang Magic
HP Restore
Defense UP
Move+2

Star Bag

Flash Hat
Chain Vest
Dracula Mantle

Potion, X-Potion, Echo Grass
Blind, Pray Faith, Zombie, Paralyze, Petrify



Yarby
Male
Pisces
47
62
Mediator
Jump
Counter Flood
Equip Sword
Waterbreathing

Save the Queen

Leather Hat
Earth Clothes
Defense Ring

Praise, Threaten, Negotiate, Refute, Rehabilitate
Level Jump5, Vertical Jump7



Hasterious
Female
Capricorn
50
42
Samurai
Jump
Arrow Guard
Magic Defense UP
Ignore Height

Muramasa

Platinum Helmet
Bronze Armor
108 Gems

Asura, Koutetsu, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji, Masamune, Chirijiraden
Level Jump3, Vertical Jump8



ZZ Yoshi
Male
Pisces
78
64
Mime

Counter Flood
Dual Wield
Waterwalking



Leather Hat
Adaman Vest
Defense Ring

Mimic


Player: !Black
Team: Black Team
Palettes: Black/Red



Actual JP
Female
Virgo
46
38
Ninja
White Magic
Brave Save
Attack UP
Move+3

Short Edge
Air Knife
Holy Miter
Wizard Outfit
Cursed Ring

Shuriken, Knife
Cure, Cure 2, Cure 3, Cure 4, Raise 2, Reraise, Protect 2, Shell, Esuna



Calle888
Male
Aquarius
53
64
Chemist
Jump
Earplug
Long Status
Waterbreathing

Stone Gun

Twist Headband
Earth Clothes
Magic Ring

Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Level Jump2, Vertical Jump6



DesertWooder
Female
Capricorn
40
47
Summoner
Black Magic
Parry
Doublehand
Move-MP Up

Sage Staff

Red Hood
Light Robe
Genji Gauntlet

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Salamander, Silf, Fairy
Fire 2, Fire 4, Bolt 4, Ice 2, Empower, Death



Zeroroute
Female
Pisces
49
69
Geomancer
Item
Auto Potion
Magic Attack UP
Levitate

Star Bag
Crystal Shield
Green Beret
Judo Outfit
Defense Armlet

Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
X-Potion, Ether, Antidote, Eye Drop, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Novapuppets
Male
Libra
45
49
Geomancer
Draw Out
Abandon
Magic Defense UP
Teleport

Battle Axe
Bronze Shield
Black Hood
Black Robe
Spike Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Kiyomori, Muramasa



DavenIII
Male
Gemini
74
50
Monk
Time Magic
Auto Potion
Equip Bow
Jump+1

Yoichi Bow

Black Hood
Clothes
Germinas Boots

Spin Fist, Pummel, Purification, Chakra, Revive
Haste, Slow, Float, Demi, Stabilize Time, Meteor



Gormkt30
Female
Capricorn
62
74
Dancer
Time Magic
Counter Magic
Dual Wield
Waterbreathing

Cashmere
Cashmere
Black Hood
Wizard Robe
Angel Ring

Disillusion
Float, Demi, Demi 2, Stabilize Time



Eldente
Male
Capricorn
80
78
Summoner
Throw
Arrow Guard
Short Status
Jump+1

Thunder Rod

Golden Hairpin
Chameleon Robe
Germinas Boots

Ramuh, Ifrit, Titan, Bahamut, Leviathan, Fairy
Shuriken

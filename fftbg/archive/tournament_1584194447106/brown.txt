Player: !Brown
Team: Brown Team
Palettes: Brown/Green



JoeykinsGaming
Male
Aquarius
61
44
Priest
Black Magic
Brave Up
Dual Wield
Levitate

Wizard Staff
White Staff
Golden Hairpin
Black Robe
Angel Ring

Cure 3, Raise, Raise 2, Protect 2, Wall, Esuna
Fire 2, Fire 3, Bolt, Bolt 2, Ice, Ice 2, Ice 3, Ice 4, Empower



Volgrathemoose
Female
Virgo
47
44
Ninja
Dance
Parry
Sicken
Ignore Height

Blind Knife
Blind Knife
Barette
Mythril Vest
Feather Boots

Bomb, Spear
Polka Polka, Last Dance



Z32o
Female
Virgo
69
73
Lancer
Charge
Hamedo
Concentrate
Waterbreathing

Obelisk
Escutcheon
Diamond Helmet
Crystal Mail
Wizard Mantle

Level Jump2, Vertical Jump8
Charge+2, Charge+3, Charge+4, Charge+10, Charge+20



Haaaaaank
Male
Scorpio
78
69
Geomancer
Black Magic
Brave Up
Attack UP
Waterbreathing

Giant Axe
Flame Shield
Golden Hairpin
Clothes
Spike Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Blizzard, Lava Ball
Fire 3, Fire 4, Bolt 4, Ice, Ice 3, Frog

Player: !White
Team: White Team
Palettes: White/Blue



Serperemagus
Female
Cancer
54
64
Lancer
Charge
Brave Save
Halve MP
Ignore Terrain

Dragon Whisker
Escutcheon
Crystal Helmet
Genji Armor
Wizard Mantle

Level Jump3, Vertical Jump4
Charge+1, Charge+7



Old Overholt
Male
Scorpio
75
59
Mediator
Throw
MA Save
Dual Wield
Lava Walking

Papyrus Codex
Papyrus Codex
Leather Hat
Linen Robe
Sprint Shoes

Death Sentence, Rehabilitate
Bomb



Gelwain
Male
Virgo
54
41
Ninja
Steal
MA Save
Attack UP
Move-MP Up

Kunai
Morning Star
Flash Hat
Mystic Vest
Sprint Shoes

Shuriken, Bomb
Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory



Aznwanderor
Monster
Cancer
50
74
Blue Dragon








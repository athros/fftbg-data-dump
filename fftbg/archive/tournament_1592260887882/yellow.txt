Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Grandlanzer
Female
Leo
58
49
Mediator
Jump
HP Restore
Martial Arts
Move-HP Up



Red Hood
Chameleon Robe
Defense Ring

Invitation, Praise, Insult, Mimic Daravon, Rehabilitate
Level Jump3, Vertical Jump8



J2DaBibbles
Female
Capricorn
45
63
Summoner
Time Magic
Dragon Spirit
Short Charge
Ignore Terrain

Poison Rod

Leather Hat
Wizard Outfit
Diamond Armlet

Ramuh, Titan, Carbunkle, Silf
Haste 2, Slow 2, Immobilize, Quick, Stabilize Time



InOzWeTrust
Female
Sagittarius
44
58
Geomancer
Charge
Mana Shield
Equip Bow
Jump+1

Night Killer
Bronze Shield
Thief Hat
Mystic Vest
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Charge+3, Charge+4



Spuzzmocker
Female
Pisces
75
70
Thief
Item
Mana Shield
Magic Attack UP
Swim

Sasuke Knife

Twist Headband
Chain Vest
Magic Ring

Gil Taking, Steal Heart, Steal Armor, Steal Status
Ether, Hi-Ether, Remedy, Phoenix Down

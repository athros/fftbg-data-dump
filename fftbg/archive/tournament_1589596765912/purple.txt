Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TheUnforgivenRage
Female
Leo
63
60
Mime

Distribute
Martial Arts
Jump+1



Green Beret
Mystic Vest
N-Kai Armlet

Mimic




LeoNightFury
Female
Pisces
59
57
Chemist
Jump
Arrow Guard
Equip Gun
Waterwalking

Madlemgen

Feather Hat
Earth Clothes
Cursed Ring

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Level Jump8, Vertical Jump8



Jakeduhhsnake
Female
Sagittarius
45
52
Geomancer
Time Magic
Sunken State
Halve MP
Waterbreathing

Battle Axe
Crystal Shield
Feather Hat
Earth Clothes
Cursed Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste, Slow 2, Stop, Reflect, Demi, Demi 2, Stabilize Time



B0shii
Male
Gemini
48
54
Ninja
Sing
Meatbone Slash
Equip Sword
Levitate

Air Knife
Bizen Boat
Green Beret
Mythril Vest
Feather Boots

Shuriken, Dictionary
Life Song, Nameless Song

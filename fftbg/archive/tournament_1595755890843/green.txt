Player: !Green
Team: Green Team
Palettes: Green/White



Nickelbank
Male
Capricorn
54
67
Wizard
Basic Skill
Meatbone Slash
Short Charge
Levitate

Flame Rod

Feather Hat
Robe of Lords
Elf Mantle

Fire, Fire 2, Ice 4
Dash, Throw Stone, Heal, Tickle, Yell



E Ballard
Male
Aries
71
78
Lancer
Talk Skill
Counter Magic
Equip Knife
Waterbreathing

Blind Knife
Diamond Shield
Cross Helmet
Chameleon Robe
Angel Ring

Level Jump4, Vertical Jump5
Invitation, Persuade, Praise, Threaten, Insult, Mimic Daravon, Refute



DamnThatShark
Male
Taurus
64
73
Monk
Black Magic
Regenerator
Dual Wield
Teleport



Golden Hairpin
Wizard Outfit
Defense Armlet

Wave Fist, Earth Slash, Purification
Fire 3, Bolt 4, Ice 3, Ice 4



CassiePhoenix
Male
Pisces
56
46
Lancer
Sing
Counter Magic
Doublehand
Levitate

Octagon Rod

Crystal Helmet
Platinum Armor
Feather Boots

Level Jump5, Vertical Jump5
Angel Song, Cheer Song, Magic Song, Last Song, Diamond Blade

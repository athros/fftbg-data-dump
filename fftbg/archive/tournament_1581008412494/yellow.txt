Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lord Burrah
Female
Taurus
56
73
Squire
Black Magic
Sunken State
Equip Polearm
Fly

Flame Whip

Holy Miter
Black Costume
Germinas Boots

Dash, Heal, Yell, Wish
Fire 4, Bolt 3, Bolt 4, Ice 2



Nightyukiame
Male
Virgo
78
62
Mime

Arrow Guard
Equip Shield
Move-HP Up


Genji Shield
Flash Hat
Secret Clothes
Dracula Mantle

Mimic




IndecisiveNinja
Male
Libra
53
43
Time Mage
Elemental
Damage Split
Defend
Move-HP Up

Bestiary

Feather Hat
Light Robe
Dracula Mantle

Haste, Slow, Stop, Float, Reflect, Demi 2, Stabilize Time
Pitfall, Hell Ivy, Local Quake, Static Shock, Quicksand, Gusty Wind



SpencoJFrog
Male
Aquarius
72
74
Summoner
Yin Yang Magic
Distribute
Short Status
Retreat

Flame Rod

Black Hood
Brigandine
Feather Mantle

Moogle, Shiva, Titan, Carbunkle, Silf, Fairy, Lich, Cyclops
Poison, Spell Absorb, Pray Faith, Zombie, Dispel Magic, Paralyze, Sleep

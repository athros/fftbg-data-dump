Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ElNab
Male
Taurus
47
47
Calculator
White Magic
Absorb Used MP
Equip Gun
Move-MP Up

Thunder Rod

Leather Hat
Wizard Robe
108 Gems

CT, Height, Prime Number, 5
Cure, Raise, Raise 2, Protect, Esuna



Lowlf
Female
Sagittarius
53
48
Time Mage
Talk Skill
Auto Potion
Concentrate
Ignore Height

Wizard Staff

Headgear
White Robe
Magic Gauntlet

Haste, Slow, Slow 2, Stop, Immobilize, Reflect, Stabilize Time, Meteor, Galaxy Stop
Invitation, Persuade, Preach, Solution, Refute, Rehabilitate



NIghtdew14
Male
Sagittarius
52
43
Monk
Jump
Faith Up
Attack UP
Levitate



Red Hood
Judo Outfit
Bracer

Spin Fist, Earth Slash, Purification, Revive
Level Jump5, Vertical Jump7



Ragnarok784
Male
Virgo
69
71
Monk
Black Magic
Counter
Dual Wield
Ignore Terrain



Holy Miter
Chain Vest
N-Kai Armlet

Wave Fist, Secret Fist, Purification, Revive
Fire, Fire 2, Bolt 3, Bolt 4, Ice 4, Flare

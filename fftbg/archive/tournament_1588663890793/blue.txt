Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ewan E
Female
Aquarius
48
50
Wizard
Steal
Critical Quick
Halve MP
Retreat

Air Knife

Feather Hat
Silk Robe
Power Wrist

Bolt, Bolt 4, Ice 4
Steal Weapon, Leg Aim



ZZ Yoshi
Female
Scorpio
61
62
Wizard
Item
Mana Shield
Short Charge
Move+1

Poison Rod

Thief Hat
Black Robe
Sprint Shoes

Fire 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Empower, Death
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



Nifboy
Female
Scorpio
40
76
Dancer
Jump
Abandon
Magic Attack UP
Ignore Terrain

Cashmere

Triangle Hat
Wizard Robe
Sprint Shoes

Wiznaibus, Polka Polka, Obsidian Blade, Nether Demon
Level Jump3, Vertical Jump3



Squasian
Female
Leo
50
56
Thief
Punch Art
Mana Shield
Concentrate
Swim

Coral Sword

Golden Hairpin
Judo Outfit
108 Gems

Steal Weapon, Steal Accessory, Steal Status
Wave Fist, Earth Slash, Purification, Seal Evil

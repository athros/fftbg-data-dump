Player: !Black
Team: Black Team
Palettes: Black/Red



Midori Ribbon
Female
Cancer
75
67
Archer
Steal
Speed Save
Dual Wield
Fly

Lightning Bow

Golden Hairpin
Earth Clothes
Jade Armlet

Charge+1, Charge+7, Charge+10
Steal Helmet, Steal Armor, Steal Status



PatSouI
Male
Virgo
79
73
Ninja
Black Magic
Counter
Attack UP
Waterwalking

Orichalcum
Mage Masher
Ribbon
Secret Clothes
N-Kai Armlet

Bomb
Fire 2, Bolt, Bolt 3, Ice 2, Ice 4, Empower, Death



DeathTaxesAndAnime
Female
Sagittarius
70
79
Samurai
Dance
HP Restore
Beastmaster
Levitate

Holy Lance

Cross Helmet
Crystal Mail
Angel Ring

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Witch Hunt, Wiznaibus, Polka Polka, Disillusion



Leakimiko
Female
Scorpio
78
52
Mediator
Summon Magic
Meatbone Slash
Concentrate
Move+2

Battle Folio

Leather Hat
Earth Clothes
Sprint Shoes

Invitation, Persuade, Praise, Threaten, Negotiate, Mimic Daravon, Refute
Moogle, Shiva, Carbunkle, Bahamut, Odin, Fairy, Lich

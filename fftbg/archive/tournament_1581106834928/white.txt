Player: !White
Team: White Team
Palettes: White/Blue



IndecisiveNinja
Female
Aries
64
41
Chemist
Charge
Abandon
Dual Wield
Waterwalking

Blind Knife
Assassin Dagger
Leather Hat
Brigandine
Genji Gauntlet

Potion, Hi-Potion, X-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Charge+2, Charge+3, Charge+4, Charge+5, Charge+20



A Perfect Blue
Female
Scorpio
45
65
Oracle
Throw
Faith Up
Dual Wield
Move+3

Papyrus Codex
Madlemgen
Thief Hat
Power Sleeve
Defense Ring

Blind, Poison, Zombie, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Knife



Lord Tranquility
Male
Aries
72
69
Summoner
Battle Skill
Sunken State
Halve MP
Ignore Height

Thunder Rod

Green Beret
Linen Robe
Bracer

Moogle, Titan, Golem, Carbunkle, Odin, Zodiac
Shield Break, Weapon Break, Magic Break, Speed Break, Stasis Sword, Justice Sword



King Slime
Monster
Taurus
79
55
Ultima Demon








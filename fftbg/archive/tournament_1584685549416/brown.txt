Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Leakimiko
Male
Gemini
52
57
Lancer
Steal
Caution
Magic Attack UP
Teleport

Javelin
Crystal Shield
Leather Helmet
White Robe
Magic Gauntlet

Level Jump8, Vertical Jump6
Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Leg Aim



Gelwain
Female
Aquarius
75
47
Summoner
Battle Skill
Caution
Short Charge
Teleport

Wizard Rod

Golden Hairpin
Black Costume
Genji Gauntlet

Moogle, Ifrit, Titan, Carbunkle, Leviathan, Silf, Cyclops
Weapon Break



Red Celt
Female
Sagittarius
53
40
Dancer
Draw Out
Regenerator
Maintenance
Jump+3

Persia

Red Hood
Wizard Robe
Genji Gauntlet

Disillusion, Obsidian Blade, Dragon Pit
Heaven's Cloud, Muramasa



SeriousFaceFace
Female
Sagittarius
75
69
Ninja
Dance
HP Restore
Equip Armor
Waterwalking

Kunai
Ninja Edge
Triangle Hat
Chameleon Robe
108 Gems

Shuriken
Slow Dance, Disillusion, Dragon Pit

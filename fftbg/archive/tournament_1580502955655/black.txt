Player: !Black
Team: Black Team
Palettes: Black/Red



Maldoree
Female
Libra
70
47
Dancer
Punch Art
Sunken State
Equip Sword
Fly

Defender

Cachusha
Judo Outfit
Defense Armlet

Wiznaibus, Obsidian Blade, Void Storage
Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil



Mikeyoffbeat
Male
Gemini
71
71
Bard
White Magic
Parry
Sicken
Ignore Terrain

Bloody Strings

Twist Headband
Adaman Vest
Leather Mantle

Battle Song, Magic Song, Space Storage, Sky Demon
Cure 3, Raise 2, Reraise, Protect, Shell, Esuna



Grininda
Monster
Virgo
40
41
Dark Behemoth










BillyTheCop
Male
Aquarius
46
62
Chemist
Basic Skill
Regenerator
Equip Gun
Swim

Main Gauche

Golden Hairpin
Adaman Vest
Small Mantle

Potion, Hi-Ether, Soft, Phoenix Down
Throw Stone, Cheer Up

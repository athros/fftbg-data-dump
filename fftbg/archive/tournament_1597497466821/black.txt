Player: !Black
Team: Black Team
Palettes: Black/Red



Aldrammech
Female
Sagittarius
70
53
Monk
Basic Skill
Counter Tackle
Sicken
Levitate



Golden Hairpin
Leather Outfit
Cursed Ring

Pummel, Purification
Dash, Throw Stone, Heal, Yell, Wish



SephDarkheart
Female
Capricorn
73
76
Wizard
Dance
Counter Tackle
Equip Sword
Swim

Coral Sword

Headgear
Silk Robe
Jade Armlet

Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice, Ice 2, Ice 4
Slow Dance, Disillusion



Zeroroute
Male
Gemini
59
66
Mime

Distribute
Attack UP
Move+1



Triangle Hat
Leather Outfit
Red Shoes

Mimic




DesertWooder
Female
Sagittarius
60
79
Knight
Steal
Damage Split
Equip Sword
Lava Walking

Iron Sword
Mythril Shield
Leather Helmet
Genji Armor
Cursed Ring

Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Dark Sword
Gil Taking, Steal Accessory

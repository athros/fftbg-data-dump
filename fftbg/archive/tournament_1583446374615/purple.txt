Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Eldente
Female
Sagittarius
55
73
Calculator
Black Magic
MA Save
Magic Attack UP
Move+3

Ice Rod

Golden Hairpin
Brigandine
Spike Shoes

CT, Prime Number, 4, 3
Fire, Bolt, Bolt 3, Ice 3, Death, Flare



Tugboat1
Female
Aries
41
80
Monk
Time Magic
MA Save
Sicken
Waterwalking



Cachusha
Mythril Vest
Magic Ring

Wave Fist, Purification
Haste, Haste 2, Slow, Slow 2, Float, Stabilize Time, Meteor



GreatRedDragon
Male
Cancer
77
40
Time Mage
Battle Skill
Caution
Equip Bow
Jump+3

Lightning Bow

Flash Hat
Earth Clothes
Spike Shoes

Immobilize, Reflect, Demi, Stabilize Time
Head Break, Weapon Break, Magic Break, Mind Break, Justice Sword



ZergTwitch
Male
Aquarius
60
42
Ninja
Black Magic
Counter
Equip Sword
Move+1

Ice Brand
Nagrarock
Twist Headband
Brigandine
Sprint Shoes

Shuriken
Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Flare

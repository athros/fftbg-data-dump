Player: !Red
Team: Red Team
Palettes: Red/Brown



Matthewmuscles
Male
Virgo
58
70
Monk
Basic Skill
Critical Quick
Defend
Waterwalking



Green Beret
Mystic Vest
Wizard Mantle

Spin Fist, Earth Slash, Purification
Throw Stone, Heal, Tickle, Cheer Up, Wish



Ps36wii
Male
Scorpio
75
72
Calculator
Black Magic
PA Save
Doublehand
Move-HP Up

Battle Folio

Leather Hat
Black Robe
Spike Shoes

Height, 4, 3
Fire 2, Fire 3, Bolt 2, Bolt 3, Ice 2, Empower, Flare



JustDoomathon
Male
Cancer
77
65
Chemist
Draw Out
Arrow Guard
Secret Hunt
Move+2

Blind Knife

Twist Headband
Rubber Costume
Magic Ring

Potion, Hi-Potion, X-Potion, Eye Drop, Remedy
Murasame, Kiyomori



LDHaten
Male
Gemini
47
70
Priest
Yin Yang Magic
Counter Magic
Equip Axe
Teleport

Mace of Zeus

Flash Hat
Clothes
Small Mantle

Cure 4, Raise, Protect, Esuna, Holy
Life Drain, Silence Song, Dispel Magic, Sleep

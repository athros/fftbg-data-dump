Player: !Green
Team: Green Team
Palettes: Green/White



WireLord
Male
Capricorn
49
73
Thief
Black Magic
MA Save
Halve MP
Jump+3

Long Sword

Green Beret
Judo Outfit
108 Gems

Steal Heart, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Fire, Bolt 3, Ice 3, Empower, Frog



HaplessOne
Female
Virgo
44
46
Calculator
Yin Yang Magic
Parry
Magic Attack UP
Move+3

Ice Rod

Feather Hat
White Robe
Rubber Shoes

Height, Prime Number, 3
Blind, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze



Ko2q
Female
Cancer
45
51
Priest
Time Magic
Parry
Equip Gun
Jump+2

Bloody Strings

Red Hood
Chameleon Robe
Spike Shoes

Cure 3, Cure 4, Raise, Protect 2, Shell
Stop, Immobilize, Float, Demi, Demi 2, Meteor



Elijahrockers
Male
Serpentarius
75
44
Samurai
Basic Skill
MA Save
Equip Axe
Move-MP Up

Battle Axe

Cross Helmet
Crystal Mail
108 Gems

Kiyomori
Dash, Throw Stone, Heal, Cheer Up

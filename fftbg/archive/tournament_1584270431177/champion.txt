Player: !zChamp
Team: Champion Team
Palettes: Black/Red



RoachCarnival
Female
Leo
50
73
Ninja
Item
Dragon Spirit
Equip Gun
Waterwalking

Romanda Gun
Blaze Gun
Red Hood
Judo Outfit
Germinas Boots

Knife
Potion, Hi-Ether, Elixir, Holy Water, Phoenix Down



Dexef
Male
Capricorn
49
40
Ninja
Time Magic
Catch
Short Charge
Swim

Kunai
Main Gauche
Headgear
Rubber Costume
Red Shoes

Shuriken
Haste, Haste 2, Slow 2, Immobilize, Quick, Demi, Demi 2, Meteor



Sonata Of Senpai
Female
Taurus
62
54
Summoner
Talk Skill
Blade Grasp
Defend
Swim

Wizard Rod

Black Hood
Earth Clothes
Rubber Shoes

Shiva, Ifrit, Titan, Bahamut, Leviathan
Invitation, Persuade, Praise, Threaten, Solution, Insult, Refute, Rehabilitate



Treapvort
Male
Sagittarius
72
71
Monk
Steal
Abandon
Maintenance
Move-MP Up



Ribbon
Power Sleeve
Sprint Shoes

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive
Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Arm Aim, Leg Aim

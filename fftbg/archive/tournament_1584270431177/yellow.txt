Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Alc Trinity
Male
Libra
48
50
Time Mage
Throw
Counter Flood
Long Status
Levitate

Madlemgen

Flash Hat
Power Sleeve
Cursed Ring

Haste, Slow 2, Float, Demi 2, Stabilize Time
Shuriken, Bomb, Staff



LordSDB
Male
Cancer
53
60
Geomancer
Battle Skill
Counter Flood
Equip Axe
Ignore Height

Giant Axe
Bronze Shield
Black Hood
Linen Robe
Red Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Head Break, Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Justice Sword



Hzor
Male
Aquarius
63
40
Ninja
Elemental
Sunken State
Equip Shield
Move+2

Hidden Knife
Escutcheon
Triangle Hat
Judo Outfit
Feather Boots

Shuriken
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Zachara
Monster
Aquarius
66
61
Apanda








Final Bets: white - 10 bets for 12,540G (81.6%, x0.22); black - 6 bets for 2,820G (18.4%, x4.45)

white bets:
JumbocactuarX27: 7,765G (61.9%, 7,765G)
soren_of_tyto: 1,497G (11.9%, 1,497G)
Forkmore: 1,127G (9.0%, 1,127G)
sinnyil2: 951G (7.6%, 1,903G)
Draconis345: 500G (4.0%, 18,994G)
ar_tactic: 200G (1.6%, 78,733G)
datadrivenbot: 200G (1.6%, 65,231G)
AllInBot: 100G (0.8%, 100G)
Aldrammech: 100G (0.8%, 6,589G)
Thyrandaal: 100G (0.8%, 52,159G)

black bets:
BirbBrainsBot: 1,000G (35.5%, 26,999G)
E_Ballard: 744G (26.4%, 51,806G)
getthemoneyz: 476G (16.9%, 1,732,186G)
ArashiKurobara: 380G (13.5%, 5,246G)
Absalom_20: 120G (4.3%, 5,169G)
ravingsockmonkey: 100G (3.5%, 5,991G)

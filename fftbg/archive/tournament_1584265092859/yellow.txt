Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ranmilia
Female
Sagittarius
77
53
Summoner
Punch Art
Mana Shield
Equip Bow
Move+2

Poison Bow

Holy Miter
White Robe
Magic Gauntlet

Moogle, Shiva, Silf, Fairy
Spin Fist, Pummel, Purification, Chakra



Carledo
Male
Gemini
43
76
Archer
Time Magic
Meatbone Slash
Concentrate
Jump+1

Ultimus Bow

Black Hood
Black Costume
Germinas Boots

Charge+3, Charge+7, Charge+10
Stop, Demi 2, Stabilize Time



HaplessOne
Female
Pisces
52
74
Oracle
Black Magic
Parry
Doublehand
Levitate

Bestiary

Green Beret
Linen Robe
Elf Mantle

Blind, Poison, Life Drain, Pray Faith, Zombie, Blind Rage, Dispel Magic, Sleep, Dark Holy
Ice 2



Firesheath
Male
Pisces
49
59
Calculator
Yin Yang Magic
PA Save
Equip Polearm
Fly

Javelin

Feather Hat
Adaman Vest
Bracer

CT, Prime Number, 5, 4
Blind, Life Drain, Pray Faith, Zombie, Silence Song, Dispel Magic, Paralyze

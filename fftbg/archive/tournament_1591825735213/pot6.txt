Final Bets: black - 27 bets for 19,315G (63.0%, x0.59); brown - 13 bets for 11,336G (37.0%, x1.70)

black bets:
Evewho: 4,000G (20.7%, 18,257G)
OneHundredFists: 2,500G (12.9%, 26,010G)
Lydian_C: 2,400G (12.4%, 7,543G)
TheChainNerd: 1,038G (5.4%, 1,038G)
BlackFireUK: 1,000G (5.2%, 22,220G)
BirbBrainsBot: 1,000G (5.2%, 97,003G)
NicoSavoy: 1,000G (5.2%, 6,384G)
Oobs56: 697G (3.6%, 697G)
acid_flashback: 600G (3.1%, 3,834G)
Laserman1000: 600G (3.1%, 27,300G)
getthemoneyz: 524G (2.7%, 900,950G)
Cryptopsy70: 520G (2.7%, 9,832G)
WitchHunterIX: 500G (2.6%, 1,626G)
potgodtopdog: 444G (2.3%, 42,642G)
dogsandcatsand: 332G (1.7%, 332G)
twelfthrootoftwo: 320G (1.7%, 320G)
letdowncity: 300G (1.6%, 12,780G)
reddwind_: 265G (1.4%, 9,047G)
Creggers: 250G (1.3%, 4,679G)
Aldrammech: 225G (1.2%, 2,252G)
IphoneDarkness: 200G (1.0%, 1,144G)
Quadh0nk: 100G (0.5%, 820G)
E_Ballard: 100G (0.5%, 3,785G)
Digitalsocrates: 100G (0.5%, 4,264G)
datadrivenbot: 100G (0.5%, 35,469G)
nifboy: 100G (0.5%, 3,192G)
CorpusCav: 100G (0.5%, 2,274G)

brown bets:
DeathTaxesAndAnime: 5,456G (48.1%, 10,699G)
VolgraTheMoose: 2,001G (17.7%, 48,164G)
HASTERIOUS: 723G (6.4%, 723G)
HaateXIII: 712G (6.3%, 712G)
CapnChaos12: 500G (4.4%, 9,867G)
mpghappiness: 480G (4.2%, 480G)
benticore: 396G (3.5%, 396G)
CaptainGarlock: 300G (2.6%, 2,690G)
Anasetsuken: 212G (1.9%, 212G)
KasugaiRoastedPeas: 200G (1.8%, 6,588G)
AllInBot: 148G (1.3%, 148G)
porkchop5158: 108G (1.0%, 108G)
Firesheath: 100G (0.9%, 2,912G)

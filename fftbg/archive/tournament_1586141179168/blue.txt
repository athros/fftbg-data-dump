Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sairentozon7
Male
Taurus
64
76
Archer
Steal
Counter
Secret Hunt
Move+2

Romanda Gun
Flame Shield
Red Hood
Earth Clothes
Battle Boots

Charge+2, Charge+5
Steal Armor, Steal Weapon



Anethum
Female
Capricorn
53
73
Squire
Jump
Parry
Martial Arts
Teleport

Coral Sword
Buckler
Platinum Helmet
Mythril Vest
Dracula Mantle

Dash, Heal, Fury
Level Jump2, Vertical Jump8



JudgeSkrapz
Female
Aquarius
62
78
Dancer
Jump
Mana Shield
Concentrate
Teleport

Cashmere

Headgear
Silk Robe
Wizard Mantle

Wiznaibus, Polka Polka, Nameless Dance, Void Storage
Level Jump5, Vertical Jump7



DudeMonkey77
Female
Leo
70
79
Chemist
Steal
Brave Up
Equip Bow
Fly

Poison Bow

Red Hood
Mythril Vest
N-Kai Armlet

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Steal Armor, Steal Shield, Steal Accessory

Final Bets: red - 19 bets for 9,863G (36.4%, x1.74); blue - 26 bets for 17,201G (63.6%, x0.57)

red bets:
holyonline: 1,341G (13.6%, 1,341G)
Aldrammech: 1,000G (10.1%, 81,045G)
Wonser: 1,000G (10.1%, 2,826G)
BirbBrainsBot: 1,000G (10.1%, 75,204G)
Vampire_Killer: 750G (7.6%, 17,866G)
Chrysaors: 695G (7.0%, 17,386G)
aStatue: 600G (6.1%, 600G)
ANFz: 500G (5.1%, 18,068G)
killth3kid: 500G (5.1%, 28,760G)
ShintaroNayaka: 400G (4.1%, 9,937G)
Cryptopsy70: 380G (3.9%, 29,943G)
joewcarson: 364G (3.7%, 364G)
Chuckolator: 325G (3.3%, 22,938G)
FoxtrotNovemberCharlie: 264G (2.7%, 264G)
SQUiDSQUARKLIN: 244G (2.5%, 244G)
rjA0zcOQ96: 200G (2.0%, 36,916G)
Realitydown: 100G (1.0%, 6,673G)
gggamezzz: 100G (1.0%, 100G)
nifboy: 100G (1.0%, 5,505G)

blue bets:
Zeroroute: 2,394G (13.9%, 2,394G)
Mirrorclaw: 2,000G (11.6%, 11,201G)
KasugaiRoastedPeas: 1,641G (9.5%, 1,641G)
fenaen: 1,500G (8.7%, 66,705G)
JonnyCue: 1,200G (7.0%, 34,579G)
volgrathemoose: 1,001G (5.8%, 13,790G)
Hamborn: 1,000G (5.8%, 58,726G)
EnemyController: 1,000G (5.8%, 149,729G)
onetrickwolf: 1,000G (5.8%, 36,125G)
ko2q: 850G (4.9%, 850G)
Laserman1000: 784G (4.6%, 56,284G)
ZephyrTempest: 596G (3.5%, 3,974G)
Banzem: 447G (2.6%, 447G)
AmewTheFox: 250G (1.5%, 2,447G)
ungabunga_bot: 229G (1.3%, 276,275G)
Reductions: 205G (1.2%, 205G)
courtjesteroriginal: 200G (1.2%, 1,669G)
itsdigitalbro: 104G (0.6%, 104G)
FriendSquirrel: 100G (0.6%, 25,329G)
pLifer: 100G (0.6%, 2,042G)
Evewho: 100G (0.6%, 13,956G)
RunicMagus: 100G (0.6%, 51,794G)
CosmicTactician: 100G (0.6%, 5,739G)
frozentomato: 100G (0.6%, 1,391G)
datadrivenbot: 100G (0.6%, 5,102G)
Who_lio42: 100G (0.6%, 833G)

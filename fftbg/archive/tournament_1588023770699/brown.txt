Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Chuckolator
Female
Cancer
45
71
Samurai
Battle Skill
Damage Split
Beastmaster
Teleport

Heaven's Cloud

Cross Helmet
Light Robe
Spike Shoes

Asura, Koutetsu, Murasame, Kikuichimoji
Magic Break, Justice Sword, Explosion Sword



DeathTaxesAndAnime
Female
Libra
51
49
Samurai
Battle Skill
Parry
Concentrate
Retreat

Koutetsu Knife

Iron Helmet
Linen Robe
Spike Shoes

Koutetsu, Bizen Boat, Murasame, Muramasa
Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Justice Sword, Surging Sword



CosmicTactician
Female
Taurus
44
43
Monk
Item
Brave Up
Equip Bow
Jump+3

Silver Bow

Cachusha
Adaman Vest
Rubber Shoes

Pummel, Wave Fist, Chakra, Revive, Seal Evil
Hi-Potion, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Hales Bopp It
Male
Taurus
59
66
Thief
Punch Art
Critical Quick
Attack UP
Ignore Height

Mage Masher

Twist Headband
Wizard Outfit
Germinas Boots

Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Spin Fist, Earth Slash, Secret Fist, Chakra, Revive

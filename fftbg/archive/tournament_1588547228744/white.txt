Player: !White
Team: White Team
Palettes: White/Blue



Lijarkh
Male
Libra
46
48
Monk
Draw Out
Critical Quick
Dual Wield
Move-HP Up



Headgear
Judo Outfit
Magic Gauntlet

Purification, Revive, Seal Evil
Asura, Bizen Boat, Murasame, Kiyomori



Shakarak
Male
Capricorn
61
59
Samurai
White Magic
Parry
Doublehand
Move+3

Asura Knife

Circlet
Light Robe
108 Gems

Asura, Murasame, Masamune
Cure, Cure 4, Raise, Regen, Protect, Wall, Esuna



Baron Von Scrub
Female
Virgo
72
80
Dancer
Charge
HP Restore
Throw Item
Move+2

Cashmere

Flash Hat
Mystic Vest
Defense Ring

Slow Dance, Polka Polka, Disillusion, Obsidian Blade
Charge+2, Charge+3



Clippopo
Female
Scorpio
75
54
Lancer
Talk Skill
Faith Up
Short Charge
Fly

Javelin
Crystal Shield
Circlet
Genji Armor
Defense Ring

Level Jump3, Vertical Jump6
Invitation, Praise

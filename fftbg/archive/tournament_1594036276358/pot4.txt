Final Bets: purple - 6 bets for 4,070G (70.3%, x0.42); brown - 6 bets for 1,719G (29.7%, x2.37)

purple bets:
DustBirdEX: 1,204G (29.6%, 1,204G)
BirbBrainsBot: 1,000G (24.6%, 81,937G)
run_with_stone_GUNs: 618G (15.2%, 12,365G)
NIghtdew14: 500G (12.3%, 10,332G)
skillomono: 500G (12.3%, 13,835G)
getthemoneyz: 248G (6.1%, 1,204,115G)

brown bets:
E_Ballard: 476G (27.7%, 476G)
Belkra: 308G (17.9%, 308G)
xLilAsia: 287G (16.7%, 287G)
JethroThrul: 248G (14.4%, 994G)
g1nger4le: 200G (11.6%, 1,889G)
datadrivenbot: 200G (11.6%, 56,526G)

Player: !White
Team: White Team
Palettes: White/Blue



Watzel
Male
Scorpio
61
65
Calculator
White Magic
Regenerator
Equip Knife
Move+2

Wizard Rod

Feather Hat
Rubber Costume
108 Gems

Height, Prime Number, 4
Cure, Raise, Shell, Shell 2, Esuna



Rolodex
Female
Sagittarius
71
50
Mime

Speed Save
Equip Shield
Move+3


Bronze Shield
Green Beret
Light Robe
Salty Rage

Mimic




UA Futt
Male
Capricorn
43
75
Squire
Talk Skill
Hamedo
Long Status
Move-MP Up

Morning Star

Green Beret
Secret Clothes
Rubber Shoes

Accumulate, Heal, Yell
Refute, Rehabilitate



Tynootnoot
Female
Aquarius
54
80
Mime

Meatbone Slash
Defense UP
Lava Walking



Golden Hairpin
Mythril Vest
Small Mantle

Mimic


Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Chuckolator
Female
Leo
80
46
Oracle
Draw Out
Counter
Martial Arts
Move+3



Triangle Hat
Black Costume
Defense Ring

Blind, Life Drain, Petrify, Dark Holy
Asura, Murasame



Helpimabug
Male
Aries
48
56
Thief
White Magic
Faith Save
Equip Polearm
Move+1

Obelisk

Holy Miter
Wizard Outfit
Defense Ring

Steal Helmet, Steal Shield, Leg Aim
Raise, Raise 2, Wall, Esuna



Seaweed B
Female
Libra
77
58
Wizard
Steal
Abandon
Equip Sword
Levitate

Ragnarok

Triangle Hat
Black Costume
108 Gems

Fire, Fire 2, Bolt 2, Ice, Ice 4, Empower
Gil Taking, Steal Weapon, Steal Accessory



HaateXIII
Male
Aries
38
68
Geomancer
Throw
Auto Potion
Beastmaster
Move-MP Up

Slasher
Genji Shield
Holy Miter
Wizard Robe
Defense Ring

Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Bomb

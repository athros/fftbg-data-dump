Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Roqqqpsi
Male
Taurus
46
72
Summoner
Jump
MP Restore
Equip Sword
Ignore Height

Platinum Sword

Golden Hairpin
Judo Outfit
Wizard Mantle

Moogle, Titan, Silf, Cyclops
Level Jump5, Vertical Jump7



HaplessOne
Male
Leo
65
79
Knight
Punch Art
Catch
Short Charge
Move+3

Excalibur
Platinum Shield
Platinum Helmet
Gold Armor
Wizard Mantle

Head Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword
Spin Fist, Pummel, Earth Slash, Purification, Revive



TheNGUYENNER
Male
Leo
60
58
Calculator
White Magic
Absorb Used MP
Equip Armor
Jump+3

Bestiary

Holy Miter
Leather Armor
Germinas Boots

CT, Height, Prime Number, 4, 3
Regen, Protect, Protect 2, Shell, Shell 2, Esuna, Holy



Foofermoofer
Male
Gemini
69
47
Monk
Sing
Arrow Guard
Equip Armor
Move+3



Twist Headband
Robe of Lords
Genji Gauntlet

Wave Fist, Purification, Chakra, Revive
Angel Song, Cheer Song, Magic Song, Sky Demon, Hydra Pit

Player: !Black
Team: Black Team
Palettes: Black/Red



Lyner87
Female
Virgo
70
71
Oracle
Basic Skill
Critical Quick
Secret Hunt
Retreat

Iron Fan

Barette
Light Robe
Spike Shoes

Zombie, Blind Rage, Confusion Song, Dispel Magic
Dash, Throw Stone, Heal, Tickle, Yell



Nifboy
Female
Pisces
76
53
Time Mage
Battle Skill
HP Restore
Magic Attack UP
Jump+2

Healing Staff

Holy Miter
Black Costume
Elf Mantle

Haste, Haste 2, Slow 2, Quick, Demi 2
Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Explosion Sword



ArchKnightX
Male
Pisces
56
57
Bard
White Magic
Meatbone Slash
Equip Knife
Move+3

Mage Masher

Black Hood
Bronze Armor
Elf Mantle

Battle Song, Sky Demon, Hydra Pit
Cure 3, Raise 2, Reraise, Regen, Protect, Protect 2, Shell, Esuna



Grandlanzer
Male
Gemini
41
53
Ninja
Talk Skill
HP Restore
Equip Axe
Move+1

Flail
Morning Star
Feather Hat
Wizard Outfit
Defense Ring

Shuriken, Sword, Dictionary
Solution, Death Sentence, Insult, Mimic Daravon, Refute

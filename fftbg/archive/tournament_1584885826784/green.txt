Player: !Green
Team: Green Team
Palettes: Green/White



Winnosh
Female
Cancer
68
46
Ninja
Punch Art
Sunken State
Attack UP
Lava Walking

Mythril Knife
Sasuke Knife
Black Hood
Chain Vest
Defense Ring

Shuriken, Ninja Sword
Spin Fist, Pummel, Purification, Chakra, Revive



BenYuPoker
Male
Taurus
67
76
Ninja
Jump
Regenerator
Equip Sword
Waterbreathing

Defender
Save the Queen
Feather Hat
Secret Clothes
Feather Mantle

Bomb, Ninja Sword, Stick
Level Jump4, Vertical Jump6



Rastanar
Male
Serpentarius
67
46
Monk
Black Magic
Catch
Defense UP
Fly



Green Beret
Leather Outfit
Leather Mantle

Spin Fist, Wave Fist, Purification, Chakra, Revive
Fire 3, Bolt 2, Ice 3, Ice 4



JustSuperish
Female
Pisces
54
40
Ninja
Jump
Dragon Spirit
Magic Attack UP
Jump+2

Mage Masher
Dagger
Black Hood
Secret Clothes
Small Mantle

Shuriken, Bomb, Knife, Dictionary
Level Jump2, Vertical Jump8

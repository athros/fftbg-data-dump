Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lavatis
Male
Libra
81
69
Lancer
Throw
PA Save
Sicken
Waterwalking

Spear
Aegis Shield
Gold Helmet
Wizard Robe
Angel Ring

Level Jump8, Vertical Jump2
Shuriken, Ninja Sword



Shalloween
Male
Scorpio
76
57
Lancer
Draw Out
Catch
Attack UP
Jump+1

Octagon Rod
Round Shield
Bronze Helmet
Carabini Mail
Small Mantle

Level Jump4, Vertical Jump6
Asura, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



JonnyCue
Female
Pisces
54
74
Ninja
White Magic
Counter
Equip Armor
Ignore Terrain

Ninja Edge
Kunai
Golden Hairpin
Linen Robe
Red Shoes

Shuriken, Bomb, Ninja Sword
Cure 3, Raise 2, Reraise, Regen, Protect, Shell, Esuna



Killth3kid
Female
Capricorn
56
65
Samurai
Steal
Regenerator
Secret Hunt
Move+1

Gungnir

Cross Helmet
Genji Armor
Angel Ring

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji
Steal Shield, Arm Aim

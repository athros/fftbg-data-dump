Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rislyeu
Female
Gemini
63
63
Priest
Black Magic
Brave Up
Defense UP
Ignore Height

Gold Staff

Headgear
Rubber Costume
Battle Boots

Cure 2, Cure 3, Raise, Raise 2, Protect 2, Shell, Shell 2, Wall, Esuna
Fire, Fire 2, Bolt 3, Bolt 4, Ice 3, Frog



UmaiJam
Female
Pisces
70
57
Ninja
Steal
Caution
Equip Gun
Waterbreathing

Stone Gun
Romanda Gun
Holy Miter
Earth Clothes
Wizard Mantle

Shuriken, Bomb, Spear
Gil Taking, Steal Shield, Steal Accessory



Killth3kid
Male
Scorpio
69
72
Archer
Punch Art
Meatbone Slash
Dual Wield
Jump+1

Romanda Gun
Blast Gun
Headgear
Judo Outfit
Feather Boots

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Earth Slash, Purification, Revive



Lionhermit
Male
Taurus
43
71
Chemist
Black Magic
Mana Shield
Short Charge
Waterbreathing

Dagger

Feather Hat
Judo Outfit
Leather Mantle

X-Potion, Ether, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Fire 3, Bolt 3, Bolt 4, Ice 3

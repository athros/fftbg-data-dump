Player: !Red
Team: Red Team
Palettes: Red/Brown



Lawnboxer
Monster
Sagittarius
55
76
Floating Eye










ExecutedGiraffe
Male
Cancer
49
50
Geomancer
Summon Magic
Regenerator
Maintenance
Ignore Terrain

Sleep Sword
Genji Shield
Triangle Hat
Linen Robe
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Ifrit, Carbunkle, Salamander



Dictatorhowells
Male
Aries
76
73
Geomancer
Item
Counter Tackle
Beastmaster
Swim

Battle Axe
Kaiser Plate
Barette
Linen Robe
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Hi-Potion, Ether, Antidote, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



CosmicTactician
Female
Virgo
42
62
Oracle
Item
Dragon Spirit
Throw Item
Ignore Terrain

Gokuu Rod

Twist Headband
Mythril Vest
Defense Ring

Poison, Spell Absorb, Zombie, Foxbird, Confusion Song, Dispel Magic, Petrify, Dark Holy
Potion, Hi-Ether, Elixir, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down

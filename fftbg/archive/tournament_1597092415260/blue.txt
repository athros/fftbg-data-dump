Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Northkoreancleetus
Male
Capricorn
47
50
Archer
White Magic
Counter Magic
Short Charge
Move+1

Long Bow

Triangle Hat
Earth Clothes
Cursed Ring

Charge+1, Charge+3, Charge+7
Cure, Cure 2, Cure 3, Cure 4, Protect, Esuna



Maldoree
Male
Taurus
40
66
Priest
Yin Yang Magic
Earplug
Defense UP
Move-HP Up

White Staff

Twist Headband
Wizard Robe
Jade Armlet

Cure, Cure 2, Raise, Protect 2, Shell, Esuna
Poison, Spell Absorb, Doubt Faith, Zombie, Dispel Magic, Paralyze, Petrify



DeathTaxesAndAnime
Female
Cancer
75
58
Wizard
Math Skill
Sunken State
Defense UP
Teleport

Flame Rod

Leather Hat
Linen Robe
Power Wrist

Fire 2, Fire 4, Bolt, Bolt 2, Ice 3, Ice 4, Frog, Flare
CT, Prime Number, 5, 4, 3



Moonliquor
Monster
Taurus
77
56
Minotaur








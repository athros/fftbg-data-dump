Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ScurvyMitch
Male
Taurus
47
71
Knight
Item
Meatbone Slash
Dual Wield
Move-HP Up

Defender
Ancient Sword
Circlet
Light Robe
Leather Mantle

Shield Break, Weapon Break, Power Break
Potion, Eye Drop, Maiden's Kiss



Valentine009
Monster
Taurus
63
54
Mindflayer










OneHundredFists
Female
Aries
47
59
Mediator
Summon Magic
HP Restore
Dual Wield
Swim

Mythril Gun
Blast Gun
Barette
Leather Outfit
Salty Rage

Preach, Death Sentence, Insult, Negotiate
Moogle, Shiva, Bahamut, Leviathan, Silf, Cyclops



O Heyno
Female
Aries
59
60
Knight
Throw
Speed Save
Equip Axe
Move-HP Up

Mace of Zeus
Diamond Shield
Cross Helmet
Gold Armor
Bracer

Armor Break, Shield Break, Magic Break, Speed Break, Mind Break
Bomb, Stick

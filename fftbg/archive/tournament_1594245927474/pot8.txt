Final Bets: brown - 9 bets for 5,996G (45.4%, x1.20); champion - 9 bets for 7,220G (54.6%, x0.83)

brown bets:
VolgraTheMoose: 2,001G (33.4%, 9,239G)
Aldrammech: 1,000G (16.7%, 10,948G)
roofiepops: 888G (14.8%, 2,854G)
Laserman1000: 700G (11.7%, 28,200G)
roqqqpsi: 470G (7.8%, 47,057G)
twelfthrootoftwo: 436G (7.3%, 436G)
gorgewall: 201G (3.4%, 14,963G)
datadrivenbot: 200G (3.3%, 60,511G)
DeathTaxesAndAnime: 100G (1.7%, 1,524G)

champion bets:
NicoSavoy: 2,000G (27.7%, 149,838G)
ACSpree: 1,200G (16.6%, 9,670G)
Evewho: 1,000G (13.9%, 31,378G)
BirbBrainsBot: 1,000G (13.9%, 155,146G)
getthemoneyz: 994G (13.8%, 1,227,864G)
letdowncity: 376G (5.2%, 5,470G)
Shalloween: 300G (4.2%, 59,412G)
benticore: 250G (3.5%, 4,042G)
TasisSai: 100G (1.4%, 1,196G)

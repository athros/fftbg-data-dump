Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Brokenknight201
Female
Scorpio
62
73
Mime

Hamedo
Equip Armor
Jump+1



Iron Helmet
Wizard Robe
Bracer

Mimic




SephDarkheart
Female
Aquarius
67
46
Chemist
Throw
Catch
Long Status
Move+2

Panther Bag

Leather Hat
Black Costume
Reflect Ring

Potion, Hi-Potion, X-Potion, Hi-Ether, Phoenix Down
Bomb



Superdevon1
Female
Gemini
49
53
Oracle
White Magic
Critical Quick
Secret Hunt
Move+2

Iron Fan

Feather Hat
Silk Robe
Battle Boots

Blind, Silence Song, Dispel Magic, Dark Holy
Cure 4, Raise, Reraise, Shell 2, Esuna



Nhammen
Male
Pisces
50
54
Ninja
Punch Art
Hamedo
Short Charge
Teleport

Morning Star
Ninja Edge
Red Hood
Secret Clothes
Rubber Shoes

Bomb
Purification, Revive, Seal Evil

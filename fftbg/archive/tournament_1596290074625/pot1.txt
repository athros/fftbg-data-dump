Final Bets: red - 11 bets for 10,039G (67.4%, x0.48); blue - 6 bets for 4,848G (32.6%, x2.07)

red bets:
randgridr: 2,699G (26.9%, 13,498G)
NIghtdew14: 2,644G (26.3%, 5,289G)
Draconis345: 1,000G (10.0%, 43,029G)
Aldrammech: 1,000G (10.0%, 59,832G)
VolgraTheMoose: 880G (8.8%, 880G)
douchetron: 468G (4.7%, 468G)
Miku_Shikhu: 448G (4.5%, 448G)
maximumcrit: 300G (3.0%, 7,981G)
AllInBot: 200G (2.0%, 200G)
WillFitzgerald: 200G (2.0%, 8,075G)
datadrivenbot: 200G (2.0%, 44,557G)

blue bets:
Nizaha: 1,655G (34.1%, 3,311G)
RaIshtar: 1,051G (21.7%, 1,051G)
BirbBrainsBot: 1,000G (20.6%, 111,145G)
Shalloween: 500G (10.3%, 14,336G)
getthemoneyz: 382G (7.9%, 1,454,179G)
Chambs12: 260G (5.4%, 260G)

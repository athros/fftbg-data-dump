Final Bets: green - 14 bets for 15,451G (62.3%, x0.60); champion - 18 bets for 9,336G (37.7%, x1.65)

green bets:
dogsandcatsand: 5,000G (32.4%, 7,232G)
VolgraTheMoose: 3,435G (22.2%, 6,870G)
maximumcrit: 3,000G (19.4%, 7,811G)
douchetron: 640G (4.1%, 640G)
killth3kid: 584G (3.8%, 9,604G)
Zagorsek: 559G (3.6%, 1,119G)
Smashy: 500G (3.2%, 7,091G)
letdowncity: 476G (3.1%, 14,388G)
RageImmortaI: 316G (2.0%, 1,891G)
roqqqpsi: 241G (1.6%, 322G)
AllInBot: 200G (1.3%, 200G)
Miku_Shikhu: 200G (1.3%, 971G)
datadrivenbot: 200G (1.3%, 43,982G)
Aldrammech: 100G (0.6%, 53,834G)

champion bets:
Thyrandaal: 2,000G (21.4%, 132,486G)
Shalloween: 1,200G (12.9%, 13,817G)
BirbBrainsBot: 1,000G (10.7%, 114,418G)
Draconis345: 1,000G (10.7%, 43,492G)
RaIshtar: 1,000G (10.7%, 4,016G)
benticore: 500G (5.4%, 44,664G)
reinoe: 500G (5.4%, 72,850G)
Artea_: 332G (3.6%, 332G)
Chambs12: 264G (2.8%, 1,717G)
Zathra: 250G (2.7%, 709G)
nhammen: 250G (2.7%, 27,756G)
getthemoneyz: 236G (2.5%, 1,457,716G)
ArlanKels: 200G (2.1%, 923G)
WillFitzgerald: 200G (2.1%, 8,257G)
popotinha1889: 104G (1.1%, 104G)
GoatShow2: 100G (1.1%, 630G)
Bongomon7: 100G (1.1%, 6,546G)
Lali_Lulelo: 100G (1.1%, 24,572G)

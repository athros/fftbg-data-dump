Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Lali Lulelo
Male
Capricorn
77
46
Monk
Item
Dragon Spirit
Throw Item
Waterbreathing

Star Bag

Headgear
Earth Clothes
Magic Ring

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down



Lokenwow
Female
Aquarius
79
46
Geomancer
Basic Skill
Mana Shield
Maintenance
Move+3

Bizen Boat
Crystal Shield
Thief Hat
Adaman Vest
Sprint Shoes

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Heal, Wish



Bongomon7
Male
Sagittarius
49
80
Squire
Throw
Damage Split
Martial Arts
Waterwalking


Buckler
Mythril Helmet
Platinum Armor
Feather Mantle

Yell, Scream
Hammer



Galkife
Female
Virgo
44
69
Priest
Charge
Distribute
Equip Shield
Retreat

Sage Staff
Round Shield
Leather Hat
Brigandine
Dracula Mantle

Cure, Cure 2, Cure 3, Raise, Protect 2, Shell, Wall, Esuna
Charge+4, Charge+7

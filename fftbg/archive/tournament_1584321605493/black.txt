Player: !Black
Team: Black Team
Palettes: Black/Red



Numbersborne
Male
Sagittarius
68
70
Squire
Elemental
Counter
Defense UP
Move+2

Ice Brand
Diamond Shield
Twist Headband
Clothes
Power Wrist

Accumulate, Heal, Tickle, Yell, Wish
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard, Lava Ball



StealthModeLocke
Male
Scorpio
63
41
Mediator
Item
Meatbone Slash
Long Status
Swim

Blind Knife

Twist Headband
Chameleon Robe
Defense Ring

Invitation, Praise, Preach, Death Sentence, Refute, Rehabilitate
Ether, Antidote, Echo Grass, Maiden's Kiss, Soft



Spartan Paladin
Male
Taurus
74
70
Knight
Throw
Parry
Halve MP
Ignore Terrain

Nagrarock
Escutcheon
Barbuta
Gold Armor
Angel Ring

Shield Break, Speed Break, Power Break, Surging Sword
Shuriken, Ninja Sword, Spear



Oogthecaveman
Female
Taurus
45
44
Squire
Draw Out
Critical Quick
Secret Hunt
Levitate

Assassin Dagger
Genji Shield
Twist Headband
Linen Cuirass
Defense Armlet

Heal, Cheer Up, Fury, Wish
Asura, Bizen Boat, Murasame

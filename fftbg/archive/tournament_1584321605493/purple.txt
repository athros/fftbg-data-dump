Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Maeveen
Female
Capricorn
79
64
Calculator
Black Magic
Speed Save
Equip Bow
Levitate

Night Killer

Headgear
Leather Outfit
Leather Mantle

CT, Height, Prime Number, 5, 3
Bolt 2, Bolt 4, Ice 2, Ice 4, Frog, Death



PrudishDuckling
Monster
Sagittarius
77
44
Minotaur










Edgehead62888
Monster
Aquarius
42
60
Mindflayer










MrUbiq
Male
Scorpio
78
51
Mediator
Item
Auto Potion
Throw Item
Move+2

Mage Masher

Twist Headband
Earth Clothes
Magic Gauntlet

Persuade, Insult, Negotiate, Refute, Rehabilitate
Potion, Ether, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down

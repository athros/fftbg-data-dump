Player: !Green
Team: Green Team
Palettes: Green/White



AM3P Batman
Female
Sagittarius
71
79
Squire
White Magic
PA Save
Magic Attack UP
Move-MP Up

Zorlin Shape
Diamond Shield
Triangle Hat
Secret Clothes
Feather Boots

Accumulate, Throw Stone, Heal
Cure 4, Raise, Raise 2, Reraise, Protect, Protect 2, Esuna



Bad1dea
Female
Virgo
52
76
Thief
Elemental
Mana Shield
Short Charge
Waterwalking

Assassin Dagger

Flash Hat
Power Sleeve
Leather Mantle

Gil Taking, Steal Helmet, Steal Shield
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Toka222
Male
Capricorn
70
75
Knight
Steal
Critical Quick
Maintenance
Move+3

Excalibur

Genji Helmet
Mythril Armor
Germinas Boots

Head Break, Stasis Sword
Gil Taking, Steal Helmet, Steal Accessory, Steal Status, Leg Aim



Xeroeffekt
Female
Libra
51
48
Geomancer
Throw
PA Save
Equip Bow
Move+2

Cute Bag
Genji Shield
Green Beret
Black Costume
Defense Armlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shuriken

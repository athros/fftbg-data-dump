Final Bets: blue - 8 bets for 2,910G (31.7%, x2.16); purple - 11 bets for 6,284G (68.3%, x0.46)

blue bets:
Vyroxe: 1,500G (51.5%, 8,981G)
Goust18: 500G (17.2%, 6,578G)
Tithonus: 200G (6.9%, 14,515G)
JustSuperish: 200G (6.9%, 1,607G)
mayormcfunbags: 200G (6.9%, 4,255G)
DaveStrider55: 200G (6.9%, 2,221G)
Lythe_Caraker: 100G (3.4%, 50,344G)
Firesheath: 10G (0.3%, 2,683G)

purple bets:
HaplessOne: 1,500G (23.9%, 143,717G)
sinnyil2: 1,200G (19.1%, 15,557G)
BirbBrainsBot: 1,000G (15.9%, 70,056G)
getthemoneyz: 564G (9.0%, 472,340G)
datadrivenbot: 521G (8.3%, 25,456G)
ColetteMSLP: 500G (8.0%, 18,444G)
ungabunga_bot: 499G (7.9%, 113,588G)
KasugaiRoastedPeas: 200G (3.2%, 18,458G)
AllInBot: 100G (1.6%, 100G)
RughSontos: 100G (1.6%, 3,000G)
ko2q: 100G (1.6%, 6,224G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KasugaiRoastedPeas
Male
Taurus
47
43
Lancer
White Magic
Hamedo
Doublehand
Retreat

Obelisk

Crystal Helmet
Wizard Robe
Feather Mantle

Level Jump8, Vertical Jump8
Cure 4, Raise, Regen, Protect, Wall, Esuna, Holy



RughSontos
Male
Aries
48
41
Ninja
Punch Art
Arrow Guard
Attack UP
Fly

Flail
Assassin Dagger
Black Hood
Mystic Vest
Power Wrist

Shuriken, Bomb, Hammer, Dictionary
Revive



Mirapoix
Male
Libra
68
57
Lancer
Steal
Meatbone Slash
Doublehand
Retreat

Spear

Crystal Helmet
Black Robe
Germinas Boots

Level Jump8, Vertical Jump2
Steal Heart, Steal Shield, Steal Weapon, Arm Aim, Leg Aim



Eloisa
Female
Virgo
58
45
Dancer
White Magic
Distribute
Equip Sword
Waterwalking

Blood Sword

Red Hood
White Robe
Defense Armlet

Wiznaibus, Disillusion, Obsidian Blade, Nether Demon, Dragon Pit
Cure 3, Cure 4, Raise, Shell 2

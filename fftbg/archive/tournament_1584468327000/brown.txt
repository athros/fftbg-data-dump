Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rogueain
Female
Leo
50
52
Squire
Throw
PA Save
Secret Hunt
Ignore Height

Gastrafitis
Crystal Shield
Genji Helmet
Brigandine
Magic Ring

Accumulate, Throw Stone, Heal, Cheer Up
Shuriken



Bahamut64
Female
Aquarius
70
49
Ninja
Punch Art
Damage Split
Concentrate
Ignore Terrain

Ninja Edge
Short Edge
Headgear
Clothes
Spike Shoes

Shuriken, Bomb
Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil



Laserman1000
Male
Libra
51
78
Calculator
White Magic
Counter Magic
Equip Polearm
Ignore Height

Gokuu Rod

Twist Headband
Brigandine
Jade Armlet

CT, 5, 3
Cure, Cure 3, Cure 4, Reraise, Protect, Protect 2, Shell 2, Wall, Esuna, Holy



Jigglefluffenstuff
Male
Scorpio
51
48
Summoner
Elemental
Critical Quick
Short Charge
Move-MP Up

Poison Rod

Triangle Hat
Adaman Vest
Magic Ring

Carbunkle, Bahamut, Fairy
Pitfall, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball

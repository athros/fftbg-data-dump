Final Bets: purple - 9 bets for 3,805G (57.9%, x0.73); brown - 8 bets for 2,769G (42.1%, x1.37)

purple bets:
Breakdown777: 1,002G (26.3%, 20,604G)
nifboy: 937G (24.6%, 937G)
TheChainNerd: 500G (13.1%, 3,795G)
metagameface: 500G (13.1%, 16,403G)
woohoo_brokeback: 420G (11.0%, 10,255G)
Anasetsuken: 146G (3.8%, 146G)
Belkra: 100G (2.6%, 1,298G)
datadrivenbot: 100G (2.6%, 36,907G)
Quadh0nk: 100G (2.6%, 715G)

brown bets:
ApplesNP: 797G (28.8%, 797G)
BirbBrainsBot: 470G (17.0%, 31,804G)
SaintOmerville: 386G (13.9%, 386G)
getthemoneyz: 366G (13.2%, 872,630G)
pplvee1: 350G (12.6%, 6,039G)
prince_rogers_nelson_: 200G (7.2%, 3,043G)
AllInBot: 100G (3.6%, 100G)
GiggleStik: 100G (3.6%, 6,366G)

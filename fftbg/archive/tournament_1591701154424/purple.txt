Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Quadh0nk
Female
Aries
50
42
Time Mage
Draw Out
Counter
Equip Gun
Move+3

Papyrus Codex

Twist Headband
Clothes
Wizard Mantle

Haste, Slow, Slow 2, Immobilize, Reflect, Demi, Stabilize Time, Galaxy Stop
Asura, Koutetsu



Thenextlantern
Monster
Capricorn
53
54
Dragon










Serperemagus
Male
Leo
59
49
Squire
Time Magic
Distribute
Equip Sword
Fly

Koutetsu Knife
Mythril Shield
Feather Hat
Plate Mail
Battle Boots

Heal, Cheer Up
Haste 2, Slow, Demi 2



ThePineappleSalesman
Female
Scorpio
55
62
Oracle
Summon Magic
MP Restore
Long Status
Move+2

Ivory Rod

Thief Hat
Black Robe
Spike Shoes

Zombie, Foxbird, Dispel Magic
Shiva, Ramuh, Carbunkle, Silf

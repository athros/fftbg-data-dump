Final Bets: red - 23 bets for 16,848G (57.1%, x0.75); green - 25 bets for 12,650G (42.9%, x1.33)

red bets:
HaplessOne: 4,444G (26.4%, 104,969G)
volgrathemoose: 2,628G (15.6%, 5,257G)
CCRUNNER149UWP: 2,212G (13.1%, 2,212G)
Estan_AD: 1,117G (6.6%, 2,234G)
twelfthrootoftwo: 1,000G (5.9%, 12,206G)
gooseyourself: 703G (4.2%, 703G)
DaveStrider55: 540G (3.2%, 540G)
Breakdown777: 500G (3.0%, 3,973G)
reinoe: 500G (3.0%, 8,125G)
Strifu: 384G (2.3%, 384G)
helpimabug: 360G (2.1%, 360G)
Kyune: 348G (2.1%, 348G)
ZZ_Yoshi: 320G (1.9%, 31,717G)
joewcarson: 300G (1.8%, 8,239G)
TheUnforgivenRage: 300G (1.8%, 300G)
Kyrious: 297G (1.8%, 297G)
Lolprinze: 228G (1.4%, 228G)
AsceticGamer: 200G (1.2%, 200G)
O_Heyno: 150G (0.9%, 959G)
Vultuous: 100G (0.6%, 2,267G)
datadrivenbot: 100G (0.6%, 19,077G)
getthemoneyz: 66G (0.4%, 679,901G)
dude1ko: 51G (0.3%, 100G)

green bets:
OneHundredFists: 1,756G (13.9%, 3,444G)
SkylerBunny: 1,500G (11.9%, 59,071G)
BirbBrainsBot: 1,000G (7.9%, 105,683G)
HawkSick: 750G (5.9%, 6,113G)
SamaelMerihem: 666G (5.3%, 1,074G)
ungabunga_bot: 663G (5.2%, 508,418G)
Firesheath: 568G (4.5%, 568G)
killth3kid: 555G (4.4%, 8,593G)
Drusic: 500G (4.0%, 6,042G)
GladiatorLupe: 500G (4.0%, 7,304G)
Evewho: 500G (4.0%, 3,563G)
JLinkletter: 500G (4.0%, 4,815G)
Arcblazer23: 500G (4.0%, 6,903G)
neocarbuncle: 386G (3.1%, 386G)
ApplesauceBoss: 372G (2.9%, 372G)
CorpusCav: 330G (2.6%, 4,113G)
argentbast: 300G (2.4%, 5,022G)
interalia: 204G (1.6%, 400G)
otakutaylor: 200G (1.6%, 5,808G)
Palladigm: 200G (1.6%, 4,459G)
Moshyhero: 200G (1.6%, 200G)
LionelParks: 200G (1.6%, 1,194G)
StealthModeLocke: 100G (0.8%, 40,788G)
maakur_: 100G (0.8%, 2,395G)
Killing_Moon: 100G (0.8%, 956G)

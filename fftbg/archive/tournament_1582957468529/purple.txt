Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sodeko
Female
Scorpio
65
46
Oracle
Summon Magic
Arrow Guard
Martial Arts
Levitate

Musk Rod

Red Hood
Clothes
Sprint Shoes

Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Sleep, Petrify
Moogle, Ifrit, Titan, Golem, Lich



Watzel
Male
Aquarius
77
68
Lancer
Throw
MP Restore
Beastmaster
Retreat

Obelisk
Mythril Shield
Bronze Helmet
Reflect Mail
Spike Shoes

Level Jump2, Vertical Jump2
Shuriken, Staff, Dictionary



Elelor
Female
Scorpio
76
42
Knight
Jump
Critical Quick
Equip Polearm
Ignore Terrain

Iron Fan
Buckler
Genji Helmet
Black Robe
Magic Ring

Head Break, Magic Break, Mind Break, Justice Sword
Level Jump4, Vertical Jump7



Hyvi
Male
Virgo
48
47
Bard
Punch Art
Speed Save
Beastmaster
Move+3

Fairy Harp

Headgear
Chain Mail
Jade Armlet

Life Song, Battle Song, Magic Song, Sky Demon
Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil

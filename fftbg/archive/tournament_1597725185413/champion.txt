Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Just Here2
Female
Pisces
40
64
Knight
Dance
Counter Tackle
Equip Knife
Jump+2

Hidden Knife
Bronze Shield
Genji Helmet
Chameleon Robe
Bracer

Armor Break, Weapon Break, Magic Break, Mind Break, Justice Sword
Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Obsidian Blade



Killth3kid
Female
Libra
64
69
Time Mage
Summon Magic
Caution
Short Charge
Teleport

Iron Fan

Ribbon
Light Robe
Rubber Shoes

Haste 2, Slow 2, Demi 2, Galaxy Stop
Moogle, Shiva, Titan, Carbunkle, Bahamut, Leviathan, Fairy



CosmicTactician
Female
Scorpio
56
45
Mediator
Time Magic
HP Restore
Dual Wield
Teleport

Battle Folio
Papyrus Codex
Thief Hat
Clothes
Battle Boots

Invitation, Preach, Solution, Death Sentence
Haste 2, Slow, Quick, Demi



Helpimabug
Male
Cancer
53
47
Lancer
Time Magic
Earplug
Long Status
Jump+3

Octagon Rod
Flame Shield
Iron Helmet
Leather Armor
Elf Mantle

Level Jump8, Vertical Jump6
Haste, Haste 2, Stop, Float, Quick, Stabilize Time, Meteor

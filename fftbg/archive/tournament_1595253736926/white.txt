Player: !White
Team: White Team
Palettes: White/Blue



Pplvee1
Female
Aries
67
74
Chemist
Time Magic
Counter
Dual Wield
Move-HP Up

Star Bag
Star Bag
Red Hood
Earth Clothes
Reflect Ring

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Phoenix Down
Haste, Slow, Slow 2, Stop, Float, Demi, Meteor



RaIshtar
Female
Aquarius
68
43
Samurai
Summon Magic
Damage Split
Sicken
Levitate

Bizen Boat

Cross Helmet
Leather Armor
Wizard Mantle

Koutetsu, Murasame
Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Salamander, Fairy, Lich



Zagorsek
Female
Capricorn
68
69
Geomancer
Yin Yang Magic
Catch
Equip Bow
Fly

Windslash Bow

Flash Hat
Brigandine
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Blind, Spell Absorb, Zombie, Paralyze



Actbg09
Male
Cancer
47
53
Geomancer
White Magic
Meatbone Slash
Short Status
Move-HP Up

Asura Knife
Mythril Shield
Black Hood
Wizard Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Cure, Reraise, Regen, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Dynasti
Male
Gemini
65
76
Ninja
Steal
Faith Up
Equip Shield
Waterbreathing

Flame Whip
Bronze Shield
Golden Hairpin
Adaman Vest
Magic Gauntlet

Shuriken, Knife
Steal Accessory, Arm Aim, Leg Aim



Rogueain
Male
Taurus
66
68
Squire
Charge
PA Save
Equip Axe
Jump+1

Giant Axe
Buckler
Red Hood
Mythril Armor
Bracer

Accumulate, Throw Stone, Heal
Charge+1, Charge+3, Charge+7



LordPaimon
Monster
Aquarius
64
70
Steel Giant










Evdoggity
Female
Sagittarius
77
58
Samurai
Talk Skill
Regenerator
Magic Defense UP
Swim

Bizen Boat

Circlet
Linen Cuirass
Battle Boots

Asura, Murasame, Kiyomori, Muramasa
Praise, Preach, Insult, Negotiate, Refute

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Lodrak
Female
Gemini
56
62
Archer
Item
Meatbone Slash
Maintenance
Levitate

Mythril Gun
Ice Shield
Platinum Helmet
Adaman Vest
Magic Ring

Charge+1, Charge+3, Charge+7, Charge+10
Potion, Hi-Potion, Antidote, Remedy, Phoenix Down



DustBirdEX
Male
Pisces
65
65
Ninja
Steal
Counter Magic
Equip Bow
Swim

Hunting Bow
Hunting Bow
Flash Hat
Brigandine
Elf Mantle

Shuriken, Bomb, Staff
Steal Heart, Steal Shield, Steal Weapon, Steal Accessory



Ko2q
Male
Scorpio
50
72
Knight
Steal
Speed Save
Concentrate
Move+2

Ragnarok
Buckler
Diamond Helmet
Chain Mail
Germinas Boots

Shield Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Leg Aim



Lali Lulelo
Male
Scorpio
79
67
Mediator
Sing
Caution
Short Charge
Ignore Terrain

Main Gauche

Twist Headband
Power Sleeve
Spike Shoes

Praise, Mimic Daravon, Refute, Rehabilitate
Angel Song, Life Song, Cheer Song, Battle Song, Nameless Song, Last Song, Diamond Blade, Space Storage

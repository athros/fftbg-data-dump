Player: !Black
Team: Black Team
Palettes: Black/Red



Waterwatereverywhere
Male
Sagittarius
62
70
Squire
Elemental
Brave Up
Attack UP
Retreat

Blood Sword
Ice Shield
Ribbon
Mystic Vest
Feather Mantle

Throw Stone, Heal, Yell, Wish
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



KaLam1ty
Male
Leo
38
72
Wizard
Item
Parry
Secret Hunt
Move+1

Flame Rod

Leather Hat
Black Robe
Wizard Mantle

Fire, Fire 2, Fire 3, Bolt 3, Bolt 4, Ice, Death
X-Potion, Maiden's Kiss, Holy Water, Remedy



Electric Glass
Male
Leo
48
69
Mime

Damage Split
Defend
Move-HP Up



Green Beret
Mystic Vest
Bracer

Mimic




RughSontos
Female
Cancer
76
69
Chemist
Basic Skill
Blade Grasp
Equip Armor
Retreat

Panther Bag

Gold Helmet
White Robe
Diamond Armlet

Hi-Potion, X-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down
Dash, Throw Stone, Heal, Tickle, Cheer Up

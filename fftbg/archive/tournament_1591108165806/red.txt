Player: !Red
Team: Red Team
Palettes: Red/Brown



Richardserious
Male
Virgo
77
68
Lancer
Sing
Parry
Defend
Move-HP Up

Mythril Spear
Flame Shield
Diamond Helmet
Crystal Mail
Genji Gauntlet

Level Jump5, Vertical Jump5
Life Song, Battle Song, Diamond Blade



KasugaiRoastedPeas
Male
Cancer
62
69
Thief
Throw
Absorb Used MP
Magic Defense UP
Lava Walking

Dagger

Holy Miter
Judo Outfit
Wizard Mantle

Steal Heart, Steal Helmet, Arm Aim
Wand



Oobs56
Female
Cancer
45
41
Samurai
Summon Magic
Dragon Spirit
Dual Wield
Ignore Height

Muramasa
Heaven's Cloud
Diamond Helmet
Chameleon Robe
108 Gems

Koutetsu, Bizen Boat, Murasame, Chirijiraden
Ramuh, Leviathan, Fairy, Lich



Cryptopsy70
Male
Gemini
50
48
Knight
Charge
Parry
Equip Axe
Move+3

Flail
Escutcheon
Gold Helmet
Linen Cuirass
Bracer

Head Break, Magic Break, Power Break, Stasis Sword, Night Sword
Charge+3

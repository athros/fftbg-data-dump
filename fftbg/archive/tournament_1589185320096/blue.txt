Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kyune
Female
Capricorn
80
49
Archer
Jump
Mana Shield
Magic Defense UP
Waterbreathing

Cross Bow
Aegis Shield
Golden Hairpin
Leather Outfit
Magic Ring

Charge+5, Charge+7, Charge+20
Level Jump4, Vertical Jump8



ZZ Yoshi
Female
Scorpio
63
65
Priest
Yin Yang Magic
Dragon Spirit
Beastmaster
Move-HP Up

Healing Staff

Feather Hat
Brigandine
N-Kai Armlet

Cure 2, Raise, Protect, Protect 2, Wall, Esuna
Blind, Spell Absorb, Doubt Faith, Foxbird, Dispel Magic, Paralyze



R Raynos
Female
Taurus
76
62
Squire
Throw
Meatbone Slash
Concentrate
Fly

Assassin Dagger
Flame Shield
Golden Hairpin
Platinum Armor
Cursed Ring

Heal, Tickle, Cheer Up, Fury, Wish
Shuriken, Bomb, Staff



Ko2q
Male
Aries
48
80
Knight
Charge
Blade Grasp
Attack UP
Retreat

Defender
Gold Shield
Bronze Helmet
Light Robe
Germinas Boots

Head Break, Weapon Break, Power Break
Charge+1, Charge+4, Charge+10, Charge+20

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Optiblast
Male
Aquarius
48
71
Ninja
Steal
Counter Magic
Martial Arts
Move+3



Cachusha
Adaman Vest
Defense Armlet

Shuriken
Steal Armor, Steal Weapon, Steal Status, Arm Aim



Goust18
Monster
Scorpio
53
66
Serpentarius










CJ Soulstar
Female
Sagittarius
65
40
Archer
Steal
Counter Flood
Secret Hunt
Waterbreathing

Gastrafitis
Escutcheon
Golden Hairpin
Clothes
Feather Boots

Charge+3, Charge+7
Gil Taking, Steal Heart, Steal Armor, Arm Aim, Leg Aim



Waterwatereverywhere
Male
Leo
62
43
Geomancer
Time Magic
Distribute
Long Status
Jump+2

Sleep Sword
Crystal Shield
Red Hood
Brigandine
Angel Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Slow, Slow 2, Stop, Float, Quick, Stabilize Time

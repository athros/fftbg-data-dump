Player: !Black
Team: Black Team
Palettes: Black/Red



DavenIII
Female
Taurus
59
49
Wizard
Basic Skill
Caution
Short Status
Fly

Zorlin Shape

Golden Hairpin
Light Robe
108 Gems

Fire 4, Bolt 2, Ice 2, Death, Flare
Accumulate, Heal, Tickle, Yell, Fury



Douchetron
Male
Taurus
57
56
Ninja
Time Magic
Catch
Beastmaster
Retreat

Orichalcum
Flail
Red Hood
Judo Outfit
Feather Mantle

Shuriken
Haste, Haste 2, Reflect, Demi, Demi 2



Mpghappiness
Male
Libra
41
59
Squire
Yin Yang Magic
Speed Save
Beastmaster
Waterwalking

Long Sword
Ice Shield
Flash Hat
Crystal Mail
Spike Shoes

Dash, Throw Stone, Heal, Yell, Cheer Up, Fury
Pray Faith, Silence Song, Blind Rage, Paralyze



Actual JP
Male
Aries
77
45
Oracle
Battle Skill
MA Save
Long Status
Waterbreathing

Octagon Rod

Holy Miter
Rubber Costume
Feather Boots

Poison, Spell Absorb, Life Drain, Silence Song, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Speed Break, Power Break, Mind Break, Stasis Sword

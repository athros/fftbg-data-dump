Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lokenwow
Female
Scorpio
48
63
Lancer
Charge
Hamedo
Defend
Ignore Height

Partisan
Diamond Shield
Gold Helmet
Chain Mail
Defense Ring

Level Jump8, Vertical Jump5
Charge+1, Charge+4, Charge+5, Charge+7



Kellios11
Female
Sagittarius
60
55
Squire
Yin Yang Magic
Damage Split
Beastmaster
Levitate

Ice Brand
Ice Shield
Cachusha
Power Sleeve
Angel Ring

Accumulate, Throw Stone, Wish
Blind, Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Sleep



Thyrandaal
Male
Aries
72
68
Mediator
White Magic
Arrow Guard
Dual Wield
Lava Walking

Blast Gun
Blaze Gun
Twist Headband
Chameleon Robe
Spike Shoes

Praise, Solution, Insult
Cure 2, Raise, Shell 2, Esuna, Holy, Magic Barrier



TheDeeyo
Female
Virgo
54
44
Geomancer
Punch Art
Brave Save
Short Charge
Jump+1

Giant Axe
Flame Shield
Triangle Hat
Black Robe
Small Mantle

Pitfall, Water Ball, Will-O-Wisp, Sand Storm, Blizzard
Spin Fist, Pummel, Wave Fist, Revive

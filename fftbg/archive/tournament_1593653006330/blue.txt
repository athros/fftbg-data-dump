Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kalvin
Male
Capricorn
63
50
Chemist
Summon Magic
Meatbone Slash
Secret Hunt
Levitate

Blast Gun

Golden Hairpin
Clothes
Diamond Armlet

Potion, Hi-Potion, Soft, Holy Water, Remedy, Phoenix Down
Moogle, Ramuh, Fairy, Cyclops



Whitby
Male
Virgo
79
79
Wizard
Yin Yang Magic
Catch
Sicken
Ignore Terrain

Wizard Rod

Twist Headband
Wizard Robe
Red Shoes

Fire 2, Bolt, Bolt 2, Bolt 4, Ice 3
Spell Absorb, Doubt Faith, Zombie, Silence Song, Foxbird, Paralyze, Dark Holy



Delphinia
Monster
Sagittarius
75
60
Behemoth










Sianna
Female
Scorpio
47
75
Lancer
Basic Skill
Auto Potion
Short Status
Levitate

Obelisk
Diamond Shield
Barbuta
Reflect Mail
Leather Mantle

Level Jump5, Vertical Jump3
Accumulate, Dash, Heal, Tickle

Player: !Black
Team: Black Team
Palettes: Black/Red



Bryan792
Monster
Cancer
44
75
Ultima Demon










Ultimate Infinity
Male
Aquarius
72
61
Monk
Time Magic
Counter
Attack UP
Move-HP Up



Flash Hat
Leather Outfit
Angel Ring

Pummel, Purification, Revive
Haste 2, Slow, Immobilize, Quick, Stabilize Time



HASTERIOUS
Male
Sagittarius
47
78
Knight
Steal
Arrow Guard
Equip Polearm
Retreat

Ivory Rod
Venetian Shield
Leather Helmet
Linen Robe
Genji Gauntlet

Armor Break, Magic Break, Speed Break, Mind Break, Justice Sword
Steal Helmet, Steal Accessory, Arm Aim



Lijarkh
Male
Leo
51
48
Monk
Summon Magic
Counter
Long Status
Move+1



Headgear
Judo Outfit
Dracula Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Chakra, Seal Evil
Moogle, Shiva, Ifrit, Leviathan, Lich

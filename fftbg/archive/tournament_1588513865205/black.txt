Player: !Black
Team: Black Team
Palettes: Black/Red



Squatting Bear
Male
Aries
61
78
Archer
White Magic
Abandon
Equip Sword
Levitate

Ragnarok

Leather Hat
Chain Vest
Angel Ring

Charge+1, Charge+5, Charge+10
Cure 3, Raise, Raise 2, Protect



Lifeguard Dan
Female
Aries
61
49
Lancer
Draw Out
Absorb Used MP
Equip Sword
Swim

Ice Brand
Escutcheon
Leather Helmet
Chameleon Robe
Feather Boots

Level Jump8, Vertical Jump7
Muramasa



J2DaBibbles
Female
Libra
50
40
Ninja
Jump
Catch
Equip Gun
Jump+3

Papyrus Codex
Battle Folio
Holy Miter
Mythril Vest
Small Mantle

Shuriken, Bomb, Spear
Level Jump2, Vertical Jump6



Dexsana
Male
Pisces
40
68
Time Mage
Draw Out
Distribute
Equip Armor
Waterbreathing

Wizard Staff

Mythril Helmet
Crystal Mail
Small Mantle

Haste, Slow 2, Stop, Demi, Demi 2, Stabilize Time, Meteor
Koutetsu, Kikuichimoji

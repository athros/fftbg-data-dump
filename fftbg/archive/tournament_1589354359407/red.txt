Player: !Red
Team: Red Team
Palettes: Red/Brown



ApplesauceBoss
Male
Serpentarius
52
59
Ninja
Steal
Dragon Spirit
Attack UP
Jump+1

Iga Knife
Flail
Golden Hairpin
Wizard Outfit
Salty Rage

Sword
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Steal Status



Phytik
Male
Virgo
77
46
Knight
Item
HP Restore
Doublehand
Move+2

Defender

Bronze Helmet
Reflect Mail
Battle Boots

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Surging Sword
Potion, Elixir, Eye Drop, Soft, Holy Water, Phoenix Down



Lythe Caraker
Female
Capricorn
62
50
Geomancer
Battle Skill
Critical Quick
Equip Axe
Jump+2

Morning Star
Platinum Shield
Leather Hat
White Robe
Diamond Armlet

Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Head Break, Magic Break, Power Break, Justice Sword



Superdevon1
Male
Capricorn
75
54
Archer
Throw
Sunken State
Maintenance
Waterwalking

Ice Bow

Bronze Helmet
Wizard Outfit
Angel Ring

Charge+2, Charge+7
Shuriken

Player: !White
Team: White Team
Palettes: White/Blue



Kaelsun
Female
Aries
56
44
Time Mage
Basic Skill
Critical Quick
Long Status
Retreat

Octagon Rod

Thief Hat
Clothes
Dracula Mantle

Haste, Stop, Immobilize, Demi 2, Stabilize Time
Accumulate, Cheer Up, Wish, Scream



Grandlanzer
Female
Capricorn
64
62
Dancer
Black Magic
Mana Shield
Defend
Levitate

Cashmere

Leather Hat
Chameleon Robe
Defense Armlet

Witch Hunt, Wiznaibus, Polka Polka, Disillusion, Nameless Dance
Fire



DeadGirlzREasy
Female
Capricorn
38
79
Geomancer
Black Magic
HP Restore
Equip Sword
Jump+3

Long Sword
Gold Shield
Headgear
Black Costume
Defense Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Bolt, Bolt 2, Ice, Ice 3, Ice 4



Deathmaker06
Male
Pisces
43
75
Summoner
Draw Out
MA Save
Concentrate
Move-MP Up

Wizard Staff

Flash Hat
Power Sleeve
Power Wrist

Moogle, Shiva, Ramuh, Ifrit, Golem, Salamander, Silf, Fairy, Lich, Cyclops
Asura, Koutetsu, Bizen Boat, Chirijiraden

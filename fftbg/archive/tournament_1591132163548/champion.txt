Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



AlenaZarek
Female
Capricorn
47
41
Calculator
Black Magic
MA Save
Equip Armor
Move-HP Up

Bestiary

Gold Helmet
Platinum Armor
Genji Gauntlet

Height, 4, 3
Fire, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Frog, Death, Flare



Cryptopsy70
Male
Scorpio
66
56
Lancer
Steal
Dragon Spirit
Equip Armor
Retreat

Obelisk
Diamond Shield
Mythril Helmet
Mystic Vest
Wizard Mantle

Level Jump8, Vertical Jump7
Steal Heart, Steal Helmet



DrPoopersMD
Male
Scorpio
77
57
Mediator
Jump
Earplug
Dual Wield
Move-MP Up

Mythril Gun
Blaze Gun
Holy Miter
Chain Vest
108 Gems

Persuade, Praise, Refute
Level Jump2, Vertical Jump2



LanseDM
Male
Aries
58
59
Geomancer
Summon Magic
Counter Flood
Equip Knife
Swim

Hidden Knife
Flame Shield
Twist Headband
Power Sleeve
Power Wrist

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Titan, Carbunkle, Bahamut, Odin, Fairy

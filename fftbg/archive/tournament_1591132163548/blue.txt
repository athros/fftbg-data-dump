Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CorpusCav
Female
Libra
65
68
Wizard
Throw
Meatbone Slash
Short Charge
Levitate

Mage Masher

Holy Miter
Light Robe
Angel Ring

Fire, Fire 2, Ice 2, Ice 4, Frog
Shuriken, Knife, Staff



Cam ATS
Female
Taurus
47
58
Samurai
Black Magic
Counter
Dual Wield
Move+3

Javelin
Spear
Leather Helmet
Bronze Armor
Genji Gauntlet

Asura, Koutetsu, Kiyomori, Muramasa, Kikuichimoji
Fire 2, Fire 3, Ice, Ice 2, Empower



ZZ Yoshi
Male
Pisces
47
80
Mime

Critical Quick
Equip Armor
Ignore Height



Gold Helmet
Silk Robe
Genji Gauntlet

Mimic




Jeeboheebo
Male
Aquarius
48
46
Archer
Time Magic
Speed Save
Maintenance
Ignore Terrain

Stone Gun
Buckler
Golden Hairpin
Wizard Outfit
Sprint Shoes

Charge+2, Charge+3, Charge+4, Charge+5
Haste, Slow, Slow 2, Stop, Reflect, Quick

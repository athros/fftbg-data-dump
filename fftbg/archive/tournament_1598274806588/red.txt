Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Male
Libra
50
60
Lancer
Black Magic
Counter
Attack UP
Jump+2

Cypress Rod
Buckler
Leather Helmet
Bronze Armor
Diamond Armlet

Level Jump2, Vertical Jump8
Fire 2, Bolt 2, Empower, Frog



DLJuggernaut
Female
Cancer
44
79
Squire
Summon Magic
Absorb Used MP
Maintenance
Swim

Slasher
Ice Shield
Flash Hat
Secret Clothes
Sprint Shoes

Accumulate, Throw Stone, Tickle
Shiva, Fairy



Galkife
Female
Scorpio
67
39
Mediator
White Magic
HP Restore
Halve MP
Waterbreathing

Battle Folio

Cachusha
White Robe
Power Wrist

Persuade, Praise, Threaten, Preach, Mimic Daravon, Refute
Cure, Raise, Reraise, Protect, Shell, Wall, Esuna



Mirapoix
Male
Taurus
67
76
Oracle
Punch Art
Distribute
Equip Armor
Teleport

Ivory Rod

Platinum Helmet
Bronze Armor
Dracula Mantle

Blind, Life Drain, Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep
Earth Slash, Revive, Seal Evil

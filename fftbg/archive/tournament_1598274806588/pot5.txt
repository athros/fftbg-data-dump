Final Bets: red - 9 bets for 7,925G (41.9%, x1.39); green - 9 bets for 10,994G (58.1%, x0.72)

red bets:
latebit: 4,191G (52.9%, 8,219G)
DLJuggernaut: 1,000G (12.6%, 6,687G)
E_Ballard: 732G (9.2%, 51,538G)
Aldrammech: 700G (8.8%, 700G)
JumbocactuarX27: 636G (8.0%, 636G)
Rytor: 266G (3.4%, 13,348G)
datadrivenbot: 200G (2.5%, 67,620G)
ArashiKurobara: 100G (1.3%, 1,013G)
Thyrandaal: 100G (1.3%, 31,626G)

green bets:
DavenIII: 5,000G (45.5%, 73,396G)
toka222: 2,000G (18.2%, 222,445G)
dogsandcatsand: 1,361G (12.4%, 1,361G)
BirbBrainsBot: 1,000G (9.1%, 1,156G)
powergems: 677G (6.2%, 11,574G)
SephDarkheart: 600G (5.5%, 165,795G)
getthemoneyz: 156G (1.4%, 1,731,885G)
AllInBot: 100G (0.9%, 100G)
RunicMagus: 100G (0.9%, 5,995G)

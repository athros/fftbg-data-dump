Final Bets: green - 7 bets for 47,056G (75.9%, x0.32); yellow - 24 bets for 14,927G (24.1%, x3.15)

green bets:
AllInBot: 41,217G (87.6%, 41,217G)
RaIshtar: 3,000G (6.4%, 23,763G)
Shalloween: 1,000G (2.1%, 60,141G)
Cryptopsy70: 646G (1.4%, 646G)
Phi_Sig: 555G (1.2%, 23,711G)
datadrivenbot: 538G (1.1%, 38,389G)
fluffskull: 100G (0.2%, 6,175G)

yellow bets:
prince_rogers_nelson_: 4,612G (30.9%, 4,612G)
Creggers: 1,000G (6.7%, 43,591G)
BirbBrainsBot: 1,000G (6.7%, 177,348G)
sinnyil2: 908G (6.1%, 908G)
Laserman1000: 688G (4.6%, 27,088G)
getthemoneyz: 688G (4.6%, 1,301,716G)
dogsandcatsand: 560G (3.8%, 560G)
EnemyController: 555G (3.7%, 1,449,620G)
Nizaha: 548G (3.7%, 7,623G)
twelfthrootoftwo: 532G (3.6%, 532G)
TheMurkGnome: 468G (3.1%, 468G)
letdowncity: 416G (2.8%, 5,448G)
killth3kid: 404G (2.7%, 404G)
ACSpree: 396G (2.7%, 396G)
randgridr: 340G (2.3%, 340G)
BoneMiser: 300G (2.0%, 300G)
silentkaster: 300G (2.0%, 14,726G)
benticore: 250G (1.7%, 19,057G)
Geffro1908: 236G (1.6%, 236G)
boosted420: 200G (1.3%, 956G)
TasisSai: 169G (1.1%, 332G)
Xoomwaffle: 132G (0.9%, 132G)
flacococo: 124G (0.8%, 124G)
gorgewall: 101G (0.7%, 2,049G)

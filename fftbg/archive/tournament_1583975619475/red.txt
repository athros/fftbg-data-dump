Player: !Red
Team: Red Team
Palettes: Red/Brown



Dimmitree
Male
Capricorn
73
55
Lancer
Talk Skill
Blade Grasp
Equip Axe
Move+2

Battle Axe
Flame Shield
Bronze Helmet
Black Robe
Cursed Ring

Level Jump3, Vertical Jump8
Persuade, Threaten, Solution, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Technominari
Female
Taurus
43
51
Priest
Item
Arrow Guard
Equip Sword
Ignore Terrain

Diamond Sword

Headgear
Power Sleeve
Power Wrist

Cure, Cure 2, Cure 3, Raise, Raise 2, Regen, Protect 2, Shell 2, Esuna, Magic Barrier
Ether, Antidote, Eye Drop, Maiden's Kiss, Phoenix Down



Metal Gear Flex
Female
Gemini
50
50
Chemist
Jump
Regenerator
Sicken
Waterwalking

Blind Knife

Feather Hat
Wizard Outfit
Feather Mantle

Potion, Hi-Potion, Ether, Elixir, Maiden's Kiss, Remedy, Phoenix Down
Level Jump2, Vertical Jump3



Smegma Sorcerer
Monster
Gemini
50
44
Iron Hawk








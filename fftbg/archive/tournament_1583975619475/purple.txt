Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Aldrammech
Male
Capricorn
45
56
Mime

Regenerator
Equip Armor
Swim



Feather Hat
Genji Armor
Diamond Armlet

Mimic




Gormkt30
Female
Aquarius
57
58
Mediator
Punch Art
Regenerator
Beastmaster
Move-HP Up

Blaze Gun

Green Beret
Silk Robe
Rubber Shoes

Invitation, Threaten, Preach, Negotiate, Mimic Daravon, Rehabilitate
Purification, Revive



Alacor
Male
Taurus
68
68
Knight
Draw Out
Brave Up
Short Charge
Move-MP Up

Slasher
Gold Shield
Gold Helmet
Bronze Armor
Germinas Boots

Weapon Break, Power Break, Surging Sword
Heaven's Cloud, Kikuichimoji, Chirijiraden



TheOneRanger
Male
Cancer
78
69
Samurai
Jump
HP Restore
Defense UP
Waterbreathing

Bizen Boat

Barbuta
Wizard Robe
Defense Armlet

Koutetsu, Bizen Boat, Murasame, Kikuichimoji
Level Jump8, Vertical Jump7

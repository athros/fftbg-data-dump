Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Omegasuspekt
Monster
Sagittarius
60
55
Red Dragon










Reddwind
Female
Scorpio
57
76
Knight
Elemental
Counter
Dual Wield
Teleport

Defender
Save the Queen
Circlet
Bronze Armor
Defense Ring

Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Surging Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Blizzard



Oobs56
Female
Taurus
45
65
Lancer
White Magic
Abandon
Martial Arts
Move+3


Crystal Shield
Platinum Helmet
Mythril Armor
Power Wrist

Level Jump8, Vertical Jump2
Cure, Cure 2, Raise, Regen, Protect 2, Wall, Esuna



Neerrm
Male
Virgo
49
78
Knight
Item
Brave Save
Doublehand
Move+2

Ragnarok

Platinum Helmet
White Robe
Angel Ring

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword
Hi-Potion, X-Potion, Elixir, Antidote, Maiden's Kiss, Soft

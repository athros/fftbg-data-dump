Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



NIghtdew14
Female
Aquarius
80
67
Dancer
Draw Out
Hamedo
Magic Attack UP
Jump+2

Persia

Black Hood
Judo Outfit
Magic Gauntlet

Witch Hunt, Wiznaibus, Nameless Dance, Last Dance
Koutetsu, Murasame, Chirijiraden



Killth3kid
Male
Virgo
76
46
Archer
Throw
Distribute
Attack UP
Move-HP Up

Snipe Bow
Mythril Shield
Twist Headband
Chain Vest
Spike Shoes

Charge+3, Charge+4, Charge+5
Spear, Dictionary



Thyrandaal
Male
Scorpio
65
76
Ninja
Battle Skill
PA Save
Equip Gun
Levitate

Papyrus Codex
Bestiary
Green Beret
Wizard Outfit
Bracer

Shuriken, Sword, Hammer
Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Stasis Sword



CT 5 Holy
Male
Capricorn
71
66
Lancer
White Magic
Counter
Defend
Waterwalking

Musk Rod
Escutcheon
Cross Helmet
Plate Mail
Red Shoes

Level Jump5, Vertical Jump7
Raise, Regen, Shell, Wall

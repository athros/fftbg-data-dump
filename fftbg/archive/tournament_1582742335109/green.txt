Player: !Green
Team: Green Team
Palettes: Green/White



Flowtaro
Monster
Leo
52
40
Sekhret










KupoKel
Male
Cancer
44
60
Oracle
Summon Magic
Parry
Defend
Lava Walking

Gokuu Rod

Twist Headband
Chameleon Robe
Magic Ring

Spell Absorb, Zombie, Sleep
Moogle, Shiva, Carbunkle, Bahamut, Odin, Silf, Fairy, Lich



Eldente
Female
Libra
68
37
Priest
Item
Sunken State
Dual Wield
Jump+1

Flame Whip
Flail
Headgear
Linen Robe
Wizard Mantle

Cure, Raise, Raise 2, Protect, Protect 2, Shell 2, Wall, Esuna, Holy
Potion, Hi-Ether, Eye Drop, Echo Grass, Phoenix Down



Saithis1
Female
Capricorn
71
67
Monk
White Magic
HP Restore
Halve MP
Jump+3



Flash Hat
Clothes
Spike Shoes

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Cure 3, Protect 2, Esuna

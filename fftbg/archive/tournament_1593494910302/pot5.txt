Final Bets: red - 8 bets for 3,118G (37.8%, x1.64); yellow - 8 bets for 5,126G (62.2%, x0.61)

red bets:
DeathTaxesAndAnime: 838G (26.9%, 838G)
BirbBrainsBot: 806G (25.8%, 159,556G)
getthemoneyz: 632G (20.3%, 1,114,304G)
Spuzzmocker: 292G (9.4%, 292G)
Evewho: 200G (6.4%, 10,622G)
Jinglejangler: 150G (4.8%, 400G)
Ring_Wyrm: 100G (3.2%, 1,794G)
InskipJester: 100G (3.2%, 100G)

yellow bets:
DuraiPapers: 2,000G (39.0%, 8,595G)
SkylerBunny: 1,100G (21.5%, 502,489G)
Firesheath: 664G (13.0%, 664G)
Arcblazer23: 500G (9.8%, 7,458G)
superdevon1: 340G (6.6%, 340G)
ThreeMileIsland: 222G (4.3%, 405G)
thewondertrickster: 200G (3.9%, 2,831G)
datadrivenbot: 100G (2.0%, 46,413G)

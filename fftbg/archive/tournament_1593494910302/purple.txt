Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lowlf
Female
Libra
65
70
Mediator
Item
Counter Tackle
Dual Wield
Lava Walking

Mythril Gun
Blast Gun
Black Hood
Adaman Vest
Small Mantle

Invitation, Threaten, Negotiate
Potion, Ether, Phoenix Down



DeathTaxesAndAnime
Female
Scorpio
76
66
Lancer
Throw
MA Save
Equip Sword
Retreat

Heaven's Cloud
Escutcheon
Mythril Helmet
Linen Cuirass
Sprint Shoes

Level Jump4, Vertical Jump3
Stick



Tyerenex
Female
Virgo
77
72
Squire
Charge
Counter
Beastmaster
Swim

Gastrafitis
Round Shield
Genji Helmet
Gold Armor
Bracer

Accumulate, Heal, Yell, Cheer Up, Scream
Charge+1, Charge+2, Charge+4, Charge+5, Charge+7



DuraiPapers
Male
Leo
62
68
Squire
Charge
Counter Flood
Defense UP
Ignore Terrain

Battle Axe
Buckler
Bronze Helmet
Judo Outfit
Battle Boots

Accumulate, Heal, Cheer Up, Fury, Scream
Charge+1, Charge+5

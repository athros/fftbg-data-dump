Player: !Black
Team: Black Team
Palettes: Black/Red



Fenaen
Male
Capricorn
54
64
Squire
Talk Skill
Regenerator
Monster Talk
Teleport

Giant Axe
Mythril Shield
Red Hood
Mystic Vest
Sprint Shoes

Dash, Tickle, Yell
Invitation, Threaten, Preach, Negotiate, Refute, Rehabilitate



XLilAsia
Male
Virgo
46
55
Squire
Talk Skill
Speed Save
Equip Polearm
Move-HP Up

Ryozan Silk
Gold Shield
Headgear
Black Costume
Dracula Mantle

Heal, Cheer Up, Scream
Persuade, Threaten, Solution, Insult, Refute



Shalloween
Female
Aquarius
80
38
Thief
Item
Critical Quick
Short Charge
Move+2

Mythril Knife

Twist Headband
Wizard Outfit
Battle Boots

Gil Taking, Steal Armor, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Potion, X-Potion, Ether, Holy Water, Phoenix Down



OneHundredFists
Male
Aquarius
60
51
Knight
Time Magic
Regenerator
Magic Attack UP
Move+3

Ragnarok
Kaiser Plate
Gold Helmet
Black Robe
Magic Ring

Head Break, Shield Break, Weapon Break, Mind Break, Justice Sword, Surging Sword
Haste 2, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time

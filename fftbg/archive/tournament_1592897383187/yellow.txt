Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Quadh0nk
Female
Capricorn
75
73
Ninja
White Magic
Caution
Equip Gun
Retreat

Bestiary
Battle Folio
Leather Hat
Chain Vest
Angel Ring

Shuriken, Bomb, Stick
Cure 2, Raise, Regen, Protect, Protect 2, Esuna



Spuzzmocker
Male
Capricorn
72
66
Ninja
Time Magic
Critical Quick
Magic Defense UP
Move-MP Up

Scorpion Tail
Scorpion Tail
Triangle Hat
Mystic Vest
Leather Mantle

Shuriken, Bomb, Knife, Staff
Slow 2, Stop, Demi, Stabilize Time, Meteor



Ruebyy
Male
Libra
44
58
Knight
Steal
MA Save
Equip Knife
Move+3

Short Edge
Hero Shield
Crystal Helmet
Leather Armor
Feather Boots

Weapon Break, Speed Break, Night Sword
Gil Taking, Steal Heart, Steal Accessory



Justbungus
Female
Aries
77
52
Monk
Time Magic
Counter Magic
Equip Armor
Jump+1



Ribbon
Gold Armor
Setiemson

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive
Haste, Haste 2, Slow 2, Immobilize, Quick

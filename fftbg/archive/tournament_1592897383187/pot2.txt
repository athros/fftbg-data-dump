Final Bets: green - 9 bets for 3,018G (21.9%, x3.57); yellow - 9 bets for 10,780G (78.1%, x0.28)

green bets:
BirbBrainsBot: 1,000G (33.1%, 77,464G)
KyleWonToLiveForever: 603G (20.0%, 603G)
getthemoneyz: 514G (17.0%, 1,011,041G)
gorgewall: 301G (10.0%, 5,526G)
Evewho: 200G (6.6%, 3,461G)
OverlordSS4: 100G (3.3%, 2,978G)
delcake: 100G (3.3%, 1,194G)
SuzakuReii: 100G (3.3%, 4,376G)
ruleof5: 100G (3.3%, 33,314G)

yellow bets:
bruubarg: 3,457G (32.1%, 3,457G)
soren_of_tyto: 2,650G (24.6%, 2,650G)
reinoe: 2,000G (18.6%, 27,182G)
CrownOfHorns: 810G (7.5%, 810G)
Spuzzmocker: 767G (7.1%, 767G)
prince_rogers_nelson_: 600G (5.6%, 600G)
Arcblazer23: 296G (2.7%, 296G)
justbungus: 100G (0.9%, 100G)
datadrivenbot: 100G (0.9%, 49,081G)

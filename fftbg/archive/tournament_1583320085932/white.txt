Player: !White
Team: White Team
Palettes: White/Blue



Thaetreis
Female
Capricorn
78
73
Squire
Black Magic
Counter Tackle
Equip Sword
Teleport

Ancient Sword
Platinum Shield
Mythril Helmet
Reflect Mail
Reflect Ring

Accumulate, Cheer Up, Wish, Scream
Fire 3, Fire 4, Bolt, Ice 2, Ice 3



DeathTaxesAndAnime
Female
Pisces
51
75
Ninja
Draw Out
Meatbone Slash
Equip Knife
Jump+1

Morning Star
Flame Rod
Twist Headband
Chain Vest
Cursed Ring

Bomb, Wand
Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa



Ominnous
Male
Aries
78
68
Ninja
Black Magic
Meatbone Slash
Equip Sword
Move-HP Up

Kiyomori
Platinum Sword
Leather Hat
Earth Clothes
Reflect Ring

Shuriken
Fire 3, Bolt 4, Ice 2, Flare



I Nod My Head When I Lose
Male
Aries
51
69
Knight
Basic Skill
Mana Shield
Short Charge
Waterbreathing

Ragnarok

Platinum Helmet
Platinum Armor
Dracula Mantle

Head Break, Shield Break, Speed Break, Power Break, Mind Break, Justice Sword
Accumulate, Heal, Scream

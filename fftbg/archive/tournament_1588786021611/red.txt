Player: !Red
Team: Red Team
Palettes: Red/Brown



Wizblizz
Male
Scorpio
56
54
Bard
Talk Skill
PA Save
Dual Wield
Fly

Lightning Bow

Golden Hairpin
Power Sleeve
Battle Boots

Cheer Song, Battle Song, Nameless Song, Last Song, Space Storage, Sky Demon, Hydra Pit
Praise, Preach, Solution



Error72
Female
Scorpio
69
74
Samurai
Time Magic
Abandon
Magic Attack UP
Ignore Height

Bizen Boat

Platinum Helmet
Leather Armor
Feather Boots

Bizen Boat, Kiyomori, Muramasa
Stop, Reflect, Quick, Galaxy Stop



DrAntiSocial
Female
Aries
79
80
Summoner
Black Magic
PA Save
Short Charge
Waterwalking

Poison Rod

Holy Miter
Silk Robe
Germinas Boots

Moogle, Ifrit, Carbunkle, Fairy, Lich
Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Flare



Cryptopsy70
Female
Scorpio
56
58
Time Mage
Jump
Abandon
Equip Gun
Levitate

Mythril Gun

Green Beret
Brigandine
N-Kai Armlet

Haste, Quick, Demi, Demi 2, Stabilize Time
Level Jump8, Vertical Jump7

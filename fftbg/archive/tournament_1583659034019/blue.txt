Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WhiteTigress
Female
Leo
45
72
Calculator
Black Magic
Faith Up
Martial Arts
Fly



Feather Hat
Light Robe
Defense Ring

CT, Height, Prime Number, 3
Ice 3



Nosablake
Male
Leo
71
76
Ninja
Elemental
Counter Flood
Equip Armor
Jump+1

Short Edge
Spell Edge
Iron Helmet
White Robe
Feather Mantle

Shuriken, Bomb, Knife
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Z32o
Male
Sagittarius
48
60
Geomancer
Punch Art
Counter
Equip Armor
Jump+3

Giant Axe
Buckler
Cross Helmet
Linen Cuirass
Germinas Boots

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm
Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



Nickelbank
Male
Sagittarius
47
50
Mime

Counter Tackle
Short Status
Swim


Mythril Shield
Triangle Hat
Wizard Outfit
Genji Gauntlet

Mimic


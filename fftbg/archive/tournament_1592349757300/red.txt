Player: !Red
Team: Red Team
Palettes: Red/Brown



DuraiPapers
Male
Leo
73
80
Squire
Time Magic
Counter Flood
Defense UP
Move-MP Up

Platinum Sword
Buckler
Holy Miter
Genji Armor
Feather Boots

Accumulate, Throw Stone, Heal, Fury, Wish
Haste, Slow, Stop, Float, Quick, Stabilize Time



Powerpinch
Male
Leo
40
66
Ninja
Item
Blade Grasp
Secret Hunt
Ignore Terrain

Kunai
Spell Edge
Holy Miter
Power Sleeve
Genji Gauntlet

Shuriken, Bomb, Wand
Hi-Potion, Ether, Hi-Ether, Holy Water, Phoenix Down



HASTERIOUS
Female
Scorpio
51
40
Oracle
Draw Out
Critical Quick
Dual Wield
Jump+1

Battle Bamboo
Gokuu Rod
Green Beret
Silk Robe
Genji Gauntlet

Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic
Asura, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa, Masamune



SSwing
Female
Sagittarius
69
80
Calculator
Farm Skill
Meatbone Slash
Magic Defense UP
Fly

Bestiary
Buckler
Genji Helmet
Silk Robe
Feather Mantle

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nizaha
Female
Scorpio
45
47
Monk
Draw Out
Arrow Guard
Maintenance
Swim



Golden Hairpin
Judo Outfit
Defense Armlet

Spin Fist, Wave Fist, Purification, Chakra, Revive, Seal Evil
Bizen Boat, Murasame, Heaven's Cloud, Masamune



Sinnyil2
Male
Sagittarius
74
80
Calculator
Black Magic
Caution
Secret Hunt
Jump+2

Papyrus Codex

Red Hood
Wizard Robe
Magic Gauntlet

CT, Height, 5, 4, 3
Fire 2, Bolt 3, Bolt 4, Ice, Ice 3, Empower



Bluciferous
Male
Leo
73
75
Oracle
White Magic
Distribute
Equip Knife
Ignore Height

Short Edge

Triangle Hat
White Robe
Battle Boots

Doubt Faith, Silence Song, Dispel Magic, Paralyze, Dark Holy
Cure, Cure 3, Cure 4, Reraise, Esuna



Laserman1000
Male
Pisces
69
72
Bard
Item
Catch
Throw Item
Swim

Ultimus Bow

Headgear
Carabini Mail
Jade Armlet

Cheer Song, Last Song
Hi-Potion, Eye Drop

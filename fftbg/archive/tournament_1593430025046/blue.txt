Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ring Wyrm
Female
Capricorn
54
64
Ninja
Basic Skill
Sunken State
Equip Sword
Waterbreathing

Koutetsu Knife
Muramasa
Red Hood
Secret Clothes
Sprint Shoes

Bomb, Knife
Accumulate, Dash, Throw Stone, Tickle, Yell, Fury, Scream



MrFlabyo
Female
Sagittarius
58
57
Knight
Basic Skill
Brave Save
Attack UP
Jump+3

Battle Axe
Mythril Shield
Platinum Helmet
Black Robe
N-Kai Armlet

Weapon Break, Speed Break, Power Break, Mind Break, Dark Sword
Yell



Prince Rogers Nelson
Male
Aquarius
41
45
Squire
Sing
Absorb Used MP
Sicken
Jump+2

Coral Sword
Escutcheon
Thief Hat
Mythril Vest
Feather Mantle

Heal, Wish
Angel Song, Nameless Song, Last Song, Diamond Blade, Sky Demon



Rytor
Male
Libra
64
63
Summoner
Elemental
Critical Quick
Maintenance
Jump+1

Rod

Triangle Hat
Robe of Lords
Sprint Shoes

Moogle, Ramuh, Golem, Carbunkle, Odin, Salamander
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Player: !zChamp
Team: Champion Team
Palettes: Green/White



E Ballard
Male
Taurus
43
72
Mediator
Item
PA Save
Magic Attack UP
Waterbreathing

Papyrus Codex

Triangle Hat
Black Robe
Angel Ring

Threaten, Death Sentence, Insult, Rehabilitate
Potion, Hi-Potion, Hi-Ether, Antidote, Soft, Holy Water, Remedy



Aldrammech
Male
Leo
79
73
Monk
Throw
Counter
Sicken
Ignore Terrain



Green Beret
Secret Clothes
Sprint Shoes

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Shuriken, Bomb



Mesmaster
Male
Sagittarius
41
59
Samurai
Battle Skill
Counter Flood
Doublehand
Jump+3

Asura Knife

Cross Helmet
Reflect Mail
Elf Mantle

Asura, Murasame, Heaven's Cloud
Weapon Break, Magic Break, Power Break, Night Sword



Dogsandcatsand
Male
Scorpio
65
75
Mime

Hamedo
Secret Hunt
Jump+3



Golden Hairpin
Earth Clothes
Power Wrist

Mimic


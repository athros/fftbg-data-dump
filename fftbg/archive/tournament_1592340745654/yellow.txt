Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Firesheath
Male
Leo
77
67
Oracle
Battle Skill
Critical Quick
Beastmaster
Levitate

Iron Fan

Holy Miter
Power Sleeve
Leather Mantle

Spell Absorb, Life Drain, Zombie, Foxbird, Dispel Magic, Sleep
Armor Break, Magic Break, Speed Break, Stasis Sword, Night Sword



Deathmaker06
Male
Taurus
52
52
Mime

Critical Quick
Magic Attack UP
Lava Walking



Triangle Hat
Chain Vest
Jade Armlet

Mimic




CrownOfHorns
Male
Taurus
60
52
Thief
Punch Art
Dragon Spirit
Maintenance
Move-HP Up

Diamond Sword

Twist Headband
Black Costume
Feather Boots

Steal Armor, Steal Shield, Steal Accessory, Steal Status
Pummel, Wave Fist, Chakra, Revive



Just Here2
Male
Gemini
50
62
Mediator
Battle Skill
PA Save
Equip Shield
Ignore Terrain

Stone Gun
Gold Shield
Red Hood
White Robe
Reflect Ring

Praise, Threaten, Refute
Magic Break, Mind Break, Justice Sword, Explosion Sword

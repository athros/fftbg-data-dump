Player: !White
Team: White Team
Palettes: White/Blue



Zeroroute
Male
Capricorn
70
53
Calculator
Tentacle Skill
HP Restore
Magic Attack UP
Move+1

Battle Folio

Circlet
White Robe
Battle Boots

Blue Magic
Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast, Tendrils, Lick, Goo, Bad Breath, Moldball Virus



Aka Gilly
Male
Aries
73
62
Chemist
White Magic
Faith Up
Equip Gun
Move-HP Up

Battle Folio

Green Beret
Mystic Vest
108 Gems

Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Cure, Cure 2, Raise, Raise 2, Esuna



Loveyouallfriends
Monster
Scorpio
61
77
Red Panther










DudeMonkey77
Female
Aries
66
61
Calculator
Bird Skill
Caution
Equip Knife
Move+1

Cultist Dagger

Headgear
Power Sleeve
Feather Boots

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak

Player: !Green
Team: Green Team
Palettes: Green/White



Rislyeu
Female
Serpentarius
53
75
Mime

Counter Flood
Sicken
Move+1



Green Beret
Black Costume
Angel Ring

Mimic




ALY327
Male
Aquarius
42
61
Squire
Black Magic
Critical Quick
Equip Axe
Move-HP Up

Sage Staff
Buckler
Diamond Helmet
Chain Mail
Leather Mantle

Accumulate, Heal, Tickle
Fire, Bolt, Bolt 4, Ice, Flare



Ruleof5
Male
Gemini
68
55
Time Mage
Yin Yang Magic
Catch
Equip Polearm
Retreat

Persia

Twist Headband
Light Robe
108 Gems

Haste, Slow, Quick
Blind, Spell Absorb, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Petrify, Dark Holy



MilesDong
Male
Aries
52
50
Wizard
Item
Auto Potion
Defend
Move+2

Wizard Rod

Triangle Hat
Leather Outfit
Jade Armlet

Bolt 2, Ice
Potion, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down

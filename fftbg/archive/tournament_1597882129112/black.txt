Player: !Black
Team: Black Team
Palettes: Black/Red



Blastinus
Female
Aquarius
75
56
Archer
Battle Skill
MA Save
Defense UP
Move+3

Stone Gun
Bronze Shield
Circlet
Chain Vest
Power Wrist

Charge+4
Magic Break, Mind Break, Explosion Sword



TheBinklive
Female
Capricorn
64
47
Knight
Charge
Critical Quick
Secret Hunt
Ignore Terrain

Long Sword
Mythril Shield
Diamond Helmet
Chain Mail
Bracer

Armor Break, Shield Break, Magic Break, Power Break, Justice Sword, Night Sword, Surging Sword
Charge+20



Lanshaft
Female
Aquarius
68
62
Squire
White Magic
Regenerator
Short Charge
Move+1

Flail
Ice Shield
Leather Hat
Wizard Outfit
Cursed Ring

Heal, Tickle
Cure 2, Raise, Protect, Protect 2, Shell 2, Esuna, Holy, Magic Barrier



Latebit
Female
Aries
57
79
Samurai
Basic Skill
Distribute
Short Charge
Retreat

Masamune

Iron Helmet
Silk Robe
Defense Ring

Asura, Koutetsu
Dash, Throw Stone, Tickle

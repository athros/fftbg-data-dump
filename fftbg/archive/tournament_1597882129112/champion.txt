Player: !zChamp
Team: Champion Team
Palettes: Black/Red



CosmicTactician
Female
Libra
80
76
Mediator
Summon Magic
Auto Potion
Dual Wield
Waterbreathing

Papyrus Codex
Papyrus Codex
Golden Hairpin
Mythril Vest
Jade Armlet

Invitation, Preach, Negotiate, Mimic Daravon, Refute
Shiva, Ramuh, Titan, Odin, Fairy, Cyclops



Forkmore
Female
Aquarius
60
59
Time Mage
White Magic
Faith Save
Short Charge
Levitate

Cypress Rod

Headgear
White Robe
Genji Gauntlet

Slow, Immobilize, Reflect, Quick, Demi 2, Stabilize Time, Galaxy Stop
Cure, Raise, Protect, Protect 2, Shell, Esuna



RunicMagus
Female
Sagittarius
44
66
Wizard
Math Skill
Blade Grasp
Defend
Jump+3

Cultist Dagger

Holy Miter
Mythril Vest
Vanish Mantle

Fire 3, Bolt 3, Bolt 4, Ice 2
Height, Prime Number, 3



E Ballard
Male
Pisces
72
75
Mime

Meatbone Slash
Attack UP
Waterwalking



Green Beret
Clothes
Spike Shoes

Mimic


Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Tommy Havoc
Female
Libra
59
69
Samurai
Elemental
Earplug
Beastmaster
Waterwalking

Heaven's Cloud

Mythril Helmet
Leather Armor
Battle Boots

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Goust18
Male
Sagittarius
49
76
Summoner
Elemental
Counter
Short Charge
Waterwalking

Flame Rod

Headgear
Adaman Vest
Bracer

Moogle, Ifrit, Titan, Salamander, Silf, Fairy
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard



Eldente
Male
Libra
42
61
Calculator
Yin Yang Magic
Dragon Spirit
Beastmaster
Swim

Papyrus Codex

Feather Hat
White Robe
N-Kai Armlet

CT, Height, Prime Number, 5, 4, 3
Blind, Life Drain, Doubt Faith, Foxbird, Dispel Magic, Sleep, Dark Holy



Pathogen7
Male
Serpentarius
79
75
Archer
Summon Magic
Counter Flood
Halve MP
Waterbreathing

Hunting Bow
Hero Shield
Feather Hat
Black Costume
Jade Armlet

Charge+1, Charge+4, Charge+10
Moogle, Ramuh, Titan, Carbunkle, Bahamut, Odin, Silf, Cyclops

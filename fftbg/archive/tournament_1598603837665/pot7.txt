Final Bets: yellow - 9 bets for 12,272G (59.6%, x0.68); purple - 10 bets for 8,325G (40.4%, x1.47)

yellow bets:
Dastoric: 2,252G (18.4%, 2,252G)
NovaKnight21: 2,083G (17.0%, 4,166G)
amiture: 2,000G (16.3%, 31,352G)
NicoSavoy: 2,000G (16.3%, 54,892G)
superdevon1: 1,592G (13.0%, 79,618G)
BirbBrainsBot: 1,000G (8.1%, 92,849G)
CT_5_Holy: 617G (5.0%, 12,353G)
CassiePhoenix: 528G (4.3%, 528G)
datadrivenbot: 200G (1.6%, 62,860G)

purple bets:
Mudrockk: 2,290G (27.5%, 11,452G)
Mesmaster: 2,000G (24.0%, 87,702G)
JCBooBot: 1,000G (12.0%, 46,500G)
MemoriesofFinal: 979G (11.8%, 979G)
gorgewall: 636G (7.6%, 636G)
bruubarg: 576G (6.9%, 576G)
ColetteMSLP: 400G (4.8%, 4,637G)
AllInBot: 260G (3.1%, 260G)
getthemoneyz: 136G (1.6%, 1,760,992G)
DeathTaxesAndAnime: 48G (0.6%, 3,832G)

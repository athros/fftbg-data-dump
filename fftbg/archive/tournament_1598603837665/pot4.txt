Final Bets: purple - 7 bets for 5,706G (43.7%, x1.29); brown - 11 bets for 7,338G (56.3%, x0.78)

purple bets:
amiture: 2,000G (35.1%, 31,280G)
BirbBrainsBot: 1,000G (17.5%, 91,460G)
Mesmaster: 1,000G (17.5%, 83,734G)
NovaKnight21: 700G (12.3%, 700G)
CT_5_Holy: 522G (9.1%, 10,455G)
ColetteMSLP: 400G (7.0%, 4,523G)
getthemoneyz: 84G (1.5%, 1,761,244G)

brown bets:
NicoSavoy: 2,000G (27.3%, 56,416G)
JCBooBot: 1,000G (13.6%, 48,262G)
Seaweed_B: 1,000G (13.6%, 46,957G)
superdevon1: 794G (10.8%, 79,481G)
gorgewall: 636G (8.7%, 636G)
ko2q: 580G (7.9%, 580G)
Dastoric: 500G (6.8%, 2,319G)
CassiePhoenix: 428G (5.8%, 428G)
datadrivenbot: 200G (2.7%, 63,212G)
AllInBot: 100G (1.4%, 100G)
MemoriesofFinal: 100G (1.4%, 1,179G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ELzukkor
Female
Aquarius
73
63
Lancer
Talk Skill
Meatbone Slash
Equip Sword
Swim

Heaven's Cloud
Genji Shield
Gold Helmet
Linen Robe
Cursed Ring

Level Jump8, Vertical Jump8
Invitation, Persuade, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate



Jeeboheebo
Male
Gemini
67
44
Squire
Charge
MA Save
Equip Knife
Waterwalking

Poison Rod
Round Shield
Feather Hat
Reflect Mail
Angel Ring

Accumulate, Cheer Up, Fury
Charge+1, Charge+5, Charge+7, Charge+10



HaplessOne
Male
Scorpio
45
56
Knight
Jump
Mana Shield
Equip Bow
Move+3

Ice Bow

Barbuta
Bronze Armor
Angel Ring

Head Break, Weapon Break, Power Break, Mind Break, Justice Sword, Dark Sword
Level Jump5, Vertical Jump3



NovaKnight21
Male
Sagittarius
78
49
Knight
Talk Skill
Earplug
Dual Wield
Jump+3

Excalibur
Battle Axe
Platinum Helmet
Light Robe
Battle Boots

Armor Break, Weapon Break, Power Break, Dark Sword
Solution, Mimic Daravon, Rehabilitate

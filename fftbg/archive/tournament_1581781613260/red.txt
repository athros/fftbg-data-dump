Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Female
Scorpio
61
51
Mediator
Black Magic
Speed Save
Equip Axe
Move+1

Rainbow Staff

Green Beret
Light Robe
Battle Boots

Invitation, Solution, Refute
Fire, Fire 3, Bolt 3, Ice 3



Holyonline
Female
Serpentarius
76
54
Chemist
Punch Art
Faith Up
Defense UP
Move-MP Up

Romanda Gun

Headgear
Chain Vest
Wizard Mantle

Potion, Hi-Ether, Antidote, Soft, Phoenix Down
Pummel, Secret Fist, Purification, Chakra



Dataloss783
Female
Virgo
46
70
Ninja
Dance
Dragon Spirit
Doublehand
Move+1

Main Gauche

Green Beret
Clothes
Germinas Boots

Shuriken, Bomb
Witch Hunt, Disillusion, Nameless Dance



SomthingMore
Female
Taurus
68
43
Wizard
Jump
Mana Shield
Short Charge
Swim

Poison Rod

Black Hood
Leather Outfit
Defense Ring

Fire 3, Bolt, Bolt 2, Bolt 4, Ice 3, Death, Flare
Level Jump5, Vertical Jump7

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Leakimiko
Male
Virgo
42
76
Wizard
Basic Skill
Arrow Guard
Magic Attack UP
Fly

Ice Rod

Leather Hat
White Robe
Feather Boots

Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Ice 2
Heal, Cheer Up



Draconis345
Male
Capricorn
58
68
Archer
Draw Out
Parry
Magic Attack UP
Move+3

Cross Bow
Genji Shield
Flash Hat
Power Sleeve
Cursed Ring

Charge+1
Asura



Coyowijuju
Female
Libra
57
54
Wizard
Draw Out
Catch
Short Status
Jump+1

Flame Rod

Triangle Hat
White Robe
Red Shoes

Fire, Fire 2, Fire 4, Bolt 2, Ice, Death, Flare
Bizen Boat, Heaven's Cloud, Muramasa



Mistborn12
Female
Cancer
79
65
Ninja
White Magic
Regenerator
Equip Polearm
Move-MP Up

Javelin
Javelin
Leather Hat
Mythril Vest
108 Gems

Shuriken, Hammer
Raise, Raise 2, Regen, Protect 2, Wall, Esuna

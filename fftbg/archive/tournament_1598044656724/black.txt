Player: !Black
Team: Black Team
Palettes: Black/Red



Maakur
Female
Taurus
67
72
Samurai
Charge
Catch
Doublehand
Waterwalking

Asura Knife

Barbuta
Gold Armor
Rubber Shoes

Bizen Boat, Murasame, Masamune
Charge+1, Charge+2, Charge+7, Charge+10, Charge+20



Sinnyil2
Female
Aquarius
44
53
Summoner
Yin Yang Magic
Dragon Spirit
Magic Attack UP
Swim

Thunder Rod

Ribbon
Mythril Vest
Rubber Shoes

Moogle, Shiva, Ramuh, Carbunkle, Leviathan, Silf, Cyclops
Blind, Zombie, Silence Song, Blind Rage, Sleep, Dark Holy



SkylerBunny
Male
Virgo
53
73
Knight
Black Magic
Caution
Martial Arts
Move-MP Up

Defender
Escutcheon
Leather Helmet
Linen Cuirass
Cursed Ring

Shield Break, Weapon Break, Speed Break, Power Break, Stasis Sword
Fire, Fire 2, Fire 3, Bolt 2, Empower



Tarheels218
Male
Aries
47
54
Knight
Punch Art
Sunken State
Secret Hunt
Move+1

Ancient Sword
Ice Shield
Iron Helmet
Mythril Armor
Reflect Ring

Magic Break, Speed Break, Stasis Sword
Earth Slash

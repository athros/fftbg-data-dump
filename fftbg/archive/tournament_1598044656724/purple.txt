Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evdoggity
Male
Scorpio
69
73
Knight
Punch Art
Faith Save
Equip Sword
Fly

Ragnarok
Mythril Shield
Mythril Helmet
Chain Mail
Feather Boots

Armor Break, Speed Break, Night Sword
Pummel, Wave Fist, Earth Slash, Secret Fist, Revive, Seal Evil



Reddwind
Male
Sagittarius
43
73
Knight
Steal
Regenerator
Short Charge
Move-HP Up

Battle Axe
Buckler
Circlet
Light Robe
Rubber Shoes

Head Break, Weapon Break, Mind Break, Night Sword, Explosion Sword
Arm Aim



Laserman1000
Monster
Virgo
65
41
Swine










Kronikle
Female
Taurus
75
56
Summoner
Battle Skill
Counter
Short Charge
Ignore Terrain

Mace of Zeus

Golden Hairpin
Linen Robe
Angel Ring

Moogle, Ifrit, Titan, Golem, Salamander, Lich
Weapon Break, Power Break, Stasis Sword, Justice Sword

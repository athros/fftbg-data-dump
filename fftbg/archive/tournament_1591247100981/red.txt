Player: !Red
Team: Red Team
Palettes: Red/Brown



Metagameface
Monster
Capricorn
80
48
Explosive










Xeldena
Male
Virgo
78
64
Time Mage
Battle Skill
Regenerator
Beastmaster
Teleport

Sage Staff

Twist Headband
Silk Robe
Bracer

Haste, Haste 2, Float, Reflect, Demi 2, Stabilize Time, Meteor
Shield Break, Speed Break, Power Break, Justice Sword



Rislyeu
Male
Aries
61
72
Archer
Jump
Faith Save
Concentrate
Fly

Ultimus Bow

Holy Miter
Leather Outfit
Cherche

Charge+4, Charge+5, Charge+7, Charge+10
Level Jump2, Vertical Jump7



Grandlanzer
Female
Cancer
76
57
Lancer
Elemental
Counter
Equip Armor
Move-MP Up

Octagon Rod
Aegis Shield
Holy Miter
Mystic Vest
Bracer

Level Jump3, Vertical Jump7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

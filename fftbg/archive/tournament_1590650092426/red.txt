Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Male
Virgo
59
51
Geomancer
Throw
Brave Save
Short Charge
Jump+3

Heaven's Cloud
Buckler
Flash Hat
Leather Outfit
Reflect Ring

Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shuriken, Knife, Sword, Spear



HorusTaurus
Male
Aries
67
71
Lancer
Item
Counter Tackle
Equip Gun
Fly

Battle Folio
Gold Shield
Mythril Helmet
Wizard Robe
Power Wrist

Level Jump2, Vertical Jump6
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Remedy, Phoenix Down



LAGBOT30000
Male
Sagittarius
68
79
Thief
Punch Art
HP Restore
Equip Knife
Waterwalking

Thunder Rod

Flash Hat
Clothes
Small Mantle

Gil Taking, Steal Armor, Steal Weapon, Steal Accessory
Purification, Revive



HaychDub
Female
Serpentarius
78
47
Samurai
Black Magic
Regenerator
Concentrate
Move-HP Up

Asura Knife

Cross Helmet
Black Robe
Defense Ring

Asura, Murasame, Heaven's Cloud, Muramasa
Fire, Fire 2, Fire 3, Bolt, Ice 4, Empower, Flare

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Roqqqpsi
Female
Aquarius
61
40
Knight
White Magic
Earplug
Magic Attack UP
Waterbreathing

Defender
Round Shield
Leather Helmet
Silk Robe
N-Kai Armlet

Shield Break, Magic Break, Power Break, Justice Sword
Cure, Raise, Esuna



VolgraTheMoose
Male
Pisces
68
58
Monk
Draw Out
Damage Split
Dual Wield
Teleport



Flash Hat
Power Sleeve
Angel Ring

Purification, Chakra, Revive, Seal Evil
Asura, Koutetsu, Muramasa



JumbocactuarX27
Male
Gemini
50
68
Monk
Elemental
Hamedo
Defense UP
Move-HP Up



Feather Hat
Judo Outfit
Wizard Mantle

Spin Fist, Pummel, Secret Fist, Purification
Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Treafa
Male
Aquarius
40
74
Oracle
Black Magic
Counter Magic
Maintenance
Ignore Terrain

Octagon Rod

Black Hood
Adaman Vest
Red Shoes

Poison, Spell Absorb, Pray Faith, Silence Song, Foxbird, Confusion Song, Dispel Magic
Fire, Fire 4, Bolt 4, Ice 3, Ice 4, Empower, Flare

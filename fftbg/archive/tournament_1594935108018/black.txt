Player: !Black
Team: Black Team
Palettes: Black/Red



Creggers
Female
Leo
75
55
Mime

Absorb Used MP
Maintenance
Jump+3



Bronze Helmet
Adaman Vest
108 Gems

Mimic




Zagorsek
Female
Leo
77
52
Ninja
Charge
Speed Save
Sicken
Teleport

Orichalcum
Spell Edge
Feather Hat
Wizard Outfit
Cherche

Shuriken, Knife
Charge+1, Charge+3, Charge+7, Charge+10



CosmicTactician
Female
Taurus
73
48
Priest
Draw Out
Damage Split
Equip Axe
Move-MP Up

Wizard Staff

Black Hood
Chain Vest
Rubber Shoes

Cure 3, Raise, Reraise, Protect, Esuna
Koutetsu, Murasame, Kiyomori, Muramasa



EnemyController
Female
Leo
68
73
Mediator
Draw Out
Abandon
Maintenance
Waterwalking

Mythril Gun

Red Hood
Linen Robe
Feather Boots

Invitation, Praise, Threaten, Refute
Koutetsu, Murasame, Kikuichimoji

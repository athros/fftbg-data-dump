Final Bets: white - 12 bets for 4,554G (41.6%, x1.41); brown - 14 bets for 6,406G (58.4%, x0.71)

white bets:
argentbast: 1,000G (22.0%, 1,997G)
toka222: 800G (17.6%, 56,794G)
CosmicTactician: 500G (11.0%, 5,848G)
killth3kid: 500G (11.0%, 6,935G)
EnemyController: 500G (11.0%, 593,673G)
TheChainNerd: 350G (7.7%, 3,420G)
ColetteMSLP: 300G (6.6%, 1,985G)
placidphoenix: 204G (4.5%, 204G)
ko2q: 100G (2.2%, 8,557G)
Firesheath: 100G (2.2%, 12,120G)
DeathTaxesAndAnime: 100G (2.2%, 1,736G)
GrandmasterFrankerZ: 100G (2.2%, 4,182G)

brown bets:
VolgraTheMoose: 2,208G (34.5%, 42,208G)
HaateXIII: 1,150G (18.0%, 1,150G)
DAC169: 609G (9.5%, 1,196G)
mpghappiness: 500G (7.8%, 5,286G)
O_Heyno: 428G (6.7%, 428G)
biffcake01: 310G (4.8%, 310G)
Lythe_Caraker: 250G (3.9%, 110,930G)
Kyune: 250G (3.9%, 10,597G)
silentkaster: 185G (2.9%, 3,285G)
Alaquane: 116G (1.8%, 116G)
AllInBot: 100G (1.6%, 100G)
datadrivenbot: 100G (1.6%, 42,612G)
OneHundredFists: 100G (1.6%, 10,952G)
MadnessR: 100G (1.6%, 1,494G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Zagorsek
Male
Libra
45
74
Archer
Black Magic
Counter Tackle
Equip Polearm
Move-MP Up

Gungnir
Hero Shield
Leather Hat
Black Costume
Magic Ring

Charge+2
Fire 3, Ice 4, Empower



Evewho
Monster
Taurus
73
42
Wild Boar










Just Here2
Male
Sagittarius
38
41
Bard
White Magic
Catch
Maintenance
Jump+2

Bloody Strings

Ribbon
Chain Mail
Elf Mantle

Life Song, Cheer Song, Magic Song
Cure, Raise, Protect, Protect 2



Fluffskull
Female
Pisces
72
64
Chemist
Elemental
HP Restore
Concentrate
Move+1

Romanda Gun

Headgear
Black Costume
Spike Shoes

Hi-Potion, Ether, Antidote, Eye Drop, Soft, Remedy, Phoenix Down
Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lali Lulelo
Male
Leo
53
69
Calculator
White Magic
Critical Quick
Equip Bow
Jump+2

Mythril Bow

Red Hood
Linen Robe
Red Shoes

CT, 5, 4, 3
Cure 3, Cure 4, Protect, Esuna



Sage The Kitsune
Female
Capricorn
68
47
Ninja
Basic Skill
Brave Up
Concentrate
Move+3

Assassin Dagger
Cultist Dagger
Green Beret
Judo Outfit
Leather Mantle

Dictionary
Accumulate, Throw Stone, Heal, Wish, Scream



DLJuggernaut
Male
Taurus
51
59
Archer
Item
Speed Save
Equip Shield
Ignore Height

Night Killer
Round Shield
Triangle Hat
Adaman Vest
Leather Mantle

Charge+1, Charge+3, Charge+4, Charge+10
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Phoenix Down



Mayfu
Female
Leo
75
59
Lancer
Summon Magic
PA Save
Equip Knife
Ignore Height

Spell Edge
Flame Shield
Diamond Helmet
Gold Armor
Genji Gauntlet

Level Jump5, Vertical Jump5
Moogle, Shiva, Ramuh, Bahamut, Silf

Player: !Black
Team: Black Team
Palettes: Black/Red



FullStream
Male
Sagittarius
40
44
Archer
Black Magic
Parry
Doublehand
Levitate

Poison Bow

Golden Hairpin
Adaman Vest
Reflect Ring

Charge+2, Charge+3, Charge+4
Bolt, Ice 2, Empower



BlackFireUK
Female
Virgo
65
75
Mediator
Black Magic
Counter
Dual Wield
Waterbreathing

Mythril Gun
Romanda Gun
Holy Miter
Brigandine
Diamond Armlet

Invitation, Persuade, Praise, Solution, Mimic Daravon, Refute, Rehabilitate
Fire 3, Bolt, Bolt 2, Bolt 4



Fftbchamp
Male
Aquarius
69
55
Lancer
Steal
PA Save
Attack UP
Jump+1

Obelisk
Escutcheon
Barbuta
Chain Mail
Angel Ring

Level Jump4, Vertical Jump6
Gil Taking, Steal Weapon, Leg Aim



VirulenceXT
Female
Aquarius
76
71
Wizard
Jump
Parry
Defense UP
Fly

Thunder Rod

Flash Hat
Black Costume
Defense Ring

Fire 3, Bolt 3, Bolt 4, Ice 2, Death
Level Jump5, Vertical Jump7

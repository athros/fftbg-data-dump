Player: !Black
Team: Black Team
Palettes: Black/Red



Legitimized
Male
Aquarius
37
43
Monk
Draw Out
Mana Shield
Attack UP
Jump+1



Twist Headband
Adaman Vest
Rubber Shoes

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Koutetsu, Bizen Boat



TheMurkGnome
Male
Cancer
37
63
Geomancer
Battle Skill
Hamedo
Doublehand
Move-MP Up

Battle Axe

Flash Hat
Earth Clothes
Cursed Ring

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind
Head Break, Magic Break, Speed Break, Mind Break, Surging Sword



Geffro1908
Female
Leo
48
44
Summoner
Yin Yang Magic
Damage Split
Equip Axe
Move+1

Slasher

Golden Hairpin
Black Costume
Sprint Shoes

Moogle, Titan, Golem, Carbunkle, Bahamut, Salamander, Silf, Lich
Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze, Petrify



Upvla
Male
Sagittarius
60
62
Ninja
Battle Skill
Auto Potion
Attack UP
Fly

Short Edge
Ninja Edge
Cachusha
Brigandine
Genji Gauntlet

Shuriken
Head Break, Power Break, Mind Break, Justice Sword, Surging Sword

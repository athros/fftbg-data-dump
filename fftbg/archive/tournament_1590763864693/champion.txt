Player: !zChamp
Team: Champion Team
Palettes: Green/White



NWOW 44
Male
Aquarius
42
77
Mediator
Jump
Abandon
Magic Defense UP
Fly

Battle Folio

Holy Miter
Chain Vest
Power Wrist

Invitation, Persuade, Death Sentence, Negotiate, Rehabilitate
Level Jump2, Vertical Jump8



CosmicTactician
Female
Cancer
52
59
Oracle
Item
Faith Save
Dual Wield
Move+2

Papyrus Codex
Bestiary
Green Beret
Robe of Lords
Elf Mantle

Blind, Poison, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep, Dark Holy
Potion, X-Potion, Ether, Antidote, Eye Drop, Remedy, Phoenix Down



Powergems
Female
Sagittarius
55
56
Lancer
Punch Art
Catch
Dual Wield
Teleport

Mythril Spear
Holy Lance
Mythril Helmet
Chameleon Robe
N-Kai Armlet

Level Jump8, Vertical Jump7
Earth Slash, Purification, Revive, Seal Evil



Sinnyil2
Male
Leo
45
70
Lancer
Basic Skill
Caution
Dual Wield
Move+3

Javelin
Obelisk
Crystal Helmet
Mythril Armor
Small Mantle

Level Jump4, Vertical Jump5
Dash, Heal, Tickle, Yell, Wish

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



FreefallSirius
Male
Virgo
75
80
Time Mage
Talk Skill
Parry
Attack UP
Jump+3

Battle Folio

Thief Hat
Silk Robe
108 Gems

Haste, Stop, Stabilize Time, Galaxy Stop
Invitation, Threaten, Solution, Insult, Rehabilitate



Nofreewill
Male
Sagittarius
58
57
Priest
Black Magic
HP Restore
Equip Axe
Jump+3

White Staff

Black Hood
Power Sleeve
Germinas Boots

Cure 2, Cure 3, Raise 2, Reraise, Protect
Bolt 2, Bolt 4, Ice, Ice 3



StormclawGaming
Monster
Capricorn
57
40
Goblin










ShintaroNayaka
Female
Virgo
62
49
Calculator
Bio
Brave Up
Attack UP
Ignore Terrain

Papyrus Codex

Circlet
Carabini Mail
108 Gems

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis

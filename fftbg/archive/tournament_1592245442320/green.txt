Player: !Green
Team: Green Team
Palettes: Green/White



LDSkinny
Female
Pisces
76
79
Oracle
Throw
Arrow Guard
Equip Gun
Fly

Battle Folio

Flash Hat
Linen Robe
Diamond Armlet

Blind, Life Drain, Pray Faith, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep
Shuriken, Bomb, Knife, Hammer



KasugaiRoastedPeas
Female
Gemini
60
72
Archer
Basic Skill
Counter Tackle
Doublehand
Move-HP Up

Poison Bow

Black Hood
Clothes
Reflect Ring

Charge+2, Charge+4, Charge+7
Throw Stone, Heal



Serperemagus
Female
Capricorn
63
69
Ninja
Charge
Meatbone Slash
Magic Defense UP
Waterbreathing

Hidden Knife
Ninja Edge
Red Hood
Wizard Outfit
Defense Armlet

Bomb, Ninja Sword
Charge+1, Charge+3, Charge+4, Charge+5, Charge+10



DudeMonkey77
Female
Aries
69
38
Ninja
Charge
Dragon Spirit
Equip Sword
Fly

Rune Blade
Diamond Sword
Leather Hat
Earth Clothes
Defense Armlet

Shuriken, Bomb
Charge+2, Charge+3, Charge+4, Charge+5

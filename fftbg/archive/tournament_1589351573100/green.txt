Player: !Green
Team: Green Team
Palettes: Green/White



Aestheta
Female
Leo
79
75
Knight
Charge
Absorb Used MP
Beastmaster
Move-HP Up

Ragnarok
Mythril Shield
Bronze Helmet
Mythril Armor
Jade Armlet

Head Break, Weapon Break, Stasis Sword
Charge+4



Strifu
Monster
Capricorn
46
60
Ultima Demon










Dymntd
Male
Scorpio
65
78
Archer
White Magic
MA Save
Doublehand
Move+1

Silver Bow

Crystal Helmet
Earth Clothes
Wizard Mantle

Charge+2, Charge+20
Cure, Cure 3, Raise, Protect 2, Shell, Wall, Esuna, Magic Barrier



Sinnyil2
Male
Leo
49
55
Bard
Summon Magic
Catch
Secret Hunt
Jump+3

Ramia Harp

Golden Hairpin
Chain Mail
Battle Boots

Life Song, Cheer Song, Magic Song, Diamond Blade, Hydra Pit
Moogle, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Cyclops

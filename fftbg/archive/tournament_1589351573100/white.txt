Player: !White
Team: White Team
Palettes: White/Blue



Laserman1000
Male
Sagittarius
69
80
Ninja
Time Magic
Brave Up
Defense UP
Teleport

Spell Edge
Sasuke Knife
Golden Hairpin
Wizard Outfit
Vanish Mantle

Shuriken, Bomb, Dictionary
Haste, Slow 2, Stop, Immobilize, Reflect, Demi, Demi 2, Stabilize Time



DaveStrider55
Male
Pisces
77
71
Thief
Throw
Mana Shield
Beastmaster
Move-HP Up

Broad Sword

Holy Miter
Power Sleeve
Power Wrist

Gil Taking, Steal Armor, Arm Aim
Shuriken



SirCherish
Male
Aquarius
51
60
Samurai
Punch Art
Brave Up
Short Charge
Waterwalking

Murasame

Mythril Helmet
Platinum Armor
Battle Boots

Asura, Kiyomori, Muramasa
Wave Fist, Earth Slash, Purification, Chakra



UmaiJam
Female
Sagittarius
61
53
Archer
Steal
Absorb Used MP
Doublehand
Jump+2

Blast Gun

Red Hood
Leather Outfit
Power Wrist

Charge+2, Charge+3, Charge+4, Charge+20
Steal Armor, Steal Shield, Steal Accessory, Steal Status, Arm Aim, Leg Aim

Final Bets: green - 10 bets for 11,409G (67.6%, x0.48); yellow - 9 bets for 5,480G (32.4%, x2.08)

green bets:
Lordminsc: 5,483G (48.1%, 10,967G)
Aldrammech: 1,933G (16.9%, 1,933G)
HaplessOne: 1,234G (10.8%, 19,915G)
volgrathemoose: 920G (8.1%, 1,841G)
Rislyeu: 698G (6.1%, 698G)
Breakdown777: 500G (4.4%, 16,992G)
ungabunga_bot: 241G (2.1%, 314,738G)
sukotsuto: 200G (1.8%, 2,507G)
Langrisser: 100G (0.9%, 6,750G)
datadrivenbot: 100G (0.9%, 11,967G)

yellow bets:
Shakarak: 1,000G (18.2%, 15,179G)
BirbBrainsBot: 1,000G (18.2%, 170,870G)
catfashions: 1,000G (18.2%, 3,675G)
Mesmaster: 1,000G (18.2%, 70,513G)
Lionhermit: 500G (9.1%, 35,987G)
gooseyourself: 428G (7.8%, 428G)
getthemoneyz: 252G (4.6%, 614,290G)
Mudrockk: 200G (3.6%, 525G)
maakur_: 100G (1.8%, 118,054G)

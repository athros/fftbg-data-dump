Player: !Red
Team: Red Team
Palettes: Red/Brown



Hydroshade
Male
Cancer
69
58
Bard
Time Magic
Counter
Equip Axe
Jump+2

Wizard Staff

Twist Headband
Clothes
Magic Gauntlet

Life Song, Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade
Stop, Reflect, Quick, Demi, Stabilize Time



RunicMagus
Female
Aries
54
72
Oracle
Item
Sunken State
Doublehand
Jump+1

Ivory Rod

Holy Miter
Adaman Vest
Jade Armlet

Spell Absorb, Pray Faith, Blind Rage, Foxbird, Confusion Song, Sleep, Petrify
X-Potion, Ether, Soft, Remedy, Phoenix Down



Hales Bopp It
Male
Leo
42
47
Oracle
Battle Skill
Counter Tackle
Sicken
Jump+3

Iron Fan

Holy Miter
Chain Vest
Wizard Mantle

Poison, Doubt Faith, Zombie, Dispel Magic, Sleep
Shield Break, Power Break, Mind Break



Mesmaster
Male
Sagittarius
74
56
Lancer
Sing
Sunken State
Attack UP
Levitate

Holy Lance
Diamond Shield
Gold Helmet
Chain Mail
Feather Mantle

Level Jump4, Vertical Jump8
Life Song, Magic Song

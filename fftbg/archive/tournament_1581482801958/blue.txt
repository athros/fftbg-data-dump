Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Godsblackarm
Male
Taurus
72
69
Knight
Basic Skill
Damage Split
Doublehand
Move-HP Up

Nagrarock

Mythril Helmet
Linen Cuirass
Bracer

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break
Accumulate, Dash, Heal, Tickle, Scream



Plasticcanvas
Male
Virgo
80
80
Thief
Jump
MA Save
Concentrate
Waterwalking

Assassin Dagger

Diamond Helmet
Mystic Vest
Cursed Ring

Steal Helmet, Steal Shield, Steal Accessory, Arm Aim
Level Jump8, Vertical Jump6



Gabbagooluigi
Female
Taurus
52
43
Ninja
Jump
Counter
Defense UP
Waterbreathing

Short Edge
Kunai
Headgear
Chain Vest
Feather Mantle

Shuriken, Bomb, Dictionary
Level Jump5, Vertical Jump8



NoxeGS
Male
Libra
58
69
Knight
Yin Yang Magic
MP Restore
Equip Shield
Lava Walking

Rune Blade
Gold Shield
Cross Helmet
Leather Armor
Wizard Mantle

Armor Break, Speed Break, Power Break, Stasis Sword, Dark Sword
Blind, Doubt Faith, Dispel Magic, Sleep

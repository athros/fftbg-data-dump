Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lawnboxer
Male
Virgo
73
62
Thief
Draw Out
Mana Shield
Equip Polearm
Swim

Whale Whisker

Red Hood
Adaman Vest
Sprint Shoes

Gil Taking, Steal Helmet, Steal Accessory, Arm Aim
Koutetsu, Bizen Boat, Murasame, Kiyomori, Kikuichimoji



Red Lancer
Male
Libra
58
74
Calculator
Time Magic
HP Restore
Equip Sword
Move-MP Up

Defender

Twist Headband
Judo Outfit
Genji Gauntlet

CT, 5, 3
Haste, Haste 2, Slow, Float, Quick, Stabilize Time



Farshadows
Female
Cancer
64
55
Monk
Elemental
Meatbone Slash
Defend
Move-MP Up



Red Hood
Black Costume
Spike Shoes

Wave Fist, Earth Slash, Secret Fist, Purification
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball



Winterharte
Female
Pisces
64
68
Wizard
Draw Out
PA Save
Magic Defense UP
Teleport

Flame Rod

Leather Hat
Power Sleeve
Jade Armlet

Fire, Fire 3, Ice, Empower, Frog
Asura, Koutetsu, Murasame

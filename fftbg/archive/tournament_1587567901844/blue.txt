Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Hamborn
Female
Taurus
48
55
Dancer
White Magic
Brave Up
Short Charge
Move+1

Ryozan Silk

Leather Hat
Wizard Robe
Genji Gauntlet

Slow Dance, Nameless Dance, Last Dance, Obsidian Blade, Void Storage, Nether Demon, Dragon Pit
Cure, Cure 2, Raise 2, Protect, Protect 2, Shell, Wall, Esuna



DLJuggernaut
Male
Libra
48
60
Mediator
Sing
Dragon Spirit
Equip Sword
Jump+2

Mythril Sword

Black Hood
Mystic Vest
108 Gems

Praise, Threaten, Preach, Solution, Insult, Negotiate, Refute, Rehabilitate
Magic Song, Space Storage, Sky Demon



Daveb
Female
Aries
54
59
Ninja
Battle Skill
Earplug
Secret Hunt
Move-HP Up

Kunai
Sasuke Knife
Red Hood
Chain Vest
Feather Mantle

Shuriken, Bomb, Spear
Mind Break



B0shii
Male
Aquarius
55
77
Priest
Summon Magic
Absorb Used MP
Equip Gun
Retreat

Bestiary

Twist Headband
Judo Outfit
Red Shoes

Cure 3, Cure 4, Raise, Protect, Shell, Wall, Esuna, Holy
Moogle, Titan, Golem, Carbunkle, Silf, Fairy

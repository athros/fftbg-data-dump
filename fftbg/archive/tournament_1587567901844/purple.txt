Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ANFz
Male
Cancer
63
69
Mediator
Time Magic
Abandon
Equip Sword
Swim

Long Sword

Twist Headband
Chain Vest
Dracula Mantle

Persuade, Threaten, Insult, Negotiate, Refute
Haste, Haste 2, Reflect, Quick, Demi 2



Baconbacon1207
Male
Leo
70
71
Priest
Charge
Sunken State
Short Status
Move-MP Up

Flame Whip

Leather Hat
Wizard Outfit
Dracula Mantle

Cure, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect 2, Shell, Esuna
Charge+7, Charge+10



Nizaha
Female
Cancer
40
63
Oracle
Dance
Faith Up
Short Charge
Fly

Battle Folio

Twist Headband
White Robe
N-Kai Armlet

Poison, Spell Absorb, Life Drain, Zombie, Blind Rage, Dispel Magic, Sleep, Dark Holy
Slow Dance, Disillusion, Obsidian Blade, Void Storage



Nifboy
Male
Sagittarius
65
61
Mediator
Jump
Damage Split
Secret Hunt
Waterbreathing

Glacier Gun

Flash Hat
Leather Outfit
N-Kai Armlet

Invitation, Persuade, Preach, Solution, Negotiate, Refute
Level Jump4, Vertical Jump2

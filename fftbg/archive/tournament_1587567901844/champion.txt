Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Go2sleepTV
Female
Pisces
44
62
Summoner
Steal
Earplug
Short Charge
Move+3

Flame Rod

Golden Hairpin
Wizard Robe
Angel Ring

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Leviathan, Salamander, Silf, Fairy, Lich, Cyclops
Gil Taking, Steal Heart, Steal Shield, Steal Weapon



MirtaiAtana
Female
Cancer
73
80
Dancer
Time Magic
Regenerator
Equip Knife
Fly

Ice Rod

Thief Hat
Chain Vest
N-Kai Armlet

Nameless Dance, Last Dance
Haste, Slow 2, Reflect, Quick, Demi, Demi 2



Vicemond
Male
Leo
54
64
Lancer
Punch Art
MA Save
Magic Defense UP
Lava Walking

Gungnir
Escutcheon
Cross Helmet
Chain Mail
Genji Gauntlet

Level Jump2, Vertical Jump7
Pummel, Wave Fist, Secret Fist, Purification, Revive, Seal Evil



LDSkinny
Female
Scorpio
75
69
Oracle
Charge
PA Save
Long Status
Levitate

Iron Fan

Holy Miter
Earth Clothes
Elf Mantle

Blind, Life Drain, Pray Faith, Zombie, Blind Rage, Sleep
Charge+5, Charge+7

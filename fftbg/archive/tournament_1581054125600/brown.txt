Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Boefghut
Male
Scorpio
60
60
Squire
Draw Out
Brave Up
Magic Defense UP
Move+2

Broad Sword

Headgear
Adaman Vest
Diamond Armlet

Heal, Wish
Koutetsu, Bizen Boat, Muramasa



SirOwnzAlot
Female
Aquarius
59
61
Oracle
Battle Skill
Faith Up
Defend
Move-HP Up

Octagon Rod

Holy Miter
Black Robe
Wizard Mantle

Poison, Life Drain, Dispel Magic, Dark Holy
Armor Break, Shield Break, Speed Break, Power Break



Neversye
Female
Gemini
43
50
Monk
White Magic
Abandon
Equip Sword
Lava Walking

Heaven's Cloud

Triangle Hat
Brigandine
Defense Ring

Purification
Cure, Raise, Protect, Shell, Shell 2



Dreygyron
Male
Aquarius
50
80
Calculator
Byblos
Abandon
Dual Wield
Retreat

Madlemgen
Bestiary
Twist Headband
Carabini Mail
N-Kai Armlet

Blue Magic
Energize, Parasite, Vengeance, Manaburn, Chicken

Final Bets: yellow - 15 bets for 13,595G (30.2%, x2.31); champion - 23 bets for 31,404G (69.8%, x0.43)

yellow bets:
sinnyil2: 6,441G (47.4%, 12,883G)
WireLord: 2,000G (14.7%, 43,601G)
ungabunga_bot: 1,000G (7.4%, 108,428G)
BirbBrainsBot: 1,000G (7.4%, 83,510G)
Maldoree: 904G (6.6%, 904G)
Lanshaft: 600G (4.4%, 6,713G)
DudeMonkey77: 333G (2.4%, 3,865G)
ForestMagi: 330G (2.4%, 330G)
Chuckolator: 223G (1.6%, 4,032G)
Tithonus: 200G (1.5%, 12,941G)
nifboy: 164G (1.2%, 164G)
AllInBot: 100G (0.7%, 100G)
Lordminsc: 100G (0.7%, 8,542G)
sect_cor: 100G (0.7%, 20,320G)
ApplesauceBoss: 100G (0.7%, 1,308G)

champion bets:
SolarisFall: 8,768G (27.9%, 17,537G)
RongRongArts: 5,582G (17.8%, 5,582G)
arch8000: 3,251G (10.4%, 3,251G)
Aldrammech: 2,244G (7.1%, 2,244G)
CapnChaos12: 2,049G (6.5%, 2,049G)
Tougou: 1,770G (5.6%, 1,770G)
silentperogy: 1,000G (3.2%, 16,961G)
SkepticalMinotaur: 1,000G (3.2%, 5,821G)
roofiepops: 943G (3.0%, 943G)
datadrivenbot: 722G (2.3%, 23,012G)
volgrathemoose: 605G (1.9%, 605G)
Zeroroute: 600G (1.9%, 600G)
Laserman1000: 586G (1.9%, 11,486G)
dennarai: 500G (1.6%, 34,717G)
fenaen: 460G (1.5%, 460G)
TheMurkGnome: 300G (1.0%, 2,040G)
JackOnFire1: 252G (0.8%, 252G)
b0shii: 222G (0.7%, 1,055G)
Langrisser: 150G (0.5%, 5,153G)
Chompie: 100G (0.3%, 7,076G)
Heroebal: 100G (0.3%, 2,302G)
DrAntiSocial: 100G (0.3%, 1,700G)
ZephyrTempest: 100G (0.3%, 53,604G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Brokenknight201
Monster
Cancer
45
72
Apanda










CapnChaos12
Female
Leo
40
56
Summoner
Basic Skill
Arrow Guard
Short Charge
Move-HP Up

Thunder Rod

Cachusha
Black Costume
Angel Ring

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan, Silf, Fairy
Dash, Throw Stone, Tickle, Cheer Up



Lydian C
Female
Capricorn
74
41
Oracle
Math Skill
Damage Split
Sicken
Move+1

Cypress Rod

Triangle Hat
Clothes
Diamond Armlet

Poison, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Dark Holy
CT, Height, Prime Number, 5, 4



Ruleof5
Female
Gemini
62
58
Knight
Punch Art
HP Restore
Equip Armor
Move-MP Up

Defender
Mythril Shield
Ribbon
Reflect Mail
Angel Ring

Head Break, Armor Break, Magic Break, Speed Break, Justice Sword, Night Sword, Surging Sword
Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil

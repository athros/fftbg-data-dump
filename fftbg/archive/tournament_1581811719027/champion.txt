Player: !zChamp
Team: Champion Team
Palettes: Green/White



FACADE8634
Male
Leo
46
57
Geomancer
Item
Caution
Doublehand
Jump+1

Battle Axe

Green Beret
Wizard Outfit
Small Mantle

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Potion, Echo Grass, Remedy, Phoenix Down



Psychomidget
Female
Gemini
79
75
Priest
Battle Skill
Blade Grasp
Short Status
Move+1

Morning Star

Golden Hairpin
Adaman Vest
Power Wrist

Cure, Cure 3, Raise, Reraise, Regen
Head Break, Shield Break, Weapon Break, Power Break



Masara11
Male
Aries
56
70
Summoner
Punch Art
Earplug
Equip Axe
Teleport

Scorpion Tail

Green Beret
Wizard Robe
Defense Ring

Ramuh, Odin, Leviathan, Silf, Fairy, Lich
Earth Slash, Purification, Chakra, Revive, Seal Evil



Maldoree
Monster
Aries
37
75
Red Dragon








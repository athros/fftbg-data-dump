Player: !zChamp
Team: Champion Team
Palettes: Green/White



TheChainNerd
Male
Aquarius
63
80
Knight
Sing
Mana Shield
Beastmaster
Move-MP Up

Ancient Sword
Diamond Shield
Barbuta
Linen Cuirass
N-Kai Armlet

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Dark Sword, Surging Sword
Angel Song, Magic Song, Last Song



Serperemagus
Male
Gemini
52
77
Samurai
Throw
Meatbone Slash
Martial Arts
Retreat

Obelisk

Crystal Helmet
Bronze Armor
Rubber Shoes

Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji
Bomb



Gooseyourself
Female
Aquarius
62
79
Summoner
Elemental
Auto Potion
Secret Hunt
Levitate

Papyrus Codex

Leather Hat
White Robe
Defense Armlet

Moogle, Shiva, Ramuh, Golem, Bahamut, Silf, Fairy
Pitfall, Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Daveb
Female
Gemini
75
36
Ninja
Charge
Damage Split
Equip Polearm
Ignore Terrain

Ryozan Silk
Octagon Rod
Thief Hat
Earth Clothes
Defense Armlet

Shuriken, Bomb
Charge+1, Charge+10

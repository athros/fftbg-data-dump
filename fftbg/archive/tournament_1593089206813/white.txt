Player: !White
Team: White Team
Palettes: White/Blue



Run With Stone GUNs
Male
Aquarius
73
62
Monk
Jump
Parry
Equip Gun
Jump+3

Ramia Harp

Black Hood
Earth Clothes
Defense Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive
Level Jump8, Vertical Jump8



CosmicTactician
Female
Pisces
46
77
Thief
Elemental
Auto Potion
Equip Bow
Waterbreathing

Hunting Bow

Flash Hat
Mystic Vest
Feather Mantle

Steal Heart, Steal Accessory
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard



LDSkinny
Female
Pisces
49
61
Oracle
Summon Magic
MP Restore
Equip Shield
Move-HP Up

Gokuu Rod
Platinum Shield
Headgear
White Robe
Germinas Boots

Poison, Spell Absorb, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Paralyze
Shiva, Carbunkle, Silf, Fairy, Cyclops



Lastly
Male
Cancer
52
79
Chemist
Charge
Distribute
Equip Shield
Waterwalking

Panther Bag
Crystal Shield
Flash Hat
Clothes
Leather Mantle

Potion, X-Potion, Phoenix Down
Charge+5

Final Bets: purple - 22 bets for 17,352G (47.6%, x1.10); brown - 32 bets for 19,094G (52.4%, x0.91)

purple bets:
Virilikus: 5,000G (28.8%, 157,619G)
LivingHitokiri: 2,142G (12.3%, 2,142G)
volgrathemoose: 1,213G (7.0%, 1,213G)
Lydian_C: 1,200G (6.9%, 74,431G)
Baron_von_Scrub: 1,111G (6.4%, 80,097G)
itzover9001: 1,000G (5.8%, 61,087G)
braumbles: 1,000G (5.8%, 86,907G)
JackOnFire1: 800G (4.6%, 800G)
Laserman1000: 600G (3.5%, 49,900G)
XalSmellsBad: 500G (2.9%, 8,474G)
Blain_Cooper: 500G (2.9%, 2,384G)
OmnibotGamma: 500G (2.9%, 2,488G)
ewan_e: 500G (2.9%, 1,069G)
BroHamma: 300G (1.7%, 8,877G)
FoeSquirrel: 231G (1.3%, 463G)
killth3kid: 204G (1.2%, 19,838G)
ZephyrTempest: 101G (0.6%, 183,082G)
AllInBot: 100G (0.6%, 100G)
Jerboj: 100G (0.6%, 53,811G)
SteeleIT: 100G (0.6%, 100G)
mirapoix: 100G (0.6%, 4,313G)
ThunderGodKhan: 50G (0.3%, 240G)

brown bets:
HaplessOne: 2,222G (11.6%, 131,520G)
LAGBOT30000: 2,196G (11.5%, 43,932G)
GreatRedDragon: 2,037G (10.7%, 51,329G)
Pie108: 2,000G (10.5%, 88,647G)
J2DaBibbles: 1,500G (7.9%, 19,853G)
ungabunga_bot: 1,000G (5.2%, 214,184G)
BirbBrainsBot: 1,000G (5.2%, 142,349G)
benticore: 1,000G (5.2%, 2,526G)
gooseyourself: 864G (4.5%, 864G)
reinoe: 600G (3.1%, 600G)
Nizaha: 501G (2.6%, 14,204G)
SwaffleWaffles: 500G (2.6%, 64,828G)
gorgewall: 500G (2.6%, 7,872G)
Dymntd: 500G (2.6%, 2,385G)
evontno: 456G (2.4%, 456G)
Lanshaft: 368G (1.9%, 4,344G)
getthemoneyz: 214G (1.1%, 566,808G)
KasugaiRoastedPeas: 200G (1.0%, 7,408G)
EnemyController: 200G (1.0%, 90,971G)
trigger15: 150G (0.8%, 582G)
RunicMagus: 100G (0.5%, 33,882G)
ProdigyAzn: 100G (0.5%, 397G)
duskps: 100G (0.5%, 10,570G)
FriendSquirrel: 100G (0.5%, 35,319G)
avathesunqueen: 100G (0.5%, 100G)
datadrivenbot: 100G (0.5%, 2,393G)
aznwanderor: 100G (0.5%, 2,137G)
Kiroy12: 100G (0.5%, 1,149G)
Evewho: 100G (0.5%, 3,883G)
Rikrizen: 70G (0.4%, 1,411G)
b0shii: 66G (0.3%, 5,055G)
DrAntiSocial: 50G (0.3%, 25,122G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Fenaen
Monster
Aries
67
45
Cockatrice










Firesheath
Male
Aries
66
48
Mime

Counter Flood
Long Status
Move+2



Black Hood
Adaman Vest
Dracula Mantle

Mimic




Rinhander
Female
Sagittarius
78
44
Mime

Arrow Guard
Secret Hunt
Swim



Black Hood
Black Costume
Wizard Mantle

Mimic




Ko2q
Female
Libra
79
69
Summoner
Charge
Counter Flood
Short Charge
Waterbreathing

Wizard Rod

Headgear
Light Robe
Small Mantle

Moogle, Shiva, Ifrit, Titan, Golem, Carbunkle, Silf, Fairy
Charge+1, Charge+2, Charge+4

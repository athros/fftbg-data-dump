Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sotehr
Female
Aquarius
75
53
Summoner
Yin Yang Magic
Damage Split
Short Charge
Fly

Panther Bag

Leather Hat
Silk Robe
Diamond Armlet

Shiva, Ramuh, Titan, Golem, Bahamut, Leviathan, Fairy
Blind, Poison, Life Drain, Doubt Faith, Confusion Song, Paralyze



IndecisiveNinja
Monster
Leo
41
79
Steel Giant










Goust18
Female
Libra
43
58
Geomancer
Throw
Damage Split
Concentrate
Ignore Height

Bizen Boat
Ice Shield
Black Hood
Mythril Vest
Dracula Mantle

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand
Knife



Bpc2163
Female
Aries
66
49
Wizard
Talk Skill
Counter
Short Charge
Move+1

Rod

Triangle Hat
Linen Robe
Reflect Ring

Fire 3, Bolt, Bolt 3, Ice, Ice 4
Invitation, Persuade, Insult

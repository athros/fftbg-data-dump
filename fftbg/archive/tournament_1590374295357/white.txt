Player: !White
Team: White Team
Palettes: White/Blue



Mardokttv
Male
Gemini
69
65
Calculator
Limit
Speed Save
Martial Arts
Retreat



Iron Helmet
Linen Cuirass
Reflect Ring

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



Meta Five
Male
Sagittarius
49
53
Mime

Caution
Magic Defense UP
Move-MP Up



Leather Hat
Mystic Vest
Dracula Mantle

Mimic




Firesheath
Male
Capricorn
41
77
Summoner
Jump
MA Save
Long Status
Ignore Terrain

Papyrus Codex

Leather Hat
Black Robe
Magic Gauntlet

Moogle, Ifrit, Golem, Carbunkle, Leviathan, Salamander
Level Jump8, Vertical Jump7



Digitalsocrates
Male
Pisces
62
61
Samurai
Elemental
Parry
Equip Shield
Jump+2

Partisan
Escutcheon
Diamond Helmet
Wizard Robe
Wizard Mantle

Koutetsu, Murasame, Heaven's Cloud
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



O Heyno
Female
Libra
70
55
Time Mage
Charge
Counter
Halve MP
Move-MP Up

Mace of Zeus

Triangle Hat
Silk Robe
Angel Ring

Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi 2, Stabilize Time, Meteor
Charge+1, Charge+5, Charge+7, Charge+10



Vorap
Male
Pisces
71
73
Lancer
Charge
Counter Magic
Long Status
Move-MP Up

Mythril Spear
Kaiser Plate
Genji Helmet
Leather Armor
Sprint Shoes

Level Jump4, Vertical Jump8
Charge+3, Charge+4, Charge+20



Eyepoor
Female
Scorpio
57
47
Ninja
White Magic
Counter Magic
Equip Knife
Move-HP Up

Main Gauche
Cultist Dagger
Golden Hairpin
Judo Outfit
Bracer

Bomb, Knife, Sword
Raise, Raise 2, Reraise, Regen, Protect 2, Shell 2, Wall, Esuna



DHaveWord
Female
Sagittarius
71
71
Monk
Battle Skill
Absorb Used MP
Equip Axe
Lava Walking

Healing Staff

Green Beret
Black Costume
Chantage

Spin Fist, Purification
Armor Break, Shield Break, Speed Break, Mind Break, Explosion Sword

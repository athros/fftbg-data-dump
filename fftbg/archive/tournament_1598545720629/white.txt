Player: !White
Team: White Team
Palettes: White/Blue



Pplvee1
Male
Cancer
47
70
Ninja
Item
Dragon Spirit
Equip Knife
Move-HP Up

Blind Knife
Hidden Knife
Leather Hat
Power Sleeve
Feather Boots

Shuriken, Stick
Ether, Hi-Ether, Maiden's Kiss, Holy Water, Phoenix Down



Ar Tactic
Male
Gemini
75
68
Archer
Time Magic
Counter Magic
Dual Wield
Move+1

Lightning Bow

Triangle Hat
Wizard Outfit
Defense Ring

Charge+5
Haste, Haste 2, Stop, Stabilize Time



ZCKaiser
Female
Capricorn
76
55
Ninja
Talk Skill
Mana Shield
Maintenance
Move-MP Up

Koga Knife
Ninja Edge
Ribbon
Mythril Vest
Leather Mantle

Shuriken
Solution, Death Sentence, Insult



Bruubarg
Female
Capricorn
67
74
Geomancer
Basic Skill
Parry
Secret Hunt
Retreat

Giant Axe
Gold Shield
Feather Hat
White Robe
Vanish Mantle

Pitfall, Hell Ivy, Static Shock, Quicksand, Blizzard, Lava Ball
Accumulate, Heal, Yell, Fury

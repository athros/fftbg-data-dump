Player: !Green
Team: Green Team
Palettes: Green/White



O Heyno
Male
Gemini
78
57
Monk
White Magic
MP Restore
Equip Bow
Move+1

Ice Bow

Headgear
Rubber Costume
108 Gems

Spin Fist, Purification, Chakra, Seal Evil
Raise, Reraise, Wall, Esuna



BStarTV
Male
Virgo
64
66
Archer
Time Magic
Absorb Used MP
Doublehand
Waterwalking

Windslash Bow

Leather Helmet
Leather Outfit
Red Shoes

Charge+1, Charge+2, Charge+4
Haste, Immobilize, Quick, Demi, Demi 2



DejaPoo21
Male
Libra
70
53
Lancer
Sing
Critical Quick
Concentrate
Jump+2

Javelin
Bronze Shield
Crystal Helmet
Bronze Armor
Sprint Shoes

Level Jump8, Vertical Jump5
Life Song, Cheer Song, Hydra Pit



Just Here2
Male
Capricorn
65
38
Knight
Summon Magic
Arrow Guard
Magic Defense UP
Swim

Cute Bag
Diamond Shield
Cross Helmet
Bronze Armor
Feather Boots

Armor Break, Magic Break, Power Break, Night Sword
Ramuh, Carbunkle, Leviathan, Cyclops

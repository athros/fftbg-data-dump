Player: !Green
Team: Green Team
Palettes: Green/White



UmaiJam
Female
Serpentarius
50
56
Mediator
Elemental
Speed Save
Long Status
Levitate

Papyrus Codex

Holy Miter
Black Costume
Spike Shoes

Praise, Negotiate, Refute
Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard



Dynasti
Male
Sagittarius
81
49
Ninja
Charge
Counter Flood
Equip Gun
Move-MP Up

Madlemgen
Bestiary
Headgear
Judo Outfit
Wizard Mantle

Shuriken, Bomb
Charge+1, Charge+7, Charge+20



HysteriaDays
Male
Sagittarius
43
51
Archer
Sing
Auto Potion
Dual Wield
Move-HP Up

Windslash Bow

Twist Headband
Chain Vest
Angel Ring

Charge+1, Charge+3, Charge+20
Life Song, Magic Song, Diamond Blade, Sky Demon



Basmal
Male
Sagittarius
77
44
Summoner
Punch Art
Counter Flood
Long Status
Jump+2

Healing Staff

Flash Hat
Wizard Robe
Rubber Shoes

Moogle, Ifrit, Golem, Carbunkle, Salamander, Silf, Fairy, Lich, Cyclops
Pummel, Wave Fist, Secret Fist, Purification, Revive

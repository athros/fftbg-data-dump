Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



LanseDM
Male
Taurus
79
71
Ninja
Punch Art
Parry
Short Charge
Move-MP Up

Spell Edge
Ninja Edge
Ribbon
Chain Vest
Cursed Ring

Sword, Staff, Dictionary
Purification, Revive



Sypheck
Monster
Virgo
45
57
Juravis










RRazza
Male
Cancer
64
47
Ninja
Draw Out
Parry
Equip Bow
Jump+3

Hunting Bow
Night Killer
Golden Hairpin
Earth Clothes
Cursed Ring

Shuriken, Bomb
Koutetsu



Alc Trinity
Male
Sagittarius
66
56
Thief
Battle Skill
Auto Potion
Equip Bow
Fly

Hunting Bow

Black Hood
Chain Vest
Germinas Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Shield Break, Speed Break, Power Break, Mind Break, Justice Sword

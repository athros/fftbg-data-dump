Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CosmicTactician
Female
Gemini
59
43
Chemist
Talk Skill
Distribute
Maintenance
Jump+2

Orichalcum

Triangle Hat
Black Costume
Reflect Ring

Hi-Potion, Eye Drop, Echo Grass, Phoenix Down
Praise, Death Sentence, Insult, Refute, Rehabilitate



ExecutedGiraffe
Female
Virgo
79
53
Mediator
Battle Skill
Abandon
Defend
Levitate

Bestiary

Flash Hat
Rubber Costume
Leather Mantle

Persuade, Solution, Insult, Rehabilitate
Weapon Break, Speed Break, Stasis Sword, Justice Sword, Night Sword, Surging Sword, Explosion Sword



Roqqqpsi
Female
Pisces
59
44
Time Mage
Draw Out
Counter
Short Charge
Move+3

Ivory Rod

Twist Headband
White Robe
Reflect Ring

Haste 2, Slow 2, Stop, Quick, Demi 2, Stabilize Time
Koutetsu, Bizen Boat, Kiyomori



TheChainNerd
Female
Libra
43
57
Knight
Jump
Counter Tackle
Defend
Waterbreathing

Broad Sword
Crystal Shield
Cross Helmet
Linen Cuirass
N-Kai Armlet

Head Break, Shield Break, Weapon Break, Speed Break, Power Break, Justice Sword
Level Jump4, Vertical Jump2

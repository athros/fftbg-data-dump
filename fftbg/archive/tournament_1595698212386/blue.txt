Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nekojin
Male
Libra
60
43
Mime

Earplug
Martial Arts
Move-MP Up



Flash Hat
Mystic Vest
Genji Gauntlet

Mimic




Dtrain332
Male
Virgo
79
41
Summoner
Time Magic
Blade Grasp
Long Status
Move+1

Thunder Rod

Red Hood
Silk Robe
Dracula Mantle

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Leviathan, Lich, Cyclops
Haste, Haste 2, Demi 2



Xoomwaffle
Male
Gemini
63
77
Ninja
White Magic
Counter Tackle
Equip Sword
Teleport 2

Ragnarok
Koutetsu Knife
Twist Headband
Mythril Vest
Power Wrist

Shuriken, Bomb, Wand, Dictionary
Cure, Cure 3, Raise, Regen



Lanshaft
Male
Scorpio
71
65
Mediator
Charge
Regenerator
Secret Hunt
Move+2

Battle Folio

Golden Hairpin
Silk Robe
Genji Gauntlet

Persuade, Solution, Death Sentence, Insult, Refute
Charge+1, Charge+4, Charge+7

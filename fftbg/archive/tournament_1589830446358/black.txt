Player: !Black
Team: Black Team
Palettes: Black/Red



Lanshaft
Female
Sagittarius
72
43
Calculator
Black Magic
Counter Magic
Equip Axe
Teleport

Battle Axe

Twist Headband
Light Robe
Reflect Ring

CT, Prime Number, 5, 4, 3
Fire, Bolt, Empower, Flare



Killth3kid
Female
Capricorn
45
80
Ninja
Charge
Mana Shield
Equip Bow
Ignore Terrain

Ice Bow

Holy Miter
Chain Vest
Spike Shoes

Shuriken
Charge+4, Charge+5, Charge+7, Charge+10



Firesheath
Female
Cancer
52
38
Ninja
Talk Skill
Mana Shield
Secret Hunt
Move+3

Hidden Knife
Spell Edge
Golden Hairpin
Judo Outfit
Cursed Ring

Shuriken, Knife
Invitation, Threaten, Preach, Solution, Insult, Negotiate, Refute



Run With Stone GUNs
Male
Gemini
40
63
Archer
Elemental
Counter Tackle
Doublehand
Waterbreathing

Blast Gun

Feather Hat
Mystic Vest
Leather Mantle

Charge+1, Charge+4, Charge+5
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard

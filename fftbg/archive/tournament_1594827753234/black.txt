Player: !Black
Team: Black Team
Palettes: Black/Red



CapnChaos12
Female
Leo
73
41
Summoner
Charge
Caution
Short Charge
Swim

Madlemgen

Headgear
Power Sleeve
Defense Ring

Moogle, Shiva, Ramuh, Golem, Odin, Zodiac
Charge+3, Charge+4, Charge+5, Charge+7, Charge+20



TasisSai
Monster
Gemini
46
46
Iron Hawk










Seef2208
Male
Sagittarius
52
58
Wizard
Draw Out
Meatbone Slash
Equip Polearm
Teleport 2

Octagon Rod

Twist Headband
Chain Vest
Feather Mantle

Bolt, Bolt 2, Bolt 4, Ice 4, Frog
Koutetsu, Bizen Boat, Kiyomori



Xoomwaffle
Female
Libra
48
43
Samurai
Item
Parry
Long Status
Jump+2

Heaven's Cloud

Barbuta
Bronze Armor
Bracer

Bizen Boat, Kiyomori, Muramasa
Ether, Hi-Ether, Soft, Holy Water, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



KyleWonToLiveForever
Male
Leo
72
62
Wizard
White Magic
Counter Flood
Beastmaster
Move-HP Up

Air Knife

Red Hood
Black Costume
Diamond Armlet

Fire, Fire 2, Fire 4, Ice 2, Frog, Flare
Cure, Cure 2, Raise, Regen, Protect, Shell 2, Esuna



Kronikle
Female
Capricorn
68
69
Samurai
Charge
HP Restore
Magic Attack UP
Move-MP Up

Mythril Spear

Iron Helmet
Bronze Armor
Sprint Shoes

Kiyomori, Muramasa
Charge+1, Charge+2, Charge+5, Charge+7



Ericzubat
Monster
Taurus
76
68
Dark Behemoth










SSwing
Female
Sagittarius
75
73
Samurai
White Magic
MA Save
Equip Shield
Teleport

Partisan
Escutcheon
Leather Helmet
Diamond Armor
Diamond Armlet

Bizen Boat
Cure 2, Raise, Regen, Protect, Wall, Esuna, Holy

Player: !Red
Team: Red Team
Palettes: Red/Brown



Kyune
Female
Libra
35
74
Priest
Black Magic
Hamedo
Doublehand
Move+1

Flail

Cachusha
Adaman Vest
Sprint Shoes

Cure 2, Raise, Regen, Protect, Protect 2, Shell 2, Wall, Esuna
Bolt 2, Bolt 3, Ice, Ice 4, Frog, Flare



Maakur
Male
Pisces
74
71
Archer
Item
Auto Potion
Dual Wield
Move+3

Ultimus Bow

Twist Headband
Chain Vest
Feather Mantle

Charge+4, Charge+7
Potion, Antidote, Phoenix Down



Lokenwow
Female
Aries
46
55
Geomancer
White Magic
Mana Shield
Sicken
Swim

Slasher
Bronze Shield
Golden Hairpin
Clothes
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Lava Ball
Cure 2, Cure 4, Raise, Raise 2, Reraise, Shell 2, Wall, Esuna, Holy



Roofiepops
Female
Gemini
54
71
Ninja
Punch Art
Earplug
Doublehand
Move+3

Ninja Edge

Cachusha
Chain Vest
Reflect Ring

Shuriken, Dictionary
Spin Fist, Secret Fist, Revive, Seal Evil

Player: !Black
Team: Black Team
Palettes: Black/Red



Lali Lulelo
Male
Cancer
71
57
Ninja
Battle Skill
Counter
Equip Shield
Ignore Terrain

Short Edge
Round Shield
Feather Hat
Chain Vest
Dracula Mantle

Wand
Head Break, Magic Break, Speed Break, Power Break



Gooseyourself
Female
Leo
40
65
Squire
White Magic
Absorb Used MP
Equip Polearm
Retreat

Battle Bamboo
Hero Shield
Black Hood
Earth Clothes
Bracer

Heal, Wish
Raise



Vorap
Female
Pisces
74
80
Squire
Black Magic
Regenerator
Magic Attack UP
Retreat

Hunting Bow
Genji Shield
Holy Miter
Earth Clothes
108 Gems

Dash, Tickle, Cheer Up, Scream
Fire 3, Fire 4, Bolt, Ice 3, Empower, Death



Thethorndog1
Female
Aquarius
58
78
Samurai
Basic Skill
Earplug
Magic Defense UP
Waterwalking

Kikuichimoji

Platinum Helmet
Leather Armor
Red Shoes

Koutetsu, Murasame, Heaven's Cloud
Throw Stone, Heal, Tickle, Scream

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dinin991
Female
Libra
73
73
Summoner
Charge
Sunken State
Short Charge
Teleport

Oak Staff

Headgear
Chameleon Robe
Power Wrist

Titan, Carbunkle, Bahamut, Leviathan, Lich
Charge+1, Charge+7



Maximumcrit
Monster
Leo
45
59
Archaic Demon










InOzWeTrust
Male
Virgo
57
56
Lancer
Elemental
Counter Flood
Equip Gun
Waterwalking

Mythril Gun
Genji Shield
Barbuta
Leather Armor
N-Kai Armlet

Level Jump2, Vertical Jump2
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball



SephDarkheart
Male
Virgo
47
76
Priest
Battle Skill
Counter
Short Status
Move-HP Up

Scorpion Tail

Black Hood
Chameleon Robe
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Protect, Shell, Wall, Esuna
Shield Break, Weapon Break, Magic Break, Stasis Sword

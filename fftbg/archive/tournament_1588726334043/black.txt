Player: !Black
Team: Black Team
Palettes: Black/Red



Nifboy
Monster
Scorpio
70
51
Explosive










Genericco
Female
Sagittarius
42
65
Dancer
Basic Skill
Counter
Magic Attack UP
Retreat

Ryozan Silk

Headgear
White Robe
Red Shoes

Last Dance, Dragon Pit
Heal, Wish



Old Overholt
Male
Capricorn
80
55
Priest
Item
PA Save
Throw Item
Ignore Height

Rainbow Staff

Thief Hat
Silk Robe
Battle Boots

Cure 2, Cure 4, Raise, Raise 2, Protect, Shell, Wall, Esuna
Potion, Hi-Potion, Hi-Ether, Antidote, Holy Water, Phoenix Down



Rogueain
Male
Scorpio
79
72
Mediator
Item
Brave Up
Short Status
Move-MP Up

Romanda Gun

Headgear
Light Robe
Magic Ring

Invitation, Threaten, Solution, Death Sentence, Refute
Hi-Potion, Ether, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down

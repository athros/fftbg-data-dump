Player: !Black
Team: Black Team
Palettes: Black/Red



Tsuike
Female
Leo
74
55
Chemist
Talk Skill
HP Restore
Secret Hunt
Lava Walking

Hydra Bag

Leather Hat
Leather Outfit
Rubber Shoes

Potion, X-Potion, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Solution, Death Sentence, Negotiate, Mimic Daravon, Refute



Heretic SC
Female
Capricorn
50
43
Priest
Talk Skill
Speed Save
Concentrate
Fly

Oak Staff

Headgear
Brigandine
108 Gems

Raise, Reraise, Regen, Shell, Shell 2, Wall, Esuna, Holy
Invitation, Persuade, Death Sentence, Insult, Negotiate



Rothgar72
Male
Taurus
48
47
Summoner
Throw
HP Restore
Secret Hunt
Fly

Poison Rod

Leather Hat
Chameleon Robe
Germinas Boots

Moogle, Ifrit, Golem, Carbunkle, Odin, Leviathan, Silf, Fairy, Lich
Bomb, Knife, Staff



Colonel Sheridan
Male
Gemini
64
71
Archer
Basic Skill
Arrow Guard
Short Charge
Jump+3

Cross Bow
Ice Shield
Red Hood
Leather Vest
Chantage

Charge+1, Charge+2, Charge+4
Accumulate, Dash, Heal

Player: !Red
Team: Red Team
Palettes: Red/Brown



Ominnous
Male
Scorpio
50
68
Archer
Yin Yang Magic
Distribute
Dual Wield
Retreat

Silver Bow

Black Hood
Clothes
Magic Ring

Charge+2, Charge+3, Charge+20
Blind, Pray Faith, Silence Song, Blind Rage, Confusion Song



Typicalfanboy
Female
Cancer
52
76
Monk
Talk Skill
Counter Magic
Equip Knife
Ignore Height

Dragon Rod

Black Hood
Chain Vest
Jade Armlet

Purification, Revive
Threaten, Insult



Ctharvey
Female
Aquarius
47
52
Thief
White Magic
Catch
Beastmaster
Ignore Terrain

Short Edge

Flash Hat
Adaman Vest
Power Wrist

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Cure 4, Raise, Raise 2, Reraise, Shell 2, Esuna



VirulenceXT
Female
Leo
65
44
Mediator
Steal
Critical Quick
Beastmaster
Retreat

Assassin Dagger

Holy Miter
Black Robe
Defense Armlet

Persuade, Solution, Mimic Daravon, Refute
Steal Armor, Steal Weapon, Steal Status

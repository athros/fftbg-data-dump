Final Bets: white - 9 bets for 4,264G (37.2%, x1.69); black - 16 bets for 7,193G (62.8%, x0.59)

white bets:
NovaKnight21: 1,136G (26.6%, 1,136G)
BirbBrainsBot: 1,000G (23.5%, 56,401G)
prince_rogers_nelson_: 544G (12.8%, 544G)
DLJuggernaut: 500G (11.7%, 16,629G)
getthemoneyz: 464G (10.9%, 963,544G)
MadnessR: 200G (4.7%, 11,073G)
benticore: 168G (3.9%, 3,371G)
LAGBOT30000: 152G (3.6%, 1,522G)
fluffskull: 100G (2.3%, 498G)

black bets:
DuraiPapers: 1,500G (20.9%, 6,277G)
electric_algus: 1,000G (13.9%, 17,363G)
sir_uther115: 988G (13.7%, 988G)
DAC169: 600G (8.3%, 1,191G)
Cryptopsy70: 552G (7.7%, 23,350G)
DamnThatShark: 500G (7.0%, 965G)
CorpusCav: 400G (5.6%, 3,162G)
byrdturbo: 333G (4.6%, 14,940G)
letdowncity: 300G (4.2%, 21,955G)
4shcrows: 216G (3.0%, 216G)
Kronikle: 200G (2.8%, 17,941G)
AltimaMantoid: 200G (2.8%, 3,000G)
BStarTV: 104G (1.4%, 104G)
AllInBot: 100G (1.4%, 100G)
CT_5_Holy: 100G (1.4%, 1,555G)
datadrivenbot: 100G (1.4%, 42,513G)

Player: !zChamp
Team: Champion Team
Palettes: Green/White



Miku Shikhu
Male
Aquarius
76
63
Knight
Time Magic
Counter Magic
Martial Arts
Jump+3

Save the Queen
Mythril Shield
Cross Helmet
Platinum Armor
Elf Mantle

Head Break, Shield Break, Magic Break, Night Sword, Explosion Sword
Haste, Immobilize, Reflect, Demi, Stabilize Time



Skillomono
Male
Cancer
78
75
Geomancer
Summon Magic
Auto Potion
Sicken
Teleport

Asura Knife
Platinum Shield
Flash Hat
Wizard Outfit
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Moogle, Shiva, Ifrit, Titan, Salamander, Lich



Dinin991
Male
Libra
75
77
Calculator
Yin Yang Magic
Parry
Equip Armor
Move+3

Battle Folio

Leather Helmet
Power Sleeve
Feather Boots

Height, Prime Number, 5, 4, 3
Blind, Poison, Life Drain, Pray Faith, Paralyze, Petrify, Dark Holy



Serperemagus
Female
Pisces
55
64
Thief
Elemental
Arrow Guard
Equip Knife
Waterwalking

Spell Edge

Flash Hat
Wizard Outfit
Wizard Mantle

Gil Taking, Steal Weapon, Steal Accessory, Arm Aim
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind

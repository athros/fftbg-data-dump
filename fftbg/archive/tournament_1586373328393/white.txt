Player: !White
Team: White Team
Palettes: White/Blue



DrAntiSocial
Male
Sagittarius
54
79
Time Mage
Charge
Meatbone Slash
Attack UP
Move+2

Oak Staff

Headgear
Light Robe
Elf Mantle

Haste, Haste 2, Stop, Float, Demi 2, Stabilize Time
Charge+1, Charge+3, Charge+10



JumbocactuarX27
Male
Capricorn
50
79
Priest
Sing
MA Save
Long Status
Lava Walking

Wizard Staff

Feather Hat
Wizard Outfit
Rubber Shoes

Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Esuna
Battle Song, Nameless Song



Mtueni
Female
Aries
56
80
Dancer
Draw Out
Sunken State
Long Status
Fly

Panther Bag

Triangle Hat
Wizard Robe
Genji Gauntlet

Last Dance, Obsidian Blade, Dragon Pit
Asura, Koutetsu, Muramasa



ApplesauceBoss
Male
Pisces
43
51
Ninja
Battle Skill
Mana Shield
Equip Gun
Waterwalking

Madlemgen
Bestiary
Red Hood
Power Sleeve
Rubber Shoes

Sword
Weapon Break, Speed Break, Power Break, Stasis Sword, Explosion Sword

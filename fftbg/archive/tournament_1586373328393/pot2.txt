Final Bets: green - 13 bets for 15,254G (43.1%, x1.32); yellow - 36 bets for 20,116G (56.9%, x0.76)

green bets:
Nizaha: 6,782G (44.5%, 13,565G)
HeroponThrawn: 1,498G (9.8%, 1,498G)
Shakarak: 1,292G (8.5%, 1,292G)
Lanshaft: 1,115G (7.3%, 2,230G)
MantisFinch: 1,000G (6.6%, 5,894G)
SeedSC: 1,000G (6.6%, 174,949G)
waterwatereverywhere: 980G (6.4%, 1,922G)
coralreeferz: 550G (3.6%, 12,953G)
Ant_ihistamine: 537G (3.5%, 537G)
rubenflonne: 200G (1.3%, 3,345G)
Firesheath: 100G (0.7%, 846G)
Lodrak: 100G (0.7%, 4,226G)
Lythe_Caraker: 100G (0.7%, 48,889G)

yellow bets:
DeathTaxesAndAnime: 2,006G (10.0%, 3,935G)
Shalloween: 1,200G (6.0%, 9,411G)
Slowbrofist: 1,097G (5.5%, 1,097G)
kai_shee: 1,001G (5.0%, 112,814G)
ungabunga_bot: 1,000G (5.0%, 85,819G)
BirbBrainsBot: 1,000G (5.0%, 277,874G)
mirapoix: 1,000G (5.0%, 4,110G)
red__lancer: 1,000G (5.0%, 21,460G)
nickelbank: 1,000G (5.0%, 24,125G)
Eldente: 735G (3.7%, 735G)
ANFz: 699G (3.5%, 699G)
YaBoy125: 600G (3.0%, 600G)
mexskacin: 582G (2.9%, 582G)
TheChainNerd: 552G (2.7%, 552G)
RughSontos: 539G (2.7%, 539G)
TheOneRanger: 500G (2.5%, 7,418G)
LDSkinny: 500G (2.5%, 4,661G)
getthemoneyz: 490G (2.4%, 446,084G)
ForagerCats: 484G (2.4%, 484G)
kingchadking: 460G (2.3%, 460G)
maakur_: 424G (2.1%, 424G)
old_overholt_: 390G (1.9%, 390G)
ko2q: 384G (1.9%, 384G)
ZephyrTempest: 347G (1.7%, 35,705G)
HaateXIII: 308G (1.5%, 308G)
luminarii: 300G (1.5%, 12,600G)
galkife: 250G (1.2%, 87,206G)
JustSuperish: 200G (1.0%, 3,352G)
JumbocactuarX27: 200G (1.0%, 1,167G)
Ivan_Rockwell: 200G (1.0%, 1,213G)
AllInBot: 168G (0.8%, 168G)
friendsquirrel: 100G (0.5%, 7,754G)
IndecisiveNinja: 100G (0.5%, 3,168G)
BlinkFS: 100G (0.5%, 822G)
RunicMagus: 100G (0.5%, 935G)
datadrivenbot: 100G (0.5%, 25,680G)

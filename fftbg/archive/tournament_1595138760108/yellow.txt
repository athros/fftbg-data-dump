Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mesmaster
Male
Aquarius
73
50
Mime

Catch
Dual Wield
Retreat



Headgear
Reflect Mail
Defense Ring

Mimic




Aldrammech
Male
Libra
51
67
Oracle
Draw Out
Mana Shield
Defend
Move+2

Ivory Rod

Twist Headband
Light Robe
Elf Mantle

Blind, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



Twisted Nutsatchel
Male
Aquarius
61
42
Samurai
Basic Skill
Counter Tackle
Dual Wield
Ignore Height

Javelin
Spear
Cross Helmet
Silk Robe
Diamond Armlet

Asura, Muramasa
Accumulate, Dash, Heal, Scream



ZicaX
Monster
Pisces
71
66
Behemoth








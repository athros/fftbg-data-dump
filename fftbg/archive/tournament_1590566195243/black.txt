Player: !Black
Team: Black Team
Palettes: Black/Red



YaBoy125
Male
Scorpio
47
48
Knight
Summon Magic
Distribute
Dual Wield
Jump+1

Broad Sword
Broad Sword
Iron Helmet
Gold Armor
Power Wrist

Head Break, Armor Break, Speed Break
Moogle, Ramuh, Titan, Silf, Lich



Dowdlron
Female
Scorpio
55
54
Knight
Black Magic
Sunken State
Equip Armor
Ignore Terrain

Battle Axe
Mythril Shield
Black Hood
Leather Armor
Elf Mantle

Armor Break, Speed Break, Dark Sword, Night Sword
Fire, Fire 2, Fire 3, Bolt, Ice 3, Ice 4



DrAntiSocial
Male
Virgo
70
71
Ninja
Talk Skill
Sunken State
Equip Sword
Levitate

Long Sword
Muramasa
Twist Headband
Adaman Vest
Jade Armlet

Shuriken, Bomb, Knife, Staff, Ninja Sword, Dictionary
Death Sentence, Mimic Daravon, Refute



Sinnyil2
Male
Scorpio
75
72
Bard
Summon Magic
Damage Split
Doublehand
Move+3

Windslash Bow

Red Hood
Mythril Vest
Bracer

Magic Song, Diamond Blade, Sky Demon
Shiva, Salamander, Lich

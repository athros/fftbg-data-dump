Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ApplesauceBoss
Male
Gemini
57
80
Thief
Draw Out
Brave Save
Equip Gun
Move+3

Papyrus Codex

Twist Headband
Earth Clothes
Battle Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Asura, Bizen Boat, Muramasa, Kikuichimoji



Nok
Female
Leo
57
79
Thief
Time Magic
Sunken State
Equip Sword
Fly

Kikuichimoji

Thief Hat
Earth Clothes
Bracer

Steal Armor, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Haste, Slow, Slow 2, Float, Reflect, Quick, Demi, Demi 2, Meteor



Sairentozon7
Female
Leo
81
63
Mime

Counter
Maintenance
Fly


Aegis Shield
Bronze Helmet
Leather Outfit
Power Wrist

Mimic




QuestMoreLand
Female
Aries
44
77
Archer
Item
Hamedo
Equip Bow
Swim

Star Bag
Hero Shield
Flash Hat
Earth Clothes
Battle Boots

Charge+1, Charge+2, Charge+5, Charge+7
Potion, Hi-Potion, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down

Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



HASTERIOUS
Female
Gemini
52
71
Oracle
Math Skill
Mana Shield
Long Status
Waterwalking

Ivory Rod

Green Beret
Black Robe
Rubber Shoes

Blind, Spell Absorb, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Paralyze, Petrify, Dark Holy
CT, Height, Prime Number, 5, 3



Bryan792
Monster
Gemini
45
65
Ultima Demon










Powermhero
Male
Cancer
74
75
Lancer
Yin Yang Magic
Arrow Guard
Short Charge
Move+1

Obelisk
Diamond Shield
Gold Helmet
Genji Armor
Feather Boots

Level Jump5, Vertical Jump4
Blind, Pray Faith, Silence Song, Confusion Song, Paralyze, Sleep



HaateXIII
Male
Sagittarius
75
81
Thief
Punch Art
Dragon Spirit
Martial Arts
Jump+3

Diamond Sword

Headgear
Clothes
Salty Rage

Steal Shield
Pummel, Secret Fist, Revive

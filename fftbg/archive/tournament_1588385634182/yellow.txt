Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lawnboxer
Female
Cancer
73
46
Chemist
Punch Art
Parry
Magic Attack UP
Lava Walking

Panther Bag

Green Beret
Leather Outfit
Red Shoes

Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Spin Fist, Earth Slash, Secret Fist, Purification, Revive



ColetteMSLP
Monster
Capricorn
48
76
Byblos










Exaltedhamster
Female
Scorpio
63
52
Mime

MP Restore
Equip Armor
Swim



Barbuta
Reflect Mail
Spike Shoes

Mimic




Reinoe
Male
Capricorn
76
72
Summoner
Yin Yang Magic
Dragon Spirit
Short Charge
Waterwalking

Oak Staff

Flash Hat
Wizard Robe
Small Mantle

Moogle, Ifrit, Golem, Salamander
Pray Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic

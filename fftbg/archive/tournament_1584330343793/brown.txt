Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nomoment
Female
Leo
42
63
Geomancer
Yin Yang Magic
Meatbone Slash
Halve MP
Jump+2

Coral Sword
Bronze Shield
Black Hood
Black Robe
Battle Boots

Pitfall, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Pray Faith, Blind Rage, Confusion Song, Dispel Magic



Rogueain
Monster
Sagittarius
65
63
Black Chocobo










Imranimran
Male
Cancer
77
70
Samurai
Throw
HP Restore
Defense UP
Lava Walking

Holy Lance

Genji Helmet
Light Robe
Rubber Shoes

Koutetsu, Murasame, Heaven's Cloud, Kiyomori
Shuriken, Bomb, Sword, Ninja Sword, Wand, Dictionary



Numbersborne
Female
Libra
80
40
Mediator
Yin Yang Magic
Arrow Guard
Martial Arts
Levitate

Bestiary

Headgear
Black Robe
Sprint Shoes

Invitation, Persuade, Threaten, Preach, Insult, Mimic Daravon, Rehabilitate
Life Drain, Pray Faith, Dispel Magic, Paralyze, Sleep, Dark Holy

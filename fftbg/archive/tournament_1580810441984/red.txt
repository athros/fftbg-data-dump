Player: !Red
Team: Red Team
Palettes: Red/Brown



Macho Madness CO
Monster
Virgo
60
42
Serpentarius










Treapvort
Female
Scorpio
77
49
Time Mage
Talk Skill
Earplug
Equip Polearm
Move-MP Up

Spear

Black Hood
Power Sleeve
Diamond Armlet

Haste, Slow 2, Float, Stabilize Time
Invitation, Solution, Mimic Daravon



ItsTokey89
Female
Taurus
48
80
Summoner
Dance
Sunken State
Magic Attack UP
Move-MP Up

Flame Rod

Flash Hat
Wizard Outfit
Red Shoes

Moogle, Shiva, Ramuh, Golem, Carbunkle, Bahamut, Odin, Fairy
Disillusion, Last Dance



SystemShut
Female
Cancer
76
70
Chemist
Yin Yang Magic
Sunken State
Concentrate
Jump+2

Cultist Dagger

Flash Hat
Brigandine
Rubber Shoes

Potion, Hi-Potion, Ether, Hi-Ether, Elixir, Maiden's Kiss, Remedy
Blind, Spell Absorb, Doubt Faith, Silence Song, Dispel Magic

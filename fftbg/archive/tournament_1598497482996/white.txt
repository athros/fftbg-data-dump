Player: !White
Team: White Team
Palettes: White/Blue



Sinnyil2
Male
Capricorn
59
55
Archer
Black Magic
Counter Tackle
Doublehand
Move+3

Lightning Bow

Flash Hat
Mystic Vest
N-Kai Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+20
Fire 2, Fire 3, Bolt



CapnChaos12
Female
Virgo
57
72
Archer
Throw
Counter Flood
Secret Hunt
Move+3

Night Killer
Kaiser Plate
Iron Helmet
Wizard Outfit
Magic Ring

Charge+1, Charge+2
Shuriken, Hammer



Silentkaster
Male
Sagittarius
72
47
Knight
Throw
Auto Potion
Magic Defense UP
Ignore Height

Ragnarok
Crystal Shield
Mythril Helmet
Wizard Robe
Chantage

Weapon Break, Power Break, Mind Break, Stasis Sword
Shuriken, Bomb



Holandrix
Male
Cancer
60
65
Priest
Black Magic
Distribute
Equip Gun
Move+2

Battle Folio

Red Hood
Linen Robe
Leather Mantle

Cure, Cure 2, Cure 4, Raise, Protect, Protect 2, Shell 2, Esuna
Bolt 2, Bolt 4, Ice 2, Ice 3, Flare

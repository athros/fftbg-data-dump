Player: !Green
Team: Green Team
Palettes: Green/White



BoneMiser
Male
Sagittarius
48
78
Thief
Punch Art
Abandon
Maintenance
Move+2

Sleep Sword

Flash Hat
Leather Outfit
Cursed Ring

Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory
Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil



Reinoe
Female
Aries
58
68
Calculator
Black Magic
Counter
Equip Axe
Waterwalking

Oak Staff

Twist Headband
Black Robe
Vanish Mantle

CT, Prime Number, 5
Fire 4, Bolt, Bolt 3, Ice 2, Death



ArchKnightX
Female
Leo
55
68
Summoner
Talk Skill
Dragon Spirit
Short Status
Move-MP Up

Papyrus Codex

Green Beret
Chain Vest
Jade Armlet

Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Bahamut
Invitation, Persuade, Threaten, Solution



Smashy
Male
Virgo
60
73
Archer
Battle Skill
Auto Potion
Equip Polearm
Teleport

Spear
Escutcheon
Flash Hat
Adaman Vest
Genji Gauntlet

Charge+1, Charge+10
Shield Break, Speed Break, Mind Break, Stasis Sword, Dark Sword

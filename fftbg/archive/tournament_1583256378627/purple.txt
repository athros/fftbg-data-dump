Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SeedSC
Female
Leo
52
73
Archer
Steal
Dragon Spirit
Defense UP
Waterwalking

Romanda Gun
Diamond Shield
Headgear
Black Costume
Elf Mantle

Charge+1, Charge+2, Charge+7, Charge+10
Steal Helmet, Steal Shield, Leg Aim



Powder Donut
Female
Cancer
72
53
Mediator
White Magic
Counter
Defense UP
Move-MP Up

Stone Gun

Headgear
Linen Robe
N-Kai Armlet

Invitation, Persuade, Praise, Solution, Insult, Mimic Daravon, Rehabilitate
Raise, Raise 2, Regen, Shell 2, Esuna



Swordoftheseven
Male
Scorpio
80
66
Bard
White Magic
Dragon Spirit
Long Status
Move-MP Up

Bloody Strings

Thief Hat
Crystal Mail
Reflect Ring

Angel Song, Life Song, Diamond Blade, Space Storage
Cure 4, Raise, Raise 2, Esuna



Coyowijuju
Male
Capricorn
76
54
Thief
Black Magic
Meatbone Slash
Martial Arts
Move+1



Green Beret
Black Costume
Magic Ring

Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Fire 2, Fire 3, Bolt 3, Ice 3

Final Bets: red - 6 bets for 4,558G (53.5%, x0.87); blue - 7 bets for 3,962G (46.5%, x1.15)

red bets:
NIghtdew14: 2,500G (54.8%, 13,699G)
BirbBrainsBot: 1,000G (21.9%, 49,494G)
TheDeeyo: 400G (8.8%, 1,288G)
AllInBot: 312G (6.8%, 312G)
getthemoneyz: 246G (5.4%, 1,616,472G)
MinBetBot: 100G (2.2%, 21,117G)

blue bets:
ko2q: 1,124G (28.4%, 1,124G)
powergems: 1,000G (25.2%, 2,799G)
dogsandcatsand: 595G (15.0%, 595G)
silentkaster: 507G (12.8%, 5,508G)
DouglasDragonThePoet: 436G (11.0%, 436G)
datadrivenbot: 200G (5.0%, 59,561G)
CosmicTactician: 100G (2.5%, 8,436G)

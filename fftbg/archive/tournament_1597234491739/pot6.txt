Final Bets: white - 14 bets for 9,046G (69.8%, x0.43); brown - 6 bets for 3,908G (30.2%, x2.31)

white bets:
reinoe: 3,000G (33.2%, 129,203G)
dogsandcatsand: 1,629G (18.0%, 3,195G)
ShintaroNayaka: 659G (7.3%, 659G)
coralreeferz: 592G (6.5%, 19,652G)
SephDarkheart: 544G (6.0%, 107,867G)
Draconis345: 500G (5.5%, 9,734G)
ohhinm: 430G (4.8%, 430G)
maximumcrit: 360G (4.0%, 360G)
TheDeeyo: 300G (3.3%, 1,308G)
BaronHaynes: 300G (3.3%, 5,158G)
xBizzy: 232G (2.6%, 1,412G)
AllInBot: 200G (2.2%, 200G)
datadrivenbot: 200G (2.2%, 60,178G)
CosmicTactician: 100G (1.1%, 8,136G)

brown bets:
UmaiJam: 2,000G (51.2%, 31,253G)
BirbBrainsBot: 1,000G (25.6%, 50,088G)
DouglasDragonThePoet: 520G (13.3%, 520G)
arctodus13: 200G (5.1%, 3,219G)
maddrave09: 132G (3.4%, 819G)
getthemoneyz: 56G (1.4%, 1,616,786G)

Player: !White
Team: White Team
Palettes: White/Blue



ColetteMSLP
Female
Scorpio
43
67
Time Mage
Steal
Arrow Guard
Equip Shield
Move-HP Up

Healing Staff
Escutcheon
Barette
Silk Robe
Angel Ring

Haste 2, Slow, Immobilize, Demi, Stabilize Time, Meteor
Gil Taking, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Just Here2
Female
Sagittarius
65
56
Chemist
Basic Skill
Arrow Guard
Dual Wield
Move+2

Dagger
Cute Bag
Twist Headband
Brigandine
Diamond Armlet

Potion, Ether, Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Accumulate, Heal, Cheer Up



Bongomon7
Male
Aries
55
67
Chemist
Time Magic
HP Restore
Equip Sword
Waterwalking

Platinum Sword

Golden Hairpin
Black Costume
108 Gems

Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Float, Reflect, Quick, Demi, Stabilize Time, Meteor



Lyner87
Female
Sagittarius
72
48
Samurai
Steal
PA Save
Halve MP
Jump+1

Mythril Spear

Iron Helmet
Silk Robe
N-Kai Armlet

Asura, Koutetsu, Kiyomori, Muramasa
Gil Taking, Steal Heart, Steal Helmet

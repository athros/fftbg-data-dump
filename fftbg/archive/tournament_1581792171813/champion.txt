Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Smiggenator
Female
Taurus
47
49
Wizard
Charge
Counter Magic
Short Charge
Lava Walking

Dragon Rod

Feather Hat
White Robe
Elf Mantle

Fire 3, Bolt, Bolt 2, Ice 3, Empower
Charge+7



AoiTsukiYuri
Monster
Sagittarius
76
62
Byblos










LordPaimon
Male
Capricorn
72
77
Archer
Item
Sunken State
Concentrate
Move+1

Ice Bow

Thief Hat
Black Costume
Defense Armlet

Charge+2, Charge+20
Antidote, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down



G00gly
Male
Pisces
72
58
Archer
Elemental
Faith Up
Attack UP
Lava Walking

Ultimus Bow

Thief Hat
Brigandine
Dracula Mantle

Charge+4, Charge+5, Charge+7, Charge+10
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

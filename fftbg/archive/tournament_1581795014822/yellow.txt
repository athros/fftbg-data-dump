Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Psychomidget
Monster
Leo
61
64
Revenant










Bannana Slug
Female
Gemini
74
71
Knight
Steal
Mana Shield
Defend
Levitate

Chaos Blade

Barbuta
Wizard Robe
Chantage

Armor Break, Speed Break
Steal Heart, Steal Weapon



AlysSariel
Female
Scorpio
61
56
Monk
Time Magic
MA Save
Attack UP
Waterbreathing



Headgear
Wizard Outfit
Magic Ring

Spin Fist, Earth Slash, Secret Fist, Purification
Haste, Haste 2, Stop, Float, Reflect, Quick, Meteor



ToastedZergling
Male
Virgo
78
77
Summoner
Steal
Counter Tackle
Equip Axe
Waterbreathing

Flail

Red Hood
Light Robe
Germinas Boots

Moogle, Ramuh, Ifrit, Golem, Leviathan, Silf, Lich, Cyclops
Steal Shield, Steal Weapon, Steal Status, Leg Aim

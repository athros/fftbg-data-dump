Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Upvla
Male
Cancer
67
62
Ninja
Item
PA Save
Sicken
Swim

Mythril Knife
Blind Knife
Leather Hat
Mystic Vest
Elf Mantle

Shuriken, Knife, Staff
Potion, X-Potion, Ether, Antidote, Phoenix Down



Galkife
Female
Taurus
56
56
Archer
Throw
Counter Flood
Dual Wield
Move-MP Up

Bow Gun
Poison Bow
Holy Miter
Wizard Outfit
Bracer

Charge+4, Charge+7
Shuriken, Bomb



Mtueni
Male
Libra
51
71
Summoner
Time Magic
Absorb Used MP
Equip Sword
Waterwalking

Nagrarock

Twist Headband
Mythril Vest
Genji Gauntlet

Ramuh, Carbunkle, Salamander, Silf, Lich, Cyclops
Haste, Immobilize, Float, Stabilize Time



RunicMagus
Female
Taurus
52
46
Squire
Punch Art
Critical Quick
Martial Arts
Move+3

Hunting Bow
Buckler
Platinum Helmet
Maximillian
N-Kai Armlet

Accumulate, Throw Stone, Heal, Yell, Wish
Spin Fist, Wave Fist, Earth Slash, Purification, Revive

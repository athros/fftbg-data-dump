Player: !White
Team: White Team
Palettes: White/Blue



Oreo Pizza
Female
Virgo
72
54
Archer
Black Magic
Faith Save
Short Charge
Waterbreathing

Mythril Bow

Headgear
Clothes
Battle Boots

Charge+1, Charge+2, Charge+4, Charge+10
Bolt, Ice 2, Empower, Frog



NicoSavoy
Male
Libra
70
43
Mime

Counter Magic
Martial Arts
Ignore Terrain



Flash Hat
Power Sleeve
Angel Ring

Mimic




ANFz
Male
Scorpio
52
56
Calculator
Demon Skill
Mana Shield
Defend
Ignore Height

Iron Fan

Triangle Hat
Crystal Mail
Reflect Ring

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



ArchKnightX
Female
Libra
74
54
Oracle
Black Magic
PA Save
Short Status
Move+2

Musk Rod

Golden Hairpin
Rubber Costume
Elf Mantle

Silence Song, Blind Rage, Foxbird, Confusion Song, Sleep
Fire 2, Bolt 4, Ice, Empower

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ZZ Yoshi
Male
Aries
74
57
Lancer
White Magic
Counter
Dual Wield
Levitate

Spear
Whale Whisker
Gold Helmet
Platinum Armor
Leather Mantle

Level Jump4, Vertical Jump8
Cure, Cure 3, Protect 2, Shell 2, Esuna, Holy



CorpusCav
Female
Scorpio
48
59
Mediator
White Magic
Auto Potion
Dual Wield
Fly

Mythril Gun
Glacier Gun
Flash Hat
Power Sleeve
Diamond Armlet

Invitation, Threaten, Insult, Rehabilitate
Cure 4, Raise, Reraise, Regen, Shell, Esuna



Tronfonne
Female
Sagittarius
75
51
Summoner
Charge
Speed Save
Long Status
Waterbreathing

Wizard Rod

Golden Hairpin
Judo Outfit
Leather Mantle

Moogle, Shiva, Ramuh, Carbunkle, Odin, Salamander
Charge+1, Charge+20



Lali Lulelo
Female
Aries
71
43
Squire
Black Magic
Critical Quick
Doublehand
Waterbreathing

Hunting Bow

Thief Hat
Black Costume
Power Wrist

Accumulate, Dash, Tickle, Wish
Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Ice, Empower, Death

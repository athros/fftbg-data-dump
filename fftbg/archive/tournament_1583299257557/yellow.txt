Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zebobz
Female
Libra
65
44
Summoner
Punch Art
Counter
Equip Shield
Jump+1

Wizard Rod
Crystal Shield
Red Hood
Black Robe
Magic Gauntlet

Shiva, Ramuh, Golem, Carbunkle, Fairy
Chakra, Revive



Shakarak
Male
Gemini
41
68
Monk
Jump
Counter
Equip Polearm
Move+3

Ivory Rod

Red Hood
Wizard Outfit
Magic Ring

Spin Fist, Pummel, Secret Fist, Chakra
Level Jump4, Vertical Jump8



Theatheologist
Male
Pisces
58
60
Knight
Summon Magic
Catch
Beastmaster
Move-MP Up

Giant Axe
Aegis Shield
Gold Helmet
Carabini Mail
Diamond Armlet

Armor Break, Magic Break, Mind Break, Stasis Sword
Moogle, Ramuh, Carbunkle, Lich



Pie108
Female
Virgo
77
65
Knight
Punch Art
Mana Shield
Doublehand
Jump+3

Coral Sword

Gold Helmet
Chameleon Robe
Feather Boots

Armor Break, Shield Break, Magic Break, Power Break, Dark Sword
Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Duarian
Male
Aries
53
57
Archer
Summon Magic
Earplug
Equip Shield
Levitate

Lightning Bow
Escutcheon
Triangle Hat
Judo Outfit
Rubber Shoes

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10, Charge+20
Moogle, Shiva, Golem, Fairy



ZephyrTempest
Female
Gemini
38
56
Samurai
Black Magic
Meatbone Slash
Equip Armor
Move+1

Kikuichimoji

Flash Hat
Clothes
Rubber Shoes

Asura, Koutetsu, Bizen Boat, Kiyomori
Fire, Fire 3, Fire 4, Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Death



I Nod My Head When I Lose
Female
Scorpio
78
58
Dancer
Time Magic
Auto Potion
Dual Wield
Lava Walking

Persia
Persia
Golden Hairpin
Clothes
Dracula Mantle

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Nether Demon
Haste, Immobilize, Demi, Stabilize Time



SirOwnzAlot
Male
Pisces
47
42
Lancer
Steal
Blade Grasp
Magic Defense UP
Jump+3

Holy Lance
Escutcheon
Barbuta
Diamond Armor
Cherche

Level Jump2, Vertical Jump2
Gil Taking, Steal Helmet, Steal Shield, Steal Weapon

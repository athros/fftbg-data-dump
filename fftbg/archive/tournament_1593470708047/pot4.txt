Final Bets: purple - 13 bets for 11,687G (66.7%, x0.50); brown - 13 bets for 5,824G (33.3%, x2.01)

purple bets:
SkylerBunny: 5,000G (42.8%, 498,761G)
Nizaha: 2,349G (20.1%, 2,349G)
prince_rogers_nelson_: 1,584G (13.6%, 1,584G)
null_sett: 500G (4.3%, 2,709G)
RaIshtar: 500G (4.3%, 7,733G)
DaKoopa49: 500G (4.3%, 2,494G)
killth3kid: 444G (3.8%, 1,668G)
Twisted_Nutsatchel: 200G (1.7%, 3,652G)
username123132: 200G (1.7%, 3,479G)
just_here2: 200G (1.7%, 12,093G)
Ring_Wyrm: 100G (0.9%, 2,342G)
datadrivenbot: 100G (0.9%, 46,646G)
moonliquor: 10G (0.1%, 1,952G)

brown bets:
VolgraTheMoose: 2,001G (34.4%, 22,719G)
BirbBrainsBot: 1,000G (17.2%, 141,790G)
fluffskull: 576G (9.9%, 576G)
reinoe: 500G (8.6%, 16,636G)
Forkmore: 500G (8.6%, 3,796G)
twelfthrootoftwo: 300G (5.2%, 14,658G)
KasugaiRoastedPeas: 200G (3.4%, 17,532G)
casteia: 200G (3.4%, 1,527G)
sososteve: 200G (3.4%, 6,522G)
Lydian_C: 121G (2.1%, 162,825G)
Firesheath: 100G (1.7%, 18,260G)
Lub1s: 100G (1.7%, 296G)
getthemoneyz: 26G (0.4%, 1,111,092G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Anomandaris
Male
Taurus
61
67
Archer
Black Magic
Absorb Used MP
Doublehand
Move+3

Poison Bow

Circlet
Black Costume
Defense Ring

Charge+1, Charge+2, Charge+5, Charge+10
Fire 3, Bolt 2, Bolt 3, Flare



Lali Lulelo
Male
Sagittarius
46
50
Ninja
Draw Out
Arrow Guard
Concentrate
Swim

Spell Edge
Hidden Knife
Feather Hat
Black Costume
Small Mantle

Bomb
Murasame, Muramasa



HaplessOne
Male
Pisces
80
79
Thief
Item
Catch
Dual Wield
Swim

Ice Brand
Mythril Sword
Golden Hairpin
Mystic Vest
N-Kai Armlet

Gil Taking, Steal Helmet, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Potion, Hi-Potion, X-Potion, Hi-Ether, Soft, Phoenix Down



FaytKion
Male
Aries
71
51
Calculator
Robosnake Skill
MA Save
Attack UP
Jump+1

Rod

Cross Helmet
Adaman Vest
Diamond Armlet

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm

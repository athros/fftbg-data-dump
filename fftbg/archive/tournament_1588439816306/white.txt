Player: !White
Team: White Team
Palettes: White/Blue



Langrisser
Monster
Gemini
73
48
Red Chocobo










Datkins728
Monster
Libra
49
47
Bull Demon










Dustmasc
Female
Cancer
67
41
Summoner
Time Magic
Counter Flood
Long Status
Ignore Terrain

Rod

Headgear
Wizard Robe
Wizard Mantle

Shiva, Ramuh, Titan, Carbunkle, Leviathan, Silf, Fairy
Slow 2, Immobilize, Reflect, Quick, Stabilize Time, Meteor



Aldrammech
Female
Sagittarius
65
76
Squire
Battle Skill
Faith Up
Beastmaster
Ignore Height

Hunting Bow
Crystal Shield
Iron Helmet
Platinum Armor
Spike Shoes

Accumulate, Heal, Fury, Wish, Ultima
Head Break, Magic Break

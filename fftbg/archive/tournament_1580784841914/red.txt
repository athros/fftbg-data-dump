Player: !Red
Team: Red Team
Palettes: Red/Brown



ChronoKnight
Female
Leo
44
73
Calculator
Time Magic
Damage Split
Beastmaster
Retreat

Papyrus Codex

Black Hood
Clothes
Wizard Mantle

CT, Prime Number, 5, 4, 3
Slow, Immobilize, Float, Quick, Demi, Demi 2, Stabilize Time



YourGMJack
Male
Virgo
65
60
Archer
Yin Yang Magic
Distribute
Equip Gun
Fly

Stone Gun
Buckler
Holy Miter
Mythril Vest
Jade Armlet

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Spell Absorb, Zombie, Sleep



Caivyre
Male
Capricorn
72
57
Lancer
Battle Skill
Auto Potion
Short Status
Levitate

Holy Lance
Platinum Shield
Mythril Helmet
Diamond Armor
Feather Boots

Level Jump4, Vertical Jump8
Armor Break, Shield Break, Mind Break



Pandasforsale
Female
Gemini
74
43
Squire
Item
Sunken State
Defense UP
Swim

Mythril Knife

Headgear
Adaman Vest
Magic Ring

Heal, Tickle, Scream
Hi-Potion, Antidote, Soft, Holy Water, Phoenix Down

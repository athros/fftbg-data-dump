Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



EvilLego6
Female
Taurus
60
60
Monk
Jump
Counter Flood
Long Status
Ignore Terrain



Golden Hairpin
Power Sleeve
Magic Ring

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Level Jump3, Vertical Jump2



Basmal
Female
Pisces
52
61
Ninja
Time Magic
Parry
Equip Bow
Levitate

Cross Bow
Gastrafitis
Golden Hairpin
Black Costume
Germinas Boots

Shuriken, Bomb, Dictionary
Haste, Slow 2, Reflect, Demi, Demi 2, Stabilize Time, Meteor



Blaster Bomb
Female
Capricorn
62
75
Summoner
Elemental
Arrow Guard
Dual Wield
Lava Walking

Mace of Zeus
Wizard Staff
Feather Hat
Linen Robe
Magic Gauntlet

Moogle, Shiva, Titan, Odin, Salamander, Lich
Hell Ivy, Quicksand, Gusty Wind



Sebsebseabass
Male
Libra
72
41
Calculator
Limit
Parry
Equip Shield
Jump+2

Musk Rod
Flame Shield
Holy Miter
White Robe
Wizard Mantle

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom

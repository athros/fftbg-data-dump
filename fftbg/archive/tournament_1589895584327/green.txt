Player: !Green
Team: Green Team
Palettes: Green/White



Red Lancer
Male
Aries
63
63
Summoner
Jump
Regenerator
Short Charge
Ignore Terrain

Oak Staff

Golden Hairpin
Mystic Vest
Defense Ring

Ramuh, Ifrit, Carbunkle, Bahamut, Odin, Fairy
Level Jump4, Vertical Jump8



Go Pack
Female
Pisces
76
80
Geomancer
Basic Skill
Earplug
Magic Attack UP
Jump+1

Koutetsu Knife
Flame Shield
Holy Miter
Leather Outfit
Red Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Dash, Scream



Rastanar
Male
Aquarius
70
39
Ninja
Item
Catch
Throw Item
Ignore Terrain

Koga Knife
Orichalcum
Red Hood
Clothes
Battle Boots

Shuriken, Bomb, Knife
Potion, Ether, Soft, Holy Water, Remedy, Phoenix Down



RagequitSA
Male
Taurus
51
39
Thief
Sing
HP Restore
Equip Knife
Swim

Assassin Dagger

Feather Hat
Power Sleeve
Power Wrist

Steal Helmet, Steal Armor, Steal Weapon
Battle Song, Magic Song, Nameless Song, Last Song, Diamond Blade

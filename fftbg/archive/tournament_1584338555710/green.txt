Player: !Green
Team: Green Team
Palettes: Green/White



I Nod My Head When I Lose
Female
Scorpio
40
48
Lancer
Draw Out
Parry
Defend
Move+1

Partisan
Diamond Shield
Leather Helmet
Bronze Armor
Genji Gauntlet

Level Jump5, Vertical Jump8
Koutetsu, Heaven's Cloud, Muramasa



Raixelol
Male
Leo
61
79
Summoner
Battle Skill
Counter Tackle
Doublehand
Levitate

Poison Rod

Feather Hat
Wizard Robe
Germinas Boots

Moogle, Ramuh, Titan, Golem, Bahamut, Leviathan
Head Break, Speed Break, Power Break, Mind Break, Surging Sword



Ranmilia
Female
Gemini
53
47
Dancer
Throw
Sunken State
Equip Polearm
Move+2

Ryozan Silk

Green Beret
Mythril Vest
Rubber Shoes

Disillusion
Shuriken, Bomb



PotionDweller
Male
Pisces
73
79
Archer
Punch Art
Catch
Equip Gun
Teleport

Glacier Gun
Round Shield
Triangle Hat
Power Sleeve
Feather Boots

Charge+2, Charge+4
Pummel, Purification, Revive

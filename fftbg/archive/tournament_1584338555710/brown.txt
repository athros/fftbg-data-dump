Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rhuarc Redhammer
Female
Virgo
77
80
Archer
Black Magic
Sunken State
Equip Axe
Ignore Terrain

White Staff
Genji Shield
Holy Miter
Mystic Vest
Magic Ring

Charge+3, Charge+5, Charge+7
Fire, Bolt 3, Ice 3, Empower



Hyvi
Male
Scorpio
65
78
Bard
Summon Magic
MP Restore
Magic Attack UP
Teleport

Ramia Harp

Feather Hat
Mystic Vest
Feather Boots

Life Song, Battle Song, Sky Demon
Moogle, Golem, Carbunkle, Lich



Maeveen
Monster
Scorpio
55
54
Porky










CaptainAdmiralSPATULA
Female
Gemini
70
52
Knight
Black Magic
Counter Flood
Attack UP
Waterwalking

Ragnarok

Genji Helmet
Linen Robe
Germinas Boots

Head Break, Weapon Break, Speed Break, Mind Break, Justice Sword
Bolt, Bolt 2, Ice, Ice 3, Frog

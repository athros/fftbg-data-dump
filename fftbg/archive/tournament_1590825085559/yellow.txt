Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zanderriv
Female
Libra
76
67
Squire
Dance
Counter
Equip Armor
Move+1

Giant Axe
Platinum Shield
Crystal Helmet
Crystal Mail
Wizard Mantle

Accumulate, Throw Stone, Heal, Tickle, Fury
Wiznaibus, Nameless Dance, Nether Demon



Flameatron
Female
Sagittarius
63
53
Ninja
Battle Skill
Counter
Attack UP
Teleport 2

Flail
Cultist Dagger
Black Hood
Judo Outfit
108 Gems

Shuriken, Staff, Axe, Wand
Power Break, Mind Break, Night Sword



Lali Lulelo
Female
Scorpio
57
58
Time Mage
Punch Art
Dragon Spirit
Defense UP
Swim

Battle Bamboo

Thief Hat
Black Robe
Rubber Shoes

Haste, Haste 2, Slow 2, Float, Stabilize Time
Purification



Zachara
Male
Leo
75
55
Monk
Time Magic
Blade Grasp
Equip Shield
Fly


Round Shield
Red Hood
Secret Clothes
Diamond Armlet

Wave Fist, Purification, Chakra, Revive, Seal Evil
Haste, Slow, Stop, Quick, Stabilize Time, Meteor

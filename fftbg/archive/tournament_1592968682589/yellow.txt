Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



OpHendoslice
Male
Aries
45
42
Ninja
Black Magic
Faith Save
Halve MP
Move-MP Up

Main Gauche
Sasuke Knife
Headgear
Chain Vest
108 Gems

Shuriken
Fire 2, Bolt 2, Ice, Ice 3, Death, Flare



DLJuggernaut
Female
Scorpio
71
69
Thief
Jump
Mana Shield
Equip Gun
Ignore Height

Mythril Gun

Feather Hat
Mystic Vest
Spike Shoes

Gil Taking, Steal Armor, Arm Aim, Leg Aim
Level Jump4, Vertical Jump8



Quadh0nk
Female
Leo
68
74
Squire
Dance
Counter
Equip Armor
Jump+2

Mythril Sword
Venetian Shield
Grand Helmet
Light Robe
Jade Armlet

Accumulate, Heal, Yell, Cheer Up, Fury, Wish, Ultima
Wiznaibus, Polka Polka, Last Dance



Aldrammech
Female
Aquarius
54
62
Chemist
Black Magic
Auto Potion
Defense UP
Jump+2

Star Bag

Black Hood
Mystic Vest
Bracer

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Soft
Bolt 2, Ice 4, Empower, Frog

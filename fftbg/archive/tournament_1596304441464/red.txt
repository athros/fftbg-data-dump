Player: !Red
Team: Red Team
Palettes: Red/Brown



Thyrandaal
Male
Gemini
41
43
Lancer
White Magic
Critical Quick
Short Status
Swim

Partisan
Round Shield
Crystal Helmet
White Robe
Elf Mantle

Level Jump5, Vertical Jump8
Cure, Cure 2, Cure 4, Raise 2, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna



Pandasforsale
Female
Capricorn
55
80
Monk
White Magic
Counter
Sicken
Levitate



Green Beret
Mystic Vest
Jade Armlet

Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil
Cure, Cure 2, Cure 4, Raise, Reraise, Shell 2, Esuna



SkylerBunny
Female
Scorpio
54
45
Oracle
Steal
Counter
Equip Shield
Fly

Madlemgen
Platinum Shield
Feather Hat
Black Costume
108 Gems

Blind, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Sleep, Petrify
Steal Heart, Steal Helmet, Steal Shield



Hasterious
Female
Cancer
58
52
Dancer
Elemental
Arrow Guard
Magic Defense UP
Ignore Height

Star Bag

Flash Hat
Brigandine
Feather Boots

Polka Polka, Nether Demon
Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

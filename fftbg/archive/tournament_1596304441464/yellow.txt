Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sairentozon7
Female
Sagittarius
76
74
Time Mage
Charge
Counter
Equip Gun
Lava Walking

Stone Gun

Ribbon
Wizard Robe
Germinas Boots

Haste, Slow, Slow 2, Reflect, Demi 2
Charge+1, Charge+4, Charge+5, Charge+7, Charge+10



Zeroroute
Female
Libra
52
50
Geomancer
Summon Magic
Critical Quick
Concentrate
Waterbreathing

Slasher
Diamond Shield
Twist Headband
Leather Outfit
Small Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Moogle, Shiva, Ifrit, Carbunkle, Odin, Leviathan, Fairy



Run With Stone GUNs
Male
Taurus
61
49
Monk
Elemental
Damage Split
Short Charge
Retreat



Twist Headband
Judo Outfit
Genji Gauntlet

Secret Fist, Purification, Seal Evil
Water Ball, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball



Maximumcrit
Female
Gemini
62
79
Archer
Throw
Regenerator
Short Charge
Move+2

Ice Bow

Green Beret
Secret Clothes
Bracer

Charge+1, Charge+4, Charge+10, Charge+20
Shuriken, Stick, Wand

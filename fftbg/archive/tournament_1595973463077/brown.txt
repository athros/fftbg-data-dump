Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ar Tactic
Female
Cancer
54
65
Samurai
White Magic
Counter Magic
Sicken
Lava Walking

Dragon Whisker

Gold Helmet
Mythril Armor
Wizard Mantle

Asura, Bizen Boat, Kiyomori, Muramasa
Cure, Cure 3, Raise, Shell, Esuna



Moonliquor
Female
Aries
80
59
Wizard
Battle Skill
Counter
Martial Arts
Move+3

Flame Rod

Holy Miter
Chameleon Robe
Magic Gauntlet

Fire, Fire 4, Ice, Death
Head Break, Armor Break, Shield Break, Magic Break, Stasis Sword, Justice Sword



Lydian C
Female
Aries
74
51
Time Mage
Draw Out
Parry
Magic Defense UP
Ignore Height

Musk Rod

Headgear
Clothes
Magic Ring

Haste 2, Slow 2, Stop, Immobilize, Demi, Stabilize Time, Galaxy Stop
Koutetsu, Heaven's Cloud, Kikuichimoji



Error72
Male
Virgo
76
49
Ninja
Draw Out
Counter
Equip Polearm
Move+3

Ryozan Silk
Musk Rod
Red Hood
Earth Clothes
Battle Boots

Bomb, Hammer, Dictionary
Bizen Boat, Heaven's Cloud, Kiyomori

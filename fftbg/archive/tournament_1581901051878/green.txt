Player: !Green
Team: Green Team
Palettes: Green/White



Fenixcrest
Male
Virgo
69
57
Knight
Charge
Damage Split
Long Status
Waterbreathing

Panther Bag
Ice Shield
Crystal Helmet
Silk Robe
Sprint Shoes

Armor Break, Weapon Break, Power Break
Charge+1, Charge+2, Charge+3, Charge+4, Charge+20



DaveKap
Monster
Aquarius
44
75
King Behemoth










Trix04
Male
Aries
71
78
Priest
Throw
Auto Potion
Short Status
Move+3

Oak Staff

Headgear
Clothes
Power Wrist

Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2
Shuriken, Stick



WalkerNash
Female
Libra
59
39
Time Mage
Summon Magic
Meatbone Slash
Secret Hunt
Lava Walking

Rainbow Staff

Flash Hat
Wizard Outfit
Feather Boots

Stop, Immobilize, Demi 2, Stabilize Time
Moogle, Shiva, Bahamut, Leviathan, Lich

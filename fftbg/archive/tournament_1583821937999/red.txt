Player: !Red
Team: Red Team
Palettes: Red/Brown



Bryan792
Male
Virgo
62
36
Ninja
Summon Magic
Absorb Used MP
Attack UP
Fly

Kunai
Morning Star
Golden Hairpin
Wizard Outfit
Magic Gauntlet

Bomb, Knife, Hammer, Ninja Sword
Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Odin, Fairy, Cyclops



Loverish
Male
Aries
69
73
Mediator
Item
Arrow Guard
Throw Item
Jump+3

Blaze Gun

Red Hood
Power Sleeve
Cursed Ring

Praise, Solution, Mimic Daravon
Potion, Hi-Potion, Hi-Ether, Soft, Remedy, Phoenix Down



Lowlf
Male
Libra
62
79
Chemist
Draw Out
Faith Up
Maintenance
Ignore Height

Hydra Bag

Green Beret
Judo Outfit
Feather Mantle

Potion, Ether, Hi-Ether, Remedy, Phoenix Down
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Chirijiraden



Hzor
Female
Scorpio
60
47
Ninja
Item
HP Restore
Defend
Move-MP Up

Mythril Knife
Flame Whip
Golden Hairpin
Leather Outfit
Red Shoes

Bomb, Ninja Sword
Potion, Hi-Potion, Eye Drop, Holy Water, Remedy

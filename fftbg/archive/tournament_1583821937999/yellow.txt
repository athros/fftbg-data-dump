Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Cougboi
Male
Aquarius
57
44
Summoner
Item
Brave Up
Defend
Swim

Rainbow Staff

Red Hood
Clothes
Spike Shoes

Moogle, Titan, Bahamut
Potion, Ether, Echo Grass, Soft, Holy Water, Phoenix Down



Lionhermit
Male
Serpentarius
80
62
Mediator
Time Magic
Sunken State
Dual Wield
Move+2

Bestiary
Bestiary
Holy Miter
Black Robe
Defense Ring

Persuade, Threaten, Insult, Negotiate, Refute
Immobilize, Quick, Demi 2, Stabilize Time



HaychDub
Male
Virgo
38
40
Lancer
Charge
HP Restore
Doublehand
Move-MP Up

Javelin

Crystal Helmet
Chain Mail
Cursed Ring

Level Jump2, Vertical Jump8
Charge+3, Charge+7, Charge+20



RongRongArts
Male
Sagittarius
72
62
Chemist
Elemental
Dragon Spirit
Defend
Ignore Terrain

Star Bag

Headgear
Wizard Outfit
Bracer

Ether, Soft, Phoenix Down
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Lava Ball

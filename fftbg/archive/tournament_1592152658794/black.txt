Player: !Black
Team: Black Team
Palettes: Black/Red



Brokenknight201
Male
Libra
37
59
Lancer
White Magic
Auto Potion
Equip Polearm
Lava Walking

Cashmere
Round Shield
Genji Helmet
Mythril Armor
Spike Shoes

Level Jump8, Vertical Jump2
Raise



DustBirdEX
Male
Aquarius
56
72
Geomancer
Yin Yang Magic
Speed Save
Equip Axe
Swim

Flail
Mythril Shield
Flash Hat
White Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Spell Absorb, Zombie, Dispel Magic, Sleep, Dark Holy



CaptainGarlock
Male
Taurus
66
47
Ninja
Time Magic
Counter Magic
Magic Defense UP
Waterwalking

Ninja Edge
Sasuke Knife
Flash Hat
Brigandine
Genji Gauntlet

Ninja Sword, Axe
Stop, Quick, Stabilize Time



DeathTaxesAndAnime
Female
Virgo
74
81
Lancer
Time Magic
Arrow Guard
Equip Axe
Retreat

Flail
Diamond Shield
Leather Helmet
Gold Armor
Feather Mantle

Level Jump2, Vertical Jump4
Haste, Slow, Demi 2, Stabilize Time, Meteor

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Grandlanzer
Male
Taurus
44
56
Knight
Punch Art
Counter Flood
Long Status
Ignore Height

Long Sword
Genji Shield
Genji Helmet
Reflect Mail
Diamond Armlet

Head Break, Armor Break, Stasis Sword, Surging Sword
Spin Fist, Earth Slash, Purification, Revive, Seal Evil



JoeykinsGaming
Male
Aries
44
52
Archer
White Magic
Meatbone Slash
Doublehand
Move+3

Windslash Bow

Mythril Helmet
Leather Outfit
Bracer

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Cure 3, Cure 4, Esuna



HaychDub
Male
Serpentarius
62
66
Lancer
Elemental
Counter Tackle
Short Status
Ignore Height

Mythril Spear
Diamond Shield
Barbuta
Silk Robe
Jade Armlet

Level Jump8, Vertical Jump6
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Kaelsun
Female
Aquarius
76
45
Time Mage
Item
Counter Magic
Martial Arts
Waterwalking

Wizard Staff

Leather Hat
Silk Robe
Feather Mantle

Demi, Stabilize Time
Ether, Hi-Ether, Soft, Remedy, Phoenix Down

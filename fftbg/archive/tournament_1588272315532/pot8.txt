Final Bets: brown - 18 bets for 6,403G (16.7%, x5.00); champion - 43 bets for 32,044G (83.3%, x0.20)

brown bets:
Baron_von_Scrub: 1,111G (17.4%, 83,086G)
superdevon1: 973G (15.2%, 10,288G)
SarrgeQc: 501G (7.8%, 12,569G)
HaateXIII: 500G (7.8%, 57,707G)
hydroshade: 500G (7.8%, 3,360G)
CapnChaos12: 500G (7.8%, 51,820G)
Cryptopsy70: 384G (6.0%, 35,187G)
ValtonZenola: 368G (5.7%, 368G)
mirapoix: 300G (4.7%, 1,453G)
Mesmaster: 300G (4.7%, 50,385G)
cgMcWhiskers: 200G (3.1%, 2,574G)
Bioticism: 150G (2.3%, 3,331G)
eudes89: 108G (1.7%, 108G)
genericco: 108G (1.7%, 108G)
WoooBlaaa: 100G (1.6%, 15,434G)
Firesheath: 100G (1.6%, 12,355G)
vampyreinabox: 100G (1.6%, 1,026G)
run_with_STONE_GUNS: 100G (1.6%, 5,588G)

champion bets:
sinnyil2: 3,232G (10.1%, 3,232G)
Zeroroute: 2,734G (8.5%, 7,708G)
DeathTaxesAndAnime: 2,376G (7.4%, 4,660G)
Nizaha: 2,001G (6.2%, 8,063G)
Pie108: 2,000G (6.2%, 173,052G)
coralreeferz: 1,500G (4.7%, 5,269G)
troublesometree: 1,280G (4.0%, 1,280G)
Shalloween: 1,040G (3.2%, 2,041G)
BirbBrainsBot: 1,000G (3.1%, 155,530G)
JumbocactuarX27: 1,000G (3.1%, 23,666G)
Mtueni: 1,000G (3.1%, 248,803G)
LDSkinny: 1,000G (3.1%, 2,433G)
getthemoneyz: 888G (2.8%, 607,618G)
proper__noun__: 863G (2.7%, 863G)
DLJuggernaut: 842G (2.6%, 3,371G)
itsdigitalbro: 837G (2.6%, 837G)
ungabunga_bot: 741G (2.3%, 283,979G)
Odin007_88: 728G (2.3%, 728G)
Lordminsc: 528G (1.6%, 528G)
CoderCalamity: 517G (1.6%, 1,724G)
aStatue: 500G (1.6%, 2,352G)
Theseawolf1: 500G (1.6%, 6,020G)
Laserman1000: 500G (1.6%, 72,200G)
vorap: 500G (1.6%, 31,054G)
Cataphract116: 500G (1.6%, 18,195G)
ZephyrTempest: 456G (1.4%, 1,950G)
Wonser: 435G (1.4%, 1,731G)
TheGuesty: 386G (1.2%, 30,826G)
joewcarson: 380G (1.2%, 380G)
Lythe_Caraker: 250G (0.8%, 119,833G)
trigger15: 250G (0.8%, 6,550G)
HorusTaurus: 248G (0.8%, 248G)
dorman_777: 116G (0.4%, 116G)
sage_the_kitsune: 104G (0.3%, 104G)
nifboy: 100G (0.3%, 9,703G)
GrayGhostGaming: 100G (0.3%, 22,053G)
FriendSquirrel: 100G (0.3%, 19,007G)
ANFz: 100G (0.3%, 24,979G)
CosmicTactician: 100G (0.3%, 12,263G)
maakur_: 100G (0.3%, 113,532G)
DrAntiSocial: 100G (0.3%, 9,558G)
datadrivenbot: 100G (0.3%, 11,371G)
daveb_: 12G (0.0%, 1,820G)

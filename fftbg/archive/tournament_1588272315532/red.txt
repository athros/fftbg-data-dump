Player: !Red
Team: Red Team
Palettes: Red/Brown



Dorman 777
Monster
Cancer
51
80
Red Dragon










DeathTaxesAndAnime
Female
Gemini
80
60
Samurai
Summon Magic
Mana Shield
Sicken
Retreat

Javelin

Iron Helmet
Bronze Armor
Spike Shoes

Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa
Shiva, Ifrit, Golem, Odin, Silf, Fairy



Shakarak
Male
Aries
50
41
Geomancer
Yin Yang Magic
HP Restore
Concentrate
Ignore Terrain

Ancient Sword
Gold Shield
Green Beret
Chameleon Robe
108 Gems

Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball
Blind, Life Drain



Fluffywormhole
Female
Gemini
78
43
Chemist
Yin Yang Magic
Catch
Equip Polearm
Move+3

Battle Bamboo

Flash Hat
Brigandine
Rubber Shoes

Potion, Hi-Ether, Antidote, Echo Grass, Remedy
Blind, Life Drain, Zombie, Paralyze

Player: !Red
Team: Red Team
Palettes: Red/Brown



The Pengwin
Male
Scorpio
55
58
Monk
Yin Yang Magic
Auto Potion
Maintenance
Move-HP Up



Green Beret
Clothes
Bracer

Pummel, Purification, Revive
Spell Absorb, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze



Lunchboxtac
Male
Gemini
76
42
Bard
Talk Skill
Counter Flood
Short Charge
Swim

Fairy Harp

Thief Hat
Chain Mail
Salty Rage

Cheer Song
Persuade, Preach, Insult, Negotiate



Leakimiko
Monster
Virgo
61
72
Malboro










Aldrammech
Male
Virgo
78
38
Archer
Jump
Counter Magic
Attack UP
Move+1

Mythril Gun
Bronze Shield
Golden Hairpin
Mythril Vest
Setiemson

Charge+4, Charge+5
Level Jump5, Vertical Jump2

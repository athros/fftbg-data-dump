Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Tikotikotiko
Female
Libra
79
74
Chemist
Charge
Mana Shield
Equip Armor
Jump+1

Mage Masher

Flash Hat
Robe of Lords
Rubber Shoes

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Charge+1, Charge+7



Glennfk
Male
Leo
59
66
Lancer
Punch Art
Arrow Guard
Equip Sword
Levitate

Asura Knife
Crystal Shield
Cross Helmet
Gold Armor
Wizard Mantle

Level Jump4, Vertical Jump6
Purification, Revive



DaurosTD
Monster
Cancer
52
62
Sekhret










Superdevon1
Male
Aries
53
80
Priest
Sing
MP Restore
Equip Gun
Teleport

Bestiary

Feather Hat
Power Sleeve
Small Mantle

Cure, Raise, Raise 2, Shell, Wall, Esuna
Cheer Song, Magic Song, Last Song

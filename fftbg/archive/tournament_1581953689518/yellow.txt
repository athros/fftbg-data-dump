Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lodrak
Male
Pisces
37
71
Chemist
Steal
Speed Save
Defense UP
Waterwalking

Star Bag

Flash Hat
Earth Clothes
Magic Gauntlet

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Soft, Phoenix Down
Steal Heart, Steal Armor, Steal Accessory, Arm Aim



Littlep2000
Female
Gemini
45
72
Monk
Battle Skill
Critical Quick
Dual Wield
Waterwalking



Twist Headband
Earth Clothes
108 Gems

Wave Fist, Purification, Chakra, Revive
Shield Break, Power Break, Surging Sword



AdaephonD
Female
Cancer
40
74
Wizard
Talk Skill
Auto Potion
Long Status
Jump+1

Thunder Rod

Black Hood
Adaman Vest
Jade Armlet

Bolt 2, Ice, Ice 3
Invitation, Persuade, Solution, Mimic Daravon



Joeycastaldo
Female
Pisces
66
43
Squire
Punch Art
Earplug
Beastmaster
Move-HP Up

Flame Whip

Golden Hairpin
Chain Vest
Magic Gauntlet

Accumulate, Throw Stone, Heal, Tickle, Yell, Cheer Up, Wish, Scream
Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive

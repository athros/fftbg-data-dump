Player: !Black
Team: Black Team
Palettes: Black/Red



Rico Flex
Female
Leo
46
78
Wizard
Item
MP Restore
Defend
Move+3

Poison Rod

Barette
Brigandine
Genji Gauntlet

Fire, Fire 4, Ice 3, Ice 4, Empower, Frog
Hi-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Soft, Phoenix Down



Evewho
Male
Capricorn
70
51
Mime

Critical Quick
Maintenance
Ignore Height



Twist Headband
Leather Outfit
Angel Ring

Mimic




Sairentozon7
Female
Sagittarius
61
46
Mediator
Dance
Abandon
Magic Defense UP
Move+2

Bestiary

Headgear
Leather Outfit
Magic Gauntlet

Invitation, Praise, Threaten, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Witch Hunt, Disillusion, Nameless Dance, Last Dance, Void Storage



Fenixcrest
Female
Aries
80
60
Thief
White Magic
Meatbone Slash
Defense UP
Lava Walking

Assassin Dagger

Leather Hat
Chain Vest
Germinas Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Steal Status
Cure, Raise, Raise 2, Regen, Shell, Wall, Esuna, Holy

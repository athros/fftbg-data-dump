Player: !Red
Team: Red Team
Palettes: Red/Brown



JustSuperish
Female
Leo
76
68
Mime

Abandon
Monster Talk
Move+3



Holy Miter
Chain Vest
N-Kai Armlet

Mimic




NUMBER1DB
Male
Leo
60
75
Summoner
Battle Skill
Meatbone Slash
Sicken
Move+3

Poison Rod

Leather Hat
Clothes
Reflect Ring

Golem, Silf, Fairy, Cyclops
Weapon Break, Speed Break, Power Break, Stasis Sword



JumbocactuarX27
Female
Scorpio
47
71
Knight
Elemental
Meatbone Slash
Martial Arts
Jump+1


Aegis Shield
Bronze Helmet
Diamond Armor
Angel Ring

Power Break, Stasis Sword, Dark Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Sand Storm, Blizzard, Gusty Wind, Lava Ball



ThePineappleSalesman
Female
Taurus
78
73
Archer
Punch Art
Damage Split
Maintenance
Move-HP Up

Ice Bow

Twist Headband
Power Sleeve
Power Wrist

Charge+2, Charge+3, Charge+4, Charge+5, Charge+10
Pummel, Wave Fist, Purification, Revive, Seal Evil

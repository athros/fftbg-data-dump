Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Laserman1000
Male
Pisces
41
69
Geomancer
Charge
Abandon
Secret Hunt
Move-MP Up

Coral Sword
Genji Shield
Twist Headband
White Robe
Reflect Ring

Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Charge+3, Charge+20



Bryan792
Male
Libra
45
51
Lancer
Black Magic
Critical Quick
Dual Wield
Ignore Height

Partisan
Holy Lance
Iron Helmet
White Robe
Defense Ring

Level Jump4, Vertical Jump5
Fire 3, Ice, Ice 4, Flare



RRazza
Female
Capricorn
65
67
Monk
Yin Yang Magic
Counter
Dual Wield
Lava Walking



Green Beret
Black Costume
Jade Armlet

Pummel, Earth Slash, Secret Fist, Purification, Revive
Spell Absorb, Pray Faith, Silence Song, Foxbird, Confusion Song, Paralyze



DaveStrider55
Female
Scorpio
72
66
Mediator
Draw Out
Damage Split
Short Charge
Move+1

Romanda Gun

Flash Hat
White Robe
Power Wrist

Praise, Threaten, Preach, Negotiate, Mimic Daravon
Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Muramasa

Player: !Black
Team: Black Team
Palettes: Black/Red



Cryptopsy70
Monster
Virgo
69
80
Holy Dragon










JonnyCue
Female
Taurus
43
45
Squire
Dance
PA Save
Equip Knife
Move-HP Up

Diamond Sword

Twist Headband
Leather Armor
Defense Ring

Heal, Yell
Witch Hunt, Wiznaibus, Nether Demon, Dragon Pit



SkylerBunny
Male
Leo
73
76
Bard
Charge
PA Save
Beastmaster
Waterwalking

Silver Bow

Green Beret
Wizard Outfit
Red Shoes

Angel Song, Life Song, Magic Song, Hydra Pit
Charge+1



Alaylle
Male
Taurus
79
47
Knight
Summon Magic
PA Save
Equip Sword
Lava Walking

Blood Sword
Genji Shield
Iron Helmet
Carabini Mail
Salty Rage

Head Break, Armor Break, Shield Break, Speed Break, Stasis Sword, Justice Sword, Surging Sword
Titan, Bahamut, Leviathan, Salamander

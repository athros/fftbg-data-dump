Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



FriendSquirrel
Male
Cancer
70
49
Chemist
Time Magic
Absorb Used MP
Equip Sword
Ignore Terrain

Long Sword

Green Beret
Clothes
Genji Gauntlet

Potion, Hi-Potion, Hi-Ether, Echo Grass, Soft, Remedy, Phoenix Down
Stop, Reflect, Stabilize Time



Douchetron
Female
Leo
49
43
Dancer
Yin Yang Magic
Earplug
Defense UP
Levitate

Panther Bag

Headgear
Judo Outfit
Power Wrist

Witch Hunt, Wiznaibus, Nameless Dance, Last Dance, Void Storage, Nether Demon
Blind, Poison, Spell Absorb, Life Drain, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Sleep



JonnyCue
Female
Cancer
57
52
Time Mage
Battle Skill
Counter Tackle
Equip Gun
Move+1

Battle Folio

Leather Hat
Silk Robe
Diamond Armlet

Haste, Immobilize, Float, Demi, Demi 2
Head Break, Armor Break, Shield Break, Speed Break



Laserman1000
Male
Aries
57
71
Mime

Counter
Martial Arts
Retreat



Black Hood
Judo Outfit
Dracula Mantle

Mimic


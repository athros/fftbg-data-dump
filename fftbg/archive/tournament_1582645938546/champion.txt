Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Oogthecaveman
Male
Capricorn
50
62
Calculator
Yin Yang Magic
HP Restore
Equip Gun
Levitate

Stone Gun

Black Hood
Chameleon Robe
Dracula Mantle

CT, Prime Number, 5, 3
Spell Absorb, Doubt Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep, Dark Holy



Sn0bb
Male
Aquarius
56
76
Bard
Throw
Mana Shield
Dual Wield
Lava Walking

Bloody Strings
Ramia Harp
Black Hood
Mythril Vest
Feather Boots

Angel Song, Life Song, Last Song
Shuriken, Bomb, Dictionary



Bad1dea
Female
Libra
74
78
Summoner
Dance
PA Save
Magic Attack UP
Teleport

Healing Staff

Holy Miter
White Robe
Leather Mantle

Moogle, Shiva, Ramuh, Titan, Carbunkle, Silf, Cyclops
Wiznaibus, Polka Polka, Nameless Dance, Last Dance



Grimmace45
Male
Virgo
61
53
Lancer
Draw Out
Sunken State
Equip Bow
Waterwalking

Silver Bow

Circlet
Plate Mail
Sprint Shoes

Level Jump5, Vertical Jump5
Bizen Boat, Muramasa

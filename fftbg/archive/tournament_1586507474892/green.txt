Player: !Green
Team: Green Team
Palettes: Green/White



R Raynos
Female
Libra
43
74
Mime

Earplug
Equip Shield
Move+2


Ice Shield
Green Beret
Adaman Vest
108 Gems

Mimic




Zetchryn
Male
Aquarius
59
52
Mime

Caution
Equip Armor
Waterwalking



Crystal Helmet
Mystic Vest
Dracula Mantle

Mimic




LOKITHUS
Female
Scorpio
58
42
Monk
Charge
Hamedo
Short Status
Levitate



Holy Miter
Mythril Vest
Magic Gauntlet

Pummel, Earth Slash, Purification
Charge+3, Charge+4, Charge+5



Sairentozon7
Male
Gemini
45
77
Mediator
Elemental
Counter
Defense UP
Move+1

Glacier Gun

Twist Headband
Clothes
Germinas Boots

Solution, Negotiate
Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball

Final Bets: red - 7 bets for 20,257G (44.3%, x1.26); blue - 19 bets for 25,480G (55.7%, x0.80)

red bets:
prince_rogers_nelson_: 12,392G (61.2%, 12,392G)
AllInBot: 3,937G (19.4%, 3,937G)
amiture: 2,000G (9.9%, 39,357G)
Aldrammech: 1,000G (4.9%, 61,995G)
NovaKnight21: 500G (2.5%, 500G)
rednecknazgul: 228G (1.1%, 228G)
TasisSai: 200G (1.0%, 12,417G)

blue bets:
reinoe: 5,000G (19.6%, 120,247G)
sinnyil2: 3,987G (15.6%, 7,974G)
Thyrandaal: 3,333G (13.1%, 75,145G)
boxyID: 2,000G (7.8%, 45,628G)
NicoSavoy: 2,000G (7.8%, 336,228G)
HaateXIII: 1,181G (4.6%, 1,181G)
CosmicTactician: 1,000G (3.9%, 25,396G)
ColetteMSLP: 1,000G (3.9%, 28,487G)
getthemoneyz: 1,000G (3.9%, 1,446,762G)
BirbBrainsBot: 1,000G (3.9%, 50,766G)
Zbgs: 1,000G (3.9%, 6,913G)
Chambs12: 810G (3.2%, 810G)
brenogarwin: 515G (2.0%, 515G)
gorgewall: 504G (2.0%, 504G)
nhammen: 400G (1.6%, 26,171G)
coralreeferz: 250G (1.0%, 1,623G)
datadrivenbot: 200G (0.8%, 46,785G)
ArlanKels: 200G (0.8%, 1,081G)
Brainstew29: 100G (0.4%, 10,040G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Masta Glenn
Male
Leo
43
65
Mime

Catch
Martial Arts
Move+3



Black Hood
Brigandine
Diamond Armlet

Mimic




WireLord
Male
Libra
78
64
Archer
Draw Out
Counter
Doublehand
Lava Walking

Ice Bow

Flash Hat
Wizard Outfit
Elf Mantle

Charge+1, Charge+3, Charge+4, Charge+20
Heaven's Cloud, Kiyomori



Byrdturbo
Male
Cancer
38
59
Mediator
Item
Auto Potion
Defense UP
Fly

Mythril Gun

Black Hood
Earth Clothes
Feather Mantle

Praise, Death Sentence, Insult
Potion, Ether, Hi-Ether, Eye Drop



Krexenn
Female
Aquarius
64
53
Ninja
Charge
Catch
Beastmaster
Jump+2

Spell Edge
Ninja Edge
Barette
Black Costume
Cursed Ring

Hammer, Staff, Spear, Dictionary
Charge+2, Charge+7

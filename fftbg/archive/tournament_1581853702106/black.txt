Player: !Black
Team: Black Team
Palettes: Black/Red



Nosablake
Female
Scorpio
54
74
Oracle
White Magic
Faith Up
Equip Gun
Move+1

Madlemgen

Black Hood
Robe of Lords
Small Mantle

Pray Faith, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze, Sleep
Cure, Cure 4, Raise 2, Regen, Esuna



Spikey82
Female
Gemini
74
47
Wizard
Elemental
Counter Magic
Defend
Jump+1

Dagger

Twist Headband
Chameleon Robe
Dracula Mantle

Fire 3, Ice, Ice 4, Empower, Frog, Death, Flare
Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



ApplesauceBoss
Male
Aquarius
69
57
Mime

Counter
Martial Arts
Levitate



Twist Headband
Gold Armor
Genji Gauntlet

Mimic




KoreanUsher
Male
Aquarius
72
75
Archer
White Magic
Counter
Doublehand
Move+1

Stone Gun

Feather Hat
Black Costume
N-Kai Armlet

Charge+1, Charge+2, Charge+4, Charge+10
Cure 3, Cure 4, Shell, Shell 2, Esuna

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Byrdturbo
Male
Aquarius
77
46
Squire
Summon Magic
Caution
Secret Hunt
Move+3

Star Bag
Platinum Shield
Gold Helmet
Chain Mail
Red Shoes

Accumulate, Throw Stone, Heal, Tickle, Scream
Titan, Golem, Carbunkle, Leviathan, Lich



Volgrathemoose
Male
Cancer
68
78
Squire
Yin Yang Magic
Auto Potion
Equip Bow
Move+2

Bow Gun
Diamond Shield
Twist Headband
Black Costume
Cursed Ring

Yell, Wish, Scream
Confusion Song, Paralyze



Ninjapenguim
Female
Capricorn
63
67
Monk
Jump
Auto Potion
Short Charge
Jump+3



Headgear
Chain Vest
Feather Boots

Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Seal Evil
Level Jump5, Vertical Jump8



L M D
Male
Libra
48
64
Wizard
Steal
Distribute
Equip Sword
Ignore Terrain

Koutetsu Knife

Flash Hat
Mystic Vest
Elf Mantle

Fire 3, Fire 4, Bolt 2, Ice, Ice 4, Empower, Flare
Steal Heart, Steal Helmet, Steal Shield, Arm Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



Ar Tactic
Monster
Cancer
79
76
Bull Demon










Phytik
Female
Aries
71
77
Lancer
Throw
Counter Tackle
Concentrate
Levitate

Spear
Bronze Shield
Iron Helmet
Reflect Mail
N-Kai Armlet

Level Jump2, Vertical Jump3
Staff



Lijarkh
Male
Leo
39
56
Monk
Sing
PA Save
Attack UP
Fly



Leather Hat
Clothes
Leather Mantle

Wave Fist, Purification, Chakra, Revive, Seal Evil
Nameless Song, Sky Demon



NovaKnight21
Male
Capricorn
64
66
Squire
Yin Yang Magic
Earplug
Short Status
Ignore Height

Slasher
Platinum Shield
Flash Hat
Adaman Vest
Chantage

Accumulate, Yell, Cheer Up, Wish
Life Drain, Confusion Song

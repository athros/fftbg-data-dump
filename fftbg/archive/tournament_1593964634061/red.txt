Player: !Red
Team: Red Team
Palettes: Red/Brown



BlackFireUK
Female
Virgo
67
63
Oracle
Black Magic
PA Save
Dual Wield
Jump+1

Whale Whisker
Gokuu Rod
Feather Hat
Silk Robe
Bracer

Poison, Life Drain, Foxbird, Dispel Magic
Fire 2, Fire 4, Ice 2, Ice 3, Ice 4



ALY327
Female
Sagittarius
45
49
Priest
Battle Skill
Critical Quick
Dual Wield
Lava Walking

Gold Staff
Wizard Staff
Triangle Hat
Power Sleeve
Magic Ring

Cure, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Wall, Esuna, Holy
Shield Break, Weapon Break, Dark Sword



Cam ATS
Female
Gemini
62
60
Dancer
Talk Skill
Absorb Used MP
Monster Talk
Lava Walking

Star Bag

Cachusha
Wizard Robe
Dracula Mantle

Disillusion, Obsidian Blade, Nether Demon
Threaten, Mimic Daravon



Baconbacon1207
Female
Virgo
73
56
Chemist
Summon Magic
HP Restore
Attack UP
Ignore Height

Dagger

Black Hood
Mythril Vest
Jade Armlet

Potion, Hi-Ether, Eye Drop, Phoenix Down
Moogle, Shiva

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SeniorBunk
Male
Gemini
52
51
Bard
Charge
Auto Potion
Beastmaster
Ignore Height

Fairy Harp

Flash Hat
Judo Outfit
108 Gems

Cheer Song, Battle Song, Nameless Song, Last Song
Charge+2, Charge+10



ZCKaiser
Male
Sagittarius
47
54
Bard
Black Magic
Faith Save
Equip Bow
Move+3

Ice Bow

Holy Miter
Chain Mail
Wizard Mantle

Angel Song, Cheer Song, Battle Song, Magic Song, Space Storage, Sky Demon
Fire, Fire 2, Fire 3, Bolt, Ice 2



Mpghappiness
Male
Capricorn
72
46
Archer
Basic Skill
Distribute
Doublehand
Ignore Terrain

Romanda Gun

Golden Hairpin
Chain Vest
Sprint Shoes

Charge+2
Accumulate, Throw Stone, Heal, Tickle, Cheer Up



Nifboy
Female
Pisces
50
67
Calculator
White Magic
HP Restore
Magic Attack UP
Move+1

Musk Rod

Holy Miter
Mythril Vest
Cursed Ring

CT, Prime Number, 5, 4, 3
Raise, Protect, Protect 2, Wall, Esuna

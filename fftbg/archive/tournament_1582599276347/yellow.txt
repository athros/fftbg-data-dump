Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DaveStrider55
Female
Libra
62
62
Knight
Throw
HP Restore
Long Status
Retreat

Save the Queen

Gold Helmet
Gold Armor
Magic Ring

Weapon Break, Speed Break, Stasis Sword, Justice Sword
Shuriken, Knife, Hammer



Selphet
Monster
Gemini
74
48
Hydra










Ass Brains
Female
Libra
68
57
Summoner
Elemental
Parry
Equip Axe
Swim

Flail

Thief Hat
Light Robe
Feather Mantle

Moogle, Shiva, Ramuh, Odin, Leviathan, Salamander, Fairy, Lich
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Trix04
Male
Aries
50
71
Chemist
Charge
Meatbone Slash
Equip Shield
Fly

Mythril Knife
Genji Shield
Black Hood
Mystic Vest
Diamond Armlet

Potion, X-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Charge+2, Charge+10

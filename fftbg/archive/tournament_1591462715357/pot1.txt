Final Bets: red - 14 bets for 8,273G (66.8%, x0.50); blue - 9 bets for 4,113G (33.2%, x2.01)

red bets:
Aldrammech: 1,000G (12.1%, 30,618G)
flowinprose: 1,000G (12.1%, 108,092G)
DeathTaxesAndAnime: 902G (10.9%, 902G)
Luizsanq: 876G (10.6%, 876G)
Monopool: 852G (10.3%, 852G)
maakur_: 644G (7.8%, 644G)
Thyrandaal: 580G (7.0%, 580G)
TinchoT: 568G (6.9%, 568G)
gogofromtogo: 531G (6.4%, 531G)
letdowncity: 500G (6.0%, 20,038G)
Lydian_C: 420G (5.1%, 9,225G)
the_shep: 200G (2.4%, 2,233G)
Milkman153: 100G (1.2%, 1,376G)
datadrivenbot: 100G (1.2%, 34,559G)

blue bets:
jakeduhhsnake: 1,296G (31.5%, 1,296G)
BirbBrainsBot: 1,000G (24.3%, 106,560G)
BlackFireUK: 500G (12.2%, 4,905G)
pandasforsale: 328G (8.0%, 328G)
Breakdown777: 300G (7.3%, 8,276G)
benticore: 300G (7.3%, 3,994G)
dinin991: 169G (4.1%, 169G)
Kithmar796: 120G (2.9%, 120G)
HokTuey: 100G (2.4%, 1,051G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Choco Joe
Male
Capricorn
79
68
Ninja
Sing
HP Restore
Equip Gun
Waterwalking

Papyrus Codex
Battle Folio
Red Hood
Power Sleeve
N-Kai Armlet

Shuriken, Bomb
Angel Song, Life Song, Nameless Song, Hydra Pit



Creggers
Monster
Scorpio
54
78
Blue Dragon










RunicMagus
Male
Leo
67
69
Summoner
Basic Skill
Meatbone Slash
Short Charge
Ignore Height

Dragon Rod

Twist Headband
Earth Clothes
Cursed Ring

Moogle, Shiva, Titan, Bahamut, Odin, Salamander, Fairy, Zodiac
Accumulate, Heal, Yell, Cheer Up, Scream



Superdevon1
Male
Leo
38
65
Thief
Punch Art
Counter Tackle
Martial Arts
Ignore Terrain

Platinum Sword

Flash Hat
Mystic Vest
Elf Mantle

Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Status
Spin Fist, Pummel, Earth Slash, Purification, Revive, Seal Evil

Player: !White
Team: White Team
Palettes: White/Blue



NIghtdew14
Female
Capricorn
51
54
Dancer
White Magic
HP Restore
Equip Bow
Retreat

Ultimus Bow

Triangle Hat
Mythril Vest
Leather Mantle

Wiznaibus, Nameless Dance, Dragon Pit
Raise, Regen, Shell, Esuna



Grandlanzer
Female
Taurus
52
52
Priest
Battle Skill
MA Save
Doublehand
Levitate

Morning Star

Leather Hat
Adaman Vest
Cursed Ring

Raise, Regen, Protect 2, Shell 2, Esuna
Weapon Break, Speed Break, Power Break, Stasis Sword, Surging Sword



NovaKnight21
Female
Capricorn
69
38
Time Mage
Throw
Arrow Guard
Equip Armor
Move-HP Up

Healing Staff

Twist Headband
Crystal Mail
Feather Mantle

Haste 2, Slow, Reflect, Demi, Demi 2, Stabilize Time
Staff



O Heyno
Female
Leo
75
80
Wizard
Steal
Faith Save
Equip Knife
Lava Walking

Koga Knife

Flash Hat
Leather Outfit
Feather Boots

Fire 2, Bolt, Empower
Steal Helmet, Steal Accessory, Steal Status

Player: !Green
Team: Green Team
Palettes: Green/White



SQUiDSQUARKLIN
Male
Libra
72
61
Calculator
White Magic
Dragon Spirit
Maintenance
Teleport 2

Battle Folio

Headgear
White Robe
Spike Shoes

Height, Prime Number, 5
Cure 2, Cure 3, Protect, Protect 2, Wall



Grandlanzer
Monster
Cancer
65
78
Chocobo










Mirapoix
Male
Aries
39
48
Chemist
White Magic
Distribute
Concentrate
Jump+3

Main Gauche

Leather Hat
Wizard Outfit
Sprint Shoes

Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Phoenix Down
Cure, Raise, Shell, Shell 2, Esuna



VolgraTheMoose
Female
Cancer
71
46
Mediator
Charge
Earplug
Magic Defense UP
Lava Walking

Battle Folio

Headgear
Mythril Vest
Small Mantle

Praise, Threaten, Insult, Refute
Charge+1, Charge+5, Charge+10

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



MyFakeLife
Male
Libra
55
63
Monk
Throw
Absorb Used MP
Sicken
Teleport



Holy Miter
Leather Outfit
Salty Rage

Spin Fist, Earth Slash, Chakra, Revive, Seal Evil
Bomb



Archelous
Male
Aquarius
75
80
Monk
Item
MP Restore
Magic Attack UP
Levitate



Green Beret
Brigandine
Leather Mantle

Wave Fist, Purification, Revive
Potion, Hi-Potion, Elixir, Antidote, Soft, Holy Water



Victoriolue
Male
Leo
66
45
Knight
Time Magic
MP Restore
Doublehand
Retreat

Ragnarok

Cross Helmet
Linen Cuirass
Bracer

Armor Break, Weapon Break, Justice Sword, Night Sword
Haste, Haste 2, Slow, Immobilize, Stabilize Time



Undyingshdw
Male
Aries
56
43
Knight
Black Magic
Critical Quick
Equip Polearm
Move+2

Gungnir
Aegis Shield
Crystal Helmet
Chameleon Robe
108 Gems

Armor Break, Mind Break, Surging Sword
Fire 2, Bolt, Empower

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nickyfive
Male
Pisces
56
51
Monk
Black Magic
Abandon
Concentrate
Levitate



Golden Hairpin
Judo Outfit
Leather Mantle

Spin Fist, Earth Slash, Secret Fist, Purification, Revive
Fire 2, Bolt 3, Bolt 4, Ice 4, Death, Flare



Draakaa
Female
Capricorn
62
54
Knight
Draw Out
MP Restore
Dual Wield
Move+1

Defender
Giant Axe
Mythril Helmet
Platinum Armor
Magic Gauntlet

Shield Break, Weapon Break, Stasis Sword
Koutetsu, Bizen Boat, Murasame, Kiyomori



Ominnous
Male
Gemini
57
46
Ninja
Summon Magic
HP Restore
Equip Sword
Teleport

Defender
Blood Sword
Cachusha
Judo Outfit
N-Kai Armlet

Bomb, Knife
Moogle, Titan, Odin, Leviathan, Cyclops



Alaylle
Male
Cancer
74
67
Archer
White Magic
Parry
Dual Wield
Move+3

Lightning Bow

Ribbon
Black Costume
Wizard Mantle

Charge+4, Charge+5, Charge+10
Cure, Cure 2, Cure 3, Raise, Raise 2, Wall

Final Bets: brown - 7 bets for 5,310G (39.5%, x1.53); champion - 11 bets for 8,129G (60.5%, x0.65)

brown bets:
braisethelard: 2,195G (41.3%, 2,195G)
prince_rogers_nelson_: 1,057G (19.9%, 1,057G)
Zachara: 875G (16.5%, 146,375G)
soren_of_tyto: 521G (9.8%, 521G)
CT_5_Holy: 462G (8.7%, 462G)
delcake: 100G (1.9%, 1,141G)
datadrivenbot: 100G (1.9%, 49,028G)

champion bets:
SkylerBunny: 2,000G (24.6%, 580,703G)
Chuckolator: 1,237G (15.2%, 38,328G)
nekojin: 1,000G (12.3%, 4,328G)
BirbBrainsBot: 1,000G (12.3%, 79,464G)
reinoe: 1,000G (12.3%, 28,182G)
getthemoneyz: 546G (6.7%, 1,010,705G)
SuzakuReii: 500G (6.2%, 4,372G)
Arcblazer23: 296G (3.6%, 296G)
Lythe_Caraker: 250G (3.1%, 104,558G)
Evewho: 200G (2.5%, 4,261G)
OverlordSS4: 100G (1.2%, 4,178G)

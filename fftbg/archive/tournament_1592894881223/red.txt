Player: !Red
Team: Red Team
Palettes: Red/Brown



BStarTV
Male
Leo
40
61
Monk
Basic Skill
PA Save
Beastmaster
Ignore Terrain

Cute Bag

Thief Hat
Mythril Vest
Leather Mantle

Pummel, Secret Fist, Revive
Accumulate, Dash, Throw Stone



Quadh0nk
Female
Cancer
54
63
Time Mage
Basic Skill
Hamedo
Magic Defense UP
Ignore Height

Iron Fan

Black Hood
Clothes
Elf Mantle

Haste, Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time, Galaxy Stop
Accumulate, Heal, Tickle, Wish



Nekojin
Male
Virgo
70
50
Knight
Charge
Counter
Defense UP
Levitate

Battle Axe
Aegis Shield
Platinum Helmet
Reflect Mail
Defense Armlet

Head Break, Speed Break, Power Break
Charge+1, Charge+4, Charge+5



Reinoe
Female
Sagittarius
79
60
Monk
White Magic
Abandon
Concentrate
Jump+2



Headgear
Earth Clothes
Defense Ring

Purification, Revive
Cure 3, Raise, Raise 2, Reraise, Esuna, Holy

Player: !Green
Team: Green Team
Palettes: Green/White



Forkmore
Male
Libra
41
78
Archer
Sing
Absorb Used MP
Equip Bow
Move+2

Mythril Bow

Thief Hat
Wizard Outfit
Elf Mantle

Charge+5
Magic Song, Last Song, Diamond Blade



Joewcarson
Male
Cancer
65
77
Knight
Elemental
Speed Save
Equip Polearm
Waterwalking

Partisan
Hero Shield
Circlet
Chain Mail
Dracula Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind



ArashiKurobara
Male
Virgo
72
78
Summoner
Item
Counter
Beastmaster
Move-MP Up

Faith Rod

Feather Hat
White Robe
Small Mantle

Moogle, Shiva, Golem, Carbunkle, Leviathan, Salamander, Cyclops
Potion, X-Potion, Ether, Hi-Ether, Antidote, Echo Grass, Phoenix Down



VolgraTheMoose
Male
Capricorn
59
46
Chemist
Charge
Catch
Equip Shield
Ignore Terrain

Star Bag
Diamond Shield
Triangle Hat
Rubber Costume
Sprint Shoes

Antidote, Eye Drop, Soft, Holy Water, Phoenix Down
Charge+1, Charge+4, Charge+5, Charge+10

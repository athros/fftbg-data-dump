Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SQUiDSQUARKLIN
Male
Aquarius
59
77
Samurai
Charge
Parry
Secret Hunt
Jump+3

Muramasa

Iron Helmet
Crystal Mail
Bracer

Koutetsu, Masamune
Charge+1, Charge+4



Nhammen
Male
Capricorn
38
45
Knight
Time Magic
Earplug
Beastmaster
Move+2

Rune Blade
Flame Shield
Diamond Helmet
Chameleon Robe
Germinas Boots

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Stasis Sword, Explosion Sword
Haste 2, Stop, Quick



Brokenknight201
Female
Libra
50
45
Samurai
Throw
Earplug
Equip Knife
Waterbreathing

Hidden Knife

Grand Helmet
Wizard Robe
Genji Gauntlet

Bizen Boat, Kiyomori
Shuriken, Knife



Lastly
Male
Virgo
54
46
Bard
White Magic
Earplug
Attack UP
Jump+2

Bloody Strings

Headgear
Genji Armor
Sprint Shoes

Angel Song, Cheer Song, Magic Song, Nameless Song, Sky Demon, Hydra Pit
Cure 2, Raise, Reraise, Regen, Protect, Protect 2, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Chef1984
Male
Pisces
64
48
Thief
Punch Art
Counter Flood
Secret Hunt
Move+2

Hidden Knife

Holy Miter
Judo Outfit
Dracula Mantle

Steal Helmet, Arm Aim, Leg Aim
Spin Fist, Purification, Chakra, Revive



I3ahamut Prime
Female
Aries
75
47
Lancer
Summon Magic
Earplug
Short Status
Retreat

Javelin
Diamond Shield
Bronze Helmet
Wizard Robe
Sprint Shoes

Level Jump8, Vertical Jump7
Moogle, Ifrit, Carbunkle, Fairy



HughJeffner
Female
Taurus
50
54
Priest
Yin Yang Magic
Distribute
Short Charge
Move+2

Morning Star

Thief Hat
Judo Outfit
Diamond Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Protect, Protect 2, Esuna, Holy, Magic Barrier
Doubt Faith, Zombie, Confusion Song, Dispel Magic



SneakyFingerz
Male
Pisces
73
61
Geomancer
Throw
Brave Up
Equip Knife
Move-HP Up

Air Knife
Mythril Shield
Twist Headband
Chameleon Robe
Rubber Shoes

Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Wand

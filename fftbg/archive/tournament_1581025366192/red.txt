Player: !Red
Team: Red Team
Palettes: Red/Brown



Riot Jayway
Female
Aquarius
47
78
Knight
Talk Skill
Regenerator
Equip Axe
Ignore Terrain

Slasher
Aegis Shield
Circlet
Light Robe
Defense Ring

Armor Break, Shield Break, Magic Break, Power Break, Dark Sword
Death Sentence, Insult, Refute



DudeMonkey77
Male
Taurus
58
51
Chemist
Throw
Parry
Equip Gun
Move-HP Up

Fairy Harp

Red Hood
Power Sleeve
N-Kai Armlet

Potion, Antidote, Maiden's Kiss, Phoenix Down
Bomb, Knife, Dictionary



ArchKnightX
Female
Aries
61
46
Monk
Battle Skill
Brave Up
Defend
Teleport

Hydra Bag

Green Beret
Wizard Outfit
Bracer

Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil
Head Break



Willjin
Female
Gemini
57
46
Monk
Talk Skill
Sunken State
Magic Defense UP
Jump+3



Feather Hat
Leather Vest
N-Kai Armlet

Wave Fist, Secret Fist, Purification, Revive
Invitation, Preach, Solution, Death Sentence, Insult, Negotiate

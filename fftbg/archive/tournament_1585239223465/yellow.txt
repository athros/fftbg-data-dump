Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ShintaroNayaka
Female
Aquarius
53
75
Chemist
Elemental
Critical Quick
Secret Hunt
Move+3

Mythril Gun

Green Beret
Clothes
Dracula Mantle

Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball



SealPoulpe
Male
Aries
74
39
Calculator
Time Magic
Parry
Halve MP
Jump+1

Papyrus Codex

Golden Hairpin
Robe of Lords
Angel Ring

Height, 4
Haste 2, Slow 2, Stop, Immobilize, Float, Demi, Demi 2, Stabilize Time



BadBlanket
Monster
Gemini
76
65
Ghoul










AlenaZarek
Male
Sagittarius
52
48
Chemist
Basic Skill
Distribute
Long Status
Ignore Height

Panther Bag

Triangle Hat
Wizard Outfit
Red Shoes

Potion, X-Potion, Ether, Soft, Holy Water, Remedy, Phoenix Down
Accumulate, Heal, Fury

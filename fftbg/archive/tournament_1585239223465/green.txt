Player: !Green
Team: Green Team
Palettes: Green/White



MalakoFox
Male
Aries
58
47
Lancer
Battle Skill
Sunken State
Dual Wield
Swim

Musk Rod
Holy Lance
Platinum Helmet
Carabini Mail
108 Gems

Level Jump8, Vertical Jump4
Armor Break, Weapon Break, Magic Break, Mind Break



Heroebal
Male
Libra
74
63
Archer
Time Magic
Auto Potion
Concentrate
Levitate

Yoichi Bow

Mythril Helmet
Secret Clothes
Dracula Mantle

Charge+2, Charge+3, Charge+4, Charge+10, Charge+20
Haste, Haste 2, Slow, Slow 2, Float, Stabilize Time



MeleeWizard
Male
Taurus
77
54
Wizard
Charge
Regenerator
Equip Knife
Ignore Terrain

Dragon Rod

Twist Headband
Black Robe
Leather Mantle

Fire, Fire 2, Bolt 3, Ice 4
Charge+2, Charge+3, Charge+4, Charge+5, Charge+10, Charge+20



Aurilliux
Female
Gemini
43
61
Oracle
Talk Skill
Mana Shield
Monster Talk
Lava Walking

Iron Fan

Thief Hat
Chameleon Robe
Magic Ring

Poison, Pray Faith, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Sleep, Petrify
Threaten, Negotiate

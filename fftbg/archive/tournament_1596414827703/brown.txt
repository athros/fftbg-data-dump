Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Dogsandcatsand
Male
Scorpio
79
38
Monk
Yin Yang Magic
Parry
Equip Sword
Move-HP Up

Koutetsu Knife

Triangle Hat
Mythril Vest
Bracer

Earth Slash, Purification, Chakra, Revive
Spell Absorb, Life Drain, Pray Faith, Silence Song, Foxbird, Dispel Magic, Paralyze, Sleep



Lyans00
Male
Aquarius
51
53
Archer
Basic Skill
Speed Save
Equip Shield
Teleport

Snipe Bow
Escutcheon
Headgear
Adaman Vest
Bracer

Charge+1, Charge+4, Charge+5
Dash, Throw Stone, Heal, Yell, Cheer Up, Fury, Scream



Laserman1000
Female
Libra
68
46
Thief
Item
Counter
Equip Bow
Ignore Height

Mythril Bow

Headgear
Clothes
Wizard Mantle

Steal Heart, Steal Armor, Leg Aim
Potion, Hi-Potion, X-Potion, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Hasterious
Male
Taurus
50
51
Lancer
Time Magic
Parry
Secret Hunt
Ignore Terrain

Javelin
Round Shield
Diamond Helmet
Plate Mail
Bracer

Level Jump2, Vertical Jump8
Haste, Haste 2, Slow, Immobilize, Quick, Stabilize Time

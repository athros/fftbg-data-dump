Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NoxeGS
Female
Aries
52
48
Summoner
White Magic
Mana Shield
Short Status
Teleport

Poison Rod

Red Hood
Chameleon Robe
Diamond Armlet

Moogle, Shiva, Ifrit, Carbunkle, Salamander, Silf, Lich
Cure 4, Raise, Protect, Shell 2, Esuna, Holy



PolloPuerco
Female
Leo
61
43
Geomancer
Steal
MP Restore
Halve MP
Jump+2

Giant Axe
Diamond Shield
Black Hood
Secret Clothes
Leather Mantle

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Steal Heart, Steal Helmet, Steal Shield, Steal Accessory, Leg Aim



Lydian C
Female
Aquarius
69
69
Time Mage
Draw Out
Parry
Sicken
Ignore Height

Rainbow Staff

Ribbon
Wizard Robe
Feather Mantle

Haste 2, Slow, Slow 2, Immobilize, Float, Reflect, Demi
Asura



Cold Sauce
Female
Sagittarius
63
75
Knight
Dance
Counter Magic
Sicken
Jump+3

Slasher
Aegis Shield
Leather Helmet
Black Robe
Genji Gauntlet

Head Break, Power Break, Mind Break, Justice Sword
Slow Dance, Nameless Dance

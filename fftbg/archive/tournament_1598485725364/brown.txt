Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sairentozon7
Female
Virgo
79
44
Calculator
Catfish Skill
Counter
Long Status
Jump+1

Gokuu Rod
Gold Shield
Feather Hat
Wizard Robe
Leather Mantle

Blue Magic
Scratch, Poison Nail, Cat Kick, Blaster, Blood Suck, Tentacle, Black Ink, Odd Soundwave, Mind Blast, Level Blast



CosmicTactician
Female
Aries
78
69
Squire
Draw Out
Mana Shield
Equip Axe
Waterwalking

Rainbow Staff
Round Shield
Gold Helmet
Earth Clothes
Genji Gauntlet

Dash, Heal, Tickle, Yell, Wish
Koutetsu, Bizen Boat, Murasame



SkylerBunny
Female
Virgo
53
52
Archer
Draw Out
Catch
Equip Axe
Fly

Slasher
Aegis Shield
Barbuta
Earth Clothes
Sprint Shoes

Charge+1, Charge+2
Koutetsu, Heaven's Cloud, Kikuichimoji



Thunderducker
Male
Sagittarius
67
76
Thief
Talk Skill
Brave Save
Equip Shield
Jump+1

Cultist Dagger
Round Shield
Holy Miter
Earth Clothes
Rubber Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield
Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Negotiate, Refute

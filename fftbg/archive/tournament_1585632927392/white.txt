Player: !White
Team: White Team
Palettes: White/Blue



Chronoxtrigger
Female
Capricorn
41
78
Ninja
Time Magic
Counter Tackle
Equip Sword
Fly

Sleep Sword
Chirijiraden
Twist Headband
Power Sleeve
Bracer

Shuriken, Knife, Ninja Sword
Haste, Slow 2, Float, Demi 2, Stabilize Time, Meteor



Numbersborne
Female
Cancer
55
75
Chemist
Black Magic
Counter Flood
Equip Polearm
Jump+3

Battle Bamboo

Holy Miter
Mythril Vest
Dracula Mantle

Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy
Fire, Fire 3, Bolt 3, Ice 2, Ice 3, Death, Flare



Bluuuuuuuh
Female
Aquarius
48
63
Priest
Battle Skill
Arrow Guard
Magic Defense UP
Fly

Rainbow Staff

Holy Miter
Earth Clothes
Battle Boots

Raise, Raise 2, Reraise, Regen, Protect, Shell, Esuna
Head Break, Shield Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword, Surging Sword



Cougboi
Female
Libra
63
53
Summoner
Jump
Damage Split
Equip Bow
Move-HP Up

Cross Bow

Twist Headband
Linen Robe
Power Wrist

Moogle, Shiva, Ifrit, Carbunkle, Leviathan, Silf, Fairy, Lich
Level Jump2, Vertical Jump8

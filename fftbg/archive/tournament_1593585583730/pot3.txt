Final Bets: white - 16 bets for 7,431G (65.2%, x0.53); black - 6 bets for 3,963G (34.8%, x1.88)

white bets:
lowlf: 1,212G (16.3%, 94,582G)
helpimabug: 1,008G (13.6%, 1,008G)
BirbBrainsBot: 1,000G (13.5%, 29,529G)
Smokegiant: 1,000G (13.5%, 92,373G)
DamnThatShark: 704G (9.5%, 704G)
Magicandy: 400G (5.4%, 31,191G)
ColetteMSLP: 400G (5.4%, 6,766G)
InskipJester: 371G (5.0%, 371G)
LAGBOT30000: 300G (4.0%, 3,931G)
cougboi: 300G (4.0%, 16,520G)
ThreeMileIsland: 200G (2.7%, 509G)
letdowncity: 150G (2.0%, 10,943G)
Aquios_: 100G (1.3%, 3,323G)
datadrivenbot: 100G (1.3%, 50,223G)
CT_5_Holy: 100G (1.3%, 4,002G)
getthemoneyz: 86G (1.2%, 1,126,824G)

black bets:
Firesheath: 1,501G (37.9%, 1,501G)
HuffFlex: 1,000G (25.2%, 2,627G)
Zachara: 1,000G (25.2%, 135,500G)
Phytik: 352G (8.9%, 352G)
moocaotao: 100G (2.5%, 4,902G)
moonliquor: 10G (0.3%, 578G)

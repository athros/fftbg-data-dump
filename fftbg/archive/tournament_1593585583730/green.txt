Player: !Green
Team: Green Team
Palettes: Green/White



Sairentozon7
Male
Cancer
49
42
Squire
Battle Skill
Dragon Spirit
Doublehand
Move+2

Broad Sword

Bronze Helmet
Earth Clothes
108 Gems

Dash, Yell, Scream
Shield Break, Weapon Break, Power Break, Stasis Sword, Dark Sword



Vorackgriever
Female
Cancer
48
63
Chemist
Jump
Speed Save
Equip Bow
Move+2

Windslash Bow

Holy Miter
Mystic Vest
Red Shoes

Hi-Ether, Antidote, Phoenix Down
Level Jump2, Vertical Jump4



Cougboi
Female
Aries
71
70
Mime

Hamedo
Attack UP
Move-MP Up



Headgear
Secret Clothes
Spike Shoes

Mimic




Jaritras
Male
Serpentarius
43
79
Chemist
Talk Skill
Auto Potion
Equip Bow
Fly

Night Killer

Leather Hat
Rubber Costume
Small Mantle

Potion, X-Potion, Elixir, Antidote, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down
Threaten, Insult, Mimic Daravon, Refute, Rehabilitate

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SkylerBunny
Male
Libra
62
52
Ninja
Black Magic
Counter Tackle
Short Charge
Waterwalking

Hidden Knife
Scorpion Tail
Thief Hat
Judo Outfit
Leather Mantle

Shuriken, Wand
Fire, Fire 2, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 4, Frog



HaateXIII
Male
Virgo
52
64
Summoner
Jump
Critical Quick
Equip Polearm
Move+2

Holy Lance

Red Hood
White Robe
Battle Boots

Moogle, Shiva, Ifrit, Titan, Golem, Leviathan, Silf
Level Jump5, Vertical Jump8



DeathTaxesAndAnime
Female
Pisces
47
70
Priest
Punch Art
Speed Save
Martial Arts
Waterbreathing

Gold Staff

Black Hood
Black Robe
Cursed Ring

Cure 2, Cure 3, Raise, Raise 2, Protect, Shell, Wall, Esuna
Pummel, Purification, Chakra, Revive



Meanderandreturn
Female
Aries
46
69
Ninja
Dance
PA Save
Halve MP
Jump+2

Kunai
Ninja Edge
Black Hood
Wizard Outfit
Magic Gauntlet

Shuriken, Staff
Polka Polka, Disillusion, Void Storage, Nether Demon

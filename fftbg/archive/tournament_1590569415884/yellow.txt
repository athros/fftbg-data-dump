Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



The Pengwin
Female
Aquarius
61
68
Squire
Talk Skill
Auto Potion
Equip Polearm
Move+1

Ryozan Silk
Bronze Shield
Barette
Clothes
Small Mantle

Dash, Throw Stone, Heal, Tickle
Invitation, Praise, Threaten, Preach, Death Sentence, Insult, Negotiate, Refute



NovaKnight21
Male
Libra
49
45
Ninja
Yin Yang Magic
Auto Potion
Attack UP
Move+2

Flame Whip
Sasuke Knife
Triangle Hat
Power Sleeve
Genji Gauntlet

Shuriken, Knife
Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Foxbird, Dark Holy



Gorgewall
Female
Taurus
46
40
Ninja
Talk Skill
HP Restore
Doublehand
Move-HP Up

Short Edge

Headgear
Adaman Vest
Angel Ring

Shuriken, Bomb, Knife
Invitation, Persuade, Praise, Threaten, Preach, Death Sentence, Mimic Daravon, Refute



ApplesauceBoss
Male
Aquarius
72
51
Knight
Jump
Distribute
Attack UP
Ignore Terrain

Ragnarok
Bronze Shield
Mythril Helmet
Bronze Armor
Jade Armlet

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break
Level Jump5, Vertical Jump5

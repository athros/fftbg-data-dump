Player: !White
Team: White Team
Palettes: White/Blue



Soren Of Tyto
Female
Scorpio
52
70
Thief
Punch Art
Arrow Guard
Defense UP
Move+2

Orichalcum

Flash Hat
Wizard Outfit
Diamond Armlet

Steal Helmet, Steal Status, Leg Aim
Earth Slash, Secret Fist, Chakra, Revive



Moonliquor
Female
Taurus
74
54
Summoner
Punch Art
Mana Shield
Doublehand
Move+1

Rod

Leather Hat
Chain Vest
Defense Ring

Moogle, Shiva, Ramuh, Titan, Golem, Fairy
Purification, Chakra, Revive, Seal Evil



Thenextlantern
Female
Capricorn
59
69
Thief
Draw Out
Counter
Equip Shield
Move+1

Blind Knife
Bronze Shield
Twist Headband
Wizard Outfit
Cursed Ring

Steal Heart, Steal Armor, Arm Aim
Asura



Evewho
Female
Pisces
75
50
Knight
Yin Yang Magic
Auto Potion
Doublehand
Fly

Ragnarok

Barbuta
Black Robe
Spike Shoes

Head Break, Armor Break, Shield Break, Speed Break, Justice Sword, Surging Sword
Spell Absorb, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Petrify

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



SneakyFingerz
Male
Capricorn
48
53
Knight
Jump
HP Restore
Magic Attack UP
Teleport

Giant Axe
Buckler
Gold Helmet
Chameleon Robe
Angel Ring

Armor Break, Weapon Break, Speed Break, Power Break, Justice Sword, Surging Sword
Level Jump8, Vertical Jump2



MoonSlayerRS
Female
Cancer
63
51
Ninja
Battle Skill
Arrow Guard
Equip Bow
Swim

Perseus Bow

Green Beret
Adaman Vest
Defense Armlet

Shuriken, Sword
Power Break, Justice Sword, Dark Sword



Tithonus
Male
Gemini
73
69
Geomancer
Time Magic
Dragon Spirit
Equip Knife
Move+2

Sasuke Knife
Ice Shield
Feather Hat
Earth Clothes
Bracer

Pitfall, Water Ball, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Haste 2, Float, Demi, Stabilize Time



Frostxjr
Male
Capricorn
44
60
Mediator
Jump
MP Restore
Long Status
Lava Walking

Bestiary

Thief Hat
Linen Robe
Jade Armlet

Praise, Preach, Death Sentence, Negotiate, Mimic Daravon, Refute
Level Jump2, Vertical Jump2

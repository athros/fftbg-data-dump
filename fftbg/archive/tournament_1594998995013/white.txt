Player: !White
Team: White Team
Palettes: White/Blue



Killth3kid
Male
Virgo
40
66
Wizard
Summon Magic
Counter Tackle
Martial Arts
Waterwalking



Black Hood
Wizard Robe
Bracer

Fire 2, Fire 3, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4
Carbunkle, Odin, Leviathan, Fairy



Randgridr
Male
Sagittarius
77
54
Ninja
Summon Magic
Critical Quick
Equip Polearm
Ignore Height

Holy Lance
Ryozan Silk
Golden Hairpin
Adaman Vest
Red Shoes

Bomb, Staff
Ramuh, Ifrit, Salamander, Silf



Rytor
Female
Aquarius
55
59
Squire
Punch Art
HP Restore
Halve MP
Teleport

Bow Gun
Escutcheon
Black Hood
Clothes
Cursed Ring

Dash, Throw Stone, Heal, Ultima
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive



Joewcarson
Female
Cancer
79
43
Mime

Mana Shield
Equip Shield
Ignore Height


Platinum Shield
Leather Hat
Light Robe
Wizard Mantle

Mimic


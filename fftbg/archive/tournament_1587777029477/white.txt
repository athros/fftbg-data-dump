Player: !White
Team: White Team
Palettes: White/Blue



Nizaha
Male
Taurus
63
69
Geomancer
Summon Magic
Sunken State
Equip Sword
Swim

Mythril Sword
Platinum Shield
Leather Hat
Silk Robe
Bracer

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard
Moogle, Shiva, Ramuh, Ifrit, Golem, Leviathan



Conome
Male
Aries
45
63
Wizard
Steal
Dragon Spirit
Attack UP
Ignore Height

Rod

Flash Hat
Wizard Robe
Cursed Ring

Fire 3, Bolt, Bolt 3, Empower, Death
Steal Armor



Sovattaro
Male
Pisces
46
49
Oracle
Summon Magic
Meatbone Slash
Equip Polearm
Lava Walking

Octagon Rod

Feather Hat
White Robe
Leather Mantle

Life Drain, Pray Faith, Blind Rage, Foxbird, Dark Holy
Moogle, Ramuh, Ifrit, Golem, Bahamut, Odin, Leviathan



Arch8000
Female
Scorpio
49
49
Priest
Dance
Brave Up
Equip Bow
Move+2

Lightning Bow

Leather Hat
Wizard Robe
Sprint Shoes

Raise, Reraise, Regen, Protect 2, Shell 2, Wall, Esuna, Holy
Wiznaibus, Disillusion, Last Dance, Obsidian Blade, Void Storage

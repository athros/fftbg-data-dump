Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Leo
71
73
Lancer
Summon Magic
Speed Save
Short Charge
Ignore Height

Partisan
Buckler
Barbuta
Silk Robe
Diamond Armlet

Level Jump8, Vertical Jump7
Moogle, Titan, Carbunkle, Leviathan, Silf, Fairy



Alacor
Male
Capricorn
75
48
Knight
White Magic
MA Save
Doublehand
Ignore Height

Blood Sword

Mythril Helmet
Linen Cuirass
Defense Ring

Head Break, Armor Break, Shield Break, Magic Break, Power Break, Dark Sword
Cure 2, Regen, Esuna



Sebsebseabass
Male
Virgo
79
55
Priest
Time Magic
Faith Up
Doublehand
Move+2

Oak Staff

Headgear
Clothes
Germinas Boots

Cure, Raise, Raise 2, Protect, Protect 2, Esuna
Haste 2, Slow, Stop, Quick, Demi, Stabilize Time



Wonderkush
Male
Capricorn
76
72
Archer
Draw Out
Counter Flood
Equip Bow
Move+2

Long Bow

Feather Hat
Judo Outfit
108 Gems

Charge+1, Charge+3, Charge+5, Charge+7
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



DavenIII
Male
Gemini
62
53
Monk
Battle Skill
Speed Save
Dual Wield
Move+3



Flash Hat
Power Sleeve
Bracer

Spin Fist, Purification, Revive
Magic Break, Mind Break, Justice Sword, Surging Sword



Wahsup
Male
Leo
62
78
Summoner
Elemental
Blade Grasp
Equip Polearm
Move+3

Spear

Headgear
Black Robe
Jade Armlet

Moogle, Titan, Golem, Bahamut, Odin, Leviathan, Fairy, Lich, Cyclops
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Mirapoix
Male
Pisces
80
42
Wizard
Summon Magic
Faith Up
Equip Polearm
Move-MP Up

Rod

Green Beret
Leather Vest
Cursed Ring

Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Ice
Moogle, Shiva, Titan, Carbunkle



Kingbananabear
Monster
Aries
55
72
Dragon








Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Jigglefluffenstuff
Male
Gemini
56
54
Monk
Steal
Mana Shield
Equip Shield
Waterbreathing


Escutcheon
Holy Miter
Wizard Outfit
Sprint Shoes

Spin Fist, Pummel, Secret Fist, Purification, Chakra, Revive, Seal Evil
Steal Shield, Steal Weapon, Steal Accessory, Steal Status



Hzor
Monster
Taurus
75
65
Steel Giant










Laserman1000
Male
Pisces
42
73
Knight
Steal
Damage Split
Concentrate
Ignore Terrain

Save the Queen

Gold Helmet
Bronze Armor
Battle Boots

Weapon Break, Power Break, Justice Sword, Dark Sword, Night Sword
Steal Heart, Steal Helmet, Steal Armor



LuckyLuckLuc2
Female
Cancer
54
51
Priest
Summon Magic
Counter Flood
Magic Attack UP
Waterbreathing

Rainbow Staff

Green Beret
Silk Robe
Feather Mantle

Cure, Cure 4, Raise, Raise 2, Reraise, Protect, Shell 2, Wall, Holy
Moogle, Shiva, Ifrit, Carbunkle, Bahamut, Cyclops

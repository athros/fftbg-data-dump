Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Hales Bopp It
Female
Serpentarius
65
41
Ninja
Yin Yang Magic
Abandon
Equip Armor
Move-HP Up

Mage Masher
Blind Knife
Barbuta
Platinum Armor
Leather Mantle

Bomb, Axe
Blind, Poison, Zombie, Foxbird, Dispel Magic, Paralyze



Sinnyil2
Male
Leo
61
79
Thief
Talk Skill
Counter Magic
Dual Wield
Move-HP Up

Hidden Knife
Kunai
Flash Hat
Black Costume
Spike Shoes

Steal Helmet, Steal Shield, Steal Weapon, Steal Status, Leg Aim
Invitation, Persuade, Praise, Threaten, Preach, Refute



MoonSlayerRS
Male
Aquarius
49
79
Samurai
Item
Counter Flood
Maintenance
Levitate

Masamune

Cross Helmet
Linen Robe
Battle Boots

Asura, Bizen Boat, Heaven's Cloud
Hi-Potion, X-Potion, Eye Drop, Echo Grass, Soft, Phoenix Down



SomeCallMeGON
Female
Sagittarius
78
63
Monk
Throw
HP Restore
Dual Wield
Move+3



Thief Hat
Clothes
Red Shoes

Wave Fist, Earth Slash, Secret Fist, Purification, Revive
Bomb, Knife

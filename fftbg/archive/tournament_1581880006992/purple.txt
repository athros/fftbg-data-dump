Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Kinzata
Female
Aquarius
78
66
Mime

MA Save
Equip Armor
Ignore Terrain



Red Hood
Diamond Armor
Leather Mantle

Mimic




Shadowkept
Female
Sagittarius
72
52
Priest
Basic Skill
Meatbone Slash
Beastmaster
Fly

Healing Staff

Golden Hairpin
Earth Clothes
Spike Shoes

Cure 2, Raise, Raise 2, Reraise, Protect, Shell 2, Wall, Esuna
Throw Stone



Jptawok
Female
Aquarius
45
71
Dancer
Throw
Auto Potion
Martial Arts
Swim

Cashmere

Black Hood
Black Robe
Jade Armlet

Witch Hunt, Polka Polka, Nameless Dance, Obsidian Blade, Nether Demon, Dragon Pit
Shuriken



Chocobodundee
Female
Capricorn
57
48
Thief
White Magic
Counter
Equip Shield
Waterbreathing

Cultist Dagger
Flame Shield
Triangle Hat
Judo Outfit
Defense Armlet

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Raise 2, Shell 2, Wall, Holy

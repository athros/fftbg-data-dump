Player: !Black
Team: Black Team
Palettes: Black/Red



Elelor
Monster
Virgo
66
55
Iron Hawk










Mythren
Female
Libra
77
58
Mime

Hamedo
Defense UP
Retreat



Crystal Helmet
Secret Clothes
Sprint Shoes

Mimic




Wonkierlynx
Male
Taurus
47
69
Monk
Yin Yang Magic
Auto Potion
Concentrate
Jump+3



Leather Hat
Black Costume
Feather Boots

Pummel, Purification, Seal Evil
Blind, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Sleep



ZephyrTempest
Female
Sagittarius
68
49
Knight
Steal
Meatbone Slash
Maintenance
Move+2

Ragnarok

Iron Helmet
White Robe
Sprint Shoes

Armor Break
Steal Accessory, Steal Status

Player: !White
Team: White Team
Palettes: White/Blue



Lord Burrah
Male
Scorpio
64
67
Calculator
Black Magic
Abandon
Attack UP
Teleport

Flame Rod

Twist Headband
Black Robe
Rubber Shoes

CT, Height, Prime Number, 5, 4, 3
Fire, Fire 2, Fire 3, Bolt 3, Ice 4



Galkife
Female
Cancer
70
54
Lancer
Punch Art
Arrow Guard
Equip Sword
Waterbreathing

Defender

Circlet
Leather Armor
Diamond Armlet

Level Jump3, Vertical Jump7
Pummel, Earth Slash, Secret Fist, Purification, Chakra, Revive



Mikeyoffbeat
Male
Aquarius
80
78
Oracle
Summon Magic
Abandon
Dual Wield
Move-MP Up

Papyrus Codex
Papyrus Codex
Cachusha
Wizard Robe
Red Shoes

Life Drain, Pray Faith, Doubt Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic
Moogle, Odin, Silf



Fithoslusec
Female
Aquarius
71
76
Lancer
Elemental
Sunken State
Equip Sword
Move-HP Up

Coral Sword
Kaiser Plate
Genji Helmet
Diamond Armor
Leather Mantle

Level Jump2, Vertical Jump3
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

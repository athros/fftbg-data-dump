Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ApplesauceBoss
Female
Leo
50
72
Archer
Dance
Absorb Used MP
Equip Sword
Move+1

Coral Sword
Platinum Shield
Leather Hat
Wizard Outfit
Power Wrist

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7
Wiznaibus, Slow Dance, Polka Polka, Disillusion, Void Storage



Dynasti
Male
Virgo
63
66
Knight
Charge
Counter Magic
Dual Wield
Waterbreathing

Slasher
Battle Axe
Bronze Helmet
Gold Armor
Rubber Shoes

Head Break, Weapon Break, Mind Break, Stasis Sword, Justice Sword
Charge+2, Charge+5



MixReveal
Monster
Aries
58
76
Blue Dragon










Powergems
Female
Taurus
56
47
Thief
Charge
MP Restore
Concentrate
Swim

Main Gauche

Twist Headband
Leather Outfit
Spike Shoes

Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Charge+1

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nekojin
Female
Leo
67
49
Lancer
Charge
Meatbone Slash
Sicken
Lava Walking

Obelisk
Crystal Shield
Bronze Helmet
Chain Mail
Salty Rage

Level Jump3, Vertical Jump8
Charge+1, Charge+4, Charge+7



VolgraTheMoose
Female
Gemini
57
42
Samurai
Summon Magic
Mana Shield
Short Status
Move+3

Asura Knife

Leather Helmet
Genji Armor
Feather Mantle

Murasame, Muramasa
Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Odin, Leviathan, Salamander, Fairy, Lich



Mrfripps
Monster
Scorpio
70
65
Revenant










Error72
Female
Libra
73
41
Oracle
Draw Out
Counter Flood
Equip Armor
Retreat

Octagon Rod

Green Beret
Crystal Mail
Magic Gauntlet

Life Drain, Pray Faith, Zombie, Silence Song, Foxbird, Dispel Magic, Sleep
Bizen Boat, Murasame

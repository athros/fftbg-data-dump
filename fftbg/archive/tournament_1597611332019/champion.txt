Player: !zChamp
Team: Champion Team
Palettes: White/Blue



HASTERIOUS
Female
Gemini
40
46
Geomancer
Draw Out
Auto Potion
Magic Defense UP
Move-HP Up

Hydra Bag
Ice Shield
Green Beret
Judo Outfit
Dracula Mantle

Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji



ArchKnightX
Male
Pisces
75
53
Mediator
White Magic
Arrow Guard
Martial Arts
Fly



Green Beret
Wizard Outfit
Genji Gauntlet

Praise, Preach, Solution, Death Sentence, Refute
Cure, Raise, Shell, Wall, Esuna, Holy



VolgraTheMoose
Monster
Aquarius
66
70
Tiamat










Fattunaking
Male
Capricorn
75
61
Samurai
Battle Skill
MP Restore
Equip Shield
Ignore Height

Kikuichimoji
Bronze Shield
Platinum Helmet
Linen Cuirass
N-Kai Armlet

Asura, Koutetsu, Bizen Boat, Muramasa
Magic Break, Speed Break

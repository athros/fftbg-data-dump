Player: !Black
Team: Black Team
Palettes: Black/Red



Kl0kwurk
Monster
Scorpio
79
49
Holy Dragon










Alrightbye
Female
Aries
74
79
Oracle
Steal
Critical Quick
Dual Wield
Waterbreathing

Bestiary
Papyrus Codex
Golden Hairpin
Silk Robe
N-Kai Armlet

Poison, Spell Absorb, Life Drain, Silence Song, Dispel Magic, Sleep, Dark Holy
Steal Heart, Steal Armor, Steal Weapon, Steal Status



Smegma Sorcerer
Male
Cancer
67
49
Oracle
Throw
Faith Up
Short Status
Teleport

Musk Rod

Flash Hat
Linen Robe
Angel Ring

Blind, Spell Absorb, Life Drain, Dispel Magic, Petrify
Shuriken, Hammer



Bad1dea
Female
Scorpio
71
65
Wizard
Charge
Counter Tackle
Equip Gun
Ignore Terrain

Blast Gun

Triangle Hat
Black Robe
Rubber Shoes

Fire, Fire 2, Fire 3, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice, Ice 2, Ice 3, Empower
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10

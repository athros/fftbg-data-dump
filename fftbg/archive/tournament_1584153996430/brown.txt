Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BadBlanket
Male
Serpentarius
51
73
Ninja
Charge
Faith Up
Equip Bow
Move-MP Up

Perseus Bow

Triangle Hat
Wizard Outfit
Defense Ring

Shuriken, Staff
Charge+1, Charge+2, Charge+3, Charge+4



ZephyrTempest
Male
Scorpio
54
58
Thief
Talk Skill
Meatbone Slash
Secret Hunt
Lava Walking

Short Edge

Flash Hat
Leather Outfit
N-Kai Armlet

Steal Helmet, Steal Shield, Steal Status, Arm Aim
Invitation, Death Sentence



BenYuPoker
Male
Aquarius
55
54
Monk
Black Magic
Catch
Dual Wield
Move+3



Red Hood
Wizard Outfit
Feather Boots

Pummel, Wave Fist, Secret Fist, Purification
Fire 2, Fire 4, Bolt 2



CandyTamer
Female
Leo
64
65
Lancer
Talk Skill
MP Restore
Martial Arts
Swim


Gold Shield
Leather Helmet
Linen Cuirass
Defense Armlet

Level Jump2, Vertical Jump6
Persuade, Praise, Threaten, Negotiate, Mimic Daravon, Refute

Player: !Red
Team: Red Team
Palettes: Red/Brown



Astrozin11
Male
Aquarius
62
70
Samurai
Steal
Earplug
Short Status
Teleport

Partisan

Diamond Helmet
Robe of Lords
Elf Mantle

Koutetsu, Bizen Boat, Kiyomori, Kikuichimoji
Gil Taking, Steal Armor, Steal Shield, Steal Status



JTB
Female
Aries
50
72
Oracle
Elemental
Sunken State
Long Status
Move+1

Papyrus Codex

Leather Hat
Light Robe
Magic Gauntlet

Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Cryptopsy70
Female
Libra
79
56
Lancer
Dance
Hamedo
Magic Attack UP
Ignore Terrain

Mythril Spear
Buckler
Bronze Helmet
White Robe
Battle Boots

Level Jump3, Vertical Jump5
Witch Hunt, Wiznaibus, Last Dance, Nether Demon



UmbraKnights
Female
Aquarius
76
65
Lancer
Time Magic
HP Restore
Magic Defense UP
Jump+3

Ivory Rod
Diamond Shield
Barbuta
Crystal Mail
Reflect Ring

Level Jump5, Vertical Jump8
Haste, Slow 2, Stop, Meteor

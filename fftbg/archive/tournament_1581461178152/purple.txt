Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Spidergun0
Female
Leo
48
50
Knight
Dance
PA Save
Dual Wield
Waterwalking

Battle Axe
Broad Sword
Crystal Helmet
White Robe
Red Shoes

Head Break, Mind Break, Surging Sword
Wiznaibus, Slow Dance



Dodgeroll
Male
Gemini
50
75
Knight
Talk Skill
Earplug
Dual Wield
Lava Walking

Ice Brand
Blood Sword
Gold Helmet
Genji Armor
Germinas Boots

Power Break, Stasis Sword, Justice Sword, Surging Sword
Praise, Solution, Insult, Refute



Herecomedeth
Male
Aquarius
78
41
Squire
Draw Out
Speed Save
Secret Hunt
Waterwalking

Slasher

Iron Helmet
Crystal Mail
Bracer

Dash, Throw Stone, Heal, Tickle
Koutetsu, Bizen Boat, Kikuichimoji



Tougou
Male
Taurus
71
58
Knight
Punch Art
Caution
Secret Hunt
Jump+3

Platinum Sword
Diamond Shield
Iron Helmet
Plate Mail
Reflect Ring

Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Stasis Sword
Pummel, Purification, Chakra, Revive, Seal Evil

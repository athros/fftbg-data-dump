Player: !Red
Team: Red Team
Palettes: Red/Brown



DudeMonkey77
Male
Aries
56
51
Knight
Draw Out
Counter Tackle
Equip Sword
Jump+3

Kikuichimoji
Crystal Shield
Mythril Helmet
Platinum Armor
Angel Ring

Armor Break, Shield Break, Power Break, Justice Sword
Asura, Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



KonzeraLive
Female
Aries
78
41
Mediator
Black Magic
HP Restore
Martial Arts
Teleport



Twist Headband
Silk Robe
Rubber Shoes

Invitation, Praise, Solution, Death Sentence, Negotiate, Refute, Rehabilitate
Fire, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 3



Metagameface
Male
Taurus
66
37
Squire
Elemental
Abandon
Equip Polearm
Jump+1

Persia
Buckler
Mythril Helmet
Earth Clothes
Power Wrist

Dash, Cheer Up, Fury, Scream
Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Lava Ball



Chompie
Male
Capricorn
48
65
Samurai
Basic Skill
Catch
Equip Bow
Move-HP Up

Cross Bow

Cross Helmet
Plate Mail
Reflect Ring

Asura, Bizen Boat, Heaven's Cloud
Heal, Yell, Fury, Scream

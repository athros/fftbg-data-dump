Player: !Black
Team: Black Team
Palettes: Black/Red



Maakur
Male
Taurus
77
45
Priest
Talk Skill
Auto Potion
Secret Hunt
Levitate

Flail

Holy Miter
Light Robe
Leather Mantle

Cure, Cure 3, Raise, Shell 2, Wall, Esuna
Persuade, Threaten, Preach, Solution, Mimic Daravon, Rehabilitate



Volgrathemoose
Female
Sagittarius
51
68
Thief
Yin Yang Magic
Damage Split
Long Status
Retreat

Ninja Edge

Holy Miter
Clothes
Dracula Mantle

Gil Taking, Steal Helmet, Steal Status, Leg Aim
Spell Absorb, Pray Faith, Zombie, Blind Rage, Foxbird, Confusion Song



Zagorsek
Female
Aquarius
69
77
Thief
Basic Skill
MP Restore
Short Status
Lava Walking

Long Sword

Green Beret
Power Sleeve
Elf Mantle

Steal Armor, Steal Weapon, Arm Aim, Leg Aim
Accumulate, Dash, Cheer Up, Wish



CaptainAdmiralSPATULA
Male
Capricorn
57
80
Bard
Talk Skill
Parry
Dual Wield
Move-MP Up

Long Bow

Red Hood
Brigandine
Jade Armlet

Cheer Song, Last Song, Sky Demon
Invitation, Death Sentence, Refute

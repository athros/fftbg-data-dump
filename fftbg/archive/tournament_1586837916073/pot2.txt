Final Bets: green - 15 bets for 15,567G (54.1%, x0.85); yellow - 19 bets for 13,202G (45.9%, x1.18)

green bets:
KitchTowel: 10,000G (64.2%, 45,281G)
HaplessOne: 1,500G (9.6%, 59,133G)
datadrivenbot: 857G (5.5%, 13,453G)
Aldrammech: 604G (3.9%, 604G)
ColetteMSLP: 600G (3.9%, 34,640G)
NovaKnight21: 500G (3.2%, 6,103G)
fenaen: 376G (2.4%, 376G)
mirapoix: 329G (2.1%, 658G)
DrAntiSocial: 185G (1.2%, 3,706G)
ungabunga_bot: 116G (0.7%, 118,468G)
ApplesauceBoss: 100G (0.6%, 6,254G)
JoeykinsGaming: 100G (0.6%, 7,540G)
potgodtopdog: 100G (0.6%, 1,000G)
Jerboj: 100G (0.6%, 42,743G)
DHaveWord: 100G (0.6%, 3,360G)

yellow bets:
SeedSC: 3,000G (22.7%, 222,930G)
Arunic: 2,000G (15.1%, 8,673G)
moonliquor: 1,378G (10.4%, 1,378G)
BirbBrainsBot: 1,000G (7.6%, 98,960G)
haplo064: 1,000G (7.6%, 3,823G)
DeathTaxesAndAnime: 890G (6.7%, 890G)
HaateXIII: 728G (5.5%, 728G)
Kronikle: 671G (5.1%, 671G)
reinoe: 500G (3.8%, 20,412G)
NoNotBees: 469G (3.6%, 469G)
getthemoneyz: 336G (2.5%, 481,157G)
JIDkomu: 296G (2.2%, 296G)
Evewho: 200G (1.5%, 3,349G)
RunicMagus: 200G (1.5%, 958G)
LuckyLuckLuc2: 184G (1.4%, 184G)
AllInBot: 100G (0.8%, 100G)
richardserious: 100G (0.8%, 1,219G)
ko2q: 100G (0.8%, 2,481G)
evontno: 50G (0.4%, 1,746G)

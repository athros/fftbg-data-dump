Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



EnemyController
Male
Gemini
61
61
Thief
White Magic
Caution
Secret Hunt
Move-MP Up

Rune Blade

Twist Headband
Rubber Costume
Power Wrist

Gil Taking, Steal Heart, Steal Weapon, Steal Status
Cure, Cure 2, Raise, Protect, Shell 2, Esuna



Soren Of Tyto
Monster
Aquarius
65
56
Red Chocobo










RedRiderAyame
Female
Virgo
67
63
Dancer
Charge
Regenerator
Doublehand
Move+2

Star Bag

Holy Miter
Black Robe
Sprint Shoes

Wiznaibus, Obsidian Blade, Void Storage, Dragon Pit
Charge+1, Charge+5, Charge+7



KasugaiRoastedPeas
Male
Pisces
50
80
Ninja
Punch Art
MA Save
Attack UP
Move+3

Orichalcum
Main Gauche
Green Beret
Adaman Vest
Leather Mantle

Bomb, Axe
Spin Fist, Purification, Revive, Seal Evil

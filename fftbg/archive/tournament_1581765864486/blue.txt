Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Dropdatsupabass
Male
Pisces
72
47
Squire
Yin Yang Magic
Catch
Equip Knife
Move+3

Ninja Edge
Buckler
Circlet
Plate Mail
Cursed Ring

Accumulate, Heal
Blind, Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Foxbird, Confusion Song, Dispel Magic



Coyowijuju
Male
Scorpio
59
49
Samurai
Steal
Catch
Maintenance
Jump+3

Murasame

Platinum Helmet
Maximillian
Dracula Mantle

Koutetsu
Steal Helmet, Steal Shield



RongRongArts
Male
Scorpio
72
77
Chemist
White Magic
Regenerator
Secret Hunt
Move-HP Up

Romanda Gun

Leather Hat
Clothes
Diamond Armlet

Ether, Maiden's Kiss, Soft, Remedy, Phoenix Down
Raise



Mr Tesstickle
Female
Aries
54
67
Calculator
Yin Yang Magic
Distribute
Equip Knife
Move-MP Up

Thunder Rod

Leather Hat
Earth Clothes
Wizard Mantle

CT, Height, Prime Number
Spell Absorb, Silence Song, Confusion Song, Dispel Magic

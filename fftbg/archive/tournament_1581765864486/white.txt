Player: !White
Team: White Team
Palettes: White/Blue



WhiteTigress
Female
Leo
44
78
Calculator
Black Magic
Damage Split
Martial Arts
Waterwalking



Leather Hat
Black Robe
Genji Gauntlet

CT, Prime Number, 5, 4, 3
Fire 2, Fire 3, Bolt 2, Ice, Ice 2, Ice 3, Empower, Frog



IRavenIRush
Male
Virgo
44
43
Lancer
Draw Out
Dragon Spirit
Equip Axe
Lava Walking

Gokuu Rod
Ice Shield
Gold Helmet
Bronze Armor
Genji Gauntlet

Level Jump2, Vertical Jump8
Asura, Koutetsu



Zmoses
Male
Virgo
38
52
Priest
Steal
MA Save
Secret Hunt
Waterbreathing

Healing Staff

Feather Hat
Wizard Robe
Spike Shoes

Raise, Raise 2, Regen, Protect, Protect 2, Wall, Esuna
Steal Helmet, Steal Shield, Steal Weapon, Steal Status



Hyvi
Female
Gemini
77
49
Archer
Basic Skill
Blade Grasp
Dual Wield
Waterwalking

Mythril Gun
Romanda Gun
Black Hood
Power Sleeve
Dracula Mantle

Charge+3, Charge+4, Charge+20
Accumulate, Throw Stone, Tickle

Player: !Red
Team: Red Team
Palettes: Red/Brown



Monopool
Monster
Sagittarius
83
77
Archaic Demon










Ibrakadabra Legend
Male
Scorpio
50
72
Thief
Basic Skill
Counter Flood
Concentrate
Fly

Ninja Edge

Headgear
Leather Outfit
Battle Boots

Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Accumulate, Heal



BigGlorious
Female
Sagittarius
60
45
Archer
Black Magic
Arrow Guard
Equip Polearm
Jump+3

Obelisk
Aegis Shield
Circlet
Mythril Vest
Bracer

Charge+7
Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 2, Ice 4



Cougboi
Female
Sagittarius
56
66
Mime

MA Save
Equip Armor
Jump+3



Gold Helmet
Bronze Armor
Cursed Ring

Mimic


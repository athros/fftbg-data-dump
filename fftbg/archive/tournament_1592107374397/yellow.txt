Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ko2q
Male
Capricorn
40
80
Thief
Throw
Parry
Equip Bow
Move+3

Long Bow

Red Hood
Power Sleeve
Bracer

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Shuriken, Staff, Spear



DamnThatShark
Male
Aries
57
54
Bard
Time Magic
Counter Flood
Doublehand
Teleport

Silver Bow

Twist Headband
Leather Outfit
Defense Armlet

Cheer Song, Battle Song, Nameless Song, Last Song, Space Storage, Hydra Pit
Slow 2, Stop, Reflect, Quick, Demi, Stabilize Time, Galaxy Stop



LDSkinny
Female
Leo
75
60
Oracle
Summon Magic
Speed Save
Maintenance
Move-HP Up

Papyrus Codex

Green Beret
Leather Outfit
Reflect Ring

Life Drain, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Dark Holy
Moogle, Shiva, Leviathan, Fairy



Belkra
Female
Libra
60
80
Time Mage
Elemental
Arrow Guard
Magic Defense UP
Waterbreathing

Ivory Rod

Holy Miter
Robe of Lords
Magic Ring

Slow 2, Stop, Immobilize, Demi, Demi 2, Stabilize Time
Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nizaha
Female
Capricorn
58
51
Mediator
Item
Auto Potion
Dual Wield
Move+2

Papyrus Codex
Papyrus Codex
Black Hood
Black Costume
Bracer

Preach, Insult, Negotiate, Refute
Potion, Hi-Ether, Maiden's Kiss, Phoenix Down



Oogthecaveman
Male
Aquarius
62
61
Bard
Battle Skill
Parry
Concentrate
Teleport

Fairy Harp

Headgear
Judo Outfit
Feather Boots

Angel Song, Cheer Song, Last Song, Diamond Blade, Hydra Pit
Weapon Break, Magic Break, Speed Break



Volgrathemoose
Male
Sagittarius
46
69
Geomancer
White Magic
Counter
Doublehand
Lava Walking

Muramasa

Red Hood
Wizard Robe
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure, Cure 2, Cure 3, Raise, Protect, Shell 2, Esuna, Holy



Galkife
Female
Leo
41
51
Thief
Jump
Counter
Equip Gun
Fly

Stone Gun

Green Beret
Wizard Outfit
Bracer

Steal Helmet, Steal Armor, Steal Status, Arm Aim
Level Jump5, Vertical Jump2

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zetchryn
Female
Pisces
68
41
Mediator
Battle Skill
Sunken State
Equip Armor
Move-MP Up

Blaze Gun

Golden Hairpin
Diamond Armor
Defense Ring

Invitation, Persuade, Preach, Death Sentence
Head Break, Armor Break, Magic Break



Masta Glenn
Male
Aquarius
47
55
Monk
Jump
HP Restore
Attack UP
Move+3



Red Hood
Earth Clothes
Feather Mantle

Earth Slash, Secret Fist, Purification, Revive
Level Jump5, Vertical Jump6



The Pengwin
Female
Virgo
71
41
Calculator
Mighty Sword
Meatbone Slash
Secret Hunt
Jump+3

Battle Folio

Holy Miter
Chain Vest
Defense Armlet

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



LOKITHUS
Female
Aries
80
57
Time Mage
Elemental
Critical Quick
Sicken
Swim

Rainbow Staff

Thief Hat
Rubber Costume
Vanish Mantle

Haste, Slow 2, Immobilize, Demi, Stabilize Time, Meteor
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball

Final Bets: white - 11 bets for 6,577G (59.0%, x0.69); black - 8 bets for 4,564G (41.0%, x1.44)

white bets:
BirbBrainsBot: 1,000G (15.2%, 122,021G)
getthemoneyz: 1,000G (15.2%, 1,462,242G)
silentkaster: 1,000G (15.2%, 6,363G)
Thyrandaal: 1,000G (15.2%, 146,643G)
E_Ballard: 903G (13.7%, 903G)
coralreeferz: 528G (8.0%, 4,152G)
roofiepops: 474G (7.2%, 17,413G)
MMagnetMan: 232G (3.5%, 232G)
KidaCray: 200G (3.0%, 420G)
datadrivenbot: 200G (3.0%, 45,836G)
ArlanKels: 40G (0.6%, 1,549G)

black bets:
reinoe: 1,000G (21.9%, 70,103G)
ColetteMSLP: 1,000G (21.9%, 24,299G)
Laserman1000: 757G (16.6%, 757G)
KasugaiRoastedPeas: 652G (14.3%, 652G)
Lanshaft: 544G (11.9%, 22,540G)
resjudicata3: 252G (5.5%, 252G)
AllInBot: 236G (5.2%, 236G)
Lydian_C: 123G (2.7%, 3,852G)

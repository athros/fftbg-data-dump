Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Volgrathemoose
Female
Taurus
58
72
Ninja
Talk Skill
Speed Save
Equip Gun
Ignore Terrain

Blast Gun
Blast Gun
Flash Hat
Chain Vest
108 Gems

Shuriken, Bomb, Staff, Stick
Persuade, Praise, Insult, Negotiate, Mimic Daravon, Refute



Lordminsc
Male
Sagittarius
60
63
Summoner
Elemental
Catch
Equip Shield
Levitate

Wizard Rod
Diamond Shield
Feather Hat
Adaman Vest
Feather Mantle

Moogle, Odin, Leviathan, Fairy
Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Reinoe
Male
Aquarius
52
57
Squire
Time Magic
PA Save
Sicken
Jump+2

Cross Bow
Platinum Shield
Golden Hairpin
Mythril Vest
Feather Boots

Dash, Throw Stone, Heal, Tickle, Yell, Cheer Up
Haste, Slow 2, Reflect, Quick, Demi, Stabilize Time



SeraphPDH
Female
Cancer
70
43
Summoner
Draw Out
Meatbone Slash
Equip Polearm
Waterbreathing

Cypress Rod

Flash Hat
Adaman Vest
Dracula Mantle

Moogle, Ramuh, Golem, Carbunkle, Salamander, Lich, Cyclops
Koutetsu, Muramasa, Kikuichimoji

Player: !White
Team: White Team
Palettes: White/Blue



Chuckolator
Female
Scorpio
44
68
Knight
Summon Magic
Abandon
Concentrate
Jump+3

Save the Queen
Genji Shield
Platinum Helmet
Diamond Armor
Defense Armlet

Magic Break, Justice Sword, Surging Sword
Ramuh, Titan, Leviathan, Fairy



Blorpy
Male
Aquarius
57
38
Samurai
Talk Skill
Meatbone Slash
Equip Shield
Move-HP Up

Obelisk
Ice Shield
Cross Helmet
Linen Robe
Red Shoes

Murasame, Kiyomori
Invitation, Persuade, Praise, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute



Fattunaking
Male
Aries
42
73
Monk
Elemental
MP Restore
Dual Wield
Move+2



Holy Miter
Wizard Outfit
Red Shoes

Pummel, Purification, Chakra, Revive
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Superdevon1
Male
Pisces
80
56
Thief
Basic Skill
PA Save
Equip Shield
Ignore Height

Kunai
Genji Shield
Ribbon
Clothes
Bracer

Steal Heart, Steal Helmet, Arm Aim
Accumulate, Dash, Heal, Tickle

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



DHaveWord
Female
Leo
77
64
Squire
Jump
Meatbone Slash
Magic Attack UP
Ignore Terrain

Hunting Bow
Hero Shield
Black Hood
Chain Vest
Small Mantle

Accumulate, Yell, Cheer Up, Wish
Level Jump8, Vertical Jump3



Nifboy
Male
Libra
43
60
Chemist
Time Magic
Counter Magic
Equip Knife
Retreat

Zorlin Shape

Headgear
Chain Vest
Wizard Mantle

X-Potion, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Haste 2, Slow, Stop, Reflect, Quick, Demi 2, Meteor



Mirapoix
Male
Cancer
80
49
Mime

Blade Grasp
Sicken
Ignore Terrain



Cachusha
Judo Outfit
Elf Mantle

Mimic




HaychDub
Male
Pisces
80
44
Geomancer
Charge
Auto Potion
Short Status
Jump+3

Kiyomori
Gold Shield
Holy Miter
Earth Clothes
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Charge+7

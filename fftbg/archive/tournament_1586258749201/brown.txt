Player: !Brown
Team: Brown Team
Palettes: Brown/Green



TheKillerNacho
Male
Capricorn
43
67
Chemist
Sing
Counter
Secret Hunt
Ignore Terrain

Blaze Gun

Leather Hat
Wizard Outfit
Angel Ring

Potion, X-Potion, Ether, Soft, Phoenix Down
Magic Song, Nameless Song, Last Song, Diamond Blade, Sky Demon



Masta Glenn
Female
Libra
46
75
Samurai
Time Magic
Hamedo
Maintenance
Ignore Terrain

Javelin

Mythril Helmet
Bronze Armor
Jade Armlet

Kiyomori, Muramasa, Kikuichimoji
Haste, Quick, Demi, Stabilize Time, Meteor



Upvla
Male
Libra
41
77
Ninja
Punch Art
Parry
Martial Arts
Ignore Height



Triangle Hat
Judo Outfit
Angel Ring

Bomb, Knife
Secret Fist, Chakra, Revive, Seal Evil



Powergems
Female
Sagittarius
80
46
Samurai
Talk Skill
HP Restore
Equip Polearm
Move+2

Holy Lance

Barbuta
Wizard Robe
Diamond Armlet

Asura, Koutetsu, Bizen Boat, Muramasa
Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate

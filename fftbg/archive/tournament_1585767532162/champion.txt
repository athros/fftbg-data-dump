Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Lythe Caraker
Female
Gemini
70
61
Time Mage
Summon Magic
MA Save
Equip Bow
Retreat

Bow Gun

Holy Miter
Mystic Vest
Magic Ring

Immobilize, Quick, Stabilize Time
Shiva, Ramuh, Titan, Golem, Silf, Fairy



OttoRaynar
Male
Taurus
62
73
Archer
Basic Skill
Catch
Defend
Levitate

Poison Bow
Round Shield
Feather Hat
Power Sleeve
Magic Ring

Charge+2, Charge+3
Dash, Throw Stone, Heal, Tickle, Yell, Fury, Wish



Shalloween
Monster
Cancer
39
47
Byblos










Lowlf
Female
Taurus
60
76
Time Mage
Elemental
Abandon
Beastmaster
Retreat

White Staff

Twist Headband
Mystic Vest
Jade Armlet

Haste, Slow 2, Reflect, Demi 2, Stabilize Time
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Blizzard, Lava Ball

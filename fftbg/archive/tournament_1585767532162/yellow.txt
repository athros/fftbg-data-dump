Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lodrak
Male
Virgo
82
77
Chemist
Summon Magic
Counter
Halve MP
Move-HP Up

Panther Bag

Leather Hat
Clothes
Feather Boots

Hi-Potion, X-Potion, Antidote, Holy Water, Phoenix Down
Moogle, Ramuh, Ifrit, Bahamut, Odin, Leviathan, Salamander



Lowlf
Monster
Aries
77
71
Coeurl










Luminarii
Male
Aries
74
55
Samurai
Elemental
Counter
Equip Knife
Teleport

Sasuke Knife

Iron Helmet
White Robe
Jade Armlet

Koutetsu, Bizen Boat
Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Zachara
Female
Scorpio
57
76
Thief
Punch Art
Counter Flood
Equip Armor
Move+3

Coral Sword

Red Hood
Chameleon Robe
Cherche

Steal Heart, Steal Armor, Steal Shield, Arm Aim
Revive

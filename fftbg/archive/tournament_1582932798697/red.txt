Player: !Red
Team: Red Team
Palettes: Red/Brown



Clippopo
Male
Taurus
77
60
Monk
Black Magic
Parry
Halve MP
Fly



Holy Miter
Chain Vest
Cherche

Spin Fist, Earth Slash, Purification, Chakra, Seal Evil
Fire, Fire 2, Fire 3, Fire 4, Ice, Ice 2, Ice 3, Empower, Frog



Cryptopsy70
Female
Scorpio
70
68
Thief
Draw Out
Arrow Guard
Equip Gun
Waterwalking

Romanda Gun

Flash Hat
Adaman Vest
Reflect Ring

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Status, Leg Aim
Koutetsu, Muramasa



Sprawl5
Female
Aquarius
48
58
Squire
Punch Art
Absorb Used MP
Magic Attack UP
Levitate

Mythril Sword
Bronze Shield
Flash Hat
Black Costume
Dracula Mantle

Tickle
Spin Fist, Purification



Ninjapenguim
Female
Cancer
67
67
Knight
Black Magic
Counter Magic
Beastmaster
Ignore Height

Ice Brand
Gold Shield
Genji Helmet
Reflect Mail
Dracula Mantle

Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword
Fire, Fire 2, Ice, Ice 4, Death

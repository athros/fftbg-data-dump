Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Treapvort
Male
Taurus
48
43
Squire
Item
Arrow Guard
Throw Item
Move+2

Mythril Sword
Mythril Shield
Cross Helmet
Leather Armor
Cherche

Dash, Throw Stone, Ultima
Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down



Thejoden
Female
Aquarius
68
62
Summoner
Dance
Counter Magic
Equip Polearm
Swim

Partisan

Triangle Hat
Mythril Vest
Salty Rage

Ramuh, Titan, Golem, Carbunkle, Odin, Salamander, Silf
Witch Hunt, Slow Dance, Polka Polka, Nameless Dance



BenYuPoker
Male
Gemini
69
54
Bard
Throw
Auto Potion
Dual Wield
Ignore Terrain

Ramia Harp
Fairy Harp
Feather Hat
Wizard Outfit
Genji Gauntlet

Angel Song, Cheer Song, Last Song, Diamond Blade
Knife, Hammer, Dictionary



Red Celt
Male
Libra
80
46
Mediator
Draw Out
Speed Save
Doublehand
Swim

Stone Gun

Leather Hat
Chameleon Robe
Sprint Shoes

Invitation, Persuade, Mimic Daravon, Refute, Rehabilitate
Koutetsu

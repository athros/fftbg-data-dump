Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RunicMagus
Male
Leo
45
40
Wizard
Math Skill
Regenerator
Maintenance
Levitate

Dagger

Red Hood
Clothes
Jade Armlet

Fire 4, Ice
CT, Prime Number, 5, 4, 3



Chuckolator
Male
Leo
76
56
Mediator
Steal
Faith Save
Halve MP
Retreat

Panther Bag

Black Hood
Light Robe
Spike Shoes

Persuade, Preach, Solution, Death Sentence, Insult, Negotiate, Refute
Gil Taking, Steal Status, Arm Aim



Lord Gwarth
Male
Cancer
54
51
Archer
Steal
Sunken State
Doublehand
Ignore Height

Glacier Gun

Leather Hat
Chain Vest
Magic Ring

Charge+3, Charge+4, Charge+5, Charge+7
Steal Helmet, Steal Weapon



Maakur
Male
Libra
73
80
Mediator
Throw
Counter Flood
Defense UP
Jump+3

Bestiary

Headgear
Wizard Robe
Magic Ring

Invitation, Threaten, Solution, Negotiate, Refute
Shuriken

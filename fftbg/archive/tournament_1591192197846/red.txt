Player: !Red
Team: Red Team
Palettes: Red/Brown



Laserman1000
Female
Virgo
57
50
Wizard
Time Magic
Faith Save
Concentrate
Move-MP Up

Wizard Rod

Green Beret
Wizard Robe
Magic Gauntlet

Fire 2, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Ice 4, Death
Haste, Slow, Immobilize, Float, Reflect, Demi, Stabilize Time



Outer Monologue
Male
Libra
71
76
Oracle
White Magic
Regenerator
Magic Attack UP
Ignore Terrain

Gokuu Rod

Green Beret
Earth Clothes
Jade Armlet

Blind, Spell Absorb, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Cure, Reraise, Protect 2, Shell 2, Wall, Esuna, Holy



Letdowncity
Male
Leo
45
44
Archer
Black Magic
Counter
Defense UP
Jump+1

Mythril Bow

Green Beret
Wizard Outfit
Bracer

Charge+1, Charge+2
Fire 3, Bolt, Ice 4, Flare



Zagorsek
Female
Taurus
51
54
Priest
Steal
Blade Grasp
Beastmaster
Move+2

Wizard Staff

Triangle Hat
Mystic Vest
Red Shoes

Cure 3, Cure 4, Raise, Reraise, Protect, Shell 2, Wall, Esuna
Gil Taking, Steal Heart, Steal Shield, Steal Accessory, Leg Aim

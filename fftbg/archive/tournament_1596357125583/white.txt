Player: !White
Team: White Team
Palettes: White/Blue



DeathTaxesAndAnime
Female
Scorpio
58
62
Mime

Counter Magic
Equip Armor
Ignore Height



Feather Hat
Linen Robe
Small Mantle

Mimic




Randgridr
Female
Libra
74
50
Chemist
Punch Art
MA Save
Equip Bow
Move-MP Up

Poison Bow

Black Hood
Mythril Vest
Genji Gauntlet

Potion, Ether, Hi-Ether, Holy Water, Remedy, Phoenix Down
Pummel, Earth Slash, Chakra



Nifboy
Male
Pisces
71
59
Chemist
Throw
PA Save
Magic Attack UP
Jump+1

Mythril Gun

Black Hood
Power Sleeve
Defense Ring

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Phoenix Down
Shuriken, Knife, Wand



ALY327
Female
Libra
56
77
Chemist
Time Magic
Regenerator
Doublehand
Fly

Hydra Bag

Headgear
Clothes
Battle Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Phoenix Down
Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Quick, Stabilize Time

Final Bets: red - 10 bets for 9,794G (80.4%, x0.24); green - 4 bets for 2,390G (19.6%, x4.10)

red bets:
Draconis345: 4,467G (45.6%, 4,467G)
NicoSavoy: 2,263G (23.1%, 113,157G)
randgridr: 864G (8.8%, 1,728G)
BirbBrainsBot: 529G (5.4%, 127,040G)
josephiroth_143: 500G (5.1%, 5,464G)
AllInBot: 316G (3.2%, 316G)
theyoungkroh: 300G (3.1%, 1,004G)
byrdturbo: 255G (2.6%, 23,730G)
datadrivenbot: 200G (2.0%, 49,966G)
TheseJeans: 100G (1.0%, 984G)

green bets:
douchetron: 1,286G (53.8%, 2,522G)
maximumcrit: 500G (20.9%, 11,340G)
getthemoneyz: 310G (13.0%, 1,468,402G)
Zachara: 294G (12.3%, 128,794G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Drusiform
Female
Capricorn
46
59
Summoner
White Magic
MA Save
Sicken
Move+2

Poison Rod

Twist Headband
Mystic Vest
Small Mantle

Moogle, Shiva, Titan, Golem, Silf, Zodiac
Cure 2, Cure 4, Raise, Reraise, Protect 2, Wall, Esuna



E Ballard
Male
Scorpio
52
53
Samurai
Charge
Counter Magic
Long Status
Jump+2

Holy Lance

Cross Helmet
Bronze Armor
Feather Mantle

Koutetsu, Bizen Boat, Heaven's Cloud
Charge+2, Charge+3



J2DaBibbles
Male
Aquarius
70
80
Archer
Basic Skill
Catch
Dual Wield
Ignore Terrain

Romanda Gun
Blaze Gun
Black Hood
Leather Outfit
Dracula Mantle

Charge+1, Charge+3, Charge+5, Charge+7, Charge+10
Dash, Throw Stone, Heal, Fury, Scream



Gorgewall
Male
Aquarius
46
70
Wizard
Talk Skill
Meatbone Slash
Monster Talk
Ignore Terrain

Rod

Thief Hat
Chameleon Robe
Sprint Shoes

Fire 3, Bolt 4, Ice 2, Ice 3, Ice 4
Praise, Preach, Death Sentence, Insult, Negotiate, Refute

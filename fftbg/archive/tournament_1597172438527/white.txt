Player: !White
Team: White Team
Palettes: White/Blue



Lillifen
Male
Aquarius
58
71
Wizard
White Magic
Caution
Halve MP
Move-MP Up

Dragon Rod

Green Beret
White Robe
Defense Armlet

Bolt, Bolt 4, Ice, Ice 2, Ice 3, Empower
Cure, Cure 4, Raise, Raise 2, Esuna



Kellios11
Male
Gemini
61
54
Knight
Summon Magic
Distribute
Equip Bow
Move-MP Up

Mythril Bow
Diamond Shield
Leather Helmet
Carabini Mail
Magic Gauntlet

Head Break, Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Stasis Sword, Night Sword
Moogle, Shiva, Ramuh, Carbunkle, Bahamut



Forkmore
Male
Sagittarius
45
75
Knight
Draw Out
Catch
Defend
Jump+3

Chaos Blade
Escutcheon
Gold Helmet
Linen Cuirass
Power Wrist

Armor Break, Power Break, Mind Break
Koutetsu, Heaven's Cloud, Muramasa, Kikuichimoji



Galkife
Female
Scorpio
69
65
Calculator
Time Magic
Abandon
Defend
Ignore Terrain

Bestiary

Headgear
Linen Robe
Germinas Boots

CT, Height, Prime Number, 5, 3
Immobilize, Float, Reflect

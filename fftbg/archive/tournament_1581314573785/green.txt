Player: !Green
Team: Green Team
Palettes: Green/White



Flowinprose
Male
Capricorn
54
76
Archer
Draw Out
Critical Quick
Equip Gun
Teleport

Romanda Gun
Platinum Shield
Green Beret
Black Costume
Sprint Shoes

Charge+1, Charge+2, Charge+3, Charge+4, Charge+10
Koutetsu



WireLord
Male
Gemini
64
53
Mediator
Sing
Counter
Magic Defense UP
Lava Walking

Romanda Gun

Holy Miter
Linen Robe
Wizard Mantle

Invitation, Praise, Death Sentence, Insult, Mimic Daravon, Rehabilitate
Life Song, Magic Song, Last Song, Space Storage



Galkife
Male
Taurus
48
36
Mime

Counter Magic
Secret Hunt
Move-HP Up



Leather Hat
Black Costume
Magic Gauntlet

Mimic




Reamplify
Male
Aries
66
45
Knight
Sing
Brave Up
Defend
Jump+1

Platinum Sword
Mythril Shield
Barbuta
Crystal Mail
Elf Mantle

Head Break, Weapon Break, Speed Break, Power Break, Mind Break
Angel Song, Cheer Song, Battle Song, Nameless Song, Diamond Blade

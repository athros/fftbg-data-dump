Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



MgKamui
Male
Cancer
68
69
Ninja
Battle Skill
Counter
Long Status
Waterwalking

Mage Masher
Short Edge
Thief Hat
Judo Outfit
Leather Mantle

Shuriken
Shield Break, Magic Break



Evontno
Monster
Scorpio
78
67
Squidraken










TheMurkGnome
Female
Aquarius
47
63
Wizard
Draw Out
MA Save
Long Status
Lava Walking

Flame Rod

Thief Hat
Brigandine
N-Kai Armlet

Fire 2, Bolt 3, Ice 2, Ice 3, Flare
Koutetsu, Bizen Boat, Murasame



Sypheck
Male
Gemini
60
72
Chemist
Talk Skill
Absorb Used MP
Equip Shield
Jump+1

Romanda Gun
Genji Shield
Golden Hairpin
Secret Clothes
Genji Gauntlet

Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Invitation, Persuade, Preach, Negotiate, Mimic Daravon, Refute

Player: !Green
Team: Green Team
Palettes: Green/White



BigDLicious91
Male
Taurus
48
52
Thief
Elemental
Critical Quick
Defend
Move+2

Dagger

Cross Helmet
Leather Outfit
Spike Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status
Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard



Victoriolue
Male
Gemini
69
46
Ninja
Punch Art
Meatbone Slash
Equip Gun
Swim

Bestiary
Fairy Harp
Black Hood
Judo Outfit
Red Shoes

Shuriken, Knife
Earth Slash, Secret Fist, Purification, Chakra



ColorblindGamer
Female
Scorpio
78
47
Dancer
Jump
MP Restore
Equip Axe
Ignore Height

Flame Whip

Triangle Hat
Black Costume
Jade Armlet

Wiznaibus, Polka Polka, Disillusion, Nameless Dance, Obsidian Blade, Nether Demon
Level Jump2, Vertical Jump8



MattmanX311
Male
Aries
77
47
Monk
Charge
Damage Split
Dual Wield
Jump+2



Flash Hat
Secret Clothes
108 Gems

Pummel, Purification, Chakra, Revive
Charge+1, Charge+4, Charge+10

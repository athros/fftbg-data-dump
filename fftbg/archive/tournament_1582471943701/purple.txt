Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Treapvort
Monster
Cancer
40
56
Dragon










Agemo027
Female
Cancer
60
41
Thief
Battle Skill
Counter
Dual Wield
Waterbreathing

Zorlin Shape
Hidden Knife
Barbuta
Secret Clothes
Dracula Mantle

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim
Head Break, Armor Break, Shield Break, Weapon Break, Speed Break



Coledot
Male
Sagittarius
57
45
Knight
Charge
Damage Split
Equip Armor
Jump+2

Ancient Sword
Flame Shield
Red Hood
Adaman Vest
Bracer

Armor Break, Magic Break, Stasis Sword, Night Sword
Charge+1, Charge+2, Charge+4, Charge+5, Charge+10, Charge+20



Rolodex
Female
Scorpio
54
75
Monk
Charge
Speed Save
Equip Shield
Move-MP Up


Crystal Shield
Leather Hat
Mythril Vest
Rubber Shoes

Spin Fist, Earth Slash, Purification, Chakra, Revive
Charge+1, Charge+7

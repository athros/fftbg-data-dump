Player: !Black
Team: Black Team
Palettes: Black/Red



Rnark
Male
Taurus
48
63
Bard
Elemental
Abandon
Magic Attack UP
Teleport

Bloody Strings

Twist Headband
Judo Outfit
Red Shoes

Life Song, Cheer Song, Nameless Song, Diamond Blade
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Holyonline
Monster
Cancer
75
47
Floating Eye










Tiroatrain
Male
Aquarius
45
57
Chemist
Basic Skill
Brave Up
Martial Arts
Jump+3



Feather Hat
Wizard Outfit
Feather Mantle

Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Heal, Tickle, Wish



Snkey
Male
Virgo
54
45
Monk
Item
Regenerator
Throw Item
Swim



Twist Headband
Adaman Vest
Germinas Boots

Earth Slash, Purification, Revive, Seal Evil
Hi-Potion, Elixir, Eye Drop, Echo Grass, Phoenix Down

Player: !Black
Team: Black Team
Palettes: Black/Red



StealthModeLocke
Male
Pisces
67
56
Mediator
Battle Skill
Absorb Used MP
Defend
Move+1

Blaze Gun

Golden Hairpin
Chameleon Robe
Power Wrist

Persuade, Praise, Preach, Insult, Refute, Rehabilitate
Shield Break, Weapon Break, Mind Break, Stasis Sword



Lastly
Female
Cancer
74
79
Wizard
Jump
MA Save
Short Status
Ignore Terrain

Dagger

Feather Hat
Black Robe
Cherche

Fire 2, Bolt, Bolt 3, Bolt 4, Ice 4, Frog, Flare
Level Jump5, Vertical Jump8



Baconbacon1207
Male
Libra
53
76
Knight
Talk Skill
MA Save
Maintenance
Lava Walking

Nagrarock
Aegis Shield
Bronze Helmet
Black Robe
Defense Ring

Shield Break, Weapon Break, Magic Break
Praise, Insult, Negotiate, Refute, Rehabilitate



Fenaen
Female
Aries
52
75
Time Mage
Elemental
Caution
Equip Bow
Lava Walking

Night Killer

Red Hood
Brigandine
Cursed Ring

Haste, Haste 2, Stop, Float, Stabilize Time
Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

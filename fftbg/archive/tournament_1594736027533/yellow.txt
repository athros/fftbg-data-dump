Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Run With Stone GUNs
Male
Virgo
41
56
Mediator
Throw
Absorb Used MP
Dual Wield
Move-HP Up

Mythril Gun
Glacier Gun
Headgear
Clothes
Battle Boots

Praise, Death Sentence, Negotiate, Mimic Daravon, Refute, Rehabilitate
Shuriken, Sword



Pplvee1
Male
Cancer
59
54
Ninja
Elemental
Auto Potion
Equip Bow
Jump+1

Bow Gun
Snipe Bow
Triangle Hat
Black Costume
Elf Mantle

Shuriken, Ninja Sword
Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



LDSkinny
Female
Taurus
77
63
Oracle
Draw Out
Counter Flood
Dual Wield
Teleport

Cypress Rod
Gokuu Rod
Twist Headband
Light Robe
Cursed Ring

Blind, Poison, Foxbird, Dispel Magic
Asura, Kikuichimoji



Miku Shikhu
Male
Aries
62
71
Time Mage
Draw Out
Earplug
Maintenance
Move-HP Up

Cypress Rod

Leather Hat
Silk Robe
Battle Boots

Slow 2, Stop, Immobilize, Stabilize Time, Meteor
Muramasa, Kikuichimoji

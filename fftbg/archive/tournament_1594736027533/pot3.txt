Final Bets: white - 7 bets for 8,135G (70.5%, x0.42); black - 10 bets for 3,403G (29.5%, x2.39)

white bets:
toka222: 5,000G (61.5%, 91,854G)
dogsandcatsand: 1,305G (16.0%, 1,305G)
rechaun: 532G (6.5%, 532G)
mirapoix: 500G (6.1%, 3,099G)
Aldrammech: 448G (5.5%, 448G)
datadrivenbot: 200G (2.5%, 58,295G)
RedRiderAyame: 150G (1.8%, 566G)

black bets:
BirbBrainsBot: 1,000G (29.4%, 149,938G)
Error72: 584G (17.2%, 584G)
fenaen: 535G (15.7%, 535G)
ArashiKurobara: 324G (9.5%, 3,970G)
dantayystv: 268G (7.9%, 268G)
getthemoneyz: 242G (7.1%, 1,271,124G)
Mysteriousdewd: 200G (5.9%, 5,329G)
CosmicTactician: 100G (2.9%, 38,487G)
SephDarkheart: 100G (2.9%, 20,789G)
captainmilestw: 50G (1.5%, 714G)

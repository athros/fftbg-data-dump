Player: !Red
Team: Red Team
Palettes: Red/Brown



Fenixcrest
Male
Scorpio
47
44
Calculator
Yin Yang Magic
Absorb Used MP
Defense UP
Teleport

Battle Folio

Holy Miter
Brigandine
Wizard Mantle

CT, Prime Number, 4, 3
Life Drain, Zombie, Sleep, Dark Holy



Oogthecaveman
Male
Gemini
70
72
Summoner
Talk Skill
Caution
Equip Knife
Teleport

Mage Masher

Green Beret
Wizard Robe
Small Mantle

Shiva, Odin, Salamander
Threaten, Solution, Death Sentence



Kai Shee
Male
Aries
77
79
Geomancer
Draw Out
Critical Quick
Equip Armor
Move-MP Up

Battle Axe
Escutcheon
Platinum Helmet
Crystal Mail
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Murasame, Kiyomori



Wackyzachyy
Female
Aries
65
66
Priest
Charge
Counter Tackle
Defense UP
Move+1

Wizard Staff

Feather Hat
Wizard Robe
Reflect Ring

Cure 3, Cure 4, Raise, Regen, Protect 2, Holy
Charge+1, Charge+2, Charge+3

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



YaBoy125
Male
Sagittarius
58
55
Knight
Charge
Counter Magic
Beastmaster
Jump+2

Ragnarok
Flame Shield
Genji Helmet
Carabini Mail
Rubber Shoes

Head Break, Armor Break, Magic Break, Mind Break, Stasis Sword
Charge+3, Charge+4, Charge+7, Charge+20



Firesheath
Female
Gemini
46
81
Time Mage
Dance
Damage Split
Doublehand
Ignore Terrain

Octagon Rod

Triangle Hat
White Robe
Power Wrist

Haste, Slow 2, Meteor
Witch Hunt, Polka Polka, Nether Demon



Galkife
Male
Taurus
74
63
Mediator
Sing
Regenerator
Magic Attack UP
Swim

Mythril Gun

Black Hood
Adaman Vest
Spike Shoes

Praise, Solution, Death Sentence, Insult, Refute, Rehabilitate
Cheer Song, Battle Song, Hydra Pit



HASTERIOUS
Male
Capricorn
74
49
Lancer
Charge
Critical Quick
Martial Arts
Teleport


Crystal Shield
Genji Helmet
Crystal Mail
Dracula Mantle

Level Jump5, Vertical Jump6
Charge+2, Charge+7

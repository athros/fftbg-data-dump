Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NicoSavoy
Male
Aries
71
79
Lancer
Basic Skill
Counter
Short Charge
Jump+1

Mythril Spear
Aegis Shield
Genji Helmet
Chain Mail
Germinas Boots

Level Jump4, Vertical Jump8
Heal, Tickle, Ultima



Lowlf
Male
Sagittarius
60
60
Lancer
Item
Auto Potion
Magic Attack UP
Jump+3

Holy Lance
Genji Shield
Cross Helmet
Gold Armor
Spike Shoes

Level Jump2, Vertical Jump4
Potion, Holy Water, Remedy, Phoenix Down



SSwing
Male
Cancer
41
69
Ninja
Steal
Sunken State
Equip Gun
Swim

Bestiary
Fairy Harp
Flash Hat
Judo Outfit
Defense Ring

Bomb, Knife
Gil Taking, Steal Status



Zachara
Female
Sagittarius
43
63
Geomancer
Charge
Dragon Spirit
Defense UP
Move+1

Coral Sword
Platinum Shield
Flash Hat
Chameleon Robe
Magic Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Charge+4, Charge+5, Charge+20

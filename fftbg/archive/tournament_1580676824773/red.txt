Player: !Red
Team: Red Team
Palettes: Red/Brown



TheUnforgivenRage
Female
Virgo
61
49
Ninja
Item
Speed Save
Throw Item
Jump+1

Short Edge
Sasuke Knife
Ribbon
Brigandine
Wizard Mantle

Staff
Hi-Potion, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down



JustDoomathon
Male
Scorpio
62
54
Knight
Elemental
Counter
Defense UP
Lava Walking

Long Sword
Ice Shield
Iron Helmet
Linen Cuirass
Small Mantle

Head Break, Shield Break, Weapon Break, Power Break
Hell Ivy, Hallowed Ground, Will-O-Wisp, Gusty Wind



Nazgorath
Male
Libra
72
42
Chemist
Draw Out
Dragon Spirit
Attack UP
Retreat

Romanda Gun

Feather Hat
Wizard Outfit
Leather Mantle

Potion, X-Potion, Eye Drop, Soft, Phoenix Down
Asura, Murasame



Ironchefj
Male
Virgo
73
41
Oracle
Battle Skill
Counter Tackle
Equip Sword
Teleport

Ivory Rod

Thief Hat
White Robe
Magic Ring

Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Foxbird, Dispel Magic, Petrify
Head Break, Armor Break, Dark Sword

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Oobs56
Male
Capricorn
51
47
Thief
Summon Magic
Distribute
Equip Armor
Move+1

Sasuke Knife

Circlet
Black Robe
Elf Mantle

Gil Taking, Steal Armor, Steal Status, Arm Aim
Golem, Carbunkle, Leviathan, Fairy, Cyclops



TheChainNerd
Male
Aquarius
47
46
Wizard
Item
Absorb Used MP
Short Charge
Fly

Orichalcum

Feather Hat
Rubber Costume
Elf Mantle

Bolt 2, Ice, Ice 3, Ice 4, Flare
Potion, X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down



CrownOfHorns
Female
Gemini
49
38
Knight
Basic Skill
PA Save
Defense UP
Move+3

Slasher
Gold Shield
Cross Helmet
White Robe
Defense Armlet

Head Break, Shield Break, Speed Break, Stasis Sword
Accumulate, Heal, Tickle, Cheer Up, Fury, Wish



JumbocactuarX27
Female
Taurus
73
39
Geomancer
Black Magic
Caution
Equip Armor
Move-HP Up

Bizen Boat
Aegis Shield
Genji Helmet
Mythril Armor
Feather Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Bolt 2, Bolt 3, Ice 4, Flare

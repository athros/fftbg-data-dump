Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Alacor
Male
Virgo
52
55
Knight
Elemental
Dragon Spirit
Short Charge
Move+1

Broad Sword
Gold Shield
Barbuta
Gold Armor
Diamond Armlet

Head Break, Magic Break, Mind Break, Dark Sword, Night Sword
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Gusty Wind



ZephyrTempest
Male
Leo
73
44
Knight
Item
Parry
Throw Item
Move-MP Up

Battle Axe
Genji Shield
Leather Helmet
Carabini Mail
Dracula Mantle

Head Break, Armor Break, Magic Break, Justice Sword, Dark Sword, Night Sword
Ether, Hi-Ether, Antidote, Soft, Holy Water, Remedy, Phoenix Down



Shakarak
Female
Leo
75
69
Calculator
Yin Yang Magic
Distribute
Dual Wield
Swim

Madlemgen
Papyrus Codex
Flash Hat
Linen Robe
Power Wrist

CT, Height, Prime Number
Poison, Spell Absorb, Silence Song, Blind Rage, Sleep, Petrify



Shalloween
Female
Taurus
76
45
Calculator
Black Magic
Counter Flood
Defense UP
Fly

Gokuu Rod

Leather Hat
Mythril Vest
Germinas Boots

CT, Prime Number, 5, 3
Fire 4, Bolt, Ice 2, Ice 3

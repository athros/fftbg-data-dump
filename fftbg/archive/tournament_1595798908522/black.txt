Player: !Black
Team: Black Team
Palettes: Black/Red



E Ballard
Male
Libra
47
52
Bard
Talk Skill
MP Restore
Monster Talk
Jump+2

Bloody Strings

Green Beret
Secret Clothes
Battle Boots

Hydra Pit
Persuade, Threaten, Death Sentence, Negotiate



Victoriolue
Male
Pisces
74
76
Mime

Arrow Guard
Dual Wield
Jump+3



Flash Hat
Secret Clothes
Cursed Ring

Mimic




Reddwind
Male
Scorpio
58
47
Knight
Item
Abandon
Doublehand
Jump+2

Materia Blade

Barbuta
Crystal Mail
Cursed Ring

Mind Break, Stasis Sword
Potion, X-Potion, Soft, Phoenix Down



Just Here2
Male
Pisces
63
73
Chemist
Sing
Damage Split
Martial Arts
Move-MP Up

Cultist Dagger

Red Hood
Earth Clothes
Small Mantle

Potion, Hi-Potion, Ether, Antidote, Maiden's Kiss
Life Song, Nameless Song, Hydra Pit

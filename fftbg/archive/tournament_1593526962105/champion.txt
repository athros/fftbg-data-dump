Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



ArashiKurobara
Male
Cancer
56
54
Thief
Throw
Mana Shield
Secret Hunt
Swim

Mythril Knife

Leather Hat
Judo Outfit
Defense Armlet

Steal Helmet, Steal Shield
Shuriken



VolgraTheMoose
Female
Cancer
58
70
Priest
Elemental
MA Save
Doublehand
Move+2

Rainbow Staff

Red Hood
Leather Outfit
Chantage

Cure, Cure 2, Raise, Reraise, Regen, Protect, Shell, Esuna
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



SomthingMore
Male
Taurus
68
68
Priest
Basic Skill
Meatbone Slash
Equip Bow
Waterbreathing

Poison Bow

Golden Hairpin
Chameleon Robe
Bracer

Cure, Cure 3, Raise, Reraise, Regen, Protect, Wall, Esuna
Accumulate, Heal, Yell, Scream



TheChainNerd
Male
Pisces
63
41
Samurai
Jump
Parry
Attack UP
Levitate

Holy Lance

Iron Helmet
Crystal Mail
Defense Ring

Murasame, Heaven's Cloud, Kiyomori, Muramasa
Level Jump8, Vertical Jump8

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



The Pengwin
Male
Cancer
43
41
Bard
Black Magic
Faith Save
Secret Hunt
Teleport

Bloody Strings

Black Hood
Genji Armor
Bracer

Last Song, Diamond Blade, Space Storage, Hydra Pit
Fire, Fire 3, Empower



Nok
Female
Taurus
80
76
Knight
Time Magic
Regenerator
Secret Hunt
Swim

Giant Axe
Bronze Shield
Iron Helmet
Linen Cuirass
Wizard Mantle

Shield Break, Weapon Break, Magic Break, Power Break, Night Sword, Surging Sword, Explosion Sword
Haste, Slow 2, Float, Quick, Stabilize Time



ZZ Yoshi
Female
Libra
75
53
Calculator
Time Magic
Regenerator
Maintenance
Jump+1

Bestiary

Headgear
Clothes
Sprint Shoes

CT, Height, 5, 4, 3
Haste, Immobilize, Float, Quick, Stabilize Time



Cameron
Male
Sagittarius
72
67
Bard
Basic Skill
Distribute
Equip Gun
Move-HP Up

Blast Gun

Feather Hat
Leather Armor
Diamond Armlet

Angel Song
Throw Stone, Heal, Tickle, Wish

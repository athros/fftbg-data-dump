Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Skipsandwiches
Male
Aquarius
52
69
Geomancer
Black Magic
Sunken State
Equip Gun
Lava Walking

Glacier Gun
Crystal Shield
Red Hood
Clothes
Jade Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard
Fire, Fire 4, Bolt 2, Bolt 3, Bolt 4, Flare



DeathTaxesAndAnime
Female
Scorpio
49
43
Mediator
Draw Out
Sunken State
Magic Attack UP
Move+3

Bestiary

Flash Hat
Silk Robe
Small Mantle

Invitation, Praise, Preach, Death Sentence
Bizen Boat



HASTERIOUS
Male
Leo
57
72
Lancer
Steal
PA Save
Secret Hunt
Jump+3

Obelisk
Ice Shield
Bronze Helmet
Linen Cuirass
Sprint Shoes

Level Jump5, Vertical Jump8
Gil Taking, Steal Helmet, Steal Weapon



Cleyon
Male
Scorpio
67
80
Lancer
Steal
Brave Save
Equip Bow
Move+2

Ultimus Bow

Circlet
Chain Mail
Cursed Ring

Level Jump5, Vertical Jump6
Gil Taking, Steal Weapon, Arm Aim, Leg Aim

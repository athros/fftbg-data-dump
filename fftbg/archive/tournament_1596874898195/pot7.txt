Final Bets: yellow - 11 bets for 9,810G (69.9%, x0.43); brown - 7 bets for 4,230G (30.1%, x2.32)

yellow bets:
ko2q: 2,698G (27.5%, 2,698G)
UmaiJam: 2,000G (20.4%, 18,702G)
Drusiform: 1,884G (19.2%, 1,884G)
AllInBot: 1,010G (10.3%, 1,010G)
Aestheta: 662G (6.7%, 662G)
BirbBrainsBot: 659G (6.7%, 84,452G)
resjudicata3: 364G (3.7%, 364G)
InOzWeTrust: 232G (2.4%, 232G)
gorgewall: 101G (1.0%, 2,967G)
MinBetBot: 100G (1.0%, 18,247G)
Firesheath: 100G (1.0%, 22,519G)

brown bets:
NicoSavoy: 1,269G (30.0%, 126,960G)
douchetron: 1,100G (26.0%, 1,100G)
Lumberinjack: 628G (14.8%, 628G)
Draconis345: 500G (11.8%, 4,418G)
PantherIscariot: 333G (7.9%, 4,833G)
butterbelljedi: 250G (5.9%, 2,057G)
holdenmagronik: 150G (3.5%, 3,485G)

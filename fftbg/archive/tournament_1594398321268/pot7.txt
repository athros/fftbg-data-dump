Final Bets: yellow - 8 bets for 5,830G (41.0%, x1.44); black - 9 bets for 8,389G (59.0%, x0.69)

yellow bets:
DavenIII: 1,681G (28.8%, 1,681G)
ShintaroNayaka: 1,010G (17.3%, 1,010G)
BirbBrainsBot: 1,000G (17.2%, 33,587G)
DeathTaxesAndAnime: 936G (16.1%, 936G)
neerrm: 543G (9.3%, 543G)
CapnChaos12: 300G (5.1%, 6,990G)
LazarusAjin: 260G (4.5%, 260G)
RunicMagus: 100G (1.7%, 3,832G)

black bets:
Chuckolator: 5,164G (61.6%, 90,984G)
randgridr: 1,263G (15.1%, 6,319G)
gorgewall: 812G (9.7%, 812G)
Shalloween: 400G (4.8%, 54,676G)
Snowfats: 250G (3.0%, 5,527G)
datadrivenbot: 200G (2.4%, 62,960G)
CosmicTactician: 100G (1.2%, 43,249G)
MaraDyne: 100G (1.2%, 100G)
nifboy: 100G (1.2%, 7,543G)

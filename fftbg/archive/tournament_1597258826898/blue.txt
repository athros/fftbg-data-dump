Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



XBizzy
Male
Libra
51
44
Lancer
Talk Skill
PA Save
Secret Hunt
Ignore Terrain

Obelisk
Gold Shield
Circlet
Wizard Robe
Rubber Shoes

Level Jump2, Vertical Jump8
Invitation, Persuade, Praise, Threaten, Preach, Solution, Insult, Refute



Error72
Male
Pisces
51
46
Ninja
Sing
Auto Potion
Martial Arts
Waterwalking



Twist Headband
Chain Vest
Leather Mantle

Bomb, Staff, Dictionary
Cheer Song, Battle Song, Magic Song, Hydra Pit



MonchoStrife
Female
Libra
58
52
Summoner
Battle Skill
MA Save
Equip Shield
Ignore Terrain

Madlemgen
Platinum Shield
Ribbon
Chameleon Robe
Small Mantle

Moogle, Titan, Odin, Leviathan
Head Break, Armor Break, Shield Break, Mind Break, Justice Sword



DavenIII
Female
Sagittarius
74
71
Geomancer
Talk Skill
MP Restore
Equip Polearm
Move+2

Battle Bamboo
Genji Shield
Leather Hat
Linen Robe
Feather Mantle

Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Death Sentence, Refute

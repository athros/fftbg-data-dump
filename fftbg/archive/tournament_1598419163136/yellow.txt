Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Resjudicata3
Female
Gemini
79
53
Mime

Arrow Guard
Equip Armor
Move-HP Up



Leather Helmet
Mythril Vest
108 Gems

Mimic




Roofiepops
Male
Taurus
38
70
Chemist
Steal
Auto Potion
Magic Defense UP
Move+1

Cute Bag

Red Hood
Leather Outfit
Setiemson

Potion, Ether, Holy Water, Remedy
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Arm Aim



Killth3kid
Male
Capricorn
79
45
Thief
Item
Earplug
Short Charge
Move+3

Ninja Edge

Triangle Hat
Clothes
Angel Ring

Steal Helmet, Steal Accessory, Steal Status, Arm Aim
Potion, X-Potion, Ether, Eye Drop, Phoenix Down



Lowlf
Female
Aquarius
57
65
Priest
Steal
Regenerator
Doublehand
Swim

Oak Staff

Black Hood
Silk Robe
Small Mantle

Cure 2, Cure 3, Raise, Raise 2, Shell, Wall
Gil Taking, Steal Shield

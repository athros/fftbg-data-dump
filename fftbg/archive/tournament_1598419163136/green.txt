Player: !Green
Team: Green Team
Palettes: Green/White



Reinoe
Male
Sagittarius
53
62
Thief
Basic Skill
Absorb Used MP
Long Status
Teleport

Sleep Sword

Flash Hat
Adaman Vest
Genji Gauntlet

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Arm Aim
Accumulate, Dash, Heal, Yell, Wish



CorpusCav
Female
Virgo
53
62
Chemist
Punch Art
MP Restore
Dual Wield
Waterbreathing

Mythril Knife
Cute Bag
Barette
Secret Clothes
Power Wrist

Potion, X-Potion, Hi-Ether, Soft, Holy Water, Phoenix Down
Spin Fist, Purification, Revive, Seal Evil



Latebit
Male
Gemini
51
70
Priest
Throw
Mana Shield
Doublehand
Lava Walking

Flame Whip

Golden Hairpin
Judo Outfit
Magic Gauntlet

Cure 2, Raise, Raise 2, Reraise, Shell 2, Esuna, Holy
Shuriken, Bomb, Staff, Dictionary



Nekojin
Female
Leo
73
60
Chemist
Charge
Critical Quick
Equip Armor
Move-HP Up

Blast Gun

Black Hood
Light Robe
Sprint Shoes

Potion, Hi-Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down
Charge+1, Charge+5, Charge+7

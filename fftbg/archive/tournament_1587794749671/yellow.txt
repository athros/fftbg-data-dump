Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



OneHundredFists
Male
Gemini
50
43
Bard
Battle Skill
HP Restore
Dual Wield
Move+3

Bloody Strings
Bloody Strings
Headgear
Bronze Armor
Angel Ring

Battle Song, Magic Song, Last Song
Head Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Surging Sword



Reinoe
Male
Virgo
59
63
Knight
Draw Out
PA Save
Equip Sword
Move+2

Kiyomori
Escutcheon
Iron Helmet
White Robe
Reflect Ring

Armor Break, Shield Break, Speed Break, Justice Sword
Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Masamune



Ross From Cali
Female
Virgo
73
77
Mediator
Time Magic
Absorb Used MP
Equip Armor
Move+1

Mythril Gun

Leather Helmet
Chameleon Robe
Feather Boots

Preach, Death Sentence, Insult, Negotiate, Mimic Daravon
Immobilize, Float, Reflect, Quick, Stabilize Time



EnemyController
Male
Aquarius
72
54
Monk
Black Magic
HP Restore
Attack UP
Retreat



Golden Hairpin
Clothes
Leather Mantle

Pummel, Wave Fist, Earth Slash, Purification, Revive
Fire, Fire 3, Ice 2

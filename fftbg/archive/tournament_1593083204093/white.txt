Player: !White
Team: White Team
Palettes: White/Blue



Zagorsek
Monster
Scorpio
37
50
Bomb










Hasterious
Male
Scorpio
76
76
Chemist
Steal
Counter Tackle
Dual Wield
Move+3

Romanda Gun
Glacier Gun
Black Hood
Mythril Vest
Feather Boots

Potion, X-Potion, Soft, Phoenix Down
Steal Weapon, Steal Accessory



Ar Tactic
Male
Aquarius
50
51
Knight
Throw
Brave Save
Dual Wield
Waterwalking

Ragnarok
Broad Sword
Barbuta
Black Robe
Defense Armlet

Head Break, Shield Break, Mind Break, Stasis Sword
Shuriken



Grininda
Male
Libra
54
55
Summoner
Draw Out
Arrow Guard
Beastmaster
Lava Walking

Thunder Rod

Red Hood
Mystic Vest
Cursed Ring

Ifrit, Bahamut, Silf, Lich, Cyclops
Koutetsu

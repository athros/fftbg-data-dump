Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DeathTaxesAndAnime
Female
Taurus
62
50
Lancer
Draw Out
Abandon
Defend
Move-HP Up

Spear
Bronze Shield
Leather Helmet
Robe of Lords
Jade Armlet

Level Jump5, Vertical Jump5
Koutetsu, Kiyomori



Cryptopsy70
Male
Cancer
77
78
Wizard
Charge
Meatbone Slash
Equip Armor
Waterwalking

Mythril Knife

Barbuta
Leather Armor
Rubber Shoes

Fire 4, Bolt 4, Ice, Empower
Charge+1, Charge+2, Charge+5, Charge+20



Nizaha
Female
Pisces
58
80
Summoner
Black Magic
Caution
Short Charge
Waterbreathing

Poison Rod

Leather Hat
White Robe
Feather Boots

Moogle, Ifrit, Titan, Golem, Carbunkle, Leviathan, Salamander, Fairy, Lich
Fire 2, Empower, Flare



DejaPoo21
Male
Aquarius
79
66
Thief
Item
Sunken State
Throw Item
Move-HP Up

Spell Edge

Black Hood
Black Costume
Defense Ring

Steal Heart, Steal Armor, Steal Shield, Steal Weapon
Potion, Echo Grass, Phoenix Down

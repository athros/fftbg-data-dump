Final Bets: blue - 13 bets for 17,632G (45.7%, x1.19); green - 25 bets for 20,940G (54.3%, x0.84)

blue bets:
Digitalsocrates: 10,803G (61.3%, 21,606G)
Thyrandaal: 2,000G (11.3%, 3,826G)
SkylerBunny: 1,400G (7.9%, 104,537G)
CorpusCav: 846G (4.8%, 846G)
HaateXIII: 709G (4.0%, 1,419G)
Spuzzmocker: 476G (2.7%, 476G)
JLinkletter: 393G (2.2%, 393G)
AllInBot: 205G (1.2%, 205G)
AkiriXx: 204G (1.2%, 204G)
gorgewall: 201G (1.1%, 7,994G)
nok____: 195G (1.1%, 6,995G)
Ring_Wyrm: 100G (0.6%, 2,519G)
fluffskull: 100G (0.6%, 1,348G)

green bets:
OneHundredFists: 4,000G (19.1%, 44,088G)
Mesmaster: 3,000G (14.3%, 25,074G)
NicoSavoy: 2,251G (10.7%, 2,251G)
superdevon1: 1,151G (5.5%, 11,513G)
Deathmaker06: 1,000G (4.8%, 8,051G)
BirbBrainsBot: 1,000G (4.8%, 106,125G)
solomongrundy85: 1,000G (4.8%, 4,842G)
DeathTaxesAndAnime: 984G (4.7%, 984G)
NovaKnight21: 870G (4.2%, 870G)
electric_algus: 800G (3.8%, 8,506G)
Banzem: 708G (3.4%, 708G)
Evewho: 648G (3.1%, 648G)
Rislyeu: 596G (2.8%, 596G)
prince_rogers_nelson_: 448G (2.1%, 448G)
Lydian_C: 396G (1.9%, 396G)
CapnChaos12: 300G (1.4%, 11,727G)
roofiepops: 300G (1.4%, 300G)
getthemoneyz: 256G (1.2%, 902,598G)
serperemagus: 248G (1.2%, 248G)
abaven_alistair: 204G (1.0%, 204G)
mr_kindaro: 204G (1.0%, 204G)
GladiatorLupe: 200G (1.0%, 2,190G)
Arcblazer23: 176G (0.8%, 176G)
rednecknazgul: 100G (0.5%, 1,022G)
datadrivenbot: 100G (0.5%, 35,338G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Gorgewall
Female
Pisces
60
60
Dancer
Basic Skill
Earplug
Defend
Jump+1

Persia

Triangle Hat
Black Robe
Feather Boots

Polka Polka, Nameless Dance, Obsidian Blade
Dash, Throw Stone, Heal



WhiteTigress
Female
Virgo
61
65
Geomancer
Battle Skill
Critical Quick
Magic Attack UP
Lava Walking

Heaven's Cloud
Mythril Shield
Feather Hat
Wizard Robe
Genji Gauntlet

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shield Break, Magic Break, Mind Break, Dark Sword, Night Sword



HentaiWriter
Female
Aquarius
80
66
Priest
Basic Skill
Sunken State
Equip Armor
Move-MP Up

Flame Whip

Cross Helmet
Brigandine
Defense Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Regen, Shell, Wall, Esuna, Holy
Tickle, Yell, Cheer Up



Actual JP
Male
Taurus
75
65
Archer
Elemental
Auto Potion
Equip Shield
Move+1

Silver Bow
Crystal Shield
Triangle Hat
Earth Clothes
Feather Boots

Charge+1, Charge+4, Charge+5, Charge+7, Charge+10
Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !White
Team: White Team
Palettes: White/Blue



CorpusCav
Male
Taurus
57
73
Thief
Yin Yang Magic
Caution
Dual Wield
Move-HP Up

Dagger
Rune Blade
Leather Hat
Leather Outfit
Cursed Ring

Steal Shield, Steal Weapon, Steal Status, Leg Aim
Doubt Faith, Zombie, Silence Song, Foxbird, Sleep, Dark Holy



Deathmaker06
Female
Scorpio
46
55
Calculator
White Magic
Speed Save
Beastmaster
Retreat

Ivory Rod

Flash Hat
Mystic Vest
Germinas Boots

CT, 4, 3
Cure 4, Raise, Regen, Esuna



TheApprentice251
Female
Capricorn
79
65
Mediator
Basic Skill
Catch
Equip Armor
Swim

Battle Folio

Triangle Hat
Carabini Mail
Red Shoes

Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute, Rehabilitate
Dash, Heal, Cheer Up, Scream



Laserman1000
Male
Libra
60
70
Priest
Time Magic
Blade Grasp
Martial Arts
Levitate



Twist Headband
Brigandine
Feather Mantle

Raise, Protect 2, Esuna
Haste, Stop, Float, Stabilize Time, Meteor

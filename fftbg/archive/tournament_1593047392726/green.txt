Player: !Green
Team: Green Team
Palettes: Green/White



Galkife
Female
Aries
51
57
Summoner
Charge
Caution
Equip Axe
Jump+3

Gold Staff

Thief Hat
Silk Robe
Genji Gauntlet

Ifrit, Carbunkle, Bahamut, Odin, Salamander, Silf
Charge+1, Charge+7



Kohlingen
Female
Capricorn
51
42
Archer
Draw Out
Counter Flood
Equip Axe
Waterbreathing

Morning Star
Gold Shield
Flash Hat
Mystic Vest
Sprint Shoes

Charge+1, Charge+3, Charge+4
Asura, Kiyomori



Mesmaster
Monster
Pisces
47
77
Serpentarius










KonzeraLive
Female
Gemini
57
81
Summoner
Battle Skill
Dragon Spirit
Sicken
Jump+1

Battle Folio

Red Hood
Earth Clothes
Red Shoes

Shiva, Titan, Golem, Salamander, Silf, Fairy
Head Break, Magic Break, Stasis Sword

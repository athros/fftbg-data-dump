Final Bets: red - 16 bets for 42,207G (80.2%, x0.25); yellow - 14 bets for 10,436G (19.8%, x4.04)

red bets:
sinnyil2: 16,836G (39.9%, 33,673G)
upvla: 10,000G (23.7%, 21,103G)
DustBirdEX: 3,488G (8.3%, 3,488G)
roqqqpsi: 2,719G (6.4%, 6,180G)
evontno: 2,573G (6.1%, 2,573G)
Error72: 1,730G (4.1%, 1,730G)
Creggers: 1,000G (2.4%, 40,636G)
datadrivenbot: 963G (2.3%, 37,222G)
thaetreis: 700G (1.7%, 27,875G)
Xoomwaffle: 500G (1.2%, 2,917G)
killth3kid: 404G (1.0%, 1,636G)
ColetteMSLP: 400G (0.9%, 4,923G)
BoneMiser: 300G (0.7%, 300G)
letdowncity: 293G (0.7%, 1,709G)
gorgewall: 201G (0.5%, 3,881G)
CosmicTactician: 100G (0.2%, 42,586G)

yellow bets:
Geffro1908: 4,600G (44.1%, 9,396G)
BirbBrainsBot: 1,000G (9.6%, 176,439G)
UmaiJam: 1,000G (9.6%, 53,603G)
Shalloween: 1,000G (9.6%, 55,933G)
evdoggity: 500G (4.8%, 86,622G)
WitchHunterIX: 500G (4.8%, 39,302G)
AllInBot: 487G (4.7%, 487G)
byrdturbo: 420G (4.0%, 33,781G)
BlueAbs: 300G (2.9%, 5,394G)
SUGRboi: 273G (2.6%, 273G)
skipsandwiches: 120G (1.1%, 120G)
SephDarkheart: 100G (1.0%, 29,855G)
Miku_Shikhu: 100G (1.0%, 10,413G)
getthemoneyz: 36G (0.3%, 1,299,271G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



AlenaZarek
Female
Libra
58
58
Chemist
Time Magic
Speed Save
Equip Armor
Lava Walking

Hydra Bag

Cachusha
Linen Robe
Magic Ring

Potion, Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down
Haste, Slow, Slow 2, Demi



FriendSquirrel
Male
Capricorn
64
73
Summoner
Throw
Speed Save
Martial Arts
Swim

Poison Rod

Twist Headband
Light Robe
Vanish Mantle

Moogle, Ramuh, Ifrit, Carbunkle, Bahamut, Leviathan, Salamander, Fairy
Shuriken, Knife, Dictionary



KonzeraLive
Male
Sagittarius
52
44
Thief
Item
Abandon
Equip Shield
Jump+1

Mage Masher
Buckler
Leather Hat
Brigandine
Diamond Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon
Hi-Potion, Ether, Holy Water, Remedy, Phoenix Down



Phi Sig
Female
Pisces
50
72
Samurai
Summon Magic
PA Save
Defense UP
Fly

Heaven's Cloud

Barbuta
White Robe
Rubber Shoes

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori
Moogle, Ramuh, Carbunkle, Silf, Lich

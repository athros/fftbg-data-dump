Player: !Green
Team: Green Team
Palettes: Green/White



Breakdown777
Female
Aries
51
59
Lancer
Time Magic
Speed Save
Concentrate
Waterbreathing

Javelin
Bronze Shield
Cross Helmet
Diamond Armor
Leather Mantle

Level Jump2, Vertical Jump8
Haste, Slow 2, Reflect, Demi, Demi 2, Stabilize Time



FoeSquirrel
Female
Capricorn
51
62
Thief
Draw Out
Damage Split
Equip Polearm
Teleport

Persia

Feather Hat
Earth Clothes
Bracer

Gil Taking, Steal Heart, Steal Accessory
Koutetsu, Bizen Boat, Muramasa, Kikuichimoji



Lanshaft
Female
Aquarius
61
65
Ninja
Time Magic
Auto Potion
Concentrate
Jump+3

Flame Whip
Ninja Edge
Thief Hat
Judo Outfit
Sprint Shoes

Shuriken
Float, Quick, Demi 2



Bahamutlagooon
Female
Taurus
45
42
Oracle
Black Magic
Sunken State
Magic Attack UP
Move+2

Battle Folio

Thief Hat
Silk Robe
Battle Boots

Spell Absorb, Life Drain, Pray Faith, Foxbird, Confusion Song, Dispel Magic, Paralyze
Bolt, Bolt 3, Ice, Empower

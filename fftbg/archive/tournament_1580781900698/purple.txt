Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Steveicus
Female
Pisces
76
42
Ninja
Black Magic
Parry
Equip Bow
Waterwalking

Perseus Bow

Golden Hairpin
Mythril Vest
Spike Shoes

Shuriken, Bomb
Fire, Fire 2, Bolt 4, Ice, Ice 2



8BitAngel
Male
Taurus
79
60
Priest
Elemental
Parry
Equip Sword
Lava Walking

Heaven's Cloud

Black Hood
Robe of Lords
Wizard Mantle

Cure 3, Raise, Raise 2, Protect 2, Shell, Esuna
Pitfall, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Thronewolf
Male
Libra
51
64
Archer
Item
Sunken State
Equip Bow
Jump+3

Cross Bow
Flame Shield
Green Beret
Mythril Vest
Small Mantle

Charge+3, Charge+7
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down



Go Pack
Female
Taurus
76
58
Wizard
Charge
Earplug
Magic Attack UP
Ignore Terrain

Assassin Dagger

Holy Miter
Black Robe
Dracula Mantle

Fire, Fire 3, Bolt, Bolt 2, Bolt 4, Ice, Frog
Charge+1

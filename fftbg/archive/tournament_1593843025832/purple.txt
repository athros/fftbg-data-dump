Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Smokegiant
Male
Gemini
47
75
Monk
Time Magic
Arrow Guard
Dual Wield
Teleport



Barette
Black Costume
Elf Mantle

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Haste, Slow, Slow 2, Stop, Reflect, Quick, Demi 2, Stabilize Time, Meteor



Omegasuspekt
Female
Capricorn
42
48
Oracle
Elemental
Faith Save
Equip Polearm
Jump+3

Spear

Green Beret
Light Robe
Dracula Mantle

Poison, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Blizzard, Lava Ball



DeathTaxesAndAnime
Female
Sagittarius
58
62
Lancer
Item
Counter Flood
Throw Item
Ignore Terrain

Javelin
Aegis Shield
Platinum Helmet
Plate Mail
Small Mantle

Level Jump8, Vertical Jump2
Potion, Hi-Potion, Ether, Soft



OrgasmicToast
Female
Libra
70
65
Ninja
Steal
Caution
Defend
Fly

Dagger
Main Gauche
Holy Miter
Brigandine
Spike Shoes

Shuriken, Staff, Spear, Wand
Gil Taking

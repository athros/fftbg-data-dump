Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ar Tactic
Male
Sagittarius
62
55
Mediator
Steal
MA Save
Magic Attack UP
Waterwalking

Bestiary

Triangle Hat
Judo Outfit
Leather Mantle

Death Sentence, Insult, Negotiate, Rehabilitate
Arm Aim



Lordminsc
Male
Libra
51
41
Mime

Sunken State
Short Status
Jump+3



Twist Headband
Judo Outfit
Cherche

Mimic




Vorap
Male
Taurus
48
61
Thief
Elemental
Counter Magic
Equip Knife
Swim

Thunder Rod

Golden Hairpin
Power Sleeve
Reflect Ring

Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



LanseDM
Male
Virgo
51
55
Monk
Draw Out
Arrow Guard
Dual Wield
Move+1



Feather Hat
Mystic Vest
Germinas Boots

Wave Fist, Earth Slash, Chakra, Revive
Asura, Muramasa

Player: !Black
Team: Black Team
Palettes: Black/Red



Rytor
Male
Libra
71
65
Samurai
Punch Art
Hamedo
Short Charge
Jump+1

Asura Knife

Diamond Helmet
Wizard Robe
Red Shoes

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Muramasa
Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Seal Evil



Choco Joe
Female
Gemini
42
46
Time Mage
Talk Skill
Critical Quick
Equip Polearm
Waterwalking

Octagon Rod

Black Hood
Linen Robe
N-Kai Armlet

Haste, Demi, Demi 2, Stabilize Time
Preach, Insult, Rehabilitate



FriendSquirrel
Monster
Scorpio
51
77
Bull Demon










TheMurkGnome
Male
Cancer
60
71
Ninja
Time Magic
Brave Save
Secret Hunt
Waterbreathing

Sasuke Knife
Kunai
Green Beret
Earth Clothes
Elf Mantle

Bomb
Haste, Slow 2, Stop, Demi, Stabilize Time, Meteor

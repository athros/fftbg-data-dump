Player: !Green
Team: Green Team
Palettes: Green/White



Diddleyumcious
Male
Sagittarius
55
68
Mediator
Draw Out
Distribute
Equip Knife
Ignore Height

Ice Rod

Red Hood
Chameleon Robe
Magic Ring

Invitation, Persuade, Praise, Threaten, Solution
Koutetsu, Bizen Boat, Muramasa



Evontno
Female
Leo
47
63
Calculator
Yin Yang Magic
PA Save
Equip Knife
Swim

Koga Knife

Flash Hat
Wizard Outfit
Cursed Ring

Height, Prime Number, 5, 3
Life Drain, Dispel Magic, Paralyze, Sleep



SolarisFall
Male
Libra
37
49
Samurai
Item
MP Restore
Defense UP
Levitate

Kikuichimoji

Crystal Helmet
Linen Cuirass
Battle Boots

Murasame, Heaven's Cloud
X-Potion, Phoenix Down



Kahlindra
Female
Aries
47
53
Dancer
Summon Magic
Faith Up
Equip Bow
Jump+2

Ice Bow

Feather Hat
Leather Outfit
Genji Gauntlet

Wiznaibus, Slow Dance, Nameless Dance, Last Dance, Nether Demon
Moogle, Titan, Golem, Bahamut, Leviathan, Salamander, Silf, Fairy, Lich

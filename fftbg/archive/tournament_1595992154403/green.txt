Player: !Green
Team: Green Team
Palettes: Green/White



Sinnyil2
Male
Virgo
57
79
Mime

Arrow Guard
Martial Arts
Move+2



Red Hood
Brigandine
Elf Mantle

Mimic




Shalloween
Male
Gemini
69
56
Thief
Sing
Counter Tackle
Equip Bow
Teleport

Bow Gun

Holy Miter
Wizard Outfit
Rubber Shoes

Steal Heart, Steal Accessory, Leg Aim
Nameless Song, Last Song



Zagorsek
Male
Taurus
61
69
Lancer
Summon Magic
Parry
Equip Polearm
Lava Walking

Cypress Rod
Kaiser Plate
Mythril Helmet
Diamond Armor
Wizard Mantle

Level Jump3, Vertical Jump6
Moogle, Ramuh, Titan, Golem, Carbunkle, Silf, Fairy



Roofiepops
Female
Libra
51
67
Geomancer
Time Magic
HP Restore
Defense UP
Move-HP Up

Battle Axe
Crystal Shield
Flash Hat
Wizard Outfit
Reflect Ring

Pitfall, Hallowed Ground, Local Quake, Sand Storm, Gusty Wind
Haste, Haste 2, Slow 2, Stop, Float, Demi, Stabilize Time

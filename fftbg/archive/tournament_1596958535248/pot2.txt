Final Bets: green - 5 bets for 7,116G (50.2%, x0.99); yellow - 12 bets for 7,064G (49.8%, x1.01)

green bets:
AllInBot: 5,743G (80.7%, 5,743G)
CT_5_Holy: 1,000G (14.1%, 5,261G)
holdenmagronik: 150G (2.1%, 3,005G)
Lydian_C: 123G (1.7%, 19,492G)
MinBetBot: 100G (1.4%, 16,447G)

yellow bets:
DeathTaxesAndAnime: 1,000G (14.2%, 1,000G)
Lythe_Caraker: 1,000G (14.2%, 162,925G)
BirbBrainsBot: 1,000G (14.2%, 137,450G)
getthemoneyz: 1,000G (14.2%, 1,559,716G)
EnemyController: 1,000G (14.2%, 1,546,306G)
randgridr: 600G (8.5%, 6,444G)
CorpusCav: 400G (5.7%, 5,031G)
Zachara: 304G (4.3%, 140,304G)
DesertWooder: 260G (3.7%, 260G)
datadrivenbot: 200G (2.8%, 47,294G)
sinnyil2: 200G (2.8%, 3,305G)
Chompie: 100G (1.4%, 9,231G)

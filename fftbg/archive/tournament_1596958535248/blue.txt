Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Chuckolator
Male
Taurus
48
67
Bard
Item
Meatbone Slash
Throw Item
Retreat

Bloody Strings

Flash Hat
Power Sleeve
Wizard Mantle

Angel Song, Magic Song, Last Song
Potion, Hi-Potion, X-Potion, Hi-Ether, Echo Grass, Holy Water, Phoenix Down



Mister Quof
Monster
Sagittarius
77
39
Chocobo










DeathTaxesAndAnime
Female
Aquarius
46
72
Mime

Auto Potion
Defend
Swim



Platinum Helmet
Mythril Vest
Small Mantle

Mimic




Seaweed B
Female
Aries
80
71
Mediator
Item
Brave Save
Dual Wield
Waterbreathing

Bestiary
Battle Folio
Leather Hat
Power Sleeve
Feather Boots

Invitation, Threaten, Preach, Solution, Mimic Daravon, Refute, Rehabilitate
Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down

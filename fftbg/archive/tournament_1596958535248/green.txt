Player: !Green
Team: Green Team
Palettes: Green/White



CT 5 Holy
Male
Leo
58
69
Archer
Draw Out
Arrow Guard
Short Charge
Move+3

Night Killer
Buckler
Leather Hat
Judo Outfit
Bracer

Charge+2, Charge+5, Charge+7, Charge+20
Asura



Douchetron
Male
Libra
48
60
Samurai
Black Magic
Brave Save
Dual Wield
Ignore Height

Kiyomori
Muramasa
Genji Helmet
Platinum Armor
Magic Gauntlet

Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa, Kikuichimoji
Fire 2, Fire 4, Bolt, Ice 3, Flare



Terminusterminal
Female
Leo
40
70
Calculator
Time Magic
Speed Save
Equip Gun
Teleport

Blast Gun

Barette
Judo Outfit
Magic Gauntlet

CT, Height, Prime Number, 4, 3
Haste, Slow, Reflect, Stabilize Time, Meteor



Lowlf
Male
Gemini
52
71
Monk
Yin Yang Magic
Damage Split
Magic Defense UP
Ignore Terrain



Feather Hat
Chain Vest
Bracer

Pummel, Earth Slash, Purification, Chakra, Revive, Seal Evil
Blind, Life Drain, Doubt Faith, Silence Song, Foxbird

Player: !White
Team: White Team
Palettes: White/Blue



Superdevon1
Male
Taurus
69
45
Lancer
Basic Skill
Speed Save
Sicken
Retreat

Mythril Spear
Gold Shield
Diamond Helmet
Wizard Robe
Jade Armlet

Level Jump8, Vertical Jump4
Accumulate



Evdoggity
Female
Libra
53
54
Geomancer
Summon Magic
Dragon Spirit
Beastmaster
Teleport

Battle Axe
Escutcheon
Cachusha
Power Sleeve
Germinas Boots

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shiva, Ifrit, Titan, Golem, Salamander



Randgridr
Female
Aquarius
62
70
Wizard
Throw
Damage Split
Concentrate
Fly

Orichalcum

Red Hood
Light Robe
Spike Shoes

Fire, Bolt 2, Ice 3, Frog, Death
Knife, Hammer



Powermhero
Male
Sagittarius
63
42
Monk
Summon Magic
Counter Flood
Defend
Move-MP Up



Triangle Hat
Chain Vest
Power Wrist

Wave Fist, Secret Fist, Purification, Revive
Moogle, Shiva, Carbunkle, Salamander, Lich

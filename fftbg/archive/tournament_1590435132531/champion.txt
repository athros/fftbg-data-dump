Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Benticore
Monster
Aries
71
43
Dark Behemoth










Lawnboxer
Female
Taurus
57
63
Knight
Steal
Counter Tackle
Attack UP
Teleport

Defender
Ice Shield
Diamond Helmet
Diamond Armor
Reflect Ring

Head Break, Armor Break, Speed Break, Power Break, Dark Sword
Steal Heart, Steal Shield, Steal Weapon, Steal Accessory, Steal Status



Ferroniss
Male
Capricorn
77
71
Lancer
Sing
Damage Split
Doublehand
Ignore Terrain

Holy Lance

Cross Helmet
Genji Armor
Red Shoes

Level Jump4, Vertical Jump8
Angel Song, Cheer Song, Battle Song, Magic Song, Last Song, Space Storage, Sky Demon, Hydra Pit



Powergems
Male
Libra
78
77
Archer
White Magic
Sunken State
Dual Wield
Jump+3

Poison Bow
Poison Bow
Thief Hat
Clothes
108 Gems

Charge+1, Charge+2, Charge+7
Raise, Reraise, Protect, Protect 2, Wall

Player: !Green
Team: Green Team
Palettes: Green/White



DustBirdEX
Female
Pisces
71
69
Archer
Steal
MA Save
Magic Defense UP
Ignore Height

Long Bow

Twist Headband
Earth Clothes
Germinas Boots

Charge+1, Charge+2, Charge+4, Charge+5, Charge+10, Charge+20
Leg Aim



OneHundredFists
Female
Aquarius
50
49
Chemist
Dance
Critical Quick
Short Status
Fly

Hydra Bag

Red Hood
Wizard Outfit
Angel Ring

Potion, Hi-Ether, Antidote, Maiden's Kiss, Phoenix Down
Slow Dance, Obsidian Blade, Nether Demon



TheChainNerd
Male
Sagittarius
41
69
Monk
Battle Skill
Blade Grasp
Equip Knife
Teleport 2

Main Gauche

Red Hood
Clothes
Defense Armlet

Wave Fist, Purification, Chakra, Revive
Shield Break, Weapon Break, Justice Sword



Arch8000
Female
Sagittarius
68
69
Thief
White Magic
Earplug
Concentrate
Levitate

Sleep Sword

Red Hood
Leather Outfit
108 Gems

Steal Status, Arm Aim, Leg Aim
Cure, Cure 3, Raise, Shell, Shell 2, Esuna

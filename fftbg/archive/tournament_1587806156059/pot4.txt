Final Bets: purple - 14 bets for 20,143G (84.0%, x0.19); brown - 5 bets for 3,838G (16.0%, x5.25)

purple bets:
sinnyil2: 3,500G (17.4%, 67,550G)
JackOnFire1: 3,395G (16.9%, 3,395G)
Lydian_C: 2,400G (11.9%, 54,541G)
vorap: 2,000G (9.9%, 4,566G)
WireLord: 2,000G (9.9%, 21,157G)
troublesometree: 1,879G (9.3%, 1,879G)
HaplessOne: 1,500G (7.4%, 203,849G)
leakimiko: 1,106G (5.5%, 13,834G)
KasugaiRoastedPeas: 1,000G (5.0%, 7,285G)
XalSmellsBad: 500G (2.5%, 11,113G)
TheMurkGnome: 500G (2.5%, 16,888G)
ungabunga_bot: 163G (0.8%, 265,674G)
ApplesauceBoss: 100G (0.5%, 10,761G)
datadrivenbot: 100G (0.5%, 1,195G)

brown bets:
mayfu: 1,500G (39.1%, 50,782G)
Zachara: 1,000G (26.1%, 58,000G)
BirbBrainsBot: 1,000G (26.1%, 193,337G)
getthemoneyz: 238G (6.2%, 584,673G)
Basmal: 100G (2.6%, 50,782G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vorap
Female
Libra
56
75
Ninja
Talk Skill
Speed Save
Doublehand
Move-HP Up

Orichalcum

Black Hood
Clothes
Sprint Shoes

Shuriken, Bomb
Invitation, Praise, Threaten, Preach, Solution, Insult, Refute



Comely312
Male
Virgo
73
51
Ninja
Talk Skill
Sunken State
Magic Attack UP
Waterwalking

Sasuke Knife
Orichalcum
Golden Hairpin
Adaman Vest
Reflect Ring

Shuriken
Praise, Threaten, Preach, Death Sentence, Insult



Nocturnal Perspective
Female
Gemini
58
53
Priest
Charge
Absorb Used MP
Equip Gun
Fly

Papyrus Codex

Holy Miter
Chameleon Robe
Defense Armlet

Cure 4, Protect 2, Wall, Esuna
Charge+1, Charge+4



ApplesauceBoss
Female
Aries
68
42
Time Mage
Basic Skill
Critical Quick
Equip Armor
Levitate

Rainbow Staff

Barbuta
Linen Cuirass
Feather Mantle

Haste, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time, Galaxy Stop
Dash, Cheer Up, Wish

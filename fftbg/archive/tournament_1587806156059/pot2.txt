Final Bets: green - 7 bets for 12,197G (56.7%, x0.76); yellow - 12 bets for 9,310G (43.3%, x1.31)

green bets:
Dexsana: 4,000G (32.8%, 4,206G)
JackOnFire1: 3,851G (31.6%, 3,851G)
evontno: 2,500G (20.5%, 11,626G)
upvla: 1,234G (10.1%, 5,268G)
ewan_e: 300G (2.5%, 9,547G)
KasugaiRoastedPeas: 200G (1.6%, 6,907G)
ungabunga_bot: 112G (0.9%, 266,115G)

yellow bets:
Lydian_C: 2,400G (25.8%, 52,221G)
WireLord: 2,000G (21.5%, 25,157G)
HaplessOne: 1,500G (16.1%, 202,852G)
BirbBrainsBot: 1,000G (10.7%, 195,337G)
getthemoneyz: 524G (5.6%, 585,561G)
TheMurkGnome: 500G (5.4%, 17,388G)
vorap: 400G (4.3%, 3,617G)
Zachara: 362G (3.9%, 58,362G)
Jahshine: 224G (2.4%, 224G)
gorgewall: 200G (2.1%, 3,294G)
datadrivenbot: 100G (1.1%, 1,183G)
Evewho: 100G (1.1%, 12,245G)

Player: !Green
Team: Green Team
Palettes: Green/White



Dexsana
Monster
Aquarius
68
48
Holy Dragon










Tempo Tantrum
Female
Sagittarius
54
39
Dancer
Throw
Meatbone Slash
Halve MP
Lava Walking

Cashmere

Green Beret
Chain Vest
N-Kai Armlet

Disillusion, Nether Demon, Dragon Pit
Shuriken, Wand



WireLord
Male
Gemini
64
61
Archer
Punch Art
Counter
Maintenance
Fly

Hunting Bow
Buckler
Holy Miter
Secret Clothes
Defense Ring

Charge+7, Charge+20
Spin Fist, Purification, Revive



Sinnyil2
Female
Libra
43
70
Dancer
Steal
Parry
Equip Knife
Ignore Height

Wizard Rod

Golden Hairpin
Brigandine
Rubber Shoes

Witch Hunt, Slow Dance
Steal Armor

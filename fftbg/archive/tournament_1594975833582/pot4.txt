Final Bets: purple - 11 bets for 4,081G (66.6%, x0.50); brown - 4 bets for 2,049G (33.4%, x1.99)

purple bets:
SkyridgeZero: 1,500G (36.8%, 47,649G)
BirbBrainsBot: 1,000G (24.5%, 25,690G)
DHaveWord: 324G (7.9%, 9,000G)
ColetteMSLP: 300G (7.4%, 4,835G)
Evewho: 200G (4.9%, 2,095G)
datadrivenbot: 200G (4.9%, 30,837G)
soapydragonfly: 136G (3.3%, 136G)
Lydian_C: 121G (3.0%, 65,097G)
Arkreaver: 100G (2.5%, 10,027G)
Chompie: 100G (2.5%, 8,342G)
nerigera: 100G (2.5%, 100G)

brown bets:
AllInBot: 1,171G (57.1%, 1,171G)
TheMurkGnome: 612G (29.9%, 2,600G)
dtrain332: 144G (7.0%, 144G)
getthemoneyz: 122G (6.0%, 1,302,449G)

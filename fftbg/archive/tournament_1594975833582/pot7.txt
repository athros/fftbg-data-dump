Final Bets: blue - 5 bets for 2,646G (42.7%, x1.34); white - 10 bets for 3,548G (57.3%, x0.75)

blue bets:
prince_rogers_nelson_: 1,242G (46.9%, 1,242G)
TeaTime29: 1,000G (37.8%, 5,658G)
nebukin: 204G (7.7%, 204G)
Chompie: 100G (3.8%, 8,654G)
Arkreaver: 100G (3.8%, 10,042G)

white bets:
BirbBrainsBot: 1,000G (28.2%, 25,514G)
randgridr: 500G (14.1%, 3,886G)
TheMurkGnome: 435G (12.3%, 4,423G)
dtrain332: 407G (11.5%, 407G)
UnderOneLight: 300G (8.5%, 2,001G)
Usurpationblitz: 222G (6.3%, 1,242G)
AllInBot: 200G (5.6%, 200G)
datadrivenbot: 200G (5.6%, 30,802G)
getthemoneyz: 184G (5.2%, 1,302,221G)
nerigera: 100G (2.8%, 100G)

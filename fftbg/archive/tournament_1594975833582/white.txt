Player: !White
Team: White Team
Palettes: White/Blue



KasugaiRoastedPeas
Monster
Taurus
76
67
Holy Dragon










NovaKnight21
Female
Scorpio
57
58
Priest
Yin Yang Magic
Meatbone Slash
Equip Knife
Move+3

Spell Edge

Twist Headband
Mystic Vest
Cursed Ring

Cure, Cure 2, Cure 4, Reraise, Shell 2, Esuna
Poison, Spell Absorb, Life Drain, Silence Song, Foxbird, Dispel Magic, Sleep, Dark Holy



Letdowncity
Female
Libra
69
60
Dancer
Punch Art
Dragon Spirit
Short Charge
Waterbreathing

Cashmere

Golden Hairpin
Black Robe
Cursed Ring

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Obsidian Blade, Void Storage, Dragon Pit
Wave Fist, Revive



ALY327
Male
Capricorn
81
78
Oracle
White Magic
Counter
Magic Attack UP
Move+2

Battle Folio

Black Hood
Judo Outfit
N-Kai Armlet

Spell Absorb, Confusion Song, Dispel Magic, Sleep
Raise, Reraise, Protect, Wall, Esuna, Holy, Magic Barrier

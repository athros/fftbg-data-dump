Final Bets: black - 22 bets for 11,242G (55.6%, x0.80); purple - 21 bets for 8,975G (44.4%, x1.25)

black bets:
Zeroroute: 2,923G (26.0%, 6,072G)
Riot_Jayway: 2,222G (19.8%, 171,983G)
pandasforsale: 1,508G (13.4%, 1,508G)
HaplessOne: 708G (6.3%, 17,780G)
Lanshaft: 552G (4.9%, 19,339G)
EnemyController: 500G (4.4%, 20,013G)
Dothan89: 400G (3.6%, 1,539G)
humble_fabio: 333G (3.0%, 5,266G)
kingchadking: 256G (2.3%, 256G)
Lythe_Caraker: 250G (2.2%, 43,339G)
M0nk3yNutz: 240G (2.1%, 240G)
AlenaZarek: 200G (1.8%, 10,046G)
BenYuPoker: 200G (1.8%, 1,607G)
rico_flex: 150G (1.3%, 1,822G)
Zagorsek: 100G (0.9%, 279,055G)
ANFz: 100G (0.9%, 22,327G)
EvilLego6: 100G (0.9%, 4,505G)
SeraphPDH: 100G (0.9%, 5,246G)
Avin_Chaos: 100G (0.9%, 584G)
Jeardra: 100G (0.9%, 100G)
chronoxtrigger: 100G (0.9%, 2,089G)
datadrivenbot: 100G (0.9%, 12,724G)

purple bets:
BirbBrainsBot: 1,000G (11.1%, 195,829G)
bad1dea: 1,000G (11.1%, 188,390G)
SkylerBunny: 1,000G (11.1%, 80,643G)
DudeMonkey77: 1,000G (11.1%, 46,779G)
SeedSC: 1,000G (11.1%, 71,213G)
OmnibotGamma: 600G (6.7%, 600G)
orlandu_lv: 500G (5.6%, 10,032G)
Winnosh: 500G (5.6%, 2,517G)
Baron_von_Scrub: 477G (5.3%, 5,252G)
getthemoneyz: 464G (5.2%, 348,030G)
CorpusCav: 200G (2.2%, 1,699G)
Lazerkick: 200G (2.2%, 13,232G)
Oshimi12: 200G (2.2%, 2,945G)
ungabunga_bot: 142G (1.6%, 142G)
SpaZmodeus: 142G (1.6%, 142G)
Tougou: 100G (1.1%, 2,576G)
PotionDweller: 100G (1.1%, 10,932G)
RyuTsuno: 100G (1.1%, 2,354G)
deesignal: 100G (1.1%, 914G)
BadBlanket: 100G (1.1%, 5,283G)
rrazzoug: 50G (0.6%, 7,689G)

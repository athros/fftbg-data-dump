Player: !Red
Team: Red Team
Palettes: Red/Brown



Evdoggity
Female
Aquarius
62
78
Thief
Throw
Counter Magic
Equip Axe
Jump+2

Battle Axe

Leather Hat
Earth Clothes
Sprint Shoes

Steal Heart, Steal Armor, Steal Weapon, Steal Accessory
Shuriken, Knife, Wand



Alackovi
Male
Pisces
41
54
Time Mage
Punch Art
Regenerator
Martial Arts
Waterbreathing



Holy Miter
Secret Clothes
Dracula Mantle

Haste, Stop, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time
Earth Slash, Secret Fist, Chakra, Revive



Roofiepops
Male
Virgo
57
70
Priest
Steal
Counter Magic
Magic Attack UP
Jump+3

Scorpion Tail

Black Hood
Adaman Vest
Genji Gauntlet

Cure, Cure 4, Raise, Regen, Protect, Shell 2, Esuna, Magic Barrier
Gil Taking, Steal Helmet, Steal Armor, Steal Weapon



MyFakeLife
Female
Scorpio
48
71
Time Mage
Dance
Distribute
Doublehand
Ignore Terrain

Healing Staff

Red Hood
Linen Robe
Sprint Shoes

Haste, Slow, Slow 2, Reflect, Demi, Demi 2
Witch Hunt, Nameless Dance, Last Dance

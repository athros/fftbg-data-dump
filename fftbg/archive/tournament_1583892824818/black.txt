Player: !Black
Team: Black Team
Palettes: Black/Red



Zhyyn
Male
Pisces
78
73
Calculator
Beast Skill
Arrow Guard
Dual Wield
Swim

Musk Rod
Musk Rod
Thief Hat
Mythril Armor
Spike Shoes

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



Shalloween
Female
Gemini
61
53
Geomancer
Talk Skill
Arrow Guard
Equip Gun
Waterwalking

Blaze Gun
Aegis Shield
Red Hood
Earth Clothes
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Persuade, Praise, Negotiate, Refute



Mits89
Male
Gemini
40
44
Squire
Draw Out
Brave Up
Halve MP
Waterbreathing

Hunting Bow

Black Hood
Chain Mail
Jade Armlet

Dash, Throw Stone, Tickle, Yell, Scream
Muramasa



Phi Sig
Female
Aquarius
72
73
Samurai
Basic Skill
Counter Flood
Doublehand
Levitate

Muramasa

Bronze Helmet
Linen Cuirass
Chantage

Bizen Boat, Murasame, Kiyomori, Muramasa
Cheer Up

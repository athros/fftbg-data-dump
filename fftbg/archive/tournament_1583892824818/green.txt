Player: !Green
Team: Green Team
Palettes: Green/White



Tikotikotiko
Female
Capricorn
57
60
Chemist
Charge
Sunken State
Dual Wield
Waterbreathing

Air Knife
Cute Bag
Holy Miter
Wizard Outfit
Genji Gauntlet

Potion, Hi-Potion, Eye Drop, Soft, Phoenix Down
Charge+2, Charge+3, Charge+5, Charge+7



Alaylle
Male
Cancer
75
72
Chemist
Steal
Speed Save
Concentrate
Jump+2

Panther Bag

Green Beret
Chain Vest
Reflect Ring

Hi-Ether, Echo Grass, Holy Water, Remedy
Steal Heart, Steal Armor, Steal Status



Alacor
Male
Serpentarius
42
59
Knight
Throw
Mana Shield
Attack UP
Lava Walking

Platinum Sword
Escutcheon
Bronze Helmet
Platinum Armor
Feather Mantle

Armor Break, Shield Break, Magic Break, Power Break, Justice Sword
Bomb, Knife, Dictionary



Sect Cor
Male
Taurus
59
51
Thief
Summon Magic
Counter
Equip Knife
Waterbreathing

Assassin Dagger

Headgear
Adaman Vest
Red Shoes

Steal Accessory, Steal Status
Moogle, Titan, Golem, Bahamut, Odin, Salamander, Fairy, Lich

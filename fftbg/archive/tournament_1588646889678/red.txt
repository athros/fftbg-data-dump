Player: !Red
Team: Red Team
Palettes: Red/Brown



Galkife
Male
Aquarius
63
77
Bard
Battle Skill
Auto Potion
Equip Sword
Move+2

Save the Queen

Flash Hat
Adaman Vest
Feather Boots

Cheer Song, Magic Song, Nameless Song, Hydra Pit
Shield Break



UmaiJam
Male
Cancer
44
74
Monk
Time Magic
MP Restore
Doublehand
Fly



Headgear
Leather Outfit
Spike Shoes

Spin Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil
Haste, Slow 2, Demi 2, Stabilize Time



Lionhermit
Female
Capricorn
80
42
Wizard
Talk Skill
Arrow Guard
Halve MP
Teleport

Main Gauche

Golden Hairpin
Wizard Robe
Leather Mantle

Fire 4, Bolt, Bolt 2, Ice 2, Frog
Invitation, Persuade, Death Sentence, Refute, Rehabilitate



Byrdturbo
Male
Sagittarius
47
59
Mime

Damage Split
Equip Shield
Move+2


Bronze Shield
Triangle Hat
Power Sleeve
Rubber Shoes

Mimic


Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Aldrammech
Female
Taurus
63
58
Chemist
Time Magic
Parry
Sicken
Ignore Terrain

Hydra Bag

Twist Headband
Mythril Vest
Leather Mantle

Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down
Slow, Float, Reflect, Demi 2, Stabilize Time



DudeMonkey77
Female
Capricorn
55
65
Priest
Battle Skill
Catch
Attack UP
Move-MP Up

Morning Star

Flash Hat
White Robe
Sprint Shoes

Cure, Cure 2, Shell, Shell 2, Esuna, Holy
Armor Break, Magic Break, Power Break, Mind Break, Night Sword



Fluffywormhole
Female
Aries
57
65
Wizard
Talk Skill
Brave Up
Short Status
Move+3

Dragon Rod

Ribbon
Mystic Vest
Wizard Mantle

Fire 2, Bolt, Bolt 3, Bolt 4
Solution, Death Sentence, Mimic Daravon, Refute



BurningSaph
Female
Capricorn
70
65
Knight
Item
Earplug
Defense UP
Ignore Terrain

Ragnarok

Platinum Helmet
Mythril Armor
Magic Ring

Head Break, Magic Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Hi-Ether, Elixir, Holy Water, Remedy, Phoenix Down

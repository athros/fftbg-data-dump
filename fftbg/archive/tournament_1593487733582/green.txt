Player: !Green
Team: Green Team
Palettes: Green/White



Moshyhero
Male
Scorpio
63
45
Summoner
Black Magic
MP Restore
Attack UP
Waterwalking

Flame Rod

Triangle Hat
Mythril Vest
Battle Boots

Ramuh, Ifrit, Carbunkle, Odin, Salamander, Silf, Lich
Fire, Bolt, Bolt 4, Ice, Ice 4



Baron Von Scrub
Male
Cancer
44
43
Time Mage
Steal
Sunken State
Equip Knife
Jump+1

Hidden Knife

Red Hood
Black Robe
Power Wrist

Haste, Haste 2, Slow, Slow 2, Immobilize, Float, Reflect, Demi, Stabilize Time
Steal Armor, Steal Shield, Leg Aim



Forkmore
Female
Scorpio
68
45
Dancer
Item
MA Save
Magic Defense UP
Teleport

Ryozan Silk

Flash Hat
Linen Robe
Germinas Boots

Slow Dance, Last Dance, Void Storage, Nether Demon
Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down



ColetteMSLP
Male
Taurus
46
41
Mime

Counter Magic
Short Status
Retreat



Leather Hat
Mythril Vest
Wizard Mantle

Mimic


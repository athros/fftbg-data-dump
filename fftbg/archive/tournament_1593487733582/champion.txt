Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ApplesauceBoss
Male
Aries
49
65
Knight
Basic Skill
Auto Potion
Attack UP
Waterbreathing

Ragnarok
Crystal Shield
Crystal Helmet
Bronze Armor
Magic Ring

Shield Break, Weapon Break, Magic Break, Justice Sword
Accumulate, Dash, Heal



Evewho
Female
Taurus
69
72
Knight
Black Magic
Dragon Spirit
Defense UP
Move+3

Star Bag
Mythril Shield
Leather Helmet
Light Robe
Battle Boots

Weapon Break, Speed Break, Mind Break, Justice Sword, Explosion Sword
Fire, Bolt, Bolt 3, Ice 3, Empower



Twelfthrootoftwo
Male
Cancer
40
67
Knight
Jump
HP Restore
Short Status
Swim

Defender
Escutcheon
Platinum Helmet
Plate Mail
Angel Ring

Head Break, Shield Break, Power Break, Mind Break
Level Jump4, Vertical Jump8



Magicandy
Female
Aquarius
76
80
Summoner
Basic Skill
Catch
Dual Wield
Move+1

Oak Staff
Wizard Rod
Holy Miter
Light Robe
Dracula Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle
Heal

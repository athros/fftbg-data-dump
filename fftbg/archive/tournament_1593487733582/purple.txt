Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Hyperhal
Male
Libra
42
53
Chemist
Yin Yang Magic
Auto Potion
Equip Axe
Retreat

Slasher

Triangle Hat
Wizard Outfit
Defense Armlet

Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water
Spell Absorb, Dispel Magic



HaychDub
Male
Libra
64
42
Monk
Talk Skill
Damage Split
Equip Knife
Waterwalking

Poison Rod

Flash Hat
Adaman Vest
Magic Gauntlet

Revive, Seal Evil
Invitation, Persuade, Threaten, Refute



Evdoggity
Female
Gemini
55
66
Thief
Throw
PA Save
Secret Hunt
Teleport

Ninja Edge

Thief Hat
Earth Clothes
Red Shoes

Steal Heart, Steal Helmet, Steal Armor, Steal Shield
Shuriken, Bomb, Sword



DeathTaxesAndAnime
Female
Pisces
73
57
Lancer
Dance
PA Save
Equip Axe
Ignore Height

Slasher
Buckler
Circlet
Wizard Robe
Reflect Ring

Level Jump8, Vertical Jump8
Wiznaibus, Slow Dance, Void Storage, Nether Demon, Dragon Pit

Final Bets: blue - 8 bets for 6,297G (34.1%, x1.93); yellow - 9 bets for 12,167G (65.9%, x0.52)

blue bets:
latebit: 3,000G (47.6%, 10,855G)
Aldrammech: 1,412G (22.4%, 1,412G)
killth3kid: 672G (10.7%, 14,262G)
resjudicata3: 500G (7.9%, 2,429G)
Rurk: 312G (5.0%, 312G)
datadrivenbot: 200G (3.2%, 59,115G)
gorgewall: 101G (1.6%, 4,641G)
MinBetBot: 100G (1.6%, 22,969G)

yellow bets:
Mesmaster: 6,790G (55.8%, 13,314G)
NicoSavoy: 2,111G (17.4%, 52,781G)
BirbBrainsBot: 1,000G (8.2%, 190,403G)
Drusiform: 500G (4.1%, 20,323G)
zenlion: 500G (4.1%, 3,031G)
TheDeeyo: 500G (4.1%, 9,963G)
randgridr: 304G (2.5%, 304G)
getthemoneyz: 262G (2.2%, 1,591,889G)
AllInBot: 200G (1.6%, 200G)

Final Bets: white - 8 bets for 6,939G (37.4%, x1.67); brown - 11 bets for 11,594G (62.6%, x0.60)

white bets:
DeathTaxesAndAnime: 3,441G (49.6%, 6,749G)
Drusiform: 1,000G (14.4%, 20,582G)
killth3kid: 672G (9.7%, 13,590G)
Aldrammech: 652G (9.4%, 652G)
randgridr: 465G (6.7%, 465G)
BoneMiser: 409G (5.9%, 409G)
datadrivenbot: 200G (2.9%, 58,915G)
RunicMagus: 100G (1.4%, 6,494G)

brown bets:
latebit: 7,855G (67.8%, 7,855G)
BirbBrainsBot: 1,000G (8.6%, 190,921G)
zenlion: 500G (4.3%, 3,290G)
sinnyil2: 500G (4.3%, 14,632G)
resjudicata3: 500G (4.3%, 1,929G)
Rurk: 312G (2.7%, 312G)
AllInBot: 304G (2.6%, 304G)
getthemoneyz: 214G (1.8%, 1,592,025G)
Lillifen: 208G (1.8%, 208G)
gorgewall: 101G (0.9%, 4,540G)
MinBetBot: 100G (0.9%, 22,869G)

Player: !White
Team: White Team
Palettes: White/Blue



LDHaten
Female
Scorpio
52
40
Geomancer
Item
Catch
Secret Hunt
Teleport

Giant Axe
Diamond Shield
Feather Hat
Wizard Robe
N-Kai Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Lava Ball
Potion, Antidote, Holy Water, Phoenix Down



Lube Squid
Male
Aries
64
52
Time Mage
Sing
Hamedo
Equip Polearm
Move-HP Up

Bestiary

Leather Hat
Linen Robe
Magic Ring

Haste, Haste 2, Slow, Immobilize, Float, Quick, Stabilize Time
Magic Song, Nameless Song



TheManInPlaid
Monster
Virgo
60
53
Red Chocobo










Extinctosaurus
Male
Aries
71
55
Chemist
Summon Magic
Counter
Attack UP
Move-MP Up

Star Bag

Golden Hairpin
Rubber Costume
Spike Shoes

Potion, Ether, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down
Moogle, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Lich

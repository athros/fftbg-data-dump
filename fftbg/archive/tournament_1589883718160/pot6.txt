Final Bets: white - 9 bets for 3,262G (42.8%, x1.33); brown - 9 bets for 4,354G (57.2%, x0.75)

white bets:
superdevon1: 856G (26.2%, 10,706G)
rocl: 625G (19.2%, 6,254G)
helpimabug: 500G (15.3%, 5,210G)
joewcarson: 400G (12.3%, 3,176G)
Killing_Moon: 338G (10.4%, 338G)
SQUiDSQUARKLIN: 328G (10.1%, 328G)
ungabunga_bot: 102G (3.1%, 506,134G)
datadrivenbot: 100G (3.1%, 18,709G)
roqqqpsi: 13G (0.4%, 1,317G)

brown bets:
BirbBrainsBot: 1,000G (23.0%, 172,480G)
Evewho: 1,000G (23.0%, 11,322G)
getthemoneyz: 730G (16.8%, 686,792G)
Zachara: 500G (11.5%, 82,000G)
JLinkletter: 423G (9.7%, 423G)
Candina: 246G (5.6%, 246G)
NovaKnight21: 200G (4.6%, 25,513G)
zeando: 155G (3.6%, 1,555G)
E_Ballard: 100G (2.3%, 6,711G)

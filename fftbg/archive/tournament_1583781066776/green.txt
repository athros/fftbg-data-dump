Player: !Green
Team: Green Team
Palettes: Green/White



LazialeChicago
Monster
Taurus
39
65
Mindflayer










SacrificialToast
Female
Libra
46
62
Wizard
Item
Counter
Sicken
Jump+1

Faith Rod

Leather Hat
Robe of Lords
Bracer

Fire, Bolt, Bolt 3, Ice, Ice 2, Frog, Death, Flare
Potion, X-Potion, Hi-Ether, Echo Grass, Holy Water, Remedy, Phoenix Down



Gelwain
Female
Capricorn
59
55
Priest
Basic Skill
Parry
Magic Attack UP
Move-HP Up

White Staff

Black Hood
Black Costume
Power Wrist

Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Shell 2, Esuna
Accumulate, Heal, Tickle, Yell, Wish



Cryptopsy70
Male
Libra
54
41
Archer
Black Magic
Abandon
Sicken
Ignore Terrain

Ice Bow

Feather Hat
Wizard Outfit
Rubber Shoes

Charge+2, Charge+7
Fire, Bolt, Bolt 3

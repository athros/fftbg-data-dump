Player: !Green
Team: Green Team
Palettes: Green/White



DudeMonkey77
Male
Cancer
41
38
Samurai
Sing
MA Save
Concentrate
Levitate

Kikuichimoji

Cross Helmet
Linen Cuirass
Reflect Ring

Asura, Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa
Life Song, Cheer Song



Kl0kwurk
Female
Sagittarius
58
44
Wizard
Battle Skill
Hamedo
Equip Armor
Jump+2

Assassin Dagger

Gold Helmet
Linen Robe
Wizard Mantle

Fire 3, Fire 4, Bolt, Bolt 3, Ice 3, Empower
Shield Break, Weapon Break, Speed Break, Mind Break



AniZero
Male
Aries
79
51
Ninja
Elemental
Earplug
Concentrate
Lava Walking

Morning Star
Flame Whip
Leather Hat
Brigandine
Small Mantle

Shuriken, Bomb, Knife
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Babooshness
Male
Leo
41
57
Mime

PA Save
Equip Armor
Move+3



Platinum Helmet
Chameleon Robe
Magic Ring

Mimic


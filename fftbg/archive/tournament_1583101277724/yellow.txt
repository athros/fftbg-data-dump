Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Smegma Sorcerer
Male
Taurus
45
72
Wizard
Talk Skill
PA Save
Martial Arts
Fly



Twist Headband
Chain Vest
Rubber Shoes

Fire 3, Bolt 4, Empower
Solution, Death Sentence, Refute, Rehabilitate



Thegapman
Male
Capricorn
75
70
Knight
Talk Skill
Brave Up
Maintenance
Move-HP Up

Blood Sword
Ice Shield
Crystal Helmet
Gold Armor
Germinas Boots

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword
Solution, Mimic Daravon, Rehabilitate



RjA0zcOQ96
Male
Gemini
78
66
Mime

Arrow Guard
Martial Arts
Move+1



Headgear
Earth Clothes
Cursed Ring

Mimic




ForagerCats
Female
Sagittarius
77
67
Archer
Talk Skill
Sunken State
Long Status
Ignore Terrain

Long Bow

Black Hood
Mystic Vest
Dracula Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5
Persuade, Threaten, Preach, Death Sentence, Insult, Negotiate, Mimic Daravon, Refute

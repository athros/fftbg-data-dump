Player: !Red
Team: Red Team
Palettes: Red/Brown



ApplesauceBoss
Male
Libra
50
57
Lancer
Basic Skill
HP Restore
Doublehand
Move+3

Mythril Spear

Cross Helmet
Gold Armor
108 Gems

Level Jump5, Vertical Jump5
Accumulate, Heal, Tickle, Cheer Up



Oobs56
Monster
Cancer
75
62
Apanda










Evewho
Female
Taurus
41
52
Geomancer
Charge
Sunken State
Long Status
Jump+1

Iron Sword
Gold Shield
Feather Hat
Judo Outfit
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Charge+1, Charge+3, Charge+4, Charge+5, Charge+20



Prince Rogers Nelson
Male
Cancer
59
75
Lancer
Yin Yang Magic
Sunken State
Dual Wield
Jump+3

Spear
Holy Lance
Barbuta
Chain Mail
Defense Ring

Level Jump2, Vertical Jump6
Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Paralyze, Sleep

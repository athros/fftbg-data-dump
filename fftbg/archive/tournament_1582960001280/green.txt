Player: !Green
Team: Green Team
Palettes: Green/White



Diablos24
Male
Libra
43
64
Knight
Elemental
Catch
Doublehand
Move+1

Mythril Sword

Platinum Helmet
Genji Armor
Reflect Ring

Head Break, Armor Break, Shield Break, Power Break, Dark Sword, Night Sword
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball



TheUnforgivenRage
Female
Cancer
43
73
Thief
Punch Art
MP Restore
Long Status
Move-HP Up

Main Gauche

Leather Hat
Chain Vest
Rubber Shoes

Steal Heart, Arm Aim
Wave Fist, Secret Fist, Purification, Chakra, Revive



Mizucrux
Male
Leo
64
60
Squire
White Magic
Earplug
Short Status
Ignore Height

Giant Axe
Buckler
Diamond Helmet
Linen Cuirass
Angel Ring

Yell, Wish
Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect 2, Esuna



Numbersborne
Male
Aries
41
42
Mediator
Punch Art
Parry
Equip Shield
Waterbreathing

Romanda Gun
Escutcheon
Thief Hat
Silk Robe
Power Wrist

Invitation, Persuade, Insult, Negotiate, Refute, Rehabilitate
Secret Fist, Revive

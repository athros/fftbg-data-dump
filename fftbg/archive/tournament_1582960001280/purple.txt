Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



R Raynos
Male
Scorpio
73
37
Monk
Throw
HP Restore
Short Status
Jump+1



Flash Hat
Power Sleeve
Feather Boots

Pummel, Wave Fist, Secret Fist, Purification
Shuriken, Wand, Dictionary



Dhekix
Male
Libra
68
80
Ninja
Elemental
Counter Magic
Equip Bow
Move-HP Up

Ice Bow

Holy Miter
Earth Clothes
Leather Mantle

Knife
Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Sypheck
Female
Aries
60
44
Priest
Charge
Meatbone Slash
Dual Wield
Teleport

Oak Staff
Sage Staff
Triangle Hat
Black Costume
Defense Armlet

Cure 2, Cure 3, Raise, Raise 2, Regen, Shell 2, Esuna
Charge+2, Charge+10



Strumisgod
Monster
Sagittarius
64
54
Holy Dragon








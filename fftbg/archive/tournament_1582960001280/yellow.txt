Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Leakimiko
Male
Capricorn
70
39
Wizard
Draw Out
Counter Magic
Dual Wield
Waterwalking

Blind Knife
Blind Knife
Twist Headband
Wizard Outfit
N-Kai Armlet

Fire 4, Bolt, Ice, Ice 3, Frog
Asura, Heaven's Cloud, Muramasa



Pandasforsale
Female
Aries
51
71
Wizard
Steal
Catch
Equip Sword
Move-MP Up

Koutetsu Knife

Black Hood
Silk Robe
Power Wrist

Fire, Fire 4, Bolt 3, Bolt 4, Ice 2, Ice 3, Ice 4, Death
Gil Taking, Steal Shield



Dj Ghost Phrat
Male
Aquarius
63
43
Archer
Draw Out
Earplug
Beastmaster
Ignore Height

Lightning Bow

Twist Headband
Black Costume
Magic Gauntlet

Charge+1
Bizen Boat, Heaven's Cloud



Dynasti
Female
Cancer
41
53
Archer
Draw Out
Absorb Used MP
Defend
Move+1

Long Bow

Platinum Helmet
Brigandine
Diamond Armlet

Charge+2, Charge+3, Charge+5, Charge+7, Charge+10
Asura, Kiyomori

Final Bets: white - 15 bets for 14,570G (34.9%, x1.87); champion - 15 bets for 27,209G (65.1%, x0.54)

white bets:
Aldrammech: 8,208G (56.3%, 8,208G)
SkylerBunny: 1,200G (8.2%, 517,824G)
DuraiPapers: 1,000G (6.9%, 12,414G)
BirbBrainsBot: 1,000G (6.9%, 63,683G)
reinoe: 607G (4.2%, 1,215G)
Baron_von_Scrub: 512G (3.5%, 512G)
killth3kid: 500G (3.4%, 10,246G)
nhammen: 484G (3.3%, 484G)
DustBirdEX: 345G (2.4%, 4,524G)
getthemoneyz: 214G (1.5%, 1,077,347G)
maakur_: 100G (0.7%, 74,777G)
CosmicTactician: 100G (0.7%, 21,581G)
ANFz: 100G (0.7%, 6,699G)
nifboy: 100G (0.7%, 4,948G)
datadrivenbot: 100G (0.7%, 46,586G)

champion bets:
cam_ATS: 12,616G (46.4%, 12,616G)
WalkerNash: 5,000G (18.4%, 42,130G)
joewcarson: 3,000G (11.0%, 6,096G)
run_with_stone_GUNs: 1,408G (5.2%, 28,174G)
fenaen: 1,200G (4.4%, 29,866G)
dogsandcatsand: 1,111G (4.1%, 20,023G)
Rurk: 500G (1.8%, 10,958G)
Oreo_Pizza: 500G (1.8%, 2,341G)
Jeras01: 500G (1.8%, 1,951G)
roqqqpsi: 472G (1.7%, 47,260G)
Rilgon: 278G (1.0%, 556G)
cactus_tony_: 212G (0.8%, 212G)
Forkmore: 200G (0.7%, 200G)
RampagingRobot: 112G (0.4%, 112G)
Error72: 100G (0.4%, 17,369G)

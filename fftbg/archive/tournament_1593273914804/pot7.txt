Final Bets: red - 15 bets for 24,860G (75.3%, x0.33); white - 11 bets for 8,154G (24.7%, x3.05)

red bets:
Baron_von_Scrub: 5,804G (23.3%, 5,804G)
cam_ATS: 4,500G (18.1%, 17,116G)
Forkmore: 4,336G (17.4%, 4,336G)
fenaen: 3,000G (12.1%, 32,866G)
DudeMonkey77: 1,127G (4.5%, 1,127G)
cactus_tony_: 1,000G (4.0%, 1,000G)
dogsandcatsand: 1,000G (4.0%, 21,023G)
Aldrammech: 912G (3.7%, 9,120G)
Laserman1000: 800G (3.2%, 8,400G)
Rilgon: 555G (2.2%, 1,111G)
nhammen: 484G (1.9%, 968G)
RampagingRobot: 481G (1.9%, 481G)
roqqqpsi: 477G (1.9%, 47,737G)
run_with_stone_GUNs: 284G (1.1%, 28,458G)
datadrivenbot: 100G (0.4%, 46,686G)

white bets:
DuraiPapers: 3,066G (37.6%, 3,066G)
nifboy: 1,222G (15.0%, 1,222G)
SkylerBunny: 1,200G (14.7%, 514,165G)
BirbBrainsBot: 1,000G (12.3%, 60,634G)
Rurk: 500G (6.1%, 9,434G)
reinoe: 300G (3.7%, 300G)
DustBirdEX: 234G (2.9%, 3,811G)
getthemoneyz: 232G (2.8%, 1,076,640G)
Grandlanzer: 200G (2.5%, 31,529G)
maakur_: 100G (1.2%, 74,468G)
CosmicTactician: 100G (1.2%, 21,276G)

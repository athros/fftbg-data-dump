Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Ko2q
Male
Pisces
50
77
Knight
Black Magic
Meatbone Slash
Defense UP
Teleport 2

Ancient Sword
Flame Shield
Crystal Helmet
Diamond Armor
Diamond Armlet

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Fire, Bolt, Ice



Lythe Caraker
Male
Virgo
73
49
Mediator
Steal
Dragon Spirit
Defense UP
Move-MP Up

Stone Gun

Feather Hat
Linen Robe
Red Shoes

Invitation, Persuade, Preach, Refute
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Leg Aim



Cupholderr
Male
Virgo
63
44
Mediator
Yin Yang Magic
Brave Up
Short Charge
Levitate

Bestiary

Feather Hat
Light Robe
Elf Mantle

Praise, Threaten, Preach, Rehabilitate
Blind, Poison, Spell Absorb, Life Drain, Blind Rage, Foxbird, Dispel Magic, Dark Holy



Gorgewall
Female
Gemini
73
47
Monk
Basic Skill
Caution
Sicken
Move-MP Up



Holy Miter
Mythril Vest
Angel Ring

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Revive
Accumulate, Heal, Tickle, Fury

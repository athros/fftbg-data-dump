Player: !Red
Team: Red Team
Palettes: Red/Brown



Gripheenix
Male
Leo
66
41
Summoner
Charge
Counter Flood
Halve MP
Move-HP Up

Dragon Rod

Feather Hat
Power Sleeve
Spike Shoes

Moogle, Titan, Bahamut, Leviathan, Lich
Charge+2, Charge+3



Sairentozon7
Male
Pisces
70
78
Lancer
Time Magic
Critical Quick
Secret Hunt
Teleport

Partisan
Bronze Shield
Iron Helmet
Leather Armor
Diamond Armlet

Level Jump8, Vertical Jump4
Reflect, Stabilize Time, Galaxy Stop



Leakimiko
Male
Leo
48
62
Bard
Time Magic
Faith Up
Doublehand
Ignore Height

Ramia Harp

Holy Miter
Mythril Vest
Vanish Mantle

Life Song, Magic Song, Nameless Song, Diamond Blade, Hydra Pit
Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi, Demi 2, Stabilize Time



Waterwatereverywhere
Male
Gemini
67
55
Knight
Talk Skill
Parry
Dual Wield
Ignore Height

Broad Sword
Giant Axe
Platinum Helmet
Carabini Mail
Spike Shoes

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword, Surging Sword, Explosion Sword
Invitation, Persuade, Negotiate, Mimic Daravon, Refute, Rehabilitate

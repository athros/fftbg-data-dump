Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Breakdown777
Female
Pisces
53
77
Geomancer
Dance
Critical Quick
Concentrate
Swim

Giant Axe
Round Shield
Twist Headband
Wizard Outfit
Angel Ring

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Polka Polka, Nether Demon



Celdia
Male
Libra
75
58
Monk
Basic Skill
Mana Shield
Defense UP
Jump+2



Golden Hairpin
Mystic Vest
Battle Boots

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive
Accumulate, Yell



Omegasuspekt
Monster
Taurus
65
49
Hydra










Just Here2
Male
Serpentarius
46
68
Monk
Time Magic
Meatbone Slash
Defense UP
Lava Walking



Holy Miter
Mystic Vest
Jade Armlet

Spin Fist, Pummel, Secret Fist, Purification, Chakra
Haste, Slow 2, Stop, Stabilize Time

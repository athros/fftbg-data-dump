Player: !Green
Team: Green Team
Palettes: Green/White



Anak761
Male
Scorpio
54
47
Mediator
Summon Magic
MP Restore
Maintenance
Retreat

Mythril Gun

Flash Hat
Light Robe
Jade Armlet

Praise, Negotiate, Mimic Daravon, Refute
Moogle, Titan, Carbunkle, Odin, Leviathan, Zodiac



Fenixcrest
Male
Aries
77
70
Knight
Time Magic
Auto Potion
Equip Gun
Fly

Bloody Strings
Diamond Shield
Platinum Helmet
Linen Cuirass
Bracer

Shield Break, Weapon Break, Magic Break, Mind Break
Slow 2, Stop, Demi, Stabilize Time



ANFz
Male
Capricorn
42
57
Thief
Throw
Auto Potion
Equip Sword
Jump+3

Heaven's Cloud

Green Beret
Chain Vest
Bracer

Gil Taking, Steal Shield, Steal Status
Spear



Sinnyil2
Male
Scorpio
65
65
Monk
White Magic
Counter Flood
Attack UP
Fly



Barette
Earth Clothes
Angel Ring

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Raise, Protect, Shell 2, Esuna

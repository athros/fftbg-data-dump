Player: !Green
Team: Green Team
Palettes: Green/White



Superdevon1
Female
Taurus
46
41
Geomancer
Dance
Parry
Equip Polearm
Move+1

Ivory Rod
Round Shield
Twist Headband
Light Robe
Angel Ring

Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Blizzard, Gusty Wind
Witch Hunt, Last Dance, Obsidian Blade



RjA0zcOQ96
Male
Capricorn
43
44
Oracle
White Magic
Parry
Dual Wield
Ignore Height

Gokuu Rod
Musk Rod
Twist Headband
Linen Robe
Leather Mantle

Blind, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Petrify
Cure, Cure 2, Raise, Reraise, Protect 2, Esuna



ColetteMSLP
Female
Gemini
79
53
Calculator
Black Magic
Counter
Concentrate
Move-HP Up

Flame Rod

Holy Miter
Mythril Vest
108 Gems

CT, Prime Number, 5, 4, 3
Fire 2, Fire 3, Fire 4, Bolt, Ice 2, Frog, Flare



IndecisiveNinja
Male
Libra
41
45
Chemist
Punch Art
Auto Potion
Equip Shield
Lava Walking

Hydra Bag
Mythril Shield
Headgear
Earth Clothes
108 Gems

Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Wave Fist, Earth Slash, Secret Fist, Revive

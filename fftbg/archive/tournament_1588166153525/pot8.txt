Final Bets: purple - 20 bets for 8,624G (19.1%, x4.23); champion - 19 bets for 36,438G (80.9%, x0.24)

purple bets:
leakimiko: 1,235G (14.3%, 2,376G)
sinnyil2: 1,139G (13.2%, 1,139G)
ColetteMSLP: 1,000G (11.6%, 41,433G)
BirbBrainsBot: 1,000G (11.6%, 121,744G)
megaman2202: 843G (9.8%, 843G)
DavenIII: 576G (6.7%, 576G)
ZephyrTempest: 400G (4.6%, 1,581G)
DudeMonkey77: 333G (3.9%, 10,509G)
itsdigitalbro: 299G (3.5%, 299G)
PetitFoulard: 244G (2.8%, 244G)
Fanduscus: 219G (2.5%, 219G)
floopinthepig: 211G (2.4%, 211G)
Anethum: 200G (2.3%, 1,441G)
Mesmaster: 200G (2.3%, 24,392G)
commandlistfgc: 171G (2.0%, 171G)
Jahshine: 150G (1.7%, 3,024G)
Clydethecamel: 104G (1.2%, 104G)
AllInBot: 100G (1.2%, 100G)
CosmicTactician: 100G (1.2%, 6,944G)
troublesometree: 100G (1.2%, 1,726G)

champion bets:
FoxtrotNovemberCharlie: 18,319G (50.3%, 18,319G)
HaplessOne: 5,435G (14.9%, 10,871G)
edgehead62888: 4,202G (11.5%, 4,202G)
Lord_Burrah: 1,500G (4.1%, 109,000G)
SerumD: 1,331G (3.7%, 1,331G)
rjA0zcOQ96: 1,000G (2.7%, 29,569G)
Draconis345: 1,000G (2.7%, 87,934G)
JumbocactuarX27: 1,000G (2.7%, 17,839G)
Lydian_C: 600G (1.6%, 600G)
RagequitSA: 500G (1.4%, 4,232G)
getthemoneyz: 378G (1.0%, 611,012G)
fenaen: 300G (0.8%, 300G)
Pink_LuigiX: 272G (0.7%, 272G)
run_with_STONE_GUNS: 200G (0.5%, 4,333G)
ungabunga_bot: 129G (0.4%, 288,115G)
GrayGhostGaming: 100G (0.3%, 19,628G)
Bongomon7: 100G (0.3%, 4,460G)
DrAntiSocial: 50G (0.1%, 6,293G)
daveb_: 22G (0.1%, 2,662G)

Player: !White
Team: White Team
Palettes: White/Blue



E Ballard
Male
Serpentarius
56
54
Knight
Item
Sunken State
Doublehand
Jump+3

Long Sword

Genji Helmet
Linen Cuirass
Rubber Shoes

Shield Break, Speed Break, Power Break
Potion, Hi-Ether, Phoenix Down



Cryptopsy70
Monster
Libra
64
77
Byblos










Digitalsocrates
Male
Sagittarius
72
47
Chemist
Basic Skill
Critical Quick
Defend
Swim

Romanda Gun

Triangle Hat
Wizard Outfit
Feather Mantle

Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Heal, Yell, Fury, Scream



Reddwind
Male
Taurus
47
72
Knight
Black Magic
Counter Flood
Defend
Move+1

Long Sword
Bronze Shield
Circlet
Black Robe
Defense Armlet

Head Break, Weapon Break, Magic Break, Power Break, Night Sword
Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Frog, Flare

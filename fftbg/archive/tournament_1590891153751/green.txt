Player: !Green
Team: Green Team
Palettes: Green/White



Firesheath
Female
Virgo
49
58
Thief
Charge
Faith Save
Dual Wield
Fly

Blood Sword
Cultist Dagger
Golden Hairpin
Earth Clothes
Defense Armlet

Steal Accessory
Charge+2



Soren Of Tyto
Female
Capricorn
46
48
Mime

Catch
Martial Arts
Retreat



Red Hood
Wizard Outfit
Battle Boots

Mimic




Baron Von Scrub
Male
Gemini
60
44
Lancer
Item
Earplug
Doublehand
Ignore Height

Gokuu Rod

Platinum Helmet
Reflect Mail
108 Gems

Level Jump3, Vertical Jump6
Potion, Hi-Potion, Hi-Ether, Antidote, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Grandlanzer
Male
Cancer
71
59
Geomancer
Jump
Critical Quick
Equip Polearm
Retreat

Cypress Rod
Flame Shield
Red Hood
Silk Robe
N-Kai Armlet

Pitfall, Water Ball, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Level Jump8, Vertical Jump2

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Theseawolf1
Male
Aries
70
52
Mediator
Draw Out
Counter
Equip Polearm
Waterwalking

Iron Fan

Feather Hat
Earth Clothes
Wizard Mantle

Invitation, Preach, Negotiate, Mimic Daravon, Refute, Rehabilitate
Murasame, Heaven's Cloud, Muramasa



Roofiepops
Female
Pisces
64
73
Time Mage
Yin Yang Magic
Caution
Short Charge
Retreat

Cypress Rod

Leather Hat
Earth Clothes
Red Shoes

Stop, Immobilize, Demi, Stabilize Time, Meteor
Blind, Spell Absorb, Life Drain, Zombie, Confusion Song



Aeonicvector
Female
Libra
58
56
Oracle
Steal
Parry
Beastmaster
Move+2

Ivory Rod

Feather Hat
Chameleon Robe
Diamond Armlet

Spell Absorb, Zombie, Confusion Song, Dispel Magic, Paralyze, Dark Holy
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory



Omegasuspekt
Female
Gemini
46
39
Time Mage
Throw
Blade Grasp
Martial Arts
Move-MP Up



Holy Miter
Black Robe
Bracer

Haste, Slow, Slow 2, Stop, Immobilize, Demi, Stabilize Time
Shuriken, Bomb, Staff

Player: !Black
Team: Black Team
Palettes: Black/Red



AniZero
Female
Scorpio
56
54
Calculator
Yin Yang Magic
Distribute
Long Status
Swim

Battle Folio

Golden Hairpin
Earth Clothes
Magic Gauntlet

CT, 5, 4, 3
Poison, Life Drain, Doubt Faith, Confusion Song, Petrify



VVitchFaye
Male
Capricorn
44
53
Time Mage
Sing
Absorb Used MP
Martial Arts
Ignore Height



Triangle Hat
Silk Robe
Cursed Ring

Haste 2, Slow 2, Reflect, Quick, Demi
Angel Song, Cheer Song, Nameless Song



EunosXX
Male
Scorpio
52
41
Samurai
Punch Art
Counter
Sicken
Jump+3

Masamune

Circlet
Leather Armor
Vanish Mantle

Asura, Kiyomori, Muramasa
Secret Fist, Purification, Chakra, Revive



Teknakon
Male
Gemini
53
57
Wizard
Item
Mana Shield
Throw Item
Jump+2

Orichalcum

Triangle Hat
Clothes
Rubber Shoes

Fire, Fire 2, Fire 3, Bolt 2, Empower
Potion, Hi-Potion, Hi-Ether, Antidote, Phoenix Down

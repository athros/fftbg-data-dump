Player: !Green
Team: Green Team
Palettes: Green/White



LDHaten
Female
Leo
77
62
Geomancer
Item
Catch
Short Charge
Jump+3

Slasher
Buckler
Triangle Hat
Chameleon Robe
Small Mantle

Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Potion, Hi-Potion, X-Potion, Ether, Antidote, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



Aelmarkin
Male
Serpentarius
66
67
Chemist
Time Magic
Mana Shield
Short Status
Jump+2

Star Bag

Ribbon
Adaman Vest
Spike Shoes

Potion, Hi-Ether, Antidote, Eye Drop, Remedy
Haste 2, Slow 2, Quick, Demi, Stabilize Time



JDogg2K4
Female
Sagittarius
64
57
Archer
White Magic
Parry
Equip Shield
Jump+2

Ice Bow
Round Shield
Feather Hat
Judo Outfit
Defense Armlet

Charge+1, Charge+3, Charge+7, Charge+10
Cure 4, Raise, Reraise, Regen, Protect, Esuna



Gandrin
Monster
Libra
47
56
Vampire








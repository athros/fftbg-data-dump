Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Reverie3
Female
Serpentarius
76
72
Summoner
Time Magic
Arrow Guard
Magic Attack UP
Lava Walking

Poison Rod

Flash Hat
Clothes
Angel Ring

Moogle, Titan, Golem, Carbunkle, Lich
Float, Quick, Demi, Meteor



Firesheath
Male
Capricorn
38
47
Calculator
Black Magic
Critical Quick
Maintenance
Move-MP Up

Wizard Rod

Triangle Hat
Linen Robe
108 Gems

CT, Prime Number, 5, 4
Fire, Fire 4, Bolt, Bolt 2, Ice, Death, Flare



Bryan792
Male
Libra
61
76
Lancer
Throw
Auto Potion
Dual Wield
Jump+3

Partisan
Holy Lance
Diamond Helmet
Crystal Mail
Genji Gauntlet

Level Jump3, Vertical Jump6
Bomb, Knife, Wand



Sairentozon7
Male
Virgo
45
49
Ninja
Jump
Parry
Attack UP
Waterwalking

Main Gauche
Assassin Dagger
Cachusha
Judo Outfit
Jade Armlet

Shuriken, Bomb, Dictionary
Level Jump2, Vertical Jump6

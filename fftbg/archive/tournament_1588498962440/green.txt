Player: !Green
Team: Green Team
Palettes: Green/White



HaplessOne
Male
Aries
64
71
Knight
Jump
Counter
Equip Sword
Waterwalking

Long Sword
Round Shield
Diamond Helmet
Platinum Armor
Setiemson

Head Break, Shield Break, Weapon Break, Speed Break, Power Break, Stasis Sword, Surging Sword
Level Jump2, Vertical Jump7



Josephiroth 143
Male
Virgo
72
53
Lancer
White Magic
Mana Shield
Defense UP
Move-MP Up

Mythril Spear
Round Shield
Grand Helmet
Chain Mail
Small Mantle

Level Jump4, Vertical Jump7
Cure 3, Cure 4, Raise, Raise 2, Protect 2, Shell, Shell 2, Esuna



WrathfulRemy
Female
Cancer
46
53
Geomancer
Punch Art
Parry
Halve MP
Jump+1

Slasher
Diamond Shield
Flash Hat
Robe of Lords
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Pummel, Earth Slash, Purification, Seal Evil



RubenFlonne
Female
Aquarius
77
44
Chemist
Summon Magic
Parry
Halve MP
Teleport

Hydra Bag

Triangle Hat
Clothes
Magic Ring

Potion, X-Potion, Hi-Ether, Holy Water, Phoenix Down
Moogle, Shiva, Titan, Golem, Odin, Leviathan, Silf, Fairy

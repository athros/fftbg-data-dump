Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Pplvee1
Female
Pisces
53
80
Geomancer
Time Magic
Regenerator
Concentrate
Retreat

Slasher
Crystal Shield
Green Beret
Wizard Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Slow, Immobilize, Reflect, Galaxy Stop



Sairentozon7
Male
Gemini
51
76
Thief
Sing
MP Restore
Attack UP
Teleport

Diamond Sword

Feather Hat
Brigandine
Defense Ring

Gil Taking, Steal Helmet, Steal Armor, Arm Aim
Battle Song, Magic Song, Space Storage



Actbg09
Male
Virgo
71
70
Mime

Counter
Equip Armor
Move+3



Bronze Helmet
Robe of Lords
Rubber Shoes

Mimic




Legionx13
Female
Cancer
61
48
Squire
Item
MP Restore
Throw Item
Move+2

Flail
Buckler
Mythril Helmet
Wizard Outfit
Reflect Ring

Accumulate, Throw Stone, Cheer Up
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft, Remedy, Phoenix Down

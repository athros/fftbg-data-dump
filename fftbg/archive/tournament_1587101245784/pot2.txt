Final Bets: green - 15 bets for 24,095G (77.8%, x0.28); yellow - 12 bets for 6,864G (22.2%, x3.51)

green bets:
Nizaha: 8,222G (34.1%, 16,445G)
Nickyfive: 5,862G (24.3%, 7,817G)
reinoe: 4,826G (20.0%, 4,826G)
sinnyil2: 1,200G (5.0%, 49,567G)
HASTERIOUS: 752G (3.1%, 752G)
Shalloween: 600G (2.5%, 31,218G)
Maeveen: 500G (2.1%, 1,904G)
waterwatereverywhere: 497G (2.1%, 497G)
datadrivenbot: 469G (1.9%, 475G)
LAGBOT30000: 466G (1.9%, 9,324G)
Rinhander: 240G (1.0%, 240G)
GrandmasterFrankerZ: 150G (0.6%, 1,428G)
ungabunga_bot: 111G (0.5%, 145,683G)
Tougou: 100G (0.4%, 6,748G)
ko2q: 100G (0.4%, 9,507G)

yellow bets:
EmperorBeef: 2,232G (32.5%, 27,232G)
CapnChaos12: 1,312G (19.1%, 1,312G)
BirbBrainsBot: 1,000G (14.6%, 187,146G)
mirapoix: 960G (14.0%, 1,921G)
getthemoneyz: 312G (4.5%, 500,758G)
Ross_from_Cali: 262G (3.8%, 262G)
Evewho: 200G (2.9%, 5,216G)
ZephyrTempest: 182G (2.7%, 76,185G)
Scotty297: 104G (1.5%, 104G)
Firesheath: 100G (1.5%, 9,732G)
potgodtopdog: 100G (1.5%, 5,050G)
Roelath: 100G (1.5%, 513G)

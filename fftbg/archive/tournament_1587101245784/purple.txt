Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Tinyjeans
Female
Cancer
49
69
Wizard
Punch Art
Counter Magic
Maintenance
Move-HP Up

Poison Rod

Twist Headband
Chameleon Robe
Magic Ring

Fire 4, Bolt, Bolt 3, Ice 4
Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



Alrightbye
Female
Scorpio
61
66
Dancer
Jump
Speed Save
Short Status
Levitate

Persia

Headgear
Wizard Robe
Defense Ring

Wiznaibus, Nameless Dance, Void Storage, Dragon Pit
Level Jump5, Vertical Jump2



Pandasforsale
Male
Aries
51
72
Priest
Steal
Faith Up
Short Status
Lava Walking

Wizard Staff

Twist Headband
Chain Vest
Battle Boots

Raise, Reraise, Esuna
Gil Taking, Steal Accessory, Leg Aim



Phrossi V
Female
Taurus
37
79
Ninja
Jump
Hamedo
Equip Armor
Jump+2

Orichalcum
Ninja Edge
Grand Helmet
Linen Cuirass
Battle Boots

Shuriken, Bomb, Sword, Stick
Level Jump5, Vertical Jump2

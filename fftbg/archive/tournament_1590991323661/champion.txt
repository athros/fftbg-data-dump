Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



DAC169
Monster
Aquarius
63
42
Steel Giant










Grandlanzer
Male
Capricorn
55
80
Time Mage
Yin Yang Magic
Critical Quick
Equip Sword
Levitate

Kiyomori

Green Beret
Brigandine
Reflect Ring

Haste, Haste 2, Float, Quick, Demi 2, Stabilize Time
Blind, Spell Absorb, Pray Faith, Foxbird, Paralyze, Sleep, Dark Holy



Powerpinch
Male
Aquarius
62
78
Wizard
Time Magic
MP Restore
Martial Arts
Move-MP Up

Mage Masher

Headgear
Brigandine
Germinas Boots

Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3
Haste, Haste 2, Stabilize Time



Kyune
Male
Taurus
62
74
Bard
Talk Skill
Damage Split
Short Charge
Lava Walking

Windslash Bow

Red Hood
Wizard Outfit
Rubber Shoes

Angel Song, Life Song, Cheer Song, Diamond Blade, Hydra Pit
Invitation, Threaten, Insult, Mimic Daravon, Rehabilitate

Player: !White
Team: White Team
Palettes: White/Blue



Xoomwaffle
Female
Virgo
79
67
Knight
Draw Out
PA Save
Attack UP
Move+2

Save the Queen
Mythril Shield
Bronze Helmet
Bronze Armor
Elf Mantle

Shield Break, Weapon Break, Magic Break
Asura, Murasame, Muramasa



Redmage4evah
Female
Aries
47
52
Mediator
Summon Magic
Counter Flood
Sicken
Move-MP Up

Bestiary

Thief Hat
Chain Vest
N-Kai Armlet

Threaten, Insult, Refute, Rehabilitate
Moogle, Ramuh, Golem, Carbunkle, Leviathan, Salamander



DouglasDragonThePoet
Monster
Pisces
59
63
Gobbledeguck










NIghtdew14
Female
Scorpio
48
77
Dancer
Jump
PA Save
Magic Defense UP
Ignore Height

Persia

Triangle Hat
Mythril Vest
Diamond Armlet

Wiznaibus, Slow Dance, Last Dance
Level Jump8, Vertical Jump5

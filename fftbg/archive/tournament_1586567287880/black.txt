Player: !Black
Team: Black Team
Palettes: Black/Red



KaLam1ty
Male
Taurus
54
46
Priest
Summon Magic
Counter
Short Charge
Lava Walking

Wizard Staff

Feather Hat
Wizard Robe
Sprint Shoes

Cure 2, Reraise, Protect, Shell 2
Moogle, Titan, Odin, Salamander, Silf, Cyclops



StealthModeLocke
Monster
Aries
55
55
Draugr










Lord Burrah
Male
Capricorn
67
57
Ninja
Time Magic
MP Restore
Short Status
Teleport

Flame Whip
Morning Star
Headgear
Mystic Vest
Red Shoes

Shuriken, Bomb, Knife, Hammer
Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time



Pie108
Male
Virgo
70
77
Chemist
Black Magic
HP Restore
Maintenance
Ignore Terrain

Hydra Bag

Twist Headband
Adaman Vest
Feather Boots

Hi-Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Fire 2, Ice, Ice 2, Ice 3, Empower

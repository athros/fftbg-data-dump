Player: !Green
Team: Green Team
Palettes: Green/White



Zagorsek
Female
Libra
71
40
Oracle
Time Magic
Counter Tackle
Maintenance
Jump+3

Battle Folio

Feather Hat
Black Robe
Setiemson

Poison, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Slow, Stop, Float, Reflect, Meteor



Sociallubricant1
Female
Scorpio
55
70
Thief
Summon Magic
Absorb Used MP
Equip Sword
Retreat

Kiyomori

Leather Hat
Chain Vest
Salty Rage

Arm Aim
Moogle, Shiva, Ramuh, Carbunkle, Silf, Cyclops



Coledot
Female
Gemini
67
75
Archer
Basic Skill
Meatbone Slash
Attack UP
Retreat

Glacier Gun
Gold Shield
Triangle Hat
Brigandine
Reflect Ring

Charge+1, Charge+3, Charge+7, Charge+20
Accumulate, Fury, Wish



BuffaloCrunch
Female
Libra
71
45
Ninja
White Magic
HP Restore
Equip Gun
Move-HP Up

Bestiary
Battle Folio
Leather Hat
Black Costume
N-Kai Armlet

Shuriken, Spear, Wand, Dictionary
Cure, Shell, Wall

Player: !White
Team: White Team
Palettes: White/Blue



WireLord
Male
Aries
63
75
Lancer
Yin Yang Magic
Hamedo
Secret Hunt
Teleport 2

Javelin
Genji Shield
Platinum Helmet
Plate Mail
Defense Ring

Level Jump5, Vertical Jump6
Pray Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Petrify, Dark Holy



Old Overholt
Monster
Taurus
49
38
Black Chocobo










OneHundredFists
Female
Virgo
69
47
Oracle
Dance
Earplug
Equip Armor
Jump+1

Battle Bamboo

Bronze Helmet
Plate Mail
Dracula Mantle

Life Drain, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Paralyze, Petrify
Polka Polka, Nameless Dance



Joewcarson
Male
Aquarius
55
77
Summoner
Steal
Mana Shield
Concentrate
Ignore Terrain

Gold Staff

Twist Headband
Wizard Outfit
Defense Armlet

Shiva, Titan, Salamander, Silf
Gil Taking, Steal Armor, Steal Shield, Steal Weapon, Leg Aim

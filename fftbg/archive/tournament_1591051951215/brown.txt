Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HaateXIII
Female
Virgo
45
37
Calculator
Curse Skill
Speed Save
Dual Wield
Move+3

Bestiary
Bestiary
Black Hood
Mythril Vest
Leather Mantle

Blue Magic
Wing Attack, Look of Fright, Look of Devil, Doom, Beam, Throw Spirit, Drain Touch, Zombie Touch, Sleep Touch, Grease Touch



Laserman1000
Female
Aries
65
80
Mime

Counter Magic
Monster Talk
Move-MP Up



Flash Hat
Crystal Mail
Cursed Ring

Mimic




Killth3kid
Female
Taurus
64
68
Geomancer
Throw
HP Restore
Dual Wield
Move-HP Up

Giant Axe
Slasher
Ribbon
Black Costume
Vanish Mantle

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Bomb, Knife



Fluffywormhole
Female
Cancer
65
64
Oracle
Black Magic
MA Save
Doublehand
Retreat

Octagon Rod

Thief Hat
Secret Clothes
Reflect Ring

Life Drain, Dispel Magic, Paralyze, Sleep, Petrify, Dark Holy
Fire 2, Fire 3, Fire 4, Bolt, Bolt 3

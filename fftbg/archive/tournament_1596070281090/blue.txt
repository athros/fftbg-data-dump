Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



BoneMiser
Male
Sagittarius
79
52
Bard
Draw Out
Hamedo
Secret Hunt
Teleport

Fairy Harp

Feather Hat
Mystic Vest
Small Mantle

Nameless Song
Koutetsu, Bizen Boat, Heaven's Cloud



Randgridr
Female
Cancer
72
58
Knight
Time Magic
Critical Quick
Martial Arts
Lava Walking


Mythril Shield
Circlet
Gold Armor
Cherche

Armor Break, Mind Break, Night Sword
Slow, Float, Demi 2, Galaxy Stop



SephDarkheart
Female
Aquarius
69
57
Ninja
Black Magic
Counter Flood
Equip Polearm
Move-MP Up

Holy Lance
Gokuu Rod
Black Hood
Adaman Vest
Reflect Ring

Bomb, Knife, Wand
Fire, Fire 2, Bolt 3, Ice 2



Nifboy
Male
Capricorn
71
64
Knight
Time Magic
Absorb Used MP
Secret Hunt
Lava Walking

Blood Sword
Crystal Shield
Genji Helmet
Platinum Armor
Defense Ring

Head Break, Weapon Break, Speed Break, Power Break, Justice Sword
Float, Reflect, Quick, Demi, Meteor

Player: !Red
Team: Red Team
Palettes: Red/Brown



Victoriolue
Female
Taurus
74
49
Oracle
Draw Out
Absorb Used MP
Beastmaster
Waterwalking

Battle Bamboo

Green Beret
White Robe
Red Shoes

Blind, Pray Faith, Silence Song, Sleep, Dark Holy
Asura, Bizen Boat



ValtonZenola
Female
Serpentarius
54
81
Geomancer
Jump
Abandon
Secret Hunt
Move+2

Asura Knife
Diamond Shield
Triangle Hat
Light Robe
Jade Armlet

Pitfall, Local Quake, Static Shock, Quicksand, Sand Storm
Level Jump8, Vertical Jump5



Reddwind
Male
Taurus
78
44
Knight
Black Magic
Counter Magic
Equip Gun
Retreat

Mythril Gun
Diamond Shield
Genji Helmet
Linen Cuirass
Bracer

Shield Break, Speed Break, Mind Break
Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 2



Thyrandaal
Male
Scorpio
56
68
Time Mage
Yin Yang Magic
Catch
Short Charge
Jump+3

Wizard Staff

Triangle Hat
Black Robe
Cursed Ring

Slow, Slow 2, Stop, Float, Reflect, Demi
Poison, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Sleep, Petrify, Dark Holy

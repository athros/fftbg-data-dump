Player: !Red
Team: Red Team
Palettes: Red/Brown



Gripheenix
Female
Libra
57
72
Thief
Yin Yang Magic
Auto Potion
Equip Gun
Retreat

Papyrus Codex

Headgear
Wizard Outfit
N-Kai Armlet

Gil Taking, Steal Heart, Steal Shield, Leg Aim
Doubt Faith



Astrozin11
Male
Cancer
49
49
Geomancer
Sing
Distribute
Defend
Move+1

Rune Blade
Ice Shield
Flash Hat
Earth Clothes
Magic Gauntlet

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Angel Song, Cheer Song, Hydra Pit



HighJayster
Male
Scorpio
64
73
Summoner
Draw Out
PA Save
Dual Wield
Retreat

Thunder Rod
Ice Rod
Green Beret
Clothes
Wizard Mantle

Moogle, Shiva
Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud



Galkife
Male
Taurus
53
66
Chemist
Summon Magic
Absorb Used MP
Halve MP
Move+1

Star Bag

Thief Hat
Mystic Vest
Sprint Shoes

Potion, Hi-Potion, Ether, Soft
Moogle, Ramuh, Ifrit, Odin, Leviathan

Final Bets: blue - 9 bets for 3,848G (37.7%, x1.65); yellow - 8 bets for 6,351G (62.3%, x0.61)

blue bets:
Smokegiant: 1,000G (26.0%, 85,561G)
safira125: 748G (19.4%, 748G)
Laserman1000: 629G (16.3%, 24,329G)
RampagingRobot: 599G (15.6%, 8,599G)
superdevon1: 372G (9.7%, 372G)
KasugaiRoastedPeas: 200G (5.2%, 7,207G)
enkikavlar: 100G (2.6%, 2,997G)
AO110893: 100G (2.6%, 4,212G)
victoriolue: 100G (2.6%, 5,242G)

yellow bets:
Thyrandaal: 1,532G (24.1%, 1,532G)
HaateXIII: 1,156G (18.2%, 1,156G)
BirbBrainsBot: 1,000G (15.7%, 47,741G)
electric_algus: 1,000G (15.7%, 41,262G)
DeathTaxesAndAnime: 925G (14.6%, 1,814G)
silentkaster: 332G (5.2%, 11,332G)
Grandlanzer: 300G (4.7%, 56,145G)
getthemoneyz: 106G (1.7%, 1,190,697G)

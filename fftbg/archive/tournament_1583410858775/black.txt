Player: !Black
Team: Black Team
Palettes: Black/Red



YaBoy125
Male
Virgo
39
71
Knight
Time Magic
Damage Split
Magic Defense UP
Move-HP Up

Chaos Blade

Bronze Helmet
Black Robe
Feather Boots

Weapon Break, Magic Break, Speed Break, Surging Sword
Slow, Slow 2, Stop, Demi, Demi 2, Stabilize Time



Satystar
Male
Capricorn
46
72
Summoner
Punch Art
Counter Magic
Sicken
Ignore Height

Poison Rod

Holy Miter
Black Costume
Cherche

Moogle, Shiva, Ifrit, Golem, Carbunkle, Bahamut, Salamander, Fairy
Purification, Seal Evil



DaveStrider55
Female
Libra
71
63
Samurai
Battle Skill
Speed Save
Equip Knife
Teleport

Air Knife

Mythril Helmet
Wizard Robe
Red Shoes

Asura, Murasame
Head Break, Shield Break, Weapon Break, Night Sword, Surging Sword



Alexmozg
Male
Aries
68
41
Squire
Battle Skill
Brave Up
Beastmaster
Fly

Air Knife
Mythril Shield
Feather Hat
Diamond Armor
Magic Ring

Dash, Throw Stone, Heal, Wish, Ultima
Weapon Break, Power Break

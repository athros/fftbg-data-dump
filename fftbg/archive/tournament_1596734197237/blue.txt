Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gorgewall
Male
Taurus
41
74
Archer
Punch Art
Catch
Dual Wield
Teleport

Ice Bow

Flash Hat
Clothes
Genji Gauntlet

Charge+1, Charge+3
Pummel, Secret Fist, Purification, Chakra, Revive, Seal Evil



Latebit
Female
Gemini
35
57
Ninja
Charge
Absorb Used MP
Secret Hunt
Move+3

Flame Whip
Flail
Triangle Hat
Mystic Vest
Leather Mantle

Shuriken, Bomb, Knife
Charge+1, Charge+4, Charge+7



Sinnyil2
Female
Sagittarius
70
49
Summoner
Time Magic
Catch
Sicken
Retreat

Rod

Golden Hairpin
White Robe
Rubber Shoes

Shiva, Ramuh, Titan, Golem, Salamander
Slow, Quick, Demi, Demi 2



SkylerBunny
Male
Cancer
70
69
Chemist
Steal
Abandon
Long Status
Waterwalking

Main Gauche

Thief Hat
Wizard Outfit
Reflect Ring

Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down
Gil Taking, Steal Shield, Steal Weapon, Arm Aim

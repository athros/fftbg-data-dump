Final Bets: green - 16 bets for 10,099G (39.6%, x1.53); yellow - 12 bets for 15,406G (60.4%, x0.66)

green bets:
DeathTaxesAndAnime: 1,995G (19.8%, 1,995G)
JonnyCue: 1,582G (15.7%, 5,582G)
BirbBrainsBot: 1,000G (9.9%, 83,632G)
VolgraTheMoose: 943G (9.3%, 943G)
E_Ballard: 906G (9.0%, 906G)
actual_JP: 723G (7.2%, 723G)
killth3kid: 628G (6.2%, 628G)
CosmicTactician: 500G (5.0%, 3,313G)
nekojin: 500G (5.0%, 34,207G)
brenogarwin: 350G (3.5%, 350G)
Lord_Gwarth: 280G (2.8%, 280G)
datadrivenbot: 200G (2.0%, 50,657G)
getthemoneyz: 192G (1.9%, 1,529,801G)
Rook512: 100G (1.0%, 100G)
Happy0ni: 100G (1.0%, 100G)
amsans1247: 100G (1.0%, 4,866G)

yellow bets:
douchetron: 6,869G (44.6%, 13,469G)
SkylerBunny: 3,000G (19.5%, 191,476G)
TheMurkGnome: 2,000G (13.0%, 49,532G)
gorgewall: 1,200G (7.8%, 1,200G)
Zagorsek: 560G (3.6%, 560G)
benticore: 500G (3.2%, 46,326G)
DesertWooder: 500G (3.2%, 6,145G)
AllInBot: 295G (1.9%, 295G)
ivellioz: 200G (1.3%, 7,136G)
holdenmagronik: 150G (1.0%, 1,731G)
Miku_Shikhu: 100G (0.6%, 1,868G)
dtrain332: 32G (0.2%, 3,239G)

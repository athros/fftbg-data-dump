Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Eldente
Male
Capricorn
64
67
Samurai
Jump
Catch
Attack UP
Lava Walking

Kikuichimoji

Barbuta
Platinum Armor
Red Shoes

Asura, Bizen Boat, Murasame, Kikuichimoji
Level Jump2, Vertical Jump4



WinnerBit
Female
Sagittarius
58
57
Oracle
Jump
Auto Potion
Maintenance
Move+3

Cypress Rod

Headgear
Silk Robe
Bracer

Blind, Poison, Spell Absorb, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Level Jump8, Vertical Jump6



DHaveWord
Male
Serpentarius
47
56
Mediator
Item
Catch
Magic Attack UP
Move+2

Mythril Gun

Red Hood
Earth Clothes
Red Shoes

Preach, Death Sentence, Negotiate, Mimic Daravon, Refute
Potion, Maiden's Kiss, Soft, Holy Water



Rassyu
Male
Scorpio
71
46
Bard
Jump
Meatbone Slash
Martial Arts
Move-HP Up

Bloody Strings

Black Hood
Leather Outfit
108 Gems

Angel Song, Battle Song, Diamond Blade, Sky Demon, Hydra Pit
Level Jump2, Vertical Jump7

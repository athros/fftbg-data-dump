Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Alecttox
Monster
Capricorn
55
52
Steel Giant










Bad1dea
Male
Virgo
60
64
Chemist
Draw Out
Mana Shield
Secret Hunt
Move+1

Dagger

Green Beret
Mythril Vest
Elf Mantle

Potion, Hi-Potion, Ether, Hi-Ether, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Bizen Boat, Murasame, Muramasa



Cryptopsy70
Male
Sagittarius
40
47
Archer
Item
Counter Flood
Equip Axe
Move+2

Morning Star
Diamond Shield
Green Beret
Chain Vest
Battle Boots

Charge+1
X-Potion, Ether, Antidote, Eye Drop, Holy Water



Oogthecaveman
Male
Taurus
40
66
Priest
Sing
Regenerator
Magic Defense UP
Fly

Flail

Cachusha
Black Robe
Magic Gauntlet

Cure 4, Raise 2, Regen, Protect, Protect 2, Shell 2, Wall, Esuna, Magic Barrier
Cheer Song, Magic Song, Diamond Blade, Sky Demon

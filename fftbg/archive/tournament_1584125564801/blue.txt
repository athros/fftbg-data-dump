Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LeepingJJ
Male
Virgo
65
78
Oracle
Jump
MA Save
Concentrate
Ignore Height

Ivory Rod

Golden Hairpin
Chameleon Robe
Magic Gauntlet

Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Paralyze, Sleep
Level Jump8, Vertical Jump8



Jeeboheebo
Male
Taurus
52
52
Mime

Mana Shield
Monster Talk
Jump+3



Feather Hat
Earth Clothes
Diamond Armlet

Mimic




Nizaha
Male
Aquarius
39
46
Monk
Elemental
Counter
Equip Armor
Move+3



Circlet
Silk Robe
Bracer

Pummel, Wave Fist, Earth Slash, Purification, Chakra, Seal Evil
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



RongRongArts
Female
Leo
45
77
Chemist
Dance
Critical Quick
Equip Gun
Teleport

Bestiary

Feather Hat
Adaman Vest
Dracula Mantle

Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Wiznaibus, Slow Dance, Disillusion, Obsidian Blade

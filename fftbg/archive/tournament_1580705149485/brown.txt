Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nightyukiame
Female
Libra
80
59
Priest
Draw Out
Auto Potion
Maintenance
Jump+1

Oak Staff

Flash Hat
Chameleon Robe
Diamond Armlet

Cure, Cure 4, Regen, Protect 2, Wall, Esuna
Bizen Boat, Murasame, Heaven's Cloud



KitchTowel
Male
Leo
42
77
Mime

Arrow Guard
Martial Arts
Move+1



Twist Headband
Wizard Outfit
Setiemson

Mimic




Spoonstealer
Male
Leo
41
59
Oracle
Item
Mana Shield
Magic Attack UP
Jump+1

Papyrus Codex

Holy Miter
Light Robe
Battle Boots

Blind, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Petrify, Dark Holy
Potion, Hi-Potion, Hi-Ether, Eye Drop, Phoenix Down



Kijin151
Female
Aquarius
37
64
Dancer
Summon Magic
Parry
Short Status
Lava Walking

Cashmere

Twist Headband
Brigandine
Battle Boots

Slow Dance, Polka Polka, Last Dance, Void Storage
Moogle, Shiva, Carbunkle, Odin, Lich

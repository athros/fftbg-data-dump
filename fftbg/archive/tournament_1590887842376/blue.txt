Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



NovaKnight21
Female
Capricorn
73
49
Geomancer
Jump
Counter Flood
Equip Gun
Jump+2

Battle Folio
Escutcheon
Triangle Hat
Mythril Vest
Salty Rage

Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball
Level Jump8, Vertical Jump8



CrownOfHorns
Monster
Leo
41
63
Goblin










Wyonearth
Monster
Virgo
75
48
Minotaur










Potgodtopdog
Male
Pisces
78
43
Samurai
Talk Skill
Earplug
Monster Talk
Move+3

Koutetsu Knife

Leather Helmet
Bronze Armor
Dracula Mantle

Asura, Bizen Boat, Heaven's Cloud, Kiyomori
Praise, Solution, Mimic Daravon, Refute, Rehabilitate

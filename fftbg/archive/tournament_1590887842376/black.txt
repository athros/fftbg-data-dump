Player: !Black
Team: Black Team
Palettes: Black/Red



ThePineappleSalesman
Female
Capricorn
80
52
Knight
Dance
Abandon
Equip Axe
Jump+2

Flail
Diamond Shield
Leather Helmet
Linen Cuirass
Bracer

Head Break, Speed Break
Witch Hunt, Slow Dance, Obsidian Blade, Dragon Pit



DashXero
Male
Cancer
68
76
Knight
White Magic
Catch
Defend
Retreat

Ragnarok
Buckler
Mythril Helmet
Crystal Mail
Reflect Ring

Shield Break, Weapon Break, Power Break
Cure 2, Raise 2, Reraise, Protect, Shell, Esuna



Zepharoth89
Monster
Pisces
51
57
Iron Hawk










Reddwind
Female
Gemini
51
70
Knight
Steal
Arrow Guard
Equip Axe
Jump+1

Giant Axe
Aegis Shield
Circlet
Plate Mail
Elf Mantle

Armor Break, Weapon Break, Magic Break, Speed Break, Justice Sword, Dark Sword, Night Sword
Steal Heart, Steal Shield, Steal Status

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



SSwing
Male
Cancer
53
75
Calculator
Demon Skill
Speed Save
Equip Armor
Jump+1

Dragon Rod

Platinum Helmet
Wizard Robe
Battle Boots

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



DeathTaxesAndAnime
Female
Sagittarius
61
80
Geomancer
Draw Out
Dragon Spirit
Magic Attack UP
Move+3

Murasame
Aegis Shield
Holy Miter
Power Sleeve
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Heaven's Cloud, Muramasa



Rislyeu
Male
Cancer
59
65
Ninja
Steal
Meatbone Slash
Beastmaster
Move-MP Up

Blind Knife
Mythril Knife
Leather Hat
Judo Outfit
Sprint Shoes

Shuriken
Steal Weapon, Steal Status, Leg Aim



Cloud92684
Female
Libra
46
58
Dancer
Yin Yang Magic
Distribute
Concentrate
Retreat

Cashmere

Headgear
Mystic Vest
Leather Mantle

Wiznaibus, Slow Dance, Disillusion, Last Dance, Void Storage
Blind, Poison, Silence Song, Confusion Song, Dispel Magic, Dark Holy

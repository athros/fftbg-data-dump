Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Leakimiko
Male
Taurus
53
43
Geomancer
Yin Yang Magic
HP Restore
Magic Attack UP
Ignore Height

Slasher
Flame Shield
Feather Hat
Silk Robe
Genji Gauntlet

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Life Drain, Pray Faith, Confusion Song, Dispel Magic



Segomod
Male
Scorpio
55
70
Chemist
Yin Yang Magic
Auto Potion
Sicken
Jump+2

Star Bag

Twist Headband
Clothes
Dracula Mantle

Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy
Blind, Doubt Faith, Blind Rage, Dispel Magic, Paralyze



OneHundredFists
Male
Libra
40
73
Mediator
Throw
Regenerator
Dual Wield
Teleport

Mage Masher
Dagger
Black Hood
Adaman Vest
N-Kai Armlet

Threaten, Preach, Death Sentence, Refute, Rehabilitate
Shuriken, Bomb



CrystalHeroXI
Female
Gemini
69
49
Calculator
Black Magic
Distribute
Equip Armor
Jump+2

Battle Folio

Crystal Helmet
Mythril Armor
Sprint Shoes

CT, Prime Number, 5, 3
Fire 2, Bolt 3, Bolt 4, Ice 2, Ice 3, Death

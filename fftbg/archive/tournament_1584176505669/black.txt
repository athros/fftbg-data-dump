Player: !Black
Team: Black Team
Palettes: Black/Red



KasugaiRoastedPeas
Female
Sagittarius
69
71
Calculator
White Magic
Damage Split
Equip Polearm
Move-HP Up

Battle Bamboo

Feather Hat
Mythril Vest
Genji Gauntlet

CT, Height, Prime Number, 5, 4
Cure, Cure 3, Raise, Raise 2, Protect, Protect 2, Wall, Esuna



HaateXIII
Male
Leo
56
76
Bard
Charge
Speed Save
Doublehand
Move+1

Silver Bow

Feather Hat
Adaman Vest
Elf Mantle

Cheer Song, Magic Song, Last Song, Hydra Pit
Charge+2



Bryan792
Male
Pisces
43
55
Lancer
Battle Skill
Speed Save
Dual Wield
Waterwalking

Cypress Rod
Spear
Platinum Helmet
Robe of Lords
Vanish Mantle

Level Jump5, Vertical Jump2
Head Break, Armor Break, Shield Break, Magic Break, Mind Break



DNazarok
Male
Taurus
63
76
Knight
Elemental
Parry
Defend
Move+1

Ancient Sword
Genji Shield
Cross Helmet
Gold Armor
Vanish Mantle

Magic Break, Speed Break, Dark Sword
Water Ball, Hell Ivy, Static Shock, Sand Storm, Blizzard, Lava Ball

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lowlf
Male
Cancer
74
77
Summoner
Yin Yang Magic
Regenerator
Sicken
Move+1

Poison Rod

Triangle Hat
Robe of Lords
Feather Boots

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Odin, Fairy, Lich, Cyclops
Blind, Spell Absorb, Foxbird, Sleep



Mesmaster
Male
Aquarius
80
42
Time Mage
Sing
Counter
Secret Hunt
Waterwalking

Healing Staff

Feather Hat
White Robe
Defense Armlet

Reflect, Stabilize Time
Angel Song, Magic Song, Sky Demon



SQUiDSQUARKLIN
Male
Libra
70
57
Ninja
White Magic
Arrow Guard
Equip Armor
Waterbreathing

Orichalcum
Air Knife
Circlet
Plate Mail
Genji Gauntlet

Shuriken, Bomb, Wand
Cure 3, Cure 4, Raise, Esuna



ThePineappleSalesman
Female
Sagittarius
44
63
Monk
Draw Out
Abandon
Attack UP
Swim



Green Beret
Leather Outfit
Battle Boots

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Revive, Seal Evil
Asura, Koutetsu

Player: !White
Team: White Team
Palettes: White/Blue



Sinnyil2
Male
Gemini
68
70
Monk
Draw Out
PA Save
Sicken
Jump+3



Leather Hat
Earth Clothes
Chantage

Pummel, Wave Fist, Purification, Revive, Seal Evil
Koutetsu, Murasame, Kikuichimoji



RongRongArts
Male
Aquarius
61
74
Chemist
Time Magic
Faith Up
Equip Bow
Retreat

Lightning Bow

Twist Headband
Black Costume
Sprint Shoes

Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Phoenix Down
Slow, Slow 2, Demi, Stabilize Time



Humble Fabio
Female
Pisces
43
40
Mediator
Basic Skill
PA Save
Equip Shield
Swim

Stone Gun
Mythril Shield
Leather Hat
Wizard Robe
Germinas Boots

Persuade, Praise, Preach, Death Sentence, Insult, Rehabilitate
Heal, Tickle, Ultima



Go2sleepTV
Female
Taurus
43
49
Geomancer
White Magic
Counter Flood
Halve MP
Move-MP Up

Ancient Sword
Crystal Shield
Barette
Chameleon Robe
Small Mantle

Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Cure 4, Raise, Reraise, Protect, Wall, Esuna, Holy

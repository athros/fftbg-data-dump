Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bad1dea
Male
Sagittarius
61
46
Calculator
Yin Yang Magic
PA Save
Defend
Jump+1

Battle Folio

Headgear
Power Sleeve
Elf Mantle

CT, Prime Number, 5, 4, 3
Poison, Life Drain, Doubt Faith, Silence Song, Foxbird, Dispel Magic



Basmal
Male
Sagittarius
44
60
Mime

Damage Split
Dual Wield
Ignore Terrain



Thief Hat
White Robe
Bracer

Mimic




Ko2q
Male
Libra
77
60
Archer
Talk Skill
MA Save
Doublehand
Teleport

Romanda Gun

Black Hood
Secret Clothes
Cursed Ring

Charge+3, Charge+5
Invitation, Persuade, Threaten, Death Sentence, Mimic Daravon, Refute, Rehabilitate



Lali Lulelo
Female
Leo
55
76
Summoner
Elemental
Faith Up
Short Charge
Move+2

Rod

Headgear
White Robe
Diamond Armlet

Moogle, Shiva, Golem, Carbunkle, Leviathan, Silf, Fairy, Lich, Cyclops
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball

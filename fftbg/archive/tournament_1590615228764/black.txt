Player: !Black
Team: Black Team
Palettes: Black/Red



Error72
Male
Virgo
50
43
Archer
Time Magic
Brave Save
Doublehand
Lava Walking

Night Killer

Holy Miter
Mythril Vest
Defense Ring

Charge+20
Haste, Haste 2, Slow 2, Stop, Reflect, Stabilize Time, Meteor



Lowlf
Female
Cancer
77
78
Chemist
Draw Out
Speed Save
Attack UP
Move+2

Dagger

Holy Miter
Judo Outfit
Spike Shoes

Eye Drop, Soft, Holy Water, Remedy, Phoenix Down
Asura, Koutetsu, Murasame, Muramasa



GrayGhostGaming
Female
Gemini
58
50
Chemist
Yin Yang Magic
Brave Save
Long Status
Move+1

Star Bag

Green Beret
Mystic Vest
Rubber Shoes

X-Potion, Ether, Elixir, Antidote, Echo Grass, Holy Water, Remedy, Phoenix Down
Poison, Spell Absorb, Dispel Magic, Paralyze, Sleep, Petrify



Sociallubricant1
Female
Aries
73
74
Ninja
Punch Art
Distribute
Equip Gun
Teleport

Papyrus Codex
Ramia Harp
Twist Headband
Mythril Vest
Diamond Armlet

Shuriken, Knife, Staff, Spear
Earth Slash, Chakra, Seal Evil

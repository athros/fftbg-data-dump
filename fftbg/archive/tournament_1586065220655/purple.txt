Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



LuckyLuckLuc2
Female
Taurus
62
79
Priest
Steal
Arrow Guard
Maintenance
Jump+2

Morning Star

Twist Headband
Wizard Outfit
108 Gems

Cure 2, Cure 4, Raise, Raise 2, Protect, Protect 2, Esuna, Holy
Steal Armor, Steal Weapon, Steal Status, Arm Aim



Mirapoix
Female
Scorpio
59
78
Priest
Summon Magic
MP Restore
Martial Arts
Fly



Black Hood
Black Robe
Elf Mantle

Cure, Cure 2, Cure 3, Raise, Raise 2, Protect, Protect 2, Wall, Esuna
Ramuh, Carbunkle, Silf



E7bbk
Female
Taurus
46
77
Wizard
Basic Skill
Caution
Short Charge
Waterwalking

Poison Rod

Flash Hat
Black Robe
Dracula Mantle

Fire, Fire 2, Ice 2, Ice 3, Ice 4
Dash, Heal, Tickle, Yell, Wish



Technominari
Female
Sagittarius
79
80
Priest
Talk Skill
Counter Magic
Short Charge
Lava Walking

Rainbow Staff

Black Hood
Light Robe
Dracula Mantle

Cure 3, Raise, Reraise, Regen, Protect, Protect 2, Shell, Wall, Esuna, Holy
Persuade, Preach, Negotiate, Mimic Daravon, Refute

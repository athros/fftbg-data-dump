Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Sinnyil2
Female
Leo
51
55
Dancer
Charge
MA Save
Dual Wield
Lava Walking

Cashmere
Cashmere
Red Hood
Light Robe
Feather Mantle

Disillusion, Void Storage, Nether Demon
Charge+1, Charge+7



YaBoy125
Female
Libra
62
80
Summoner
Yin Yang Magic
Auto Potion
Short Charge
Jump+1

Thunder Rod

Holy Miter
White Robe
108 Gems

Shiva, Ifrit, Golem, Carbunkle, Bahamut, Odin
Life Drain, Pray Faith, Zombie, Blind Rage, Confusion Song, Dispel Magic



SSwing
Male
Virgo
59
46
Lancer
Black Magic
Damage Split
Dual Wield
Move+2

Musk Rod
Whale Whisker
Bronze Helmet
Platinum Armor
Battle Boots

Level Jump3, Vertical Jump8
Fire 2, Fire 4, Bolt 4, Ice, Ice 2, Ice 3, Flare



VynxYukida
Female
Taurus
45
46
Time Mage
Charge
Absorb Used MP
Martial Arts
Move+1

Iron Fan

Cachusha
Mythril Vest
Bracer

Haste 2, Slow, Float, Demi, Demi 2, Stabilize Time
Charge+1, Charge+7

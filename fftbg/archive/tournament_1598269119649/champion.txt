Player: !zChamp
Team: Champion Team
Palettes: Green/White



DaveStrider55
Monster
Sagittarius
80
67
Juravis










Ruleof5
Female
Virgo
46
71
Archer
Steal
Abandon
Long Status
Move+3

Hunting Bow
Gold Shield
Platinum Helmet
Chain Vest
Leather Mantle

Charge+1, Charge+3, Charge+4, Charge+5
Steal Heart, Steal Helmet, Arm Aim



ProteinBiscuit
Monster
Leo
54
58
Ghoul










WhiteTigress
Male
Libra
63
72
Archer
Basic Skill
Damage Split
Doublehand
Move-HP Up

Silver Bow

Crystal Helmet
Black Costume
Sprint Shoes

Charge+2, Charge+7
Heal, Tickle, Cheer Up, Wish

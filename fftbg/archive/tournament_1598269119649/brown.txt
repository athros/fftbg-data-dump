Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HASTERIOUS
Male
Leo
80
55
Lancer
Punch Art
Dragon Spirit
Attack UP
Lava Walking

Holy Lance
Buckler
Gold Helmet
Carabini Mail
Spike Shoes

Level Jump4, Vertical Jump8
Spin Fist, Secret Fist, Purification, Revive



Phi Sig
Female
Sagittarius
50
38
Geomancer
Draw Out
Hamedo
Equip Knife
Move-MP Up

Sasuke Knife
Genji Shield
Holy Miter
Mythril Vest
N-Kai Armlet

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Asura, Kiyomori



CosmicTactician
Female
Pisces
42
67
Ninja
Black Magic
Mana Shield
Equip Gun
Teleport

Madlemgen
Bestiary
Ribbon
Black Costume
Sprint Shoes

Shuriken, Bomb, Stick, Dictionary
Fire, Bolt, Ice 3, Frog



DeathTaxesAndAnime
Female
Taurus
51
53
Mediator
Draw Out
MA Save
Sicken
Waterwalking

Stone Gun

Leather Hat
Brigandine
Magic Gauntlet

Invitation, Insult, Mimic Daravon, Rehabilitate
Heaven's Cloud, Kiyomori, Muramasa

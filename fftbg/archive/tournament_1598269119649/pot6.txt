Final Bets: white - 11 bets for 2,281G (32.8%, x2.05); purple - 7 bets for 4,672G (67.2%, x0.49)

white bets:
LAGBOT30000: 500G (21.9%, 11,071G)
BirbBrainsBot: 342G (15.0%, 342G)
gongonono: 300G (13.2%, 9,708G)
kliffw: 232G (10.2%, 14,283G)
Chihuahua_Charity: 220G (9.6%, 220G)
datadrivenbot: 200G (8.8%, 67,531G)
Lydian_C: 123G (5.4%, 1,813G)
ArashiKurobara: 100G (4.4%, 1,330G)
FoxMime: 100G (4.4%, 378G)
MemoriesofFinal: 100G (4.4%, 787G)
getthemoneyz: 64G (2.8%, 1,733,939G)

purple bets:
skipsandwiches: 2,224G (47.6%, 2,224G)
dogsandcatsand: 846G (18.1%, 846G)
NIghtdew14: 657G (14.1%, 657G)
pplvee1: 520G (11.1%, 8,692G)
Mushufasa_: 224G (4.8%, 224G)
gorgewall: 101G (2.2%, 22,345G)
AllInBot: 100G (2.1%, 100G)

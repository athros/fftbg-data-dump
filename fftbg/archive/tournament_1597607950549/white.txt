Player: !White
Team: White Team
Palettes: White/Blue



Reinoe
Female
Pisces
53
61
Wizard
Yin Yang Magic
Regenerator
Short Charge
Fly

Zorlin Shape

Black Hood
Silk Robe
Rubber Shoes

Fire 2, Bolt, Bolt 2, Bolt 3
Life Drain, Blind Rage, Foxbird, Dispel Magic



DustBirdEX
Female
Libra
46
66
Knight
Charge
Regenerator
Long Status
Jump+2

Slasher
Round Shield
Barbuta
Carabini Mail
Small Mantle

Weapon Break, Speed Break, Justice Sword, Dark Sword
Charge+3, Charge+4



Toka222
Female
Capricorn
77
42
Samurai
Charge
Caution
Equip Bow
Waterwalking

Silver Bow

Crystal Helmet
Chameleon Robe
Magic Gauntlet

Bizen Boat
Charge+1



ThreeMileIsland
Male
Libra
52
74
Thief
Elemental
Counter Tackle
Beastmaster
Retreat

Platinum Sword

Twist Headband
Judo Outfit
Cursed Ring

Steal Heart, Steal Armor, Steal Weapon, Leg Aim
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp

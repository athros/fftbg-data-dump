Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Scuba Steve3
Male
Capricorn
51
61
Samurai
Throw
Mana Shield
Doublehand
Ignore Height

Muramasa

Bronze Helmet
Crystal Mail
Leather Mantle

Koutetsu, Kikuichimoji
Shuriken, Staff, Dictionary



Forkmore
Male
Gemini
61
40
Knight
Yin Yang Magic
HP Restore
Beastmaster
Move+2

Chaos Blade
Buckler
Grand Helmet
Silk Robe
Magic Gauntlet

Head Break, Shield Break, Power Break, Dark Sword
Blind, Life Drain, Silence Song, Foxbird, Dispel Magic, Sleep



Tarheels218
Female
Aquarius
74
74
Wizard
Item
Damage Split
Throw Item
Lava Walking

Zorlin Shape

Holy Miter
Black Costume
Diamond Armlet

Bolt 4, Ice, Flare
Potion, X-Potion, Hi-Ether, Eye Drop, Phoenix Down



Just Here2
Female
Pisces
65
77
Summoner
Dance
MP Restore
Defense UP
Jump+2

Papyrus Codex

Feather Hat
Clothes
Setiemson

Moogle, Ifrit, Golem, Carbunkle, Salamander
Witch Hunt, Wiznaibus, Void Storage

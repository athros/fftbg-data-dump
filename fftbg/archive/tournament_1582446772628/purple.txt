Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Plutonium Jones
Female
Scorpio
63
74
Samurai
Item
Dragon Spirit
Halve MP
Ignore Terrain

Murasame

Diamond Helmet
Black Robe
Red Shoes

Murasame, Heaven's Cloud
Potion, Hi-Potion, X-Potion, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down



Waterwatereverywhere
Female
Aquarius
64
43
Monk
Elemental
HP Restore
Equip Knife
Jump+2

Assassin Dagger

Green Beret
Clothes
Spike Shoes

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Water Ball, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Draakaa
Female
Cancer
62
68
Samurai
Summon Magic
Meatbone Slash
Magic Attack UP
Move-MP Up

Muramasa

Leather Helmet
Gold Armor
Bracer

Asura, Koutetsu, Heaven's Cloud, Kiyomori
Moogle, Ifrit, Golem, Carbunkle, Silf, Fairy



Fspll
Male
Cancer
71
42
Knight
Basic Skill
MP Restore
Magic Attack UP
Jump+2

Battle Axe
Flame Shield
Platinum Helmet
Chain Mail
Genji Gauntlet

Head Break, Power Break, Mind Break, Stasis Sword
Heal, Tickle, Cheer Up

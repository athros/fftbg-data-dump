Player: !Green
Team: Green Team
Palettes: Green/White



EchoBearHands
Female
Sagittarius
69
78
Ninja
Charge
Earplug
Equip Gun
Fly

Bestiary
Papyrus Codex
Green Beret
Chain Vest
Battle Boots

Bomb, Wand
Charge+3, Charge+5



Wizard 01
Male
Virgo
73
74
Squire
Summon Magic
Parry
Long Status
Retreat

Mythril Knife
Diamond Shield
Green Beret
Bronze Armor
Setiemson

Accumulate, Dash, Heal, Tickle, Yell
Moogle, Shiva, Ramuh, Ifrit, Salamander, Fairy



Twelfthrootoftwo
Male
Leo
42
58
Priest
Punch Art
Earplug
Dual Wield
Move-MP Up

Gold Staff
White Staff
Triangle Hat
Silk Robe
Sprint Shoes

Cure 2, Cure 3, Cure 4, Reraise, Regen, Protect, Shell 2, Wall
Pummel, Wave Fist, Purification, Revive



Ominnous
Female
Gemini
72
55
Knight
Black Magic
Meatbone Slash
Equip Armor
Waterwalking

Save the Queen

Feather Hat
White Robe
Angel Ring

Head Break, Speed Break, Power Break, Mind Break, Justice Sword
Fire, Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Ice 3, Ice 4

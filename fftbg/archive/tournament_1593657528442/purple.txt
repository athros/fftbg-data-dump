Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ZombiFrancis
Female
Scorpio
45
75
Chemist
Summon Magic
PA Save
Equip Shield
Retreat

Cultist Dagger
Flame Shield
Twist Headband
Earth Clothes
Small Mantle

X-Potion, Ether, Antidote, Maiden's Kiss, Soft, Phoenix Down
Moogle, Golem, Leviathan, Lich



HuffFlex
Male
Leo
46
76
Mediator
Sing
Counter Magic
Halve MP
Jump+1

Bestiary

Barette
Black Costume
Magic Gauntlet

Threaten, Death Sentence, Insult, Negotiate, Mimic Daravon, Rehabilitate
Life Song, Battle Song, Space Storage



OpHendoslice
Male
Libra
69
59
Squire
Sing
Counter
Dual Wield
Move+3

Sleep Sword
Main Gauche
Bronze Helmet
Mystic Vest
Spike Shoes

Heal, Fury, Scream
Angel Song, Battle Song, Diamond Blade



Kronikle
Male
Libra
66
68
Archer
Item
Faith Save
Throw Item
Fly

Blast Gun
Diamond Shield
Green Beret
Judo Outfit
Defense Armlet

Charge+2, Charge+3, Charge+20
Potion, X-Potion, Ether, Antidote, Soft, Remedy, Phoenix Down

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Eltaggerung
Male
Aquarius
56
49
Thief
Item
Damage Split
Dual Wield
Ignore Height

Sasuke Knife
Air Knife
Diamond Helmet
Clothes
Power Wrist

Steal Helmet, Steal Armor, Steal Status, Leg Aim
X-Potion, Echo Grass, Soft, Phoenix Down



Denamda
Male
Aquarius
65
60
Chemist
White Magic
HP Restore
Sicken
Jump+3

Panther Bag

Feather Hat
Mystic Vest
Defense Armlet

Potion, Hi-Potion, X-Potion, Ether, Hi-Ether, Maiden's Kiss, Soft, Remedy, Phoenix Down
Cure, Cure 3, Raise, Protect, Shell, Esuna, Holy



Laserman1000
Male
Aries
72
73
Ninja
Charge
Critical Quick
Equip Gun
Swim

Battle Folio
Ramia Harp
Thief Hat
Earth Clothes
Reflect Ring

Shuriken, Bomb
Charge+2



Sinnyil2
Female
Gemini
60
44
Samurai
Steal
Hamedo
Equip Axe
Lava Walking

Flail

Mythril Helmet
Linen Robe
Magic Gauntlet

Koutetsu, Murasame, Heaven's Cloud
Steal Heart, Steal Armor, Steal Weapon, Steal Accessory

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Zetchryn
Monster
Aries
79
71
Holy Dragon










DouglasDragonThePoet
Male
Aquarius
49
66
Squire
Jump
Counter Flood
Concentrate
Ignore Height

Air Knife
Flame Shield
Flash Hat
Secret Clothes
Jade Armlet

Dash, Cheer Up, Wish
Level Jump4, Vertical Jump8



CT 5 Holy
Male
Scorpio
52
68
Bard
White Magic
Faith Save
Short Status
Move+2

Long Bow

Holy Miter
Carabini Mail
Bracer

Angel Song, Battle Song, Magic Song, Hydra Pit
Cure, Cure 3, Raise, Shell, Wall



Humphrey
Male
Pisces
77
43
Archer
Black Magic
Dragon Spirit
Equip Sword
Teleport

Defender

Golden Hairpin
Leather Outfit
Dracula Mantle

Charge+1, Charge+4, Charge+10
Bolt 3, Bolt 4, Ice, Ice 4, Flare

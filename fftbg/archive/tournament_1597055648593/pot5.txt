Final Bets: blue - 6 bets for 5,201G (51.3%, x0.95); green - 7 bets for 4,944G (48.7%, x1.05)

blue bets:
SkyridgeZero: 1,500G (28.8%, 33,713G)
ko2q: 1,249G (24.0%, 1,249G)
Kysune: 1,100G (21.1%, 1,100G)
randgridr: 600G (11.5%, 600G)
DouglasDragonThePoet: 552G (10.6%, 552G)
datadrivenbot: 200G (3.8%, 53,731G)

green bets:
reinoe: 2,000G (40.5%, 170,843G)
douchetron: 1,182G (23.9%, 2,319G)
BirbBrainsBot: 1,000G (20.2%, 167,387G)
AllInBot: 361G (7.3%, 361G)
foxfaeez: 200G (4.0%, 784G)
gorgewall: 101G (2.0%, 5,260G)
MinBetBot: 100G (2.0%, 21,048G)

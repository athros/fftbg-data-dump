Final Bets: red - 21 bets for 14,831G (81.2%, x0.23); yellow - 11 bets for 3,434G (18.8%, x4.32)

red bets:
HorusTaurus: 3,000G (20.2%, 5,388G)
mpghappiness: 2,472G (16.7%, 2,472G)
killth3kid: 2,000G (13.5%, 83,125G)
Grimmace45: 1,201G (8.1%, 6,009G)
BirbBrainsBot: 1,000G (6.7%, 75,709G)
JumbocactuarX27: 1,000G (6.7%, 9,941G)
proper__noun__: 1,000G (6.7%, 11,157G)
CapnChaos12: 500G (3.4%, 47,540G)
ungabunga_bot: 378G (2.5%, 380,799G)
alphara2: 350G (2.4%, 713G)
hydroshade: 300G (2.0%, 5,164G)
Faplo: 250G (1.7%, 725G)
VeolaDrum: 250G (1.7%, 953G)
duckfist: 244G (1.6%, 244G)
MrMarkPolo: 200G (1.3%, 1,705G)
amhamor: 143G (1.0%, 143G)
caprinovoa: 143G (1.0%, 143G)
datadrivenbot: 100G (0.7%, 11,958G)
CosmicTactician: 100G (0.7%, 25,117G)
bahamutlagooon: 100G (0.7%, 1,048G)
KonzeraLive: 100G (0.7%, 1,710G)

yellow bets:
KasugaiRoastedPeas: 1,000G (29.1%, 5,027G)
DavenIII: 600G (17.5%, 600G)
TheChainNerd: 428G (12.5%, 428G)
Cryptopsy70: 396G (11.5%, 17,567G)
FNCardascia_: 314G (9.1%, 314G)
getthemoneyz: 204G (5.9%, 633,205G)
VarkilTV: 126G (3.7%, 126G)
mannequ1n: 100G (2.9%, 441G)
nifboy: 100G (2.9%, 5,977G)
RestIessNight: 100G (2.9%, 939G)
ZephyrTempest: 66G (1.9%, 1,333G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Toksu
Female
Gemini
78
53
Ninja
Black Magic
Speed Save
Equip Sword
Jump+1

Asura Knife
Platinum Sword
Triangle Hat
Judo Outfit
Diamond Armlet

Shuriken, Ninja Sword
Fire 4, Bolt 4, Ice, Ice 3



LOKITHUS
Female
Libra
76
55
Squire
Time Magic
Speed Save
Equip Axe
Move+3

Giant Axe

Red Hood
Diamond Armor
Salty Rage

Throw Stone, Heal, Yell, Cheer Up
Haste, Reflect, Demi, Stabilize Time, Meteor



DudeMonkey77
Female
Pisces
56
64
Dancer
White Magic
Sunken State
Equip Knife
Ignore Terrain

Main Gauche

Golden Hairpin
Linen Robe
N-Kai Armlet

Last Dance, Obsidian Blade, Dragon Pit
Cure, Cure 2, Cure 3, Raise, Protect 2, Shell, Wall, Esuna



Wizard 01
Male
Sagittarius
52
50
Knight
Elemental
Parry
Magic Defense UP
Ignore Terrain

Platinum Sword
Mythril Shield
Mythril Helmet
Reflect Mail
Setiemson

Armor Break, Magic Break, Stasis Sword
Pitfall, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

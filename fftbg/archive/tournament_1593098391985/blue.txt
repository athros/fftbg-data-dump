Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Holyonline
Female
Gemini
66
79
Chemist
Dance
Counter
Attack UP
Jump+2

Cute Bag

Headgear
Judo Outfit
Battle Boots

Potion, Hi-Potion, X-Potion, Hi-Ether, Remedy, Phoenix Down
Wiznaibus, Obsidian Blade, Void Storage, Dragon Pit



Belkra
Monster
Leo
66
67
Red Panther










Red Lancer
Male
Taurus
79
44
Ninja
Sing
Counter
Secret Hunt
Waterwalking

Morning Star
Assassin Dagger
Golden Hairpin
Earth Clothes
Elf Mantle

Wand
Cheer Song, Battle Song, Sky Demon



FriendSquirrel
Male
Scorpio
50
80
Squire
Steal
Parry
Martial Arts
Waterwalking


Crystal Shield
Golden Hairpin
Brigandine
Magic Ring

Accumulate, Dash, Throw Stone, Heal, Scream
Gil Taking, Steal Heart, Steal Armor

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Odym82
Female
Taurus
47
63
Summoner
Item
Brave Save
Equip Bow
Waterwalking

Poison Bow

Golden Hairpin
Power Sleeve
Defense Armlet

Shiva, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Salamander
Potion, Hi-Ether, Holy Water, Phoenix Down



Killth3kid
Male
Cancer
41
64
Chemist
Elemental
Absorb Used MP
Halve MP
Fly

Dagger

Green Beret
Mystic Vest
Angel Ring

Hi-Ether, Eye Drop, Soft, Phoenix Down
Sand Storm



AniZero
Monster
Aries
61
58
Revenant










PoroTact
Female
Gemini
66
77
Wizard
Dance
PA Save
Equip Bow
Swim

Snipe Bow

Headgear
Chain Vest
Red Shoes

Fire 3, Fire 4, Bolt 2, Bolt 4, Flare
Disillusion, Last Dance, Obsidian Blade, Dragon Pit

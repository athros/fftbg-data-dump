Player: !Red
Team: Red Team
Palettes: Red/Brown



Ruebyy
Male
Libra
77
39
Mime

Absorb Used MP
Magic Attack UP
Retreat



Green Beret
Black Costume
Feather Boots

Mimic




Digitalsocrates
Male
Pisces
65
74
Samurai
Yin Yang Magic
PA Save
Equip Axe
Levitate

Slasher

Barbuta
Gold Armor
Salty Rage

Koutetsu, Bizen Boat, Murasame
Spell Absorb, Silence Song, Foxbird, Sleep



SkylerBunny
Male
Taurus
77
54
Squire
Time Magic
MP Restore
Sicken
Swim

Battle Axe
Mythril Shield
Golden Hairpin
Genji Armor
Bracer

Dash, Heal, Cheer Up
Haste, Stabilize Time



Flameatron
Male
Aquarius
63
49
Mediator
Sing
Sunken State
Magic Defense UP
Lava Walking

Blast Gun

Thief Hat
Linen Robe
Magic Ring

Invitation, Preach, Solution, Death Sentence, Mimic Daravon, Refute
Life Song, Magic Song, Sky Demon

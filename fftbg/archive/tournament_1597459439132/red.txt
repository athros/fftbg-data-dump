Player: !Red
Team: Red Team
Palettes: Red/Brown



Lord Gwarth
Monster
Leo
78
65
Archaic Demon










OneHundredFists
Male
Leo
52
42
Ninja
Time Magic
Counter Flood
Martial Arts
Jump+2



Black Hood
Brigandine
Magic Gauntlet

Shuriken, Knife
Haste, Slow 2, Demi, Stabilize Time



Thyrandaal
Male
Leo
53
51
Ninja
Jump
MA Save
Magic Defense UP
Waterbreathing

Cultist Dagger
Mage Masher
Leather Hat
Rubber Costume
Diamond Armlet

Shuriken, Bomb, Knife, Ninja Sword, Wand
Level Jump5, Vertical Jump5



ThreeMileIsland
Female
Cancer
80
45
Oracle
Elemental
Parry
Equip Axe
Move+3

Scorpion Tail

Feather Hat
Mythril Vest
Red Shoes

Silence Song, Foxbird, Dispel Magic, Paralyze
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard

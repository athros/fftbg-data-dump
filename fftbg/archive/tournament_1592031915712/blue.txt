Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zmoses
Female
Taurus
57
52
Calculator
Time Magic
Faith Save
Equip Sword
Jump+3

Defender

Twist Headband
Chameleon Robe
108 Gems

CT, Height, Prime Number
Haste, Stop, Float, Quick, Demi, Stabilize Time



Reinoe
Female
Pisces
77
64
Priest
Dance
Arrow Guard
Short Status
Move-MP Up

Flail

Leather Hat
White Robe
Power Wrist

Cure, Cure 2, Cure 4, Reraise, Protect, Shell, Esuna, Holy
Nameless Dance, Nether Demon, Dragon Pit



HaychDub
Female
Pisces
67
78
Mediator
Draw Out
Parry
Dual Wield
Move+2

Romanda Gun
Glacier Gun
Green Beret
Silk Robe
Magic Ring

Praise, Threaten, Preach, Death Sentence, Refute
Bizen Boat



Maicovisky
Female
Aries
63
55
Wizard
Draw Out
Catch
Equip Polearm
Jump+2

Cypress Rod

Leather Hat
White Robe
Feather Boots

Fire, Bolt, Bolt 2, Ice, Ice 2, Flare
Koutetsu, Heaven's Cloud

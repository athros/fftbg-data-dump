Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DAC169
Male
Taurus
54
47
Thief
Sing
HP Restore
Defend
Fly

Sasuke Knife

Thief Hat
Chain Vest
Diamond Armlet

Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Leg Aim
Angel Song, Battle Song, Magic Song, Sky Demon



Nok
Male
Libra
76
80
Ninja
Elemental
Brave Save
Defend
Jump+3

Mythril Knife
Flame Whip
Red Hood
Wizard Outfit
Power Wrist

Shuriken, Sword, Dictionary
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind



L2 Sentinel
Male
Leo
61
46
Lancer
Throw
Sunken State
Equip Armor
Move+3

Holy Lance
Aegis Shield
Black Hood
Reflect Mail
Reflect Ring

Level Jump3, Vertical Jump5
Shuriken, Bomb



ApplesauceBoss
Female
Gemini
56
61
Ninja
Dance
Distribute
Equip Gun
Fly

Mythril Gun
Mythril Gun
Black Hood
Chain Vest
Bracer

Knife, Dictionary
Witch Hunt, Last Dance, Void Storage

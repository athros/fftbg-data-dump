Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Scorpio
56
68
Knight
Talk Skill
PA Save
Halve MP
Jump+1

Mythril Sword
Gold Shield
Genji Helmet
White Robe
Leather Mantle

Magic Break, Power Break, Mind Break, Stasis Sword, Dark Sword, Night Sword
Mimic Daravon



TeaTime29
Male
Sagittarius
49
53
Mediator
Time Magic
Caution
Halve MP
Jump+2

Bestiary

Black Hood
Leather Outfit
N-Kai Armlet

Invitation, Praise, Threaten, Negotiate, Refute, Rehabilitate
Haste, Slow 2, Float, Reflect, Demi, Demi 2, Meteor



CrownOfHorns
Female
Pisces
76
77
Calculator
Time Magic
Mana Shield
Beastmaster
Move+3

Ice Rod

Thief Hat
Wizard Robe
Reflect Ring

CT, Height, Prime Number, 5, 4
Haste, Haste 2, Slow, Stop, Quick, Demi 2, Stabilize Time



Solomongrundy85
Female
Pisces
54
45
Samurai
Throw
Counter Flood
Magic Attack UP
Move+3

Muramasa

Leather Helmet
Bronze Armor
Elf Mantle

Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori
Knife, Staff, Wand

Final Bets: white - 16 bets for 20,922G (74.6%, x0.34); purple - 13 bets for 7,131G (25.4%, x2.93)

white bets:
Zeroroute: 8,379G (40.0%, 14,209G)
NicoSavoy: 5,000G (23.9%, 86,737G)
volgrathemoose: 1,501G (7.2%, 7,636G)
BirbBrainsBot: 1,000G (4.8%, 178,102G)
HaplessOne: 888G (4.2%, 274,397G)
FoeSquirrel: 856G (4.1%, 856G)
roofiepops: 600G (2.9%, 600G)
killth3kid: 500G (2.4%, 15,894G)
MADTheta: 500G (2.4%, 5,216G)
EnemyController: 500G (2.4%, 333,828G)
luminarii: 450G (2.2%, 18,360G)
Drusic: 248G (1.2%, 248G)
letdowncity: 200G (1.0%, 5,103G)
datadrivenbot: 100G (0.5%, 19,603G)
maakur_: 100G (0.5%, 4,220G)
Saldarin: 100G (0.5%, 4,760G)

purple bets:
Candina: 2,000G (28.0%, 20,623G)
DeathTaxesAndAnime: 1,170G (16.4%, 1,170G)
Theseawolf1: 1,000G (14.0%, 50,226G)
Maldoree: 576G (8.1%, 576G)
getthemoneyz: 526G (7.4%, 724,596G)
skillomono: 500G (7.0%, 14,681G)
Lanshaft: 424G (5.9%, 424G)
ANFz: 380G (5.3%, 380G)
atlatlchocolate: 250G (3.5%, 1,000G)
AllInBot: 100G (1.4%, 100G)
CorpusCav: 100G (1.4%, 8,136G)
Evewho: 100G (1.4%, 1,768G)
moonliquor: 5G (0.1%, 1,541G)

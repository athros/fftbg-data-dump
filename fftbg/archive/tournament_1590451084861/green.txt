Player: !Green
Team: Green Team
Palettes: Green/White



Evewho
Female
Aquarius
60
77
Chemist
Talk Skill
Parry
Defense UP
Jump+2

Mythril Knife

Red Hood
Mystic Vest
Defense Ring

Potion, Hi-Potion, X-Potion, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Threaten, Death Sentence, Mimic Daravon, Refute



Zeroroute
Male
Sagittarius
79
60
Time Mage
Black Magic
Meatbone Slash
Magic Defense UP
Lava Walking

Sage Staff

Leather Hat
Wizard Robe
Spike Shoes

Immobilize, Reflect, Quick, Demi, Demi 2, Stabilize Time
Fire, Fire 3, Fire 4, Bolt, Empower



Grimmace45
Male
Scorpio
62
78
Wizard
Time Magic
Hamedo
Equip Bow
Ignore Height

Hunting Bow

Red Hood
Linen Robe
Bracer

Fire, Fire 4, Bolt, Bolt 4, Ice 3, Ice 4, Frog, Flare
Haste, Stop, Reflect, Demi, Stabilize Time



Gorgewall
Male
Aries
66
51
Bard
Draw Out
Arrow Guard
Concentrate
Teleport

Fairy Harp

Headgear
Black Costume
Magic Gauntlet

Life Song, Magic Song, Nameless Song, Last Song
Asura, Murasame, Kikuichimoji

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



CosmicTactician
Female
Aquarius
47
79
Wizard
Time Magic
Counter Tackle
Magic Attack UP
Teleport 2

Thunder Rod

Triangle Hat
Judo Outfit
Red Shoes

Fire 2, Fire 3, Bolt 3, Ice 2, Empower
Haste, Slow, Stop, Immobilize, Demi



Nifboy
Male
Serpentarius
68
76
Chemist
Throw
Parry
Secret Hunt
Ignore Terrain

Romanda Gun

Triangle Hat
Mystic Vest
Jade Armlet

Potion, Hi-Potion, Ether, Antidote, Maiden's Kiss, Phoenix Down
Shuriken, Bomb



SephDarkheart
Male
Scorpio
52
67
Wizard
Yin Yang Magic
Meatbone Slash
Short Charge
Waterbreathing

Wizard Rod

Leather Hat
Wizard Robe
N-Kai Armlet

Fire 2, Bolt 2, Bolt 4, Ice 2, Empower
Blind, Spell Absorb, Pray Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic



Evdoggity
Female
Serpentarius
44
66
Time Mage
Draw Out
Regenerator
Equip Bow
Jump+2

Poison Bow

Leather Hat
Linen Robe
Bracer

Haste, Haste 2, Stabilize Time
Asura, Koutetsu, Kiyomori, Kikuichimoji

Player: !Black
Team: Black Team
Palettes: Black/Red



Feffle
Male
Leo
77
42
Thief
Black Magic
Meatbone Slash
Dual Wield
Levitate

Spell Edge
Orichalcum
Flash Hat
Power Sleeve
Battle Boots

Steal Heart, Steal Accessory
Bolt 3, Ice 2, Death



Draaaan
Female
Gemini
53
49
Lancer
Time Magic
Parry
Maintenance
Ignore Terrain

Cypress Rod
Buckler
Mythril Helmet
Chameleon Robe
Jade Armlet

Level Jump8, Vertical Jump8
Slow 2, Stop, Immobilize, Quick, Demi 2, Stabilize Time



Marblescrew
Male
Sagittarius
80
64
Monk
Talk Skill
Counter
Equip Gun
Move-MP Up

Ramia Harp

Feather Hat
Leather Vest
Wizard Mantle

Spin Fist, Pummel, Wave Fist, Purification, Revive
Insult, Refute



PleXmito
Monster
Aquarius
72
40
Archaic Demon








Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



OoTrillionoO
Monster
Leo
63
52
Cockatrice










Zmoses
Female
Leo
57
72
Time Mage
Charge
Faith Up
Beastmaster
Teleport

White Staff

Ribbon
Light Robe
Genji Gauntlet

Haste, Immobilize, Float, Reflect, Stabilize Time, Meteor
Charge+1, Charge+3, Charge+5, Charge+20



TempestuousT
Female
Leo
77
51
Knight
Punch Art
Sunken State
Maintenance
Jump+3

Save the Queen

Iron Helmet
Mythril Armor
Bracer

Head Break, Speed Break, Power Break
Spin Fist, Earth Slash, Revive, Seal Evil



Finalagon
Female
Pisces
74
47
Calculator
Time Magic
Arrow Guard
Maintenance
Jump+1

Papyrus Codex

Red Hood
Light Robe
Red Shoes

Height, 5
Stop, Immobilize, Reflect, Quick, Stabilize Time

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Ranmilia
Male
Virgo
50
58
Monk
Throw
Absorb Used MP
Sicken
Lava Walking



Triangle Hat
Adaman Vest
Rubber Shoes

Pummel, Earth Slash, Purification, Revive
Shuriken



Toka222
Male
Scorpio
70
69
Monk
Battle Skill
Auto Potion
Equip Sword
Move+1

Iron Sword

Holy Miter
Wizard Outfit
Jade Armlet

Spin Fist, Pummel, Secret Fist, Purification, Chakra
Head Break, Armor Break, Shield Break, Power Break, Justice Sword, Night Sword



Snkey
Female
Capricorn
57
43
Dancer
Charge
Caution
Dual Wield
Move+1

Cashmere
Ryozan Silk
Black Hood
Silk Robe
Vanish Mantle

Witch Hunt, Wiznaibus, Obsidian Blade, Void Storage
Charge+3, Charge+4, Charge+10



ManaBirb
Male
Libra
68
69
Knight
Sing
Speed Save
Equip Armor
Move-MP Up

Coral Sword
Crystal Shield
Golden Hairpin
Brigandine
Sprint Shoes

Head Break, Armor Break, Magic Break, Mind Break, Stasis Sword, Dark Sword
Life Song, Cheer Song, Sky Demon

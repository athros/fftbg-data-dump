Player: !Green
Team: Green Team
Palettes: Green/White



Gelwain
Male
Virgo
65
41
Squire
Throw
Dragon Spirit
Equip Knife
Jump+1

Sasuke Knife
Mythril Shield
Thief Hat
Platinum Armor
Reflect Ring

Dash, Heal, Tickle, Cheer Up, Fury
Shuriken, Bomb, Staff



Jordache7K
Male
Sagittarius
47
65
Chemist
Jump
Catch
Halve MP
Waterwalking

Cute Bag

Leather Hat
Leather Outfit
Diamond Armlet

Potion, Hi-Potion, X-Potion, Eye Drop, Maiden's Kiss, Phoenix Down
Level Jump5, Vertical Jump8



Ominnous
Female
Aries
70
59
Samurai
Item
Distribute
Throw Item
Move+3

Muramasa

Leather Helmet
Carabini Mail
Germinas Boots

Bizen Boat, Kiyomori, Kikuichimoji
Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Soft, Holy Water, Phoenix Down



TrueRhyme
Male
Virgo
79
45
Lancer
Summon Magic
Counter
Equip Bow
Move-MP Up

Night Killer
Kaiser Plate
Genji Helmet
Leather Armor
Feather Mantle

Level Jump8, Vertical Jump8
Moogle, Shiva, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Fairy, Cyclops

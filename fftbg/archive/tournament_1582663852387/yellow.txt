Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Maicovisky
Female
Aries
40
60
Archer
Summon Magic
MA Save
Equip Shield
Lava Walking

Ultimus Bow
Crystal Shield
Feather Hat
Leather Outfit
Spike Shoes

Charge+5, Charge+10
Moogle, Ramuh, Titan, Golem, Carbunkle, Odin, Leviathan, Salamander, Fairy



Coyowijuju
Female
Aries
57
58
Wizard
Throw
Counter
Dual Wield
Levitate

Rod
Blind Knife
Leather Hat
Black Costume
Power Wrist

Ice, Ice 3, Empower, Frog
Shuriken, Hammer



Selphet
Female
Pisces
78
54
Thief
Talk Skill
Damage Split
Maintenance
Move+2

Main Gauche

Holy Miter
Wizard Outfit
Red Shoes

Steal Heart, Steal Shield, Arm Aim, Leg Aim
Praise, Insult, Refute, Rehabilitate



Jigglefluffenstuff
Female
Libra
65
51
Ninja
Black Magic
Auto Potion
Equip Armor
Move-HP Up

Sasuke Knife
Flail
Mythril Helmet
Silk Robe
Diamond Armlet

Shuriken, Knife
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 4

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Ominnous
Male
Gemini
77
42
Knight
Yin Yang Magic
Damage Split
Equip Shield
Move+3

Chaos Blade
Bronze Shield
Platinum Helmet
Robe of Lords
Germinas Boots

Armor Break, Magic Break, Power Break, Mind Break, Stasis Sword
Blind, Poison, Doubt Faith, Confusion Song



Rnark
Monster
Capricorn
45
52
Wild Boar










KasugaiRoastedPeas
Female
Gemini
73
49
Time Mage
Punch Art
Sunken State
Magic Attack UP
Teleport

Wizard Staff

Triangle Hat
Linen Robe
Genji Gauntlet

Haste, Haste 2, Slow, Stop, Float, Quick, Demi 2, Stabilize Time
Wave Fist, Earth Slash, Purification, Revive



Upvla
Male
Cancer
79
42
Lancer
Battle Skill
Regenerator
Dual Wield
Move+2

Cypress Rod
Spear
Leather Helmet
Plate Mail
Magic Ring

Level Jump3, Vertical Jump8
Head Break, Armor Break, Power Break

Player: !Red
Team: Red Team
Palettes: Red/Brown



BoneMiser
Female
Taurus
42
80
Chemist
Dance
Caution
Beastmaster
Move+1

Assassin Dagger

Holy Miter
Black Costume
Rubber Shoes

X-Potion, Eye Drop
Nether Demon



Lord Gwarth
Female
Sagittarius
70
74
Summoner
Item
Sunken State
Throw Item
Retreat

Bestiary

Black Hood
Silk Robe
Rubber Shoes

Ifrit, Golem, Carbunkle, Leviathan, Lich
Hi-Ether, Elixir, Eye Drop



Holdenmagronik
Female
Serpentarius
71
46
Thief
Item
MP Restore
Attack UP
Retreat

Dagger

Red Hood
Judo Outfit
Diamond Armlet

Gil Taking, Steal Armor, Steal Weapon, Steal Status, Arm Aim
Potion, X-Potion, Elixir, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



NewbGaming
Female
Cancer
64
79
Thief
Talk Skill
HP Restore
Equip Sword
Jump+1

Asura Knife

Leather Hat
Clothes
Jade Armlet

Steal Heart, Steal Helmet, Leg Aim
Persuade, Rehabilitate

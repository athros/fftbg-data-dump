Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Error72
Female
Taurus
48
61
Samurai
Time Magic
Counter
Halve MP
Move+3

Holy Lance

Diamond Helmet
Gold Armor
Genji Gauntlet

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Haste, Haste 2, Stop, Demi, Stabilize Time



GatsbysGhost
Monster
Scorpio
65
75
Holy Dragon










Gorgewall
Female
Aries
46
75
Time Mage
Steal
Faith Save
Maintenance
Jump+3

Gokuu Rod

Triangle Hat
Light Robe
Defense Ring

Haste, Haste 2, Slow, Slow 2, Quick, Stabilize Time
Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Zagorsek
Female
Pisces
54
43
Ninja
Time Magic
Counter
Short Charge
Swim

Mythril Knife
Cultist Dagger
Leather Hat
Chain Vest
Angel Ring

Shuriken, Ninja Sword, Dictionary
Haste, Slow, Stop, Reflect, Stabilize Time

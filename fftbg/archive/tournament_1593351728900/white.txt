Player: !White
Team: White Team
Palettes: White/Blue



Roqqqpsi
Male
Aries
73
76
Thief
Talk Skill
Hamedo
Maintenance
Move-MP Up

Mage Masher

Headgear
Mythril Vest
Angel Ring

Steal Helmet
Invitation, Threaten, Solution, Insult, Mimic Daravon, Rehabilitate



Dexsana
Monster
Leo
63
46
Explosive










SeniorBunk
Female
Scorpio
41
58
Ninja
Summon Magic
Catch
Equip Bow
Lava Walking

Yoichi Bow

Thief Hat
Chain Vest
Chantage

Shuriken, Knife
Moogle, Shiva, Golem, Carbunkle, Fairy, Cyclops



RaIshtar
Female
Capricorn
77
64
Archer
Battle Skill
Meatbone Slash
Equip Armor
Swim

Yoichi Bow

Holy Miter
Bronze Armor
Elf Mantle

Charge+2, Charge+7, Charge+10
Armor Break, Weapon Break

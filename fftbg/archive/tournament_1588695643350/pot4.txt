Final Bets: purple - 13 bets for 10,656G (47.0%, x1.13); brown - 13 bets for 12,031G (53.0%, x0.89)

purple bets:
sinnyil2: 2,458G (23.1%, 4,916G)
killth3kid: 2,000G (18.8%, 47,263G)
Grimmace45: 1,345G (12.6%, 5,382G)
leakimiko: 1,169G (11.0%, 3,340G)
TheChainNerd: 1,000G (9.4%, 16,919G)
getthemoneyz: 818G (7.7%, 622,385G)
SerumD: 511G (4.8%, 511G)
Who_lio42: 500G (4.7%, 5,834G)
JoeykinsGaming: 500G (4.7%, 12,578G)
CoderCalamity: 100G (0.9%, 2,117G)
datadrivenbot: 100G (0.9%, 12,428G)
Linkn: 100G (0.9%, 1,411G)
daveb_: 55G (0.5%, 968G)

brown bets:
joewcarson: 5,000G (41.6%, 13,422G)
DeathTaxesAndAnime: 1,623G (13.5%, 3,184G)
ungabunga_bot: 1,000G (8.3%, 342,011G)
BirbBrainsBot: 1,000G (8.3%, 41,731G)
JumbocactuarX27: 1,000G (8.3%, 5,712G)
Lionhermit: 1,000G (8.3%, 31,044G)
ZephyrTempest: 456G (3.8%, 709G)
benticore: 220G (1.8%, 220G)
RoboticNomad: 200G (1.7%, 2,873G)
amiture: 200G (1.7%, 4,697G)
gggamezzz: 124G (1.0%, 124G)
belife42: 108G (0.9%, 108G)
volgrathemoose: 100G (0.8%, 4,185G)

Player: !White
Team: White Team
Palettes: White/Blue



VentoLivre
Male
Aquarius
47
59
Oracle
Talk Skill
MA Save
Attack UP
Retreat

Gokuu Rod

Triangle Hat
Chameleon Robe
Jade Armlet

Poison, Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Paralyze
Invitation, Praise, Solution, Death Sentence, Refute, Rehabilitate



FoeSquirrel
Male
Capricorn
61
71
Priest
Summon Magic
Mana Shield
Martial Arts
Lava Walking



Feather Hat
Leather Outfit
Genji Gauntlet

Raise, Raise 2, Reraise, Protect, Shell, Holy
Golem, Carbunkle, Bahamut, Odin, Leviathan, Fairy



Old Overholt
Monster
Aquarius
57
50
Vampire










Volgrathemoose
Male
Aries
46
77
Samurai
Elemental
Counter Flood
Defend
Move+2

Asura Knife

Grand Helmet
Bronze Armor
Cursed Ring

Asura, Bizen Boat, Masamune
Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

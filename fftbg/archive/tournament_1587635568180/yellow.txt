Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Hamborn
Female
Sagittarius
79
68
Ninja
Elemental
Counter Tackle
Equip Axe
Move-HP Up

Flame Whip
Flail
Golden Hairpin
Mythril Vest
Reflect Ring

Shuriken, Knife, Stick
Pitfall, Water Ball, Local Quake, Sand Storm, Lava Ball



ColetteMSLP
Female
Capricorn
46
58
Ninja
Punch Art
Counter Flood
Martial Arts
Move+3



Black Hood
Mystic Vest
Leather Mantle

Bomb, Staff, Wand
Pummel, Secret Fist, Purification, Chakra, Revive



Anethum
Female
Virgo
52
78
Monk
Elemental
Brave Up
Equip Sword
Move-HP Up

Bizen Boat

Leather Hat
Secret Clothes
Sprint Shoes

Wave Fist, Secret Fist, Purification, Chakra, Revive
Pitfall, Water Ball, Hallowed Ground, Quicksand, Sand Storm, Gusty Wind



CassiePhoenix
Female
Cancer
65
65
Samurai
Talk Skill
Critical Quick
Equip Armor
Jump+2

Heaven's Cloud

Leather Helmet
Brigandine
Elf Mantle

Bizen Boat, Kikuichimoji
Persuade, Praise, Threaten, Insult, Mimic Daravon, Refute

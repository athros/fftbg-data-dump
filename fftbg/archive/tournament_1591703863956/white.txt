Player: !White
Team: White Team
Palettes: White/Blue



Belkra
Male
Aquarius
77
68
Oracle
Basic Skill
Mana Shield
Short Status
Jump+1

Gokuu Rod

Headgear
White Robe
Defense Armlet

Life Drain, Doubt Faith, Paralyze, Sleep
Accumulate, Throw Stone, Yell, Cheer Up, Wish



Lastly
Male
Scorpio
78
59
Priest
Jump
Faith Save
Dual Wield
Move+1

Gold Staff
Rainbow Staff
Thief Hat
Silk Robe
Angel Ring

Cure, Cure 4, Raise, Shell, Shell 2, Wall, Esuna, Holy
Level Jump4, Vertical Jump5



GiggleStik
Female
Pisces
47
76
Archer
Black Magic
Dragon Spirit
Equip Sword
Ignore Terrain

Kikuichimoji
Escutcheon
Green Beret
Black Costume
Defense Armlet

Charge+1, Charge+4, Charge+5, Charge+7, Charge+20
Fire 2, Fire 4, Ice 4, Flare



ShintaroNayaka
Female
Libra
45
79
Dancer
Elemental
Catch
Equip Sword
Retreat

Defender

Feather Hat
Mythril Vest
Genji Gauntlet

Polka Polka, Nameless Dance, Obsidian Blade
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

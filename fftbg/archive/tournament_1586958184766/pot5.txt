Final Bets: red - 17 bets for 6,858G (42.8%, x1.34); green - 13 bets for 9,165G (57.2%, x0.75)

red bets:
Nickyfive: 1,500G (21.9%, 13,824G)
MoonSlayerRS: 1,100G (16.0%, 1,100G)
skysa250: 1,000G (14.6%, 28,132G)
ColetteMSLP: 500G (7.3%, 20,900G)
BirbBrainsBot: 404G (5.9%, 177,547G)
benticore: 400G (5.8%, 2,498G)
LDSkinny: 356G (5.2%, 356G)
getthemoneyz: 274G (4.0%, 502,358G)
ZephyrTempest: 224G (3.3%, 66,871G)
Cataphract116: 200G (2.9%, 5,174G)
catfashions: 200G (2.9%, 5,220G)
daveb_: 200G (2.9%, 2,649G)
superdevon1: 100G (1.5%, 1,541G)
ApplesauceBoss: 100G (1.5%, 3,113G)
Luizsanq: 100G (1.5%, 1,086G)
DeathTaxesAndAnime: 100G (1.5%, 1,411G)
datadrivenbot: 100G (1.5%, 100G)

green bets:
Lanshaft: 1,714G (18.7%, 3,429G)
sinnyil2: 1,300G (14.2%, 57,949G)
upvla: 1,234G (13.5%, 35,216G)
TheChainNerd: 1,000G (10.9%, 15,647G)
DavenIII: 1,000G (10.9%, 11,581G)
Treapvort: 614G (6.7%, 614G)
ShintaroNayaka: 500G (5.5%, 16,910G)
friendsquirrel: 428G (4.7%, 428G)
YaBoy125: 420G (4.6%, 7,817G)
astrozin11: 332G (3.6%, 332G)
Ross_from_Cali: 295G (3.2%, 295G)
AllInBot: 223G (2.4%, 223G)
ungabunga_bot: 105G (1.1%, 145,163G)

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BenYuPoker
Female
Sagittarius
64
52
Summoner
Punch Art
Counter Tackle
Magic Attack UP
Swim

Poison Rod

Feather Hat
Chain Vest
Leather Mantle

Moogle, Ramuh, Golem, Carbunkle, Odin, Leviathan, Silf
Secret Fist, Purification



Newbie404
Female
Aries
77
66
Monk
Talk Skill
Arrow Guard
Long Status
Waterwalking



Leather Hat
Brigandine
Red Shoes

Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive
Invitation, Insult, Negotiate, Refute



NovaKnight21
Female
Pisces
52
79
Monk
Throw
Speed Save
Defend
Waterbreathing



Flash Hat
Power Sleeve
N-Kai Armlet

Earth Slash, Purification, Chakra, Revive
Sword, Hammer



Ranmilia
Female
Leo
73
52
Samurai
Throw
Caution
Secret Hunt
Jump+2

Kiyomori

Grand Helmet
Plate Mail
Diamond Armlet

Koutetsu, Murasame, Kiyomori
Shuriken

Player: !White
Team: White Team
Palettes: White/Blue



Quadh0nk
Male
Capricorn
68
70
Mime

Counter
Monster Talk
Teleport



Red Hood
Wizard Robe
Power Wrist

Mimic




Lowlf
Male
Leo
70
53
Oracle
Charge
Critical Quick
Halve MP
Move+3

Battle Folio

Twist Headband
Black Costume
Wizard Mantle

Pray Faith, Doubt Faith, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify
Charge+1, Charge+7, Charge+10



Killth3kid
Male
Sagittarius
67
44
Oracle
Battle Skill
Parry
Maintenance
Lava Walking

Ivory Rod

Twist Headband
Black Robe
108 Gems

Poison, Life Drain, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Paralyze, Petrify, Dark Holy
Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword



SeniorBunk
Female
Cancer
75
55
Archer
Talk Skill
Counter
Dual Wield
Waterbreathing

Long Bow

Bronze Helmet
Black Costume
Setiemson

Charge+1, Charge+4, Charge+7
Invitation, Negotiate, Rehabilitate

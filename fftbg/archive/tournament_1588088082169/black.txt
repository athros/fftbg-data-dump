Player: !Black
Team: Black Team
Palettes: Black/Red



SomeCallMeGON
Male
Capricorn
54
46
Bard
Yin Yang Magic
Brave Up
Short Charge
Waterwalking

Ramia Harp

Twist Headband
Leather Outfit
Reflect Ring

Angel Song, Life Song, Battle Song
Spell Absorb, Zombie, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze



NovaKnight21
Male
Virgo
47
54
Lancer
Basic Skill
Meatbone Slash
Dual Wield
Move+2

Holy Lance
Javelin
Grand Helmet
Leather Armor
Wizard Mantle

Level Jump8, Vertical Jump7
Dash, Tickle, Scream



Jeeboheebo
Monster
Libra
51
80
Grenade










Arch8000
Female
Capricorn
52
80
Time Mage
White Magic
Caution
Short Charge
Move+1

Healing Staff

Thief Hat
Mythril Vest
Sprint Shoes

Haste, Slow 2, Stop, Float, Reflect, Demi, Demi 2, Stabilize Time
Cure, Cure 2, Raise, Raise 2, Regen, Protect, Shell 2, Esuna

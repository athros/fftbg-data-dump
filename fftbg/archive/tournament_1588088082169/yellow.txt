Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ApplesauceBoss
Male
Aquarius
68
59
Ninja
Charge
Sunken State
Equip Bow
Jump+3

Night Killer
Cross Bow
Golden Hairpin
Wizard Outfit
Reflect Ring

Shuriken, Knife, Stick, Dictionary
Charge+1, Charge+4, Charge+10



Thefyeman
Male
Cancer
62
42
Lancer
Charge
Catch
Equip Knife
Teleport

Kunai
Round Shield
Leather Helmet
White Robe
Defense Armlet

Level Jump5, Vertical Jump8
Charge+1, Charge+3, Charge+5



Shakarak
Male
Libra
36
63
Lancer
Basic Skill
Hamedo
Equip Armor
Teleport

Mythril Spear
Aegis Shield
Headgear
Brigandine
Wizard Mantle

Level Jump5, Vertical Jump8
Throw Stone, Heal, Yell



Ko2q
Male
Taurus
48
60
Oracle
Steal
MA Save
Secret Hunt
Move-MP Up

Iron Fan

Green Beret
Light Robe
Battle Boots

Blind, Spell Absorb, Life Drain, Zombie, Silence Song, Dispel Magic, Dark Holy
Steal Heart, Steal Helmet, Steal Weapon, Arm Aim

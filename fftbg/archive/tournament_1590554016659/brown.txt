Player: !Brown
Team: Brown Team
Palettes: Brown/Green



NovaKnight21
Female
Aquarius
59
53
Summoner
Battle Skill
Sunken State
Short Charge
Swim

Flame Rod

Thief Hat
Linen Robe
Leather Mantle

Ramuh, Titan, Golem, Fairy
Head Break, Armor Break, Magic Break, Speed Break



ApplesauceBoss
Male
Capricorn
53
79
Knight
Yin Yang Magic
MP Restore
Short Charge
Move+1

Defender
Flame Shield
Circlet
Crystal Mail
Red Shoes

Magic Break
Life Drain, Silence Song, Confusion Song, Sleep, Petrify



DudeMonkey77
Male
Cancer
73
70
Mime

MP Restore
Defense UP
Waterwalking



Golden Hairpin
Clothes
Elf Mantle

Mimic




Benticore
Male
Gemini
53
79
Ninja
Item
Regenerator
Equip Axe
Jump+3

Slasher
Mythril Knife
Golden Hairpin
Mystic Vest
Feather Boots

Sword, Axe, Stick, Wand
Potion, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down

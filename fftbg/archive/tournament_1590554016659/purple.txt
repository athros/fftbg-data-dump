Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Reinoe
Female
Scorpio
72
42
Knight
Yin Yang Magic
Sunken State
Magic Attack UP
Waterbreathing

Ancient Sword
Bronze Shield
Leather Helmet
Platinum Armor
Defense Armlet

Armor Break, Weapon Break, Speed Break, Mind Break, Stasis Sword, Justice Sword, Surging Sword
Poison, Pray Faith, Blind Rage, Sleep, Dark Holy



Heroebal
Male
Leo
58
79
Ninja
Sing
Hamedo
Equip Polearm
Move+3

Ryozan Silk
Gungnir
Black Hood
Black Costume
Power Wrist

Shuriken, Stick, Wand
Diamond Blade



Stealthtoucan
Female
Pisces
64
56
Samurai
Elemental
Damage Split
Short Status
Retreat

Murasame

Mythril Helmet
Reflect Mail
Rubber Shoes

Koutetsu, Murasame, Muramasa
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Mannequ1n
Female
Gemini
80
70
Summoner
Battle Skill
Regenerator
Martial Arts
Jump+1

Poison Rod

Triangle Hat
Silk Robe
Cherche

Moogle, Shiva, Carbunkle, Bahamut, Odin, Salamander, Zodiac
Weapon Break, Mind Break, Dark Sword

Player: !Black
Team: Black Team
Palettes: Black/Red



SeniorBunk
Male
Aquarius
45
72
Geomancer
Black Magic
HP Restore
Equip Armor
Move+1

Battle Axe
Buckler
Cross Helmet
Wizard Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Fire 2, Bolt 3, Ice, Ice 3, Ice 4, Empower, Flare



Fluffywormhole
Monster
Capricorn
64
55
Swine










ZephyrTempest
Male
Libra
48
74
Geomancer
Summon Magic
Speed Save
Equip Gun
Move+1

Mythril Gun
Flame Shield
Green Beret
Black Robe
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Moogle, Ifrit, Golem, Silf, Fairy



Killth3kid
Monster
Scorpio
66
69
Red Chocobo








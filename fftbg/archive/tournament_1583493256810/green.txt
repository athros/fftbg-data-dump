Player: !Green
Team: Green Team
Palettes: Green/White



Hyvi
Female
Taurus
45
73
Ninja
White Magic
Distribute
Equip Armor
Fly

Short Edge
Mythril Knife
Platinum Helmet
Chain Mail
Defense Armlet

Shuriken, Bomb, Knife, Sword
Cure 2, Raise, Reraise, Regen, Shell, Esuna



Noopockets
Female
Cancer
37
71
Dancer
White Magic
MA Save
Martial Arts
Move-HP Up

Ryozan Silk

Triangle Hat
Mystic Vest
Germinas Boots

Wiznaibus, Void Storage
Cure 4, Raise, Raise 2, Regen, Protect 2, Shell, Shell 2



Dynasti
Female
Pisces
47
36
Wizard
Elemental
Caution
Equip Shield
Move+3

Ice Rod
Buckler
Black Hood
Leather Outfit
Leather Mantle

Fire, Fire 2, Bolt, Bolt 3, Ice 2, Frog
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball



Fortysw4rm
Female
Cancer
47
69
Chemist
Dance
Earplug
Concentrate
Jump+1

Star Bag

Feather Hat
Wizard Outfit
Small Mantle

Antidote, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Wiznaibus, Dragon Pit

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Im4u2nvqt
Male
Aries
71
68
Knight
Punch Art
Counter Flood
Equip Gun
Move-MP Up

Bloody Strings
Aegis Shield
Bronze Helmet
Gold Armor
Power Wrist

Speed Break, Power Break, Surging Sword
Pummel, Earth Slash, Purification, Revive, Seal Evil



YaBoy125
Male
Sagittarius
74
46
Geomancer
Throw
Mana Shield
Dual Wield
Swim

Slasher
Battle Axe
Flash Hat
Wizard Robe
Reflect Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Lava Ball
Staff



Gdreaper1
Female
Cancer
78
73
Chemist
Draw Out
MP Restore
Defense UP
Teleport

Romanda Gun

Thief Hat
Judo Outfit
Diamond Armlet

Potion, Eye Drop, Holy Water, Phoenix Down
Bizen Boat, Muramasa, Kikuichimoji



Nicki
Female
Sagittarius
63
77
Wizard
Summon Magic
Caution
Equip Polearm
Retreat

Ryozan Silk

Headgear
Chameleon Robe
Feather Mantle

Fire 2, Fire 4, Bolt, Bolt 4, Ice 2
Moogle, Ifrit, Golem, Odin, Leviathan, Silf, Fairy, Lich

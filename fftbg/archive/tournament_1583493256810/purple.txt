Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Alacor
Male
Libra
73
56
Knight
Talk Skill
Distribute
Equip Axe
Move+3

Rainbow Staff
Mythril Shield
Gold Helmet
Genji Armor
Genji Gauntlet

Magic Break, Speed Break, Mind Break, Justice Sword
Threaten, Preach, Rehabilitate



DashXero
Female
Virgo
45
81
Monk
Black Magic
Critical Quick
Concentrate
Move-MP Up



Barette
Mythril Vest
Defense Armlet

Pummel, Purification, Chakra
Fire 4, Ice 2, Empower



Hzor
Female
Cancer
56
52
Priest
Charge
Counter Flood
Attack UP
Move+1

White Staff

Headgear
Wizard Robe
Rubber Shoes

Cure 2, Cure 3, Cure 4, Raise 2, Protect, Shell 2, Wall, Esuna, Holy
Charge+7



Murial
Female
Cancer
57
69
Geomancer
Dance
Sunken State
Defense UP
Move+3

Giant Axe
Aegis Shield
Leather Hat
Chameleon Robe
Sprint Shoes

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Wiznaibus, Slow Dance, Polka Polka, Nether Demon

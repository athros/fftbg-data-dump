Player: !Red
Team: Red Team
Palettes: Red/Brown



Arch8000
Female
Virgo
46
79
Lancer
Talk Skill
Critical Quick
Magic Defense UP
Retreat

Partisan
Genji Shield
Gold Helmet
Mythril Armor
Magic Gauntlet

Level Jump4, Vertical Jump8
Persuade, Praise, Threaten, Solution, Mimic Daravon



Baron Von Scrub
Female
Leo
80
71
Squire
Punch Art
Counter Magic
Short Charge
Move+2

Long Sword
Genji Shield
Triangle Hat
Mythril Vest
108 Gems

Accumulate, Heal, Tickle, Yell, Cheer Up
Pummel, Purification, Revive, Seal Evil



E Ballard
Monster
Sagittarius
64
41
Goblin










AUrato
Female
Sagittarius
61
79
Ninja
Draw Out
Dragon Spirit
Defend
Move+1

Assassin Dagger
Cultist Dagger
Flash Hat
Chain Vest
Sprint Shoes

Shuriken, Bomb
Koutetsu, Murasame

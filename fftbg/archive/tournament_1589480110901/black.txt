Player: !Black
Team: Black Team
Palettes: Black/Red



Lowlf
Female
Sagittarius
69
69
Summoner
Jump
Auto Potion
Equip Armor
Move-MP Up

Ice Rod

Platinum Helmet
Linen Cuirass
Elf Mantle

Carbunkle, Leviathan, Silf, Fairy
Level Jump8, Vertical Jump7



HaplessOne
Female
Pisces
76
39
Calculator
White Magic
MP Restore
Concentrate
Teleport

Papyrus Codex

Golden Hairpin
Clothes
Rubber Shoes

CT, Prime Number, 5, 4, 3
Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Shell 2, Esuna



Smiggenator
Male
Taurus
52
45
Thief
Item
HP Restore
Throw Item
Fly

Orichalcum

Cachusha
Mystic Vest
Reflect Ring

Gil Taking, Steal Helmet, Steal Shield, Steal Status, Leg Aim
Potion, Hi-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



0v3rr8d
Female
Cancer
40
68
Geomancer
Throw
Catch
Maintenance
Levitate

Slasher
Ice Shield
Thief Hat
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Blizzard
Shuriken, Bomb

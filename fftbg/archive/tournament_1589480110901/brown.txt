Player: !Brown
Team: Brown Team
Palettes: Brown/Green



A Mo22
Female
Libra
60
43
Monk
Item
Catch
Maintenance
Move+2



Leather Hat
Earth Clothes
Genji Gauntlet

Wave Fist, Secret Fist, Purification, Chakra
Potion, Echo Grass, Phoenix Down



Cryptopsy70
Male
Libra
44
79
Wizard
Basic Skill
Counter
Defend
Teleport

Air Knife

Green Beret
Black Costume
Germinas Boots

Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3
Throw Stone, Heal, Tickle



Run With Stone GUNs
Male
Virgo
50
80
Geomancer
Punch Art
Counter Tackle
Magic Attack UP
Jump+1

Giant Axe
Escutcheon
Feather Hat
Adaman Vest
108 Gems

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Gusty Wind
Spin Fist, Earth Slash, Secret Fist, Revive



Holyonline
Male
Capricorn
49
55
Archer
White Magic
MP Restore
Martial Arts
Waterbreathing


Gold Shield
Red Hood
Wizard Outfit
Angel Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Cure, Cure 3, Raise, Shell 2, Esuna

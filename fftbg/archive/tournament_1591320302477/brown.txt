Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Evewho
Female
Libra
56
53
Knight
Charge
Regenerator
Halve MP
Swim

Defender
Genji Shield
Grand Helmet
Silk Robe
N-Kai Armlet

Power Break, Mind Break, Dark Sword
Charge+3, Charge+7



Omegasuspekt
Female
Capricorn
44
48
Archer
Item
Counter
Equip Shield
Ignore Height

Stone Gun
Flame Shield
Headgear
Brigandine
Sprint Shoes

Charge+2, Charge+3, Charge+4, Charge+5
X-Potion, Soft, Phoenix Down



Nok
Male
Cancer
51
63
Bard
White Magic
Absorb Used MP
Short Status
Move+2

Ultimus Bow

Triangle Hat
Mythril Armor
Elf Mantle

Life Song, Cheer Song, Hydra Pit
Raise, Raise 2, Shell, Esuna, Holy



Aeonicvector
Male
Leo
62
57
Oracle
Punch Art
Counter Magic
Attack UP
Ignore Height

Battle Folio

Black Hood
Rubber Costume
Jade Armlet

Poison, Confusion Song, Dispel Magic
Pummel, Earth Slash, Secret Fist, Purification, Seal Evil

Player: !Green
Team: Green Team
Palettes: Green/White



ThePineappleSalesman
Male
Virgo
68
80
Thief
Talk Skill
Damage Split
Equip Gun
Move-HP Up

Battle Folio

Twist Headband
Mythril Vest
Battle Boots

Steal Heart, Steal Weapon, Arm Aim
Threaten, Death Sentence, Insult, Negotiate, Rehabilitate



Spartan Paladin
Male
Sagittarius
54
61
Knight
Item
Auto Potion
Attack UP
Jump+3

Iron Sword
Platinum Shield
Circlet
Linen Robe
Bracer

Head Break, Armor Break, Weapon Break, Magic Break, Dark Sword
Potion, Ether, Remedy, Phoenix Down



Moonliquor
Female
Pisces
60
53
Thief
Basic Skill
Parry
Magic Attack UP
Move-HP Up

Dagger

Holy Miter
Wizard Outfit
Feather Boots

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Accumulate, Dash, Heal, Tickle



Meta Five
Male
Scorpio
74
55
Summoner
White Magic
Counter
Equip Gun
Waterbreathing

Battle Folio

Leather Hat
White Robe
Angel Ring

Moogle, Shiva, Ramuh, Titan, Golem, Carbunkle, Odin, Silf
Cure 2, Cure 3, Raise 2, Protect, Shell, Esuna, Holy

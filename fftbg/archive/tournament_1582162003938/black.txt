Player: !Black
Team: Black Team
Palettes: Black/Red



Kohlingen
Male
Capricorn
63
70
Wizard
Talk Skill
Damage Split
Monster Talk
Jump+2

Rod

Holy Miter
White Robe
Defense Armlet

Fire 4, Bolt 3, Ice 2, Ice 3
Invitation, Praise, Death Sentence, Negotiate, Mimic Daravon



Vivithegr8
Male
Leo
51
58
Mime

Counter Tackle
Defense UP
Waterbreathing



Barette
Clothes
Dracula Mantle

Mimic




RoyZet
Male
Scorpio
73
63
Chemist
Talk Skill
Dragon Spirit
Magic Attack UP
Move-HP Up

Zorlin Shape

Holy Miter
Power Sleeve
Power Wrist

X-Potion, Ether, Hi-Ether, Antidote, Phoenix Down
Preach, Refute, Rehabilitate



NoxeGS
Male
Libra
68
71
Calculator
Talk Skill
Speed Save
Monster Talk
Levitate

Bestiary

Leather Hat
Power Sleeve
Elf Mantle

CT, Height, Prime Number, 5, 4, 3
Preach, Negotiate, Rehabilitate

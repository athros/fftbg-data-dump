Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Bad1dea
Male
Libra
74
60
Lancer
Talk Skill
Parry
Equip Polearm
Jump+2

Battle Bamboo
Gold Shield
Cross Helmet
Leather Armor
Diamond Armlet

Level Jump3, Vertical Jump6
Persuade, Insult, Refute



ZZ Yoshi
Female
Scorpio
37
35
Dancer
Charge
PA Save
Equip Armor
Retreat

Cashmere

Leather Hat
Chain Mail
Feather Boots

Witch Hunt, Slow Dance, Polka Polka, Last Dance, Obsidian Blade, Nether Demon
Charge+20



NoNotBees
Male
Aries
78
74
Knight
Time Magic
Damage Split
Equip Axe
Jump+1

Morning Star
Escutcheon
Grand Helmet
Light Robe
Magic Gauntlet

Shield Break, Power Break, Justice Sword, Dark Sword
Haste 2, Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time, Meteor



Z32o
Female
Leo
58
51
Calculator
Yin Yang Magic
Catch
Halve MP
Lava Walking

Ivory Rod

Leather Hat
Black Costume
Sprint Shoes

CT, Height, Prime Number, 5
Blind, Poison, Pray Faith, Silence Song, Blind Rage, Dispel Magic

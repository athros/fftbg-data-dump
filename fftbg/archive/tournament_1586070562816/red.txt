Player: !Red
Team: Red Team
Palettes: Red/Brown



ThanatosXRagnarok
Male
Aquarius
56
43
Ninja
Battle Skill
Absorb Used MP
Secret Hunt
Swim

Blind Knife
Orichalcum
Twist Headband
Brigandine
Diamond Armlet

Knife, Staff, Dictionary
Power Break



JackOnFire1
Male
Pisces
76
69
Calculator
Black Magic
Absorb Used MP
Equip Knife
Waterwalking

Dragon Rod

Ribbon
Earth Clothes
Chantage

CT, Prime Number, 5, 4
Fire 2, Fire 4, Bolt 2, Bolt 3, Ice 3, Empower, Frog



Dreadnxught
Male
Leo
76
55
Calculator
Mighty Sword
Faith Up
Defend
Jump+3

Papyrus Codex
Platinum Shield
Leather Helmet
Chameleon Robe
Genji Gauntlet

Blue Magic
Shellburst Stab, Blastar Punch, Hellcry Punch, Icewolf Bite



DarrenDinosaurs
Female
Aries
72
73
Oracle
Throw
Arrow Guard
Halve MP
Waterbreathing

Battle Bamboo

Green Beret
Black Costume
Magic Gauntlet

Life Drain, Pray Faith, Silence Song, Foxbird, Paralyze, Dark Holy
Shuriken, Bomb

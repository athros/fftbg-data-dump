Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



JonnyCue
Female
Leo
64
54
Archer
Dance
Counter Flood
Maintenance
Jump+3

Bow Gun
Ice Shield
Ribbon
Wizard Outfit
Sprint Shoes

Charge+1, Charge+3, Charge+4, Charge+10
Witch Hunt, Wiznaibus, Slow Dance, Disillusion, Nameless Dance, Last Dance



Howplausible
Male
Virgo
60
52
Mediator
Jump
Critical Quick
Secret Hunt
Move-HP Up

Mythril Gun

Feather Hat
Chameleon Robe
Elf Mantle

Praise, Threaten, Preach, Negotiate, Mimic Daravon, Refute
Level Jump2, Vertical Jump7



King Slime
Male
Leo
68
65
Summoner
Black Magic
Absorb Used MP
Martial Arts
Waterbreathing



Headgear
Mystic Vest
Reflect Ring

Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Salamander, Fairy, Lich, Cyclops
Fire 2, Bolt, Ice 4



Skipias
Female
Gemini
79
45
Thief
White Magic
Faith Up
Short Status
Move+2

Main Gauche

Triangle Hat
Mythril Vest
Germinas Boots

Gil Taking, Steal Heart, Steal Armor, Arm Aim
Cure 3, Raise, Protect 2, Wall, Esuna, Holy

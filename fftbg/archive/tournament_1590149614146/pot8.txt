Final Bets: red - 11 bets for 7,587G (66.0%, x0.51); champion - 4 bets for 3,903G (34.0%, x1.94)

red bets:
ThePineappleSalesman: 3,696G (48.7%, 3,696G)
run_with_stone_GUNs: 666G (8.8%, 10,086G)
mardokttv: 661G (8.7%, 661G)
Nickyfive: 576G (7.6%, 576G)
LAGBOT30000: 538G (7.1%, 26,907G)
BirbBrainsBot: 534G (7.0%, 94,124G)
rechaun: 384G (5.1%, 384G)
d4rr1n: 166G (2.2%, 1,663G)
getthemoneyz: 166G (2.2%, 691,638G)
CosmicTactician: 100G (1.3%, 23,096G)
datadrivenbot: 100G (1.3%, 21,432G)

champion bets:
Zachara: 2,504G (64.2%, 92,504G)
CorpusCav: 999G (25.6%, 18,626G)
SerumD: 300G (7.7%, 4,064G)
zepharoth89: 100G (2.6%, 100G)

Final Bets: purple - 11 bets for 5,498G (53.6%, x0.87); brown - 8 bets for 4,756G (46.4%, x1.16)

purple bets:
DavenIII: 1,500G (27.3%, 1,500G)
BirbBrainsBot: 1,000G (18.2%, 93,050G)
getthemoneyz: 900G (16.4%, 691,401G)
CosmicTactician: 500G (9.1%, 22,775G)
LeoNightFury: 500G (9.1%, 3,775G)
ThePineappleSalesman: 316G (5.7%, 316G)
d4rr1n: 282G (5.1%, 282G)
KasugaiRoastedPeas: 200G (3.6%, 939G)
CorpusCav: 100G (1.8%, 19,645G)
ko2q: 100G (1.8%, 1,511G)
datadrivenbot: 100G (1.8%, 21,457G)

brown bets:
Shakarak: 3,000G (63.1%, 50,550G)
roqqqpsi: 992G (20.9%, 9,022G)
LAGBOT30000: 267G (5.6%, 26,795G)
placeholdercats: 147G (3.1%, 738G)
nifboy: 100G (2.1%, 4,480G)
lastly: 100G (2.1%, 13,767G)
maakur_: 100G (2.1%, 4,091G)
DrAntiSocial: 50G (1.1%, 18,480G)

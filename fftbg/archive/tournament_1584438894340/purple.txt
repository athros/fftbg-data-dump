Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CT 5 Holy
Female
Serpentarius
64
50
Summoner
Dance
PA Save
Magic Attack UP
Jump+1

Oak Staff

Leather Hat
Mystic Vest
Chantage

Moogle, Shiva, Ifrit, Golem, Bahamut, Odin, Salamander, Fairy
Wiznaibus, Disillusion, Obsidian Blade, Dragon Pit



Zmanwise
Male
Cancer
74
63
Wizard
Yin Yang Magic
HP Restore
Long Status
Move+3

Thunder Rod

Golden Hairpin
Silk Robe
Leather Mantle

Fire 2, Fire 3, Bolt 3, Empower
Blind, Poison, Silence Song, Blind Rage, Foxbird, Confusion Song, Petrify



Kittiesoink
Male
Taurus
49
54
Time Mage
Summon Magic
Critical Quick
Equip Gun
Move+1

Stone Gun

Feather Hat
Wizard Robe
Jade Armlet

Haste, Float, Meteor
Shiva, Titan, Leviathan



Masaplata
Male
Aries
71
72
Bard
Battle Skill
Arrow Guard
Long Status
Ignore Terrain

Ice Bow

Cachusha
Linen Cuirass
Spike Shoes

Angel Song, Battle Song, Nameless Song, Diamond Blade, Space Storage
Head Break, Weapon Break, Speed Break, Stasis Sword

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Virilikus
Male
Taurus
51
66
Mediator
Summon Magic
Caution
Dual Wield
Move-HP Up

Papyrus Codex
Papyrus Codex
Leather Hat
Wizard Outfit
Genji Gauntlet

Persuade, Praise, Threaten, Preach, Refute
Moogle, Shiva, Ramuh, Ifrit, Leviathan



Dogsandcatsand
Male
Leo
78
48
Ninja
Steal
Brave Save
Equip Gun
Move+3

Stone Gun
Blast Gun
Headgear
Brigandine
Small Mantle

Shuriken, Bomb, Knife, Staff
Gil Taking, Steal Accessory



Lythe Caraker
Male
Pisces
64
78
Monk
Battle Skill
Abandon
Attack UP
Jump+3



Flash Hat
Judo Outfit
Germinas Boots

Pummel, Earth Slash, Purification
Armor Break, Justice Sword, Surging Sword



MemoriesofFinal
Female
Taurus
66
40
Mediator
Punch Art
Counter
Doublehand
Jump+2

Bestiary

Holy Miter
Earth Clothes
Feather Mantle

Threaten, Preach, Solution, Refute
Pummel, Purification, Revive, Seal Evil

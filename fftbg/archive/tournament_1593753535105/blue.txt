Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



StealthModeLocke
Female
Capricorn
49
61
Summoner
Battle Skill
MP Restore
Martial Arts
Lava Walking



Twist Headband
Light Robe
Magic Ring

Moogle, Ramuh, Leviathan, Silf, Fairy
Head Break, Weapon Break, Magic Break, Speed Break, Power Break



RampagingRobot
Female
Libra
55
53
Time Mage
Draw Out
Mana Shield
Defend
Move-HP Up

Gold Staff

Feather Hat
Wizard Robe
Spike Shoes

Haste, Haste 2, Slow, Immobilize, Reflect, Quick, Demi 2
Kiyomori, Kikuichimoji



YaBoy125
Female
Libra
47
64
Geomancer
Item
Dragon Spirit
Throw Item
Teleport

Battle Axe
Aegis Shield
Triangle Hat
Linen Robe
Sprint Shoes

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Potion, Ether, Echo Grass, Soft



EnemyController
Male
Gemini
46
79
Calculator
Black Magic
Mana Shield
Equip Axe
Teleport

Flame Whip

Golden Hairpin
Silk Robe
Rubber Shoes

CT, Height, Prime Number, 5
Ice, Empower

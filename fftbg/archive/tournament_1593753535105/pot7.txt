Final Bets: red - 8 bets for 2,384G (24.8%, x3.03); black - 10 bets for 7,214G (75.2%, x0.33)

red bets:
BirbBrainsBot: 1,000G (41.9%, 162,070G)
TheMM42: 500G (21.0%, 500G)
alekzanndrr: 300G (12.6%, 1,159G)
getthemoneyz: 184G (7.7%, 1,166,447G)
E_Ballard: 100G (4.2%, 9,419G)
fluffskull: 100G (4.2%, 5,832G)
Thyrandaal: 100G (4.2%, 11,196G)
RasenRendan: 100G (4.2%, 100G)

black bets:
ANFz: 2,000G (27.7%, 2,000G)
kaidykat: 1,579G (21.9%, 157,944G)
RampagingRobot: 1,127G (15.6%, 3,627G)
Laserman1000: 683G (9.5%, 13,483G)
prince_rogers_nelson_: 600G (8.3%, 600G)
superdevon1: 573G (7.9%, 573G)
placidphoenix: 252G (3.5%, 252G)
datadrivenbot: 200G (2.8%, 51,418G)
enkikavlar: 100G (1.4%, 2,523G)
Firesheath: 100G (1.4%, 404G)

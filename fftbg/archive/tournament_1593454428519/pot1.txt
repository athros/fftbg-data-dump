Final Bets: red - 23 bets for 9,803G (56.2%, x0.78); blue - 11 bets for 7,644G (43.8%, x1.28)

red bets:
Nizaha: 2,148G (21.9%, 4,297G)
VolgraTheMoose: 1,808G (18.4%, 1,808G)
SkylerBunny: 1,200G (12.2%, 501,719G)
TheChainNerd: 664G (6.8%, 6,649G)
HaateXIII: 500G (5.1%, 4,288G)
getthemoneyz: 412G (4.2%, 1,112,339G)
ChaoreLance: 400G (4.1%, 1,010G)
DLJuggernaut: 328G (3.3%, 328G)
Lythe_Caraker: 250G (2.6%, 143,260G)
Forkmore: 250G (2.6%, 1,429G)
latebit: 240G (2.4%, 240G)
RampagingRobot: 228G (2.3%, 228G)
ArashiKurobara: 224G (2.3%, 224G)
gorgewall: 201G (2.1%, 26,000G)
CosmicTactician: 200G (2.0%, 19,845G)
nifboy: 100G (1.0%, 2,449G)
nhammen: 100G (1.0%, 5,929G)
Firesheath: 100G (1.0%, 17,532G)
safira125: 100G (1.0%, 240G)
Neo_Exodus: 100G (1.0%, 6,598G)
datadrivenbot: 100G (1.0%, 46,144G)
Sans_from_Snowdin: 100G (1.0%, 26,633G)
moonliquor: 50G (0.5%, 3,269G)

blue bets:
UmaiJam: 1,500G (19.6%, 52,757G)
BirbBrainsBot: 1,000G (13.1%, 143,723G)
DudeMonkey77: 1,000G (13.1%, 14,088G)
E_Ballard: 1,000G (13.1%, 3,148G)
DeathTaxesAndAnime: 756G (9.9%, 756G)
Cryptopsy70: 608G (8.0%, 17,826G)
Shalloween: 500G (6.5%, 72,083G)
BlackFireUK: 488G (6.4%, 488G)
Chronolynx42: 347G (4.5%, 34,763G)
DustBirdEX: 345G (4.5%, 7,473G)
Thyrandaal: 100G (1.3%, 20,658G)

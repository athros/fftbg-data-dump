Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Chambs12
Monster
Leo
58
79
Steel Giant










Chuckolator
Monster
Libra
66
44
Great Malboro










Evdoggity
Female
Capricorn
57
41
Squire
Dance
PA Save
Equip Gun
Teleport

Blast Gun
Gold Shield
Feather Hat
Adaman Vest
Reflect Ring

Accumulate, Throw Stone, Heal, Yell, Wish
Disillusion



Rook512
Female
Serpentarius
73
41
Knight
Talk Skill
Parry
Monster Talk
Lava Walking

Long Sword
Round Shield
Mythril Helmet
Plate Mail
Defense Ring

Shield Break, Magic Break, Speed Break
Threaten, Death Sentence, Mimic Daravon

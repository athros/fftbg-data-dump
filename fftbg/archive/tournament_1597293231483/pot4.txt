Final Bets: purple - 11 bets for 16,957G (37.8%, x1.64); brown - 16 bets for 27,849G (62.2%, x0.61)

purple bets:
thaetreis: 7,000G (41.3%, 45,045G)
AllInBot: 4,973G (29.3%, 4,973G)
itskage: 1,073G (6.3%, 10,737G)
SkylerBunny: 1,000G (5.9%, 3,381G)
resjudicata3: 1,000G (5.9%, 11,219G)
OneHundredFists: 668G (3.9%, 668G)
Chuckolator: 364G (2.1%, 1,720G)
WhiteTigress: 320G (1.9%, 320G)
Chambs12: 268G (1.6%, 268G)
getthemoneyz: 190G (1.1%, 1,621,398G)
gorgewall: 101G (0.6%, 6,034G)

brown bets:
ANFz: 11,125G (39.9%, 11,125G)
latebit: 5,000G (18.0%, 29,161G)
DeathTaxesAndAnime: 2,333G (8.4%, 4,575G)
nhammen: 2,246G (8.1%, 5,617G)
ko2q: 1,700G (6.1%, 1,700G)
BirbBrainsBot: 1,000G (3.6%, 76,417G)
Sairentozon7: 1,000G (3.6%, 84,094G)
Mesmaster: 986G (3.5%, 986G)
DAC169: 811G (2.9%, 811G)
technominari: 500G (1.8%, 88,033G)
Arcblazer23: 296G (1.1%, 296G)
Rook512: 252G (0.9%, 2,527G)
blandshee: 200G (0.7%, 1,309G)
datadrivenbot: 200G (0.7%, 63,648G)
MinBetBot: 100G (0.4%, 4,632G)
TheDali23: 100G (0.4%, 1,718G)

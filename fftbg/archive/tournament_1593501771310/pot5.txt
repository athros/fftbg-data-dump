Final Bets: blue - 10 bets for 7,871G (37.2%, x1.68); yellow - 15 bets for 13,260G (62.8%, x0.59)

blue bets:
TheMM42: 3,000G (38.1%, 24,951G)
Digitalsocrates: 1,054G (13.4%, 1,054G)
prince_rogers_nelson_: 1,000G (12.7%, 8,011G)
BirbBrainsBot: 1,000G (12.7%, 159,660G)
CITOtheMOSQUITO: 704G (8.9%, 704G)
shs_: 500G (6.4%, 14,504G)
getthemoneyz: 212G (2.7%, 1,113,695G)
gorgewall: 201G (2.6%, 27,155G)
Ring_Wyrm: 100G (1.3%, 2,936G)
Evewho: 100G (1.3%, 6,499G)

yellow bets:
Arcblazer23: 3,301G (24.9%, 3,301G)
escobro: 2,000G (15.1%, 5,773G)
kaidykat: 1,422G (10.7%, 142,207G)
Lemonjohns: 1,082G (8.2%, 1,082G)
BoneMiser: 1,000G (7.5%, 1,000G)
DuraiPapers: 1,000G (7.5%, 6,719G)
cougboi: 1,000G (7.5%, 12,466G)
twelfthrootoftwo: 500G (3.8%, 24,866G)
alekzanndrr: 400G (3.0%, 2,344G)
Coolguye: 375G (2.8%, 3,752G)
superdevon1: 340G (2.6%, 340G)
Nova_riety: 320G (2.4%, 1,602G)
Zachara: 320G (2.4%, 136,720G)
datadrivenbot: 100G (0.8%, 46,480G)
serperemagus: 100G (0.8%, 9,487G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Loveyouallfriends
Female
Pisces
60
55
Geomancer
Item
Distribute
Equip Axe
Move+2

Flail
Gold Shield
Flash Hat
Light Robe
108 Gems

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Hi-Ether, Eye Drop



E Ballard
Male
Pisces
75
52
Bard
Draw Out
Abandon
Halve MP
Ignore Terrain

Ramia Harp

Red Hood
Black Costume
Bracer

Cheer Song, Last Song
Heaven's Cloud, Kikuichimoji



Draconis345
Male
Serpentarius
70
59
Priest
Battle Skill
Counter
Attack UP
Lava Walking

Flail

Triangle Hat
Linen Robe
Angel Ring

Cure 2, Raise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna
Head Break, Armor Break, Shield Break, Magic Break, Night Sword



Inzo24
Male
Sagittarius
51
40
Oracle
Summon Magic
Earplug
Beastmaster
Waterbreathing

Musk Rod

Red Hood
Earth Clothes
Power Wrist

Poison, Life Drain, Paralyze
Moogle, Shiva, Ifrit, Golem

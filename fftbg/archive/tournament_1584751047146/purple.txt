Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



RunicMagus
Male
Gemini
46
76
Knight
Punch Art
Blade Grasp
Doublehand
Jump+3

Slasher

Platinum Helmet
Diamond Armor
N-Kai Armlet

Head Break, Shield Break, Power Break, Mind Break, Justice Sword, Dark Sword
Secret Fist, Purification



Gooseyourself
Female
Cancer
76
62
Samurai
Item
Sunken State
Equip Knife
Swim

Assassin Dagger

Crystal Helmet
Bronze Armor
Germinas Boots

Koutetsu, Bizen Boat, Kikuichimoji
Potion, Hi-Potion, Hi-Ether, Holy Water, Remedy, Phoenix Down



Basmal
Female
Cancer
50
48
Archer
Summon Magic
Sunken State
Short Status
Move-HP Up

Mythril Bow

Gold Helmet
Leather Outfit
Feather Mantle

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Moogle, Ramuh, Ifrit, Salamander, Silf



Tougou
Male
Aquarius
80
49
Lancer
Steal
Abandon
Martial Arts
Waterbreathing

Javelin
Crystal Shield
Iron Helmet
Crystal Mail
Sprint Shoes

Level Jump2, Vertical Jump4
Steal Heart, Steal Helmet, Steal Armor, Arm Aim, Leg Aim

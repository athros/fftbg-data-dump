Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Galkife
Male
Aries
53
51
Time Mage
Talk Skill
Catch
Long Status
Swim

Octagon Rod

Flash Hat
Silk Robe
Vanish Mantle

Slow 2, Stop, Float, Reflect, Quick, Stabilize Time
Invitation, Praise, Rehabilitate



Chaoskl21
Female
Taurus
56
47
Priest
Throw
Critical Quick
Equip Bow
Ignore Terrain

Long Bow

Flash Hat
Linen Robe
Jade Armlet

Cure 2, Cure 3, Cure 4, Raise, Reraise, Protect 2, Holy
Shuriken, Bomb, Knife, Hammer, Dictionary



Roofiepops
Female
Scorpio
70
67
Thief
Elemental
Parry
Magic Attack UP
Levitate

Main Gauche

Barette
Mystic Vest
N-Kai Armlet

Steal Heart, Steal Accessory, Arm Aim, Leg Aim
Pitfall, Water Ball, Hell Ivy, Quicksand, Sand Storm, Blizzard



ZCKaiser
Monster
Sagittarius
64
64
Tiamat








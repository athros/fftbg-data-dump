Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Waterwatereverywhere
Female
Scorpio
67
47
Oracle
Punch Art
Absorb Used MP
Halve MP
Move+1

Ivory Rod

Thief Hat
Black Costume
N-Kai Armlet

Blind, Poison, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy
Earth Slash, Secret Fist, Purification



Thevilmen
Male
Gemini
68
71
Calculator
White Magic
Brave Up
Equip Gun
Ignore Height

Mythril Gun

Feather Hat
Clothes
Power Wrist

CT, Height, 3
Raise, Raise 2, Reraise, Protect 2, Shell, Esuna, Holy



Victoriolue
Male
Taurus
62
40
Bard
Battle Skill
Auto Potion
Halve MP
Levitate

Ramia Harp

Feather Hat
Adaman Vest
Red Shoes

Life Song, Last Song, Sky Demon, Hydra Pit
Weapon Break



BenYuPoker
Male
Leo
60
67
Mediator
Yin Yang Magic
Regenerator
Concentrate
Waterwalking

Blast Gun

Cachusha
Mystic Vest
Feather Boots

Solution, Insult, Mimic Daravon
Life Drain, Pray Faith, Zombie, Silence Song, Dispel Magic, Sleep, Petrify

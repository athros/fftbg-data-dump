Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



Old Overholt
Monster
Serpentarius
58
70
Ultima Demon










Nocturnal Perspective
Male
Capricorn
40
72
Knight
Item
Distribute
Secret Hunt
Ignore Terrain

Ragnarok
Buckler
Circlet
Linen Cuirass
Feather Mantle

Shield Break, Weapon Break, Speed Break, Mind Break, Justice Sword, Explosion Sword
Potion, Hi-Potion, X-Potion, Ether, Antidote, Echo Grass, Remedy



Tougou
Male
Taurus
73
73
Archer
Sing
Distribute
Dual Wield
Jump+2

Perseus Bow

Red Hood
Power Sleeve
Sprint Shoes

Charge+1, Charge+2, Charge+3
Magic Song, Last Song, Diamond Blade, Hydra Pit



Arch8000
Male
Pisces
58
48
Lancer
Throw
Counter
Defense UP
Teleport

Obelisk
Bronze Shield
Bronze Helmet
Chameleon Robe
Power Wrist

Level Jump4, Vertical Jump8
Shuriken, Staff

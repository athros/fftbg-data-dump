Player: !Red
Team: Red Team
Palettes: Red/Brown



Moocaotao
Male
Virgo
47
70
Squire
Battle Skill
Speed Save
Equip Gun
Jump+3

Papyrus Codex
Aegis Shield
Gold Helmet
Wizard Outfit
Angel Ring

Accumulate, Dash, Heal
Head Break, Armor Break, Weapon Break, Speed Break, Justice Sword, Dark Sword



Nifboy
Female
Sagittarius
44
39
Summoner
Item
Distribute
Short Charge
Lava Walking

Flame Rod

Golden Hairpin
Clothes
Wizard Mantle

Shiva, Titan, Carbunkle, Bahamut, Silf
Potion, Hi-Potion, Elixir, Eye Drop, Phoenix Down



NeoKevlar
Monster
Aries
59
61
Sekhret










Lydian C
Female
Aries
43
60
Ninja
Punch Art
HP Restore
Equip Gun
Jump+1

Papyrus Codex
Ramia Harp
Ribbon
Secret Clothes
Magic Gauntlet

Shuriken, Spear
Pummel, Wave Fist, Secret Fist, Purification, Chakra

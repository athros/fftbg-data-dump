Player: !Black
Team: Black Team
Palettes: Black/Red



Nok
Male
Gemini
54
77
Summoner
Jump
Faith Save
Short Charge
Waterwalking

Bestiary

Red Hood
Silk Robe
Rubber Shoes

Moogle, Leviathan, Salamander, Silf, Lich
Level Jump4, Vertical Jump8



Deathmaker06
Male
Gemini
58
79
Samurai
Item
Absorb Used MP
Equip Shield
Move+2

Javelin
Mythril Shield
Circlet
White Robe
Magic Ring

Koutetsu, Heaven's Cloud, Kiyomori, Kikuichimoji
X-Potion, Antidote, Maiden's Kiss, Soft, Phoenix Down



Metagameface
Female
Scorpio
67
53
Wizard
Throw
MP Restore
Equip Knife
Move-HP Up

Dragon Rod

Green Beret
Black Costume
Rubber Shoes

Fire, Fire 2, Fire 4, Bolt 2, Bolt 3, Ice, Ice 2
Shuriken, Bomb, Stick



Zbgs
Female
Pisces
55
44
Knight
Yin Yang Magic
Counter Tackle
Equip Gun
Lava Walking

Blast Gun
Aegis Shield
Leather Helmet
Bronze Armor
Genji Gauntlet

Power Break, Mind Break, Justice Sword, Surging Sword
Poison, Pray Faith, Zombie, Silence Song, Dispel Magic, Paralyze

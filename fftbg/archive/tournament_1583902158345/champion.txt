Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Carchan131
Female
Sagittarius
43
63
Knight
Punch Art
Speed Save
Equip Axe
Fly

Giant Axe
Diamond Shield
Gold Helmet
Silk Robe
Genji Gauntlet

Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Dark Sword
Pummel, Earth Slash, Purification, Seal Evil



Ninjasteepler
Female
Leo
57
68
Chemist
Draw Out
Caution
Martial Arts
Swim

Mage Masher

Holy Miter
Black Costume
Feather Mantle

Potion, X-Potion, Ether, Hi-Ether, Antidote, Soft, Phoenix Down
Bizen Boat, Murasame, Heaven's Cloud



ArchKnightX
Male
Scorpio
55
45
Bard
Draw Out
MA Save
Attack UP
Move+3

Bloody Strings

Thief Hat
Chain Mail
Dracula Mantle

Life Song, Battle Song, Last Song, Sky Demon, Hydra Pit
Murasame, Heaven's Cloud



Davarian
Male
Scorpio
56
74
Lancer
Item
MA Save
Beastmaster
Move+1

Obelisk
Escutcheon
Gold Helmet
Chain Mail
Magic Gauntlet

Level Jump8, Vertical Jump4
Potion, X-Potion, Ether, Eye Drop

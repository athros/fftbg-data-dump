Final Bets: white - 15 bets for 12,576G (71.3%, x0.40); black - 12 bets for 5,057G (28.7%, x2.49)

white bets:
NicoSavoy: 2,000G (15.9%, 180,605G)
Cryptopsy70: 1,540G (12.2%, 51,349G)
upvla: 1,500G (11.9%, 3,428G)
twelfthrootoftwo: 1,160G (9.2%, 2,276G)
BirbBrainsBot: 1,000G (8.0%, 137,044G)
dantayystv: 1,000G (8.0%, 39,097G)
Humma_kavula24: 1,000G (8.0%, 5,146G)
Evewho: 796G (6.3%, 796G)
killth3kid: 700G (5.6%, 102,799G)
KyleWonToLiveForever: 543G (4.3%, 543G)
MagicBottle: 536G (4.3%, 11,707G)
gorgewall: 201G (1.6%, 5,022G)
fluffskull: 200G (1.6%, 2,591G)
datadrivenbot: 200G (1.6%, 55,768G)
lijerrr: 200G (1.6%, 1,159G)

black bets:
getthemoneyz: 1,000G (19.8%, 1,280,319G)
cougboi: 914G (18.1%, 914G)
Rislyeu: 885G (17.5%, 885G)
E_Ballard: 678G (13.4%, 678G)
randgridr: 308G (6.1%, 308G)
superdevon1: 286G (5.7%, 5,730G)
Seaweed_B: 236G (4.7%, 3,276G)
silentkaster: 200G (4.0%, 17,738G)
winteriscorning: 200G (4.0%, 1,071G)
KasugaiRoastedPeas: 200G (4.0%, 14,587G)
DeathTaxesAndAnime: 100G (2.0%, 3,911G)
moonliquor: 50G (1.0%, 13,458G)

Final Bets: red - 8 bets for 5,370G (61.7%, x0.62); blue - 10 bets for 3,332G (38.3%, x1.61)

red bets:
LDSkinny: 2,177G (40.5%, 2,177G)
TheChainNerd: 1,500G (27.9%, 13,873G)
Kennymillennium: 716G (13.3%, 716G)
Shinzutalos: 348G (6.5%, 348G)
BigGlorious: 229G (4.3%, 229G)
outer_monologue: 200G (3.7%, 1,930G)
CosmicTactician: 100G (1.9%, 16,845G)
datadrivenbot: 100G (1.9%, 32,917G)

blue bets:
BirbBrainsBot: 1,000G (30.0%, 23,516G)
RaIshtar: 500G (15.0%, 3,968G)
Rislyeu: 400G (12.0%, 12,705G)
skillomono: 300G (9.0%, 808G)
marin1987: 264G (7.9%, 264G)
getthemoneyz: 214G (6.4%, 810,910G)
gogofromtogo: 200G (6.0%, 3,983G)
BlackFireUK: 200G (6.0%, 4,173G)
alterworlds: 154G (4.6%, 154G)
E_Ballard: 100G (3.0%, 5,135G)

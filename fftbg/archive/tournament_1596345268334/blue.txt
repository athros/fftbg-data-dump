Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



HaateXIII
Female
Capricorn
79
43
Summoner
Throw
MP Restore
Attack UP
Move+1

Poison Rod

Twist Headband
Adaman Vest
Bracer

Moogle, Shiva, Ifrit, Golem, Carbunkle, Leviathan
Bomb, Hammer



Roofiepops
Female
Aquarius
72
47
Ninja
Basic Skill
Counter Flood
Equip Polearm
Jump+1

Partisan
Partisan
Green Beret
Black Costume
Chantage

Shuriken, Bomb
Throw Stone



Flacococo
Female
Aquarius
39
43
Monk
White Magic
Caution
Maintenance
Move+2



Feather Hat
Power Sleeve
Dracula Mantle

Pummel, Purification, Chakra, Revive
Cure 3, Cure 4, Raise, Reraise, Regen, Protect 2, Shell 2, Esuna



Lord Gwarth
Male
Virgo
73
78
Mime

Catch
Defend
Move+3



Red Hood
Clothes
Leather Mantle

Mimic


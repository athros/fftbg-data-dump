Final Bets: blue - 8 bets for 3,406G (19.3%, x4.17); yellow - 8 bets for 14,204G (80.7%, x0.24)

blue bets:
sinnyil2: 1,200G (35.2%, 14,942G)
prince_rogers_nelson_: 576G (16.9%, 576G)
electric_algus: 400G (11.7%, 12,900G)
TeaTime29: 300G (8.8%, 12,446G)
getthemoneyz: 256G (7.5%, 984,879G)
Spuzzmocker: 256G (7.5%, 256G)
BirbBrainsBot: 218G (6.4%, 94,564G)
Evewho: 200G (5.9%, 11,740G)

yellow bets:
Firesheath: 5,000G (35.2%, 16,724G)
NicoSavoy: 5,000G (35.2%, 14,027G)
opHendoslice: 2,000G (14.1%, 4,089G)
kilrazin: 1,000G (7.0%, 2,569G)
CapnChaos12: 500G (3.5%, 18,629G)
AltimaMantoid: 279G (2.0%, 1,779G)
ThisGuyLovesMath: 225G (1.6%, 225G)
Kronikle: 200G (1.4%, 13,226G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CapnChaos12
Male
Aquarius
52
55
Samurai
Jump
Distribute
Short Charge
Fly

Spear

Platinum Helmet
Silk Robe
Diamond Armlet

Bizen Boat, Muramasa, Chirijiraden
Level Jump8, Vertical Jump8



Victoriolue
Male
Sagittarius
72
49
Monk
Steal
Earplug
Dual Wield
Move+1



Red Hood
Judo Outfit
Bracer

Earth Slash, Secret Fist, Chakra, Revive
Steal Status, Leg Aim



Realitydown
Female
Aries
61
66
Calculator
Time Magic
Mana Shield
Concentrate
Waterwalking

Musk Rod

Triangle Hat
Black Robe
Feather Mantle

CT, Prime Number
Haste, Haste 2, Slow, Immobilize, Quick, Demi 2



OpHendoslice
Male
Aquarius
65
73
Knight
Charge
Catch
Dual Wield
Move+3

Ragnarok
Coral Sword
Platinum Helmet
Leather Armor
Feather Mantle

Power Break, Stasis Sword, Dark Sword
Charge+2, Charge+3, Charge+4, Charge+7

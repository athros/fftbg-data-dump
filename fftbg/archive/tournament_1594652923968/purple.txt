Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nsm013
Male
Aries
61
59
Mediator
Time Magic
Auto Potion
Defense UP
Waterwalking

Blast Gun

Golden Hairpin
Earth Clothes
Rubber Shoes

Invitation, Praise, Threaten, Death Sentence, Negotiate
Haste, Haste 2, Reflect, Demi 2, Stabilize Time



CapnChaos12
Female
Cancer
57
76
Summoner
Jump
Abandon
Short Charge
Move-HP Up

Flame Rod

Flash Hat
Silk Robe
Defense Ring

Moogle, Ramuh, Carbunkle, Leviathan, Fairy, Lich
Level Jump2, Vertical Jump7



LDSkinny
Female
Sagittarius
48
61
Oracle
White Magic
Damage Split
Equip Bow
Waterbreathing

Poison Bow

Black Hood
White Robe
Diamond Armlet

Spell Absorb, Doubt Faith, Blind Rage, Dispel Magic
Cure, Cure 3, Raise, Raise 2, Reraise, Protect, Wall



Mysteriousdewd
Male
Cancer
80
50
Chemist
Throw
Counter Flood
Long Status
Move+2

Hydra Bag

Flash Hat
Earth Clothes
Power Wrist

Potion, X-Potion, Hi-Ether, Holy Water
Shuriken, Wand

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PoroTact
Male
Leo
68
46
Mediator
Jump
Distribute
Short Charge
Jump+3

Blaze Gun

Holy Miter
Linen Robe
Sprint Shoes

Invitation, Insult, Refute
Level Jump8, Vertical Jump8



Benticore
Female
Cancer
61
46
Oracle
Item
Parry
Equip Knife
Move-HP Up

Thunder Rod

Twist Headband
Black Robe
Red Shoes

Spell Absorb, Life Drain, Pray Faith, Blind Rage, Dispel Magic
Potion, X-Potion, Echo Grass, Phoenix Down



RaIshtar
Male
Sagittarius
53
80
Chemist
Sing
Absorb Used MP
Concentrate
Jump+2

Main Gauche

Thief Hat
Black Costume
Angel Ring

Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Angel Song, Magic Song, Hydra Pit



Creggers
Female
Cancer
42
61
Oracle
Time Magic
HP Restore
Secret Hunt
Move+1

Gokuu Rod

Black Hood
Leather Outfit
Defense Armlet

Spell Absorb, Life Drain, Silence Song, Paralyze, Dark Holy
Haste 2, Slow, Reflect, Demi 2, Stabilize Time

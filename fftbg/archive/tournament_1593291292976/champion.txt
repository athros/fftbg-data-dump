Player: !zChamp
Team: Champion Team
Palettes: White/Blue



FriendSquirrel
Male
Taurus
68
70
Monk
Black Magic
MA Save
Defense UP
Ignore Height



Green Beret
Black Costume
Leather Mantle

Pummel, Wave Fist, Secret Fist, Purification, Chakra, Revive
Fire, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Flare



Pplvee1
Female
Aries
69
57
Archer
Black Magic
Distribute
Equip Gun
Fly

Romanda Gun
Flame Shield
Holy Miter
Leather Outfit
Red Shoes

Charge+1, Charge+5, Charge+10
Fire, Fire 4, Bolt 3, Ice 2, Ice 4, Empower, Frog, Death



OpHendoslice
Male
Pisces
52
46
Ninja
Basic Skill
Catch
Magic Defense UP
Swim

Cultist Dagger
Blind Knife
Golden Hairpin
Mystic Vest
Rubber Shoes

Knife, Dictionary
Dash, Tickle, Yell, Wish, Scream



Forkmore
Female
Sagittarius
63
73
Archer
Draw Out
Critical Quick
Doublehand
Move-MP Up

Ultimus Bow

Cross Helmet
Power Sleeve
Red Shoes

Charge+3, Charge+7, Charge+10
Asura

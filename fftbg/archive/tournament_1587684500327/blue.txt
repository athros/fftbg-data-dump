Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Fenixcrest
Male
Leo
67
55
Thief
Summon Magic
Mana Shield
Short Charge
Levitate

Assassin Dagger

Green Beret
Wizard Outfit
Genji Gauntlet

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Moogle, Ifrit, Silf



Lordminsc
Female
Aquarius
52
68
Dancer
Draw Out
Damage Split
Equip Polearm
Levitate

Partisan

Feather Hat
Wizard Outfit
Bracer

Witch Hunt, Polka Polka, Disillusion, Nameless Dance, Void Storage, Nether Demon, Dragon Pit
Asura, Muramasa, Kikuichimoji



Nedryerson01
Female
Sagittarius
68
59
Dancer
Talk Skill
Distribute
Short Status
Move+2

Cashmere

Barette
Judo Outfit
Bracer

Slow Dance, Disillusion, Last Dance, Obsidian Blade, Dragon Pit
Threaten, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate



Urieltheflameofgod
Female
Sagittarius
53
61
Thief
Charge
Counter
Attack UP
Move-HP Up

Air Knife

Twist Headband
Mystic Vest
Magic Ring

Gil Taking, Steal Heart, Steal Shield, Steal Accessory
Charge+1, Charge+4

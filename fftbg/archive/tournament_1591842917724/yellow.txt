Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Joewcarson
Male
Libra
60
78
Samurai
Time Magic
Counter Flood
Magic Defense UP
Lava Walking

Javelin

Diamond Helmet
Mythril Armor
Dracula Mantle

Koutetsu, Muramasa
Haste, Haste 2, Slow 2, Float, Reflect, Quick, Demi, Galaxy Stop



Aldrammech
Male
Pisces
43
71
Thief
Punch Art
Absorb Used MP
Beastmaster
Waterbreathing

Dagger

Triangle Hat
Power Sleeve
Power Wrist

Steal Shield, Arm Aim, Leg Aim
Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive



Spartan Paladin
Male
Virgo
47
48
Knight
Elemental
Arrow Guard
Equip Sword
Jump+2

Koutetsu Knife
Diamond Shield
Iron Helmet
White Robe
Magic Gauntlet

Armor Break, Shield Break, Weapon Break, Speed Break, Mind Break
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



LanseDM
Male
Leo
49
52
Monk
Yin Yang Magic
Meatbone Slash
Short Status
Move-HP Up



Triangle Hat
Clothes
Red Shoes

Purification, Revive, Seal Evil
Blind, Dispel Magic, Sleep

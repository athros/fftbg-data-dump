Player: !Green
Team: Green Team
Palettes: Green/White



Dem0nj0ns
Female
Gemini
51
75
Lancer
Summon Magic
Counter Tackle
Defend
Move+3

Javelin
Diamond Shield
Diamond Helmet
White Robe
Angel Ring

Level Jump4, Vertical Jump2
Moogle, Shiva, Carbunkle, Salamander



ALY327
Female
Pisces
71
72
Summoner
Jump
Sunken State
Halve MP
Swim

White Staff

Golden Hairpin
Chameleon Robe
Defense Armlet

Moogle, Shiva, Ifrit, Golem, Bahamut
Level Jump8, Vertical Jump6



Gorgewall
Male
Scorpio
55
53
Chemist
Elemental
Counter Tackle
Dual Wield
Jump+2

Mythril Gun
Romanda Gun
Feather Hat
Earth Clothes
Angel Ring

Potion, Hi-Potion, X-Potion, Ether, Holy Water
Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Go2sleepTV
Female
Virgo
70
63
Wizard
Charge
Speed Save
Short Charge
Retreat

Poison Rod

Green Beret
Linen Robe
Power Wrist

Fire 2, Fire 3, Bolt 2, Ice 3
Charge+1, Charge+3, Charge+7, Charge+20

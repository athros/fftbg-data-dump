Player: !Black
Team: Black Team
Palettes: Black/Red



Jeeboheebo
Female
Sagittarius
43
67
Samurai
Jump
Counter
Long Status
Move-HP Up

Bizen Boat

Platinum Helmet
Mythril Armor
Reflect Ring

Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Chirijiraden
Level Jump8, Vertical Jump8



CT 5 Holy
Female
Gemini
79
64
Time Mage
Draw Out
MA Save
Halve MP
Jump+3

Wizard Staff

Leather Hat
Light Robe
Power Wrist

Slow, Stop, Float, Reflect, Demi 2, Stabilize Time
Koutetsu, Heaven's Cloud



Panushenko
Male
Virgo
53
52
Lancer
Item
Counter Flood
Maintenance
Move+2

Dragon Whisker
Buckler
Barbuta
Carabini Mail
Feather Mantle

Level Jump8, Vertical Jump2
Potion, Hi-Potion, X-Potion, Ether, Soft, Phoenix Down



Bioticism
Female
Scorpio
69
77
Lancer
Charge
Counter
Equip Sword
Waterwalking

Nagrarock
Bronze Shield
Barbuta
Diamond Armor
Elf Mantle

Level Jump5, Vertical Jump8
Charge+1, Charge+3, Charge+7

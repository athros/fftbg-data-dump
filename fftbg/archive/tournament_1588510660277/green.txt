Player: !Green
Team: Green Team
Palettes: Green/White



Maakur
Male
Libra
49
53
Knight
Basic Skill
Brave Up
Sicken
Move-MP Up

Sleep Sword
Flame Shield
Platinum Helmet
Crystal Mail
Feather Boots

Head Break, Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Dash, Heal, Tickle



Zachara
Female
Leo
80
72
Dancer
Charge
MA Save
Equip Bow
Teleport

Night Killer

Black Hood
Black Robe
108 Gems

Disillusion, Last Dance, Nether Demon, Dragon Pit
Charge+2, Charge+3, Charge+4, Charge+5, Charge+10



Solomongrundy85
Male
Leo
69
54
Archer
Black Magic
Counter Magic
Doublehand
Teleport

Night Killer

Feather Hat
Wizard Outfit
Germinas Boots

Charge+1, Charge+3, Charge+4, Charge+7, Charge+20
Fire, Fire 2, Bolt 3, Bolt 4, Ice 3



FNCardascia
Female
Leo
68
59
Archer
Punch Art
Caution
Concentrate
Move+1

Windslash Bow

Headgear
Judo Outfit
Leather Mantle

Charge+3, Charge+4, Charge+7
Pummel, Chakra, Revive

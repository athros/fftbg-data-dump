Player: !Red
Team: Red Team
Palettes: Red/Brown



Mesmaster
Male
Cancer
39
66
Ninja
Talk Skill
Faith Up
Attack UP
Move-HP Up

Hidden Knife
Sasuke Knife
Headgear
Adaman Vest
Wizard Mantle

Shuriken, Hammer
Praise, Preach, Solution, Death Sentence, Negotiate, Mimic Daravon



WinnerBit
Male
Leo
53
70
Time Mage
Black Magic
Counter
Beastmaster
Waterwalking

Mace of Zeus

Green Beret
Wizard Outfit
Rubber Shoes

Haste, Haste 2, Immobilize, Demi
Bolt, Bolt 2, Empower



PatSouI
Male
Leo
47
73
Ninja
Time Magic
Speed Save
Doublehand
Teleport

Sasuke Knife

Green Beret
Judo Outfit
Spike Shoes

Bomb, Knife
Float, Reflect, Quick



WrathfulRemy
Male
Gemini
42
50
Archer
White Magic
Auto Potion
Secret Hunt
Move+1

Gastrafitis
Diamond Shield
Red Hood
Mystic Vest
Feather Mantle

Charge+2, Charge+3
Cure, Cure 2, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Shell, Shell 2, Wall, Esuna

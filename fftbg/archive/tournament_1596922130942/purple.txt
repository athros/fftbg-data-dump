Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Oreo Pizza
Female
Taurus
68
62
Knight
Dance
Distribute
Long Status
Move-MP Up

Giant Axe
Venetian Shield
Barbuta
Carabini Mail
Genji Gauntlet

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword
Wiznaibus, Disillusion, Nameless Dance, Dragon Pit



Forkmore
Female
Aries
53
39
Dancer
White Magic
Counter Tackle
Equip Bow
Jump+3

Night Killer

Green Beret
Wizard Robe
Rubber Shoes

Witch Hunt, Disillusion
Cure, Cure 2, Raise, Reraise, Shell, Wall, Esuna



Lemonjohns
Male
Libra
52
47
Lancer
Draw Out
Counter Tackle
Short Status
Move-MP Up

Octagon Rod
Genji Shield
Cross Helmet
Wizard Robe
Bracer

Level Jump4, Vertical Jump5
Koutetsu, Kiyomori, Kikuichimoji, Chirijiraden



Amorial
Female
Gemini
80
48
Knight
Elemental
Auto Potion
Sicken
Jump+2

Save the Queen
Flame Shield
Gold Helmet
Chain Mail
Wizard Mantle

Head Break, Weapon Break, Magic Break, Power Break, Justice Sword
Pitfall, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Lava Ball

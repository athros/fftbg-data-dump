Player: !White
Team: White Team
Palettes: White/Blue



Resjudicata3
Monster
Leo
39
72
Floating Eye










Powerpinch
Female
Cancer
46
73
Ninja
Summon Magic
Auto Potion
Magic Defense UP
Move+3

Flame Whip
Morning Star
Triangle Hat
Adaman Vest
108 Gems

Bomb, Ninja Sword, Stick, Wand
Ifrit, Titan, Golem, Leviathan



Evdoggity
Male
Aquarius
56
43
Wizard
Sing
Sunken State
Dual Wield
Fly

Wizard Rod
Dragon Rod
Holy Miter
Light Robe
Spike Shoes

Fire 4, Bolt 3, Ice 3
Life Song, Nameless Song, Last Song



ArchKnightX
Female
Pisces
79
64
Squire
Talk Skill
MP Restore
Equip Sword
Waterwalking

Defender

Mythril Helmet
Chain Vest
Vanish Mantle

Accumulate, Yell, Scream
Persuade, Preach, Death Sentence, Insult, Negotiate, Rehabilitate

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bryon W
Female
Aquarius
62
66
Mediator
Basic Skill
Counter Tackle
Short Status
Jump+3

Bestiary

Headgear
Wizard Robe
Rubber Shoes

Invitation, Threaten, Solution, Insult, Negotiate, Refute
Accumulate, Heal



Oogthecaveman
Male
Libra
71
43
Monk
Item
Abandon
Equip Bow
Levitate

Ultimus Bow

Ribbon
Black Costume
Dracula Mantle

Purification
X-Potion, Ether, Hi-Ether, Antidote, Phoenix Down



Bad1dea
Female
Aries
76
76
Archer
Black Magic
Hamedo
Secret Hunt
Jump+3

Lightning Bow

Green Beret
Rubber Costume
Sprint Shoes

Charge+1, Charge+2, Charge+3, Charge+7, Charge+10
Fire, Bolt 3, Bolt 4, Ice 4, Death, Flare



Scotobot
Male
Sagittarius
44
71
Mime

Critical Quick
Equip Armor
Waterwalking


Genji Shield
Leather Helmet
Mythril Armor
108 Gems

Mimic


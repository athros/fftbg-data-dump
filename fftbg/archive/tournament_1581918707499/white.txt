Player: !White
Team: White Team
Palettes: White/Blue



Minina Bella
Female
Capricorn
46
68
Summoner
Punch Art
Counter Tackle
Defense UP
Ignore Height

Oak Staff

Holy Miter
Wizard Outfit
Leather Mantle

Moogle, Golem, Carbunkle, Leviathan, Salamander
Spin Fist



Gibbindef
Female
Aries
73
73
Priest
Item
Damage Split
Equip Bow
Waterwalking

Long Bow

Golden Hairpin
Adaman Vest
Battle Boots

Cure 2, Cure 3, Cure 4, Protect 2, Esuna
Potion, Antidote, Eye Drop, Soft



TheMurkGnome
Male
Pisces
64
59
Knight
Punch Art
Abandon
Doublehand
Waterwalking

Defender

Barbuta
Leather Armor
Cursed Ring

Weapon Break, Surging Sword
Pummel, Secret Fist, Purification



Ominnous
Male
Capricorn
67
40
Ninja
Elemental
MP Restore
Equip Sword
Swim

Diamond Sword
Kikuichimoji
Black Hood
Earth Clothes
Sprint Shoes

Shuriken, Bomb
Water Ball, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind

Player: !Black
Team: Black Team
Palettes: Black/Red



Turbn
Female
Aries
63
75
Time Mage
Jump
Absorb Used MP
Short Charge
Jump+1

White Staff

Green Beret
Chameleon Robe
Spike Shoes

Haste, Immobilize, Float, Reflect, Stabilize Time
Level Jump8, Vertical Jump6



Zagorsek
Monster
Gemini
68
62
Chocobo










ZombiFrancis
Female
Sagittarius
77
60
Wizard
Charge
MP Restore
Secret Hunt
Swim

Mage Masher

Triangle Hat
Linen Robe
Elf Mantle

Fire 2, Bolt 2, Bolt 3, Ice, Ice 2, Ice 3, Ice 4, Empower, Frog
Charge+1, Charge+2, Charge+3, Charge+5



CITOtheMOSQUITO
Male
Gemini
59
44
Calculator
Black Magic
HP Restore
Equip Bow
Levitate

Ultimus Bow

Triangle Hat
Power Sleeve
Jade Armlet

CT, Height, Prime Number, 3
Fire, Fire 2, Bolt 2, Bolt 4

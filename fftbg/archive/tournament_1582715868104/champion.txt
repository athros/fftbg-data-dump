Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



ValtonZenola
Male
Capricorn
59
61
Squire
Draw Out
Counter
Equip Knife
Ignore Height

Rod
Gold Shield
Crystal Helmet
Chain Vest
Red Shoes

Throw Stone, Heal, Yell, Cheer Up, Scream
Muramasa



Tithonus
Female
Virgo
61
76
Thief
Draw Out
Speed Save
Equip Armor
Teleport

Hidden Knife

Crystal Helmet
Silk Robe
Dracula Mantle

Steal Helmet, Steal Armor
Murasame, Muramasa, Kikuichimoji



Tougou
Female
Libra
67
50
Geomancer
Time Magic
Counter
Magic Attack UP
Ignore Terrain

Diamond Sword
Venetian Shield
Headgear
Chameleon Robe
Angel Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Haste, Haste 2, Immobilize, Quick, Demi 2, Stabilize Time



Hasterious
Male
Virgo
62
55
Ninja
Charge
Counter Flood
Equip Gun
Levitate

Stone Gun
Glacier Gun
Holy Miter
Wizard Outfit
Angel Ring

Shuriken, Dictionary
Charge+3, Charge+10

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



RyuTsuno
Female
Sagittarius
51
39
Knight
Punch Art
Speed Save
Defend
Teleport

Chaos Blade

Leather Helmet
Carabini Mail
108 Gems

Head Break, Armor Break, Weapon Break, Dark Sword
Pummel, Secret Fist, Purification, Revive, Seal Evil



Mizucrux
Female
Taurus
49
68
Geomancer
Draw Out
MA Save
Equip Polearm
Move+3

Cypress Rod
Aegis Shield
Holy Miter
Linen Robe
Elf Mantle

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Koutetsu, Murasame, Muramasa, Kikuichimoji



Nickelbank
Male
Capricorn
78
61
Summoner
Steal
Speed Save
Equip Gun
Lava Walking

Mythril Gun

Headgear
Brigandine
108 Gems

Moogle, Shiva, Ramuh, Carbunkle, Silf, Fairy, Lich, Cyclops
Steal Weapon, Steal Status



Dexef
Male
Aquarius
70
51
Lancer
Talk Skill
MA Save
Equip Axe
Fly

Slasher
Crystal Shield
Barbuta
Gold Armor
Leather Mantle

Level Jump8, Vertical Jump2
Solution, Death Sentence, Mimic Daravon, Refute

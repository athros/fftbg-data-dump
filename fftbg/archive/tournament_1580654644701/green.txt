Player: !Green
Team: Green Team
Palettes: Green/White



Minina Bella
Female
Aries
81
52
Dancer
Jump
Abandon
Equip Polearm
Move+3

Panther Bag

Flash Hat
Earth Clothes
Battle Boots

Slow Dance, Disillusion, Nameless Dance, Nether Demon
Level Jump5, Vertical Jump2



Itzena
Monster
Taurus
66
71
Red Chocobo










MohawkBigNOS
Female
Taurus
78
56
Knight
Dance
MP Restore
Equip Bow
Move+2

Ragnarok

Circlet
Light Robe
Defense Ring

Speed Break, Mind Break
Wiznaibus, Slow Dance, Disillusion, Dragon Pit



Insonder
Male
Sagittarius
66
67
Summoner
Jump
MA Save
Martial Arts
Ignore Terrain



Green Beret
Wizard Robe
Magic Ring

Moogle, Ifrit, Titan, Carbunkle, Leviathan, Salamander, Silf, Cyclops
Level Jump4, Vertical Jump6

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



L2 Sentinel
Male
Aquarius
50
56
Archer
Elemental
Counter
Beastmaster
Swim

Hunting Bow
Flame Shield
Red Hood
Wizard Outfit
Genji Gauntlet

Charge+5, Charge+7
Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard



LoLDrones
Male
Scorpio
73
44
Ninja
Summon Magic
Brave Up
Attack UP
Ignore Height

Spell Edge
Assassin Dagger
Feather Hat
Brigandine
Reflect Ring

Knife
Moogle, Shiva, Ifrit, Golem, Carbunkle, Bahamut, Odin, Fairy, Cyclops



Trowaba
Male
Cancer
58
50
Ninja
Jump
Regenerator
Defend
Swim

Scorpion Tail
Hidden Knife
Ribbon
Wizard Outfit
Elf Mantle

Shuriken
Level Jump5, Vertical Jump8



MynenARS
Male
Aries
80
74
Archer
Item
HP Restore
Equip Polearm
Levitate

Mythril Spear
Round Shield
Red Hood
Leather Outfit
Sprint Shoes

Charge+1, Charge+2, Charge+3
Potion, Ether, Maiden's Kiss

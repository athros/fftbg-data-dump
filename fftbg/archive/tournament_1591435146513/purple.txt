Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Creggers
Male
Aries
68
47
Mime

Auto Potion
Martial Arts
Teleport



Flash Hat
Clothes
108 Gems

Mimic




Zachara
Female
Virgo
69
43
Samurai
Battle Skill
Counter
Equip Knife
Move-HP Up

Cultist Dagger

Circlet
Plate Mail
Sprint Shoes

Asura, Kiyomori, Muramasa, Kikuichimoji
Armor Break, Speed Break, Power Break, Mind Break, Justice Sword



RaIshtar
Male
Pisces
70
47
Knight
Punch Art
Counter Tackle
Beastmaster
Ignore Terrain

Long Sword
Platinum Shield
Crystal Helmet
Carabini Mail
Battle Boots

Armor Break, Weapon Break, Speed Break, Power Break, Mind Break
Pummel, Earth Slash, Purification, Revive



WitchHunterIX
Male
Gemini
58
64
Geomancer
Item
Abandon
Beastmaster
Jump+3

Hydra Bag
Gold Shield
Feather Hat
Wizard Robe
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down

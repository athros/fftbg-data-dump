Player: !Black
Team: Black Team
Palettes: Black/Red



Einhanderz
Female
Gemini
63
60
Squire
Dance
Brave Save
Equip Armor
Move+1

Broad Sword
Gold Shield
Barbuta
Linen Robe
Sprint Shoes

Throw Stone, Heal, Yell, Wish
Witch Hunt, Disillusion



CassiePhoenix
Female
Aries
47
80
Summoner
Dance
Parry
Short Charge
Fly

Ice Rod

Red Hood
White Robe
Defense Ring

Moogle, Shiva, Ifrit, Golem, Leviathan
Last Dance



Almontys
Female
Cancer
79
59
Summoner
Charge
Parry
Sicken
Move+1

Dragon Rod

Twist Headband
Silk Robe
Cursed Ring

Moogle, Shiva, Ifrit, Titan, Odin, Leviathan
Charge+3, Charge+4, Charge+20



DAC169
Monster
Libra
44
75
Pisco Demon








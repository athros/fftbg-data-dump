Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



GatsbysGhost
Monster
Taurus
50
69
Bomb










SephDarkheart
Female
Scorpio
46
62
Lancer
Black Magic
Dragon Spirit
Equip Gun
Jump+2

Bloody Strings
Aegis Shield
Barbuta
Bronze Armor
Rubber Shoes

Level Jump2, Vertical Jump4
Bolt, Bolt 3, Ice 2, Ice 4



Fenaen
Male
Capricorn
76
69
Geomancer
Throw
PA Save
Doublehand
Move-MP Up

Mythril Sword

Thief Hat
Chain Vest
Magic Ring

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Bomb, Stick



RaIshtar
Female
Scorpio
39
75
Chemist
Punch Art
Counter Magic
Dual Wield
Move+2

Hydra Bag
Panther Bag
Flash Hat
Clothes
Wizard Mantle

Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down
Pummel, Purification, Chakra, Revive

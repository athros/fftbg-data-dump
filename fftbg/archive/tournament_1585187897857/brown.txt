Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ra1kia
Monster
Cancer
53
61
Explosive










Leakimiko
Female
Taurus
42
62
Summoner
Steal
Earplug
Short Charge
Move-MP Up

Wizard Rod

Green Beret
Wizard Robe
Reflect Ring

Moogle, Shiva, Titan, Golem, Carbunkle, Odin, Salamander, Silf
Steal Helmet, Steal Shield, Steal Status, Leg Aim



Lodrak
Male
Cancer
76
51
Knight
Yin Yang Magic
Earplug
Maintenance
Move+2

Blood Sword
Ice Shield
Genji Helmet
Silk Robe
Salty Rage

Armor Break, Surging Sword
Spell Absorb, Life Drain, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Petrify, Dark Holy



ForagerCats
Female
Cancer
64
72
Time Mage
Basic Skill
Counter Flood
Secret Hunt
Fly

White Staff

Headgear
Wizard Robe
Feather Boots

Slow, Immobilize, Stabilize Time
Accumulate, Dash, Throw Stone, Heal, Yell, Scream

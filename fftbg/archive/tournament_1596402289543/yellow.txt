Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



RaIshtar
Female
Gemini
52
66
Summoner
Battle Skill
Blade Grasp
Short Charge
Waterbreathing

Bestiary

Flash Hat
Black Costume
Cursed Ring

Moogle, Ramuh, Titan, Odin, Leviathan, Lich, Cyclops
Armor Break, Shield Break, Stasis Sword, Justice Sword



Firesheath
Female
Sagittarius
58
55
Time Mage
Battle Skill
Blade Grasp
Equip Gun
Lava Walking

Bloody Strings

Black Hood
Chameleon Robe
Dracula Mantle

Haste, Stop, Float, Demi 2
Head Break, Armor Break, Shield Break, Magic Break, Surging Sword



Sinnyil2
Male
Gemini
70
43
Knight
Summon Magic
Counter
Maintenance
Teleport 2

Battle Axe
Escutcheon
Barbuta
Leather Armor
Cursed Ring

Weapon Break, Stasis Sword, Justice Sword, Night Sword
Shiva, Ramuh, Ifrit, Carbunkle, Bahamut, Leviathan, Salamander, Fairy



Legitimized
Male
Leo
73
68
Mime

Earplug
Dual Wield
Move+3



Cross Helmet
Mystic Vest
Magic Gauntlet

Mimic


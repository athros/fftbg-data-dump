Player: !Black
Team: Black Team
Palettes: Black/Red



Powergems
Male
Sagittarius
50
46
Thief
Jump
Counter
Magic Defense UP
Lava Walking

Long Sword

Red Hood
Judo Outfit
Wizard Mantle

Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Leg Aim
Level Jump2, Vertical Jump7



Argentbast
Female
Leo
47
75
Mime

Counter
Doublehand
Retreat



Red Hood
Secret Clothes
Feather Boots

Mimic




Error72
Female
Leo
50
49
Geomancer
Yin Yang Magic
Counter Tackle
Magic Attack UP
Jump+1

Giant Axe
Escutcheon
Headgear
Wizard Robe
Genji Gauntlet

Pitfall, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Poison, Life Drain, Confusion Song, Dispel Magic, Petrify



Neerrm
Male
Sagittarius
75
66
Ninja
Basic Skill
Caution
Secret Hunt
Retreat

Assassin Dagger
Mythril Knife
Flash Hat
Clothes
Bracer

Shuriken, Dictionary
Dash, Heal, Fury

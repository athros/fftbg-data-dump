Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DudeMonkey77
Male
Cancer
45
52
Archer
Draw Out
Speed Save
Attack UP
Lava Walking

Gastrafitis
Escutcheon
Flash Hat
Mythril Vest
Power Wrist

Charge+5
Asura, Murasame, Heaven's Cloud, Muramasa



EnemyController
Male
Scorpio
71
74
Chemist
Throw
Arrow Guard
Short Charge
Move+2

Dagger

Triangle Hat
Earth Clothes
Bracer

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Soft, Phoenix Down
Shuriken, Knife



MantisFinch
Female
Libra
45
71
Mediator
Punch Art
Auto Potion
Concentrate
Ignore Terrain

Mythril Knife

Feather Hat
Wizard Outfit
Dracula Mantle

Invitation, Praise, Preach, Death Sentence, Insult, Refute
Spin Fist, Purification



Mesmaster
Female
Pisces
80
55
Monk
White Magic
Blade Grasp
Defend
Jump+1



Thief Hat
Adaman Vest
Leather Mantle

Spin Fist, Pummel, Earth Slash, Purification
Cure 2, Cure 3, Raise, Reraise, Protect, Wall, Esuna

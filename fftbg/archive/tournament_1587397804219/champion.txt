Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



ShintaroNayaka
Monster
Cancer
70
80
King Behemoth










Error72
Male
Aries
35
70
Mediator
Black Magic
Damage Split
Dual Wield
Retreat

Bestiary
Battle Folio
Leather Hat
Wizard Robe
Germinas Boots

Invitation, Persuade, Praise, Preach, Solution, Insult, Negotiate, Mimic Daravon, Refute
Fire 3, Fire 4, Bolt 2, Bolt 4, Ice 4



Mirapoix
Female
Scorpio
79
44
Calculator
Lucavi Skill
Absorb Used MP
Short Status
Ignore Terrain

Battle Folio

Diamond Helmet
Secret Clothes
Bracer

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Alacor
Male
Gemini
72
77
Knight
Time Magic
Counter Flood
Short Charge
Move+3

Defender
Flame Shield
Gold Helmet
Plate Mail
N-Kai Armlet

Armor Break, Shield Break, Weapon Break, Power Break, Stasis Sword
Haste, Quick, Stabilize Time

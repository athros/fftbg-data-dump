Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Leakimiko
Female
Capricorn
67
80
Chemist
Talk Skill
Faith Up
Beastmaster
Waterbreathing

Stone Gun

Feather Hat
Judo Outfit
Feather Mantle

Potion, Hi-Potion, X-Potion, Ether, Maiden's Kiss, Remedy, Phoenix Down
Invitation, Death Sentence, Mimic Daravon



CapnChaos12
Female
Leo
65
54
Geomancer
Draw Out
Parry
Doublehand
Move+3

Kiyomori

Triangle Hat
Judo Outfit
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Sand Storm, Blizzard, Gusty Wind
Heaven's Cloud



ExecutedGiraffe
Male
Libra
64
50
Archer
Black Magic
Dragon Spirit
Magic Defense UP
Fly

Stone Gun
Mythril Shield
Crystal Helmet
Brigandine
Diamond Armlet

Charge+1, Charge+3, Charge+4, Charge+5, Charge+20
Fire 2, Fire 3, Bolt 3, Ice 4



RunicMagus
Female
Leo
59
41
Squire
Summon Magic
Counter Flood
Secret Hunt
Swim

Flail
Buckler
Diamond Helmet
Leather Armor
Leather Mantle

Accumulate, Heal, Yell, Cheer Up
Ifrit, Golem, Carbunkle, Odin, Salamander

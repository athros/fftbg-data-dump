Player: !White
Team: White Team
Palettes: White/Blue



LDSkinny
Female
Scorpio
79
80
Oracle
Summon Magic
Sunken State
Equip Polearm
Teleport

Javelin

Green Beret
Earth Clothes
Chantage

Blind, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze
Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Bahamut, Fairy



Solomongrundy85
Male
Libra
39
79
Lancer
Basic Skill
Absorb Used MP
Sicken
Jump+1

Obelisk
Escutcheon
Crystal Helmet
Leather Armor
Defense Armlet

Level Jump8, Vertical Jump8
Accumulate, Throw Stone, Tickle, Wish



Enkikavlar
Male
Leo
57
51
Geomancer
Throw
Counter
Equip Polearm
Jump+2

Partisan
Venetian Shield
Twist Headband
Mystic Vest
Feather Mantle

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Bomb, Hammer



HaateXIII
Female
Taurus
62
70
Wizard
Punch Art
MA Save
Equip Polearm
Move-MP Up

Cypress Rod

Golden Hairpin
Earth Clothes
Wizard Mantle

Fire, Fire 2, Bolt, Ice 4, Flare
Pummel, Purification, Revive, Seal Evil

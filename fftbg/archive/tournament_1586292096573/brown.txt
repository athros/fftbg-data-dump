Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sinnyil2
Male
Aries
76
76
Ninja
Talk Skill
Hamedo
Attack UP
Levitate

Morning Star
Scorpion Tail
Triangle Hat
Chain Vest
Defense Ring

Bomb, Staff, Dictionary
Invitation, Persuade, Mimic Daravon, Refute



JackOnFire1
Female
Gemini
60
68
Calculator
Bio
Meatbone Slash
Dual Wield
Jump+3

Flame Rod
Poison Rod
Gold Helmet
Bronze Armor
Jade Armlet

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



NoNotBees
Male
Pisces
50
49
Ninja
Talk Skill
Faith Up
Equip Axe
Levitate

Flail
Battle Axe
Feather Hat
Brigandine
Battle Boots

Shuriken, Bomb
Invitation, Persuade, Solution, Negotiate, Rehabilitate



Dynasti
Male
Pisces
44
79
Ninja
Talk Skill
Sunken State
Martial Arts
Retreat



Flash Hat
Clothes
Small Mantle

Shuriken, Bomb, Knife
Preach, Death Sentence, Insult, Refute

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Nekojin
Monster
Cancer
46
53
Archaic Demon










Neerrm
Male
Gemini
41
68
Archer
Battle Skill
Meatbone Slash
Defense UP
Jump+3

Hunting Bow
Gold Shield
Twist Headband
Wizard Outfit
Salty Rage

Charge+2, Charge+3, Charge+4, Charge+7, Charge+10
Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword



Rannock57
Female
Taurus
56
79
Mediator
Steal
Brave Save
Defense UP
Retreat

Blaze Gun

Twist Headband
Robe of Lords
Sprint Shoes

Persuade, Threaten, Preach, Solution, Death Sentence, Insult, Mimic Daravon, Refute
Steal Heart, Steal Armor, Steal Shield, Steal Accessory



ShintaroNayaka
Monster
Leo
64
76
Malboro








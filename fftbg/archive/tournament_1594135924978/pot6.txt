Final Bets: white - 5 bets for 2,522G (25.8%, x2.88); brown - 12 bets for 7,263G (74.2%, x0.35)

white bets:
killth3kid: 1,000G (39.7%, 114,578G)
Firesheath: 617G (24.5%, 617G)
EnemyController: 555G (22.0%, 1,377,565G)
RaIshtar: 250G (9.9%, 15,214G)
captainmilestw: 100G (4.0%, 1,171G)

brown bets:
Nizaha: 1,345G (18.5%, 1,345G)
Jumza: 1,000G (13.8%, 1,781G)
Magicandy: 1,000G (13.8%, 23,738G)
BirbBrainsBot: 1,000G (13.8%, 123,857G)
Forkmore: 1,000G (13.8%, 11,998G)
Nickyfive: 604G (8.3%, 604G)
benticore: 435G (6.0%, 8,708G)
ArashiKurobara: 288G (4.0%, 5,916G)
Avin_Chaos: 200G (2.8%, 10,183G)
datadrivenbot: 200G (2.8%, 55,794G)
Rannock57: 157G (2.2%, 157G)
getthemoneyz: 34G (0.5%, 1,219,397G)

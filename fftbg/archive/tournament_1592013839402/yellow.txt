Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DLJuggernaut
Male
Taurus
66
77
Archer
Steal
Mana Shield
Doublehand
Lava Walking

Hunting Bow

Green Beret
Power Sleeve
Leather Mantle

Charge+1, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Steal Heart, Steal Helmet, Steal Weapon, Steal Status, Leg Aim



Meta Five
Female
Leo
72
65
Time Mage
Yin Yang Magic
Dragon Spirit
Concentrate
Retreat

Gokuu Rod

Leather Hat
Wizard Outfit
Diamond Armlet

Haste, Haste 2, Slow, Stop, Float, Quick, Demi 2, Stabilize Time, Galaxy Stop
Life Drain, Blind Rage, Dispel Magic, Paralyze



ColetteMSLP
Female
Sagittarius
49
51
Time Mage
Basic Skill
Catch
Halve MP
Move+3

Musk Rod

Red Hood
Silk Robe
Dracula Mantle

Haste, Slow, Slow 2, Float, Reflect, Quick, Demi
Heal, Tickle, Yell



I Nod My Head When I Lose
Female
Sagittarius
42
67
Lancer
Time Magic
Caution
Magic Attack UP
Jump+3

Javelin
Platinum Shield
Mythril Helmet
Linen Cuirass
Feather Boots

Level Jump2, Vertical Jump5
Haste, Haste 2, Slow 2, Immobilize, Demi, Demi 2

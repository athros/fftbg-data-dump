Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Acedude1981
Male
Sagittarius
54
44
Mime

MP Restore
Martial Arts
Move+2



Black Hood
Judo Outfit
Feather Mantle

Mimic




DudeMonkey77
Male
Virgo
76
60
Wizard
Throw
Counter
Long Status
Teleport

Flame Rod

Holy Miter
Secret Clothes
Leather Mantle

Fire 3, Fire 4, Bolt, Bolt 2, Ice, Ice 3, Empower, Frog, Death
Shuriken, Bomb, Sword, Stick



Crownjo
Male
Aquarius
77
78
Chemist
Time Magic
HP Restore
Equip Polearm
Jump+2

Obelisk

Twist Headband
Wizard Outfit
Angel Ring

Potion, X-Potion, Antidote, Eye Drop, Remedy, Phoenix Down
Haste, Slow 2, Float, Demi, Demi 2, Stabilize Time



DarrenDinosaurs
Male
Gemini
61
40
Archer
Draw Out
Meatbone Slash
Short Status
Lava Walking

Lightning Bow

Black Hood
Judo Outfit
Feather Boots

Charge+3, Charge+4, Charge+7, Charge+20
Asura, Koutetsu, Murasame, Kiyomori, Kikuichimoji

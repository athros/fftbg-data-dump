Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



CaptainAdmiralSPATULA
Monster
Aries
70
68
Coeurl










Ribbiks
Female
Scorpio
58
52
Samurai
Battle Skill
Counter Flood
Equip Knife
Retreat

Mythril Knife

Diamond Helmet
Crystal Mail
Battle Boots

Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji
Shield Break, Power Break, Mind Break



Neerrm
Female
Libra
73
55
Mediator
Yin Yang Magic
Dragon Spirit
Equip Gun
Ignore Height

Ramia Harp

Twist Headband
Black Costume
Defense Ring

Invitation, Threaten, Preach, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate
Spell Absorb, Life Drain, Pray Faith, Zombie, Sleep



Snowfats
Male
Libra
48
58
Monk
Elemental
Regenerator
Sicken
Retreat



Feather Hat
Power Sleeve
108 Gems

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Revive
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

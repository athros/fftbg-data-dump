Player: !Red
Team: Red Team
Palettes: Red/Brown



Lifebregin
Female
Taurus
63
69
Summoner
Steal
Arrow Guard
Equip Bow
Teleport

Ice Bow

Thief Hat
Clothes
Red Shoes

Moogle, Ifrit, Titan, Carbunkle, Leviathan, Silf
Steal Heart, Steal Helmet, Steal Armor



Bioticism
Male
Virgo
47
59
Archer
Item
Distribute
Attack UP
Waterwalking

Snipe Bow
Escutcheon
Holy Miter
Black Costume
Germinas Boots

Charge+1, Charge+2, Charge+3
Potion, Echo Grass, Soft, Holy Water, Phoenix Down



Gongonono
Female
Cancer
44
79
Geomancer
Jump
Critical Quick
Defense UP
Lava Walking

Kiyomori
Genji Shield
Thief Hat
Adaman Vest
Cherche

Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Level Jump5, Vertical Jump7



Theseawolf1
Female
Cancer
67
59
Mime

Arrow Guard
Equip Shield
Waterwalking


Gold Shield
Flash Hat
White Robe
108 Gems

Mimic


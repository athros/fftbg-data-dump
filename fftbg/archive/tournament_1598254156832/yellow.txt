Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DaveStrider55
Monster
Leo
80
52
Archaic Demon










NicoSavoy
Female
Capricorn
62
60
Mime

Dragon Spirit
Dual Wield
Jump+1



Twist Headband
White Robe
108 Gems

Mimic




Superdevon1
Female
Aquarius
76
46
Wizard
Yin Yang Magic
Mana Shield
Beastmaster
Move+1

Blind Knife

Feather Hat
Mystic Vest
Magic Gauntlet

Bolt, Bolt 2, Bolt 3, Ice 2, Ice 3, Death
Blind, Poison, Spell Absorb, Pray Faith, Silence Song, Confusion Song, Dispel Magic, Sleep



Sinnyil2
Female
Capricorn
48
50
Knight
Basic Skill
Faith Save
Short Charge
Move+1

Slasher
Venetian Shield
Mythril Helmet
Mythril Armor
Small Mantle

Head Break, Magic Break, Power Break, Dark Sword, Night Sword
Accumulate, Dash, Yell, Cheer Up, Fury

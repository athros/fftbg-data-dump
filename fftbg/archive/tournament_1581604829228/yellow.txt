Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



JarMustard
Male
Pisces
62
63
Wizard
Summon Magic
Absorb Used MP
Equip Shield
Move-MP Up

Dagger
Genji Shield
Golden Hairpin
Judo Outfit
Sprint Shoes

Fire 2, Fire 3, Bolt, Bolt 3, Bolt 4, Ice 4, Death
Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Odin, Lich



Claytonio
Male
Gemini
72
76
Oracle
Summon Magic
Faith Up
Magic Attack UP
Fly

Gokuu Rod

Flash Hat
Light Robe
Spike Shoes

Spell Absorb, Blind Rage, Foxbird, Dispel Magic, Sleep, Petrify
Moogle, Ifrit, Golem, Leviathan



Deciimos
Female
Gemini
58
61
Summoner
Punch Art
Mana Shield
Short Charge
Move-MP Up

Mace of Zeus

Holy Miter
Wizard Robe
Jade Armlet

Moogle, Ifrit, Carbunkle, Leviathan, Fairy
Wave Fist, Purification, Revive, Seal Evil



Reddwind
Male
Aquarius
40
77
Knight
Throw
HP Restore
Dual Wield
Waterbreathing

Ancient Sword
Diamond Sword
Barbuta
Silk Robe
Small Mantle

Head Break, Weapon Break, Magic Break, Power Break, Mind Break
Wand

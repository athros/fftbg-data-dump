Player: !Red
Team: Red Team
Palettes: Red/Brown



Sotehr
Female
Capricorn
61
65
Wizard
Draw Out
Damage Split
Equip Sword
Move+2

Save the Queen

Flash Hat
Chameleon Robe
Leather Mantle

Fire 2, Ice, Ice 3, Ice 4, Death
Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa



Ominnous
Male
Aquarius
67
80
Lancer
Battle Skill
Counter Flood
Magic Defense UP
Move-HP Up

Cypress Rod
Crystal Shield
Diamond Helmet
Reflect Mail
Red Shoes

Level Jump5, Vertical Jump5
Head Break, Shield Break, Weapon Break, Power Break, Mind Break, Justice Sword, Surging Sword



Hamborn
Male
Leo
60
58
Bard
Time Magic
PA Save
Magic Attack UP
Move+1

Fairy Harp

Flash Hat
Black Costume
Genji Gauntlet

Battle Song, Magic Song, Nameless Song, Diamond Blade, Sky Demon, Hydra Pit
Haste, Slow, Slow 2, Immobilize, Reflect, Quick, Stabilize Time



CaptainAdmiralSPATULA
Female
Gemini
50
78
Thief
Punch Art
Faith Up
Magic Defense UP
Ignore Height

Spell Edge

Green Beret
Earth Clothes
Feather Boots

Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Leg Aim
Pummel, Secret Fist, Purification, Revive

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Thaetreis
Male
Aquarius
43
46
Ninja
Elemental
HP Restore
Equip Gun
Ignore Terrain

Battle Folio
Bestiary
Headgear
Clothes
Red Shoes

Shuriken
Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Lanshaft
Male
Pisces
70
56
Lancer
Battle Skill
Counter Magic
Equip Bow
Jump+3

Night Killer
Diamond Shield
Gold Helmet
Black Robe
Cursed Ring

Level Jump4, Vertical Jump8
Head Break, Magic Break, Mind Break, Justice Sword, Dark Sword



Nhammen
Male
Virgo
50
71
Knight
Yin Yang Magic
Meatbone Slash
Long Status
Waterbreathing

Giant Axe
Flame Shield
Grand Helmet
Chain Mail
Power Wrist

Head Break, Armor Break, Speed Break, Mind Break
Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic



Phi Sig
Male
Sagittarius
74
44
Knight
Jump
Counter Magic
Long Status
Ignore Height

Materia Blade
Escutcheon
Mythril Helmet
Linen Cuirass
N-Kai Armlet

Armor Break, Power Break, Justice Sword
Level Jump8, Vertical Jump2

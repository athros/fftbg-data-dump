Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SQUiDSQUARKLIN
Male
Capricorn
53
43
Samurai
Sing
Meatbone Slash
Martial Arts
Jump+2



Mythril Helmet
Leather Armor
Chantage

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Battle Song, Diamond Blade



Quadh0nk
Male
Taurus
39
42
Samurai
Throw
Arrow Guard
Concentrate
Move-MP Up

Heaven's Cloud

Barbuta
Carabini Mail
Diamond Armlet

Asura, Koutetsu, Heaven's Cloud, Muramasa
Shuriken



Evewho
Female
Taurus
68
50
Chemist
Yin Yang Magic
PA Save
Sicken
Move+3

Panther Bag

Leather Hat
Brigandine
Jade Armlet

Eye Drop, Soft, Phoenix Down
Spell Absorb, Life Drain, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify



RageImmortaI
Male
Sagittarius
65
43
Knight
Black Magic
MA Save
Concentrate
Move+1

Excalibur
Venetian Shield
Crystal Helmet
White Robe
Red Shoes

Armor Break, Speed Break, Power Break
Fire 2, Bolt, Bolt 2, Ice 2, Ice 3

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mesmaster
Male
Libra
56
57
Archer
White Magic
Parry
Doublehand
Jump+1

Night Killer

Black Hood
Judo Outfit
Rubber Shoes

Charge+1, Charge+3, Charge+10
Cure 3, Cure 4, Raise, Raise 2, Regen, Shell, Shell 2, Wall



Maakur
Male
Gemini
67
69
Oracle
Black Magic
Absorb Used MP
Equip Armor
Jump+3

Iron Fan

Iron Helmet
Diamond Armor
Red Shoes

Spell Absorb, Life Drain, Pray Faith, Doubt Faith, Zombie, Silence Song, Dispel Magic, Paralyze
Fire 2, Bolt 2, Ice 3



Joewcarson
Male
Capricorn
50
62
Samurai
White Magic
Counter
Doublehand
Levitate

Heaven's Cloud

Iron Helmet
Platinum Armor
Chantage

Asura, Kiyomori, Muramasa, Kikuichimoji
Cure, Cure 4, Raise, Reraise, Esuna



Fenaen
Male
Libra
45
52
Lancer
Throw
HP Restore
Dual Wield
Move+2

Octagon Rod
Ivory Rod
Leather Helmet
Leather Armor
Reflect Ring

Level Jump5, Vertical Jump7
Shuriken

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Red



Deathmaker06
Male
Pisces
48
69
Geomancer
Draw Out
Abandon
Equip Sword
Move+3

Asura Knife
Mythril Shield
Twist Headband
Chameleon Robe
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Murasame, Kiyomori



Evewho
Monster
Libra
78
40
Holy Dragon










ALY327
Male
Leo
48
59
Lancer
Summon Magic
Abandon
Dual Wield
Ignore Terrain

Holy Lance
Mythril Spear
Platinum Helmet
Diamond Armor
Small Mantle

Level Jump8, Vertical Jump7
Moogle, Ifrit, Titan, Carbunkle, Silf



DAC169
Female
Cancer
45
79
Calculator
Yin Yang Magic
Speed Save
Equip Axe
Levitate

Healing Staff

Triangle Hat
Mystic Vest
Dracula Mantle

CT, Height, Prime Number, 4, 3
Poison, Spell Absorb, Life Drain, Doubt Faith, Confusion Song, Dispel Magic, Paralyze, Sleep

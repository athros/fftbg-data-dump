Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Lythe Caraker
Female
Aquarius
51
70
Thief
Elemental
MA Save
Doublehand
Fly

Diamond Sword

Feather Hat
Leather Outfit
Defense Ring

Gil Taking, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Pitfall, Hell Ivy, Hallowed Ground, Quicksand, Blizzard



Solomongrundy85
Female
Pisces
73
74
Wizard
Item
HP Restore
Equip Knife
Move+3

Dragon Rod

Twist Headband
Linen Robe
Wizard Mantle

Fire, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 3, Empower, Death
Potion, Hi-Potion, Maiden's Kiss



YaBoy125
Male
Capricorn
74
72
Knight
Time Magic
Mana Shield
Equip Bow
Jump+1

Ice Bow
Aegis Shield
Crystal Helmet
Leather Armor
Dracula Mantle

Shield Break, Weapon Break, Magic Break, Mind Break, Stasis Sword, Dark Sword, Surging Sword
Haste, Slow 2, Stop, Float, Reflect, Demi, Stabilize Time, Meteor



Serperemagus
Female
Scorpio
49
79
Dancer
Black Magic
PA Save
Dual Wield
Ignore Height

Cute Bag
Star Bag
Golden Hairpin
Black Robe
N-Kai Armlet

Wiznaibus, Slow Dance, Nameless Dance, Void Storage
Fire, Fire 2, Fire 4, Bolt 2, Ice, Frog

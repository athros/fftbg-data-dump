Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



YaBoy125
Male
Sagittarius
59
69
Knight
Throw
Counter
Beastmaster
Fly

Giant Axe
Platinum Shield
Leather Helmet
Platinum Armor
Diamond Armlet

Armor Break, Night Sword, Surging Sword
Shuriken, Knife, Dictionary



Avin Chaos
Monster
Aries
68
63
Holy Dragon










Vorap
Female
Cancer
48
60
Priest
Item
Caution
Throw Item
Ignore Height

Healing Staff

Holy Miter
Chameleon Robe
Magic Ring

Cure, Raise, Regen, Shell 2, Esuna
Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down



Gelwain
Male
Serpentarius
72
75
Archer
White Magic
MP Restore
Doublehand
Move+2

Windslash Bow

Golden Hairpin
Adaman Vest
Defense Armlet

Charge+1, Charge+3
Cure, Raise, Raise 2, Shell 2, Esuna

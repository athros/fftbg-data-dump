Final Bets: red - 12 bets for 13,188G (86.9%, x0.15); green - 7 bets for 1,981G (13.1%, x6.66)

red bets:
NicoSavoy: 7,420G (56.3%, 371,029G)
DamnThatShark: 1,002G (7.6%, 1,002G)
BirbBrainsBot: 1,000G (7.6%, 118,815G)
sinnyil2: 874G (6.6%, 1,748G)
Chuckolator: 655G (5.0%, 9,470G)
prince_rogers_nelson_: 638G (4.8%, 638G)
randgridr: 500G (3.8%, 3,847G)
superdevon1: 398G (3.0%, 39,890G)
gorgewall: 201G (1.5%, 5,639G)
AllInBot: 200G (1.5%, 200G)
datadrivenbot: 200G (1.5%, 47,088G)
zarath79: 100G (0.8%, 100G)

green bets:
getthemoneyz: 692G (34.9%, 1,399,306G)
ruleof5: 500G (25.2%, 10,108G)
douchetron: 357G (18.0%, 357G)
poGpopE: 200G (10.1%, 14,418G)
CT_5_Holy: 100G (5.0%, 3,654G)
ar_tactic: 100G (5.0%, 83,245G)
dtrain332: 32G (1.6%, 3,284G)

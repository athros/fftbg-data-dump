Player: !White
Team: White Team
Palettes: White/Blue



Reddwind
Male
Virgo
48
42
Ninja
Sing
Speed Save
Equip Sword
Move-HP Up

Murasame
Ninja Edge
Feather Hat
Power Sleeve
Bracer

Shuriken, Dictionary
Battle Song, Space Storage



B00tyt00ts
Male
Gemini
58
53
Thief
Summon Magic
Speed Save
Martial Arts
Waterwalking



Black Hood
Mystic Vest
Battle Boots

Steal Helmet, Steal Status
Moogle, Ifrit, Titan, Golem, Bahamut, Odin, Fairy, Cyclops



YoungSax
Monster
Aries
77
58
Pisco Demon










Evdoggity
Male
Cancer
76
74
Knight
Sing
Counter Tackle
Maintenance
Ignore Terrain

Chaos Blade
Crystal Shield
Platinum Helmet
Mythril Armor
Wizard Mantle

Head Break, Armor Break, Weapon Break, Speed Break, Justice Sword, Night Sword
Cheer Song, Hydra Pit

Final Bets: white - 5 bets for 6,731G (43.1%, x1.32); purple - 8 bets for 8,891G (56.9%, x0.76)

white bets:
Thyrandaal: 4,000G (59.4%, 72,932G)
ColetteMSLP: 1,000G (14.9%, 28,380G)
BirbBrainsBot: 1,000G (14.9%, 44,729G)
Xoomwaffle: 429G (6.4%, 429G)
douchetron: 302G (4.5%, 302G)

purple bets:
AllInBot: 5,600G (63.0%, 5,600G)
VolgraTheMoose: 1,073G (12.1%, 1,073G)
TheChainNerd: 764G (8.6%, 764G)
ruleof5: 500G (5.6%, 11,245G)
SephDarkheart: 364G (4.1%, 364G)
DouglasDragonThePoet: 300G (3.4%, 2,131G)
datadrivenbot: 200G (2.2%, 38,909G)
getthemoneyz: 90G (1.0%, 1,370,556G)

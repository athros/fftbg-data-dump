Final Bets: red - 7 bets for 9,677G (79.1%, x0.26); blue - 5 bets for 2,560G (20.9%, x3.78)

red bets:
helpimabug: 6,566G (67.9%, 13,132G)
NIghtdew14: 1,000G (10.3%, 44,631G)
prince_rogers_nelson_: 552G (5.7%, 552G)
randgridr: 518G (5.4%, 518G)
Artea_: 500G (5.2%, 6,195G)
Miku_Shikhu: 341G (3.5%, 341G)
datadrivenbot: 200G (2.1%, 38,989G)

blue bets:
ColetteMSLP: 1,000G (39.1%, 23,663G)
BirbBrainsBot: 1,000G (39.1%, 42,153G)
getthemoneyz: 310G (12.1%, 1,370,350G)
AllInBot: 200G (7.8%, 200G)
DouglasDragonThePoet: 50G (2.0%, 1,785G)

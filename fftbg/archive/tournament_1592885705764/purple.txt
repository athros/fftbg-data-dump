Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mesmaster
Female
Taurus
57
54
Samurai
Steal
Sunken State
Magic Attack UP
Move+2

Dragon Whisker

Gold Helmet
Carabini Mail
Defense Armlet

Koutetsu, Murasame, Kiyomori
Gil Taking, Steal Helmet, Steal Weapon



CosmicTactician
Female
Cancer
79
62
Mediator
Elemental
PA Save
Equip Shield
Teleport

Bestiary
Escutcheon
Green Beret
Black Robe
Red Shoes

Persuade, Praise, Preach, Death Sentence, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Sairentozon7
Male
Cancer
52
44
Bard
White Magic
Meatbone Slash
Concentrate
Lava Walking

Bloody Strings

Golden Hairpin
Clothes
Red Shoes

Angel Song, Battle Song, Magic Song, Last Song, Diamond Blade
Raise, Protect, Shell, Esuna



Chuckolator
Female
Libra
42
71
Calculator
Yin Yang Magic
Damage Split
Equip Armor
Retreat

Bestiary

Green Beret
Leather Armor
Red Shoes

CT, Height, Prime Number, 4, 3
Spell Absorb, Life Drain, Doubt Faith, Zombie, Confusion Song, Dispel Magic, Sleep, Petrify

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BStarTV
Male
Libra
79
80
Oracle
Steal
Counter
Equip Sword
Teleport

Chaos Blade

Leather Hat
Black Robe
Angel Ring

Blind, Spell Absorb, Pray Faith, Blind Rage, Dispel Magic, Paralyze, Petrify
Steal Shield, Steal Weapon, Steal Accessory



CrownOfHorns
Female
Aries
61
62
Calculator
White Magic
HP Restore
Equip Gun
Retreat

Mythril Gun

Flash Hat
Chameleon Robe
Dracula Mantle

CT, 5, 3
Protect 2, Shell 2, Wall, Esuna, Holy



JoeykinsX
Male
Virgo
48
51
Thief
Time Magic
Counter Flood
Attack UP
Jump+2

Hidden Knife

Triangle Hat
Black Costume
Genji Gauntlet

Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Arm Aim
Haste, Slow, Slow 2, Stop, Reflect, Quick, Demi, Demi 2



Grininda
Female
Libra
63
56
Dancer
Elemental
Sunken State
Secret Hunt
Jump+2

Cute Bag

Triangle Hat
Mystic Vest
Wizard Mantle

Witch Hunt, Slow Dance, Disillusion, Obsidian Blade
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind, Lava Ball

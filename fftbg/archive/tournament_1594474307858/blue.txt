Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Thaetreis
Monster
Aquarius
58
71
Swine










Breakdown777
Female
Capricorn
68
80
Knight
Item
Parry
Throw Item
Ignore Height

Chaos Blade
Genji Shield
Gold Helmet
Chain Mail
Germinas Boots

Weapon Break, Speed Break, Night Sword, Explosion Sword
Hi-Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass



Prince Rogers Nelson
Female
Virgo
77
58
Chemist
Black Magic
Arrow Guard
Maintenance
Waterwalking

Dagger

Golden Hairpin
Wizard Outfit
Elf Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Fire, Fire 3, Fire 4, Bolt 3, Ice 2, Ice 3, Empower, Frog



KasugaiRoastedPeas
Female
Leo
39
55
Wizard
Time Magic
Arrow Guard
Short Status
Move+2

Blind Knife

Ribbon
Judo Outfit
Bracer

Fire 3, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Frog
Haste, Slow 2, Immobilize, Demi

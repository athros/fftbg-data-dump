Final Bets: green - 6 bets for 5,604G (64.8%, x0.54); white - 11 bets for 3,049G (35.2%, x1.84)

green bets:
ko2q: 1,598G (28.5%, 1,598G)
superdevon1: 1,329G (23.7%, 66,493G)
NovaKnight21: 1,319G (23.5%, 1,319G)
KasugaiRoastedPeas: 1,135G (20.3%, 1,135G)
Lydian_C: 123G (2.2%, 7,524G)
MinBetBot: 100G (1.8%, 21,806G)

white bets:
Mesmaster: 947G (31.1%, 947G)
synra609: 500G (16.4%, 1,056G)
OniXiion: 350G (11.5%, 4,973G)
xBizzy: 232G (7.6%, 816G)
AllInBot: 200G (6.6%, 200G)
datadrivenbot: 200G (6.6%, 59,220G)
ArrenJevleth: 200G (6.6%, 1,839G)
BirbBrainsBot: 145G (4.8%, 50,700G)
gorgewall: 101G (3.3%, 748G)
DAC169: 100G (3.3%, 1,110G)
getthemoneyz: 74G (2.4%, 1,618,042G)

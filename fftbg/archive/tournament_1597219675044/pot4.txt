Final Bets: purple - 11 bets for 9,252G (78.5%, x0.27); brown - 6 bets for 2,528G (21.5%, x3.66)

purple bets:
Mesmaster: 3,507G (37.9%, 3,507G)
Lydian_C: 1,200G (13.0%, 8,519G)
BirbBrainsBot: 1,000G (10.8%, 49,550G)
Sairentozon7: 1,000G (10.8%, 90,889G)
ko2q: 808G (8.7%, 808G)
AllInBot: 587G (6.3%, 587G)
OniXiion: 400G (4.3%, 3,738G)
ArrenJevleth: 250G (2.7%, 1,266G)
datadrivenbot: 200G (2.2%, 58,963G)
DAC169: 200G (2.2%, 853G)
MinBetBot: 100G (1.1%, 21,979G)

brown bets:
KasugaiRoastedPeas: 688G (27.2%, 688G)
superdevon1: 669G (26.5%, 66,971G)
WhiteTigress: 500G (19.8%, 5,664G)
getthemoneyz: 338G (13.4%, 1,618,226G)
xBizzy: 232G (9.2%, 681G)
gorgewall: 101G (4.0%, 2,299G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Kolonel Panic
Female
Serpentarius
78
58
Squire
Black Magic
HP Restore
Magic Defense UP
Move-HP Up

Battle Axe

Golden Hairpin
Black Costume
Spike Shoes

Accumulate, Dash, Heal, Yell, Wish, Ultima
Fire, Fire 2, Fire 3, Fire 4, Bolt, Bolt 3, Ice 2, Frog



8BitAngel
Male
Leo
67
55
Time Mage
Punch Art
Hamedo
Attack UP
Jump+2

Oak Staff

Headgear
Earth Clothes
Cursed Ring

Slow 2, Immobilize, Demi 2, Stabilize Time, Meteor
Secret Fist, Chakra, Revive



Unclesambam
Male
Capricorn
50
54
Ninja
Punch Art
Hamedo
Equip Polearm
Retreat

Holy Lance
Gokuu Rod
Thief Hat
Mythril Vest
Elf Mantle

Knife, Staff, Spear, Dictionary
Secret Fist, Purification



NoxeGS
Male
Aries
42
78
Knight
Elemental
Mana Shield
Equip Bow
Move-MP Up

Coral Sword
Venetian Shield
Bronze Helmet
Plate Mail
Cursed Ring

Magic Break, Dark Sword
Water Ball, Static Shock, Will-O-Wisp, Blizzard, Lava Ball

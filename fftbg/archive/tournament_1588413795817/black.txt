Player: !Black
Team: Black Team
Palettes: Black/Red



LorianFaust
Monster
Sagittarius
49
55
King Behemoth










RyuTsuno
Male
Virgo
54
71
Lancer
White Magic
HP Restore
Dual Wield
Move-HP Up

Holy Lance
Javelin
Leather Helmet
Crystal Mail
Setiemson

Level Jump8, Vertical Jump8
Cure, Cure 3, Raise, Shell



Ewan E
Male
Taurus
62
65
Summoner
Elemental
Dragon Spirit
Equip Sword
Lava Walking

Rune Blade

Flash Hat
Silk Robe
108 Gems

Moogle, Shiva, Ramuh, Titan, Odin, Lich
Hallowed Ground, Static Shock, Sand Storm, Lava Ball



Captainpeaches
Male
Aquarius
56
59
Geomancer
Time Magic
Earplug
Maintenance
Ignore Height

Coral Sword
Escutcheon
Twist Headband
Wizard Outfit
Dracula Mantle

Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Haste 2, Slow, Slow 2, Stop, Float, Quick, Demi, Demi 2, Stabilize Time, Meteor

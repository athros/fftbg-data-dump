Final Bets: blue - 11 bets for 6,813G (64.7%, x0.55); yellow - 8 bets for 3,715G (35.3%, x1.83)

blue bets:
FoxtrotNovemberCharlie: 1,465G (21.5%, 1,465G)
HaplessOne: 1,357G (19.9%, 20,130G)
ungabunga_bot: 1,000G (14.7%, 309,565G)
BirbBrainsBot: 1,000G (14.7%, 172,866G)
Zachara: 815G (12.0%, 62,315G)
PotionDweller: 324G (4.8%, 324G)
OverlordCuddles: 200G (2.9%, 4,434G)
twelfthrootoftwo: 200G (2.9%, 3,537G)
ThePineappleSalesman: 200G (2.9%, 23,416G)
getthemoneyz: 152G (2.2%, 613,969G)
DeathTaxesAndAnime: 100G (1.5%, 32,940G)

yellow bets:
thunderducker: 1,700G (45.8%, 3,294G)
Rislyeu: 649G (17.5%, 1,299G)
crimson_hellkite: 556G (15.0%, 11,056G)
KasugaiRoastedPeas: 399G (10.7%, 2,863G)
LorianFaust: 161G (4.3%, 161G)
CrashCat: 100G (2.7%, 2,927G)
datadrivenbot: 100G (2.7%, 11,502G)
Selppa: 50G (1.3%, 300G)

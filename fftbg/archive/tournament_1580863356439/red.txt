Player: !Red
Team: Red Team
Palettes: Red/Brown



Ultimainferno
Monster
Capricorn
60
47
Steel Giant










Sye990
Female
Libra
51
74
Oracle
Time Magic
Counter
Equip Shield
Jump+3

Octagon Rod
Venetian Shield
Twist Headband
Black Robe
Reflect Ring

Zombie, Dispel Magic, Petrify
Quick, Demi 2



BigDLicious91
Male
Capricorn
46
51
Knight
Steal
Parry
Defend
Move+1

Ragnarok

Barbuta
Chameleon Robe
Small Mantle

Shield Break
Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



IronMongrel
Male
Leo
63
59
Samurai
Punch Art
Hamedo
Equip Bow
Levitate

Dragon Whisker

Gold Helmet
Chain Mail
Genji Gauntlet

Murasame, Kikuichimoji
Spin Fist, Purification, Chakra, Revive

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



KitchTowel
Female
Aries
66
70
Geomancer
Summon Magic
HP Restore
Magic Defense UP
Ignore Terrain

Heaven's Cloud
Buckler
Green Beret
Power Sleeve
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Moogle, Ramuh, Titan, Carbunkle, Leviathan



MuguTheMangler
Male
Scorpio
51
42
Mime

Speed Save
Doublehand
Jump+2



Red Hood
Black Robe
108 Gems

Mimic




Shellback27
Male
Capricorn
62
47
Archer
Elemental
Hamedo
Equip Gun
Jump+2

Battle Folio
Gold Shield
Twist Headband
Wizard Outfit
Bracer

Charge+3, Charge+4
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Hales Bopp It
Male
Libra
67
62
Time Mage
Steal
Hamedo
Magic Attack UP
Jump+1

Bestiary

Thief Hat
Judo Outfit
Defense Ring

Haste 2, Slow 2, Immobilize, Reflect, Demi 2, Stabilize Time
Gil Taking, Steal Heart, Steal Accessory, Steal Status, Arm Aim

Final Bets: red - 6 bets for 3,414G (33.8%, x1.96); brown - 13 bets for 6,678G (66.2%, x0.51)

red bets:
E_Ballard: 2,128G (62.3%, 2,128G)
helpimabug: 718G (21.0%, 718G)
Tarheels218: 308G (9.0%, 3,743G)
gorgewall: 101G (3.0%, 6,339G)
ko2q: 100G (2.9%, 1,812G)
theBinklive: 59G (1.7%, 238G)

brown bets:
Thyrandaal: 2,000G (29.9%, 474,859G)
BirbBrainsBot: 1,000G (15.0%, 51,782G)
Sairentozon7: 1,000G (15.0%, 8,016G)
FalseLobster: 600G (9.0%, 4,258G)
Lazarus_DS: 500G (7.5%, 1,443G)
TheDeeyo: 400G (6.0%, 1,620G)
skipsandwiches: 344G (5.2%, 344G)
CosmicTactician: 200G (3.0%, 30,505G)
datadrivenbot: 200G (3.0%, 61,897G)
ar_tactic: 150G (2.2%, 78,628G)
AllInBot: 100G (1.5%, 100G)
neerrm: 100G (1.5%, 25,648G)
getthemoneyz: 84G (1.3%, 1,684,094G)

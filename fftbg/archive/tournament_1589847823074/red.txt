Player: !Red
Team: Red Team
Palettes: Red/Brown



DLJuggernaut
Male
Aries
67
42
Lancer
Talk Skill
Brave Save
Long Status
Waterwalking

Spear
Aegis Shield
Leather Helmet
Crystal Mail
Battle Boots

Level Jump8, Vertical Jump7
Praise, Solution, Insult, Rehabilitate



KasugaiRoastedPeas
Male
Aries
69
44
Squire
Charge
Counter Magic
Magic Defense UP
Swim

Bow Gun
Flame Shield
Headgear
Diamond Armor
Spike Shoes

Heal, Fury, Scream
Charge+2



Roofiepops
Male
Capricorn
78
50
Oracle
Jump
HP Restore
Equip Knife
Retreat

Assassin Dagger

Green Beret
Earth Clothes
Defense Ring

Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze, Sleep
Level Jump2, Vertical Jump5



FoeSquirrel
Female
Taurus
45
55
Squire
White Magic
Dragon Spirit
Equip Armor
Jump+2

Slasher
Mythril Shield
Iron Helmet
Black Robe
Red Shoes

Accumulate, Throw Stone, Heal, Tickle, Fury, Wish
Cure 2, Cure 3, Raise, Protect, Shell, Esuna

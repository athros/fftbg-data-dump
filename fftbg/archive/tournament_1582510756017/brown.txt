Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ninjapenguim
Male
Gemini
38
54
Samurai
Time Magic
Hamedo
Magic Attack UP
Ignore Height

Murasame

Cross Helmet
Plate Mail
Genji Gauntlet

Koutetsu, Murasame, Kikuichimoji
Haste, Float, Reflect, Meteor



Galkife
Male
Virgo
54
69
Mime

PA Save
Monster Talk
Move-MP Up



Green Beret
Judo Outfit
Feather Mantle

Mimic




Harbinger605
Male
Aries
59
59
Geomancer
Basic Skill
Counter
Short Charge
Jump+2

Slasher
Gold Shield
Green Beret
Black Robe
Power Wrist

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Dash, Heal, Tickle, Cheer Up



Powergems
Male
Capricorn
46
76
Mediator
White Magic
Absorb Used MP
Dual Wield
Retreat

Stone Gun
Glacier Gun
Red Hood
Wizard Robe
Spike Shoes

Persuade, Solution, Negotiate, Refute
Cure, Cure 2, Cure 3, Cure 4, Raise, Protect, Protect 2, Shell, Wall, Esuna

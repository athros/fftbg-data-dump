Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Kahlindra
Female
Libra
48
61
Monk
Time Magic
Regenerator
Attack UP
Move-HP Up



Green Beret
Brigandine
Jade Armlet

Pummel, Wave Fist, Purification, Seal Evil
Haste, Slow, Immobilize, Demi, Stabilize Time



Arkthepieking
Female
Capricorn
63
59
Ninja
Yin Yang Magic
HP Restore
Beastmaster
Waterbreathing

Hidden Knife
Kunai
Flash Hat
Wizard Outfit
Red Shoes

Shuriken, Wand
Spell Absorb, Zombie, Confusion Song, Paralyze, Sleep, Petrify



Fairgrim
Female
Cancer
46
80
Wizard
Dance
Auto Potion
Equip Shield
Ignore Terrain

Poison Rod
Flame Shield
Barette
Chain Vest
Leather Mantle

Fire, Fire 4, Ice, Ice 3, Empower, Flare
Wiznaibus, Polka Polka, Dragon Pit



Reddwind
Male
Virgo
53
78
Summoner
Yin Yang Magic
Brave Up
Maintenance
Jump+2

Thunder Rod

Feather Hat
Silk Robe
Defense Ring

Moogle, Ramuh, Titan, Golem, Carbunkle, Salamander, Silf
Blind, Pray Faith, Sleep

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Mizucrux
Male
Taurus
75
61
Archer
Elemental
Meatbone Slash
Doublehand
Jump+2

Hunting Bow

Black Hood
Brigandine
Rubber Shoes

Charge+2, Charge+20
Hell Ivy, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Ramzah666
Female
Gemini
51
54
Knight
Jump
Regenerator
Short Status
Move-HP Up

Defender

Cross Helmet
Plate Mail
Reflect Ring

Shield Break, Magic Break, Speed Break, Mind Break, Justice Sword
Level Jump8, Vertical Jump8



Jptawok
Male
Sagittarius
69
58
Wizard
Charge
MA Save
Equip Bow
Swim

Silver Bow

Flash Hat
Brigandine
Small Mantle

Fire 4, Bolt, Bolt 2, Ice 3, Ice 4
Charge+4, Charge+5, Charge+7, Charge+10



WireLord
Male
Cancer
80
77
Squire
Item
Distribute
Equip Polearm
Move+2

Partisan

Iron Helmet
Black Costume
Wizard Mantle

Dash, Throw Stone, Heal, Tickle, Wish
Potion, Hi-Ether, Antidote, Holy Water, Phoenix Down

Player: !Red
Team: Red Team
Palettes: Red/Brown



Lohakilo
Monster
Virgo
70
74
Tiamat










Rogueain
Male
Gemini
75
60
Knight
Elemental
Caution
Equip Gun
Lava Walking

Romanda Gun
Buckler
Barbuta
Gold Armor
Battle Boots

Head Break, Armor Break, Speed Break, Justice Sword, Surging Sword
Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



IndecisiveNinja
Female
Aries
42
57
Ninja
Jump
Caution
Equip Sword
Lava Walking

Koutetsu Knife
Hidden Knife
Holy Miter
Power Sleeve
Wizard Mantle

Shuriken, Stick, Wand
Level Jump8, Vertical Jump6



Lanshaft
Male
Libra
46
40
Priest
Item
Mana Shield
Throw Item
Move-HP Up

Gold Staff

Twist Headband
Earth Clothes
Diamond Armlet

Cure, Cure 3, Cure 4, Protect, Shell, Wall, Esuna, Holy
Potion, Hi-Potion, Holy Water, Remedy, Phoenix Down

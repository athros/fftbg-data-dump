Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Bryon W
Female
Scorpio
69
80
Geomancer
White Magic
Parry
Martial Arts
Jump+2


Buckler
Twist Headband
Secret Clothes
Bracer

Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Cure, Cure 4, Raise, Raise 2, Regen, Esuna



Nizaha
Monster
Aries
65
48
Goblin










PlatinumPlume
Female
Leo
68
50
Wizard
White Magic
Dragon Spirit
Martial Arts
Jump+1

Dagger

Triangle Hat
Silk Robe
Spike Shoes

Fire 2, Fire 4, Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 2, Ice 4, Frog
Cure 2, Cure 3, Raise, Protect 2, Shell 2, Wall, Esuna



Daveb
Female
Sagittarius
46
59
Ninja
Charge
Earplug
Equip Gun
Teleport

Papyrus Codex
Battle Folio
Headgear
Chain Vest
Small Mantle

Shuriken
Charge+4, Charge+5

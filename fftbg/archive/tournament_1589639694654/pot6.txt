Final Bets: white - 9 bets for 5,212G (34.9%, x1.86); brown - 13 bets for 9,707G (65.1%, x0.54)

white bets:
E_Ballard: 1,584G (30.4%, 1,584G)
catfashions: 1,500G (28.8%, 8,968G)
Aldrammech: 1,000G (19.2%, 10,413G)
Breakdown777: 500G (9.6%, 10,850G)
JLinkletter: 200G (3.8%, 2,137G)
foofermoofer: 128G (2.5%, 128G)
Lawndough: 100G (1.9%, 1,282G)
datadrivenbot: 100G (1.9%, 17,891G)
Rislyeu: 100G (1.9%, 2,984G)

brown bets:
wooyuji: 1,500G (15.5%, 16,898G)
Dexsana: 1,475G (15.2%, 1,475G)
maakur_: 1,127G (11.6%, 1,127G)
ungabunga_bot: 1,000G (10.3%, 517,156G)
BirbBrainsBot: 1,000G (10.3%, 97,923G)
Lionhermit: 1,000G (10.3%, 53,460G)
getthemoneyz: 698G (7.2%, 685,223G)
fenaen: 600G (6.2%, 11,113G)
Indainity: 500G (5.2%, 3,566G)
Nova_riety: 252G (2.6%, 252G)
MibudemoneyeKyo: 200G (2.1%, 8,895G)
marin1987: 200G (2.1%, 4,497G)
Arbitae: 155G (1.6%, 959G)

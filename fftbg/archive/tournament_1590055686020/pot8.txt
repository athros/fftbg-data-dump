Final Bets: green - 7 bets for 3,005G (59.2%, x0.69); champion - 5 bets for 2,071G (40.8%, x1.45)

green bets:
roqqqpsi: 1,300G (43.3%, 11,826G)
NovaKnight21: 548G (18.2%, 548G)
Evewho: 500G (16.6%, 7,437G)
d4rr1n: 307G (10.2%, 307G)
Communis: 150G (5.0%, 7,667G)
serperemagus: 100G (3.3%, 108G)
datadrivenbot: 100G (3.3%, 23,461G)

champion bets:
UmaiJam: 1,000G (48.3%, 7,052G)
BirbBrainsBot: 610G (29.5%, 33,134G)
Digitalsocrates: 248G (12.0%, 248G)
gorgewall: 201G (9.7%, 13,144G)
getthemoneyz: 12G (0.6%, 682,855G)

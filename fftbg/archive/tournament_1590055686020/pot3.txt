Final Bets: white - 5 bets for 2,328G (39.4%, x1.54); black - 8 bets for 3,574G (60.6%, x0.65)

white bets:
UmaiJam: 1,000G (43.0%, 6,205G)
NovaKnight21: 548G (23.5%, 548G)
ShintaroNayaka: 395G (17.0%, 395G)
BirbBrainsBot: 275G (11.8%, 31,562G)
getthemoneyz: 110G (4.7%, 682,184G)

black bets:
volgrathemoose: 1,126G (31.5%, 2,253G)
Evewho: 500G (14.0%, 7,576G)
Digitalsocrates: 497G (13.9%, 497G)
Lanshaft: 400G (11.2%, 400G)
Scotty297: 350G (9.8%, 2,483G)
gorgewall: 301G (8.4%, 13,307G)
asherban: 300G (8.4%, 1,795G)
datadrivenbot: 100G (2.8%, 23,498G)

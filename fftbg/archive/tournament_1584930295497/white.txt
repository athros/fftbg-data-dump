Player: !White
Team: White Team
Palettes: White/Blue



Cenalte
Female
Leo
47
55
Monk
Throw
Auto Potion
Short Charge
Teleport



Barette
Adaman Vest
Power Wrist

Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil
Shuriken, Bomb, Axe, Wand



Kellios11
Female
Pisces
76
59
Samurai
Battle Skill
Meatbone Slash
Secret Hunt
Move-HP Up

Holy Lance

Circlet
Crystal Mail
Small Mantle

Asura, Koutetsu, Kiyomori
Magic Break, Power Break, Surging Sword



Lyner87
Male
Scorpio
54
63
Squire
Throw
Absorb Used MP
Secret Hunt
Jump+3

Battle Axe
Ice Shield
Ribbon
Leather Outfit
Jade Armlet

Heal, Tickle, Yell
Shuriken, Bomb



Basmal
Male
Scorpio
55
80
Archer
Basic Skill
Damage Split
Sicken
Waterwalking

Silver Bow

Cross Helmet
Wizard Outfit
Jade Armlet

Charge+1, Charge+2, Charge+3, Charge+4, Charge+7
Accumulate, Dash, Heal, Tickle

Player: !zChamp
Team: Champion Team
Palettes: Green/White



RjA0zcOQ96
Monster
Aquarius
80
53
Apanda










LDSkinny
Female
Sagittarius
44
58
Oracle
Time Magic
Hamedo
Equip Polearm
Ignore Height

Gungnir

Golden Hairpin
Mystic Vest
Spike Shoes

Life Drain, Pray Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Dark Holy
Slow, Reflect, Demi 2, Galaxy Stop



ThePineappleSalesman
Female
Aquarius
75
68
Wizard
Jump
Counter Magic
Short Charge
Move+2

Orichalcum

Flash Hat
Black Robe
Feather Boots

Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Ice 4
Level Jump5, Vertical Jump6



Arch8000
Female
Scorpio
60
77
Ninja
Steal
MP Restore
Equip Bow
Swim

Cross Bow
Bow Gun
Thief Hat
Clothes
Elf Mantle

Shuriken, Wand
Steal Weapon

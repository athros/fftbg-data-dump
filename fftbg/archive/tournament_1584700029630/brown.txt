Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Powergems
Female
Aquarius
54
38
Monk
Time Magic
Distribute
Short Status
Jump+3



Holy Miter
Brigandine
Cursed Ring

Spin Fist, Pummel, Purification, Chakra, Revive
Haste, Haste 2, Slow 2, Float, Demi, Meteor



Tresbienhien
Male
Aries
54
49
Monk
Talk Skill
Critical Quick
Sicken
Ignore Terrain



Thief Hat
Clothes
Magic Ring

Earth Slash, Secret Fist, Purification, Revive
Invitation, Death Sentence, Insult, Mimic Daravon, Refute



7Cerulean7
Female
Aquarius
75
49
Geomancer
White Magic
Speed Save
Dual Wield
Swim

Battle Axe
Giant Axe
Flash Hat
Chain Vest
Jade Armlet

Pitfall, Water Ball, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure 3, Raise, Raise 2, Esuna



Maeveen
Male
Scorpio
55
64
Mime

Auto Potion
Maintenance
Teleport



Green Beret
Leather Outfit
Magic Ring

Mimic


Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PlatinumPlume
Female
Capricorn
75
51
Dancer
Black Magic
Counter Magic
Martial Arts
Ignore Height

Cashmere

Holy Miter
Wizard Robe
Angel Ring

Witch Hunt, Slow Dance, Disillusion, Last Dance, Obsidian Blade
Fire, Fire 2, Fire 3, Fire 4, Flare



Bad1dea
Male
Libra
51
51
Mediator
Black Magic
PA Save
Concentrate
Move-MP Up

Romanda Gun

Black Hood
Brigandine
Jade Armlet

Persuade, Praise, Refute, Rehabilitate
Fire, Fire 2, Bolt 3, Ice 3, Ice 4, Death



WireLord
Female
Gemini
71
70
Time Mage
Yin Yang Magic
Dragon Spirit
Beastmaster
Lava Walking

White Staff

Green Beret
Chameleon Robe
Wizard Mantle

Stop, Immobilize, Demi
Blind, Pray Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify



LuckyLuckLuc2
Male
Scorpio
65
68
Time Mage
Elemental
Earplug
Short Charge
Move+2

Wizard Staff

Thief Hat
Chain Vest
Genji Gauntlet

Haste, Haste 2, Slow 2, Stop, Stabilize Time
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball

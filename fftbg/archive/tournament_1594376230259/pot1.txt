Final Bets: red - 11 bets for 7,293G (49.6%, x1.01); blue - 8 bets for 7,402G (50.4%, x0.99)

red bets:
BirbBrainsBot: 1,000G (13.7%, 14,802G)
RoboticNomad: 1,000G (13.7%, 6,223G)
Mesmaster: 1,000G (13.7%, 41,623G)
Sairentozon7: 1,000G (13.7%, 5,201G)
CassiePhoenix: 824G (11.3%, 824G)
Hasterious: 756G (10.4%, 756G)
dogsandcatsand: 666G (9.1%, 5,933G)
Lanshaft: 476G (6.5%, 10,913G)
ColetteMSLP: 300G (4.1%, 1,668G)
superdevon1: 139G (1.9%, 1,395G)
getthemoneyz: 132G (1.8%, 1,250,867G)

blue bets:
ko2q: 2,213G (29.9%, 4,427G)
randgridr: 1,511G (20.4%, 1,511G)
SkyridgeZero: 1,500G (20.3%, 46,604G)
NovaKnight21: 1,000G (13.5%, 20,775G)
upvla: 678G (9.2%, 7,807G)
datadrivenbot: 200G (2.7%, 62,356G)
unclebearboy: 200G (2.7%, 8,401G)
dantayystv: 100G (1.4%, 1,054G)

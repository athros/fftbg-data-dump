Player: !Black
Team: Black Team
Palettes: Black/Red



Cho Pin
Female
Taurus
68
45
Oracle
Charge
Counter
Short Status
Move+2

Bestiary

Twist Headband
Rubber Costume
Feather Boots

Poison, Spell Absorb, Life Drain, Blind Rage, Confusion Song, Dispel Magic, Sleep, Petrify
Charge+2, Charge+5, Charge+10



Butterbelljedi
Female
Sagittarius
47
42
Wizard
Throw
PA Save
Equip Axe
Ignore Terrain

Morning Star

Triangle Hat
Chain Vest
Defense Armlet

Fire 4, Ice 4, Empower, Frog, Death, Flare
Sword, Ninja Sword, Wand



Skillomono
Male
Pisces
72
44
Geomancer
Battle Skill
Counter Magic
Martial Arts
Ignore Height

Slasher
Aegis Shield
Black Hood
Silk Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Blizzard, Gusty Wind
Dark Sword



Ko2q
Monster
Gemini
52
42
Red Chocobo








Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Holyonline
Male
Capricorn
57
48
Wizard
Summon Magic
Faith Up
Magic Defense UP
Ignore Height

Flame Rod

Feather Hat
Brigandine
Feather Mantle

Fire 3, Fire 4, Bolt, Bolt 2, Bolt 3
Moogle, Ramuh, Titan, Golem, Carbunkle, Bahamut, Leviathan, Lich



IndecisiveNinja
Monster
Aquarius
44
45
Red Chocobo










BurningSaph
Male
Gemini
68
37
Knight
Sing
MA Save
Equip Knife
Jump+2

Ragnarok

Bronze Helmet
Wizard Robe
Genji Gauntlet

Head Break, Armor Break, Speed Break, Mind Break
Magic Song, Nameless Song



Nizdra
Female
Virgo
48
53
Priest
Basic Skill
Brave Up
Beastmaster
Retreat

Gold Staff

Feather Hat
Linen Robe
Magic Gauntlet

Cure 3, Cure 4, Raise, Raise 2, Protect, Shell 2, Esuna
Heal, Cheer Up, Scream

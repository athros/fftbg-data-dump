Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LanseDM
Female
Capricorn
59
55
Mediator
Charge
Mana Shield
Equip Bow
Waterbreathing

Hunting Bow

Headgear
Chain Vest
Battle Boots

Persuade, Preach, Solution, Death Sentence, Mimic Daravon
Charge+2



Maakur
Male
Cancer
58
66
Ninja
Talk Skill
Counter Tackle
Equip Gun
Fly

Papyrus Codex
Papyrus Codex
Headgear
Clothes
Battle Boots

Shuriken
Invitation, Persuade, Insult, Negotiate, Mimic Daravon



B0shii
Male
Cancer
61
48
Mime

Critical Quick
Monster Talk
Move-MP Up



Green Beret
Brigandine
Small Mantle

Mimic




Nifboy
Male
Libra
76
63
Knight
Jump
Critical Quick
Dual Wield
Fly

Ice Brand
Battle Axe
Bronze Helmet
Chain Mail
Leather Mantle

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Justice Sword
Level Jump4, Vertical Jump5

Final Bets: red - 10 bets for 8,184G (77.5%, x0.29); blue - 9 bets for 2,372G (22.5%, x3.45)

red bets:
Willjin: 3,000G (36.7%, 105,725G)
WireLord: 1,476G (18.0%, 1,476G)
BirbBrainsBot: 1,000G (12.2%, 64,624G)
NovaKnight21: 933G (11.4%, 933G)
Draconis345: 500G (6.1%, 18,506G)
TakKerna: 500G (6.1%, 1,000G)
3ngag3: 274G (3.3%, 16,774G)
datadrivenbot: 200G (2.4%, 64,598G)
DeathRtopper: 200G (2.4%, 1,567G)
gorgewall: 101G (1.2%, 12,450G)

blue bets:
reddwind_: 500G (21.1%, 10,309G)
Evewho: 500G (21.1%, 4,046G)
Sairentozon7: 436G (18.4%, 436G)
Raixelol: 316G (13.3%, 25,882G)
MemoriesofFinal: 232G (9.8%, 232G)
Absalom_20: 120G (5.1%, 7,805G)
AllInBot: 100G (4.2%, 100G)
Meyples: 100G (4.2%, 1,328G)
getthemoneyz: 68G (2.9%, 1,762,532G)

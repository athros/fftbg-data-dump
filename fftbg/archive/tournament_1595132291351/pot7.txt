Final Bets: green - 10 bets for 5,365G (46.2%, x1.16); purple - 9 bets for 6,246G (53.8%, x0.86)

green bets:
Aldrammech: 1,297G (24.2%, 1,297G)
superdevon1: 1,231G (22.9%, 6,157G)
Laserman1000: 700G (13.0%, 31,700G)
TasisSai: 520G (9.7%, 1,021G)
TheMurkGnome: 516G (9.6%, 516G)
silentperogy: 500G (9.3%, 30,038G)
gorgewall: 201G (3.7%, 7,551G)
datadrivenbot: 200G (3.7%, 34,441G)
Treafa: 100G (1.9%, 1,436G)
Belkra: 100G (1.9%, 1,009G)

purple bets:
randgridr: 1,610G (25.8%, 1,610G)
Thyrandaal: 1,157G (18.5%, 1,157G)
twelfthrootoftwo: 1,030G (16.5%, 2,020G)
BirbBrainsBot: 1,000G (16.0%, 94,231G)
KyleWonToLiveForever: 541G (8.7%, 541G)
prince_rogers_nelson_: 480G (7.7%, 480G)
AllInBot: 200G (3.2%, 200G)
JDogg2K4: 180G (2.9%, 180G)
getthemoneyz: 48G (0.8%, 1,325,760G)

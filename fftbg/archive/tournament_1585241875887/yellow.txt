Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



PlatinumSalty
Female
Libra
47
61
Summoner
Punch Art
Sunken State
Equip Bow
Teleport 2

Lightning Bow

Thief Hat
Linen Robe
Diamond Armlet

Ifrit, Carbunkle, Leviathan, Salamander, Silf, Fairy, Lich
Earth Slash, Purification, Chakra, Revive



AlenaZarek
Male
Capricorn
78
49
Thief
Talk Skill
Caution
Monster Talk
Ignore Terrain

Air Knife

Feather Hat
Judo Outfit
Jade Armlet

Gil Taking, Leg Aim
Persuade, Threaten, Preach, Death Sentence, Mimic Daravon, Refute



Broomstar
Female
Sagittarius
73
51
Ninja
Steal
Counter
Equip Polearm
Waterwalking

Musk Rod
Partisan
Holy Miter
Adaman Vest
Power Wrist

Shuriken, Knife, Sword, Stick, Dictionary
Steal Shield, Steal Weapon, Steal Status, Arm Aim



Nomoment
Male
Capricorn
47
49
Oracle
White Magic
Speed Save
Dual Wield
Jump+2

Ivory Rod
Octagon Rod
Holy Miter
Light Robe
Cursed Ring

Blind, Life Drain, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic
Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Shell, Esuna, Magic Barrier

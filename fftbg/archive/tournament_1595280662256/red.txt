Player: !Red
Team: Red Team
Palettes: Red/Brown



0v3rr8d
Female
Aries
50
55
Calculator
Black Magic
Damage Split
Short Charge
Retreat

Rod

Flash Hat
Leather Outfit
Magic Ring

CT, 4, 3
Bolt 2, Ice, Ice 3, Flare



Pplvee1
Male
Aries
46
60
Knight
Elemental
Distribute
Equip Bow
Lava Walking

Bow Gun
Round Shield
Gold Helmet
Genji Armor
Setiemson

Armor Break, Magic Break, Speed Break, Power Break, Dark Sword
Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



LDSkinny
Female
Sagittarius
58
73
Oracle
Steal
Earplug
Short Charge
Jump+3

Iron Fan

Triangle Hat
Mystic Vest
Germinas Boots

Blind, Poison, Pray Faith, Zombie, Silence Song, Confusion Song, Sleep
Steal Status, Leg Aim



Daveb
Male
Libra
62
61
Ninja
Jump
Damage Split
Defense UP
Move-MP Up

Dagger
Short Edge
Flash Hat
Adaman Vest
Leather Mantle

Bomb, Knife, Staff, Stick, Dictionary
Level Jump3, Vertical Jump3

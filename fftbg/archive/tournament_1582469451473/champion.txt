Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Draconis345
Male
Aries
72
81
Chemist
Yin Yang Magic
Damage Split
Doublehand
Move+1

Star Bag

Twist Headband
Judo Outfit
Feather Boots

X-Potion, Ether, Hi-Ether, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Life Drain, Doubt Faith, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy



Treapvort
Female
Scorpio
77
61
Knight
Talk Skill
Abandon
Defense UP
Ignore Terrain

Long Sword
Flame Shield
Iron Helmet
Light Robe
Spike Shoes

Head Break, Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Praise, Threaten, Negotiate, Refute



Volgrathemoose
Male
Leo
78
75
Monk
Battle Skill
Sunken State
Attack UP
Retreat



Green Beret
Brigandine
Cursed Ring

Spin Fist, Earth Slash, Purification, Revive, Seal Evil
Armor Break, Justice Sword



PorcelainCow
Female
Aquarius
67
46
Ninja
Charge
Counter
Equip Knife
Move+2

Morning Star
Koga Knife
Green Beret
Earth Clothes
Defense Armlet

Shuriken, Bomb, Wand, Dictionary
Charge+1, Charge+20

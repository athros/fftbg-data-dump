Final Bets: white - 8 bets for 2,964G (54.8%, x0.83); champion - 7 bets for 2,446G (45.2%, x1.21)

white bets:
BirbBrainsBot: 1,000G (33.7%, 17,959G)
CT_5_Holy: 500G (16.9%, 1,596G)
blorpy_: 394G (13.3%, 19,723G)
iBardic: 358G (12.1%, 358G)
wildetrys: 312G (10.5%, 2,799G)
datadrivenbot: 200G (6.7%, 70,899G)
MinBetBot: 100G (3.4%, 5,964G)
reddwind_: 100G (3.4%, 41,315G)

champion bets:
Dragon_RW: 1,106G (45.2%, 1,106G)
Draconis345: 500G (20.4%, 14,303G)
getthemoneyz: 278G (11.4%, 1,653,721G)
WhiteTigress: 200G (8.2%, 2,868G)
HASTERIOUS: 161G (6.6%, 3,237G)
gorgewall: 101G (4.1%, 5,135G)
AllInBot: 100G (4.1%, 100G)

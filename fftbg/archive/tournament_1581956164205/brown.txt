Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Nizdra
Male
Leo
63
48
Archer
Elemental
Hamedo
Martial Arts
Move+3


Platinum Shield
Triangle Hat
Black Costume
Feather Mantle

Charge+2, Charge+3, Charge+4, Charge+5, Charge+7
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm



Waterwatereverywhere
Female
Cancer
48
74
Squire
Battle Skill
Earplug
Equip Bow
Lava Walking

Perseus Bow

Black Hood
Bronze Armor
Rubber Shoes

Yell, Wish
Shield Break, Mind Break



MyFakeLife
Female
Capricorn
45
61
Chemist
Basic Skill
PA Save
Equip Armor
Teleport

Star Bag

Iron Helmet
Genji Armor
Feather Boots

Hi-Potion, X-Potion, Elixir, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Throw Stone, Heal, Tickle, Yell, Wish, Scream, Ultima



Alc Trinity
Female
Libra
48
40
Chemist
Jump
Dragon Spirit
Defense UP
Fly

Dagger

Thief Hat
Brigandine
Spike Shoes

Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Soft, Remedy, Phoenix Down
Level Jump8, Vertical Jump2

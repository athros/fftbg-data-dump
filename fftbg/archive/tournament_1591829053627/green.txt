Player: !Green
Team: Green Team
Palettes: Green/White



ColetteMSLP
Male
Aquarius
69
69
Monk
Yin Yang Magic
Abandon
Defense UP
Waterbreathing



Flash Hat
Leather Outfit
Power Wrist

Earth Slash, Purification, Chakra, Revive
Spell Absorb, Zombie, Dispel Magic, Paralyze, Sleep



Oobs56
Monster
Taurus
50
67
Vampire










Benticore
Male
Leo
69
79
Summoner
Talk Skill
Distribute
Equip Shield
Levitate

Gold Staff
Genji Shield
Red Hood
White Robe
Jade Armlet

Moogle, Ramuh, Carbunkle, Bahamut, Leviathan
Invitation, Praise, Threaten, Insult, Refute, Rehabilitate



Alanaire
Male
Taurus
50
75
Priest
Battle Skill
MA Save
Long Status
Lava Walking

Morning Star

Red Hood
Light Robe
Magic Gauntlet

Cure, Cure 2, Cure 3, Raise, Protect 2, Esuna, Holy
Armor Break, Shield Break, Stasis Sword, Dark Sword, Explosion Sword

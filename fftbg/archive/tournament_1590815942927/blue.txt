Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kaidykat
Male
Aquarius
39
52
Samurai
Throw
Counter Flood
Concentrate
Teleport

Spear

Mythril Helmet
Mythril Armor
Wizard Mantle

Asura, Heaven's Cloud, Kikuichimoji
Shuriken, Knife, Staff



Computerghoul
Monster
Aquarius
60
51
Dark Behemoth










MiseryKoss
Male
Cancer
62
59
Knight
Jump
Counter
Concentrate
Fly

Battle Axe
Flame Shield
Barbuta
Mythril Armor
Magic Ring

Head Break, Weapon Break, Justice Sword, Surging Sword
Level Jump8, Vertical Jump5



DudeMonkey77
Male
Aquarius
66
61
Bard
Summon Magic
Regenerator
Equip Polearm
Waterbreathing

Obelisk

Triangle Hat
Brigandine
Leather Mantle

Battle Song, Nameless Song, Last Song
Moogle, Golem, Leviathan, Silf, Fairy, Lich

Player: !Green
Team: Green Team
Palettes: Green/White



SilverChangeling
Female
Sagittarius
70
61
Oracle
White Magic
Meatbone Slash
Long Status
Fly

Iron Fan

Twist Headband
Linen Robe
Magic Gauntlet

Life Drain, Doubt Faith, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Cure 3, Reraise, Protect, Protect 2, Shell 2, Wall, Esuna



Klednar21
Male
Cancer
70
64
Ninja
Black Magic
Abandon
Equip Knife
Teleport

Scorpion Tail
Poison Rod
Twist Headband
Leather Outfit
Reflect Ring

Shuriken, Bomb, Ninja Sword, Wand
Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 2, Ice 3, Empower



Lube Squid
Monster
Gemini
57
78
Grenade










ArrenJevleth
Male
Cancer
47
62
Mediator
Item
Dragon Spirit
Defense UP
Waterbreathing

Blast Gun

Black Hood
Judo Outfit
Dracula Mantle

Persuade, Praise, Preach, Negotiate, Refute, Rehabilitate
Hi-Potion, Ether, Hi-Ether, Echo Grass, Phoenix Down

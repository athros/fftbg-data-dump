Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Archelous
Male
Virgo
66
66
Time Mage
Throw
Absorb Used MP
Equip Sword
Fly

Muramasa

Red Hood
Linen Robe
Magic Ring

Haste, Slow 2, Immobilize, Demi, Demi 2, Stabilize Time
Wand



Laserman1000
Male
Leo
64
38
Archer
Sing
Catch
Sicken
Move+1

Mythril Gun
Gold Shield
Black Hood
Power Sleeve
Bracer

Charge+1, Charge+3, Charge+4, Charge+5, Charge+10
Cheer Song, Nameless Song



GoAwayImBaitin
Female
Libra
69
55
Ninja
Item
Catch
Long Status
Levitate

Spell Edge
Ninja Edge
Black Hood
Earth Clothes
Feather Boots

Shuriken
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Soft, Remedy, Phoenix Down



Fspll
Male
Pisces
38
54
Knight
Steal
Faith Up
Long Status
Move+1

Coral Sword
Aegis Shield
Genji Helmet
Leather Armor
Magic Ring

Armor Break, Weapon Break, Magic Break, Speed Break, Power Break, Stasis Sword
Steal Heart, Steal Armor, Steal Shield, Steal Accessory, Arm Aim, Leg Aim

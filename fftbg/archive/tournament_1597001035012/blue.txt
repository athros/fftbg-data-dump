Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Victoriolue
Female
Virgo
68
63
Priest
Yin Yang Magic
Parry
Short Charge
Ignore Height

Morning Star

Black Hood
Chameleon Robe
Germinas Boots

Cure 2, Cure 4, Raise, Reraise, Protect 2, Shell, Shell 2, Esuna
Life Drain, Pray Faith, Foxbird, Confusion Song



Skjodo
Male
Libra
71
45
Thief
Talk Skill
Damage Split
Short Status
Jump+1

Orichalcum

Black Hood
Mythril Vest
Battle Boots

Gil Taking, Steal Helmet, Steal Weapon, Steal Status, Leg Aim
Invitation, Persuade, Praise, Preach, Negotiate, Refute



Nhammen
Female
Virgo
68
80
Dancer
Draw Out
Sunken State
Dual Wield
Retreat

Ryozan Silk
Persia
Twist Headband
Wizard Robe
Power Wrist

Polka Polka, Disillusion, Nameless Dance, Void Storage
Murasame, Muramasa



Laserman1000
Female
Taurus
58
80
Summoner
Jump
Damage Split
Short Charge
Ignore Terrain

Poison Rod

Leather Hat
Earth Clothes
Bracer

Shiva, Ramuh, Ifrit, Carbunkle, Bahamut, Silf, Fairy, Cyclops
Level Jump3, Vertical Jump7

Final Bets: red - 6 bets for 7,567G (44.2%, x1.26); green - 11 bets for 9,559G (55.8%, x0.79)

red bets:
sinnyil2: 4,381G (57.9%, 8,762G)
KyleWonToLiveForever: 2,000G (26.4%, 5,460G)
killth3kid: 520G (6.9%, 5,735G)
AllInBot: 365G (4.8%, 365G)
gorgewall: 201G (2.7%, 7,948G)
RaIshtar: 100G (1.3%, 12,418G)

green bets:
NicoSavoy: 2,000G (20.9%, 343,952G)
resjudicata3: 1,715G (17.9%, 1,715G)
pandasforsale: 1,329G (13.9%, 1,329G)
superdevon1: 1,191G (12.5%, 39,714G)
BirbBrainsBot: 1,000G (10.5%, 104,479G)
CT_5_Holy: 792G (8.3%, 792G)
UnderOneLight: 508G (5.3%, 508G)
ruleof5: 500G (5.2%, 10,862G)
datadrivenbot: 200G (2.1%, 39,158G)
Furgers: 196G (2.1%, 196G)
getthemoneyz: 128G (1.3%, 1,398,264G)

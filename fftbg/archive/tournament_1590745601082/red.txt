Player: !Red
Team: Red Team
Palettes: Red/Brown



Ar Tactic
Male
Aries
57
65
Monk
Draw Out
Caution
Equip Knife
Waterwalking

Spell Edge

Black Hood
Mystic Vest
Dracula Mantle

Spin Fist, Chakra, Revive
Asura, Koutetsu, Bizen Boat, Heaven's Cloud



Evewho
Female
Leo
47
52
Samurai
Time Magic
Absorb Used MP
Defense UP
Move-HP Up

Bizen Boat

Mythril Helmet
Crystal Mail
Feather Boots

Asura, Muramasa, Kikuichimoji
Haste, Stop, Immobilize, Reflect, Quick, Demi 2, Stabilize Time



Lydian C
Female
Scorpio
60
62
Calculator
Yin Yang Magic
Mana Shield
Defense UP
Move+3

Battle Folio

Golden Hairpin
Silk Robe
Spike Shoes

CT, Height, Prime Number, 4, 3
Blind, Spell Absorb, Life Drain, Foxbird, Confusion Song, Dispel Magic, Dark Holy



Lionhermit
Female
Gemini
41
66
Oracle
Item
HP Restore
Long Status
Teleport

Ivory Rod

Headgear
Mystic Vest
Spike Shoes

Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify
Potion, Maiden's Kiss, Phoenix Down

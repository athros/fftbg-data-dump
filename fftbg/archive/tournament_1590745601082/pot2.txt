Final Bets: green - 10 bets for 4,867G (52.7%, x0.90); yellow - 14 bets for 4,369G (47.3%, x1.11)

green bets:
BirbBrainsBot: 1,000G (20.5%, 65,901G)
Lionhermit: 1,000G (20.5%, 3,231G)
Zachara: 793G (16.3%, 87,793G)
gogofromtogo: 500G (10.3%, 7,593G)
CassiePhoenix: 415G (8.5%, 415G)
Lydian_C: 340G (7.0%, 2,862G)
HASTERIOUS: 305G (6.3%, 6,107G)
getthemoneyz: 214G (4.4%, 759,807G)
Saldarin: 200G (4.1%, 1,391G)
lastly: 100G (2.1%, 11,972G)

yellow bets:
UmaiJam: 986G (22.6%, 986G)
volgrathemoose: 921G (21.1%, 921G)
Digitalsocrates: 448G (10.3%, 448G)
AllInBot: 442G (10.1%, 442G)
LivingHitokiri: 380G (8.7%, 380G)
josephiroth_143: 244G (5.6%, 244G)
gorgewall: 201G (4.6%, 16,799G)
ar_tactic: 150G (3.4%, 49,317G)
CrownOfHorns: 148G (3.4%, 148G)
TeaTime29: 147G (3.4%, 295G)
datadrivenbot: 100G (2.3%, 28,548G)
Evewho: 100G (2.3%, 6,414G)
d4rr1n: 100G (2.3%, 6,951G)
daveb_: 2G (0.0%, 897G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LivingHitokiri
Male
Gemini
57
50
Lancer
Elemental
Regenerator
Doublehand
Teleport 2

Musk Rod

Diamond Helmet
Plate Mail
Defense Armlet

Level Jump2, Vertical Jump5
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Nok
Female
Aries
44
56
Knight
Summon Magic
Mana Shield
Maintenance
Ignore Terrain

Defender
Aegis Shield
Grand Helmet
Mythril Armor
Setiemson

Magic Break, Speed Break, Power Break, Justice Sword
Moogle, Fairy



Mesmaster
Monster
Cancer
74
59
Floating Eye










Gorgewall
Male
Virgo
60
74
Bard
Basic Skill
Critical Quick
Equip Sword
Waterwalking

Asura Knife

Twist Headband
Crystal Mail
Wizard Mantle

Angel Song, Magic Song, Last Song, Sky Demon, Hydra Pit
Heal, Yell, Cheer Up, Wish

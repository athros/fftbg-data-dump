Player: !Red
Team: Red Team
Palettes: Red/Brown



SomthingMore
Male
Libra
46
59
Calculator
Farm Skill
Regenerator
Short Charge
Waterwalking

Battle Folio
Mythril Shield
Triangle Hat
Bronze Armor
Power Wrist

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Straight Dash, Oink, Toot, Snort, Bequeath Bacon



BlackFireUK
Female
Leo
50
57
Calculator
Yin Yang Magic
Regenerator
Beastmaster
Move-HP Up

Papyrus Codex

Green Beret
Judo Outfit
Power Wrist

CT, Height, 5, 3
Blind, Doubt Faith, Silence Song, Paralyze



Artea
Male
Aries
54
75
Mime

Counter
Long Status
Move-HP Up



Green Beret
Plate Mail
Magic Gauntlet

Mimic




Bahumat989
Male
Virgo
45
58
Oracle
White Magic
Regenerator
Secret Hunt
Move+2

Gokuu Rod

Holy Miter
Mystic Vest
108 Gems

Foxbird, Confusion Song, Sleep
Cure, Cure 3, Protect, Protect 2, Shell, Esuna

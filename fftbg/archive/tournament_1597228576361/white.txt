Player: !White
Team: White Team
Palettes: White/Blue



Zachara
Male
Sagittarius
37
66
Chemist
Elemental
Parry
Equip Axe
Swim

White Staff

Golden Hairpin
Brigandine
Dracula Mantle

Potion, Eye Drop, Echo Grass, Soft, Phoenix Down
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Lava Ball



Ko2q
Female
Sagittarius
60
65
Wizard
Charge
Critical Quick
Halve MP
Move-HP Up

Ice Rod

Leather Hat
Chain Vest
Rubber Shoes

Fire, Fire 4, Bolt 3, Bolt 4, Ice, Ice 2, Ice 4, Frog
Charge+4



ColetteMSLP
Male
Scorpio
40
77
Summoner
Yin Yang Magic
Regenerator
Magic Defense UP
Move+2

Poison Rod

Holy Miter
Wizard Robe
Defense Ring

Ifrit, Titan, Carbunkle, Salamander, Zodiac
Spell Absorb, Zombie, Confusion Song, Petrify



VolgraTheMoose
Female
Gemini
49
63
Samurai
Punch Art
Blade Grasp
Beastmaster
Waterbreathing

Mythril Spear

Barbuta
Linen Robe
Rubber Shoes

Kiyomori
Purification, Chakra, Revive, Seal Evil

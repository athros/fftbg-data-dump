Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Sir Uther115
Male
Sagittarius
72
59
Knight
Black Magic
Auto Potion
Doublehand
Ignore Terrain

Save the Queen

Barbuta
Plate Mail
Defense Ring

Magic Break, Speed Break, Night Sword
Fire 4, Bolt 3, Ice 4, Flare



Oobs56
Male
Taurus
67
42
Knight
Elemental
Parry
Concentrate
Waterbreathing

Sleep Sword
Gold Shield
Circlet
Diamond Armor
Bracer

Weapon Break, Power Break
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Kaidykat
Male
Virgo
58
48
Time Mage
Throw
Mana Shield
Doublehand
Move+3

White Staff

Headgear
White Robe
Germinas Boots

Haste 2, Stop, Immobilize, Float, Quick, Meteor
Shuriken, Staff, Wand, Dictionary



Reinoe
Male
Virgo
52
43
Squire
Talk Skill
Sunken State
Secret Hunt
Retreat

Blind Knife
Buckler
Black Hood
Leather Armor
Cursed Ring

Dash, Throw Stone, Heal, Tickle, Wish
Preach, Solution, Insult, Rehabilitate

Final Bets: purple - 11 bets for 4,089G (41.5%, x1.41); brown - 8 bets for 5,767G (58.5%, x0.71)

purple bets:
ko2q: 910G (22.3%, 910G)
NIghtdew14: 500G (12.2%, 7,588G)
josephiroth_143: 500G (12.2%, 3,040G)
dogsandcatsand: 484G (11.8%, 484G)
Miku_Shikhu: 452G (11.1%, 452G)
BirbBrainsBot: 440G (10.8%, 86,824G)
getthemoneyz: 258G (6.3%, 1,561,495G)
lastly: 200G (4.9%, 51,656G)
Smugzug: 144G (3.5%, 144G)
ANFz: 101G (2.5%, 46,362G)
northkoreancleetus: 100G (2.4%, 100G)

brown bets:
reinoe: 2,000G (34.7%, 127,433G)
RetroGameCommander: 1,544G (26.8%, 1,544G)
CT_5_Holy: 1,103G (19.1%, 1,103G)
VolgraTheMoose: 301G (5.2%, 2,439G)
roqqqpsi: 267G (4.6%, 1,785G)
DesertWooder: 252G (4.4%, 252G)
AllInBot: 200G (3.5%, 200G)
MinBetBot: 100G (1.7%, 17,916G)

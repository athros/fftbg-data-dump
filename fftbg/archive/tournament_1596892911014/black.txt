Player: !Black
Team: Black Team
Palettes: Black/Red



SSwing
Male
Libra
67
58
Time Mage
Sing
Brave Save
Defend
Move+1

White Staff

Green Beret
Judo Outfit
Salty Rage

Haste, Stop, Reflect, Quick, Stabilize Time
Battle Song, Space Storage, Sky Demon



Legitimized
Monster
Capricorn
53
49
Vampire










Meta Five
Monster
Libra
72
56
Revenant










UmaiJam
Female
Taurus
55
71
Ninja
White Magic
Auto Potion
Equip Gun
Move-HP Up

Battle Folio
Battle Folio
Twist Headband
Earth Clothes
Small Mantle

Shuriken, Bomb, Staff
Cure 2, Cure 3, Raise 2, Regen, Shell 2, Wall, Holy

Player: !Red
Team: Red Team
Palettes: Red/Brown



Fluffywormhole
Female
Aquarius
66
50
Archer
Steal
Counter
Equip Axe
Move+1

Flame Whip
Genji Shield
Circlet
Mystic Vest
Rubber Shoes

Charge+2, Charge+7
Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim



Vampire Killer
Male
Capricorn
77
79
Calculator
Dungeon Skill
PA Save
Short Status
Jump+3

Cypress Rod
Aegis Shield
Cachusha
Light Robe
Diamond Armlet

Blue Magic
Tackle, Eye Gouge, Goblin Punch, Turn Punch, Mutilate, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



Mannequ1n
Female
Scorpio
77
75
Samurai
Basic Skill
Critical Quick
Halve MP
Teleport

Partisan

Bronze Helmet
Genji Armor
Power Wrist

Asura, Murasame, Heaven's Cloud, Muramasa
Throw Stone, Cheer Up



StealthModeLocke
Male
Aries
61
65
Priest
Yin Yang Magic
Critical Quick
Beastmaster
Fly

Gold Staff

Holy Miter
Robe of Lords
Magic Ring

Cure 2, Cure 4, Raise, Regen, Shell 2, Esuna
Silence Song, Sleep

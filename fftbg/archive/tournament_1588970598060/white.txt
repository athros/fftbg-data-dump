Player: !White
Team: White Team
Palettes: White/Blue



Wonser
Male
Gemini
68
50
Wizard
Draw Out
Parry
Magic Attack UP
Teleport

Dragon Rod

Black Hood
Brigandine
Wizard Mantle

Fire 4, Bolt 3, Ice 3, Ice 4, Flare
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



ShintaroNayaka
Monster
Leo
42
50
Plague










Firesheath
Female
Pisces
54
52
Priest
Basic Skill
MA Save
Long Status
Move+2

White Staff

Cachusha
Leather Outfit
Diamond Armlet

Raise, Protect, Wall, Esuna
Accumulate, Throw Stone, Heal



Zoomopie
Male
Scorpio
55
75
Wizard
Item
PA Save
Equip Bow
Jump+3

Ice Bow

Black Hood
Wizard Robe
Rubber Shoes

Fire 3, Bolt, Death
X-Potion, Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down

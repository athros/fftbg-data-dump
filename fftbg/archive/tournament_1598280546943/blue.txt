Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Geekialrage
Female
Libra
41
43
Priest
Talk Skill
Arrow Guard
Beastmaster
Move+1

Gold Staff

Leather Hat
Light Robe
Salty Rage

Cure, Raise, Regen, Protect, Shell 2, Wall, Esuna
Praise, Death Sentence, Insult, Negotiate, Refute



DavenIII
Male
Capricorn
60
44
Monk
Throw
Earplug
Attack UP
Move+1



Holy Miter
Leather Outfit
Bracer

Pummel, Secret Fist, Purification, Seal Evil
Bomb



VolgraTheMoose
Male
Scorpio
39
62
Lancer
Elemental
Speed Save
Defense UP
Ignore Terrain

Octagon Rod
Platinum Shield
Gold Helmet
Carabini Mail
Magic Gauntlet

Level Jump2, Vertical Jump7
Pitfall, Water Ball, Quicksand, Sand Storm, Lava Ball



Powergems
Female
Aries
80
42
Thief
Talk Skill
Parry
Secret Hunt
Ignore Height

Kunai

Headgear
Adaman Vest
Bracer

Gil Taking, Leg Aim
Persuade, Threaten, Solution, Death Sentence, Mimic Daravon, Refute

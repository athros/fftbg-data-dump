Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SQUiDSQUARKLIN
Male
Pisces
37
45
Archer
Black Magic
MP Restore
Doublehand
Waterbreathing

Snipe Bow

Headgear
Leather Outfit
N-Kai Armlet

Charge+1, Charge+3, Charge+5
Bolt 2, Bolt 4, Ice 2, Death



Thyrandaal
Male
Taurus
46
46
Oracle
Battle Skill
HP Restore
Long Status
Jump+3

Gokuu Rod

Flash Hat
Mythril Vest
Feather Mantle

Blind, Doubt Faith, Foxbird, Dispel Magic, Paralyze, Petrify, Dark Holy
Head Break, Weapon Break, Magic Break, Speed Break, Dark Sword



Lord Gwarth
Female
Gemini
79
79
Time Mage
Black Magic
MA Save
Magic Defense UP
Retreat

White Staff

Golden Hairpin
Clothes
Red Shoes

Haste, Haste 2, Stop, Float, Reflect, Demi 2, Stabilize Time
Fire, Bolt 3, Ice, Empower, Death, Flare



Nhammen
Female
Pisces
46
72
Knight
Elemental
Meatbone Slash
Equip Gun
Move+1

Papyrus Codex
Buckler
Mythril Helmet
Plate Mail
Reflect Ring

Armor Break, Weapon Break, Speed Break, Power Break, Mind Break, Stasis Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Zagorsek
Female
Leo
74
59
Thief
Time Magic
Parry
Maintenance
Jump+2

Main Gauche

Black Hood
Leather Outfit
108 Gems

Steal Heart, Steal Weapon
Haste, Stop, Quick



Mexskacin
Female
Aries
79
53
Squire
Item
Dragon Spirit
Secret Hunt
Move+1

Battle Axe
Escutcheon
Leather Hat
Plate Mail
Angel Ring

Heal, Yell, Cheer Up
Hi-Potion, Maiden's Kiss, Remedy, Phoenix Down



Greghatch
Male
Gemini
71
48
Ninja
Draw Out
Catch
Long Status
Swim

Short Edge
Flame Whip
Thief Hat
Judo Outfit
Angel Ring

Shuriken, Dictionary
Asura, Koutetsu, Muramasa, Masamune



Lord Burrah
Female
Sagittarius
42
49
Knight
Steal
Counter Tackle
Defend
Waterbreathing

Ragnarok
Round Shield
Platinum Helmet
Crystal Mail
Small Mantle

Magic Break, Power Break, Justice Sword, Surging Sword
Steal Armor, Steal Weapon

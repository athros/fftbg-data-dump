Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Megaman2202
Male
Scorpio
39
66
Samurai
White Magic
Caution
Equip Gun
Move+3

Battle Folio

Crystal Helmet
Bronze Armor
Spike Shoes

Asura, Koutetsu, Bizen Boat, Muramasa
Cure 3, Raise, Protect, Shell 2, Wall, Esuna, Holy



Yoshima75
Male
Cancer
44
59
Geomancer
Summon Magic
Counter Tackle
Equip Gun
Ignore Height

Blaze Gun
Platinum Shield
Feather Hat
Chameleon Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Moogle, Ifrit, Carbunkle, Odin, Silf



OttoRaynar
Male
Taurus
66
63
Squire
Charge
Counter Magic
Halve MP
Swim

Mythril Knife
Flame Shield
Barette
Plate Mail
Rubber Shoes

Accumulate, Heal, Ultima
Charge+1, Charge+4



JumbocactuarX27
Male
Pisces
65
44
Samurai
Basic Skill
MP Restore
Halve MP
Ignore Terrain

Muramasa

Circlet
Linen Cuirass
Genji Gauntlet

Bizen Boat, Heaven's Cloud
Throw Stone, Heal, Cheer Up

Player: !Red
Team: Red Team
Palettes: Red/Brown



RongRongArts
Female
Gemini
59
64
Chemist
Draw Out
Blade Grasp
Halve MP
Ignore Terrain

Blaze Gun

Green Beret
Black Costume
Defense Ring

Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy, Phoenix Down
Bizen Boat, Murasame



Monopool
Male
Serpentarius
64
52
Ninja
Yin Yang Magic
Brave Up
Equip Gun
Levitate

Papyrus Codex
Bestiary
Twist Headband
Earth Clothes
Diamond Armlet

Shuriken, Knife, Staff
Blind, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Sleep



Theatheologist
Male
Aries
51
54
Archer
Elemental
Brave Up
Equip Polearm
Waterwalking

Javelin
Genji Shield
Platinum Helmet
Chain Vest
Power Wrist

Charge+2, Charge+3, Charge+5, Charge+7, Charge+10
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



BurlapChampion
Female
Scorpio
44
73
Oracle
Talk Skill
Counter Flood
Monster Talk
Retreat

Bestiary

Flash Hat
Chameleon Robe
Spike Shoes

Life Drain, Doubt Faith, Blind Rage, Dispel Magic, Paralyze, Dark Holy
Persuade, Threaten, Negotiate

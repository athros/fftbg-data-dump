Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



FullMetalScrubby
Male
Gemini
43
73
Wizard
Elemental
Mana Shield
Equip Knife
Move+1

Air Knife

Holy Miter
Wizard Robe
Elf Mantle

Fire, Fire 3, Bolt 3, Ice 4, Empower
Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



Valvalis
Female
Scorpio
74
64
Time Mage
Draw Out
Damage Split
Doublehand
Fly

Healing Staff

Golden Hairpin
Brigandine
Red Shoes

Haste 2, Slow, Slow 2, Stop, Immobilize, Reflect, Demi 2, Stabilize Time
Bizen Boat



ElDufferino
Male
Gemini
75
48
Monk
White Magic
PA Save
Equip Bow
Jump+1

Night Killer

Triangle Hat
Leather Outfit
Battle Boots

Spin Fist, Pummel, Purification, Revive, Seal Evil
Cure 2, Cure 3, Raise 2, Protect 2, Shell 2, Esuna



MantisFinch
Male
Aries
60
78
Calculator
Yin Yang Magic
Arrow Guard
Secret Hunt
Fly

Battle Bamboo

Triangle Hat
Adaman Vest
Feather Boots

CT, Height, Prime Number, 4
Life Drain, Blind Rage, Dispel Magic

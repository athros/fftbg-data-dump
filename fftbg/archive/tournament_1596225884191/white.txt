Player: !White
Team: White Team
Palettes: White/Blue



Powergems
Female
Virgo
73
65
Ninja
Elemental
Brave Save
Equip Sword
Waterbreathing

Nagrarock
Asura Knife
Headgear
Mystic Vest
Feather Mantle

Shuriken, Bomb, Ninja Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Nifboy
Male
Aquarius
60
47
Calculator
Yin Yang Magic
Damage Split
Short Charge
Lava Walking

Bestiary

Leather Hat
Light Robe
Power Wrist

CT, Prime Number, 4
Poison, Spell Absorb, Pray Faith, Doubt Faith, Silence Song, Foxbird, Dispel Magic, Petrify



Sinnyil2
Male
Libra
45
78
Samurai
Battle Skill
MA Save
Short Status
Swim

Mythril Spear

Bronze Helmet
Plate Mail
Diamond Armlet

Asura, Koutetsu, Bizen Boat, Murasame, Heaven's Cloud, Chirijiraden
Magic Break, Power Break, Justice Sword



Gelwain
Male
Gemini
72
42
Monk
Jump
Counter Magic
Short Charge
Fly



Headgear
Wizard Outfit
Rubber Shoes

Spin Fist, Purification, Chakra
Level Jump5, Vertical Jump6

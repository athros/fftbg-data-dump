Final Bets: purple - 7 bets for 3,700G (40.1%, x1.49); brown - 18 bets for 5,531G (59.9%, x0.67)

purple bets:
DeathTaxesAndAnime: 1,373G (37.1%, 1,373G)
KSops: 1,000G (27.0%, 2,258G)
killth3kid: 576G (15.6%, 17,102G)
roqqqpsi: 231G (6.2%, 244G)
AllInBot: 200G (5.4%, 200G)
Miku_Shikhu: 200G (5.4%, 1,201G)
holdenmagronik: 120G (3.2%, 1,817G)

brown bets:
BirbBrainsBot: 1,000G (18.1%, 75,937G)
helpimabug: 635G (11.5%, 1,271G)
Nizaha: 632G (11.4%, 11,209G)
nifboy: 504G (9.1%, 504G)
douchetron: 479G (8.7%, 479G)
maximumcrit: 300G (5.4%, 7,384G)
CapnChaos12: 300G (5.4%, 15,425G)
Wooplestein: 220G (4.0%, 220G)
TheseJeans: 208G (3.8%, 208G)
TasisSai: 200G (3.6%, 15,760G)
DouglasDragonThePoet: 200G (3.6%, 804G)
rNdOrchestra: 200G (3.6%, 1,372G)
datadrivenbot: 200G (3.6%, 45,838G)
getthemoneyz: 132G (2.4%, 1,458,692G)
gooblz: 116G (2.1%, 116G)
Firesheath: 100G (1.8%, 3,085G)
PoroTact: 100G (1.8%, 6,574G)
moonliquor: 5G (0.1%, 36,116G)

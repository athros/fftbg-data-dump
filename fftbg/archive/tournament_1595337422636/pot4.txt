Final Bets: purple - 7 bets for 11,804G (61.2%, x0.63); brown - 11 bets for 7,487G (38.8%, x1.58)

purple bets:
AllInBot: 8,883G (75.3%, 8,883G)
NovaKnight21: 1,000G (8.5%, 7,372G)
pplvee1: 1,000G (8.5%, 16,639G)
roofiepops: 400G (3.4%, 1,447G)
MattMan119: 300G (2.5%, 6,507G)
Lydian_C: 121G (1.0%, 99,278G)
Bongomon7: 100G (0.8%, 6,387G)

brown bets:
NIghtdew14: 2,500G (33.4%, 16,454G)
Thyrandaal: 1,219G (16.3%, 1,219G)
BirbBrainsBot: 1,000G (13.4%, 153,635G)
Zeroroute: 600G (8.0%, 600G)
getthemoneyz: 522G (7.0%, 1,333,414G)
fenaen: 428G (5.7%, 428G)
Error72: 340G (4.5%, 340G)
DavenIII: 336G (4.5%, 336G)
Xoomwaffle: 292G (3.9%, 292G)
datadrivenbot: 200G (2.7%, 32,394G)
DouglasDragonThePoet: 50G (0.7%, 1,838G)

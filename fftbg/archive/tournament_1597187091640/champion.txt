Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



VolgraTheMoose
Male
Aries
79
53
Lancer
Basic Skill
Counter Magic
Magic Defense UP
Swim

Dragon Whisker
Flame Shield
Barbuta
Chain Mail
Cherche

Level Jump5, Vertical Jump8
Tickle, Yell, Cheer Up, Fury



Coralreeferz
Male
Aries
55
49
Monk
Summon Magic
Earplug
Dual Wield
Jump+1



Feather Hat
Chain Vest
Angel Ring

Spin Fist, Pummel, Wave Fist, Purification, Chakra, Revive
Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Silf



SephDarkheart
Monster
Aquarius
59
70
Ahriman










DeathTaxesAndAnime
Female
Leo
46
74
Wizard
Math Skill
Arrow Guard
Equip Bow
Jump+1

Night Killer

Thief Hat
Chameleon Robe
Magic Ring

Fire 3, Bolt 2, Ice 2, Ice 3, Empower, Death, Flare
CT, Height, Prime Number, 5, 4, 3

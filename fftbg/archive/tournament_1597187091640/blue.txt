Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lowlf
Female
Cancer
41
51
Oracle
Draw Out
Meatbone Slash
Equip Axe
Swim

Giant Axe

Black Hood
Brigandine
Power Wrist

Spell Absorb, Foxbird, Dispel Magic, Sleep, Petrify
Asura, Koutetsu, Murasame, Muramasa, Kikuichimoji



Powergems
Male
Sagittarius
38
70
Archer
White Magic
Counter Flood
Short Status
Move+2

Mythril Bow

Flash Hat
Leather Outfit
Defense Ring

Charge+1, Charge+2, Charge+10, Charge+20
Raise 2, Reraise, Protect 2, Shell, Shell 2, Wall, Esuna



Powermhero
Monster
Sagittarius
43
65
Red Dragon










Kronikle
Female
Virgo
56
47
Dancer
Jump
HP Restore
Dual Wield
Waterbreathing

Air Knife
Panther Bag
Golden Hairpin
Linen Robe
Defense Ring

Wiznaibus, Disillusion, Last Dance
Level Jump5, Vertical Jump3

Player: !Black
Team: Black Team
Palettes: Black/Red



SephDarkheart
Male
Capricorn
46
70
Oracle
Steal
Absorb Used MP
Martial Arts
Ignore Terrain



Golden Hairpin
Chameleon Robe
Dracula Mantle

Pray Faith, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy
Steal Accessory, Steal Status



Mirapoix
Monster
Leo
80
80
Goblin










Tarheels218
Male
Taurus
50
75
Mediator
White Magic
Auto Potion
Magic Defense UP
Levitate

Star Bag

Triangle Hat
Silk Robe
Genji Gauntlet

Negotiate
Cure, Reraise, Esuna



NIghtdew14
Female
Capricorn
46
42
Dancer
White Magic
Parry
Halve MP
Move+3

Panther Bag

Golden Hairpin
Silk Robe
Reflect Ring

Wiznaibus, Disillusion, Dragon Pit
Cure, Raise, Raise 2, Regen, Protect 2

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Galkife
Male
Aries
70
73
Bard
Jump
Faith Save
Attack UP
Ignore Height

Ramia Harp

Holy Miter
Gold Armor
Salty Rage

Angel Song, Cheer Song, Battle Song, Magic Song, Diamond Blade, Hydra Pit
Level Jump8, Vertical Jump7



Neocarbuncle
Male
Virgo
41
71
Oracle
Draw Out
Absorb Used MP
Equip Knife
Jump+2

Flame Rod

Black Hood
Robe of Lords
108 Gems

Pray Faith, Dispel Magic, Petrify
Asura, Heaven's Cloud, Muramasa



Dogsandcatsand
Male
Virgo
61
52
Lancer
Talk Skill
Dragon Spirit
Monster Talk
Ignore Height

Iron Fan
Platinum Shield
Bronze Helmet
Bronze Armor
Power Wrist

Level Jump5, Vertical Jump8
Invitation, Death Sentence, Insult, Mimic Daravon, Refute, Rehabilitate



Brokenknight201
Female
Taurus
44
80
Lancer
Dance
Speed Save
Equip Polearm
Swim

Ivory Rod
Kaiser Plate
Grand Helmet
Mythril Armor
Vanish Mantle

Level Jump8, Vertical Jump7
Disillusion, Nameless Dance

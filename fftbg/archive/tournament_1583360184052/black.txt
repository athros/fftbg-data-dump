Player: !Black
Team: Black Team
Palettes: Black/Red



Hzor
Male
Libra
64
76
Ninja
Basic Skill
Mana Shield
Long Status
Jump+1

Flail
Kunai
Ribbon
Chain Vest
Red Shoes

Bomb, Hammer, Axe
Dash, Throw Stone, Heal



Masta Glenn
Male
Gemini
42
76
Chemist
Yin Yang Magic
Dragon Spirit
Short Status
Teleport

Panther Bag

Leather Hat
Wizard Outfit
Angel Ring

Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down
Blind, Spell Absorb, Zombie, Foxbird, Dispel Magic, Paralyze



Buckstarcovfefe
Male
Libra
47
44
Monk
Steal
Regenerator
Equip Armor
Move+3



Platinum Helmet
Power Sleeve
108 Gems

Pummel, Wave Fist, Revive, Seal Evil
Steal Status



TheKittenWithaPancake
Female
Pisces
66
76
Mediator
Summon Magic
Faith Up
Short Status
Jump+2

Blast Gun

Headgear
White Robe
Germinas Boots

Persuade, Praise, Threaten, Preach, Insult
Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Leviathan

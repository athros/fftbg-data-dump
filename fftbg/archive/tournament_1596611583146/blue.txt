Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Firesheath
Female
Libra
68
49
Thief
Basic Skill
Dragon Spirit
Equip Bow
Move+1

Hunting Bow

Green Beret
Mystic Vest
Rubber Shoes

Gil Taking, Steal Heart, Steal Shield
Dash, Heal, Tickle, Cheer Up



Ribbiks
Female
Libra
60
70
Samurai
Item
Counter Flood
Throw Item
Jump+3

Bizen Boat

Cross Helmet
Plate Mail
Dracula Mantle

Bizen Boat
Potion, Ether, Hi-Ether, Eye Drop, Soft, Holy Water



Ruebyy
Female
Sagittarius
56
78
Samurai
Summon Magic
Faith Save
Attack UP
Fly

Asura Knife

Bronze Helmet
Carabini Mail
Diamond Armlet

Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Moogle, Ramuh, Carbunkle, Salamander, Cyclops



Aeriyah
Female
Aquarius
40
79
Archer
Dance
Brave Save
Secret Hunt
Move+2

Romanda Gun
Diamond Shield
Leather Helmet
Leather Outfit
Magic Gauntlet

Charge+4
Slow Dance, Polka Polka, Disillusion, Obsidian Blade, Nether Demon

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ALY327
Male
Scorpio
73
54
Mediator
Item
Dragon Spirit
Dual Wield
Jump+2

Mythril Gun
Blaze Gun
Holy Miter
Mystic Vest
108 Gems

Invitation, Praise, Mimic Daravon, Refute, Rehabilitate
Potion, Elixir, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down



Nifboy
Male
Gemini
78
42
Chemist
Black Magic
Absorb Used MP
Sicken
Move-HP Up

Cultist Dagger

Flash Hat
Adaman Vest
Feather Mantle

Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Fire, Fire 2, Bolt 2, Ice, Ice 2, Ice 4, Death



Grininda
Male
Gemini
69
44
Lancer
Talk Skill
Parry
Doublehand
Waterwalking

Gokuu Rod

Barbuta
Linen Cuirass
Cursed Ring

Level Jump5, Vertical Jump7
Invitation, Praise, Threaten, Insult, Refute, Rehabilitate



Braya
Female
Libra
53
74
Lancer
Throw
Damage Split
Magic Defense UP
Levitate

Mythril Spear
Flame Shield
Gold Helmet
Diamond Armor
Spike Shoes

Level Jump4, Vertical Jump8
Bomb

Player: !Black
Team: Black Team
Palettes: Black/Red



Kellios11
Male
Leo
64
41
Calculator
Yin Yang Magic
Distribute
Martial Arts
Move+3



Thief Hat
Mythril Vest
Genji Gauntlet

CT, Height, Prime Number
Poison, Life Drain, Pray Faith, Foxbird, Sleep



Aldrammech
Female
Aries
70
43
Squire
Charge
Caution
Halve MP
Jump+2

Flame Whip
Flame Shield
Leather Hat
Black Costume
Magic Ring

Accumulate, Dash, Yell, Wish
Charge+1, Charge+2, Charge+5, Charge+7



ZephyrTempest
Female
Scorpio
67
46
Wizard
Math Skill
Counter Magic
Equip Bow
Ignore Height

Bow Gun

Twist Headband
Wizard Robe
Feather Boots

Bolt, Bolt 3, Ice, Ice 2, Empower, Flare
CT, Height, Prime Number, 5, 3



Baron Von Scrub
Male
Taurus
55
62
Lancer
Summon Magic
Speed Save
Equip Knife
Ignore Terrain

Rod
Genji Shield
Genji Helmet
Bronze Armor
Chantage

Level Jump8, Vertical Jump2
Moogle, Shiva, Ifrit, Titan, Carbunkle, Bahamut, Silf, Fairy, Cyclops

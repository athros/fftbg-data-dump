Player: !White
Team: White Team
Palettes: White/Blue



Nickyfive
Monster
Gemini
60
76
Grenade










Duskps
Female
Cancer
48
75
Archer
Talk Skill
Distribute
Equip Gun
Jump+3

Mythril Gun
Buckler
Holy Miter
Mystic Vest
Feather Boots

Charge+1, Charge+2, Charge+4, Charge+7, Charge+20
Invitation, Persuade, Death Sentence, Mimic Daravon, Refute



KasugaiRoastedPeas
Male
Capricorn
52
79
Mediator
Black Magic
Counter Magic
Defense UP
Lava Walking

Hydra Bag

Leather Hat
White Robe
Defense Ring

Threaten, Insult, Negotiate, Refute, Rehabilitate
Fire 4, Bolt 3, Ice 2, Ice 3, Ice 4



HaateXIII
Male
Pisces
37
45
Bard
Battle Skill
Hamedo
Equip Shield
Move+2

Fairy Harp
Escutcheon
Golden Hairpin
Mystic Vest
Feather Mantle

Life Song, Cheer Song, Magic Song, Diamond Blade, Sky Demon
Head Break, Magic Break, Speed Break, Explosion Sword

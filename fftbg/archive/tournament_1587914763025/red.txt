Player: !Red
Team: Red Team
Palettes: Red/Brown



Leakimiko
Male
Aries
76
48
Archer
Sing
HP Restore
Martial Arts
Levitate

Long Bow

Headgear
Adaman Vest
108 Gems

Charge+1, Charge+2, Charge+4, Charge+7, Charge+10
Battle Song, Magic Song, Last Song



Onetrickwolf
Female
Sagittarius
44
73
Monk
Draw Out
Parry
Equip Axe
Ignore Terrain

Gold Staff

Red Hood
Mystic Vest
Leather Mantle

Spin Fist, Pummel, Wave Fist, Secret Fist, Purification, Chakra, Seal Evil
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori, Muramasa



MirtaiAtana
Monster
Libra
53
66
Ahriman










Ewan E
Female
Aries
62
66
Archer
Time Magic
PA Save
Short Charge
Jump+1

Windslash Bow

Golden Hairpin
Secret Clothes
Bracer

Charge+5
Haste, Slow 2, Immobilize, Float, Reflect, Stabilize Time

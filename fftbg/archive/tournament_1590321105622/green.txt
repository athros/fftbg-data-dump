Player: !Green
Team: Green Team
Palettes: Green/White



CosmicTactician
Male
Aries
68
46
Samurai
Elemental
Parry
Equip Bow
Move+2

Bow Gun

Cross Helmet
Gold Armor
Wizard Mantle

Koutetsu, Murasame
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



OrganicArtist
Female
Leo
72
50
Thief
Punch Art
Caution
Defense UP
Jump+1

Sasuke Knife

Red Hood
Clothes
Elf Mantle

Gil Taking, Steal Status, Leg Aim
Secret Fist, Revive



ALY327
Male
Gemini
42
60
Summoner
Jump
Counter Tackle
Martial Arts
Jump+3

Bestiary

Red Hood
Chain Vest
Rubber Shoes

Moogle, Titan, Carbunkle, Odin, Fairy
Level Jump8, Vertical Jump8



TheFALLofLindsay
Male
Aquarius
42
53
Geomancer
White Magic
Absorb Used MP
Equip Polearm
Move+3

Dragon Whisker
Bronze Shield
Thief Hat
Linen Robe
Battle Boots

Water Ball, Hell Ivy, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Cure 2, Raise, Esuna

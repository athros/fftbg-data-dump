Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DamnThatShark
Male
Aquarius
58
45
Monk
Steal
Counter
Equip Armor
Move+2



Bronze Helmet
Diamond Armor
Defense Ring

Pummel, Earth Slash, Purification
Gil Taking, Steal Armor



Chuckolator
Monster
Gemini
48
80
Plague










Legitimized
Female
Leo
68
42
Priest
Steal
Counter
Beastmaster
Jump+3

Flame Whip

Feather Hat
Adaman Vest
Setiemson

Cure 2, Cure 3, Cure 4, Raise, Raise 2, Regen, Protect, Esuna
Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Leg Aim



Zachara
Male
Leo
52
64
Knight
Summon Magic
Brave Save
Doublehand
Swim

Panther Bag

Bronze Helmet
Carabini Mail
108 Gems

Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword
Moogle, Shiva, Ifrit, Golem, Leviathan, Fairy, Lich

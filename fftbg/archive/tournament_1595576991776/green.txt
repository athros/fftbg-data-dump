Player: !Green
Team: Green Team
Palettes: Green/White



Douchetron
Female
Cancer
64
45
Archer
Jump
Earplug
Halve MP
Move+1

Long Bow

Leather Helmet
Brigandine
Wizard Mantle

Charge+3, Charge+4, Charge+7
Level Jump8, Vertical Jump8



Prince Rogers Nelson
Monster
Cancer
67
71
Steel Giant










Victoriolue
Male
Pisces
63
51
Thief
Throw
MP Restore
Equip Sword
Teleport

Defender

Red Hood
Clothes
Angel Ring

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory
Shuriken, Bomb



Flacococo
Female
Aries
45
47
Geomancer
Battle Skill
Catch
Beastmaster
Move-HP Up

Blood Sword
Flame Shield
Headgear
Adaman Vest
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Weapon Break, Speed Break, Power Break, Mind Break, Surging Sword, Explosion Sword

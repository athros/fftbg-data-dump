Player: !Black
Team: Black Team
Palettes: Black/Red



Astrozin11
Female
Capricorn
44
66
Mediator
Charge
Abandon
Beastmaster
Jump+2

Papyrus Codex

Red Hood
Brigandine
Leather Mantle

Invitation, Persuade, Threaten, Preach, Solution, Insult, Rehabilitate
Charge+3, Charge+5, Charge+7



WesSideVandal
Monster
Scorpio
79
43
Swine










LordTomS
Monster
Cancer
46
50
Archaic Demon










NIghtdew14
Female
Aries
75
61
Geomancer
Battle Skill
Counter Flood
Magic Attack UP
Move+2

Battle Axe
Buckler
Ribbon
Leather Outfit
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Armor Break, Dark Sword

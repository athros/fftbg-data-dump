Final Bets: green - 21 bets for 8,228G (70.3%, x0.42); yellow - 5 bets for 3,473G (29.7%, x2.37)

green bets:
mexskacin: 1,052G (12.8%, 1,052G)
nifboy: 1,000G (12.2%, 11,461G)
reinoe: 1,000G (12.2%, 15,852G)
datadrivenbot: 951G (11.6%, 22,033G)
JumbocactuarX27: 885G (10.8%, 885G)
Lanshaft: 600G (7.3%, 600G)
SomeCallMeGON: 500G (6.1%, 6,021G)
MoonSlayerRS: 424G (5.2%, 424G)
TheMurkGnome: 413G (5.0%, 3,304G)
joewcarson: 200G (2.4%, 1,152G)
Sye990: 200G (2.4%, 1,746G)
InOzWeTrust: 162G (2.0%, 162G)
MirtaiAtana: 156G (1.9%, 156G)
DrAntiSocial: 101G (1.2%, 1,225G)
AllInBot: 100G (1.2%, 100G)
IndecisiveNinja: 100G (1.2%, 5,275G)
wollise89: 100G (1.2%, 754G)
maakur_: 100G (1.2%, 4,183G)
HeroponThrawn: 100G (1.2%, 5,854G)
getthemoneyz: 82G (1.0%, 471,338G)
daveb_: 2G (0.0%, 2,011G)

yellow bets:
TheChainNerd: 1,636G (47.1%, 8,184G)
BirbBrainsBot: 1,000G (28.8%, 58,745G)
Draconis345: 500G (14.4%, 5,904G)
ZephyrTempest: 260G (7.5%, 50,875G)
Ross_from_Cali: 77G (2.2%, 2,297G)

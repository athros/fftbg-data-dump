Final Bets: purple - 10 bets for 4,673G (37.2%, x1.69); brown - 16 bets for 7,895G (62.8%, x0.59)

purple bets:
Boojob: 2,000G (42.8%, 10,328G)
Nickyfive: 1,401G (30.0%, 14,016G)
nifboy: 500G (10.7%, 12,071G)
sandball: 200G (4.3%, 2,402G)
joewcarson: 200G (4.3%, 1,310G)
L2_Sentinel: 200G (4.3%, 18,807G)
HeroponThrawn: 100G (2.1%, 5,933G)
BirbBrainsBot: 61G (1.3%, 58,114G)
getthemoneyz: 6G (0.1%, 471,516G)
ko2q: 5G (0.1%, 4,790G)

brown bets:
leakimiko: 1,550G (19.6%, 31,012G)
TheChainNerd: 1,309G (16.6%, 6,548G)
Lord_Burrah: 1,200G (15.2%, 93,000G)
MoonSlayerRS: 825G (10.4%, 825G)
datadrivenbot: 760G (9.6%, 22,704G)
Lanshaft: 600G (7.6%, 600G)
mexskacin: 370G (4.7%, 1,607G)
MirtaiAtana: 304G (3.9%, 304G)
SomeCallMeGON: 300G (3.8%, 6,379G)
AllInBot: 100G (1.3%, 100G)
friendsquirrel: 100G (1.3%, 7,012G)
IndecisiveNinja: 100G (1.3%, 5,217G)
maakur_: 100G (1.3%, 4,225G)
wollise89: 100G (1.3%, 833G)
ZephyrTempest: 100G (1.3%, 50,724G)
Ross_from_Cali: 77G (1.0%, 2,510G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zmoses
Male
Gemini
53
73
Squire
Punch Art
Mana Shield
Sicken
Waterwalking

Morning Star
Ice Shield
Triangle Hat
Genji Armor
108 Gems

Dash, Heal, Cheer Up
Wave Fist, Purification, Revive, Seal Evil



Rislyeu
Male
Sagittarius
63
75
Ninja
Yin Yang Magic
Arrow Guard
Equip Gun
Ignore Height

Mythril Gun
Blaze Gun
Feather Hat
Wizard Outfit
Salty Rage

Shuriken, Bomb, Ninja Sword, Dictionary
Blind, Blind Rage, Dispel Magic, Sleep



ArchKnightX
Female
Leo
72
44
Knight
Time Magic
Counter Magic
Defend
Move+2

Chaos Blade
Aegis Shield
Diamond Helmet
Linen Cuirass
Sprint Shoes

Head Break, Shield Break, Weapon Break, Power Break, Stasis Sword, Justice Sword, Night Sword, Surging Sword
Haste, Haste 2, Slow, Stop, Immobilize, Float, Quick, Demi, Demi 2



ApplesauceBoss
Female
Leo
66
55
Mediator
Draw Out
Regenerator
Defense UP
Jump+1

Battle Folio

Black Hood
Light Robe
Small Mantle

Threaten, Preach, Death Sentence, Mimic Daravon, Refute, Rehabilitate
Asura, Muramasa

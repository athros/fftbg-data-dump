Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Run With Stone GUNs
Male
Leo
46
60
Mediator
Black Magic
Caution
Dual Wield
Jump+1

Mythril Gun
Blast Gun
Twist Headband
Earth Clothes
Defense Ring

Invitation, Solution, Insult, Mimic Daravon, Refute
Fire 3, Fire 4, Bolt 2, Bolt 3, Ice 2



Evewho
Female
Virgo
60
61
Summoner
Jump
Parry
Equip Bow
Fly

Night Killer

Barette
Linen Robe
Elf Mantle

Moogle, Carbunkle, Bahamut, Leviathan, Cyclops
Level Jump8, Vertical Jump8



BigGlorious
Male
Capricorn
51
62
Thief
Black Magic
Sunken State
Equip Sword
Levitate

Ice Brand

Feather Hat
Leather Outfit
Cursed Ring

Steal Shield, Steal Accessory, Steal Status, Arm Aim, Leg Aim
Bolt 4, Ice 2, Ice 4, Empower



NeoKevlar
Male
Capricorn
77
55
Calculator
White Magic
Abandon
Long Status
Jump+1

Bestiary

Ribbon
Wizard Robe
Magic Ring

CT, 5, 4, 3
Cure, Cure 4, Raise, Protect, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Vorap
Female
Virgo
75
46
Chemist
Elemental
Auto Potion
Martial Arts
Swim



Barette
Earth Clothes
Magic Gauntlet

Potion, X-Potion, Ether, Antidote, Phoenix Down
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard



Fenaen
Male
Aquarius
56
78
Archer
Talk Skill
Parry
Doublehand
Fly

Perseus Bow

Leather Hat
Mythril Vest
Defense Ring

Charge+3, Charge+5, Charge+20
Threaten, Mimic Daravon, Refute



Boojob
Male
Taurus
48
51
Ninja
Charge
Damage Split
Equip Gun
Lava Walking

Bestiary
Bestiary
Green Beret
Black Costume
Genji Gauntlet

Bomb, Staff, Dictionary
Charge+4, Charge+7, Charge+20



Benticore
Female
Leo
46
66
Time Mage
Elemental
Absorb Used MP
Secret Hunt
Swim

Rainbow Staff

Black Hood
Chameleon Robe
Genji Gauntlet

Haste, Haste 2, Stop, Immobilize, Float, Demi
Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sairentozon7
Female
Serpentarius
47
43
Monk
Time Magic
Absorb Used MP
Concentrate
Waterbreathing



Golden Hairpin
Earth Clothes
Rubber Shoes

Pummel, Earth Slash, Purification, Chakra, Revive
Haste, Slow 2, Immobilize, Reflect, Demi



Pallidsable
Monster
Aquarius
55
51
Vampire










Vorap
Female
Virgo
54
73
Archer
Draw Out
Hamedo
Equip Bow
Waterbreathing

Hunting Bow
Platinum Shield
Flash Hat
Chain Vest
Defense Armlet

Charge+1, Charge+2, Charge+4, Charge+7
Koutetsu, Bizen Boat



Deathmaker06
Female
Capricorn
63
70
Thief
Charge
HP Restore
Dual Wield
Swim

Short Edge
Iron Sword
Green Beret
Mystic Vest
Magic Gauntlet

Steal Armor, Steal Weapon, Steal Status, Leg Aim
Charge+1, Charge+7, Charge+10

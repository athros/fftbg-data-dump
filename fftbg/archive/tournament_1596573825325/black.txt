Player: !Black
Team: Black Team
Palettes: Black/Red



Nizaha
Male
Gemini
78
60
Thief
Summon Magic
Counter Magic
Doublehand
Waterwalking

Hidden Knife

Twist Headband
Power Sleeve
Defense Armlet

Steal Armor, Steal Weapon, Leg Aim
Moogle, Shiva, Titan, Fairy



Meta Five
Male
Taurus
80
57
Mediator
Time Magic
Hamedo
Equip Polearm
Move-MP Up

Persia

Green Beret
Chameleon Robe
Red Shoes

Mimic Daravon, Refute
Haste, Haste 2, Slow, Stop, Demi 2



E Ballard
Female
Pisces
47
54
Priest
Basic Skill
Earplug
Equip Bow
Move-HP Up

Silver Bow

Cachusha
Linen Robe
N-Kai Armlet

Cure 2, Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Shell, Shell 2, Wall
Dash



LordTomS
Female
Pisces
44
53
Samurai
Jump
Mana Shield
Magic Attack UP
Swim

Asura Knife

Genji Helmet
Chain Mail
Defense Ring

Koutetsu, Bizen Boat, Murasame
Level Jump3, Vertical Jump2

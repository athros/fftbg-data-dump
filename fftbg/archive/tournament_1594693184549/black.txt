Player: !Black
Team: Black Team
Palettes: Black/Red



Rurk
Monster
Capricorn
46
68
Blue Dragon










Lemonjohns
Male
Libra
41
70
Knight
Steal
Caution
Concentrate
Move-HP Up

Ragnarok
Diamond Shield
Diamond Helmet
Platinum Armor
Chantage

Armor Break, Power Break
Gil Taking, Steal Shield, Steal Status, Leg Aim



TasisSai
Male
Libra
59
73
Summoner
Black Magic
Brave Save
Equip Axe
Swim

Wizard Staff

Twist Headband
Black Robe
Small Mantle

Shiva, Titan, Golem, Leviathan
Fire, Fire 4, Ice 3, Ice 4



ANFz
Male
Aquarius
62
50
Squire
Item
Counter Magic
Magic Defense UP
Jump+2

Ancient Sword
Diamond Shield
Crystal Helmet
Secret Clothes
Red Shoes

Dash, Heal, Yell, Cheer Up, Fury
Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Remedy

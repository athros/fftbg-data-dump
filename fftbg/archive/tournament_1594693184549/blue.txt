Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



LDSkinny
Female
Taurus
60
46
Oracle
White Magic
HP Restore
Short Charge
Move+2

Ivory Rod

Headgear
Linen Robe
Wizard Mantle

Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Cure, Cure 3, Raise 2, Protect, Protect 2, Esuna, Holy



SQUiDSQUARKLIN
Male
Capricorn
47
79
Archer
Summon Magic
MP Restore
Doublehand
Jump+1

Ice Bow

Red Hood
Adaman Vest
Elf Mantle

Charge+1, Charge+4, Charge+5, Charge+7
Moogle, Shiva, Ifrit, Titan, Fairy



ArchKnightX
Female
Capricorn
59
42
Priest
Black Magic
Arrow Guard
Attack UP
Move+2

Gold Staff

Green Beret
White Robe
Germinas Boots

Cure, Cure 3, Raise, Raise 2, Reraise, Protect 2, Esuna
Fire 2, Fire 3, Fire 4, Bolt 2, Bolt 4



Kellios11
Female
Capricorn
71
77
Priest
Summon Magic
Auto Potion
Attack UP
Swim

Flame Whip

Golden Hairpin
Earth Clothes
Red Shoes

Cure 4, Raise, Shell, Shell 2, Esuna
Moogle, Shiva, Ramuh, Salamander, Lich

Player: !White
Team: White Team
Palettes: White/Blue



Zarath79
Male
Scorpio
57
80
Priest
Steal
Auto Potion
Attack UP
Teleport

Gold Staff

Holy Miter
Silk Robe
Feather Mantle

Cure, Cure 3, Cure 4, Shell, Shell 2, Esuna
Steal Shield, Steal Accessory, Steal Status, Arm Aim, Leg Aim



Phytik
Male
Pisces
65
40
Archer
Black Magic
Abandon
Long Status
Move+3

Windslash Bow

Cross Helmet
Mythril Vest
Vanish Mantle

Charge+1, Charge+20
Fire 3, Bolt 3, Empower, Frog



DamnThatShark
Male
Sagittarius
61
64
Monk
Item
Hamedo
Dual Wield
Jump+3



Ribbon
Mystic Vest
Genji Gauntlet

Spin Fist, Pummel, Wave Fist, Earth Slash, Secret Fist, Purification, Seal Evil
Potion, Ether, Hi-Ether, Echo Grass, Remedy



Superdevon1
Female
Taurus
59
80
Dancer
Yin Yang Magic
Absorb Used MP
Equip Shield
Move+1

Cashmere
Genji Shield
Headgear
Black Costume
Germinas Boots

Polka Polka, Nameless Dance, Obsidian Blade
Poison, Life Drain, Pray Faith, Dispel Magic

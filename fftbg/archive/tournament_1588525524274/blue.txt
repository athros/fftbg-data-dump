Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



WrathfulRemy
Male
Cancer
79
49
Knight
Throw
Counter
Magic Defense UP
Teleport

Coral Sword
Flame Shield
Mythril Helmet
Diamond Armor
Cursed Ring

Head Break, Armor Break, Shield Break, Weapon Break, Speed Break, Power Break, Stasis Sword
Knife



Lali Lulelo
Monster
Sagittarius
64
58
Bull Demon










Rico Flex
Female
Capricorn
42
76
Geomancer
Draw Out
Earplug
Equip Axe
Ignore Height

Battle Axe
Escutcheon
Barette
Wizard Outfit
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard
Asura, Bizen Boat, Murasame



Lordminsc
Female
Scorpio
46
66
Oracle
Jump
Catch
Dual Wield
Lava Walking

Gokuu Rod
Ivory Rod
Golden Hairpin
Mystic Vest
Setiemson

Blind, Zombie, Silence Song, Blind Rage, Dispel Magic, Paralyze
Level Jump8, Vertical Jump2

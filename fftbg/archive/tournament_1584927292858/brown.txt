Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Mirapoix
Female
Libra
57
54
Calculator
Bird Skill
Speed Save
Magic Attack UP
Waterbreathing

Bestiary
Escutcheon
Twist Headband
Bronze Armor
Jade Armlet

Blue Magic
Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor, Scratch Up, Feather Bomb, Shine Lover, Peck, Beak



YaBoy125
Monster
Scorpio
56
59
Red Chocobo










Maeveen
Female
Aries
64
49
Geomancer
White Magic
Counter
Secret Hunt
Waterbreathing

Slasher
Crystal Shield
Green Beret
Chameleon Robe
108 Gems

Water Ball, Hell Ivy, Will-O-Wisp, Blizzard
Cure, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Shell 2



Rinhander
Male
Libra
62
59
Samurai
Jump
Speed Save
Defend
Move-MP Up

Koutetsu Knife

Gold Helmet
Black Robe
Genji Gauntlet

Asura, Bizen Boat, Murasame, Muramasa
Level Jump4, Vertical Jump6

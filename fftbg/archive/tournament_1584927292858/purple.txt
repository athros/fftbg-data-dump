Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Zagorsek
Female
Virgo
40
44
Priest
Throw
Distribute
Defend
Ignore Terrain

Gold Staff

Golden Hairpin
Mystic Vest
Magic Gauntlet

Cure 2, Raise, Esuna
Shuriken, Sword, Staff



DustBirdEX
Female
Aquarius
57
60
Priest
Punch Art
Counter
Equip Bow
Fly

Lightning Bow

Green Beret
Light Robe
Red Shoes

Cure, Raise, Reraise, Protect
Spin Fist, Earth Slash, Revive, Seal Evil



Thefyeman
Male
Leo
77
42
Lancer
Talk Skill
Distribute
Doublehand
Ignore Height

Spear

Platinum Helmet
Crystal Mail
108 Gems

Level Jump8, Vertical Jump8
Threaten, Preach, Solution, Insult, Mimic Daravon, Refute



Mtueni
Male
Sagittarius
68
39
Chemist
Talk Skill
Distribute
Defend
Ignore Terrain

Cute Bag

Barette
Wizard Outfit
Genji Gauntlet

Potion, X-Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy
Invitation, Persuade, Threaten, Preach, Insult, Mimic Daravon, Refute, Rehabilitate

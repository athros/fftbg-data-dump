Player: !Black
Team: Black Team
Palettes: Black/Red



Humble Fabio
Male
Sagittarius
46
69
Mediator
Yin Yang Magic
Catch
Martial Arts
Waterwalking



Twist Headband
Brigandine
Bracer

Invitation, Threaten, Preach, Insult
Blind, Foxbird, Dispel Magic, Sleep



Trez
Female
Leo
76
47
Chemist
Throw
Counter Tackle
Magic Defense UP
Move+1

Cute Bag

Holy Miter
Earth Clothes
Battle Boots

Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down
Sword



Coledot
Female
Cancer
76
45
Samurai
White Magic
Counter
Short Charge
Levitate

Mythril Spear

Leather Helmet
White Robe
Genji Gauntlet

Bizen Boat, Kikuichimoji
Raise, Protect, Protect 2, Shell, Wall, Esuna



Vivithegr8
Female
Sagittarius
67
62
Geomancer
Basic Skill
Damage Split
Martial Arts
Teleport


Diamond Shield
Leather Hat
Mystic Vest
Angel Ring

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Throw Stone, Heal, Tickle, Yell, Wish, Scream

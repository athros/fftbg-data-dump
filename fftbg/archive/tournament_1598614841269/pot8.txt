Final Bets: green - 8 bets for 5,584G (29.6%, x2.37); champion - 11 bets for 13,250G (70.4%, x0.42)

green bets:
Mesmaster: 3,000G (53.7%, 73,029G)
JCBooBot: 1,000G (17.9%, 50,702G)
gorgewall: 644G (11.5%, 644G)
CosmicTactician: 300G (5.4%, 41,494G)
AllInBot: 240G (4.3%, 240G)
datadrivenbot: 200G (3.6%, 60,969G)
Error72: 100G (1.8%, 7,830G)
Leonidusx: 100G (1.8%, 510G)

champion bets:
pplvee1: 7,371G (55.6%, 73,715G)
Raixelol: 1,000G (7.5%, 20,858G)
BirbBrainsBot: 1,000G (7.5%, 88,979G)
ruleof5: 1,000G (7.5%, 47,921G)
DavenIII: 921G (7.0%, 921G)
WhiteTigress: 700G (5.3%, 13,327G)
skipsandwiches: 376G (2.8%, 376G)
iBardic: 332G (2.5%, 332G)
Mushufasa_: 250G (1.9%, 1,933G)
getthemoneyz: 200G (1.5%, 1,762,427G)
RunicMagus: 100G (0.8%, 3,352G)

Player: !Black
Team: Black Team
Palettes: Black/Red



LanseDM
Male
Serpentarius
59
44
Wizard
Sing
HP Restore
Short Charge
Retreat

Mage Masher

Twist Headband
Robe of Lords
Genji Gauntlet

Fire 3, Bolt 2, Flare
Angel Song, Cheer Song, Magic Song



Illudia
Male
Taurus
80
42
Monk
Item
Caution
Magic Defense UP
Fly



Feather Hat
Secret Clothes
Cursed Ring

Spin Fist, Pummel, Chakra, Revive, Seal Evil
Potion, Ether, Hi-Ether, Eye Drop, Holy Water



Shs
Female
Pisces
54
64
Archer
Jump
Dragon Spirit
Secret Hunt
Waterbreathing

Blast Gun
Crystal Shield
Golden Hairpin
Wizard Outfit
Leather Mantle

Charge+1, Charge+2, Charge+7
Level Jump8, Vertical Jump2



Holyonline
Female
Scorpio
55
57
Knight
Talk Skill
Earplug
Doublehand
Move+1

Rune Blade

Gold Helmet
Crystal Mail
Feather Boots

Shield Break, Speed Break
Persuade, Praise, Threaten, Preach, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate

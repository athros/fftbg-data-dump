Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



O Heyno
Female
Sagittarius
60
59
Archer
White Magic
Counter
Equip Shield
Swim

Ultimus Bow
Platinum Shield
Mythril Helmet
Judo Outfit
Angel Ring

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Cure, Cure 4, Reraise, Shell, Esuna



JumbocactuarX27
Male
Sagittarius
64
42
Calculator
White Magic
Brave Save
Equip Bow
Teleport

Windslash Bow

Feather Hat
Linen Robe
Power Wrist

CT, Height, 5, 4
Cure 3, Raise, Reraise, Regen, Esuna



Kaelsun
Female
Taurus
46
79
Squire
Dance
Counter
Attack UP
Jump+1

Poison Bow
Diamond Shield
Headgear
Rubber Costume
Feather Mantle

Accumulate, Heal, Yell, Cheer Up, Fury, Wish
Wiznaibus, Polka Polka, Last Dance, Obsidian Blade, Dragon Pit



ExecutedGiraffe
Female
Sagittarius
69
50
Dancer
Battle Skill
Counter
Equip Armor
Move+3

Mage Masher

Circlet
Platinum Armor
Germinas Boots

Obsidian Blade, Void Storage, Dragon Pit
Shield Break, Weapon Break, Mind Break, Justice Sword, Explosion Sword

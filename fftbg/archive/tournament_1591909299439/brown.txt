Player: !Brown
Team: Brown Team
Palettes: Brown/Green



VolgraTheMoose
Male
Aries
79
79
Mediator
Item
Abandon
Dual Wield
Lava Walking

Blast Gun
Glacier Gun
Feather Hat
Black Robe
Sprint Shoes

Invitation, Threaten, Preach, Death Sentence, Insult, Refute, Rehabilitate
Potion, Hi-Potion, Hi-Ether, Antidote, Maiden's Kiss, Holy Water, Phoenix Down



Evewho
Female
Scorpio
49
45
Knight
Basic Skill
Mana Shield
Defense UP
Move-MP Up

Iron Sword
Kaiser Plate
Gold Helmet
Light Robe
Rubber Shoes

Shield Break, Mind Break, Justice Sword
Accumulate, Heal



Just Here2
Female
Leo
49
47
Dancer
Elemental
Critical Quick
Equip Gun
Move+2

Mythril Gun

Flash Hat
Judo Outfit
Rubber Shoes

Witch Hunt, Polka Polka, Nameless Dance, Last Dance
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Blizzard



Cryptopsy70
Female
Sagittarius
42
48
Squire
Talk Skill
Counter
Equip Sword
Jump+1

Save the Queen

Holy Miter
Mystic Vest
Jade Armlet

Dash, Throw Stone, Heal, Wish
Preach, Mimic Daravon

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Zagorsek
Female
Libra
62
67
Calculator
White Magic
Regenerator
Concentrate
Waterbreathing

Battle Folio

Golden Hairpin
Wizard Robe
Angel Ring

CT, Height, 3
Cure, Cure 2, Cure 3, Protect, Protect 2, Esuna



Brokenknight201
Male
Aquarius
69
77
Wizard
Sing
Caution
Equip Sword
Jump+2

Ice Brand

Twist Headband
Leather Outfit
Reflect Ring

Fire, Bolt, Bolt 2, Ice 2, Ice 3
Angel Song, Magic Song, Nameless Song



Arch8000
Monster
Cancer
80
55
Draugr










LDSkinny
Female
Libra
64
78
Oracle
Draw Out
Distribute
Short Charge
Jump+1

Musk Rod

Holy Miter
Mystic Vest
Small Mantle

Blind, Spell Absorb, Pray Faith, Doubt Faith, Confusion Song, Dispel Magic
Asura, Bizen Boat, Murasame, Heaven's Cloud, Muramasa

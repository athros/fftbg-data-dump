Player: !Black
Team: Black Team
Palettes: Black/Red



Actual JP
Female
Cancer
68
50
Ninja
Punch Art
Earplug
Equip Bow
Ignore Terrain

Lightning Bow

Red Hood
Chain Vest
Bracer

Shuriken, Bomb, Knife, Stick
Spin Fist, Purification, Chakra



Skipsandwiches
Female
Aries
47
46
Geomancer
Throw
Catch
Equip Knife
Move+3

Kunai
Flame Shield
Triangle Hat
Wizard Robe
Defense Ring

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Shuriken, Bomb



NewbGaming
Female
Aquarius
60
76
Priest
Draw Out
MA Save
Martial Arts
Move+3



Thief Hat
Brigandine
Diamond Armlet

Cure, Cure 2, Cure 3, Raise, Reraise, Protect, Protect 2, Shell, Esuna, Holy
Asura, Koutetsu



DaveStrider55
Female
Leo
61
42
Thief
Talk Skill
MP Restore
Equip Polearm
Move-MP Up

Cashmere

Golden Hairpin
Brigandine
Diamond Armlet

Gil Taking, Steal Shield, Arm Aim
Invitation, Death Sentence, Mimic Daravon

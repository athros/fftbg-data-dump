Player: !Brown
Team: Brown Team
Palettes: Brown/Green



SomthingMore
Female
Virgo
56
71
Dancer
Charge
Distribute
Equip Shield
Lava Walking

Panther Bag
Buckler
Flash Hat
Judo Outfit
Vanish Mantle

Polka Polka, Nameless Dance
Charge+3



Bryon W
Male
Scorpio
58
66
Knight
Charge
Auto Potion
Equip Gun
Jump+2

Blast Gun
Genji Shield
Crystal Helmet
Chameleon Robe
Diamond Armlet

Head Break, Shield Break, Weapon Break, Magic Break, Mind Break, Night Sword
Charge+3



Jeeboheebo
Female
Aries
43
75
Samurai
Yin Yang Magic
Sunken State
Magic Defense UP
Move+1

Obelisk

Iron Helmet
White Robe
Elf Mantle

Bizen Boat, Kiyomori
Blind, Pray Faith, Doubt Faith, Dispel Magic



Brokenknight201
Male
Sagittarius
54
80
Thief
Sing
Blade Grasp
Dual Wield
Move+3

Mythril Sword
Assassin Dagger
Triangle Hat
Earth Clothes
Rubber Shoes

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Arm Aim
Cheer Song, Battle Song, Nameless Song, Diamond Blade, Space Storage, Sky Demon, Hydra Pit

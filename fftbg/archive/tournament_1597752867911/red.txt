Player: !Red
Team: Red Team
Palettes: Red/Brown



Lydian C
Female
Capricorn
78
58
Geomancer
Talk Skill
Abandon
Short Charge
Fly

Kikuichimoji
Ice Shield
Green Beret
Light Robe
Red Shoes

Hell Ivy, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Invitation, Threaten, Mimic Daravon



LDSkinny
Female
Libra
42
45
Oracle
Item
MA Save
Equip Shield
Move-MP Up

Madlemgen
Ice Shield
Thief Hat
Linen Robe
Red Shoes

Blind, Poison, Life Drain, Pray Faith, Silence Song, Blind Rage, Dispel Magic, Sleep
Potion, X-Potion, Antidote, Eye Drop, Phoenix Down



Just Here2
Monster
Libra
70
55
Ultima Demon










ALY327
Female
Libra
64
45
Dancer
Charge
MA Save
Maintenance
Retreat

Cashmere

Holy Miter
Chameleon Robe
Magic Ring

Wiznaibus, Polka Polka, Disillusion, Last Dance
Charge+1, Charge+3, Charge+4, Charge+5, Charge+7

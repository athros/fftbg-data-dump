Player: !Black
Team: Black Team
Palettes: Black/Red



Mesmaster
Female
Libra
57
46
Priest
Item
Mana Shield
Short Charge
Swim

Oak Staff

Flash Hat
Black Costume
Germinas Boots

Raise, Raise 2, Shell 2, Wall, Esuna, Holy
Potion, X-Potion, Maiden's Kiss, Remedy, Phoenix Down



TJ FordCDN
Female
Gemini
57
40
Knight
Yin Yang Magic
MA Save
Secret Hunt
Ignore Height

Materia Blade
Aegis Shield
Mythril Helmet
Chain Mail
Red Shoes

Weapon Break, Stasis Sword, Justice Sword, Dark Sword
Spell Absorb, Silence Song, Foxbird, Paralyze, Dark Holy



DustBirdEX
Male
Leo
69
50
Bard
Time Magic
Abandon
Equip Polearm
Swim

Partisan

Holy Miter
Plate Mail
Defense Armlet

Angel Song, Life Song, Last Song, Sky Demon
Stop, Float, Reflect, Stabilize Time



Ring Wyrm
Male
Scorpio
51
60
Knight
Elemental
Brave Save
Magic Defense UP
Retreat

Defender
Round Shield
Iron Helmet
Diamond Armor
Battle Boots

Mind Break, Dark Sword, Surging Sword
Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Cloud92684
Female
Capricorn
58
39
Ninja
Time Magic
Brave Save
Equip Gun
Swim

Papyrus Codex
Bestiary
Barette
Mystic Vest
Reflect Ring

Shuriken, Bomb, Staff
Haste, Haste 2, Slow 2, Stop, Float, Reflect, Demi, Stabilize Time



DeathTaxesAndAnime
Female
Cancer
70
65
Wizard
Math Skill
Parry
Short Status
Ignore Height

Main Gauche

Thief Hat
Wizard Robe
Spike Shoes

Fire 3, Bolt, Bolt 4, Ice 4, Empower
CT, Height, Prime Number, 4, 3



Thenextlantern
Male
Capricorn
50
58
Monk
Throw
Speed Save
Short Charge
Move-HP Up



Flash Hat
Wizard Outfit
Small Mantle

Earth Slash, Purification, Chakra, Revive
Shuriken, Knife



StealthModeLocke
Female
Capricorn
53
45
Chemist
Jump
Faith Save
Short Status
Move+1

Blind Knife

Twist Headband
Leather Outfit
Genji Gauntlet

Potion, Hi-Potion, X-Potion, Eye Drop, Maiden's Kiss, Soft, Phoenix Down
Level Jump8, Vertical Jump8

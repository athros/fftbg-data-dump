Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Humble Fabio
Female
Capricorn
56
51
Time Mage
Jump
PA Save
Equip Polearm
Jump+2

Cypress Rod

Feather Hat
Chain Vest
Spike Shoes

Haste, Slow, Slow 2, Reflect, Quick, Stabilize Time
Level Jump8, Vertical Jump4



Tomion
Female
Capricorn
36
62
Knight
Punch Art
Distribute
Sicken
Move+2

Coral Sword
Round Shield
Gold Helmet
Linen Cuirass
N-Kai Armlet

Shield Break, Weapon Break, Stasis Sword, Dark Sword
Pummel, Secret Fist, Purification



EODTex
Female
Pisces
50
79
Chemist
Battle Skill
Absorb Used MP
Beastmaster
Teleport

Hydra Bag

Green Beret
Earth Clothes
Magic Gauntlet

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Phoenix Down
Weapon Break



Maakur
Male
Libra
66
42
Thief
Basic Skill
Abandon
Equip Bow
Levitate

Cross Bow

Green Beret
Chain Vest
Jade Armlet

Steal Heart, Arm Aim
Accumulate, Dash, Throw Stone, Heal, Cheer Up, Wish

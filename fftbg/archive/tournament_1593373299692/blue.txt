Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Forkmore
Female
Aries
62
76
Calculator
Time Magic
Auto Potion
Equip Sword
Lava Walking

Mythril Sword

Golden Hairpin
Silk Robe
Magic Ring

CT, Height, Prime Number, 3
Haste, Haste 2, Demi, Demi 2, Galaxy Stop



Fluffywormhole
Female
Capricorn
43
61
Geomancer
Talk Skill
Regenerator
Dual Wield
Jump+2

Battle Axe
Diamond Sword
Triangle Hat
Wizard Outfit
Bracer

Pitfall, Water Ball, Local Quake, Static Shock, Gusty Wind, Lava Ball
Persuade, Threaten, Death Sentence, Insult, Mimic Daravon, Refute



E Ballard
Monster
Virgo
67
48
Draugr










Grandlanzer
Male
Scorpio
66
75
Lancer
Basic Skill
Abandon
Short Charge
Move+1

Javelin
Buckler
Iron Helmet
Genji Armor
Small Mantle

Level Jump3, Vertical Jump8
Dash, Throw Stone, Heal, Tickle, Fury

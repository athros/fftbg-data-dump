Player: !Green
Team: Green Team
Palettes: Green/White



HaplessOne
Female
Gemini
76
56
Oracle
Throw
Earplug
Dual Wield
Levitate

Iron Fan
Iron Fan
Thief Hat
Clothes
Feather Mantle

Blind, Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Paralyze
Shuriken, Bomb, Dictionary



SeedSC
Female
Virgo
69
72
Priest
Battle Skill
HP Restore
Maintenance
Ignore Terrain

Wizard Staff

Leather Hat
Chameleon Robe
Genji Gauntlet

Cure 3, Raise, Protect, Wall, Holy
Head Break, Shield Break, Weapon Break, Magic Break, Surging Sword



Hamborn
Male
Taurus
63
55
Time Mage
Throw
Caution
Equip Armor
Jump+3

Battle Folio

Circlet
Carabini Mail
Cherche

Haste, Haste 2, Slow 2, Stop, Quick, Demi, Demi 2, Stabilize Time, Meteor
Bomb, Knife



Bryon W
Female
Aquarius
67
45
Squire
Throw
Distribute
Defend
Swim

Poison Bow
Round Shield
Black Hood
Secret Clothes
Chantage

Dash, Throw Stone, Yell
Shuriken, Bomb

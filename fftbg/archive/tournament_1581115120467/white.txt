Player: !White
Team: White Team
Palettes: White/Blue



MeleeWizard
Male
Capricorn
51
53
Summoner
Draw Out
Parry
Doublehand
Fly

Wizard Rod

Holy Miter
Silk Robe
Bracer

Moogle, Titan, Carbunkle
Murasame, Heaven's Cloud



Bryan792
Female
Sagittarius
72
70
Summoner
Battle Skill
Arrow Guard
Short Charge
Ignore Height

Poison Rod

Black Hood
Leather Vest
Rubber Shoes

Moogle, Titan, Carbunkle, Odin, Silf, Lich
Mind Break, Justice Sword, Night Sword



Boojob
Male
Gemini
58
62
Mime

Sunken State
Secret Hunt
Swim



Red Hood
Brigandine
Diamond Armlet

Mimic




Nightyukiame
Male
Scorpio
45
43
Monk
Basic Skill
Damage Split
Beastmaster
Waterbreathing



Thief Hat
Black Costume
Defense Armlet

Spin Fist, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil
Accumulate, Throw Stone, Heal, Tickle, Wish

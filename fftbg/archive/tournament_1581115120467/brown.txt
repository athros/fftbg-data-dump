Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Moskey71
Female
Virgo
73
46
Samurai
Throw
Counter Magic
Short Status
Waterbreathing

Javelin

Gold Helmet
Black Robe
Small Mantle

Koutetsu, Muramasa
Knife, Staff



Aviner42
Monster
Capricorn
76
53
Blue Dragon










Buttersheep
Male
Cancer
59
52
Wizard
Sing
HP Restore
Secret Hunt
Levitate

Mage Masher

Flash Hat
Leather Vest
Feather Mantle

Bolt 3, Ice, Ice 4, Empower
Angel Song, Life Song, Magic Song, Nameless Song, Hydra Pit



Hamborn
Female
Virgo
70
75
Time Mage
Basic Skill
Parry
Magic Defense UP
Move+3

White Staff

Green Beret
Light Robe
Wizard Mantle

Stop, Immobilize, Demi, Demi 2, Stabilize Time
Scream

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Shakarak
Female
Aries
46
68
Priest
Draw Out
Counter Magic
Maintenance
Jump+1

Oak Staff

Golden Hairpin
White Robe
Vanish Mantle

Cure 4, Raise, Raise 2, Reraise, Regen, Wall, Esuna
Asura, Koutetsu, Muramasa, Kikuichimoji



SystemShut
Male
Gemini
48
45
Knight
Jump
Damage Split
Long Status
Retreat

Materia Blade
Platinum Shield
Iron Helmet
Platinum Armor
Feather Mantle

Weapon Break, Mind Break, Stasis Sword, Surging Sword
Level Jump8, Vertical Jump5



Mayormcfunbags
Female
Leo
51
56
Dancer
Elemental
Brave Up
Doublehand
Teleport

Cashmere

Holy Miter
Black Robe
Germinas Boots

Witch Hunt, Polka Polka, Nether Demon
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard



Chuckolator
Male
Sagittarius
53
60
Lancer
Time Magic
Catch
Beastmaster
Teleport

Dragon Whisker
Bronze Shield
Barbuta
Platinum Armor
Magic Gauntlet

Level Jump4, Vertical Jump8
Haste, Slow, Stop, Demi 2, Stabilize Time

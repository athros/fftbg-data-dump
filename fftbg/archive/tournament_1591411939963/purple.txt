Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Nok
Male
Scorpio
68
71
Squire
Elemental
Absorb Used MP
Defend
Jump+3

Blind Knife
Diamond Shield
Bronze Helmet
Power Sleeve
Reflect Ring

Accumulate, Dash, Heal, Yell, Cheer Up, Scream, Ultima
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard



Gorgewall
Monster
Sagittarius
45
61
Ultima Demon










StealthModeLocke
Male
Taurus
78
64
Geomancer
Charge
Counter Tackle
Secret Hunt
Ignore Height

Kikuichimoji
Diamond Shield
Green Beret
Linen Robe
N-Kai Armlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Charge+2, Charge+4, Charge+10



Error72
Male
Capricorn
67
76
Archer
Battle Skill
Meatbone Slash
Doublehand
Move+2

Bow Gun

Cross Helmet
Black Costume
Leather Mantle

Charge+1, Charge+3, Charge+20
Armor Break, Magic Break, Justice Sword

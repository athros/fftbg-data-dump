Player: !Black
Team: Black Team
Palettes: Black/Red



Aldrammech
Female
Libra
58
52
Oracle
Jump
Counter
Equip Knife
Jump+2

Kunai

Flash Hat
Chameleon Robe
108 Gems

Spell Absorb, Pray Faith, Dispel Magic, Paralyze, Sleep, Dark Holy
Level Jump5, Vertical Jump8



Nifboy
Male
Aries
47
61
Samurai
Yin Yang Magic
Brave Save
Doublehand
Teleport

Masamune

Bronze Helmet
Linen Cuirass
Wizard Mantle

Bizen Boat, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Spell Absorb, Life Drain, Silence Song, Paralyze



Baron Von Scrub
Female
Aries
70
48
Ninja
Battle Skill
Counter Magic
Equip Sword
Move-MP Up

Defender
Asura Knife
Feather Hat
Black Costume
Reflect Ring

Staff, Wand
Head Break, Weapon Break, Power Break, Stasis Sword, Justice Sword



Holyonline
Female
Libra
57
74
Squire
Item
Faith Save
Equip Polearm
Ignore Terrain

Mythril Spear
Genji Shield
Twist Headband
Brigandine
Jade Armlet

Throw Stone, Heal, Tickle, Cheer Up
Potion, Eye Drop, Holy Water, Remedy

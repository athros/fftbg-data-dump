Player: !Red
Team: Red Team
Palettes: Red/Brown



Dantayystv
Female
Libra
37
48
Ninja
White Magic
Auto Potion
Secret Hunt
Move+3

Mythril Knife
Sasuke Knife
Leather Hat
Judo Outfit
Wizard Mantle

Shuriken, Knife, Staff, Spear, Stick
Cure 2, Cure 4, Raise, Regen, Protect, Protect 2, Shell, Shell 2, Esuna



ALY327
Male
Taurus
80
48
Ninja
White Magic
Meatbone Slash
Defense UP
Fly

Kunai
Short Edge
Triangle Hat
Leather Outfit
Germinas Boots

Shuriken, Bomb, Staff
Cure 3, Raise 2, Reraise, Protect, Shell 2, Wall, Esuna



DAC169
Male
Serpentarius
54
49
Priest
Steal
Sunken State
Magic Defense UP
Ignore Terrain

Scorpion Tail

Twist Headband
Chameleon Robe
Rubber Shoes

Regen, Protect, Protect 2, Shell 2, Esuna
Steal Heart, Steal Shield, Steal Weapon, Steal Status, Leg Aim



Ar Tactic
Male
Sagittarius
55
80
Ninja
Yin Yang Magic
Brave Save
Defend
Waterwalking

Spell Edge
Ninja Edge
Feather Hat
Leather Outfit
Germinas Boots

Shuriken, Dictionary
Blind, Poison, Life Drain, Pray Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Petrify, Dark Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Vacillents
Male
Gemini
50
59
Knight
Sing
Catch
Attack UP
Move+1

Ragnarok

Bronze Helmet
Crystal Mail
Reflect Ring

Armor Break, Shield Break, Weapon Break, Magic Break, Stasis Sword, Surging Sword
Battle Song



TheChainNerd
Female
Cancer
76
57
Calculator
Time Magic
MP Restore
Concentrate
Move-HP Up

Bestiary

Holy Miter
Linen Robe
Angel Ring

Height, 5, 4, 3
Haste, Haste 2, Float, Reflect, Stabilize Time



TheFalseTruth
Male
Leo
43
70
Archer
Battle Skill
Regenerator
Dual Wield
Jump+2

Romanda Gun
Mythril Gun
Ribbon
Mythril Vest
108 Gems

Charge+2, Charge+4, Charge+5, Charge+7, Charge+10, Charge+20
Armor Break, Shield Break, Weapon Break, Magic Break, Justice Sword, Dark Sword



Maakur
Male
Serpentarius
54
54
Samurai
Yin Yang Magic
Brave Up
Magic Defense UP
Levitate

Spear

Genji Helmet
Genji Armor
Magic Gauntlet

Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji
Life Drain, Zombie, Confusion Song, Paralyze

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Upvla
Female
Taurus
60
49
Geomancer
Draw Out
Regenerator
Halve MP
Jump+2

Diamond Sword
Mythril Shield
Ribbon
Earth Clothes
Power Wrist

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Asura, Murasame, Heaven's Cloud, Kiyomori, Kikuichimoji



Laserman1000
Female
Gemini
75
79
Thief
Charge
PA Save
Equip Gun
Jump+1

Blast Gun

Feather Hat
Mystic Vest
Magic Ring

Gil Taking, Steal Heart, Steal Helmet, Arm Aim
Charge+1, Charge+4, Charge+5, Charge+10



Noopockets
Female
Aquarius
45
61
Mime

Counter Tackle
Defense UP
Levitate



Feather Hat
Wizard Outfit
Sprint Shoes

Mimic




Mnemogenic
Male
Virgo
73
78
Mime

Counter
Doublehand
Jump+2



Red Hood
Power Sleeve
Power Wrist

Mimic


Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Yayamandua
Male
Cancer
74
46
Priest
Summon Magic
Distribute
Attack UP
Waterwalking

Mace of Zeus

Golden Hairpin
Silk Robe
Red Shoes

Cure 2, Cure 4, Raise, Reraise, Protect, Protect 2, Wall, Esuna
Shiva, Carbunkle, Leviathan, Fairy, Lich



Stratosthegreek
Monster
Leo
65
42
Behemoth










RampantMind
Male
Scorpio
61
76
Mime

Distribute
Magic Attack UP
Lava Walking



Green Beret
Clothes
Magic Gauntlet

Mimic




MixReveal
Male
Gemini
48
49
Geomancer
Black Magic
Earplug
Attack UP
Waterwalking

Slasher
Ice Shield
Feather Hat
Clothes
Diamond Armlet

Water Ball, Hallowed Ground, Local Quake, Quicksand, Blizzard, Gusty Wind
Fire, Ice, Ice 2, Ice 4, Frog

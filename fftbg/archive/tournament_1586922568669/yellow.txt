Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Alrightbye
Female
Cancer
53
63
Mime

Absorb Used MP
Martial Arts
Lava Walking



Golden Hairpin
Brigandine
Genji Gauntlet

Mimic




Roofiepops
Male
Virgo
70
62
Priest
Yin Yang Magic
Meatbone Slash
Equip Shield
Move+3

Morning Star
Hero Shield
Green Beret
Wizard Robe
Defense Armlet

Cure 4, Raise, Reraise, Esuna
Blind, Poison, Zombie, Silence Song, Foxbird, Dispel Magic, Paralyze



Ko2q
Female
Taurus
67
77
Wizard
Draw Out
Faith Up
Beastmaster
Move+3

Dragon Rod

Green Beret
Mythril Vest
Rubber Shoes

Fire 2, Bolt 4, Empower
Asura, Murasame



Pandasforsale
Male
Capricorn
80
62
Samurai
Battle Skill
Caution
Equip Bow
Levitate

Ice Bow

Cross Helmet
Carabini Mail
Small Mantle

Asura, Bizen Boat, Murasame, Kiyomori, Muramasa
Head Break, Armor Break, Justice Sword, Dark Sword

Player: !Black
Team: Black Team
Palettes: Black/Red



ArdentDrops
Female
Cancer
73
47
Oracle
Punch Art
Catch
Sicken
Ignore Terrain

Iron Fan

Leather Hat
Black Robe
Jade Armlet

Life Drain, Zombie, Blind Rage, Dispel Magic, Paralyze
Pummel, Chakra, Revive



Legitimized
Male
Capricorn
74
62
Oracle
White Magic
Faith Up
Concentrate
Jump+2

Iron Fan

Golden Hairpin
Power Sleeve
Defense Ring

Poison, Life Drain, Doubt Faith, Blind Rage, Confusion Song, Dispel Magic, Petrify
Cure, Cure 2, Cure 3, Cure 4, Reraise, Protect, Esuna



Tougou
Female
Gemini
58
58
Geomancer
Draw Out
Distribute
Defense UP
Lava Walking

Blood Sword
Buckler
Black Hood
Adaman Vest
Leather Mantle

Pitfall, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball
Kiyomori



HaychDub
Male
Serpentarius
73
66
Knight
Elemental
Damage Split
Doublehand
Ignore Height

Coral Sword

Mythril Helmet
Mythril Armor
Magic Ring

Head Break, Weapon Break, Mind Break
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball

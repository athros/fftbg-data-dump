Player: !Green
Team: Green Team
Palettes: Green/White



Aldrammech
Male
Leo
48
79
Priest
Basic Skill
Absorb Used MP
Equip Polearm
Retreat

Ivory Rod

Golden Hairpin
Light Robe
Genji Gauntlet

Cure, Cure 3, Raise, Raise 2, Shell 2, Esuna
Accumulate, Heal, Scream



Lord Gwarth
Monster
Aries
59
59
Steel Giant










Forkmore
Male
Cancer
81
71
Thief
Talk Skill
Dragon Spirit
Defend
Move+2

Orichalcum

Green Beret
Secret Clothes
Small Mantle

Steal Helmet, Steal Shield, Steal Weapon
Threaten, Death Sentence, Negotiate, Refute



Roofiepops
Male
Taurus
57
55
Ninja
White Magic
Distribute
Defense UP
Jump+3

Morning Star
Short Edge
Barette
Mystic Vest
108 Gems

Shuriken, Bomb, Staff
Cure, Cure 2, Cure 3, Raise 2, Reraise, Protect, Shell, Shell 2, Esuna, Magic Barrier

Final Bets: red - 22 bets for 8,954G (56.7%, x0.76); blue - 11 bets for 6,835G (43.3%, x1.31)

red bets:
TheChainNerd: 2,000G (22.3%, 14,957G)
DeathTaxesAndAnime: 1,445G (16.1%, 2,834G)
thefyeman: 1,000G (11.2%, 19,643G)
mpghappiness: 512G (5.7%, 512G)
Theseawolf1: 500G (5.6%, 9,315G)
roofiepops: 444G (5.0%, 16,187G)
Duckshut: 400G (4.5%, 939G)
HorusTaurus: 349G (3.9%, 349G)
Grimmace45: 328G (3.7%, 328G)
CorpusCav: 300G (3.4%, 30,470G)
klonoa331: 240G (2.7%, 240G)
Nizaha: 201G (2.2%, 7,506G)
VarkilTV: 200G (2.2%, 610G)
EnemyController: 200G (2.2%, 174,842G)
RestIessNight: 120G (1.3%, 120G)
ungabunga_bot: 107G (1.2%, 332,878G)
belife42: 104G (1.2%, 104G)
karmakamikaze: 104G (1.2%, 104G)
mirapoix: 100G (1.1%, 3,525G)
datadrivenbot: 100G (1.1%, 14,131G)
Jerboj: 100G (1.1%, 39,078G)
REDnSombra: 100G (1.1%, 1,000G)

blue bets:
killth3kid: 2,000G (29.3%, 57,221G)
Pie108: 2,000G (29.3%, 147,829G)
BirbBrainsBot: 621G (9.1%, 1,736G)
CapnChaos12: 500G (7.3%, 53,701G)
CoderCalamity: 477G (7.0%, 1,590G)
SarrgeQc: 301G (4.4%, 3,443G)
Eyepoor_: 300G (4.4%, 300G)
moonliquor: 294G (4.3%, 294G)
FNCardascia_: 220G (3.2%, 220G)
nifboy: 100G (1.5%, 3,986G)
daveb_: 22G (0.3%, 1,143G)

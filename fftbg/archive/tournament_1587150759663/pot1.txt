Final Bets: red - 18 bets for 7,660G (45.2%, x1.21); blue - 18 bets for 9,272G (54.8%, x0.83)

red bets:
Shalloween: 1,000G (13.1%, 30,161G)
ungabunga_bot: 1,000G (13.1%, 160,945G)
BirbBrainsBot: 1,000G (13.1%, 197,244G)
reinoe: 600G (7.8%, 600G)
Bulleta: 500G (6.5%, 1,006G)
ThePuss: 500G (6.5%, 2,982G)
Theseawolf1: 500G (6.5%, 5,494G)
getthemoneyz: 432G (5.6%, 498,580G)
nifboy: 400G (5.2%, 400G)
wurstwaesserchen: 350G (4.6%, 27,573G)
ZephyrTempest: 314G (4.1%, 81,558G)
Lanshaft: 312G (4.1%, 687G)
SpookyCookieMonster: 233G (3.0%, 233G)
SwaffleWaffles: 119G (1.6%, 2,985G)
SquidgyXom: 100G (1.3%, 5,582G)
ko2q: 100G (1.3%, 13,830G)
moonliquor: 100G (1.3%, 871G)
ManaSphere: 100G (1.3%, 1,459G)

blue bets:
TheChainNerd: 2,691G (29.0%, 2,691G)
kai_shee: 1,001G (10.8%, 147,169G)
JonnyCue: 1,000G (10.8%, 6,931G)
Pie108: 700G (7.5%, 21,225G)
fenaen: 500G (5.4%, 6,644G)
evdoggity: 500G (5.4%, 3,813G)
SerumD: 500G (5.4%, 8,684G)
LDSkinny: 500G (5.4%, 3,352G)
DeathTaxesAndAnime: 300G (3.2%, 2,271G)
JackOnFire1: 268G (2.9%, 268G)
killth3kid: 268G (2.9%, 837G)
DLJuggernaut: 250G (2.7%, 4,818G)
Lavatis: 204G (2.2%, 204G)
JumbocactuarX27: 200G (2.2%, 2,047G)
elkydeluxe: 200G (2.2%, 200G)
ApplesauceBoss: 100G (1.1%, 3,552G)
datadrivenbot: 80G (0.9%, 100G)
LordMaxoss: 10G (0.1%, 216G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



MalakoFox
Male
Cancer
79
45
Archer
Steal
Faith Up
Equip Shield
Ignore Terrain

Bow Gun
Flame Shield
Holy Miter
Black Costume
Bracer

Charge+1, Charge+4, Charge+5
Gil Taking, Steal Armor, Steal Accessory, Leg Aim



Technominari
Female
Aries
77
52
Priest
Throw
Faith Up
Magic Attack UP
Levitate

Rainbow Staff

Twist Headband
Linen Robe
Angel Ring

Cure, Raise, Reraise, Wall, Esuna
Bomb



Theseawolf1
Female
Aquarius
46
57
Thief
Elemental
Abandon
Equip Armor
Waterwalking

Mythril Knife

Platinum Helmet
Secret Clothes
Diamond Armlet

Gil Taking, Steal Heart, Steal Helmet, Leg Aim
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard



0v3rr8d
Male
Sagittarius
79
44
Ninja
Basic Skill
Counter Tackle
Attack UP
Retreat

Hidden Knife
Hidden Knife
Black Hood
Leather Outfit
Angel Ring

Shuriken
Accumulate, Heal, Tickle

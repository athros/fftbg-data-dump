Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Pie108
Female
Virgo
47
48
Mime

Distribute
Magic Attack UP
Levitate



Triangle Hat
Adaman Vest
Sprint Shoes

Mimic




JumbocactuarX27
Female
Cancer
50
54
Knight
Steal
Mana Shield
Magic Attack UP
Move+1

Sleep Sword
Gold Shield
Grand Helmet
Mythril Armor
Leather Mantle

Shield Break, Speed Break, Justice Sword, Surging Sword
Steal Helmet, Steal Armor, Leg Aim



Nickyfive
Female
Aquarius
44
77
Chemist
Talk Skill
Auto Potion
Monster Talk
Waterbreathing

Star Bag

Feather Hat
Black Costume
Feather Mantle

Potion, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Persuade, Praise, Mimic Daravon, Refute



Moonliquor
Male
Taurus
62
56
Bard
Talk Skill
Counter
Monster Talk
Ignore Height

Bloody Strings

Triangle Hat
Leather Armor
Defense Ring

Angel Song, Space Storage, Sky Demon
Invitation, Solution, Death Sentence, Negotiate

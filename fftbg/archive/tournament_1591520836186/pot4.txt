Final Bets: purple - 11 bets for 9,067G (85.0%, x0.18); brown - 6 bets for 1,605G (15.0%, x5.65)

purple bets:
cloud92684: 2,000G (22.1%, 7,266G)
NicoSavoy: 1,766G (19.5%, 1,766G)
sinnyil2: 1,200G (13.2%, 6,465G)
prince_rogers_nelson_: 1,184G (13.1%, 1,184G)
DeathTaxesAndAnime: 1,017G (11.2%, 1,017G)
d4rr1n: 500G (5.5%, 4,865G)
Ross_from_Cali: 401G (4.4%, 401G)
DAC169: 400G (4.4%, 400G)
Lythe_Caraker: 250G (2.8%, 120,377G)
ruleof5: 249G (2.7%, 249G)
datadrivenbot: 100G (1.1%, 35,706G)

brown bets:
Rislyeu: 500G (31.2%, 5,551G)
josephiroth_143: 500G (31.2%, 8,914G)
BirbBrainsBot: 281G (17.5%, 154,868G)
getthemoneyz: 124G (7.7%, 841,804G)
Celdia: 100G (6.2%, 2,385G)
Evewho: 100G (6.2%, 6,959G)

Final Bets: red - 19 bets for 9,223G (19.1%, x4.24); blue - 36 bets for 39,130G (80.9%, x0.24)

red bets:
Mtueni: 2,000G (21.7%, 277,295G)
Baron_von_Scrub: 1,111G (12.0%, 73,580G)
onetrickwolf: 1,000G (10.8%, 37,125G)
nedryerson01: 773G (8.4%, 773G)
LordSDB: 750G (8.1%, 4,113G)
killth3kid: 500G (5.4%, 30,166G)
deenglow: 500G (5.4%, 52,811G)
superdevon1: 500G (5.4%, 9,942G)
DrAntiSocial: 420G (4.6%, 20,898G)
Avin_Chaos: 300G (3.3%, 17,848G)
ANFz: 256G (2.8%, 18,061G)
genericco: 200G (2.2%, 1,674G)
gggamezzz: 197G (2.1%, 197G)
getthemoneyz: 164G (1.8%, 602,334G)
dirtpigishbrown: 152G (1.6%, 152G)
CassiePhoenix: 100G (1.1%, 10,919G)
EnemyController: 100G (1.1%, 151,180G)
CosmicTactician: 100G (1.1%, 4,881G)
FriendSquirrel: 100G (1.1%, 26,212G)

blue bets:
Shakarak: 5,000G (12.8%, 8,900G)
Sairentozon7: 4,336G (11.1%, 4,336G)
catfashions: 4,017G (10.3%, 4,017G)
DavenIII: 3,000G (7.7%, 14,811G)
Zeroroute: 2,537G (6.5%, 2,537G)
JumbocactuarX27: 2,000G (5.1%, 5,154G)
Pie108: 2,000G (5.1%, 289,538G)
Grimmace45: 1,599G (4.1%, 6,399G)
nifboy: 1,543G (3.9%, 1,543G)
fenaen: 1,500G (3.8%, 78,354G)
museofdoom: 1,225G (3.1%, 1,225G)
leakimiko: 1,152G (2.9%, 7,680G)
JanusII: 1,000G (2.6%, 21,383G)
BirbBrainsBot: 1,000G (2.6%, 69,340G)
TheChainNerd: 1,000G (2.6%, 7,959G)
cgMcWhiskers: 928G (2.4%, 928G)
ko2q: 742G (1.9%, 742G)
DustBirdEX: 555G (1.4%, 2,454G)
vorap: 500G (1.3%, 22,326G)
CapnChaos12: 500G (1.3%, 44,303G)
PetitFoulard: 467G (1.2%, 467G)
Jeeboheebo: 376G (1.0%, 376G)
L_M_D: 300G (0.8%, 2,324G)
EizanTayama: 283G (0.7%, 283G)
rjA0zcOQ96: 200G (0.5%, 36,059G)
SeniorBunk: 200G (0.5%, 4,378G)
trigger15: 200G (0.5%, 693G)
GenieMilenko: 200G (0.5%, 2,484G)
ungabunga_bot: 119G (0.3%, 271,709G)
ZephyrTempest: 101G (0.3%, 7,721G)
Firesheath: 100G (0.3%, 11,161G)
snakeddL: 100G (0.3%, 108G)
frozentomato: 100G (0.3%, 1,458G)
datadrivenbot: 100G (0.3%, 5,405G)
Wonser: 100G (0.3%, 4,601G)
richardserious: 50G (0.1%, 1,044G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



VolgraTheMoose
Male
Cancer
39
45
Lancer
Item
Caution
Defense UP
Teleport

Mythril Spear
Platinum Shield
Circlet
Reflect Mail
Rubber Shoes

Level Jump8, Vertical Jump8
Echo Grass, Soft, Holy Water, Phoenix Down



Timthehoodie
Female
Capricorn
43
73
Summoner
Jump
Faith Save
Long Status
Jump+2

Wizard Staff

Green Beret
Brigandine
Germinas Boots

Moogle, Shiva, Leviathan, Silf, Lich
Level Jump8, Vertical Jump3



Lube Squid
Monster
Libra
68
47
Squidraken










Thyrandaal
Female
Aries
64
69
Summoner
Steal
Counter
Short Charge
Ignore Height

Poison Rod

Feather Hat
Power Sleeve
Small Mantle

Moogle, Golem, Carbunkle, Odin, Leviathan, Salamander, Cyclops
Steal Helmet, Steal Accessory

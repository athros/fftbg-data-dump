Final Bets: blue - 9 bets for 5,034G (15.0%, x5.65); yellow - 9 bets for 28,421G (85.0%, x0.18)

blue bets:
SkylerBunny: 1,932G (38.4%, 1,932G)
Firesheath: 1,216G (24.2%, 1,216G)
Aldrammech: 567G (11.3%, 5,673G)
getthemoneyz: 528G (10.5%, 1,641,538G)
arctodus13: 300G (6.0%, 620G)
roqqqpsi: 191G (3.8%, 3,834G)
AllInBot: 100G (2.0%, 100G)
Thyrandaal: 100G (2.0%, 250,219G)
datadrivenbot: 100G (2.0%, 65,027G)

yellow bets:
Zeroroute: 10,744G (37.8%, 32,914G)
reinoe: 10,000G (35.2%, 200,195G)
Forkmore: 3,593G (12.6%, 7,047G)
wildetrys: 1,100G (3.9%, 1,100G)
BirbBrainsBot: 1,000G (3.5%, 161,350G)
pandasforsale: 666G (2.3%, 13,559G)
SephDarkheart: 580G (2.0%, 115,882G)
nekojin: 488G (1.7%, 13,927G)
Drusiform: 250G (0.9%, 15,905G)

Player: !Green
Team: Green Team
Palettes: Green/White



Laserman1000
Female
Scorpio
77
61
Dancer
Item
Arrow Guard
Throw Item
Jump+1

Star Bag

Thief Hat
Linen Robe
Battle Boots

Wiznaibus, Polka Polka, Obsidian Blade, Nether Demon, Dragon Pit
Hi-Ether, Eye Drop, Maiden's Kiss, Soft, Remedy



Lythe Caraker
Male
Sagittarius
67
45
Squire
Sing
Dragon Spirit
Beastmaster
Move-MP Up

Giant Axe
Gold Shield
Barette
Power Sleeve
Bracer

Accumulate, Dash, Throw Stone, Heal
Nameless Song, Sky Demon



Forkmore
Female
Aries
49
69
Time Mage
Item
Mana Shield
Short Charge
Ignore Terrain

Wizard Staff

Red Hood
Light Robe
Small Mantle

Haste 2, Slow
Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Soft, Remedy, Phoenix Down



ArchKnightX
Female
Aries
46
54
Summoner
Steal
Counter Magic
Short Status
Move-HP Up

Flame Rod

Red Hood
Adaman Vest
Sprint Shoes

Moogle, Carbunkle, Silf, Fairy, Lich
Steal Accessory

Player: !Black
Team: Black Team
Palettes: Black/Red



Dirty Naise
Female
Leo
62
73
Time Mage
Dance
Damage Split
Doublehand
Retreat

Gold Staff

Flash Hat
Power Sleeve
Power Wrist

Haste 2, Slow, Slow 2, Stop, Immobilize, Reflect, Quick, Demi, Stabilize Time
Wiznaibus, Nameless Dance



Reddwind
Female
Sagittarius
71
63
Knight
Dance
Meatbone Slash
Concentrate
Swim

Materia Blade
Crystal Shield
Circlet
Gold Armor
Wizard Mantle

Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword, Dark Sword
Disillusion, Nameless Dance



Blaster Bomb
Male
Leo
43
52
Wizard
Sing
Speed Save
Equip Axe
Jump+3

Wizard Rod

Holy Miter
Wizard Outfit
Feather Mantle

Fire 2, Fire 4, Bolt 2, Empower, Frog
Nameless Song, Sky Demon



Gabbagooluigi
Male
Taurus
71
50
Lancer
Sing
Faith Up
Concentrate
Ignore Terrain

Javelin
Mythril Shield
Barbuta
Light Robe
Sprint Shoes

Level Jump5, Vertical Jump7
Angel Song, Magic Song, Nameless Song, Sky Demon

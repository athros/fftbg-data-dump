Player: !Black
Team: Black Team
Palettes: Black/Red



Killth3kid
Monster
Aquarius
43
59
Bomb










Draconis345
Male
Gemini
51
43
Squire
Steal
Auto Potion
Sicken
Jump+1

Rune Blade
Escutcheon
Green Beret
Earth Clothes
Genji Gauntlet

Heal, Wish
Gil Taking, Steal Helmet, Steal Armor, Steal Accessory, Leg Aim



Run With Stone GUNs
Male
Sagittarius
64
64
Ninja
Talk Skill
Damage Split
Equip Axe
Waterwalking

Flail
Gold Staff
Black Hood
Clothes
Dracula Mantle

Bomb, Wand
Invitation, Persuade, Praise, Preach, Insult, Refute



Reddwind
Female
Capricorn
56
66
Knight
Time Magic
Dragon Spirit
Halve MP
Jump+3

Mythril Sword
Flame Shield
Bronze Helmet
Crystal Mail
Spike Shoes

Weapon Break, Power Break, Mind Break, Night Sword, Surging Sword
Haste, Float, Quick, Stabilize Time

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



CorpusCav
Female
Taurus
42
60
Dancer
Battle Skill
Counter
Magic Defense UP
Move+2

Cashmere

Barette
Light Robe
Bracer

Wiznaibus, Last Dance
Magic Break, Speed Break, Mind Break, Surging Sword



Upvla
Female
Scorpio
61
63
Summoner
Dance
Sunken State
Short Charge
Move-MP Up

Healing Staff

Black Hood
Silk Robe
Leather Mantle

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Bahamut, Odin, Leviathan
Polka Polka, Disillusion, Nameless Dance, Last Dance, Nether Demon



Ququroon
Male
Aries
67
37
Oracle
Sing
Counter
Secret Hunt
Ignore Height

Cypress Rod

Headgear
Black Costume
Magic Ring

Poison, Life Drain, Silence Song, Blind Rage, Foxbird, Paralyze, Dark Holy
Cheer Song, Magic Song, Diamond Blade



StealthModeLocke
Female
Aries
64
71
Monk
Charge
Earplug
Equip Knife
Move+2

Mage Masher

Red Hood
Clothes
Reflect Ring

Spin Fist, Pummel, Purification, Revive
Charge+1, Charge+2, Charge+3, Charge+4, Charge+5

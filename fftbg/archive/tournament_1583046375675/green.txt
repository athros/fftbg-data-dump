Player: !Green
Team: Green Team
Palettes: Green/White



Goust18
Female
Aries
76
58
Archer
Yin Yang Magic
Parry
Doublehand
Waterbreathing

Silver Bow

Barbuta
Black Costume
Feather Mantle

Charge+1, Charge+2, Charge+3, Charge+4, Charge+10
Poison, Life Drain, Dispel Magic, Paralyze



Omegasuspekt
Male
Libra
73
73
Ninja
Punch Art
Distribute
Sicken
Ignore Height

Panther Bag
Ninja Edge
Thief Hat
Rubber Costume
Cursed Ring

Shuriken, Bomb, Stick, Wand
Purification, Revive, Seal Evil



RRazza
Male
Capricorn
71
45
Ninja
Talk Skill
Counter
Magic Defense UP
Jump+3

Blind Knife
Assassin Dagger
Twist Headband
Adaman Vest
Bracer

Shuriken, Bomb, Staff
Invitation, Insult, Rehabilitate



AoiTsukiYuri
Female
Scorpio
50
48
Chemist
Basic Skill
Earplug
Sicken
Levitate

Sleep Sword

Feather Hat
Mythril Vest
Angel Ring

Hi-Potion, Ether, Antidote, Remedy, Phoenix Down
Tickle, Cheer Up, Wish

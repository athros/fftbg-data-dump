Player: !Red
Team: Red Team
Palettes: Red/Brown



Neerrm
Female
Taurus
55
49
Calculator
Robosnake Skill
Brave Save
Magic Attack UP
Jump+1

Ice Rod

Cachusha
Linen Cuirass
N-Kai Armlet

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



DeathTaxesAndAnime
Female
Aries
65
55
Mime

Hamedo
Attack UP
Ignore Terrain



Headgear
Secret Clothes
Power Wrist

Mimic




DamnThatShark
Male
Leo
70
45
Monk
Summon Magic
PA Save
Defense UP
Waterwalking



Leather Hat
Rubber Costume
Jade Armlet

Pummel, Wave Fist, Earth Slash, Purification
Titan, Golem, Carbunkle



Hasterious
Male
Leo
46
47
Lancer
Basic Skill
Counter
Martial Arts
Waterbreathing

Gungnir
Buckler
Cross Helmet
Linen Cuirass
Small Mantle

Level Jump2, Vertical Jump2
Accumulate, Throw Stone, Heal, Cheer Up, Wish

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Soren Of Tyto
Monster
Pisces
64
72
Red Chocobo










StealthModeLocke
Male
Capricorn
69
69
Thief
Summon Magic
Damage Split
Equip Shield
Jump+1

Sleep Sword
Genji Shield
Golden Hairpin
Chain Vest
Power Wrist

Steal Helmet, Steal Accessory
Moogle, Ramuh, Titan, Carbunkle, Salamander, Fairy



Mirapoix
Female
Virgo
61
57
Thief
Item
Mana Shield
Defend
Fly

Air Knife

Green Beret
Leather Outfit
108 Gems

Steal Helmet, Steal Weapon, Arm Aim
Potion, Hi-Potion, Antidote, Maiden's Kiss, Soft, Remedy, Phoenix Down



Chronolynx42
Female
Capricorn
77
42
Archer
Dance
Absorb Used MP
Concentrate
Ignore Height

Ice Bow

Platinum Helmet
Clothes
Spike Shoes

Charge+4, Charge+7, Charge+10, Charge+20
Polka Polka, Disillusion, Nameless Dance, Void Storage, Dragon Pit

Player: !White
Team: White Team
Palettes: White/Blue



Gorgewall
Female
Cancer
51
39
Lancer
Punch Art
Auto Potion
Maintenance
Move+3

Musk Rod
Crystal Shield
Platinum Helmet
Leather Armor
Defense Armlet

Level Jump5, Vertical Jump2
Purification, Revive



Ravingsockmonkey
Female
Sagittarius
65
55
Lancer
Steal
Brave Save
Equip Gun
Waterbreathing

Romanda Gun
Mythril Shield
Barbuta
Diamond Armor
Rubber Shoes

Level Jump5, Vertical Jump6
Leg Aim



Sinnyil2
Male
Libra
74
74
Monk
Battle Skill
Earplug
Equip Knife
Jump+2

Ice Rod

Triangle Hat
Clothes
Battle Boots

Spin Fist, Purification, Chakra, Revive, Seal Evil
Power Break, Surging Sword



Rintastic
Female
Libra
76
55
Squire
Summon Magic
MP Restore
Equip Armor
Move+1

Flail
Flame Shield
Triangle Hat
White Robe
108 Gems

Dash, Heal
Moogle, Ramuh, Titan, Odin, Cyclops

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Thyrandaal
Male
Leo
56
45
Chemist
Jump
Hamedo
Equip Polearm
Retreat

Iron Fan

Holy Miter
Power Sleeve
Angel Ring

Potion, Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Remedy, Phoenix Down
Level Jump3, Vertical Jump4



VolgraTheMoose
Monster
Scorpio
73
48
Vampire










Lemonjohns
Male
Capricorn
79
66
Priest
Jump
MP Restore
Equip Sword
Retreat

Nagrarock

Triangle Hat
Linen Robe
Small Mantle

Cure, Cure 2, Cure 3, Reraise, Regen, Shell 2, Wall, Magic Barrier
Level Jump3, Vertical Jump5



Redmage4evah
Female
Aries
42
76
Thief
Talk Skill
Absorb Used MP
Halve MP
Lava Walking

Assassin Dagger

Barette
Adaman Vest
Dracula Mantle

Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory
Persuade, Insult, Mimic Daravon, Refute

Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Twelfthrootoftwo
Female
Scorpio
73
49
Samurai
Punch Art
Regenerator
Sicken
Teleport 2

Asura Knife

Platinum Helmet
Linen Robe
Defense Armlet

Asura, Koutetsu, Murasame, Heaven's Cloud, Kikuichimoji
Spin Fist, Pummel, Purification, Chakra



K1ngofthechill
Male
Capricorn
70
74
Mime

Counter
Defend
Levitate



Leather Helmet
Power Sleeve
Dracula Mantle

Mimic




Phi Sig
Male
Libra
72
67
Mediator
Charge
Caution
Dual Wield
Waterwalking

Blast Gun
Blaze Gun
Flash Hat
Black Robe
Reflect Ring

Solution, Refute
Charge+1, Charge+4



Zagorsek
Male
Capricorn
76
71
Ninja
Battle Skill
Damage Split
Short Status
Fly

Air Knife
Kunai
Leather Hat
Chain Vest
Sprint Shoes

Dictionary
Magic Break, Stasis Sword, Surging Sword

Final Bets: white - 12 bets for 7,533G (65.0%, x0.54); black - 7 bets for 4,049G (35.0%, x1.86)

white bets:
megaman2202: 2,275G (30.2%, 3,550G)
TheChainNerd: 1,000G (13.3%, 8,484G)
NicoSavoy: 1,000G (13.3%, 20,873G)
Dexsana: 1,000G (13.3%, 6,011G)
ShintaroNayaka: 752G (10.0%, 752G)
twelfthrootoftwo: 500G (6.6%, 8,333G)
AmaninAmide: 300G (4.0%, 3,301G)
Aestheta: 290G (3.8%, 290G)
Strifu: 112G (1.5%, 112G)
thethorndog1: 104G (1.4%, 104G)
datadrivenbot: 100G (1.3%, 14,989G)
CosmicTactician: 100G (1.3%, 10,017G)

black bets:
ko2q: 1,078G (26.6%, 1,078G)
BirbBrainsBot: 1,000G (24.7%, 21,880G)
LordSDB: 650G (16.1%, 650G)
roqqqpsi: 579G (14.3%, 579G)
getthemoneyz: 326G (8.1%, 674,717G)
Dexef: 316G (7.8%, 316G)
lastly: 100G (2.5%, 12,085G)

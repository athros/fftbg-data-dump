Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lijarkh
Male
Cancer
60
57
Monk
Elemental
Catch
Dual Wield
Lava Walking



Holy Miter
Adaman Vest
Feather Mantle

Spin Fist, Pummel, Wave Fist, Earth Slash, Chakra, Seal Evil
Pitfall, Water Ball, Hallowed Ground, Static Shock, Gusty Wind, Lava Ball



Mtueni
Monster
Serpentarius
56
65
Red Chocobo










Lastly
Female
Cancer
67
80
Chemist
Dance
MA Save
Equip Armor
Lava Walking

Panther Bag

Iron Helmet
Linen Cuirass
Angel Ring

X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Maiden's Kiss, Remedy, Phoenix Down
Wiznaibus, Slow Dance, Last Dance



Tomion
Female
Capricorn
53
51
Archer
Battle Skill
Abandon
Dual Wield
Jump+3

Bow Gun
Cross Bow
Iron Helmet
Wizard Outfit
Bracer

Charge+4, Charge+5, Charge+20
Weapon Break, Power Break

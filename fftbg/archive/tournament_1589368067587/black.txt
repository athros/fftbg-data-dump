Player: !Black
Team: Black Team
Palettes: Black/Red



ShintaroNayaka
Male
Virgo
41
48
Thief
Time Magic
Arrow Guard
Equip Knife
Fly

Spell Edge

Flash Hat
Adaman Vest
Germinas Boots

Steal Armor, Steal Accessory, Leg Aim
Slow, Float, Reflect, Quick, Stabilize Time, Meteor



Roqqqpsi
Female
Capricorn
52
49
Oracle
Talk Skill
Counter Tackle
Monster Talk
Waterbreathing

Papyrus Codex

Leather Hat
Chameleon Robe
Angel Ring

Blind, Doubt Faith, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Mimic Daravon, Refute, Rehabilitate



WinnerBit
Male
Taurus
44
71
Thief
Throw
Counter
Defend
Move+1

Platinum Sword

Leather Hat
Judo Outfit
Wizard Mantle

Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Sword, Wand



Evewho
Female
Gemini
58
68
Priest
Jump
Counter
Beastmaster
Lava Walking

Flame Whip

Golden Hairpin
Adaman Vest
Dracula Mantle

Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell, Esuna, Holy
Level Jump2, Vertical Jump6

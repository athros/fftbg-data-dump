Final Bets: purple - 6 bets for 2,600G (41.3%, x1.42); brown - 10 bets for 3,695G (58.7%, x0.70)

purple bets:
TheChainNerd: 1,000G (38.5%, 9,022G)
solomongrundy85: 1,000G (38.5%, 19,094G)
twelfthrootoftwo: 300G (11.5%, 8,602G)
lastly: 100G (3.8%, 11,985G)
datadrivenbot: 100G (3.8%, 15,043G)
CosmicTactician: 100G (3.8%, 10,071G)

brown bets:
BirbBrainsBot: 1,000G (27.1%, 20,880G)
AmaninAmide: 500G (13.5%, 3,462G)
rechaun: 468G (12.7%, 468G)
Aestheta: 446G (12.1%, 446G)
Dexef: 316G (8.6%, 316G)
roqqqpsi: 297G (8.0%, 540G)
getthemoneyz: 218G (5.9%, 674,391G)
AUrato: 200G (5.4%, 1,658G)
NicoSavoy: 200G (5.4%, 21,415G)
Strifu: 50G (1.4%, 172G)

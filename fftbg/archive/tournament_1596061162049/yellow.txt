Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lastly
Female
Scorpio
37
77
Squire
Draw Out
Parry
Martial Arts
Lava Walking

Cute Bag
Buckler
Ribbon
Judo Outfit
Diamond Armlet

Dash, Heal, Cheer Up, Fury, Wish, Scream
Koutetsu, Bizen Boat



Calajo
Female
Aries
64
68
Summoner
Item
HP Restore
Throw Item
Move+2

White Staff

Headgear
Chameleon Robe
Elf Mantle

Moogle, Ifrit, Golem, Carbunkle, Zodiac
Potion, Hi-Ether, Maiden's Kiss, Soft, Holy Water, Phoenix Down



StealthModeLocke
Male
Taurus
70
52
Ninja
Basic Skill
Arrow Guard
Equip Shield
Move+2

Short Edge
Flame Shield
Headgear
Judo Outfit
Leather Mantle

Shuriken, Bomb, Staff
Heal, Wish



Dogsandcatsand
Male
Capricorn
49
43
Time Mage
Basic Skill
Speed Save
Magic Attack UP
Waterwalking

Gold Staff

Twist Headband
Light Robe
Defense Armlet

Haste, Slow, Stop, Stabilize Time
Dash, Throw Stone, Tickle, Fury, Wish

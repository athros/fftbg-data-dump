Player: !White
Team: White Team
Palettes: White/Blue



Firesheath
Female
Scorpio
56
57
Ninja
Talk Skill
Critical Quick
Equip Axe
Teleport 2

Mace of Zeus
Assassin Dagger
Red Hood
Chain Vest
Leather Mantle

Bomb, Knife
Invitation, Persuade, Praise, Preach, Rehabilitate



Zagorsek
Female
Capricorn
64
65
Summoner
White Magic
Faith Save
Short Charge
Waterbreathing

Thunder Rod

Leather Hat
Black Costume
Defense Ring

Moogle, Shiva, Odin, Leviathan, Lich
Cure 2, Cure 3, Raise, Reraise, Protect, Protect 2, Shell 2, Esuna



Galkife
Female
Sagittarius
74
46
Chemist
Summon Magic
Caution
Magic Defense UP
Levitate

Cute Bag

Black Hood
Leather Outfit
Battle Boots

Potion, Hi-Potion, Ether, Eye Drop, Holy Water, Phoenix Down
Shiva, Carbunkle, Bahamut, Fairy



Twelfthrootoftwo
Female
Scorpio
76
79
Samurai
Throw
Abandon
Dual Wield
Move-MP Up

Partisan
Spear
Barbuta
Chameleon Robe
Battle Boots

Bizen Boat, Heaven's Cloud
Ninja Sword, Spear

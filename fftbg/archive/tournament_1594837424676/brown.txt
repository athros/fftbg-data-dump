Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Sinnyil2
Male
Taurus
42
77
Knight
Time Magic
Parry
Defense UP
Teleport 2

Mythril Sword
Buckler
Leather Helmet
Gold Armor
Angel Ring

Mind Break
Slow 2, Float, Quick, Stabilize Time



Mpghappiness
Male
Aries
70
46
Bard
White Magic
Brave Save
Defense UP
Waterwalking

Ramia Harp

Black Hood
Leather Outfit
Sprint Shoes

Cheer Song, Magic Song, Nameless Song, Last Song, Sky Demon
Cure, Cure 2, Cure 3, Regen, Protect, Esuna



E Ballard
Male
Sagittarius
70
46
Knight
Black Magic
Counter Magic
Concentrate
Waterwalking

Slasher
Round Shield
Circlet
Wizard Robe
Rubber Shoes

Head Break, Shield Break, Power Break, Justice Sword, Surging Sword, Explosion Sword
Fire, Fire 2, Fire 3, Bolt, Bolt 3, Ice 3, Death, Flare



LDSkinny
Female
Capricorn
66
64
Oracle
Steal
Meatbone Slash
Equip Shield
Retreat

Battle Folio
Buckler
Headgear
Black Robe
Reflect Ring

Poison, Life Drain, Doubt Faith, Zombie, Silence Song, Confusion Song, Dispel Magic, Petrify
Steal Weapon, Arm Aim

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Deathmaker06
Male
Virgo
57
79
Knight
Throw
Distribute
Dual Wield
Move-MP Up

Battle Axe
Slasher
Diamond Helmet
Genji Armor
Genji Gauntlet

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Dark Sword, Night Sword
Shuriken



Inosukeanu
Female
Aries
53
65
Mediator
Summon Magic
Counter Magic
Equip Knife
Swim

Spell Edge

Thief Hat
Secret Clothes
108 Gems

Persuade, Insult, Mimic Daravon
Moogle, Carbunkle, Odin



TJ FordCDN
Male
Libra
76
42
Squire
Time Magic
HP Restore
Short Charge
Move+2

Sleep Sword
Diamond Shield
Twist Headband
Wizard Outfit
Wizard Mantle

Accumulate, Heal, Cheer Up, Wish, Scream
Haste, Haste 2, Demi 2, Stabilize Time, Meteor



Tronfonne
Male
Pisces
54
63
Squire
Black Magic
Counter Tackle
Defend
Jump+1

Night Killer
Buckler
Green Beret
Clothes
Diamond Armlet

Accumulate, Heal, Yell, Cheer Up, Fury, Wish
Bolt, Ice 2, Ice 3, Ice 4

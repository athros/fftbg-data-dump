Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



XalSmellsBad
Monster
Capricorn
67
47
Tiamat










DudeMonkey77
Female
Gemini
69
47
Dancer
White Magic
Caution
Magic Defense UP
Move-HP Up

Cashmere

Green Beret
Robe of Lords
Spike Shoes

Last Dance, Obsidian Blade, Nether Demon, Dragon Pit
Cure 2, Raise, Reraise, Esuna



YaBoy125
Male
Taurus
63
44
Chemist
Basic Skill
Sunken State
Concentrate
Move-MP Up

Mythril Gun

Leather Hat
Mythril Vest
Cursed Ring

Ether, Antidote, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Throw Stone, Cheer Up, Wish



Evewho
Female
Scorpio
71
57
Mediator
Summon Magic
Speed Save
Equip Bow
Ignore Height

Mythril Bow

Leather Hat
Silk Robe
Spike Shoes

Praise, Threaten, Solution, Death Sentence, Negotiate, Rehabilitate
Moogle, Carbunkle, Odin, Leviathan, Salamander

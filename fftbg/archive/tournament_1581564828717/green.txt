Player: !Green
Team: Green Team
Palettes: Green/White



TheMurkGnome
Female
Leo
65
75
Squire
Black Magic
MA Save
Defense UP
Ignore Terrain

Bow Gun

Ribbon
Linen Cuirass
Defense Armlet

Heal, Yell, Wish
Fire 4, Ice, Ice 3, Ice 4



Bad1dea
Monster
Leo
66
50
Red Panther










Rocl
Male
Libra
43
52
Priest
Draw Out
Sunken State
Sicken
Lava Walking

Scorpion Tail

Leather Hat
White Robe
Dracula Mantle

Cure 2, Cure 3, Raise, Reraise, Protect, Esuna
Heaven's Cloud



SpencoJFrog
Male
Scorpio
64
74
Monk
Yin Yang Magic
Dragon Spirit
Long Status
Waterwalking



Cachusha
Chain Vest
Germinas Boots

Spin Fist, Wave Fist, Earth Slash, Purification, Chakra, Revive
Blind, Spell Absorb, Doubt Faith, Silence Song, Dispel Magic, Paralyze

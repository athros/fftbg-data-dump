Player: !White
Team: White Team
Palettes: White/Blue



CorpusCav
Monster
Gemini
72
60
Archaic Demon










WeatherWolf4
Male
Capricorn
65
72
Ninja
Yin Yang Magic
Faith Up
Martial Arts
Lava Walking



Feather Hat
Leather Vest
Dracula Mantle

Knife
Blind, Doubt Faith, Foxbird, Confusion Song, Dispel Magic



CynicalRazor
Male
Virgo
45
74
Ninja
Summon Magic
MA Save
Martial Arts
Move+2



Twist Headband
Earth Clothes
Bracer

Shuriken, Wand
Moogle, Shiva, Bahamut



Concles
Female
Sagittarius
78
64
Geomancer
Time Magic
MP Restore
Beastmaster
Move+1

Slasher
Mythril Shield
Headgear
Chameleon Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard
Haste, Slow, Slow 2, Stop, Float, Stabilize Time

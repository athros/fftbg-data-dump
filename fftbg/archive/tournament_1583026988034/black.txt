Player: !Black
Team: Black Team
Palettes: Black/Red



BigDLicious91
Male
Leo
41
73
Samurai
Throw
Parry
Magic Attack UP
Move+3

Asura Knife

Gold Helmet
Black Robe
Angel Ring

Koutetsu, Bizen Boat, Murasame
Shuriken, Bomb, Knife, Dictionary



Clippopo
Male
Serpentarius
73
45
Wizard
Sing
Critical Quick
Dual Wield
Waterwalking

Flame Rod
Flame Rod
Golden Hairpin
Leather Outfit
Magic Gauntlet

Fire, Fire 3, Bolt 2, Ice 3, Empower, Flare
Life Song, Battle Song, Last Song, Hydra Pit



MyFakeLife
Male
Libra
57
50
Monk
Summon Magic
Counter
Doublehand
Move+3



Holy Miter
Earth Clothes
Dracula Mantle

Wave Fist, Secret Fist, Purification, Revive, Seal Evil
Titan



Spartan Paladin
Male
Taurus
51
41
Knight
Charge
Damage Split
Magic Defense UP
Ignore Terrain

Giant Axe
Ice Shield
Leather Helmet
Reflect Mail
N-Kai Armlet

Armor Break, Shield Break, Speed Break, Surging Sword
Charge+3, Charge+7

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Omegasuspekt
Male
Leo
58
42
Ninja
Yin Yang Magic
Dragon Spirit
Equip Bow
Move+3

Ultimus Bow

Headgear
Mythril Vest
Angel Ring

Shuriken, Bomb, Knife, Staff
Blind, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Paralyze



Vivithegr8
Male
Virgo
57
76
Oracle
Charge
Auto Potion
Defense UP
Swim

Battle Folio

Black Hood
Power Sleeve
Reflect Ring

Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic
Charge+2, Charge+3, Charge+5, Charge+7



Kl0kwurk
Male
Gemini
66
43
Knight
Item
PA Save
Throw Item
Retreat

Ice Brand
Gold Shield
Barbuta
Carabini Mail
Angel Ring

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Dark Sword, Night Sword, Surging Sword
Potion, Hi-Potion, Ether, Maiden's Kiss, Holy Water, Phoenix Down



Rolodex
Female
Sagittarius
42
47
Time Mage
Item
Arrow Guard
Magic Attack UP
Lava Walking

Healing Staff

Headgear
Mystic Vest
Elf Mantle

Haste, Slow 2, Reflect, Demi, Demi 2
Hi-Potion, Eye Drop

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Coralreeferz
Female
Libra
46
58
Ninja
Draw Out
Counter Tackle
Equip Gun
Jump+1

Fairy Harp
Battle Folio
Triangle Hat
Wizard Outfit
Dracula Mantle

Bomb, Knife
Asura, Bizen Boat, Muramasa



Killth3kid
Male
Scorpio
59
52
Chemist
Elemental
Abandon
Equip Polearm
Jump+3

Obelisk

Leather Hat
Adaman Vest
Wizard Mantle

Potion, Hi-Potion, X-Potion, Ether, Elixir, Eye Drop, Soft, Remedy, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



RuneS 77
Male
Serpentarius
76
68
Mime

Damage Split
Equip Shield
Ignore Terrain


Bronze Shield
Headgear
Judo Outfit
Leather Mantle

Mimic




Miku Shikhu
Female
Gemini
56
51
Mediator
Basic Skill
Earplug
Equip Shield
Ignore Terrain

Blast Gun
Diamond Shield
Feather Hat
Black Costume
Feather Boots

Mimic Daravon, Refute
Dash, Heal, Tickle

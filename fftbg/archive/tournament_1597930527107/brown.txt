Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Latebit
Male
Taurus
54
65
Wizard
Elemental
Distribute
Beastmaster
Jump+1

Thunder Rod

Ribbon
Silk Robe
Dracula Mantle

Fire 2, Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Ice 4
Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Sparker9
Monster
Taurus
60
48
Grenade










Phi Sig
Male
Aquarius
46
79
Chemist
Steal
Critical Quick
Equip Shield
Retreat

Main Gauche
Bronze Shield
Twist Headband
Earth Clothes
Diamond Armlet

Potion, Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Steal Shield, Steal Accessory, Leg Aim



Ar Tactic
Female
Gemini
67
39
Archer
Battle Skill
Counter
Maintenance
Retreat

Mythril Bow

Twist Headband
Brigandine
Leather Mantle

Charge+1, Charge+2
Shield Break, Weapon Break, Power Break, Mind Break, Night Sword

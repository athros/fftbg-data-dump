Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Roqqqpsi
Female
Capricorn
41
44
Mediator
Throw
Earplug
Short Status
Jump+3

Glacier Gun

Black Hood
Wizard Robe
Red Shoes

Persuade, Praise, Threaten, Solution, Death Sentence
Shuriken, Dictionary



Ramza Chan
Male
Cancer
62
39
Oracle
Sing
Absorb Used MP
Secret Hunt
Retreat

Ivory Rod

Twist Headband
Mythril Vest
Battle Boots

Life Drain, Doubt Faith, Blind Rage, Paralyze
Angel Song



Cataphract116
Female
Aquarius
47
56
Ninja
Steal
Counter Magic
Doublehand
Retreat

Mythril Knife

Black Hood
Power Sleeve
Feather Mantle

Shuriken, Bomb, Hammer
Steal Helmet



Lydian C
Female
Taurus
71
73
Calculator
White Magic
Auto Potion
Beastmaster
Move-MP Up

Octagon Rod

Headgear
Silk Robe
Angel Ring

CT, Prime Number, 3
Cure 3, Regen, Shell, Shell 2, Holy

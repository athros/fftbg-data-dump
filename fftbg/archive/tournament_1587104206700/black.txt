Player: !Black
Team: Black Team
Palettes: Black/Red



Evewho
Female
Aquarius
43
46
Knight
Black Magic
MA Save
Sicken
Retreat

Materia Blade
Venetian Shield
Platinum Helmet
Leather Armor
Feather Boots

Shield Break, Dark Sword
Bolt 3, Ice 2, Frog



Ross From Cali
Male
Aries
66
66
Priest
Basic Skill
Auto Potion
Equip Shield
Retreat

Rainbow Staff
Round Shield
Leather Hat
Robe of Lords
Germinas Boots

Cure, Raise, Regen, Shell, Wall, Holy
Accumulate, Dash, Tickle, Cheer Up, Fury, Wish, Scream



NovaKnight21
Female
Sagittarius
43
72
Calculator
Limit
Faith Up
Beastmaster
Waterwalking

Papyrus Codex

Gold Helmet
Power Sleeve
Feather Boots

Blue Magic
Braver, Cross-Slash, Blade Beam, Climhazzard, Meteorain, Finish Touch, Omnislash, Cherry Blossom



RongRongArts
Male
Cancer
40
75
Chemist
Draw Out
Counter Magic
Halve MP
Lava Walking

Stone Gun

Flash Hat
Adaman Vest
Bracer

Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Muramasa

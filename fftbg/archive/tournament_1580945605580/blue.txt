Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Haatexiii
Monster
Aquarius
69
46
Bomb










CapnChaos12
Male
Capricorn
66
69
Monk
Draw Out
MA Save
Sicken
Retreat



Holy Miter
Adaman Vest
Bracer

Revive, Seal Evil
Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori



De1337ed
Male
Libra
61
68
Ninja
White Magic
PA Save
Equip Bow
Swim

Bow Gun
Poison Bow
Feather Hat
Leather Outfit
Magic Gauntlet

Shuriken, Bomb
Cure 2, Cure 4, Raise, Raise 2, Regen, Protect, Shell 2, Esuna



Wolflux
Male
Pisces
70
50
Chemist
Throw
Abandon
Sicken
Ignore Terrain

Assassin Dagger

Thief Hat
Leather Outfit
Leather Mantle

Potion, Hi-Potion, Elixir, Eye Drop, Maiden's Kiss, Soft, Holy Water, Phoenix Down
Shuriken

Player: !Green
Team: Green Team
Palettes: Green/White



JJ Roo
Male
Cancer
74
77
Geomancer
Item
Faith Up
Throw Item
Jump+1

Slasher
Ice Shield
Holy Miter
Wizard Robe
Diamond Armlet

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Hi-Potion, Phoenix Down



Sypheck
Female
Aquarius
47
66
Time Mage
Jump
Meatbone Slash
Equip Armor
Jump+2

Rainbow Staff

Green Beret
Silk Robe
Battle Boots

Haste, Float, Demi 2, Stabilize Time
Level Jump4, Vertical Jump5



Vanesco
Male
Gemini
45
45
Wizard
Talk Skill
Catch
Secret Hunt
Lava Walking

Mage Masher

Cachusha
Wizard Robe
Defense Armlet

Bolt, Bolt 3, Ice 2, Flare
Invitation, Solution, Insult, Negotiate, Rehabilitate



Eltaggerung
Female
Cancer
68
62
Summoner
White Magic
MA Save
Short Status
Swim

Oak Staff

Headgear
Black Robe
Red Shoes

Moogle, Shiva, Titan, Carbunkle, Leviathan, Silf, Fairy
Regen, Shell, Esuna

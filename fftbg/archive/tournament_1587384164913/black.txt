Player: !Black
Team: Black Team
Palettes: Black/Red



UmaiJam
Female
Gemini
74
46
Calculator
Black Magic
Abandon
Maintenance
Ignore Height

Papyrus Codex

Leather Hat
Chameleon Robe
Power Wrist

Height, Prime Number, 3
Fire, Fire 4, Bolt, Bolt 3, Bolt 4, Flare



Hamborn
Male
Sagittarius
72
70
Squire
Steal
Regenerator
Beastmaster
Move-HP Up

Platinum Sword
Bronze Shield
Golden Hairpin
Chain Mail
Germinas Boots

Heal, Yell
Steal Heart, Steal Helmet, Steal Shield, Arm Aim



ExecutedGiraffe
Male
Capricorn
72
78
Mediator
White Magic
Sunken State
Equip Armor
Teleport

Mythril Gun

Twist Headband
Crystal Mail
Magic Gauntlet

Persuade, Threaten, Insult, Mimic Daravon, Refute
Cure 2, Raise, Protect, Esuna



Dexsana
Female
Sagittarius
59
61
Mediator
Charge
Regenerator
Equip Polearm
Jump+1

Javelin

Thief Hat
White Robe
Diamond Armlet

Invitation, Persuade, Threaten, Solution, Insult, Mimic Daravon, Rehabilitate
Charge+5

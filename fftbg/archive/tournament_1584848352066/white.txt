Player: !White
Team: White Team
Palettes: White/Blue



Virilikus
Female
Taurus
71
58
Summoner
Punch Art
Auto Potion
Short Charge
Move+1

Faith Rod

Headgear
Black Robe
Defense Ring

Shiva, Titan, Carbunkle, Odin
Wave Fist, Earth Slash, Revive



Sotehr
Male
Leo
57
76
Archer
Talk Skill
Absorb Used MP
Doublehand
Fly

Bow Gun

Leather Hat
Judo Outfit
Bracer

Charge+1, Charge+3, Charge+4, Charge+5
Praise, Threaten, Death Sentence, Insult, Negotiate, Refute



Heroebal
Male
Pisces
70
65
Ninja
Time Magic
Caution
Equip Sword
Lava Walking

Ice Brand
Ancient Sword
Cachusha
Rubber Costume
Cherche

Shuriken, Knife, Wand
Haste, Demi, Stabilize Time, Meteor



Bpc2163
Male
Gemini
47
68
Monk
Time Magic
Absorb Used MP
Defense UP
Levitate



Green Beret
Rubber Costume
Power Wrist

Wave Fist, Secret Fist, Purification
Slow, Immobilize, Reflect, Demi 2

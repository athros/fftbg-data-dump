Player: !Red
Team: Red Team
Palettes: Red/Brown



PoroTact
Male
Libra
77
77
Thief
Item
HP Restore
Short Status
Move+1

Main Gauche

Headgear
Mythril Vest
Bracer

Steal Heart, Arm Aim
Potion, Ether, Hi-Ether, Antidote, Phoenix Down



Shalloween
Male
Aries
60
77
Knight
Draw Out
Blade Grasp
Beastmaster
Teleport 2

Coral Sword
Ice Shield
Genji Helmet
Chain Mail
Defense Ring

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Dark Sword, Explosion Sword
Koutetsu, Muramasa, Kikuichimoji, Masamune



Meyples
Female
Capricorn
61
58
Archer
Draw Out
Regenerator
Equip Armor
Lava Walking

Cute Bag
Buckler
Holy Miter
Silk Robe
Leather Mantle

Charge+1, Charge+5, Charge+7, Charge+10, Charge+20
Asura, Bizen Boat, Murasame, Kiyomori



Xoomwaffle
Female
Scorpio
74
67
Chemist
Battle Skill
Distribute
Equip Gun
Move+2

Blast Gun

Leather Hat
Judo Outfit
Red Shoes

Potion, Phoenix Down
Head Break, Night Sword, Explosion Sword

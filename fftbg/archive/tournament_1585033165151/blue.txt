Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Unyubaby
Female
Taurus
42
70
Calculator
Black Magic
Regenerator
Defend
Waterbreathing

Papyrus Codex

Feather Hat
Wizard Outfit
Angel Ring

CT, Height, 4
Fire, Fire 2, Bolt 2, Ice 4, Frog, Death



Kyton786
Male
Sagittarius
54
45
Bard
Elemental
Parry
Concentrate
Levitate

Fairy Harp

Holy Miter
Crystal Mail
Leather Mantle

Angel Song, Life Song, Cheer Song, Nameless Song, Last Song
Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Lava Ball



Thunderducker
Female
Aries
51
80
Dancer
Yin Yang Magic
Earplug
Doublehand
Swim

Star Bag

Black Hood
Chameleon Robe
Vanish Mantle

Witch Hunt, Wiznaibus, Nameless Dance, Last Dance, Obsidian Blade, Dragon Pit
Pray Faith, Dispel Magic, Paralyze



Superdevon1
Male
Serpentarius
72
75
Chemist
Throw
Damage Split
Equip Armor
Move+3

Blaze Gun

Crystal Helmet
Linen Robe
Reflect Ring

Potion, X-Potion, Ether, Hi-Ether, Antidote, Soft, Phoenix Down
Stick

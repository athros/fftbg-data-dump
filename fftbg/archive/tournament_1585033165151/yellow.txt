Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Grininda
Female
Virgo
70
48
Monk
Jump
Critical Quick
Equip Axe
Retreat

Flame Whip

Red Hood
Black Costume
Bracer

Spin Fist, Pummel, Wave Fist, Purification, Revive, Seal Evil
Level Jump5, Vertical Jump8



SeedSC
Female
Leo
80
44
Ninja
Talk Skill
Parry
Equip Gun
Swim

Bestiary
Bestiary
Red Hood
Power Sleeve
Cursed Ring

Shuriken, Knife
Persuade, Praise, Death Sentence, Negotiate, Mimic Daravon, Refute



Bad1dea
Female
Pisces
43
72
Calculator
Beast Skill
Hamedo
Equip Shield
Ignore Height

Dragon Rod
Bronze Shield
Feather Hat
Carabini Mail
Feather Mantle

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



EunosXX
Male
Aries
44
56
Ninja
Talk Skill
Regenerator
Equip Gun
Swim

Madlemgen
Papyrus Codex
Cachusha
Wizard Outfit
Elf Mantle

Shuriken, Bomb, Knife, Hammer, Wand
Invitation, Threaten, Solution, Refute

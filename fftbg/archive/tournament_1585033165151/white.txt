Player: !White
Team: White Team
Palettes: White/Blue



Humble Fabio
Female
Gemini
79
74
Geomancer
Item
Damage Split
Long Status
Move+1

Slasher
Flame Shield
Green Beret
White Robe
Reflect Ring

Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Soft



Jabuamba
Male
Cancer
45
62
Ninja
Item
Caution
Martial Arts
Levitate

Spell Edge
Star Bag
Ribbon
Mythril Vest
Feather Mantle

Dictionary
Hi-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Holy Water, Remedy, Phoenix Down



AzuriteReaction
Male
Leo
77
52
Ninja
Black Magic
Counter
Equip Axe
Move-HP Up

Slasher
White Staff
Red Hood
Earth Clothes
Leather Mantle

Shuriken, Bomb
Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4



Cryptopsy70
Female
Leo
53
57
Monk
Dance
Mana Shield
Beastmaster
Fly



Golden Hairpin
Leather Outfit
Bracer

Pummel, Secret Fist, Purification
Disillusion, Nameless Dance

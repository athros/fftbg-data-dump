Player: !Black
Team: Black Team
Palettes: Black/Red



DarkLeviathan89
Male
Taurus
71
73
Monk
Draw Out
Auto Potion
Sicken
Jump+2



Green Beret
Black Costume
Spike Shoes

Spin Fist, Pummel, Purification, Chakra, Revive, Seal Evil
Heaven's Cloud, Chirijiraden



Mousepad22
Female
Cancer
68
57
Squire
Charge
Earplug
Concentrate
Levitate

Cute Bag

Headgear
Leather Outfit
Magic Gauntlet

Accumulate, Throw Stone, Heal, Scream
Charge+3, Charge+7



MoonSlayerRS
Male
Virgo
50
55
Squire
Punch Art
Abandon
Dual Wield
Move-MP Up

Iron Sword
Blood Sword
Leather Hat
Clothes
Power Wrist

Dash, Heal, Cheer Up, Wish, Scream
Spin Fist, Purification, Chakra, Revive, Seal Evil



SergeantTeddy
Female
Gemini
70
46
Archer
Dance
Damage Split
Beastmaster
Jump+1

Yoichi Bow

Golden Hairpin
Earth Clothes
Sprint Shoes

Charge+7, Charge+10
Wiznaibus, Slow Dance, Disillusion, Last Dance, Obsidian Blade, Void Storage, Nether Demon

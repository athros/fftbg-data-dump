Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Sinnyil2
Male
Capricorn
69
42
Lancer
Punch Art
MA Save
Secret Hunt
Levitate

Spear
Aegis Shield
Crystal Helmet
Plate Mail
Sprint Shoes

Level Jump8, Vertical Jump7
Spin Fist, Pummel, Purification, Revive



SkylerBunny
Female
Serpentarius
53
60
Wizard
Throw
MA Save
Halve MP
Ignore Terrain

Faith Rod

Triangle Hat
Chain Vest
Dracula Mantle

Fire, Fire 3, Fire 4, Bolt 4, Ice, Empower
Shuriken, Dictionary



Lokenwow
Male
Scorpio
70
67
Knight
Yin Yang Magic
Sunken State
Equip Armor
Ignore Terrain

Defender
Flame Shield
Feather Hat
Diamond Armor
Diamond Armlet

Head Break, Armor Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword
Life Drain, Doubt Faith, Foxbird, Confusion Song



UmaiJam
Female
Capricorn
80
76
Summoner
Elemental
MP Restore
Short Charge
Lava Walking

Ice Rod

Golden Hairpin
Brigandine
Diamond Armlet

Carbunkle, Fairy
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball

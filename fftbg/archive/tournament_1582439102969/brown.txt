Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Bryan792
Monster
Capricorn
58
44
Steel Giant










Shadowkept
Female
Aquarius
54
59
Ninja
Draw Out
Parry
Concentrate
Move-MP Up

Scorpion Tail
Flame Whip
Thief Hat
Chain Vest
Germinas Boots

Axe
Koutetsu, Heaven's Cloud



Seeingeye24
Male
Sagittarius
75
58
Priest
Talk Skill
Catch
Equip Sword
Move+2

Diamond Sword

Thief Hat
Mythril Vest
Diamond Armlet

Cure, Cure 2, Cure 3, Raise, Protect 2, Wall, Esuna, Holy
Invitation, Preach, Solution, Rehabilitate



Dynasti
Female
Libra
74
78
Archer
Item
Auto Potion
Throw Item
Move+1

Long Bow

Golden Hairpin
Black Costume
Cursed Ring

Charge+1, Charge+2, Charge+5, Charge+7, Charge+10
Potion, Ether, Hi-Ether, Eye Drop

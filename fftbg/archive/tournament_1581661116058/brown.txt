Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Tomion
Monster
Scorpio
60
51
Squidraken










Denamda
Female
Libra
66
48
Geomancer
Dance
Earplug
Maintenance
Jump+2

Giant Axe
Flame Shield
Triangle Hat
Light Robe
Salty Rage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard, Gusty Wind
Witch Hunt, Polka Polka, Nameless Dance, Void Storage, Dragon Pit



Sypheck
Female
Gemini
68
50
Samurai
Black Magic
Counter
Equip Bow
Move-HP Up

Hunting Bow

Platinum Helmet
Gold Armor
Red Shoes

Heaven's Cloud, Kikuichimoji
Bolt 3, Ice



Haatexiii
Female
Cancer
54
45
Priest
Elemental
MP Restore
Magic Attack UP
Ignore Terrain

Star Bag

Triangle Hat
Black Robe
Chantage

Regen, Protect, Protect 2, Shell, Esuna
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

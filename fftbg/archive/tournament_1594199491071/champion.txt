Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



KasugaiRoastedPeas
Male
Aries
52
55
Archer
Time Magic
HP Restore
Doublehand
Move-MP Up

Hunting Bow

Headgear
Chain Vest
Diamond Armlet

Charge+2, Charge+3, Charge+20
Haste, Slow, Stop, Quick, Demi 2, Stabilize Time



PrancesWithWolves
Female
Taurus
54
41
Archer
Summon Magic
MA Save
Secret Hunt
Ignore Terrain

Snipe Bow
Escutcheon
Red Hood
Earth Clothes
Battle Boots

Charge+1, Charge+3, Charge+4, Charge+7
Moogle, Ramuh, Lich



Lowlf
Female
Taurus
52
78
Monk
White Magic
Meatbone Slash
Sicken
Move+1



Flash Hat
Judo Outfit
Vanish Mantle

Spin Fist, Wave Fist, Earth Slash, Purification, Revive
Cure, Cure 2, Cure 3, Raise, Reraise, Regen, Shell, Wall, Esuna, Holy



Shineeyo
Male
Sagittarius
76
47
Calculator
Black Magic
Damage Split
Concentrate
Move-MP Up

Battle Bamboo

Holy Miter
Wizard Outfit
Reflect Ring

CT, Height, 5, 4, 3
Fire 3, Fire 4, Ice, Ice 4, Death, Flare

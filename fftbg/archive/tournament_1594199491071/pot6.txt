Final Bets: white - 9 bets for 5,066G (41.2%, x1.43); brown - 7 bets for 7,241G (58.8%, x0.70)

white bets:
RaIshtar: 1,800G (35.5%, 13,882G)
NovaKnight21: 1,000G (19.7%, 45,505G)
Evewho: 1,000G (19.7%, 11,137G)
Spuzzmocker: 300G (5.9%, 300G)
wyonearth: 268G (5.3%, 268G)
cbergs: 236G (4.7%, 236G)
datadrivenbot: 200G (3.9%, 59,296G)
getthemoneyz: 162G (3.2%, 1,228,155G)
randgridr: 100G (2.0%, 1,387G)

brown bets:
Hasterious: 2,470G (34.1%, 2,470G)
twelfthrootoftwo: 1,141G (15.8%, 2,238G)
Draconis345: 1,000G (13.8%, 72,782G)
BirbBrainsBot: 1,000G (13.8%, 151,274G)
Sairentozon7: 1,000G (13.8%, 4,363G)
Digitalsocrates: 380G (5.2%, 1,800G)
Lythe_Caraker: 250G (3.5%, 149,523G)

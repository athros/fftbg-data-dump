Player: !Green
Team: Green Team
Palettes: Green/White



ALY327
Female
Taurus
41
64
Squire
Summon Magic
Abandon
Long Status
Move+3

Blind Knife
Genji Shield
Black Hood
Secret Clothes
Jade Armlet

Accumulate, Throw Stone, Heal, Tickle, Fury
Moogle, Shiva, Titan, Carbunkle, Leviathan, Silf, Lich



Slider1012
Female
Aries
43
59
Mediator
Basic Skill
Arrow Guard
Equip Polearm
Levitate

Ivory Rod

Feather Hat
Chameleon Robe
Battle Boots

Persuade, Praise, Threaten, Preach, Solution, Insult, Negotiate, Mimic Daravon, Refute
Accumulate, Throw Stone, Heal, Fury



Bahamutlagooon
Male
Virgo
60
73
Calculator
Lucavi Skill
PA Save
Secret Hunt
Teleport

Thunder Rod
Crystal Shield
Twist Headband
Chain Vest
108 Gems

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



GrayGhostGaming
Female
Serpentarius
59
76
Dancer
White Magic
Counter
Defense UP
Swim

Persia

Holy Miter
Clothes
Feather Boots

Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Last Dance, Obsidian Blade
Cure, Cure 2, Raise, Raise 2, Regen, Protect, Wall, Esuna

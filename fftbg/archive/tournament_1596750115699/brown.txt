Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Rednecknazgul
Female
Taurus
40
53
Thief
Dance
Parry
Doublehand
Waterwalking

Platinum Sword

Golden Hairpin
Adaman Vest
Power Wrist

Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Polka Polka, Disillusion



DeathTaxesAndAnime
Female
Virgo
60
65
Mime

Counter
Equip Shield
Ignore Terrain


Genji Shield
Holy Miter
Mythril Vest
Angel Ring

Mimic




Zenlion
Male
Aquarius
76
68
Chemist
Talk Skill
Dragon Spirit
Monster Talk
Jump+1

Cute Bag

Headgear
Chain Vest
108 Gems

Potion, Antidote, Echo Grass, Maiden's Kiss, Phoenix Down
Invitation, Persuade, Threaten, Refute



Holdenmagronik
Female
Taurus
69
56
Knight
Summon Magic
Abandon
Maintenance
Jump+2

Defender
Platinum Shield
Leather Helmet
Diamond Armor
Cherche

Head Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Dark Sword
Ifrit, Golem, Carbunkle, Silf, Fairy

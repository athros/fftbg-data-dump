Player: !Red
Team: Red Team
Palettes: Red/Brown



Windsah
Male
Pisces
65
66
Monk
Item
Counter
Doublehand
Jump+1



Holy Miter
Clothes
Angel Ring

Wave Fist, Earth Slash, Chakra, Revive
Potion, X-Potion, Antidote, Holy Water, Phoenix Down



Gorgewall
Female
Gemini
52
65
Thief
Punch Art
Auto Potion
Long Status
Move-HP Up

Rune Blade

Leather Hat
Mythril Vest
Vanish Mantle

Steal Heart, Steal Shield, Steal Accessory, Steal Status, Leg Aim
Secret Fist, Purification, Revive



VolgraTheMoose
Male
Gemini
51
68
Mime

Counter
Dual Wield
Waterwalking



Leather Hat
Earth Clothes
Leather Mantle

Mimic




Firesheath
Male
Aries
79
60
Samurai
Battle Skill
Meatbone Slash
Short Status
Fly

Heaven's Cloud

Circlet
Plate Mail
Battle Boots

Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji
Shield Break, Magic Break, Speed Break, Stasis Sword

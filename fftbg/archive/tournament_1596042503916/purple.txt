Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Helpimabug
Male
Capricorn
44
43
Thief
Jump
Auto Potion
Beastmaster
Waterwalking

Short Edge

Barette
Judo Outfit
N-Kai Armlet

Steal Armor
Level Jump4, Vertical Jump7



BlackfyreRoy
Male
Sagittarius
40
66
Calculator
Black Magic
Parry
Equip Knife
Jump+1

Thunder Rod

Flash Hat
Chain Vest
N-Kai Armlet

Height, 5, 3
Fire, Fire 3, Fire 4, Bolt 4, Ice, Ice 4, Death



Actual JP
Male
Gemini
53
49
Knight
Basic Skill
Counter Tackle
Magic Defense UP
Teleport

Defender
Aegis Shield
Genji Helmet
Wizard Robe
Wizard Mantle

Armor Break, Weapon Break, Magic Break, Speed Break, Mind Break, Stasis Sword, Justice Sword
Accumulate, Throw Stone, Tickle, Fury



Daveb
Male
Leo
77
65
Ninja
Elemental
Mana Shield
Equip Gun
Ignore Height

Battle Folio
Bloody Strings
Leather Hat
Adaman Vest
Feather Boots

Shuriken, Bomb, Axe, Stick, Dictionary
Hallowed Ground, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

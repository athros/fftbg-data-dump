Player: !Red
Team: Red Team
Palettes: Red/Brown



Zagorsek
Male
Virgo
57
63
Oracle
Time Magic
Distribute
Defend
Ignore Terrain

Papyrus Codex

Twist Headband
Judo Outfit
Jade Armlet

Blind, Poison, Spell Absorb, Blind Rage, Foxbird, Confusion Song, Sleep
Slow, Reflect, Quick, Demi, Stabilize Time



LDSkinny
Female
Aries
45
68
Oracle
Draw Out
Arrow Guard
Equip Gun
Retreat

Madlemgen

Green Beret
Silk Robe
Reflect Ring

Blind, Poison, Life Drain, Silence Song, Foxbird, Dispel Magic, Paralyze
Koutetsu, Bizen Boat



Nifboy
Female
Capricorn
69
58
Ninja
Time Magic
Counter Flood
Equip Bow
Swim

Bow Gun
Hunting Bow
Triangle Hat
Wizard Outfit
Wizard Mantle

Shuriken, Bomb, Ninja Sword
Haste, Haste 2, Slow, Reflect, Stabilize Time



Brokenknight201
Male
Libra
71
65
Knight
Yin Yang Magic
Parry
Dual Wield
Move+2

Rune Blade
Blood Sword
Gold Helmet
Genji Armor
Feather Boots

Head Break, Magic Break, Speed Break, Night Sword
Blind, Poison, Pray Faith, Zombie, Dispel Magic, Sleep

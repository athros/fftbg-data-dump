Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Forkmore
Male
Taurus
77
63
Geomancer
Battle Skill
Critical Quick
Secret Hunt
Ignore Terrain

Battle Axe
Platinum Shield
Thief Hat
Silk Robe
Defense Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Shield Break, Power Break, Stasis Sword, Night Sword, Surging Sword



Gawyel95
Female
Pisces
52
51
Mime

PA Save
Secret Hunt
Ignore Height



Thief Hat
Wizard Outfit
Genji Gauntlet

Mimic




HaateXIII
Female
Taurus
71
46
Wizard
Talk Skill
Caution
Halve MP
Move-MP Up

Dagger

Golden Hairpin
Silk Robe
Rubber Shoes

Fire 3, Fire 4, Bolt, Bolt 2, Bolt 4, Ice 2, Empower, Flare
Praise, Threaten, Insult, Negotiate, Rehabilitate



Miku Shikhu
Female
Virgo
69
78
Mediator
Dance
Distribute
Attack UP
Move+2

Glacier Gun

Twist Headband
Earth Clothes
Defense Ring

Persuade, Praise, Negotiate, Mimic Daravon, Refute
Polka Polka, Last Dance, Obsidian Blade, Void Storage, Nether Demon

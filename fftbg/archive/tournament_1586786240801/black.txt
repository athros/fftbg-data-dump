Player: !Black
Team: Black Team
Palettes: Black/Red



Club Sodar
Male
Cancer
57
71
Ninja
Draw Out
MA Save
Equip Polearm
Move+3

Ryozan Silk
Persia
Red Hood
Mystic Vest
Power Wrist

Shuriken, Bomb, Sword, Wand
Bizen Boat, Murasame



Gongonono
Female
Cancer
54
56
Calculator
Yin Yang Magic
Hamedo
Magic Attack UP
Move+2

Flame Rod

Golden Hairpin
Black Costume
Dracula Mantle

CT, Height, Prime Number, 3
Poison, Life Drain, Pray Faith, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze



Fluffywormhole
Male
Gemini
55
64
Thief
Throw
Absorb Used MP
Equip Bow
Teleport

Mythril Bow

Holy Miter
Brigandine
Defense Ring

Steal Weapon, Steal Accessory
Spear



Kennymillennium
Female
Scorpio
56
68
Mediator
Time Magic
Counter Flood
Equip Sword
Jump+3

Long Sword

Black Hood
Mythril Vest
Sprint Shoes

Persuade, Praise, Threaten, Preach, Insult, Mimic Daravon, Rehabilitate
Haste, Haste 2, Stop, Float, Quick, Demi, Demi 2, Stabilize Time, Meteor, Galaxy Stop

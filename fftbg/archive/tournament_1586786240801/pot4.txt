Final Bets: purple - 18 bets for 19,698G (76.6%, x0.31); brown - 15 bets for 6,030G (23.4%, x3.27)

purple bets:
YaBoy125: 6,895G (35.0%, 6,895G)
toka222: 5,750G (29.2%, 517,014G)
kai_shee: 1,001G (5.1%, 126,954G)
BirbBrainsBot: 1,000G (5.1%, 81,661G)
datadrivenbot: 857G (4.4%, 22,455G)
ungabunga_bot: 682G (3.5%, 117,813G)
HeroponThrawn: 624G (3.2%, 624G)
ColetteMSLP: 500G (2.5%, 25,294G)
athros13: 467G (2.4%, 467G)
Club_Sodar: 400G (2.0%, 1,506G)
ZephyrTempest: 369G (1.9%, 51,243G)
ShintaroNayaka: 300G (1.5%, 10,970G)
JherrichoWulf: 279G (1.4%, 279G)
LazarusAjin: 200G (1.0%, 1,000G)
lastly: 150G (0.8%, 13,981G)
SaintOmerville: 104G (0.5%, 104G)
Jennero: 100G (0.5%, 2,100G)
daveb_: 20G (0.1%, 1,826G)

brown bets:
Kennymillennium: 1,233G (20.4%, 1,233G)
TheChainNerd: 1,153G (19.1%, 1,153G)
Lanshaft: 600G (10.0%, 600G)
RubenFlonne: 500G (8.3%, 3,079G)
catfashions: 500G (8.3%, 846G)
RunicMagus: 336G (5.6%, 336G)
JumbocactuarX27: 328G (5.4%, 328G)
luminarii: 302G (5.0%, 15,906G)
AllInBot: 280G (4.6%, 280G)
AlenaZarek: 200G (3.3%, 3,359G)
getthemoneyz: 190G (3.2%, 479,184G)
gongonono: 108G (1.8%, 108G)
IndecisiveNinja: 100G (1.7%, 4,781G)
ApplesauceBoss: 100G (1.7%, 3,878G)
Treapvort: 100G (1.7%, 1,232G)

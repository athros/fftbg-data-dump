Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



CT 5 Holy
Monster
Aries
63
54
Serpentarius










Mikumari12
Male
Cancer
58
80
Summoner
Elemental
Brave Up
Defend
Waterwalking

Flame Rod

Twist Headband
Mystic Vest
Magic Gauntlet

Shiva, Ramuh, Ifrit, Carbunkle, Silf, Lich
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball



PatSouI
Male
Libra
47
68
Lancer
White Magic
Caution
Dual Wield
Ignore Height

Javelin
Dragon Whisker
Bronze Helmet
Carabini Mail
Sprint Shoes

Level Jump8, Vertical Jump4
Cure 3, Raise, Regen, Protect, Wall



ColetteMSLP
Female
Pisces
54
57
Dancer
Basic Skill
Abandon
Equip Sword
Waterbreathing

Masamune

Golden Hairpin
Silk Robe
Feather Mantle

Disillusion, Nether Demon
Accumulate, Dash, Throw Stone, Heal, Cheer Up, Wish, Scream

Player: !Black
Team: Black Team
Palettes: Black/Red



Ar Tactic
Male
Gemini
43
59
Knight
Throw
Catch
Equip Gun
Jump+1

Romanda Gun
Kaiser Plate
Circlet
Carabini Mail
Rubber Shoes

Head Break, Weapon Break, Magic Break, Speed Break, Power Break, Mind Break, Justice Sword
Shuriken



JLinkletter
Female
Libra
42
43
Priest
Elemental
Brave Up
Halve MP
Teleport

Healing Staff

Golden Hairpin
Silk Robe
Spike Shoes

Cure, Cure 3, Cure 4, Raise, Raise 2, Reraise, Protect 2, Shell, Shell 2, Esuna
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind



Kyrious
Female
Leo
78
64
Archer
Yin Yang Magic
Counter Flood
Maintenance
Swim

Glacier Gun
Buckler
Triangle Hat
Mystic Vest
Genji Gauntlet

Charge+5, Charge+10
Confusion Song, Paralyze, Sleep, Dark Holy



Killth3kid
Male
Capricorn
65
80
Ninja
Yin Yang Magic
Catch
Equip Gun
Teleport

Battle Folio
Madlemgen
Green Beret
Leather Outfit
Leather Mantle

Shuriken
Blind, Zombie, Foxbird

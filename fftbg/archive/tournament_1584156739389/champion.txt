Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Tougou
Female
Virgo
64
65
Archer
Jump
Earplug
Doublehand
Move-HP Up

Cross Bow

Platinum Helmet
Leather Outfit
Dracula Mantle

Charge+1, Charge+3, Charge+4, Charge+5
Level Jump3, Vertical Jump8



MalakoFox
Male
Scorpio
45
63
Samurai
Battle Skill
HP Restore
Attack UP
Retreat

Obelisk

Grand Helmet
Mythril Armor
Defense Armlet

Asura, Heaven's Cloud, Kikuichimoji
Head Break, Shield Break, Magic Break, Speed Break, Surging Sword



Genoguy
Female
Taurus
67
71
Priest
Draw Out
HP Restore
Equip Sword
Jump+1

Ancient Sword

Green Beret
Light Robe
Germinas Boots

Raise, Protect, Protect 2, Shell 2, Esuna
Asura, Bizen Boat, Heaven's Cloud, Muramasa, Kikuichimoji



Powermhero
Female
Scorpio
57
51
Thief
Time Magic
Faith Up
Martial Arts
Jump+2

Koga Knife

Triangle Hat
Black Costume
Power Wrist

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Haste, Slow 2, Stop, Float, Reflect, Quick, Demi 2, Stabilize Time, Meteor

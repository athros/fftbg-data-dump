Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Amiture
Female
Libra
51
50
Samurai
Throw
Counter Tackle
Magic Defense UP
Fly

Murasame

Diamond Helmet
Wizard Robe
Bracer

Kiyomori, Muramasa, Masamune
Hammer



DeliciousSub
Male
Leo
64
77
Chemist
Sing
Earplug
Dual Wield
Jump+2

Panther Bag
Cute Bag
Golden Hairpin
Clothes
Magic Gauntlet

Potion, Hi-Potion, X-Potion, Phoenix Down
Battle Song, Nameless Song



RagequitSA
Male
Pisces
65
49
Ninja
Draw Out
Brave Up
Equip Armor
Jump+3

Spell Edge
Ninja Edge
Barbuta
Gold Armor
Jade Armlet

Shuriken, Knife
Asura



GreatRedDragon
Female
Virgo
38
47
Oracle
Battle Skill
Parry
Defend
Waterwalking

Musk Rod

Red Hood
Wizard Robe
Reflect Ring

Blind, Life Drain, Pray Faith
Weapon Break, Magic Break, Speed Break

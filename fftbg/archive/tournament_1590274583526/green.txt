Player: !Green
Team: Green Team
Palettes: Green/White



CorpusCav
Female
Aries
59
54
Knight
Talk Skill
Auto Potion
Monster Talk
Jump+2

Defender
Crystal Shield
Bronze Helmet
Linen Cuirass
Battle Boots

Armor Break, Magic Break, Speed Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Persuade, Praise, Preach, Solution, Mimic Daravon



Kyune
Male
Virgo
73
46
Knight
Throw
HP Restore
Defense UP
Move-HP Up

Sleep Sword
Diamond Shield
Leather Helmet
Linen Cuirass
Germinas Boots

Weapon Break, Magic Break, Surging Sword
Shuriken, Knife



Hydroshade
Female
Sagittarius
47
75
Ninja
Battle Skill
Abandon
Equip Gun
Move+1

Battle Folio
Papyrus Codex
Thief Hat
Earth Clothes
Battle Boots

Shuriken
Weapon Break, Mind Break



Baron Von Scrub
Male
Aquarius
48
48
Knight
Talk Skill
Speed Save
Defense UP
Teleport 2

Sleep Sword
Aegis Shield
Bronze Helmet
Diamond Armor
Wizard Mantle

Armor Break, Magic Break, Speed Break, Power Break, Mind Break
Invitation, Praise, Threaten, Death Sentence, Mimic Daravon, Refute

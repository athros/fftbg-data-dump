Player: !White
Team: White Team
Palettes: White/Blue



Firesheath
Monster
Capricorn
35
79
Iron Hawk










Serperemagus
Male
Aquarius
72
63
Bard
Steal
Counter Tackle
Martial Arts
Jump+3



Twist Headband
Carabini Mail
Vanish Mantle

Angel Song, Cheer Song, Diamond Blade, Hydra Pit
Steal Armor, Steal Shield



BigGlorious
Female
Cancer
54
73
Knight
Black Magic
Parry
Dual Wield
Move+1

Ragnarok
Ancient Sword
Bronze Helmet
Reflect Mail
N-Kai Armlet

Armor Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword
Fire, Fire 4, Bolt 2, Ice, Ice 3, Ice 4, Frog



Powergems
Male
Serpentarius
74
55
Time Mage
Yin Yang Magic
Auto Potion
Beastmaster
Move-MP Up

Gokuu Rod

Leather Hat
Black Costume
Angel Ring

Haste, Slow, Immobilize, Stabilize Time, Meteor
Pray Faith, Blind Rage, Foxbird, Dispel Magic, Sleep

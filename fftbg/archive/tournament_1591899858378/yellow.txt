Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Cryptopsy70
Male
Virgo
57
76
Oracle
Elemental
Damage Split
Short Status
Waterwalking

Musk Rod

Triangle Hat
Mystic Vest
Feather Mantle

Life Drain, Pray Faith, Doubt Faith, Confusion Song
Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind



Baron Von Scrub
Male
Aquarius
59
66
Lancer
Sing
Parry
Equip Armor
Move+2

Javelin
Escutcheon
Golden Hairpin
Diamond Armor
Jade Armlet

Level Jump3, Vertical Jump7
Angel Song, Life Song, Magic Song, Space Storage, Sky Demon



Soltaker21
Female
Aries
61
69
Calculator
Beast Skill
Damage Split
Martial Arts
Jump+2


Crystal Shield
Black Hood
Chain Mail
Red Shoes

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Frozen Cry, Giga Flare, Hurricane, Ulmaguest



OneHundredFists
Female
Scorpio
62
47
Chemist
Yin Yang Magic
Dragon Spirit
Doublehand
Teleport

Star Bag

Red Hood
Adaman Vest
Diamond Armlet

Hi-Potion, Echo Grass, Maiden's Kiss
Poison, Life Drain, Doubt Faith, Silence Song, Petrify, Dark Holy

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Bojanges
Male
Leo
71
57
Oracle
Elemental
Mana Shield
Maintenance
Waterbreathing

Gokuu Rod

Flash Hat
Mythril Vest
Leather Mantle

Blind, Foxbird, Dispel Magic, Paralyze
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Gotastick
Male
Taurus
54
52
Monk
Time Magic
Parry
Equip Gun
Move+3

Battle Folio

Black Hood
Clothes
Magic Ring

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Haste, Slow, Slow 2, Stabilize Time



Lijarkh
Male
Aries
50
66
Monk
Talk Skill
Earplug
Equip Shield
Move-HP Up


Escutcheon
Feather Hat
Secret Clothes
Wizard Mantle

Pummel, Wave Fist, Earth Slash, Purification, Revive
Invitation, Persuade, Solution, Insult, Mimic Daravon, Refute, Rehabilitate



RyuTsuno
Male
Taurus
69
75
Wizard
Draw Out
Brave Up
Defense UP
Jump+3

Flame Rod

Leather Hat
Clothes
Angel Ring

Fire 2, Fire 3, Bolt 3, Flare
Bizen Boat, Murasame, Kikuichimoji, Chirijiraden

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Biske13
Male
Taurus
76
55
Oracle
Item
Sunken State
Equip Knife
Teleport

Musk Rod

Leather Hat
White Robe
Reflect Ring

Spell Absorb, Pray Faith, Zombie, Silence Song, Blind Rage, Dispel Magic, Petrify, Dark Holy
Potion, Ether, Antidote, Soft, Remedy, Phoenix Down



Goust18
Female
Libra
75
71
Chemist
Elemental
Faith Up
Magic Defense UP
Teleport

Cute Bag

Golden Hairpin
Adaman Vest
Feather Boots

Hi-Ether, Antidote, Eye Drop, Echo Grass, Holy Water, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm



L2 Sentinel
Male
Leo
43
70
Lancer
Black Magic
Mana Shield
Beastmaster
Ignore Terrain

Octagon Rod
Genji Shield
Circlet
Diamond Armor
Elf Mantle

Level Jump8, Vertical Jump3
Fire, Fire 3, Bolt, Ice 2, Ice 3



KGM20
Male
Pisces
70
71
Summoner
Charge
Caution
Equip Bow
Move-MP Up

Lightning Bow

Thief Hat
Silk Robe
Feather Boots

Moogle, Golem, Odin, Silf
Charge+1, Charge+5, Charge+7

Final Bets: red - 11 bets for 4,973G (30.0%, x2.33); white - 21 bets for 11,581G (70.0%, x0.43)

red bets:
edgehead62888: 1,000G (20.1%, 34,506G)
BirbBrainsBot: 1,000G (20.1%, 83,475G)
Thyrandaal: 1,000G (20.1%, 18,425G)
kaelsun: 475G (9.6%, 475G)
DLJuggernaut: 350G (7.0%, 3,084G)
gorgewall: 348G (7.0%, 348G)
nothclif: 200G (4.0%, 1,152G)
Who_lio42: 200G (4.0%, 15,635G)
Primeval_33: 200G (4.0%, 16,787G)
getthemoneyz: 100G (2.0%, 825,833G)
Alanaire: 100G (2.0%, 2,570G)

white bets:
RabidHampsters: 2,000G (17.3%, 10,496G)
DeathTaxesAndAnime: 1,889G (16.3%, 3,705G)
reinoe: 1,046G (9.0%, 1,046G)
VolgraTheMoose: 1,001G (8.6%, 9,965G)
nifboy: 1,000G (8.6%, 36,471G)
turbn: 1,000G (8.6%, 10,712G)
roofiepops: 600G (5.2%, 600G)
letdowncity: 500G (4.3%, 18,168G)
Flameatron: 500G (4.3%, 3,352G)
just_here2: 500G (4.3%, 5,890G)
OneHundredFists: 206G (1.8%, 1,554G)
Grandlanzer: 200G (1.7%, 2,617G)
outer_monologue: 200G (1.7%, 1,695G)
Kalabain: 200G (1.7%, 746G)
kliffw: 180G (1.6%, 180G)
Firesheath: 101G (0.9%, 1,742G)
lastly: 100G (0.9%, 14,334G)
Lythe_Caraker: 100G (0.9%, 119,424G)
datadrivenbot: 100G (0.9%, 32,776G)
alecttox: 100G (0.9%, 25,659G)
HASTERIOUS: 58G (0.5%, 1,175G)

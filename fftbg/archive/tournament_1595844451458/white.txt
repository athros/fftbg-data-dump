Player: !White
Team: White Team
Palettes: White/Blue



Unclebearboy
Female
Virgo
74
72
Samurai
Dance
Counter Flood
Defense UP
Lava Walking

Kikuichimoji

Barbuta
Crystal Mail
Battle Boots

Asura, Murasame, Heaven's Cloud, Kikuichimoji, Chirijiraden
Witch Hunt, Wiznaibus, Obsidian Blade



Lastly
Female
Taurus
39
42
Chemist
Black Magic
Counter Magic
Defense UP
Move+1

Blaze Gun

Green Beret
Brigandine
Rubber Shoes

Potion, X-Potion, Echo Grass, Maiden's Kiss, Holy Water, Remedy
Bolt, Bolt 2, Bolt 3, Empower, Death



Dtrain332
Female
Virgo
73
63
Ninja
Black Magic
Absorb Used MP
Maintenance
Teleport

Short Edge
Flail
Green Beret
Black Costume
Dracula Mantle

Bomb, Knife
Fire 4, Bolt, Bolt 4, Ice 2, Empower



Ruthanne
Female
Capricorn
45
41
Summoner
Talk Skill
Auto Potion
Halve MP
Levitate

Poison Rod

Triangle Hat
Light Robe
Small Mantle

Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Cyclops
Threaten, Death Sentence, Refute, Rehabilitate

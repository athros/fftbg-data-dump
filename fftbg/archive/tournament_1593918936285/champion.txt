Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



E Ballard
Male
Libra
70
78
Knight
Yin Yang Magic
Abandon
Short Charge
Levitate

Battle Axe
Genji Shield
Iron Helmet
Gold Armor
N-Kai Armlet

Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Stasis Sword
Blind, Poison, Spell Absorb, Zombie, Paralyze, Petrify, Dark Holy



OneHundredFists
Monster
Aries
66
65
Steel Giant










HyC81
Monster
Cancer
44
50
Ultima Demon










G1nger4le
Male
Libra
41
57
Lancer
Punch Art
Counter
Beastmaster
Move+3

Obelisk
Round Shield
Diamond Helmet
Mythril Armor
N-Kai Armlet

Level Jump8, Vertical Jump8
Spin Fist, Pummel, Wave Fist, Purification, Revive

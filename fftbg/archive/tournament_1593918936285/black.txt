Player: !Black
Team: Black Team
Palettes: Black/Red



Error72
Male
Taurus
51
53
Lancer
Basic Skill
Caution
Doublehand
Levitate

Holy Lance

Iron Helmet
Gold Armor
Genji Gauntlet

Level Jump8, Vertical Jump7
Throw Stone, Heal, Wish



PoroTact
Female
Aries
56
45
Thief
Jump
Distribute
Doublehand
Waterbreathing

Coral Sword

Flash Hat
Mythril Vest
Diamond Armlet

Steal Helmet, Steal Armor, Steal Accessory, Leg Aim
Level Jump8, Vertical Jump4



Just Here2
Female
Capricorn
60
38
Calculator
White Magic
Speed Save
Magic Attack UP
Jump+3

Battle Folio

Leather Hat
White Robe
Salty Rage

CT, Prime Number, 5
Cure, Raise, Raise 2, Protect 2, Esuna, Holy



ApplesauceBoss
Female
Aquarius
49
82
Summoner
Black Magic
Caution
Short Charge
Jump+2

Flame Rod

Flash Hat
Light Robe
Small Mantle

Ifrit, Titan, Carbunkle, Silf
Fire 3, Bolt, Ice 4, Flare

Player: !Green
Team: Green Team
Palettes: Green/White



Vorap
Female
Scorpio
45
42
Oracle
Item
MA Save
Equip Axe
Waterbreathing

Rainbow Staff

Feather Hat
Silk Robe
Defense Armlet

Blind, Poison, Life Drain, Pray Faith, Zombie, Dispel Magic, Petrify
Hi-Potion, X-Potion, Ether, Hi-Ether, Soft, Holy Water



GatsbysGhost
Male
Scorpio
65
43
Oracle
White Magic
Sunken State
Equip Sword
Retreat

Kikuichimoji

Headgear
Black Robe
Spike Shoes

Life Drain, Pray Faith, Foxbird, Confusion Song, Paralyze, Sleep
Cure 2, Cure 3, Cure 4, Raise, Protect, Protect 2, Shell, Esuna



TimeJannies
Male
Gemini
72
50
Thief
Time Magic
Sunken State
Doublehand
Move-HP Up

Orichalcum

Leather Hat
Mystic Vest
Battle Boots

Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Leg Aim
Float, Reflect, Quick, Stabilize Time



Serperemagus
Female
Cancer
57
44
Dancer
Battle Skill
Counter Flood
Equip Gun
Move+1

Papyrus Codex

Golden Hairpin
White Robe
Magic Gauntlet

Witch Hunt, Nameless Dance, Last Dance, Obsidian Blade, Dragon Pit
Magic Break

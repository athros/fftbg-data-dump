Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



EnemyController
Male
Sagittarius
60
61
Calculator
Time Magic
Abandon
Beastmaster
Move+1

Papyrus Codex

Barette
Silk Robe
Spike Shoes

CT, Height, 5, 4, 3
Haste 2, Stop, Immobilize, Reflect, Stabilize Time



Maakur
Monster
Sagittarius
79
63
Malboro










Lowlf
Male
Libra
77
77
Priest
Black Magic
Abandon
Short Charge
Ignore Height

Healing Staff

Headgear
Light Robe
Defense Ring

Cure, Raise, Reraise, Shell, Holy, Magic Barrier
Fire, Bolt 4, Ice 2, Flare



Lemonjohns
Female
Aries
55
66
Monk
Steal
Meatbone Slash
Equip Polearm
Lava Walking

Persia

Twist Headband
Rubber Costume
Feather Boots

Spin Fist, Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive
Steal Accessory, Steal Status, Leg Aim

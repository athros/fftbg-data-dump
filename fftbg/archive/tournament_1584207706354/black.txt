Player: !Black
Team: Black Team
Palettes: Black/Red



Furorcelt
Female
Aries
67
62
Geomancer
Charge
HP Restore
Equip Gun
Jump+1

Fairy Harp
Platinum Shield
Golden Hairpin
Wizard Outfit
Genji Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Charge+2, Charge+4, Charge+5, Charge+10, Charge+20



VulcanPanderp
Female
Aries
54
58
Ninja
Black Magic
Damage Split
Secret Hunt
Lava Walking

Mage Masher
Kunai
Thief Hat
Brigandine
N-Kai Armlet

Shuriken, Bomb, Knife
Fire 2



Toksu
Female
Capricorn
42
64
Lancer
Summon Magic
Earplug
Dual Wield
Move-MP Up

Javelin
Holy Lance
Bronze Helmet
Plate Mail
Magic Gauntlet

Level Jump8, Vertical Jump2
Moogle, Ramuh, Titan, Silf, Cyclops, Zodiac



Lali Lulelo
Female
Taurus
50
38
Mime

Arrow Guard
Dual Wield
Lava Walking



Red Hood
Chain Vest
Elf Mantle

Mimic


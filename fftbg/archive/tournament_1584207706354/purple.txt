Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



BuffaloCrunch
Female
Cancer
77
62
Thief
Talk Skill
Abandon
Monster Talk
Lava Walking

Dagger

Holy Miter
Earth Clothes
Genji Gauntlet

Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Threaten, Death Sentence, Negotiate, Mimic Daravon, Rehabilitate



Psychomidget
Female
Gemini
71
69
Lancer
White Magic
Parry
Long Status
Waterbreathing

Battle Bamboo
Platinum Shield
Leather Helmet
Genji Armor
Germinas Boots

Level Jump5, Vertical Jump2
Cure, Regen, Shell, Shell 2



Rolodex
Male
Taurus
62
45
Time Mage
Jump
Abandon
Long Status
Waterwalking

Gold Staff

Thief Hat
Linen Robe
Defense Armlet

Slow, Stop, Float, Stabilize Time
Level Jump3, Vertical Jump7



Rockmem21
Female
Aquarius
49
74
Squire
White Magic
MA Save
Secret Hunt
Move-MP Up

Nagrarock
Round Shield
Bronze Helmet
Adaman Vest
Magic Gauntlet

Dash, Scream
Cure 2, Cure 3, Raise, Raise 2, Protect, Shell 2, Wall

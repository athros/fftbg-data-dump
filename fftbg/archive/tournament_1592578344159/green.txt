Player: !Green
Team: Green Team
Palettes: Green/White



Lastly
Female
Virgo
69
80
Knight
Basic Skill
Brave Save
Magic Attack UP
Swim

Defender
Ice Shield
Gold Helmet
Mythril Armor
Magic Ring

Head Break, Speed Break, Dark Sword, Night Sword, Surging Sword
Accumulate, Heal, Yell, Cheer Up, Fury



Ruebyy
Male
Taurus
73
68
Knight
White Magic
Counter Flood
Equip Sword
Fly

Murasame
Aegis Shield
Mythril Helmet
Linen Robe
Dracula Mantle

Stasis Sword, Dark Sword
Cure, Cure 4, Raise, Protect, Shell, Wall, Esuna



Nifboy
Male
Virgo
65
43
Summoner
Yin Yang Magic
Counter Flood
Magic Attack UP
Ignore Height

Battle Folio

Triangle Hat
Silk Robe
Defense Armlet

Shiva, Titan, Carbunkle, Odin, Leviathan
Blind, Poison, Doubt Faith, Silence Song, Blind Rage, Dispel Magic, Petrify



AltimaMantoid
Female
Capricorn
46
67
Squire
Draw Out
HP Restore
Short Charge
Move-HP Up

Rune Blade
Bronze Shield
Cachusha
Brigandine
Bracer

Accumulate, Dash, Heal, Tickle, Yell, Wish
Bizen Boat, Murasame, Heaven's Cloud

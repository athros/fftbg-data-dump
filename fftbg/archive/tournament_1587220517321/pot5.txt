Final Bets: blue - 14 bets for 10,828G (36.5%, x1.74); yellow - 25 bets for 18,847G (63.5%, x0.57)

blue bets:
TheChainNerd: 2,000G (18.5%, 16,704G)
Zeroroute: 1,992G (18.4%, 3,704G)
Baron_von_Scrub: 1,111G (10.3%, 26,522G)
Nizaha: 1,016G (9.4%, 2,033G)
edgehead62888: 1,000G (9.2%, 3,874G)
MeleeWizard: 902G (8.3%, 902G)
Bryon_W: 803G (7.4%, 803G)
Aldrammech: 500G (4.6%, 10,431G)
MrFlabyo: 500G (4.6%, 9,363G)
SkylerBunny: 376G (3.5%, 376G)
Fenrislfr: 243G (2.2%, 243G)
itzover9001: 232G (2.1%, 232G)
Carledo: 103G (1.0%, 3,449G)
LordMaxoss: 50G (0.5%, 841G)

yellow bets:
spaceslugg: 2,500G (13.3%, 10,041G)
volgrathemoose: 1,352G (7.2%, 12,325G)
flowinprose: 1,282G (6.8%, 1,282G)
leakimiko: 1,254G (6.7%, 1,254G)
superdevon1: 1,217G (6.5%, 1,217G)
Mtueni: 1,000G (5.3%, 195,809G)
reinoe: 1,000G (5.3%, 13,382G)
ungabunga_bot: 1,000G (5.3%, 170,646G)
BirbBrainsBot: 1,000G (5.3%, 218,548G)
HaateXIII: 1,000G (5.3%, 2,808G)
NovaKnight21: 984G (5.2%, 984G)
JIDkomu: 959G (5.1%, 959G)
fenaen: 782G (4.1%, 782G)
getthemoneyz: 702G (3.7%, 507,672G)
YaBoy125: 461G (2.4%, 461G)
tinyjeans: 415G (2.2%, 415G)
IamInfamous: 400G (2.1%, 906G)
JackOnFire1: 375G (2.0%, 375G)
TheGuesty: 323G (1.7%, 6,323G)
Lanshaft: 312G (1.7%, 529G)
GingerDynomite: 224G (1.2%, 224G)
SwaffleWaffles: 126G (0.7%, 3,163G)
AlenaZarek: 100G (0.5%, 2,373G)
datadrivenbot: 66G (0.4%, 100G)
braumbles: 13G (0.1%, 270G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ZZ Yoshi
Monster
Gemini
43
60
Explosive










PotionDweller
Male
Aquarius
62
58
Ninja
Charge
Caution
Magic Attack UP
Jump+3

Sasuke Knife
Flail
Headgear
Chain Vest
Cursed Ring

Shuriken, Bomb, Wand
Charge+2, Charge+10



Ashintar
Male
Leo
76
47
Squire
Charge
Meatbone Slash
Equip Axe
Teleport

Wizard Staff
Round Shield
Cross Helmet
Mythril Vest
Jade Armlet

Accumulate, Dash, Throw Stone, Heal, Tickle, Yell, Wish, Scream
Charge+2, Charge+4



Firesheath
Male
Libra
65
44
Priest
Item
Critical Quick
Equip Polearm
Move-MP Up

Musk Rod

Leather Hat
Silk Robe
Battle Boots

Cure, Cure 2, Cure 3, Regen, Shell
Hi-Potion, Soft, Holy Water, Remedy

Final Bets: white - 9 bets for 5,574G (79.0%, x0.27); black - 6 bets for 1,482G (21.0%, x3.76)

white bets:
NicoSavoy: 2,000G (35.9%, 80,112G)
Firesheath: 776G (13.9%, 776G)
Laserman1000: 700G (12.6%, 700G)
WireLord: 656G (11.8%, 656G)
bruubarg: 500G (9.0%, 36,736G)
Greggernaut: 368G (6.6%, 368G)
MemoriesofFinal: 264G (4.7%, 264G)
regios91: 212G (3.8%, 3,822G)
BirbBrainsBot: 98G (1.8%, 117,543G)

black bets:
krombobreaker: 500G (33.7%, 5,315G)
Moshyhero: 250G (16.9%, 4,906G)
datadrivenbot: 200G (13.5%, 58,463G)
AllInBot: 180G (12.1%, 180G)
emptygoat: 180G (12.1%, 180G)
getthemoneyz: 172G (11.6%, 1,773,775G)

Final Bets: white - 11 bets for 3,812G (53.5%, x0.87); black - 6 bets for 3,318G (46.5%, x1.15)

white bets:
DeathTaxesAndAnime: 1,052G (27.6%, 1,052G)
BirbBrainsBot: 1,000G (26.2%, 6,658G)
BaronHaynes: 435G (11.4%, 435G)
PantherIscariot: 222G (5.8%, 5,872G)
itsonlyspencer: 200G (5.2%, 1,661G)
datadrivenbot: 200G (5.2%, 57,547G)
getthemoneyz: 196G (5.1%, 1,600,103G)
Zeera98: 157G (4.1%, 157G)
shadricklul: 150G (3.9%, 150G)
srdonko2098: 100G (2.6%, 940G)
CrimsonK97: 100G (2.6%, 141G)

black bets:
AllInBot: 2,231G (67.2%, 2,231G)
CT_5_Holy: 524G (15.8%, 524G)
vorap: 212G (6.4%, 212G)
maddrave09: 128G (3.9%, 175G)
Lydian_C: 123G (3.7%, 13,594G)
MinBetBot: 100G (3.0%, 22,690G)

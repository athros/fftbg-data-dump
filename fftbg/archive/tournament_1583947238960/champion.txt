Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



Shs
Monster
Scorpio
79
42
Blue Dragon










Kenaeisamazing
Male
Libra
58
68
Samurai
Time Magic
MP Restore
Halve MP
Teleport

Partisan

Mythril Helmet
White Robe
Elf Mantle

Asura, Bizen Boat, Murasame, Heaven's Cloud
Haste 2, Slow 2, Float, Reflect, Quick, Stabilize Time



Nomoment
Monster
Aries
76
71
Floating Eye










Reinoe
Female
Scorpio
40
58
Lancer
Elemental
Brave Up
Short Status
Jump+1

Gungnir
Gold Shield
Genji Helmet
White Robe
Small Mantle

Level Jump4, Vertical Jump2
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

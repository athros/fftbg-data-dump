Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jenenebatmann
Male
Cancer
47
68
Priest
Jump
Counter Magic
Secret Hunt
Teleport

Oak Staff

Holy Miter
Black Robe
Leather Mantle

Cure, Cure 3, Raise, Raise 2, Regen, Shell, Wall, Esuna, Holy
Level Jump8, Vertical Jump4



ApplesauceBoss
Female
Scorpio
79
72
Wizard
Punch Art
Damage Split
Magic Attack UP
Fly

Thunder Rod

Headgear
Mystic Vest
Magic Gauntlet

Fire 2, Bolt, Bolt 4, Ice, Ice 2, Ice 3
Spin Fist, Pummel, Wave Fist, Purification, Revive



SQUiDSQUARKLIN
Monster
Aquarius
67
67
Serpentarius










Spartan Paladin
Male
Aries
55
76
Knight
Punch Art
Counter
Equip Sword
Ignore Terrain

Save the Queen
Ice Shield
Bronze Helmet
Crystal Mail
Small Mantle

Armor Break, Weapon Break, Speed Break, Stasis Sword, Justice Sword
Earth Slash, Purification, Revive

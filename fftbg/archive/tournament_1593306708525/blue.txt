Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Prince Rogers Nelson
Monster
Aquarius
70
44
Steel Giant










GrayGhostGaming
Monster
Pisces
51
40
King Behemoth










Lanshaft
Female
Serpentarius
77
52
Geomancer
Yin Yang Magic
Brave Save
Magic Attack UP
Move+1

Platinum Sword
Mythril Shield
Flash Hat
Chameleon Robe
Small Mantle

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Blind, Life Drain, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep, Dark Holy



Twelfthrootoftwo
Female
Libra
73
62
Wizard
White Magic
MP Restore
Short Charge
Waterwalking

Dagger

Black Hood
Chameleon Robe
Rubber Shoes

Fire 3, Bolt 2, Bolt 3, Ice 3, Ice 4, Flare
Cure 2, Cure 4, Raise, Esuna, Holy

Final Bets: purple - 13 bets for 6,260G (59.3%, x0.69); brown - 4 bets for 4,299G (40.7%, x1.46)

purple bets:
Lythe_Caraker: 1,000G (16.0%, 147,853G)
Mesmaster: 1,000G (16.0%, 65,601G)
dantayystv: 1,000G (16.0%, 4,774G)
helpimabug: 895G (14.3%, 1,791G)
Ring_Wyrm: 552G (8.8%, 552G)
Digitalsocrates: 408G (6.5%, 3,571G)
letdowncity: 388G (6.2%, 2,443G)
PrancesWithWolves: 300G (4.8%, 1,139G)
Treafa: 204G (3.3%, 204G)
datadrivenbot: 200G (3.2%, 63,774G)
BirbBrainsBot: 113G (1.8%, 54,832G)
fluffskull: 100G (1.6%, 1,871G)
CT_5_Holy: 100G (1.6%, 1,537G)

brown bets:
evontno: 1,925G (44.8%, 1,925G)
DamnThatShark: 1,000G (23.3%, 3,935G)
Evewho: 748G (17.4%, 748G)
getthemoneyz: 626G (14.6%, 1,263,620G)

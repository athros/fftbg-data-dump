Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Hasterious
Female
Taurus
40
43
Oracle
Dance
Meatbone Slash
Short Charge
Teleport

Whale Whisker

Feather Hat
Adaman Vest
N-Kai Armlet

Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic, Sleep, Petrify
Witch Hunt, Wiznaibus, Nameless Dance



NovaKnight21
Female
Taurus
80
77
Samurai
Black Magic
Earplug
Defense UP
Teleport

Kikuichimoji

Gold Helmet
Mythril Armor
Battle Boots

Koutetsu, Bizen Boat, Heaven's Cloud, Kiyomori
Fire 3, Bolt 2, Bolt 3, Ice 2, Empower, Frog



Lowlf
Male
Capricorn
53
70
Samurai
Sing
Damage Split
Equip Polearm
Swim

Gokuu Rod

Cross Helmet
Linen Cuirass
Power Wrist

Koutetsu, Kikuichimoji
Cheer Song, Last Song



Serperemagus
Female
Pisces
64
74
Monk
Jump
Counter
Secret Hunt
Retreat



Holy Miter
Earth Clothes
Spike Shoes

Pummel, Earth Slash, Chakra, Revive, Seal Evil
Level Jump8, Vertical Jump7

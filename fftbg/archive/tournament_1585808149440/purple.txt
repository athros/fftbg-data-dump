Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ForestMagi
Female
Libra
51
81
Calculator
Yin Yang Magic
Parry
Equip Axe
Waterbreathing

Gold Staff

Holy Miter
White Robe
Diamond Armlet

CT, 5, 3
Spell Absorb, Life Drain, Pray Faith, Zombie, Silence Song, Confusion Song



Lawnboxer
Female
Virgo
62
58
Calculator
Dragon Skill
Sunken State
Long Status
Ignore Height

Bestiary
Aegis Shield
Crystal Helmet
Adaman Vest
Feather Mantle

Blue Magic
Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath



Goust18
Male
Taurus
49
79
Archer
Draw Out
Regenerator
Dual Wield
Jump+1

Long Bow

Headgear
Power Sleeve
Cherche

Charge+3, Charge+4, Charge+10
Asura, Kiyomori, Muramasa



JustSuperish
Male
Sagittarius
59
54
Bard
White Magic
Sunken State
Concentrate
Swim

Ramia Harp

Cachusha
Plate Mail
Reflect Ring

Angel Song
Cure 3, Raise, Regen, Protect, Shell

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ArchKnightX
Female
Libra
61
58
Time Mage
Punch Art
Catch
Concentrate
Teleport

Oak Staff

Holy Miter
Judo Outfit
Dracula Mantle

Haste 2, Quick, Stabilize Time
Spin Fist, Secret Fist, Purification, Revive



StealthModeLocke
Female
Capricorn
52
65
Samurai
Talk Skill
Brave Save
Equip Knife
Jump+3

Hidden Knife

Gold Helmet
Chameleon Robe
Elf Mantle

Asura, Koutetsu, Bizen Boat, Muramasa, Kikuichimoji, Masamune
Solution, Death Sentence, Refute, Rehabilitate



Breakdown777
Monster
Virgo
50
58
Plague










UmaiJam
Male
Libra
46
80
Monk
Item
Parry
Equip Gun
Fly

Bestiary

Red Hood
Black Costume
Angel Ring

Pummel, Secret Fist, Chakra, Revive, Seal Evil
Hi-Potion, Hi-Ether, Maiden's Kiss, Remedy, Phoenix Down

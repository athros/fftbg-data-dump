Player: !Green
Team: Green Team
Palettes: Green/White



Deathmaker06
Male
Cancer
52
69
Mime

PA Save
Martial Arts
Ignore Height



Leather Hat
Chain Vest
Small Mantle

Mimic




Vorap
Male
Serpentarius
62
79
Oracle
Steal
Brave Save
Equip Bow
Teleport

Night Killer

Thief Hat
Leather Outfit
Defense Ring

Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Paralyze, Petrify
Steal Heart, Steal Helmet



Serperemagus
Female
Taurus
55
50
Time Mage
Item
Parry
Attack UP
Move-HP Up

Ivory Rod

Holy Miter
Linen Robe
Feather Mantle

Haste, Slow 2, Demi 2
Hi-Ether, Eye Drop, Soft, Holy Water, Remedy, Phoenix Down



Ferroniss
Female
Libra
69
54
Ninja
Punch Art
Counter
Equip Gun
Fly

Papyrus Codex
Papyrus Codex
Black Hood
Power Sleeve
Dracula Mantle

Knife
Secret Fist, Purification, Chakra, Revive, Seal Evil

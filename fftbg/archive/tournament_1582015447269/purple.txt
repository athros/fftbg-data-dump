Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



HaychDub
Female
Sagittarius
53
72
Squire
Steal
Regenerator
Equip Sword
Move-MP Up

Rune Blade
Genji Shield
Green Beret
Power Sleeve
Elf Mantle

Accumulate, Throw Stone, Heal, Wish, Scream
Steal Heart, Steal Helmet



JanusII
Female
Pisces
65
51
Chemist
Punch Art
Parry
Equip Sword
Lava Walking

Mythril Sword

Flash Hat
Power Sleeve
Angel Ring

Potion, X-Potion, Echo Grass, Phoenix Down
Spin Fist, Pummel, Purification, Chakra, Revive



Xihollywoodix
Male
Libra
57
51
Knight
Jump
Counter Tackle
Short Charge
Move+3

Save the Queen

Platinum Helmet
Silk Robe
Spike Shoes

Head Break, Power Break, Mind Break, Stasis Sword, Dark Sword
Level Jump2, Vertical Jump3



WireLord
Male
Aries
51
69
Monk
Item
MA Save
Equip Polearm
Jump+2

Cypress Rod

Holy Miter
Brigandine
Bracer

Spin Fist, Pummel, Wave Fist, Earth Slash, Revive
Ether, Antidote, Eye Drop, Holy Water, Remedy

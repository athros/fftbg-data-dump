Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



ApplesauceBoss
Male
Gemini
81
44
Priest
Throw
Distribute
Short Charge
Move+3

Wizard Staff

Feather Hat
Earth Clothes
Wizard Mantle

Cure, Raise, Regen, Protect 2, Shell, Wall, Esuna
Bomb, Stick, Wand



HaychDub
Male
Gemini
45
46
Thief
Black Magic
Counter
Equip Armor
Move+2

Hidden Knife

Red Hood
Wizard Robe
Feather Mantle

Steal Helmet, Arm Aim, Leg Aim
Bolt, Bolt 2, Bolt 3, Ice 3, Flare



JLinkletter
Female
Taurus
57
41
Lancer
Punch Art
Parry
Secret Hunt
Jump+3

Gokuu Rod
Platinum Shield
Diamond Helmet
Gold Armor
Angel Ring

Level Jump5, Vertical Jump8
Pummel, Earth Slash, Secret Fist, Purification



0v3rr8d
Male
Virgo
46
54
Lancer
Battle Skill
Brave Save
Attack UP
Move+1

Mythril Spear
Mythril Shield
Diamond Helmet
Linen Robe
108 Gems

Level Jump2, Vertical Jump2
Head Break, Shield Break, Weapon Break, Dark Sword, Night Sword

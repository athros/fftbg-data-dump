Final Bets: black - 16 bets for 4,012G (13.4%, x6.47); champion - 33 bets for 25,977G (86.6%, x0.15)

black bets:
BirbBrainsBot: 642G (16.0%, 139,958G)
KasugaiRoastedPeas: 500G (12.5%, 3,567G)
skillomono: 500G (12.5%, 3,688G)
FriendSquirrel: 401G (10.0%, 401G)
CorpusCav: 396G (9.9%, 396G)
getthemoneyz: 310G (7.7%, 677,887G)
HaateXIII: 250G (6.2%, 5,293G)
Lordminsc: 200G (5.0%, 4,374G)
Lolprinze: 150G (3.7%, 2,891G)
Evewho: 100G (2.5%, 2,585G)
CosmicTactician: 100G (2.5%, 6,984G)
moocaotao: 100G (2.5%, 2,063G)
FoeSquirrel: 100G (2.5%, 1,307G)
Mtueni: 100G (2.5%, 8,635G)
tronfonne: 88G (2.2%, 1,088G)
a_mo22: 75G (1.9%, 1,173G)

champion bets:
Mesmaster: 5,000G (19.2%, 55,082G)
EnemyController: 4,000G (15.4%, 244,942G)
volgrathemoose: 2,442G (9.4%, 4,884G)
joewcarson: 2,000G (7.7%, 7,329G)
Maldoree: 1,115G (4.3%, 1,115G)
mpghappiness: 1,001G (3.9%, 18,169G)
red__lancer: 1,000G (3.8%, 54,360G)
sinnyil2: 788G (3.0%, 788G)
itsZaes: 623G (2.4%, 623G)
Laserman1000: 600G (2.3%, 12,400G)
AmaninAmide: 600G (2.3%, 3,156G)
killth3kid: 600G (2.3%, 4,315G)
Estan_AD: 576G (2.2%, 1,152G)
Miyokari: 550G (2.1%, 17,950G)
Dellusionsx: 500G (1.9%, 4,680G)
luminarii: 500G (1.9%, 14,309G)
sSwing: 469G (1.8%, 4,469G)
DustBirdEX: 456G (1.8%, 14,925G)
fire_________________: 398G (1.5%, 398G)
Lanshaft: 384G (1.5%, 384G)
Rislyeu: 360G (1.4%, 360G)
twelfthrootoftwo: 300G (1.2%, 15,090G)
neocarbuncle: 295G (1.1%, 295G)
Strifu: 244G (0.9%, 244G)
alecttox: 244G (0.9%, 244G)
LAGBOT30000: 235G (0.9%, 23,503G)
ungabunga_bot: 113G (0.4%, 530,604G)
nekojin: 104G (0.4%, 104G)
ANFz: 100G (0.4%, 1,633G)
Tougou: 100G (0.4%, 4,516G)
datadrivenbot: 100G (0.4%, 18,991G)
mannequ1n: 100G (0.4%, 3,831G)
DoublePrizes: 80G (0.3%, 684G)

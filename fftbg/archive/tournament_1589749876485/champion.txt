Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Rislyeu
Female
Capricorn
64
55
Knight
Basic Skill
Critical Quick
Defense UP
Move+1

Excalibur
Flame Shield
Barbuta
Bronze Armor
Bracer

Head Break, Magic Break, Power Break, Stasis Sword, Dark Sword, Night Sword, Surging Sword
Dash, Heal, Tickle, Cheer Up, Wish



Lanshaft
Female
Aries
82
55
Oracle
Jump
Counter
Attack UP
Jump+1

Gokuu Rod

Thief Hat
Judo Outfit
Reflect Ring

Life Drain, Pray Faith, Doubt Faith, Zombie, Foxbird, Confusion Song
Level Jump5, Vertical Jump2



Zeroroute
Male
Aries
71
67
Knight
Summon Magic
MP Restore
Dual Wield
Waterbreathing

Excalibur
Slasher
Barbuta
Maximillian
Genji Gauntlet

Head Break, Shield Break, Power Break, Mind Break, Justice Sword, Surging Sword
Leviathan, Salamander, Silf, Fairy



Joewcarson
Male
Cancer
44
73
Knight
Yin Yang Magic
Auto Potion
Equip Sword
Ignore Terrain

Kikuichimoji
Gold Shield
Diamond Helmet
Leather Armor
Germinas Boots

Head Break, Shield Break, Weapon Break, Magic Break, Justice Sword, Night Sword, Surging Sword
Blind, Poison, Spell Absorb, Confusion Song, Dispel Magic, Sleep, Dark Holy

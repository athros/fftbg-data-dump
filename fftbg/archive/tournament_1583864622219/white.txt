Player: !White
Team: White Team
Palettes: White/Blue



Nizaha
Male
Capricorn
75
50
Geomancer
Draw Out
HP Restore
Long Status
Ignore Height

Long Sword
Genji Shield
Green Beret
Mythril Vest
Angel Ring

Pitfall, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind
Bizen Boat, Murasame, Heaven's Cloud, Kikuichimoji



Lunaticruin
Male
Gemini
56
63
Summoner
Time Magic
Regenerator
Magic Defense UP
Fly

Rod

Black Hood
Black Robe
Angel Ring

Moogle, Titan, Golem, Carbunkle, Leviathan, Silf
Haste, Haste 2, Slow 2, Float, Stabilize Time



LazialeChicago
Female
Virgo
53
74
Mime

Earplug
Magic Attack UP
Move+1



Golden Hairpin
Clothes
Cursed Ring

Mimic




Big Bola
Male
Leo
60
45
Calculator
White Magic
Earplug
Long Status
Ignore Terrain

Dragon Rod

Black Hood
White Robe
108 Gems

CT, Height, Prime Number, 5, 3
Cure 2, Cure 4, Raise, Raise 2, Reraise, Esuna, Holy

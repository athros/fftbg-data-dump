Final Bets: red - 13 bets for 29,415G (60.4%, x0.66); blue - 9 bets for 19,311G (39.6%, x1.52)

red bets:
mirapoix: 24,415G (83.0%, 24,415G)
amiture: 1,000G (3.4%, 6,702G)
prince_rogers_nelson_: 900G (3.1%, 900G)
Evewho: 572G (1.9%, 572G)
CT_5_Holy: 477G (1.6%, 477G)
AllInBot: 443G (1.5%, 443G)
Lemonjohns: 420G (1.4%, 4,418G)
dem0nj0ns: 300G (1.0%, 1,642G)
Mudrockk: 272G (0.9%, 272G)
resjudicata3: 216G (0.7%, 216G)
datadrivenbot: 200G (0.7%, 40,857G)
Vultuous: 100G (0.3%, 1,068G)
dtrain332: 100G (0.3%, 1,702G)

blue bets:
Vaxaldan: 14,487G (75.0%, 14,487G)
UmaiJam: 1,600G (8.3%, 67,706G)
getthemoneyz: 1,000G (5.2%, 1,386,981G)
BirbBrainsBot: 1,000G (5.2%, 64,605G)
iBardic: 500G (2.6%, 19,520G)
ruleof5: 300G (1.6%, 15,787G)
douchetron: 296G (1.5%, 296G)
Carledo: 103G (0.5%, 89,561G)
renkei_fukai: 25G (0.1%, 101G)

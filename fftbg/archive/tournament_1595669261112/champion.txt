Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



UmaiJam
Female
Aries
54
57
Wizard
Draw Out
Counter Magic
Defend
Swim

Wizard Rod

Headgear
Leather Outfit
Chantage

Fire 2, Bolt 2, Bolt 4, Ice, Ice 3
Murasame



Laserman1000
Female
Gemini
67
41
Thief
Item
Speed Save
Equip Gun
Ignore Terrain

Stone Gun

Feather Hat
Brigandine
Spike Shoes

Steal Armor, Steal Shield, Leg Aim
Potion, Hi-Potion



Omegasuspekt
Male
Serpentarius
61
55
Mime

Auto Potion
Magic Defense UP
Move+1



Triangle Hat
Judo Outfit
Red Shoes

Mimic




ALY327
Female
Cancer
81
69
Monk
Draw Out
Speed Save
Equip Axe
Jump+1

Healing Staff

Ribbon
Leather Outfit
Genji Gauntlet

Earth Slash, Purification
Asura, Murasame, Muramasa

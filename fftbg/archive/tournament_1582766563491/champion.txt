Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Kalthonia
Female
Pisces
64
65
Mediator
Punch Art
Absorb Used MP
Attack UP
Jump+1

Blind Knife

Triangle Hat
Mythril Vest
Cursed Ring

Invitation, Praise, Preach, Negotiate, Mimic Daravon, Refute
Purification, Revive, Seal Evil



JBradMaximus
Male
Virgo
73
58
Monk
Jump
Absorb Used MP
Long Status
Move+1



Leather Hat
Mystic Vest
Spike Shoes

Secret Fist, Purification, Revive, Seal Evil
Level Jump2, Vertical Jump7



DaurosTD
Male
Aries
71
59
Time Mage
Draw Out
Mana Shield
Equip Bow
Move+2

Mythril Bow

Cachusha
Earth Clothes
N-Kai Armlet

Haste, Haste 2, Slow, Stop, Float, Reflect, Quick, Demi, Demi 2, Stabilize Time
Asura, Bizen Boat, Kiyomori, Kikuichimoji



IcePattern
Female
Taurus
51
69
Priest
Throw
Dragon Spirit
Equip Bow
Move-HP Up

Poison Bow

Triangle Hat
Wizard Outfit
Feather Mantle

Cure 2, Cure 4, Raise 2, Protect, Shell 2, Wall, Esuna
Sword

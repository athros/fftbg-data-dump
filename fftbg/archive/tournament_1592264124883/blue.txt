Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Reinoe
Female
Sagittarius
64
66
Samurai
Steal
Earplug
Magic Attack UP
Jump+1

Kikuichimoji

Genji Helmet
Leather Armor
Defense Ring

Asura, Bizen Boat, Heaven's Cloud, Kiyomori, Kikuichimoji
Steal Shield, Steal Weapon, Steal Status, Leg Aim



TheMM42
Male
Scorpio
46
79
Summoner
Basic Skill
Earplug
Magic Attack UP
Fly

Poison Rod

Feather Hat
Adaman Vest
Spike Shoes

Moogle, Odin
Dash, Heal, Cheer Up, Wish



Fluffskull
Female
Scorpio
56
56
Time Mage
Jump
Auto Potion
Short Status
Move+3

Battle Bamboo

Green Beret
Linen Robe
Chantage

Haste, Haste 2, Immobilize, Stabilize Time
Level Jump8, Vertical Jump8



RaIshtar
Male
Leo
69
65
Squire
Talk Skill
Brave Save
Monster Talk
Fly

Slasher
Buckler
Red Hood
Leather Outfit
Diamond Armlet

Accumulate, Heal, Fury, Wish
Invitation, Praise, Insult, Negotiate, Mimic Daravon, Refute

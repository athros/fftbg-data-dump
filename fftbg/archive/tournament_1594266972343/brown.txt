Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Hasterious
Monster
Libra
73
78
Plague










Slickdang
Monster
Virgo
56
78
Archaic Demon










Skillomono
Male
Virgo
63
63
Geomancer
Item
Abandon
Throw Item
Jump+1

Long Sword
Bronze Shield
Twist Headband
Robe of Lords
Diamond Armlet

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Echo Grass, Maiden's Kiss, Remedy, Phoenix Down



Fluffskull
Male
Cancer
78
52
Samurai
Item
Parry
Equip Polearm
Jump+3

Partisan

Mythril Helmet
Leather Armor
Germinas Boots

Asura, Murasame, Heaven's Cloud, Muramasa, Kikuichimoji
Potion, Hi-Potion, Echo Grass, Phoenix Down

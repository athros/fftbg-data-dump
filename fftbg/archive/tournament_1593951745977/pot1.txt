Final Bets: red - 8 bets for 4,380G (33.6%, x1.98); blue - 10 bets for 8,652G (66.4%, x0.51)

red bets:
roqqqpsi: 1,230G (28.1%, 123,031G)
getthemoneyz: 1,000G (22.8%, 1,187,755G)
SephDarkheart: 530G (12.1%, 530G)
NIghtdew14: 500G (11.4%, 4,537G)
AO110893: 500G (11.4%, 6,377G)
prince_rogers_nelson_: 320G (7.3%, 320G)
twelfthrootoftwo: 200G (4.6%, 6,644G)
enkikavlar: 100G (2.3%, 1,983G)

blue bets:
BoneMiser: 2,458G (28.4%, 2,458G)
VolgraTheMoose: 1,613G (18.6%, 1,613G)
DuneMeta: 1,403G (16.2%, 2,807G)
Draconis345: 1,000G (11.6%, 70,797G)
BirbBrainsBot: 634G (7.3%, 49,384G)
serperemagus: 500G (5.8%, 14,740G)
dogsandcatsand: 448G (5.2%, 448G)
Evewho: 200G (2.3%, 7,586G)
just_here2: 200G (2.3%, 22,936G)
josephiroth_143: 196G (2.3%, 196G)

Final Bets: white - 6 bets for 5,726G (59.6%, x0.68); black - 12 bets for 3,887G (40.4%, x1.47)

white bets:
Breakdown777: 1,918G (33.5%, 3,837G)
richardserious: 1,000G (17.5%, 10,266G)
Draconis345: 1,000G (17.5%, 71,504G)
BirbBrainsBot: 1,000G (17.5%, 50,906G)
josephiroth_143: 658G (11.5%, 658G)
GRRR12: 150G (2.6%, 690G)

black bets:
roqqqpsi: 1,084G (27.9%, 108,403G)
prince_rogers_nelson_: 704G (18.1%, 704G)
serperemagus: 500G (12.9%, 14,993G)
ChaosPaladin: 400G (10.3%, 589G)
YaBoy125: 367G (9.4%, 12,257G)
Mysteriousdewd: 212G (5.5%, 212G)
BoneMiser: 200G (5.1%, 3,942G)
AO110893: 100G (2.6%, 5,777G)
ACSpree: 100G (2.6%, 4,456G)
enkikavlar: 100G (2.6%, 2,003G)
DuneMeta: 100G (2.6%, 3,757G)
getthemoneyz: 20G (0.5%, 1,187,010G)

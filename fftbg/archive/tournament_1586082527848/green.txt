Player: !Green
Team: Green Team
Palettes: Green/White



Leakimiko
Female
Capricorn
59
42
Ninja
Battle Skill
Counter Magic
Equip Bow
Move+3

Silver Bow

Red Hood
Judo Outfit
Spike Shoes

Bomb, Staff, Ninja Sword
Magic Break, Speed Break, Power Break, Justice Sword, Explosion Sword



Herrshaun
Male
Virgo
77
74
Mediator
Yin Yang Magic
Counter
Short Status
Retreat

Blast Gun

Black Hood
Light Robe
Bracer

Invitation, Persuade, Preach, Negotiate
Blind, Poison, Pray Faith, Blind Rage, Confusion Song, Dispel Magic, Sleep



Bpc2163
Male
Cancer
49
62
Time Mage
Draw Out
Damage Split
Short Charge
Move+3

Rainbow Staff

Black Hood
Silk Robe
Dracula Mantle

Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time, Meteor
Asura, Koutetsu



LOKITHUS
Monster
Sagittarius
43
76
Archaic Demon








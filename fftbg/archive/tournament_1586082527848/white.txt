Player: !White
Team: White Team
Palettes: White/Blue



Skarthe
Female
Aquarius
64
53
Archer
Punch Art
Catch
Equip Axe
Move+3

Oak Staff
Diamond Shield
Green Beret
Black Costume
Angel Ring

Charge+1, Charge+4
Spin Fist, Earth Slash, Purification, Chakra, Revive



Chompie
Female
Gemini
60
65
Summoner
Yin Yang Magic
Critical Quick
Equip Polearm
Retreat

Iron Fan

Triangle Hat
Robe of Lords
Genji Gauntlet

Titan, Carbunkle, Bahamut, Leviathan, Salamander, Silf, Cyclops
Life Drain, Doubt Faith, Foxbird, Paralyze



DustBirdEX
Male
Virgo
80
59
Priest
Jump
Sunken State
Equip Knife
Levitate

Poison Rod

Barette
Secret Clothes
Bracer

Cure 2, Cure 4, Raise, Reraise, Regen, Protect 2, Shell, Shell 2, Wall, Esuna, Holy
Level Jump5, Vertical Jump7



Mayormcfunbags
Male
Virgo
71
72
Calculator
Dragon Skill
Abandon
Magic Defense UP
Ignore Height

Poison Rod
Platinum Shield
Gold Helmet
White Robe
Bracer

Blue Magic
Triple Attack, Triple Breath, Noxious Gas, Triple Flame, Triple Thunder, Dark Whisper, Dash, Tail Swing, Fire Breath, Ice Breath, Thunder Breath, Holy Breath

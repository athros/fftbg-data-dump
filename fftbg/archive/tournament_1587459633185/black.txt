Player: !Black
Team: Black Team
Palettes: Black/Red



Lydian C
Female
Scorpio
41
62
Calculator
Beast Skill
Regenerator
Equip Gun
Jump+1

Glacier Gun

Leather Helmet
Gold Armor
Defense Ring

Blue Magic
Shake Off, Wave Around, Blow Fire, Mimic Titan, Gather Power, Stab Up, Sudden Cry, Giga Flare, Hurricane, Ulmaguest



Comradecry
Monster
Sagittarius
63
71
Red Panther










Reverie3
Male
Virgo
61
45
Oracle
Punch Art
Arrow Guard
Halve MP
Move-HP Up

Battle Folio

Golden Hairpin
Brigandine
N-Kai Armlet

Blind, Poison, Dispel Magic, Sleep
Wave Fist, Purification, Chakra, Revive



Evewho
Female
Capricorn
43
55
Knight
Item
Brave Up
Secret Hunt
Lava Walking

Diamond Sword
Buckler
Iron Helmet
Gold Armor
Feather Mantle

Shield Break, Magic Break, Power Break, Mind Break, Justice Sword
Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Remedy, Phoenix Down

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Metagameface
Male
Virgo
78
52
Calculator
Time Magic
Mana Shield
Equip Polearm
Levitate

Musk Rod

Feather Hat
Robe of Lords
Germinas Boots

CT, Height, Prime Number
Slow, Immobilize, Quick, Stabilize Time



Coralreeferz
Male
Gemini
71
55
Geomancer
Throw
Mana Shield
Defense UP
Swim

Battle Axe
Genji Shield
Headgear
Silk Robe
Genji Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shuriken, Ninja Sword



Saldarin
Monster
Scorpio
45
76
Gobbledeguck










OneHundredFists
Male
Scorpio
60
45
Samurai
Item
Catch
Throw Item
Lava Walking

Kikuichimoji

Iron Helmet
Bronze Armor
Cursed Ring

Koutetsu, Murasame, Heaven's Cloud, Muramasa
X-Potion, Remedy, Phoenix Down

Player: !Red
Team: Red Team
Palettes: Red/Brown



Omegasuspekt
Female
Leo
54
69
Mediator
Charge
Earplug
Equip Knife
Levitate

Orichalcum

Cachusha
Judo Outfit
Spike Shoes

Praise, Threaten, Preach, Death Sentence, Refute
Charge+1, Charge+2, Charge+4, Charge+7



TrueRhyme
Female
Pisces
72
74
Summoner
Jump
Critical Quick
Short Charge
Waterwalking

Rod

Twist Headband
Black Robe
N-Kai Armlet

Ramuh, Titan, Carbunkle, Fairy, Cyclops
Level Jump8, Vertical Jump6



Cryptopsy70
Female
Leo
55
52
Wizard
Draw Out
MP Restore
Doublehand
Ignore Terrain

Dragon Rod

Leather Hat
Mythril Vest
Power Wrist

Fire, Fire 4, Bolt, Bolt 3, Ice 2, Ice 3
Koutetsu, Murasame, Kiyomori, Muramasa, Kikuichimoji



Mrfripps
Female
Scorpio
69
60
Geomancer
Black Magic
Critical Quick
Short Charge
Waterbreathing

Slasher
Platinum Shield
Triangle Hat
Wizard Outfit
Elf Mantle

Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Fire 3, Bolt, Ice 4, Death

Final Bets: red - 27 bets for 12,952G (58.2%, x0.72); brown - 15 bets for 9,311G (41.8%, x1.39)

red bets:
leakimiko: 1,185G (9.1%, 14,814G)
fenaen: 1,001G (7.7%, 36,886G)
Pie108: 1,000G (7.7%, 58,112G)
ungabunga_bot: 1,000G (7.7%, 206,984G)
BirbBrainsBot: 1,000G (7.7%, 90,835G)
lifeguard_dan: 780G (6.0%, 780G)
Jeeboheebo: 689G (5.3%, 689G)
SpookyCookieMonster: 584G (4.5%, 584G)
ColetteMSLP: 500G (3.9%, 28,872G)
MrPhuui: 500G (3.9%, 10,301G)
JumbocactuarX27: 500G (3.9%, 10,435G)
EnemyController: 500G (3.9%, 62,610G)
CapnChaos12: 500G (3.9%, 50,154G)
catfashions: 400G (3.1%, 2,224G)
Justice0320: 356G (2.7%, 3,564G)
goro_0924: 352G (2.7%, 352G)
roqqqpsi: 312G (2.4%, 1,422G)
rico_flex: 300G (2.3%, 15,055G)
toka222: 300G (2.3%, 300G)
killth3kid: 288G (2.2%, 3,692G)
Nizaha: 201G (1.6%, 5,601G)
Lavatis: 200G (1.5%, 4,384G)
itsdigitalbro: 103G (0.8%, 103G)
ZephyrTempest: 101G (0.8%, 166,108G)
AllInBot: 100G (0.8%, 100G)
lastly: 100G (0.8%, 2,453G)
datadrivenbot: 100G (0.8%, 1,432G)

brown bets:
sinnyil2: 2,500G (26.8%, 109,673G)
upvla: 1,234G (13.3%, 24,643G)
KasugaiRoastedPeas: 1,200G (12.9%, 10,694G)
SomeCallMeGON: 1,000G (10.7%, 12,162G)
braumbles: 999G (10.7%, 49,989G)
Laserman1000: 619G (6.6%, 24,419G)
Tekenu: 500G (5.4%, 1,066G)
nedryerson01: 400G (4.3%, 3,356G)
nifboy: 305G (3.3%, 7,627G)
thaetreis: 176G (1.9%, 176G)
ko2q: 100G (1.1%, 1,271G)
Tithonus: 100G (1.1%, 3,232G)
Kellios11: 100G (1.1%, 14,090G)
getthemoneyz: 56G (0.6%, 543,506G)
daveb_: 22G (0.2%, 1,825G)

Player: !Green
Team: Green Team
Palettes: Green/White



Coralreeferz
Male
Taurus
53
79
Ninja
Summon Magic
Damage Split
Attack UP
Lava Walking

Iga Knife
Kunai
Red Hood
Adaman Vest
Wizard Mantle

Shuriken, Knife
Moogle, Shiva, Carbunkle, Leviathan, Fairy



Lionhermit
Male
Pisces
80
73
Bard
Punch Art
Sunken State
Attack UP
Fly

Bloody Strings

Headgear
Mythril Armor
Jade Armlet

Angel Song, Battle Song, Last Song
Spin Fist, Pummel, Revive, Seal Evil



AniZero
Male
Gemini
42
69
Lancer
Throw
Critical Quick
Equip Knife
Jump+2

Dagger
Kaiser Plate
Circlet
Gold Armor
Feather Mantle

Level Jump8, Vertical Jump2
Bomb



CoderCalamity
Female
Aries
74
39
Geomancer
White Magic
Arrow Guard
Short Charge
Waterwalking

Mythril Sword
Flame Shield
Golden Hairpin
Earth Clothes
Wizard Mantle

Pitfall, Hell Ivy, Hallowed Ground, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Cure 4, Raise, Reraise, Shell, Wall, Esuna

Player: !Black
Team: Black Team
Palettes: Black/Red



Kidikorus
Male
Serpentarius
53
63
Knight
Sing
Regenerator
Equip Axe
Fly

Wizard Staff
Round Shield
Platinum Helmet
Bronze Armor
Cursed Ring

Shield Break, Dark Sword
Angel Song, Magic Song



Evewho
Female
Aquarius
74
42
Geomancer
Draw Out
Sunken State
Dual Wield
Move+2

Slasher
Koutetsu Knife
Flash Hat
Earth Clothes
Spike Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Heaven's Cloud, Kiyomori, Muramasa, Kikuichimoji



Potgodtopdog
Male
Gemini
72
71
Chemist
Basic Skill
MP Restore
Magic Attack UP
Move-HP Up

Hydra Bag

Headgear
Wizard Outfit
Jade Armlet

Potion, Antidote, Echo Grass, Soft, Phoenix Down
Accumulate, Dash, Throw Stone, Heal, Fury



Volgrathemoose
Monster
Sagittarius
50
71
Mindflayer








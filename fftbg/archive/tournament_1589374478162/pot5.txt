Final Bets: blue - 14 bets for 4,751G (36.9%, x1.71); yellow - 14 bets for 8,129G (63.1%, x0.58)

blue bets:
HaplessOne: 1,111G (23.4%, 18,454G)
toka222: 650G (13.7%, 28,208G)
LeoNightFury: 500G (10.5%, 5,120G)
megaman2202: 500G (10.5%, 6,810G)
coralreeferz: 450G (9.5%, 14,448G)
MirtaiAtana: 437G (9.2%, 437G)
CosmicTactician: 200G (4.2%, 9,189G)
powergems: 200G (4.2%, 5,496G)
nok____: 162G (3.4%, 10,644G)
reshiramdude16: 137G (2.9%, 137G)
getthemoneyz: 104G (2.2%, 673,194G)
ko2q: 100G (2.1%, 2,724G)
AUrato: 100G (2.1%, 826G)
datadrivenbot: 100G (2.1%, 15,283G)

yellow bets:
DavenIII: 1,200G (14.8%, 12,239G)
JumbocactuarX27: 1,000G (12.3%, 29,497G)
LDSkinny: 1,000G (12.3%, 51,645G)
BirbBrainsBot: 1,000G (12.3%, 26,858G)
Lionhermit: 1,000G (12.3%, 53,860G)
Baron_von_Scrub: 628G (7.7%, 25,488G)
catfashions: 572G (7.0%, 572G)
fenaen: 533G (6.6%, 533G)
roqqqpsi: 370G (4.6%, 674G)
PetitFoulard: 314G (3.9%, 314G)
ungabunga_bot: 286G (3.5%, 483,418G)
TheManInPlaid: 125G (1.5%, 625G)
lifeguard_dan: 100G (1.2%, 1,516G)
FaytKion: 1G (0.0%, 5,917G)

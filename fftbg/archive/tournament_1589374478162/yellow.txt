Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Arch8000
Male
Libra
41
69
Squire
Steal
Catch
Halve MP
Jump+3

Orichalcum
Bronze Shield
Feather Hat
Power Sleeve
Genji Gauntlet

Throw Stone, Yell, Cheer Up, Scream
Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory



UmbraKnights
Male
Cancer
77
75
Ninja
Steal
MA Save
Equip Armor
Swim

Flail
Koga Knife
Gold Helmet
Chameleon Robe
Spike Shoes

Shuriken
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Leg Aim



LDSkinny
Female
Aries
40
78
Oracle
Black Magic
Hamedo
Equip Armor
Teleport

Musk Rod

Crystal Helmet
Earth Clothes
Feather Boots

Poison, Spell Absorb, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic, Paralyze, Dark Holy
Fire 2, Fire 4, Ice 2



JumbocactuarX27
Female
Sagittarius
52
55
Monk
Throw
Earplug
Attack UP
Move+1



Twist Headband
Clothes
Dracula Mantle

Wave Fist, Earth Slash, Revive
Shuriken, Bomb, Axe, Spear, Stick, Wand

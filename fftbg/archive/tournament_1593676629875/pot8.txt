Final Bets: purple - 6 bets for 3,297G (21.5%, x3.65); champion - 10 bets for 12,038G (78.5%, x0.27)

purple bets:
Evewho: 1,000G (30.3%, 21,046G)
Dragon_RW: 665G (20.2%, 665G)
KyleWonToLiveForever: 654G (19.8%, 654G)
alekzanndrr: 500G (15.2%, 1,329G)
phoenix6107: 306G (9.3%, 306G)
getthemoneyz: 172G (5.2%, 1,157,188G)

champion bets:
Arcblazer23: 3,020G (25.1%, 3,020G)
Zachara: 2,004G (16.6%, 140,004G)
Mesmaster: 2,000G (16.6%, 8,672G)
UmaiJam: 1,800G (15.0%, 82,762G)
SaltiestMage: 1,000G (8.3%, 5,718G)
BirbBrainsBot: 898G (7.5%, 128,621G)
Digitalsocrates: 500G (4.2%, 2,337G)
moonliquor: 416G (3.5%, 416G)
twelfthrootoftwo: 300G (2.5%, 12,317G)
datadrivenbot: 100G (0.8%, 51,954G)

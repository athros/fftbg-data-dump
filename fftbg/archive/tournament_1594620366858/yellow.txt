Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



SSwing
Male
Aries
45
76
Samurai
Charge
Parry
Defense UP
Move+1

Partisan

Genji Helmet
Chameleon Robe
Genji Gauntlet

Koutetsu, Bizen Boat, Muramasa
Charge+2, Charge+7, Charge+20



Gorgewall
Male
Capricorn
78
64
Priest
Yin Yang Magic
MA Save
Equip Armor
Ignore Height

Mace of Zeus

Mythril Helmet
Power Sleeve
Genji Gauntlet

Cure, Cure 2, Cure 3, Raise, Protect, Shell, Shell 2, Esuna, Holy
Poison, Life Drain, Zombie, Dispel Magic, Paralyze



KasugaiRoastedPeas
Female
Scorpio
68
73
Dancer
Draw Out
Absorb Used MP
Secret Hunt
Move+2

Ryozan Silk

Holy Miter
Wizard Robe
Angel Ring

Wiznaibus, Slow Dance, Polka Polka
Koutetsu, Kiyomori, Kikuichimoji



ThreeMileIsland
Female
Scorpio
49
68
Wizard
Item
Arrow Guard
Throw Item
Ignore Terrain

Wizard Rod

Green Beret
Linen Robe
Angel Ring

Fire 2, Fire 4, Bolt 4, Ice, Ice 2, Ice 4, Empower, Death, Flare
Potion, X-Potion, Hi-Ether, Maiden's Kiss, Phoenix Down

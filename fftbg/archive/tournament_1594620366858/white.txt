Player: !White
Team: White Team
Palettes: White/Blue



Twelfthrootoftwo
Male
Pisces
74
59
Mime

HP Restore
Equip Shield
Levitate


Bronze Shield
Flash Hat
Power Sleeve
Cursed Ring

Mimic




Kronikle
Female
Capricorn
63
52
Wizard
Dance
Counter
Short Charge
Move+1

Mythril Knife

Thief Hat
Wizard Robe
Cursed Ring

Bolt, Bolt 3, Bolt 4, Ice, Ice 3, Frog
Witch Hunt, Wiznaibus, Obsidian Blade, Nether Demon



CT 5 Holy
Male
Serpentarius
70
69
Mediator
Time Magic
Auto Potion
Sicken
Waterbreathing

Romanda Gun

Red Hood
Linen Robe
Genji Gauntlet

Persuade, Praise, Preach, Solution, Insult, Mimic Daravon, Refute
Haste, Slow, Slow 2, Immobilize, Reflect, Demi, Stabilize Time



Lydian C
Female
Aquarius
72
71
Geomancer
Throw
Mana Shield
Equip Armor
Retreat

Bizen Boat
Aegis Shield
Mythril Helmet
Chain Vest
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Shuriken, Knife, Hammer

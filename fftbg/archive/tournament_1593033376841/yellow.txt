Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Actual JP
Female
Scorpio
56
75
Calculator
Black Magic
Counter Tackle
Short Charge
Lava Walking

Papyrus Codex

Red Hood
Black Robe
Germinas Boots

CT, Height, Prime Number, 5, 3
Fire, Fire 2, Bolt 2, Ice 3



Grandlanzer
Male
Scorpio
53
69
Ninja
White Magic
Dragon Spirit
Doublehand
Ignore Terrain

Flame Whip

Red Hood
Chain Vest
Red Shoes

Shuriken, Bomb, Knife
Raise, Raise 2, Reraise, Protect, Shell, Shell 2, Wall, Esuna, Holy



Ericzubat
Female
Pisces
65
44
Lancer
Talk Skill
Absorb Used MP
Dual Wield
Jump+1

Javelin
Javelin
Gold Helmet
Silk Robe
Jade Armlet

Level Jump4, Vertical Jump4
Preach, Insult, Mimic Daravon, Refute, Rehabilitate



Galkife
Female
Libra
56
66
Time Mage
Steal
Meatbone Slash
Halve MP
Lava Walking

Musk Rod

Golden Hairpin
Wizard Robe
Bracer

Haste, Haste 2, Slow, Float, Reflect, Quick, Demi 2, Stabilize Time
Gil Taking, Steal Heart, Steal Weapon, Steal Accessory, Steal Status, Arm Aim

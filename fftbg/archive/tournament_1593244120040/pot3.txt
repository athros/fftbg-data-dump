Final Bets: white - 5 bets for 37,061G (77.7%, x0.29); black - 14 bets for 10,644G (22.3%, x3.48)

white bets:
reinoe: 35,300G (95.2%, 35,300G)
Digitalsocrates: 800G (2.2%, 800G)
Aquios_: 441G (1.2%, 441G)
Heyndog: 420G (1.1%, 1,000G)
Dragon_RW: 100G (0.3%, 100G)

black bets:
VeryVert: 2,394G (22.5%, 2,394G)
Spuzzmocker: 2,130G (20.0%, 2,130G)
Mesmaster: 2,000G (18.8%, 104,208G)
BirbBrainsBot: 1,000G (9.4%, 52,658G)
prince_rogers_nelson_: 600G (5.6%, 600G)
getthemoneyz: 532G (5.0%, 1,071,674G)
Tithonus: 332G (3.1%, 332G)
superdevon1: 312G (2.9%, 312G)
neerrm: 300G (2.8%, 26,668G)
Ring_Wyrm: 300G (2.8%, 2,723G)
placidphoenix: 244G (2.3%, 244G)
twelfthrootoftwo: 200G (1.9%, 4,567G)
Evewho: 200G (1.9%, 5,503G)
datadrivenbot: 100G (0.9%, 47,186G)

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Chuckolator
Monster
Leo
77
70
Chocobo










Sairentozon7
Female
Serpentarius
63
59
Archer
Throw
Counter Magic
Defense UP
Waterwalking

Mythril Gun
Escutcheon
Holy Miter
Leather Outfit
Cursed Ring

Charge+1, Charge+7
Dictionary



Superdevon1
Female
Pisces
47
79
Geomancer
Black Magic
Catch
Equip Sword
Move+3

Defender

Thief Hat
Linen Robe
Red Shoes

Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind
Ice 4, Empower



Kalinda
Female
Virgo
50
55
Samurai
Time Magic
Earplug
Equip Shield
Retreat

Koutetsu Knife
Ice Shield
Iron Helmet
Light Robe
Elf Mantle

Koutetsu, Heaven's Cloud
Float, Demi, Stabilize Time, Galaxy Stop

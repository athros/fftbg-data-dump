Player: !Green
Team: Green Team
Palettes: Green/White



Kingchadking
Male
Scorpio
71
47
Knight
Item
Dragon Spirit
Throw Item
Waterbreathing

Ragnarok
Gold Shield
Barbuta
Genji Armor
Feather Boots

Head Break, Stasis Sword
X-Potion, Ether, Hi-Ether, Maiden's Kiss, Phoenix Down



Maeveen
Female
Taurus
78
56
Calculator
Black Magic
Distribute
Doublehand
Move-HP Up

Ivory Rod

Twist Headband
Wizard Outfit
Cursed Ring

CT, Height, 5, 4, 3
Fire, Fire 2, Fire 4, Bolt 3, Ice 2, Flare



Bpc2163
Male
Scorpio
67
47
Bard
Black Magic
Abandon
Defense UP
Fly

Lightning Bow

Green Beret
Crystal Mail
Cursed Ring

Angel Song, Battle Song, Nameless Song, Hydra Pit
Bolt, Bolt 4, Ice 2, Death



KasugaiRoastedPeas
Female
Pisces
48
53
Geomancer
Draw Out
Absorb Used MP
Concentrate
Waterbreathing

Giant Axe
Genji Shield
Holy Miter
Earth Clothes
Sprint Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard
Asura

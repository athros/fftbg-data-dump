Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mirapoix
Male
Cancer
40
64
Geomancer
White Magic
Brave Up
Dual Wield
Move-MP Up

Battle Axe
Kiyomori
Red Hood
Wizard Outfit
108 Gems

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cure, Raise, Raise 2, Regen, Protect, Protect 2, Esuna



Tyerenex
Male
Aries
81
63
Wizard
Charge
Earplug
Halve MP
Swim

Poison Rod

Twist Headband
Brigandine
Germinas Boots

Fire 2, Fire 4, Bolt, Ice, Ice 3, Frog
Charge+5, Charge+7



Lali Lulelo
Male
Aries
80
42
Calculator
Black Magic
Mana Shield
Short Status
Jump+1

Battle Folio

Feather Hat
Mythril Vest
Feather Boots

CT, Height, 5
Fire 2, Bolt, Flare



Waterwatereverywhere
Female
Gemini
56
55
Ninja
Talk Skill
MP Restore
Sicken
Move-HP Up

Morning Star
Flame Whip
Holy Miter
Judo Outfit
Red Shoes

Shuriken, Bomb, Knife
Persuade, Death Sentence, Insult, Mimic Daravon

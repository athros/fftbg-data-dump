Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Tapemeasure
Female
Leo
48
70
Monk
Charge
Speed Save
Secret Hunt
Jump+3



Twist Headband
Black Costume
Jade Armlet

Spin Fist, Pummel, Secret Fist, Purification
Charge+5



Rogueain
Male
Aquarius
61
76
Priest
Talk Skill
Mana Shield
Maintenance
Waterbreathing

Sage Staff

Twist Headband
Adaman Vest
Dracula Mantle

Cure 2, Cure 3, Cure 4, Reraise, Esuna
Invitation, Persuade, Insult



Oreo Pizza
Male
Sagittarius
74
78
Wizard
Item
Earplug
Short Charge
Levitate

Rod

Leather Hat
White Robe
Battle Boots

Fire 3, Bolt 2, Bolt 4, Ice 2, Frog, Death
Potion, Hi-Potion, X-Potion, Hi-Ether, Eye Drop, Maiden's Kiss, Phoenix Down



Ko2q
Male
Aries
49
57
Lancer
Item
Dragon Spirit
Magic Attack UP
Jump+3

Gokuu Rod
Ice Shield
Leather Helmet
Linen Cuirass
N-Kai Armlet

Level Jump2, Vertical Jump8
Potion, Hi-Potion, Ether, Soft, Remedy, Phoenix Down

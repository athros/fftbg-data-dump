Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lanshaft
Monster
Aquarius
60
79
Chocobo










CorpusCav
Female
Cancer
51
80
Squire
Jump
Regenerator
Equip Gun
Jump+2

Glacier Gun
Flame Shield
Twist Headband
Mythril Vest
Rubber Shoes

Throw Stone, Tickle, Wish, Scream
Level Jump2, Vertical Jump4



Lokenwow
Male
Pisces
61
63
Priest
Time Magic
Speed Save
Dual Wield
Ignore Height

Morning Star
Rainbow Staff
Flash Hat
Power Sleeve
Wizard Mantle

Cure 3, Raise, Raise 2, Reraise, Regen, Protect, Esuna
Haste, Haste 2, Slow, Reflect, Demi, Stabilize Time



Foofermoofer
Male
Aquarius
78
67
Bard
Summon Magic
Damage Split
Secret Hunt
Waterwalking

Hydra Bag

Black Hood
Leather Armor
Red Shoes

Life Song, Cheer Song, Magic Song, Diamond Blade, Space Storage, Hydra Pit
Salamander, Silf, Lich

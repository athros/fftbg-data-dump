Player: !Green
Team: Green Team
Palettes: Green/White



Unclebearboy
Female
Aquarius
52
59
Squire
Battle Skill
Catch
Martial Arts
Teleport


Flame Shield
Headgear
Black Costume
Salty Rage

Accumulate, Throw Stone, Heal, Yell, Scream, Ultima
Head Break, Night Sword



NIghtdew14
Female
Virgo
60
62
Dancer
Elemental
Meatbone Slash
Dual Wield
Waterbreathing

Persia
Ryozan Silk
Holy Miter
Mythril Vest
Dracula Mantle

Wiznaibus, Slow Dance, Polka Polka, Disillusion, Last Dance, Dragon Pit
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Superdevon1
Female
Taurus
78
42
Wizard
Battle Skill
Arrow Guard
Magic Defense UP
Move-MP Up

Blind Knife

Flash Hat
Earth Clothes
108 Gems

Fire 2, Fire 3, Bolt 2, Bolt 3, Ice
Head Break, Armor Break, Speed Break, Mind Break, Dark Sword



Ruleof5
Female
Leo
55
67
Archer
Yin Yang Magic
Earplug
Equip Bow
Retreat

Windslash Bow

Green Beret
Power Sleeve
Defense Armlet

Charge+10, Charge+20
Poison, Pray Faith, Doubt Faith, Blind Rage, Dispel Magic, Dark Holy

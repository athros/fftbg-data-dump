Player: !Red
Team: Red Team
Palettes: Red/Brown



DAC169
Male
Scorpio
73
70
Chemist
Punch Art
Catch
Martial Arts
Teleport

Cute Bag

Triangle Hat
Adaman Vest
Feather Boots

Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Remedy, Phoenix Down
Pummel, Chakra, Revive



WitchHunterIX
Female
Aries
69
43
Oracle
Steal
Parry
Magic Defense UP
Move-HP Up

Gokuu Rod

Feather Hat
Chameleon Robe
Feather Mantle

Blind, Silence Song, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy
Steal Helmet, Steal Armor, Steal Accessory, Arm Aim



Helpimabug
Female
Serpentarius
60
52
Priest
Draw Out
MA Save
Equip Sword
Teleport

Mythril Sword

Black Hood
Chameleon Robe
N-Kai Armlet

Cure, Cure 2, Cure 3, Protect, Protect 2, Shell, Shell 2, Esuna
Asura, Koutetsu, Murasame, Heaven's Cloud



Grininda
Male
Scorpio
54
71
Lancer
Basic Skill
PA Save
Dual Wield
Jump+2

Ivory Rod
Cypress Rod
Leather Helmet
Leather Armor
Wizard Mantle

Level Jump4, Vertical Jump4
Heal, Fury, Wish

Final Bets: green - 5 bets for 4,674G (44.2%, x1.26); champion - 9 bets for 5,890G (55.8%, x0.79)

green bets:
NIghtdew14: 1,500G (32.1%, 43,610G)
NovaKnight21: 1,400G (30.0%, 2,386G)
superdevon1: 1,026G (22.0%, 17,100G)
prince_rogers_nelson_: 548G (11.7%, 548G)
datadrivenbot: 200G (4.3%, 39,402G)

champion bets:
VolgraTheMoose: 1,156G (19.6%, 1,156G)
RaIshtar: 1,025G (17.4%, 1,025G)
BirbBrainsBot: 1,000G (17.0%, 42,693G)
neerrm: 750G (12.7%, 51,313G)
AllInBot: 567G (9.6%, 567G)
ruleof5: 500G (8.5%, 11,187G)
upvla: 456G (7.7%, 14,451G)
getthemoneyz: 264G (4.5%, 1,370,635G)
douchetron: 172G (2.9%, 172G)

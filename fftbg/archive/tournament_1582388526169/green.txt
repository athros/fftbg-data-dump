Player: !Green
Team: Green Team
Palettes: Green/White



ThePineappleSalesman
Male
Leo
66
47
Squire
Steal
Earplug
Secret Hunt
Move+1

Bow Gun

Cachusha
Black Costume
Rubber Shoes

Dash, Heal, Tickle, Cheer Up, Scream
Gil Taking, Steal Accessory, Arm Aim



Hard Reset
Female
Scorpio
79
68
Lancer
Throw
Mana Shield
Long Status
Fly

Musk Rod
Flame Shield
Genji Helmet
Gold Armor
Spike Shoes

Level Jump8, Vertical Jump8
Bomb, Knife, Dictionary



Unclesambam
Male
Sagittarius
71
66
Samurai
White Magic
Faith Up
Magic Attack UP
Waterwalking

Heaven's Cloud

Iron Helmet
White Robe
108 Gems

Asura, Koutetsu, Heaven's Cloud, Kiyomori, Chirijiraden
Cure 3, Raise, Shell, Wall, Esuna



Ominnous
Female
Scorpio
67
70
Mediator
Item
Caution
Dual Wield
Jump+1

Mage Masher
Orichalcum
Golden Hairpin
Wizard Robe
Battle Boots

Invitation, Praise, Threaten, Insult, Refute
X-Potion, Maiden's Kiss, Soft, Remedy, Phoenix Down

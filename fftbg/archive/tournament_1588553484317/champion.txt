Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ThePineappleSalesman
Female
Pisces
60
77
Archer
Talk Skill
Sunken State
Doublehand
Jump+1

Mythril Bow

Holy Miter
Leather Outfit
108 Gems

Charge+1, Charge+4, Charge+5
Invitation, Persuade, Solution, Insult, Mimic Daravon, Refute



Oreo Pizza
Male
Aries
69
77
Ninja
Yin Yang Magic
Mana Shield
Equip Gun
Move+3

Bestiary
Battle Folio
Holy Miter
Mythril Vest
Setiemson

Shuriken, Staff, Dictionary
Blind, Spell Absorb, Zombie, Dispel Magic, Paralyze, Sleep



MrDiggs49
Male
Capricorn
43
76
Geomancer
Item
Sunken State
Throw Item
Ignore Terrain

Coral Sword
Flame Shield
Headgear
Leather Outfit
Defense Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
X-Potion, Elixir, Maiden's Kiss, Soft, Holy Water, Phoenix Down



SkylerBunny
Female
Aries
58
56
Thief
Talk Skill
Critical Quick
Dual Wield
Waterwalking

Hidden Knife
Dagger
Holy Miter
Black Costume
Elf Mantle

Gil Taking, Steal Weapon, Steal Accessory
Invitation, Praise, Solution, Death Sentence, Negotiate, Refute

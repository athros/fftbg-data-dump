Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SkylerBunny
Female
Cancer
80
47
Wizard
Summon Magic
Parry
Dual Wield
Move+1

Assassin Dagger
Air Knife
Triangle Hat
Wizard Robe
Defense Armlet

Fire 4, Bolt 3
Moogle, Carbunkle, Odin, Salamander, Lich



Fenixcrest
Male
Scorpio
57
62
Calculator
Time Magic
Sunken State
Short Charge
Move-MP Up

Wizard Rod

Headgear
Light Robe
Germinas Boots

CT, Prime Number, 5, 4, 3
Haste 2, Slow, Stop, Immobilize, Stabilize Time



DrAntiSocial
Male
Taurus
49
76
Knight
Talk Skill
Counter Magic
Secret Hunt
Waterwalking

Defender
Escutcheon
Genji Helmet
Diamond Armor
Wizard Mantle

Head Break, Armor Break, Weapon Break, Speed Break, Mind Break, Stasis Sword
Invitation, Solution, Negotiate, Refute, Rehabilitate



Setdevildog
Male
Taurus
64
75
Archer
Black Magic
Absorb Used MP
Sicken
Fly

Night Killer
Genji Shield
Black Hood
Clothes
Magic Ring

Charge+2, Charge+3, Charge+5, Charge+7
Fire, Fire 4, Ice 2, Empower, Frog, Flare

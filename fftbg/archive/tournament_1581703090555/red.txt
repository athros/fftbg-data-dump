Player: !Red
Team: Red Team
Palettes: Red/Brown



Mirapoix
Male
Leo
80
69
Lancer
Basic Skill
Meatbone Slash
Attack UP
Swim

Iron Fan
Platinum Shield
Gold Helmet
Linen Robe
Dracula Mantle

Level Jump5, Vertical Jump4
Throw Stone, Heal, Tickle



Electric Glass
Male
Aquarius
52
60
Calculator
White Magic
Dragon Spirit
Defense UP
Ignore Height

Battle Bamboo

Twist Headband
White Robe
Magic Ring

CT, Height, Prime Number, 5, 3
Cure, Cure 4, Raise, Protect 2, Esuna



DarrenDinosaurs
Male
Pisces
38
78
Lancer
Time Magic
Damage Split
Magic Defense UP
Move-MP Up

Holy Lance
Ice Shield
Platinum Helmet
Chameleon Robe
Reflect Ring

Level Jump8, Vertical Jump7
Slow, Slow 2, Stop, Immobilize, Stabilize Time



RageImmortaI
Male
Aquarius
78
63
Geomancer
Battle Skill
Catch
Magic Defense UP
Teleport

Long Sword
Buckler
Barette
Leather Outfit
N-Kai Armlet

Pitfall, Water Ball, Local Quake, Blizzard, Gusty Wind, Lava Ball
Head Break, Shield Break, Magic Break, Power Break

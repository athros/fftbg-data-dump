Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Pedrossj
Male
Gemini
38
51
Archer
Basic Skill
Arrow Guard
Equip Gun
Swim

Mythril Gun
Round Shield
Crystal Helmet
Wizard Outfit
Germinas Boots

Charge+7
Dash, Throw Stone, Yell, Wish



LuckyLuckLuc2
Female
Aquarius
80
54
Archer
Basic Skill
Parry
Sicken
Move+1

Mythril Gun
Crystal Shield
Twist Headband
Black Costume
Feather Boots

Charge+1, Charge+3, Charge+4, Charge+7
Dash, Throw Stone, Yell, Wish



LanseDM
Female
Sagittarius
65
54
Dancer
White Magic
Damage Split
Equip Gun
Lava Walking

Bestiary

Flash Hat
Silk Robe
Feather Boots

Polka Polka, Dragon Pit
Raise, Protect 2, Shell, Wall, Esuna



VySaika
Female
Aquarius
66
57
Ninja
Punch Art
Parry
Equip Gun
Retreat

Mythril Gun
Romanda Gun
Green Beret
Power Sleeve
Defense Ring

Shuriken, Knife, Stick, Dictionary
Earth Slash, Secret Fist, Purification, Revive

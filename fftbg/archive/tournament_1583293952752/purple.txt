Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Denamda
Female
Virgo
43
43
Ninja
Draw Out
Blade Grasp
Equip Gun
Retreat

Stone Gun
Romanda Gun
Holy Miter
Mythril Vest
Germinas Boots

Staff
Asura, Bizen Boat, Heaven's Cloud



Urieltheflameofgod
Female
Capricorn
54
72
Ninja
Steal
PA Save
Attack UP
Waterwalking

Air Knife
Mythril Knife
Twist Headband
Mythril Vest
Chantage

Shuriken, Wand, Dictionary
Steal Accessory



Clippopo
Female
Leo
70
75
Time Mage
Jump
Parry
Beastmaster
Jump+1

Sage Staff

Red Hood
Wizard Robe
Elf Mantle

Slow, Stop, Immobilize, Reflect, Demi, Stabilize Time, Meteor
Level Jump8, Vertical Jump6



DeathTaxesAndAnime
Female
Libra
70
82
Geomancer
Draw Out
Arrow Guard
Secret Hunt
Jump+1

Platinum Sword
Platinum Shield
Red Hood
Robe of Lords
Spike Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball
Koutetsu, Muramasa

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Powerpinch
Male
Gemini
50
41
Bard
Time Magic
Regenerator
Magic Attack UP
Teleport

Bloody Strings

Headgear
Adaman Vest
Spike Shoes

Angel Song, Cheer Song, Magic Song, Hydra Pit
Slow, Slow 2, Quick, Demi, Demi 2, Stabilize Time



Richardserious
Male
Pisces
46
76
Archer
Sing
Brave Save
Equip Armor
Lava Walking

Mythril Bow

Cross Helmet
Mythril Vest
Red Shoes

Charge+1, Charge+2, Charge+4, Charge+10
Life Song, Battle Song, Magic Song, Last Song, Diamond Blade, Space Storage, Hydra Pit



SkylerBunny
Female
Aries
80
41
Mediator
Black Magic
Parry
Defense UP
Move+1

Battle Folio

Headgear
White Robe
Bracer

Invitation, Threaten, Preach, Insult, Negotiate, Refute, Rehabilitate
Fire, Fire 2, Fire 3, Bolt, Bolt 3, Ice 2, Empower, Frog, Death



Lowlf
Female
Scorpio
74
80
Monk
Steal
Distribute
Secret Hunt
Jump+2

Hydra Bag

Leather Hat
Mythril Vest
108 Gems

Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Steal Helmet, Steal Armor, Steal Accessory, Steal Status, Leg Aim

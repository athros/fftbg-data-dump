Final Bets: purple - 9 bets for 5,812G (49.0%, x1.04); brown - 7 bets for 6,059G (51.0%, x0.96)

purple bets:
reinoe: 2,000G (34.4%, 153,139G)
douchetron: 1,780G (30.6%, 3,491G)
ColetteMSLP: 1,000G (17.2%, 39,973G)
mirapoix: 299G (5.1%, 3,683G)
gorgewall: 201G (3.5%, 7,770G)
superbrute9k: 200G (3.4%, 2,289G)
datadrivenbot: 200G (3.4%, 45,190G)
DaKoopa49: 113G (1.9%, 653G)
dtrain332: 19G (0.3%, 1,945G)

brown bets:
NovaKnight21: 2,000G (33.0%, 7,044G)
EnemyController: 1,111G (18.3%, 1,484,014G)
BirbBrainsBot: 1,000G (16.5%, 134,656G)
AllInBot: 728G (12.0%, 728G)
prince_rogers_nelson_: 620G (10.2%, 620G)
ribbiks: 500G (8.3%, 6,860G)
PremaritalHandHolding: 100G (1.7%, 100G)

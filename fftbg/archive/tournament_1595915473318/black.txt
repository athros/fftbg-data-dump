Player: !Black
Team: Black Team
Palettes: Black/Red



EnemyController
Male
Taurus
75
79
Ninja
Elemental
Critical Quick
Equip Gun
Lava Walking

Bloody Strings
Bloody Strings
Green Beret
Black Costume
108 Gems

Wand
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Redrhinofever
Male
Virgo
77
57
Mime

PA Save
Short Status
Move-MP Up



Headgear
Earth Clothes
Battle Boots

Mimic




Nekojin
Male
Pisces
56
63
Knight
Punch Art
Mana Shield
Short Charge
Swim

Ragnarok
Aegis Shield
Iron Helmet
Black Robe
Defense Ring

Weapon Break, Magic Break, Power Break, Stasis Sword, Dark Sword
Spin Fist, Wave Fist, Purification, Chakra



Chuckolator
Female
Aquarius
52
45
Mediator
White Magic
Caution
Dual Wield
Waterwalking

Papyrus Codex
Battle Folio
Black Hood
Silk Robe
Red Shoes

Invitation, Solution, Negotiate, Refute
Cure 4, Raise, Protect 2, Shell, Shell 2, Esuna

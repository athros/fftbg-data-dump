Final Bets: green - 10 bets for 6,412G (49.7%, x1.01); yellow - 12 bets for 6,480G (50.3%, x0.99)

green bets:
VolgraTheMoose: 1,001G (15.6%, 66,884G)
maximumcrit: 1,000G (15.6%, 9,799G)
gorgewall: 900G (14.0%, 900G)
DeathTaxesAndAnime: 860G (13.4%, 860G)
Firesheath: 712G (11.1%, 1,398G)
Laserman1000: 599G (9.3%, 23,199G)
Zeroroute: 500G (7.8%, 500G)
killth3kid: 500G (7.8%, 504G)
datadrivenbot: 200G (3.1%, 66,192G)
Strifu: 140G (2.2%, 276G)

yellow bets:
Lord_Gwarth: 1,337G (20.6%, 3,070G)
BirbBrainsBot: 1,000G (15.4%, 2,423G)
Sairentozon7: 741G (11.4%, 741G)
E_Ballard: 740G (11.4%, 49,918G)
DustBirdEX: 660G (10.2%, 660G)
Smashy: 500G (7.7%, 8,434G)
dogsandcatsand: 416G (6.4%, 416G)
resjudicata3: 388G (6.0%, 388G)
ravingsockmonkey: 347G (5.4%, 347G)
Absalom_20: 199G (3.1%, 199G)
AllInBot: 100G (1.5%, 100G)
getthemoneyz: 52G (0.8%, 1,733,499G)

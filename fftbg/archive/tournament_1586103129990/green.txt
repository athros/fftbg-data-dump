Player: !Green
Team: Green Team
Palettes: Green/White



SerumD
Female
Leo
60
65
Archer
Dance
MP Restore
Concentrate
Move+2

Bow Gun
Round Shield
Gold Helmet
Judo Outfit
Defense Ring

Charge+1, Charge+2, Charge+4, Charge+7
Slow Dance, Polka Polka, Disillusion, Last Dance, Nether Demon, Dragon Pit



Rocl
Female
Aquarius
58
58
Time Mage
Steal
HP Restore
Equip Armor
Move-MP Up

Papyrus Codex

Diamond Helmet
Light Robe
Defense Armlet

Slow, Slow 2, Reflect, Demi 2
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon



ALY327
Male
Aries
59
55
Wizard
White Magic
Faith Up
Beastmaster
Waterwalking

Rod

Holy Miter
Clothes
Magic Gauntlet

Fire, Fire 2, Fire 3, Bolt 2, Ice 3, Flare
Cure 2, Cure 3, Raise, Regen, Protect, Shell 2, Esuna



Lazlodamus
Female
Gemini
72
70
Mime

Dragon Spirit
Defend
Fly



Headgear
Chain Vest
Wizard Mantle

Mimic


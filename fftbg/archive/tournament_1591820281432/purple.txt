Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Just Here2
Male
Virgo
77
41
Geomancer
Steal
Counter
Doublehand
Move-HP Up

Slasher

Leather Hat
Judo Outfit
Elf Mantle

Pitfall, Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Weapon, Steal Status, Leg Aim



Sairentozon7
Female
Gemini
49
42
Priest
Throw
Brave Save
Short Charge
Move-HP Up

Gold Staff

Feather Hat
Secret Clothes
Leather Mantle

Cure 2, Raise, Protect, Esuna, Holy
Knife, Ninja Sword, Axe



NicoSavoy
Female
Sagittarius
56
80
Oracle
Battle Skill
MA Save
Defend
Fly

Cypress Rod

Flash Hat
Wizard Outfit
Jade Armlet

Blind, Zombie, Foxbird, Dispel Magic, Paralyze, Sleep, Petrify
Power Break, Mind Break, Surging Sword



Spartan Paladin
Male
Taurus
39
69
Knight
Time Magic
Mana Shield
Equip Gun
Move-MP Up

Battle Folio
Round Shield
Iron Helmet
Linen Robe
Wizard Mantle

Head Break, Armor Break, Magic Break, Mind Break, Stasis Sword
Haste, Haste 2, Slow, Immobilize, Stabilize Time, Meteor

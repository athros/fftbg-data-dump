Player: !Black
Team: Black Team
Palettes: Black/Red



Old Overholt
Male
Cancer
63
45
Monk
Battle Skill
Arrow Guard
Equip Axe
Move+2

Gold Staff

Green Beret
Mystic Vest
Magic Ring

Chakra, Revive, Seal Evil
Armor Break, Shield Break, Speed Break, Power Break, Justice Sword, Surging Sword



Grandlanzer
Male
Leo
35
41
Lancer
Black Magic
Auto Potion
Equip Bow
Move+3

Lightning Bow

Platinum Helmet
Chain Mail
Power Wrist

Level Jump2, Vertical Jump7
Bolt 4, Ice, Ice 2, Ice 4, Empower, Frog



Roqqqpsi
Male
Libra
59
77
Time Mage
Steal
Counter
Beastmaster
Ignore Height

Battle Bamboo

Headgear
Wizard Robe
Cursed Ring

Slow, Slow 2, Demi, Galaxy Stop
Steal Shield



Oreo Pizza
Male
Taurus
72
58
Geomancer
Punch Art
Counter Magic
Dual Wield
Waterwalking

Giant Axe
Murasame
Leather Hat
White Robe
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Purification, Chakra, Revive

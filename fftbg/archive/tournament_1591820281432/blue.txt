Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nifboy
Female
Aquarius
63
77
Knight
Summon Magic
Counter Flood
Short Status
Fly

Defender
Bronze Shield
Circlet
Linen Cuirass
Bracer

Armor Break, Shield Break, Power Break, Mind Break, Stasis Sword, Night Sword
Ramuh, Ifrit, Golem, Carbunkle, Silf, Fairy, Lich



LDSkinny
Female
Libra
71
81
Oracle
Elemental
Parry
Maintenance
Move-HP Up

Musk Rod

Leather Hat
Wizard Robe
Bracer

Spell Absorb, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic
Pitfall, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball



Creggers
Male
Cancer
55
75
Ninja
Punch Art
Speed Save
Long Status
Move+3

Ninja Edge
Flame Whip
Green Beret
Secret Clothes
Reflect Ring

Shuriken, Bomb, Dictionary
Pummel, Wave Fist, Earth Slash, Chakra, Revive, Seal Evil



Meta Five
Female
Virgo
70
60
Oracle
White Magic
Counter Tackle
Maintenance
Jump+1

Octagon Rod

Cachusha
Wizard Robe
Reflect Ring

Blind, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Paralyze, Petrify, Dark Holy
Cure, Cure 2, Cure 3, Raise, Raise 2, Shell, Shell 2, Wall, Esuna

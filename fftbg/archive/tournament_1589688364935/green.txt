Player: !Green
Team: Green Team
Palettes: Green/White



Nifboy
Male
Leo
58
78
Samurai
White Magic
Hamedo
Halve MP
Jump+2

Kiyomori

Iron Helmet
Silk Robe
N-Kai Armlet

Asura, Koutetsu, Bizen Boat, Muramasa, Kikuichimoji
Cure 4, Raise, Raise 2, Reraise, Protect, Shell, Esuna



ANFz
Female
Capricorn
63
62
Oracle
Elemental
Earplug
Concentrate
Retreat

Bestiary

Leather Hat
Wizard Outfit
N-Kai Armlet

Poison, Dark Holy
Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



KasugaiRoastedPeas
Male
Cancer
67
63
Time Mage
Steal
Arrow Guard
Equip Armor
Fly

Musk Rod

Iron Helmet
Clothes
Jade Armlet

Haste, Haste 2, Slow 2, Stop, Reflect, Demi, Demi 2, Stabilize Time
Gil Taking, Steal Shield, Steal Accessory, Arm Aim



UmaiJam
Male
Sagittarius
68
64
Monk
Yin Yang Magic
Auto Potion
Dual Wield
Move+3



Twist Headband
Leather Outfit
Diamond Armlet

Pummel, Secret Fist, Purification, Revive
Blind, Foxbird, Sleep

Player: !zChamp
Team: Champion Team
Palettes: Brown/Green



ArlanKels
Monster
Libra
73
63
Sekhret










VolgraTheMoose
Male
Pisces
80
61
Lancer
Time Magic
Sunken State
Concentrate
Move+2

Spear
Escutcheon
Gold Helmet
Black Robe
Elf Mantle

Level Jump8, Vertical Jump8
Haste, Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi, Demi 2



ArashiKurobara
Male
Libra
67
39
Bard
Steal
Counter Flood
Doublehand
Swim

Yoichi Bow

Feather Hat
Crystal Mail
Feather Mantle

Angel Song, Life Song, Last Song, Diamond Blade, Sky Demon
Gil Taking, Steal Armor



Maximumcrit
Male
Taurus
69
58
Archer
Battle Skill
Brave Save
Maintenance
Jump+2

Blast Gun
Ice Shield
Circlet
Mythril Vest
Sprint Shoes

Charge+2, Charge+4, Charge+5, Charge+10
Shield Break, Weapon Break, Magic Break, Power Break, Stasis Sword, Justice Sword, Dark Sword

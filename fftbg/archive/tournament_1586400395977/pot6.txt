Final Bets: black - 17 bets for 10,965G (57.5%, x0.74); purple - 21 bets for 8,103G (42.5%, x1.35)

black bets:
JackOnFire1: 2,000G (18.2%, 8,000G)
upvla: 1,219G (11.1%, 2,804G)
Jonusfatson: 1,101G (10.0%, 1,101G)
Mtueni: 1,000G (9.1%, 105,521G)
MrDiggs49: 1,000G (9.1%, 8,819G)
waterwatereverywhere: 968G (8.8%, 968G)
Aldrammech: 600G (5.5%, 600G)
Goust18: 600G (5.5%, 14,621G)
JIDkomu: 517G (4.7%, 517G)
EnemyController: 400G (3.6%, 36,222G)
CCRUNNER149UWP: 344G (3.1%, 344G)
CapnChaos12: 300G (2.7%, 4,799G)
proper__noun__: 248G (2.3%, 248G)
Conome: 236G (2.2%, 236G)
TheManInPlaid: 232G (2.1%, 1,267G)
ApplesauceBoss: 100G (0.9%, 3,828G)
TheMM42: 100G (0.9%, 2,136G)

purple bets:
sinnyil2: 1,200G (14.8%, 7,541G)
BirbBrainsBot: 1,000G (12.3%, 292,101G)
getthemoneyz: 908G (11.2%, 448,371G)
Shalloween: 700G (8.6%, 15,461G)
rico_flex: 514G (6.3%, 514G)
Faplo: 500G (6.2%, 1,032G)
DarrenDinosaurs: 404G (5.0%, 404G)
OmnibotGamma: 360G (4.4%, 360G)
lijarkh: 360G (4.4%, 360G)
HaateXIII: 312G (3.9%, 312G)
DrAntiSocial: 305G (3.8%, 1,585G)
TheMurkGnome: 300G (3.7%, 4,059G)
ZephyrTempest: 284G (3.5%, 39,044G)
b0shii: 248G (3.1%, 248G)
Generalunderdog: 108G (1.3%, 1,136G)
AllInBot: 100G (1.2%, 100G)
Tougou: 100G (1.2%, 4,094G)
KingofTricksters: 100G (1.2%, 100G)
jjones032109: 100G (1.2%, 1,219G)
BlinkFS: 100G (1.2%, 763G)
datadrivenbot: 100G (1.2%, 24,695G)

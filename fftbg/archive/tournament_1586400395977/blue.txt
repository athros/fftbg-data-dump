Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Generalunderdog
Female
Libra
60
64
Dancer
Yin Yang Magic
Parry
Doublehand
Jump+2

Cashmere

Golden Hairpin
Power Sleeve
Defense Armlet

Witch Hunt, Slow Dance, Last Dance, Nether Demon, Dragon Pit
Poison, Zombie, Dispel Magic, Paralyze, Sleep



Red Lancer
Male
Capricorn
48
48
Lancer
Throw
Meatbone Slash
Magic Attack UP
Levitate

Holy Lance
Diamond Shield
Genji Helmet
Platinum Armor
Rubber Shoes

Level Jump8, Vertical Jump2
Bomb, Hammer, Wand



Vampire Killer
Male
Sagittarius
74
68
Ninja
Summon Magic
Abandon
Equip Gun
Fly

Bestiary
Battle Folio
Holy Miter
Mythril Vest
Power Wrist

Shuriken, Spear
Moogle, Shiva, Titan, Carbunkle, Cyclops



HASTERIOUS
Male
Aries
77
66
Mime

Absorb Used MP
Monster Talk
Jump+2



Black Hood
Leather Outfit
Red Shoes

Mimic


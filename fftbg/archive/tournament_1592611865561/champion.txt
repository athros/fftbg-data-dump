Player: !zChamp
Team: Champion Team
Palettes: Black/Red



Toka222
Male
Aries
51
46
Geomancer
Draw Out
Parry
Magic Defense UP
Move-MP Up

Giant Axe
Genji Shield
Feather Hat
Robe of Lords
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard, Lava Ball
Murasame, Heaven's Cloud, Muramasa



Biffcake01
Female
Leo
65
75
Geomancer
Draw Out
Counter Flood
Equip Knife
Fly

Spell Edge
Ice Shield
Triangle Hat
Wizard Robe
Dracula Mantle

Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard
Bizen Boat, Heaven's Cloud, Muramasa



Phrossi V
Female
Sagittarius
78
47
Calculator
Bio
Critical Quick
Martial Arts
Move+2



Feather Hat
Maximillian
Defense Ring

Blue Magic
Bio Tenebris, Bio Venenum, Bio Oleum, Bio Ranae, Bio Sanctus, Bio Silentium, Bio Lapis, Bio Immortuos, Bio Mortem, Bio Insanis



Ruebyy
Male
Taurus
71
54
Knight
Sing
Counter Flood
Doublehand
Waterbreathing

Iron Sword

Crystal Helmet
Linen Cuirass
Small Mantle

Armor Break, Magic Break, Mind Break, Justice Sword
Battle Song, Magic Song, Last Song, Diamond Blade, Space Storage, Sky Demon

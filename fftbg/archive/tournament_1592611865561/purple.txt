Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Skillomono
Female
Virgo
61
56
Time Mage
Black Magic
MA Save
Equip Polearm
Lava Walking

Battle Bamboo

Golden Hairpin
Mystic Vest
Elf Mantle

Haste 2, Slow 2, Stop, Immobilize, Float, Reflect, Stabilize Time, Meteor
Fire 4, Bolt 2, Ice 4, Empower



Run With Stone GUNs
Female
Virgo
58
54
Lancer
Talk Skill
Hamedo
Short Status
Ignore Terrain

Holy Lance
Gold Shield
Mythril Helmet
Platinum Armor
Defense Ring

Level Jump8, Vertical Jump8
Persuade, Praise, Preach, Insult, Negotiate



Reddwind
Male
Cancer
70
69
Wizard
White Magic
Distribute
Secret Hunt
Move+2

Wizard Rod

Twist Headband
Silk Robe
Jade Armlet

Fire 2, Fire 4, Ice, Ice 3, Empower, Frog
Cure, Raise, Raise 2, Protect 2, Shell, Shell 2, Esuna



Killth3kid
Male
Cancer
52
79
Geomancer
Jump
Arrow Guard
Short Charge
Ignore Terrain

Slasher
Genji Shield
Leather Hat
Brigandine
Chantage

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Level Jump5, Vertical Jump7

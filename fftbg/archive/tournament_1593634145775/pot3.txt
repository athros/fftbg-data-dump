Final Bets: white - 10 bets for 4,823G (17.1%, x4.85); black - 24 bets for 23,397G (82.9%, x0.21)

white bets:
DeathTaxesAndAnime: 1,406G (29.2%, 2,757G)
CrownOfHorns: 1,351G (28.0%, 1,351G)
Firesheath: 1,000G (20.7%, 72,327G)
killth3kid: 444G (9.2%, 132,914G)
CITOtheMOSQUITO: 212G (4.4%, 212G)
roqqqpsi: 148G (3.1%, 14,870G)
vanquishedfool: 100G (2.1%, 1,762G)
nifboy: 100G (2.1%, 4,771G)
Aquios_: 50G (1.0%, 3,309G)
getthemoneyz: 12G (0.2%, 1,156,187G)

black bets:
reinoe: 3,000G (12.8%, 11,482G)
dodgeroll: 2,782G (11.9%, 5,565G)
Lythe_Caraker: 2,500G (10.7%, 143,070G)
Nickyfive: 2,500G (10.7%, 10,157G)
Nizaha: 1,917G (8.2%, 1,917G)
cam_ATS: 1,567G (6.7%, 25,506G)
NIghtdew14: 1,200G (5.1%, 12,234G)
Forkmore: 1,111G (4.7%, 30,049G)
RampagingRobot: 1,000G (4.3%, 21,000G)
edgehead62888: 1,000G (4.3%, 20,239G)
TheChainNerd: 880G (3.8%, 8,804G)
Laserman1000: 816G (3.5%, 17,316G)
ArashiKurobara: 500G (2.1%, 23,949G)
DustBirdEX: 432G (1.8%, 432G)
gorgewall: 404G (1.7%, 404G)
Shalloween: 400G (1.7%, 70,439G)
NightwolfXVI: 300G (1.3%, 554G)
sososteve: 250G (1.1%, 2,931G)
BirbBrainsBot: 237G (1.0%, 119,419G)
Grandlanzer: 200G (0.9%, 43,514G)
letdowncity: 101G (0.4%, 10,941G)
ApplesauceBoss: 100G (0.4%, 13,544G)
datadrivenbot: 100G (0.4%, 51,401G)
Twisted_Nutsatchel: 100G (0.4%, 6,527G)

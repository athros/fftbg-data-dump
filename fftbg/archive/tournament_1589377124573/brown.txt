Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lifeguard Dan
Male
Capricorn
71
79
Lancer
Battle Skill
Faith Up
Equip Sword
Waterwalking

Sleep Sword
Mythril Shield
Crystal Helmet
Plate Mail
Dracula Mantle

Level Jump5, Vertical Jump7
Magic Break, Power Break, Stasis Sword



Catfashions
Male
Aries
57
69
Mime

Counter Magic
Martial Arts
Move+3



Headgear
Wizard Robe
Elf Mantle

Mimic




Maakur
Female
Aquarius
65
41
Samurai
Talk Skill
Counter
Defense UP
Lava Walking

Kiyomori

Mythril Helmet
Crystal Mail
Rubber Shoes

Koutetsu, Heaven's Cloud, Kiyomori, Muramasa
Invitation, Death Sentence, Refute, Rehabilitate



Drusic
Female
Virgo
60
72
Mime

Arrow Guard
Defend
Move-HP Up



Leather Hat
Robe of Lords
Defense Armlet

Mimic


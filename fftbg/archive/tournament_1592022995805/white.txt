Player: !White
Team: White Team
Palettes: White/Blue



Alithesillybird
Male
Leo
63
78
Lancer
Draw Out
Abandon
Defend
Waterbreathing

Mythril Spear
Genji Shield
Circlet
White Robe
Germinas Boots

Level Jump8, Vertical Jump7
Heaven's Cloud, Kiyomori, Kikuichimoji



Cougboi
Female
Scorpio
66
67
Mime

Counter Tackle
Martial Arts
Jump+3



Holy Miter
Rubber Costume
Feather Mantle

Mimic




Thyrandaal
Female
Taurus
62
49
Squire
Summon Magic
Critical Quick
Sicken
Fly

Rune Blade
Bronze Shield
Twist Headband
Reflect Mail
Feather Mantle

Dash, Heal, Wish, Scream, Ultima
Moogle, Ramuh, Titan, Bahamut, Odin, Leviathan, Fairy, Lich



KittymanAdvance
Male
Taurus
44
66
Time Mage
Math Skill
Hamedo
Defend
Ignore Height

Rainbow Staff

Leather Hat
Black Robe
Sprint Shoes

Haste, Demi, Demi 2
CT, 3

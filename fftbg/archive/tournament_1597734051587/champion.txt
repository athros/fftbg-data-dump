Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Drusiform
Male
Libra
43
54
Mime

Faith Save
Maintenance
Jump+3



Twist Headband
Mythril Vest
Diamond Armlet

Mimic




Blorpy
Male
Capricorn
71
49
Mime

HP Restore
Defense UP
Move+2



Headgear
Power Sleeve
Magic Ring

Mimic




WhiteTigress
Monster
Taurus
79
65
Blue Dragon










Chuckolator
Female
Taurus
70
76
Geomancer
Black Magic
Counter Tackle
Short Charge
Jump+3

Bizen Boat
Aegis Shield
Red Hood
Silk Robe
Salty Rage

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind, Lava Ball
Fire 2, Fire 3, Fire 4, Bolt, Bolt 2, Empower, Frog

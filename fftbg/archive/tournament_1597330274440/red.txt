Player: !Red
Team: Red Team
Palettes: Red/Brown



ZombiFrancis
Female
Libra
70
35
Oracle
Dance
Counter Flood
Beastmaster
Fly

Musk Rod

Green Beret
Silk Robe
108 Gems

Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Slow Dance



Rytor
Female
Leo
51
57
Archer
Yin Yang Magic
Distribute
Doublehand
Retreat

Lightning Bow

Green Beret
Secret Clothes
Defense Ring

Charge+2, Charge+3, Charge+4, Charge+5
Blind, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze



Powergems
Male
Sagittarius
56
73
Lancer
Throw
Damage Split
Defense UP
Move-MP Up

Obelisk
Diamond Shield
Mythril Helmet
Bronze Armor
Sprint Shoes

Level Jump8, Vertical Jump4
Shuriken, Bomb



SQUiDSQUARKLIN
Male
Cancer
59
66
Lancer
Item
Counter Magic
Martial Arts
Waterbreathing


Aegis Shield
Barbuta
Leather Armor
Feather Mantle

Level Jump8, Vertical Jump6
Potion, Hi-Potion, Ether, Hi-Ether, Maiden's Kiss, Phoenix Down

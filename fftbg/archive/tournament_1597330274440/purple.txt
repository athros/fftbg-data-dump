Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Evdoggity
Male
Sagittarius
79
52
Wizard
Punch Art
Absorb Used MP
Attack UP
Jump+2

Main Gauche

Triangle Hat
Wizard Outfit
Reflect Ring

Bolt 2, Frog, Death
Pummel, Wave Fist, Purification, Chakra



Kohlingen
Female
Scorpio
62
69
Mime

Speed Save
Defense UP
Jump+1


Buckler
Leather Hat
Brigandine
Power Wrist

Mimic




JonnyCue
Male
Aquarius
59
72
Monk
Basic Skill
Parry
Attack UP
Move+2



Twist Headband
Mythril Vest
Spike Shoes

Spin Fist, Pummel, Secret Fist, Purification
Accumulate, Tickle, Fury, Wish



MattMan119
Male
Aries
40
42
Ninja
Talk Skill
Absorb Used MP
Equip Gun
Ignore Height

Bestiary
Papyrus Codex
Green Beret
Brigandine
N-Kai Armlet

Knife, Axe
Invitation, Persuade, Praise, Threaten, Death Sentence, Mimic Daravon, Rehabilitate

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DrakeFS
Male
Capricorn
80
68
Monk
Talk Skill
MA Save
Short Status
Jump+1



Barette
Mythril Vest
Genji Gauntlet

Secret Fist, Purification, Chakra, Revive
Persuade, Insult, Negotiate, Refute



MoonSlayerRS
Female
Scorpio
74
63
Knight
Punch Art
Damage Split
Magic Attack UP
Move-HP Up

Blood Sword
Genji Shield
Gold Helmet
Bronze Armor
Wizard Mantle

Head Break, Weapon Break, Speed Break, Justice Sword
Wave Fist, Purification, Chakra, Revive



Hi Pewpew
Female
Aries
69
56
Thief
Item
Meatbone Slash
Concentrate
Fly

Nagrarock

Headgear
Clothes
Feather Boots

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status
Potion, Ether, Hi-Ether, Eye Drop, Holy Water, Phoenix Down



Kalpho
Male
Scorpio
79
67
Squire
Draw Out
PA Save
Magic Defense UP
Move-HP Up

Diamond Sword

Green Beret
Power Sleeve
Rubber Shoes

Dash, Heal, Tickle, Yell, Scream
Koutetsu, Heaven's Cloud, Muramasa

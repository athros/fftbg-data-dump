Player: !Green
Team: Green Team
Palettes: Green/White



Meta Five
Female
Virgo
52
63
Geomancer
Dance
Arrow Guard
Secret Hunt
Levitate

Hydra Bag
Platinum Shield
Golden Hairpin
Chameleon Robe
Wizard Mantle

Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball
Wiznaibus, Slow Dance, Obsidian Blade, Nether Demon



B0shii
Female
Aries
72
56
Thief
Basic Skill
Critical Quick
Martial Arts
Move+2



Twist Headband
Earth Clothes
Red Shoes

Steal Shield, Arm Aim
Fury



404namesnotfound
Male
Aquarius
76
60
Mime

Damage Split
Defend
Teleport


Gold Shield
Headgear
Brigandine
Magic Gauntlet

Mimic




Lunchboxtac
Female
Capricorn
71
70
Time Mage
Item
HP Restore
Halve MP
Ignore Terrain

Gold Staff

Ribbon
Clothes
N-Kai Armlet

Haste, Haste 2, Slow, Stop, Immobilize, Float, Reflect, Quick, Stabilize Time
Potion, Ether, Hi-Ether, Eye Drop, Maiden's Kiss

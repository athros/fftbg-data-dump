Player: !Green
Team: Green Team
Palettes: Green/White



Adenis222
Male
Taurus
74
74
Thief
Time Magic
Catch
Doublehand
Ignore Height

Mythril Knife

Iron Helmet
Earth Clothes
Spike Shoes

Gil Taking, Steal Heart, Steal Weapon, Steal Accessory
Float, Demi, Stabilize Time



Lanshaft
Male
Pisces
79
62
Geomancer
Summon Magic
Counter Magic
Magic Attack UP
Ignore Height

Kikuichimoji
Ice Shield
Red Hood
Clothes
Dracula Mantle

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Ifrit, Titan, Golem, Carbunkle, Bahamut, Leviathan, Salamander, Fairy, Lich



Hzor
Female
Leo
67
53
Archer
Talk Skill
Faith Up
Doublehand
Move+3

Poison Bow

Green Beret
Brigandine
Feather Boots

Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Persuade, Threaten, Solution, Mimic Daravon, Refute



Peeweemerman
Female
Capricorn
43
68
Priest
Dance
Sunken State
Equip Armor
Move+1

Wizard Staff

Barbuta
Mythril Armor
Sprint Shoes

Cure 2, Raise, Reraise, Regen, Protect, Esuna
Witch Hunt, Wiznaibus, Slow Dance, Polka Polka, Nameless Dance, Obsidian Blade

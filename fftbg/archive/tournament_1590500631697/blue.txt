Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



RunicMagus
Male
Capricorn
47
73
Wizard
Basic Skill
Regenerator
Martial Arts
Move+2



Triangle Hat
Wizard Robe
Genji Gauntlet

Fire, Fire 3, Bolt 3, Bolt 4, Ice 3
Tickle, Cheer Up



Aldrammech
Male
Pisces
55
40
Bard
Steal
Faith Save
Magic Attack UP
Waterwalking

Bloody Strings

Holy Miter
Adaman Vest
Germinas Boots

Cheer Song, Battle Song, Sky Demon, Hydra Pit
Gil Taking, Steal Heart, Steal Armor



Corky McCorkscrew
Male
Scorpio
44
41
Time Mage
Black Magic
Regenerator
Short Status
Move+3

Octagon Rod

Green Beret
Chain Vest
Magic Gauntlet

Haste, Haste 2, Stop, Immobilize, Quick, Demi, Demi 2
Bolt, Bolt 2, Bolt 4, Ice 2



Bringmoreknives
Female
Gemini
80
49
Squire
Dance
Meatbone Slash
Halve MP
Teleport 2

Slasher
Platinum Shield
Black Hood
Gold Armor
N-Kai Armlet

Accumulate, Dash, Throw Stone, Heal, Yell, Scream
Wiznaibus, Slow Dance, Disillusion, Void Storage

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Kyune
Female
Cancer
80
52
Lancer
Talk Skill
MP Restore
Monster Talk
Levitate

Gungnir
Platinum Shield
Gold Helmet
Chain Mail
Wizard Mantle

Level Jump5, Vertical Jump6
Persuade, Threaten, Death Sentence, Refute



Kellios11
Female
Pisces
48
62
Lancer
Draw Out
Counter
Defense UP
Waterwalking

Spear
Round Shield
Iron Helmet
Genji Armor
Diamond Armlet

Level Jump5, Vertical Jump7
Asura, Bizen Boat, Murasame



ExecutedGiraffe
Female
Leo
74
70
Squire
Dance
Meatbone Slash
Short Status
Teleport

Gastrafitis
Escutcheon
Grand Helmet
Judo Outfit
Elf Mantle

Accumulate, Throw Stone, Tickle, Wish
Polka Polka



CosmicTactician
Male
Capricorn
43
48
Oracle
Steal
Distribute
Doublehand
Lava Walking

Battle Bamboo

Red Hood
Chameleon Robe
Feather Mantle

Blind, Spell Absorb, Pray Faith, Silence Song, Dispel Magic, Sleep
Gil Taking, Steal Heart, Steal Status

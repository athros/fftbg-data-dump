Final Bets: black - 10 bets for 8,792G (53.7%, x0.86); brown - 7 bets for 7,573G (46.3%, x1.16)

black bets:
KasugaiRoastedPeas: 2,881G (32.8%, 10,690G)
ValtonZenola: 2,000G (22.7%, 19,084G)
JethroThrul: 1,191G (13.5%, 1,191G)
BirbBrainsBot: 1,000G (11.4%, 73,477G)
electric_algus: 600G (6.8%, 37,067G)
ColetteMSLP: 400G (4.5%, 6,607G)
SuzakuReii: 268G (3.0%, 268G)
ThreeMileIsland: 216G (2.5%, 216G)
datadrivenbot: 200G (2.3%, 54,708G)
getthemoneyz: 36G (0.4%, 1,200,389G)

brown bets:
Evewho: 4,881G (64.5%, 9,572G)
YaBoy125: 1,228G (16.2%, 12,286G)
Laserman1000: 600G (7.9%, 24,400G)
Digitalsocrates: 360G (4.8%, 2,867G)
twelfthrootoftwo: 300G (4.0%, 10,309G)
hiros13gts: 104G (1.4%, 104G)
moocaotao: 100G (1.3%, 4,660G)

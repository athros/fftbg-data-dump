Player: !White
Team: White Team
Palettes: White/Blue



CassiePhoenix
Female
Aquarius
72
66
Archer
Time Magic
Hamedo
Short Charge
Swim

Silver Bow

Leather Hat
Wizard Outfit
Magic Ring

Charge+1, Charge+3
Immobilize, Reflect, Stabilize Time



CT 5 Holy
Male
Virgo
62
56
Knight
Black Magic
Sunken State
Doublehand
Jump+3

Ancient Sword

Cross Helmet
Carabini Mail
Dracula Mantle

Armor Break, Shield Break, Justice Sword
Fire 2, Bolt, Bolt 3, Ice 2, Death



Zmoses
Female
Libra
73
78
Oracle
Item
Abandon
Magic Attack UP
Move-MP Up

Battle Folio

Barette
Linen Robe
Sprint Shoes

Blind, Spell Absorb, Pray Faith, Doubt Faith, Zombie, Blind Rage, Dispel Magic, Paralyze, Sleep, Petrify
Potion, Hi-Ether, Antidote, Echo Grass, Holy Water, Phoenix Down



ShintaroNayaka
Male
Sagittarius
72
62
Oracle
Throw
Counter Tackle
Equip Polearm
Levitate

Partisan

Green Beret
Judo Outfit
N-Kai Armlet

Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Dark Holy
Shuriken, Bomb, Knife, Staff

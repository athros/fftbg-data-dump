Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Galkife
Male
Gemini
64
50
Lancer
White Magic
PA Save
Short Charge
Ignore Terrain

Mythril Spear
Round Shield
Cross Helmet
Reflect Mail
Power Wrist

Level Jump4, Vertical Jump4
Cure 4, Raise, Protect, Protect 2, Shell, Shell 2, Wall, Esuna



Ar Tactic
Monster
Taurus
44
63
Taiju










Miku Shikhu
Female
Leo
77
45
Archer
Draw Out
Mana Shield
Equip Sword
Jump+3

Defender

Twist Headband
Earth Clothes
Dracula Mantle

Charge+1, Charge+20
Koutetsu, Bizen Boat, Muramasa



LegendaryWaffle91
Female
Virgo
75
59
Oracle
Dance
Counter
Equip Knife
Move+2

Cultist Dagger

Green Beret
Wizard Robe
Defense Ring

Blind, Doubt Faith, Dispel Magic, Paralyze
Slow Dance

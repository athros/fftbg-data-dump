Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



DudeMonkey77
Male
Libra
72
56
Geomancer
Basic Skill
Mana Shield
Equip Bow
Move+1

Ice Bow

Feather Hat
Chameleon Robe
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Quicksand, Sand Storm, Lava Ball
Dash, Throw Stone, Heal, Yell, Fury



Daveb
Male
Aries
44
76
Ninja
Draw Out
Counter
Martial Arts
Levitate

Flame Whip
Flame Whip
Twist Headband
Leather Outfit
Elf Mantle

Sword, Dictionary
Asura, Koutetsu, Bizen Boat, Murasame, Kiyomori, Muramasa, Kikuichimoji



CapnChaos12
Female
Scorpio
80
58
Geomancer
Draw Out
Faith Up
Magic Attack UP
Waterwalking

Giant Axe
Venetian Shield
Green Beret
Light Robe
Germinas Boots

Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Muramasa, Kikuichimoji



Baron Von Scrub
Female
Sagittarius
66
66
Priest
Draw Out
Absorb Used MP
Equip Bow
Move+3

Lightning Bow

Twist Headband
Wizard Outfit
Germinas Boots

Cure 3, Raise, Raise 2, Reraise, Shell, Esuna
Asura, Kikuichimoji, Chirijiraden

Player: !White
Team: White Team
Palettes: White/Blue



Cloud92684
Male
Pisces
80
64
Geomancer
Throw
HP Restore
Sicken
Move-MP Up

Slasher
Ice Shield
Triangle Hat
Power Sleeve
Dracula Mantle

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Ninja Sword



HaateXIII
Male
Cancer
75
64
Ninja
Talk Skill
Meatbone Slash
Equip Axe
Move+3

Hidden Knife
Healing Staff
Black Hood
Mystic Vest
108 Gems

Bomb, Sword, Staff, Axe
Praise, Death Sentence, Mimic Daravon



Brokenknight201
Male
Capricorn
40
53
Summoner
Time Magic
Counter
Dual Wield
Retreat

Rod
Rainbow Staff
Flash Hat
Clothes
Dracula Mantle

Ifrit, Titan, Bahamut, Odin, Leviathan, Fairy
Slow



Mayfu
Female
Leo
49
54
Dancer
Jump
Faith Save
Attack UP
Waterbreathing

Persia

Green Beret
Wizard Robe
Defense Armlet

Witch Hunt, Slow Dance, Polka Polka, Void Storage
Level Jump3, Vertical Jump6

Final Bets: blue - 13 bets for 6,825G (35.4%, x1.82); yellow - 15 bets for 12,442G (64.6%, x0.55)

blue bets:
Whiskybrave: 1,200G (17.6%, 1,200G)
scottydeluxe: 1,108G (16.2%, 1,108G)
ColetteMSLP: 1,000G (14.7%, 22,011G)
HaateXIII: 888G (13.0%, 888G)
VolgraTheMoose: 808G (11.8%, 808G)
killth3kid: 528G (7.7%, 4,009G)
Seaweed_B: 268G (3.9%, 11,150G)
Lord_Gwarth: 228G (3.3%, 228G)
gorgewall: 201G (2.9%, 4,613G)
AllInBot: 200G (2.9%, 200G)
datadrivenbot: 200G (2.9%, 41,680G)
cameloramelo: 150G (2.2%, 2,464G)
getthemoneyz: 46G (0.7%, 1,405,380G)

yellow bets:
reinoe: 3,000G (24.1%, 125,825G)
NovaKnight21: 2,000G (16.1%, 8,204G)
E_Ballard: 1,660G (13.3%, 1,660G)
BirbBrainsBot: 1,000G (8.0%, 127,431G)
Netmonmatt: 1,000G (8.0%, 1,961G)
Zagorsek: 976G (7.8%, 976G)
ArlanKels: 636G (5.1%, 1,272G)
prince_rogers_nelson_: 592G (4.8%, 592G)
Lanshaft: 516G (4.1%, 18,594G)
douchetron: 328G (2.6%, 328G)
ohhinm: 234G (1.9%, 2,885G)
Hirameki85: 200G (1.6%, 4,410G)
HolyDragoonXIV: 100G (0.8%, 698G)
DAC169: 100G (0.8%, 1,699G)
nhammen: 100G (0.8%, 22,938G)

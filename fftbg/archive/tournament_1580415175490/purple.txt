Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



KIYOMORI
Female
Sagittarius
76
57
Dancer
Elemental
Auto Potion
Equip Armor
Swim

Ryozan Silk

Flash Hat
Platinum Armor
Bracer

Disillusion, Obsidian Blade, Void Storage
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Lava Ball



Glennfk
Male
Taurus
74
67
Knight
Black Magic
Counter
Dual Wield
Ignore Height

Giant Axe
Ancient Sword
Genji Helmet
Light Robe
Bracer

Armor Break, Justice Sword
Fire 2, Fire 3, Bolt 2, Ice, Ice 2, Ice 3, Flare



Ichbinmiah
Monster
Taurus
68
80
Chocobo










Cloudycube
Male
Taurus
43
78
Geomancer
Draw Out
Counter
Defend
Retreat

Coral Sword
Escutcheon
Holy Miter
Black Costume
Defense Armlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Koutetsu, Bizen Boat, Murasame

Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Candina
Monster
Taurus
72
66
Wyvern










Nizaha
Male
Aries
44
57
Monk
Jump
HP Restore
Magic Defense UP
Jump+2



Feather Hat
Mythril Vest
Magic Gauntlet

Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Level Jump2, Vertical Jump5



TheChainNerd
Female
Leo
70
49
Monk
Dance
Mana Shield
Equip Knife
Teleport 2

Dragon Rod

Ribbon
Mythril Vest
Salty Rage

Spin Fist, Purification, Chakra, Revive
Polka Polka, Last Dance



Poulol
Female
Virgo
69
60
Monk
Jump
Counter Flood
Magic Defense UP
Move+1



Leather Hat
Chain Vest
Elf Mantle

Spin Fist, Wave Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive
Level Jump4, Vertical Jump7

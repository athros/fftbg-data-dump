Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Solomongrundy85
Female
Scorpio
69
43
Oracle
Draw Out
Speed Save
Short Status
Move-MP Up

Octagon Rod

Golden Hairpin
Wizard Robe
Power Wrist

Poison, Spell Absorb, Life Drain, Pray Faith, Blind Rage, Dispel Magic, Paralyze
Murasame, Kiyomori



Manc0
Male
Aries
77
70
Mime

Damage Split
Martial Arts
Waterbreathing



Iron Helmet
Wizard Outfit
Elf Mantle

Mimic




TheBlobZ
Male
Aquarius
79
53
Archer
Steal
Arrow Guard
Defense UP
Jump+3

Ice Bow

Red Hood
Mythril Vest
Bracer

Charge+1, Charge+5, Charge+7, Charge+10, Charge+20
Steal Weapon, Steal Accessory



Level99Magikarp
Female
Serpentarius
74
67
Lancer
Punch Art
Earplug
Equip Gun
Ignore Terrain

Mythril Gun
Crystal Shield
Diamond Helmet
Wizard Robe
Feather Boots

Level Jump4, Vertical Jump7
Secret Fist, Purification

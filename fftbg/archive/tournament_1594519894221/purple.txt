Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



TasisSai
Male
Libra
56
77
Bard
Time Magic
Parry
Dual Wield
Jump+2

Bloody Strings
Bloody Strings
Red Hood
Chain Vest
Defense Armlet

Magic Song, Nameless Song, Hydra Pit
Haste, Slow 2, Immobilize, Float, Quick, Stabilize Time



Grandlanzer
Male
Gemini
61
57
Knight
Summon Magic
HP Restore
Attack UP
Waterbreathing

Platinum Sword
Crystal Shield
Grand Helmet
Chain Mail
Chantage

Armor Break, Mind Break
Ifrit, Golem, Salamander, Silf



Hamborn
Male
Capricorn
70
51
Geomancer
Yin Yang Magic
Auto Potion
Concentrate
Lava Walking

Kikuichimoji
Crystal Shield
Twist Headband
White Robe
Defense Ring

Pitfall, Water Ball, Hallowed Ground, Static Shock, Quicksand, Gusty Wind
Zombie, Silence Song, Blind Rage, Dispel Magic



Roofiepops
Male
Sagittarius
80
75
Time Mage
Summon Magic
Arrow Guard
Secret Hunt
Teleport

Mace of Zeus

Triangle Hat
Earth Clothes
Defense Ring

Slow 2, Reflect, Stabilize Time
Moogle, Titan, Leviathan, Salamander

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nachtkrieger
Male
Aquarius
68
58
Archer
Talk Skill
Regenerator
Doublehand
Jump+2

Poison Bow

Triangle Hat
Black Costume
Sprint Shoes

Charge+1, Charge+3, Charge+4, Charge+5, Charge+20
Invitation, Persuade, Threaten, Solution, Negotiate



RongRongArts
Female
Capricorn
67
75
Chemist
Basic Skill
Auto Potion
Equip Sword
Jump+2

Long Sword

Leather Hat
Adaman Vest
Power Wrist

Hi-Ether, Echo Grass, Soft, Remedy
Throw Stone, Heal, Tickle, Yell, Wish



Maeveen
Male
Sagittarius
45
68
Ninja
Basic Skill
Hamedo
Sicken
Jump+2

Scorpion Tail
Flame Whip
Headgear
Earth Clothes
Dracula Mantle

Knife
Dash, Heal, Yell, Wish



Sypheck
Male
Gemini
50
43
Monk
White Magic
MA Save
Equip Polearm
Jump+2

Cypress Rod

Thief Hat
Mythril Vest
Bracer

Earth Slash, Secret Fist, Purification, Chakra, Seal Evil
Cure, Cure 3, Raise, Protect 2, Wall, Esuna

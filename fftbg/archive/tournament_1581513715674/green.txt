Player: !Green
Team: Green Team
Palettes: Green/White



Maha099
Female
Taurus
74
43
Summoner
Charge
Counter Magic
Short Charge
Move+1

Thunder Rod

Feather Hat
Leather Outfit
Feather Mantle

Moogle, Ramuh, Titan, Carbunkle, Odin, Salamander
Charge+1, Charge+4, Charge+5



SerumD
Female
Libra
49
70
Ninja
Jump
Arrow Guard
Concentrate
Move-MP Up

Short Edge
Spell Edge
Leather Hat
Adaman Vest
Leather Mantle

Sword, Ninja Sword
Level Jump4, Vertical Jump3



Life Tactics
Male
Aquarius
40
72
Monk
Summon Magic
Parry
Attack UP
Move+1



Green Beret
Rubber Costume
Power Wrist

Earth Slash, Purification, Chakra, Revive
Titan, Fairy, Lich, Cyclops



Glitchminded
Female
Pisces
65
68
Chemist
Dance
MA Save
Equip Knife
Levitate

Blind Knife

Triangle Hat
Wizard Outfit
Defense Armlet

Potion, X-Potion, Elixir, Antidote, Soft
Polka Polka, Nameless Dance

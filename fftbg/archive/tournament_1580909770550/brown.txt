Player: !Brown
Team: Brown Team
Palettes: Brown/Green



La Boomstick
Male
Leo
56
61
Archer
Black Magic
Brave Up
Beastmaster
Jump+1

Night Killer
Ice Shield
Green Beret
Judo Outfit
Rubber Shoes

Charge+1, Charge+2, Charge+3, Charge+5, Charge+7
Fire, Bolt 3, Ice, Ice 3, Ice 4



AStatue
Female
Sagittarius
57
72
Time Mage
Steal
Counter
Secret Hunt
Retreat

Wizard Staff

Flash Hat
Power Sleeve
Cursed Ring

Haste, Haste 2, Reflect
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Accessory, Arm Aim



Soltaker21
Male
Leo
78
75
Archer
Basic Skill
Mana Shield
Equip Gun
Lava Walking

Glacier Gun
Platinum Shield
Twist Headband
Chain Vest
Battle Boots

Charge+2, Charge+5, Charge+7
Heal, Yell, Scream



Alc Trinity
Male
Scorpio
79
68
Lancer
Elemental
Absorb Used MP
Doublehand
Jump+2

Javelin

Crystal Helmet
Silk Robe
Magic Gauntlet

Level Jump3, Vertical Jump7
Pitfall, Water Ball, Hallowed Ground, Local Quake, Sand Storm, Blizzard, Gusty Wind, Lava Ball

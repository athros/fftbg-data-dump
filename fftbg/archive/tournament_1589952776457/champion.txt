Player: !zChamp
Team: Champion Team
Palettes: Green/White



Skillomono
Male
Gemini
69
73
Calculator
White Magic
Sunken State
Equip Knife
Waterwalking

Short Edge

Triangle Hat
Chameleon Robe
Sprint Shoes

CT, Height, Prime Number, 5, 4, 3
Cure 2, Cure 4, Raise, Protect, Protect 2, Esuna, Holy



Electric Glass
Female
Libra
66
65
Mime

HP Restore
Attack UP
Move+2



Twist Headband
Silk Robe
Genji Gauntlet

Mimic




Evewho
Monster
Taurus
44
67
Red Chocobo










Solomongrundy85
Male
Gemini
53
79
Thief
Jump
Critical Quick
Magic Attack UP
Swim

Diamond Sword

Leather Hat
Secret Clothes
Power Wrist

Steal Weapon, Steal Accessory, Steal Status
Level Jump8, Vertical Jump6

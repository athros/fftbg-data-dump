Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DAC169
Male
Leo
69
55
Chemist
Jump
Brave Save
Equip Axe
Move+1

Slasher

Thief Hat
Brigandine
Leather Mantle

Potion, Hi-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down
Level Jump4, Vertical Jump8



Serperemagus
Male
Taurus
52
40
Bard
Charge
Catch
Halve MP
Move-MP Up

Ramia Harp

Triangle Hat
Judo Outfit
Germinas Boots

Battle Song, Last Song, Hydra Pit
Charge+2, Charge+3, Charge+4, Charge+5, Charge+20



OneHundredFists
Male
Cancer
50
63
Geomancer
Jump
Damage Split
Equip Gun
Move+3

Glacier Gun
Hero Shield
Flash Hat
Secret Clothes
Cherche

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind, Lava Ball
Level Jump8, Vertical Jump3



Reinoe
Female
Leo
48
59
Geomancer
Dance
Faith Save
Defense UP
Jump+3

Broad Sword
Crystal Shield
Triangle Hat
Light Robe
Spike Shoes

Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Slow Dance, Last Dance

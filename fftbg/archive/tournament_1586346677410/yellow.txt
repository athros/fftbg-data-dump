Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lastly
Male
Leo
63
76
Monk
Charge
Brave Up
Equip Shield
Retreat


Aegis Shield
Triangle Hat
Black Costume
Leather Mantle

Spin Fist, Pummel, Secret Fist, Revive
Charge+1, Charge+5



Kingbananabear
Monster
Scorpio
66
55
Serpentarius










ScarletSpiderBen
Male
Capricorn
53
49
Archer
Battle Skill
Counter Tackle
Equip Shield
Move-HP Up

Poison Bow
Platinum Shield
Circlet
Power Sleeve
N-Kai Armlet

Charge+3, Charge+5, Charge+20
Armor Break, Mind Break, Stasis Sword, Dark Sword



UmaiJam
Female
Virgo
47
71
Ninja
Battle Skill
Counter Magic
Equip Gun
Jump+2

Romanda Gun
Mythril Gun
Thief Hat
Earth Clothes
Defense Ring

Axe
Justice Sword, Surging Sword

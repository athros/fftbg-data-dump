Player: !Black
Team: Black Team
Palettes: Black/Red



ZephyrTempest
Female
Capricorn
37
63
Priest
Battle Skill
Dragon Spirit
Equip Gun
Move-MP Up

Stone Gun

Thief Hat
Light Robe
Magic Ring

Cure, Cure 2, Raise, Regen, Esuna
Head Break, Armor Break, Weapon Break, Mind Break, Justice Sword, Dark Sword



Rubenflonne
Monster
Aquarius
66
57
Red Chocobo










Upvla
Male
Leo
67
66
Ninja
Charge
PA Save
Doublehand
Ignore Height

Cultist Dagger

Golden Hairpin
Mythril Vest
Setiemson

Shuriken, Bomb
Charge+4, Charge+5



Leakimiko
Female
Leo
58
56
Ninja
Elemental
Arrow Guard
Equip Gun
Swim

Battle Folio
Battle Folio
Twist Headband
Mythril Vest
Genji Gauntlet

Shuriken, Wand
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Lava Ball

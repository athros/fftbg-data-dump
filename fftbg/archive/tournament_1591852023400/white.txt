Player: !White
Team: White Team
Palettes: White/Blue



GladiatorLupe
Male
Capricorn
76
49
Bard
Charge
Counter Tackle
Defense UP
Jump+3

Bloody Strings

Green Beret
Clothes
Elf Mantle

Magic Song, Nameless Song, Space Storage
Charge+1, Charge+3, Charge+5, Charge+10



Mesmaster
Female
Sagittarius
61
66
Priest
Jump
Faith Save
Short Charge
Move-MP Up

Oak Staff

Red Hood
Chain Vest
Jade Armlet

Cure 2, Cure 3, Raise, Raise 2, Regen, Shell, Esuna
Level Jump5, Vertical Jump6



NovaKnight21
Monster
Scorpio
58
67
Mindflayer










Prince Rogers Nelson
Female
Aries
67
59
Priest
Basic Skill
Counter Magic
Dual Wield
Ignore Height

Flail
Wizard Staff
Green Beret
Earth Clothes
Battle Boots

Cure 2, Cure 4, Raise, Regen, Shell 2, Esuna, Holy
Accumulate

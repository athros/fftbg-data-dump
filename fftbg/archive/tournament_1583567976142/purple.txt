Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ZergTwitch
Male
Capricorn
57
77
Archer
Throw
Regenerator
Dual Wield
Teleport

Mythril Bow

Thief Hat
Adaman Vest
Dracula Mantle

Charge+3, Charge+5, Charge+7
Shuriken, Knife



Twelfthrootoftwo
Monster
Leo
41
69
Great Malboro










Ring Wyrm
Female
Gemini
42
40
Oracle
Elemental
Speed Save
Beastmaster
Ignore Height

Iron Fan

Headgear
Black Costume
Angel Ring

Blind, Spell Absorb, Life Drain, Doubt Faith, Zombie, Foxbird, Dispel Magic, Sleep
Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball



Clockthief
Male
Cancer
53
77
Mediator
White Magic
Counter
Doublehand
Fly

Stone Gun

Black Hood
Silk Robe
Dracula Mantle

Invitation, Persuade, Rehabilitate
Reraise, Regen, Protect, Wall, Esuna

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lord Gwarth
Male
Libra
48
78
Knight
Sing
Earplug
Attack UP
Retreat

Giant Axe
Escutcheon
Platinum Helmet
Linen Cuirass
Magic Gauntlet

Head Break, Weapon Break, Speed Break, Surging Sword
Angel Song



Rikrizen
Female
Gemini
78
52
Wizard
Throw
Catch
Defend
Teleport

Ice Rod

Black Hood
Linen Robe
Reflect Ring

Bolt, Bolt 2, Bolt 3, Bolt 4, Ice 4
Bomb



THE REAL SEADOG
Male
Virgo
39
54
Wizard
Summon Magic
Caution
Defense UP
Lava Walking

Mage Masher

Leather Hat
Wizard Robe
Diamond Armlet

Fire, Fire 2, Bolt, Bolt 3, Bolt 4, Ice, Ice 4, Death
Moogle, Ifrit, Golem, Leviathan, Salamander



DavenIII
Male
Scorpio
76
51
Monk
Basic Skill
MA Save
Equip Shield
Move+1


Genji Shield
Golden Hairpin
Wizard Outfit
Feather Mantle

Purification, Revive, Seal Evil
Accumulate, Throw Stone, Heal, Cheer Up, Fury, Wish, Scream

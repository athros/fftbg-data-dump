Player: !Black
Team: Black Team
Palettes: Black/Red



Roqqqpsi
Male
Taurus
77
54
Knight
Draw Out
Absorb Used MP
Dual Wield
Move+2

Coral Sword
Long Sword
Diamond Helmet
Leather Armor
Elf Mantle

Head Break, Shield Break, Magic Break, Power Break, Mind Break, Night Sword
Koutetsu, Murasame



MagicBottle
Monster
Sagittarius
74
60
Behemoth










Daveb
Female
Libra
61
66
Ninja
White Magic
Damage Split
Equip Armor
Move-MP Up

Sasuke Knife
Ninja Edge
Crystal Helmet
Black Robe
Power Wrist

Bomb, Knife, Axe
Cure 3, Cure 4, Raise, Protect 2, Shell 2, Esuna



Actual JP
Male
Gemini
45
71
Knight
Black Magic
Critical Quick
Magic Defense UP
Ignore Height

Giant Axe
Diamond Shield
Circlet
Diamond Armor
Leather Mantle

Weapon Break, Speed Break, Surging Sword
Fire, Fire 2, Ice 4, Frog

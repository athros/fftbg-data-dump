Player: !Green
Team: Green Team
Palettes: Green/White



BuffaloCrunch
Female
Virgo
62
78
Knight
White Magic
Caution
Magic Defense UP
Waterbreathing

Iron Sword
Mythril Shield
Barbuta
Gold Armor
Cursed Ring

Shield Break, Weapon Break, Power Break
Cure, Raise, Raise 2, Regen, Shell 2, Wall, Esuna



Solomongrundy85
Male
Gemini
78
71
Priest
Summon Magic
Counter
Attack UP
Retreat

Morning Star

Golden Hairpin
Clothes
Small Mantle

Cure, Raise, Raise 2, Protect, Wall, Esuna
Moogle, Shiva, Titan, Golem, Carbunkle, Silf, Lich



PoroTact
Male
Aquarius
46
68
Ninja
Battle Skill
Critical Quick
Equip Knife
Waterbreathing

Spell Edge
Dragon Rod
Golden Hairpin
Leather Outfit
Magic Ring

Shuriken, Bomb
Armor Break, Weapon Break, Stasis Sword



Firesheath
Male
Pisces
67
40
Samurai
Jump
Speed Save
Doublehand
Move+3

Koutetsu Knife

Leather Helmet
Linen Robe
Leather Mantle

Asura, Heaven's Cloud, Muramasa
Level Jump4, Vertical Jump4

Final Bets: purple - 20 bets for 11,986G (55.2%, x0.81); brown - 17 bets for 9,711G (44.8%, x1.23)

purple bets:
Snowfats: 1,419G (11.8%, 1,419G)
ACSpree: 1,283G (10.7%, 2,439G)
VolgraTheMoose: 1,001G (8.4%, 5,982G)
porkchop5158: 1,000G (8.3%, 43,288G)
Error72: 969G (8.1%, 969G)
twelfthrootoftwo: 962G (8.0%, 1,887G)
fluffskull: 844G (7.0%, 844G)
datadrivenbot: 589G (4.9%, 39,047G)
Zeroroute: 500G (4.2%, 500G)
pplvee1: 500G (4.2%, 500G)
letdowncity: 416G (3.5%, 2,076G)
killth3kid: 404G (3.4%, 404G)
Kronikle: 400G (3.3%, 1,881G)
roqqqpsi: 363G (3.0%, 1,652G)
CapnChaos12: 300G (2.5%, 6,985G)
BoneMiser: 300G (2.5%, 300G)
Smokegiant: 236G (2.0%, 236G)
AllInBot: 200G (1.7%, 200G)
RyanTheRebs: 200G (1.7%, 1,936G)
Miku_Shikhu: 100G (0.8%, 10,068G)

brown bets:
NicoSavoy: 3,000G (30.9%, 176,017G)
Shalloween: 1,500G (15.4%, 62,563G)
dogsandcatsand: 1,077G (11.1%, 1,077G)
BirbBrainsBot: 1,000G (10.3%, 176,253G)
TheMurkGnome: 906G (9.3%, 906G)
roofiepops: 444G (4.6%, 4,409G)
Rislyeu: 376G (3.9%, 376G)
Geffro1908: 236G (2.4%, 236G)
boosted420: 200G (2.1%, 779G)
skipsandwiches: 199G (2.0%, 199G)
TasisSai: 169G (1.7%, 332G)
Xoomwaffle: 132G (1.4%, 132G)
Anamalous_Accountant: 104G (1.1%, 104G)
Lemonjohns: 100G (1.0%, 2,448G)
drkarma503: 100G (1.0%, 445G)
acedude1981: 100G (1.0%, 5,679G)
getthemoneyz: 68G (0.7%, 1,300,665G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Firesheath
Female
Cancer
60
53
Squire
Summon Magic
Regenerator
Maintenance
Swim

Ice Brand
Escutcheon
Crystal Helmet
Black Costume
Defense Armlet

Dash, Throw Stone, Heal, Yell, Wish
Moogle, Shiva, Titan, Bahamut



ShintaroNayaka
Male
Libra
63
54
Geomancer
Yin Yang Magic
Counter Magic
Short Status
Ignore Height

Battle Axe
Crystal Shield
Leather Hat
Earth Clothes
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Gusty Wind, Lava Ball
Blind, Life Drain, Pray Faith, Foxbird, Confusion Song



Captainmilestw
Female
Aquarius
75
70
Geomancer
White Magic
Dragon Spirit
Defend
Waterbreathing

Battle Axe
Aegis Shield
Black Hood
Power Sleeve
Jade Armlet

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind
Cure 3, Raise, Reraise, Regen, Protect



Actual JP
Male
Taurus
54
45
Knight
Sing
Dragon Spirit
Secret Hunt
Teleport

Giant Axe
Ice Shield
Crystal Helmet
Gold Armor
Leather Mantle

Head Break, Speed Break, Justice Sword, Night Sword, Surging Sword
Angel Song, Life Song, Last Song, Sky Demon

Player: !White
Team: White Team
Palettes: White/Blue



RuneS 77
Monster
Aquarius
43
67
Squidraken










DAC169
Male
Libra
76
44
Ninja
Draw Out
Parry
Halve MP
Waterwalking

Hidden Knife
Air Knife
Black Hood
Rubber Costume
Genji Gauntlet

Shuriken, Sword, Spear, Wand, Dictionary
Murasame, Muramasa, Kikuichimoji



Zeroroute
Male
Serpentarius
69
56
Archer
Yin Yang Magic
Meatbone Slash
Equip Bow
Levitate

Windslash Bow

Leather Hat
Earth Clothes
Elf Mantle

Charge+1, Charge+4, Charge+5
Poison, Dispel Magic



Superdevon1
Female
Libra
50
45
Time Mage
Throw
Counter Magic
Equip Sword
Jump+1

Ice Brand

Barette
Black Robe
Jade Armlet

Haste, Slow, Stop, Immobilize, Float
Shuriken, Wand

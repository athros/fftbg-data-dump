Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Daveb
Male
Pisces
51
79
Ninja
Elemental
Faith Save
Equip Gun
Swim

Battle Folio
Fairy Harp
Headgear
Mystic Vest
Diamond Armlet

Shuriken, Bomb, Knife, Wand, Dictionary
Pitfall, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



BreederLancet
Female
Gemini
80
53
Wizard
White Magic
Absorb Used MP
Long Status
Fly

Assassin Dagger

Holy Miter
Judo Outfit
108 Gems

Fire 3, Fire 4, Bolt 3, Ice, Ice 3, Flare
Cure 2, Cure 3, Raise, Raise 2, Protect 2, Shell 2



Firesheath
Male
Libra
43
53
Squire
Summon Magic
Catch
Equip Armor
Levitate

Morning Star
Platinum Shield
Platinum Helmet
White Robe
Power Wrist

Dash, Heal
Moogle, Ramuh, Ifrit, Carbunkle, Silf



Holyonline
Female
Taurus
41
59
Geomancer
Throw
Critical Quick
Equip Sword
Levitate

Koutetsu Knife
Bronze Shield
Feather Hat
Linen Robe
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Shuriken

Player: !Black
Team: Black Team
Palettes: Black/Red



LOKITHUS
Female
Taurus
42
39
Summoner
Talk Skill
Speed Save
Short Charge
Retreat

Dragon Rod

Thief Hat
Wizard Robe
Reflect Ring

Moogle, Golem, Silf, Fairy
Invitation, Solution, Death Sentence, Negotiate, Refute



Mirapoix
Female
Sagittarius
38
75
Geomancer
Battle Skill
Abandon
Equip Axe
Ignore Terrain

Flame Whip
Genji Shield
Red Hood
Mystic Vest
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Armor Break, Power Break, Mind Break, Dark Sword



Anox Skell
Female
Scorpio
52
47
Geomancer
Steal
Counter Magic
Equip Axe
Move+2

Slasher
Genji Shield
Leather Hat
Clothes
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard
Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim



HaplessOne
Male
Serpentarius
58
74
Archer
Sing
Meatbone Slash
Doublehand
Waterwalking

Hunting Bow

Headgear
Clothes
Rubber Shoes

Charge+3, Charge+7, Charge+20
Angel Song, Magic Song, Diamond Blade

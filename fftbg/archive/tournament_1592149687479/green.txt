Player: !Green
Team: Green Team
Palettes: Green/White



L2 Sentinel
Male
Capricorn
60
68
Mediator
Punch Art
Meatbone Slash
Doublehand
Move+2

Bestiary

Holy Miter
Light Robe
Cursed Ring

Invitation, Persuade, Preach, Solution, Insult, Mimic Daravon
Secret Fist, Purification, Chakra, Revive, Seal Evil



Nappychicken
Male
Gemini
64
72
Knight
Elemental
Catch
Equip Axe
Ignore Terrain

Morning Star
Flame Shield
Mythril Helmet
Gold Armor
Spike Shoes

Magic Break, Dark Sword
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm



Sans From Snowdin
Male
Capricorn
58
68
Monk
Jump
Hamedo
Equip Bow
Teleport

Windslash Bow

Green Beret
Power Sleeve
Genji Gauntlet

Purification, Revive
Level Jump5, Vertical Jump6



Breakdown777
Male
Libra
57
52
Knight
Draw Out
Parry
Sicken
Waterwalking

Defender
Venetian Shield
Gold Helmet
Silk Robe
Feather Mantle

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Night Sword, Surging Sword
Asura, Heaven's Cloud, Kiyomori

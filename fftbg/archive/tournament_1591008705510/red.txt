Player: !Red
Team: Red Team
Palettes: Red/Brown



ApplesauceBoss
Female
Gemini
47
62
Calculator
Ranch Skill
Regenerator
Beastmaster
Retreat

Battle Folio

Leather Hat
Mystic Vest
Spike Shoes

Blue Magic
Shake Off, Storm Around, Blow Fire, Mimic Titan, Gather Power, Choco Attack, Choco Cure, Choco Esuna, Choco Ball, Choco Meteor



Humble Fabio
Male
Scorpio
61
73
Chemist
Basic Skill
Earplug
Magic Attack UP
Move-HP Up

Romanda Gun

Triangle Hat
Wizard Outfit
Jade Armlet

Potion, X-Potion, Ether, Hi-Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Throw Stone, Heal, Yell, Cheer Up, Wish, Scream



Vorap
Male
Cancer
71
70
Archer
Black Magic
Auto Potion
Equip Armor
Move-HP Up

Bow Gun
Aegis Shield
Mythril Helmet
Linen Robe
Small Mantle

Charge+2, Charge+3
Bolt, Bolt 2, Bolt 4, Ice 2



Gorgewall
Female
Aquarius
70
77
Geomancer
Item
Earplug
Long Status
Ignore Terrain

Giant Axe
Gold Shield
Holy Miter
Leather Outfit
Rubber Shoes

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Potion, X-Potion, Ether, Hi-Ether, Elixir, Soft, Holy Water, Phoenix Down

Final Bets: purple - 16 bets for 9,128G (10.7%, x8.35); champion - 22 bets for 76,256G (89.3%, x0.12)

purple bets:
mirapoix: 1,789G (19.6%, 1,789G)
DeathTaxesAndAnime: 1,597G (17.5%, 1,597G)
Blain_Cooper: 1,000G (11.0%, 3,399G)
BirbBrainsBot: 1,000G (11.0%, 50,054G)
sinnyil2: 581G (6.4%, 581G)
z32o: 500G (5.5%, 4,690G)
ungabunga_bot: 493G (5.4%, 95,078G)
DarrenDinosaurs: 408G (4.5%, 408G)
ColetteMSLP: 400G (4.4%, 16,358G)
upvla: 322G (3.5%, 37,268G)
Slowbrofist: 304G (3.3%, 304G)
ZephyrTempest: 234G (2.6%, 46,038G)
OmnibotGamma: 200G (2.2%, 867G)
RughSontos: 100G (1.1%, 2,632G)
maakur_: 100G (1.1%, 3,147G)
Evewho: 100G (1.1%, 5,739G)

champion bets:
Ragnarok784: 50,386G (66.1%, 50,386G)
reinoe: 9,084G (11.9%, 9,084G)
Aldrammech: 3,534G (4.6%, 35,344G)
Laserman1000: 2,550G (3.3%, 23,450G)
WireLord: 2,000G (2.6%, 36,044G)
roofiepops: 1,600G (2.1%, 15,590G)
Lord_Burrah: 1,200G (1.6%, 98,574G)
volgrathemoose: 1,001G (1.3%, 3,018G)
datadrivenbot: 927G (1.2%, 26,388G)
HASTERIOUS: 692G (0.9%, 1,357G)
EnemyController: 607G (0.8%, 40,107G)
mpghappiness: 491G (0.6%, 491G)
moonliquor: 444G (0.6%, 888G)
b0shii: 333G (0.4%, 1,357G)
Tithonus: 300G (0.4%, 14,876G)
DrAntiSocial: 296G (0.4%, 296G)
L2_Sentinel: 200G (0.3%, 19,895G)
ANFz: 200G (0.3%, 1,629G)
Jordache7K: 111G (0.1%, 21,296G)
ApplesauceBoss: 100G (0.1%, 6,769G)
Pochitchi: 100G (0.1%, 100G)
ko2q: 100G (0.1%, 2,147G)

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mirapoix
Monster
Sagittarius
51
78
Ghost










L2 Sentinel
Male
Virgo
37
49
Samurai
Battle Skill
Brave Up
Defense UP
Jump+3

Bizen Boat

Barbuta
Diamond Armor
Small Mantle

Asura, Koutetsu, Bizen Boat
Head Break, Armor Break, Magic Break, Mind Break



Lord Burrah
Monster
Sagittarius
77
62
Squidraken










HASTERIOUS
Male
Aquarius
74
43
Geomancer
Yin Yang Magic
Dragon Spirit
Short Charge
Move-MP Up

Giant Axe
Gold Shield
Twist Headband
Chain Vest
Wizard Mantle

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Gusty Wind
Doubt Faith, Zombie

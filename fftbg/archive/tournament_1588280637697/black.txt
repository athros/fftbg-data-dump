Player: !Black
Team: Black Team
Palettes: Black/Red



Pie108
Female
Scorpio
73
60
Thief
Yin Yang Magic
Sunken State
Equip Knife
Waterwalking

Dagger

Twist Headband
Chain Vest
Rubber Shoes

Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Blind, Poison, Life Drain, Dispel Magic, Sleep



Gelwain
Male
Aries
61
79
Oracle
Elemental
Critical Quick
Equip Axe
Waterwalking

Flail

Triangle Hat
Black Robe
Spike Shoes

Poison, Zombie, Silence Song, Confusion Song
Pitfall, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind



Volgrathemoose
Male
Aries
43
60
Ninja
Elemental
MA Save
Attack UP
Ignore Height

Hidden Knife
Short Edge
Twist Headband
Mythril Vest
Power Wrist

Knife
Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard



ZephyrTempest
Monster
Libra
59
79
Taiju








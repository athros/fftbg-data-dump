Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



Coralreeferz
Male
Libra
50
49
Geomancer
Sing
Absorb Used MP
Short Charge
Levitate

Slasher
Flame Shield
Headgear
Clothes
Genji Gauntlet

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Cheer Song, Last Song, Sky Demon



Galkife
Female
Gemini
41
52
Knight
Summon Magic
Meatbone Slash
Sicken
Jump+2

Slasher
Crystal Shield
Iron Helmet
Chameleon Robe
Magic Gauntlet

Armor Break, Shield Break, Magic Break, Power Break, Mind Break, Surging Sword
Moogle, Shiva, Carbunkle, Leviathan, Fairy



Troublesometree
Male
Sagittarius
47
38
Lancer
Charge
Earplug
Doublehand
Teleport

Spear

Platinum Helmet
Crystal Mail
Defense Ring

Level Jump2, Vertical Jump4
Charge+1, Charge+2, Charge+3, Charge+5, Charge+20



Pie108
Male
Aquarius
61
75
Squire
Draw Out
Mana Shield
Long Status
Move-MP Up

Flail
Platinum Shield
Thief Hat
Platinum Armor
Leather Mantle

Accumulate, Dash, Heal, Yell, Cheer Up, Wish
Koutetsu, Bizen Boat

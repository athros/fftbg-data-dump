Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Lokenwow
Male
Sagittarius
61
56
Mediator
Elemental
Parry
Martial Arts
Move+3



Cachusha
Adaman Vest
Feather Boots

Praise, Threaten, Preach, Death Sentence, Insult, Negotiate, Refute
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Quicksand, Blizzard



Lythe Caraker
Female
Libra
51
65
Mediator
Item
Distribute
Throw Item
Lava Walking

Bestiary

Golden Hairpin
Chameleon Robe
Elf Mantle

Invitation, Threaten, Insult, Negotiate, Refute, Rehabilitate
Potion, Hi-Ether, Antidote, Soft, Remedy, Phoenix Down



NovaKnight21
Female
Taurus
57
61
Time Mage
Steal
Counter Magic
Equip Shield
Jump+3

Oak Staff
Flame Shield
Green Beret
Judo Outfit
Battle Boots

Slow, Slow 2, Stop, Float, Stabilize Time
Steal Status, Arm Aim, Leg Aim



Zed
Male
Serpentarius
53
78
Bard
Throw
Faith Save
Magic Defense UP
Move-HP Up

Bloody Strings

Red Hood
Chain Vest
Magic Ring

Cheer Song, Battle Song
Shuriken, Staff

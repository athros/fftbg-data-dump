Player: !Green
Team: Green Team
Palettes: Green/White



Aldrammech
Female
Taurus
71
62
Priest
Throw
Distribute
Maintenance
Jump+1

Rainbow Staff

Red Hood
Wizard Robe
Angel Ring

Cure, Cure 2, Cure 4, Raise, Raise 2, Shell 2, Esuna, Holy
Shuriken, Staff, Ninja Sword



Cougboi
Male
Capricorn
78
45
Mime

Speed Save
Dual Wield
Move+3



Green Beret
Earth Clothes
N-Kai Armlet

Mimic




Rico Flex
Female
Virgo
61
70
Wizard
Steal
Caution
Long Status
Move+2

Blind Knife

Green Beret
Black Costume
Bracer

Fire, Fire 2, Bolt, Ice 3
Gil Taking, Steal Heart, Steal Armor, Steal Weapon, Steal Accessory, Steal Status



Caprinovoa
Male
Cancer
76
63
Mime

Arrow Guard
Long Status
Fly


Mythril Shield
Red Hood
Secret Clothes
Vanish Mantle

Mimic


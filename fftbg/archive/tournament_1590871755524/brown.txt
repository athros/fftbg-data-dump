Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Vorap
Female
Sagittarius
57
45
Geomancer
Punch Art
Speed Save
Equip Armor
Jump+1

Koutetsu Knife
Platinum Shield
Iron Helmet
Crystal Mail
Wizard Mantle

Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Pummel, Secret Fist, Purification, Revive



ZombiFrancis
Monster
Gemini
56
80
Archaic Demon










Kyune
Female
Scorpio
45
58
Ninja
White Magic
Caution
Equip Axe
Fly

Giant Axe
Battle Axe
Headgear
Clothes
N-Kai Armlet

Shuriken
Cure, Cure 4, Raise, Protect 2, Esuna



Zepharoth89
Female
Leo
76
50
Chemist
Elemental
Caution
Long Status
Move+1

Blaze Gun

Flash Hat
Power Sleeve
Battle Boots

Potion, Hi-Potion, Ether, Antidote, Phoenix Down
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind

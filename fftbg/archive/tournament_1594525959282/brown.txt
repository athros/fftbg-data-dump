Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Forkmore
Monster
Cancer
52
48
Archaic Demon










Pplvee1
Male
Leo
77
75
Geomancer
Draw Out
Blade Grasp
Concentrate
Fly

Asura Knife
Crystal Shield
Triangle Hat
Judo Outfit
Defense Armlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Muramasa



Thyrandaal
Male
Aquarius
73
62
Lancer
Sing
MA Save
Doublehand
Jump+1

Partisan

Crystal Helmet
Robe of Lords
108 Gems

Level Jump8, Vertical Jump8
Battle Song, Nameless Song, Hydra Pit



Dantayystv
Female
Capricorn
61
46
Time Mage
Charge
Counter Flood
Equip Bow
Ignore Terrain

Yoichi Bow

Red Hood
Adaman Vest
Rubber Shoes

Haste, Slow 2, Stop, Immobilize, Quick, Demi 2
Charge+1, Charge+2, Charge+3, Charge+7, Charge+20

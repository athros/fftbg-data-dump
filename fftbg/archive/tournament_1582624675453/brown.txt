Player: !Brown
Team: Brown Team
Palettes: Brown/Green



YaBoy125
Male
Pisces
76
45
Knight
Charge
MA Save
Sicken
Jump+1

Ice Brand
Flame Shield
Circlet
Gold Armor
N-Kai Armlet

Weapon Break, Justice Sword
Charge+2, Charge+4



Mirapoix
Female
Capricorn
43
76
Geomancer
Steal
Counter
Secret Hunt
Retreat

Muramasa
Bronze Shield
Ribbon
Chameleon Robe
Cursed Ring

Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Steal Heart, Steal Helmet, Steal Armor



The Pengwin
Male
Taurus
55
71
Wizard
Steal
HP Restore
Equip Shield
Jump+3

Mythril Knife
Flame Shield
Black Hood
Silk Robe
Elf Mantle

Fire 3, Bolt 3, Bolt 4, Ice 3, Ice 4, Death
Steal Helmet, Steal Weapon



Musclestache
Male
Pisces
65
42
Geomancer
Sing
PA Save
Equip Axe
Fly

Flail
Round Shield
Triangle Hat
Light Robe
Magic Gauntlet

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Quicksand, Sand Storm
Angel Song, Life Song, Diamond Blade, Hydra Pit

Player: !White
Team: White Team
Palettes: White/Blue



DocSmock
Female
Sagittarius
78
59
Knight
Talk Skill
Counter Tackle
Short Charge
Swim

Platinum Sword
Platinum Shield
Gold Helmet
Chameleon Robe
Reflect Ring

Weapon Break, Magic Break, Speed Break, Stasis Sword
Solution, Insult, Mimic Daravon, Refute



Bigshowens
Male
Cancer
54
69
Time Mage
Yin Yang Magic
Distribute
Short Status
Move+1

Rainbow Staff

Barette
Light Robe
Rubber Shoes

Slow, Stop, Immobilize, Demi, Demi 2, Stabilize Time
Poison, Life Drain, Pray Faith, Doubt Faith, Blind Rage, Confusion Song, Paralyze



Sun Pixie
Female
Libra
53
62
Summoner
Punch Art
Distribute
Short Charge
Move+3

Ice Rod

Triangle Hat
Silk Robe
Red Shoes

Moogle, Shiva, Ramuh, Golem, Carbunkle, Odin, Salamander, Silf, Fairy, Cyclops
Purification, Chakra, Revive, Seal Evil



Jarekx123
Male
Cancer
70
44
Summoner
Punch Art
Absorb Used MP
Short Charge
Lava Walking

Gold Staff

Triangle Hat
Black Costume
108 Gems

Moogle, Ramuh, Ifrit, Titan, Carbunkle, Fairy
Pummel, Wave Fist, Secret Fist, Purification, Revive

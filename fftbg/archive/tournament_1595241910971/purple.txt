Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Superdevon1
Female
Capricorn
59
74
Geomancer
Draw Out
Caution
Equip Polearm
Retreat

Ryozan Silk
Mythril Shield
Feather Hat
Light Robe
Rubber Shoes

Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Koutetsu, Bizen Boat, Kikuichimoji, Chirijiraden



Hasterious
Male
Capricorn
64
64
Bard
Item
Blade Grasp
Sicken
Waterbreathing

Bloody Strings

Twist Headband
Plate Mail
Cursed Ring

Angel Song, Last Song, Diamond Blade, Sky Demon
Potion, Hi-Potion, Soft, Remedy, Phoenix Down



Maddrave09
Female
Capricorn
43
41
Calculator
Black Magic
MP Restore
Equip Armor
Move+1

Iron Fan

Barbuta
Plate Mail
Battle Boots

CT, 5
Ice 2



ForagerCats
Male
Pisces
42
45
Knight
Sing
Sunken State
Short Charge
Ignore Terrain

Save the Queen
Mythril Shield
Cross Helmet
White Robe
Angel Ring

Head Break, Armor Break, Shield Break, Weapon Break, Power Break, Justice Sword
Angel Song, Life Song, Last Song, Sky Demon

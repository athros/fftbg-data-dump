Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



BuffaloCrunch
Male
Aries
76
56
Samurai
White Magic
Counter
Equip Axe
Teleport

Healing Staff

Mythril Helmet
Linen Cuirass
Sprint Shoes

Koutetsu, Bizen Boat, Kiyomori, Muramasa
Cure 4, Raise, Protect, Shell 2, Esuna



Breakdown777
Male
Aquarius
75
79
Monk
Throw
Brave Up
Attack UP
Waterbreathing



Flash Hat
Chain Vest
Battle Boots

Spin Fist, Earth Slash, Revive
Sword



Rislyeu
Male
Taurus
79
74
Mime

Counter Tackle
Defense UP
Move+2



Golden Hairpin
Clothes
Battle Boots

Mimic




EnemyController
Female
Virgo
80
74
Time Mage
Steal
PA Save
Beastmaster
Move+2

Oak Staff

Ribbon
Black Costume
Defense Ring

Stop, Float, Quick, Demi, Stabilize Time
Gil Taking, Steal Heart, Steal Shield, Steal Weapon, Steal Accessory

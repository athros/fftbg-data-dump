Player: !Green
Team: Green Team
Palettes: Green/White



Evontno
Male
Taurus
69
58
Thief
Punch Art
HP Restore
Magic Attack UP
Jump+1

Mage Masher

Red Hood
Earth Clothes
N-Kai Armlet

Steal Heart
Pummel, Earth Slash, Purification, Chakra, Revive



Baron Von Scrub
Male
Aquarius
65
76
Ninja
Time Magic
Parry
Attack UP
Teleport

Kunai
Sasuke Knife
Headgear
Chain Vest
Chantage

Shuriken
Haste, Haste 2, Slow, Slow 2, Stop, Meteor



Fenaen
Male
Scorpio
41
52
Monk
Summon Magic
Parry
Attack UP
Move-HP Up



Headgear
Chain Vest
Feather Boots

Spin Fist, Wave Fist, Seal Evil
Moogle, Ramuh, Ifrit, Golem, Carbunkle, Silf



Cupholderr
Male
Aquarius
48
68
Samurai
Charge
Faith Up
Beastmaster
Teleport

Murasame

Leather Helmet
Platinum Armor
Cursed Ring

Bizen Boat, Murasame, Heaven's Cloud, Kiyomori
Charge+20

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Thyrandaal
Male
Taurus
64
64
Monk
Draw Out
Earplug
Equip Sword
Ignore Height

Iron Sword

Twist Headband
Black Costume
Bracer

Spin Fist, Wave Fist, Purification, Revive, Seal Evil
Koutetsu, Heaven's Cloud



Gunz232323
Male
Libra
60
77
Lancer
Draw Out
Earplug
Equip Gun
Fly

Stone Gun
Escutcheon
Leather Helmet
Carabini Mail
Feather Mantle

Level Jump8, Vertical Jump8
Koutetsu, Heaven's Cloud



G1nger4le
Female
Sagittarius
76
55
Lancer
White Magic
Counter Flood
Concentrate
Swim

Spear
Diamond Shield
Bronze Helmet
Genji Armor
Battle Boots

Level Jump4, Vertical Jump7
Cure 3, Raise, Raise 2, Shell 2, Esuna



DesertWooder
Female
Scorpio
43
48
Dancer
Elemental
Meatbone Slash
Short Charge
Jump+1

Main Gauche

Holy Miter
Wizard Outfit
Genji Gauntlet

Witch Hunt, Last Dance, Nether Demon, Dragon Pit
Pitfall, Water Ball, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

Final Bets: white - 12 bets for 5,633G (20.2%, x3.95); champion - 21 bets for 22,263G (79.8%, x0.25)

white bets:
latebit: 1,400G (24.9%, 36,072G)
BirbBrainsBot: 1,000G (17.8%, 49,523G)
reinoe: 1,000G (17.8%, 80,059G)
killth3kid: 620G (11.0%, 10,587G)
AllInBot: 395G (7.0%, 395G)
sinnyil2: 350G (6.2%, 3,515G)
LordTomS: 200G (3.6%, 5,637G)
ZCKaiser: 200G (3.6%, 10,891G)
getthemoneyz: 156G (2.8%, 1,516,400G)
JonnyCue: 112G (2.0%, 112G)
Miku_Shikhu: 100G (1.8%, 2,010G)
E_Ballard: 100G (1.8%, 2,986G)

champion bets:
SephDarkheart: 4,981G (22.4%, 9,962G)
gooblz: 3,000G (13.5%, 11,218G)
DeathTaxesAndAnime: 2,266G (10.2%, 4,444G)
maximumcrit: 2,000G (9.0%, 13,726G)
TasisSai: 2,000G (9.0%, 13,447G)
loveyouallfriends: 1,552G (7.0%, 31,552G)
powergems: 1,416G (6.4%, 1,416G)
Shalloween: 1,200G (5.4%, 49,572G)
JumbocactuarX27: 1,000G (4.5%, 41,904G)
Nizaha: 668G (3.0%, 21,569G)
DLJuggernaut: 521G (2.3%, 1,023G)
VolgraTheMoose: 501G (2.3%, 4,343G)
Error72: 416G (1.9%, 416G)
ArlanKels: 126G (0.6%, 6,333G)
ohhinm: 123G (0.6%, 3,935G)
Gunz232323: 108G (0.5%, 108G)
gorgewall: 101G (0.5%, 2,208G)
nifboy: 100G (0.4%, 2,317G)
CosmicTactician: 100G (0.4%, 5,654G)
dtrain332: 83G (0.4%, 8,393G)
DarrenDinosaurs: 1G (0.0%, 5,243G)

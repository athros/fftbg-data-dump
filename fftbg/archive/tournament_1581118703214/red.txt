Player: !Red
Team: Red Team
Palettes: Red/Brown



Yowakusuru
Male
Taurus
48
62
Ninja
Talk Skill
MP Restore
Equip Armor
Move+1

Spell Edge
Short Edge
Circlet
Linen Cuirass
Magic Gauntlet

Shuriken, Knife
Invitation, Negotiate, Refute, Rehabilitate



KitchTowel
Female
Taurus
72
62
Calculator
Black Magic
Abandon
Beastmaster
Waterwalking

Papyrus Codex

Headgear
Silk Robe
Red Shoes

CT, Prime Number, 3
Bolt 3, Bolt 4, Ice 2



Error72
Monster
Virgo
48
75
Ultima Demon










Barrbear
Male
Virgo
66
54
Archer
Throw
Caution
Beastmaster
Move+2

Mythril Gun
Platinum Shield
Holy Miter
Adaman Vest
Jade Armlet

Charge+1, Charge+5
Shuriken, Bomb, Dictionary

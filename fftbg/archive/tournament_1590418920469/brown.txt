Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Fluffywormhole
Male
Sagittarius
43
41
Chemist
Punch Art
Auto Potion
Maintenance
Jump+3

Mythril Gun

Golden Hairpin
Judo Outfit
Diamond Armlet

Potion, Hi-Potion, X-Potion, Ether, Antidote, Echo Grass, Holy Water, Remedy
Pummel, Purification, Chakra, Revive



Victoriolue
Female
Taurus
77
78
Summoner
Yin Yang Magic
Catch
Short Charge
Fly

White Staff

Twist Headband
Black Robe
Feather Boots

Ramuh, Golem, Salamander, Silf, Lich
Poison, Spell Absorb, Life Drain, Doubt Faith, Paralyze



HaychDub
Male
Leo
49
58
Knight
Throw
Speed Save
Equip Axe
Waterbreathing

Morning Star
Crystal Shield
Genji Helmet
Linen Cuirass
Magic Gauntlet

Head Break, Weapon Break, Power Break, Mind Break, Stasis Sword, Justice Sword
Hammer



Cloud92684
Female
Sagittarius
55
74
Mediator
Item
Earplug
Equip Armor
Retreat

Blaze Gun

Twist Headband
Linen Cuirass
Rubber Shoes

Invitation, Praise, Solution, Death Sentence, Negotiate, Refute
Potion, Hi-Potion, Ether, Hi-Ether, Eye Drop, Phoenix Down

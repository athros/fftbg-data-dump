Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



DustBirdEX
Female
Aries
76
51
Archer
Yin Yang Magic
Sunken State
Halve MP
Teleport

Long Bow

Golden Hairpin
Earth Clothes
Rubber Shoes

Charge+1, Charge+3, Charge+4
Blind, Poison, Confusion Song



Flowinprose
Female
Taurus
49
41
Knight
White Magic
Meatbone Slash
Equip Sword
Jump+2

Koutetsu Knife
Aegis Shield
Gold Helmet
Bronze Armor
Power Wrist

Head Break, Armor Break, Shield Break
Regen, Protect 2, Esuna



Rabbitlogik
Male
Scorpio
49
41
Squire
Item
Catch
Short Status
Jump+1

Slasher
Flame Shield
Bronze Helmet
Chain Mail
Feather Boots

Accumulate, Heal, Yell
Potion, X-Potion, Antidote, Echo Grass, Soft, Holy Water, Phoenix Down



Baron Von Scrub
Male
Leo
46
69
Bard
Yin Yang Magic
MP Restore
Equip Armor
Ignore Terrain

Long Bow

Iron Helmet
Chameleon Robe
Magic Gauntlet

Magic Song, Diamond Blade, Sky Demon, Hydra Pit
Poison, Life Drain, Doubt Faith, Foxbird, Paralyze, Dark Holy

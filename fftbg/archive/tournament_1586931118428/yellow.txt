Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Egil72
Male
Cancer
43
67
Priest
Summon Magic
Arrow Guard
Maintenance
Jump+1

Flail

Feather Hat
Light Robe
108 Gems

Raise, Raise 2, Regen, Protect 2, Shell, Shell 2, Esuna
Moogle, Shiva, Ramuh, Golem, Carbunkle



Lordminsc
Female
Leo
47
65
Wizard
Charge
Abandon
Defense UP
Move-HP Up

Thunder Rod

Golden Hairpin
Judo Outfit
Power Wrist

Fire 3, Bolt 3, Bolt 4, Ice 2, Empower, Flare
Charge+7



Realitydown
Monster
Aries
57
47
Iron Hawk










Evewho
Female
Cancer
69
44
Knight
Charge
Auto Potion
Equip Axe
Teleport

Giant Axe
Round Shield
Genji Helmet
Chain Mail
Feather Mantle

Weapon Break, Power Break, Mind Break, Dark Sword
Charge+4, Charge+7

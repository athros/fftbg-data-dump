Final Bets: purple - 9 bets for 2,789G (7.6%, x12.13); brown - 10 bets for 33,831G (92.4%, x0.08)

purple bets:
BirbBrainsBot: 1,000G (35.9%, 193,884G)
thevilmen: 500G (17.9%, 7,248G)
getthemoneyz: 376G (13.5%, 501,704G)
HASTERIOUS: 364G (13.1%, 364G)
gloomdog: 200G (7.2%, 1,115G)
Tithonus: 100G (3.6%, 7,560G)
Arkreaver: 100G (3.6%, 1,349G)
gorgewall: 99G (3.5%, 3,199G)
potgodtopdog: 50G (1.8%, 4,743G)

brown bets:
Lydian_C: 15,000G (44.3%, 57,662G)
sinnyil2: 5,555G (16.4%, 24,395G)
Jerboj: 5,000G (14.8%, 47,715G)
HaplessOne: 5,000G (14.8%, 83,237G)
DeathTaxesAndAnime: 1,236G (3.7%, 1,236G)
Phytik: 751G (2.2%, 7,911G)
Treapvort: 500G (1.5%, 2,661G)
ungabunga_bot: 482G (1.4%, 149,225G)
datadrivenbot: 157G (0.5%, 160G)
Slider1012: 150G (0.4%, 150G)

Player: !Black
Team: Black Team
Palettes: Black/Red



Lastly
Female
Aquarius
75
65
Squire
Steal
Faith Save
Long Status
Retreat

Ancient Sword
Gold Shield
Feather Hat
Mythril Vest
Small Mantle

Accumulate, Heal, Fury, Wish
Gil Taking, Steal Helmet, Steal Shield, Steal Status, Arm Aim, Leg Aim



Douchetron
Female
Leo
45
58
Oracle
Time Magic
Faith Save
Secret Hunt
Ignore Height

Iron Fan

Black Hood
Wizard Robe
Spike Shoes

Dispel Magic, Sleep
Haste 2, Slow, Slow 2, Stop, Immobilize, Stabilize Time, Meteor



ForagerCats
Male
Pisces
68
40
Chemist
Throw
Speed Save
Equip Bow
Move-MP Up

Poison Bow

Golden Hairpin
Clothes
Power Wrist

Potion, Hi-Potion, X-Potion, Soft, Remedy, Phoenix Down
Shuriken, Hammer, Spear



Ar Tactic
Female
Cancer
66
66
Dancer
Talk Skill
MP Restore
Short Status
Retreat

Ryozan Silk

Holy Miter
White Robe
Defense Armlet

Witch Hunt, Last Dance
Invitation, Threaten, Insult, Mimic Daravon, Refute, Rehabilitate

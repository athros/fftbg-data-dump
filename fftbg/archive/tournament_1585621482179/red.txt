Player: !Red
Team: Red Team
Palettes: Red/Brown



RunicMagus
Monster
Aries
59
48
Archaic Demon










Aeolus 000
Female
Aries
72
46
Priest
Talk Skill
Absorb Used MP
Beastmaster
Swim

Rainbow Staff

Thief Hat
Judo Outfit
Reflect Ring

Cure 3, Raise, Raise 2, Reraise, Protect, Wall, Esuna
Invitation, Praise, Death Sentence, Negotiate



Etce
Female
Scorpio
80
45
Time Mage
White Magic
Catch
Short Status
Swim

Papyrus Codex

Leather Hat
Light Robe
Dracula Mantle

Haste, Slow, Slow 2, Immobilize, Demi 2, Stabilize Time, Meteor
Regen, Shell, Shell 2, Esuna



Babieapple
Male
Pisces
58
42
Knight
Talk Skill
Regenerator
Monster Talk
Move+1

Defender
Ice Shield
Iron Helmet
Black Robe
Diamond Armlet

Head Break, Speed Break, Justice Sword
Invitation, Persuade, Preach, Solution, Negotiate, Refute, Rehabilitate

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



PoroTact
Female
Taurus
45
55
Mediator
Summon Magic
Critical Quick
Maintenance
Move-HP Up

Battle Folio

Flash Hat
Wizard Robe
Defense Armlet

Threaten, Preach, Mimic Daravon, Refute
Moogle, Titan, Odin, Salamander, Silf, Cyclops



RageImmortaI
Male
Taurus
50
54
Knight
Yin Yang Magic
Parry
Equip Sword
Jump+3

Chaos Blade
Mythril Shield
Cross Helmet
Linen Robe
Feather Boots

Head Break, Armor Break, Speed Break, Stasis Sword
Blind, Foxbird, Confusion Song



TasisSai
Female
Aries
60
59
Samurai
Yin Yang Magic
Catch
Equip Knife
Ignore Height

Thunder Rod

Circlet
Diamond Armor
Spike Shoes

Asura, Koutetsu, Murasame, Heaven's Cloud, Kiyomori, Muramasa
Poison, Life Drain, Blind Rage, Sleep, Dark Holy



Nizaha
Female
Aquarius
47
48
Thief
Draw Out
Counter
Equip Armor
Lava Walking

Cultist Dagger

Green Beret
Crystal Mail
Sprint Shoes

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Status, Arm Aim
Koutetsu, Murasame

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



DamnThatShark
Male
Cancer
39
51
Monk
Throw
Auto Potion
Secret Hunt
Jump+3



Green Beret
Black Costume
Red Shoes

Pummel, Earth Slash, Purification, Revive, Seal Evil
Hammer, Staff, Spear, Dictionary



Legionx13
Female
Libra
74
46
Oracle
Elemental
Abandon
Martial Arts
Move+1



Cachusha
Earth Clothes
Salty Rage

Spell Absorb, Life Drain, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify
Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Gusty Wind, Lava Ball



Powergems
Female
Aries
75
65
Archer
Punch Art
Counter Tackle
Doublehand
Move-HP Up

Ultimus Bow

Barette
Leather Outfit
Defense Armlet

Charge+1, Charge+5, Charge+10
Spin Fist, Wave Fist, Purification



Tapemeasure
Male
Cancer
52
54
Archer
Jump
PA Save
Maintenance
Move+3

Poison Bow
Round Shield
Crystal Helmet
Black Costume
Dracula Mantle

Charge+2, Charge+3, Charge+4
Level Jump3, Vertical Jump8

Player: !Red
Team: Red Team
Palettes: Red/Brown



Seaweed B
Male
Capricorn
78
74
Mediator
Black Magic
Arrow Guard
Equip Knife
Ignore Terrain

Cultist Dagger

Golden Hairpin
Chameleon Robe
Feather Boots

Persuade, Death Sentence, Insult
Fire, Fire 2, Fire 3, Bolt, Death



Prince Rogers Nelson
Female
Scorpio
46
62
Calculator
Demon Skill
Parry
Equip Polearm
Jump+1

Spear
Ice Shield
Iron Helmet
Rubber Costume
Defense Armlet

Blue Magic
Nanoflare, Dark Holy, Lifebreak, Empower, Despair, Despair 2



Sinnyil2
Male
Taurus
53
40
Bard
Summon Magic
Damage Split
Doublehand
Lava Walking

Ice Bow

Flash Hat
Linen Cuirass
Magic Gauntlet

Life Song, Cheer Song, Space Storage
Moogle, Ifrit, Titan, Carbunkle, Bahamut, Leviathan, Salamander



Gorgewall
Male
Aries
60
45
Lancer
Yin Yang Magic
Catch
Doublehand
Move+3

Iron Fan

Barbuta
Silk Robe
Cursed Ring

Level Jump4, Vertical Jump7
Poison, Spell Absorb, Pray Faith, Zombie, Blind Rage, Paralyze

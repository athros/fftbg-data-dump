Player: !zChamp
Team: Champion Team
Palettes: Yellow/Black



Dogsandcatsand
Male
Pisces
46
72
Archer
Punch Art
Distribute
Doublehand
Ignore Terrain

Lightning Bow

Flash Hat
Power Sleeve
Rubber Shoes

Charge+1
Revive, Seal Evil



Kronikle
Male
Virgo
74
72
Thief
Throw
MP Restore
Equip Gun
Ignore Height

Mythril Gun

Cachusha
Earth Clothes
Cherche

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Steal Status, Arm Aim, Leg Aim
Shuriken



Omegasuspekt
Male
Cancer
38
73
Calculator
Lucavi Skill
Absorb Used MP
Equip Gun
Move+2

Ramia Harp

Barette
Platinum Armor
Genji Gauntlet

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



Legitimized
Female
Aquarius
64
57
Summoner
White Magic
Caution
Short Charge
Move-HP Up

Rod

Triangle Hat
Light Robe
Sprint Shoes

Moogle, Ifrit, Golem, Leviathan
Cure, Cure 3, Raise, Shell 2, Esuna, Holy

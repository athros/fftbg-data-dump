Player: !White
Team: White Team
Palettes: White/Blue



Kaidykat
Female
Gemini
54
52
Wizard
Item
Caution
Equip Bow
Ignore Terrain

Silver Bow

Green Beret
Wizard Outfit
Feather Boots

Fire, Fire 3, Bolt, Bolt 3, Death
Hi-Potion, Phoenix Down



Nifboy
Monster
Leo
49
49
Wyvern










Galkife
Male
Aries
46
48
Ninja
Punch Art
Counter Tackle
Short Charge
Ignore Height

Iga Knife
Blind Knife
Cachusha
Chain Vest
Reflect Ring

Shuriken, Staff
Pummel, Wave Fist, Earth Slash, Revive, Seal Evil



TasisSai
Female
Sagittarius
67
79
Mediator
Item
Counter Flood
Equip Gun
Waterwalking

Fairy Harp

Flash Hat
White Robe
Jade Armlet

Persuade, Praise, Threaten, Preach, Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Eye Drop, Echo Grass, Soft

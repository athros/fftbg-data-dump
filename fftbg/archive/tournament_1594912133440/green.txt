Player: !Green
Team: Green Team
Palettes: Green/White



Killth3kid
Monster
Scorpio
52
55
Chocobo










Fenaen
Female
Pisces
42
62
Samurai
Yin Yang Magic
MA Save
Doublehand
Move+2

Koutetsu Knife

Circlet
Light Robe
Vanish Mantle

Asura, Bizen Boat
Blind, Spell Absorb, Life Drain, Doubt Faith, Silence Song, Dispel Magic, Paralyze, Sleep



DouglasDragonThePoet
Male
Scorpio
80
80
Geomancer
Sing
Dragon Spirit
Magic Attack UP
Move+2

Asura Knife
Flame Shield
Green Beret
Silk Robe
Power Wrist

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Blizzard, Gusty Wind, Lava Ball
Life Song, Battle Song, Magic Song, Space Storage



Thyrandaal
Male
Capricorn
45
47
Archer
Talk Skill
Sunken State
Doublehand
Jump+2

Poison Bow

Headgear
Wizard Outfit
Elf Mantle

Charge+1, Charge+3, Charge+4, Charge+7, Charge+20
Persuade, Praise, Threaten, Refute

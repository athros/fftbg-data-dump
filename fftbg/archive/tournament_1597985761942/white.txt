Player: !White
Team: White Team
Palettes: White/Blue



Nhammen
Male
Pisces
41
60
Bard
Throw
Faith Save
Equip Shield
Jump+1

Ramia Harp
Hero Shield
Green Beret
Leather Armor
Reflect Ring

Life Song, Cheer Song, Last Song, Space Storage
Sword, Dictionary



IBardic
Female
Aquarius
69
57
Mediator
Elemental
Counter Magic
Magic Attack UP
Jump+1

Cute Bag

Triangle Hat
Linen Robe
Wizard Mantle

Invitation, Preach, Death Sentence, Negotiate, Refute, Rehabilitate
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind



Milk Tea9919
Male
Cancer
63
73
Geomancer
Yin Yang Magic
Meatbone Slash
Equip Axe
Ignore Height

Gold Staff
Buckler
Green Beret
Earth Clothes
Defense Ring

Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Blizzard, Gusty Wind
Blind, Poison, Spell Absorb, Pray Faith, Zombie, Confusion Song, Paralyze



Lowlf
Male
Scorpio
44
42
Oracle
Talk Skill
Counter Magic
Dual Wield
Jump+3

Iron Fan
Battle Bamboo
Headgear
Robe of Lords
Defense Ring

Blind, Spell Absorb, Life Drain, Silence Song, Foxbird, Confusion Song, Dispel Magic, Paralyze
Invitation, Persuade, Threaten, Insult

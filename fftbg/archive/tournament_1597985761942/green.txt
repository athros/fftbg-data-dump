Player: !Green
Team: Green Team
Palettes: Green/White



Sinnyil2
Female
Aries
61
40
Chemist
Summon Magic
Damage Split
Equip Sword
Levitate

Mythril Sword

Leather Hat
Chain Vest
Spike Shoes

Hi-Potion, Maiden's Kiss, Remedy
Moogle, Shiva, Carbunkle, Silf



Chuckolator
Female
Aquarius
79
72
Knight
Elemental
Abandon
Long Status
Jump+3

Diamond Sword
Round Shield
Barbuta
Genji Armor
Power Wrist

Armor Break, Shield Break, Stasis Sword, Justice Sword, Dark Sword
Hallowed Ground, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball



Fattunaking
Male
Aries
45
57
Monk
Item
Hamedo
Sicken
Move-MP Up



Triangle Hat
Power Sleeve
Leather Mantle

Wave Fist, Purification, Revive, Seal Evil
Hi-Potion, Ether, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Holy Water, Remedy, Phoenix Down



Galkife
Female
Leo
60
63
Oracle
Draw Out
Mana Shield
Attack UP
Waterwalking

Iron Fan

Barette
Clothes
Magic Ring

Blind, Poison, Life Drain, Pray Faith, Dispel Magic, Dark Holy
Koutetsu

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Friendsquirrel
Male
Gemini
67
44
Priest
Charge
Meatbone Slash
Short Charge
Move+1

Morning Star

Thief Hat
Brigandine
Leather Mantle

Cure, Cure 2, Regen, Protect, Shell 2, Esuna
Charge+2, Charge+3, Charge+5



Rogueain
Female
Sagittarius
78
71
Time Mage
Talk Skill
Absorb Used MP
Monster Talk
Fly

Rainbow Staff

Holy Miter
Chameleon Robe
Angel Ring

Haste, Slow, Reflect, Quick, Demi 2, Stabilize Time
Insult, Refute



Coralreeferz
Female
Gemini
69
42
Monk
Jump
MA Save
Magic Attack UP
Ignore Height



Green Beret
Leather Outfit
Genji Gauntlet

Pummel, Purification, Revive
Level Jump2, Vertical Jump8



Roofiepops
Female
Sagittarius
63
79
Monk
Dance
Critical Quick
Magic Defense UP
Jump+3



Red Hood
Mythril Vest
Magic Gauntlet

Wave Fist, Purification, Chakra, Revive
Wiznaibus, Last Dance

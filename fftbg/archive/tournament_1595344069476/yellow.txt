Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Gooseyourself
Female
Taurus
43
59
Chemist
Draw Out
Damage Split
Equip Sword
Jump+1

Kikuichimoji

Golden Hairpin
Chain Vest
Feather Mantle

Hi-Potion, X-Potion, Ether, Antidote, Eye Drop, Holy Water, Phoenix Down
Murasame, Heaven's Cloud, Muramasa



Pplvee1
Male
Leo
55
68
Ninja
Basic Skill
Meatbone Slash
Attack UP
Retreat

Morning Star
Air Knife
Ribbon
Secret Clothes
Wizard Mantle

Shuriken, Bomb
Accumulate, Heal, Tickle, Wish, Scream



Fenaen
Male
Scorpio
57
54
Chemist
Jump
Counter Magic
Equip Armor
Jump+2

Stone Gun

Leather Helmet
Crystal Mail
Wizard Mantle

Potion, Hi-Potion, X-Potion, Antidote, Echo Grass, Remedy, Phoenix Down
Level Jump3, Vertical Jump6



RaIshtar
Male
Taurus
54
80
Monk
Steal
Counter Tackle
Dual Wield
Move+1



Black Hood
Chain Vest
Dracula Mantle

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Weapon, Steal Accessory, Leg Aim

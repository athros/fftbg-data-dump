Player: !Brown
Team: Brown Team
Palettes: Brown/Green



BlastedAmerican
Male
Gemini
69
79
Squire
Time Magic
Brave Save
Doublehand
Lava Walking

Hydra Bag

Red Hood
Diamond Armor
Red Shoes

Dash, Heal, Wish, Scream
Haste, Haste 2, Stop, Demi, Demi 2, Stabilize Time



LDSkinny
Female
Leo
53
68
Oracle
Summon Magic
Catch
Dual Wield
Move+2

Bestiary
Papyrus Codex
Green Beret
Black Robe
Feather Boots

Blind, Pray Faith, Zombie, Confusion Song, Dispel Magic, Paralyze
Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Leviathan



Cryptopsy70
Female
Scorpio
66
68
Dancer
Steal
Damage Split
Equip Sword
Move-MP Up

Koutetsu Knife

Headgear
Silk Robe
Chantage

Polka Polka, Disillusion, Nameless Dance, Void Storage, Dragon Pit
Steal Armor, Steal Weapon, Arm Aim



Practice Pad
Male
Scorpio
72
44
Ninja
Talk Skill
MP Restore
Equip Bow
Ignore Terrain

Windslash Bow

Black Hood
Earth Clothes
Small Mantle

Shuriken
Invitation, Praise, Threaten, Solution, Negotiate

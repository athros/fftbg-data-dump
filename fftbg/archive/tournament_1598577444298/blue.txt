Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Shalloween
Female
Pisces
76
60
Oracle
Steal
Abandon
Doublehand
Move-HP Up

Bestiary

Green Beret
Chameleon Robe
Red Shoes

Blind, Blind Rage, Foxbird, Dispel Magic, Paralyze, Petrify
Steal Helmet, Steal Status



Phi Sig
Female
Aquarius
41
63
Time Mage
Draw Out
Catch
Doublehand
Waterwalking

Gold Staff

Holy Miter
Wizard Robe
Diamond Armlet

Stop, Reflect, Demi, Stabilize Time
Asura, Koutetsu, Bizen Boat, Kiyomori, Muramasa



Powergems
Male
Sagittarius
76
47
Summoner
Black Magic
Parry
Long Status
Waterbreathing

Papyrus Codex

Headgear
Clothes
Angel Ring

Moogle, Ramuh, Leviathan
Fire 2, Fire 4, Ice 4, Empower, Frog



Wrath189
Male
Libra
58
57
Summoner
Talk Skill
Arrow Guard
Monster Talk
Fly

Thunder Rod

Feather Hat
White Robe
Defense Ring

Moogle, Titan, Leviathan, Fairy
Persuade, Solution, Rehabilitate

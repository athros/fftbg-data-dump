Player: !Green
Team: Green Team
Palettes: Green/White



Belivalan
Female
Virgo
74
42
Oracle
Draw Out
Catch
Attack UP
Levitate

Whale Whisker

Flash Hat
Light Robe
Red Shoes

Life Drain, Silence Song, Foxbird, Paralyze
Kiyomori



TheMM42
Male
Capricorn
54
42
Calculator
White Magic
Sunken State
Concentrate
Levitate

Battle Folio

Golden Hairpin
Earth Clothes
N-Kai Armlet

CT, Prime Number, 5
Raise, Regen, Protect, Wall



Aneyus
Female
Virgo
47
39
Squire
Black Magic
Auto Potion
Equip Armor
Ignore Terrain

Flail
Buckler
Black Hood
Light Robe
Dracula Mantle

Accumulate, Dash, Throw Stone, Heal
Fire, Bolt, Ice 3



Arch8000
Female
Scorpio
42
44
Lancer
Item
Damage Split
Throw Item
Fly

Musk Rod
Genji Shield
Barbuta
Black Robe
Rubber Shoes

Level Jump4, Vertical Jump7
Potion, X-Potion, Echo Grass, Soft, Remedy, Phoenix Down

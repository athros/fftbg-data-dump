Player: !zChamp
Team: Champion Team
Palettes: Blue/Purple



BoneMiser
Male
Cancer
37
45
Monk
Elemental
Hamedo
Short Status
Levitate



Holy Miter
Brigandine
Cursed Ring

Earth Slash, Secret Fist, Revive
Pitfall, Water Ball, Hell Ivy, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



Panushenko
Male
Cancer
61
74
Knight
Time Magic
Auto Potion
Short Charge
Swim

Rune Blade
Gold Shield
Circlet
Mythril Armor
Dracula Mantle

Armor Break, Weapon Break, Mind Break, Night Sword
Haste, Haste 2, Float, Quick, Demi 2, Stabilize Time, Meteor



Writingsins
Female
Aries
50
56
Ninja
Time Magic
Meatbone Slash
Maintenance
Lava Walking

Mage Masher
Blind Knife
Thief Hat
Power Sleeve
Cursed Ring

Axe, Stick
Haste 2, Immobilize, Demi, Demi 2, Stabilize Time, Galaxy Stop



Lodrak
Male
Pisces
76
61
Bard
Elemental
Sunken State
Doublehand
Ignore Height

Ramia Harp

Flash Hat
Power Sleeve
Dracula Mantle

Diamond Blade, Space Storage
Pitfall, Water Ball, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind

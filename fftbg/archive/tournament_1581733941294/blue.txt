Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Kahlindra
Female
Aquarius
44
58
Knight
Summon Magic
Earplug
Short Charge
Levitate

Slasher
Mythril Shield
Barbuta
Reflect Mail
Salty Rage

Head Break, Shield Break, Magic Break, Justice Sword, Surging Sword
Ramuh, Silf, Lich



Chocobomog123
Male
Capricorn
51
65
Squire
Item
MA Save
Throw Item
Jump+1

Zorlin Shape

Barbuta
Earth Clothes
Small Mantle

Heal, Yell
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Phoenix Down



Vivithegr8
Female
Capricorn
49
41
Monk
Summon Magic
Sunken State
Equip Armor
Move-MP Up



Green Beret
Black Robe
Rubber Shoes

Pummel, Secret Fist, Purification, Revive
Moogle, Titan, Carbunkle, Odin, Silf, Lich, Cyclops



Novapuppets
Female
Capricorn
56
80
Lancer
Talk Skill
Sunken State
Beastmaster
Move-HP Up

Battle Bamboo
Buckler
Genji Helmet
Reflect Mail
Reflect Ring

Level Jump4, Vertical Jump5
Praise, Preach, Solution, Mimic Daravon, Refute

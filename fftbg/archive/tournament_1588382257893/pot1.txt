Final Bets: red - 15 bets for 10,700G (42.2%, x1.37); blue - 21 bets for 14,658G (57.8%, x0.73)

red bets:
gooseyourself: 2,000G (18.7%, 16,360G)
Shalloween: 1,872G (17.5%, 3,672G)
superdevon1: 1,318G (12.3%, 1,318G)
Aldrammech: 1,000G (9.3%, 7,497G)
SkylerBunny: 1,000G (9.3%, 39,418G)
twelfthrootoftwo: 718G (6.7%, 718G)
DeathTaxesAndAnime: 656G (6.1%, 656G)
SarrgeQc: 501G (4.7%, 1,685G)
CapnChaos12: 500G (4.7%, 49,256G)
astrozin11: 485G (4.5%, 485G)
Estan_AD: 250G (2.3%, 4,330G)
Lali_Lulelo: 100G (0.9%, 34,672G)
ANFz: 100G (0.9%, 26,439G)
datadrivenbot: 100G (0.9%, 11,249G)
Lythe_Caraker: 100G (0.9%, 117,664G)

blue bets:
onetrickwolf: 3,000G (20.5%, 51,209G)
killth3kid: 2,000G (13.6%, 56,763G)
SolarisFall: 1,527G (10.4%, 1,527G)
volgrathemoose: 1,001G (6.8%, 9,873G)
rjA0zcOQ96: 1,000G (6.8%, 26,694G)
ungabunga_bot: 1,000G (6.8%, 294,546G)
BirbBrainsBot: 1,000G (6.8%, 171,214G)
Lionhermit: 1,000G (6.8%, 29,181G)
Dymntd: 500G (3.4%, 41,283G)
Kronikle: 420G (2.9%, 420G)
DustBirdEX: 321G (2.2%, 1,313G)
Tithonus: 308G (2.1%, 308G)
Mullet_Knight: 300G (2.0%, 6,761G)
Master_Metroid: 279G (1.9%, 279G)
cgMcWhiskers: 252G (1.7%, 252G)
DaMadPanda182: 200G (1.4%, 21,310G)
KasugaiRoastedPeas: 200G (1.4%, 3,383G)
nifboy: 100G (0.7%, 5,076G)
maakur_: 100G (0.7%, 117,604G)
sphintus_koyote: 100G (0.7%, 9,455G)
b0shii: 50G (0.3%, 14,248G)

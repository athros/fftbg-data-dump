Player: !Black
Team: Black Team
Palettes: Black/Red



BlackFireUK
Male
Leo
67
46
Monk
Charge
Absorb Used MP
Dual Wield
Move+2



Golden Hairpin
Mythril Vest
Diamond Armlet

Spin Fist, Pummel, Earth Slash, Purification, Chakra, Revive
Charge+3



Hasterious
Male
Virgo
68
54
Bard
Steal
Sunken State
Maintenance
Ignore Height

Bloody Strings

Flash Hat
Chain Vest
Jade Armlet

Life Song, Cheer Song, Magic Song, Last Song, Diamond Blade, Hydra Pit
Steal Heart, Steal Armor



Spuzzmocker
Male
Gemini
64
58
Bard
Basic Skill
Counter Tackle
Attack UP
Jump+3

Ramia Harp

Black Hood
Clothes
Dracula Mantle

Life Song, Cheer Song, Last Song
Heal



Stan
Male
Capricorn
69
45
Oracle
Basic Skill
Regenerator
Dual Wield
Move-HP Up

Ivory Rod
Octagon Rod
Green Beret
Brigandine
Genji Gauntlet

Poison, Life Drain, Blind Rage, Foxbird, Dispel Magic, Sleep
Heal, Yell

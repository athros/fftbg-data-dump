Final Bets: red - 12 bets for 5,971G (74.2%, x0.35); green - 4 bets for 2,075G (25.8%, x2.88)

red bets:
Mesmaster: 2,000G (33.5%, 90,434G)
Draconis345: 1,000G (16.7%, 57,619G)
ShintaroNayaka: 676G (11.3%, 676G)
NovaKnight21: 500G (8.4%, 7,375G)
dogsandcatsand: 416G (7.0%, 416G)
ColetteMSLP: 400G (6.7%, 3,059G)
getthemoneyz: 328G (5.5%, 1,060,024G)
gorgewall: 201G (3.4%, 25,264G)
ar_tactic: 150G (2.5%, 55,111G)
nifboy: 100G (1.7%, 5,898G)
datadrivenbot: 100G (1.7%, 49,442G)
Spuzzmocker: 100G (1.7%, 3,925G)

green bets:
serperemagus: 1,130G (54.5%, 1,130G)
prince_rogers_nelson_: 600G (28.9%, 600G)
BirbBrainsBot: 245G (11.8%, 11,214G)
Ring_Wyrm: 100G (4.8%, 5,044G)

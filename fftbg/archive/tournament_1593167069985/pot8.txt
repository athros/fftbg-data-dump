Final Bets: purple - 12 bets for 8,444G (67.5%, x0.48); champion - 4 bets for 4,072G (32.5%, x2.07)

purple bets:
Spuzzmocker: 3,360G (39.8%, 3,360G)
ShintaroNayaka: 1,389G (16.4%, 1,389G)
BirbBrainsBot: 1,000G (11.8%, 12,968G)
prince_rogers_nelson_: 600G (7.1%, 600G)
dogsandcatsand: 416G (4.9%, 416G)
CT_5_Holy: 406G (4.8%, 406G)
getthemoneyz: 322G (3.8%, 1,059,954G)
gorgewall: 301G (3.6%, 25,889G)
ar_tactic: 250G (3.0%, 55,106G)
Evewho: 200G (2.4%, 7,108G)
nifboy: 100G (1.2%, 5,879G)
datadrivenbot: 100G (1.2%, 49,423G)

champion bets:
Zachara: 2,748G (67.5%, 142,748G)
Mesmaster: 1,000G (24.6%, 91,129G)
Ruvelia_BibeI: 224G (5.5%, 224G)
CosmicTactician: 100G (2.5%, 20,692G)

Final Bets: purple - 9 bets for 6,340G (82.8%, x0.21); brown - 8 bets for 1,318G (17.2%, x4.81)

purple bets:
Digitalsocrates: 1,982G (31.3%, 1,982G)
Mesmaster: 1,641G (25.9%, 1,641G)
Evewho: 1,549G (24.4%, 3,039G)
fluffskull: 567G (8.9%, 567G)
gorgewall: 201G (3.2%, 3,503G)
AllInBot: 100G (1.6%, 100G)
Ring_Wyrm: 100G (1.6%, 3,130G)
Firesheath: 100G (1.6%, 2,411G)
datadrivenbot: 100G (1.6%, 36,286G)

brown bets:
getthemoneyz: 390G (29.6%, 872,466G)
prince_rogers_nelson_: 200G (15.2%, 6,518G)
kaidykat: 200G (15.2%, 26,173G)
serperemagus: 200G (15.2%, 3,020G)
Zachara: 120G (9.1%, 101,520G)
spootoo: 104G (7.9%, 104G)
CT_5_Holy: 100G (7.6%, 1,440G)
BirbBrainsBot: 4G (0.3%, 33,477G)

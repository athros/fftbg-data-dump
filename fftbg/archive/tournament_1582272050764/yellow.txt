Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



WarpZealot
Female
Libra
72
69
Chemist
Elemental
Earplug
Equip Axe
Fly

Flail

Red Hood
Earth Clothes
Magic Gauntlet

Potion, Hi-Potion, X-Potion, Eye Drop, Maiden's Kiss
Pitfall, Static Shock, Quicksand, Blizzard, Gusty Wind, Lava Ball



MithrasArslan
Male
Gemini
68
43
Monk
Steal
Blade Grasp
Beastmaster
Move-HP Up



Black Hood
Power Sleeve
Red Shoes

Spin Fist, Earth Slash, Secret Fist, Purification, Chakra, Revive, Seal Evil
Gil Taking, Steal Weapon, Steal Accessory, Leg Aim



Farseli
Male
Taurus
49
53
Mediator
Yin Yang Magic
Regenerator
Maintenance
Levitate

Romanda Gun

Feather Hat
Adaman Vest
Diamond Armlet

Solution, Insult, Negotiate, Mimic Daravon
Doubt Faith, Blind Rage, Foxbird, Confusion Song



Tommy Havoc
Female
Aries
67
52
Oracle
Battle Skill
Mana Shield
Defense UP
Fly

Ivory Rod

Headgear
Black Robe
Chantage

Spell Absorb, Doubt Faith, Paralyze, Dark Holy
Head Break, Armor Break, Shield Break, Weapon Break

Player: !Green
Team: Green Team
Palettes: Green/White



Leakimiko
Monster
Sagittarius
67
78
Steel Giant










StealthModeLocke
Male
Serpentarius
73
42
Samurai
Throw
Damage Split
Martial Arts
Ignore Terrain

Kikuichimoji

Gold Helmet
Wizard Robe
Jade Armlet

Murasame, Muramasa, Masamune
Shuriken, Staff



Evewho
Female
Virgo
60
75
Mime

Mana Shield
Magic Defense UP
Waterwalking


Crystal Shield
Leather Hat
Earth Clothes
Genji Gauntlet

Mimic




Goust18
Male
Serpentarius
52
71
Knight
Punch Art
Sunken State
Secret Hunt
Move-MP Up

Save the Queen
Buckler
Mythril Helmet
Black Robe
Reflect Ring

Head Break, Magic Break, Power Break
Pummel, Earth Slash

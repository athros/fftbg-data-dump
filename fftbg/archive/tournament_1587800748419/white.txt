Player: !White
Team: White Team
Palettes: White/Blue



EnemyController
Male
Libra
62
59
Oracle
Summon Magic
Abandon
Short Charge
Teleport

Gokuu Rod

Red Hood
Leather Outfit
Genji Gauntlet

Life Drain, Pray Faith, Doubt Faith, Silence Song, Dispel Magic, Sleep, Dark Holy
Moogle, Ramuh, Salamander, Silf



KasugaiRoastedPeas
Monster
Scorpio
71
73
Tiamat










Mayormcfunbags
Male
Virgo
48
44
Calculator
Robosnake Skill
Brave Up
Equip Axe
Jump+2

Slasher
Bronze Shield
Circlet
Chain Vest
Bracer

Blue Magic
Destroy, Compress, Dispose, Repair, Snake Carrier, Toxic Frog, Midgar Swarm



Anethum
Female
Virgo
53
69
Knight
Time Magic
Auto Potion
Doublehand
Teleport

Mythril Sword

Bronze Helmet
Mythril Armor
108 Gems

Head Break, Armor Break, Shield Break, Power Break
Haste, Haste 2, Immobilize, Stabilize Time, Meteor

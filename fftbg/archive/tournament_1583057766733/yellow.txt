Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Ominnous
Female
Gemini
61
59
Geomancer
Draw Out
Parry
Equip Axe
Waterwalking

Oak Staff
Genji Shield
Twist Headband
Earth Clothes
Leather Mantle

Water Ball, Hell Ivy, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Asura, Koutetsu, Kiyomori, Muramasa



Sypheck
Male
Virgo
63
45
Mediator
Draw Out
Regenerator
Dual Wield
Move+3

Romanda Gun
Blaze Gun
Barette
Judo Outfit
Cherche

Solution, Insult, Mimic Daravon, Refute, Rehabilitate
Muramasa



Unclebearboy
Female
Sagittarius
52
72
Thief
Summon Magic
Faith Up
Beastmaster
Ignore Terrain

Mythril Knife

Holy Miter
Wizard Outfit
Reflect Ring

Steal Heart, Steal Helmet, Steal Accessory
Moogle, Carbunkle, Silf, Cyclops



Draconis345
Male
Libra
43
60
Summoner
White Magic
HP Restore
Magic Defense UP
Swim

Gold Staff

Black Hood
Mythril Vest
Sprint Shoes

Moogle, Carbunkle, Odin, Leviathan, Lich
Raise 2, Protect, Protect 2, Shell, Shell 2, Esuna

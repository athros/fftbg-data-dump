Player: !White
Team: White Team
Palettes: White/Blue



Narcius
Male
Leo
45
51
Oracle
Item
Damage Split
Dual Wield
Fly

Gokuu Rod
Ivory Rod
Twist Headband
Light Robe
Reflect Ring

Life Drain, Pray Faith, Blind Rage, Petrify
X-Potion, Ether, Hi-Ether, Antidote, Maiden's Kiss, Soft, Remedy



Lemonjohns
Female
Libra
66
66
Mediator
Jump
Faith Save
Attack UP
Ignore Height

Blaze Gun

Barette
Black Costume
Dracula Mantle

Threaten, Preach, Insult, Refute
Level Jump8, Vertical Jump8



Run With Stone GUNs
Female
Virgo
56
41
Wizard
Item
Counter Flood
Short Charge
Fly

Wizard Rod

Cachusha
Power Sleeve
Feather Boots

Fire, Fire 3, Bolt, Bolt 2, Bolt 4, Ice 3, Frog
Potion, Hi-Ether, Echo Grass, Maiden's Kiss, Phoenix Down



Jjhull3
Female
Pisces
70
66
Dancer
Steal
PA Save
Defend
Move-HP Up

Ryozan Silk

Holy Miter
Earth Clothes
Small Mantle

Slow Dance, Disillusion, Last Dance
Steal Accessory, Leg Aim

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DrAntiSocial
Male
Gemini
45
64
Ninja
Time Magic
Sunken State
Equip Sword
Levitate

Murasame
Muramasa
Holy Miter
Power Sleeve
Defense Ring

Shuriken, Knife, Axe
Haste, Reflect, Demi, Stabilize Time



Rislyeu
Female
Sagittarius
56
76
Mime

Regenerator
Equip Armor
Levitate



Green Beret
Linen Robe
Defense Ring

Mimic




Volgrathemoose
Male
Aries
66
55
Archer
Black Magic
Damage Split
Dual Wield
Jump+2

Lightning Bow

Golden Hairpin
Leather Outfit
Feather Boots

Charge+4, Charge+5, Charge+7
Fire, Fire 3, Bolt 2, Empower, Flare



Leakimiko
Male
Capricorn
36
58
Monk
Talk Skill
Critical Quick
Dual Wield
Waterwalking



Golden Hairpin
Judo Outfit
Feather Mantle

Spin Fist, Pummel, Purification
Invitation, Praise, Solution, Refute

Player: !Red
Team: Red Team
Palettes: Red/Brown



ShintaroNayaka
Female
Scorpio
54
45
Priest
Item
Speed Save
Magic Attack UP
Lava Walking

Rainbow Staff

Green Beret
Judo Outfit
Small Mantle

Cure 2, Cure 4, Raise, Reraise, Regen, Protect, Protect 2, Shell, Holy
Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Maiden's Kiss, Soft, Phoenix Down



RjA0zcOQ96
Female
Gemini
63
71
Archer
Summon Magic
Counter Flood
Equip Bow
Ignore Terrain

Hunting Bow
Platinum Shield
Flash Hat
Power Sleeve
Jade Armlet

Charge+2, Charge+3, Charge+7, Charge+10, Charge+20
Moogle, Titan, Carbunkle, Bahamut, Odin, Leviathan, Fairy, Cyclops



Wizblizz
Male
Sagittarius
76
77
Ninja
Charge
Distribute
Doublehand
Swim

Short Edge

Flash Hat
Mystic Vest
Sprint Shoes

Bomb, Staff, Axe, Wand
Charge+1, Charge+10



Rocl
Female
Aries
69
80
Lancer
Time Magic
Parry
Short Charge
Lava Walking

Javelin
Round Shield
Iron Helmet
Reflect Mail
Genji Gauntlet

Level Jump8, Vertical Jump2
Haste 2, Stop, Immobilize, Float, Reflect, Stabilize Time

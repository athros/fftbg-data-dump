Player: !Green
Team: Green Team
Palettes: Green/White



SephDarkheart
Female
Cancer
73
58
Thief
Item
Speed Save
Magic Attack UP
Move-MP Up

Ninja Edge

Holy Miter
Secret Clothes
Angel Ring

Steal Helmet, Steal Armor, Arm Aim
Potion, X-Potion, Eye Drop, Soft, Holy Water



Oreo Pizza
Female
Taurus
41
73
Calculator
White Magic
Auto Potion
Martial Arts
Waterwalking



Red Hood
Mythril Vest
Battle Boots

CT, 5, 4
Cure, Raise, Raise 2, Reraise, Regen, Shell 2



Lowlf
Female
Pisces
54
51
Monk
Draw Out
Arrow Guard
Sicken
Levitate



Headgear
Mystic Vest
Vanish Mantle

Spin Fist, Purification, Revive, Seal Evil
Bizen Boat, Heaven's Cloud, Muramasa, Chirijiraden



Mirapoix
Female
Cancer
50
61
Oracle
Draw Out
PA Save
Attack UP
Move+1

Battle Bamboo

Flash Hat
Silk Robe
Battle Boots

Life Drain, Blind Rage, Confusion Song, Dispel Magic
Koutetsu

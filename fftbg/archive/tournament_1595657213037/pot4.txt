Final Bets: purple - 11 bets for 7,017G (40.5%, x1.47); brown - 11 bets for 10,320G (59.5%, x0.68)

purple bets:
UmaiJam: 1,500G (21.4%, 55,681G)
BirbBrainsBot: 1,000G (14.3%, 59,772G)
Laserman1000: 981G (14.0%, 1,925G)
poGpopE: 910G (13.0%, 910G)
douchetron: 807G (11.5%, 807G)
Hirameki85: 609G (8.7%, 609G)
josephiroth_143: 500G (7.1%, 5,140G)
holdenmagronik: 224G (3.2%, 1,446G)
datadrivenbot: 200G (2.9%, 41,444G)
getthemoneyz: 186G (2.7%, 1,380,742G)
AugnosMusic: 100G (1.4%, 100G)

brown bets:
sinnyil2: 4,484G (43.4%, 8,969G)
DeathTaxesAndAnime: 1,238G (12.0%, 1,238G)
Lythe_Caraker: 1,000G (9.7%, 155,244G)
Draconis345: 1,000G (9.7%, 46,620G)
AllInBot: 551G (5.3%, 551G)
amiture: 500G (4.8%, 7,486G)
superdevon1: 478G (4.6%, 23,932G)
resjudicata3: 369G (3.6%, 369G)
timthehoodie: 300G (2.9%, 576G)
unclebearboy: 300G (2.9%, 27,310G)
Firesheath: 100G (1.0%, 11,938G)

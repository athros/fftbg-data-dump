Final Bets: purple - 8 bets for 5,966G (74.3%, x0.35); brown - 6 bets for 2,061G (25.7%, x2.89)

purple bets:
WalkerNash: 1,521G (25.5%, 30,434G)
getthemoneyz: 1,000G (16.8%, 1,018,213G)
BirbBrainsBot: 1,000G (16.8%, 55,729G)
BlackFireUK: 1,000G (16.8%, 31,860G)
Smokegiant: 500G (8.4%, 900G)
CassiePhoenix: 424G (7.1%, 424G)
gorgewall: 401G (6.7%, 17,891G)
Lydian_C: 120G (2.0%, 196,028G)

brown bets:
VolgraTheMoose: 1,001G (48.6%, 21,502G)
prince_rogers_nelson_: 500G (24.3%, 14,212G)
actual_JP: 200G (9.7%, 5,965G)
Evewho: 200G (9.7%, 2,388G)
datadrivenbot: 100G (4.9%, 45,248G)
roqqqpsi: 60G (2.9%, 6,034G)

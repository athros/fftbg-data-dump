Player: !White
Team: White Team
Palettes: White/Blue



Prince Rogers Nelson
Male
Virgo
64
56
Archer
Yin Yang Magic
Speed Save
Doublehand
Jump+3

Cross Bow

Twist Headband
Power Sleeve
Magic Ring

Charge+1, Charge+2, Charge+3, Charge+4, Charge+5, Charge+7, Charge+10
Spell Absorb, Pray Faith, Blind Rage, Foxbird, Dispel Magic, Petrify, Dark Holy



ColetteMSLP
Female
Cancer
80
54
Squire
Jump
Absorb Used MP
Doublehand
Move-MP Up

Main Gauche

Iron Helmet
Wizard Outfit
Small Mantle

Heal, Yell
Level Jump3, Vertical Jump6



Spuzzmocker
Female
Virgo
60
40
Summoner
Item
Mana Shield
Equip Sword
Fly

Masamune

Red Hood
Light Robe
Dracula Mantle

Ramuh, Titan, Carbunkle, Odin
Potion, X-Potion, Ether, Echo Grass, Maiden's Kiss, Soft, Remedy, Phoenix Down



Evewho
Female
Aries
62
59
Chemist
Charge
Counter
Equip Sword
Teleport 2

Ice Brand

Flash Hat
Clothes
Magic Ring

Potion, X-Potion, Antidote, Eye Drop, Phoenix Down
Charge+1, Charge+3, Charge+5

Player: !Red
Team: Red Team
Palettes: Red/Brown



LivingHitokiri
Male
Taurus
74
65
Archer
Basic Skill
Auto Potion
Doublehand
Swim

Blast Gun

Grand Helmet
Mythril Vest
Sprint Shoes

Charge+1, Charge+4
Heal, Tickle



ZZ Yoshi
Female
Capricorn
58
70
Geomancer
Punch Art
Caution
Equip Axe
Jump+3

Wizard Staff
Aegis Shield
Feather Hat
Earth Clothes
Germinas Boots

Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind, Lava Ball
Pummel, Earth Slash, Purification, Chakra, Revive



DustyRelic
Male
Taurus
47
76
Bard
Punch Art
Arrow Guard
Magic Attack UP
Waterwalking

Long Bow

Twist Headband
Chain Vest
Wizard Mantle

Angel Song, Battle Song, Diamond Blade, Sky Demon
Earth Slash, Purification, Revive



Miyokari
Male
Cancer
56
45
Knight
Punch Art
Catch
Equip Sword
Waterbreathing

Heaven's Cloud
Ice Shield
Crystal Helmet
Bronze Armor
Leather Mantle

Weapon Break, Magic Break, Speed Break
Pummel, Wave Fist, Purification, Chakra, Revive, Seal Evil

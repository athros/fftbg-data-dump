Player: !Red
Team: Red Team
Palettes: Red/Brown



Volgrathemoose
Male
Libra
71
53
Mime

Speed Save
Secret Hunt
Waterwalking



Green Beret
Mythril Vest
Small Mantle

Mimic




GundamVGZ
Female
Aries
40
59
Ninja
Basic Skill
Mana Shield
Secret Hunt
Ignore Terrain

Kunai
Short Edge
Feather Hat
Brigandine
Feather Mantle

Bomb, Knife
Accumulate, Throw Stone, Heal, Cheer Up



Mayormcfunbags
Male
Capricorn
60
43
Monk
Black Magic
Counter
Equip Gun
Waterbreathing

Battle Folio

Red Hood
Black Costume
Battle Boots

Spin Fist, Earth Slash, Purification, Chakra
Fire 2, Fire 3, Fire 4, Bolt 3, Bolt 4, Ice 3, Empower, Flare



WireLord
Female
Pisces
62
48
Oracle
Dance
Critical Quick
Short Charge
Move-MP Up

Iron Fan

Green Beret
Light Robe
Bracer

Life Drain, Zombie, Blind Rage, Confusion Song, Dispel Magic, Paralyze
Witch Hunt, Polka Polka, Last Dance, Obsidian Blade

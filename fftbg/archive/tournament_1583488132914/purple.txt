Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



SeedSC
Female
Cancer
68
44
Chemist
Charge
Meatbone Slash
Magic Attack UP
Move-MP Up

Assassin Dagger

Feather Hat
Mystic Vest
Bracer

Potion, Maiden's Kiss, Remedy
Charge+5, Charge+7



Maeveen
Male
Pisces
68
47
Ninja
Talk Skill
Distribute
Equip Armor
Move-MP Up

Hidden Knife
Flame Whip
Iron Helmet
Silk Robe
Power Wrist

Shuriken
Insult, Refute



Bad1dea
Female
Aquarius
76
81
Lancer
Time Magic
Absorb Used MP
Defend
Swim

Gungnir
Diamond Shield
Leather Helmet
Reflect Mail
Leather Mantle

Level Jump8, Vertical Jump6
Haste, Immobilize, Float, Reflect, Demi, Demi 2, Stabilize Time



I Nod My Head When I Lose
Female
Aquarius
42
66
Priest
Talk Skill
MP Restore
Halve MP
Move+2

Gold Staff

Twist Headband
Wizard Robe
Magic Ring

Cure 3, Cure 4, Regen, Protect 2, Wall, Holy
Threaten, Death Sentence, Negotiate, Mimic Daravon

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Lowlf
Male
Gemini
64
69
Monk
Battle Skill
Damage Split
Short Charge
Move+2



Feather Hat
Earth Clothes
Jade Armlet

Secret Fist, Purification, Revive
Head Break, Armor Break, Weapon Break, Magic Break



NIghtdew14
Female
Sagittarius
64
56
Dancer
Charge
Critical Quick
Equip Gun
Retreat

Papyrus Codex

Feather Hat
Mystic Vest
Cursed Ring

Last Dance, Obsidian Blade
Charge+1, Charge+5, Charge+20



Resjudicata3
Female
Aquarius
72
74
Ninja
Steal
Blade Grasp
Magic Defense UP
Jump+1

Spell Edge
Blind Knife
Flash Hat
Wizard Outfit
Jade Armlet

Shuriken
Steal Accessory



Seaweed B
Female
Aquarius
63
41
Mediator
Time Magic
HP Restore
Dual Wield
Lava Walking

Mythril Gun
Stone Gun
Headgear
Brigandine
108 Gems

Invitation, Threaten, Death Sentence, Insult
Haste, Slow 2, Float, Reflect, Quick, Stabilize Time, Meteor, Galaxy Stop

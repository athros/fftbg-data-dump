Player: !Brown
Team: Brown Team
Palettes: Brown/Green



AStatue
Female
Taurus
42
70
Archer
Jump
Damage Split
Equip Axe
Waterbreathing

Battle Axe
Aegis Shield
Holy Miter
Power Sleeve
Germinas Boots

Charge+1, Charge+2, Charge+5, Charge+7
Level Jump2, Vertical Jump7



ZephyrTempest
Female
Aquarius
43
67
Knight
Black Magic
HP Restore
Long Status
Jump+2

Battle Axe
Ice Shield
Leather Helmet
Gold Armor
Dracula Mantle

Magic Break, Mind Break
Fire 2, Fire 4, Ice 3, Frog, Flare



Shakarak
Female
Capricorn
50
76
Summoner
Dance
Damage Split
Defense UP
Waterbreathing

White Staff

Ribbon
Chameleon Robe
Magic Gauntlet

Moogle, Shiva, Ramuh, Golem, Bahamut, Odin, Silf, Fairy
Witch Hunt, Slow Dance, Disillusion, Nameless Dance, Last Dance



DustBirdEX
Male
Leo
61
76
Monk
Item
Parry
Throw Item
Retreat



Holy Miter
Black Costume
Feather Boots

Spin Fist, Pummel, Secret Fist, Chakra, Revive
Potion, Antidote, Echo Grass, Maiden's Kiss, Soft, Phoenix Down

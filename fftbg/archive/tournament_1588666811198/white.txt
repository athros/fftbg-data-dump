Player: !White
Team: White Team
Palettes: White/Blue



Twelfthrootoftwo
Male
Capricorn
78
50
Time Mage
Talk Skill
Arrow Guard
Halve MP
Move-MP Up

Rainbow Staff

Leather Hat
Wizard Robe
Dracula Mantle

Slow, Slow 2, Stop, Immobilize, Float, Quick, Demi 2, Stabilize Time, Meteor
Praise, Threaten, Preach, Negotiate, Rehabilitate



Lythe Caraker
Male
Pisces
43
46
Bard
Time Magic
Speed Save
Equip Gun
Fly

Bestiary

Twist Headband
Chain Vest
N-Kai Armlet

Angel Song, Life Song, Cheer Song, Battle Song, Nameless Song, Hydra Pit
Demi, Stabilize Time, Meteor



Gorgewall
Male
Sagittarius
55
49
Samurai
White Magic
Dragon Spirit
Equip Knife
Move+3

Air Knife

Crystal Helmet
Linen Cuirass
Rubber Shoes

Koutetsu, Bizen Boat, Kiyomori, Muramasa
Cure 3, Raise, Protect, Protect 2, Shell, Wall, Esuna



Go2sleepTV
Male
Aries
66
51
Samurai
Yin Yang Magic
Counter Flood
Concentrate
Lava Walking

Obelisk

Bronze Helmet
White Robe
Elf Mantle

Muramasa, Kikuichimoji
Pray Faith, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Dark Holy

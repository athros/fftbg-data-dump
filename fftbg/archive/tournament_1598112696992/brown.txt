Player: !Brown
Team: Brown Team
Palettes: Brown/Green



MagicBottle
Male
Taurus
41
51
Geomancer
Time Magic
Sunken State
Short Charge
Teleport

Battle Axe
Mythril Shield
Leather Hat
Earth Clothes
Reflect Ring

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Sand Storm, Gusty Wind, Lava Ball
Reflect, Quick, Demi 2, Stabilize Time, Meteor



Twisted Nutsatchel
Monster
Aquarius
47
55
Steel Giant










DLJuggernaut
Male
Libra
71
58
Squire
Jump
Parry
Sicken
Lava Walking

Bow Gun
Buckler
Black Hood
Clothes
Defense Armlet

Heal
Level Jump5, Vertical Jump7



Shalloween
Male
Libra
60
75
Squire
Time Magic
Mana Shield
Maintenance
Lava Walking

Gastrafitis
Mythril Shield
Red Hood
Bronze Armor
Feather Boots

Accumulate, Throw Stone, Heal, Cheer Up, Wish, Scream
Haste, Haste 2, Immobilize, Float, Demi, Stabilize Time

Player: !Red
Team: Red Team
Palettes: Red/Brown



CosmicTactician
Male
Virgo
81
49
Calculator
Yin Yang Magic
Meatbone Slash
Martial Arts
Teleport

Papyrus Codex

Triangle Hat
Silk Robe
Magic Gauntlet

CT, Height, Prime Number, 3
Poison, Spell Absorb, Life Drain, Doubt Faith, Foxbird, Dispel Magic



Redmage4evah
Male
Gemini
62
78
Oracle
Steal
Mana Shield
Equip Shield
Lava Walking

Bestiary
Mythril Shield
Green Beret
Secret Clothes
Magic Gauntlet

Blind, Poison, Pray Faith, Doubt Faith, Silence Song, Blind Rage, Foxbird, Dispel Magic, Paralyze, Sleep
Steal Heart, Arm Aim



Pplvee1
Male
Scorpio
57
44
Thief
Throw
MP Restore
Equip Sword
Move-HP Up

Defender

Cachusha
Chain Vest
108 Gems

Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Accessory
Bomb, Staff, Ninja Sword, Wand



Galkife
Male
Aquarius
79
40
Wizard
Sing
Catch
Magic Attack UP
Jump+3

Mage Masher

Red Hood
Judo Outfit
Germinas Boots

Fire, Fire 3, Bolt, Bolt 2, Bolt 4
Life Song, Cheer Song, Magic Song, Hydra Pit

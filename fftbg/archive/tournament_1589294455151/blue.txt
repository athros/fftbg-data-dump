Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Anethum
Male
Scorpio
66
65
Geomancer
Black Magic
HP Restore
Concentrate
Waterbreathing

Battle Axe
Mythril Shield
Feather Hat
Chain Vest
Jade Armlet

Pitfall, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Fire, Fire 2, Bolt, Bolt 2, Bolt 4, Ice



Lokenwow
Female
Cancer
65
58
Calculator
Time Magic
MA Save
Beastmaster
Move+3

Papyrus Codex

Black Hood
Chameleon Robe
N-Kai Armlet

CT, Height, Prime Number, 5, 4, 3
Immobilize, Stabilize Time



ExecutedGiraffe
Male
Aries
62
44
Knight
Elemental
Mana Shield
Equip Armor
Move-MP Up

Save the Queen
Platinum Shield
Platinum Helmet
Judo Outfit
Feather Mantle

Magic Break, Speed Break, Mind Break
Water Ball, Hell Ivy, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind, Lava Ball



OperatorTheory
Monster
Sagittarius
80
66
Ghost








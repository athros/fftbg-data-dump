Player: !Green
Team: Green Team
Palettes: Green/White



S O B E Q
Monster
Cancer
68
77
Chocobo










Aldrammech
Male
Capricorn
60
79
Oracle
Jump
Counter Tackle
Long Status
Move-HP Up

Octagon Rod

Feather Hat
Brigandine
Spike Shoes

Blind, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze, Petrify
Level Jump4, Vertical Jump6



Wyonearth
Female
Capricorn
68
75
Time Mage
Steal
Distribute
Maintenance
Move+1

Rainbow Staff

Twist Headband
Chameleon Robe
Red Shoes

Haste, Haste 2, Slow, Stop, Immobilize, Reflect, Quick, Demi 2, Stabilize Time
Gil Taking, Steal Heart, Steal Armor, Steal Shield, Steal Status, Leg Aim



UmaiJam
Male
Capricorn
44
75
Monk
Black Magic
Speed Save
Dual Wield
Move-HP Up



Thief Hat
Earth Clothes
Cursed Ring

Pummel, Wave Fist, Secret Fist, Revive, Seal Evil
Fire, Fire 3, Bolt, Ice 2, Ice 3, Empower, Death

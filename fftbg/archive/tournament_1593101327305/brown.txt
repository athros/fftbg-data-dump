Player: !Brown
Team: Brown Team
Palettes: Brown/Green



DavenIII
Male
Capricorn
62
53
Squire
White Magic
MA Save
Defense UP
Move-MP Up

Snipe Bow
Bronze Shield
Gold Helmet
Mystic Vest
Feather Boots

Accumulate, Tickle
Cure 2, Cure 3, Cure 4, Raise, Regen, Shell, Shell 2, Wall, Esuna



Dogsandcatsand
Male
Cancer
61
50
Squire
Elemental
Distribute
Defense UP
Ignore Terrain

Giant Axe
Platinum Shield
Iron Helmet
Leather Armor
Defense Ring

Throw Stone, Heal, Cheer Up, Scream
Pitfall, Water Ball, Static Shock, Quicksand, Sand Storm, Blizzard, Gusty Wind



Fenixcrest
Female
Scorpio
56
67
Squire
Elemental
Dragon Spirit
Equip Gun
Move-MP Up

Battle Folio
Bronze Shield
Holy Miter
Gold Armor
Vanish Mantle

Accumulate, Dash, Heal, Yell, Fury, Ultima
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Nifboy
Male
Aquarius
74
78
Squire
Jump
Sunken State
Equip Axe
Jump+3

Battle Axe
Platinum Shield
Flash Hat
Mythril Vest
Elf Mantle

Accumulate, Heal, Yell
Level Jump8, Vertical Jump6

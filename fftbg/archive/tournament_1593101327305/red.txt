Player: !Red
Team: Red Team
Palettes: Red/Brown



CaptainGarlock
Male
Cancer
78
53
Squire
Time Magic
Abandon
Doublehand
Move-HP Up

Rune Blade

Black Hood
Platinum Armor
Defense Ring

Dash, Throw Stone, Yell, Wish
Haste, Slow 2, Demi 2, Stabilize Time, Meteor



Just Here2
Male
Capricorn
70
69
Squire
Charge
Counter Flood
Long Status
Jump+2

Long Sword
Crystal Shield
Ribbon
Platinum Armor
Jade Armlet

Accumulate, Dash, Heal, Tickle, Wish
Charge+1, Charge+2



SQUiDSQUARKLIN
Male
Leo
52
60
Squire
Item
Mana Shield
Equip Knife
Move+1

Rod
Platinum Shield
Genji Helmet
Genji Armor
N-Kai Armlet

Accumulate, Dash, Heal, Tickle
Potion, Hi-Potion, Eye Drop, Echo Grass, Soft, Remedy



Amiture
Male
Libra
53
49
Squire
Time Magic
Parry
Long Status
Jump+1

Flame Whip
Crystal Shield
Triangle Hat
Mythril Armor
Jade Armlet

Accumulate, Dash, Heal, Cheer Up, Wish
Float, Reflect, Quick, Meteor

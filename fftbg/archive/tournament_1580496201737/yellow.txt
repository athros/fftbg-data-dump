Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



DieInObscurity
Male
Libra
60
49
Ninja
Elemental
Blade Grasp
Martial Arts
Swim



Black Hood
Power Sleeve
Reflect Ring

Axe, Wand, Dictionary
Pitfall, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Blizzard



Spidergun0
Male
Libra
55
80
Ninja
Black Magic
Critical Quick
Attack UP
Move-MP Up

Sasuke Knife
Kunai
Flash Hat
Mythril Vest
Genji Gauntlet

Knife, Axe, Stick
Fire 3, Bolt 4, Ice, Death



TravisBlack
Female
Pisces
73
63
Chemist
Black Magic
Catch
Equip Axe
Lava Walking

Romanda Gun

Flash Hat
Judo Outfit
Dracula Mantle

Potion, X-Potion, Ether, Elixir, Antidote, Eye Drop, Echo Grass, Maiden's Kiss, Soft, Phoenix Down
Fire 4, Ice



ItsTheCrazy1
Male
Libra
51
51
Wizard
Sing
Regenerator
Defend
Jump+3

Assassin Dagger

Leather Hat
White Robe
Diamond Armlet

Fire, Ice 2, Ice 3, Empower, Frog
Space Storage

Player: !White
Team: White Team
Palettes: White/Blue



Vics
Female
Cancer
74
80
Archer
Item
PA Save
Maintenance
Move-MP Up

Ultimus Bow

Green Beret
Brigandine
Battle Boots

Charge+1, Charge+2, Charge+5
X-Potion, Hi-Ether, Echo Grass, Phoenix Down



Sypheck
Female
Gemini
57
42
Dancer
Talk Skill
Hamedo
Equip Armor
Fly

Cashmere

Twist Headband
Platinum Armor
Genji Gauntlet

Slow Dance, Polka Polka, Nameless Dance, Nether Demon, Dragon Pit
Persuade, Praise, Threaten, Death Sentence, Negotiate, Refute, Rehabilitate



Yayifications
Female
Capricorn
47
77
Mime

Absorb Used MP
Martial Arts
Move+3



Flash Hat
Secret Clothes
Jade Armlet

Mimic




KIYOMORI
Male
Leo
49
43
Samurai
Elemental
Auto Potion
Doublehand
Levitate

Murasame

Mythril Helmet
Gold Armor
Red Shoes

Asura, Koutetsu, Murasame, Kiyomori, Chirijiraden
Pitfall, Water Ball, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball

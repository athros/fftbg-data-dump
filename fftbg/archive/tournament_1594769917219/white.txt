Player: !White
Team: White Team
Palettes: White/Blue



Pplvee1
Male
Scorpio
77
43
Ninja
Draw Out
MP Restore
Attack UP
Levitate

Spell Edge
Short Edge
Flash Hat
Black Costume
Red Shoes

Shuriken, Bomb
Kikuichimoji



Dtrain332
Female
Cancer
80
75
Geomancer
Dance
Regenerator
Attack UP
Teleport

Giant Axe
Platinum Shield
Holy Miter
Black Robe
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Blizzard, Gusty Wind, Lava Ball
Void Storage, Nether Demon



Kronikle
Female
Virgo
44
51
Geomancer
Basic Skill
Catch
Defense UP
Move+1

Battle Axe
Crystal Shield
Barette
Adaman Vest
Spike Shoes

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Accumulate, Heal, Tickle, Yell, Cheer Up, Fury, Scream



Xoomwaffle
Male
Pisces
43
52
Bard
Elemental
Regenerator
Martial Arts
Ignore Terrain



Headgear
Brigandine
Magic Gauntlet

Battle Song
Pitfall, Water Ball, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind

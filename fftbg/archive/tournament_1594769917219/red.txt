Player: !Red
Team: Red Team
Palettes: Red/Brown



Daveb
Male
Taurus
57
63
Ninja
Draw Out
Auto Potion
Attack UP
Levitate

Flail
Assassin Dagger
Black Hood
Mythril Vest
Jade Armlet

Shuriken, Bomb, Spear
Koutetsu, Murasame, Heaven's Cloud, Kiyomori



Beowulfrulez
Female
Libra
52
72
Mediator
Yin Yang Magic
MP Restore
Long Status
Move+3

Blast Gun

Feather Hat
Clothes
Sprint Shoes

Invitation, Negotiate, Mimic Daravon, Refute
Poison, Silence Song, Dispel Magic, Paralyze



Rislyeu
Female
Gemini
45
72
Mime

Counter
Equip Shield
Waterwalking


Kaiser Plate
Thief Hat
Secret Clothes
Magic Ring

Mimic




Sparker9
Male
Aries
66
51
Ninja
Yin Yang Magic
Arrow Guard
Equip Polearm
Waterbreathing

Holy Lance
Ryozan Silk
Twist Headband
Black Costume
Spike Shoes

Bomb, Stick
Blind, Life Drain, Pray Faith, Dark Holy

Player: !Green
Team: Green Team
Palettes: Green/White



DLJuggernaut
Female
Libra
75
77
Summoner
Basic Skill
Regenerator
Halve MP
Jump+1

Dragon Rod

Black Hood
Light Robe
Genji Gauntlet

Shiva, Ramuh, Bahamut, Silf, Fairy, Lich, Cyclops
Accumulate, Heal, Scream



StormclawGaming
Female
Aquarius
80
52
Mediator
Steal
Faith Up
Equip Gun
Jump+1

Blast Gun

Twist Headband
Wizard Outfit
Magic Ring

Persuade, Rehabilitate
Steal Armor, Steal Shield, Steal Accessory, Steal Status



Numbersborne
Female
Sagittarius
51
63
Priest
Battle Skill
Speed Save
Long Status
Teleport

Flame Whip

Feather Hat
Mythril Vest
Red Shoes

Cure, Cure 3, Raise, Reraise, Protect, Protect 2, Wall, Esuna
Head Break, Mind Break, Stasis Sword



Maakur
Female
Leo
49
65
Lancer
Black Magic
Caution
Defense UP
Jump+1

Dragon Whisker
Ice Shield
Platinum Helmet
Carabini Mail
Defense Ring

Level Jump2, Vertical Jump8
Fire 2, Bolt, Bolt 2, Bolt 4, Ice 2, Empower, Frog

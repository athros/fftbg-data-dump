Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Ruvelia BibeI
Male
Scorpio
58
49
Lancer
Black Magic
Meatbone Slash
Secret Hunt
Ignore Terrain

Spear
Bronze Shield
Cross Helmet
Genji Armor
Power Wrist

Level Jump2, Vertical Jump2
Fire, Fire 3, Bolt 2, Ice 2, Ice 3



Pandasforsale
Female
Pisces
71
73
Dancer
Steal
Counter
Equip Axe
Ignore Height

Rainbow Staff

Headgear
Clothes
Rubber Shoes

Witch Hunt, Slow Dance
Steal Weapon, Steal Accessory, Arm Aim, Leg Aim



HASTERIOUS
Female
Libra
52
64
Summoner
Time Magic
Faith Save
Short Charge
Move+3

Faith Rod

Feather Hat
Adaman Vest
Jade Armlet

Carbunkle, Bahamut, Fairy
Haste, Slow, Stop, Float, Reflect, Demi, Demi 2, Galaxy Stop



CorpusCav
Female
Libra
52
63
Lancer
Basic Skill
Earplug
Defend
Ignore Terrain

Gungnir
Buckler
Crystal Helmet
Crystal Mail
Battle Boots

Level Jump8, Vertical Jump6
Accumulate, Throw Stone, Fury

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Twelfthrootoftwo
Female
Gemini
60
62
Mediator
Dance
Earplug
Equip Bow
Retreat

Bow Gun

Triangle Hat
Judo Outfit
Germinas Boots

Solution, Death Sentence, Rehabilitate
Witch Hunt, Wiznaibus, Nether Demon, Dragon Pit



DeathTaxesAndAnime
Female
Aquarius
63
74
Geomancer
Basic Skill
Counter Tackle
Equip Bow
Ignore Terrain

Cross Bow
Genji Shield
Triangle Hat
Power Sleeve
Sprint Shoes

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Lava Ball
Heal, Fury



Sairentozon7
Male
Leo
67
67
Priest
Charge
PA Save
Defend
Fly

White Staff

Holy Miter
Leather Outfit
Diamond Armlet

Cure 2, Cure 4, Raise, Raise 2, Regen, Shell, Esuna
Charge+2, Charge+5, Charge+7



Daveb
Female
Scorpio
50
37
Ninja
Punch Art
Auto Potion
Secret Hunt
Jump+2

Mythril Knife
Flame Whip
Green Beret
Clothes
Bracer

Bomb
Spin Fist, Secret Fist

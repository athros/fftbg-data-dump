Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Maleblackfiora
Male
Gemini
48
79
Oracle
Battle Skill
Speed Save
Equip Knife
Lava Walking

Kunai

Headgear
Silk Robe
Spike Shoes

Blind, Life Drain, Silence Song, Confusion Song, Dispel Magic, Petrify, Dark Holy
Head Break, Shield Break, Weapon Break, Magic Break, Speed Break, Mind Break



Nekojin
Male
Aquarius
56
48
Oracle
Punch Art
Absorb Used MP
Equip Axe
Fly

Morning Star

Black Hood
Wizard Robe
Rubber Shoes

Blind, Silence Song, Blind Rage, Confusion Song, Dispel Magic, Sleep, Dark Holy
Revive



Xcessive30
Male
Virgo
37
74
Chemist
Jump
Abandon
Equip Shield
Waterbreathing

Dagger
Gold Shield
Red Hood
Mythril Vest
Feather Mantle

Hi-Ether, Elixir, Antidote, Eye Drop, Echo Grass, Remedy, Phoenix Down
Level Jump3, Vertical Jump7



Lionhermit
Male
Pisces
73
57
Geomancer
Summon Magic
MA Save
Equip Axe
Teleport

Morning Star
Round Shield
Triangle Hat
Chameleon Robe
108 Gems

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Static Shock, Will-O-Wisp, Quicksand, Lava Ball
Moogle, Ramuh, Ifrit, Leviathan, Silf, Fairy, Cyclops

Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Reddwind
Male
Capricorn
44
55
Summoner
Time Magic
Counter
Equip Shield
Levitate

Healing Staff
Escutcheon
Flash Hat
Wizard Robe
Feather Boots

Moogle, Ifrit, Titan, Golem, Carbunkle, Bahamut, Odin, Silf, Lich, Cyclops
Haste 2, Slow 2, Stop, Immobilize, Meteor



Holyonline
Female
Aquarius
52
41
Dancer
Battle Skill
Counter
Beastmaster
Ignore Height

Cashmere

Holy Miter
Chain Vest
Feather Boots

Wiznaibus, Nameless Dance, Last Dance, Nether Demon, Dragon Pit
Weapon Break, Mind Break, Justice Sword, Dark Sword, Surging Sword



KasugaiRoastedPeas
Female
Virgo
52
58
Monk
Battle Skill
Distribute
Long Status
Move+2



Thief Hat
Leather Outfit
Bracer

Wave Fist, Chakra, Revive
Speed Break, Night Sword



Nifboy
Female
Aquarius
71
37
Priest
Math Skill
Counter Magic
Equip Polearm
Ignore Terrain

Cashmere

Green Beret
Black Robe
N-Kai Armlet

Cure 3, Raise, Raise 2, Shell, Wall, Esuna
CT, Height, 5, 4

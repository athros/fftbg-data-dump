Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Gorgewall
Male
Serpentarius
69
53
Mime

Critical Quick
Equip Shield
Retreat


Bronze Shield
Feather Hat
Adaman Vest
Red Shoes

Mimic




GrandmasterFrankerZ
Male
Pisces
80
79
Ninja
Draw Out
Brave Save
Attack UP
Retreat

Spell Edge
Koga Knife
Golden Hairpin
Adaman Vest
Chantage

Ninja Sword
Murasame, Kiyomori



Grandlanzer
Female
Leo
53
63
Chemist
Time Magic
Parry
Beastmaster
Jump+2

Cute Bag

Flash Hat
Brigandine
Vanish Mantle

Ether, Hi-Ether, Eye Drop, Echo Grass, Remedy
Haste 2, Slow, Slow 2, Stop, Immobilize, Float, Reflect, Quick, Demi 2, Stabilize Time



Prince Rogers Nelson
Male
Pisces
53
63
Mediator
Charge
Brave Save
Dual Wield
Move+3

Blast Gun
Glacier Gun
Ribbon
Brigandine
Bracer

Threaten, Negotiate, Mimic Daravon, Refute
Charge+1, Charge+4

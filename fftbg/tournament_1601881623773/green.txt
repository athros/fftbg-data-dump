Player: !Green
Team: Green Team
Palettes: Green/White



Old Overholt
Female
Cancer
73
67
Priest
Elemental
Counter Tackle
Beastmaster
Teleport

Healing Staff

Leather Hat
Mystic Vest
Elf Mantle

Cure 3, Cure 4, Raise, Regen, Wall, Esuna
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand



FatTunaKing
Male
Capricorn
80
77
Monk
Battle Skill
Abandon
Dual Wield
Swim



Leather Hat
Chain Vest
Sprint Shoes

Spin Fist, Pummel, Earth Slash, Secret Fist, Chakra, Revive
Head Break, Armor Break, Weapon Break, Explosion Sword



Mirapoix
Male
Aries
56
57
Chemist
Steal
Counter Tackle
Equip Armor
Ignore Terrain

Cute Bag

Headgear
Gold Armor
Magic Ring

Potion, Hi-Potion, Elixir, Eye Drop, Soft, Holy Water, Phoenix Down
Steal Shield, Steal Weapon, Steal Accessory



Lijarkh
Female
Pisces
53
52
Monk
Basic Skill
MP Restore
Equip Armor
Teleport



Headgear
Bronze Armor
Cursed Ring

Secret Fist, Chakra, Revive, Seal Evil
Throw Stone, Heal

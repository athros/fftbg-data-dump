Final Bets: red - 14 bets for 6,564G (43.1%, x1.32); white - 13 bets for 8,679G (56.9%, x0.76)

red bets:
DarrenDinosaurs: 1,100G (16.8%, 1,100G)
BirbBrainsBot: 1,000G (15.2%, 182,008G)
reinoe: 1,000G (15.2%, 20,837G)
helpimabug: 840G (12.8%, 840G)
krombobreaker: 500G (7.6%, 17,614G)
DanielRKnealeArt: 500G (7.6%, 5,509G)
adellraynes: 300G (4.6%, 700G)
killth3kid: 250G (3.8%, 11,866G)
AllInBot: 200G (3.0%, 200G)
CosmicTactician: 200G (3.0%, 28,292G)
SnarkMage: 200G (3.0%, 4,105G)
FlavaFraze: 200G (3.0%, 200G)
ericzubat: 200G (3.0%, 10,516G)
getthemoneyz: 74G (1.1%, 2,022,979G)

white bets:
dire_drenz: 1,875G (21.6%, 1,875G)
gorgewall: 1,500G (17.3%, 1,500G)
evdoggity: 1,000G (11.5%, 21,349G)
mindblownnnn: 890G (10.3%, 7,029G)
upvla: 600G (6.9%, 6,005G)
technominari: 500G (5.8%, 81,279G)
evontno: 500G (5.8%, 11,047G)
silentkaster: 452G (5.2%, 1,860G)
ar_tactic: 400G (4.6%, 94,601G)
ValensEXP_: 356G (4.1%, 952G)
superdevon1: 306G (3.5%, 1,227G)
datadrivenbot: 200G (2.3%, 72,269G)
RunicMagus: 100G (1.2%, 11,071G)

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ANFz
Male
Cancer
62
43
Geomancer
Draw Out
Earplug
Beastmaster
Ignore Height

Giant Axe
Buckler
Feather Hat
Black Costume
Bracer

Local Quake, Static Shock, Quicksand, Sand Storm, Blizzard
Heaven's Cloud, Kikuichimoji



Aldrammech
Monster
Cancer
43
75
Goblin










Mesmaster
Male
Sagittarius
59
45
Samurai
Black Magic
Counter Tackle
Attack UP
Jump+2

Heaven's Cloud

Circlet
Chameleon Robe
Genji Gauntlet

Koutetsu, Bizen Boat
Fire 4, Bolt



Upvla
Male
Virgo
73
46
Oracle
Charge
PA Save
Equip Shield
Move+2

Musk Rod
Ice Shield
Feather Hat
White Robe
Bracer

Poison, Spell Absorb, Doubt Faith, Zombie, Silence Song, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Paralyze
Charge+4, Charge+5

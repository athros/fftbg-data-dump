Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ALY327
Female
Taurus
70
41
Knight
Draw Out
Critical Quick
Equip Sword
Lava Walking

Ragnarok
Buckler
Iron Helmet
Carabini Mail
Defense Armlet

Head Break, Armor Break, Weapon Break
Bizen Boat, Heaven's Cloud



Mathlexis
Male
Serpentarius
37
66
Wizard
Draw Out
Mana Shield
Doublehand
Levitate

Flame Rod

Flash Hat
Wizard Robe
Genji Gauntlet

Fire 2, Fire 3, Bolt 2, Ice 2, Empower, Flare
Bizen Boat



Reddwind
Male
Sagittarius
53
69
Knight
Item
Meatbone Slash
Equip Polearm
Jump+2

Cashmere
Gold Shield
Mythril Helmet
Linen Cuirass
Power Wrist

Dark Sword
Potion, X-Potion, Echo Grass, Holy Water, Remedy, Phoenix Down



VolgraTheMoose
Male
Leo
59
80
Archer
Elemental
Counter
Doublehand
Jump+2

Ultimus Bow

Cachusha
Earth Clothes
Leather Mantle

Charge+3, Charge+5, Charge+20
Water Ball, Hell Ivy, Local Quake, Gusty Wind

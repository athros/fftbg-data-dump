Final Bets: red - 23 bets for 21,008G (79.3%, x0.26); yellow - 11 bets for 5,483G (20.7%, x3.83)

red bets:
nhammen: 8,000G (38.1%, 435,479G)
snkey: 2,452G (11.7%, 24,518G)
latebit: 2,000G (9.5%, 4,912G)
brenogarwin: 1,919G (9.1%, 1,919G)
BirbBrainsBot: 1,000G (4.8%, 65,639G)
Laserman1000: 623G (3.0%, 3,223G)
Phi_Sig: 555G (2.6%, 30,253G)
OneHundredFists: 549G (2.6%, 549G)
VMuliadi: 500G (2.4%, 1,685G)
DejaPoo21: 452G (2.2%, 452G)
Rikrizen: 409G (1.9%, 8,189G)
CorpusCav: 400G (1.9%, 20,598G)
reddwind_: 384G (1.8%, 1,669G)
BartTradingCompany: 277G (1.3%, 2,778G)
killth3kid: 250G (1.2%, 22,041G)
minorfffanatic1: 216G (1.0%, 216G)
datadrivenbot: 200G (1.0%, 53,898G)
ArrenJevleth: 200G (1.0%, 4,111G)
Leonidusx: 200G (1.0%, 26,975G)
resjudicata3: 200G (1.0%, 484G)
seppu777: 111G (0.5%, 2,396G)
gorgewall: 101G (0.5%, 4,024G)
dinin991: 10G (0.0%, 3,879G)

yellow bets:
AllInBot: 2,580G (47.1%, 2,580G)
reinoe: 779G (14.2%, 779G)
Firesheath: 536G (9.8%, 536G)
SephDarkheart: 368G (6.7%, 8,193G)
krombobreaker: 328G (6.0%, 328G)
Sietaha: 250G (4.6%, 10,766G)
Omally_: 216G (3.9%, 561G)
ValensEXP_: 204G (3.7%, 204G)
akash7: 100G (1.8%, 1,346G)
dogsandcatsand: 100G (1.8%, 4,125G)
getthemoneyz: 22G (0.4%, 1,859,907G)

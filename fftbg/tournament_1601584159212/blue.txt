Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Lord Gwarth
Male
Pisces
65
53
Knight
Basic Skill
Mana Shield
Equip Axe
Move+3

Oak Staff
Crystal Shield
Gold Helmet
Wizard Robe
Elf Mantle

Armor Break, Power Break, Mind Break, Stasis Sword
Dash, Throw Stone, Heal



Powergems
Female
Pisces
80
50
Samurai
White Magic
Absorb Used MP
Concentrate
Ignore Height

Muramasa

Platinum Helmet
Plate Mail
Wizard Mantle

Asura, Koutetsu, Heaven's Cloud
Cure 3, Raise, Reraise, Regen, Protect, Shell 2, Wall, Esuna



DavenIII
Male
Taurus
71
53
Samurai
Basic Skill
Catch
Attack UP
Jump+1

Partisan

Cross Helmet
Wizard Robe
Defense Armlet

Asura, Bizen Boat, Murasame, Kiyomori, Kikuichimoji
Accumulate, Heal, Tickle, Yell



Thyrandaal
Female
Pisces
44
57
Wizard
Summon Magic
Abandon
Maintenance
Move-MP Up

Wizard Rod

Feather Hat
Wizard Robe
Salty Rage

Fire, Fire 2, Fire 3, Bolt 2, Bolt 3, Ice, Empower
Titan, Carbunkle

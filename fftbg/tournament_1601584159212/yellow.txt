Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Krombobreaker
Female
Capricorn
79
79
Oracle
Draw Out
Damage Split
Equip Sword
Waterwalking

Bizen Boat

Holy Miter
White Robe
Small Mantle

Poison, Pray Faith, Doubt Faith, Zombie, Blind Rage, Foxbird, Dispel Magic, Dark Holy
Asura, Murasame



Pplvee1
Male
Cancer
51
79
Thief
Charge
MP Restore
Concentrate
Teleport

Kunai

Triangle Hat
Clothes
Elf Mantle

Steal Helmet, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Charge+1, Charge+2, Charge+5, Charge+20



E Ballard
Male
Aries
51
75
Calculator
Lucavi Skill
Distribute
Equip Gun
Jump+3

Battle Folio
Crystal Shield
Crystal Helmet
Genji Armor
Diamond Armlet

Blue Magic
Melt, Tornado, Quake, Flare 2, Demi 3, Grand Cross, All-Ultima



SQUiDSQUARKLIN
Female
Leo
61
67
Time Mage
Basic Skill
Brave Save
Short Charge
Waterbreathing

Gokuu Rod

Triangle Hat
Light Robe
Feather Mantle

Haste, Haste 2, Slow, Slow 2, Immobilize, Float, Demi 2
Accumulate, Heal, Cheer Up, Wish

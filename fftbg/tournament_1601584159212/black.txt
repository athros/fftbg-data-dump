Player: !Black
Team: Black Team
Palettes: Black/Red



BuffaloCrunch
Male
Aquarius
48
55
Monk
Talk Skill
Distribute
Sicken
Jump+3



Ribbon
Mystic Vest
Dracula Mantle

Pummel, Earth Slash, Chakra
Invitation, Preach, Solution, Rehabilitate



CosmicTactician
Female
Sagittarius
60
74
Knight
Jump
MP Restore
Attack UP
Retreat

Defender
Bronze Shield
Bronze Helmet
Wizard Robe
Genji Gauntlet

Mind Break, Dark Sword
Level Jump3, Vertical Jump7



Gorgewall
Male
Libra
71
52
Time Mage
Charge
Sunken State
Sicken
Move-HP Up

Gokuu Rod

Flash Hat
White Robe
Leather Mantle

Haste 2, Slow, Immobilize, Quick, Stabilize Time
Charge+1, Charge+3, Charge+4, Charge+10, Charge+20



Panushenko
Male
Aquarius
47
56
Archer
Basic Skill
Dragon Spirit
Dual Wield
Waterwalking

Hunting Bow
Poison Bow
Leather Hat
Clothes
Genji Gauntlet

Charge+2, Charge+4
Accumulate, Heal, Tickle, Cheer Up, Fury

Player: !Black
Team: Black Team
Palettes: Black/Red



Nhammen
Female
Taurus
75
61
Priest
Draw Out
Counter
Defense UP
Waterbreathing

Healing Staff

Red Hood
Judo Outfit
Defense Armlet

Cure, Cure 2, Cure 4, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Wall, Esuna, Holy
Koutetsu, Murasame, Kiyomori, Kikuichimoji



Runeseeker22
Male
Aries
55
51
Knight
Time Magic
Counter Tackle
Long Status
Ignore Height

Battle Axe
Mythril Shield
Diamond Helmet
Diamond Armor
Bracer

Armor Break, Power Break, Stasis Sword, Justice Sword, Explosion Sword
Haste, Slow, Reflect



Kellios11
Female
Taurus
55
58
Lancer
Basic Skill
Parry
Defend
Move-MP Up

Holy Lance
Mythril Shield
Bronze Helmet
Linen Cuirass
Defense Armlet

Level Jump4, Vertical Jump4
Accumulate, Cheer Up



Roofiepops
Male
Virgo
69
55
Summoner
Steal
Earplug
Short Charge
Retreat

Gold Staff

Black Hood
Wizard Robe
Sprint Shoes

Moogle, Carbunkle, Fairy, Cyclops
Steal Heart, Steal Helmet, Steal Weapon, Steal Accessory, Leg Aim

Player: !Black
Team: Black Team
Palettes: Black/Red



LivingHitokiri
Female
Aries
49
66
Dancer
Item
Speed Save
Concentrate
Waterbreathing

Persia

Green Beret
Light Robe
Magic Ring

Witch Hunt, Nameless Dance, Nether Demon, Dragon Pit
Potion, Hi-Potion, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down



BuffaloCrunch
Female
Scorpio
57
48
Geomancer
Battle Skill
Distribute
Defend
Teleport

Giant Axe
Ice Shield
Golden Hairpin
Light Robe
Leather Mantle

Pitfall, Hell Ivy, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Speed Break, Stasis Sword, Justice Sword



DarrenDinosaurs
Male
Virgo
43
81
Squire
Charge
Distribute
Secret Hunt
Jump+3

Mythril Sword
Crystal Shield
Leather Hat
Mythril Armor
Dracula Mantle

Accumulate, Dash, Throw Stone, Heal, Cheer Up, Scream
Charge+1, Charge+2, Charge+4, Charge+5



Nekojin
Monster
Taurus
53
73
Dark Behemoth








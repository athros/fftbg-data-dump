Final Bets: purple - 8 bets for 3,333G (37.7%, x1.65); brown - 11 bets for 5,501G (62.3%, x0.61)

purple bets:
BirbBrainsBot: 1,000G (30.0%, 164,360G)
lowlf: 632G (19.0%, 11,804G)
AllInBot: 562G (16.9%, 562G)
ColetteMSLP: 480G (14.4%, 7,960G)
TheSabretoothe: 292G (8.8%, 292G)
Aaron2Nerdy: 156G (4.7%, 156G)
PantherIscariot: 111G (3.3%, 3,212G)
b0lt: 100G (3.0%, 100G)

brown bets:
OneHundredFists: 2,000G (36.4%, 22,851G)
Deathmaker06: 1,000G (18.2%, 37,975G)
upvla: 600G (10.9%, 600G)
lijarkh: 500G (9.1%, 500G)
Lythe_Caraker: 320G (5.8%, 320G)
getthemoneyz: 248G (4.5%, 2,131,291G)
Evewho: 200G (3.6%, 2,489G)
Palladigm: 200G (3.6%, 10,563G)
datadrivenbot: 200G (3.6%, 81,592G)
Batshine: 132G (2.4%, 132G)
gorgewall: 101G (1.8%, 10,454G)

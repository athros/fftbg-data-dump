Player: !Green
Team: Green Team
Palettes: Green/White



Leaferrm
Male
Aries
43
65
Mime

Arrow Guard
Maintenance
Ignore Terrain



Flash Hat
Rubber Costume
108 Gems

Mimic




TheBrett
Female
Taurus
75
49
Wizard
Summon Magic
Counter Flood
Attack UP
Waterwalking

Ice Rod

Ribbon
Black Costume
Angel Ring

Fire 2, Bolt, Bolt 2, Bolt 3, Ice 4, Empower, Death
Carbunkle, Bahamut, Silf



Zachara
Male
Taurus
75
63
Lancer
Charge
Brave Save
Dual Wield
Waterbreathing

Battle Bamboo
Dragon Whisker
Diamond Helmet
Mythril Armor
Battle Boots

Level Jump2, Vertical Jump5
Charge+2, Charge+3, Charge+4



Ominnous
Male
Capricorn
70
67
Knight
Throw
Sunken State
Beastmaster
Move+3

Ragnarok
Gold Shield
Crystal Helmet
Linen Cuirass
Rubber Shoes

Head Break, Magic Break, Speed Break, Surging Sword
Shuriken, Staff, Ninja Sword, Wand

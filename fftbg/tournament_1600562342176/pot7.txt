Final Bets: red - 11 bets for 6,746G (49.0%, x1.04); black - 10 bets for 7,025G (51.0%, x0.96)

red bets:
Zerguzen: 1,500G (22.2%, 14,936G)
latebit: 1,280G (19.0%, 2,511G)
CorpusCav: 1,201G (17.8%, 1,201G)
AniZero: 1,000G (14.8%, 34,420G)
Smashy: 500G (7.4%, 5,285G)
SephDarkheart: 364G (5.4%, 35,266G)
BirbBrainsBot: 333G (4.9%, 148,755G)
AllInBot: 200G (3.0%, 200G)
datadrivenbot: 200G (3.0%, 71,322G)
MemoriesofFinal: 100G (1.5%, 30,124G)
getthemoneyz: 68G (1.0%, 1,989,638G)

black bets:
CT_5_Holy: 1,338G (19.0%, 5,355G)
mindblownnnn: 1,000G (14.2%, 5,443G)
loveyouallfriends: 1,000G (14.2%, 44,321G)
Shalloween: 1,000G (14.2%, 4,938G)
OneHundredFists: 972G (13.8%, 972G)
SkylerBunny: 716G (10.2%, 716G)
krombobreaker: 500G (7.1%, 4,364G)
ValensEXP_: 348G (5.0%, 5,658G)
gorgewall: 101G (1.4%, 9,433G)
Lord_Gwarth: 50G (0.7%, 2,524G)

Player: !White
Team: White Team
Palettes: White/Blue



Blaster Bomb
Male
Taurus
75
48
Knight
Yin Yang Magic
Counter Flood
Concentrate
Swim

Defender
Buckler
Platinum Helmet
Chain Mail
Battle Boots

Head Break, Weapon Break, Magic Break, Mind Break, Stasis Sword
Blind, Poison, Pray Faith, Zombie, Silence Song, Foxbird



CassiePhoenix
Male
Aries
55
81
Monk
Elemental
Critical Quick
Dual Wield
Jump+1



Leather Hat
Wizard Outfit
Bracer

Pummel, Revive
Water Ball, Local Quake, Will-O-Wisp, Blizzard, Lava Ball



Tithonus
Female
Pisces
76
57
Ninja
Black Magic
Speed Save
Equip Bow
Move+2

Ultimus Bow

Black Hood
Wizard Outfit
Feather Mantle

Shuriken, Bomb, Staff, Wand, Dictionary
Fire 2, Bolt 2, Bolt 3, Ice, Flare



Dru
Male
Gemini
79
74
Mediator
Battle Skill
Counter Magic
Equip Polearm
Fly

Battle Bamboo

Black Hood
White Robe
Elf Mantle

Solution, Death Sentence, Insult, Mimic Daravon, Rehabilitate
Head Break, Shield Break, Weapon Break, Power Break, Mind Break

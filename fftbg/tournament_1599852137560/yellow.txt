Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Acid Flashback
Female
Virgo
41
78
Summoner
Time Magic
Counter
Beastmaster
Lava Walking

Bestiary

Flash Hat
Light Robe
Feather Mantle

Moogle, Ramuh, Golem, Carbunkle, Silf, Cyclops
Slow, Stop, Reflect, Demi



Lythe Caraker
Female
Libra
48
64
Dancer
Charge
Distribute
Concentrate
Ignore Terrain

Ryozan Silk

Green Beret
Earth Clothes
Wizard Mantle

Wiznaibus, Nameless Dance, Nether Demon, Dragon Pit
Charge+3, Charge+4



Laserman1000
Female
Sagittarius
55
58
Thief
Time Magic
Speed Save
Equip Gun
Move+3

Papyrus Codex

Red Hood
Judo Outfit
Red Shoes

Steal Weapon, Steal Accessory, Steal Status, Leg Aim
Slow, Immobilize, Stabilize Time



SkylerBunny
Female
Taurus
71
64
Summoner
Black Magic
MA Save
Martial Arts
Levitate

Wizard Rod

Feather Hat
Black Robe
Dracula Mantle

Moogle, Shiva, Carbunkle, Fairy
Fire, Fire 3, Ice, Ice 2, Ice 3, Flare

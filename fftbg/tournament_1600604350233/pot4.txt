Final Bets: purple - 11 bets for 5,938G (64.1%, x0.56); brown - 8 bets for 3,324G (35.9%, x1.79)

purple bets:
OneHundredFists: 1,775G (29.9%, 1,775G)
FrozenFelid: 1,000G (16.8%, 7,549G)
nifboy: 792G (13.3%, 8,539G)
DaveStrider55: 722G (12.2%, 722G)
Afro_Gundam: 527G (8.9%, 527G)
DeathTaxesAndAnime: 384G (6.5%, 2,600G)
getthemoneyz: 238G (4.0%, 2,013,608G)
datadrivenbot: 200G (3.4%, 73,112G)
genkidou: 150G (2.5%, 816G)
king_smashington: 100G (1.7%, 1,028G)
formerlydrdong: 50G (0.8%, 2,624G)

brown bets:
VolgraTheMoose: 1,001G (30.1%, 12,618G)
BirbBrainsBot: 1,000G (30.1%, 162,083G)
skipsandwiches: 428G (12.9%, 428G)
Zachara: 345G (10.4%, 145,145G)
AllInBot: 200G (6.0%, 200G)
MemoriesofFinal: 200G (6.0%, 30,510G)
lwtest: 100G (3.0%, 4,299G)
Lord_Gwarth: 50G (1.5%, 1,735G)

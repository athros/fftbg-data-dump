Player: !Black
Team: Black Team
Palettes: Black/Red



RobotOcelot
Male
Gemini
67
75
Thief
Draw Out
Counter Magic
Equip Armor
Move+1

Air Knife

Genji Helmet
Leather Outfit
Jade Armlet

Gil Taking, Steal Helmet, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim
Bizen Boat, Murasame, Kikuichimoji



Nickyfive
Female
Pisces
72
58
Monk
Black Magic
Absorb Used MP
Equip Shield
Lava Walking

Hydra Bag
Crystal Shield
Black Hood
Clothes
Diamond Armlet

Spin Fist, Pummel, Earth Slash, Secret Fist, Purification, Chakra
Fire 4, Bolt 4, Ice 2, Death



Chihuahua Charity
Male
Cancer
69
58
Oracle
Basic Skill
Catch
Equip Axe
Move+3

Rainbow Staff

Triangle Hat
Wizard Robe
Battle Boots

Poison, Life Drain, Zombie, Confusion Song, Dispel Magic, Paralyze, Sleep, Petrify
Throw Stone, Heal, Yell, Fury



LivingHitokiri
Female
Gemini
80
67
Archer
Basic Skill
Counter Tackle
Short Status
Jump+2

Windslash Bow

Leather Helmet
Judo Outfit
Spike Shoes

Charge+1
Accumulate, Fury, Wish

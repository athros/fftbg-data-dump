Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Leonidusx
Male
Scorpio
60
76
Knight
Jump
Counter Flood
Beastmaster
Ignore Terrain

Slasher
Buckler
Grand Helmet
Linen Robe
Cursed Ring

Shield Break, Power Break, Stasis Sword, Justice Sword
Level Jump4, Vertical Jump2



RunicMagus
Monster
Gemini
57
56
Red Chocobo










Gelwain
Female
Cancer
60
73
Summoner
Jump
Critical Quick
Short Charge
Retreat

Wizard Rod

Feather Hat
Robe of Lords
Genji Gauntlet

Moogle, Bahamut, Odin, Leviathan, Salamander, Silf, Fairy
Level Jump3, Vertical Jump8



Brokenknight201
Female
Pisces
58
39
Knight
Jump
Counter Tackle
Defense UP
Jump+3

Blood Sword
Gold Shield
Barbuta
Diamond Armor
Jade Armlet

Armor Break, Magic Break, Speed Break, Stasis Sword, Justice Sword
Level Jump3, Vertical Jump2

Player: !zChamp
Team: Champion Team
Palettes: Red/Brown



PopsLIVE
Monster
Capricorn
74
66
Gobbledeguck










Run With Stone GUNs
Male
Cancer
79
63
Mediator
Steal
Mana Shield
Dual Wield
Move-MP Up

Mythril Gun
Stone Gun
Thief Hat
Rubber Costume
Salty Rage

Threaten, Preach, Solution, Mimic Daravon, Refute, Rehabilitate
Steal Heart, Arm Aim, Leg Aim



Zeroroute
Female
Pisces
49
52
Calculator
Yin Yang Magic
Counter Tackle
Short Charge
Jump+1

Bestiary

Leather Hat
Silk Robe
108 Gems

CT, Height, Prime Number, 5, 3
Poison, Doubt Faith, Silence Song, Blind Rage, Foxbird, Paralyze



HaateXIII
Male
Scorpio
50
49
Squire
Time Magic
MP Restore
Equip Gun
Ignore Terrain

Bloody Strings
Flame Shield
Green Beret
Adaman Vest
N-Kai Armlet

Throw Stone, Heal, Yell, Cheer Up
Slow, Slow 2, Stop, Demi, Demi 2

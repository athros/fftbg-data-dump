Final Bets: white - 11 bets for 5,285G (61.5%, x0.63); black - 6 bets for 3,306G (38.5%, x1.60)

white bets:
ColetteMSLP: 1,040G (19.7%, 1,040G)
AllInBot: 826G (15.6%, 826G)
Shalloween: 825G (15.6%, 1,651G)
VolgraTheMoose: 501G (9.5%, 13,973G)
krombobreaker: 500G (9.5%, 5,215G)
BirbBrainsBot: 489G (9.3%, 126,592G)
ValensEXP_: 348G (6.6%, 4,071G)
J_MacC1991: 276G (5.2%, 276G)
killth3kid: 250G (4.7%, 8,148G)
datadrivenbot: 200G (3.8%, 71,795G)
7Cerulean7: 30G (0.6%, 6,200G)

black bets:
Error72: 1,001G (30.3%, 32,989G)
thunderducker: 600G (18.1%, 600G)
OneHundredFists: 592G (17.9%, 1,161G)
CorpusCav: 555G (16.8%, 8,174G)
getthemoneyz: 458G (13.9%, 1,990,339G)
arumz: 100G (3.0%, 36,049G)

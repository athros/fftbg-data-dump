Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



ChiChiRodriguez
Female
Libra
65
68
Samurai
Yin Yang Magic
Parry
Equip Shield
Move+3

Heaven's Cloud
Mythril Shield
Leather Helmet
Carabini Mail
Germinas Boots

Asura, Bizen Boat, Heaven's Cloud, Muramasa
Poison, Dispel Magic, Dark Holy



Lygg Mubblez
Male
Pisces
61
45
Thief
Black Magic
Caution
Equip Axe
Teleport

Flail

Feather Hat
Leather Outfit
Defense Ring

Gil Taking, Steal Helmet, Steal Armor, Steal Shield, Steal Status, Leg Aim
Fire 4, Bolt, Bolt 2, Bolt 3, Ice, Ice 3, Empower



NovaKnight21
Female
Libra
79
42
Geomancer
Talk Skill
Catch
Monster Talk
Jump+3

Giant Axe
Genji Shield
Triangle Hat
Light Robe
Bracer

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball
Invitation, Persuade, Death Sentence, Negotiate



HASTERIOUS
Female
Sagittarius
58
66
Chemist
Charge
Earplug
Defend
Teleport

Romanda Gun

Green Beret
Brigandine
Red Shoes

Potion, Hi-Potion, Ether, Antidote, Echo Grass, Soft, Holy Water, Remedy
Charge+4, Charge+5

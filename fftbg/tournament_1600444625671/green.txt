Player: !Green
Team: Green Team
Palettes: Green/White



Wonser
Male
Taurus
67
65
Chemist
White Magic
Counter Magic
Beastmaster
Jump+3

Blast Gun

Triangle Hat
Judo Outfit
Wizard Mantle

Potion, X-Potion, Hi-Ether, Antidote, Eye Drop, Echo Grass, Soft, Holy Water, Phoenix Down
Cure 4, Raise 2, Esuna, Holy



Thyrandaal
Male
Sagittarius
60
78
Ninja
Charge
Caution
Equip Gun
Ignore Height

Papyrus Codex
Bestiary
Triangle Hat
Black Costume
Dracula Mantle

Knife, Sword
Charge+1, Charge+2, Charge+3



ZCKaiser
Female
Cancer
57
66
Priest
Summon Magic
Regenerator
Doublehand
Move-MP Up

Flail

Twist Headband
Wizard Robe
Germinas Boots

Cure 2, Raise, Shell, Esuna, Magic Barrier
Shiva, Fairy



ValensEXP
Male
Aquarius
65
44
Knight
White Magic
Auto Potion
Dual Wield
Jump+2

Ragnarok
Diamond Sword
Gold Helmet
Diamond Armor
Red Shoes

Weapon Break, Speed Break, Power Break
Cure, Cure 3, Cure 4, Raise, Raise 2, Reraise, Regen, Protect 2, Shell 2, Wall, Esuna, Holy

Player: !White
Team: White Team
Palettes: White/Blue



CosmicTactician
Female
Aquarius
59
41
Time Mage
Draw Out
Auto Potion
Beastmaster
Move+1

Musk Rod

Holy Miter
Judo Outfit
Magic Gauntlet

Immobilize, Stabilize Time
Kiyomori



PoroTact
Female
Gemini
49
79
Dancer
Elemental
Dragon Spirit
Attack UP
Move+3

Hydra Bag

Thief Hat
Chameleon Robe
Feather Boots

Slow Dance, Nether Demon, Dragon Pit
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Sand Storm, Blizzard, Lava Ball



ValensEXP
Male
Scorpio
80
42
Thief
Throw
Counter Tackle
Short Status
Levitate

Blind Knife

Thief Hat
Black Costume
Feather Mantle

Steal Weapon, Steal Status, Leg Aim
Shuriken, Knife, Axe



Runeseeker22
Male
Sagittarius
60
71
Monk
Time Magic
PA Save
Magic Defense UP
Ignore Height



Green Beret
Brigandine
Jade Armlet

Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil
Haste, Slow, Reflect, Demi 2, Stabilize Time

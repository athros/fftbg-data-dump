Final Bets: red - 19 bets for 12,421G (34.8%, x1.87); blue - 16 bets for 23,248G (65.2%, x0.53)

red bets:
upvla: 2,000G (16.1%, 2,920G)
Spartan_Paladin: 1,500G (12.1%, 17,507G)
CorpusCav: 1,200G (9.7%, 22,472G)
Error72: 1,001G (8.1%, 45,638G)
DeathTaxesAndAnime: 1,001G (8.1%, 18,647G)
seppu777: 777G (6.3%, 12,695G)
VolgraTheMoose: 772G (6.2%, 772G)
LDSkinny: 644G (5.2%, 644G)
PoroTact: 544G (4.4%, 544G)
lowlf: 520G (4.2%, 520G)
JonnyCue: 500G (4.0%, 2,363G)
evontno: 400G (3.2%, 600G)
Cryptopsy70: 372G (3.0%, 642G)
SephDarkheart: 340G (2.7%, 24,594G)
killth3kid: 250G (2.0%, 13,753G)
datadrivenbot: 200G (1.6%, 65,583G)
CosmicTactician: 200G (1.6%, 30,208G)
AllInBot: 100G (0.8%, 100G)
mykelwolfscar: 100G (0.8%, 286G)

blue bets:
nhammen: 7,000G (30.1%, 351,365G)
Shalloween: 6,766G (29.1%, 6,766G)
reinoe: 3,000G (12.9%, 12,711G)
Smashy: 1,101G (4.7%, 1,101G)
BirbBrainsBot: 1,000G (4.3%, 171,464G)
Nizaha: 836G (3.6%, 67,604G)
roofiepops: 790G (3.4%, 790G)
DavenIII: 632G (2.7%, 632G)
Stiqqery: 601G (2.6%, 1,202G)
holdenmagronik: 436G (1.9%, 10,718G)
LivingHitokiri: 316G (1.4%, 316G)
ValensEXP_: 312G (1.3%, 4,082G)
getthemoneyz: 158G (0.7%, 1,945,539G)
BartTradingCompany: 100G (0.4%, 10,399G)
maakur_: 100G (0.4%, 4,486G)
latebit: 100G (0.4%, 3,741G)

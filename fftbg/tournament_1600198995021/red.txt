Player: !Red
Team: Red Team
Palettes: Red/Brown



Run With Stone GUNs
Male
Capricorn
41
64
Chemist
Time Magic
PA Save
Defense UP
Fly

Mage Masher

Red Hood
Secret Clothes
Leather Mantle

Potion, Hi-Potion, X-Potion, Ether, Eye Drop, Echo Grass, Maiden's Kiss, Holy Water, Phoenix Down
Immobilize, Reflect, Demi, Demi 2, Stabilize Time



Mrfripps
Male
Taurus
51
57
Time Mage
White Magic
Brave Save
Equip Sword
Jump+1

Kiyomori

Red Hood
Silk Robe
108 Gems

Haste 2, Stop, Float, Demi 2, Stabilize Time, Meteor
Reraise, Regen, Shell, Shell 2, Esuna



Error72
Female
Aquarius
60
79
Time Mage
Yin Yang Magic
Counter Flood
Maintenance
Jump+1

Mace of Zeus

Flash Hat
Wizard Robe
Feather Mantle

Haste 2, Slow 2, Stop, Immobilize, Demi 2, Stabilize Time
Spell Absorb, Life Drain, Doubt Faith, Zombie, Dispel Magic



DeathTaxesAndAnime
Female
Cancer
75
41
Wizard
Math Skill
Brave Save
Equip Sword
Swim

Coral Sword

Thief Hat
Black Robe
Red Shoes

Fire 4, Bolt, Bolt 3, Ice
CT, Prime Number, 5, 4, 3

Player: !White
Team: White Team
Palettes: White/Blue



Chuckolator
Female
Aries
79
56
Priest
Math Skill
Blade Grasp
Doublehand
Move+1

Healing Staff

Triangle Hat
Silk Robe
N-Kai Armlet

Cure 4, Raise, Regen, Esuna
CT, Height, Prime Number, 5, 4, 3



Musashi45
Male
Libra
47
48
Summoner
Yin Yang Magic
Damage Split
Equip Polearm
Waterbreathing

Holy Lance

Twist Headband
Judo Outfit
Feather Mantle

Moogle, Ramuh, Ifrit, Titan, Golem, Carbunkle, Fairy
Blind, Blind Rage, Dispel Magic, Sleep



CapnChaos12
Female
Leo
74
77
Calculator
White Magic
HP Restore
Defend
Ignore Height

Papyrus Codex

Black Hood
White Robe
Battle Boots

CT, Height, Prime Number, 5, 3
Cure 2, Cure 3, Raise, Shell 2, Esuna, Holy



CorpusCav
Female
Libra
75
52
Chemist
Battle Skill
PA Save
Equip Polearm
Levitate

Cashmere

Green Beret
Black Costume
108 Gems

Hi-Potion, X-Potion, Ether, Maiden's Kiss, Phoenix Down
Armor Break, Shield Break, Weapon Break, Magic Break, Mind Break, Dark Sword

Player: !zChamp
Team: Champion Team
Palettes: Black/Red



ColetteMSLP
Female
Scorpio
62
56
Thief
Basic Skill
Parry
Equip Polearm
Jump+3

Partisan

Feather Hat
Power Sleeve
Leather Mantle

Steal Armor, Steal Weapon, Steal Accessory, Arm Aim, Leg Aim
Dash, Throw Stone, Heal, Cheer Up



Forkmore
Female
Pisces
62
55
Summoner
White Magic
HP Restore
Short Charge
Retreat

Poison Rod

Leather Hat
Adaman Vest
Elf Mantle

Moogle, Ramuh, Ifrit, Golem, Carbunkle, Bahamut, Silf, Fairy, Lich, Cyclops
Cure, Raise, Raise 2, Reraise, Regen, Protect, Protect 2, Shell 2, Esuna



ANFz
Male
Libra
50
72
Mime

MP Restore
Equip Armor
Teleport



Flash Hat
Bronze Armor
Bracer

Mimic




Ribbiks
Female
Scorpio
59
52
Dancer
Draw Out
Hamedo
Halve MP
Jump+3

Persia

Green Beret
Adaman Vest
Small Mantle

Witch Hunt, Slow Dance, Nameless Dance
Heaven's Cloud

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Macal0ser
Monster
Aquarius
68
59
Dark Behemoth










Zetchryn
Male
Gemini
45
45
Bard
White Magic
Parry
Defense UP
Waterbreathing

Bloody Strings

Twist Headband
Judo Outfit
Diamond Armlet

Angel Song, Life Song, Battle Song, Magic Song, Sky Demon
Cure 2, Raise, Protect 2, Shell, Esuna, Holy



Breakdown777
Female
Scorpio
46
64
Ninja
Dance
Critical Quick
Equip Gun
Fly

Papyrus Codex
Papyrus Codex
Green Beret
Power Sleeve
Magic Ring

Bomb, Staff
Obsidian Blade, Void Storage, Dragon Pit



StealthModeLocke
Female
Scorpio
66
65
Thief
Summon Magic
Absorb Used MP
Dual Wield
Move+1

Assassin Dagger
Cultist Dagger
Twist Headband
Mystic Vest
Cursed Ring

Gil Taking, Steal Heart, Steal Helmet, Steal Accessory, Arm Aim
Ifrit, Titan, Silf, Cyclops

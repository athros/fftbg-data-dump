Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Technominari
Female
Virgo
68
52
Priest
Punch Art
MP Restore
Equip Polearm
Swim

Musk Rod

Barette
Leather Outfit
Germinas Boots

Cure, Cure 3, Raise 2, Shell, Shell 2, Esuna, Holy, Magic Barrier
Pummel, Purification, Chakra, Seal Evil



Zeroroute
Male
Libra
47
46
Knight
Sing
Counter
Magic Attack UP
Swim

Battle Axe
Flame Shield
Gold Helmet
Genji Armor
Feather Mantle

Head Break, Armor Break, Shield Break, Magic Break, Speed Break, Stasis Sword, Justice Sword
Cheer Song, Battle Song, Nameless Song, Diamond Blade, Sky Demon



Ruleof5
Female
Gemini
61
68
Archer
Time Magic
Absorb Used MP
Doublehand
Move+1

Poison Bow

Red Hood
Mystic Vest
Spike Shoes

Charge+1, Charge+4, Charge+7
Haste, Haste 2, Slow, Quick, Demi 2, Stabilize Time, Meteor



Projekt Mike
Male
Virgo
61
60
Lancer
Summon Magic
Regenerator
Equip Polearm
Ignore Terrain

Obelisk
Platinum Shield
Genji Helmet
Bronze Armor
Spike Shoes

Level Jump2, Vertical Jump2
Shiva, Carbunkle

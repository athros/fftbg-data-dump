Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Jeeboheebo
Female
Taurus
55
77
Mime

Blade Grasp
Dual Wield
Move+2



Red Hood
Wizard Robe
Setiemson

Mimic




LDSkinny
Female
Aries
50
41
Oracle
Item
Counter Tackle
Halve MP
Levitate

Iron Fan

Red Hood
Chameleon Robe
Bracer

Poison, Spell Absorb, Life Drain, Zombie, Silence Song, Foxbird, Confusion Song, Dispel Magic, Petrify
Potion, Hi-Potion, Echo Grass, Maiden's Kiss, Soft



Powergems
Male
Cancer
60
70
Summoner
Black Magic
Counter Tackle
Short Charge
Ignore Height

Papyrus Codex

Black Hood
Chameleon Robe
Defense Armlet

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Silf, Fairy, Zodiac
Ice, Death



Error72
Male
Capricorn
47
68
Mime

Meatbone Slash
Doublehand
Lava Walking



Diamond Helmet
Secret Clothes
Power Wrist

Mimic


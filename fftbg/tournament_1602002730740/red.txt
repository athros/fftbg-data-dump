Player: !Red
Team: Red Team
Palettes: Red/Brown



Randgridr
Male
Gemini
46
77
Mime

Brave Save
Maintenance
Swim



Black Hood
Rubber Costume
Power Wrist

Mimic




Nhammen
Female
Aquarius
43
46
Geomancer
Basic Skill
Mana Shield
Defense UP
Levitate

Diamond Sword
Diamond Shield
Golden Hairpin
Chameleon Robe
Germinas Boots

Pitfall, Water Ball, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Blizzard, Gusty Wind, Lava Ball
Dash, Throw Stone, Heal



Fenaen
Male
Gemini
42
65
Mime

HP Restore
Martial Arts
Move+1



Green Beret
Mystic Vest
Battle Boots

Mimic




Sharosa
Male
Aries
79
60
Lancer
Draw Out
Arrow Guard
Equip Knife
Waterbreathing

Wizard Rod
Gold Shield
Diamond Helmet
Linen Cuirass
Spike Shoes

Level Jump4, Vertical Jump2
Koutetsu, Heaven's Cloud

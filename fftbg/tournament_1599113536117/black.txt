Player: !Black
Team: Black Team
Palettes: Black/Red



3ngag3
Male
Virgo
42
71
Squire
Punch Art
Speed Save
Halve MP
Waterwalking

Flame Whip
Aegis Shield
Genji Helmet
Carabini Mail
Diamond Armlet

Accumulate, Dash, Heal, Tickle, Scream
Pummel, Purification, Chakra



0v3rr8d
Male
Taurus
70
60
Lancer
Throw
MA Save
Attack UP
Fly

Holy Lance
Aegis Shield
Crystal Helmet
Gold Armor
Germinas Boots

Level Jump5, Vertical Jump7
Hammer



HASTERIOUS
Male
Aries
44
57
Lancer
Sing
Parry
Martial Arts
Jump+1

Mythril Spear
Genji Shield
Mythril Helmet
Carabini Mail
Magic Gauntlet

Level Jump8, Vertical Jump8
Angel Song, Life Song, Cheer Song



Holandrix
Male
Virgo
67
73
Mediator
Black Magic
PA Save
Equip Gun
Ignore Height

Bestiary

Holy Miter
Silk Robe
Small Mantle

Invitation, Persuade, Praise, Threaten, Insult
Fire, Fire 2, Bolt 2, Ice

Final Bets: black - 13 bets for 9,036G (49.9%, x1.00); brown - 3 bets for 9,070G (50.1%, x1.00)

black bets:
Raixelol: 2,157G (23.9%, 2,157G)
turquoise_mage: 1,000G (11.1%, 1,351G)
BaronHaynes: 1,000G (11.1%, 53,518G)
BirbBrainsBot: 1,000G (11.1%, 62,495G)
reinoe: 1,000G (11.1%, 47,588G)
CT_5_Holy: 672G (7.4%, 13,444G)
3ngag3: 549G (6.1%, 1,549G)
thunderducker: 500G (5.5%, 2,533G)
getthemoneyz: 328G (3.6%, 1,813,374G)
macradarkstar: 292G (3.2%, 292G)
PuzzleSecretary: 238G (2.6%, 238G)
datadrivenbot: 200G (2.2%, 65,542G)
DeathTaxesAndAnime: 100G (1.1%, 18,507G)

brown bets:
JonnyCue: 7,500G (82.7%, 7,500G)
lowlf: 1,370G (15.1%, 1,370G)
Yangusburger: 200G (2.2%, 10,025G)

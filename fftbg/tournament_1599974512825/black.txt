Player: !Black
Team: Black Team
Palettes: Black/Red



Superdevon1
Female
Leo
57
39
Geomancer
Basic Skill
Arrow Guard
Equip Armor
Fly

Heaven's Cloud
Ice Shield
Golden Hairpin
Diamond Armor
Feather Mantle

Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Lava Ball
Heal, Tickle



Seaweed B
Monster
Capricorn
61
41
Swine










ArrenJevleth
Female
Virgo
78
46
Squire
Black Magic
Sunken State
Sicken
Jump+2

Ice Brand
Aegis Shield
Green Beret
Leather Outfit
Germinas Boots

Dash, Heal, Tickle, Yell, Cheer Up, Fury, Wish
Fire 3, Bolt 4, Ice 4



ANFz
Male
Pisces
54
80
Geomancer
Throw
Auto Potion
Short Status
Lava Walking

Giant Axe
Flame Shield
Black Hood
Black Robe
Magic Ring

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Gusty Wind, Lava Ball
Bomb, Staff

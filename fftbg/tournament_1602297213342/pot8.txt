Final Bets: blue - 17 bets for 20,813G (63.5%, x0.57); champion - 16 bets for 11,949G (36.5%, x1.74)

blue bets:
DLJuggernaut: 5,000G (24.0%, 78,454G)
OneHundredFists: 4,069G (19.6%, 80,186G)
VolgraTheMoose: 3,185G (15.3%, 6,371G)
Smashy: 2,471G (11.9%, 2,471G)
shineeyo: 1,000G (4.8%, 43,206G)
xXTysongXx: 900G (4.3%, 1,600G)
reinoe: 748G (3.6%, 748G)
Laserman1000: 700G (3.4%, 7,000G)
Zagorsek: 592G (2.8%, 592G)
Rhyser76: 568G (2.7%, 568G)
krombobreaker: 500G (2.4%, 32,943G)
MelTorefas: 251G (1.2%, 2,251G)
JethroThrul: 229G (1.1%, 2,290G)
AllInBot: 200G (1.0%, 200G)
datadrivenbot: 200G (1.0%, 82,982G)
CosmicTactician: 100G (0.5%, 2,464G)
rkane398: 100G (0.5%, 1,182G)

champion bets:
Evewho: 3,000G (25.1%, 55,672G)
DeathTaxesAndAnime: 1,568G (13.1%, 1,568G)
ExplanationMarkWow: 1,269G (10.6%, 1,269G)
BirbBrainsBot: 1,000G (8.4%, 2,601G)
YowaneHaku: 1,000G (8.4%, 4,179G)
E_Ballard: 972G (8.1%, 972G)
GenZealot: 500G (4.2%, 22,926G)
2b_yorha_b: 500G (4.2%, 35,267G)
SRG_Chugganomics: 500G (4.2%, 1,482G)
KGenjy: 400G (3.3%, 8,004G)
Runeseeker22: 392G (3.3%, 19,306G)
otakutaylor: 340G (2.8%, 340G)
getthemoneyz: 168G (1.4%, 2,203,358G)
Lydian_C: 123G (1.0%, 3,486G)
DirkC_Fenrir84: 116G (1.0%, 116G)
gorgewall: 101G (0.8%, 6,168G)

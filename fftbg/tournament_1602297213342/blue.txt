Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Neech
Female
Virgo
66
63
Lancer
Throw
MA Save
Equip Sword
Lava Walking

Defender

Cross Helmet
Leather Armor
Bracer

Level Jump2, Vertical Jump6
Shuriken, Ninja Sword



Krombobreaker
Male
Capricorn
66
50
Bard
Steal
Faith Save
Dual Wield
Move+3

Ramia Harp
Fairy Harp
Headgear
Black Costume
Wizard Mantle

Angel Song, Hydra Pit
Steal Helmet, Steal Armor, Steal Weapon



Vorap
Monster
Gemini
53
77
Behemoth










VolgraTheMoose
Male
Serpentarius
75
48
Knight
Basic Skill
Absorb Used MP
Halve MP
Teleport 2

Save the Queen
Gold Shield
Circlet
Light Robe
Cherche

Magic Break, Power Break, Mind Break, Night Sword
Accumulate, Wish

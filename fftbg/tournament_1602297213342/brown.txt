Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Phi Sig
Monster
Scorpio
63
54
Red Chocobo










Ruleof5
Female
Aquarius
77
63
Archer
Battle Skill
Abandon
Equip Bow
Ignore Terrain

Cute Bag
Escutcheon
Feather Hat
Power Sleeve
Red Shoes

Charge+1, Charge+3, Charge+20
Head Break, Armor Break, Weapon Break, Magic Break, Power Break, Mind Break, Justice Sword, Surging Sword



Brokenknight201
Male
Scorpio
75
65
Samurai
Black Magic
PA Save
Dual Wield
Jump+2

Kiyomori
Heaven's Cloud
Genji Helmet
Gold Armor
Battle Boots

Bizen Boat, Heaven's Cloud
Bolt, Bolt 4, Ice 2



Willjin
Male
Sagittarius
49
71
Thief
Item
Parry
Throw Item
Move+2

Rune Blade

Holy Miter
Adaman Vest
N-Kai Armlet

Steal Armor, Steal Shield, Steal Accessory, Steal Status, Arm Aim
Potion, X-Potion, Eye Drop, Echo Grass, Maiden's Kiss, Phoenix Down

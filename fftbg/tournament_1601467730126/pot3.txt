Final Bets: white - 13 bets for 14,205G (73.5%, x0.36); black - 6 bets for 5,125G (26.5%, x2.77)

white bets:
HaplessOne: 7,257G (51.1%, 7,257G)
latebit: 1,359G (9.6%, 67,991G)
Lolthsmeat: 1,269G (8.9%, 1,269G)
Mushufasa_: 1,234G (8.7%, 152,173G)
E_Ballard: 1,033G (7.3%, 1,033G)
khelor_: 495G (3.5%, 495G)
SephDarkheart: 444G (3.1%, 444G)
Lord_Gwarth: 376G (2.6%, 376G)
AllInBot: 255G (1.8%, 255G)
datadrivenbot: 200G (1.4%, 82,186G)
Sharosa: 140G (1.0%, 140G)
ayeCoop: 116G (0.8%, 116G)
scsuperstar: 27G (0.2%, 1,911G)

black bets:
Girfred: 1,353G (26.4%, 1,353G)
BirbBrainsBot: 1,000G (19.5%, 101,220G)
getthemoneyz: 1,000G (19.5%, 2,111,685G)
VolgraTheMoose: 904G (17.6%, 28,377G)
SeniorBunk: 552G (10.8%, 552G)
Lyner87: 316G (6.2%, 316G)

Player: !Red
Team: Red Team
Palettes: Red/Brown



Genkidou
Female
Sagittarius
60
63
Lancer
Yin Yang Magic
Catch
Magic Defense UP
Levitate

Holy Lance
Round Shield
Diamond Helmet
Platinum Armor
Rubber Shoes

Level Jump3, Vertical Jump5
Poison, Blind Rage, Foxbird, Confusion Song



CassiePhoenix
Female
Aquarius
39
78
Summoner
Elemental
MA Save
Short Charge
Fly

Battle Folio

Flash Hat
Linen Robe
Feather Boots

Carbunkle, Cyclops
Pitfall, Water Ball, Local Quake, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball



Gorgewall
Male
Taurus
62
80
Bard
Summon Magic
Earplug
Equip Sword
Move-MP Up

Asura Knife

Flash Hat
Reflect Mail
Angel Ring

Battle Song, Magic Song, Diamond Blade, Space Storage, Sky Demon
Moogle, Carbunkle, Fairy



Go2sleepTV
Male
Aries
75
50
Archer
Talk Skill
Speed Save
Doublehand
Move+1

Long Bow

Twist Headband
Black Costume
Battle Boots

Charge+3, Charge+7, Charge+10
Persuade, Threaten, Refute

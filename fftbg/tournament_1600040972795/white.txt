Player: !White
Team: White Team
Palettes: White/Blue



Brokenknight201
Male
Sagittarius
80
80
Archer
Punch Art
Parry
Doublehand
Teleport

Bow Gun

Twist Headband
Mythril Vest
Defense Armlet

Charge+2, Charge+4, Charge+5
Pummel, Chakra, Revive



MemoriesofFinal
Female
Scorpio
53
62
Mime

Auto Potion
Doublehand
Jump+1



Golden Hairpin
Leather Outfit
Feather Boots

Mimic




Panushenko
Male
Gemini
60
74
Lancer
Sing
Earplug
Attack UP
Fly

Spear
Genji Shield
Barbuta
Gold Armor
Feather Mantle

Level Jump8, Vertical Jump7
Hydra Pit



VolgraTheMoose
Female
Gemini
57
57
Priest
Item
Parry
Defense UP
Levitate

Oak Staff

Golden Hairpin
Leather Outfit
Battle Boots

Cure 4, Raise, Protect 2, Wall, Esuna
Potion, Eye Drop, Remedy, Phoenix Down

Player: !Brown
Team: Brown Team
Palettes: Brown/Green



HuffFlex
Female
Libra
51
64
Lancer
White Magic
Distribute
Concentrate
Move+2

Octagon Rod
Diamond Shield
Circlet
Linen Cuirass
Elf Mantle

Level Jump4, Vertical Jump7
Cure, Cure 2, Cure 4, Raise 2, Protect 2, Shell



Thyrandaal
Female
Virgo
76
65
Knight
Steal
Critical Quick
Equip Knife
Jump+1

Ice Rod
Crystal Shield
Genji Helmet
Bronze Armor
Germinas Boots

Armor Break, Speed Break, Power Break, Dark Sword, Explosion Sword
Steal Armor, Steal Status, Leg Aim



Gooseyourself
Male
Capricorn
59
69
Priest
Charge
Blade Grasp
Secret Hunt
Jump+2

Oak Staff

Red Hood
Black Robe
Spike Shoes

Cure 2, Cure 4, Esuna, Holy
Charge+4



RunicMagus
Female
Pisces
67
39
Samurai
Battle Skill
Absorb Used MP
Maintenance
Move+1

Murasame

Genji Helmet
Chameleon Robe
Magic Ring

Asura, Bizen Boat, Kiyomori, Muramasa
Shield Break, Mind Break, Stasis Sword, Justice Sword, Dark Sword

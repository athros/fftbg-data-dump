Player: !Red
Team: Red Team
Palettes: Red/Brown



Gorgewall
Male
Virgo
64
73
Squire
Summon Magic
Absorb Used MP
Equip Polearm
Teleport

Cypress Rod
Escutcheon
Circlet
Earth Clothes
Cursed Ring

Heal, Fury, Scream
Shiva, Carbunkle, Fairy, Cyclops



Bigbongsmoker
Monster
Scorpio
46
49
Ghost










Resjudicata3
Male
Gemini
63
51
Archer
Steal
Dragon Spirit
Doublehand
Teleport 2

Snipe Bow

Circlet
Earth Clothes
Germinas Boots

Charge+1, Charge+2, Charge+4
Gil Taking, Steal Armor, Steal Weapon, Steal Accessory, Steal Status, Arm Aim



RunicMagus
Male
Virgo
79
67
Squire
Steal
Parry
Beastmaster
Jump+1

Bow Gun
Escutcheon
Iron Helmet
Linen Cuirass
Magic Gauntlet

Heal, Yell, Wish
Gil Taking, Steal Heart, Steal Helmet, Steal Armor, Steal Shield, Steal Weapon, Leg Aim

Final Bets: purple - 8 bets for 3,423G (62.2%, x0.61); brown - 6 bets for 2,081G (37.8%, x1.64)

purple bets:
E_Ballard: 852G (24.9%, 117,088G)
Aldrammech: 700G (20.4%, 700G)
acid_flashback: 700G (20.4%, 1,755G)
SephDarkheart: 328G (9.6%, 5,547G)
CorpusCav: 300G (8.8%, 6,277G)
krombobreaker: 268G (7.8%, 6,124G)
killth3kid: 250G (7.3%, 998G)
Xignealx: 25G (0.7%, 154G)

brown bets:
DLJuggernaut: 996G (47.9%, 996G)
lowlf: 356G (17.1%, 2,326G)
highelectricaltemperature: 355G (17.1%, 4,569G)
Neuros91: 150G (7.2%, 845G)
getthemoneyz: 124G (6.0%, 1,813,383G)
FlavaFraze: 100G (4.8%, 100G)

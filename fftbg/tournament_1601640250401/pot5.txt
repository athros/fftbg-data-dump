Final Bets: blue - 10 bets for 7,246G (67.8%, x0.47); green - 6 bets for 3,436G (32.2%, x2.11)

blue bets:
SephDarkheart: 2,169G (29.9%, 4,339G)
HorusTaurus: 1,850G (25.5%, 1,850G)
LivingHitokiri: 1,000G (13.8%, 160,594G)
nifboy: 876G (12.1%, 5,065G)
VynxYukida: 472G (6.5%, 472G)
Ogema_theDream: 244G (3.4%, 1,249G)
datadrivenbot: 200G (2.8%, 81,794G)
BirbBrainsBot: 185G (2.6%, 176,329G)
bokurakira: 150G (2.1%, 1,043G)
Lord_Gwarth: 100G (1.4%, 1,822G)

green bets:
skipsandwiches: 1,001G (29.1%, 160,855G)
getthemoneyz: 1,000G (29.1%, 2,129,907G)
2b_yorha_b: 615G (17.9%, 615G)
Bryon_W: 520G (15.1%, 520G)
AllInBot: 200G (5.8%, 200G)
RunicMagus: 100G (2.9%, 21,882G)

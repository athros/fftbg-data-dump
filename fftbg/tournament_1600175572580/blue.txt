Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



E Ballard
Male
Leo
54
47
Oracle
Sing
Mana Shield
Short Status
Retreat

Iron Fan

Leather Hat
Chain Vest
108 Gems

Spell Absorb, Blind Rage, Dispel Magic, Paralyze, Sleep
Angel Song, Magic Song



ALY327
Female
Libra
55
79
Summoner
Steal
Damage Split
Short Charge
Jump+1

Flame Rod

Feather Hat
Brigandine
Jade Armlet

Moogle, Shiva, Golem, Silf
Gil Taking, Steal Armor, Steal Accessory



ZCKaiser
Male
Aries
66
56
Squire
Throw
Brave Save
Equip Knife
Retreat

Spell Edge
Crystal Shield
Leather Hat
Leather Armor
Elf Mantle

Accumulate, Heal, Yell, Cheer Up, Fury, Scream
Knife, Staff



Bongomon7
Female
Serpentarius
63
73
Summoner
Draw Out
Regenerator
Long Status
Move-HP Up

Gold Staff

Leather Hat
Silk Robe
Battle Boots

Moogle, Shiva, Ramuh, Ifrit, Carbunkle, Salamander, Silf, Cyclops
Koutetsu, Murasame, Kiyomori

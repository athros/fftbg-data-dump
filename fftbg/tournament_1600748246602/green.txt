Player: !Green
Team: Green Team
Palettes: Green/White



ANFz
Female
Sagittarius
48
80
Monk
Summon Magic
PA Save
Magic Defense UP
Jump+1



Leather Hat
Leather Outfit
Sprint Shoes

Spin Fist, Secret Fist, Purification, Revive
Moogle, Titan, Carbunkle, Salamander



Ericzubat
Male
Aries
63
55
Ninja
Charge
Mana Shield
Defend
Move-HP Up

Hidden Knife
Assassin Dagger
Feather Hat
Mystic Vest
Elf Mantle

Bomb, Staff
Charge+1, Charge+2, Charge+3, Charge+4, Charge+7, Charge+20



MemoriesofFinal
Female
Capricorn
68
69
Lancer
Battle Skill
Counter Tackle
Long Status
Move+2

Holy Lance
Gold Shield
Circlet
Chain Mail
Reflect Ring

Level Jump2, Vertical Jump8
Shield Break, Speed Break, Power Break, Stasis Sword, Night Sword, Surging Sword



Ribbiks
Male
Aries
56
52
Time Mage
Jump
Dragon Spirit
Equip Gun
Move+1

Battle Folio

Black Hood
White Robe
Reflect Ring

Haste, Slow, Float, Quick, Stabilize Time, Meteor
Level Jump4, Vertical Jump8

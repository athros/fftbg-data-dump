Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



Reddwind
Female
Libra
44
63
Knight
Summon Magic
Abandon
Long Status
Move+1

Battle Axe
Crystal Shield
Diamond Helmet
Gold Armor
Sprint Shoes

Weapon Break, Magic Break, Power Break, Mind Break, Stasis Sword
Moogle, Ifrit, Carbunkle, Silf, Fairy



Resjudicata3
Monster
Virgo
65
58
Tiamat










Killth3kid
Female
Pisces
51
79
Samurai
Elemental
Mana Shield
Equip Axe
Teleport

Slasher

Crystal Helmet
Diamond Armor
Jade Armlet

Asura, Bizen Boat, Murasame, Muramasa, Kikuichimoji, Chirijiraden
Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Sand Storm, Blizzard, Gusty Wind



Rislyeu
Female
Pisces
50
49
Time Mage
Black Magic
Counter Magic
Short Charge
Retreat

Sage Staff

Twist Headband
Wizard Outfit
Defense Armlet

Haste, Haste 2, Immobilize, Stabilize Time
Bolt 2, Bolt 4

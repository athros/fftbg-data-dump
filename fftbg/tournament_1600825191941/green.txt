Player: !Green
Team: Green Team
Palettes: Green/White



Virilikus
Female
Sagittarius
68
42
Samurai
Steal
Meatbone Slash
Equip Armor
Move+2

Partisan

Genji Helmet
Mystic Vest
Magic Gauntlet

Koutetsu, Bizen Boat, Heaven's Cloud
Steal Helmet, Steal Shield, Steal Accessory



Spartan Paladin
Male
Aquarius
52
61
Knight
Item
Meatbone Slash
Throw Item
Teleport

Ragnarok
Diamond Shield
Platinum Helmet
Linen Robe
Germinas Boots

Head Break, Armor Break, Shield Break, Power Break, Stasis Sword
Hi-Potion, Ether, Echo Grass



Gorgewall
Female
Aquarius
78
62
Time Mage
Talk Skill
Speed Save
Magic Defense UP
Move+1

Musk Rod

Triangle Hat
Light Robe
Vanish Mantle

Haste, Reflect, Demi, Demi 2, Stabilize Time, Galaxy Stop
Invitation, Death Sentence, Refute



Tripaplex
Monster
Scorpio
70
73
Floating Eye








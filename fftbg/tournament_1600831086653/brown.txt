Player: !Brown
Team: Brown Team
Palettes: Brown/Green



Krombobreaker
Male
Taurus
62
79
Mime

Absorb Used MP
Martial Arts
Move+1



Red Hood
Leather Outfit
N-Kai Armlet

Mimic




Roofiepops
Male
Aries
48
48
Mime

Counter
Dual Wield
Move+2



Golden Hairpin
Adaman Vest
Spike Shoes

Mimic




DaveStrider55
Male
Libra
52
43
Bard
White Magic
PA Save
Sicken
Move-MP Up

Ramia Harp

Twist Headband
Plate Mail
Power Wrist

Life Song, Magic Song, Last Song, Hydra Pit
Cure, Cure 3, Cure 4, Raise, Reraise, Regen, Wall, Esuna



Shalloween
Male
Virgo
58
53
Geomancer
Summon Magic
Meatbone Slash
Magic Defense UP
Teleport

Giant Axe
Diamond Shield
Twist Headband
Brigandine
Diamond Armlet

Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Moogle, Titan, Bahamut, Leviathan, Salamander, Silf, Lich

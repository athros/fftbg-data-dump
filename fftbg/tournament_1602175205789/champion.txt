Player: !zChamp
Team: Champion Team
Palettes: Purple/Yellow



DeathTaxesAndAnime
Female
Capricorn
77
56
Thief
Item
Parry
Throw Item
Jump+3

Kunai

Headgear
Judo Outfit
Magic Gauntlet

Steal Accessory, Arm Aim
Potion, Eye Drop, Holy Water, Remedy, Phoenix Down



GenZealot
Male
Pisces
73
43
Monk
Sing
PA Save
Sicken
Jump+1



Black Hood
Earth Clothes
Red Shoes

Spin Fist, Pummel, Earth Slash, Purification, Seal Evil
Space Storage



Evewho
Female
Cancer
66
81
Samurai
Battle Skill
HP Restore
Halve MP
Teleport 2

Spear

Diamond Helmet
Chain Mail
N-Kai Armlet

Asura, Koutetsu, Kiyomori, Muramasa
Head Break, Speed Break, Dark Sword, Explosion Sword



Galkife
Male
Serpentarius
55
76
Lancer
Battle Skill
Damage Split
Defend
Ignore Height

Gungnir
Hero Shield
Circlet
Diamond Armor
Bracer

Level Jump8, Vertical Jump5
Armor Break, Shield Break

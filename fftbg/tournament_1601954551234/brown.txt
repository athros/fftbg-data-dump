Player: !Brown
Team: Brown Team
Palettes: Brown/Green



ApplesNP
Male
Libra
54
77
Lancer
Item
Abandon
Martial Arts
Retreat


Aegis Shield
Gold Helmet
Reflect Mail
Bracer

Level Jump8, Vertical Jump6
Potion, Ether, Hi-Ether, Antidote, Eye Drop, Holy Water, Phoenix Down



Mesmaster
Male
Scorpio
49
61
Knight
Throw
Parry
Magic Defense UP
Waterwalking

Battle Axe
Ice Shield
Iron Helmet
Diamond Armor
Rubber Shoes

Weapon Break, Power Break
Shuriken, Bomb, Knife, Staff



Firesheath
Female
Gemini
72
52
Ninja
Talk Skill
Arrow Guard
Monster Talk
Levitate

Morning Star
Flail
Triangle Hat
Black Costume
Feather Mantle

Bomb, Ninja Sword
Threaten, Refute



Khelor
Female
Gemini
69
39
Lancer
Punch Art
Caution
Long Status
Move-HP Up

Javelin
Ice Shield
Bronze Helmet
White Robe
Bracer

Level Jump5, Vertical Jump5
Spin Fist, Earth Slash, Purification, Chakra, Revive

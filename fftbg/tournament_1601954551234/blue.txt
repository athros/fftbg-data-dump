Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Belkra
Male
Capricorn
56
44
Calculator
White Magic
MA Save
Short Status
Jump+2

Battle Folio

Feather Hat
Chain Vest
Feather Boots

CT, 5
Cure, Raise, Protect, Shell 2, Holy



GiggleStik
Male
Virgo
70
50
Lancer
Charge
MP Restore
Equip Axe
Move+3

Flail
Bronze Shield
Mythril Helmet
Chameleon Robe
Bracer

Level Jump5, Vertical Jump3
Charge+1, Charge+3, Charge+7



Maakur
Female
Aquarius
50
71
Squire
Talk Skill
Caution
Short Status
Move+3

Star Bag
Gold Shield
Circlet
Carabini Mail
Defense Armlet

Accumulate, Heal, Cheer Up, Wish
Threaten, Solution, Rehabilitate



Thyrandaal
Female
Virgo
68
38
Lancer
Summon Magic
Counter
Halve MP
Ignore Terrain

Partisan
Escutcheon
Gold Helmet
Crystal Mail
Bracer

Level Jump5, Vertical Jump6
Shiva, Ramuh, Salamander

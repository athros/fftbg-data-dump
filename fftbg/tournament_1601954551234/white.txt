Player: !White
Team: White Team
Palettes: White/Blue



RunicMagus
Monster
Aries
60
65
Holy Dragon










Dogsandcatsand
Female
Pisces
73
82
Wizard
Elemental
Caution
Equip Axe
Move-HP Up

Healing Staff

Thief Hat
Light Robe
Spike Shoes

Fire, Fire 3, Ice, Empower, Death
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Will-O-Wisp, Blizzard, Gusty Wind



MoonSugarFiend
Male
Aquarius
57
70
Archer
Item
Sunken State
Equip Armor
Retreat

Lightning Bow

Cross Helmet
Plate Mail
Cursed Ring

Charge+5, Charge+7
Ether, Eye Drop, Soft, Holy Water, Phoenix Down



ValensEXP
Male
Gemini
59
62
Squire
Elemental
Brave Save
Attack UP
Move-MP Up

Morning Star
Platinum Shield
Leather Hat
Power Sleeve
Spike Shoes

Accumulate, Tickle, Cheer Up, Fury, Wish
Will-O-Wisp, Quicksand, Blizzard, Gusty Wind, Lava Ball

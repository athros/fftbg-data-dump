Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



ApplesNP
Male
Taurus
66
45
Samurai
Basic Skill
Parry
Short Status
Move+3

Partisan

Circlet
Genji Armor
Germinas Boots

Kiyomori, Kikuichimoji
Accumulate, Tickle



ANFz
Male
Sagittarius
67
51
Bard
Yin Yang Magic
Auto Potion
Equip Polearm
Move+2

Cashmere

Feather Hat
Gold Armor
Angel Ring

Angel Song, Battle Song, Sky Demon, Hydra Pit
Poison, Spell Absorb, Zombie, Blind Rage, Foxbird, Confusion Song, Dispel Magic, Sleep



MrJamDango
Female
Libra
53
67
Oracle
Dance
MA Save
Equip Knife
Waterwalking

Rod

Leather Hat
Robe of Lords
Cursed Ring

Zombie, Silence Song, Foxbird, Confusion Song, Paralyze
Polka Polka, Void Storage



Phi Sig
Male
Scorpio
44
76
Bard
Summon Magic
Brave Save
Doublehand
Move+3

Ramia Harp

Triangle Hat
Genji Armor
Leather Mantle

Life Song, Nameless Song, Diamond Blade
Cyclops

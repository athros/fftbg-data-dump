Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Resjudicata3
Female
Capricorn
49
48
Wizard
Basic Skill
PA Save
Martial Arts
Retreat



Green Beret
Wizard Outfit
Rubber Shoes

Fire, Fire 2, Fire 4, Bolt 3, Bolt 4, Ice, Flare
Accumulate, Dash, Heal, Tickle, Yell, Fury



Zeridoz
Male
Pisces
55
45
Bard
Summon Magic
Arrow Guard
Short Charge
Waterbreathing

Fairy Harp

Golden Hairpin
Plate Mail
Wizard Mantle

Battle Song, Diamond Blade, Space Storage, Sky Demon
Moogle, Shiva, Ramuh, Golem, Carbunkle, Salamander, Fairy



J2DaBibbles
Male
Libra
41
77
Knight
Item
Sunken State
Throw Item
Lava Walking

Ragnarok
Flame Shield
Cross Helmet
Genji Armor
Power Wrist

Armor Break, Shield Break, Weapon Break, Magic Break, Power Break, Mind Break, Night Sword
X-Potion, Antidote, Eye Drop, Maiden's Kiss, Holy Water, Phoenix Down



Thyrandaal
Male
Pisces
54
71
Bard
Item
Arrow Guard
Throw Item
Retreat

Windslash Bow

Thief Hat
Chain Mail
Germinas Boots

Angel Song, Life Song, Battle Song, Magic Song, Nameless Song
Potion, X-Potion, Eye Drop, Echo Grass, Soft, Phoenix Down

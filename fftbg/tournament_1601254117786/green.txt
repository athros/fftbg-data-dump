Player: !Green
Team: Green Team
Palettes: Green/White



RunicMagus
Female
Sagittarius
64
57
Calculator
Time Magic
Brave Save
Equip Sword
Fly

Ragnarok

Flash Hat
Chain Vest
Salty Rage

CT, Height, 4, 3
Slow 2, Stop, Immobilize, Float, Demi, Stabilize Time, Meteor



Silentkaster
Male
Aquarius
72
66
Knight
Basic Skill
Damage Split
Magic Attack UP
Waterwalking

Battle Axe
Mythril Shield
Leather Helmet
Leather Armor
N-Kai Armlet

Head Break, Armor Break, Shield Break, Speed Break, Power Break, Stasis Sword
Accumulate, Dash, Tickle, Yell



E Ballard
Monster
Capricorn
76
47
Skeleton










Ghazzzzzz
Male
Aries
40
77
Priest
Steal
Caution
Equip Shield
Move-MP Up

Rainbow Staff
Round Shield
Thief Hat
Earth Clothes
Dracula Mantle

Cure 2, Protect, Protect 2, Shell, Esuna
Gil Taking, Steal Heart, Steal Helmet, Steal Shield, Steal Weapon, Steal Status

Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Krombobreaker
Male
Gemini
43
80
Knight
Jump
Faith Save
Equip Gun
Move+2

Glacier Gun
Crystal Shield
Diamond Helmet
Bronze Armor
Bracer

Weapon Break, Magic Break, Power Break, Mind Break
Level Jump3, Vertical Jump7



Gorgewall
Male
Libra
49
58
Geomancer
Talk Skill
Speed Save
Martial Arts
Lava Walking


Gold Shield
Green Beret
Mystic Vest
Defense Armlet

Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Blizzard, Gusty Wind
Persuade, Solution, Death Sentence, Negotiate, Refute, Rehabilitate



Fenaen
Male
Gemini
69
79
Chemist
Charge
HP Restore
Maintenance
Jump+3

Hydra Bag

Golden Hairpin
Wizard Outfit
Feather Boots

Potion, Hi-Potion, X-Potion, Soft, Phoenix Down
Charge+2, Charge+4, Charge+5, Charge+10



Chuckolator
Male
Pisces
74
73
Priest
Throw
Parry
Concentrate
Waterbreathing

Gold Staff

Triangle Hat
Leather Outfit
Diamond Armlet

Cure, Raise, Reraise, Protect 2, Shell, Wall, Esuna
Shuriken

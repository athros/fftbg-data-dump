Player: !Black
Team: Black Team
Palettes: Black/Red



Joewcarson
Male
Aries
79
52
Chemist
Time Magic
Brave Save
Dual Wield
Fly

Panther Bag
Star Bag
Ribbon
Mythril Vest
Feather Boots

Potion, Antidote, Eye Drop, Soft, Phoenix Down
Haste, Immobilize, Reflect, Quick, Stabilize Time



LDSkinny
Female
Aquarius
68
49
Oracle
White Magic
Auto Potion
Doublehand
Waterwalking

Cypress Rod

Golden Hairpin
Earth Clothes
Feather Mantle

Life Drain, Pray Faith, Silence Song, Blind Rage, Dispel Magic
Cure 2, Cure 4, Raise, Raise 2, Shell, Shell 2, Wall, Esuna



FoeSquirrel
Monster
Leo
52
61
Minotaur










Aldrammech
Female
Aries
54
61
Mediator
Black Magic
Earplug
Maintenance
Levitate

Glacier Gun

Barette
Wizard Outfit
Genji Gauntlet

Invitation, Praise, Threaten, Preach, Solution, Death Sentence, Rehabilitate
Fire 2, Fire 3, Bolt, Ice, Ice 3

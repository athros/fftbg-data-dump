Player: !Red
Team: Red Team
Palettes: Red/Brown



Moshyhero
Male
Leo
80
76
Samurai
Black Magic
PA Save
Maintenance
Jump+2

Kikuichimoji

Genji Helmet
Crystal Mail
Reflect Ring

Asura, Murasame, Masamune
Fire 3, Bolt, Bolt 4, Ice, Ice 4, Frog, Flare



ALY327
Female
Aries
60
55
Geomancer
Draw Out
MA Save
Defense UP
Teleport 2

Murasame
Mythril Shield
Green Beret
Black Costume
Magic Gauntlet

Pitfall, Water Ball, Hell Ivy, Local Quake, Static Shock, Will-O-Wisp, Quicksand, Sand Storm, Lava Ball
Koutetsu, Heaven's Cloud, Muramasa



Douchetron
Female
Aquarius
47
56
Knight
Throw
Abandon
Equip Armor
Swim

Ice Brand
Mythril Shield
Leather Hat
Brigandine
Bracer

Armor Break, Magic Break, Power Break, Justice Sword, Night Sword
Shuriken, Staff, Ninja Sword



Resjudicata3
Female
Cancer
70
41
Wizard
Steal
Parry
Short Charge
Move+1

Ice Rod

Black Hood
Black Robe
Cursed Ring

Fire 2, Bolt, Bolt 2, Bolt 4, Ice, Ice 2, Ice 3, Frog
Steal Heart, Steal Shield, Leg Aim

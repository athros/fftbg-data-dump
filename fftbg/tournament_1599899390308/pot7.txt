Final Bets: red - 14 bets for 8,971G (71.7%, x0.39); white - 7 bets for 3,543G (28.3%, x2.53)

red bets:
BaronHaynes: 3,083G (34.4%, 23,083G)
lijarkh: 1,287G (14.3%, 1,287G)
threedope: 1,009G (11.2%, 1,009G)
ArrenJevleth: 1,000G (11.1%, 9,380G)
ruleof5: 1,000G (11.1%, 18,891G)
Neech: 300G (3.3%, 1,344G)
ValensEXP_: 272G (3.0%, 1,415G)
Ogema_theDream: 220G (2.5%, 690G)
datadrivenbot: 200G (2.2%, 58,761G)
Moshyhero: 200G (2.2%, 4,473G)
AllInBot: 100G (1.1%, 100G)
BartTradingCompany: 100G (1.1%, 5,022G)
TheDapperChangeling: 100G (1.1%, 3,573G)
humbertogeovane: 100G (1.1%, 2,127G)

white bets:
ZPawZ: 1,000G (28.2%, 7,214G)
BirbBrainsBot: 828G (23.4%, 88,173G)
nifboy: 732G (20.7%, 9,172G)
upvla: 345G (9.7%, 3,208G)
getthemoneyz: 222G (6.3%, 1,920,389G)
ser_pyrro: 216G (6.1%, 216G)
otakutaylor: 200G (5.6%, 268G)

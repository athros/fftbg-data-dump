Player: !Red
Team: Red Team
Palettes: Red/Brown



Lifebregin
Female
Scorpio
73
68
Calculator
White Magic
Meatbone Slash
Doublehand
Jump+2

Madlemgen

Green Beret
Linen Robe
Chantage

Height, Prime Number, 4
Cure 2, Cure 4, Regen, Protect 2, Wall, Esuna



Starfire6789
Female
Virgo
67
70
Summoner
Punch Art
Arrow Guard
Maintenance
Move+2

Thunder Rod

Triangle Hat
Wizard Robe
Red Shoes

Moogle, Shiva, Ramuh, Ifrit, Titan, Carbunkle, Odin, Leviathan, Fairy, Cyclops
Wave Fist, Purification, Revive



Pepperbuster
Female
Aquarius
62
78
Knight
Yin Yang Magic
Speed Save
Martial Arts
Waterbreathing


Crystal Shield
Iron Helmet
Wizard Robe
Small Mantle

Weapon Break, Magic Break, Speed Break, Mind Break, Justice Sword, Dark Sword
Blind, Poison, Spell Absorb, Foxbird, Dispel Magic, Dark Holy



Superdevon1
Female
Libra
65
54
Time Mage
Black Magic
Catch
Magic Attack UP
Waterwalking

Rainbow Staff

Triangle Hat
Leather Outfit
Cursed Ring

Haste 2, Slow, Immobilize, Float, Reflect, Demi, Stabilize Time
Fire 2, Fire 3, Fire 4, Bolt 4, Ice

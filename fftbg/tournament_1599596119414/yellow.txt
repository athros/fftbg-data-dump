Player: !Yellow
Team: Yellow Team
Palettes: Yellow/Black



Mudrockk
Male
Libra
40
42
Calculator
Time Magic
Parry
Equip Gun
Retreat

Fairy Harp

Ribbon
Mystic Vest
Red Shoes

CT, Prime Number
Quick, Demi, Stabilize Time



Just Here2
Monster
Capricorn
75
54
Trent










Spartan Paladin
Male
Taurus
70
80
Knight
Elemental
Counter Tackle
Equip Gun
Jump+3

Mythril Gun
Gold Shield
Circlet
Gold Armor
Dracula Mantle

Head Break, Speed Break, Power Break, Justice Sword
Pitfall, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Will-O-Wisp, Gusty Wind, Lava Ball



Skillomono
Male
Capricorn
65
76
Samurai
Battle Skill
Mana Shield
Defense UP
Lava Walking

Kikuichimoji

Leather Helmet
Genji Armor
Bracer

Asura, Koutetsu, Heaven's Cloud
Mind Break, Dark Sword

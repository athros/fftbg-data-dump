Final Bets: purple - 8 bets for 2,315G (27.9%, x2.59); champion - 10 bets for 5,995G (72.1%, x0.39)

purple bets:
Zachara: 1,052G (45.4%, 149,052G)
ColetteMSLP: 350G (15.1%, 1,529G)
ValensEXP_: 300G (13.0%, 6,840G)
khelor_: 200G (8.6%, 3,053G)
datadrivenbot: 200G (8.6%, 59,995G)
gorgewall: 101G (4.4%, 15,116G)
DracoknightZero: 100G (4.3%, 1,841G)
getthemoneyz: 12G (0.5%, 1,943,835G)

champion bets:
upvla: 3,861G (64.4%, 3,861G)
Raixelol: 600G (10.0%, 19,322G)
BirbBrainsBot: 305G (5.1%, 149,916G)
MemoriesofFinal: 300G (5.0%, 7,897G)
gingerfoo69: 245G (4.1%, 245G)
Ogema_theDream: 232G (3.9%, 232G)
ayeayex3: 140G (2.3%, 140G)
threedope: 112G (1.9%, 112G)
BartTradingCompany: 100G (1.7%, 7,378G)
AllInBot: 100G (1.7%, 100G)

Final Bets: red - 8 bets for 3,144G (32.9%, x2.04); yellow - 11 bets for 6,407G (67.1%, x0.49)

red bets:
AllInBot: 853G (27.1%, 853G)
Raixelol: 600G (19.1%, 20,012G)
bruubarg: 500G (15.9%, 11,988G)
BirbBrainsBot: 399G (12.7%, 149,471G)
ValensEXP_: 300G (9.5%, 7,187G)
ser_pyrro: 240G (7.6%, 240G)
khelor_: 200G (6.4%, 2,968G)
getthemoneyz: 52G (1.7%, 1,943,857G)

yellow bets:
upvla: 2,590G (40.4%, 2,590G)
VolgraTheMoose: 1,001G (15.6%, 6,415G)
ColetteMSLP: 890G (13.9%, 890G)
Draconis345: 556G (8.7%, 556G)
jaykaye: 438G (6.8%, 438G)
Zachara: 216G (3.4%, 147,416G)
MemoriesofFinal: 200G (3.1%, 8,195G)
datadrivenbot: 200G (3.1%, 59,981G)
threedope: 112G (1.7%, 112G)
gingerfoo69: 104G (1.6%, 104G)
BartTradingCompany: 100G (1.6%, 7,371G)

Player: !zChamp
Team: Champion Team
Palettes: White/Blue



Reddwind
Male
Taurus
61
55
Knight
Summon Magic
Abandon
Short Status
Jump+2

Chaos Blade
Diamond Shield
Crystal Helmet
Crystal Mail
Genji Gauntlet

Weapon Break, Magic Break, Stasis Sword
Moogle, Ifrit, Titan, Golem, Carbunkle, Salamander



Resjudicata3
Male
Capricorn
47
44
Archer
Yin Yang Magic
Damage Split
Concentrate
Jump+2

Ultimus Bow

Twist Headband
Leather Outfit
Small Mantle

Charge+3, Charge+4, Charge+7
Blind, Pray Faith, Doubt Faith, Foxbird, Paralyze, Dark Holy



IaibOaob
Monster
Cancer
47
55
Red Chocobo










ALY327
Male
Leo
52
64
Ninja
Draw Out
Caution
Equip Gun
Move+1

Blaze Gun
Blast Gun
Feather Hat
Black Costume
Genji Gauntlet

Shuriken, Bomb, Ninja Sword
Asura, Koutetsu

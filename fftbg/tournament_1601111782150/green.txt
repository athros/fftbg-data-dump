Player: !Green
Team: Green Team
Palettes: Green/White



Bigbongsmoker
Female
Aquarius
72
41
Chemist
Punch Art
Auto Potion
Equip Gun
Retreat

Battle Folio

Triangle Hat
Black Costume
Sprint Shoes

Hi-Potion, X-Potion, Antidote, Phoenix Down
Spin Fist, Pummel, Earth Slash, Purification, Chakra



CassiePhoenix
Female
Serpentarius
77
80
Calculator
Yin Yang Magic
Distribute
Long Status
Jump+2

Bestiary

Flash Hat
Judo Outfit
Sprint Shoes

CT, Height, 5, 4, 3
Silence Song, Dispel Magic



Russell2x
Male
Taurus
59
75
Mime

Arrow Guard
Magic Defense UP
Ignore Terrain



Green Beret
Adaman Vest
Feather Mantle

Mimic




MemoriesofFinal
Male
Cancer
64
63
Bard
Yin Yang Magic
Dragon Spirit
Short Charge
Fly

Ramia Harp

Headgear
Brigandine
Leather Mantle

Angel Song, Cheer Song, Diamond Blade
Poison, Life Drain, Zombie, Blind Rage, Dispel Magic, Sleep, Dark Holy

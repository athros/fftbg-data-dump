Player: !White
Team: White Team
Palettes: White/Blue



HuffFlex
Female
Aquarius
55
78
Squire
Black Magic
PA Save
Concentrate
Fly

Slasher
Gold Shield
Iron Helmet
Power Sleeve
Small Mantle

Dash, Heal, Yell, Scream
Fire, Fire 2, Fire 3, Fire 4, Bolt 3, Ice 2



HorusTaurus
Male
Leo
50
43
Knight
Punch Art
Faith Save
Defend
Teleport

Battle Axe
Diamond Shield
Leather Helmet
Genji Armor
Defense Ring

Mind Break, Justice Sword, Dark Sword
Pummel, Secret Fist, Purification, Revive



Resjudicata3
Male
Sagittarius
65
63
Samurai
Item
Absorb Used MP
Secret Hunt
Fly

Muramasa

Iron Helmet
Leather Armor
Magic Gauntlet

Koutetsu, Heaven's Cloud, Kikuichimoji
Potion, X-Potion, Ether, Hi-Ether



ALY327
Male
Gemini
44
69
Ninja
Black Magic
Earplug
Magic Defense UP
Move-MP Up

Main Gauche
Flail
Triangle Hat
Clothes
Small Mantle

Bomb, Stick
Fire, Fire 2, Fire 4, Bolt 4, Ice 2, Ice 4, Death, Flare

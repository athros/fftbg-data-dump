Final Bets: white - 8 bets for 4,182G (46.5%, x1.15); black - 10 bets for 4,819G (53.5%, x0.87)

white bets:
BirbBrainsBot: 1,000G (23.9%, 169,590G)
OneHundredFists: 1,000G (23.9%, 17,375G)
NovaKnight21: 892G (21.3%, 892G)
getthemoneyz: 322G (7.7%, 2,082,235G)
ColetteMSLP: 300G (7.2%, 4,703G)
Zachara: 268G (6.4%, 151,668G)
AllInBot: 200G (4.8%, 200G)
resjudicata3: 200G (4.8%, 3,192G)

black bets:
DouglasDragonThePoet: 1,784G (37.0%, 1,784G)
Evewho: 1,154G (23.9%, 1,154G)
DHaveWord: 440G (9.1%, 440G)
HorusTaurus: 352G (7.3%, 352G)
bigbongsmoker: 352G (7.3%, 7,094G)
PopsLIVE: 224G (4.6%, 224G)
datadrivenbot: 200G (4.2%, 76,102G)
Lolthsmeat: 112G (2.3%, 112G)
gorgewall: 101G (2.1%, 2,552G)
MemoriesofFinal: 100G (2.1%, 1,111G)

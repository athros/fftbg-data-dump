Player: !Red
Team: Red Team
Palettes: Red/Brown



Otakutaylor
Female
Taurus
63
80
Chemist
Battle Skill
MP Restore
Martial Arts
Jump+2



Holy Miter
Earth Clothes
Defense Ring

Potion, Hi-Potion, Ether, Eye Drop, Echo Grass, Holy Water
Armor Break, Weapon Break, Speed Break, Power Break, Justice Sword



Lord Gwarth
Female
Aquarius
77
72
Priest
Math Skill
Brave Save
Maintenance
Retreat

Gold Staff

Headgear
Light Robe
108 Gems

Cure, Cure 3, Raise, Reraise, Regen, Protect, Protect 2, Shell, Shell 2, Holy
CT, Prime Number, 5, 4



BuffaloCrunch
Female
Virgo
56
72
Calculator
Black Magic
Sunken State
Magic Attack UP
Jump+1

Octagon Rod

Green Beret
Power Sleeve
Feather Boots

CT, Height, Prime Number, 5, 4
Bolt 2, Ice 2, Frog, Death



Powergems
Male
Serpentarius
82
78
Summoner
Yin Yang Magic
Critical Quick
Short Charge
Waterwalking

Papyrus Codex

Flash Hat
White Robe
Battle Boots

Moogle, Ramuh, Golem, Carbunkle, Odin, Cyclops
Poison, Spell Absorb, Zombie, Confusion Song, Sleep, Dark Holy

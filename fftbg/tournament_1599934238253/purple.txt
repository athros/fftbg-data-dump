Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Foxfaeez
Female
Scorpio
58
57
Ninja
Time Magic
Speed Save
Short Charge
Move+1

Hidden Knife
Sasuke Knife
Holy Miter
Judo Outfit
Cherche

Knife, Staff, Axe, Stick, Dictionary
Haste, Stop, Immobilize, Reflect, Demi, Demi 2



ZCKaiser
Male
Libra
80
41
Squire
Steal
Auto Potion
Attack UP
Move-HP Up

Snipe Bow
Bronze Shield
Leather Hat
Power Sleeve
Leather Mantle

Accumulate, Cheer Up, Scream
Gil Taking, Steal Accessory



CorpusCav
Male
Capricorn
57
53
Wizard
Jump
Brave Save
Short Charge
Jump+2

Rod

Holy Miter
White Robe
Feather Mantle

Empower, Death, Flare
Level Jump3, Vertical Jump2



Nekojin
Male
Capricorn
49
64
Mime

Auto Potion
Martial Arts
Move-MP Up



Leather Hat
Wizard Outfit
Sprint Shoes

Mimic


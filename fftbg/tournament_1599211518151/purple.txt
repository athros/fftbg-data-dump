Player: !Purple
Team: Purple Team
Palettes: Purple/Yellow



Seaweed B
Female
Libra
66
62
Geomancer
Draw Out
Meatbone Slash
Long Status
Move+2

Muramasa
Escutcheon
Cachusha
White Robe
Feather Mantle

Hell Ivy, Hallowed Ground, Will-O-Wisp, Quicksand, Sand Storm, Blizzard, Gusty Wind
Murasame, Muramasa



MemoriesofFinal
Female
Gemini
55
78
Mediator
Black Magic
Regenerator
Long Status
Move+3

Battle Folio

Cachusha
Judo Outfit
Dracula Mantle

Invitation, Praise, Threaten, Death Sentence, Negotiate, Refute
Bolt 3, Bolt 4, Ice 3, Empower, Death



Gripheenix
Female
Taurus
65
50
Time Mage
Talk Skill
Parry
Equip Gun
Move+2

Battle Folio

Barette
Linen Robe
Cursed Ring

Haste 2, Float, Reflect, Demi 2, Stabilize Time, Meteor
Invitation, Death Sentence, Insult, Mimic Daravon, Rehabilitate



Raixelol
Female
Capricorn
63
77
Geomancer
Jump
PA Save
Equip Axe
Move+1

Slasher
Round Shield
Golden Hairpin
White Robe
Genji Gauntlet

Pitfall, Hallowed Ground, Static Shock, Will-O-Wisp, Gusty Wind
Level Jump2, Vertical Jump7

Player: !Red
Team: Red Team
Palettes: Red/Brown



Kosaku Zephyr
Male
Cancer
46
67
Summoner
Punch Art
Sunken State
Martial Arts
Jump+1



Headgear
Clothes
Magic Ring

Moogle, Shiva, Carbunkle, Leviathan, Salamander, Silf
Spin Fist, Wave Fist, Secret Fist, Purification, Chakra, Revive, Seal Evil



Nhammen
Female
Aries
57
66
Geomancer
Draw Out
Caution
Halve MP
Teleport

Koutetsu Knife
Platinum Shield
Green Beret
Leather Outfit
Feather Mantle

Pitfall, Water Ball, Local Quake, Quicksand, Blizzard, Gusty Wind
Asura, Bizen Boat, Heaven's Cloud, Kikuichimoji



Heropong
Female
Taurus
61
57
Geomancer
Talk Skill
PA Save
Equip Bow
Move+2

Panther Bag
Mythril Shield
Triangle Hat
Linen Robe
Wizard Mantle

Pitfall, Water Ball, Hell Ivy, Hallowed Ground, Quicksand, Gusty Wind, Lava Ball
Praise, Threaten, Solution, Insult, Mimic Daravon, Refute



Wonser
Male
Taurus
77
59
Lancer
Throw
Counter Magic
Dual Wield
Levitate

Octagon Rod
Mythril Spear
Barbuta
Diamond Armor
Power Wrist

Level Jump8, Vertical Jump3
Staff, Wand

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Thyrandaal
Female
Gemini
46
48
Dancer
Summon Magic
Damage Split
Defense UP
Levitate

Dagger

Triangle Hat
Mystic Vest
Germinas Boots

Disillusion, Last Dance, Nether Demon, Dragon Pit
Ramuh, Golem, Leviathan



Krombobreaker
Female
Leo
69
42
Time Mage
Elemental
Mana Shield
Short Charge
Move-HP Up

White Staff

Flash Hat
Adaman Vest
Chantage

Haste, Haste 2, Immobilize, Demi 2, Stabilize Time, Meteor
Water Ball, Hell Ivy, Hallowed Ground, Local Quake, Static Shock, Quicksand, Gusty Wind, Lava Ball



Neerrm
Female
Aquarius
66
46
Oracle
Throw
HP Restore
Defense UP
Fly

Ivory Rod

Green Beret
Black Robe
Power Wrist

Poison, Spell Absorb, Doubt Faith, Zombie, Foxbird, Confusion Song, Dispel Magic, Paralyze
Shuriken, Bomb, Staff



Nekojin
Female
Libra
74
71
Samurai
Jump
Blade Grasp
Defense UP
Waterwalking

Kikuichimoji

Gold Helmet
White Robe
Leather Mantle

Asura, Murasame, Heaven's Cloud, Kikuichimoji
Level Jump2, Vertical Jump8

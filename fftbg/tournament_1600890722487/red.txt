Player: !Red
Team: Red Team
Palettes: Red/Brown



Old Overholt
Female
Libra
61
51
Priest
Punch Art
Faith Save
Beastmaster
Levitate

Wizard Staff

Ribbon
Silk Robe
Red Shoes

Cure, Raise, Raise 2, Regen, Shell 2, Wall, Esuna, Holy
Pummel, Wave Fist, Earth Slash, Purification, Chakra, Revive, Seal Evil



Gorgewall
Male
Leo
57
62
Ninja
Punch Art
Speed Save
Equip Shield
Fly

Kunai
Crystal Shield
Red Hood
Mystic Vest
108 Gems

Shuriken, Knife
Secret Fist, Purification, Chakra, Revive



Lord Gwarth
Female
Capricorn
43
71
Wizard
Math Skill
Abandon
Equip Gun
Fly

Battle Folio

Feather Hat
Linen Robe
Angel Ring

Fire, Fire 4, Bolt, Bolt 3, Ice 3, Ice 4, Empower, Death
CT, Prime Number, 5, 4, 3



Error72
Male
Gemini
67
39
Knight
Summon Magic
PA Save
Martial Arts
Waterbreathing

Ragnarok
Genji Shield
Cross Helmet
Reflect Mail
Defense Armlet

Head Break, Power Break, Mind Break, Stasis Sword
Moogle, Ifrit, Golem, Carbunkle, Leviathan, Salamander, Fairy

Player: !Blue
Team: Blue Team
Palettes: Blue/Purple



Nhammen
Female
Leo
42
57
Dancer
White Magic
Speed Save
Dual Wield
Levitate

Persia
Ryozan Silk
Twist Headband
Wizard Outfit
N-Kai Armlet

Wiznaibus, Polka Polka
Cure 2, Raise, Raise 2, Regen, Protect, Wall, Esuna



Laserman1000
Male
Libra
55
70
Archer
Elemental
MA Save
Sicken
Move-MP Up

Stone Gun
Gold Shield
Feather Hat
Brigandine
Dracula Mantle

Charge+2, Charge+4, Charge+7, Charge+10
Pitfall, Hallowed Ground, Will-O-Wisp, Blizzard, Gusty Wind



Mirapoix
Male
Sagittarius
52
77
Thief
Talk Skill
Counter Tackle
Equip Knife
Jump+2

Ice Rod

Headgear
Chain Vest
Reflect Ring

Steal Helmet, Steal Accessory, Steal Status, Leg Aim
Threaten, Death Sentence, Refute



Blakelmenakle
Female
Taurus
45
46
Lancer
White Magic
Counter
Defend
Waterwalking

Mythril Spear
Ice Shield
Circlet
Leather Armor
108 Gems

Level Jump2, Vertical Jump4
Cure, Raise, Raise 2, Protect 2, Shell, Esuna
